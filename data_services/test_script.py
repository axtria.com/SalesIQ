from src.job.executetalendscenario import ExecuteTalendScenario
from src.job.postgrestosfdc import PostgresToSfdc
from src.job.sfdctopostgres import SfdcToPostgres

if __name__ == '__main__':
    # Talend execute job
    job = ExecuteTalendScenario(scenario_id='abc', user_name='pooja.dhiman@brms.dev.com', password='axtria@123',
                                security_token='9GmDsLB7tAYm4vhm0mZ4UhJ0')
    job.invoke()

    # Postgres to SFDC
    job = PostgresToSfdc(dataset_id='', scenario_id='', table_name='', user_name='pooja.dhiman@brms.dev.com',
                         password='axtria@123',
                         security_token='9GmDsLB7tAYm4vhm0mZ4UhJ0', do_promote=False)

    # SFDC to postgres master data
    job = SfdcToPostgres(data_object_ids='a0f1N00000C3ujSQAR', scenario_id=None,
                         user_name='pooja.dhiman@brms.dev.com', password='axtria@123',
                         security_token='9GmDsLB7tAYm4vhm0mZ4UhJ0')
    job.invoke()

    # SFDC to postgres - scenario id
    job = SfdcToPostgres(data_object_ids='a0f1N00000C3wg2QAB', scenario_id='a1Y1N000004XTtmUAG',
                         user_name='pooja.dhiman@brms.dev.com', password='axtria@123',
                         security_token='9GmDsLB7tAYm4vhm0mZ4UhJ0')
    job.invoke()
