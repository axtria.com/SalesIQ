unmanaged_objects = ["SIQ_Prospects__c, Custom_Object__c"]   #custom ojects not part of managed package

unmanaged_fields = {} 
#custom object part of managed package but some of its custom fields not a part 
unmanaged_fields["Position_Geography__c"] = ["Quarter__c", "ZIP__c"]
unmanaged_fields["Custom_Object__c"] = ["Custom_field_1","Custom_field_2"]   