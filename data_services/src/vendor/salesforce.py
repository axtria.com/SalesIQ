import os
import re
import ast
import json
import logging
import requests
import pandas as pd

from time import sleep
from uuid import uuid4

from src.service.helpers import timer
from simple_salesforce import Salesforce
from salesforce_bulk import SalesforceBulk, CsvDictsAdapter

session  = requests.Session()

SF_VERSION_NEW = "43.0" 
BULK_V2_URI    = 'jobs/ingest/'


class SalesforceConnector:
    pool = {}

    def __init__(self, username, password, security_token, namespace, sandbox):
        self.namespace = namespace
        try:
            self.sf = Salesforce(username=username, password=password, security_token=security_token, sandbox=sandbox)
            self.bulk_sf = SalesforceBulk(username=username, password=password, security_token=security_token,
                                          sandbox=sandbox)
            self.sf_v2 = SalesForce_V2(self.sf)
        except Exception as e:
            logging.error(e)

    @classmethod
    def initialise_from_pool(cls, username: str, password: str, security_token: str, namespace: str, sandbox: bool):
        if username + password not in SalesforceConnector.pool:
            logging.debug("Registering new connection in pool for %s " % username)
            SalesforceConnector.pool[username + password] = cls(username, password, security_token, namespace, sandbox)
        return SalesforceConnector.pool.get(username + password)

    @timer
    def query(self, query: str):
        query = self._prepare_query(query)
        logging.info('\n\n %s \n' % query)
        records = self.sf.query_all(query)
        return self._prepare_df(records['records'])

    @timer
    def query_all(self, query: str) -> pd.DataFrame:
        query = self._prepare_query(query)
        logging.info('\n\n %s \n' % query)
        records = []
        headers = {'Authorization': 'Bearer ' + self.get_session()}
        response = requests.get(self.sf.base_url + 'queryAll/?q=' + query, headers=headers).json()
        records += response['records']
        while not response['done']:
            logging.debug("nextRecordsUrl - " + response['nextRecordsUrl'])
            response = requests.get(self.sf.base_url + 'query/' + response['nextRecordsUrl'].split('/')[-1],
                                    headers=headers).json()
            records += response['records']
        return self._prepare_df(records)

    @timer
    def bulk_download(self, object_name: str, query: str, filename=None, pk_chunking = 250000) -> str:
        query = self._prepare_query(query)
        logging.info('\n\n %s \n' % query)
        query_job = self.bulk_sf.create_query_job(object_name=self._prepare_object_name(object_name), contentType='CSV', pk_chunking=pk_chunking)
        batch = self.bulk_sf.query(query_job, query)
        if not pk_chunking:
            logging.debug("Downloading Without pk_chunking")
            self.bulk_sf.close_job(query_job)
            while not self.bulk_sf.is_batch_done(batch):
                sleep(10)
            logging.debug("Batch for %s complete." % object_name)
            if not filename:
                filename = str(uuid4())
            intermediate_csv_file = os.path.join('tmp', filename + '.csv')
            with open(intermediate_csv_file, mode='a', encoding='utf-8') as f:
                for result in self.bulk_sf.get_all_results_for_query_batch(batch):
                    for each in result:
                        f.write(each.decode().replace('""', ''))
            logging.debug("Write complete.")
            return os.path.abspath(intermediate_csv_file)
        a = 0
        logging.debug("Waiting for batch to finish processing")
        sleep(2)
        while a <=0:
            l = []
            batch_list = self.bulk_sf.get_batch_list(query_job)
            for i in batch_list:
                l.append(i['state'])
            # logging.debug(set(l))
            if 'NotProcessed' in l and 'InProgress' not in l and 'Queued' not in l:
                    a = 1
            if a != 1:
                logging.debug("Waiting for batch to finish processing")
                sleep(5)
        logging.debug("Batch for %s complete." % object_name)
        logging.debug("No of internal batches created = {}".format(len(batch_list)))
        valid_batches = []
        for i in batch_list:
            if int(i['numberRecordsProcessed']) > 0:
                valid_batches.append(i)
        logging.debug("Length of Valid batches = {}".format(len(valid_batches)))
        if not filename:
            filename = object_name+str(uuid4())
        intermediate_csv_file = os.path.join('tmp', filename + '.csv')
        if valid_batches:
            with open(intermediate_csv_file, mode='a', encoding='utf-8') as f:
                for i in valid_batches:
                    logging.debug("Retrieving records for batch : {}".format(i['id']))
                    result        = self.bulk_sf.get_all_results_for_query_batch(i['id'],query_job)
                    result_byte   = b''.join(next(result))
                    result_string = result_byte.decode().replace('""','')
                    f.write(result_string)
            df = pd.read_csv(intermediate_csv_file,low_memory=False, header=None)
            df.drop_duplicates(subset = df.columns.tolist(), keep='first', inplace=True)
            df.columns = df.iloc[0]
            df.drop(df.index[0], inplace=True)
            df.to_csv(intermediate_csv_file, index = False)
        else:
            with open(intermediate_csv_file,'a') as f:
                f.write("Records not found for this query")
            logging.debug("Creating empty file")
        logging.debug("Write complete.")
        self.bulk_sf.close_job(query_job)
        return os.path.abspath(intermediate_csv_file)

    # @timer
    # def bulk_download(self, object_name: str, query: str, filename=None) -> str:
    #     query = self._prepare_query(query)
    #     logging.info('\n\n %s \n' % query)
    #     query_job = self.bulk_sf.create_query_job(object_name=self._prepare_object_name(object_name), contentType='CSV')
    #     batch = self.bulk_sf.query(query_job, query)
    #     self.bulk_sf.close_job(query_job)
    #     while not self.bulk_sf.is_batch_done(batch):
    #         sleep(10)
    #     logging.debug("Batch for %s complete." % object_name)
    #     if not filename:
    #         filename = str(uuid4())
    #     intermediate_csv_file = os.path.join('tmp', filename + '.csv')
    #     with open(intermediate_csv_file, mode='a', encoding='utf-8') as f:
    #         for result in self.bulk_sf.get_all_results_for_query_batch(batch):
    #             for each in result:
    #                 f.write(each.decode().replace('""', ''))
    #     logging.debug("Write complete.")
    #     return os.path.abspath(intermediate_csv_file)

    @timer
    def bulk_upload(self, object_name: str, df: pd.DataFrame):
        try:
            df = df.drop('Id', axis=1)
        except Exception as e:
            logging.warning("Skipped Id column deletion.")
        if not df.empty:
            return self.bulk_v2_functions("insert", object_name, df)

    @timer
    def bulk_delete(self, object_name: str, df):
        if isinstance(df, pd.Series):
            logging.warning('to be deleted records found series, converting to dataframe.')
            df = df.to_frame('Id')
        if not df.empty:
            return self.bulk_v2_functions("delete", object_name, df)

    @timer
    def bulk_update(self, object_name, df):
        if not df.empty:
            return self.bulk_v2_functions("update", object_name, df)

    def bulk_v2_functions(self, operation: str, object_name: str, df: pd.DataFrame):
        object_name = self._prepare_object_name(object_name)
        df.columns = df.columns.str.replace(self.namespace,"")
        df.columns = [self.namespace+x if x.endswith('__c') else x for x in df.columns.tolist()]
        filepath = os.path.abspath(os.path.join('tmp', object_name+str(uuid4())+ ".csv"))
        # logging.debug(filepath)
        df.to_csv(filepath, index = False)
        FILESIZE = os.stat(filepath).st_size//(1024*1024)
        no_of_files = 1
        if FILESIZE>100:
            no_of_files = int(FILESIZE//100) + 1
        df = pd.read_csv(filepath)
        logging.debug("No of records = {}".format(len(df)))
        size = len(df)//no_of_files
        # logging.debug("No of Files = {}".format(no_of_files))
        for i in range(no_of_files):
            logging.debug("Upload Iteration : {}".format(i+1))
            job_id = self.sf_v2.create_v2_job(operation, object_name)
            # logging.debug("Job_id: {}".format(job_id))
            filepath = filepath[:filepath.find(".csv")].replace("___{}".format(i-1),"") + "___"+str(i)+filepath[filepath.find(".csv"):]
            if i == no_of_files - 1:
                df.iloc[i*size:].to_csv(filepath,index = False)
            else: 
                df.iloc[i*size:(i+1)*size].to_csv(filepath, index = False)
            try:
                upload_response = self.sf_v2.upload_v2(job_id, filepath)
                # logging.debug(upload_response)
                if upload_response.status_code != 201:
                    raise requests.HTTPError("Malformed Request. HTTPError{}".format(upload_response.status_code))
                patch_response = self.sf_v2.patch_v2_job(job_id,"UploadComplete")
                if isinstance(patch_response,list):
                    logging.debug("Patch Failed")
                    logging.debug(patch_response)
                    raise ValueError
                # logging.debug(patch_response)
                self.sf_v2.wait_for_v2_job(job_id)
                status = self.sf_v2.get_v2_job_status(job_id)
                logging.debug(status)
            except Exception as e:
                patch_response = self.sf_v2.patch_v2_job(job_id, "Aborted")
                logging.debug(e)
                raise e
                return("Exception Encountered")
        logging.debug("\n\n {} complete\n".format(operation))
        return status['numberRecordsFailed']
        # return True

    def update(self, object_name, object_id, key, value):
        logging.debug(" %r %r " % (key, value))
        getattr(self.sf, self._prepare_object_name(object_name)).update(object_id, {self.namespace + key: value})

    def compound_update(self, object_name, object_id, values):
        values = self._prepare_keys(values)
        logging.debug(values)
        getattr(self.sf, self._prepare_object_name(object_name)).update(object_id, values)

    def get_session(self):
        return self.sf.session_id

    def get_apex_url(self):
        return self.sf.apex_url

    def get_pg_col_name(self, sfdc_col_name, column_details):
        return column_details.loc[
            (column_details['Source_Column__c'] == self._prepare_object_name(sfdc_col_name)),
            'tb_col_nm__c'].reset_index().loc[
            0, 'tb_col_nm__c']

    def update_column(self, df, column_name, column_value):
        df[self._prepare_object_name(column_name)] = column_value
        return df

    def _prepare_query(self, query: str) -> str:
        query = query.replace(self.namespace, "")
        for custom_object in re.findall('( [0-9aA-zZ_]+__c| [0-9aA-zZ_]+__r)', query):
            query = query.replace(custom_object, ' ' + self._prepare_object_name(custom_object[1:]))
        for custom_object in re.findall('(\.[0-9aA-zZ_]+__c)', query):
            query = query.replace(custom_object, '.' + self._prepare_object_name(custom_object[1:]))
        for custom_object in re.findall('(\.[0-9aA-zZ_]+__r)', query):
            query = query.replace(custom_object, '.' + self._prepare_object_name(custom_object[1:]))
        return query

    def _prepare_df(self, records):
        df = pd.DataFrame(records)
        try:
            df.columns = df.columns.str.replace(self.namespace, "")
            df = df.drop('attributes', axis=1)
        except Exception as e:
            logging.warning("Replacement of namespace failed or dropping attributes column.")
        logging.info("No. of records fetched %d" % len(df))
        return df

    def _prepare_keys(self, values):
        result = {}
        for key, value in values.items():
            result[self.namespace + key] = value
        return result

    def _prepare_object_name(self, object_name):
        if object_name.find(self.namespace) == -1 and (object_name.endswith('__c') or object_name.endswith('__r')):
            return self.namespace + object_name
        return object_name


class SalesForce_V2:
    def __init__(self, sf):
        self.sf = sf
        self.uploaded = False
        self.RecordsProcessed = None
        self.RecordsFailed = None
        self.wait_time = 0
        self.base_url = self.new_base_url()
        
    def new_base_url(self):
        return self.sf.base_url.replace(self.sf.sf_version,SF_VERSION_NEW)

    def create_v2_job(self, operation: str, object_name: str):
        logging.debug("Creating {} job".format(operation))
        headers     = {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        body        = {
                        "object" : object_name,
                        "contentType" : "CSV",
                        "operation" : operation
                      }
        result      = session.request("POST", self.base_url + BULK_V2_URI, headers=headers, 
                                      data=json.dumps(body))
        response    = ast.literal_eval(result.content.decode("utf-8"))
        job_id      = response["id"]
        return job_id
    
    def upload_v2(self, job_id: str, CSV_filepath: str):
        logging.debug("Uploading CSV to SFDC ...")
        headers     = {
                        'Content-Type': 'text/csv',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        body        = open(CSV_filepath, 'r').read()
        self.wait_time = max(1,len(pd.read_csv(CSV_filepath,low_memory = False))/10000)
        result      = session.request("PUT", "{}{}{}/batches".format(self.base_url,BULK_V2_URI,job_id),
                                      headers=headers, data=body)
        self.uploaded = True
        return result
    
    def get_v2_job_status(self, job_id: str):
        logging.debug("Reading job status")
        headers     = {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        result      = session.request("GET", "{}{}{}/".format(self.base_url,BULK_V2_URI,job_id), headers=headers)
        response    = ast.literal_eval(result.content.decode("utf-8"))
        logging.debug(response['state'])
        return response
    
    def patch_v2_job(self, job_id: str, patch_type: str):
        logging.debug("Pushing Job patch. {}".format(patch_type))
        if patch_type == "UploadComplete" and not self.uploaded:
            logging.debug("Upload CSV first")
            raise ValueError
        headers     = {
                        'Content-Type': 'application/json; charset=UTF-8',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        body        = {
                        'state':patch_type
                      }
        result      = session.request("PATCH", "{}{}{}/".format(self.base_url,BULK_V2_URI,job_id),
                                      headers=headers, data=json.dumps(body))
        response    = ast.literal_eval(result.content.decode("utf-8"))
        return response
    
    def wait_for_v2_job(self, job_id: str):
        logging.debug("Waiting for SFDC to process records")
        flag = True
        while flag:
            state = self.get_v2_job_status(job_id)["state"]
            if (state != 'JobComplete') and (state != 'Failed'):
                sleep((self.wait_time/2) + 0.25)
            else:
                flag = False
        return
    
    def fetch_v2_job_failed_records(self, job_id):
        logging.debug("Fetching Failed Records")
        headers     = {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        result      = session.request("GET", "{}{}{}/failedResults".format(self.base_url,BULK_V2_URI,job_id),
                                      headers=headers)
        return result
    
    def fetch_v2_job_unprocessed(self, job_id):
        logging.debug("Fetching Unprocessed Records")
        headers     = {
                        'Content-Type': 'application/json;charset=UTF-8',
                        'Accept': 'application/json',
                        'Authorization': 'Bearer ' + self.sf.session_id,
                        'X-Prettylogging.debug': '1'
                      }
        result      = session.request("GET", "{}{}{}/unprocessedrecords".format(self.base_url,BULK_V2_URI,job_id),
                                      headers=headers)
        return result