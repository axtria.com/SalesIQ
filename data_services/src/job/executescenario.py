import logging
from datetime import datetime, timedelta
from pandas import DataFrame

import requests

from src.service.helpers import debugger
from src.service.register import register_job
from src.vendor.postgres import Postgres
from src.vendor.salesforce import SalesforceConnector


@register_job("ExecuteScenario")
class ExecuteScenario:
    def __init__(self, do_promote: bool, dataset_id: str, scenario_id: str, user_name: str, password: str,
                 security_token: str, table_name: str, namespace: str, is_delta: str, sandbox: bool,
                 compare_table_name: str, compare_table_filter: str, compare_dataset_id: str,
                 customer_univ_table_name: str, customer_univ_dataset_id: str):
        self.do_promote               = do_promote
        self.dataset_id               = None if dataset_id == '' else dataset_id
        self.scenario_id              = scenario_id
        self.table_name               = table_name
        self.is_delta                 = is_delta
        self.compare_table_name       = compare_table_name
        self.namespace                = namespace
        self.compare_table_filter     = compare_table_filter.replace("'","''")  #WHERE keyword is added in postgres. 
        self.compare_dataset_id       = compare_dataset_id
        self.customer_univ_table_name = customer_univ_table_name
        self.customer_univ_dataset_id = customer_univ_dataset_id
        self.salesforce               = SalesforceConnector.initialise_from_pool(user_name, password, security_token, namespace,sandbox)
        self.postgres                 = Postgres.initialise_from(self.salesforce)
        self.RRD                      = ' '					#Run Response Details

    def invoke(self):
        try:
            self.update_scenario_rule_execution_status('Rule Execution Initiated')           
            status = str(self.execute_scenario())
            if status.lower() == 'unsuccess':
                self.RRD = 'Scenario Execution Failed. Promote not invoked.'
                self.mark_error(self.RRD)
            elif status.lower() == '-2':
                self.RRD = 'Scenario Executed successfully but promote function failed.'
                self.mark_error(self.RRD)
            return self.RRD
        except Exception as e:
            logging.exception("Error")
            self.mark_error(str(e))
            return str(e)

    def update_scenario_rule_execution_status(self,value):
        self.salesforce.update('Scenario__c',self.scenario_id,'Rule_Execution_Status__c',value)

    def mark_error(self, e):
            self.salesforce.compound_update('Scenario__c', self.scenario_id, {
                'Rule_Execution_Status__c': 'Error',
                'Run_Response_Details__c': str(e)
            })

    def execute_scenario(self):
        df = self.postgres.return_output("select public.fn_execute_scenario("+repr(self.scenario_id)+",'Run',"
                                         "'{}','{}','{}','{}','{}','{}','{}','{}')".format(str(self.do_promote).upper(),
                                                                                               self.is_delta.upper(),
                                                                                               self.compare_dataset_id,
                                                                                               self.customer_univ_table_name,
                                                                                               self.customer_univ_dataset_id,
                                                                                               self.table_name,
                                                                                               self.dataset_id,
                                                                                               self.compare_table_filter))
        logging.debug("Execute Scenario Status : {}".format(df.at[0,0]))
        return df.at[0,0]
