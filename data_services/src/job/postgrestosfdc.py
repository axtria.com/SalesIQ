import logging
import traceback

import requests

from src.service.helpers import debugger
from src.service.register import register_job
from src.vendor.postgres import Postgres
from src.vendor.salesforce import SalesforceConnector


@register_job("PostgresToSfdc")
class PostgresToSfdc:
    def __init__(self, do_promote: bool, dataset_id: str, scenario_id: str, user_name: str, password: str,
                 security_token: str, table_name: str, namespace: str, sandbox: bool):
        self.do_promote = do_promote
        self.dataset_id = None if dataset_id == '' else dataset_id
        self.scenario_id = scenario_id
        self.table_name = table_name
        self.namespace = namespace
        self.salesforce = SalesforceConnector.initialise_from_pool(user_name, password, security_token, namespace, sandbox)
        self.postgres = Postgres.initialise_from(self.salesforce)

    def invoke(self):
        try:
            if self.is_scenario_valid():

                if not self.do_promote or not self.dataset_id:
                    logging.info("Skipping ... postgres to sfdc")
                    self.mark_success()
                    return

                data_object = self.fetch_data_object()
                # Attributes
                object_name = data_object.loc[0, 'Data_Set_Object_Name__c']
                column_details = self.fetch_column_details()

                filter_for_sfdc, filter_for_postgres = self.generate_filter_condition(column_details)

                to_be_deleted = self.salesforce.query(
                    "SELECT Id FROM {object_name} {filter_condition}".format(
                        object_name=object_name, filter_condition=filter_for_sfdc))
                self.salesforce.bulk_delete(object_name, to_be_deleted)

                # Download data from Postgres
                df = self.fetch_data_from_postgres(self.table_name, column_details, filter_for_postgres)
                self.salesforce.bulk_upload(object_name, df)
                self.mark_success_for(object_name)

        except Exception as e:
            logging.error(e)
            logging.error(traceback.format_exc())
            self.update_status(e)

    def fetch_team_instance(self):
        return self.salesforce.query(
            """SELECT IC_EffstartDate__c, IC_EffEndDate__c, Alignment_Period__c FROM Team_Instance__c 
            WHERE Scenario__c = '{scenario_id}'
            """.format(scenario_id=self.scenario_id))

    def fetch_data_from_postgres(self, table_name, column_details, filter_for_postgres):
        query = "SELECT " + ', '.join(column_details['tb_col_nm__c']) + " FROM " + table_name + \
                filter_for_postgres
        return self.postgres.download_data(query, column_details)

    def fetch_column_details(self):
        return self.salesforce.query("SELECT Source_Column__c, tb_col_nm__c "
                                     " FROM Data_Set_Column_Detail__c "
                                     " WHERE dataset_id__c = '{dataset_id}' "
                                     " AND Source_Column__c != null"
                                     " AND (NOT Source_Column__c LIKE '%.%')"
                                     .format(dataset_id=self.dataset_id))

    def fetch_data_object(self):
        return self.salesforce.query("SELECT Data_Set_Object_Name__c "
                                     " FROM Data_Set__c "
                                     " WHERE Id = '{dataset_id}'".format(dataset_id=self.dataset_id))

    @debugger
    def is_scenario_valid(self):
        scenario_status = self.salesforce.query(
            """SELECT Rule_Execution_Status__c FROM Scenario__c WHERE Id = '{scenario_id}'
            """.format(scenario_id=self.scenario_id)).at[0, 'Rule_Execution_Status__c']
        return scenario_status != 'Error'

    def current(self):
        pass

    def generate_filter_condition(self, column_details):
        query = """SELECT Filter_Expression_Source__c, Filter_Expression_Destination__c
        FROM Scenario_Data_Object_Map__c WHERE Data_set__c = '{dataset_id}'
        AND Scenario__c = '{scenario_id}'""".format(dataset_id=self.dataset_id, scenario_id=self.scenario_id)

        df_null_check = self.salesforce.query(query)
        if not df_null_check.at[0, 'Filter_Expression_Source__c']:
            raise ValueError
        filter_for_sfdc = " WHERE " + df_null_check.at[0, 'Filter_Expression_Source__c']
        filter_for_postgres = " WHERE " + df_null_check.at[0, 'Filter_Expression_Destination__c']
        return filter_for_sfdc, filter_for_postgres

    def update_status(self, e):
        self.salesforce.compound_update('Scenario__c', self.scenario_id, {
            'Rule_Execution_Status__c': 'Error',
            'Run_Response_Details__c': str(e)
            })
        self.mark_component()

    def mark_success(self):
        self.salesforce.compound_update('Scenario__c', self.scenario_id, {
            'Rule_Execution_Status__c': 'Success',
            'Run_Response_Details__c': ''
            })
        self.mark_component()

    def mark_success_for(self, object_name: str):
        if object_name.lower() == (self.namespace + "position_account__c").lower():
            url_prefix = self.salesforce.get_apex_url()
            namespace_fix = self.namespace[:-2] + '/' if self.namespace else ''
            base_url = url_prefix + namespace_fix + "salesiq/reinitiateMetricsAndEsri?scenarioId="
            logging.info(base_url + self.scenario_id)
            headers = {'Authorization': 'Bearer ' + self.salesforce.get_session()}
            response = requests.get(base_url + self.scenario_id, headers=headers)
        else:
            self.mark_success()
        self.mark_component()

    def mark_component(self):
        query1 = "select  sfdc_id,scenario_id, status from t_scn_rule_instance_details  where scenario_id = '{}'".format(self.scenario_id)
        query2 = "select sfdc_id, scenario_rule_instance_details,status from t_business_rule where \
                  scenario_rule_instance_details in (SELECT sfdc_id FROM t_scn_rule_instance_details \
                  where scenario_id = '{}')".format(self.scenario_id)
        query3 = "select sfdc_id,scenario_id,status from t_config_call_balancing_map where scenario_id = '{}'".format(self.scenario_id)

        df1 = self.postgres.return_output(query1)
        df2 = self.postgres.return_output(query2)
        df3 = self.postgres.return_output(query3)


        for i in range(len(df1)):
            self.salesforce.compound_update("Scenario_Rule_Instance_Details__c", df1.at[i,0],{
                "Scenario_Id__c" : df1.at[i,1],
                "Status__c" : df1.at[i,2]
                })
        for i in range(len(df2)):
            self.salesforce.compound_update("Business_Rules__c",df2.at[i,0],{
                "ScenarioRuleInstanceDetails__c" : df2.at[i,1],
                "Status__c" : df2.at[i,2]
                })
        for i in range(len(df3)):
            self.salesforce.compound_update("Config_Call_Balancing_Map__c",df3.at[i,0],{
                "Scenario__c" : df3.at[i,1],
                "Status__c" : df3.at[i,2]
                })