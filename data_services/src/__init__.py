import logging
from glob import glob
from logging.handlers import TimedRotatingFileHandler
from os.path import dirname, join, basename

logging.basicConfig(level=logging.DEBUG,
                    handlers=[TimedRotatingFileHandler("logs//system.log", when="midnight"), logging.StreamHandler()],
                    format='%(asctime)s - %(module)s.%(funcName)s() - %(levelname)s - %(message)s')
logging.getLogger('requests').setLevel(logging.CRITICAL)

folders = ['job']

for folder in folders:
    PWD = dirname(__file__)
    PWD = join(PWD, folder)
    PACKAGE_NAME = 'src.' + folder + '.'
    logging.info("Import package - %s", PACKAGE_NAME)

    for x in glob(join(PWD, '*.py')):
        if not x.startswith('__'):
            __import__(PACKAGE_NAME + basename(x)[:-3], globals(), locals())
