import logging

from src.service.helpers import thread
from src.service.register import REGISTERED_JOBS


class Job(object):
    def __init__(self):
        self._queue = []

    def build(self, job_name, params):
        self._queue.append(REGISTERED_JOBS[job_name](**params))

    # @thread
    def execute_all(self):
        for job in self._queue:
            logging.debug("\n\n-------------------------------------------------------------------- %s -------------------------------------------\n\n" % job.__class__.__name__)
            output = job.invoke()
            if output.strip():
                break
        return output


    def __del__(self):
        logging.debug('\nJobs Terminated\n\n\n\n\n')
