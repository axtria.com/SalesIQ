import logging
from logging.handlers import TimedRotatingFileHandler

from flask import Flask, request
from src.service.util import fetch_parameter
from src.gateway.job import Job

app = Flask(__name__)
PREFIX = '/data_services'
METHODS = ['POST','GET']

@app.route(PREFIX + "/brPullDataSources",methods=METHODS)
def sfdc_to_postgres():
    data_object_ids = fetch_parameter(request, 'dataObjectIds', default='1', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default=None, type=str)
    user_name = fetch_parameter(request, 'user_name', default='1', type=str)
    password = fetch_parameter(request, 'password', default='1', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='False', type=str)
    job = Job()
    sandbox = True if sandbox.lower() == 'true' else False
    job.build('SfdcToPostgres', {
        'data_object_ids': data_object_ids,
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox
    })
    job.execute_all()
    return " "


@app.route(PREFIX + "/brPushResultToDestination",methods=METHODS)
def postgres_to_sfdc():
    dataset_id = fetch_parameter(request, 'upstream_dataset_id', default='1', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default=None, type=str)
    do_promote = fetch_parameter(request, 'do_promote', default='false', type=str)
    user_name = fetch_parameter(request, 'user_name', default='1', type=str)
    password = fetch_parameter(request, 'password', default='1', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    table_name = fetch_parameter(request, 'upstream_table_name', default='', type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='False', type=str)
    job = Job()
    do_promote_bool = True if do_promote.lower() == 'true' else False
    sandbox = True if sandbox.lower() == 'true' else False
    job.build('PostgresToSfdc', {
        'dataset_id': dataset_id,
        'scenario_id': scenario_id,
        'table_name': table_name,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'do_promote': do_promote_bool,
        'namespace': namespace,
        'sandbox': sandbox
    })
    job.execute_all()
    return " "


@app.route(PREFIX + "/brExecuteScenario",methods=METHODS)
def execute_talend_job():
    data_object_ids = fetch_parameter(request, 'dataObjects_ids', default='1', type=str)
    do_promote = fetch_parameter(request, 'do_promote', default='false', type=str)
    upstream_dataset_id = fetch_parameter(request, 'upstream_dataset_id', default='', type=str)
    upstream_table_name = fetch_parameter(request, 'upstream_table_name', default='', type=str)
    user_name = fetch_parameter(request, 'user_name', default='', type=str)
    password = fetch_parameter(request, 'password', default='', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default=None, type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='False', type=str)
    job = Job()
    sandbox = True if sandbox.lower() == 'true' else False
    job.build('SfdcToPostgres', {
        'data_object_ids': data_object_ids,
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox
    })
    job.build('SfdcToPostgresMetadataCopy', {
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox
    })
    do_promote_bool = True if do_promote.lower() == 'true' else False
    job.build('PostgresToSfdc', {
        'do_promote': do_promote_bool,
        'dataset_id': upstream_dataset_id,
        'table_name': upstream_table_name,
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox
    })
    job.execute_all()
    return ' '


@app.route(PREFIX + "/brDeltaPullDataSources",methods=METHODS)
def delta_sfdc_to_postgres():
    data_object_ids = fetch_parameter(request, 'dataObjectIds', default='1', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default=None, type=str)
    user_name = fetch_parameter(request, 'user_name', default='1', type=str)
    password = fetch_parameter(request, 'password', default='1', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='False', type=str)
    logger_id = fetch_parameter(request, 'logger_id',default='',type=str)
    logger_object_name = fetch_parameter(request, 'logger_object_name', default = '', type=str)
    job = Job()
    sandbox = True if sandbox.lower() == 'true' else False
    job.build('DeltaSfdcToPostgres', {
        'data_object_ids': data_object_ids,
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox,
        'logger_id': logger_id,
        'logger_object_name': logger_object_name,
        'called_independently': True

    })
    job.execute_all()
    return " "


@app.route(PREFIX + "/brDeltaPushResultToDestination",methods=METHODS)
def delta_postgres_to_sfdc():
    dataset_id = fetch_parameter(request, 'upstream_dataset_id', default='1', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default=None, type=str)
    do_promote = fetch_parameter(request, 'do_promote', default='false', type=str)
    user_name = fetch_parameter(request, 'user_name', default='1', type=str)
    password = fetch_parameter(request, 'password', default='1', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    table_name = fetch_parameter(request, 'upstream_table_name', default='', type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='False', type=str)
    is_delta = fetch_parameter(request, 'isDelta', default='false', type=str)
    compare_table_name = fetch_parameter(request, 'compare_table_name', default='', type=str)
    compare_table_filter = fetch_parameter(request, 'compare_table_filter', default='', type=str)
    compare_dataset_id = fetch_parameter(request, 'compare_dataset_id', default='', type=str)
    customer_univ_table_name = fetch_parameter(request, 'customer_univ_table_name', default='', type=str)
    customer_univ_dataset_id = fetch_parameter(request, 'customer_univ_dataset_id', default='', type=str)

    job = Job()
    sandbox = True if sandbox.lower() == 'true' else False
    do_promote_bool = True if do_promote.lower() == 'true' else False
    job.build('DeltaPostgresToSfdc', {
        'dataset_id': dataset_id,
        'scenario_id': scenario_id,
        'table_name': table_name,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'do_promote': do_promote_bool,
        'namespace': namespace,
        'sandbox': sandbox,
        'is_delta': is_delta.lower(),
        'compare_table_name': compare_table_name,
        'compare_table_filter': compare_table_filter,
        'compare_dataset_id': compare_dataset_id,
        'customer_univ_table_name': customer_univ_table_name,
        'customer_univ_dataset_id': customer_univ_dataset_id
    })
    job.execute_all()
    return " "


@app.route(PREFIX + "/brDeltaExecuteScenario",methods=METHODS)
def delta_execute_talend_job():
    data_object_ids = fetch_parameter(request, 'dataObjects_ids', default='1', type=str)
    do_promote = fetch_parameter(request, 'do_promote', default='false', type=str)
    upstream_dataset_id = fetch_parameter(request, 'upstream_dataset_id', default='', type=str)
    upstream_table_name = fetch_parameter(request, 'upstream_table_name', default='', type=str)
    user_name = fetch_parameter(request, 'user_name', default='', type=str)
    password = fetch_parameter(request, 'password', default='', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default=None, type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    is_delta = fetch_parameter(request, 'isDelta', default='false', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='False', type=str)
    compare_table_name = fetch_parameter(request, 'compare_table_name', default='', type=str)
    compare_table_filter = fetch_parameter(request, 'compare_table_filter', default='', type=str)
    compare_dataset_id = fetch_parameter(request, 'compare_dataset_id', default='', type=str)
    customer_univ_table_name = fetch_parameter(request, 'customer_univ_table_name', default='', type=str)
    customer_univ_dataset_id = fetch_parameter(request, 'customer_univ_dataset_id', default='', type=str)
    logger_id = fetch_parameter(request, 'logger_id',default='',type=str)
    logger_object_name = fetch_parameter(request, 'logger_object_name', default = '', type=str)

    job = Job()
    sandbox = True if sandbox.lower() == 'true' else False
    job.build('DeltaSfdcToPostgres', {
        'data_object_ids': data_object_ids,
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox,
        'logger_id': logger_id,
        'logger_object_name': logger_object_name,
        'called_independently': False
    })
    job.build('SfdcToPostgresMetadataCopy', {
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox
    })
    do_promote_bool = True if do_promote.lower() == 'true' else False
    job.build('ExecuteScenario', {
        'do_promote': do_promote_bool,
        'dataset_id': upstream_dataset_id,
        'table_name': upstream_table_name,
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'is_delta': is_delta.lower(),
        'sandbox': sandbox,
        'compare_table_name': compare_table_name,
        'compare_table_filter': compare_table_filter,
        'compare_dataset_id': compare_dataset_id,
        'customer_univ_table_name': customer_univ_table_name,
        'customer_univ_dataset_id': customer_univ_dataset_id
    })
    job.build('PromoteFromPostgres', {
        'do_promote': do_promote_bool,
        'dataset_id': upstream_dataset_id,
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox
    })
    output = job.execute_all()
    logging.debug(output)
    return output

@app.route(PREFIX + "/brMasterSyncRaw",methods=METHODS)
def master_sync_raw():
    data_object_ids = fetch_parameter(request, 'dataObjectIds', default='1', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default=None, type=str)
    user_name = fetch_parameter(request, 'user_name', default='1', type=str)
    password = fetch_parameter(request, 'password', default='1', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='False', type=str)
    job = Job()
    sandbox = True if sandbox.lower() == 'true' else False
    job.build('Master_Sync_Raw', {
        'data_object_ids': data_object_ids,
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox
    })
    job.execute_all()
    return " "

@app.route(PREFIX + "/brManagedPullDataSources",methods=METHODS)
def managed_sfdc_to_postgres():
    data_object_ids = fetch_parameter(request, 'dataObjectIds', default='1', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default=None, type=str)
    user_name = fetch_parameter(request, 'user_name', default='1', type=str)
    password = fetch_parameter(request, 'password', default='1', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='True', type=str)
    job = Job()
    sandbox = True if sandbox.lower() == 'true' else False
    job.build('SfdcToPostgres', {
        'data_object_ids': data_object_ids,
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox
    })
    job.execute_all()
    return " "


@app.route(PREFIX + "/brManagedPushResultToDestination",methods=METHODS)
def managed_postgres_to_sfdc():
    dataset_id = fetch_parameter(request, 'upstream_dataset_id', default='1', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default=None, type=str)
    do_promote = fetch_parameter(request, 'do_promote', default='false', type=str)
    user_name = fetch_parameter(request, 'user_name', default='1', type=str)
    password = fetch_parameter(request, 'password', default='1', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    table_name = fetch_parameter(request, 'upstream_table_name', default='', type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='True', type=str)

    job = Job()
    do_promote_bool = True if do_promote.lower() == 'true' else False
    sandbox = True if sandbox.lower() == 'true' else False
    job.build('PostgresToSfdc', {
        'dataset_id': dataset_id,
        'scenario_id': scenario_id,
        'table_name': table_name,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'do_promote': do_promote_bool,
        'namespace': namespace,
        'sandbox': sandbox
    })
    job.execute_all()
    return " "


@app.route(PREFIX + "/brManagedExecuteScenario",methods=METHODS)
def managed_execute_talend_job():
    data_object_ids = fetch_parameter(request, 'dataObjects_ids', default='1', type=str)
    do_promote = fetch_parameter(request, 'do_promote', default='false', type=str)
    upstream_dataset_id = fetch_parameter(request, 'upstream_dataset_id', default='', type=str)
    upstream_table_name = fetch_parameter(request, 'upstream_table_name', default='', type=str)
    user_name = fetch_parameter(request, 'user_name', default='', type=str)
    password = fetch_parameter(request, 'password', default='', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default=None, type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='True', type=str)

    job = Job()
    sandbox = True if sandbox.lower() == 'true' else False
    job.build('SfdcToPostgres', {
        'data_object_ids': data_object_ids,
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox
    })
    job.build('SfdcToPostgresMetadataCopy', {
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox
    })
    do_promote_bool = True if do_promote.lower() == 'true' else False
    job.build('PostgresToSfdc', {
        'do_promote': do_promote_bool,
        'dataset_id': upstream_dataset_id,
        'table_name': upstream_table_name,
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox
    })
    job.execute_all()
    return ' '


@app.route(PREFIX + "/brManagedDeltaPullDataSources",methods=METHODS)
def managed_delta_sfdc_to_postgres():
    data_object_ids = fetch_parameter(request, 'dataObjectIds', default='1', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default=None, type=str)
    user_name = fetch_parameter(request, 'user_name', default='1', type=str)
    password = fetch_parameter(request, 'password', default='1', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='True', type=str)
    job = Job()
    sandbox = True if sandbox.lower() == 'true' else False
    job.build('DeltaSfdcToPostgres', {
        'data_object_ids': data_object_ids,
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox
    })
    job.execute_all()
    return " "


@app.route(PREFIX + "/brManagedDeltaPushResultToDestination",methods=METHODS)
def managed_delta_postgres_to_sfdc():
    dataset_id = fetch_parameter(request, 'upstream_dataset_id', default='1', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default=None, type=str)
    do_promote = fetch_parameter(request, 'do_promote', default='false', type=str)
    user_name = fetch_parameter(request, 'user_name', default='1', type=str)
    password = fetch_parameter(request, 'password', default='1', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    table_name = fetch_parameter(request, 'upstream_table_name', default='', type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='True', type=str)
    compare_table_name = fetch_parameter(request, 'compare_table_name', default='', type=str)
    compare_table_filter = fetch_parameter(request, 'compare_table_filter', default='', type=str)
    compare_dataset_id = fetch_parameter(request, 'compare_dataset_id', default='', type=str)
    customer_univ_table_name = fetch_parameter(request, 'customer_univ_table_name', default='', type=str)
    customer_univ_dataset_id = fetch_parameter(request, 'customer_univ_dataset_id', default='', type=str)


    job = Job()
    sandbox = True if sandbox.lower() == 'true' else False
    do_promote_bool = True if do_promote.lower() == 'true' else False
    job.build('DeltaPostgresToSfdc', {
        'dataset_id': dataset_id,
        'scenario_id': scenario_id,
        'table_name': table_name,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'do_promote': do_promote_bool,
        'namespace': namespace,
        'sandbox': sandbox,
        'compare_table_name': compare_table_name,
        'compare_table_filter': compare_table_filter,
        'compare_dataset_id': compare_dataset_id,
        'customer_univ_table_name': customer_univ_table_name,
        'customer_univ_dataset_id': customer_univ_dataset_id
    })
    job.execute_all()
    return " "


@app.route(PREFIX + "/brManagedDeltaExecuteScenario",methods=METHODS)
def managed_delta_execute_talend_job():
    data_object_ids = fetch_parameter(request, 'dataObjects_ids', default='1', type=str)
    do_promote = fetch_parameter(request, 'do_promote', default='false', type=str)
    upstream_dataset_id = fetch_parameter(request, 'upstream_dataset_id', default='', type=str)
    upstream_table_name = fetch_parameter(request, 'upstream_table_name', default='', type=str)
    user_name = fetch_parameter(request, 'user_name', default='', type=str)
    password = fetch_parameter(request, 'password', default='', type=str)
    security_token = fetch_parameter(request, 'security_token', default='', type=str)
    scenario_id = fetch_parameter(request, 'scenario_id', default=None, type=str)
    namespace = fetch_parameter(request, 'namespace', default='', type=str)
    is_delta = fetch_parameter(request, 'isDelta', default='false', type=str)
    sandbox = fetch_parameter(request, 'sandbox', default='True', type=str)
    compare_table_name = fetch_parameter(request, 'compare_table_name', default='', type=str)
    compare_table_filter = fetch_parameter(request, 'compare_table_filter', default='', type=str)
    compare_dataset_id = fetch_parameter(request, 'compare_dataset_id', default='', type=str)
    customer_univ_table_name = fetch_parameter(request, 'customer_univ_table_name', default='', type=str)
    customer_univ_dataset_id = fetch_parameter(request, 'customer_univ_dataset_id', default='', type=str)

    job = Job()
    sandbox = True if sandbox.lower() == 'true' else False
    job.build('DeltaSfdcToPostgres', {
        'data_object_ids': data_object_ids,
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox
    })
    job.build('SfdcToPostgresMetadataCopy', {
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'sandbox': sandbox
    })
    do_promote_bool = True if do_promote.lower() == 'true' else False
    job.build('DeltaPostgresToSfdc', {
        'do_promote': do_promote_bool,
        'dataset_id': upstream_dataset_id,
        'table_name': upstream_table_name,
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'namespace': namespace,
        'is_delta': is_delta.lower(),
        'sandbox': sandbox,
        'compare_table_name': compare_table_name,
        'compare_table_filter': compare_table_filter,
        'compare_dataset_id': compare_dataset_id,
        'customer_univ_table_name': customer_univ_table_name,
        'customer_univ_dataset_id': customer_univ_dataset_id
    })
    job.execute_all()
    return ' '


@app.before_request
def log_request_info():
    logging.debug('Request ::: %r' % request.url)


if __name__ == '__main__':
    app.debug = True
    logging.basicConfig(level=logging.DEBUG,
                        handlers=[TimedRotatingFileHandler("logs//system.log", when="midnight"),
                                  logging.StreamHandler()],
                        format='%(asctime)s - %(module)s.%(funcName)s() - %(levelname)s - %(message)s')

    app.run()
