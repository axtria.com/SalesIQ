import json

from src.boundary.job import Job
from src.vendor.salesforce import SalesforceConnector

with open('./config.json') as f:
    config = json.load(f)

source_client = SalesforceConnector(**config['source'])
target_client = SalesforceConnector(**config['target'])

job = Job()

for object_name, meta_data in config['sf_objects'].items():
    # Fetch object from source org.
    job.build('FetchData', {
        'client': source_client,
        'object_name': object_name,
        'columns': meta_data['columns'],
        'prefix': 'raw_'
    })

    # Merge Id and name to `Name` column.
    job.build('PrepareData', {
        'object_name': object_name,
        'input_prefix': 'raw_',
        'output_prefix': 'prepared_'
    })

    # Fetch data to be deleted from target org.
    job.build('FetchData', {
        'client': target_client,
        'object_name': object_name,
        'columns': 'Id',
        'prefix': 'to_delete_'
    })

    # Delete old records from target org.
    job.build('DeleteData', {
        'client': target_client,
        'object_name': object_name,
        'prefix': 'to_delete_'
    })

    # Logic to migrate Parent Id to child objects.
    job.build('ProcessData', {
        'object_name': object_name,
        'columns': meta_data['columns'],
        'relationship': meta_data['relationship'],
        'input_prefix': 'prepared_',
        'output_prefix': 'final_'
    })

    # Push the `final_` records to target org.
    job.build('PushData', {
        'client': target_client,
        'object_name': object_name,
        'prefix': 'final_'
    })

    # Fetch new Id from target org.
    job.build('FetchData', {
        'client': target_client,
        'object_name': object_name,
        'columns': meta_data['columns'],
        'prefix': 'updated_id_'
    })

    # Prepare local file with `Old_id` and `Id`.
    job.build('DetachId', {
        'object_name': object_name,
        'input_prefix': 'updated_id_',
        'output_prefix': ''
    })

    # Update based on Id in target org.
    job.build('UpdateData', {
        'client': target_client,
        'object_name': object_name,
        'prefix': ''
    })

job.execute_all()
