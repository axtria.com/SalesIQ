import logging
import os
from parser import ParserError

import pandas
from pandas import read_csv

from src.service.register import register_job


@register_job("PrepareData")
class PrepareData:
    def __init__(self, object_name, input_prefix: str, output_prefix: str):
        self.object_name = object_name
        self.input_file_name = os.path.join("tmp", input_prefix + object_name + ".csv")
        self.output_file_name = os.path.join("tmp", output_prefix + object_name + ".csv")

    def invoke(self):
        logging.debug(self.object_name)
        records = read_csv(self.input_file_name, encoding="ISO-8859-1")
        records['Name'] = records['Id'] + records['Name']
        records = records.drop('Id', axis=1)
        records.to_csv(self.output_file_name, index=False)
