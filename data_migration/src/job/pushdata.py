import logging
import os

from pandas import read_csv

from src.service.register import register_job
from src.vendor.salesforce import SalesforceConnector


@register_job("PushData")
class PushData:
    def __init__(self, client: SalesforceConnector, object_name, prefix: str):
        self.salesforce = client
        self.object_name = object_name
        self.input_file_name = os.path.join("tmp", prefix + object_name + '.csv')

    def invoke(self):
        df = read_csv(self.input_file_name)
        self.salesforce.bulk_upload(self.object_name, df)
