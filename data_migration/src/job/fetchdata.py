from src.service.register import register_job
from src.vendor.salesforce import SalesforceConnector


@register_job("FetchData")
class FetchData:
    def __init__(self, client: SalesforceConnector, object_name: str, columns: str, prefix: str):
        self.salesforce = client
        self.object_name = object_name
        self.columns = columns
        self.prefix = prefix

    def invoke(self):
        self.salesforce.bulk_download(self.object_name, self._make_query(), prefix=self.prefix)

    def _make_query(self):
        return """ SELECT {} FROM {} """.format(self.columns, self.object_name)