import os

from pandas import read_csv

from src.service.register import register_job


@register_job("DetachId")
class DetachId:
    def __init__(self, object_name: str, input_prefix: str, output_prefix: str):
        self.object_name = object_name
        self.input_file_name = os.path.join("tmp", input_prefix + object_name + ".csv")
        self.output_file_name = os.path.join("tmp", output_prefix + object_name + ".csv")

    def invoke(self):
        df = read_csv(self.input_file_name)

        df['Id_old'] = df['Name'].str[0:18]
        df['Name'] = df['Name'].str[18:]

        df.to_csv(self.output_file_name, index=False)