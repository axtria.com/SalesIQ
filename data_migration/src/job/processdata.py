import os

from pandas import read_csv, merge

from src.service.register import register_job


@register_job("ProcessData")
class ProcessData:
    def __init__(self, object_name: str, columns: str, relationship: dict, input_prefix: str, output_prefix: str):
        self.object_name = object_name
        self.relationship = relationship
        self.input_file_name = os.path.join("tmp", input_prefix + object_name + ".csv")
        self.output_file_name = os.path.join("tmp", output_prefix + object_name + ".csv")
        self.output_prefix = output_prefix

    def invoke(self):
        df = read_csv(self.input_file_name)

        # look for parent
        for column_name, object_name in self.relationship.items():
            parent_df = read_csv(os.path.join("tmp", object_name + ".csv"))
            merged_df = merge(df[[column_name]], parent_df[['Id_old', 'Id']],
                              left_on=column_name, right_on='Id_old', how='left')
            df[column_name] = merged_df["Id"]

        df.to_csv(self.output_file_name, index=False)
