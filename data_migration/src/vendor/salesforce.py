import logging
import os
from threading import Thread
from time import sleep

import pandas as pd
from salesforce_bulk import SalesforceBulk, CsvDictsAdapter

from src.service.helpers import timer


class SalesforceConnector:
    def __init__(self, username: str, password: str, security_token: str):
        self.bulk_sf = SalesforceBulk(username=username, password=password, security_token=security_token)

    @timer
    def bulk_download(self, object_name: str, query: str, prefix=''):
        logging.info('\n\n %s \n' % query)
        try:
            query_job = self.bulk_sf.create_query_job(object_name=object_name, contentType='CSV')
            batch = self.bulk_sf.query(query_job, query)
            self.bulk_sf.close_job(query_job)
            while not self.bulk_sf.is_batch_done(batch):
                sleep(10)
            logging.debug("Batch for %s complete." % object_name)

            intermediate_csv_file = os.path.join("tmp", prefix + object_name + '.csv')
            with open(intermediate_csv_file, mode='a', encoding='utf-8') as f:

                for result in self.bulk_sf.get_all_results_for_query_batch(batch):
                    for each in result:
                        f.write(each.decode())
            logging.debug("Write complete.")

            # df = pd.read_csv(intermediate_csv_file, encoding="ISO-8859-1")
            # logging.info("Number of records fetched from SFDC: %d" % len(df))
            # os.remove(intermediate_csv_file)
            return True
        except Exception as e:
            logging.error(e)

    @timer
    def bulk_upload(self, object_name: str, df: pd.DataFrame):
        try:
            logging.debug("Removing Id column ... ")
            df = df.drop('Id', axis=1)
        except ValueError:
            logging.warning("Id column not found in uploading data.")
        try:
            tasks = []

            batch_size = 10000
            threads = int(len(df) / batch_size)

            for i in range(threads):
                start_index = batch_size * i
                end_index = batch_size * (i + 1)
                tasks.append(Thread(target=self._bulk_upload_limit, args=(object_name, df[start_index:end_index])))
            # Extra task to complete left over records
            if batch_size * threads < len(df):
                logging.debug("Pushing extra task in thread ...")
                tasks.append(Thread(target=self._bulk_upload_limit, args=(object_name, df[batch_size * threads:])))

            # Start all threads
            for task in tasks:
                task.start()

            # Wait for all of them to finish
            for task in tasks:
                task.join()

            return True
        except Exception as e:
            logging.error(e)
            return False

    def _bulk_upload_limit(self, object_name, df):
        job = self.bulk_sf.create_insert_job(object_name, contentType='CSV', concurrency='Parallel')
        csv_iter = CsvDictsAdapter(iter(df.to_dict(orient='records')))
        batch = self.bulk_sf.post_batch(job, csv_iter)
        self.bulk_sf.wait_for_batch(job, batch)
        self.bulk_sf.close_job(job)

    @timer
    def delete(self, object_name, df):
        try:
            tasks = []

            batch_size = 10000
            threads = int(len(df) / batch_size)

            for i in range(threads):
                start_index = batch_size * i
                end_index = batch_size * (i + 1)
                tasks.append(Thread(target=self._bulk_delete_limit, args=(object_name, df[start_index:end_index])))
            # Extra task to complete left over records
            if batch_size * threads < len(df):
                logging.debug("Pushing extra task in thread ...")
                tasks.append(Thread(target=self._bulk_delete_limit, args=(object_name, df[batch_size * threads:])))

            # Start all threads
            for task in tasks:
                task.start()

            # Wait for all of them to finish
            for task in tasks:
                task.join()

            return True
        except Exception as e:
            logging.error(e)
            return False

    def _bulk_delete_limit(self, object_name, df):
        job = self.bulk_sf.create_delete_job(object_name, contentType='CSV', concurrency='Parallel')
        csv_iter = CsvDictsAdapter(iter(df.to_dict(orient='records')))
        batch = self.bulk_sf.post_batch(job, csv_iter)
        self.bulk_sf.wait_for_batch(job, batch)
        self.bulk_sf.close_job(job)

    @timer
    def bulk_update(self, object_name, df):
        try:
            tasks = []

            batch_size = 10000
            threads = int(len(df) / batch_size)

            for i in range(threads):
                start_index = batch_size * i
                end_index = batch_size * (i + 1)
                tasks.append(Thread(target=self._bulk_update_limit, args=(object_name, df[start_index:end_index])))
            # Extra task to complete left over records
            if batch_size * threads < len(df):
                logging.debug("Pushing extra task in thread ...")
                tasks.append(Thread(target=self._bulk_update_limit, args=(object_name, df[batch_size * threads:])))

            # Start all threads
            for task in tasks:
                task.start()

            # Wait for all of them to finish
            for task in tasks:
                task.join()

            return True
        except Exception as e:
            logging.error(e)
            return False

    def _bulk_update_limit(self, object_name, df):
        job = self.bulk_sf.create_update_job(object_name, contentType='CSV', concurrency='Parallel')
        csv_iter = CsvDictsAdapter(iter(df.to_dict(orient='records')))
        batch = self.bulk_sf.post_batch(job, csv_iter)
        self.bulk_sf.wait_for_batch(job, batch)
        self.bulk_sf.close_job(job)
