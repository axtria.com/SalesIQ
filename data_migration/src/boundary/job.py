import logging

from src.service.register import REGISTERED_JOBS


class Job:
    def __init__(self):
        self._queue = []

    def build(self, job_name, params):
        self._queue.append(REGISTERED_JOBS[job_name](**params))

    def execute_all(self):
        for job in self._queue:
            logging.debug("---------------- %s ---------------" % job.__class__.__name__)
            job.invoke()