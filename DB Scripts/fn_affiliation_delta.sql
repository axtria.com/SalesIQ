-- FUNCTION: public.fn_affiliation_delta(text, text)

-- DROP FUNCTION public.fn_affiliation_delta(text, text);

CREATE OR REPLACE FUNCTION public.fn_affiliation_delta(
	p_t_scn_id text,
	p_t_customer_master_scn_inst_id text)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE
l_t_fn_name text := 'fn_affiliation_delta';
l_t_affiliation_scn_instance_id text;
l_t_scn_id text :=p_t_scn_id ; 
l_t_customer_master_table  text  ;
l_t_customer_master_scn_inst_id text := p_t_customer_master_scn_inst_id;
l_t_affiliation_ds_instance_id text;
l_t_aff_dataset_id text;
l_t_aff_table_name text;
l_t_data_filter text;
l_t_str_insert_data text;
l_t_account_id text; 
l_i_hierarchy_level integer := 6 ;
i_counter integer:=1;
l_t_aff_network_id text;
l_t_aff_network_parent_id text;
l_t_output_customer_master_ds text;
l_t_colm text;
l_t_colm2 text;
l_t_data_filter_customer_master text;
l_r_comp_label record;
l_t_br_stage text; 
l_t_error text;
l_t_input_table text;
l_t_output_table text;
l_t_input_customer_master_ds text;
l_t_aff_table_name_copy text;
l_t_sysmod_date text;
l_t_filter_final  text;
l_t_filter text;
i_count integer:=1;
begin
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
		 VALUES (l_t_scn_id, 'Affiliation Delta Scenario_Rule_Instance ID - '||l_t_affiliation_scn_instance_id, '47', l_t_fn_name);
		 
		raise notice 'Fetching the Delta Aff Network for Delta accounts begin';
		SELECT distinct srid.sfdc_id AS scn_rule_instance_details_id
		 FROM t_scn_rule_instance_details as srid, t_component_type_master as ctm
		 WHERE srid.scenario_id = l_t_scn_id
		 AND srid.component_type_master = ctm.sfdc_id
		 and lower (srid.component_type_label)='affiliation'
		into l_t_affiliation_scn_instance_id ;
		raise notice 'Affiliation Delta Scenario_Rule_Instance ID = %',l_t_affiliation_scn_instance_id ;
		
		
		SELECT distinct srid.sfdc_id 
			 FROM t_scn_rule_instance_details as srid, t_component_type_master as ctm
		WHERE srid.scenario_id = l_t_scn_id 
		  AND srid.component_type_master = ctm.sfdc_id
		  and lower(srid.component_type_label) in ('affiliation network' , 'account affiliation')
		into l_t_affiliation_ds_instance_id;
		raise notice 'l_t_affiliation_ds_instance_id - > %',l_t_affiliation_ds_instance_id;
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
		 VALUES (l_t_scn_id, 'Affiliation Delta Dataset Id - '||l_t_affiliation_ds_instance_id, '47', l_t_fn_name);
		
		SELECT DISTINCT drm.dataset_id,  drm.table_name
		 FROM t_dataset ds, t_dataset_rule_map drm
		WHERE ds.sfdc_id = drm.dataset_id 
		  AND drm.scenario_rule_instance_id =l_t_affiliation_ds_instance_id
			 and drm.ds_type='I'
			 and ds.is_internal='false'
		into l_t_aff_dataset_id , l_t_aff_table_name;
		raise notice 'l_t_aff_dataset_id -> %, l_t_aff_table_name -> %',l_t_aff_dataset_id , l_t_aff_table_name;
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
		 VALUES (l_t_scn_id, 'Affiliation Delta Table - '||l_t_aff_table_name, '47', l_t_fn_name);
		
		select tsd.filter_expression_destn 
		  from t_scn_data_object_map tsd, t_dataset ds
		where tsd.data_set = ds.base_data_set
		and ds.sfdc_id =  l_t_aff_dataset_id 
		and scenario = l_t_scn_id 
		into l_t_data_filter;
		
		raise notice 'l_t_data_filter -> %',l_t_data_filter;
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
		 VALUES (l_t_scn_id, 'Affiliation Delta filter - '||l_t_data_filter, '47', l_t_fn_name);
		
		-- Get Dataset id and table name for customer master output table
		select dataset_id,  drm.table_name
			from t_dataset_rule_map drm
		where scenario_rule_instance_id= l_t_customer_master_scn_inst_id
		and ds_type='O'
		into l_t_output_customer_master_ds , l_t_output_table;
		
		-- Get columns for the customer master output table
		SELECT string_agg(tb_col_nm, ',' order by column_order) 
                  FROM t_ds_col_detail 
		WHERE dataset_id =  l_t_output_customer_master_ds 
				 INTO l_t_colm;
				  
		-- dataset id for customer master input table t_fin_d0		  
		select dataset_id , drm.table_name
				from t_dataset_rule_map drm 
		where scenario_rule_instance_id= l_t_customer_master_scn_inst_id 
				and ds_type='I'
				into l_t_input_customer_master_ds , l_t_input_table;
				raise notice 'l_t_input_customer_master_ds-> %,l_t_input_table -> %', l_t_input_customer_master_ds , l_t_input_table;
		
		-- column names for the customer master input table
		SELECT string_agg(tb_col_nm, ',' order by column_order) 
                  FROM t_ds_col_detail 
        WHERE dataset_id =  l_t_input_customer_master_ds 
                  INTO l_t_colm2;
		  
		l_t_customer_master_table :=		l_t_output_table ;
				/* Filter condition added to pull external data according to filter*/
		
		--- fetch the filter criteria for the customer master t_fin_do table
		select tsd.filter_expression_destn 
			from t_scn_data_object_map tsd, t_dataset ds
		where tsd.data_set = ds.base_data_set
		and ds.sfdc_id =  l_t_input_customer_master_ds 
		and scenario = l_t_scn_id 
		into l_t_data_filter_customer_master;     	
		raise notice 'l_t_data_filter_customer_master - > %',l_t_data_filter_customer_master;
		raise notice 'l_t_input_customer_master_ds -> %',l_t_input_customer_master_ds;
		
		-- last mod stamp date need to be removed from the filter criteria, so fetch the column name for that
		l_t_str_insert_data := 'SELECT tb_col_nm from t_ds_col_detail where dataset_id = '''||l_t_input_customer_master_ds   ||''' and (lower(source_column) = ''axtriasalesiqtm__systemmodstamp'' or LOWER(source_column) = ''systemmodstamp'')' ;
		raise notice 'l_t_str_insert_data -> %' , l_t_str_insert_data;
		execute l_t_str_insert_data into l_t_sysmod_date;
		raise notice 'l_t_sysmod_date -> %', l_t_sysmod_date;
		
				
		select tsd.filter_expression_destn 
			from t_scn_data_object_map tsd, t_dataset ds
		where tsd.data_set = ds.base_data_set
		and ds.sfdc_id =  l_t_input_customer_master_ds 
		and scenario = l_t_scn_id
		and tsd.filter_expression_destn like '%'||l_t_sysmod_date||'%' 
		into l_t_filter;  
		
		if l_t_data_filter_customer_master is null then
				l_t_data_filter_customer_master = ' 1=1';
		end if;
		--removing last modified date from the where clause
		raise notice 'l_t_data_filter_customer_master   - > %',l_t_data_filter_customer_master  ;
		raise notice 'l_t_sysmod_date -> %',l_t_sysmod_date;
		raise notice 'l_t_filter -> %', l_t_filter;
		if l_t_filter = l_t_data_filter_customer_master then 
			select substr(l_t_data_filter_customer_master  , 1, position(l_t_sysmod_date  in l_t_data_filter_customer_master  )-5)  into l_t_filter_final ;
		else 
			l_t_filter_final :=l_t_data_filter_customer_master ;
		end if;
		raise notice 'l_t_filter_final -> %',l_t_filter_final;
		
		/*l_t_str_insert_data := 'select substr('||l_t_data_filter_customer_master||'  , 1, position('||l_t_sysmod_date ||' in '||l_t_data_filter_customer_master||'  )-5) ' ;
		raise notice 'l_t_str_insert_data -> %',l_t_str_insert_data ;
		execute  l_t_str_insert_data into l_t_filter_final;*/
				
		
		raise notice 'l_t_filter_final -> %', l_t_filter_final;
		
		--return 23;
		
		 ---Create a copy of the affiliation network
		l_t_aff_table_name_copy := l_t_aff_table_name||'_1';
		l_t_str_insert_data := 'Create temp table '|| l_t_aff_table_name_copy	||'   as select * from '|| l_t_aff_table_name ||' where '||l_t_data_filter ;
		raise notice 'Create a copy of the affiliation network-> %',l_t_str_insert_data;
		Execute 'drop table if exists '||l_t_aff_table_name_copy;
			
		Execute l_t_str_insert_data;
		
		l_t_str_insert_data := ' SELECT tb_col_nm  FROM t_ds_col_detail WHERE dataset_id = '''||l_t_output_customer_master_ds ||'''  AND (LOWER(source_column) = ''axtriasalesiqtm__id'' or LOWER(source_column) = ''id'') ';
		raise notice 'Fetch Id Column for Customer Master table l_t_str_insert_data -> %',l_t_str_insert_data;
		Execute l_t_str_insert_data into 	l_t_account_id ;
		raise notice 'Fetch Id Column for Customer Master table l_t_account_id -> %',l_t_account_id; 
		
		l_t_str_insert_data := ' SELECT tb_col_nm  FROM t_ds_col_detail WHERE dataset_id = '''||l_t_aff_dataset_id ||'''  AND (LOWER(source_column) = ''axtriasalesiqtm__account__c'' or LOWER(source_column) = ''account__c'')  ';
		raise notice 'Fetch Id Column for Aff Network l_t_str_insert_data -> %',l_t_str_insert_data;
		Execute l_t_str_insert_data into 	l_t_aff_network_id ;
		raise notice 'Fetch Id Column for Aff Network table l_t_aff_network_id -> %',l_t_aff_network_id; 
		
		l_t_str_insert_data := ' SELECT tb_col_nm  FROM t_ds_col_detail WHERE dataset_id = '''||l_t_aff_dataset_id ||'''  AND (LOWER(source_column) = ''axtriasalesiqtm__parent_account__c'' or LOWER(source_column) = ''parent_account__c'')  ';
		raise notice 'Fetch Id Column for Customer Master table l_t_str_insert_data -> %',l_t_str_insert_data;
		Execute l_t_str_insert_data into 	l_t_aff_network_parent_id ;
		raise notice 'Fetch Id Column for Customer Master table l_t_aff_network_parent_id -> %',l_t_aff_network_parent_id; 
		
		
		l_t_str_insert_data := ' create temp table temp_network_delta (id text, traversed text default ''N'')' ;
		Execute ' drop table if exists  temp_network_delta';
		raise notice 'l_t_str_insert_data -> %',l_t_str_insert_data;
		Execute l_t_str_insert_data ;
		
		raise notice 'l_t_account_id-> %',l_t_account_id;
		raise notice 'l_t_customer_master_table -> %',l_t_customer_master_table;
		l_t_str_insert_data := ' insert into temp_network_delta (id ) select distinct '||l_t_account_id ||  ' from '||	l_t_customer_master_table;
		raise notice 'l_t_str_insert_data -> %',l_t_str_insert_data;
		
		Execute l_t_str_insert_data;
		
		l_t_str_insert_data :=' delete from temp_network_delta  where id  is null' ;	
		raise notice 'l_t_str_insert_data -> %',l_t_str_insert_data;
		
		Execute l_t_str_insert_data;
				
		
		
        --while i_counter < l_i_hierarchy_level
		while i_count > 0 
			loop
					raise notice ' Iteration Number -> %',i_counter;
					l_t_str_insert_data := 'update  temp_network_delta n1 set traversed =''X''  where traversed =''N'' and not exists( select ''x''  from temp_network_delta n2  where n1.id=n2.id  and n2.traversed =''Y'') ';
					raise notice 'First Update All current records to X -> l_t_str_insert_data -> %',l_t_str_insert_data;
					Execute l_t_str_insert_data;
					
					l_t_str_insert_data := 'insert into temp_network_delta (id) select distinct '||l_t_aff_network_id ||'  from '||l_t_aff_table_name_copy ||' af where  '
											||l_t_aff_network_parent_id||' in( '|| 
										   '  select id from temp_network_delta n1  where traversed =''X''  and not exists( select ''x'' '|| 
										   ' from temp_network_delta n2   where n1.id=n2.id  and n2.traversed =''Y''))  ';
					raise notice 'Insert id from aff_network where parent exists-> l_t_str_insert_data -> %',l_t_str_insert_data;
					Execute l_t_str_insert_data;
					
					l_t_str_insert_data := '  insert into temp_network_delta (id)  select distinct  '||l_t_aff_network_parent_id ||'  from '||l_t_aff_table_name_copy 
											||'  af where  '||l_t_aff_network_id ||' in ( '||
										   '  select id from temp_network_delta n1  where traversed =''X'' and not exists( select ''x'' '|| 
										   '  from temp_network_delta n2   where n1.id=n2.id  and n2.traversed =''Y'' )) ';
					raise notice ' Insert from aff network where child exists -> l_t_str_insert_data -> %',l_t_str_insert_data;
					Execute l_t_str_insert_data;
					
					l_t_str_insert_data := '   update 	temp_network_delta set traversed=''Y'' where traversed =''X''  ';	
					raise notice 'update current processed to Y -> l_t_str_insert_data -> %',l_t_str_insert_data;
					Execute l_t_str_insert_data;

					l_t_str_insert_data := ' update 	temp_network_delta n1 set traversed=''Y'' where traversed =''N''  and  exists( select ''x'' '||
										   ' from temp_network_delta n2  where n1.id=n2.id  and n2.traversed =''Y'')  ';	
					raise notice 'update duplicates in network to Y ->  l_t_str_insert_data -> %',l_t_str_insert_data;
					--return 23;
					Execute l_t_str_insert_data;
			
					l_t_str_insert_data := 'select count(1) from temp_network_delta where traversed =''N''  ';
                    raise notice 'Get count of nodes which have not been traversed -> %',l_t_str_insert_data;
					EXECUTE l_t_str_insert_data into i_count;
					--return 2938;
			i_counter := i_counter +1;
		end loop;
		l_t_str_insert_data := ' create temp table t_temp as select distinct id from temp_network_delta ';
		Execute 'Drop table if exists t_temp';
		raise notice 'Remove duplicate accounts -> l_t_str_insert_data -> %',l_t_str_insert_data;
		Execute l_t_str_insert_data ; 
		
        
		
		l_t_str_insert_data := 'Truncate Table ' || l_t_output_table ;
		raise notice 'l_t_str_insert_data-> %',l_t_str_insert_data;
		EXECUTE l_t_str_insert_data;
		
		l_t_str_insert_data :=  					   'insert into '|| l_t_output_table || '(' ||  l_t_colm || ') select  ' || l_t_colm2 || 
										' from ' || l_t_input_table || ' where ' || l_t_filter_final || ' and  ' ||l_t_account_id || ' in (select   id from t_temp )';
										
		
		RAISE NOTICE 'l_t_colm - > %',l_t_colm;
		raise notice 'l_t_colm2-> %',l_t_colm2;
		RAISE NOTICE 'l_t_str_insert_data -> %',l_t_str_insert_data;
		
		EXECUTE(l_t_str_insert_data);
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
		 VALUES (l_t_scn_id, 'Affiliation Delta Execution Ended- '||l_t_affiliation_scn_instance_id, '47', l_t_fn_name);
		/*l_t_str_insert_data :=  'insert into '|| l_t_output_table || '(' ||  l_t_colm || ') select  ' || l_t_colm2 || 
										' from ' || l_t_input_table || ' where ' ||  l_t_account_id || ' in (select   id from t_temp )';
		
		RAISE NOTICE 'l_t_colm - > %',l_t_colm;
		raise notice 'l_t_colm2-> %',l_t_colm2;
		RAISE NOTICE 'l_t_str_insert_data -> %',l_t_str_insert_data;
		EXECUTE(l_t_str_insert_data);
		
		Execute 'Drop table if exists '|| l_t_output_table||'_1';
		l_t_str_insert_data := ' create table '||l_t_output_table||'_1 as select distinct '|| l_t_colm ||' from '|| l_t_output_table ;
		RAISE NOTICE 'l_t_str_insert_data -> %',l_t_str_insert_data;
		EXECUTE(l_t_str_insert_data);
		
		Execute 'drop table if exists'||l_t_output_table ;
		l_t_str_insert_data := 'alter table '||l_t_output_table||'_1 rename to '|| l_t_output_table ; 
		RAISE NOTICE 'l_t_str_insert_data -> %',l_t_str_insert_data;
		EXECUTE(l_t_str_insert_data); */
		
		return 1;
	
exception
		when others then
			GET STACKED DIAGNOSTICS l_t_error = PG_EXCEPTION_CONTEXT;
			insert into t_error_log
			(proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query)
			values (l_t_fn_name,SQLERRM,SQLSTATE,'','',p_t_scn_id,l_t_error,l_t_str_insert_data);
			insert into t_error_run_log
			(proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query)
			values (l_t_fn_name,SQLERRM,SQLSTATE,'','',p_t_scn_id,l_t_error,l_t_str_insert_data);
			--RAISE NOTICE 'Unsuccessful';
			RETURN -1;
      
END;

$BODY$;

ALTER FUNCTION public.fn_affiliation_delta(text, text)
    OWNER TO postgres;
