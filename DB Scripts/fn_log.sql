

CREATE OR REPLACE FUNCTION public.fn_log( p_org_id text , 	p_t_scenario_id text , p_scenario_run_id text , 
										  p_log_type text,p_log_level text , p_run_status text, p_log_detail text ,
										  p_line_no integer , p_function_name text)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
fn_name text := 'fn_log';
l_t_error text;
l_t_sql_query text;
l_t_count text;
l_t_ip_tbl text;
l_t_log_source text :='Postgres' ; 

begin
	
	
	insert into t_pg_run_log (org_id	,scenario_id	,scenario_run_id	,log_type	,
							log_source	,log_level	,run_status	,log_detail	,line_no ,function_name	)
	values(p_org_id , p_t_scenario_id , p_scenario_run_id , p_log_type ,l_t_log_source ,
				p_log_level , p_run_status ,p_log_detail , p_line_no , p_function_name   ) ; 

	return 1;
exception
when others then
        GET STACKED DIAGNOSTICS l_t_error = PG_EXCEPTION_CONTEXT;
        insert into t_error_log
        (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query)
        values (fn_name,SQLERRM,SQLSTATE,'','','',l_t_error,l_t_sql_query);
        insert into t_error_run_log
        (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query)
        values (fn_name,SQLERRM,SQLSTATE,'','','',l_t_count,l_t_ip_tbl);
        return -1;
        --RAISE NOTICE 'Unsuccessful';
      
END;

$BODY$;



