-- FUNCTION: public.fn_update_shared_flag_final(text)

-- DROP FUNCTION public.fn_update_shared_flag_final(text);

CREATE OR REPLACE FUNCTION public.fn_update_shared_flag_final(
	p_scenario_id text)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$


DECLARE

l_t_scenario_id text :=p_scenario_id ;
l_t_fn_name text := 'fn_update_shared_flag';
--l_t_output_table text := p_t_output_table ;
--l_t_cust_terr_ds_id text := p_t_cust_terr_ds_id ;
 l_t_cust_id_col_ct text   ;
l_t_position_ct text   ;  
l_t_isshared_flag_ct text ;
l_t_dataset_id text;
l_t_sharedwith_ct text ;
l_i_log_flag integer ;
l_t_sql_query text;
l_t_output_table text;
l_t_error text ;
AFFECTEDROWS integer;
l_rule_id_count  integer;
l_t_col_names  text;
BEGIN
l_i_log_flag = 1;
	
	/*l_t_sql_query:=  ' select dataset_id , table_name from t_dataset_rule_map drm  '||
					'  where drm.scenario_rule_instance_id in (select sfdc_id from 	t_scn_rule_instance_details as srid ' ||
					'  where srid.scenario_id = '''|| l_t_scenario_id|| '''' ||
					' and component_type_label=''Final Output'') and ds_type=''O''' ; */
	
	l_t_sql_query:= ' select dataset_id , table_name from t_dataset_rule_map drm    '||
					'  where   ds_type=''O'' ' || 
					' and drm.scenario_rule_instance_id in ( select   sfdc_id from ( '||
					' SELECT  srid.sfdc_id, ROW_NUMBER() OVER (ORDER BY srid.exec_seq DESC)    exec_seq '||
					' FROM t_scn_rule_instance_details as srid, t_component_type_master as ctm '||
					' WHERE srid.scenario_id = '''|| l_t_scenario_id|| '''' || 
					' AND srid.component_type_master = ctm.sfdc_id '||
					' and  srid.is_visible =''true'' '||
					'  and ctm.name =''Data Source'') cc  '||
					'  where exec_seq=1) ';
	
	raise notice 'l_t_sql_query -> %',l_t_sql_query;
	--return 44;
    execute l_t_sql_query into l_t_dataset_id , l_t_output_table ;
	--return 88;
    select tb_col_nm from t_ds_col_detail where dataset_id = l_t_dataset_id and (lower(source_column) = 'axtriasalesiqtm__isshared__c' or lower(source_column) = 'isshared__c') into l_t_isshared_flag_ct; --- Yes/No
    select tb_col_nm from t_ds_col_detail where dataset_id = l_t_dataset_id and (lower(source_column) = 'axtriasalesiqtm__sharedwith__c' or lower(source_column) = 'sharedwith__c') into l_t_sharedwith_ct; --- T1,T2
	select tb_col_nm from t_ds_col_detail where dataset_id = l_t_dataset_id and (lower(source_column) = 'axtriasalesiqtm__account__c' or lower(source_column) = 'account__c') into l_t_cust_id_col_ct;
    select tb_col_nm from t_ds_col_detail where dataset_id = l_t_dataset_id and (lower(source_column) = 'axtriasalesiqtm__position__r.name' or lower(source_column) = 'position__r.name') into l_t_position_ct;	
	raise notice 'l_t_isshared_flag_ct -> % ;; l_t_sharedwith_ct -> % ;; l_t_cust_id_col_ct -> % ;; l_t_position_ct -> %',l_t_isshared_flag_ct , l_t_sharedwith_ct ,l_t_cust_id_col_ct , l_t_position_ct ;
	--return 6;
	IF (l_t_isshared_flag_ct is not null ) then
        -- Update isshared flag 
		raise notice 'l_t_isshared_flag_ct is not null';
        execute 'update '|| l_t_output_table || ' set ' || l_t_isshared_flag_ct || ' = ''False''';

        l_t_sql_query:=  'update '|| l_t_output_table || ' a set ' || l_t_isshared_flag_ct || ' = ''True''
        from ' || '( select '||l_t_cust_id_col_ct ||' ,  count('||l_t_position_ct ||') from '|| l_t_output_table ||
        ' group by '|| l_t_cust_id_col_ct || ' having count('||l_t_position_ct ||') >1 ) b 
        where a.'||l_t_cust_id_col_ct || ' = b.'||l_t_cust_id_col_ct  ;
		raise notice 'l_t_sql_query -> %',l_t_sql_query;
        execute l_t_sql_query ;
		get diagnostics AFFECTEDROWS = row_count;
		raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;

    End IF ;
    
    IF (l_t_sharedwith_ct is not null) then
        -- Update sharedwith column
		raise notice 'l_t_sharedwith_ct is not null';
        l_t_sql_query :=  'update '|| l_t_output_table || ' a set ' || l_t_sharedwith_ct || ' = shared_terr
        from ' || '( select '||l_t_cust_id_col_ct ||' ,  string_agg('||l_t_position_ct ||','';'') as shared_terr from '|| l_t_output_table ||
        ' group by '|| l_t_cust_id_col_ct || ' having count('||l_t_position_ct ||') >1 ) b 
        where a.'||l_t_cust_id_col_ct || ' = b.'||l_t_cust_id_col_ct  ;
		raise notice 'l_t_sql_query -> %',l_t_sql_query;
		execute l_t_sql_query ;
        get diagnostics AFFECTEDROWS = row_count;
		raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
		
		l_t_sql_query :='';
        
        l_t_sql_query := 'update '|| l_t_output_table || ' a set ' || l_t_sharedwith_ct || ' =  case when ' || l_t_sharedwith_ct 
        || ' like ' ||l_t_position_ct || '||''%''  then replace('||l_t_sharedwith_ct||',' || l_t_position_ct ||'|| '';'' ,'''' ) when ' 
        || l_t_sharedwith_ct || ' like ''%'' ||' ||l_t_position_ct || '|| ''%'' then replace(' || l_t_sharedwith_ct ||','';''||' || 
        l_t_position_ct ||','''') else ' || l_t_sharedwith_ct || ' end '  ;
        raise notice 'l_t_sql_query -> %',l_t_sql_query;
		execute l_t_sql_query ;
		get diagnostics AFFECTEDROWS = row_count;
		raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
		
	End IF;
	
	
	/* New Code for removing duplicity after aggregation  */
		/* changes done on 30-Jan-19 */
			
			raise notice 'l_t_dataset_id - %', l_t_dataset_id ; 
			
			l_t_sql_query :=  ' select   count(tb_col_nm) cnt     FROM t_ds_col_detail    WHERE dataset_id = '''||l_t_dataset_id ||''' and Coalesce(tb_col_nm,''a'')   in (''rule_name'',''rule_id'')  ' ; 
			raise notice 'l_t_sql_query - %',l_t_sql_query;
			execute l_t_sql_query into l_rule_id_count ;
			raise notice 'l_rule_id_count- %',l_rule_id_count; 
			
			if l_rule_id_count =2 then  /* This is to exclude the product block cases where in the rule_id and rule_name columns are not there */
			/* IF 3*/
					l_t_sql_query :=  ' select string_agg(tb_col_nm, '', '')  tb_col_nm    FROM t_ds_col_detail '||
										' WHERE dataset_id = '''|| l_t_dataset_id || ''' '||
										--' and source_column is not null '
										' and Coalesce(tb_col_nm,''a'') not in (''rule_name'',''rule_id'') ';
					raise notice 'l_t_sql_query - %',l_t_sql_query;
					
					execute l_t_sql_query into l_t_col_names ;
					raise notice 'l_t_col_names- %',l_t_col_names;
					
					Execute ' Drop table if exists '|| l_t_output_table||'_1';
					--l_t_sql_query: = ' create table '||l_t_output_table||'_1 as select '|| l_t_col_names ||',count(0) cnt from '||l_t_output_table
					--				|| ' group by  '||l_t_col_names || ' having count(0)>1 ' ;
					
					--l_t_sql_query:= ' create table '||l_t_output_table||'_1 as select '|| l_t_col_names ||   ', string_agg(rule_name, '', '') rule_name  --,string_agg(rule_id, '', '') rule_id from '||l_t_output_table || ' group by '||l_t_col_names ;
					l_t_sql_query:= ' create table '||l_t_output_table||'_1 as select * from ' ||l_t_output_table || '   '; 
					raise notice 'l_t_sql_query - %',l_t_sql_query;
					
					Execute l_t_sql_query ; 
					
					l_t_sql_query:= ' delete from '|| l_t_output_table  ;
					
					raise notice 'l_t_sql_query - %',l_t_sql_query;
					execute l_t_sql_query ;
					l_t_sql_query:= ' insert into '||l_t_output_table || '( '||l_t_col_names || 
					' , rule_name , rule_id)  select '||l_t_col_names 
					--||   ', string_agg(rule_name, '', '') rule_name  ,string_agg(rule_id, '', '') rule_id  from '
					||   ', string_agg(rule_name, '', '' order by rule_id) rule_name  ,string_agg(rule_id, '', ''order by rule_id) rule_id  from '
					||l_t_output_table ||'_1 group by '||l_t_col_names ;
					--|| ' order by rule_name ';
					raise notice 'l_t_sql_query - %',l_t_sql_query;
					execute l_t_sql_query ;
			
			end if;/* IF 3*/
			/* This is to exclude the product block cases where in the rule_id and rule_name columns are not there */
		/* End New Code*/
    

RETURN 1;
        
/*EXCEPTION 
	WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS l_t_error = PG_EXCEPTION_CONTEXT;
     
        INSERT INTO t_error_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
        	 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,'','',l_t_error,l_t_sql_query,'','' );
        INSERT INTO t_error_run_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
        	 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,'','',l_t_error,l_t_sql_query,'','');
   
       
        RETURN -1;*/
END;


$BODY$;

ALTER FUNCTION public.fn_update_shared_flag_final(text)
    OWNER TO postgres;
