-- FUNCTION: public.fn_promote_scenario(text, boolean, boolean, text, text, text, text, text, text)

-- DROP FUNCTION public.fn_promote_scenario(text, boolean, boolean, text, text, text, text, text, text);

CREATE OR REPLACE FUNCTION public.fn_promote_scenario(
	p_t_scenario_id text,
	p_b_do_promote boolean,
	p_b_is_delta boolean,
	p_t_compare_dataset_id text,
	p_t_cust_univ_tbl_nm text,
	p_t_cust_univ_dataset_id text,
	p_t_upstream_tbl_nm text,
	p_t_upstream_dataset_id text,
	p_filter_condition text)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
l_t_fn_name text := 'fn_promote_scenario';
l_t_error text ;
l_t_scenario_id text := p_t_scenario_id ;
l_b_do_promote boolean := p_b_do_promote ;
l_b_is_delta boolean := p_b_is_delta ;
l_t_compare_dataset_id  text := p_t_compare_dataset_id ;
l_t_cust_univ_tbl_nm text := p_t_cust_univ_tbl_nm ;
l_t_cust_univ_dataset_id text := p_t_cust_univ_dataset_id ;
l_t_upstream_tbl_nm text := p_t_upstream_tbl_nm ;
l_t_upstream_tbl_nm_copy text = l_t_upstream_tbl_nm || '_1';
l_t_upstream_dataset_id text := p_t_upstream_dataset_id ;
l_t_dataset_object_name text ;
l_t_dynamic_query text ;
l_t_team_instance text;
l_t_alignment_period text ;
l_t_team_ins_start_date text ;
l_t_team_ins_end_date text ;
l_t_compare_filter text;
l_t_compare_tbl text;
l_t_compare_tbl_copy text ;
l_t_assign_status_clmn text ;
l_t_comp_end_dt_clmn text;
l_t_comp_acc_id_clmn text ;
l_t_comp_pos_id_clmn text ;
l_t_upstrm_acc_id_clmn text ;
l_t_upstrm_pos_id_clmn text ;
l_t_comp_sfdc_id_clmn text ;
--l_t_promoted_table text:= lower('t_final_promoted_cust_terr_'||l_t_scenario_id) ;
l_t_promoted_table text:= lower('t_final_promoted_'||l_t_scenario_id) ;
l_t_comp_actv_tb_query text;
l_t_comp_start_dt_clmn text ;
l_t_upstrm_start_dt_clmn text ;
l_t_upstrm_end_dt_clmn text ;
l_t_upstrm_sfdc_id_clmn text;
l_t_upstrm_flag_clmn text;
l_t_comp_futr_actv_tb_query text;
l_t_promote_tbl_clmn text ;
l_t_promote_tbl_clmn_actv text;
l_t_promote_tbl_clmn_futr_actv text;
l_c_update text := 'Update';
l_c_delete text := 'Delete';
l_c_insert text := 'Insert';
l_t_cust_univ_acc_id_clmn text;
l_t_promote_tbl_clmn_Futr_tm text;
l_t_actv_acct_query text;
l_t_futr_actv_acct_query text;
l_t_comp_prdct_clmn text;
l_t_upstrm_prdct_clmn text;
l_col_list_debug text ;
l_comp_table_col_list text; 
l_comp_table_col_list_1 text ; 
l_t_promote_tbl_clmn_1 text ;
l_comp_table_col_list_2 text ; 
l_t_promote_tbl_clmn_2 text ;
l_filter_condition text :=p_filter_condition ;
AFFECTEDROWS integer := 0;
l_account_alignment_type_compare text;
l_account_alignment_type_upstream text;
i integer; 
l_upstrm_id_col text;
l_promote_parameters  text;
l_t_comp_status_clmn   text;
l_r_cursor record; 
l_t_assignmet_status text;
l_t_upstream_affil_based_align text;
l_t_comp_affil_based_align text;
l_t_promote_columns text;

--Run Log
l_org_id text :='BRMS' ;
l_scenario_run_id text :=  p_t_scenario_id; 
l_log_type text;
l_log_level_basic text := 'Basic' ; 
l_log_level_detailed text := 'Detailed' ; 
l_run_status_in_progress text :='In Progress';
l_run_status_complete text :='Complete';
l_run_status_error text :='Error';
l_line_no text ; 
l_function_name text ;
l_log_type_info text :='Information';
l_log_type_warning text :='Warning';
l_log_type_error text :='Error';
l_input_count integer ;
l_output_count integer ;
p_t_scn_id text := p_t_scenario_id;
--- Shared With
l_t_is_shared_upstrm text ;
l_t_shared_with_upstrm text;
l_t_is_shared_comp text ;
l_t_shared_with_comp text;

begin
		/*perform fn_promote_fetch_parameters(
											p_t_scenario_id ,
											p_b_do_promote ,
											p_b_is_delta ,
											p_t_compare_dataset_id ,
											p_t_cust_univ_tbl_nm ,
											p_t_cust_univ_dataset_id ,
											p_t_upstream_tbl_nm ,
											p_t_upstream_dataset_id ,
											p_filter_condition 
											);	*/

		l_promote_parameters := ' p_t_scenario_id - '||p_t_scenario_id ||  
								'  p_b_do_promote - '||p_b_do_promote ||	' p_b_is_delta -'||p_b_is_delta ||
								' p_t_compare_dataset_id - '||p_t_compare_dataset_id || 
								' p_t_cust_univ_tbl_nm -'||p_t_cust_univ_tbl_nm||
								' p_t_cust_univ_dataset_id - '||p_t_cust_univ_dataset_id||
								' p_t_upstream_tbl_nm - '||p_t_upstream_tbl_nm ||
								' p_t_upstream_dataset_id - '||p_t_upstream_dataset_id||
								' p_filter_condition - '||p_filter_condition ;
								
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
					 VALUES (l_t_scenario_id, 'Promote started for scenario - '|| l_t_scenario_id|| ' parameters - '
												||l_promote_parameters, '74', l_t_fn_name);
		
		Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
				l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Promote Scenario Started with parameters : - '||l_promote_parameters ,
				 118 , l_t_fn_name) ; 
		
		-- Promote flag if condition 
		IF(l_b_do_promote = TRUE) THEN
				
				
				-----------------------------------------------------------------------------------------------
					---------------------Get Salesforce object name for upstream dataset---------------------------
					-----------------------------------------------------------------------------------------------
					RAISE NOTICE 'Get Salesforce object name for upstream dataset started';
					l_t_dynamic_query := 'select distinct data_set_object_name from t_dataset where sfdc_id = '''|| l_t_upstream_dataset_id ||'''';
					EXECUTE l_t_dynamic_query INTO l_t_dataset_object_name ;
						
					INSERT INTO t_log(scenario_id, log, line_no, function_name)
							 VALUES (l_t_scenario_id, 'Promote-Query to Get Salesforce object name for upstream dataset : '|| l_t_dynamic_query, '89', l_t_fn_name); 
					
					RAISE NOTICE 'Query to Get Salesforce object name for upstream dataset : %',l_t_dynamic_query;
					
					INSERT INTO t_log(scenario_id, log, line_no, function_name)
							 VALUES (l_t_scenario_id, 'Promote-Salesforce object name for upstream dataset : '|| l_t_dataset_object_name, '89', l_t_fn_name); 

					RAISE NOTICE 'Salesforce object name for upstream dataset : %',l_t_dataset_object_name;
					
					
					
					
					-------------------------------------------------------------------------------------------
					-----------------------Get Team instance related field data---------------------------------
					--------------------------------------------------------------------------------------------
					
					l_t_dynamic_query := 'select team_instance ,alignment_period ,team_ins_start_date ,team_ins_end_date from t_scenario where sfdc_id = '''||l_t_scenario_id || '''';  
					EXECUTE l_t_dynamic_query INTO l_t_team_instance,l_t_alignment_period ,l_t_team_ins_start_date ,l_t_team_ins_end_date ;
					
					RAISE NOTICE 'Query to Get Team instance related field data : %',l_t_dynamic_query;
					
					INSERT INTO t_log(scenario_id, log, line_no, function_name)
							 VALUES (l_t_scenario_id, 'Promote-Query to Get Team instance related field data : '|| l_t_dynamic_query, '103', l_t_fn_name);
							 
					
					
					-------------------------------------------------------------------------------------------------------------
					-----------------------Get the common column names for Compare and upstream tables ---------------------------------
					-------------------------------------------------------------------------------------------------------------
					
					EXECUTE 'drop table if exists t_promote_scn_col_detls '  ;
					l_t_dynamic_query := 'Create temp table t_promote_scn_col_detls as '
						|| ' select compare_col_name,compare_col_name compare_col_name_1, upstream_col_name ,upstream_col_name upstream_col_name_1, upstream.source_column, ROW_NUMBER () OVER ()  as row_num '
						|| ' from '
						|| ' (select  tb_col_nm compare_col_name , source_column '
						|| ' from t_ds_col_detail compare where dataset_id =  ''' ||p_t_compare_dataset_id ||''' '
						|| ' and source_column is not null and source_column not like ''%.%'') compare , '
						|| ' (select  tb_col_nm  upstream_col_name, source_column '
						|| ' from t_ds_col_detail upstream where dataset_id =  ''' || l_t_upstream_dataset_id || ''''
						|| ' and source_column is not null '
						|| ' and source_column not like ''%.%'' '
						|| ' ) upstream '
						|| ' where compare.source_column = upstream.source_column  ' ;
					RAISE NOTICE 'Query to Get the column names for Compare and upstream tables - %', l_t_dynamic_query; 
					EXECUTE l_t_dynamic_query ; 
					INSERT INTO t_log(scenario_id, log, line_no, function_name)
							 VALUES (l_t_scenario_id, 'Promote-Query to Get the column names for Compare and upstream tables - : '|| l_t_dynamic_query, '124', l_t_fn_name);			
					
										
					IF(l_filter_condition is null) THEN
						l_filter_condition = ' 1 = 1 ';
					END IF;
					
					-------------------------- Get compare table name---------------------------------------------	
					
					l_t_dynamic_query := 'select distinct final_table from t_scn_data_object_map where  scenario = '''|| l_t_scenario_id ||''' and data_set   in (select base_data_Set from t_dataset where sfdc_id= ''' || p_t_compare_dataset_id ||''' )';
					EXECUTE l_t_dynamic_query into l_t_compare_tbl;
					raise notice 'compare table name - %', l_t_compare_tbl ; 
					
					RAISE NOTICE 'Query to Get compare table name : %',l_t_dynamic_query;
					
					Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
					l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Promote - Compare Table - '|| l_t_compare_tbl ||' filter condition - '||p_filter_condition,
					118 , l_t_fn_name) ; 
										
					INSERT INTO t_log(scenario_id, log, line_no, function_name)
							 VALUES (l_t_scenario_id, 'Promote-Query to Get compare table name : '|| l_t_dynamic_query, '147', l_t_fn_name);
				
				------------------------------- Create a copy of compare table -------------------------------
					--- compare tables are huge in size --- copy created for performance improvement -------------
					l_t_compare_tbl_copy := l_t_compare_tbl || '_1';
					EXECUTE 'drop table if exists ' || l_t_compare_tbl_copy ;
					raise notice 'l_t_compare_tbl_copy - %', l_t_compare_tbl_copy;
					raise notice 'l_t_compare_tbl -%' , l_t_compare_tbl ; 
					EXECUTE 'Create temp  table '|| l_t_compare_tbl_copy ||' as select * from '|| l_t_compare_tbl || ' where ' || p_filter_condition|| ' ' ;
					raise notice 'compare table copy created';
					
					
					
					
					
					-------------------------- Query to fetch columns list of the Compare table ------------
					
					SELECT string_agg(compare_col_name, ',') 
					  From  (select *  FROM t_promote_scn_col_detls 
						 order by row_num) Cc
						INTO l_comp_table_col_list;
					 Raise notice ' Compare table columns list - % ', l_comp_table_col_list ; 
					
					-------------------------- Columns for promote table -- all columns from upstream table 
					SELECT string_agg(upstream_col_name, ',') 
						From  (select *  FROM t_promote_scn_col_detls 
						 order by row_num) Cc
						INTO l_t_promote_tbl_clmn;
					
					RAISE NOTICE 'Query to Get Columns for promote table : %',l_t_promote_tbl_clmn;
					Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
					l_log_type_info  , l_log_level_basic , l_run_status_complete , 'Promote -  Compare Table Columns -'||l_comp_table_col_list   ||' Promote Table columns -'|| l_t_promote_tbl_clmn,
					118 , l_t_fn_name) ; 
				
					perform fn_create_table (l_t_upstream_dataset_id,l_t_promoted_table,'staging_tbl');
					
					RAISE NOTICE 'Perform Create table for promote completed';
					
					raise notice 'l_t_promoted_table - %', l_t_promoted_table;
					raise notice 'l_t_promote_tbl_clmn - %', l_t_promote_tbl_clmn;
					raise notice 'l_t_upstream_tbl_nm_copy - %', l_t_upstream_tbl_nm_copy;
				
					raise notice 'l_t_promoted_table - %', l_t_promoted_table;
					raise notice 'l_t_promote_tbl_clmn - %', l_t_promote_tbl_clmn;
					raise notice 'l_t_upstream_tbl_nm_copy - %', l_t_upstream_tbl_nm_copy;
					
					
					-------------------- Get account id column name from customer universe------------------------
					l_t_dynamic_query := 'select tb_col_nm from t_ds_col_detail where dataset_id = ''' || l_t_cust_univ_dataset_id ||''' and lower(source_column) = ''id'' ';
					EXECUTE l_t_dynamic_query into l_t_cust_univ_acc_id_clmn;
					
					RAISE NOTICE 'Query to Get account id column name from customer universe : %',l_t_dynamic_query;
					
					INSERT INTO t_log(scenario_id, log, line_no, function_name)
							 VALUES (l_t_scenario_id, 'Promote-Query to Get account id column name from customer universe : '|| l_t_dynamic_query, '147', l_t_fn_name); 
					
					---------------------------- Get Compare sfdc id column name -----------------------------------
					l_t_dynamic_query := 'select tb_col_nm from t_ds_col_detail where dataset_id = ''' || p_t_compare_dataset_id ||''' and lower(source_column) = ''id'' ';
					EXECUTE l_t_dynamic_query into l_t_comp_sfdc_id_clmn;
					
					RAISE NOTICE 'Query to Get account id column name from customer universe : %',l_t_dynamic_query;
					raise notice 'l_t_comp_sfdc_id_clmn - %', l_t_comp_sfdc_id_clmn;
					
					INSERT INTO t_log(scenario_id, log, line_no, function_name)
							 VALUES (l_t_scenario_id, 'Promote-Query to Get account id column name from customer universe : '|| l_t_dynamic_query, '147', l_t_fn_name); 
					---------------------------- Get Compare Account Status column name -----------------------------------
					l_t_dynamic_query := 'select tb_col_nm from t_ds_col_detail where dataset_id = ''' || p_t_compare_dataset_id ||''' and lower(source_column) like ''%assignment_status__c%'' ';
					EXECUTE l_t_dynamic_query into l_t_assignmet_status;
					
					RAISE NOTICE 'Query to Get Compare Account Status column name : %',l_t_dynamic_query;
					raise notice 'l_t_assignmet_status - %', l_t_assignmet_status;
					
					INSERT INTO t_log(scenario_id, log, line_no, function_name)
							 VALUES (l_t_scenario_id, 'Promote-Query to Get Compare Account Status column name : '|| l_t_dynamic_query, '147', l_t_fn_name); 

					------ Get upstream sfdc id column name 
					l_t_dynamic_query := 'select /*1*/tb_col_nm from t_ds_col_detail where dataset_id = ''' || l_t_upstream_dataset_id ||''' and lower(source_column) = ''id'' ';
					EXECUTE l_t_dynamic_query into l_t_upstrm_sfdc_id_clmn;
					
					
					
					------ Get Compare Account column name 
					l_t_dynamic_query := 'select tb_col_nm from t_ds_col_detail where dataset_id = ''' || p_t_compare_dataset_id ||''' and (lower(source_column) = ''account__c'' or lower(source_column) like ''axtriasalesiqtm__account__c'')';
					EXECUTE l_t_dynamic_query into l_t_comp_acc_id_clmn;
					raise notice 'Get Compare Account column name - %',l_t_dynamic_query;
					raise notice 'l_t_comp_acc_id_clmn - %', l_t_comp_acc_id_clmn;
					
					------ Get Upstream Account column name 
					l_t_dynamic_query := 'select /*2*/tb_col_nm from t_ds_col_detail where dataset_id = ''' || l_t_upstream_dataset_id ||''' and (lower(source_column) = ''account__c'' or lower(source_column) like ''axtriasalesiqtm__account__c'')';
					EXECUTE l_t_dynamic_query into l_t_upstrm_acc_id_clmn;

					RAISE NOTICE 'Query to Get Upstream Account column name : %',l_t_dynamic_query;
					
					------ Get compare Id column name -- needed by python
					l_t_dynamic_query := 'select /*2*/tb_col_nm from t_ds_col_detail where dataset_id = ''' || p_t_compare_dataset_id ||''' and (lower(source_column) = ''id'' or lower(source_column) like ''axtriasalesiqtm__id'')';
					EXECUTE l_t_dynamic_query into l_upstrm_id_col;

					RAISE NOTICE 'Query to Get Upstream Id column name : %',l_t_dynamic_query;
					
					
					---------------------------- Get Compare affil_based_flag column name -----------------------------------
					l_t_dynamic_query := 'select tb_col_nm from t_ds_col_detail where dataset_id = ''' || p_t_compare_dataset_id ||''' and lower(source_column) like ''%affiliation_based_alignment__c%''';
					
					EXECUTE l_t_dynamic_query into l_t_comp_affil_based_align;
					
					RAISE NOTICE 'Query to Get affil_based_flag for compare : %',l_t_dynamic_query;
					raise notice 'l_t_comp_sfdc_id_clmn - %', l_t_comp_affil_based_align;
					
					INSERT INTO t_log(scenario_id, log, line_no, function_name)
							 VALUES (l_t_scenario_id, 'Promote-Query to Get affil_based_flag for compare : '|| l_t_dynamic_query, '147', l_t_fn_name); 
							 
					---------------------------- Get Upstream affil_based_flag column name -----------------------------------
					l_t_dynamic_query := 'select tb_col_nm from t_ds_col_detail where dataset_id = ''' || l_t_upstream_dataset_id ||''' and lower(source_column) like ''%affiliation_based_alignment__c%''';
					
					EXECUTE l_t_dynamic_query into l_t_upstream_affil_based_align;
					
					RAISE NOTICE 'Query to Get affil_based_flag for Upstream : %',l_t_dynamic_query;
					raise notice 'l_t_comp_sfdc_id_clmn - %', l_t_upstream_affil_based_align;
					
					INSERT INTO t_log(scenario_id, log, line_no, function_name)
							 VALUES (l_t_scenario_id, 'Promote-Query to Get affil_based_flag for Upstream : '|| l_t_dynamic_query, '147', l_t_fn_name); 
					
				
				--------------- Adding the ID column if null 
						--IF(l_t_upstrm_sfdc_id_clmn is null) THEN
							--l_t_upstrm_sfdc_id_clmn := 'id';
							--l_t_dynamic_query := 'alter table /*1*/ '|| l_t_promoted_table || ' add column '|| l_t_upstrm_sfdc_id_clmn || ' text ';
						--	raise notice 'inside l_t_upstrm_sfdc_id_clmn if -%',l_t_dynamic_query;
						
						--	EXECUTE l_t_dynamic_query ;
						
						--END IF;
						--raise notice 'id column added';
				----------------- Adding the Flag column
						--IF(l_t_upstrm_flag_clmn is null) THEN
							--l_t_upstrm_flag_clmn := 'flag';
						--	l_t_dynamic_query := 'alter table /*2*/ '|| l_t_promoted_table || ' add column '|| l_t_upstrm_flag_clmn || ' text ';
							--raise notice 'inside l_t_upstrm_flag_clmn if -%',l_t_dynamic_query;
							
							--EXECUTE l_t_dynamic_query ;
							--raise notice 'after l_t_upstrm_flag_clmn ';
						--END IF;
						
				------------------------------- Create a copy of upstream table -------------------------------
				EXECUTE 'drop table if exists ' || l_t_upstream_tbl_nm_copy ;
				EXECUTE 'Create temp table '|| l_t_upstream_tbl_nm_copy ||' as select * from '|| l_t_upstream_tbl_nm;
				raise notice 'Copy of Upstream table created - %',l_t_upstream_tbl_nm_copy;
				INSERT INTO t_log(scenario_id, log, line_no, function_name)
					 VALUES (l_t_scenario_id, 'Promote-Copy of Upstream table created - '|| l_t_upstream_tbl_nm_copy, '74', l_t_fn_name); 
				l_t_dynamic_query := 'select count(0) from  '||l_t_upstream_tbl_nm_copy ;
				execute l_t_dynamic_query into i;
				raise notice 'count in table % - %', l_t_upstream_tbl_nm_copy , i;
				
				------------------------------- Create a copy of compare table -------------------------------
				--- compare tables are huge in size --- copy created for performance improvement -------------
				/*l_t_compare_tbl_copy := l_t_compare_tbl || '_1';
				EXECUTE 'drop table if exists ' || l_t_compare_tbl_copy ;
				raise notice 'l_t_compare_tbl_copy - %', l_t_compare_tbl_copy;
				raise notice 'l_t_compare_tbl -%' , l_t_compare_tbl ; 
				EXECUTE 'Create table '|| l_t_compare_tbl_copy ||' as select * from '|| l_t_compare_tbl 
						|| ' where ' || p_filter_condition|| ' and lower('|| l_t_assignmet_status ||') <> ''inactive'''  ;
						
				raise notice 'compare table copy created';
				RAISE NOTICE 'l_t_dataset_object_name - %',l_t_dataset_object_name;
				
				l_t_dynamic_query := 'select count(0) from  '||l_t_compare_tbl_copy ;
				execute l_t_dynamic_query into i;
				raise notice 'count in table % - %', l_t_compare_tbl_copy , i;*/
				--RAISE NOTICE ''
				
				---- IF 1	
				IF (lower(l_t_dataset_object_name) = 'position_account__c' or lower(l_t_dataset_object_name) = 'axtriasalesiqtm__position_account__c' ) THEN
						RAISE NOTICE 'INSIDE IF 1 ';
						------------------------------- Create a copy of compare table -------------------------------
						--- compare tables are huge in size --- copy created for performance improvement -------------
						l_t_compare_tbl_copy := l_t_compare_tbl || '_1';
						EXECUTE 'drop table if exists ' || l_t_compare_tbl_copy ;
						raise notice 'l_t_compare_tbl_copy - %', l_t_compare_tbl_copy;
						raise notice 'l_t_compare_tbl -%' , l_t_compare_tbl ; 
						EXECUTE 'Create temp  table '|| l_t_compare_tbl_copy ||' as select * from '|| l_t_compare_tbl 
								|| ' where ' || p_filter_condition|| ' and lower('|| l_t_assignmet_status ||') <> ''inactive'''  ;
								
						raise notice 'compare table copy created';
						RAISE NOTICE 'l_t_dataset_object_name - %',l_t_dataset_object_name;
						
						l_t_dynamic_query := 'select count(0) from  '||l_t_compare_tbl_copy ;
						execute l_t_dynamic_query into i;
						raise notice 'count in table % - %', l_t_compare_tbl_copy , i;
						--RAISE NOTICE ''
						
						-----------------------------------------------------------------------------------------------
						------------------- SHARED WITH UPDATION ------------------------------------------------------
						-----------------------------------------------------------------------------------------------					
								 
						l_t_dynamic_query := 'select tb_col_nm from t_ds_col_detail where dataset_id = ''' || l_t_upstream_dataset_id ||''' and lower(source_column) like ''%isshared__c'' ';
						EXECUTE l_t_dynamic_query into  l_t_is_shared_upstrm;
						raise notice 'Upstream Is Shared l_t_is_shared_upstrm- > %',l_t_is_shared_upstrm;
						
						l_t_dynamic_query := 'select tb_col_nm from t_ds_col_detail where dataset_id = ''' || l_t_upstream_dataset_id ||''' and lower(source_column) like ''%sharedwith__c'' ';
						EXECUTE l_t_dynamic_query into  l_t_shared_with_upstrm;
						raise notice 'Upstream Is Shared l_t_shared_with_upstrm- > %',l_t_shared_with_upstrm;
						
						l_t_dynamic_query := 'select tb_col_nm from t_ds_col_detail where dataset_id = ''' || p_t_compare_dataset_id ||''' and lower(source_column) like ''%isshared__c'' ';
						EXECUTE l_t_dynamic_query into  l_t_is_shared_comp;
						raise notice 'Upstream Is Shared l_t_is_shared_comp- > %',l_t_is_shared_comp;
						
						l_t_dynamic_query := 'select tb_col_nm from t_ds_col_detail where dataset_id = ''' || p_t_compare_dataset_id ||''' and lower(source_column) like ''%sharedwith__c'' ';
						EXECUTE l_t_dynamic_query into  l_t_shared_with_comp;
						raise notice 'Upstream Is Shared l_t_shared_with_comp- > %',l_t_shared_with_comp;
						
						----------------------------Update Shared_With column in sorted order ------------
						l_t_dynamic_query :=  ' update '||l_t_compare_tbl_copy || ' set '||l_t_shared_with_comp ||' = fn_string_to_array_sorted( '|| l_t_shared_with_comp || ') '; 
						raise notice 'Compare Update Shared_With column in sorted order l_t_dynamic_query - > %' ,l_t_dynamic_query ; 
						EXECUTE l_t_dynamic_query ; 
						get diagnostics AFFECTEDROWS = row_count;
						raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
						
						
						-- Upstream table update shared with  
						l_t_dynamic_query :=  ' update '||l_t_upstream_tbl_nm || ' set '||l_t_shared_with_upstrm ||' = fn_string_to_array_sorted( '|| l_t_shared_with_upstrm || ') '; 
						raise notice 'Upstream Update Shared_With column in sorted order l_t_dynamic_query - > %' ,l_t_dynamic_query ; 
						EXECUTE l_t_dynamic_query ; 
						get diagnostics AFFECTEDROWS = row_count;
						raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
						
						--raise notice 'ínside if 1';
						------ Get Compare Assignment_status__c table column name
						l_t_dynamic_query := 'select tb_col_nm from t_ds_col_detail where dataset_id = ''' || p_t_compare_dataset_id ||''' and (lower(source_column) = ''assignment_status__c'' or lower(source_column) = ''axtriasalesiqtm__assignment_status__c'')  ';
						EXECUTE l_t_dynamic_query into l_t_assign_status_clmn;
						raise notice 'Get Compare Assignment_status__c table column name - %', l_t_dynamic_query ;
						
						------ Get Compare effective_end_date__c column name
						l_t_dynamic_query := 'select tb_col_nm from t_ds_col_detail where dataset_id = ''' || p_t_compare_dataset_id ||''' and (lower(source_column) = ''effective_end_date__c'' or lower(source_column) = ''axtriasalesiqtm__effective_end_date__c'') ';
						EXECUTE l_t_dynamic_query into l_t_comp_end_dt_clmn;
						raise notice 'Get Compare effective_end_date__c column name - %', l_t_dynamic_query ;

						------ Get Compare effective_start_date__c column name
						l_t_dynamic_query := 'select tb_col_nm from t_ds_col_detail where dataset_id = ''' || p_t_compare_dataset_id ||''' and (lower(source_column) = ''effective_start_date__c'' or lower(source_column) = ''axtriasalesiqtm__effective_start_date__c'') ';
						EXECUTE l_t_dynamic_query into l_t_comp_start_dt_clmn;
						raise notice 'Get Compare effective_start_date__c column name - %', l_t_dynamic_query ;

						------ Get upstream effective_start_date__c column name
						l_t_dynamic_query := 'select /*3*/tb_col_nm from t_ds_col_detail where dataset_id = ''' || l_t_upstream_dataset_id ||''' and (lower(source_column) = ''effective_start_date__c'' or lower(source_column) = ''axtriasalesiqtm__effective_start_date__c'') ';
						EXECUTE l_t_dynamic_query into l_t_upstrm_start_dt_clmn;
						raise notice 'Get upstream effective_start_date__c column name - %', l_t_dynamic_query ;

						------ Get upstream effective_end_date__c column name
						l_t_dynamic_query := 'select /*4*/tb_col_nm from t_ds_col_detail where dataset_id = ''' || l_t_upstream_dataset_id ||''' and (lower(source_column) = ''effective_end_date__c'' or lower(source_column) = ''axtriasalesiqtm__effective_end_date__c'') ';
						EXECUTE l_t_dynamic_query into l_t_upstrm_end_dt_clmn;
						raise notice 'Get upstream effective_end_date__c column name - %', l_t_dynamic_query ;
						
						------ Get Compare Position column name 
						l_t_dynamic_query := 'select tb_col_nm from t_ds_col_detail where dataset_id = ''' || p_t_compare_dataset_id ||''' and (lower(source_column) = ''position__c'' or lower(source_column) like ''axtriasalesiqtm__position__c'')';
						EXECUTE l_t_dynamic_query into l_t_comp_pos_id_clmn;
						raise notice 'Get Compare Position column name - %', l_t_dynamic_query ;

						------ Get Upstream Position column name 
						l_t_dynamic_query := 'select /*5*/tb_col_nm from t_ds_col_detail where dataset_id = ''' || l_t_upstream_dataset_id ||''' and (lower(source_column) = ''position__c'' or lower(source_column) like ''axtriasalesiqtm__position__c'')';
						EXECUTE l_t_dynamic_query into l_t_upstrm_pos_id_clmn;
						raise notice 'Get Upstream Position column name - %', l_t_dynamic_query ;

						------ Get upstream flag column name 
						l_t_dynamic_query := 'select /*6*/tb_col_nm from t_ds_col_detail where dataset_id = ''' || l_t_upstream_dataset_id ||''' and (lower(source_column) = ''flag__c'' or lower(source_column) = ''axtriasalesiqtm__flag__c'')';
						EXECUTE l_t_dynamic_query into l_t_upstrm_flag_clmn;
						raise notice 'Get upstream flag column name - %', l_t_dynamic_query ;
						
						---------------- Get Upstream account alignment type explicit or implicit
						l_t_dynamic_query := 'select /*7*/tb_col_nm from t_ds_col_detail where dataset_id = ''' || l_t_upstream_dataset_id ||''' and (lower(source_column) = ''account_alignment_type__c'' or lower(source_column) = ''axtriasalesiqtm__account_alignment_type__c'')';
						execute l_t_dynamic_query into l_account_alignment_type_upstream ;
						raise notice 'Get Upstream account alignment type explicit or implicit - %', l_t_dynamic_query ;
						raise notice ' l_account_alignment_type_upstream - %' , l_account_alignment_type_upstream;							
						
						---------------- Get Compare account alignment type explicit or implicit
						l_t_dynamic_query := 'select /*9*/tb_col_nm from t_ds_col_detail where dataset_id = ''' || p_t_compare_dataset_id ||''' and (lower(source_column) = ''account_alignment_type__c'' or lower(source_column) = ''axtriasalesiqtm__account_alignment_type__c'')';
						execute l_t_dynamic_query into l_account_alignment_type_compare ;
						raise notice 'Get Upstream account alignment type explicit or implicit - %', l_t_dynamic_query ;
						raise notice ' l_account_alignment_type_compare - %' , l_account_alignment_type_compare;		
						
						--------------- Adding the ID column if null 
						IF(l_t_upstrm_sfdc_id_clmn is null) THEN
							l_t_upstrm_sfdc_id_clmn := 'id';
							l_t_dynamic_query := 'alter table /*1*/ '|| l_t_promoted_table || ' add column '|| l_t_upstrm_sfdc_id_clmn || ' text ';
							raise notice 'inside l_t_upstrm_sfdc_id_clmn if -%',l_t_dynamic_query;
						
							EXECUTE l_t_dynamic_query ;
						
						END IF;
						
						----------------- Adding the Flag column
						IF(l_t_upstrm_flag_clmn is null) THEN
							l_t_upstrm_flag_clmn := 'flag';
							l_t_dynamic_query := 'alter table /*2*/ '|| l_t_promoted_table || ' add column '|| l_t_upstrm_flag_clmn || ' text ';
							raise notice 'inside l_t_upstrm_flag_clmn if -%',l_t_dynamic_query;
							
							EXECUTE l_t_dynamic_query ;
							raise notice 'after l_t_upstrm_flag_clmn ';
						END IF;
						
						--- IF 2		
						IF (lower(l_t_alignment_period) = 'current') THEN
								-- IF 3 Delta
										IF (l_b_is_delta = FALSE) THEN
												raise notice 'delta false';
												------------------------------------------------------------------------------------------------------------GET ALL COMMON DATA BETWEEN UPSTREAM AND COMPARE AND DELETE THAT DATA FROM THE COPY TABLES --------------------------------------------------------------------- --------------------------------------------
												--------------- Get Compare table names along with alias name
												select string_agg('comp.'||compare_col_name|| ' ', ',')
															from (
															select  tb_col_nm compare_col_name  
															from t_ds_col_detail compare where dataset_id =  p_t_compare_dataset_id  
															and source_column is not null and source_column not like '%.%'
															) cc
												into l_comp_table_col_list_1 ;
												raise notice 'l_comp_table_col_list - %', l_comp_table_col_list_1;
														
												raise notice 'l_comp_table_col_list_1 - %',l_comp_table_col_list_1 ; 
												raise notice 'l_t_compare_tbl_copy - %',l_t_compare_tbl_copy; 
												raise notice 'l_t_upstream_tbl_nm - %',l_t_upstream_tbl_nm; 
												raise notice 'l_t_comp_acc_id_clmn - %',l_t_comp_acc_id_clmn; 
												raise notice 'l_t_upstrm_acc_id_clmn - %',l_t_upstrm_acc_id_clmn; 
												raise notice 'l_t_comp_pos_id_clmn - %',l_t_comp_pos_id_clmn; 
												raise notice 'l_t_upstrm_pos_id_clmn - %',l_t_upstrm_pos_id_clmn; 
												raise notice 'l_account_alignment_type_compare - %',l_account_alignment_type_compare; 
												raise notice 'l_account_alignment_type_upstream - %',l_account_alignment_type_upstream; 
												
												l_t_dynamic_query :=   ' create  temp /*1*/table comp_update_tbl as select  ' 
																	|| l_comp_table_col_list_1 ||' from '
																						--|| l_t_compare_tbl ||' 	comp, ' 
																	|| l_t_compare_tbl_copy ||' 	comp, ' 
																	|| l_t_upstream_tbl_nm || ' unstrm  where comp.'|| l_t_comp_acc_id_clmn||' = unstrm.'||l_t_upstrm_acc_id_clmn || ' and comp.'
																	|| l_t_comp_pos_id_clmn || ' = unstrm.' ||l_t_upstrm_pos_id_clmn 
																	|| ' and comp.'||l_account_alignment_type_compare || ' = unstrm.'||
																	l_account_alignment_type_upstream 
																	||' and lower(comp.'|| l_t_assign_status_clmn ||') = ''active'''; 
																						 
																						
												raise notice 'l_t_dynamic_query - %',l_t_dynamic_query;
												EXECUTE 'DROP TABLE IF EXISTS comp_update_tbl ';
												execute l_t_dynamic_query ;
												raise notice 'table created';
												
												Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_complete , 'Promote -  Create table with common records in Upstream and Compare -'||l_t_dynamic_query   ||'  ',
												498 , l_t_fn_name) ; 
												
												l_t_dynamic_query := 'select count(0) from  comp_update_tbl' ;
												execute l_t_dynamic_query into i;
												raise notice 'count in table comp_update_tbl - %',  i;								
												
												Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_complete , 'Promote -  Create table with common records in Upstream and Compare Count- '|| i   ||'  ',
												498 , l_t_fn_name) ; 
												
												RAISE NOTICE 'comp_update_tbl CREATED';
												RAISE NOTICE 'l_t_compare_tbl_copy -%',l_t_compare_tbl_copy;
												RAISE NOTICE 'l_t_comp_acc_id_clmn - %',l_t_comp_acc_id_clmn;
												RAISE NOTICE 'l_t_comp_pos_id_clmn- %',l_t_comp_pos_id_clmn;
												RAISE NOTICE 'l_account_alignment_type_compare- %',l_account_alignment_type_compare;
												
												/*  Data from compare table to be deleted only in case of delta = false  */
												/*l_t_dynamic_query:= 'delete from '||l_t_compare_tbl_copy ||' comp using comp_update_tbl cm'||
															' where comp.'||l_t_comp_acc_id_clmn || ' = cm.'||l_t_comp_acc_id_clmn ||
															' and comp.'||l_t_comp_pos_id_clmn ||' = cm.'||l_t_comp_pos_id_clmn||
															' and comp.'|| l_account_alignment_type_compare || '= cm.'||l_account_alignment_type_compare ;
												raise notice 'l_t_dynamic_query - %',l_t_dynamic_query;	
												
												execute l_t_dynamic_query ;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;*/
												
												--l_t_dynamic_query := 'select count(0) from '||l_t_compare_tbl_copy ;
												--execute l_t_compare_tbl_copy into i;
												--raise notice 'count in compare copy after deletion - %', i;
												
										ELSIF (l_b_is_delta = TRUE) THEN -- if 3 Delta
												---------------------------------------------------------------------------------------------------------
												-----GET ALL COMMON DATA BETWEEN UPSTREAM AND COMPARE AND DELETE THAT DATA FROM THE COPY TABLES ----------
												----------------------------------------------------------------------------------------------------------
												-------------- Get Compare table names along with alias name
												select string_agg('comp.'||compare_col_name|| ' ', ',')
															from (
															select  tb_col_nm compare_col_name  
															from t_ds_col_detail compare where dataset_id =  p_t_compare_dataset_id  
															and source_column is not null and source_column not like '%.%'
															) cc
												into l_comp_table_col_list_1 ;
												raise notice 'l_comp_table_col_list - %', l_comp_table_col_list_1;
														
												raise notice 'Start---> new deletions from compare and upstream to remove common records';
												raise notice 'l_comp_table_col_list_1 -> %',l_comp_table_col_list_1;
												raise notice 'l_t_compare_tbl_copy-> %', l_t_compare_tbl_copy;
												raise notice 'l_t_upstream_tbl_nm-> %',l_t_upstream_tbl_nm ;
												raise notice 'l_t_comp_acc_id_clmn-> %',l_t_comp_acc_id_clmn ;
												raise notice 'l_t_upstrm_acc_id_clmn-> %',l_t_upstrm_acc_id_clmn ;
												raise notice 'l_t_comp_pos_id_clmn-> %',l_t_comp_pos_id_clmn ;
												raise notice 'l_t_upstrm_pos_id_clmn-> %', l_t_upstrm_pos_id_clmn;
												raise notice 'l_account_alignment_type_compare-> %', l_account_alignment_type_compare;
												raise notice 'l_account_alignment_type_upstream-> %',l_account_alignment_type_upstream ;
												raise notice 'l_t_comp_affil_based_align -> % ', l_t_comp_affil_based_align;
												raise notice 'l_t_upstream_affil_based_align -> %', l_t_upstream_affil_based_align;
												raise notice 'l_t_assign_status_clmn -> %',l_t_assign_status_clmn ; 

												l_t_dynamic_query := ' create  temp  table/*3*/ comp_update_tbl as select  ' || l_comp_table_col_list_1 ||' from '|| l_t_compare_tbl_copy ||' comp, ' || l_t_upstream_tbl_nm || ' unstrm where comp.' || l_t_comp_acc_id_clmn||' = unstrm.'||l_t_upstrm_acc_id_clmn || ' and comp.'  || l_t_comp_pos_id_clmn || ' = unstrm.' ||l_t_upstrm_pos_id_clmn  || ' and comp.'||l_account_alignment_type_compare 
												|| ' = unstrm.'|| l_account_alignment_type_upstream
												|| ' and lower(comp.'||l_t_comp_affil_based_align ||') = lower(unstrm.'||l_t_upstream_affil_based_align ||')'
												|| ' and lower(comp.'|| l_t_assign_status_clmn ||') = ''active''';
												raise notice 'l_t_dynamic_query - %', l_t_dynamic_query;
												Execute 'Drop table if exists comp_update_tbl';
												EXECUTE l_t_dynamic_query ;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice 'Create table to delete common records between promote and compare -%', AFFECTEDROWS;
												
												Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_complete , 'Promote -  Create table with common records in Upstream and Compare -'||l_t_dynamic_query   ||'  ',
												498 , l_t_fn_name) ; 
												
												l_t_dynamic_query:= 'delete from '||l_t_compare_tbl_copy ||' comp using comp_update_tbl cm'||
															' where comp.'||l_t_comp_acc_id_clmn || ' = cm.'||l_t_comp_acc_id_clmn ||
															' and comp.'||l_t_comp_pos_id_clmn ||' = cm.'||l_t_comp_pos_id_clmn||
															' and comp.'|| l_account_alignment_type_compare || '= cm.'||l_account_alignment_type_compare 
															|| ' and lower(comp.'||l_t_comp_affil_based_align ||') = lower(cm.'||l_t_upstream_affil_based_align ||')'
															;
												raise notice 'l_t_dynamic_query - %',l_t_dynamic_query;
												raise notice 'l_t_dynamic_query - %', l_t_dynamic_query;
												EXECUTE l_t_dynamic_query ;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice 'Delete from compare copy where records not exist in custuniv -%', AFFECTEDROWS;
												
												l_t_dynamic_query:= 'delete from '||l_t_upstream_tbl_nm_copy ||' comp using comp_update_tbl cm'||
														' where comp.'||l_t_upstrm_acc_id_clmn || ' = cm.'||l_t_comp_acc_id_clmn ||
														' and comp.'||l_t_upstrm_pos_id_clmn ||' = cm.'||l_t_comp_pos_id_clmn||
														' and comp.'|| l_account_alignment_type_upstream || '= cm.'||l_account_alignment_type_compare 
														|| ' and lower(comp.'||l_t_comp_affil_based_align ||') = lower(cm.'||l_t_upstream_affil_based_align ||')';
												raise notice 'l_t_dynamic_query - %',l_t_dynamic_query;
												execute l_t_dynamic_query ;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
												raise notice 'l_t_alignment_period - %', l_t_alignment_period;
												
												
												raise notice 'End---> new deletions from compare and upstream to remove common records';
												raise notice 'l_t_promoted_table -> %',l_t_promoted_table;
												raise notice 'l_t_upstrm_acc_id_clmn -> %',l_t_upstrm_acc_id_clmn;
												raise notice 'l_t_cust_univ_acc_id_clmn -> % ',l_t_cust_univ_acc_id_clmn;
												raise notice 'l_t_cust_univ_tbl_nm -> %',l_t_cust_univ_tbl_nm;
												
												l_t_dynamic_query := 'delete from '|| l_t_promoted_table ||' upstrm  where upstrm.'||l_t_upstrm_acc_id_clmn ||'  not in (select '||l_t_cust_univ_acc_id_clmn ||' from '||l_t_cust_univ_tbl_nm ||'  ) ';
												raise notice 'l_t_dynamic_query - %', l_t_dynamic_query;
												--return 233;
												EXECUTE l_t_dynamic_query ;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice 'Delete from Promote where records not exist in custuniv -%', AFFECTEDROWS;
												
												/*l_t_dynamic_query := 'delete from '|| l_t_upstream_tbl_nm_copy ||' upstrm  where upstrm.'||l_t_upstrm_acc_id_clmn ||'  not in (select '||l_t_cust_univ_acc_id_clmn ||' from '||l_t_cust_univ_tbl_nm ||'  ) ';
												raise notice 'l_t_dynamic_query - %', l_t_dynamic_query;
												EXECUTE l_t_dynamic_query ;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice 'Delete from Promote where records not exist in custuniv -%', AFFECTEDROWS;*/
												
												l_t_dynamic_query := 'delete from '||l_t_compare_tbl_copy ||' comp where comp.'||l_t_comp_acc_id_clmn ||' not in (select '||l_t_cust_univ_acc_id_clmn ||' from '||l_t_cust_univ_tbl_nm ||'  ) ';
												raise notice 'l_t_dynamic_query - %', l_t_dynamic_query;
												EXECUTE l_t_dynamic_query ;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice 'Delete from compare copy where records not exist in custuniv -%', AFFECTEDROWS;
												
												raise notice 'l_comp_table_col_list_1->%', l_comp_table_col_list_1;
												raise notice 'l_t_compare_tbl_copy->%',l_t_compare_tbl_copy;
												raise notice 'l_t_upstream_tbl_nm->%',l_t_upstream_tbl_nm;
												raise notice 'l_t_cust_univ_tbl_nm->%',l_t_cust_univ_tbl_nm;
												raise notice 'l_t_comp_acc_id_clmn->%',l_t_comp_acc_id_clmn;
												raise notice 'l_t_upstrm_acc_id_clmn->%',l_t_upstrm_acc_id_clmn;
												raise notice 'l_t_comp_pos_id_clmn->%',l_t_comp_pos_id_clmn;
												raise notice 'l_t_upstrm_pos_id_clmn->%',l_t_upstrm_pos_id_clmn;
												raise notice 'l_t_cust_univ_acc_id_clmn->%',l_t_cust_univ_acc_id_clmn;
												raise notice 'l_t_upstrm_acc_id_clmn->%',l_t_upstrm_acc_id_clmn;
												raise notice 'l_t_assign_status_clmn->%',l_t_assign_status_clmn;
												
												l_t_dynamic_query := ' create  temp  table/*3*/ comp_update_tbl as select  ' || l_comp_table_col_list_1 ||' from '|| l_t_compare_tbl_copy ||' comp, ' || l_t_upstream_tbl_nm || ' unstrm, '|| l_t_cust_univ_tbl_nm || ' custuniv where comp.' || l_t_comp_acc_id_clmn||' = unstrm.'||l_t_upstrm_acc_id_clmn || ' and comp.'  || l_t_comp_pos_id_clmn || ' = unstrm.' ||l_t_upstrm_pos_id_clmn  ||' and custuniv.'|| l_t_cust_univ_acc_id_clmn || ' = unstrm.' ||l_t_upstrm_acc_id_clmn 
												||' and lower(comp.'|| l_t_assign_status_clmn ||') = ''active'''; 
														
												raise notice 'l_t_dynamic_query -> %',l_t_dynamic_query;							
												EXECUTE 'DROP TABLE IF EXISTS comp_update_tbl ';
												execute l_t_dynamic_query ;
												
												l_t_dynamic_query := 'select count(0) from   comp_update_tbl ' ;
												execute l_t_dynamic_query into i;
												raise notice 'count in table comp_update_tbl - %',  i;	
												
										end if; -- if 3 Delta
						
									RAISE NOTICE 'AFTER DELTA IF ELSE ';
									
									
									
									update t_promote_scn_col_detls set compare_col_name_1 ='lower('||compare_col_name_1||'::text)';
									
									update t_promote_scn_col_detls set upstream_col_name_1 ='lower('||upstream_col_name_1||'::text)' ;
															
									select   string_agg(upstream_col_name_1||'  '||upstream_col_name  , '  ,')  
											from (
									select upstream_col_name ,upstream_col_name_1  from t_promote_scn_col_detls
									order by row_num asc
										) cc	into 	l_t_promote_tbl_clmn_2 ;
									
															
									raise notice 'l_t_promote_tbl_clmn_2 - %',l_t_promote_tbl_clmn_2;
									
									select   string_agg(compare_col_name_1||'  '||compare_col_name , '  ,')  
											from (
									select compare_col_name ,compare_col_name_1  from t_promote_scn_col_detls
									order by row_num asc
										) cc  into l_comp_table_col_list_2; 
									
									raise notice 'l_comp_table_col_list_2 - %',l_comp_table_col_list_2;
										
									/*l_t_dynamic_query := ' delete from comp_update_tbl tb where '|| l_t_comp_acc_id_clmn '||'l_t_comp_pos_id_clmn 
														  || ' in  ( select '|| l_t_comp_acc_id_clmn '||'l_t_comp_pos_id_clmn 
														  || ' from ( select ' || l_comp_table_col_list_2
														  ||' from comp_update_tbl comp '
														  || ' except '
														  || ' select '||l_t_promote_tbl_clmn_2 
														  || ' from ' || l_t_upstream_tbl_nm || ' upstrm ) cc )' ;*/
									
									l_t_dynamic_query:= ' create temp table comp_update_tbl_2 as  select '
														  ||  l_t_comp_acc_id_clmn|| '||'||l_t_comp_pos_id_clmn 
														  || ' from (( select ' || l_comp_table_col_list_2
														  ||' from comp_update_tbl comp '
														  || ' except '
														  || ' select '||l_t_promote_tbl_clmn_2 
														  || ' from ' || l_t_upstream_tbl_nm || ' upstrm) '
														  || ' except '
														  || ' ( select '||l_t_promote_tbl_clmn_2 
														  || ' from ' || l_t_upstream_tbl_nm || ' upstrm '
														  || ' except '
														  || '  select ' || l_comp_table_col_list_2
														  ||' from comp_update_tbl comp ) '
														  || ' ) cc ';
									--Execute ' drop table if exists comp_update_tbl_2 '; 
									
									--raise notice 'query to delete duplicate data -%',l_t_dynamic_query;
									--execute l_t_dynamic_query ; 
									--l_t_dynamic_query := ' select count(0) from comp_update_tbl_2 ' ;
									--execute l_t_dynamic_query into i;
									--raise notice 'total records in comp_update_tbl_2 - %' , i;
									
									--l_t_dynamic_query:= ' delete from comp_update_tbl comp where '||l_t_comp_acc_id_clmn || '||'||l_t_comp_pos_id_clmn 
									--|| ' in (select '|| l_t_comp_acc_id_clmn|| '||'|| l_t_comp_pos_id_clmn ||' from  comp_update_tbl_2 )' ;
											
									
									--raise notice ' Query to delete the data from comp_update_tbl which are not common in both';
									--raise notice 'l_t_dynamic_query -%',l_t_dynamic_query;
									--execute l_t_dynamic_query ; 
									--get diagnostics AFFECTEDROWS = row_count;
									--raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
									raise notice '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>';
									--create table comp_update_tbl_1 as select * from comp_update_tbl;
									alter table comp_update_tbl add diff text;
									
									for l_r_cursor in 
									select source_column , compare_col_name , upstream_col_name from t_promote_scn_col_detls
									   where lower(compare_col_name) not like 'effective%start%date%'
									   order by row_num asc
									loop
											i:=0;							
											
											RAISE NOTICE 'l_t_upstream_tbl_nm_copy->%',l_t_upstream_tbl_nm_copy;
											raise notice 'l_t_comp_acc_id_clmn-> %',l_t_comp_acc_id_clmn;
											raise notice 'l_t_comp_pos_id_clmn->%',l_t_comp_pos_id_clmn;
											raise notice 'l_t_upstrm_acc_id_clmn-> %',l_t_upstrm_acc_id_clmn;
											raise notice 'l_t_upstrm_pos_id_clmn->%',l_t_upstrm_pos_id_clmn;
											raise notice 'compare_col_name ->%',l_r_cursor.compare_col_name;
											raise notice 'upstream_col_name->%',l_r_cursor.upstream_col_name;
											
											l_t_dynamic_query := 'update comp_update_tbl comp set diff = ''diff'''
																|| ' from  '||l_t_upstream_tbl_nm_copy || ' upstrm  '
																|| ' where comp.'||l_t_comp_acc_id_clmn || ' = upstrm.'||l_t_upstrm_acc_id_clmn  
																||' and comp.'||l_t_comp_pos_id_clmn ||' = upstrm.'||l_t_upstrm_pos_id_clmn
																||'  and lower(comp.'||l_r_cursor.compare_col_name||'::text)  <> lower(upstrm.'
																||l_r_cursor.upstream_col_name || '::text)' 
																|| ' and coalesce(diff,''a'') <> ''diff''';
											raise notice 'l_t_dynamic_query - > %',l_t_dynamic_query;
											execute l_t_dynamic_query;
											
											 
									end loop;				 			
									
									update comp_update_tbl comp set diff ='same' where diff is null;
									get diagnostics AFFECTEDROWS = row_count;
									raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
									raise notice 'updated diff = same';
									delete from comp_update_tbl where diff = 'diff';
									get diagnostics AFFECTEDROWS = row_count;
									raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
									raise notice 'end>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>';
									
									Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Update differnet recods -'||l_t_dynamic_query   ||'  ',
												751 , l_t_fn_name) ; 
									
									/*  Data from compare table to be deleted only in case of delta = false  */
									IF (l_b_is_delta = FALSE) THEN 
									-- DELTA IF 2
												raise notice 'inside delta if - 2';
												l_t_dynamic_query:= 'delete from '||l_t_compare_tbl_copy ||' comp using comp_update_tbl cm'||
															' where comp.'||l_t_comp_acc_id_clmn || ' = cm.'||l_t_comp_acc_id_clmn ||
															' and comp.'||l_t_comp_pos_id_clmn ||' = cm.'||l_t_comp_pos_id_clmn||
															' and comp.'|| l_account_alignment_type_compare || '= cm.'||l_account_alignment_type_compare ;
												raise notice 'l_t_dynamic_query - %',l_t_dynamic_query;	
												
												execute l_t_dynamic_query ;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
									end if;
									 
									Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_complete, 'In case Delta = True, do not touch extra compare data -'||l_t_dynamic_query   ||'  ',
												770 , l_t_fn_name) ;
												
									l_t_dynamic_query:= 'delete from '||l_t_upstream_tbl_nm_copy ||' comp using comp_update_tbl cm'||
														' where comp.'||l_t_upstrm_acc_id_clmn || ' = cm.'||l_t_comp_acc_id_clmn ||
														' and comp.'||l_t_upstrm_pos_id_clmn ||' = cm.'||l_t_comp_pos_id_clmn||
														' and comp.'|| l_account_alignment_type_upstream || '= cm.'||l_account_alignment_type_compare ;
									raise notice 'l_t_dynamic_query - %',l_t_dynamic_query;
									execute l_t_dynamic_query ;
									get diagnostics AFFECTEDROWS = row_count;
									raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
									raise notice 'l_t_alignment_period - %', l_t_alignment_period;
						
						
								
								raise notice 'l_comp_table_col_list-%',l_comp_table_col_list;
								raise notice 'l_t_comp_end_dt_clmn-%',l_t_comp_end_dt_clmn;
								-- insert unmatched compare table records with Update flag and end date = D-1 start for active
								l_comp_table_col_list := replace(l_comp_table_col_list ,l_t_comp_end_dt_clmn  ,'(clock_timestamp() AT TIME ZONE ''UTC'')::date-1'  ) ;  
								
								raise notice 'l_comp_table_col_list-%',l_comp_table_col_list;
								raise notice 'l_t_promoted_table-%',l_t_promoted_table;
								raise notice 'l_t_upstrm_flag_clmn-%',l_t_upstrm_flag_clmn;
								raise notice 'l_comp_table_col_list - %',l_comp_table_col_list;
								raise notice 'l_c_update-%',l_c_update;
								raise notice 'l_t_compare_tbl_copy-%',l_t_compare_tbl_copy;
								raise notice 'l_t_assign_status_clmn-%',l_t_assign_status_clmn;
								
								l_t_dynamic_query:= 'insert into '|| l_t_promoted_table ||'(' || l_t_promote_tbl_clmn ||', '
													|| l_t_upstrm_flag_clmn || ') select ' ||l_comp_table_col_list ||' , '''|| l_c_update
													||''' '
													|| ' from '|| l_t_compare_tbl_copy 
													|| ' where lower('|| l_t_assign_status_clmn || ') = lower(''active'') ' ;
								raise notice 'l_t_dynamic_query - %',l_t_dynamic_query;
								execute l_t_dynamic_query;
								get diagnostics AFFECTEDROWS = row_count;
								raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
								-- insert unmatched compare table records with Update flag and end date = D-1 start
								Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Insert unmatched compare table records with Update flag and end date = D-1 start  -'||l_t_dynamic_query   ||'  ',
												809 , l_t_fn_name) ;
								
								-- insert with Delete flag for Update
								l_t_dynamic_query:= 'insert into '|| l_t_promoted_table ||'(' || l_t_promote_tbl_clmn ||', '
													--|| l_t_upstrm_flag_clmn || ') select ' ||l_comp_table_col_list ||' , '''|| l_c_delete
													|| l_t_upstrm_flag_clmn || ') select ' ||l_comp_table_col_list ||' , '''|| l_c_update
													||''' '
													|| ' from '|| l_t_compare_tbl_copy 
													|| ' where lower('|| l_t_assign_status_clmn || ') = lower(''Future Active'') ' ;
								raise notice 'l_t_dynamic_query - %',l_t_dynamic_query;
								execute l_t_dynamic_query;
								get diagnostics AFFECTEDROWS = row_count;
								raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
								
								raise notice 'l_t_promote_tbl_clmn-%',l_t_promote_tbl_clmn;
								raise notice 'l_t_upstrm_start_dt_clmn-%',l_t_upstrm_start_dt_clmn;
								-- insert upstream records with Insert flag and start date = today's date
								l_t_promote_tbl_clmn_1 := replace(l_t_promote_tbl_clmn ,l_t_upstrm_start_dt_clmn  ,'(clock_timestamp() AT TIME ZONE ''UTC'')::date'  ) ; 
								raise notice 'l_t_promote_tbl_clmn-%',l_t_promote_tbl_clmn;
								l_t_dynamic_query:= 'insert into '|| l_t_promoted_table ||'(' || l_t_promote_tbl_clmn ||', '
													|| l_t_upstrm_flag_clmn || ') select ' ||l_t_promote_tbl_clmn_1 ||' , '''|| l_c_insert
													||''' '
													|| ' from '|| l_t_upstream_tbl_nm_copy ;
								raise notice 'l_t_dynamic_query - %',l_t_dynamic_query;
								execute l_t_dynamic_query;
								get diagnostics AFFECTEDROWS = row_count;
								raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
								
								Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Insert upstream records with Insert flag and start date = todays date  -'||l_t_dynamic_query   ||'  ',
												839 , l_t_fn_name) ;
													 	
						-- ELSE IF 2
						ELSIF(lower(l_t_alignment_period) = 'future') THEN
									
									
									-------------------------- Insert upstream data into the new promoted table ------------------------------
									l_t_dynamic_query := 'insert into '|| l_t_promoted_table ||'('|| l_t_promote_tbl_clmn ||') select '
									|| l_t_promote_tbl_clmn || ' from ' || l_t_upstream_tbl_nm_copy  ;
									EXECUTE l_t_dynamic_query ;
									
									get diagnostics AFFECTEDROWS = row_count;
									raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
									raise notice 'Insert upstream data into the new promoted table l_t_dynamic_query - %', l_t_dynamic_query ;
									raise notice 'l_t_dataset_object_name - %', l_t_dataset_object_name;
									
									Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Insert upstream data into the new promoted table l_t_dynamic_query - '||l_t_dynamic_query   ||'  ',
												857 , l_t_fn_name) ;
									
									------ Compare table query with active data
									--l_t_comp_actv_tb_query := 'select * from '|| l_t_compare_tbl || ' where '|| l_filter_condition ;
									raise notice 'l_t_alignment_period -%', l_t_alignment_period ; 
									l_t_comp_actv_tb_query := 'select * from '|| l_t_compare_tbl_copy   ;
									
									raise notice 'l_t_comp_actv_tb_query -%', l_t_comp_actv_tb_query;
									

									l_t_promote_tbl_clmn_Futr_tm := replace(l_t_promote_tbl_clmn,l_t_upstrm_end_dt_clmn,''''||l_t_team_ins_end_date|| '''');	
									
									raise notice 'l_t_promote_tbl_clmn_Futr_tm-%',l_t_promote_tbl_clmn_Futr_tm;
									
									l_t_promote_tbl_clmn_Futr_tm := replace(l_t_promote_tbl_clmn_Futr_tm,l_t_upstrm_start_dt_clmn,''''||l_t_team_ins_start_date||'''');
									raise notice 'l_t_promote_tbl_clmn_Futr_tm - %', l_t_promote_tbl_clmn_Futr_tm; 
									
									--------------- Get Compare table names along with alias name
									select string_agg('comp.'||compare_col_name|| ' ', ',')
										from (
										select  tb_col_nm compare_col_name  
										from t_ds_col_detail compare where dataset_id =  p_t_compare_dataset_id  
										and source_column is not null and source_column not like '%.%'
										) cc
									into l_comp_table_col_list_1 ;
									raise notice 'l_comp_table_col_list_1 - %', l_comp_table_col_list_1;
									-------------------------- Columns for promote table -- all columns from upstream table 
									SELECT string_agg('unstrm.' || upstream_col_name, ',') 
										From  (select *  FROM t_promote_scn_col_detls 
										 order by row_num) Cc
										INTO l_t_promote_tbl_clmn_1;
									raise notice 'l_t_promote_tbl_clmn_1- %',l_t_promote_tbl_clmn_1 ; 
									
									-- IF 3
									IF(l_b_is_delta = FALSE ) THEN
											---- Get data for accounts
											raise notice ' inside delta = false';
											raise notice 'l_comp_table_col_list_1 - %' , l_comp_table_col_list_1;
											raise notice 'l_t_comp_actv_tb_query - %',l_t_comp_actv_tb_query;
											raise notice 'l_t_upstream_tbl_nm-%', l_t_upstream_tbl_nm;
											raise notice 'l_t_upstrm_acc_id_clmn -%', l_t_upstrm_acc_id_clmn;
											raise notice  'l_t_comp_acc_id_clmn - %', l_t_comp_acc_id_clmn;
											raise notice 'l_t_comp_pos_id_clmn-% ', l_t_comp_pos_id_clmn;
											raise notice 'l_t_upstrm_pos_id_clmn -%', l_t_upstrm_pos_id_clmn;
											
											EXECUTE 'drop table if exists comp_update_tbl';
											l_t_dynamic_query := ' create temp  table comp_update_tbl as select '
											|| l_comp_table_col_list_1 || ' from '
											|| l_t_compare_tbl_copy ||' comp, ' 
											|| l_t_upstream_tbl_nm || ' unstrm  where comp.'|| l_t_comp_acc_id_clmn||' = unstrm.'
											||l_t_upstrm_acc_id_clmn || ' and comp.'
											|| l_t_comp_pos_id_clmn || ' = unstrm.' ||l_t_upstrm_pos_id_clmn 
											|| ' and lower(comp.'||l_account_alignment_type_compare ||') = lower(unstrm.'|| l_account_alignment_type_upstream|| ')' ;
											
											raise notice 'l_t_dynamic_query - %', l_t_dynamic_query;
											
											EXECUTE l_t_dynamic_query ;
											
											raise notice 'table created';
											
											Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Create table with common data - '||l_t_dynamic_query   ||'  ',
												918 , l_t_fn_name) ;
											
											raise notice 'l_t_promoted_table -%', l_t_promoted_table;
											raise notice 'l_account_alignment_type_upstream - %', l_account_alignment_type_upstream;
											raise notice 'l_account_alignment_type_compare - %', l_account_alignment_type_compare;
											raise notice 'l_t_upstrm_acc_id_clmn - %', l_t_upstrm_acc_id_clmn;
											raise notice 'l_t_comp_acc_id_clmn - %', l_t_comp_acc_id_clmn;
											raise notice 'l_t_upstrm_pos_id_clmn - %', l_t_upstrm_pos_id_clmn;
											raise notice 'l_t_comp_pos_id_clmn - %', l_t_comp_pos_id_clmn;
											
											/*l_t_dynamic_query := 'update '|| l_t_promoted_table || ' unstrm set '|| l_account_alignment_type_upstream 
																|| ' = '||' comp.'|| l_account_alignment_type_compare
																|| ' from comp_update_tbl comp  where '
																|| ' unstrm.'||l_t_upstrm_acc_id_clmn || ' = comp.'||l_t_comp_acc_id_clmn
																|| ' and unstrm.'||l_t_upstrm_pos_id_clmn ||' = comp.'||l_t_comp_pos_id_clmn;
											
																				
											raise notice 'Update done for implicit to explict and vis-a-vis cases - l_t_dynamic_query - %',l_t_dynamic_query;
											execute l_t_dynamic_query;
											get diagnostics AFFECTEDROWS = row_count;
											raise notice 'Update done for implicit to explict and vis-a-vis cases -%', AFFECTEDROWS;
											*/
											
											l_t_dynamic_query := 'delete from '|| l_t_promoted_table ||' unstrm using comp_update_tbl comp  
											where comp.'|| l_t_comp_acc_id_clmn||' = unstrm.'||l_t_upstrm_acc_id_clmn || ' and 
											comp.'|| l_t_comp_pos_id_clmn || ' = unstrm.' ||l_t_upstrm_pos_id_clmn 
											|| ' and comp.'|| l_account_alignment_type_compare|| ' =  unstrm.'||l_account_alignment_type_upstream
											|| ' and lower(comp.'||l_t_comp_affil_based_align||') = lower(unstrm.'|| l_t_upstream_affil_based_align || ' )  ' ;
											
											raise notice 'l_t_dynamic_query -%',l_t_dynamic_query;
											EXECUTE l_t_dynamic_query ;
											get diagnostics AFFECTEDROWS = row_count;
											raise notice 'delete from upstream -%', AFFECTEDROWS;
											raise notice 'delete 2';
											
											Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Delete common accounts from Promoted Table - '||l_t_dynamic_query   ||'  ',
												956 , l_t_fn_name) ;
											
											--l_t_dynamic_query := 'insert into '|| l_t_promoted_table || '(' || l_t_promote_tbl_clmn ||', '|| l_t_upstrm_flag_clmn || ')
											--select ' || l_comp_table_col_list ||','''|| l_c_delete ||''' from  comp_update_tbl';
											--raise notice 'l_t_dynamic_query -%',l_t_dynamic_query;
											--EXECUTE l_t_dynamic_query ;
											--get diagnostics AFFECTEDROWS = row_count;
											--raise notice 'insert into promoted table -%', AFFECTEDROWS;
											--raise notice 'insert done';

											l_t_dynamic_query := 'Update '||l_t_promoted_table || ' set '||l_t_upstrm_flag_clmn|| ' = '''
											||l_c_insert || '''  where '||l_t_upstrm_flag_clmn|| ' is null '; 
											raise notice 'l_t_dynamic_query -%',l_t_dynamic_query;
											EXECUTE l_t_dynamic_query ;	
											get diagnostics AFFECTEDROWS = row_count;
											raise notice 'update promoted table -%', AFFECTEDROWS;
											raise notice 'update done';
											
											Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Update promote to update flag to Insert - '||l_t_dynamic_query   ||'  ',
												975 , l_t_fn_name) ;
											
											/*l_t_dynamic_query := 'delete from ' || l_t_promoted_table  || ' upstrm using '||l_t_compare_tbl_copy 				
															||' comp  where comp.'|| l_t_comp_acc_id_clmn||' = upstrm.'||l_t_upstrm_acc_id_clmn 
															|| ' and 	comp.'|| l_t_comp_pos_id_clmn || ' = upstrm.' ||l_t_upstrm_pos_id_clmn 
															|| ' and upstrm.' ||l_t_upstrm_end_dt_clmn || ' = ''' || l_t_team_ins_end_date ||''' ' 
															|| ' and  upstrm.' || l_t_upstrm_start_dt_clmn || ' = comp.'||l_t_comp_start_dt_clmn 
															|| ' and  comp.' || l_account_alignment_type_compare || ' = upstrm.'|| l_account_alignment_type_upstream ;
										
										
											raise notice 'NEW CHECK to compare start,end, explicit, implicit -%',l_t_dynamic_query;
											--return 22;
											EXECUTE l_t_dynamic_query ;
											get diagnostics AFFECTEDROWS = row_count;
											raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;*/
											
											--- New Code for taking care of extra records in compare which are not there in the upstream table --set their date as D-1
												l_t_dynamic_query := ' create  temp  /*1*/table comp_update_tbl as select  ' || l_comp_table_col_list_1 ||' from '
																				--|| l_t_compare_tbl ||' 	comp, ' 
																				|| l_t_compare_tbl_copy ||' 	comp, ' 
																				|| l_t_upstream_tbl_nm || ' unstrm  where comp.'|| l_t_comp_acc_id_clmn||' = unstrm.'||l_t_upstrm_acc_id_clmn || ' and comp.'
																				|| l_t_comp_pos_id_clmn || ' = unstrm.' ||l_t_upstrm_pos_id_clmn 
																				|| ' and '|| l_filter_condition
																				|| ' and comp.'|| l_account_alignment_type_compare||' = unstrm.'|| l_account_alignment_type_upstream 
																				|| ' and lower(comp.'||l_t_comp_affil_based_align||') = lower(unstrm.'|| l_t_upstream_affil_based_align ||' )' ;
																				
												raise notice 'Create table extra records in compare which are not there in the upstream table -%',l_t_dynamic_query;
												EXECUTE 'drop table if exists comp_update_tbl';
												execute l_t_dynamic_query ; 
												raise notice 'table created ';
												
												l_t_dynamic_query := ' insert into '||l_t_promoted_table ||' ( '||l_t_promote_tbl_clmn || ' , ' || l_t_upstrm_sfdc_id_clmn
																	|| ' , ' || l_t_upstrm_flag_clmn
																	|| ' ) select '|| l_comp_table_col_list || ' , '|| l_t_comp_sfdc_id_clmn ||' , ' 
																	|| ''''|| l_c_delete ||''' from '
																	|| l_t_compare_tbl_copy || '  tt ' 
																	|| ' where tt.' ||l_t_comp_sfdc_id_clmn ||   ' not in (select  '|| l_t_comp_sfdc_id_clmn  || ' from comp_update_tbl ) ';
																	
												raise notice 'insert query - %', l_t_dynamic_query;
												execute l_t_dynamic_query;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice 'extra records in compare which are not there in the upstream table -%', AFFECTEDROWS;
									-- ELSE IF 3
									ELSIF(l_b_is_delta = TRUE) THEN	
												raise notice 'inside delta false - future';
												---- Get data for customer universe accounts
												EXECUTE 'drop table if exists comp_update_tbl';
												--raise notice 'l_t_comp_sfdc_id_clmn_1 -%', l_t_comp_sfdc_id_clmn_1;
												raise notice 'l_comp_table_col_list_1-%', l_comp_table_col_list_1;
												raise notice 'l_t_compare_tbl_copy - %', l_t_compare_tbl_copy;
												raise notice 'l_t_upstream_tbl_nm -%', l_t_upstream_tbl_nm;
												raise notice 'l_t_comp_actv_tb_query -%', l_t_comp_actv_tb_query;
												raise notice 'l_t_upstream_tbl_nm - %', l_t_upstream_tbl_nm;
												raise notice 'l_t_cust_univ_tbl_nm -%', l_t_cust_univ_tbl_nm;
												raise notice 'l_t_comp_acc_id_clmn -%', l_t_comp_acc_id_clmn;
												raise notice 'l_t_upstrm_acc_id_clmn -%',l_t_upstrm_acc_id_clmn;
												raise notice 'l_t_comp_pos_id_clmn - %', l_t_comp_pos_id_clmn;
												raise notice 'l_t_upstrm_pos_id_clmn - %', l_t_upstrm_pos_id_clmn;
												raise notice 'l_t_cust_univ_acc_id_clmn-%', l_t_cust_univ_acc_id_clmn;
												raise notice 'l_t_upstrm_acc_id_clmn -%', l_t_upstrm_acc_id_clmn;
												
												/*l_t_dynamic_query := 'delete from '|| l_t_promoted_table ||' upstrm  where upstrm.'||l_t_upstrm_acc_id_clmn ||'  not in (select '||l_t_cust_univ_acc_id_clmn ||' from '||l_t_cust_univ_tbl_nm ||'  ) ';
												raise notice 'l_t_dynamic_query - %', l_t_dynamic_query;
												EXECUTE l_t_dynamic_query ;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice 'Delete from Promote where records not exist in custuniv -%', AFFECTEDROWS;*/
												
												l_t_dynamic_query := 'delete from '||l_t_compare_tbl_copy ||' comp where comp.'||l_t_comp_acc_id_clmn ||' not in (select '||l_t_cust_univ_acc_id_clmn ||' from '||l_t_cust_univ_tbl_nm ||'  ) ';
												raise notice 'l_t_dynamic_query - %', l_t_dynamic_query;
												EXECUTE l_t_dynamic_query ;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice 'Delete from compare copy where records not exist in custuniv -%', AFFECTEDROWS;
												
												l_t_dynamic_query := ' create temp  table/*3*/ comp_update_tbl as select  ' 
												|| l_comp_table_col_list_1 ||' from '|| l_t_compare_tbl_copy ||' comp, ' 
												|| l_t_upstream_tbl_nm || ' unstrm, '|| l_t_cust_univ_tbl_nm || ' custuniv where comp.' 
												|| l_t_comp_acc_id_clmn||' = unstrm.'||l_t_upstrm_acc_id_clmn || ' and comp.'  
												|| l_t_comp_pos_id_clmn || ' = unstrm.' ||l_t_upstrm_pos_id_clmn  
												||' and custuniv.'|| l_t_cust_univ_acc_id_clmn || ' = unstrm.' ||l_t_upstrm_acc_id_clmn  ;
												
												
												raise notice 'l_t_dynamic_query - %', l_t_dynamic_query;
												EXECUTE l_t_dynamic_query ;
												raise notice 'Get data for customer universe accounts -%',l_t_dynamic_query;
												
												Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_complete , 'Create table with common data - '||l_t_dynamic_query   ||'  ',
												1062 , l_t_fn_name) ;
												
												l_t_dynamic_query := 'delete from '|| l_t_promoted_table ||' upstrm  where upstrm.'||l_t_upstrm_acc_id_clmn ||'  not in (select '||l_t_cust_univ_acc_id_clmn ||' from '||l_t_cust_univ_tbl_nm ||'  ) ';
												raise notice 'l_t_dynamic_query - %', l_t_dynamic_query;
												EXECUTE l_t_dynamic_query ;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice 'Delete from Promote where records not exist in custuniv -%', AFFECTEDROWS;
												
												
												raise notice 'l_t_promoted_table -%', l_t_promoted_table;
												raise notice 'l_account_alignment_type_upstream - %', l_account_alignment_type_upstream;
												raise notice 'l_account_alignment_type_compare - %', l_account_alignment_type_compare;
												raise notice 'l_t_upstrm_acc_id_clmn - %', l_t_upstrm_acc_id_clmn;
												raise notice 'l_t_comp_acc_id_clmn - %', l_t_comp_acc_id_clmn;
												raise notice 'l_t_upstrm_pos_id_clmn - %', l_t_upstrm_pos_id_clmn;
												raise notice 'l_t_comp_pos_id_clmn - %', l_t_comp_pos_id_clmn;
												
												l_t_dynamic_query := 'update '|| l_t_promoted_table || ' unstrm set '|| l_account_alignment_type_upstream 
																	|| ' = '||' comp.'|| l_account_alignment_type_compare
																	|| ' from comp_update_tbl comp  where '
																	|| ' unstrm.'||l_t_upstrm_acc_id_clmn || ' = comp.'||l_t_comp_acc_id_clmn
																	|| ' and unstrm.'||l_t_upstrm_pos_id_clmn ||' = comp.'||l_t_comp_pos_id_clmn;
												
																					
												raise notice 'Update done for implicit to explict and vis-a-vis cases - l_t_dynamic_query - %',l_t_dynamic_query;
												execute l_t_dynamic_query;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice 'Update done for implicit to explict and vis-a-vis cases -%', AFFECTEDROWS;
												
												
												l_t_dynamic_query := 'delete from '|| l_t_promoted_table ||' unstrm using comp_update_tbl comp  
												where comp.'|| l_t_comp_acc_id_clmn||' = unstrm.'||l_t_upstrm_acc_id_clmn || ' and 
												comp.'|| l_t_comp_pos_id_clmn || ' = unstrm.' ||l_t_upstrm_pos_id_clmn 
												|| ' and comp.'|| l_account_alignment_type_compare|| ' =  unstrm.'||l_account_alignment_type_upstream
												|| ' and lower( comp.'||l_t_comp_affil_based_align||') = lower(unstrm.'|| l_t_upstream_affil_based_align ||' ) ' ;
												
												raise notice 'l_t_dynamic_query -%',l_t_dynamic_query;
												EXECUTE l_t_dynamic_query ;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice 'delete from upstream -%', AFFECTEDROWS;
												raise notice 'delete 2';
												
												--l_t_dynamic_query := 'insert into '|| l_t_promoted_table || '(' || l_t_promote_tbl_clmn ||', '|| l_t_upstrm_flag_clmn || ')
												--select ' || l_comp_table_col_list ||','''|| l_c_delete ||''' from  comp_update_tbl';
												--raise notice 'l_t_dynamic_query -%',l_t_dynamic_query;
												--EXECUTE l_t_dynamic_query ;
												--get diagnostics AFFECTEDROWS = row_count;
												--raise notice 'insert into promoted table -%', AFFECTEDROWS;
												--raise notice 'insert done';

												l_t_dynamic_query := 'Update '||l_t_promoted_table || ' set '||l_t_upstrm_flag_clmn|| ' = '''
												||l_c_insert || '''  where '||l_t_upstrm_flag_clmn|| ' is null '; 
												raise notice 'l_t_dynamic_query -%',l_t_dynamic_query;
												EXECUTE l_t_dynamic_query ;	
												get diagnostics AFFECTEDROWS = row_count;
												raise notice 'update promoted table -%', AFFECTEDROWS;
												raise notice 'update done';
												
												l_t_dynamic_query := 'delete from ' || l_t_promoted_table  || ' upstrm using '||l_t_compare_tbl_copy 				
																||' comp  where comp.'|| l_t_comp_acc_id_clmn||' = upstrm.'||l_t_upstrm_acc_id_clmn 
																|| ' and 	comp.'|| l_t_comp_pos_id_clmn || ' = upstrm.' ||l_t_upstrm_pos_id_clmn 
																|| ' and upstrm.' ||l_t_upstrm_end_dt_clmn || ' = ''' || l_t_team_ins_end_date ||''' ' 
																|| ' and  upstrm.' || l_t_upstrm_start_dt_clmn || ' = comp.'||l_t_comp_start_dt_clmn 
																|| ' and  comp.' || l_account_alignment_type_compare || ' = upstrm.'|| l_account_alignment_type_upstream 
																|| ' and lower(comp.'||l_t_comp_affil_based_align||') = lower(upstrm.'|| l_t_upstream_affil_based_align ||' ) '  ;
											
											
												raise notice 'NEW CHECK to compare start,end, explicit, implicit -%',l_t_dynamic_query;
												--return 22;
												EXECUTE l_t_dynamic_query ;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
												
												--- New Code for taking care of extra records in compare which are not there in the upstream table --set their date as D-1
													l_t_dynamic_query := ' create  temp /*1*/table comp_update_tbl as select  ' || l_comp_table_col_list_1 ||' from '
																					--|| l_t_compare_tbl ||' 	comp, ' 
																					|| l_t_compare_tbl_copy ||' 	comp, ' 
																					|| l_t_upstream_tbl_nm || ' unstrm  where comp.'|| l_t_comp_acc_id_clmn||' = unstrm.'||l_t_upstrm_acc_id_clmn || ' and comp.'
																					|| l_t_comp_pos_id_clmn || ' = unstrm.' ||l_t_upstrm_pos_id_clmn 
																					|| ' and '|| l_filter_condition
																					|| ' and lower(comp.'||l_t_comp_affil_based_align||') = lower(unstrm.'|| l_t_upstream_affil_based_align ||' ) ' ;
																					
													raise notice 'Create table extra records in compare which are not there in the upstream table -%',l_t_dynamic_query;
													EXECUTE 'drop table if exists comp_update_tbl';
													execute l_t_dynamic_query ; 
													raise notice 'table created ';
													
													l_t_dynamic_query := ' insert into '||l_t_promoted_table ||' ( '||l_t_promote_tbl_clmn || ' , ' || l_t_upstrm_sfdc_id_clmn
																		|| ' , ' || l_t_upstrm_flag_clmn
																		|| ' ) select '|| l_comp_table_col_list || ' , '|| l_t_comp_sfdc_id_clmn ||' , ' 
																		|| ''''|| l_c_delete ||''' from '
																		|| l_t_compare_tbl_copy || '  tt ' 
																		|| ' where tt.' ||l_t_comp_sfdc_id_clmn ||   ' not in (select  '|| l_t_comp_sfdc_id_clmn  || ' from comp_update_tbl ) ';
																		
													raise notice 'insert query - %', l_t_dynamic_query;
													execute l_t_dynamic_query;
													get diagnostics AFFECTEDROWS = row_count;
													raise notice 'extra records in compare which are not there in the upstream table -%', AFFECTEDROWS;
												
												
												
									-- IF 3 Close
									END IF ;
									
																	
									
								
						end if;
						-- If 2
						
							----------- New Update  - Python needs Id column for Update,Delete flags
								raise notice 'l_t_promoted_table - %', l_t_promoted_table;
								raise notice 'l_upstrm_id_col - %', l_upstrm_id_col;
								raise notice 'l_t_compare_tbl_copy - %', l_t_compare_tbl_copy;
								raise notice 'l_t_upstrm_acc_id_clmn - %', l_t_upstrm_acc_id_clmn;
								raise notice 'l_t_upstrm_pos_id_clmn - %', l_t_upstrm_pos_id_clmn;
								raise notice 'l_t_comp_pos_id_clmn - %', l_t_comp_pos_id_clmn;
								raise notice 'l_t_comp_acc_id_clmn - %', l_t_comp_acc_id_clmn;
								
								l_t_dynamic_query :=  ' Update ' || l_t_promoted_table || ' prom set id =  comp.' || l_upstrm_id_col 
													|| ' from '||l_t_compare_tbl_copy || ' comp where prom.'
													||  l_t_upstrm_pos_id_clmn ||' = comp.'||l_t_comp_pos_id_clmn 
													|| ' and 	prom.'|| l_t_upstrm_acc_id_clmn || ' = comp.' ||l_t_comp_acc_id_clmn  ;
					
								
								raise notice 'New Update  - Python needs Id column for Update,Delete flags -%',l_t_dynamic_query; 
								EXECUTE l_t_dynamic_query ;
								get diagnostics AFFECTEDROWS = row_count;
								raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
								
								Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_complete , 'Update Id column for Python - '||l_t_dynamic_query   ||'  ',
												1195 , l_t_fn_name) ;
						
				/* *********************************************Exclusion Starts ********************************* */		
				-- ELSE IF 1			
				ELSIF (lower(l_t_dataset_object_name) = 'account_exclusion__c' or lower(l_t_dataset_object_name) = 'axtriasalesiqtm__account_exclusion__c') THEN
						raise notice '**********************************************************************************************';
							raise notice 'inside else if1';
							------------------------------- Create a copy of compare table -------------------------------
							--- compare tables are huge in size --- copy created for performance improvement -------------
							l_t_compare_tbl_copy := l_t_compare_tbl || '_1';
							EXECUTE 'drop table if exists ' || l_t_compare_tbl_copy ;
							raise notice 'l_t_compare_tbl_copy - %', l_t_compare_tbl_copy;
							raise notice 'l_t_compare_tbl -%' , l_t_compare_tbl ; 
							EXECUTE 'Create  temp  table '|| l_t_compare_tbl_copy ||' as select * from '|| l_t_compare_tbl 
									|| ' where ' || p_filter_condition|| ' '  ;
									
							raise notice 'compare table copy created';
							RAISE NOTICE 'l_t_dataset_object_name - %',l_t_dataset_object_name;
							
							l_t_dynamic_query := 'select count(0) from  '||l_t_compare_tbl_copy ;
							execute l_t_dynamic_query into i;
							raise notice 'count in table % - %', l_t_compare_tbl_copy , i;
							
							-------------------------- Insert upstream data into the new promoted table ------------------------------
							l_t_dynamic_query := 'insert into '|| l_t_promoted_table ||'('|| l_t_promote_tbl_clmn ||') select '
							|| l_t_promote_tbl_clmn || ' from ' || l_t_upstream_tbl_nm_copy  ;
							EXECUTE l_t_dynamic_query ;
							
							get diagnostics AFFECTEDROWS = row_count;
							raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
							raise notice 'Insert upstream data into the new promoted table l_t_dynamic_query - %', l_t_dynamic_query ;
							raise notice 'l_t_dataset_object_name - %', l_t_dataset_object_name;
							
							------ Get Compare product column name
							l_t_dynamic_query := 'select tb_col_nm from t_ds_col_detail where dataset_id = ''' || p_t_compare_dataset_id ||''' and (lower(source_column) = ''product_master__c'' or lower(source_column) = ''axtriasalesiqtm__product_master__c'') ';
							raise notice 'l_t_dynamic_query -%', l_t_dynamic_query;
							EXECUTE l_t_dynamic_query into l_t_comp_prdct_clmn;
							raise notice 'l_t_comp_prdct_clmn-%',l_t_comp_prdct_clmn;
							
							------ Get upstream sfdc id column name 
							l_t_dynamic_query := 'select /*1*/tb_col_nm from t_ds_col_detail where dataset_id = ''' || l_t_upstream_dataset_id ||''' and lower(source_column) = ''id'' ';
							EXECUTE l_t_dynamic_query into l_t_upstrm_sfdc_id_clmn;
							--if l_t_upstrm_sfdc_id_clmn is null then
							--	l_t_upstrm_sfdc_id_clmn:= 'id';
							--end if;
							
							------ Get upstream flag column name 
							l_t_dynamic_query := 'select /*6*/tb_col_nm from t_ds_col_detail where dataset_id = ''' || l_t_upstream_dataset_id ||''' and (lower(source_column) = ''flag__c'' or lower(source_column) = ''axtriasalesiqtm__flag__c'')';
							EXECUTE l_t_dynamic_query into l_t_upstrm_flag_clmn;
							raise notice 'Get upstream flag column name - %', l_t_dynamic_query ;
							--if l_t_upstrm_flag_clmn is null then
							--	l_t_upstrm_flag_clmn:='flag';
							--end if;
							
							------ Get compare status column name 
							l_t_dynamic_query := 'select /*6*/tb_col_nm from t_ds_col_detail where dataset_id = ''' || p_t_compare_dataset_id ||''' and (lower(source_column) like ''%status__c%'' )';
							EXECUTE l_t_dynamic_query into l_t_comp_status_clmn;
							raise notice 'Get compare status column name - %', l_t_dynamic_query ;
							raise notice'l_t_comp_status_clmn - %', l_t_comp_status_clmn;
							
							----------------- Adding the Flag column
							IF(l_t_upstrm_flag_clmn is null) THEN
								l_t_upstrm_flag_clmn := 'flag';
								l_t_dynamic_query := 'alter table  '|| l_t_promoted_table || ' add column '|| l_t_upstrm_flag_clmn || ' text ';
								raise notice 'inside l_t_upstrm_flag_clmn if -%',l_t_dynamic_query;
								
								EXECUTE l_t_dynamic_query ;
								raise notice 'after l_t_upstrm_flag_clmn ';
							END IF;		
							
							--------------- Adding the ID column if null 
							IF(l_t_upstrm_sfdc_id_clmn is null) THEN
								l_t_upstrm_sfdc_id_clmn := 'id';
								l_t_dynamic_query := 'alter table  '|| l_t_promoted_table || ' add column '|| l_t_upstrm_sfdc_id_clmn || ' text ';
								raise notice 'inside l_t_upstrm_sfdc_id_clmn if -%',l_t_dynamic_query;
							
								EXECUTE l_t_dynamic_query ;
							
							END IF;
							
							
							--------------- Get Compare table names along with alias name
										select string_agg('comp.'||compare_col_name|| ' ', ',')
											from (
											select  tb_col_nm compare_col_name  
											from t_ds_col_detail compare where dataset_id =  p_t_compare_dataset_id  
											and source_column is not null and source_column not like '%.%'
											) cc
										into l_comp_table_col_list_1 ;
										raise notice 'l_comp_table_col_list_1 - %', l_comp_table_col_list_1;
								
							------ Get upstream product column name
							l_t_dynamic_query := 'select tb_col_nm from t_ds_col_detail where dataset_id = ''' || l_t_upstream_dataset_id ||''' and (lower(source_column) = ''product_master__c'' or lower(source_column) = ''axtriasalesiqtm__product_master__c'') ';
							raise notice 'Get upstream product column name - %',l_t_dynamic_query;
							EXECUTE l_t_dynamic_query into l_t_upstrm_prdct_clmn;
							raise notice 'l_t_upstrm_prdct_clmn-%',l_t_upstrm_prdct_clmn;
							
							/*------ Compare table query with active data
							--l_t_comp_actv_tb_query = 'select * from '|| l_t_compare_tbl || ' where '|| l_filter_condition ;
							l_t_dynamic_query:=' drop table if exists '|| l_t_compare_tbl_copy ;
							EXECUTE l_t_dynamic_query;
							l_t_dynamic_query:= ' create temp table '||l_t_compare_tbl_copy ||' as select * from '||l_t_compare_tbl 
												|| ' where '|| l_filter_condition ;
							raise notice 'compare table copy table creation - %',l_t_dynamic_query;
							EXECUTE l_t_dynamic_query;*/
							
							l_t_comp_actv_tb_query = 'select * from '|| l_t_compare_tbl_copy  ;	
							

							IF(l_b_is_delta = FALSE) THEN
							
								---- Get data for accounts
								EXECUTE 'drop table if exists comp_update_tbl';
								raise notice 'l_comp_table_col_list_1- %', l_comp_table_col_list_1;
								raise notice 'l_t_compare_tbl_copy - %', l_t_compare_tbl_copy;
								raise notice 'l_t_upstream_tbl_nm - %', l_t_upstream_tbl_nm;
								raise notice 'l_t_comp_acc_id_clmn -%', l_t_comp_acc_id_clmn;
								raise notice 'l_t_upstrm_acc_id_clmn-%', l_t_upstrm_acc_id_clmn;
								raise notice 'l_t_upstrm_prdct_clmn -%', l_t_upstrm_prdct_clmn;
								raise notice 'l_t_comp_prdct_clmn - %', l_t_comp_prdct_clmn ;
								--------------- Get Compare table names along with alias name
										select string_agg('comp.'||compare_col_name|| ' ', ',')
											from (
											select  tb_col_nm compare_col_name  
											from t_ds_col_detail compare where dataset_id =  p_t_compare_dataset_id  
											and source_column is not null and source_column not like '%.%'
											) cc
										into l_comp_table_col_list_1 ;
										raise notice 'l_comp_table_col_list - %', l_comp_table_col_list;
										
										select string_agg('comp.'||compare_col_name|| ' ', ',')
											from (
											select  tb_col_nm compare_col_name  
											from t_ds_col_detail compare where dataset_id =  p_t_compare_dataset_id  
											and source_column is not null and source_column not like '%.%'
											) cc
										into l_comp_table_col_list_1 ;
										raise notice 'l_comp_table_col_list - %', l_comp_table_col_list;
										-------------------------- Columns for promote table -- all columns from upstream table 
										SELECT string_agg('unstrm.' || upstream_col_name, ',') 
											From  (select *  FROM t_promote_scn_col_detls 
											 order by row_num) Cc
											INTO l_t_promote_tbl_clmn_1;
										raise notice 'l_t_promote_tbl_clmn_1- %',l_t_promote_tbl_clmn_1 ;
								
								l_t_dynamic_query := 'create temp table comp_update_tbl as select  '|| l_comp_table_col_list_1  || ' from '|| l_t_compare_tbl_copy ||' comp, ' 
								|| l_t_upstream_tbl_nm || ' unstrm  where comp.'|| l_t_comp_acc_id_clmn||' = unstrm.'||l_t_upstrm_acc_id_clmn || ' and coalesce(unstrm.'
								|| l_t_upstrm_prdct_clmn || ','''') = coalesce(comp.' ||l_t_comp_prdct_clmn || ','''')' ;

								raise notice 'Create table Get data for accounts -% ', l_t_dynamic_query;
								EXECUTE l_t_dynamic_query ;
								raise notice 'Table Created -1';
								
								Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_complete , 'Create table with common records - '||l_t_dynamic_query   ||'  ',
												1350 , l_t_fn_name) ;
							
							ELSIF(l_b_is_delta = TRUE) THEN
										
										l_t_dynamic_query := 'delete from '||l_t_compare_tbl_copy ||' comp where comp.'||l_t_comp_acc_id_clmn ||' not in (select '||l_t_cust_univ_acc_id_clmn ||' from '||l_t_cust_univ_tbl_nm ||'  ) ';
												raise notice 'l_t_dynamic_query - %', l_t_dynamic_query;
												EXECUTE l_t_dynamic_query ;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice 'Delete from compare copy where records not exist in custuniv -%', AFFECTEDROWS;
												
												execute ' Drop table if exists comp_update_tbl ';
												l_t_dynamic_query := ' create temp  table/*3*/ comp_update_tbl as select  ' 
												|| l_comp_table_col_list_1 ||' from '|| l_t_compare_tbl_copy ||' comp, ' 
												|| l_t_upstream_tbl_nm || ' unstrm, '|| l_t_cust_univ_tbl_nm || ' custuniv where comp.' 
												|| l_t_comp_acc_id_clmn||' = unstrm.'||l_t_upstrm_acc_id_clmn || ' and   coalesce(comp.'
												|| l_t_comp_prdct_clmn  || ','''') = coalesce(unstrm.' || l_t_upstrm_prdct_clmn || ','''') '
												||' and custuniv.'|| l_t_cust_univ_acc_id_clmn || ' = unstrm.' ||l_t_upstrm_acc_id_clmn  ;
												
												
												raise notice 'l_t_dynamic_query - %', l_t_dynamic_query;
												EXECUTE l_t_dynamic_query ;
												raise notice 'Get data for customer universe accounts -%',l_t_dynamic_query;
												
												l_t_dynamic_query := 'delete from '|| l_t_promoted_table ||' upstrm  where upstrm.'||l_t_upstrm_acc_id_clmn ||'  not in (select '||l_t_cust_univ_acc_id_clmn ||' from '||l_t_cust_univ_tbl_nm ||'  ) ';
												raise notice 'l_t_dynamic_query - %', l_t_dynamic_query;
												EXECUTE l_t_dynamic_query ;
												get diagnostics AFFECTEDROWS = row_count;
												raise notice 'Delete from Promote where records not exist in custuniv -%', AFFECTEDROWS;
												
												Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_complete , 'Create table with common records - '||l_t_dynamic_query   ||'  ',
												1381 , l_t_fn_name) ;
							
										---- Get data for customer universe accounts
										EXECUTE 'drop table if exists comp_update_tbl';
										
										
										raise notice 'l_t_comp_sfdc_id_clmn - %', l_t_comp_sfdc_id_clmn;
										raise notice 'l_t_comp_acc_id_clmn - %', l_t_comp_acc_id_clmn;
										raise notice 'l_t_comp_pos_id_clmn - %', l_t_comp_pos_id_clmn;
										raise notice 'l_t_comp_pos_id_clmn - %', l_t_comp_pos_id_clmn;
										raise notice 'l_t_comp_start_dt_clmn - %', l_t_comp_start_dt_clmn;
										raise notice 'l_t_comp_actv_tb_query - %', l_t_comp_actv_tb_query;
										raise notice 'l_t_upstream_tbl_nm - %', l_t_upstream_tbl_nm;
										raise notice 'l_t_cust_univ_tbl_nm - %', l_t_cust_univ_tbl_nm;
										raise notice 'l_t_comp_acc_id_clmn - %', l_t_comp_acc_id_clmn;
										raise notice 'l_t_upstrm_acc_id_clmn - %', l_t_upstrm_acc_id_clmn;
										raise notice 'l_t_upstrm_prdct_clmn - %', l_t_upstrm_prdct_clmn;
										raise notice 'l_t_comp_prdct_clmn - %', l_t_comp_prdct_clmn;
										raise notice 'l_t_cust_univ_acc_id_clmn - %', l_t_cust_univ_acc_id_clmn;
										
										l_t_dynamic_query := 'create temp table comp_update_tbl as select '|| l_comp_table_col_list_1  ||' from '|| l_t_compare_tbl_copy ||' comp, ' 
										|| l_t_upstream_tbl_nm || ' unstrm, '|| l_t_cust_univ_tbl_nm || ' custuniv where 
										comp.'|| l_t_comp_acc_id_clmn||' = unstrm.'||l_t_upstrm_acc_id_clmn || ' and coalesce(comp.'
										|| l_t_comp_prdct_clmn  || ','''') = coalesce(unstrm.' || l_t_upstrm_prdct_clmn || ','''') and 
										custuniv.'|| l_t_cust_univ_acc_id_clmn || ' = unstrm.' ||l_t_upstrm_acc_id_clmn ;
										
										raise notice 'Delta =true create table query - %', l_t_dynamic_query;
										
										EXECUTE l_t_dynamic_query ;
										raise notice 'Delta Table created -2 ';
										
							END IF;
							
							---- Get data which does not exist in upstream table
									
							/*l_t_dynamic_query := 'delete from comp_update_tbl comp using '||l_t_promoted_table ||' unstrm   where comp.'
							|| l_t_comp_acc_id_clmn||' = unstrm.'||l_t_upstrm_acc_id_clmn || ' and coalesce(comp.'
								|| l_t_comp_prdct_clmn  || ','''') = coalesce(unstrm.' ||l_t_upstrm_prdct_clmn || ','''')' ;
							
							raise notice 'Get data which does not exist in upstream table - l_t_dynamic_query -%', l_t_dynamic_query;
							EXECUTE l_t_dynamic_query ;
							get diagnostics AFFECTEDROWS = row_count;
							raise notice 'Get data which does not exist in upstream table -%', AFFECTEDROWS;*/

							---- Get data which does not exist in upstream table 1

							l_t_dynamic_query := 'delete from '|| l_t_promoted_table ||' unstrm using comp_update_tbl comp   where comp.'|| l_t_comp_acc_id_clmn||' = unstrm.'||l_t_upstrm_acc_id_clmn || ' and coalesce(comp.'
								|| l_t_comp_prdct_clmn || ','''') = coalesce(unstrm.' ||l_t_upstrm_prdct_clmn || ','''')' ;
							
							raise notice 'Get data which does not exist in upstream table 1 - l_t_dynamic_query -%', l_t_dynamic_query;
							EXECUTE l_t_dynamic_query ;
							get diagnostics AFFECTEDROWS = row_count;
							raise notice 'Get data which does not exist in upstream table 1 AFFECTEDROWS -%', AFFECTEDROWS;
							
							raise notice 'l_t_promoted_table - %', l_t_promoted_table;
							raise notice 'l_t_promote_tbl_clmn-%',l_t_promote_tbl_clmn;
							raise notice 'l_t_upstrm_flag_clmn-%',l_t_upstrm_flag_clmn;
							raise notice 'l_comp_table_col_list-%',l_comp_table_col_list;
							raise notice 'l_c_delete-%', l_c_delete;
							
						
							
							--l_t_dynamic_query := 'insert into '|| l_t_promoted_table || '(' || l_t_promote_tbl_clmn ||', '
							--|| l_t_upstrm_flag_clmn || ') select ' || l_comp_table_col_list ||','''|| l_c_delete ||''' from  comp_update_tbl' ;
							
							l_comp_table_col_list:= replace (l_comp_table_col_list , l_t_comp_status_clmn,'''Inactive''');
							raise notice 'l_comp_table_col_list - %', l_comp_table_col_list;
							raise notice 'l_t_promoted_table-%',l_t_promoted_table;
							raise notice 'l_t_upstrm_flag_clmn -%',l_t_upstrm_flag_clmn;
							raise notice 'l_comp_table_col_list-%',l_comp_table_col_list;
							raise notice 'l_c_update - %',l_c_update;
							
							/*l_t_dynamic_query := 'insert into '|| l_t_promoted_table || '(' || l_t_promote_tbl_clmn ||', '
							|| l_t_upstrm_flag_clmn || ') select ' || l_comp_table_col_list ||','''|| l_c_update ||''' from  comp_update_tbl' ;
							
							raise notice 'insert into promote table - l_t_dynamic_query -%', l_t_dynamic_query;
							EXECUTE l_t_dynamic_query ;
							get diagnostics AFFECTEDROWS = row_count;
							raise notice 'insert into promote table -%', AFFECTEDROWS;*/

							
							
							l_t_dynamic_query := 'Update '||l_t_promoted_table || ' set '||l_t_upstrm_flag_clmn|| ' = '''
							||l_c_insert || '''  where '||l_t_upstrm_flag_clmn|| ' is null '; 
							
							raise notice 'update promote table - l_t_dynamic_query -%', l_t_dynamic_query;
							EXECUTE l_t_dynamic_query ;
							get diagnostics AFFECTEDROWS = row_count;
							raise notice 'update promote table -%', AFFECTEDROWS;
							
							--- New Code for taking care of extra records in compare which are not there in the upstream table --set their date as D-1
								raise notice 'New Code for taking care of extra records in compare which are not there in the upstream table --set their date as D-1';
								raise notice 'l_comp_table_col_list_1 - %',l_comp_table_col_list_1;
								raise notice 'l_t_compare_tbl_copy-%',l_t_compare_tbl_copy;
								raise notice 'l_t_upstream_tbl_nm-%',l_t_upstream_tbl_nm;
								raise notice 'l_t_comp_acc_id_clmn-%',l_t_comp_acc_id_clmn;
								raise notice 'l_t_upstrm_acc_id_clmn-%',l_t_upstrm_acc_id_clmn;
								raise notice 'l_t_upstrm_prdct_clmn-%',l_t_upstrm_prdct_clmn;
								raise notice 'l_t_comp_prdct_clmn-%',l_t_comp_prdct_clmn;
							--	raise notice 'l_filter_condition-%',l_filter_condition;
								
								l_t_dynamic_query := ' create  temp  /*1*/table comp_update_tbl as select  ' || l_comp_table_col_list_1 ||' from '
																--|| l_t_compare_tbl ||' 	comp, ' 
																|| l_t_compare_tbl_copy ||' 	comp, ' 
																|| l_t_upstream_tbl_nm || ' unstrm  where comp.'|| l_t_comp_acc_id_clmn||' = unstrm.'||l_t_upstrm_acc_id_clmn || ' and coalesce(unstrm.'
																|| l_t_upstrm_prdct_clmn || ','''') = coalesce(comp.' ||l_t_comp_prdct_clmn || ','''')' ;
								
								raise notice 'Create table extra records in compare which are not there in the upstream table -%',l_t_dynamic_query;
								EXECUTE ' DROP TABLE IF EXISTS comp_update_tbl';
								get diagnostics AFFECTEDROWS = row_count;
								raise notice 'extra records in compare which are not there in the upstream table -%', AFFECTEDROWS;
								raise notice 'table dropped';
								
								execute l_t_dynamic_query ; 
								raise notice 'table created ';
								-- should be inactive for exclusion --update flag
								raise notice 'l_t_promoted_table-%',l_t_promoted_table;
								raise notice 'l_t_promote_tbl_clmn -%',l_t_promote_tbl_clmn;
								raise notice 'l_t_upstrm_sfdc_id_clmn-%',l_t_upstrm_sfdc_id_clmn;
								raise notice 'l_t_upstrm_flag_clmn-%',l_t_upstrm_flag_clmn;
								raise notice 'l_comp_table_col_list-%',l_comp_table_col_list;
								raise notice 'l_t_comp_sfdc_id_clmn-%',l_t_comp_sfdc_id_clmn;
								raise notice 'l_c_update-%',l_c_update;
								raise notice 'l_t_compare_tbl_copy-%',l_t_compare_tbl_copy;
								raise notice 'l_t_comp_sfdc_id_clmn-%',l_t_comp_sfdc_id_clmn;
								--raise notice 'comp_update_tbl-%',comp_update_tbl;
								
								l_t_dynamic_query := ' insert into '||l_t_promoted_table ||' ( '||l_t_promote_tbl_clmn || ' , ' || l_t_upstrm_sfdc_id_clmn
													|| ' , ' || l_t_upstrm_flag_clmn
													|| ' ) select '|| l_comp_table_col_list || ' , '|| l_t_comp_sfdc_id_clmn ||' , ' 
													|| ''''|| l_c_update ||''' from '
													|| l_t_compare_tbl_copy || '  tt ' 
													|| ' where tt.' ||l_t_comp_sfdc_id_clmn ||   ' not in (select  '|| l_t_comp_sfdc_id_clmn  || ' from comp_update_tbl ) ';
													
								raise notice 'l_t_dynamic_query->%',l_t_dynamic_query;
								raise notice 'insert query - %', l_t_dynamic_query;
								execute l_t_dynamic_query;
								get diagnostics AFFECTEDROWS = row_count;
								raise notice 'extra records in compare which are not there in the upstream table -%', AFFECTEDROWS;
						
						
						----------- New Update  - Python needs Id column for Update,Delete flags
								raise notice 'l_t_promoted_table - %', l_t_promoted_table;
								raise notice 'l_upstrm_id_col - %', l_upstrm_id_col;
								raise notice 'l_t_compare_tbl_copy - %', l_t_compare_tbl_copy;
								raise notice 'l_t_upstrm_acc_id_clmn - %', l_t_upstrm_acc_id_clmn;
								--raise notice 'l_t_upstrm_pos_id_clmn - %', l_t_upstrm_pos_id_clmn;
								--raise notice 'l_t_comp_pos_id_clmn - %', l_t_comp_pos_id_clmn;
								raise notice 'l_t_comp_acc_id_clmn - %', l_t_comp_acc_id_clmn;
								
								l_t_dynamic_query :=  ' Update ' || l_t_promoted_table || ' prom set '||l_t_upstrm_sfdc_id_clmn 
													||' =  comp.' || l_upstrm_id_col 
													|| ' from '||l_t_compare_tbl_copy || ' comp where prom.'
													||  l_t_upstrm_acc_id_clmn ||' = comp.'||l_t_comp_acc_id_clmn 
													|| ' and 	coalesce(prom.' ||l_t_upstrm_prdct_clmn || ','''') =  coalesce(comp.'
													|| l_t_comp_prdct_clmn  || ','''') ' ;									
													 
								
								raise notice 'New Update  - Python needs Id column for Update,Delete flags -%',l_t_dynamic_query; 
								EXECUTE l_t_dynamic_query ;
								get diagnostics AFFECTEDROWS = row_count;
								raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
								
								
								----------- New Update  - Python needs Id column for Update,Delete flags
								raise notice 'l_t_promoted_table - %', l_t_promoted_table;
								raise notice 'l_upstrm_id_col - %', l_upstrm_id_col;
								raise notice 'l_t_upstrm_sfdc_id_clmn -> %',l_t_upstrm_sfdc_id_clmn;
								raise notice 'l_t_compare_tbl_copy - %', l_t_compare_tbl_copy;
								raise notice 'l_t_upstrm_acc_id_clmn - %', l_t_upstrm_acc_id_clmn;
								--raise notice 'l_t_upstrm_pos_id_clmn - %', l_t_upstrm_pos_id_clmn;
								--raise notice 'l_t_comp_pos_id_clmn - %', l_t_comp_pos_id_clmn;
								raise notice 'l_t_comp_acc_id_clmn - %', l_t_comp_acc_id_clmn;
								raise notice 'l_t_upstrm_prdct_clmn-> %',l_t_upstrm_prdct_clmn;
								raise notice 'l_t_comp_prdct_clmn ->%',l_t_comp_prdct_clmn;
								
								l_t_dynamic_query :=  ' Update ' || l_t_promoted_table || ' prom set '||l_t_upstrm_sfdc_id_clmn 
													||' =  comp.' || l_upstrm_id_col 
													|| ' from '||l_t_compare_tbl_copy || ' comp where prom.'
													||  l_t_upstrm_acc_id_clmn ||' = comp.'||l_t_comp_acc_id_clmn 
													|| ' and 	coalesce(prom.' ||l_t_upstrm_prdct_clmn || ','''') =  coalesce(comp.'
													|| l_t_comp_prdct_clmn  || ','''') ' ;									
													 
								
								raise notice 'New Update  - Python needs Id column for Update,Delete flags -%',l_t_dynamic_query; 
								EXECUTE l_t_dynamic_query ;
								get diagnostics AFFECTEDROWS = row_count;
								raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
				
								Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Update Id for Delete Update - '||l_t_dynamic_query   ||'  ',
												1571 , l_t_fn_name) ;
				
				END IF ; 
				-- IF 1
				-- IF 1
		-- De-Dupe data in Promote TABLE
		l_t_dynamic_query :=  ' Select string_agg(distinct column_name ,'' , '' order by column_name )   from information_schema.columns where lower(table_name)= lower('''||l_t_promoted_table||''') ';
		raise notice 'l_t_dynamic_query - > %',l_t_dynamic_query;
		Execute l_t_dynamic_query into l_t_promote_columns ;
		raise notice 'l_t_promote_columns -> %',l_t_promote_columns;
		
		Execute ' Drop table if exists '||l_t_promoted_table||'_1';
		l_t_dynamic_query := 'Create table '||l_t_promoted_table||'_1 as select distinct  '||l_t_promote_columns ||' from '|| l_t_promoted_table ;
		raise notice 'l_t_dynamic_query-> %',l_t_dynamic_query;
		Execute l_t_dynamic_query ;
		
		
		Execute 'Drop table '||l_t_promoted_table ;
		Execute ' alter table '||l_t_promoted_table||'_1 rename to '||l_t_promoted_table ;
		
		
		-- Promote flag if condition end
		END IF ;
		
		

return 1; 
		/* Error handling */

exception 
    when others then
        GET STACKED DIAGNOSTICS l_t_error = PG_EXCEPTION_CONTEXT;
		INSERT INTO t_error_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
        	 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,'','',l_t_scenario_id,l_t_error,l_t_dynamic_query,'','');
        INSERT INTO t_error_run_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
        	 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,'','',l_t_scenario_id,l_t_error,l_t_dynamic_query,'','');
        
		Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
						l_log_type_error  , l_log_level_basic , l_run_status_error, 'Error Promote failed - '||SQLERRM || ' - '||SQLSTATE || ' Query -> '||l_t_dynamic_query   ||'  ',
						1629 , l_t_fn_name) ;
		
		return -2; 
		/* should always return -2 in case of an exception so that promote exceptions can be distinguished from scenario exceptions */
end;

$BODY$;

ALTER FUNCTION public.fn_promote_scenario(text, boolean, boolean, text, text, text, text, text, text)
    OWNER TO postgres;
