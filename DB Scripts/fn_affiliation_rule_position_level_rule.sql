-- FUNCTION: public.fn_affiliation_rule_position_level_rule(text, text, text, text)

-- DROP FUNCTION public.fn_affiliation_rule_position_level_rule(text, text, text, text);

CREATE OR REPLACE FUNCTION public.fn_affiliation_rule_position_level_rule(
	p_t_scn_rule_int_id text,
	p_br_join_rule_id text,
	p_t_output_table text,
	p_selected_positions text)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

l_t_fn_name text := 'fn_affiliation_rule';
l_t_scn_rule_int_id text := p_t_scn_rule_int_id;
l_br_join_rule_id text := p_br_join_rule_id;
l_t_scn_rule_instance RECORD;
l_t_join_sel_expr text;
l_t_join_expr text;
l_t_error text;
l_t_sql_query text := '';
l_t_output_table text := p_t_output_table||'_'||p_br_join_rule_id;
l_t_tmp_output_table text := p_t_output_table || '_1'; --- for hold intermediate affiliation based alignment
l_t_tmp_output_table1 text ; -- temp table for handle duplicate records
l_t_tmp_affiliation_table text ; -- for eligible affiliation based of business rule
l_t_tmp_cust_mas_terr_table0 text; -- for temp customer master with territory
l_t_tmp_cust_mas_terr_table text; -- for customer master with territory
l_t_tmp_block_cust_table text ; -- for blocked customer id
l_t_affiliation_table text;
l_t_affiliation_ds_id text;
l_t_cust_universe_table text;
l_t_cust_universe_ds_id text;
l_t_zip_terr_table text; 
l_t_zip_terr_ds_id text;
l_t_cust_terr_table text;
l_t_cust_terr_ds_id text;
l_t_cust_prod_block_table text;
l_t_cust_prod_block_ds_id text;
l_t_team_prod_table text;
l_t_team_prod_ds_id text;

-- ----------------Column Identifier for processing ----
l_t_child_col_aff text; -- --- Child in Affiliation table
l_t_parent_col_aff text;-- --- Parent in Affiliation table  
l_t_cust_id_col_univ text; -- Cust id Column in Universe 
l_t_zip_col_univ text; -- Zip Column in Universe 
l_t_cust_id_col_ct text; -- -- Cust id in cust-terr
l_t_position_name_ct text; --  position name in cust-terr
l_t_position_ct text; -- - Position/territory in cust-terr
l_t_zip_col_zt text; -- Zip Column in zip terr
l_t_position_zt text; -- Position Column in zip terr
l_t_prd_id_tp text; --  product id from team product
l_t_prd_id_cust_prd_blk text; --  product id from cust product block
l_t_cust_id_cust_prd_blk text; --  cust id from cust product block

l_t_insert_exec_expr text;
l_t_business_rules text;
l_br_table RECORD;
l_r_comp_label RECORD;  
flag integer;
l_n_count integer;
l_t_where_expr text;
l_t_br_stage text;
l_t_rule_type text;
l_t_geo_preference text;
l_t_hierarchy_level text;
l_i_hierarchy_level integer; 
l_c_affli_Parent text := 'parent' ;
l_c_affli_Child text := 'child' ;
l_t_parent_exp text;
l_t_child_exp text;
l_t_parent_table_str text := 'l_t_parent_table' || l_t_scn_rule_int_id;
l_t_child_table_str text := 'l_t_child_table'|| l_t_scn_rule_int_id;
l_c_geo_based_ref text := 'align in geo';
l_c_univ_ref text := 'align in universe';
l_t_elig_affli_query text;
l_t_tmp_query text; -- for execute small temp query
l_t_delete_query text;
l_t_block_query text;
l_t_insert_query text;
l_c_rule_botm_up text := 'bottom up';
l_c_rule_top_down text := 'top down';
l_t_cust_terr_colms text;
l_t_cust_terr_colms_for_rule text ;
l_t_acct_align_type_ct text ;
l_t_aff_based_align_ct text ;
l_i_log_flag integer;
l_t_isshared_flag_ct text ;
l_t_sharedwith_ct text ;
l_t_cust_terr_colms_final text ;
--- Variable for multi level affiliation
l_t_updt_orgin_brnch_aff_qry text;
l_t_insrt_orgn_data_qry text;
l_t_cust_terr_colms_for_origin text ;
l_i_counter integer:= 0; 

l_n_prnt_table_count integer;
l_n_chld_table_count integer;
l_tmp_query text;
l_query_count integer ;
l_n_prdt_count integer;
l_t_select_position  text :=p_selected_positions;
AFFECTEDROWS  integer;
l_br_rule_name  text;

--7.0 Position Product Blocking
t_prod_pos_blckng_tbl text :='t_position_product_block';
l_t_pos_id_cust_prd_blk text;
l_t_prod_id_blck text;
l_t_acc_id_blck text;
--l_t_team_prod_blck_ds_id text;
--l_t_team_prod_blck text;
--7.0 Position Product Blocking
begin
	l_i_log_flag :=0;
	raise notice 'Position level start';
	
	DROP TABLE IF EXISTS temp_position_list ;
	CREATE TEMP TABLE temp_position_list (position_id text);
	INSERT INTO temp_position_list SELECT regexp_split_to_table( l_t_select_position , E',') as position_id ;
	UPDATE temp_position_list SET position_id =  trim(from position_id);
	get diagnostics AFFECTEDROWS = row_count;
	raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;

	
	--l_t_select_position :=   chr(39)||l_t_select_position||chr(39) ;
	l_t_select_position := 'select position_id from temp_position_list ';
				
	raise notice 'l_t_select_position - %', l_t_select_position; 
    IF(l_i_log_flag = 1) THEN
        INSERT INTO t_log(scenario_id, log, line_no, function_name)
        VALUES (l_t_scn_rule_int_id, 'Affiliation Rule processing start', '96', l_t_fn_name); 
    END IF;
    
    SELECT 'Affiliation'::text as name , name as rule_name
    	FROM t_business_rule br
    	WHERE br.sfdc_id = l_br_join_rule_id
    	INTO l_t_br_stage , l_br_rule_name ;
     raise notice 'l_t_br_stage -> %',l_t_br_stage;        
     ------------------ Identify Input tables on basis of scenario -----------------
     
     -- Pick Dataset id and table name on basis of file type
          
    SELECT DISTINCT drm.dataset_id,drm.table_name
        FROM t_dataset ds, t_dataset_rule_map drm --,t_scn_rule_instance_details srd
        WHERE ds.sfdc_id = drm.dataset_id 
        --AND drm.scenario_rule_instance_id = srd.sfdc_id
        --AND srd.scenario_id = l_t_scn_rule_int_id
        AND drm.scenario_rule_instance_id = l_t_scn_rule_int_id
        AND LOWER(drm.file_type) = 'base'
        INTO l_t_cust_universe_ds_id,l_t_cust_universe_table;
   --  return 5;
    IF(l_i_log_flag = 1) THEN
        INSERT INTO t_log(scenario_id, log, line_no, function_name)
        VALUES (l_t_scn_rule_int_id, 'Customer universe dataset id: '|| l_t_cust_universe_ds_id ||' and Customer universe Table Name: '|| l_t_cust_universe_table, '125', l_t_fn_name); 
    END IF;
       
    SELECT DISTINCT drm.dataset_id,drm.table_name
        FROM t_dataset ds, t_dataset_rule_map drm --,t_scn_rule_instance_details srd
        WHERE ds.sfdc_id = drm.dataset_id 
        --AND drm.scenario_rule_instance_id = srd.sfdc_id
        --AND srd.scenario_id = l_t_scn_rule_int_id
        AND drm.scenario_rule_instance_id = l_t_scn_rule_int_id
        AND LOWER(drm.file_type) = 'zip terr'
        INTO l_t_zip_terr_ds_id,l_t_zip_terr_table;

    IF(l_i_log_flag = 1) THEN
        INSERT INTO t_log(scenario_id, log, line_no, function_name)
        VALUES (l_t_scn_rule_int_id, 'ZIP TERR dataset id: '|| l_t_zip_terr_ds_id ||' and ZIP TERR Table Name: '|| l_t_zip_terr_table, '139', l_t_fn_name); 
    END IF;

    SELECT DISTINCT drm.dataset_id,drm.table_name
        FROM t_dataset ds, t_dataset_rule_map drm --,t_scn_rule_instance_details srd
        WHERE ds.sfdc_id = drm.dataset_id 
        --AND drm.scenario_rule_instance_id = srd.sfdc_id
        --AND srd.scenario_id = l_t_scn_rule_int_id
        AND drm.scenario_rule_instance_id = l_t_scn_rule_int_id
        AND LOWER(drm.file_type) = 'cust terr'
        INTO l_t_cust_terr_ds_id,l_t_cust_terr_table;
	
    IF(l_i_log_flag = 1) THEN
        INSERT INTO t_log(scenario_id, log, line_no, function_name)
        VALUES (l_t_scn_rule_int_id, 'CUST TERR dataset id: '|| l_t_cust_terr_ds_id ||' and CUST TERR Table Name: '|| l_t_cust_terr_table, '153', l_t_fn_name); 
    END IF;

    SELECT DISTINCT drm.dataset_id,drm.table_name
        FROM t_dataset ds, t_dataset_rule_map drm --,t_scn_rule_instance_details srd
        WHERE ds.sfdc_id = drm.dataset_id 
        --AND drm.scenario_rule_instance_id = srd.sfdc_id
        --AND srd.scenario_id = l_t_scn_rule_int_id
        AND drm.scenario_rule_instance_id = l_t_scn_rule_int_id
        AND LOWER(drm.file_type) = 'affiliation'
        INTO l_t_affiliation_ds_id,l_t_affiliation_table;
	
    IF(l_i_log_flag = 1) THEN
        INSERT INTO t_log(scenario_id, log, line_no, function_name)
        VALUES (l_t_scn_rule_int_id, 'Affiliation master dataset id: '|| l_t_affiliation_ds_id ||' and Affiliation master Table Name: '|| l_t_affiliation_table, '167', l_t_fn_name); 
    END IF;
 

    SELECT DISTINCT drm.dataset_id,drm.table_name
        FROM t_dataset ds, t_dataset_rule_map drm --,t_scn_rule_instance_details srd
        WHERE ds.sfdc_id = drm.dataset_id 
        --AND drm.scenario_rule_instance_id = srd.sfdc_id
        --AND srd.scenario_id = l_t_scn_rule_int_id
        AND drm.scenario_rule_instance_id = l_t_scn_rule_int_id
        AND LOWER(drm.file_type) = 'product'
        INTO l_t_team_prod_ds_id,l_t_team_prod_table;
		
    IF(l_i_log_flag = 1) THEN
        INSERT INTO t_log(scenario_id, log, line_no, function_name)
        VALUES (l_t_scn_rule_int_id, 'Team Prod dataset id: '|| l_t_affiliation_ds_id ||' and Team Prod Table Name: '|| l_t_affiliation_table, '167', l_t_fn_name); 
    END IF;
	
    SELECT DISTINCT drm.dataset_id,drm.table_name
        FROM t_dataset ds, t_dataset_rule_map drm --,t_scn_rule_instance_details srd
        WHERE ds.sfdc_id = drm.dataset_id 
        --AND drm.scenario_rule_instance_id = srd.sfdc_id
        --AND srd.scenario_id = l_t_scn_rule_int_id
        AND drm.scenario_rule_instance_id = l_t_scn_rule_int_id
        AND LOWER(drm.file_type) = 'block'
        INTO l_t_cust_prod_block_ds_id,l_t_cust_prod_block_table;

    IF(l_i_log_flag = 1) THEN
        INSERT INTO t_log(scenario_id, log, line_no, function_name)
        VALUES (l_t_scn_rule_int_id, 'Cust_Prod Block dataset id: '|| l_t_affiliation_ds_id ||' and Cust_Prod Block Table Name: '|| l_t_affiliation_table, '167', l_t_fn_name); 
    END IF;
	 
    ----------------- Identify Column of input tables for processing
    -- *************Pick column name from dataset on basis of field attribute  *********** --
    
    SELECT tb_col_nm FROM t_ds_col_detail WHERE dataset_id = l_t_affiliation_ds_id AND (LOWER(source_column) = 'axtriasalesiqtm__account__c' or LOWER(source_column) = 'account__c') INTO l_t_child_col_aff;
    SELECT tb_col_nm FROM t_ds_col_detail WHERE dataset_id = l_t_affiliation_ds_id AND (LOWER(source_column) = 'axtriasalesiqtm__parent_account__c' or LOWER(source_column) = 'parent_account__c') INTO l_t_parent_col_aff;
	
    IF(l_i_log_flag = 1) THEN
        INSERT INTO t_log(scenario_id, log, line_no, function_name)
        VALUES (l_t_scn_rule_int_id, 'Parent account column name in Affiliation Table : '|| l_t_parent_col_aff ||' and Child account column name in Affiliation Table: '|| l_t_child_col_aff, '189', l_t_fn_name); 
    END IF;
    
    SELECT tb_col_nm from t_ds_col_detail where dataset_id = l_t_cust_universe_ds_id and (lower(source_column) = 'axtriasalesiqtm__id' or LOWER(source_column) = 'id') into l_t_cust_id_col_univ;
    SELECT tb_col_nm from t_ds_col_detail where dataset_id = l_t_cust_universe_ds_id and (lower(source_column) = 'axtriasalesiqtm__billingpostalcode' or LOWER(source_column) = 'billingpostalcode') into l_t_zip_col_univ;

	IF(l_i_log_flag = 1) THEN
        INSERT INTO t_log(scenario_id, log, line_no, function_name)
        VALUES (l_t_scn_rule_int_id, 'Custmer id column name in Customer universe Table : '|| l_t_cust_id_col_univ ||' and ZIP column name in Customer universe Table: '|| l_t_zip_col_univ, '197', l_t_fn_name); 
    END IF;

    select tb_col_nm from t_ds_col_detail where dataset_id = l_t_zip_terr_ds_id and (lower(source_column) = 'axtriasalesiqtm__geography__r.name' or lower(source_column) = 'geography__r.name') into l_t_zip_col_zt;
    select tb_col_nm from t_ds_col_detail where dataset_id = l_t_zip_terr_ds_id and (lower(source_column) = 'axtriasalesiqtm__position__c' or lower(source_column) = 'position__c') into l_t_position_zt;
    
	IF(l_i_log_flag = 1) THEN
        INSERT INTO t_log(scenario_id, log, line_no, function_name)
        VALUES (l_t_scn_rule_int_id, 'Position column name in ZIP TERR Table : '|| l_t_position_zt ||' and ZIP column name in ZIP TERR Table: '|| l_t_zip_col_zt, '205', l_t_fn_name); 
    END IF;

    select tb_col_nm from t_ds_col_detail where dataset_id = l_t_cust_terr_ds_id and (lower(source_column) = 'axtriasalesiqtm__account__c' or lower(source_column) = 'account__c') into l_t_cust_id_col_ct;
    select tb_col_nm from t_ds_col_detail where dataset_id = l_t_cust_terr_ds_id and (lower(source_column) = 'axtriasalesiqtm__position__c' or lower(source_column) = 'position__c') into l_t_position_ct;	 
    select tb_col_nm from t_ds_col_detail where dataset_id = l_t_cust_terr_ds_id and (lower(source_column) = 'axtriasalesiqtm__position__r.name' or lower(source_column) = 'position__r.name') into l_t_position_name_ct;
    select tb_col_nm from t_ds_col_detail where dataset_id = l_t_cust_terr_ds_id and (lower(source_column) = 'axtriasalesiqtm__account_alignment_type__c' or lower(source_column) = 'account_alignment_type__c') into l_t_acct_align_type_ct; -- Explicit
    select tb_col_nm from t_ds_col_detail where dataset_id = l_t_cust_terr_ds_id and (lower(source_column) = 'axtriasalesiqtm__affiliation_based_alignment__c' or lower(source_column) = 'affiliation_based_alignment__c') into l_t_aff_based_align_ct; -- YES
	select tb_col_nm from t_ds_col_detail where dataset_id = l_t_cust_terr_ds_id and (lower(source_column) = 'axtriasalesiqtm__isshared__c' or lower(source_column) = 'isshared__c') into l_t_isshared_flag_ct; -- Yes/No
    select tb_col_nm from t_ds_col_detail where dataset_id = l_t_cust_terr_ds_id and (lower(source_column) = 'axtriasalesiqtm__sharedwith__c' or lower(source_column) = 'sharedwith__c') into l_t_sharedwith_ct; -- T1,T2
	
    IF(l_i_log_flag = 1) THEN
        INSERT INTO t_log(scenario_id, log, line_no, function_name)
        VALUES (l_t_scn_rule_int_id, 'CUST ID column name in CUST TERR Table : '|| l_t_cust_id_col_ct ||' and Position column name in CUST TERR Table: '|| l_t_position_ct ||' and acct_align_type column name in CUST TERR Table: '|| l_t_acct_align_type_ct ||' and affi_based_align column name in CUST TERR Table: '|| l_t_aff_based_align_ct, '215', l_t_fn_name); 
    END IF; 
	
	select tb_col_nm from t_ds_col_detail where dataset_id = l_t_team_prod_ds_id and (lower(source_column) = 'axtriasalesiqtm__product_master__c' or LOWER(source_column) = 'product_master__c') into l_t_prd_id_tp;
	
	select tb_col_nm from t_ds_col_detail where dataset_id = l_t_cust_prod_block_ds_id and (lower(source_column) = 'axtriasalesiqtm__product_master__c' or LOWER(source_column) = 'product_master__c') into l_t_prd_id_cust_prd_blk;
	select tb_col_nm from t_ds_col_detail where dataset_id = l_t_cust_prod_block_ds_id and (lower(source_column) = 'axtriasalesiqtm__account__c' or LOWER(source_column) = 'account__c') into l_t_cust_id_cust_prd_blk;
	--End Changed on 06-Dec-18 for SFDC source column standardisation
	
	select tb_col_nm from t_ds_col_detail where dataset_id = l_t_team_prod_ds_id and (lower(source_column) = 'axtriasalesiqtm__position__c' or LOWER(source_column) = 'position__c') into l_t_pos_id_cust_prd_blk;
	
	raise notice 'l_t_prd_id_cust_prd_blk-> %, l_t_pos_id_cust_prd_blk -> %', l_t_prd_id_cust_prd_blk, l_t_pos_id_cust_prd_blk ;
	
	select tb_col_nm from t_ds_col_detail where dataset_id = l_t_cust_prod_block_ds_id and (lower(source_column) = 'axtriasalesiqtm__account__c' or LOWER(source_column) = 'account__c') into  l_t_acc_id_blck;
		raise notice 'l_t_prod_id_blck - > %, l_t_acc_id_blck -> % ', l_t_prod_id_blck , l_t_acc_id_blck ;
    /****************************
	temp table creation for processing
	********************************/
	
	
	IF(l_i_log_flag = 0) THEN 
	RAISE NOTICE 'Temp table start @@@Time ID: %', clock_timestamp();
	END IF;
    -- Creating temp table of customer territory table schema
    EXECUTE 'drop table if exists ' ||	 l_t_tmp_output_table || '' ;
    EXECUTE 'create table ' || l_t_tmp_output_table || ' as select * from ' || l_t_cust_terr_table || ' limit 0 ';
    EXECUTE 'alter table ' || l_t_tmp_output_table || ' add column h_level integer ' ;
  
  	-- Creating temp table of affiliation table schema
    l_t_tmp_affiliation_table := l_t_affiliation_table || '_1';
    EXECUTE 'drop table if exists ' ||	 l_t_tmp_affiliation_table || '' ;
    EXECUTE 'create table ' || l_t_tmp_affiliation_table || ' as select * from ' || l_t_affiliation_table || ' limit 0 ';
	EXECUTE 'alter table ' || l_t_tmp_affiliation_table || ' add column child_terr text,add column parent_terr text, add column is_origin text , add column is_branch text ' ; 
	
	
	-- Creating temp table of Blocked customer
	l_t_tmp_block_cust_table := l_t_cust_prod_block_table || '_1';
    EXECUTE 'drop table if exists ' ||	 l_t_tmp_block_cust_table || '' ;
	l_t_tmp_query := 'create table ' || l_t_tmp_block_cust_table || ' as select '|| l_t_cust_id_col_ct 
						||' from ' || l_t_cust_terr_table || ' limit 0 ';
	
	
	Execute l_t_tmp_query;
	
	IF(l_i_log_flag = 0) THEN 
	raise notice  'l_t_tmp_query for create  temp table of block customer: % ',	l_t_tmp_query ; 
    RAISE NOTICE '>>>>>>>>>>>>>>>>>>>>customer master with territory start @@@Time ID: %', clock_timestamp();
	END IF;
	
    -- creating table for customer master with territory
    l_t_tmp_cust_mas_terr_table0 := l_t_cust_universe_table || '_temp1' ;
    execute 'drop table if exists ' ||  l_t_tmp_cust_mas_terr_table0 || '' ;
    execute 'create table ' || l_t_tmp_cust_mas_terr_table0 || ' as select c.* from '||  l_t_cust_universe_table || ' c limit 0 ';
	execute 'alter table ' || l_t_tmp_cust_mas_terr_table0 || ' add column terr text ' ;
	
	l_tmp_query := 'insert into ' || l_t_tmp_cust_mas_terr_table0 || ' select c.*, z.'|| l_t_position_zt 
			||' as terr from ' ||  l_t_cust_universe_table || ' c left join ' ||  l_t_zip_terr_table 
			||' z on c.'||l_t_zip_col_univ || '= z.' ||l_t_zip_col_zt ||' ';
			
	raise notice 'l_tmp_query - > %',l_tmp_query;
    execute l_tmp_query ;
	get diagnostics AFFECTEDROWS = row_count;
	raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
	
	IF(l_i_log_flag = 0) THEN
	RAISE NOTICE '>>>>>------ Query for l_t_tmp_cust_mas_terr_table0 : %', l_tmp_query ;
	RAISE NOTICE '>>>>>>>>>>>>>>>>>>>>customer master with territory inserted @@@Time ID: %', clock_timestamp();	
	END IF;
    
	l_t_tmp_cust_mas_terr_table := l_t_cust_universe_table || '_temp' ;
    execute 'drop table if exists ' ||  l_t_tmp_cust_mas_terr_table || '' ;
    l_tmp_query :=  'create table ' || l_t_tmp_cust_mas_terr_table || ' as select * from '|| 
			l_t_tmp_cust_mas_terr_table0 || ' limit 0 ';

	
    execute l_tmp_query ;
    l_tmp_query := ' insert into '|| l_t_tmp_cust_mas_terr_table || 
     ' select * from '||  l_t_tmp_cust_mas_terr_table0  || ' where ' ||l_t_cust_id_col_univ ||
     ' in  (      select ' ||l_t_child_col_aff || ' from '||  l_t_affiliation_table  || 
	 ' union all   select '|| l_t_parent_col_aff || ' from '||  l_t_affiliation_table  || ')';
	
	raise notice 'l_tmp_query - > %',l_tmp_query;
	execute l_tmp_query ;
	get diagnostics AFFECTEDROWS = row_count;
	raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
	
	IF(l_i_log_flag = 0) THEN
    RAISE NOTICE '>>>>>>>>>------------------ Query for l_t_tmp_cust_mas_terr_table : %', l_tmp_query ;
	l_tmp_query := 'select count (*) from ' || l_t_tmp_cust_mas_terr_table ;
    execute l_tmp_query into  l_query_count;
    RAISE NOTICE '>>>>>>>>>>>>>>>>>>>>count of customer master with territory end : %', l_query_count ;
	RAISE NOTICE 'Temp table end @@@Time ID: %', clock_timestamp();
	END IF;
	
	----------------- Implement Business Rule for Eligible Affiliation record

    -- Get business rule details
	Select sfdc_id,rule_type,geography_preference,COALESCE( NULLIF(hierarchy_level,'') , '1' ) as hierarchy_level
	FROM t_business_rule br WHERE br.sfdc_id = l_br_join_rule_id  
     into l_t_business_rules,l_t_rule_type,l_t_geo_preference,l_t_hierarchy_level;

	raise notice 'l_t_business_rules -> %l_t_rule_type - > % , l_t_geo_preference - > %, l_t_hierarchy_level -> %', l_t_business_rules,l_t_rule_type,l_t_geo_preference,l_t_hierarchy_level;
	
	IF(l_i_log_flag = 0) THEN
	 raise notice  'l_t_hierarchy_level for  loop : % ',	l_t_hierarchy_level ;
	 RAISE NOTICE 'Loop BR Joinrule start @@@Time ID: %', clock_timestamp();
    END IF;
    IF(l_i_log_flag = 1) THEN
        INSERT INTO t_log(scenario_id, log, line_no, function_name)
        VALUES (l_t_scn_rule_int_id, 'Business rule sfdc Id : '|| l_t_business_rules ||' and rule type : '|| l_t_rule_type ||' and Geo reference: '|| l_t_geo_preference ||' for scenario instance id: '|| l_t_scn_rule_int_id, '231', l_t_fn_name); 
    END IF;
    
	 -- Loop br_joinrule for above business rule
	
	IF(l_i_log_flag = 1) THEN
        INSERT INTO t_log(scenario_id, log, line_no, function_name)
        VALUES (l_t_scn_rule_int_id, 'LOOP start for Business rule sfdc Id : '|| l_t_business_rules ||' in table t_br_joinrule' , '238', l_t_fn_name); 
    END IF;
    
	execute 'drop table if exists ' || l_t_parent_table_str || '';
	execute 'create table ' || l_t_parent_table_str || ' as  select * from '|| l_t_tmp_cust_mas_terr_table ||' limit 0 ';
	execute 'drop table if exists ' || l_t_child_table_str || '';
	execute 'create table ' || l_t_child_table_str || ' as  select * from '|| l_t_tmp_cust_mas_terr_table ||' limit 0 ';
	raise notice 'l_t_business_rules -> %',l_t_business_rules;
	
	
    FOR l_br_table IN 
        SELECT distinct COALESCE(execution_sequence,'0') as execution_sequence FROM t_br_joinrule 
        WHERE business_rules = l_t_business_rules

    LOOP
	
	IF(l_i_log_flag = 0) THEN
	raise notice  'l_br_table.execution_sequence: % ',	l_br_table.execution_sequence;          	
	END IF;
        
		-- Get expresssion for parent customer
        select where_expression from t_br_joinrule where business_rules = l_t_business_rules and  execution_sequence = l_br_table.execution_sequence 
        and lower(affiliation_hierarchy) = l_c_affli_Parent into l_t_parent_exp ;
		
		raise notice 'l_t_parent_exp -> % , l_t_business_rules -> %', l_t_parent_exp ,l_t_business_rules ; 
		
        -- Get expresssion for child customer
        select where_expression from t_br_joinrule where business_rules = l_t_business_rules and execution_sequence = l_br_table.execution_sequence 
        and lower(affiliation_hierarchy) = l_c_affli_child into l_t_child_exp ;
		raise notice 'l_t_child_exp -> % , l_t_business_rules -> %', l_t_child_exp ,l_t_business_rules ; 
		
        IF(l_i_log_flag = 1) THEN
            INSERT INTO t_log(scenario_id, log, line_no, function_name)
            VALUES (l_t_scn_rule_int_id, 'Expression for parent accounts : '|| l_t_parent_exp ||' and Expression for child accounts :'|| l_t_child_exp || 'for execution sequence: '|| l_br_table.execution_sequence , '257', l_t_fn_name); 
        END IF;
        
---------- Applying parent expression on customer master 
        if l_t_parent_exp is null then
        l_t_parent_exp := ' 1 = 1 ';
        end if;
		
		l_tmp_query :=  'insert into  ' || l_t_parent_table_str || '  select * from '|| l_t_tmp_cust_mas_terr_table ||' where '
							|| l_t_parent_exp || ' ';-- and  terr in ('|| l_t_select_position || ' ))';
		/*if ( lower(l_t_rule_type) = l_c_rule_top_down )	then
			l_tmp_query := l_tmp_query || '  and  terr in ('|| l_t_select_position || ') ';
		end if;*/
		raise notice 'l_tmp_query -> %',l_tmp_query;
		execute l_tmp_query ;
		get diagnostics AFFECTEDROWS = row_count;
		raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
		
		IF(l_i_log_flag = 0) THEN
		RAISE NOTICE '>>>>>>>>>      l_t_parent_table_str    : %', l_tmp_query ;
		END IF;

---------- Applying child expression on customer master  
        if l_t_child_exp is null then
        l_t_child_exp := ' 1 = 1 ';
        end if;
    	
		l_tmp_query :=   ' insert into ' || l_t_child_table_str || '  select * from '|| l_t_tmp_cust_mas_terr_table ||' where '|| l_t_child_exp ||'  ';
		--and  terr in ('|| l_t_select_position || ') )  ';
		/*if ( lower(l_t_rule_type) = l_c_rule_botm_up )	then
			l_tmp_query := l_tmp_query || '  and  terr in ('|| l_t_select_position || ') ';
		end if;*/
		
		raise notice 'l_tmp_query -> %',l_tmp_query;
		execute l_tmp_query ;
		get diagnostics AFFECTEDROWS = row_count;
		raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
		
		IF(l_i_log_flag = 0) THEN
		RAISE NOTICE '>>>>>>>>>      l_t_child_table_str    : %', l_tmp_query ;
		RAISE NOTICE '>>>>>>>>>      l_t_rule_type    : %', l_t_rule_type ;		
		END IF;
		
        IF(l_i_log_flag = 1) THEN
            INSERT INTO t_log(scenario_id, log, line_no, function_name)
            VALUES (l_t_scn_rule_int_id, 'Query string of customer master with parent account expr : '|| l_t_parent_table_str ||' and Query string of customer master with parent account expr :'|| l_t_child_table_str  , '268', l_t_fn_name); 
        END IF;
     
	 	
		
	IF ( lower(l_t_rule_type) = l_c_rule_botm_up ) THEN
	
	l_t_elig_affli_query := 'insert into ' || l_t_tmp_affiliation_table ||
	' select distinct af.*,''1'' as child_terr,p.terr as Parent_terr,''N'' as is_origin, ''N'' as is_branch from '|| 
		l_t_affiliation_table || ' af  left outer join ' || l_t_tmp_cust_mas_terr_table || ' p on  af.'
		|| l_t_parent_col_aff || ' = p.'||l_t_cust_id_col_univ || '  ';-- and  terr in ('|| l_t_select_position || ' )';
	
	ELSIF (lower(l_t_rule_type) = l_c_rule_top_down) THEN
	
	l_t_elig_affli_query := 		'insert into ' || l_t_tmp_affiliation_table ||
	' select distinct af.*,c.terr as child_terr,''1'' as Parent_terr,''N'' as is_origin, ''N'' as is_branch from ' || 
		l_t_affiliation_table || ' af left outer join ' || l_t_tmp_cust_mas_terr_table || 
		' c  on af.' || l_t_child_col_aff || ' = c.' ||l_t_cust_id_col_univ || ' ';-- and  terr in ('|| l_t_select_position || ' )';
	
	END IF;
	raise notice  'l_t_elig_affli_query : % ',	l_t_elig_affli_query ;
	
	IF(l_i_log_flag = 0) THEN
	raise notice  'fn_affiliation_rule >> l_t_elig_affli_query line 368: % ',	l_t_elig_affli_query ;	
	RAISE NOTICE '@@@@@@@@l_t_elig_affli_query start @@@Time ID: %', clock_timestamp();
	END IF;
		    
	IF(l_i_log_flag = 1) THEN
            INSERT INTO t_log(scenario_id, log, line_no, function_name)
            VALUES (l_t_scn_rule_int_id, 'Query string for eligible affiliation table : '|| l_t_elig_affli_query  , '279', l_t_fn_name); 
     END IF;		
		
	
		
	-- Populate eligible affiliation table 
	EXECUTE l_t_elig_affli_query ;
	get diagnostics AFFECTEDROWS = row_count;
	raise notice ' l_t_elig_affli_query- > AFFECTEDROWS -%', AFFECTEDROWS;
	
	IF(l_i_log_flag = 0) THEN
	RAISE NOTICE '@@@@@l_t_elig_affli_query End @@@Time ID: %', clock_timestamp();
	RAISE NOTICE '@@@@@ UPDATE in temp affiliation start @@@Time ID: %', clock_timestamp();
	END IF;	
	
	/****************************
	-- Populate base_alignment value on basis of rule and top-down/bottom-up(l_t_rule_type)
	i.e.  Populate Seed (Is_Origin) and branch (Is_branch) column on basis of l_t_rule_type
	Idea is to identify affiliation tupple with base (Origin) alignment
	********************************/

	IF ( lower(l_t_rule_type) = l_c_rule_botm_up ) THEN

		l_t_updt_orgin_brnch_aff_qry := 
		' Update '|| l_t_tmp_affiliation_table ||' af SET is_origin =''Y'' where exists ( select 1 from ' 
		||l_t_child_table_str || ' c where  af.'|| l_t_child_col_aff || ' = c.'||l_t_cust_id_col_univ || ')';

		raise notice 'l_t_updt_orgin_brnch_aff_qry-> %',l_t_updt_orgin_brnch_aff_qry;
		EXECUTE l_t_updt_orgin_brnch_aff_qry ;
		
		get diagnostics AFFECTEDROWS = row_count;
		raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
		
		l_t_updt_orgin_brnch_aff_qry := 
		' Update '|| l_t_tmp_affiliation_table ||' af SET is_branch =''Y''  where exists ( select 1 from ' 
		|| l_t_parent_table_str || ' p where  af.'|| l_t_parent_col_aff || ' = p.'||l_t_cust_id_col_univ || ')';
			
		raise notice 'l_t_updt_orgin_brnch_aff_qry-> %',l_t_updt_orgin_brnch_aff_qry;
		EXECUTE l_t_updt_orgin_brnch_aff_qry ;
		get diagnostics AFFECTEDROWS = row_count;
		raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
		
	ELSIF (lower(l_t_rule_type) = l_c_rule_top_down) THEN

		l_t_updt_orgin_brnch_aff_qry := 
		' Update '|| l_t_tmp_affiliation_table ||' af SET is_origin =''Y'' where exists ( select 1 from  ' 
		|| l_t_parent_table_str || ' p where  af.'|| l_t_parent_col_aff || ' = p.'||l_t_cust_id_col_univ || ')';

		raise notice 'l_t_updt_orgin_brnch_aff_qry-> %',l_t_updt_orgin_brnch_aff_qry;
		EXECUTE l_t_updt_orgin_brnch_aff_qry ;
		get diagnostics AFFECTEDROWS = row_count;
		raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
		
		l_t_updt_orgin_brnch_aff_qry := 
		' Update '|| l_t_tmp_affiliation_table ||' af SET is_branch =''Y'' where exists ( select 1 from  ' 
		|| l_t_child_table_str || ' c where  af.'|| l_t_child_col_aff || ' = c.'||l_t_cust_id_col_univ || ')';
			
		raise notice 'l_t_updt_orgin_brnch_aff_qry-> %',l_t_updt_orgin_brnch_aff_qry;
		EXECUTE l_t_updt_orgin_brnch_aff_qry ;
		get diagnostics AFFECTEDROWS = row_count;
		raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;

	END IF;

		IF(l_i_log_flag = 0) THEN
		raise notice  'l_t_updt_orgin_brnch_aff_qry for origin >> : % ',	l_t_updt_orgin_brnch_aff_qry  ;
		raise notice  'l_t_updt_orgin_brnch_aff_qry  for branch >> : % ',	l_t_updt_orgin_brnch_aff_qry ;
		RAISE NOTICE '@@@@@ UPDATE in temp affiliation end @@@Time ID: %', clock_timestamp();
		END IF;	
		
		-- Deleting non eligible record from temp affiliation table
		EXECUTE 'Delete from '|| l_t_tmp_affiliation_table ||' where is_origin =''N'' and is_branch =''N'' ';
        
		IF(l_i_log_flag = 0) THEN
		RAISE NOTICE '@@@@@ DELETE in temp affiliation end @@@Time ID: %', clock_timestamp();
		END IF;	
		
	END LOOP ;
	

	
	
	----  Can eliminate delete duplicate (change with inset into new new table)
	-- Delete duplicate records of l_t_tmp_affiliation_table
    l_t_delete_query := ' DELETE FROM ' || l_t_tmp_affiliation_table ||' T1 USING '|| l_t_tmp_affiliation_table || 
			' T2  WHERE   T1.ctid < T2.ctid AND  T1.'||l_t_child_col_aff || ' =  T2.' ||l_t_child_col_aff ||
        	' AND  T1.'||l_t_parent_col_aff || ' =  T2.' ||l_t_parent_col_aff || 
			' AND T1.child_terr = T2.child_terr AND  T1.parent_terr = T2.parent_terr ';
	
	raise notice 'l_t_delete_query- > %',l_t_delete_query;
	execute l_t_delete_query;
	get diagnostics AFFECTEDROWS = row_count;
	raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
		
	IF(l_i_log_flag = 0) THEN
	raise notice  'fn_affiliation_rule >> Delete duplicate records of l_t_tmp_affiliation_table -- l_t_delete_query line 562: % ',	l_t_delete_query ;	
	RAISE NOTICE '@@@@@ Delete in temp affiliation End @@@Time ID: %', clock_timestamp();
	raise notice  '---- ***** Blocked customer handling start line 565 ';
	END IF;
	
	/********************************************************************
	 					handle blocked customer
	********************************************************************/
	
	-- Check distinct product for team || assiumption is table will be populated for scenario related team only
	l_t_tmp_query := 'select count(distinct ' ||  l_t_prd_id_tp  || ' ) from ' || l_t_team_prod_table  ;
	
	EXECUTE (l_t_tmp_query) into l_n_prdt_count;
	
	------ Populate blocked customer list from customer product block
	
	l_t_block_query := 'insert into ' || l_t_tmp_block_cust_table ||
	' select  '|| l_t_cust_id_cust_prd_blk || ' from ' || l_t_cust_prod_block_table ||
	' where ( ' || l_t_prd_id_cust_prd_blk || ' is null or ' || l_t_prd_id_cust_prd_blk || ' 
	= '''' ) or '|| l_t_cust_id_cust_prd_blk || ' in ( select '|| l_t_cust_id_cust_prd_blk || 
	' from ' || l_t_cust_prod_block_table || ' group by '|| l_t_cust_id_cust_prd_blk || 
	' having count (distinct ' || l_t_prd_id_cust_prd_blk || ') = ' || l_n_prdt_count || ' ) ';
	
	--IF(l_i_log_flag = 0) THEN
	raise notice  'l_t_tmp_query for temp table of total Pruduct list : % ',	l_t_tmp_query ;
	raise notice  'l_t_tmp_query for collect blocked customer list : % ',	l_t_block_query ;
	--END IF;
	
	execute l_t_block_query ;
	get diagnostics AFFECTEDROWS = row_count;
	raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
	
	---- Delete blocked customer from eligible hierarchy
	
	
	 l_t_tmp_query := 
	 'delete from ' || l_t_tmp_affiliation_table  || ' a where  Exists ( select 1 from ' ||
	  l_t_tmp_block_cust_table || ' b where a.' || l_t_child_col_aff || ' = b.'|| l_t_cust_id_col_ct || 
	  ' ) or ' || ' Exists ( select 1 from ' || l_t_tmp_block_cust_table || ' c where a.' || l_t_parent_col_aff || 
	  ' = c.'|| l_t_cust_id_col_ct ||  ' ) ';
	 	 
-- 	 'delete from ' || l_t_tmp_affiliation_table  ||
-- 	 ' where ' || l_t_child_col_aff || ' in ( select '|| l_t_cust_id_col_ct || ' from '|| l_t_tmp_block_cust_table || ') ' 
-- 	 || ' or ' || l_t_parent_col_aff || ' in ( select '|| l_t_cust_id_col_ct || ' from ' ||l_t_tmp_block_cust_table || ')';
	raise notice  'l_t_tmp_query for temp table of delete blocked customer from affiliation hierarchy: % ',	l_t_tmp_query ;
	EXECUTE l_t_tmp_query;
	get diagnostics AFFECTEDROWS = row_count;
	raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
	
	--IF(l_i_log_flag = 0) THEN
	raise notice  'l_t_tmp_query for temp table of delete blocked customer from affiliation hierarchy: % ',	l_t_tmp_query ;
	RAISE NOTICE '@@@@@ Blocked customer handling End @@@Time ID: %', clock_timestamp();
	--END IF;
	
	execute 'analyze ' || l_t_tmp_affiliation_table;
	
	IF(l_i_log_flag = 0) THEN
	RAISE NOTICE '>>>>> l_t_tmp_affiliation_table analyzed @@@Time ID: %', clock_timestamp();
	raise notice  '---- ***** Blocked customer handling end line 623 ';
	END IF;
	
	/***************** blocked customer code end  ***********************/
		
	-- Columns for cust-terr dataset
    SELECT string_agg(tb_col_nm, ',') 
        FROM t_ds_col_detail 
        WHERE dataset_id =  l_t_cust_terr_ds_id 
        INTO l_t_cust_terr_colms;
    
     raise notice 'l_t_cust_terr_colms :%',l_t_cust_terr_colms;
	 
     SELECT 'c.'||string_agg(tb_col_nm, ',c.') 
        FROM t_ds_col_detail 
        WHERE dataset_id =  l_t_cust_terr_ds_id 
        INTO l_t_cust_terr_colms_for_origin;
    
    SELECT 'c.'||string_agg(tb_col_nm, ',c.') 
        FROM t_ds_col_detail 
        WHERE dataset_id =  l_t_cust_terr_ds_id 
        INTO l_t_cust_terr_colms_for_rule;
    
      -- raise notice 'adding c. before column name l_t_cust_terr_colms_for_rule :%',l_t_cust_terr_colms_for_rule;
	
    IF(l_i_log_flag = 1) THEN
        INSERT INTO t_log(scenario_id, log, line_no, function_name)
        VALUES (l_t_scn_rule_int_id, 'Column list of cust terr : '|| l_t_cust_terr_colms || ' Column list after appending c. '|| l_t_cust_terr_colms_for_rule , '317', l_t_fn_name); 
    END IF;    
		
	-- changes done for updating Explicit to Implicit -- 24Sept-2018 for 6.0 release
    --l_t_cust_terr_colms_for_rule := replace(l_t_cust_terr_colms_for_rule, 'c.'||l_t_acct_align_type_ct, '''Explicit'''::text) ;
	l_t_cust_terr_colms_for_rule := replace(l_t_cust_terr_colms_for_rule, 'c.'||l_t_acct_align_type_ct, '''Implicit'''::text) ;
    l_t_cust_terr_colms_for_rule := replace(l_t_cust_terr_colms_for_rule, 'c.'||l_t_aff_based_align_ct, '''True'''::text) ;
	
	IF(l_i_log_flag = 0) THEN
     raise notice  'l_t_acct_align_type_ct : % ',	l_t_acct_align_type_ct ;
     raise notice  'l_t_aff_based_align_ct : % ',	l_t_aff_based_align_ct ;
     raise notice 'replaced l_t_acct_align_type_ct : % with Expilicit and l_t_aff_based_align_ct : % with Yes for expression l_t_cust_terr_colms_for_rule :%',l_t_acct_align_type_ct,l_t_aff_based_align_ct,l_t_cust_terr_colms_for_rule;
     raise notice 'l_t_rule_type :%',l_t_rule_type;
	 raise notice 'l_t_geo_preference :%',l_t_geo_preference;
	 RAISE NOTICE '@@@@@ Creation of Affiliation based Cust-Terr start @@@Time ID: %', clock_timestamp();
	END IF;
	
    IF(l_i_log_flag = 1) THEN
        INSERT INTO t_log(scenario_id, log, line_no, function_name)
        VALUES (l_t_scn_rule_int_id, 'Column list after replacing the acct_align_type and aff_based_align columns: '|| l_t_cust_terr_colms_for_rule , '325', l_t_fn_name); 
    END IF;   
    
    ----------------------------------------------------------------------------------------------------------------
	---------- *************************** Creation of Affiliation based Cust-Terr **************************------- 
	----------------------------------------------------------------------------------------------------------------
    
	-- ***** When rule type is bottom up
			
    IF ( lower(l_t_rule_type) = l_c_rule_botm_up ) THEN
				
         l_t_insrt_orgn_data_qry := ' insert into '|| l_t_tmp_output_table ||'( ' ||  l_t_cust_terr_colms || ',h_level ) select ' 
						|| l_t_cust_terr_colms_for_origin || ',0 from ' || l_t_cust_terr_table || ' c, '|| l_t_tmp_affiliation_table 
						||' af where  af.'|| l_t_child_col_aff || ' = c.'||l_t_cust_id_col_ct || ' and af.is_origin = ''Y''';
			raise notice 'here -> ';	
			raise notice 'l_t_insrt_orgn_data_qry 11 -> %',l_t_insrt_orgn_data_qry;
			raise notice 'l_t_pos_id_cust_prd_blk-> %', l_t_pos_id_cust_prd_blk;
			raise notice 'l_t_acc_id_blck - > %',l_t_acc_id_blck;
			raise notice 't_prod_pos_blckng_tbl - > %', t_prod_pos_blckng_tbl;
			raise notice 'l_t_cust_id_col_ct-> %',l_t_cust_id_col_ct;
			raise notice 'l_t_parent_col_aff - > %',l_t_parent_col_aff; 
			raise notice 'l_t_position_ct - > %',l_t_position_ct;
			
    	--Start Blocking changed on 29-Jan for 7.0
		l_t_insrt_orgn_data_qry :=  l_t_insrt_orgn_data_qry || ' and not exists (select ''x'' from ' || t_prod_pos_blckng_tbl || '  blck where  ( blck.'||l_t_acc_id_blck ||' = c.'||l_t_cust_id_col_ct || ' or  blck.'||l_t_acc_id_blck ||' = af.'|| l_t_parent_col_aff||' ) and blck.'||l_t_pos_id_cust_prd_blk ||' = c.'||l_t_position_ct ||')';
		--End Blocking changed on 29-Jan for 7.0
		
		IF(l_i_log_flag = 0) THEN
		raise notice  'l_t_insrt_orgn_data_qry : % ',l_t_insrt_orgn_data_qry ;
		RAISE NOTICE '>>> Base Cust-Terr insertion start @@@Time ID: %', clock_timestamp();
		END IF;
		
		EXECUTE l_t_insrt_orgn_data_qry ;    
		get diagnostics AFFECTEDROWS = row_count;
		raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
	
        IF(lower(l_t_hierarchy_level) = 'all')THEN
        	l_i_hierarchy_level = 6 ;
        ELSIF (cast(l_t_hierarchy_level as integer) >0)THEN
        	l_i_hierarchy_level = cast(l_t_hierarchy_level as integer);
        END IF;
        
		IF(l_i_log_flag = 0) THEN
		RAISE NOTICE '>>> Base Cust-Terr insertion end @@@Time ID: %', clock_timestamp();
		raise notice  'l_i_hierarchy_level  >>>>>>>>>>>. : % ',l_i_hierarchy_level ;
		END IF;
		
		
        l_t_cust_terr_colms_for_rule := replace(l_t_cust_terr_colms_for_rule, 'c.'||l_t_cust_id_col_ct||',', 'a.'|| l_t_parent_col_aff||',') ;			
				
        WHILE l_i_counter < l_i_hierarchy_level
            LOOP
            	l_t_insert_query := '';
                l_t_insert_query := ' insert into '|| l_t_tmp_output_table ||'( ' ||  l_t_cust_terr_colms || ',h_level ) select distinct   ' || 
									l_t_cust_terr_colms_for_rule || ','|| l_i_counter +1 ||' from ' || l_t_tmp_output_table || 
									' c, '|| l_t_tmp_affiliation_table || ' a where a.is_branch =''Y'' and a.'||l_t_child_col_aff 
									|| ' =  c.'||l_t_cust_id_col_ct ||' and c.h_level = ' || l_i_counter ;
				
                IF (lower(l_t_geo_preference) = l_c_geo_based_ref) THEN		
                    l_t_insert_query := l_t_insert_query || ' and c.' || l_t_position_ct || ' = a.parent_terr '  ;

                    IF(l_i_log_flag = 1) THEN
                        INSERT INTO t_log(scenario_id, log, line_no, function_name)
                        VALUES (l_t_scn_rule_int_id, 'Query string for bottom up Geo based: '|| l_t_insert_query  , '403', l_t_fn_name); 
                    END IF;

				END IF;	
				--- 7.0 Position id 
				 l_t_insert_query := l_t_insert_query || ' and c.' || l_t_position_ct || ' in (select position_id from temp_position_list )  ';
				 
				 -- for blocking 7.0
				l_t_insert_query := l_t_insert_query || '  and not exists (select ''x'' from ' || t_prod_pos_blckng_tbl || ' blck where (blck.'||l_t_acc_id_blck ||' = c.'||l_t_cust_id_col_ct || ' or  blck.'||l_t_acc_id_blck ||' = a.'|| l_t_parent_col_aff ||') and blck.'||l_t_pos_id_cust_prd_blk ||' = c.'||l_t_position_ct ||')' ;
				-- for blocking 7.0
				
				IF(l_i_log_flag = 0) THEN
					raise notice  'ITERATION NUMBER  >>>>>>>>>>>. : % ',l_i_counter ;
					raise notice  'fn_affiliation_rule >>  l_t_insert_query  >>>>>>>>>>>. : % ',l_t_insert_query ;
				END IF;
				
                execute l_t_insert_query;
				get diagnostics AFFECTEDROWS = row_count;
				raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
            	l_i_counter := l_i_counter + 1 ; 
              
            END LOOP ; 
        
		
		
    -- ***** when rule type is top down
    ELSIF (lower(l_t_rule_type) = l_c_rule_top_down) THEN

		l_t_insrt_orgn_data_qry := ' insert into '|| l_t_tmp_output_table ||'( ' ||  l_t_cust_terr_colms || ',h_level ) select  distinct   ' 
									|| l_t_cust_terr_colms_for_origin || ',0 from ' || l_t_cust_terr_table || ' c, '|| 
									l_t_tmp_affiliation_table ||' af where  af.'|| l_t_parent_col_aff || ' = c.'||l_t_cust_id_col_ct || 
									' and af.is_origin = ''Y''';
		
		--Added for blocking for 7.0
		l_t_insrt_orgn_data_qry := l_t_insrt_orgn_data_qry|| '	and not exists (select ''x'' from ' || t_prod_pos_blckng_tbl || ' blck where blck.'||l_t_acc_id_blck ||' = c.'||l_t_cust_id_col_ct || ' and blck.'||l_t_pos_id_cust_prd_blk ||' = c.'||l_t_position_ct ||')';
		
		
		IF(l_i_log_flag = 0) THEN
		raise notice  'l_t_insrt_orgn_data_qry : % ',l_t_insrt_orgn_data_qry ;
		RAISE NOTICE '>>> Base Cust-Terr insertion start @@@Time ID: %', clock_timestamp();
		END IF;
		  
    	EXECUTE l_t_insrt_orgn_data_qry ;    
		get diagnostics AFFECTEDROWS = row_count;
		raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
		
        IF(lower(l_t_hierarchy_level) = 'all')THEN
        	l_i_hierarchy_level = 8 ;
        ELSIF (cast(l_t_hierarchy_level as integer) >0)THEN
        	l_i_hierarchy_level = cast(l_t_hierarchy_level as integer);
        END IF;
		
		IF(l_i_log_flag = 0) THEN
		RAISE NOTICE '>>> Base Cust-Terr insertion end @@@Time ID: %', clock_timestamp();
		raise notice  'l_i_hierarchy_level  >>>>>>>>>>>. : % ',l_i_hierarchy_level ;
		END IF;
		
		
        l_t_cust_terr_colms_for_rule := replace(l_t_cust_terr_colms_for_rule, 'c.'||l_t_cust_id_col_ct||',', 'a.'|| l_t_child_col_aff||',') ;
        
        WHILE l_i_counter < l_i_hierarchy_level
            LOOP
            l_t_insert_query :='';
            l_t_insert_query := 'insert into '|| l_t_tmp_output_table ||'( ' ||  l_t_cust_terr_colms || ',h_level ) select distinct  ' 
            || l_t_cust_terr_colms_for_rule || ','|| l_i_counter +1 || ' from ' || l_t_tmp_output_table || ' c, '|| l_t_tmp_affiliation_table 
			|| ' a where a.is_branch =''Y'' and a.'||l_t_parent_col_aff || ' =  c.'||l_t_cust_id_col_ct ||' and c.h_level = ' || l_i_counter;

            IF (lower(l_t_geo_preference) = l_c_geo_based_ref) THEN		
                l_t_insert_query := l_t_insert_query || ' and c.' || l_t_position_ct || ' = a.child_terr '  ;

                IF(l_i_log_flag = 1) THEN
                    INSERT INTO t_log(scenario_id, log, line_no, function_name)
                    VALUES (l_t_scn_rule_int_id, 'Query string for Top down Geo based: '|| l_t_insert_query  , '456', l_t_fn_name); 
                END IF;

            END IF;
			
			--- 7.0 Position id 
			l_t_insert_query := l_t_insert_query || ' and c.' || l_t_position_ct || ' in (select position_id from temp_position_list )  ';
			
			-- for blocking 7.0
				--l_t_insert_query := l_t_insert_query || 'and not exists (select ''x'' from ' || t_prod_pos_blckng_tbl || ' blck where blck.'||l_t_acc_id_blck ||' = c.'||l_t_cust_id_col_ct || ' and blck.'||l_t_pos_id_cust_prd_blk ||' = c.'||l_t_position_ct ||')' ;
                l_t_insert_query := l_t_insert_query || 'and not exists (select ''x'' from ' || t_prod_pos_blckng_tbl || ' blck where (blck.'||l_t_acc_id_blck ||' = a.'||l_t_parent_col_aff || ' or blck.'||l_t_acc_id_blck ||' = a.'|| l_t_child_col_aff  ||' ) and blck.'||l_t_pos_id_cust_prd_blk ||' = c.'||l_t_position_ct ||')' ;
			-- for blocking 7.0
			
			IF(l_i_log_flag = 0) THEN
				raise notice  'ITERATION NUMBER  >>>>>>>>>>>. : % ',l_i_counter ;
				raise notice  'fn_affiliation_rule >>  l_t_insert_query  >>>>>>>>>>>. : % ',l_t_insert_query ;
			END IF;
			
			
            execute l_t_insert_query;
			get diagnostics AFFECTEDROWS = row_count;
			raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
           	l_i_counter := l_i_counter + 1 ; 
			
            END LOOP ;
			
		
		
    END IF;
	
	IF(l_i_log_flag = 0) THEN
		RAISE NOTICE '@@@@@ Creation of Affiliation based Cust-Terr End @@@Time ID: %', clock_timestamp();
	END IF;
	
    execute ' DELETE FROM ' || l_t_tmp_output_table || ' where h_level = 0';
	---------------------------------------------------------------------------------	
    ----------- Remove duplicate from affliation cust terr data ---------------------
	---------------------------------------------------------------------------------
--     l_t_tmp_query := 'DELETE FROM ' || l_t_tmp_output_table ||' T1 USING '|| l_t_tmp_output_table 
--     || ' T2 WHERE T1.ctid < T2.ctid AND  T1.'||l_t_cust_id_col_ct || ' =  T2.' ||l_t_cust_id_col_ct 
--     || ' AND  T1.'||l_t_position_ct || ' =  T2.' ||l_t_position_ct ;

	IF(l_i_log_flag = 0) THEN
		RAISE NOTICE '>> Remove Duplicate from affiliation based cust-terr start @@@Time ID: %', clock_timestamp();
	END IF;

	l_t_tmp_output_table1 := l_t_tmp_output_table|| '_2';
	execute 'drop table if exists ' || l_t_tmp_output_table1;
	execute ' create table ' || l_t_tmp_output_table1 || ' as select * from  ' || l_t_tmp_output_table || ' limit 0 ';
	
	l_t_tmp_query := ' insert into ' || l_t_tmp_output_table1 || ' select * FROM  ' || l_t_tmp_output_table ||
					' WHERE ctid IN ( SELECT min(ctid)  FROM  ' || l_t_tmp_output_table || ' GROUP  BY ' ||
					 l_t_cust_id_col_ct || ' , ' || l_t_position_ct || ' ) ';
					
	execute l_t_tmp_query;
	get diagnostics AFFECTEDROWS = row_count;
	raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
	
	execute 'drop table if exists ' || l_t_tmp_output_table;
	execute ' alter table ' || l_t_tmp_output_table1 || ' rename to ' || l_t_tmp_output_table || ' ';
	
	IF(l_i_log_flag = 0) THEN
		raise notice  'l_t_tmp_query for insert unique record from affliation cust terr : % ',	l_t_tmp_query ;
		RAISE NOTICE '>> Remove Duplicate from affiliation based cust-terr End @@@Time ID: %', clock_timestamp();
		RAISE NOTICE ' >>>!!! Update columns of affiliation based alignment start @@@Time ID: %', clock_timestamp();
	END IF;

    -----------	 Update columns of affiliation based alignment from cust univ --------
	
	
	perform public.fn_update_affiliation_data(
	l_t_cust_universe_ds_id ,
	l_t_cust_terr_ds_id ,
	l_t_cust_universe_table ,
	l_t_tmp_output_table ,
	l_t_cust_id_col_ct ,
    l_t_cust_id_col_univ ,
	'affiliation') ;

	IF(l_i_log_flag = 0) THEN
	RAISE NOTICE '@@@@@ Update columns of affiliation based alignment End @@@Time ID: %', clock_timestamp();
	END IF;
	
	
	----------------------------------------------------------------------------------------------------------------
	-------- ***********   Insert base cust-terr and Affiliation based cust-terr in final output ******** ----------
    ----------------------------------------------------------------------------------------------------------------
	IF(l_i_log_flag = 0) THEN
	RAISE NOTICE '@@@@@ Final Cust-terr start @@@Time ID: %', clock_timestamp();
	END IF;
	
		raise notice  'l_t_output_table for Insert Affiliation based cust-terr : % ',	l_t_output_table ;
	raise notice  'l_t_cust_terr_table for Insert Affiliation based cust-terr : % ',	l_t_cust_terr_table ;
	
     -- Creating final output table of customer territory table schema
    EXECUTE 'drop table if exists ' ||	 l_t_output_table ;
    EXECUTE 'create table ' || l_t_output_table || ' as select * from ' || l_t_cust_terr_table || ' limit 0 ';
    
    -- Insert cust-terr data in output table
    execute 'insert into '|| l_t_output_table ||' ( ' ||  l_t_cust_terr_colms || ' ) select '|| l_t_cust_terr_colms ||' from ' || l_t_cust_terr_table ;

    -- Insert Affiliation based cust-terr table
    
    SELECT 'c.'||string_agg(tb_col_nm, ',c.') 
        FROM t_ds_col_detail 
        WHERE dataset_id =  l_t_cust_terr_ds_id 
       INTO l_t_cust_terr_colms_final;
    
    l_t_tmp_query := 'insert into '|| l_t_output_table ||'( ' ||  l_t_cust_terr_colms || ' ) 
    select '|| l_t_cust_terr_colms_final ||' from ' || l_t_tmp_output_table ||' c left join '||
    l_t_cust_terr_table || ' ct on  c.'||l_t_cust_id_col_ct || ' =  ct.' ||l_t_cust_id_col_ct ||
    ' AND  c.'||l_t_position_ct || ' =  ct.' ||l_t_position_ct ||
    ' where  ct.' || l_t_cust_id_col_ct || ' is null ' ||
	' AND c.'||l_t_position_ct || ' is not null ';  
    
	Execute l_t_tmp_query;
	get diagnostics AFFECTEDROWS = row_count;
	raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
	
	raise notice '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Update blank Flag to False Code Start' ;
	raise notice ' l_t_aff_based_align_ct - %',l_t_aff_based_align_ct;
	raise notice 'l_t_output_table - %',l_t_output_table;
	l_t_tmp_query := 'update '||l_t_output_table || ' set '||l_t_aff_based_align_ct ||' = ''False'' where '||l_t_aff_based_align_ct || ' is null ' ; 
	raise notice 'l_t_tmp_query - %' , l_t_tmp_query ; 
	Execute l_t_tmp_query;
	get diagnostics AFFECTEDROWS = row_count;
	raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
	raise notice '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Update blank Flag to False Code End' ;
	
	/*raise notice '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Update blank Flag to False Code Start' ;
	raise notice ' l_t_aff_based_align_ct - %',l_t_aff_based_align_ct;
	raise notice 'l_t_output_table - %',l_t_output_table;
	l_t_tmp_query := 'select name from t_business_rule where sfdc_id= '''||l_br_join_rule_id ||'''';
	raise notice 'l_t_tmp_query-> %',l_t_tmp_query;
	execute l_t_tmp_query into l_rule_name ; 
	l_t_tmp_query := 'update '||l_t_output_table ||  ' op set op.'||rule_name ||' = br.name where af.rule_name =' is null '' ; 
	raise notice 'l_t_tmp_query - %' , l_t_tmp_query ; 
	Execute l_t_tmp_query;
	get diagnostics AFFECTEDROWS = row_count;
	raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
	raise notice '<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Update blank Flag to False Code End' ;*/
	
   /*IF(l_i_log_flag = 1) THEN
    -- temp table of customer territory table schema
    EXECUTE 'drop table if exists ' ||	 l_t_tmp_output_table || '' ;
    -- temp table of affiliation table 
	EXECUTE 'drop table if exists ' ||	 l_t_tmp_affiliation_table || '' ;
	-- temp table of Blocked customer
	EXECUTE 'drop table if exists ' ||	 l_t_tmp_block_cust_table || '' ;
	-- temp table of customer territory table 
	EXECUTE 'drop table if exists ' ||  l_t_tmp_cust_mas_terr_table0 || '' ;
	EXECUTE 'drop table if exists ' ||  l_t_tmp_cust_mas_terr_table || '' ;
	-- temp table of Child/Parent Criteria
	EXECUTE ' drop table if exists ' || l_t_parent_table_str || '';
	EXECUTE ' drop table if exists ' || l_t_child_table_str || '' ;
   END IF;*/
   
 /* raise notice 'start hardcoding to set rule_id and rule_name = null for affiliation cases'; 
  l_t_sql_query := 'update '||  l_t_output_table ||' aff set rule_name = null , rule_id = null where lower(affil_based_align) = ''true'' ';
  raise notice 'l_t_sql_query - > %',l_t_sql_query;
  execute l_t_sql_query ;
  raise notice 'end hardcoding' ; 
 */
 l_t_sql_query := 'update '||  l_t_output_table ||' aff set rule_name = '''|| l_br_rule_name ||''' , rule_id = '''|| l_br_join_rule_id ||''' where lower(affil_based_align) = ''true'' ';
  raise notice 'l_t_sql_query - > %',l_t_sql_query;
  execute l_t_sql_query ;
  get diagnostics AFFECTEDROWS = row_count;
	raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
 
 l_t_sql_query := 'insert into '||p_t_output_table  ||' ( ' ||  l_t_cust_terr_colms || ' ) 
    select '|| l_t_cust_terr_colms_final ||' from ' || l_t_output_table || '  c  ';
 raise notice 'l_t_sql_query -> %', l_t_sql_query;
 --return 34;
 execute l_t_sql_query ; 
 get diagnostics AFFECTEDROWS = row_count;
 raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
 
 EXECUTE 'UPDATE t_business_rule 
        			SET status = ''Success'' 
                  WHERE status = ''In Progress'' 
                    AND sfdc_id = ''' || l_t_business_rules || '''' ;
get diagnostics AFFECTEDROWS = row_count;
raise notice 'update business rule to success AFFECTEDROWS -%', AFFECTEDROWS;
				
 RAISE NOTICE 'Affiliation End @@@Time ID: %', clock_timestamp();

RETURN 1;
        
EXCEPTION 
	WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS l_t_error = PG_EXCEPTION_CONTEXT;
        
       
        
        select component_type_label, scenario_id from t_scn_rule_instance_details where sfdc_id = l_t_scn_rule_int_id limit 1
        into l_r_comp_label;
        
        if (l_r_comp_label IS NOT NULL) THEN
        INSERT INTO t_error_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
        	 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,l_r_comp_label.component_type_label,l_r_comp_label.scenario_id,l_t_error,l_t_sql_query,l_t_scn_rule_int_id,l_t_br_stage);
        INSERT INTO t_error_run_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
        	 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,l_r_comp_label.component_type_label,l_r_comp_label.scenario_id,l_t_error,l_t_sql_query,l_t_scn_rule_int_id,l_t_br_stage);
             
      
        ELSE
        INSERT INTO t_error_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
        	 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,'l_r_comp_label.component_type_label','l_r_comp_label.scenario_id',l_t_error,l_t_sql_query,l_t_scn_rule_int_id,l_t_br_stage );
        INSERT INTO t_error_run_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
        	 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,'l_r_comp_label.component_type_label','l_r_comp_label.scenario_id',l_t_error,l_t_sql_query,l_t_scn_rule_int_id,l_t_br_stage);
        
        END IF;
        
        EXECUTE 'UPDATE t_business_rule 
        			SET status = ''Error Occured'' 
                  WHERE status = ''In Progress'' 
                    AND sfdc_id = ''' || l_t_business_rules || '''' ;
                   
        RETURN -1;
END;

$BODY$;

ALTER FUNCTION public.fn_affiliation_rule_position_level_rule(text, text, text, text)
    OWNER TO postgres;
