-- FUNCTION: public.fn_merge_rule(text, text)

-- DROP FUNCTION public.fn_merge_rule(text, text);

CREATE OR REPLACE FUNCTION public.fn_merge_rule(
	p_t_scn_rule_int_id text,
	p_t_output_table text)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

l_t_fn_name text := 'fn_merge_rule';
l_t_scn_rule_int_id text := p_t_scn_rule_int_id;
l_t_merge_sel_expr text;
l_t_merge_expr text;
l_t_error text;
l_t_sql_query text := '';
l_t_sql_query1 text := '';
l_t_output_table text := p_t_output_table;
l_t_insert_exec_expr text;
l_t_business_rules text;
l_n_count integer;
l_r_comp_label RECORD;
l_t_merge_temp_ds record;
l_t_where_expr text;
l_t_br_stage text;
l_t_merge_temp_table1 text;
l_t_merge_temp_table2 text;
l_t_merge_temp_table3 text;
l_t_merge_temp_table4 text;
l_t_merge_temp_table5 text;
l_t_merge_temp_table6 text;
l_t_merge_temp_table7 text;
l_t_sql_query_7b text;
l_t_merge_temp_table8 text;
l_t_sql_query_1 text := '';
l_t_sql_query_2 text := '';
l_t_sql_query_3 text := '';
l_t_sql_query_4 text := '';
l_t_sql_query_5 text := '';
l_t_sql_query_6 text := '';
l_t_sql_query_7 text := '';
l_t_sql_query_8 text := '';
l_inp_table text;
l_t_colm1 text ;
l_n_count_2 integer;
l_t_merge_sel_expr_2 text;
a text;
            
BEGIN
	SELECT COUNT(1)
	  FROM t_business_rule 
     WHERE scenario_rule_instance_details = l_t_scn_rule_int_id
     INTO l_n_count;
    
    IF (l_n_count = 0) THEN
    
    INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, 'No Merge Rule', '34', l_t_fn_name); 
    
    ELSE 
    	SELECT br.name
	  	  FROM t_business_rule br
     	 WHERE br.scenario_rule_instance_details = l_t_scn_rule_int_id
     	  INTO l_t_br_stage;
        
        SELECT sfdc_id
	  	  FROM t_business_rule 
     	 WHERE scenario_rule_instance_details = l_t_scn_rule_int_id
     	  INTO l_t_business_rules;
    	
        SELECT select_execute_expression, insert_exec_expression, business_rules, where_expression
          FROM t_br_joinrule 
         WHERE business_rules = l_t_business_rules
          INTO l_t_merge_sel_expr, l_t_insert_exec_expr, l_t_business_rules, l_t_where_expr;
		  
		select count(1) from t_business_rule a, t_br_type_master b
		where a.business_rule_type_master=b.id
		and b.name= 'Assignment Rules' and 
		a.key_col_value != '' and scenario 
		in (select scenario_id from t_scn_rule_instance_details where sfdc_id= l_t_scn_rule_int_id )
		into
		l_n_count_2;
		
		
		
        -- form dynamic query
        

                            
		if(l_t_where_expr='null' or l_t_where_expr is null or l_t_where_expr='')   then 
        l_t_sql_query = ' INSERT INTO ' || l_t_output_table || '(' || l_t_insert_exec_expr || ')' || l_t_merge_sel_expr ;
      --  else if (l_t_where_expr='') then
     --   l_t_sql_query = ' INSERT INTO ' || l_t_output_table || '(' || l_t_insert_exec_expr || ')' || l_t_merge_sel_expr ;   	
        else
        l_t_sql_query = 'INSERT INTO' || l_t_output_table || '(' || l_t_insert_exec_expr || ')' || l_t_merge_sel_expr ||
       ' where ' || l_t_where_expr;
        end if;
         

        INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, 'query : '|| l_t_sql_query , '59', l_t_fn_name); 

        EXECUTE (l_t_sql_query);
		
		If (l_n_count_2 !=0) Then

        l_t_sql_query = 'UPDATE t_business_rule 
                            SET status = ''Success'' 
                          WHERE status = ''In Progress'' 
                            AND sfdc_id = ''' || l_t_business_rules || '''' ;

        EXECUTE (l_t_sql_query);
		
		ELSE
		--l_n_count_2 = 1
		SELECT distinct dsrm.table_name
		FROM t_dataset_rule_map dsrm, t_ds_col_detail dscd
		WHERE dsrm.scenario_rule_instance_id = l_t_scn_rule_int_id
		AND ds_type='I'
		AND dsrm.dataset_id=dscd.dataset_id
		AND dscd.field_identifier is null
		AND dsrm.dataset_id not in (select distinct dsrm.dataset_id from t_dataset_rule_map dsrm, t_ds_col_detail dscd
                                   
        WHERE dsrm.scenario_rule_instance_id = l_t_scn_rule_int_id
		AND ds_type='I'
        AND dsrm.dataset_id=dscd.dataset_id
		AND dscd.field_identifier is not null)
		INTO l_inp_table;
		
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, l_inp_table , '128', l_t_fn_name); 
			 
			 
		l_t_merge_temp_table1 := l_t_output_table || '_1';
		l_t_merge_temp_table2 := l_t_output_table || '_2';
		l_t_merge_temp_table3 := l_t_output_table || '_3';
		l_t_merge_temp_table4 := l_t_output_table || '_4';
		l_t_merge_temp_table5 := l_t_output_table || '_5';
		l_t_merge_temp_table6 := l_t_output_table || '_6';
		l_t_merge_temp_table7 := l_t_output_table || '_7';
		
		
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, l_t_merge_temp_table1 , '130', l_t_fn_name); 
		
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, l_t_merge_temp_table2 , '130', l_t_fn_name);
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, l_t_merge_temp_table3 , '130', l_t_fn_name);
		
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, l_t_merge_temp_table4 , '130', l_t_fn_name);
		
		execute ' DROP TABLE IF EXISTS ' ||  l_t_merge_temp_table1 ;
		execute ' DROP TABLE IF EXISTS ' ||  l_t_merge_temp_table2 ;
		execute ' DROP TABLE IF EXISTS ' ||  l_t_merge_temp_table3 ;
		execute ' DROP TABLE IF EXISTS ' ||  l_t_merge_temp_table4 ;
		execute ' DROP TABLE IF EXISTS ' ||  l_t_merge_temp_table5 ;
		execute ' DROP TABLE IF EXISTS ' ||  l_t_merge_temp_table6 ;
		execute ' DROP TABLE IF EXISTS ' ||  l_t_merge_temp_table7 ;
		
		execute ' create temp table if not exists ' || l_t_merge_temp_table1 || ' ( regexp_split_to_table text) ' ;
		execute ' create temp table if not exists ' || l_t_merge_temp_table2 || ' ( reg text) ' ;
		execute ' create temp table if not exists ' || l_t_merge_temp_table3 || ' ( reg text) ' ;
		execute ' create temp table if not exists ' || l_t_merge_temp_table4 || ' ( reg text, rownum integer ) ' ;
		execute ' create temp table if not exists ' || l_t_merge_temp_table5 || ' ( reg text, rownum integer ) ' ;
		execute ' create temp table if not exists ' || l_t_merge_temp_table6 || ' ( reg text, rownum integer ) ' ;
		execute ' create temp table if not exists ' || l_t_merge_temp_table7 || ' ( reg text) ' ;
		select replace (l_t_merge_sel_expr,'''','''''') into l_t_merge_sel_expr_2;
		
		 
		 
		l_t_sql_query_1=    ' insert into  ' || l_t_merge_temp_table1 || 
							'  select regexp_split_to_table ' || '(' || '''' || l_t_merge_sel_expr_2 || '''' || ' , ' || '''union all''' || ')' ;  
							
		EXECUTE (l_t_sql_query_1);
		
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, 'query : '|| l_t_sql_query_1 , '162', l_t_fn_name); 
		
		--return 2;
		 RAISE NOTICE '>>>>>>>>>>>>>>>>>>>>customer master with territory start @@@Time ID: %', clock_timestamp();
		
		l_t_sql_query_2 =  	' Insert into ' || l_t_merge_temp_table2 || 
							 '  select REGEXP_REPLACE ' || ' (reg ' || ',' || '''\s+$''' || ',' || '''''' || ' ) reg from (' ||
							 '  select regexp_split_to_table ' || ' (reg1 ' || ',' || ''' as ''' || ' ) reg from (' ||
							 ' select regexp_split_to_table ' || ' (reg3 ' || ',' || '''select''' || ' ) reg1 from (' ||
							 ' select regexp_split_to_table ' || ' (reg2 ' || ',' || '''from''' || ' ) reg3 from (
							 select  regexp_split_to_table ' || 
							 ' (regexp_split_to_table ' || ',' || ''',''' || ' ) reg2
							 from ' ||  l_t_merge_temp_table1 || ' where regexp_split_to_table not like ' || '''' || '%'|| l_inp_table  || '%' || ''''  ||  ')' || ' a '  || ' ) b ' || ' ) c '|| ' ) d ' ; 
	    
		EXECUTE (l_t_sql_query_2); 
		
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, 'query : '|| l_t_sql_query_2 , '178', l_t_fn_name); 
		
		  RAISE NOTICE '>>>>>>>>>>>>>>>>>>>>customer master with territory start @@@Time ID: %', clock_timestamp();
		
		l_t_sql_query_3 = 	' Insert into ' || l_t_merge_temp_table3 ||  
							' select * from ' || l_t_merge_temp_table2 || ' where reg not like ( ' || '''' || '%'|| '(' || '%' || ''''  ||  ')' ||
							' and reg not like ( ' ||'''''' || ')'; 
	    
		EXECUTE (l_t_sql_query_3);
		
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, 'query : '|| l_t_sql_query_3 , '190', l_t_fn_name); 
		
		l_t_sql_query_4 = ' Insert into ' || l_t_merge_temp_table4 ||  
							' select trim(reg) , ROW_NUMBER() OVER(ORDER BY (SELECT 100)) AS rownum from ' || l_t_merge_temp_table3	; 
	    
		EXECUTE (l_t_sql_query_4);
		
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, 'query : '|| l_t_sql_query_4 , '198', l_t_fn_name); 
		
		l_t_sql_query_5 = 	' Insert into ' || l_t_merge_temp_table5 || 
							' select trim(a), ROW_NUMBER() OVER(ORDER BY (SELECT 100)) AS rownum ' || 
							' from ( select regexp_split_to_table ' || '(' || '''' || l_t_insert_exec_expr || '''' || ',' || ''','''|| ') a)b '; 
		
		EXECUTE (l_t_sql_query_5);
		
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, 'query : '|| l_t_sql_query_5 , '206', l_t_fn_name); 
		
		
		l_t_sql_query_6 =   ' Insert into ' || l_t_merge_temp_table6 || 	
							' select * from ' ||  l_t_merge_temp_table4 || ' where reg in ' || '(' ||
							' select dscd.tb_col_nm 
							FROM t_dataset_rule_map dsrm, t_ds_col_detail dscd
							WHERE dsrm.scenario_rule_instance_id ' || ' = ' ||'''' || l_t_scn_rule_int_id || '''' ||
							' AND ds_type = ' || '''I''' || 
							'AND dsrm.dataset_id=dscd.dataset_id
							AND dscd.field_identifier = ' || '''Assignment'''   || ' ) ' ;
		raise notice 'l_t_sql_query_6 -> %', l_t_sql_query_6;
		
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, 'query : '|| l_t_sql_query_6 , '220', l_t_fn_name); 
			 
		EXECUTE (l_t_sql_query_6);
		
		l_t_sql_query_7 = ' Insert into ' || l_t_merge_temp_table7 || 
						' SELECT reg FROM ' || l_t_merge_temp_table5 || 
						 ' where rownum in (select rownum from ' || l_t_merge_temp_table6  || ' ) ';
						 
		 EXECUTE (l_t_sql_query_7);
						
						 
		raise notice 'l_t_sql_query_7 -> %', l_t_sql_query_7;			
		
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, 'query : '|| l_t_sql_query_7 , '232', l_t_fn_name); 
		
		-- EXECUTE (l_t_sql_query_7b) ;
		l_t_sql_query_7b := 'select reg from ' ||  l_t_merge_temp_table7 ||' where reg in ' ||
							' (select tb_col_nm   from t_ds_col_detail where dataset_id in 
		 					( select dataset_id from t_dataset_rule_map                                                              
		 					where scenario_rule_instance_id ' || ' = ''' || l_t_scn_rule_int_id
							|| ''' and ds_type= ''O'' ))' ;
	
	raise notice 'l_t_sql_query_7b -> %',l_t_sql_query_7b;
	
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, 'query : '|| l_t_sql_query_7b , '260', l_t_fn_name); 
	
		for l_t_merge_temp_ds in execute  l_t_sql_query_7b
		
		LOOP
		
		--l_t_colm1 = l_t_merge_temp_table7.tb_col_nm;
		--return 8;
		raise notice 'l_t_output_table -> %', l_t_output_table;
		 
		raise notice 'l_t_merge_temp_table7.tb_col_nm -> %', l_t_merge_temp_ds.reg;
		 
		l_t_sql_query_8 = ' UPDATE ' || l_t_output_table || ' SET ' || l_t_merge_temp_ds.reg || '  = NULL ';
			
		raise notice 'l_t_sql_query_8 -> %', l_t_sql_query_8;
		 
		EXECUTE (l_t_sql_query_8);
		
		END LOOP;
		--return 6;
		l_t_sql_query = 'UPDATE t_business_rule 
                            SET status = ''Success'' 
                          WHERE status = ''In Progress'' 
                            AND sfdc_id = ''' || l_t_business_rules || '''' ;

        EXECUTE (l_t_sql_query);	

		END If; 
		
		
		

	END IF;
    
	RETURN 1;
        
EXCEPTION 
	WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS l_t_error = PG_EXCEPTION_CONTEXT;
        
        select component_type_label,scenario_id from t_scn_rule_instance_details where sfdc_id = p_t_scn_rule_int_id limit 1
        into l_r_comp_label;
        
        if (l_r_comp_label IS NOT NULL) THEN
        INSERT INTO t_error_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
        	 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_sql_query_7b,l_t_sql_query_5,l_t_sql_query_8,l_t_colm1,l_t_sql_query_7,l_t_sql_query_4,l_t_sql_query_6 );
        INSERT INTO t_error_run_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
        	 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,l_r_comp_label.component_type_label,l_r_comp_label.scenario_id,l_t_error,l_t_sql_query,p_t_scn_rule_int_id,l_t_br_stage );
        
        ELSE
        INSERT INTO t_error_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
        	 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,'l_r_comp_label.component_type_label','l_r_comp_label.scenario_id',l_t_error,l_t_sql_query,p_t_scn_rule_int_id,l_t_br_stage );
        INSERT INTO t_error_run_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
        	 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,'l_r_comp_label.component_type_label','l_r_comp_label.scenario_id',l_t_error,l_t_sql_query,p_t_scn_rule_int_id,l_t_br_stage );
        
        END IF;
        EXECUTE 'UPDATE t_business_rule 
        			SET status = ''Error Occured'' 
                  WHERE status = ''In Progress'' 
                    AND sfdc_id = ''' || l_t_business_rules || '''' ;
        RETURN -1;
END;

$BODY$;

ALTER FUNCTION public.fn_merge_rule(text, text)
    OWNER TO postgres;
