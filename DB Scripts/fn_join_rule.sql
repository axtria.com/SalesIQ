-- FUNCTION: public.fn_join_rule(text, text)

-- DROP FUNCTION public.fn_join_rule(text, text);

CREATE OR REPLACE FUNCTION public.fn_join_rule(
	p_t_scn_rule_int_id text,
	p_t_output_table text)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

l_t_fn_name text := 'fn_join_rule';
l_t_scn_rule_int_id text := p_t_scn_rule_int_id;
l_t_join_sel_expr text;
l_t_join_expr text;
l_t_error text;
l_t_sql_query text := '';
l_t_output_table text := p_t_output_table;
l_t_insert_exec_expr text;
l_t_business_rules text;
l_n_count integer;
l_t_where_expr text := '1=1';
l_r_comp_label RECORD;
l_t_scenario_check text;
l_t_br_stage text;
--Run Log
l_org_id text :='AA' ;
l_scenario_run_id text := p_t_scn_rule_int_id ; 
l_log_type text;
l_log_level_basic text := 'Basic' ; 
l_log_level_detailed text := 'Detailed' ; 
l_run_status_in_progress text :='In Progress';
l_run_status_complete text :='Complete';
l_run_status_error text :='Error';
l_line_no text ; 
l_function_name text ;
l_log_type_info text :='Information';
l_log_type_warning text :='Warning';
l_log_type_error text :='Error';
l_input_count integer ;
l_output_count integer ;
p_t_scn_id text;

BEGIN
   
    SELECT sfdc_id
	  FROM t_business_rule 
     WHERE scenario_rule_instance_details = l_t_scn_rule_int_id
      INTO l_t_business_rules;
          
    SELECT count(1)
      FROM t_br_joinrule 
     WHERE business_rules = l_t_business_rules
      INTO l_n_count;
	
	SELECT scenario_id 
	  FROM t_scn_rule_instance_details srid 
	 WHERE sfdc_id =l_t_scn_rule_int_id
	  INTO p_t_scn_id ;
	
	RAISE NOTICE 'l_org_id -> % ',l_org_id; 
	RAISE NOTICE 'p_t_scn_id -> % ',p_t_scn_id;
	RAISE NOTICE 'l_scenario_run_id -> % ',p_t_scn_id;
	RAISE NOTICE 'l_log_type_info -> % ',p_t_scn_id;
	RAISE NOTICE 'l_log_level_basic -> % ',p_t_scn_id;
	RAISE NOTICE 'l_run_status_in_progress  -> %' ,p_t_scn_id;
	RAISE NOTICE 'l_t_business_rules -> %' ,p_t_scn_id;
	
	--return 34;
	PERFORM fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
					l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
					'Scenario - Join Rule Id- '||l_t_business_rules ,
					78 , l_t_fn_name) ; 
	
 IF (l_n_count = 0) THEN
    
    INSERT INTO t_log(scenario_id, log, line_no, function_name)
         VALUES (l_t_scn_rule_int_id, 'No Join Rule', '43', l_t_fn_name); 
 Else
    	SELECT br.name
	  	  FROM t_business_rule br
     	 WHERE br.scenario_rule_instance_details = l_t_scn_rule_int_id
     	  INTO l_t_br_stage;
 
        SELECT select_execute_expression, join_execute_expression, insert_exec_expression, where_expression
          FROM t_br_joinrule 
         WHERE business_rules = l_t_business_rules
          INTO l_t_join_sel_expr, l_t_join_expr, l_t_insert_exec_expr, l_t_where_expr;  
		
		raise notice 'l_t_br_stage -> %',l_t_br_stage;
		raise notice 'l_t_join_sel_expr -> %',l_t_join_sel_expr;
		raise notice 'l_t_insert_exec_expr -> %',l_t_insert_exec_expr ;
		raise notice 'l_t_where_expr -> %',l_t_where_expr ;
		
		PERFORM fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
						l_log_type_info  , l_log_level_basic , l_run_status_in_progress, 
						'Scenario - Join Rule- '|| COALESCE (l_t_br_stage,'NULL')   ||' & l_t_join_sel_expr -   '
						||COALESCE (l_t_join_sel_expr,'NULL') ||' & l_t_join_expr - '||' & l_t_insert_exec_expr - '
						||COALESCE (l_t_insert_exec_expr,'NULL') ||' & l_t_where_expr - '||COALESCE (l_t_where_expr,'NULL') ,
						105 , l_t_fn_name) ; 
		-- form dynamic query

        IF(l_t_where_expr='null' or l_t_where_expr is null or l_t_where_expr='') then
			l_t_sql_query = 'INSERT INTO ' || l_t_output_table || '(' 
							|| l_t_insert_exec_expr || ') '|| l_t_join_sel_expr || ' FROM (' || 
							l_t_join_expr || ')';
        
        ELSE 
			l_t_sql_query = 'INSERT INTO ' || l_t_output_table || '(' 
							|| l_t_insert_exec_expr || ') '|| l_t_join_sel_expr || ' FROM (' || 
							l_t_join_expr || ')' || ' where ' || l_t_where_expr;             
        END IF;

        INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, 'query : '|| l_t_sql_query, '50',l_t_fn_name ); 

        EXECUTE (l_t_sql_query);
		
		
		raise notice 'l_t_sql_query -> %',l_t_sql_query;
		Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
						l_log_type_info  , l_log_level_basic , l_run_status_in_progress, 
						'Scenario - Join Rule- query : '|| l_t_sql_query ,
						129 , l_t_fn_name) ; 
		
        l_t_sql_query = 'UPDATE t_business_rule 
                            SET status = ''Success'' 
                          WHERE status = ''In Progress'' 
                            AND sfdc_id = ''' || l_t_business_rules || '''' ;

        EXECUTE (l_t_sql_query);
		
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
             VALUES (l_t_scn_rule_int_id, 'query : '|| l_t_sql_query, '134', l_t_fn_name); 
		
		Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
						l_log_type_info  , l_log_level_basic , l_run_status_complete, 
						'Scenario - Join Rule- '|| COALESCE (l_t_br_stage,'NULL')   ||' & l_t_join_sel_expr -   '
						||COALESCE (l_t_join_sel_expr,'NULL') ||' & l_t_join_expr - '||' & l_t_insert_exec_expr - '
						||COALESCE (l_t_insert_exec_expr ,'NULL')||' & l_t_where_expr - '||COALESCE (l_t_where_expr,'NULL') ,
						146 , l_t_fn_name) ; 
  END IF;
    
	RETURN 1;
        
EXCEPTION 
	WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS l_t_error = PG_EXCEPTION_CONTEXT;
        
       
        select component_type_label, scenario_id from t_scn_rule_instance_details where sfdc_id = p_t_scn_rule_int_id limit 1
        into l_r_comp_label;
        
        if (l_r_comp_label IS NOT NULL) THEN
			INSERT INTO t_error_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
				 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,l_r_comp_label.component_type_label,l_r_comp_label.scenario_id,l_t_error,l_t_sql_query,l_t_scn_rule_int_id,l_t_br_stage);
			INSERT INTO t_error_run_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
				 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,l_r_comp_label.component_type_label,l_r_comp_label.scenario_id,l_t_error,l_t_sql_query,l_t_scenario_check,l_t_br_stage);
				 
			Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
						l_log_type_error  , l_log_level_basic , l_run_status_error, 'Error Join Rule failed - '||SQLERRM || ' - '||SQLSTATE || ' Query -> '||l_t_sql_query   ||'  ',
						167 , l_t_fn_name) ;
        
        ELSE
        
			INSERT INTO t_error_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
				 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,'l_r_comp_label.component_type_label','l_r_comp_label.scenario_id',l_t_error,l_t_sql_query,p_t_scn_rule_int_id,l_t_br_stage);
			INSERT INTO t_error_run_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
				 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,'l_r_comp_label.component_type_label','l_r_comp_label.scenario_id',l_t_error,l_t_sql_query,p_t_scn_rule_int_id,l_t_br_stage);
			Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
						l_log_type_error  , l_log_level_basic , l_run_status_error, 'Error Join Rule failed - '||SQLERRM || ' - '||SQLSTATE || ' Query -> '||l_t_sql_query   ||'  ',
						177 , l_t_fn_name) ;
        
        END IF;
        EXECUTE 'UPDATE t_business_rule 
        			SET status = ''Error Occured'' 
                  WHERE status = ''In Progress'' 
                    AND sfdc_id = ''' || l_t_business_rules || '''' ;
        RETURN -1;
END;

$BODY$;

ALTER FUNCTION public.fn_join_rule(text, text)
    OWNER TO postgres;
