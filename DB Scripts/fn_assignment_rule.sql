-- FUNCTION: public.fn_assignment_rule(text, text)

-- DROP FUNCTION public.fn_assignment_rule(text, text);

CREATE OR REPLACE FUNCTION public.fn_assignment_rule(
	p_t_scn_rule_int_id text,
	p_t_output_table text)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

l_t_fn_name text := 'fn_assignment_rule';
l_t_scn_rule_int_id text := p_t_scn_rule_int_id;
l_t_scn_rule_instance RECORD;
l_t_join_sel_expr text;
l_t_join_expr text;
l_t_error text;
l_t_sql_query text := '';
l_t_output_table text := p_t_output_table;
l_t_insert_exec_expr text;
l_t_business_rules text;
l_n_count integer;
l_t_where_expr text;
l_br_table RECORD;
l_r_comp_label RECORD;  
flag integer;
l_t_select_position text ; -- FOR POSITION LEVEL CHANGES
c_position_level   text;	-- FOR POSITION LEVEL CHANGES
l_t_ds text;  -- FOR POSITION LEVEL CHANGES
AFFECTEDROWS integer := 0;
l_t_col_names  text;
l_t_dataset_id text;
l_rule_id_count integer;
l_selected_position text;
 
l_t_position_id_col text ; -- POSITION PRODUCT CHANGES
l_t_team_position_prod  text; -- POSITION PRODUCT CHANGES
l_t_position_product_dtstid  text;-- POSITION PRODUCT CHANGES
l_t_position_id_col_prod text;-- POSITION PRODUCT CHANGES
l_t_br_rule_temp_table text;-- POSITION PRODUCT CHANGES
l_t_acc_id_col text ; 

l_t_col_names_two  text; 
--Run Log
l_org_id text :='AA' ;
l_scenario_run_id text := p_t_scn_rule_int_id; 
l_log_type text;
l_log_level_basic text := 'Basic' ; 
l_log_level_detailed text := 'Detailed' ; 
l_run_status_in_progress text :='In Progress';
l_run_status_complete text :='Complete';
l_run_status_error text :='Error';
l_line_no text ; 
l_function_name text ;
l_log_type_info text :='Information';
l_log_type_warning text :='Warning';
l_log_type_error text :='Error';
l_input_count integer ;
l_output_count integer ;
p_t_scn_id text;
BEGIN

	SELECT COUNT(1)
	  FROM t_business_rule 
    WHERE scenario_rule_instance_details = l_t_scn_rule_int_id
     INTO l_n_count;
    
	SELECT scenario_id 
	  FROM t_scn_rule_instance_details srid 
	WHERE sfdc_id =l_t_scn_rule_int_id
	  INTO p_t_scn_id ;
	
    IF (l_n_count = 0) THEN
    
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
				 VALUES (l_t_scn_rule_int_id, 'No Assignment Rule', '41', l_t_fn_name); 
    
    ELSE 
		
		 INSERT INTO t_log(scenario_id, log, line_no, function_name)
				 VALUES (l_t_scn_rule_int_id, 'Assignment Rule Exist', '-44', l_t_fn_name); 
		
	   
		 FOR l_br_table IN 
				SELECT sfdc_id,
					   coalesce(upper(rule_level), 'a') rule_level,
					   selected_positions,
					   coalesce(key_col_value , 'a') key_col_value,
					   coalesce(key_col_name , 'a') key_col_name 
				FROM
					   t_business_rule 	--SELECT sfdc_id FROM t_business_rule 
				WHERE  scenario_rule_instance_details = l_t_scn_rule_int_id
				
			LOOP
				/* IF - ELSE -1 for POSITION LEVEL/TEAM LEVEL RULES */
				RAISE NOTICE 'business_rule - sfdcid - %', l_br_table.sfdc_id ;
				RAISE NOTICE 'l_br_table.rule_level - %', l_br_table.rule_level;
				--for position product rule
				l_t_br_rule_temp_table := l_t_output_table||'_'||l_br_table.sfdc_id;
				
				EXECUTE 'drop table if exists '||l_t_br_rule_temp_table;
				
				l_t_sql_query :=' create temp table '|| l_t_br_rule_temp_table ||' as select * from '|| l_t_output_table ||' limit 0 ';
				
				RAISE NOTICE 'l_t_sql_query -> %',l_t_sql_query;
				EXECUTE l_t_sql_query;
				
				PERFORM fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
								l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
								'Scenario - Assignment Rule Id- '|| coalesce( l_br_table.sfdc_id,'NULL') || ' & Rule Level - '|| coalesce( l_br_table.rule_level , 'NULL')||' ' ,
								115 , l_t_fn_name) ; 
				
				IF l_br_table.rule_level in ( 'POSITIONS', 'POSITION')  then --- POSITION LEVEL CHANGES 
				/* IF 1 */
				/* IF TEAM LEVEL = POSITION THEN THIS CODE IS TO BE FOLLOWED */
					raise notice 'inside if position';
					l_t_business_rules = l_br_table.sfdc_id;
					raise notice 'l_t_business_rules - %', l_t_business_rules;
					
					l_t_select_position := l_br_table.selected_positions ; 
					--- CREATE TEMP TABLE FOR ALL THE COMMA SEPERATED POSITIONS -- FASTER PERFORMANCE
					--select chr(39)||replace(  l_t_select_position, ',', ''',''' )||chr(39) 
					--into l_t_select_position ;
					raise notice 'l_t_select_position -> %',l_t_select_position;
					drop table if exists temp_position_list ;
					create temp table temp_position_list (position_id text);
					insert into temp_position_list select regexp_split_to_table( l_t_select_position , E',') as position_id ;
					update temp_position_list set position_id =  trim(from position_id);
					
					--l_t_select_position :=   chr(39)||l_t_select_position||chr(39) ;
					l_t_select_position := 'select position_id from temp_position_list ';
					
					raise notice 'l_t_select_position - %', l_t_select_position;
					
					SELECT select_execute_expression, join_execute_expression, insert_exec_expression, where_expression 
					  FROM t_br_joinrule 
					 WHERE business_rules = l_t_business_rules
					  INTO l_t_join_sel_expr, l_t_join_expr, l_t_insert_exec_expr, l_t_where_expr   ;--, l_t_business_rules;
					  
					  raise notice 'l_t_join_sel_expr - %', l_t_join_sel_expr;
					  raise notice 'l_t_join_expr - %', l_t_join_expr;
					  raise notice 'l_t_insert_exec_expr - %', l_t_insert_exec_expr;
					  raise notice 'l_t_where_expr - %', l_t_where_expr;
					  --raise notice 'l_t_select_position - %', l_t_select_position;
					  
					  PERFORM fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
								l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
								'Scenario - Assignment Rule Id- '|| coalesce( l_br_table.sfdc_id,'NULL') || ' & Rule Level - '|| coalesce( l_br_table.rule_level , 'NULL')||' & l_t_join_sel_expr -> '||coalesce( l_t_join_sel_expr, 'NULL') ||' & l_t_join_expr -> '|| coalesce(l_t_join_expr ,'NULL') ||' & l_t_insert_exec_expr -> '|| coalesce(l_t_insert_exec_expr ,'NULL') ||' & l_t_where_expr -> '|| coalesce( l_t_where_expr,'NULL') ,
								153 , l_t_fn_name) ; 
					  
					 /*SELECT THE COLUMN FOR CHECKING THE POSITIONS IN THE POSITION LEVEL ASSIGNMENT 
					   and assign to c_position_level
					  */				  					
					 SELECT dataset_id
					  FROM t_dataset_rule_map
					 WHERE scenario_rule_instance_id= l_t_scn_rule_int_id 
					   AND lower(file_type)='base' -- check if correct 
					  INTO l_t_ds;
					 RAISE NOTICE 'l_t_ds - %', l_t_ds;
					 
					 SELECT  tb_col_nm  
					  FROM t_ds_col_detail 
					 WHERE dataset_id =  l_t_ds 
					 --and lower(source_column) in( 'position__c', 'axtriasalesiqtm__position__c')
					 AND lower(source_column) like '%position__c'
					  INTO c_position_level;
					 
					 SELECT dataset_id
					   FROM t_dataset_rule_map
					 WHERE scenario_rule_instance_id= l_t_scn_rule_int_id 
					   AND lower(file_type)='assignment' -- check if correct 
					  INTO l_t_dataset_id;
					  
					 RAISE NOTICE 'l_t_dataset_id - %', l_t_dataset_id;
					 RAISE NOTICE 'l_t_dataset_id - %', l_t_dataset_id;
					 
					 l_t_sql_query :=  ' select   tb_col_nm    FROM t_ds_col_detail '
									 ||' WHERE dataset_id =  '''||l_t_dataset_id ||''''
									 ||' and lower(source_column)  like ''%position__c'' ';
					RAISE NOTICE 'l_t_sql_query - %', l_t_sql_query;
					EXECUTE l_t_sql_query into l_t_position_id_col;
					RAISE NOTICE 'l_t_position_id_col -> %',l_t_position_id_col;
					
					IF l_br_table.key_col_value <>'a' THEN  --to differentiate between position and position-product rule
						 RAISE NOTICE 'l_t_ds-%',l_t_ds;
						 RAISE NOTICE 'c_position_level - %', c_position_level;
						 RAISE NOTICE 'l_t_output_table-%', l_t_output_table;
						 RAISE NOTICE 'l_t_br_rule_temp_table- > %',l_t_br_rule_temp_table;
						 RAISE NOTICE 'l_t_insert_exec_expr-%',l_t_insert_exec_expr;
						 RAISE NOTICE 'l_t_join_sel_expr-%',l_t_join_sel_expr;
						 RAISE NOTICE 'l_t_join_expr-%',l_t_join_expr;
						 RAISE NOTICE 'c_position_level-%',c_position_level;
						 RAISE NOTICE 'l_t_select_position-%',l_t_select_position;
						 
						-- form dynamic query
						--l_t_sql_query = 'INSERT INTO ' || l_t_output_table || '(' 
						l_t_sql_query = 'INSERT INTO ' || l_t_br_rule_temp_table || '(' 
										|| l_t_insert_exec_expr || ') '|| l_t_join_sel_expr || ' FROM ' || 
										l_t_join_expr || ' and '||c_position_level || ' in (' || l_t_select_position ||' ) and '|| l_t_position_id_col || ' in (' || l_t_select_position ||' ) ';

						
						l_t_sql_query = l_t_sql_query || '' ;
						raise notice 'l_t_sql_query-%', l_t_sql_query;			
						Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
										'Scenario - Assignment Rule Id Query- '||l_t_sql_query||' ' ,
										212 , l_t_fn_name) ;
						
						INSERT INTO t_log(scenario_id, log, line_no, function_name)
							 VALUES (l_t_scn_rule_int_id, 'query : '|| l_t_sql_query, '112', l_t_fn_name); 

						EXECUTE (l_t_sql_query);
						get diagnostics AFFECTEDROWS = row_count;
						raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
						
					ELSE--to differentiate between position and position-product rule
						 raise notice 'l_t_ds-%',l_t_ds;
						 raise notice 'c_position_level - %', c_position_level;
						 raise notice 'l_t_output_table-%', l_t_output_table;
						 raise notice 'l_t_br_rule_temp_table- > %',l_t_br_rule_temp_table;
						 raise notice 'l_t_insert_exec_expr-%',l_t_insert_exec_expr;
						 raise notice 'l_t_join_sel_expr-%',l_t_join_sel_expr;
						 raise notice 'l_t_join_expr-%',l_t_join_expr;
						 raise notice 'c_position_level-%',c_position_level;
						 raise notice 'l_t_select_position-%',l_t_select_position;
						 
						-- form dynamic query
						--l_t_sql_query = 'INSERT INTO ' || l_t_output_table || '(' 
						l_t_sql_query = 'INSERT INTO ' || l_t_br_rule_temp_table || '(' 
										|| l_t_insert_exec_expr || ') '|| l_t_join_sel_expr || ' FROM ' || 
										l_t_join_expr || ' and '||c_position_level || ' in (' || l_t_select_position ||' )   ';

						
						l_t_sql_query = l_t_sql_query || '' ;
						raise notice 'l_t_sql_query-%', l_t_sql_query;			
						Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
								l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
								'Scenario - Assignment Rule Id Query- '||l_t_sql_query||' ' ,
								244 , l_t_fn_name) ;
						
						INSERT INTO t_log(scenario_id, log, line_no, function_name)
							 VALUES (l_t_scn_rule_int_id, 'query : '|| l_t_sql_query, '112', l_t_fn_name); 

						EXECUTE (l_t_sql_query);
						get diagnostics AFFECTEDROWS = row_count;
						raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
					
					end if;--to differentiate between position and position-product rule
					 
					l_t_sql_query = 'UPDATE t_business_rule 
										SET status = ''Success'' 
									  WHERE status = ''In Progress'' 
										AND sfdc_id = ''' || l_t_business_rules || '''' ;
					raise notice 'l_t_sql_query-%',l_t_sql_query; 
					EXECUTE (l_t_sql_query);
				 
				 else 	/* IF-ELSE 1 */
						raise notice 'inside team level position';
						l_t_business_rules = l_br_table.sfdc_id;
						IF l_br_table.key_col_value = 'a' THEN /* IF 4 */
								SELECT select_execute_expression, join_execute_expression, insert_exec_expression, where_expression--, business_rules
								  FROM t_br_joinrule 
								 WHERE business_rules = l_t_business_rules
								  INTO l_t_join_sel_expr, l_t_join_expr, l_t_insert_exec_expr, l_t_where_expr;--, l_t_business_rules;
						 
								-- form dynamic query
								--l_t_sql_query = 'INSERT INTO ' || l_t_output_table || '(' 
								l_t_sql_query = 'INSERT INTO ' || l_t_br_rule_temp_table || '(' 
												|| l_t_insert_exec_expr || ') '|| l_t_join_sel_expr || ' FROM ' || 
												l_t_join_expr;
								/*
								if(l_t_where_expr IS NOT NULL) then
								 l_t_sql_query = l_t_sql_query || ' where ' || l_t_where_expr;
								end if;
								*/
								l_t_sql_query = l_t_sql_query || '' ;
								
								Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
												'Scenario - Assignment Rule Id Query- '||l_t_sql_query||' ' ,
												286 , l_t_fn_name) ;

								INSERT INTO t_log(scenario_id, log, line_no, function_name)
									 VALUES (l_t_scn_rule_int_id, 'query : '|| l_t_sql_query, '71', l_t_fn_name); 

								EXECUTE (l_t_sql_query);

								l_t_sql_query = 'UPDATE t_business_rule 
													SET status = ''Success'' 
												  WHERE status = ''In Progress'' 
													AND sfdc_id = ''' || l_t_business_rules || '''' ;

								EXECUTE (l_t_sql_query);
						Else/* IF 4 */
								-- Get table_name for position product dataset
								raise notice 'position product type team rule'; 
								
								raise notice 'l_t_scn_rule_int_id -> %',l_t_scn_rule_int_id ;
								 
								l_t_sql_query := 'select table_name ,  dataset_id  from t_dataset_rule_map drm  '
												|| ' where drm.scenario_rule_instance_id = '''|| l_t_scn_rule_int_id || '''' --Assignment
												|| ' and lower(file_type)=''assignment'' ';
								raise notice 'l_t_sql_query -> % ', l_t_sql_query;
								execute l_t_sql_query into l_t_team_position_prod  , l_t_position_product_dtstid; 
								raise notice 'l_t_team_position_prod -> % , l_t_position_product_dtstid -> %', l_t_team_position_prod  , l_t_position_product_dtstid; 
								
								l_t_position_id_col_prod :='a';
								l_t_sql_query := ' select  coalesce( tb_col_nm ,''a'')   FROM t_ds_col_detail '
												||' WHERE dataset_id =  '''||l_t_position_product_dtstid ||''''
												||' and lower(source_column)  like ''%position__c'' '; 
								raise notice 'l_t_sql_query -> % ', l_t_sql_query;
								execute l_t_sql_query into l_t_position_id_col_prod; 
								raise notice 'l_t_position_id_col_prod - > %',l_t_position_id_col_prod;
								
								if l_t_position_id_col_prod <>'a' then -- IF Done for exclusion scenario
											raise notice 'if exclusion ';
											l_t_sql_query :=  ' SELECT DISTINCT drm.dataset_id  '||
															  ' FROM t_dataset ds, t_dataset_rule_map drm '||
															  ' WHERE ds.sfdc_id = drm.dataset_id '||
															  ' AND drm.scenario_rule_instance_id = '''||l_t_scn_rule_int_id ||'''' ||
															  '  and drm.ds_type =''O'' ';
											raise notice 'l_t_sql_query - %', l_t_sql_query;
											execute l_t_sql_query into l_t_dataset_id ;
											raise notice 'l_t_dataset_id - %', l_t_dataset_id ; 
											
											l_t_sql_query :=  ' select   tb_col_nm    FROM t_ds_col_detail '
															||' WHERE dataset_id =  '''||l_t_dataset_id ||''''
															||' and lower(source_column)  like ''%position__c'' ';
											raise notice 'l_t_sql_query - %', l_t_sql_query;
											execute l_t_sql_query into l_t_position_id_col;
											raise notice 'l_t_position_id_col -> %',l_t_position_id_col;						
																	
											-- Done for position product--> only positions linked to the product in position product dataset should be passed
											SELECT select_execute_expression, join_execute_expression, insert_exec_expression, where_expression--, business_rules
											  FROM t_br_joinrule 
											 WHERE business_rules = l_t_business_rules
											  INTO l_t_join_sel_expr, l_t_join_expr, l_t_insert_exec_expr, l_t_where_expr;--, l_t_business_rules;
									 
											-- form dynamic query
											--l_t_sql_query = 'INSERT INTO ' || l_t_output_table || '(' 
											l_t_sql_query = 'INSERT INTO ' || l_t_br_rule_temp_table || '(' 
											|| l_t_insert_exec_expr || ') '|| l_t_join_sel_expr || ' FROM ' || 
											l_t_join_expr ;
											
											/*
											if(l_t_where_expr IS NOT NULL) then
											 l_t_sql_query = l_t_sql_query || ' where ' || l_t_where_expr;
											end if;
											*/
											l_t_sql_query = l_t_sql_query || '' ;
											
											Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
												l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
												'Scenario - Assignment Rule Id Query- '||l_t_sql_query||' ' ,
												360 , l_t_fn_name) ;

											INSERT INTO t_log(scenario_id, log, line_no, function_name)
												 VALUES (l_t_scn_rule_int_id, 'query : '|| l_t_sql_query, '71', l_t_fn_name); 
											raise notice 'l_t_sql_query-> % ',l_t_sql_query;

											EXECUTE (l_t_sql_query);
											get diagnostics AFFECTEDROWS = row_count;
											raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;

											--l_t_sql_query = ' delete from '|| l_t_output_table ||' where '|| l_t_position_id_col || ' not in (select '|| l_t_position_id_col_prod  
											l_t_sql_query = ' delete from '|| l_t_br_rule_temp_table ||' where '|| l_t_position_id_col 
															|| ' not in (select '|| l_t_position_id_col_prod
															|| ' from '|| l_t_team_position_prod || ' where '|| l_br_table.key_col_name 
															||' = '''||l_br_table.key_col_value ||''' )' ;
											raise notice 'l_t_sql_query - %', l_t_sql_query;
											execute l_t_sql_query;
											get diagnostics AFFECTEDROWS = row_count;
											raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
											
											l_t_sql_query = 'UPDATE t_business_rule 
																SET status = ''Success'' 
															  WHERE status = ''In Progress'' 
																AND sfdc_id = ''' || l_t_business_rules || '''' ;

											EXECUTE (l_t_sql_query);
											
								Else -- IF Done for exclusion scenario
											raise notice 'inside exclusion else';
											SELECT select_execute_expression, join_execute_expression, insert_exec_expression, where_expression--, business_rules
											  FROM t_br_joinrule 
											 WHERE business_rules = l_t_business_rules
											  INTO l_t_join_sel_expr, l_t_join_expr, l_t_insert_exec_expr, l_t_where_expr;--, l_t_business_rules;
									 
											-- form dynamic query
											--l_t_sql_query = 'INSERT INTO ' || l_t_output_table || '(' 
											l_t_sql_query = 'INSERT INTO ' || l_t_br_rule_temp_table || '(' 
															|| l_t_insert_exec_expr || ') '|| l_t_join_sel_expr || ' FROM ' || 
															l_t_join_expr;
											/*
											if(l_t_where_expr IS NOT NULL) then
											 l_t_sql_query = l_t_sql_query || ' where ' || l_t_where_expr;
											end if;
											*/
											l_t_sql_query = l_t_sql_query || '' ;
											
											raise notice 'l_t_sql_query-> %',l_t_sql_query;
											
											Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
															l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
															'Scenario - Assignment Rule Id Query- '||l_t_sql_query||' ' ,
															411 , l_t_fn_name) ;

											INSERT INTO t_log(scenario_id, log, line_no, function_name)
												 VALUES (l_t_scn_rule_int_id, 'query : '|| l_t_sql_query, '71', l_t_fn_name); 

											EXECUTE (l_t_sql_query);
											get diagnostics AFFECTEDROWS = row_count;
											raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;

											l_t_sql_query = 'UPDATE t_business_rule 
																SET status = ''Success'' 
															  WHERE status = ''In Progress'' 
																AND sfdc_id = ''' || l_t_business_rules || '''' ;
											raise notice 'l_t_sql_query-> %',l_t_sql_query;
											get diagnostics AFFECTEDROWS = row_count;
											raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
											EXECUTE (l_t_sql_query);
								
								end if;-- IF Done for exclusion scenario
						End If; /* IF 4 */
				 end if; 	/* IF-ENDIF 1 */
				l_t_sql_query = 'insert into '||l_t_output_table ||' select * from '||l_t_br_rule_temp_table ; 
				raise notice 'l_t_sql_query-> %',l_t_sql_query;
				EXECUTE (l_t_sql_query);
				get diagnostics AFFECTEDROWS = row_count;
				raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
										
			END LOOP;
			--return 67;
			/* New Code for removing duplicity after assignment */
				raise notice 'new code started';
				l_t_sql_query :=  ' SELECT DISTINCT drm.dataset_id  '||
								  ' FROM t_dataset ds, t_dataset_rule_map drm '||
								  ' WHERE ds.sfdc_id = drm.dataset_id '||
								  ' AND drm.scenario_rule_instance_id = '''||l_t_scn_rule_int_id ||'''' ||
								  '  and drm.ds_type =''O'' ';
				raise notice 'l_t_sql_query - %', l_t_sql_query;
				execute l_t_sql_query into l_t_dataset_id ;
				raise notice 'l_t_dataset_id - %', l_t_dataset_id ; 
				
				l_t_sql_query :=  ' select   count(tb_col_nm) cnt     FROM t_ds_col_detail    WHERE dataset_id = '''
								 ||l_t_dataset_id ||''' and Coalesce(tb_col_nm,''a'')   in (''rule_name'',''rule_id'')  ' ; 
				raise notice 'l_t_sql_query - %',l_t_sql_query;
				execute l_t_sql_query into l_rule_id_count ;
				raise notice 'l_rule_id_count- %',l_rule_id_count; 
				
				--return 45;
				if l_rule_id_count =2 then  /* This is to exclude the product block cases where in the rule_id and rule_name columns are not there */
				/* IF 3*/
						l_t_sql_query :=  ' select string_agg(tb_col_nm, '', '')  tb_col_nm    FROM t_ds_col_detail '||
											' WHERE dataset_id = '''|| l_t_dataset_id || ''' '||
											--' and source_column is not null '
											' and Coalesce(tb_col_nm,''a'') not in (''rule_name'',''rule_id'') ' ; 
											--and field_identifier is null;
											
						raise notice 'l_t_sql_query - %',l_t_sql_query;
						
						execute l_t_sql_query into l_t_col_names ;
						raise notice 'l_t_col_names- %',l_t_col_names;
						
						Execute ' Drop table if exists '|| l_t_output_table||'_1';
						
						l_t_sql_query:= ' create table '||l_t_output_table||'_1 as select * from ' ||l_t_output_table || '   '; 
						raise notice 'l_t_sql_query - %',l_t_sql_query;
						
						Execute l_t_sql_query ; 
						get diagnostics AFFECTEDROWS = row_count;
						raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;

						
						execute ' drop table if exists '||l_t_output_table||'_one ' ;
						l_t_sql_query :=' create table '|| l_t_output_table||'_one '   || ' as  select '||l_t_col_names 				
						||   ', string_agg(rule_name, '', '' order by rule_id) rule_name  ,string_agg(rule_id, '', ''order by rule_id) rule_id  , count(*) cnt from '
						||l_t_output_table ||' group by '||l_t_col_names ;
						
						raise notice 'l_t_sql_query - > % ' , l_t_sql_query;
						
						Execute l_t_sql_query ; 
						get diagnostics AFFECTEDROWS = row_count;
						raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
						
						l_t_sql_query :=  ' select string_agg(tb_col_nm, '', '')  tb_col_nm    FROM t_ds_col_detail '||
											' WHERE dataset_id = '''|| l_t_dataset_id || ''' '||
											--' and source_column is not null '
											' and Coalesce(tb_col_nm,''a'') not in (''rule_name'',''rule_id'') ' 
											' and field_identifier is null ';
											
						raise notice 'l_t_sql_query - %',l_t_sql_query;
						
						execute l_t_sql_query into l_t_col_names_two ;
						get diagnostics AFFECTEDROWS = row_count;
						raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
						
						raise notice 'l_t_col_names_two- %',l_t_col_names;
						
						--Execute ' Drop table if exists '|| l_t_output_table||'_2';
					
						--l_t_sql_query:= ' create table '||l_t_output_table||'_2 as select * from ' ||l_t_output_table || '   '; 
						--raise notice 'l_t_sql_query - %',l_t_sql_query;
						
						--Execute l_t_sql_query ; 
						
						execute ' drop table if exists '||l_t_output_table||'_two ' ;
						l_t_sql_query :=' create table '|| l_t_output_table||'_two '   || ' as  select '||l_t_col_names_two 				
						||   ', string_agg(rule_name, '', '' order by rule_id) rule_name  ,string_agg(rule_id, '', ''order by rule_id) rule_id  , count(0) cnt  from '
						||l_t_output_table ||' group by '||l_t_col_names_two ;
						
						
						raise notice 'l_t_sql_query - > % ' , l_t_sql_query;
						
						Execute l_t_sql_query ; 
						get diagnostics AFFECTEDROWS = row_count;
						raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
						
						--return 55;
						l_t_sql_query :=  ' SELECT DISTINCT drm.dataset_id  '||
															  ' FROM t_dataset ds, t_dataset_rule_map drm '||
															  ' WHERE ds.sfdc_id = drm.dataset_id '||
															  ' AND drm.scenario_rule_instance_id = '''||l_t_scn_rule_int_id ||'''' ||
															  '  and drm.ds_type =''O'' ';
						raise notice 'l_t_sql_query - %', l_t_sql_query;
						execute l_t_sql_query into l_t_dataset_id ;
						get diagnostics AFFECTEDROWS = row_count;
						raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
						
						raise notice 'l_t_dataset_id - %', l_t_dataset_id ; 
						
											
						l_t_sql_query :=  ' select   tb_col_nm    FROM t_ds_col_detail '
											||' WHERE dataset_id =  '''||l_t_dataset_id ||''''
											||' and lower(source_column)  like ''%position__c'' ';
						raise notice 'l_t_sql_query - %', l_t_sql_query;
						execute l_t_sql_query into l_t_position_id_col;
						get diagnostics AFFECTEDROWS = row_count;
						raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
						
						raise notice 'l_t_position_id_col -> %',l_t_position_id_col;	
										
						l_t_sql_query :=  ' select   tb_col_nm    FROM t_ds_col_detail '
											||' WHERE dataset_id =  '''||l_t_dataset_id ||''''
											||' and lower(source_column)  like ''%id'' ';
						raise notice 'l_t_sql_query - %', l_t_sql_query;
						execute l_t_sql_query into l_t_acc_id_col;
						raise notice 'l_t_acc_id_col -> %',l_t_acc_id_col;	
						
						Execute 'Drop table if exists  t_diff ';
											
						l_t_sql_query :=  ' create table t_diff as  select one.* , two.rule_id ruleid2, two.rule_name rulename2 	from '
											||l_t_output_table||'_one one, '|| l_t_output_table||'_two two where   one.'||l_t_acc_id_col ||' ='|| ' two.'|| l_t_acc_id_col
											|| ' and one.'||l_t_position_id_col ||' = '|| ' two.'||l_t_position_id_col
											|| ' and one.cnt <>  two.cnt ' 	;
											
						raise notice 'Create diff table l_t_sql_query -> %', l_t_sql_query ;
						execute l_t_sql_query ;
						get diagnostics AFFECTEDROWS = row_count;
						raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
						
						
						l_t_sql_query := ' update t_diff set rule_id=ruleid2 , rule_name=rulename2 ';
						
						raise notice 'update the rule id and rule name -> %', l_t_sql_query ;
						execute l_t_sql_query ;
						
						l_t_sql_query := '  delete from '||l_t_output_table||'_one one  using t_diff diff where one.'||l_t_acc_id_col ||' =  diff.'||l_t_acc_id_col
						|| ' and one.'||l_t_position_id_col || ' = diff.'||l_t_position_id_col ; 
						
						raise notice 'delete from table l_t_sql_query -> %', l_t_sql_query;
						execute l_t_sql_query ;
						get diagnostics AFFECTEDROWS = row_count;
						raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
						
						l_t_sql_query := '	insert into '||l_t_output_table||'_one ( '||l_t_col_names ||' , rule_name, rule_id ) ' 
						|| ' select '||l_t_col_names  || ' , rulename2 ,  ruleid2 from t_diff  ';
						
						raise notice 'insert into one l_t_sql_query -> %', l_t_sql_query;
						execute l_t_sql_query ;
						get diagnostics AFFECTEDROWS = row_count;
						raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
						
						l_t_sql_query:= ' delete from '|| l_t_output_table  ;
						
						raise notice 'l_t_sql_query - %',l_t_sql_query;
						execute l_t_sql_query ;
						get diagnostics AFFECTEDROWS = row_count;
						raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
						
						l_t_sql_query := ' insert into '||l_t_output_table   || ' ( '||l_t_col_names ||' , rule_name, rule_id )   select   '||l_t_col_names ||' , rule_name, rule_id   from '|| l_t_output_table||'_one' ; 
						raise notice 'l_t_sql_query - %',l_t_sql_query;
						--return 34;
						execute l_t_sql_query ;
						get diagnostics AFFECTEDROWS = row_count;
						raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
						
					/*	l_t_sql_query:= ' insert into '||l_t_output_table || '( '||l_t_col_names || 
						' , rule_name , rule_id)  select '||l_t_col_names 
						--||   ', string_agg(rule_name, '', '') rule_name  ,string_agg(rule_id, '', '') rule_id  from '
						||   ', string_agg(rule_name, '', '' order by rule_id) rule_name  ,string_agg(rule_id, '', ''order by rule_id) rule_id  from '
						||l_t_output_table ||'_1 group by '||l_t_col_names ;
						--|| ' order by rule_name ';
						raise notice 'l_t_sql_query - %',l_t_sql_query;
						execute l_t_sql_query ;*/
						 
				end if;/* IF 3*/
				/* This is to exclude the product block cases where in the rule_id and rule_name columns are not there */
			/* End New Code*/

	END IF;
	
	--return 8;
	l_t_sql_query :='SELECT   coalesce( selected_positions ,''a'')   FROM t_scenario  WHERE sfdc_id  in (select scenario_id  from t_scn_rule_instance_details  where sfdc_id='''|| l_t_scn_rule_int_id ||''')';
	raise notice 'l_t_sql_query - > %',l_t_sql_query ;
	execute l_t_sql_query into l_selected_position ;
	raise notice 'l_selected_position -> %', l_selected_position;
	if l_selected_position <>'a' then 
			drop table if exists temp_position_list ;
			create temp table temp_position_list (position_id text);
			insert into temp_position_list select regexp_split_to_table( l_selected_position , E',') as position_id ;
			get diagnostics AFFECTEDROWS = row_count;
			raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
			update temp_position_list set position_id =  trim(from position_id);
			get diagnostics AFFECTEDROWS = row_count;
			raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;

			select dataset_id
				from t_dataset_rule_map
			where scenario_rule_instance_id= l_t_scn_rule_int_id 
			and lower(file_type)='base' -- check if correct 
				into l_t_ds;
			raise notice 'l_t_ds - %', l_t_ds;
						 
			SELECT  tb_col_nm  
				FROM t_ds_col_detail 
			WHERE dataset_id =  l_t_ds 
				 --and lower(source_column) in( 'position__c', 'axtriasalesiqtm__position__c')
			and lower(source_column) like '%position__c'
				 INTO c_position_level;
			raise notice 'c_position_level ->%',c_position_level;
			l_t_sql_query := 'delete from '||l_t_output_table || ' where  '|| c_position_level || ' not in (select position_id from  temp_position_list )';
			raise notice 'l_t_sql_query -> %',l_t_sql_query;
			execute l_t_sql_query;
			get diagnostics AFFECTEDROWS = row_count;
			raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;

			--return 9;
	end if;
    
	RETURN 1;
        
EXCEPTION 
	WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS l_t_error = PG_EXCEPTION_CONTEXT;
        
       
        
        select component_type_label, scenario_id from t_scn_rule_instance_details where sfdc_id = l_t_scn_rule_int_id limit 1
        into l_r_comp_label;
        
        if (l_r_comp_label IS NOT NULL) THEN
			INSERT INTO t_error_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id)
				 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,l_r_comp_label.component_type_label,l_r_comp_label.scenario_id,l_t_error,l_t_sql_query,l_t_scn_rule_int_id);
			INSERT INTO t_error_run_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id)
				 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,l_r_comp_label.component_type_label,l_r_comp_label.scenario_id,l_t_error,l_t_sql_query,l_t_scn_rule_int_id);
				 
			Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
						l_log_type_error  , l_log_level_basic , l_run_status_error, 'Error Assignment Rule failed - '||SQLERRM || ' - '||SQLSTATE || ' Query -> '||l_t_sql_query   ||'  ',
						675 , l_t_fn_name) ;
             
      
        ELSE
			INSERT INTO t_error_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id)
				 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,'l_r_comp_label.component_type_label','l_r_comp_label.scenario_id',l_t_error,l_t_sql_query,l_t_scn_rule_int_id );
			INSERT INTO t_error_run_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id)
				 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,'l_r_comp_label.component_type_label','l_r_comp_label.scenario_id',l_t_error,l_t_sql_query,l_t_scn_rule_int_id);
			
			Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
						l_log_type_error  , l_log_level_basic , l_run_status_error, 'Error Assignment Rule failed - '||SQLERRM || ' - '||SQLSTATE || ' Query -> '||l_t_sql_query   ||'  ',
						687 , l_t_fn_name) ;
			
        END IF;
        
        EXECUTE 'UPDATE t_business_rule 
        			SET status = ''Error Occured'' 
                  WHERE status = ''In Progress'' 
                    AND sfdc_id = ''' || l_t_business_rules || '''' ;
        RETURN -1;
END;

$BODY$;

ALTER FUNCTION public.fn_assignment_rule(text, text)
    OWNER TO postgres;
