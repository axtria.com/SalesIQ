-- FUNCTION: public.fn_affiliation_rule(text, text)

-- DROP FUNCTION public.fn_affiliation_rule(text, text);

CREATE OR REPLACE FUNCTION public.fn_affiliation_rule(
	p_t_scn_rule_int_id text,
	p_t_output_table text)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE

l_t_fn_name text := 'fn_affiliation_rule';
l_t_scn_rule_int_id text := p_t_scn_rule_int_id;
l_t_scn_rule_instance RECORD;
l_t_join_sel_expr text;
l_t_join_expr text;
l_t_error text;
l_t_sql_query text := '';
l_t_output_table text := p_t_output_table;
l_t_tmp_output_table text := p_t_output_table || '_1'; --- for hold intermediate affiliation based alignment
l_t_tmp_output_table1 text ; -- temp table for handle duplicate records
l_t_tmp_affiliation_table text ; -- for eligible affiliation based of business rule
l_t_tmp_cust_mas_terr_table0 text; -- for temp customer master with territory
l_t_tmp_cust_mas_terr_table text; -- for customer master with territory
l_t_tmp_block_cust_table text ; -- for blocked customer id
l_t_affiliation_table text;
l_t_affiliation_ds_id text;
l_t_cust_universe_table text;
l_t_cust_universe_ds_id text;
l_t_zip_terr_table text; 
l_t_zip_terr_ds_id text;
l_t_cust_terr_table text;
l_t_cust_terr_ds_id text;
l_t_cust_prod_block_table text;
l_t_cust_prod_block_ds_id text;
l_t_team_prod_table text;
l_t_team_prod_ds_id text;

-- ----------------Column Identifier for processing ----
l_t_child_col_aff text; -- --- Child in Affiliation table
l_t_parent_col_aff text;-- --- Parent in Affiliation table  
l_t_cust_id_col_univ text; -- Cust id Column in Universe 
l_t_zip_col_univ text; -- Zip Column in Universe 
l_t_cust_id_col_ct text; -- -- Cust id in cust-terr
l_t_position_name_ct text; --  position name in cust-terr
l_t_position_ct text; -- - Position/territory in cust-terr
l_t_zip_col_zt text; -- Zip Column in zip terr
l_t_position_zt text; -- Position Column in zip terr
l_t_prd_id_tp text; --  product id from team product
l_t_prd_id_cust_prd_blk text; --  product id from cust product block
l_t_cust_id_cust_prd_blk text; --  cust id from cust product block

l_t_insert_exec_expr text;
l_t_business_rules text;
l_br_table RECORD;
l_r_comp_label RECORD;  
flag integer;
l_n_count integer;
l_t_where_expr text;
l_t_br_stage text;
l_t_rule_type text;
l_t_geo_preference text;
l_t_hierarchy_level text;
l_i_hierarchy_level integer; 
l_c_affli_Parent text := 'parent' ;
l_c_affli_Child text := 'child' ;
l_t_parent_exp text;
l_t_child_exp text;
l_t_parent_table_str text := 'l_t_parent_table' || l_t_scn_rule_int_id;
l_t_child_table_str text := 'l_t_child_table'|| l_t_scn_rule_int_id;
l_c_geo_based_ref text := 'align in geo';
l_c_univ_ref text := 'align in universe';
l_t_elig_affli_query text;
l_t_tmp_query text; -- for execute small temp query
l_t_delete_query text;
l_t_block_query text;
l_t_insert_query text;
l_c_rule_botm_up text := 'bottom up';
l_c_rule_top_down text := 'top down';
l_t_cust_terr_colms text;
l_t_cust_terr_colms_for_rule text ;
l_t_acct_align_type_ct text ;
l_t_aff_based_align_ct text ;
l_i_log_flag integer;
l_t_isshared_flag_ct text ;
l_t_sharedwith_ct text ;
l_t_cust_terr_colms_final text ;
--- Variable for multi level affiliation
l_t_updt_orgin_brnch_aff_qry text;
l_t_insrt_orgn_data_qry text;
l_t_cust_terr_colms_for_origin text ;
l_i_counter integer:= 0; 

l_n_prnt_table_count integer;
l_n_chld_table_count integer;
l_tmp_query text;
l_query_count integer ;
l_n_prdt_count integer;
l_rule_output integer;
l_rule_output_position integer;
l_rule_output_team   integer;
AFFECTEDROWS  integer;
l_t_prod_blck text;
l_t_team_prod_blck text;
l_t_prod_blck_ds_id text;
l_t_team_prod_blck_ds_id text;
l_t_pos_id_cust_prd_blk text;
l_t_prod_id_blck text;
l_t_acc_id_blck text;
l_t_team_prod_blck_1  text;
l_t_prod_blck_1 text;
t_prod_pos_blckng_tbl text;
l_t_output_ds_id  text ; 
l_t_output_acc_id text;
l_t_output_pos_id text;
l_t_output_affil_based_align text ; 
l_t_output_rule_id text :='rule_id'; -- hard coded

--Run Log
l_org_id text :='AA' ;
l_scenario_run_id text :=  p_t_scn_rule_int_id; 
l_log_type text;
l_log_level_basic text := 'Basic' ; 
l_log_level_detailed text := 'Detailed' ; 
l_run_status_in_progress text :='In Progress';
l_run_status_complete text :='Complete';
l_run_status_error text :='Error';
l_line_no text ; 
l_function_name text ;
l_log_type_info text :='Information';
l_log_type_warning text :='Warning';
l_log_type_error text :='Error';
l_input_count integer ;
l_output_count integer ;
p_t_scn_id text;
i_cnt integer;
BEGIN
l_i_log_flag = 0;--1 ;---0;----1;---0;--1;
RAISE NOTICE 'Affiliation start @@@Time ID: %', clock_timestamp();

------ Checkinng Business rule exist for scenario
SELECT COUNT(1)
	FROM t_business_rule 
	WHERE scenario_rule_instance_details = l_t_scn_rule_int_id
	INTO l_n_count;
	
select scenario_id from t_scn_rule_instance_details srid where sfdc_id =l_t_scn_rule_int_id
	into p_t_scn_id ;
	
 /* IF 1*/
 IF (l_n_count = 0) THEN

	-------- NO rule for Affiliation; so assigning cust-terr to final cust terr
	  SELECT DISTINCT drm.table_name
			FROM t_dataset ds, t_dataset_rule_map drm --,t_scn_rule_instance_details srd
			WHERE ds.sfdc_id = drm.dataset_id 
			AND drm.scenario_rule_instance_id = l_t_scn_rule_int_id
			AND LOWER(drm.file_type) = 'cust terr'
			INTO l_t_cust_terr_table;

		l_t_tmp_query := ' drop table if exists ' || l_t_output_table ;
		RAISE NOTICE  'l_t_tmp_query for drop table: % ',	l_t_tmp_query ;
		Execute l_t_tmp_query;
		l_t_tmp_query :=  ' create table ' ||  l_t_output_table || ' as select * from ' || l_t_cust_terr_table  || '';
		RAISE NOTICE  'l_t_tmp_query for create final table: % ',	l_t_tmp_query ;
		--return 44;
		Execute l_t_tmp_query;
		get diagnostics AFFECTEDROWS = row_count;
		raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;

		RAISE NOTICE 'table created';
	  IF(l_i_log_flag = 1) THEN
			INSERT INTO t_log(scenario_id, log, line_no, function_name)
			VALUES (l_t_scn_rule_int_id, 'No Affiliation Rule', '88', l_t_fn_name);            
	  END IF;
	  
/* IF 1*/	  
ELSE ---- Processing for Affiliation as per Business Rule
		RAISE NOTICE 'Affiliation Rule Exists';
		IF(l_i_log_flag = 1) THEN
			INSERT INTO t_log(scenario_id, log, line_no, function_name)
			VALUES (l_t_scn_rule_int_id, 'Affiliation Rule processing start', '96', l_t_fn_name); 
		END IF;
		
		
		SELECT 'Affiliation'::text as name
			FROM t_business_rule br
			WHERE br.scenario_rule_instance_details = l_t_scn_rule_int_id
			INTO l_t_br_stage;
		RAISE NOTICE 'l_t_br_stage -> %',l_t_br_stage;  
		Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
							l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
							'Scenario - Affiliation Rule Name - '||l_t_br_stage||' ' ,
							177 , l_t_fn_name) ; 
		
		
		----############# START BLOCKING - POSITION PRODUCT  ############---------
		SELECT DISTINCT drm.table_name , drm.dataset_id
			FROM t_dataset ds, t_dataset_rule_map drm --,t_scn_rule_instance_details srd
			WHERE ds.sfdc_id = drm.dataset_id 
			AND drm.scenario_rule_instance_id = l_t_scn_rule_int_id
			AND LOWER(drm.file_type) = 'block'
			INTO l_t_prod_blck, l_t_prod_blck_ds_id;
		
		raise notice ' START BLOCKING - POSITION PRODUCT - l_t_prod_blck -> %,l_t_prod_blck_ds_id -> %', l_t_prod_blck ,l_t_prod_blck_ds_id; 
		Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
							l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
							'Scenario - Affiliation Rule Blocking DS Id - '||l_t_prod_blck_ds_id||' - Blocking table name - '||l_t_prod_blck ,
							192 , l_t_fn_name) ; 
		
		SELECT DISTINCT drm.table_name , drm.dataset_id
			FROM t_dataset ds, t_dataset_rule_map drm --,t_scn_rule_instance_details srd
			WHERE ds.sfdc_id = drm.dataset_id 
			AND drm.scenario_rule_instance_id = l_t_scn_rule_int_id
			AND LOWER(drm.file_type) = 'product'
			INTO l_t_team_prod_blck, l_t_team_prod_blck_ds_id ; 
		
		raise notice 'l_t_team_prod_blck-> %, l_t_team_prod_blck_ds_id-> % ',l_t_team_prod_blck , l_t_team_prod_blck_ds_id;
				
		Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
							l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
							'Scenario - Affiliation Rule Team Product DS Id - '||l_t_team_prod_blck_ds_id||' - Team Product table name - '||l_t_team_prod_blck ,
							206 , l_t_fn_name) ; 
		l_t_sql_query := 'SELECT count(0) FROM information_schema.columns WHERE table_name = '''|| l_t_output_table ||''' and column_name in (''rule_id'', ''rule_name'') ';
		raise notice 'l_t_sql_query-> %',l_t_sql_query;
		execute l_t_sql_query into  i_cnt ;
		raise notice 'V3 template or V5 Template -> i_cnt -> %',i_cnt;

		
		SELECT DISTINCT drm.dataset_id,drm.table_name
        FROM t_dataset ds, t_dataset_rule_map drm --,t_scn_rule_instance_details srd
        WHERE ds.sfdc_id = drm.dataset_id 
        --AND drm.scenario_rule_instance_id = srd.sfdc_id
        --AND srd.scenario_id = l_t_scn_rule_int_id
        AND drm.scenario_rule_instance_id = l_t_scn_rule_int_id
        AND LOWER(drm.file_type) = 'cust terr'
        INTO l_t_cust_terr_ds_id,l_t_cust_terr_table;
		
		Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
							l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
							'Scenario - Affiliation Rule Cust Terr DS Id - '||l_t_team_prod_blck_ds_id||' - Cust Terr table name - '||l_t_team_prod_blck ,
							220 , l_t_fn_name) ; 
		
		SELECT DISTINCT drm.dataset_id 
        FROM t_dataset ds, t_dataset_rule_map drm --,t_scn_rule_instance_details srd
        WHERE ds.sfdc_id = drm.dataset_id 
        --AND drm.scenario_rule_instance_id = srd.sfdc_id
        --AND srd.scenario_id = l_t_scn_rule_int_id
        AND drm.scenario_rule_instance_id = l_t_scn_rule_int_id
        AND LOWER(drm.ds_type) = 'o'
        INTO l_t_output_ds_id ;
		
		Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
							l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
							'Scenario - Affiliation Rule Output DS Id - '||l_t_team_prod_blck_ds_id||' ' ,
							234 , l_t_fn_name) ;
		 
		select tb_col_nm from t_ds_col_detail where dataset_id = l_t_output_ds_id and (lower(source_column) = 'axtriasalesiqtm__account__c' or LOWER(source_column) = 'account__c') into l_t_output_acc_id;
		select tb_col_nm from t_ds_col_detail where dataset_id = l_t_output_ds_id and (lower(source_column) = 'axtriasalesiqtm__position__c' or LOWER(source_column) = 'position__c') into l_t_output_pos_id;
		select tb_col_nm from t_ds_col_detail where dataset_id = l_t_output_ds_id and (lower(source_column) = 'axtriasalesiqtm__affiliation_based_alignment__c' or LOWER(source_column) = 'affiliation_based_alignment__c') into l_t_output_affil_based_align;
		raise notice 'l_t_output_acc_id -> % , l_t_output_pos_id -> % , l_t_output_affil_based_align - > %, l_t_output_rule_id -> %' , l_t_output_acc_id , l_t_output_pos_id  , l_t_output_affil_based_align , l_t_output_rule_id ;
		 
		raise notice 'l_t_output_table -> %',l_t_output_table; 
		raise notice 'l_t_cust_terr_table -> %',l_t_cust_terr_table; 
		EXECUTE 'drop table if exists ' ||	 l_t_output_table ;
		raise notice 'Line 264';
		
		EXECUTE 'create table ' || l_t_output_table || ' as select * from ' || l_t_cust_terr_table || ' limit 0 ';
		
		raise notice 'Line 266';
		Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
							l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
							'Scenario - Affiliation Rule   Acc ID - '||l_t_output_acc_id||' Output Pos Id- '||l_t_output_pos_id||' l_t_output_affil_based_align -'|| l_t_output_affil_based_align ,
							247 , l_t_fn_name) ;
		raise notice 'Line 270';
		select tb_col_nm from t_ds_col_detail where dataset_id = l_t_team_prod_blck_ds_id and (lower(source_column) = 'axtriasalesiqtm__product_master__c' or LOWER(source_column) = 'product_master__c') into l_t_prd_id_cust_prd_blk;
		raise notice 'Line 273';
		select tb_col_nm from t_ds_col_detail where dataset_id = l_t_team_prod_blck_ds_id and (lower(source_column) = 'axtriasalesiqtm__position__c' or LOWER(source_column) = 'position__c') into l_t_pos_id_cust_prd_blk;
		raise notice 'Line 275';
		raise notice 'l_t_prd_id_cust_prd_blk-> %, l_t_pos_id_cust_prd_blk -> %', l_t_prd_id_cust_prd_blk, l_t_pos_id_cust_prd_blk ;
		
		Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
							l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
							'Scenario - Affiliation Rule   - l_t_prd_id_cust_prd_blk- '||l_t_prd_id_cust_prd_blk||'- l_t_pos_id_cust_prd_blk- '||l_t_pos_id_cust_prd_blk ,
							257 , l_t_fn_name) ;
		
		select tb_col_nm from t_ds_col_detail where dataset_id = l_t_prod_blck_ds_id and (lower(source_column) = 'axtriasalesiqtm__product_master__c' or LOWER(source_column) = 'product_master__c') into  l_t_prod_id_blck;
		select tb_col_nm from t_ds_col_detail where dataset_id = l_t_prod_blck_ds_id and (lower(source_column) = 'axtriasalesiqtm__account__c' or LOWER(source_column) = 'account__c') into  l_t_acc_id_blck;
		
		raise notice 'l_t_prod_id_blck - > %, l_t_acc_id_blck -> % ', l_t_prod_id_blck , l_t_acc_id_blck ;
		
		Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
							l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
							'Scenario - Affiliation Rule   - l_t_prod_id_blck- '||l_t_prod_id_blck||'- l_t_acc_id_blck- '||l_t_acc_id_blck , 268 , l_t_fn_name) ;
							
		if l_t_pos_id_cust_prd_blk is not null then 
				l_t_team_prod_blck_1 := l_t_team_prod_blck ||'_1';
				Execute 'Drop table if exists '||l_t_team_prod_blck_1 ;
				l_t_sql_query := 'Create table '|| l_t_team_prod_blck_1  ||' as select '||l_t_pos_id_cust_prd_blk ||' , ' || ' string_agg(' || l_t_prd_id_cust_prd_blk || ' ,''%'' order by '|| l_t_prd_id_cust_prd_blk ||' ) '||l_t_prd_id_cust_prd_blk || ' from ' || l_t_team_prod_blck ||' group by ' || l_t_pos_id_cust_prd_blk ;
				
				raise notice ' l_t_sql_query1 -> %',l_t_sql_query;
				Execute l_t_sql_query ;
				get diagnostics AFFECTEDROWS = row_count;
				raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
				
				l_t_prod_blck_1 := l_t_prod_blck ||'_1';
				Execute ' Drop table if exists  '||l_t_prod_blck_1 ;
				l_t_sql_query := 'Create table '||l_t_prod_blck_1 ||' as select '||l_t_acc_id_blck ||' , ' || 'string_agg(' || l_t_prod_id_blck || ' ,''%'' order by '|| l_t_prod_id_blck ||' ) '||l_t_prod_id_blck || ' from ' || l_t_prod_blck ||' group by ' || l_t_acc_id_blck ;
				
				raise notice ' l_t_sql_query2 -> %',l_t_sql_query;
				raise notice 'l_t_prod_blck_1 -> %',l_t_prod_blck_1;
				raise notice ' l_t_acc_id_blck -> %',l_t_acc_id_blck ; 
				raise notice 'l_t_prod_id_blck-> %', l_t_prod_id_blck ; 
				raise notice ' l_t_prod_blck -> %',l_t_prod_blck ; 
				
				--return 77;
				Execute l_t_sql_query ;
				--return 7;
				get diagnostics AFFECTEDROWS = row_count;
				raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
				
				t_prod_pos_blckng_tbl :='t_position_product_block';
				Execute ' Drop Table if exists '||t_prod_pos_blckng_tbl;
			--	l_t_sql_query := ' Create table '||t_prod_pos_blckng_tbl||' as select w1.accountnumber_cust_blck, w1.product, w2.position_id , w2.product  from x_cust_prod w1 join      x_team_prod w2    on w1.product like ''%'' || w2.product || ''%''  ';	
				
			--	l_t_sql_query := ' Create table '||t_prod_pos_blckng_tbl||' as select w1.'||l_t_acc_id_blck||' , w1.'||l_t_prod_id_blck||' , w2.'||l_t_pos_id_cust_prd_blk || ' , w2.'||l_t_prd_id_cust_prd_blk  ||'  from '||l_t_prod_blck_1 || ' w1 join  '||    l_t_team_prod_blck_1 || ' w2    on w1.'||l_t_prod_id_blck || '  like ''%''||w2.'||l_t_prd_id_cust_prd_blk || '||''%''  ';	
			--l_t_sql_query := ' Create table '||t_prod_pos_blckng_tbl||' as select w1.'||l_t_acc_id_blck||' , w1.'||l_t_prod_id_blck||' , w2.'||l_t_pos_id_cust_prd_blk || ' , w2.'||l_t_prd_id_cust_prd_blk  ||'  from '||l_t_prod_blck_1 || ' w1 join  '||    l_t_team_prod_blck_1 || ' w2    on w2.'||l_t_prd_id_cust_prd_blk || '  like ''%''||w1.'||l_t_prod_id_blck || '||''%''  ';	
			
			l_t_sql_query := ' Create table '||t_prod_pos_blckng_tbl||' as select w1.'||l_t_acc_id_blck||' , w1.'||l_t_prod_id_blck||' , w2.'||l_t_pos_id_cust_prd_blk || ' , w2.'||l_t_prd_id_cust_prd_blk  ||'  from '||l_t_prod_blck_1 || ' w1 join  '||    l_t_team_prod_blck_1 || ' w2    on (w1.'||l_t_prod_id_blck || '  like ''%''||w2.'||l_t_prd_id_cust_prd_blk || '||''%'' or  w1.'||l_t_prod_id_blck || ' =  w2.'||l_t_prd_id_cust_prd_blk || '  ) ';

				raise notice 'l_t_sql_query3 ->  %',l_t_sql_query;
				--return 8;
				Execute l_t_sql_query ;
				get diagnostics AFFECTEDROWS = row_count;
				raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
				---- this table to be used while fetching multilevel rule in affiliation -- to exclude these accounts
				--return 9;
		end if;
		
		----############# END BLOCKING - POSITION PRODUCT  ############---------
		FOR l_br_table IN 
	     	SELECT sfdc_id ,  coalesce(upper(rule_level),'a')  rule_level , selected_positions  
			FROM t_business_rule			
			WHERE scenario_rule_instance_details = l_t_scn_rule_int_id
		LOOP
			RAISE NOTICE 'START business_rule - sfdcid - %', l_br_table.sfdc_id ;
			RAISE NOTICE 'START l_br_table.rule_level - %', l_br_table.rule_level;
			Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
							l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
							'Scenario - Affiliation Rule   - SFDC ID- '||l_br_table.sfdc_id||'- Rule Level- '||l_br_table.rule_level , 323 , l_t_fn_name) ;
			
			/*IF 2*/
			IF l_br_table.rule_level in ( 'POSITIONS', 'POSITION')  then --- POSITION LEVEL CHANGES 
					RAISE NOTICE 'POSITION LEVEL RULE EXECUTION START ';
					raise notice 'l_t_scn_rule_int_id-> %',l_t_scn_rule_int_id;
					raise notice 'l_br_table.sfdc_id -> %', l_br_table.sfdc_id;
					raise notice 'l_t_output_table -> %', l_t_output_table;
					raise notice 'l_br_table.selected_positions -> %',l_br_table.selected_positions;
					Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
							l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
							'Scenario - Affiliation Rule  Rule- SFDC ID- '||l_br_table.sfdc_id||'- Rule Level- '||l_br_table.rule_level ||' - Positions -'||l_br_table.selected_positions  , 334 , l_t_fn_name) ;
					SELECT fn_affiliation_rule_position_level_rule (
												l_t_scn_rule_int_id ,
												l_br_table.sfdc_id  ,
												l_t_output_table ,	
												l_br_table.selected_positions) INTO  l_rule_output_position;
					raise notice 'POSITION LEVEL RULE EXECUTION END ';
					raise notice 'l_rule_output_position - > % ' ,l_rule_output_position;
					if l_rule_output_position <> 1 then 
						EXECUTE 'UPDATE t_business_rule 
											SET status = ''Error'' 
										  WHERE status = ''In Progress'' 
											AND sfdc_id = ''' || l_br_table.sfdc_id || '''' ;
						get diagnostics AFFECTEDROWS = row_count;
						raise notice 'update business rule to success AFFECTEDROWS -%', AFFECTEDROWS;
						return -1;
					else 
						EXECUTE 'UPDATE t_business_rule 
											SET status = ''Success'' 
										  WHERE status = ''In Progress'' 
											AND sfdc_id = ''' || l_br_table.sfdc_id || '''' ;
						get diagnostics AFFECTEDROWS = row_count;
						raise notice 'update business rule to success AFFECTEDROWS -%', AFFECTEDROWS;
					end if;
			/*IF 2*/
			ELSE -- Team Level Rule
					RAISE NOTICE 'TEAM LEVEL RULE EXECUTION START ';
					Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
							l_log_type_info  , l_log_level_basic , l_run_status_in_progress,
							'Scenario - Affiliation Rule  Rule- SFDC ID- '||l_br_table.sfdc_id||'- Rule Level- '||l_br_table.rule_level   , 334 , l_t_fn_name) ;
					SELECT fn_affiliation_rule_team_level_rule (
												l_t_scn_rule_int_id ,
												l_br_table.sfdc_id  ,
												l_t_output_table  ) INTO  l_rule_output_team;
					RAISE NOTICE 'TEAM LEVEL RULE EXECUTION END ';
					raise notice 'l_rule_output_position - > % ' ,l_rule_output_position;
					if l_rule_output_team <> 1 then 
						EXECUTE 'UPDATE t_business_rule 
											SET status = ''Error'' 
										  WHERE status = ''In Progress'' 
											AND sfdc_id = ''' || l_br_table.sfdc_id || '''' ;
						get diagnostics AFFECTEDROWS = row_count;
						raise notice 'update business rule to success AFFECTEDROWS -%', AFFECTEDROWS;
						return -1;
					else 
						EXECUTE 'UPDATE t_business_rule 
											SET status = ''Success'' 
										  WHERE status = ''In Progress'' 
											AND sfdc_id = ''' || l_br_table.sfdc_id || '''' ;
						get diagnostics AFFECTEDROWS = row_count;
						raise notice 'update business rule to success AFFECTEDROWS -%', AFFECTEDROWS;
					end if;
			/*IF 2*/
			End If;
			RAISE NOTICE 'END business_rule - sfdcid - %', l_br_table.sfdc_id ;
			RAISE NOTICE 'END l_br_table.rule_level - %', l_br_table.rule_level;
	    End Loop;
		--- Delete Duplicates in Cust Terr
		l_t_sql_query := ' DELETE FROM ' || l_t_output_table ||' T1 USING '|| l_t_output_table || 
			' T2  WHERE   T1.ctid < T2.ctid AND  T1.'||l_t_output_acc_id || ' =  T2.' ||l_t_output_acc_id ||
        	' AND  T1.'||l_t_output_pos_id || ' =  T2.' ||l_t_output_pos_id || 
			' AND  T1.'||l_t_output_affil_based_align || ' =  T2.' ||l_t_output_affil_based_align ;
			if i_cnt =2 then		
			   l_t_sql_query := l_t_sql_query ||		' AND  T1.'||l_t_output_rule_id || ' =  T2.' ||l_t_output_rule_id;
			end if;	
	
	raise notice 'l_t_output_table -> %', l_t_output_table;
	raise notice 'l_t_output_acc_id -> %',l_t_output_acc_id;
	raise notice 'l_t_output_pos_id -> %', l_t_output_pos_id;
	raise notice 'l_t_output_affil_based_align-> %',l_t_output_affil_based_align;
	raise notice 'l_t_output_rule_id-> %',l_t_output_rule_id ;
	raise notice 'Delete Duplicate from Cust Terr - l_t_sql_query-> %', l_t_sql_query;
	execute l_t_sql_query;
	get diagnostics AFFECTEDROWS = row_count;
	raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
	
	l_t_sql_query := ' Update ' || l_t_output_table ||' T1 set '||l_t_output_affil_based_align ||' = ''False'' where '|| l_t_output_affil_based_align|| ' in (''false'',''FALSE'',''False'') ';
	raise notice 'Change case of affil based flag to initcase - > %',l_t_sql_query ;
	execute l_t_sql_query;
	get diagnostics AFFECTEDROWS = row_count;
	raise notice ' AFFECTEDROWS -%', AFFECTEDROWS;
		
/* IF 1*/			
End If;	 

RETURN 1;
        
EXCEPTION 
	WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS l_t_error = PG_EXCEPTION_CONTEXT;
        
       
        
        select component_type_label, scenario_id from t_scn_rule_instance_details where sfdc_id = l_t_scn_rule_int_id limit 1
        into l_r_comp_label;
        
        if (l_r_comp_label IS NOT NULL) THEN
			INSERT INTO t_error_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
				 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,l_r_comp_label.component_type_label,l_r_comp_label.scenario_id,l_t_error,l_t_sql_query,l_t_scn_rule_int_id,l_t_br_stage);
			INSERT INTO t_error_run_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
				 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,l_r_comp_label.component_type_label,l_r_comp_label.scenario_id,l_t_error,l_t_sql_query,l_t_scn_rule_int_id,l_t_br_stage);
            
			/*Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
						l_log_type_error  , l_log_level_basic , l_run_status_error, 'Error Affiliation Rule failed - '||SQLERRM || ' - '||SQLSTATE || ' Query -> '||l_t_sql_query   ||'  ',
						451 , l_t_fn_name) ;*/
      
        ELSE
			INSERT INTO t_error_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
				 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,'l_r_comp_label.component_type_label','l_r_comp_label.scenario_id',l_t_error,l_t_sql_query,l_t_scn_rule_int_id,l_t_br_stage );
			INSERT INTO t_error_run_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,scenario_rule_instance_id,srid_stage)
				 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,l_t_output_table,'l_r_comp_label.component_type_label','l_r_comp_label.scenario_id',l_t_error,l_t_sql_query,l_t_scn_rule_int_id,l_t_br_stage);
				 
			/*Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
							l_log_type_error  , l_log_level_basic , l_run_status_error, 'Error Affiliation Rule failed - '||SQLERRM || ' - '||SQLSTATE || ' Query -> '||l_t_sql_query   ||'  ',
							462 , l_t_fn_name) ;*/
        
        END IF;
        
        EXECUTE 'UPDATE t_business_rule 
        			SET status = ''Error Occured'' 
                  WHERE status = ''In Progress'' 
                    AND sfdc_id = ''' || l_t_business_rules || '''' ;
                   
        RETURN -1;
END;

$BODY$;

ALTER FUNCTION public.fn_affiliation_rule(text, text)
    OWNER TO postgres;

GRANT EXECUTE ON FUNCTION public.fn_affiliation_rule(text, text) TO postgres;

GRANT EXECUTE ON FUNCTION public.fn_affiliation_rule(text, text) TO PUBLIC;

