import logging
import zipfile
import os

from main.copyscenario import CopyScenario
from glob import glob
from flask import Flask, request, send_from_directory
from logging.handlers import TimedRotatingFileHandler

app = Flask(__name__)
PREFIX = '/copy_scenario'


@app.route(PREFIX)
def copy_scenario():
    source = request.args.get('source', default = '', type=str)
    issourcemanaged = request.args.get('issourcemanaged', default='False', type=str)
    issourcemanaged = True if issourcemanaged.lower() == 'true' else 'False'
    sourceteaminstanceid = request.args.get('sourceteaminstanceid', type=str)

    target = request.args.get('target', default='', type=str)
    istargetmanaged = request.args.get('istargetmanaged', default='False', type=str)
    istargetmanaged = True if issourcemanaged.lower() == 'true' else 'False'
    targetteaminstanceid = request.args.get('targetteaminstanceid', type=str)

    workspaceid= request.args.get('workspaceid', type=str)
    cpy_scnro = CopyScenario(source, issourcemanaged, sourceteaminstanceid, target, istargetmanaged, targetteaminstanceid,workspaceid)

    return cpy_scnro.invoke()

@app.before_request
def log_request_info():
    logging.debug('Request ::: %r' % request.url)

logging.getLogger('requests').setLevel(logging.CRITICAL)
logging.getLogger('urllib3').setLevel(logging.CRITICAL)


if __name__ == '__main__':
    app.debug = True
    logging.basicConfig(level=logging.DEBUG,
                        handlers=[TimedRotatingFileHandler("logs//system.log", when="midnight"),
                                  logging.StreamHandler()],
                        format='%(asctime)s - %(module)s.%(funcName)s() - %(levelname)s - %(message)s')
    app.run()