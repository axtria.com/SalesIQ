query = {
    "team_instance_name" : {
        "object_name" : "Team_Instance__c",
        "query" : "SELECT Name FROM Team_Instance__c WHERE ID = '{}'"
    },

    "team_instance_setting": {
        "object_name" : "Team_Instance__c",
        "query" : "SELECT AccountMovement__c, Account_Shared_Position_Threshold__c, Affiliation_Network__c, Alignment_Period__c, Alignment_Type__c, Allow_Account_Exclusion__c, Allow_Submission_Pending__c, Allow_Unassigned_Positions__c, Base_team_Instance__c, Base_Team__c, CIM_Available__c, Converging_Level__c, Country__c, Team__r.Country__r.Name, Create_Mirror_Position__c, Create_Overlay_Position__c, DM_End_Date__c, doContiguityCheck__c, doDoughnutCheck__c, EnableAccountSharing__c, Enable_Affiliation_Movement__c, Geography_Type_Name__c, Geography_Type_Name__r.Name, IC_EffEndDate__c, IC_EffstartDate__c, Id, isActiveCycle__c, IsAllowedManageAssignment__c, IsCascadeDeleteAssignments__c, Name, POA_EffEndDate__c, POA_EffStartDate__c, ReadOnlyCreatePosition__c, ReadOnlyDeletePosition__c, ReadOnlyEditPosition__c, Rep_End_Date__c, Restrict_Account_Affiliation__c, Restrict_Hierarchy_Change__c, Restrict_Update_Position_Attribute__c, Restrict_ZIP_Share__c, Scenario__c, Send_Back_to_Revision__c, Shared_Position_Threshold__c, Show_Custom_Metric__c, Show_Map__c, Team_Cycle_Name__c, Team_Instance_Code__c, Team__c, Team__r.Name, ZIPMovement__c FROM Team_Instance__c WHERE Id = '{}'"
    },

    "country_id_fetch": {
        "object_name" : "Country__c",
        "query" : "SELECT Id, Name FROM Country__c"
    },

    "team_id_fetch": {
        "object_name" : "Team__c",
        "query" : "SELECT Id, Name FROM Team__c"
    },

    "geo_type_id_fetch": {
        "object_name" : "Geography_Type__c",
        "query" : "SELECT Id, Name FROM Geography_Type__c"
    },

    "scenario_setting": {
        "object_name" : "Scenario__c",
        "query" : "SELECT Alignment_Type__c,Approval_Config__c,Business_Rule_Template__c,Country__c,CR_Tracking__c,Data_Object__c,Description__c,Effective_End_Date__c,Effective_Start_Date__c,Employee_Assignment_Date__c,Employee_Assignment__c,Enable_Affiliation_Based_Alignment__c,File_Path__c,Id,IsDeleted,isUniversal__c,Is_Locked__c,Last_Executed_By__c,Last_Promote_Success_Date__c,Last_Run_Date__c,Last_Sync_Start_Date__c,Last_Sync_Success_Date__c,Name,Promote_Mode__c,RecordTypeId,Request_Process_Stage__c,Rule_Execution_Status__c,Run_Response_Details__c,Scenario_Alignment_Level__c,Scenario_Name_Formula__c,Scenario_Name__c,Scenario_Source__c,Scenario_Stage__c,Scenario_Status__c,Scenario_Type__c,Send_To_CRM__c,Shared__c,Source_Team_Instance__c,Team_Instance__c,Team_Name__c,Team_Type__c,Workspace__c FROM Scenario__c WHERE Team_Instance__c = '{}'"
    },

    "recordtype_id_fetch": {
        "object_name": "RecordType",
        "query": "SELECT Id, Name FROM RecordType"
    },

    "team_inst_obj_attr_setting": {
        "object_name": "Team_Instance_Object_Attribute__c",
        "query": "SELECT Allow_External_Call__c, Attribute_API_Name__c, Attribute_Display_Name__c, Comments__c, Data_Type__c, Default_Sort__c, DisplayOn__c, Display_Column_Order__c, Display_Order_Account_Universe__c, Enabled_Advance_Search__c, Field_Relationship__c, Id, Interface_Name__c, IsAutoSuggested__c, IsDeleted, IsEditable_Account_Universe__c, isEditable__c, IsEnabled_Account_Universe__c, isEnabled__c, isRequired__c, isStatic__c, Lookup_Detail_String__c, Lookup_Object__c, Name, Object_Name__c, OwnerId, Secondary_Sort__c, Section__c, Team_Instance__c, Width__c, WrapperFieldMap__c FROM Team_Instance_Object_Attribute__c WHERE Team_Instance__c = '{}'"
    },

    "CIM_config_setting": {
        "object_name": "CIM_Config__c",
        "query": "SELECT Acceptable_Max__c, Acceptable_Min__c, Aggregation_Attribute_API_Name__c, Aggregation_Condition_Attribute_API_Name__c, Aggregation_Condition_Attribute_Value__c, Aggregation_Object_Name__c, Aggregation_Type__c, Attribute_API_Name__c, Attribute_Display_Name__c, Change_Request_Type__c, CR_Type_Name__c, Default_On__c, Display_Column_Order__c, Display_Name__c, Enable__c, Enforcable__c, Error_Message__c, Hierarchy_Level__c, Id, IsDeleted, isOptimum__c, Is_Custom_Metric__c, LookupCIMParent__c, MetricCalculationType__c, Metric_Name__c, Name, Object_Name__c, Roll_Up_Summary_Column__c, Source_Territory_CIM__c, Team_Instance_Object_Attribute__c, Team_Instance__c, Threshold_Max__c, Threshold_Min__c, Threshold_Warning_Max__c, Threshold_Warning_Min__c, Tooltip_Message__c, Visible_On_HomePage__c, Visible__c FROM CIM_Config__c WHERE Team_Instance__c = '{}'"

    },

    "Change_request_id_fetch":{
        "object_name": "Change_Request_Type__c",
        "query": "SELECT Id, Name FROM Change_Request_Type__c"
    },

    "team_instance_config_setting":{
        "object_name": "Team_Instance_Configuration__c",
        "query": "SELECT Configuration_Type__c, Hierarchy_Level__c, Id, IsDeleted, Name, Pattern_Value__c, Position_Type__c, Team_Instance__c, Team__c FROM Team_Instance_Configuration__c WHERE Team_Instance__c = '{}'"
    },

    "position_setting":{
        "object_name": "Position__c",
        "query": "SELECT Alignment_Index__c, Assignment_status__c, Calls__c, Call_Index__c, Centroid_Lattitude__c, Centroid_Longitude__c, Centroid__c, Centroid__Latitude__s, Centroid__Longitude__s, Change_Status_del__c, Client_Position_Code__c, Client_Territory_Code__c, Client_Territory_Name__c, Description__c, Effective_End_Date__c, Effective_Start_Date__c, Eligibility__c, Employee_p__c, Employee__c, Goals__c, Hierarchy_Level__c, Id, inactive__c, isCallPlanSubmitted__c, IsDeleted, isDeleted__c, isGeoBased__c, IsMaster__c, isRuleBased__c, Is_Unassigned_Position__c, LastResetToOriginalBy__c, LastResetToOriginalDate__c, Level_5_Position__c, MultiSelect__c, Name, No_of_Target__c, Parent_Position_Code__c, Parent_Position__c, Position_Category__c, Position_ID__c, Position_Status__c, Position_Type__c, Related_Position_Type__c, RGB__c, Role__c, Sales__c, Sale_Index__c, Team_iD__c, Team_Instance__c, Vacant_Since__c, Veeva_Territory_ID__c, X_Max__c, X_Min__c, Y_Max__c, Y_Min__c FROM Position__c WHERE Team_Instance__c = '{}'"
    },

    "position_team_instance_setting":{
        "object_name": "Position_Team_Instance__c",
        "query": "SELECT Centroid_Latitude__c, Centroid_Lattitude__c, Centroid_Longitude__c, Effective_End_Date__c, Effective_Start_Date__c, Employee_ID__c, Geography_String__c, Id, isCallPlanLocked__c, isCallPlanSubmitted__c, IsDeleted, isGeo__c, Metric1__c, Name, Parent_Position_ID__c, Position_ID__c, RGB__c, Team_Instance_ID__c, X_Max__c, X_Min__c, Y_Max__c, Y_Min__c FROM Position_Team_Instance__c WHERE Team_Instance_ID__c = '{}'"
    },

    "position_account_setting":{
        "object_name": "Position_Account__c",
        "query": "SELECT Account_Alignment_Type__c, Account_Relationship_Type__c, Account_Target_Type__c, Account__c, Affiliation_Based_Alignment__c, Assignment_Status__c, Change_Status__c, Comments__c, Effective_End_Date__c, Effective_Start_Date__c, Id, IsDeleted, isExcluded__c, IsShared__c, Locked__c, logged__c, Metric1__c, Metric2__c, Metric3__c, Metric4__c, Metric5__c, Metric6__c, Metric7__c, Metric8__c, Metric9__c, Metric10__c, Metric_1__c, Name, Position_Team_Instance__c, Position__c, Proposed_Position__c, Segment_1__c, SharedWith__c, Team_Instance_Account__c, Team_Instance__c, TempDestinationrgb__c FROM Position_Account__c WHERE Team_Instance__c = '{}'"
    },

    "Account_id_fetch":{
        "object_name": "Account",
        "query": "SELECT Id, AccountNumber FROM Account"
    },

    "CR_team_instance_config_setting":{
        "object_name": "CR_Team_Instance_Config__c",
        "query": "SELECT Bulk_Edit_Allowed__c, Change_Request_Types__c, Comments__c, Configuration_Name__c, Configuration_Type__c, Configuration_Value__c, Config_Value__c, Create__c, Delete__c, Edit__c, Fixed_Column_From_Left__c, Fixed_Column_From_Right__c, Hierarchy_Change__c, Id, IsDeleted, Name, Page_Size__c, Pagination_on__c, Review_Allowed__c, Team_Instance__c, Update_Position_Attributes__c FROM CR_Team_Instance_Config__c WHERE Team_Instance__c = '{}'"
    },

    "position_employee_setting": {
        "object_name":"Position_Employee__c",
        "query": "SELECT Assignment_Status__c, Assignment_Type__c, Centroid_Distance_Emp_Pos__c, Change_Status__c, Comments__c, Effective_End_Date__c, Effective_Start_Date__c, Employee_Centroid_Lat__c, Employee_Centroid_Long__c, Employee__c, Id, IsDeleted, IsNew__c, Last_Approved_Assignment_Type__c, Last_Approved_End_Date__c, Last_Approved_Start_Date__c, Name, Position_Centroid_Lat__c, Position_Centroid_Long__c, Position__c, Reason_Code__c, RecordTypeId, Status__c, TotalHospital__c FROM Position_Employee__c"
    },

    "Employee_fetch_id":{
        "object_name": "Employee__c",
        "query": "Select Id, Name FROM Employee__c"
    },

    "position_geography_setting": {
        "object_name": "Position_Geography__c",
        "query": "SELECT Assignment_Status__c, Change_Status__c, Effective_End_Date__c, Effective_Start_Date__c, Geography__c, Id, IsDeleted, IsShared__c, Locked__c, logged__c, Metric1__c, Metric2__c, Metric3__c, Metric4__c, Metric5__c, Metric6__c, Metric7__c, Metric8__c, Metric9__c, Metric10__c, Name, Position_Id_External__c, Position_Team_Instance__c, Position__c, Proposed_Position__c, Restrict_Position_Geography_Trigger__c, SharedWith__c, SystemModstamp, Team_Instance_Geography__c, Team_Instance__c, TempDestinationrgb__c FROM Position_Geography__c WHERE Team_Instance__c = '{}'"
    },

    "team_inst_obj_attr_details_setting":{
        "object_name": "Team_Instance_Object_Attribute_Detail__c",
        "query": "SELECT Id, isActive__c, IsDeleted, Lookup_Detail_String__c, Name, Object_Attibute_Team_Instance__c, Object_Value_Name_Range__c, Object_Value_Name__c, Object_Value_Seq__c, Team_Instance__c FROM Team_Instance_Object_Attribute_Detail__c WHERE Team_Instance__c = '{}'"
    },

    "cr_approval_config_setting":{
       "object_name": "CR_Approval_Config__c",
        "query": "SELECT Change_Request_Type__c, EsclationFlag__c, Esclation_After__c, Esclation_Period__c, HO_Office_User__c, HR_User__c, Id, IsDeleted, isFinalApprover__c, isHigherLevelManagers__c, isHigherLevelOverride__c, isImpactedManagerOverride__c, isImpactedManager__c, IsImpactedPosition__c, Is_Auto_Approve__c, Name, Team_Instance_ID__c FROM CR_Approval_Config__c WHERE Team_Instance_ID__c = '{}'"
    }
}