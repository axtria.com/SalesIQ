import logging
from main.salesforce import SalesforceConnector
from pandas import DataFrame, merge
from config import config
from .query import query

class CopyScenario():
    def __init__(self, source:str, issourcemanaged:bool, sourceteaminstanceid:str, target:str, istargetmanaged:bool, targetteaminstanceid:str, workspaceid:str):
        self.source = source
        self.issourcemanaged = issourcemanaged
        self.source_team_inst_id = sourceteaminstanceid
        self.target = target
        self.istargetmanaged = istargetmanaged
        self.target_team_inst_id = targetteaminstanceid
        self.workspace_id = workspaceid

        self.source_namespace = "AxtriaSalesIQTM__" if issourcemanaged == True else ""
        self.target_namespace = "AxtriaSalesIQTM__" if istargetmanaged == True else ""

        source_sf_dict= config[self.source]
        source_sf_dict.update({'namespace': self.source_namespace})
        self.source_conn = SalesforceConnector(**source_sf_dict)

        target_sf_dict = config[self.target]
        target_sf_dict.update({'namespace': self.target_namespace})
        self.target_conn = SalesforceConnector(**target_sf_dict)

        self.fetched_records = {}

    def invoke(self):

        #checking if team instance id exist in the org.
        try:
            source_scenario_name = self.source_conn.fetch_records(query["team_instance_name"]["object_name"], query["team_instance_name"]["query"].format(self.source_team_inst_id))
        except:
            return "Team Instance Id does not exist in " + self.source

        #making team instance for target org with same name as source org.
        country_mapping, team_mapping, geography_type_mapping = self.copy_team_instance()
        recordtype_mapping = self.copy_scenario(country_mapping, team_mapping)
        change_request_type_mapping = self.copy_CIM_Config()
        self.copy_team_instance_config(team_mapping)

        source_position_setting = self.copy_position(team_mapping)
        self.copy_position_employee(source_position_setting)
        self.copy_position_team_instance()
        account_mapping = self.copy_position_account()
        self.copy_position_geography(geography_type_mapping)
        self.copy_team_instance_object_attribute_details()
        self.copy_cr_approval_config(change_request_type_mapping)
        return 'Successfully copied'

    def copy_team_instance(self):
        #
        # Fetching team instance record from source
        #
        source_team_instance_setting = self.source_conn.fetch_records(query["team_instance_setting"]["object_name"],query["team_instance_setting"]["query"].format(self.source_team_inst_id))

        #
        # Finding Country__c Mapping
        #
        source_country = self.source_conn.fetch_records(query['country_id_fetch']['object_name'], query['country_id_fetch']['query'])
        target_country = self.target_conn.fetch_records(query['country_id_fetch']['object_name'], query['country_id_fetch']['query'])
        country_mapping = merge(target_country, source_country, how='left', on='Name')

        # Replace Country__c value before upload
        temp = merge(source_team_instance_setting[['Country__c']], country_mapping[['Id_x', 'Id_y']], how='left', left_on='Country__c', right_on='Id_y')
        source_team_instance_setting['Country__c'] = temp['Id_x']

        # Checking if Country__c exist in target org.
        if source_team_instance_setting['Country__c'].isna().any():
            return "Country__c: {} does not exist in the target org".format(temp['Country__c'][0])

        #
        # Finding Team__c Mapping
        #
        source_team = self.source_conn.fetch_records(query['team_id_fetch']['object_name'], query['team_id_fetch']['query'])
        target_team = self.target_conn.fetch_records(query['team_id_fetch']['object_name'], query['team_id_fetch']['query'])
        team_mapping = merge(target_team, source_team, how='left', on='Name')

        # Replacing Team__c value before upload
        temp = merge(source_team_instance_setting[['Team__c']], team_mapping[['Id_x', 'Id_y']], how='left', left_on='Team__c', right_on='Id_y')

        source_team_instance_setting['Team__c'] = temp['Id_x']

        if source_team_instance_setting['Team__c'].isna().any():
            return "Team__c: {} does not exist in the target org".format(temp['Team__c'][0])

        #
        # Finding Geography_Type_Name__c Mapping
        #
        source_geo_type = self.source_conn.fetch_records(query['geo_type_id_fetch']['object_name'], query['geo_type_id_fetch']['query'])
        target_geo_type = self.target_conn.fetch_records(query['geo_type_id_fetch']['object_name'], query['geo_type_id_fetch']['query'])
        geography_type_mapping = merge(target_geo_type, source_geo_type, how='left', on='Name')

        # Replacing Team__c value before upload
        temp = merge(source_team_instance_setting[['Geography_Type_Name__c']], geography_type_mapping[['Id_x', 'Id_y']], how='left', left_on='Geography_Type_Name__c', right_on='Id_y')

        source_team_instance_setting['Geography_Type_Name__c'] = temp['Id_x']

        if source_team_instance_setting['Geography_Type_Name__c'].isna().any():
            return "Geography_Type__c: {} does not exist in the target org".format(temp['Geography_Type_Name__c'][0])

        source_team_instance_setting.to_csv("source_team_instance_setting.csv")
        return country_mapping, team_mapping, geography_type_mapping

    def copy_scenario(self, country_mapping:DataFrame, team_mapping:DataFrame):
        #
        # Fetching scenario record from source
        #
        source_scenario_setting = self.source_conn.fetch_records(query["scenario_setting"]["object_name"],
                                                                      query["scenario_setting"]["query"].format(
                                                                          self.source_team_inst_id))
        # Replace Country__c value before upload
        temp = merge(source_scenario_setting[['Country__c']],
                     country_mapping[['Id_x', 'Id_y']], how='left', left_on='Country__c',
                     right_on='Id_y')
        source_scenario_setting['Country__c'] = temp['Id_x']

        # Checking if Country__c exist in target org.
        if source_scenario_setting['Country__c'].isna().any():
            return "Country__c: {} does not exist in the target org".format(temp['Country__c'][0])

        #
        # Finding RecordType Mapping
        #
        source_recordtype = self.source_conn.fetch_records(query['recordtype_id_fetch']['object_name'],
                                                     query['recordtype_id_fetch']['query'])
        target_recordtype = self.target_conn.fetch_records(query['recordtype_id_fetch']['object_name'],
                                                     query['recordtype_id_fetch']['query'])
        recordtype_mapping = merge(target_recordtype, source_recordtype, how='left', on='Name')

        # Replace RecordType value before upload
        temp = merge(source_scenario_setting[['RecordTypeId']],
                     recordtype_mapping[['Id_x', 'Id_y']], how='left', left_on='RecordTypeId',
                     right_on='Id_y')
        source_scenario_setting['RecordTypeId'] = temp['Id_x']

        # Checking if RecordType exist in target org.
        if source_scenario_setting['Country__c'].isna().any():
            return "RecordTypeId: {} does not exist in the target org".format(temp['RecordTypeId'][0])

        # Replace RecordType value before upload
        temp = merge(source_scenario_setting[['Team_Name__c']],
                     team_mapping[['Id_x', 'Id_y']], how='left', left_on='Team_Name__c',
                     right_on='Id_y')
        source_scenario_setting['Team_Name__c'] = temp['Id_x']

        # Checking if RecordType exist in target org.
        if source_scenario_setting['Team_Name__c'].isna().any():
            return "Team_Name__c: {} does not exist in the target org".format(temp['Team_Name__c'][0])

        # Adding Workspace target Id
        source_scenario_setting['Team_Name__c'] = self.workspace_id
        source_scenario_setting.to_csv("source_scenario_setting.csv")

        return recordtype_mapping

    def copy_team_instance_object_attribute(self):
        source_tm_inst_obj_attr_setting = self.source_conn.fetch_records(query['team_inst_obj_attr_setting']['object_name'], query['team_inst_obj_attr_setting']['query'].format(self.source_team_inst_id))
        # Need to update team instance

    def copy_CIM_Config(self):
        #
        # Fetching CIM Config record from source
        #
        source_CIM_Config_setting = self.source_conn.fetch_records(query['CIM_config_setting']['object_name'], query['CIM_config_setting']['query'].format(self.source_team_inst_id))

        #
        # Finding Geography_Type_Name__c Mapping
        #
        source_change_request_type = self.source_conn.fetch_records(query['Change_request_id_fetch']['object_name'],
                                                         query['Change_request_id_fetch']['query'])
        target_change_request_type = self.target_conn.fetch_records(query['Change_request_id_fetch']['object_name'],
                                                         query['Change_request_id_fetch']['query'])
        change_request_type_mapping = merge(target_change_request_type, source_change_request_type, how='left', on='Name')

        # Replace Geography_Type_Name__c value before upload
        temp = merge(source_CIM_Config_setting[['Change_Request_Type__c']],
                     change_request_type_mapping[['Id_x', 'Id_y']], how='left', left_on='Change_Request_Type__c',
                     right_on='Id_y')
        source_CIM_Config_setting['Change_Request_Type__c'] = temp['Id_x']

        # Checking if Geography_Type_Name__c exist in target org.
        if source_CIM_Config_setting['Change_Request_Type__c'].isna().any():
            return "CIM_Config__c: {} does not exist in the target org".format(temp['Change_Request_Type__c'][0])

        # Need to update team instance before upload

        return change_request_type_mapping

    def copy_team_instance_config(self, team_mapping:DataFrame):
        #
        # Fetching CIM Config record from source
        #
        source_team_instance_config__setting = self.source_conn.fetch_records(query['team_instance_config_setting']['object_name'], query['team_instance_config_setting']['query'].format(self.source_team_inst_id))

        # Replace Geography_Type_Name__c value before upload
        temp = merge(source_team_instance_config__setting[['Team__c']],
                     team_mapping[['Id_x', 'Id_y']], how='left', left_on='Team__c',
                     right_on='Id_y')
        source_team_instance_config__setting['Team__c'] = temp['Id_x']

        # Checking if Geography_Type_Name__c exist in target org.
        if source_team_instance_config__setting['Team__c'].isna().any():
            return "Team__c: {} does not exist in the target org".format(temp['Team__c'][0])

        # Need to update team_instance and name (which comprise of name and team_instance)

    def copy_cr_team_instance_config(self):
        #
        # Fetching CIM Config record from source
        #
        source_cr_team_instance_config_setting = self.source_conn.fetch_records(query['CR_team_instance_config_setting']['object_name'], query['CR_team_instance_config_setting']['query'].format(self.source_team_inst_id))

        # Need to update team_instance_c

    def copy_position(self, team_mapping):
        #
        # Fetching Position record from source
        #
        source_position_setting = self.source_conn.fetch_records(query['position_setting']['object_name'], query['position_setting']['query'].format(self.source_team_inst_id))

        # Replace Team_iD__c value before upload
        temp = merge(source_position_setting[['Team_iD__c']],
                     team_mapping[['Id_x', 'Id_y']], how='left', left_on='Team_iD__c',
                     right_on='Id_y')
        source_position_setting['Team_iD__c'] = temp['Id_x']

        # Checking if Geography_Type_Name__c exist in target org.
        if source_position_setting['Team_iD__c'].isna().any():
            return "Team__c: {} does not exist in the target org".format(temp['Team__c'][0])

        # Need to update team_instance and Parent_Position__c (which is self lookup of position id)
        return source_position_setting

    def copy_position_team_instance(self):
        #
        # Fetching Position record from source
        #
        source_position_team_instance__setting = self.source_conn.fetch_records(query['position_team_instance_setting']['object_name'], query['position_team_instance_setting']['query'].format(self.source_team_inst_id))

        # Need to update Parent_Position_ID__c, Position_ID__c, Team_Instance

    def copy_position_employee(self, source_position_setting):
        #
        # Fetching Position employee record from source
        #
        source_position_employee_setting = self.source_conn.fetch_records(query['position_employee_setting']['object_name'], query['position_employee_setting']['query'].format(self.source_team_inst_id))
        source_position_employee_setting = source_position_employee_setting[source_position_employee_setting['Position__c'].isin(source_position_setting['Id'])]

        if not source_position_employee_setting.empty:
            pass

        #
        # Finding Employee__c Mapping
        #
        source_employee = self.source_conn.fetch_records(query['Employee_fetch_id']['object_name'], query['Employee_fetch_id']['query'])
        target_employee = self.target_conn.fetch_records(query['Employee_fetch_id']['object_name'], query['Employee_fetch_id']['query'])
        employee_mapping = merge(target_employee, source_employee, how='left', on='Name')

        # Replace Employee__c value before upload
        temp = merge(source_position_employee_setting[['Employee__c']], employee_mapping[['Id_x', 'Id_y']], how='left', left_on='Employee__c', right_on='Id_y')
        source_position_employee_setting['Employee__c'] = temp['Id_x']

        # Checking if Employee__c exist in target org.
        if source_position_employee_setting['Employee__c'].isna().any():
            return "Employee__c: {} does not exist in the target org".format(temp['Employee__c'][0])

        # Need to update Position__c

    def copy_position_account(self):
        #
        # Fetching Position account record from source
        #
        source_position_account__setting = self.source_conn.fetch_records(query['position_account_setting']['object_name'], query['position_account_setting']['query'].format(self.source_team_inst_id))

        #
        # Finding Account__c Mapping
        #
        source_Account = self.source_conn.fetch_records(query['Account_id_fetch']['object_name'], query['Account_id_fetch']['query'])
        target_Account = self.target_conn.fetch_records(query['Account_id_fetch']['object_name'], query['Account_id_fetch']['query'])
        account_mapping = merge(target_Account, source_Account, how='left', on='AccountNumber')

        # Replace Geography_Type_Name__c value before upload
        temp = merge(source_position_account__setting[['Account__c']], account_mapping[['Id_x', 'Id_y']], how='left', left_on='Change_Request_Type__c', right_on='Id_y')
        source_position_account__setting['Account__c'] = temp['Id_x']

        # Checking if Geography_Type_Name__c exist in target org.
        if source_position_account__setting['Account__c'].isna().any():
            return "Account__c: {} does not exist in the target org".format(temp['Account__c'][0])

        # Need to update Position_Team_Instance__c, Position__c, Proposed_Position__c, team_instance__c

        return account_mapping

    def copy_position_geography(self, geography_type_mapping):
        #
        # Fetching Position geography record from source
        #
        source_position_geography__setting = self.source_conn.fetch_records(query['position_geography_setting']['object_name'], query['position_geography_setting']['query'].format(self.source_team_inst_id))

        # Replace Geography_Type_Name__c value before upload
        temp = merge(source_position_geography__setting[['Geography__c']], geography_type_mapping[['Id_x', 'Id_y']], how='left', left_on='Geography__c', right_on='Id_y')
        source_position_geography__setting['Geography__c'] = temp['Id_x']

        # Checking if Geography_Type_Name__c exist in target org.
        if source_position_geography__setting['Geography__c'].isna().any():
            return "Geography__c: {} does not exist in the target org".format(temp['Geography__c'][0])

        # Need to update Position_Id_External__c, Position_Team_Instance__c, Position__c, Proposed_Position__c before upload

    def copy_team_instance_object_attribute_details(self):
        #
        # Fetching Position record from source
        #
        source_position_geography__setting = self.source_conn.fetch_records(query['team_inst_obj_attr_details_setting']['object_name'], query['team_inst_obj_attr_details_setting']['query'].format(self.source_team_inst_id))

        # Need to update Object_Attibute_Team_Instance__c, Team_Instance__c

    def copy_cr_approval_config(self, change_request_type_mapping):
        #
        # Fetching Position record from source
        #
        source_cr_approval_c__setting = self.source_conn.fetch_records(query['team_inst_obj_attr_details_setting']['object_name'], query['team_inst_obj_attr_details_setting']['query'].format(self.source_team_inst_id))

        # Replace Geography_Type_Name__c value before upload
        temp = merge(source_cr_approval_c__setting[['Change_Request_Type__c']], change_request_type_mapping[['Id_x', 'Id_y']],
                     how='left', left_on='Change_Request_Type__c', right_on='Id_y')
        source_cr_approval_c__setting['Change_Request_Type__c'] = temp['Id_x']

        # Checking if Geography_Type_Name__c exist in target org.
        if source_cr_approval_c__setting['Change_Request_Type__c'].isna().any():
            return "Change_Request_Type__c: {} does not exist in the target org".format(temp['Change_Request_Type__c'][0])

        # Need to update Team_Instance_ID__c before upload

    def copy_CIM_Position_Metric_Summary(self):

        #
        # Fetching Position record from source
        #
        source_cr_approval_c__setting = self.source_conn.fetch_records(query['team_inst_obj_attr_details_setting']['object_name'], query['team_inst_obj_attr_details_setting']['query'].format(self.source_team_inst_id))

        # Need to update CIM_Config__c, Position_Team_Instance__c, Team_Instance__c before upload


