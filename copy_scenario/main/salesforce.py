import logging
import os
from uuid import uuid4
from salesforce_bulk import CsvDictsAdapter, SalesforceBulk
import time
import pandas as pd
from simple_salesforce import Salesforce
from .exclusions import unmanaged_objects, unmanaged_fields
import re

class SalesforceConnector:
    def __init__(self, username: str, password: str, security_token: str, sandbox:str, namespace:str):
        self.sandbox = False if sandbox.lower() == "false" else True

        self.namespace = namespace
        self.sf = Salesforce(username=username, password=password, security_token=security_token, sandbox=self.sandbox)
        self.bulk = SalesforceBulk(username=username, password=password, security_token=security_token, sandbox=self.sandbox)

    def fetch_records(self, object_name:str, query: str) -> pd.DataFrame:
        logging.debug("query input for fetch_records {}".format(query))
        query = self._prepare_query(query)
        object_name = object_name.replace(self.namespace, "")
        logging.debug("Object name = {}".format(object_name))
        if object_name not in unmanaged_objects:
            object_name = self.prepare_object_name(object_name)

        logging.debug("running query {}".format(query))
        job = self.bulk.create_query_job(object_name, contentType='CSV', concurrency='Serial')
        batch = self.bulk.query(job, query)
        self.bulk.close_job(job)
        while not self.bulk.is_batch_done(batch):
            time.sleep(10)

        if os.path.isfile('temp.csv'):
            os.remove('temp.csv')

        f = open('temp.csv', mode='a', encoding='utf-8')

        for result in self.bulk.get_all_results_for_query_batch(batch):
            for row in result:
                f.write(row.decode())
        f.close()

        df = pd.read_csv("temp.csv")
        os.remove('temp.csv')
        return df

    def _prepare_query(self, query: str) -> str:
        query = query.replace(self.namespace, "")
        object = query.split("FROM")[1].strip().split(" ")[0]
        logging.debug("input query {}".format(query))
        if object in unmanaged_objects:
            logging.debug("objects is not managed. No namespace at all in query")
            return query
        else:
            for custom_object in re.findall('( [0-9aA-zZ_]+__c| [0-9aA-zZ_]+__r)', query):
                if object in unmanaged_fields:
                    logging.debug("Object is not completely managed. Checking for unmanaged fields")
                    if custom_object.strip() in unmanaged_fields[object]:
                        logging.debug(
                            "Column {} is unmanaged. Skipping namespace attachment for it.".format(custom_object))
                        pass
                    else:
                        query = query.replace(custom_object, ' ' + self.prepare_object_name(custom_object[1:]))
                else:
                    query = query.replace(custom_object, ' ' + self.prepare_object_name(custom_object[1:]))
            for custom_object in re.findall('(\.[0-9aA-zZ_]+__c)', query):
                query = query.replace(custom_object, '.' + self.prepare_object_name(custom_object[1:]))
            for custom_object in re.findall('(\.[0-9aA-zZ_]+__r)', query):
                query = query.replace(custom_object, '.' + self.prepare_object_name(custom_object[1:]))

        logging.debug("returning query {}".format(query))
        return query

    def upload_records(self, object_name:str, data_frame: pd.DataFrame):
        data_frame.fillna('',inplace=True)
        logging.debug("uploading records")
        try:
            data_frame.drop('Id', inplace=True, axis=1)
        except Exception as e:
            logging.debug("No Id column found.")
        start = 0
        end = 0
        k = []
        failed_records = pd.DataFrame(columns=['record_num', 'created', 'error'])

        while end != len(data_frame):
            end = start + min(10000, len(data_frame) - start)
            job = self.bulk.create_insert_job(object_name, contentType='CSV', concurrency='Serial')
            csv_iter = CsvDictsAdapter(iter(data_frame[start:end].to_dict(orient='records')))
            batch = self.bulk.post_batch(job, csv_iter)
            self.bulk.wait_for_batch(job, batch)
            l = self.bulk.get_batch_results(batch, job)
            for i in range(len(l)):
                if l[i][1] == 'true':
                    k.append(l[i][0])
                else:
                    temp = [i + 1, l[i][2], l[i][3]]
                    failed_records = failed_records.append(
                        pd.DataFrame([temp], columns=['record_num', 'created', 'error']))
                    k.append('')

            self.bulk.close_job(job)
            start = end

        if len(failed_records) != 0:
            logging.debug("Dumping Failure Records of {}".format(object_name))
            failed_records.to_csv(os.path.join("failed_records_" + object_name + ".csv"))

        return pd.DataFrame({"id": k})

    def update_records(self, object_name: str, data_frame: pd.DataFrame):
        data_frame.fillna('', inplace=True)
        logging.debug("No of records to update {}".format(len(data_frame)))

        start = 0
        end = 0
        while end != len(data_frame):
            end = start + min(10000, len(data_frame) - start)
            job = self.bulk.create_update_job(object_name, contentType='CSV', concurrency='Serial')
            csv_iter = CsvDictsAdapter(iter(data_frame[start:end].to_dict(orient='records')))
            batch = self.bulk.post_batch(job, csv_iter)
            self.bulk.wait_for_batch(job, batch)
            self.bulk.close_job(job)
            start = end

        # response = getattr(self.sf.bulk, object_name).update(data_frame.to_dict(orient='records'))

    def delete_records(self, object_name: str, data_frame: pd.DataFrame):

        object_name = self.prepare_object_name(object_name)
        start = 0
        end = 0
        while end != len(data_frame):
            end = start + min(10000, len(data_frame) - start)
            job = self.bulk.create_delete_job(object_name, contentType='CSV', concurrency='Serial')
            csv_iter = CsvDictsAdapter(iter(data_frame[start:end].to_dict(orient='records')))
            batch = self.bulk.post_batch(job, csv_iter)
            self.bulk.wait_for_batch(job, batch)
            self.bulk.close_job(job)
            start = end

        # return getattr(self.sf.bulk, object_name).delete(data_frame.to_dict(orient='records'))

    def __response_handler(self, object_name: str, response: dict):
        response = pd.DataFrame(response)
        failed_records = response[~response['success']]
        if len(failed_records):
            filename = os.path.join("logs", "failed_records", object_name + str(uuid4()) + ".csv")
            logging.warning("Dumping failed upload records in %s - No. of records %d" % (filename, len(failed_records)))
            failed_records.to_csv(filename, index=False)
        return response[response['success']]

    def validate_object(self, object_name:str, meta_data):
        object_name = self.prepare_object_name(object_name)
        # logging.debug("object_name {}".format(object_name))
        details = getattr(self.sf, object_name).describe()
        org_column = []
        reference_set = set()

        for field in details["fields"]:
            org_column.append(field["name"]) if field["updateable"] else ''

            if field["referenceTo"] and field["updateable"] and field["name"].endswith('__c'):
                reference_set.add(field["name"].replace('__c','__r') + '.Name')
                logging.debug("inside validate {}".format(field["name"].replace('__c','__r') + '.Name'))

        org_column.append('Id')
        org_column_set = set(self.removing_namespace(org_column))
        json_column_set = set(meta_data['columns'].replace(" ","").split(','))

        return set(self.adding_namespace(list(org_column_set.intersection(json_column_set)))).union(reference_set)


    def prepare_object_name(self, object_name):
        if object_name.find(self.namespace) == -1 and (object_name.endswith('__c') or object_name.endswith('__r')):
            return self.namespace + object_name
        return object_name

    def removing_namespace(self, column_name:list):
        updated_columns = []
        for column in column_name:
            updated_columns.append(re.sub(self.namespace,'',column))

        return updated_columns

    def remove_namespace_df(self, df):
        df.columns = df.columns.str.replace(self.namespace, '')

    def prepare_df(self, records, source_namespace, target_namespace):
        records.columns = records.columns.str.replace(source_namespace, target_namespace)

    def rename_namespace_df(self, records, source_namespace:str, target_namepace:str):
        records.columns = records.columns.str.replace(source_namespace, '')
        records.columns = [target_namepace + col if col.endswith("__c") else col for col in records.columns]
        return records

    def adding_namespace(self, column_name:list):
        updated_columns = []
        for column in column_name:
            updated_columns.append(self.namespace + column)

        return updated_columns