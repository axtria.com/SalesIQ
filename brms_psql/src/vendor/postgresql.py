import logging

import pandas as pd
import psycopg2

from src.vendor.salesforce import SalesforceConnector


class Postgres:
    def __init__(self, connection_string: str):
        try:
            self.conn = psycopg2.connect(connection_string)
            self.conn.autocommit = True
        except Exception as e:
            logging.error(e)

    @classmethod
    def initialise_from(cls, salesforce: SalesforceConnector):
        credentials = salesforce.query(
            """SELECT BR_PG_Database__c,BR_PG_Host__c,BR_PG_Password__c,BR_PG_Port__c,BR_PG_Schema__c,
            BR_PG_UserName__c FROM ETL_Config__c
            WHERE Name = 'BRMS' LIMIT 1"""
        )

        connection_str = "dbname='{dbname}' host='{host}' port='{port}' user='{user}' password='{password}'".format(
            dbname=credentials.at[0, 'BR_PG_Database__c'],
            host=credentials.at[0, 'BR_PG_Host__c'],
            port=credentials.at[0, 'BR_PG_Port__c'],
            user=credentials.at[0, 'BR_PG_UserName__c'],
            password=credentials.at[0, 'BR_PG_Password__c']
        )
        # connection_str = "dbname='{dbname}' host='{host}' port='{port}' user='{user}' password='{password}'".format(
        #     dbname='QA_4.0_copy_dev',
        #     host='35.164.63.126',
        #     port='5432',
        #     user='ssenger',
        #     password='ssenger@126'
        # )

        return cls(connection_str)

    def execute_query(self, query):
        logging.info('\n\n %s \n' % query)
        cur = None
        try:
            cur = self.conn.cursor()
            cur.execute(query)
            return True
        except Exception as e:
            logging.error(e)
            return False
        finally:
            if cur:
                cur.close()

    def copy_from(self, abs_file_path, table, columns):
        logging.info('\n\n %s \n' % abs_file_path)
        cur = None
        try:
            cur = self.conn.cursor()
            with open(abs_file_path) as f:
                f.seek(0)
                cur.copy_from(f, table, sep='|', columns=columns, null="")
            return True
        except Exception as e:
            logging.error(e)
            return False
        finally:
            if cur:
                cur.close()

    def execute_select_query(self, query):
        logging.info('\n\n %s \n' % query)
        try:
            records = pd.read_sql_query(query, self.conn)
            return records
        except Exception as e:
            logging.error(e)
            return False

    def close(self):
        self.conn.close()
