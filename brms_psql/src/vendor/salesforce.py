import logging

import pandas as pd
from simple_salesforce import Salesforce

from src.service.helpers import timer


class SalesforceConnector:
    def __init__(self, user_name, password, security_token):
        self.sf = Salesforce(username=user_name,
                             password=password,
                             security_token=security_token)

    @timer
    def query(self, query: str):
        logging.info('\n\n %s \n' % query)
        records = self.sf.query(query)
        df = pd.DataFrame(records['records'])
        try:
            df = df.drop('attributes', axis=1)
        except ValueError:
            logging.warning("No attribute column found to delete.")
        return df

    def update(self, object_name, object_id, key, value):
        try:
            getattr(self.sf, object_name).update(object_id, {key: value})
        except Exception as e:
            logging.error(e)
