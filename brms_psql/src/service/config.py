"""
    brms_config
    ~~~~~~~~~~~

    Hold the configuration file - config.ini into a global object of class type ConfigParser

"""

from configparser import ConfigParser

brms_config = ConfigParser()
brms_config.optionxform = str
brms_config.read('./config.ini')

