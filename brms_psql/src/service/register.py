import logging

REGISTERED_JOBS = {}


def register_job(name):
    def wrapper(cls):
        logging.debug("Register job - %s" % name)
        REGISTERED_JOBS[name] = cls
        return cls

    return wrapper
