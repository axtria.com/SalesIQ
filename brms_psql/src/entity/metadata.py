from src.vendor.salesforce import SalesforceConnector


class MetaData:
    def __init__(self):
        self.scenario = None
        self.scenario_rule_instance = None
        self.scenario_rule_instance_details_str = None
        self.dataset_rule_map = None
        self.scenario_data_object_map = None
        self.dataset = None
        self.dataset_str = None
        self.dataset_column_detail = None
        self.business_rules = None
        self.br_join_rule = None
        self.business_rules_expression = None
        self.br_type_master = None
        self.component_type_master = None
        self.config_call_balancing_constraint_rule_map = None
        self.config_call_balancing_map = None
        self.config_call_balancing_constraint_detail = None
        self.business_rule_field_map_details = None
        self.call_balancing_expression_data_detail = None

        self.objects = None

    @classmethod
    def initialise(cls, salesforce_connector: SalesforceConnector, scenario_id: str):
        metadata = cls()

        metadata.scenario = salesforce_connector.query("""
        SELECT CreatedById,CreatedDate,CR_Tracking__c,Data_Object__c,Effective_End_Date__c,
        Effective_Start_Date__c,Employee_Assignment__c ,Employee_Assignment_Date__c ,Id ,IsDeleted ,
        LastModifiedById ,LastModifiedDate ,Name ,Request_Process_Stage__c ,Scenario_Alignment_Level__c ,
        Description__c ,Scenario_Name__c ,Scenario_Name_Formula__c ,Scenario_Stage__c ,Scenario_Status__c ,
        Scenario_Type__c ,Send_To_CRM__c ,Shared__c,SystemModstamp ,Team_Name__c ,Workspace__c ,
        Source_Team_Instance__c ,Team_Instance__c,RecordTypeId  FROM Scenario__c  WHERE Id = '{scenario_id}'
        """.format(scenario_id=scenario_id))

        metadata.scenario_rule_instance = salesforce_connector.query("""
        SELECT BRT_Details_Id__c ,BusinessRuleTemplateId__c ,ComponentTypeLabel__c ,Component_Display_Order__c ,
        Component_Level__c ,Component_Type_Master__c ,CreatedById ,CreatedDate ,Id ,Input_Table__c ,IsDeleted ,
        Is_Editable__c ,Is_Visible__c ,LastModifiedById ,LastModifiedDate ,Name ,Output_Table__c ,OwnerId ,
        Parent_Display_Order__c ,Parent_Level__c ,Scenario_Id__c ,Status__c ,SystemModstamp ,Exec_Seq__c ,
        Scenario_Rule_Instance_Details_Ext_Id__c FROM Scenario_Rule_Instance_Details__c WHERE Scenario_Id__c = '{scenario_id}' ORDER BY Exec_Seq__c ASC
        """.format(scenario_id=scenario_id))

        metadata.dataset_rule_map = salesforce_connector.query("""
        SELECT base_data_set_Id__c ,Business_Rule_Template_Details__c, CreatedById ,CreatedDate ,
        dataset_id__c ,DS_Rule_Map_Id__c ,ds_type__c, Id ,IsDeleted ,LastModifiedById ,LastModifiedDate ,Name ,
        OwnerId ,scenario_rule_instance_id__c ,SystemModstamp ,Table_Display_Name__c ,
        table_name__c FROM Data_Set_Rule_Map__c WHERE scenario_rule_instance_id__c IN
        (SELECT Id FROM Scenario_Rule_Instance_Details__c WHERE Scenario_Id__c = '{scenario_id}')
        """.format(scenario_id=scenario_id))

        metadata.dataset_str = "'" + "','".join(metadata.dataset_rule_map['dataset_id__c']) + "'"

        metadata.scenario_data_object_map = salesforce_connector.query("""
        SELECT CreatedById ,CreatedDate ,Data_Object__c ,Id ,IsDeleted ,LastModifiedById ,LastModifiedDate ,Name ,
        OwnerId ,Scenario__c ,Scenario_Data_Object_Map__c ,SystemModstamp ,Final_table__c ,Data_Set__c ,
        Scenario_Data_Object_Map_Ext_Id__c ,Data_set_Id__c
        FROM Scenario_Data_Object_Map__c
        WHERE Scenario__c = '{scenario_id}'
        """.format(scenario_id=scenario_id))

        metadata.dataset = salesforce_connector.query("""
        SELECT Base_Data_Set__c ,CreatedById ,CreatedDate ,Data_Set_Id__c ,destination__c, 
        dst_conn_str__c ,Id ,IsDeleted ,is_internal__c ,
        LastModifiedById ,LastModifiedDate ,Name ,OwnerId ,period__c ,source__c ,src_conn_str__c ,
        SystemModstamp ,version__c FROM Data_Set__c WHERE Scenario__c = '{scenario_id}'
        """.format(scenario_id=scenario_id))

        metadata.dataset_column_detail = salesforce_connector.query("""
        SELECT column_id__c ,CreatedById ,CreatedDate ,dataset_id__c ,datatype__c ,Data_Set_Field_Desc__c, ds_col_name__c ,
        Error_String__c, format__c ,Id ,IsDeleted ,is_mandatory__c ,Key_Column__c ,LastModifiedById ,
        LastModifiedDate ,Name ,OwnerId ,SystemModstamp ,Table_Data_Type__c ,tb_col_nm__c ,validation__c ,
        value_type__c ,variable_type__c FROM Data_Set_Column_Detail__c WHERE dataset_id__c IN
        (SELECT Id FROM Data_Set__c WHERE Scenario__c ='{scenario_id}')
        """.format(scenario_id=scenario_id))

        metadata.scenario_rule_instance_details_str = "'" + "','".join(metadata.scenario_rule_instance['Id']) + "'"

        metadata.business_rules = salesforce_connector.query("""
        SELECT BusinessRuleTemplateDetailsId__c ,Business_Rule_Type_Master__c ,CreatedById ,CreatedDate ,
        Debug_Table_Name__c ,Execution_Sequence__c ,Id ,IsDeleted ,Is_Derived__c ,Is_Mandatory__c ,
        LastModifiedById ,LastModifiedDate ,Name ,OwnerId ,Parent_Rule_Id__c, ScenarioRuleInstanceDetails__c ,
        Status__c ,SystemModstamp ,Aggregate_Business_Rule_Template_Details__c ,Aggregate_Level__c ,
        Aggregate_Scenario_Rule_Instance__c ,Business_Rules_Ext_Id__c ,Customer_Prioritization__c ,
        Prioritization_Order__c ,Scenario__c
        FROM Business_Rules__c WHERE ScenarioRuleInstanceDetails__c IN ({scenario_instance_details})
        ORDER BY Execution_Sequence__c ASC
        """.format(scenario_instance_details=metadata.scenario_rule_instance_details_str))

        metadata.br_join_rule = salesforce_connector.query("""
        SELECT Business_Rules__c, CreatedById, CreatedDate, Id, IsDeleted, Join_Display_expression__c, Join_Execute_Expression__c, LastModifiedById, LastModifiedDate, Name, OwnerId, Scenario__c, Select_Display_Expression__c, Select_Execute_Expression__c, SystemModstamp, insert_exec_expr__c, where_expression__c, where_display_expression__c, Affiliation_Hierarchy__c, Execution_Sequence__c
        FROM BR_JoinRule__c WHERE Scenario__c = '{scenario_id}'
        """.format(scenario_id=scenario_id))

        metadata.business_rules_expression = salesforce_connector.query("""
        SELECT Business_Rules__c ,CreatedById ,CreatedDate ,Group_By_Expr__c ,Id ,IsDeleted ,LastModifiedById ,
        LastModifiedDate ,Name ,OwnerId ,Select_Expr__c ,SystemModstamp ,Where_Expr__c ,
        Business_Rules_Expression_Ext_Id__c FROM Business_Rules_Expression__c WHERE
        Business_Rules__c IN
        (SELECT Id FROM Business_Rules__c WHERE ScenarioRuleInstanceDetails__c IN ({scenario_instance_details}))
        """.format(scenario_instance_details=metadata.scenario_rule_instance_details_str))

        metadata.br_type_master = salesforce_connector.query("""
        SELECT Component_Type__c, CreatedById, CreatedDate, Id, IsDeleted, LastModifiedById, LastModifiedDate,
         Name, OwnerId, SystemModstamp, Business_Rule_Type_Master_Ext_Id__c
        from Business_Rule_Type_Master__c""")

        metadata.component_type_master = salesforce_connector.query("""
        SELECT Component_Definition__c ,Component_Type_Master_Ext_Id__c ,CreatedById ,CreatedDate ,
        DisplayIconClass__c ,DisplayIcon__c ,Id ,LastModifiedById ,LastModifiedDate ,Name ,OwnerId ,SystemModstamp ,IsDeleted 
        from ComponentTypeMaster__c""")

        metadata.config_call_balancing_constraint_rule_map = salesforce_connector.query("""
        SELECT Business_Rules__c,Config_Call_Balancing_Constraint_Detail__c ,CreatedById ,CreatedDate ,
        Exec_Seq__c ,Id ,LastModifiedById ,LastModifiedDate ,Name ,OwnerId ,Scenario__c ,SystemModstamp ,IsDeleted FROM 
        Config_Call_Balancing_Constraint_RuleMap__c WHERE Scenario__c = '{scenario}'""".format(scenario=scenario_id))

        metadata.config_call_balancing_map = salesforce_connector.query("""
        SELECT Aggregate_Business_Rule_Detail__c,Aggregate_Level__c ,Aggregate_Scenario_Rule_Instance__c ,
        Base_Config_Call_Balancing_Map_Id__c,Business_Rule_Template_Details__c ,Constraint_1__c ,Constraint_2__c,
        CreatedById ,CreatedDate ,Id,LastModifiedById,LastModifiedDate ,Name ,OwnerId ,Run_order__c ,
        Scenario_Rule_Instance_Details__c,Scenario__c ,SystemModstamp ,IsDeleted,Status__c
        FROM Config_Call_Balancing_Map__c WHERE Scenario__c = '{scenario}'""".format(scenario=scenario_id))

        metadata.config_call_balancing_constraint_detail = salesforce_connector.query("""
        SELECT Base_Config_Call_Balancing_ConstraintDet__c , Config_Call_Balancing_Map_Id__c ,  Constraint_1_Goal__c ,
        Constraint_1_Value__c ,  Constraint_2_Goal__c ,  Constraint_2_Value__c ,  CreatedById , CreatedDate , Id ,
        LastModifiedById ,  LastModifiedDate ,  Name ,  OwnerId ,  Scenario__c ,  SystemModstamp ,  IsDeleted
        FROM Config_Call_Balancing_Constraint_Detail__c WHERE
        Scenario__c = '{scenario}'""".format(scenario=scenario_id))

        metadata.business_rule_field_map_details = salesforce_connector.query("""
        SELECT Id ,OwnerId ,IsDeleted ,Name ,CreatedDate ,CreatedById ,LastModifiedDate ,LastModifiedById ,
        SystemModstamp ,Business_Rule_FieldMap_Details_Id__c ,Business_Rule_Field_Map_Details_Ext_Id__c ,
        Business_Rule__c ,IO_Flag__c ,Param_Data_Type__c ,Param_Name__c ,Param_Type__c ,Scenario_Rule_Instance_Id__c
        FROM Business_Rule_FieldMap_Details__c WHERE Scenario_Rule_Instance_Id__c IN ({scenario_instance_details})
        """.format(scenario_instance_details=metadata.scenario_rule_instance_details_str))

        metadata.call_balancing_expression_data_detail = salesforce_connector.query("""
        SELECT Business_Rules__c ,Name ,CreatedById, Input_Column__c ,Input_Condition__c ,
        LastModifiedById ,Output_Column__c ,Output_Column_Values__c ,OwnerId ,Id FROM 
        Call_balancing_expression_data_detail__c WHERE Business_Rules__r.ScenarioRuleInstanceDetails__c IN ({scenario_instance_details})
        """.format(scenario_instance_details=metadata.scenario_rule_instance_details_str))

        metadata.objects = {'t_scenario': metadata.scenario,
                            't_scn_rule_instance_details': metadata.scenario_rule_instance,
                            't_dataset_rule_map': metadata.dataset_rule_map,
                            't_scn_data_object_map': metadata.scenario_data_object_map,
                            't_dataset': metadata.dataset,
                            't_ds_col_detail': metadata.dataset_column_detail,
                            't_business_rule': metadata.business_rules,
                            't_br_joinrule': metadata.br_join_rule,
                            't_br_expr': metadata.business_rules_expression,
                            't_br_type_master': metadata.br_type_master,
                            't_component_type_master': metadata.component_type_master,
                            't_config_call_balancing_constraint_rule_map': metadata.config_call_balancing_constraint_rule_map,
                            't_config_call_balancing_map': metadata.config_call_balancing_map,
                            't_config_call_balancing_constraint_detail': metadata.config_call_balancing_constraint_detail,
                            't_business_rule_field_map_details': metadata.business_rule_field_map_details,
                            't_call_balancing_expression_data_detail': metadata.call_balancing_expression_data_detail}
        return metadata
