import os

from src.entity.metadata import MetaData
from src.service.config import brms_config
from src.vendor.postgresql import Postgres
from src.vendor.salesforce import SalesforceConnector


class RunScenario:
    def __init__(self, scenario_id: str, user_name: str, password: str, security_token: str, mode: str):
        self.scenario_id = scenario_id
        self.salesforce = SalesforceConnector(user_name, password, security_token)
        self.mode = mode
        self.postgres = Postgres.initialise_from(self.salesforce)
        self.metadata = MetaData.initialise(self.salesforce, scenario_id)

    def invoke(self):
        self.fetch_metadata()
        self.push_metadata_to_postgres()
        self.execute_scenario()
        self.mark_scenario_complete()
        self.postgres.close()

    def fetch_metadata(self):
        # Mark the following objects `In Progress`
        # Scenario
        # Scenario Rule Instance
        # Business Rules
        status = 'In Progress'
        self.salesforce.update('Scenario__c', self.scenario_id, 'Request_Process_Stage__c', status)
        self.metadata.scenario.loc[self.metadata.scenario.Id == self.scenario_id, 'Request_Process_Stage__c'] = status

        for _, scn_rul_instance in self.metadata.scenario_rule_instance.iterrows():
            self.salesforce.update('Scenario_Rule_Instance_Details__c', scn_rul_instance['Id'], 'Status__c', status)
            self.metadata.scenario_rule_instance.loc[
                self.metadata.scenario_rule_instance.Id == scn_rul_instance['Id'], 'Status__c'] = status

        for _, rule in self.metadata.business_rules.iterrows():
            self.salesforce.update('Business_Rules__c', rule['Id'], 'Status__c', status)
            self.metadata.business_rules.loc[
                self.metadata.business_rules.Id == rule['Id'], 'Status__c'] = status

    def push_metadata_to_postgres(self):
        # Delete from Postgresql
        self.delete_from_pg()
        # Insert metadata into Postgresql
        self.insert_to_pg()

    def execute_scenario(self):
        query = "select fn_execute_scenario('" + self.scenario_id + "' , '" + self.mode + "');"
        result = self.postgres.execute_query(query)
        return result

    def mark_scenario_complete(self):
        # scenario, scenario rule instance, business rule, config call balancing map
        # select status and id from object
        # update sfdc object based on the ids

        # scenario
        query = "SELECT request_process_stage FROM t_scenario WHERE sfdc_id = '" + self.scenario_id + "'"
        scenario = self.postgres.execute_select_query(query)
        print('::::: status')
        print(scenario['request_process_stage'][0])
        self.salesforce.update('Scenario__c', self.scenario_id, 'Request_Process_Stage__c',
                               scenario['request_process_stage'][0])

        # scenario rule instance
        query = "SELECT status, sfdc_id FROM t_scn_rule_instance_details WHERE scenario_id = '" + self.scenario_id + "'"
        scenario_rule_instances = self.postgres.execute_select_query(query)
        for _, scenario_rule_instance in scenario_rule_instances.iterrows():
            self.salesforce.update('Scenario_Rule_Instance_Details__c', scenario_rule_instance['sfdc_id'], 'Status__c',
                                   scenario_rule_instance['status'])

        # business rule
        query = """SELECT status, sfdc_id FROM t_business_rule 
        WHERE scenario_rule_instance_details in ({scenario_rule_instance_list})
        """.format(scenario_rule_instance_list=self.metadata.scenario_rule_instance_details_str)

        business_rules = self.postgres.execute_select_query(query)
        for _, business_rule in business_rules.iterrows():
            self.salesforce.update('Business_Rules__c', business_rule['sfdc_id'], 'Status__c', business_rule['status'])

        # config call balancing map
        query = """SELECT status, sfdc_id FROM t_config_call_balancing_map WHERE scenario = '{scenario_id}'
        """.format(scenario_id=self.scenario_id)

        results = self.postgres.execute_select_query(query)
        for _, config_call_balancing_map in results.iterrows():
            self.salesforce.update('Config_Call_Balancing_Map__c', config_call_balancing_map['sfdc_id'], 'Status__c',
                                   config_call_balancing_map['status'])

    def delete_from_pg(self):
        # delete records from postgresql
        for obj, query in dict(brms_config.items('postgresql_delete')).items():
            self.postgres.execute_query(query)

        for obj, query in dict(brms_config.items('postgresql_delete_scenario')).items():
            self.postgres.execute_query(query.format(scenario_id=self.scenario_id))

        for obj, query in dict(brms_config.items('postgresql_delete_scenario_instance_details')).items():
            self.postgres.execute_query(query.format(scenario_instance_details=self.metadata.scenario_rule_instance_details_str))

        for obj, query in dict(brms_config.items('postgresql_delete_dataset_id')).items():
            self.postgres.execute_query(query.format(dataset_ids=self.metadata.dataset_str))

    def insert_to_pg(self):
        # download sfdc results
        for obj, df in self.metadata.objects.items():
            print('::::::writing ' + obj)
            # print(df)
            print('.//tmp//' + obj)
            df.to_csv('.//tmp//' + obj, header=False, index=False, sep='|')

        # self.metadata.scenario.to_csv('./tmp/scenario', header=False, index=False, sep='|')
        # self.metadata.scenario_rule_instance.to_csv('./tmp/scn_rule_instance_details', header=False, index=False, sep='|')
        # self.metadata.dataset_rule_map.to_csv('./tmp/dataset_rule_map', header=False, index=False, sep='|')
        # self.metadata.scenario_data_object_map.to_csv('./tmp/scn_data_object_map', header=False, index=False, sep='|')
        # self.metadata.dataset.to_csv('./tmp/dataset', header=False, index=False, sep='|')
        # self.metadata.dataset_column_detail.to_csv('./tmp/ds_col_detail', header=False, index=False, sep='|')
        # self.metadata.business_rules.to_csv('./tmp/business_rule', header=False, index=False, sep='|')
        # self.metadata.br_join_rule.to_csv('./tmp/br_joinrule', header=False, index=False, sep='|')
        # self.metadata.business_rules_expression.to_csv('./tmp/br_expr', header=False, index=False, sep='|')
        # self.metadata.br_type_master.to_csv('./tmp/br_type_master', header=False, index=False, sep='|')
        # self.metadata.component_type_master.to_csv('./tmp/component_type_master', header=False, index=False, sep='|')
        # self.metadata.config_call_balancing_constraint_rule_map.to_csv('./tmp/config_call_balancing_constraint_rule_map', header=False, index=False, sep='|')
        # self.metadata.config_call_balancing_map.to_csv('./tmp/config_call_balancing_map', header=False, index=False, sep='|')
        # self.metadata.config_call_balancing_constraint_detail.to_csv('./tmp/config_call_balancing_constraint_detail', header=False, index=False, sep='|')
        # self.metadata.business_rule_field_map_details.to_csv('./tmp/business_rule_field_map_details', header=False, index=False, sep='|')
        # self.metadata.call_balancing_expression_data_detail.to_csv('./tmp/call_balancing_expression_data_detail', header=False, index=False, sep='|')

    # for obj, cols in dict(brms_config.items('salesforce_nofilter')).items():
        #
        #
        #     # save records to csv
        #     records.to_csv('./tmp/' + brms_config['object_mapping'][obj], header=False, index=False, sep='|')
        #
        # for obj, cols in dict(brms_config.items('salesforce_scenario')).items():
        #     # format query to include file from which to insert
        #     query = 'SELECT ' + cols + ' FROM ' + obj + " where Scenario__c = '" + self.scenario_id + "'"
        #     records = self.salesforce.query(query)
        #
        #     # save records to csv
        #     records.to_csv('./tmp/' + brms_config['object_mapping'][obj], header=False, index=False, sep='|')
        #
        # for obj, cols in dict(brms_config.items('salesforce_scenario_instance_details')).items():
        #     # format query to include file from which to insert
        #     query = 'SELECT ' + cols + ' FROM ' + obj + " where
        #     records = self.salesforce.query(query)
        #
        #     # save records to csv
        #     records.to_csv('./tmp/' + brms_config['object_mapping'][obj], header=False, index=False, sep='|')
        #
        # for obj, cols in dict(brms_config.items('salesforce_dataset_id')).items():
        #     # format query to include file from which to insert
        #     query = 'SELECT ' + cols + ' FROM ' + obj
        #     records = self.salesforce.query(query)
        #
        #     # save records to csv
        #     records.to_csv('./tmp/' + brms_config['object_mapping'][obj], header=False, index=False, sep='|')

        # execute copy query
        for obj, cols in dict(brms_config.items('postgresql_insert')).items():
            filepath = os.path.join('.//tmp', obj)
            print('::::copy_from' + filepath)
            self.postgres.copy_from(os.path.abspath(filepath), obj, columns=cols.replace(' ', '').split(','))
