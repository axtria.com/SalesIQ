import logging
from logging.handlers import TimedRotatingFileHandler

from flask import Flask, request

from src.gateway.job import Job

app = Flask(__name__)
PREFIX = '/brms_psql'


@app.route(PREFIX + "/run_scenario")
def run_scenario():
    scenario_id = request.args.get('scenario_id', default='1', type=str)
    user_name = request.args.get('user_name', default='1', type=str)
    password = request.args.get('password', default='1', type=str)
    security_token = request.args.get('security_token', default='1', type=str)
    mode = request.args.get('mode', default='Run', type=str)
    job = Job()
    job.build('RunScenario', {
        'scenario_id': scenario_id,
        'user_name': user_name,
        'password': password,
        'security_token': security_token,
        'mode': mode
    })
    job.execute()
    return "Scenario executed %s" % scenario_id


@app.before_request
def log_request_info():
    logging.debug('Request ::: %r' % request.url)

if __name__ == '__main__':
    app.debug = True
    logging.basicConfig(level=logging.DEBUG,
                        handlers=[TimedRotatingFileHandler("logs//system.log", when="midnight"),
                                  logging.StreamHandler()],
                        format='%(asctime)s - %(module)s.%(funcName)s() - %(levelname)s - %(message)s')
    app.run(threaded=True)
