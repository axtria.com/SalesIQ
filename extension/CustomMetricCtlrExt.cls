public  class CustomMetricCtlrExt implements SalesIQBasePackage.ICustomMetricExtension{
	public static string namespace = '';
	public static list<string> positionLabelList = new list<string>();

    public static String ALIGNMENT_MODULE = 'Alignment Module' ;  
    public static String CHANGE_REQUEST = 'Change Request' ;

    
    public string getCustomMetricdata(string source, string destination, string teamInstance, list<string> impactedZIPs, string countryId, string crStatus,Change_Request__c crRequest, string movementType, string pageName){
            
        system.debug('source '+source);
        system.debug('destination '+destination);
        system.debug('teamInstance '+teamInstance);
        system.debug('impactedZIPs '+impactedZIPs);
        positionLabelList = new list<string>{'source','destination'};
        map<string,position__c> mapPositionandCode = new map<string,position__c>();
        integer sourceZIPterritoryCount=0;
        integer destinationZIPterritoryCount=0;

        system.debug('impactedZIPs after size '+impactedZIPs.size());
        string metricTable = '';

        set<id> allPosition = new set<id>();
        allPosition.add(source);
        allPosition.add(destination);

        list<Position__c> lstAllPsoition = getPositionById(allPosition);
        for(Position__c pos:lstAllPsoition){
        	mapPositionandCode.put(pos.Name,pos);
        }

        string strAssignmentStatus = getAssignmentStatusbyTeamInstance(teamInstance);

        list<Team_Instance_Mapping__c> lstMapping  = getTeamInstanceMapping(new set<id>{teamInstance}); 
        set<id> counterpartTeamInstance = new set<id>();
        if(lstMapping!=null && lstMapping.size()>0){            
            for(Team_Instance_Mapping__c mapping:lstMapping){
                if(!string.isBlank(mapping.Team_Instance1__c)){
                    counterpartTeamInstance.add(mapping.Team_Instance1__c);
                }
                if(!string.isBlank(mapping.Team_Instance2__c)){
                    counterpartTeamInstance.add(mapping.Team_Instance2__c);
                }
            }
        }

        //if no counterpart team Instance are configured the return error message
        if(counterpartTeamInstance==null || counterpartTeamInstance.size()==0){        	
        	metricTable  ='No Config Found';
        	return metricTable;
        }
        
        list<Position_Geography__c> lstPositionGeoGraphy = getZIPbyPositionId(allPosition,teamInstance);
        list<string> sourcePositionZIPs = new list<string>();
        list<string> destinationPositionZIPs = new list<string>();

       

        if(lstPositionGeoGraphy!=null && lstPositionGeoGraphy.size()>0){
        	for(Position_Geography__c pos:lstPositionGeoGraphy){
        		if(pos.Position__c==source){
        			sourcePositionZIPs.add(pos.geography__r.Name);                    
        		}
        		if(pos.Position__c==destination){
        			destinationPositionZIPs.add(pos.geography__r.Name);
        		}
        	}

           
        	AggregateResult countSourceAfter = countSourcePosition(sourcePositionZIPs,impactedZIPs,counterpartTeamInstance, strAssignmentStatus,crStatus);
            string countSource = string.valueOf(countSourceAfter.get('sourceAfter'));
            if(!string.isBlank(countSource)){
                if(integer.valueOf(countSource)>0){
                    
                    sourceZIPterritoryCount = integer.valueOf(countSource)+1;
                }
                

            }
        	AggregateResult countDestAfter  = countDestinationPosition(destinationPositionZIPs,impactedZIPs,counterpartTeamInstance,strAssignmentStatus,crStatus);  
            string countDestination = string.valueOf(countDestAfter.get('destinationAfter'));
            if(!string.isBlank(countDestination)){
                if(integer.valueOf(countDestination)>0){                    
                    destinationZIPterritoryCount = integer.valueOf(countDestination)+1;
                }               

            }

            system.debug('countSourceAfter '+countSourceAfter);
            system.debug('countDestAfter '+countDestAfter);

            set<id> setCIMConfig = new set<id>();

            list<CIM_config__c> lstCIMConfig =  getCustomMetric(new set<id>{teamInstance}, movementType);
            if(lstCIMConfig!=null && lstCIMConfig.size() > 0){
                for(CIM_config__c cim:lstCIMConfig){
                    setCIMConfig.add(cim.id);
                }
            } 

            if(setCIMConfig.size()>0 ){
                list<CIM_Position_Metric_summary__c> summary = getCIMPositionMetricSummary(allPosition,teamInstance,setCIMConfig);
                boolean isAutoApproved = false;
                system.debug('summary '+summary);
                if(crRequest!=null){
                    isAutoApproved = crRequest.Is_Auto_Approved__c;
                }
                
                
                metricTable = processCustomMetric(summary,source,destination,sourceZIPterritoryCount,destinationZIPterritoryCount, crStatus,mapPositionandCode, pageName,isAutoApproved);                             
                    
                try{
                    system.debug('crRequest '+crRequest);
                    if(crRequest!=null){
                        Boolean isFutureActive  =false;
                        string alignmentPeriod  ='';
                        alignmentPeriod = getTeamInstanceById(teamInstance).Alignment_Period__c;                        
                        isFutureActive = (crRequest.Change_Effective_Date__c > system.today())?true:false;                        

                         if(!(alignmentPeriod == SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE && isFutureActive)){
                            
                            update summary;
                         }else{
                            system.debug('Future Request');
                         }
                    }
                }
                catch(Exception ex){
                    system.debug('Error in processCustomMetric '+ex.getStackTraceString());
                }

            }   	
        }
        return metricTable;    
    }

    public static AggregateResult countSourcePosition(list<string> sourceZIPs, list<string> impactedZIPs, set<id> counterpartTeamInstance, string strAssignmentStatus, string crStatus){
    	system.debug('strAssignmentStatus '+strAssignmentStatus);
        AggregateResult aggregateCountSource=null;
        
        //Adding seprate rejected case because after rejecteing ZIP will remain as it was earlier.
        if(crStatus=='Rejected' || crStatus=='Cancelled'){
            aggregateCountSource =  [select COUNT_distinct(Position__c) sourceAfter from Position_Geography__c where Assignment_status__c=:strAssignmentStatus and Team_Instance__c in:counterpartTeamInstance and (geography__r.Name in :sourceZIPs)];

        }else{
            aggregateCountSource =  [select COUNT_distinct(Position__c) sourceAfter from Position_Geography__c where Assignment_status__c=:strAssignmentStatus and Team_Instance__c in:counterpartTeamInstance and geography__r.Name in :sourceZIPs and Geography__r.Name not in :impactedZIPs ];
        }
    	system.debug('aggregateCountSource '+aggregateCountSource);
        return aggregateCountSource;
    	
    }

    public static AggregateResult countDestinationPosition(list<string> destinationZIPs, list<string> impactedZIPs, set<id> counterpartTeamInstance, string strAssignmentStatus, string crStatus){
    	AggregateResult aggregateCountDestination=null;
        if(crStatus=='Rejected' || crStatus=='Cancelled'){
            aggregateCountDestination = [select COUNT_distinct(Position__c) destinationAfter from Position_Geography__c where Assignment_status__c=:strAssignmentStatus and Team_Instance__c in: counterpartTeamInstance and (Geography__r.name in :destinationZIPs ) ];

        }else{
            aggregateCountDestination = [select COUNT_distinct(Position__c) destinationAfter from Position_Geography__c where Assignment_status__c=:strAssignmentStatus and Team_Instance__c in: counterpartTeamInstance and (Geography__r.name in :destinationZIPs  or Geography__r.Name  in :impactedZIPs  ) ];
        }
        
        
    	system.debug('aggregateCountDestination '+aggregateCountDestination);
        return aggregateCountDestination;
    }


    public static list<Position_Geography__c> getZIPbyPositionId(set<id> positionId,string teamInstance){
        string strAssignmentStatus = getAssignmentStatusbyTeamInstance(teamInstance);
    	string strQuery = 'select id,'+namespace+'geography__r.Name, '+namespace+'position__c from '+namespace+'position_geography__c where '+namespace+'position__c in:positionId and '+namespace+'Assignment_status__c=:strAssignmentStatus and '+namespace+'Team_instance__c=:teamInstance  ';

        list<Position_Geography__c> lstposGeo = Database.query(strQuery);
        return lstposGeo;
    }

    public static list<CIM_Position_Metric_summary__c> getCIMPositionMetricSummary(set<id> allPosition, string BaseTeamInstance,set<id> cimId){
    	list<CIM_Position_Metric_summary__c> BasesourceSummary = [select id, Original__c,Proposed__c,Approved__c,Position_Team_Instance__r.Position_id__c,CIM_Config__r.Attribute_API_Name__c,CIM_Config__r.Display_Name__c,CIM_Config__r.Enforcable__c,Position_Team_Instance__r.Position_id__r.Name  from CIM_Position_Metric_summary__c 
                                                                  where Position_Team_Instance__r.Position_id__c in:allPosition and Team_instance__c =:BaseTeamInstance and Cim_config__c in:cimId];//

                
        return BasesourceSummary;
    }

    public static list<CIM_config__c> getCustomMetric(set<id> teamInstance, string movementType){
        list<CIM_config__c> lstCIMConfig = [select id,name from CIM_config__c where Is_Custom_Metric__c=true and Team_Instance__c in:teamInstance and Enable__c=true and Change_Request_Type__r.CR_Type_Name__c like :movementType];
        return lstCIMConfig;

    }
    

    public static string processCustomMetric(list<CIM_Position_Metric_summary__c> cimPositionSummary, string source, string destination, integer countSourceAfter,integer countDestAfter, string crStatus, map<string,Position__c> mapPositionandCode,string pageName, boolean isAutoApproved ){
    	list<ImpactedData> lstimpData = new list<ImpactedData>();
    	map<string, map<string, list<ImpactedData>>> mapPositionKPIData = new map<string,map<string,list<ImpactedData>>>();
    	map<string, list<ImpactedData>> kpiImpactedData = new map<string, list<ImpactedData>>();
    	map<string, list<ImpactedData>> positionImpactedData = new map<string, list<ImpactedData>>();
    	list<string> lstCIMDisplayName = new list<string>();
    	map<id,string> mapCIMDisplayName = new map<id,string>();
    	system.debug('before cimPositionSummary '+cimPositionSummary);
        for(CIM_Position_Metric_summary__c CIMSummary:cimPositionSummary){
        	system.debug('before CIMSummary.Proposed__c '+CIMSummary.Proposed__c);
        	ImpactedData impData = new ImpactedData();
            impData.original = decimal.valueOf(CIMSummary.Original__c);
            system.debug('impData.original '+impData.original);
            impData.isMetricEnforced  =CIMSummary.CIM_Config__r.Enforcable__c;
            impData.metricName = CIMSummary.CIM_Config__r.Display_Name__c;
            mapCIMDisplayName.put(CIMSummary.CIM_Config__c, CIMSummary.CIM_Config__r.Display_Name__c);
            impData.positionName = CIMSummary.Position_Team_Instance__r.Position_id__r.Name;
            
            if(CIMSummary.Position_Team_Instance__r.Position_id__c==source){
            	if(crStatus=='Pending' || crStatus=='View' || crStatus=='Rejected'){

            		CIMSummary.Proposed__c  = string.valueOf(countSourceAfter);
            	}else if(crStatus=='Approved'){
                    //Used below case because in case of Auto approved CR, we need to update proposed values as well
                    if(isAutoApproved){
                        CIMSummary.Proposed__c  = string.valueOf(countSourceAfter);
                    }
            		CIMSummary.Approved__c  = string.valueOf(countSourceAfter);
            	}
               
                impData.current = decimal.valueOf(CIMSummary.Proposed__c);
                impData.positionType = 'Source';
                positionLabelList[0] = impData.positionName;
                
            }
            if(CIMSummary.Position_Team_Instance__r.Position_id__c==destination){
            	if(crStatus=='Pending' || crStatus=='View' || crStatus=='Rejected'){
	                CIMSummary.Proposed__c  = string.valueOf(countDestAfter);
	            }else if(crStatus=='Approved'){
            		CIMSummary.Approved__c  = string.valueOf(countDestAfter);
            	}
                impData.current = decimal.valueOf(CIMSummary.Proposed__c);
                impData.positionType = 'Destination';
                positionLabelList[1] = impData.positionName;
            }
            system.debug('after CIMSummary.Proposed__c '+CIMSummary.Proposed__c);        	
            
            //system.debug('impData before '+impData);
            impData.Impact = impData.original- decimal.valueOf(CIMSummary.Proposed__c);            
            
            impData.TextColor = AlignmentUtility.GraudrailColor(impData.Current ,impData.Impact, RequestUtilityClass.getCIMConfigDatabyCIMID(CIMSummary.CIM_Config__c));
            impData.graudrailToolTipMessage = '';
            impData.graudrailRange='';
            impData.errorType = AlignmentUtility.GraudrailType(impData.textColor);
                      
            lstimpData.add(impData);   

            if(positionImpactedData.containsKey(CIMSummary.Position_Team_Instance__r.Position_id__r.Name)){
            	list<ImpactedData> tempImpactData = positionImpactedData.get(CIMSummary.Position_Team_Instance__r.Position_id__r.Name);
            	tempImpactData.add(impData);
            	positionImpactedData.put(CIMSummary.Position_Team_Instance__r.Position_id__r.Name, tempImpactData);

            } else{
            	positionImpactedData.put(CIMSummary.Position_Team_Instance__r.Position_id__r.Name, new list<ImpactedData>{impData});
            }                 
        }       
        return generateMetricTable(positionImpactedData, mapCIMDisplayName,mapPositionandCode,pageName);
    }

    /*
    @author : Arun Kumar
    @date : 22 Dec 2017  
    @description : This method will return the assignment status bases on Team Instance Alignment Period
    @param1: TeamInstance SF Id
    @return : String
    */
    private static string generateMetricTable(map<string,list<ImpactedData>> positionImpactedData, map<id,string> mapCIMDisplayName, map<string,position__c> mapPositionandCode, string pageName){
    	
    	string strTable = '<table cellpadding="1" cellspacing="1" border="1" id="table-example-1" style="border-top:none !important;">  <thead> <tr>';
        if(pageName==CHANGE_REQUEST){
            strTable +='<th rowspan="2" ></th>';

        }else{

            strTable +='<th rowspan="2" >Select</th>';
            strTable +='<th rowspan="2" >Territory Code</th>';
            strTable +='<th rowspan="2" >Territory Name</th>';
        }       

        for(Id cimName:mapCIMDisplayName.keySet()){        	
        	strTable +='<th colspan="2" style="border-bottom: 0px !important; text-align: center; border-left: 1px solid #d8dde6;">'+ mapCIMDisplayName.get(cimName)+'</th>';
        }
        strTable +='</tr>';
        strTable +='<tr>';
        for(Id cimName:mapCIMDisplayName.keySet()){        	
        	strTable +='<th style="border-left: 1px solid #d8dde6; text-align: center;">Original</th>';
        	strTable +='<th style="border-left: 1px solid #d8dde6; text-align: center;">Proposed</th>';
        	
        }
        strTable +='</tr>';
        strTable+='</thead>';
        strTable+='<tbody>';
        string positionType='';
        //looping on list coz in order to maintain source at 1st row in the table format
        for(string positionkey :positionLabelList){
        	
        	strTable +='<tr>';
            //Added below condition coz on CR details page we don't have different col for position type and position code
            if(pageName==CHANGE_REQUEST){
                 strTable +='<td>@@@@@</td>';

            }else if(pageName==ALIGNMENT_MODULE){
                strTable +='<td style="border-right: 1px solid #d8dde6;">@@@@@</td>';
                strTable +='<td style="border-right: 1px solid #d8dde6;">'+mapPositionandCode.get(positionkey).Client_Position_Code__c+'</td>';
                strTable +='<td style="border-right: 1px solid #d8dde6;">' + positionkey+'</td>';

            }
        	
        	list<ImpactedData> impkpi = positionImpactedData.get(positionkey);
        	if(impkpi!=null){
        		for(ImpactedData kpiData:impkpi){
        			if(kpiData.positionName==positionkey){
        				positionType = kpiData.positionType;
        			}

        			strTable +='<td style=" text-align: right; border-left: 1px solid #d8dde6; padding-right: 25px;" >'+kpiData.original+'</td>';
        			strTable +='<td style=" text-align: right; border-left: 1px solid #d8dde6; padding-right: 25px;" >'+kpiData.current+'</td>';

        		}
                //Added below condition coz  table structure is diffrent for Alignment Page and CR detail Page
                //in CR detail page we are showing Position Type(source/Destination) along with Position Name
                // in CR detail Page = Source : Albany_NY in one col
                if(pageName==CHANGE_REQUEST){
                     strTable = strTable.replace('@@@@@', positionType +' : '+positionkey);
                }
                else if(pageName==ALIGNMENT_MODULE){
                    strTable = strTable.replace('@@@@@', positionType);
                }
        	}
        	strTable +='</tr>';
        }
        strTable+='</tbody></table>';       

    	return strTable;
    }

   


    /*@author : Arun Kumar
    @date : 22 Dec 2017  
    @description : This method will return the assignment status bases on Team Instance Alignment Period
    @param1: TeamInstance SF Id
    @return : String
    */    
    public static String getAssignmentStatusbyTeamInstance(id teamInstance){
        String alignmentPeriod='';
        String assignmentStatus='';

        Team_Instance__c objTeamInstance= SalesIQUtility.getTeamInstanceById(teamInstance);
        if(!String.isBlank(objTeamInstance.id)){
             alignmentPeriod = objTeamInstance.Alignment_Period__c;
        }
        else{
             alignmentPeriod = SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE;
        }

        if(alignmentPeriod == SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE){
            assignmentStatus = SalesIQGlobalConstants.POSITION_GEOGRAPHY_ACTIVE;
        }else{
            assignmentStatus = SalesIQGlobalConstants.POSITION_GEOGRAPHY_FUTURE_ACTIVE;
        }

        return assignmentStatus;
    }

    /*@author : Arun Kumar
    @date : 26 Dec 2017  
    @description : This method will return the mapping record of Team Instance. This table must have one record for one team Instance
    @param1: This is the teaminstance whose alignment is loaded on alignment page
    
    @return : list<Team_Instance_Mapping__c> list of Team_Instance_Mapping__c;
    */
    public static list<Team_Instance_Mapping__c> getTeamInstanceMapping(set<id> teamInstance){        
        list<Team_Instance_Mapping__c> lstTeamInstanceMapping = [select Controlling_Team_Instance__c,Team_Instance1__c,Team_Instance2__c from Team_Instance_Mapping__c where Controlling_Team_Instance__c in:teamInstance];
        
        return lstTeamInstanceMapping;

    }  

     public static Team_Instance__c getTeamInstanceById(String teamInstanceId){
        Team_Instance__c teamInstance = null;
        list<String> TEAM_READ_LIST = new list<String>{'Type__c', 'Effective_End_Date__c', 'Controlling_Team__c'};
        list<String> TEAM_INSTANCE_READ_FIELD = new list<String>{'Base_team_Instance__c','Alignment_Type__c','Alignment_Period__c','IC_EffstartDate__c','IC_EffEndDate__c', 'team__c', 'ReadOnlyCreatePosition__c', 'ReadOnlyDeletePosition__c', 'ReadOnlyEditPosition__c', 'Restrict_Hierarchy_Change__c', 'IsAllowedManageAssignment__c', 'Scenario__c'};
        if(SecurityUtil.checkRead(team_instance__c.SObjectType, TEAM_INSTANCE_READ_FIELD, false) && SecurityUtil.checkRead(team__c.SObjectType, TEAM_READ_LIST, false) && SecurityUtil.checkRead(Scenario__c.SObjectType, new list<String>{'CR_Tracking__c'}, false)){
            list<Team_Instance__c> lstTeamInstance = [select id, Base_team_Instance__c, Alignment_Type__c, Alignment_Period__c,team__r.Controlling_Team__c, IC_EffstartDate__c,IC_EffEndDate__c, team__c, team__r.type__c, team__r.Effective_End_Date__c, ReadOnlyCreatePosition__c, ReadOnlyDeletePosition__c, ReadOnlyEditPosition__c, Restrict_Hierarchy_Change__c, IsAllowedManageAssignment__c, Scenario__c, Scenario__r.CR_Tracking__c from team_instance__c where id =: teamInstanceId];
            for(Team_Instance__c TI: lstTeamInstance){
                teamInstance = TI;
            }
        }
        return teamInstance;
    } 

    /*
    * Method to populate list of position based on Selected position and TeamInstance. 
    * This method is used in Change Request Trigger Handler
    * @param selectedPosId set of positionid    
    * @return  list<position__c> list of position
    */ 
    public static list<position__c> getPositionById(set<id> posId){                
        list<position__c> lstPosition = [Select Id, Name, Inactive__c,Parent_Position__c, Team_id__r.Name, Client_Position_code__c from Position__c  where id in:posId ];  
        
        return lstPosition;
    }


     public class ImpactedData{
        public Decimal current {get;set;}
        public Decimal original {get;set;}
        public Decimal impact {get;set;}    
        public Boolean isMetricEnforced{get;set;}
        public String textColor {get;set;}
        public String metricName{get;set;}
        public String positionName{get;set;}
        public String positionType{get;set;}
        public String errorType{get;set;}
        public string graudrailToolTipMessage {get;set;}
        public string graudrailRange {get;set;}

    }

}