global class OverlayBasePositionWrapper {
    public Position_Team_Instance__c basePositionTeamInstance {get; set;}
    public string relatedOverlayPositionName{get;set;}
    public string status {get;set;}
    public boolean isDisabled {get;set;}
    public boolean isChecked {get;set;}
}