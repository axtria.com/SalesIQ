@isTest
private class TestSalesIQTreeAndBreadcrumbCtrl 
{
    static testMethod void testbreadrumb() 
    {
        String moduleName = 'Employee Universe';
        TriggerContol__c tc = new TriggerContol__c();
            tc.Name = 'SecurityUtil';
            tc.IsStopTrigger__c = true ;
            insert tc;

            TriggerContol__c tc1 = new TriggerContol__c();
            tc1.Name = 'ScenarioTrigger';
            tc1.IsStopTrigger__c = true ;
            insert tc1;
        Team__c team = TestDataFactory.createTeam('Base');
        insert team;
        Team__c team2 = TestDataFactory.createTeam('Overlay');
        team2.Controlling_Team__c = team.Id;
        insert team2;

        Team_Instance__c team_instance1 = TestDataFactory.CreateTeamInstance(team.Id,'Zip','',Date.today()-100,Date.today()+100);
        insert team_instance1;

        Team_Instance__c team_instance = TestDataFactory.CreateTeamInstance(team2.Id,'Zip','',Date.today()-100,Date.today()+100);
        team_instance.Base_team_Instance__c = team_instance1.Id;
        insert team_instance;

        Position__C pos = CallPlanTestDataFactory.createRegionPosition(team);
        pos.hierarchy_level__C = '5';
        insert pos;
        Position__C pos1 = CallPlanTestDataFactory.createPositionParent(pos,team);
        pos1.hierarchy_level__C = '4';
        insert pos1;
        Position__C pos2 = CallPlanTestDataFactory.createPosition(pos1,team2);
        pos2.hierarchy_level__C = '3';
        insert pos2;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User usr = TestDataFactory.createUser(p.Id);
        insert usr;
        System.runAs(usr)
        { 
        
            PermissionSet ps = [SELECT Id, Name From PermissionSet WHERE Name = 'SalesIQ_HO'];
            insert new PermissionSetAssignment(AssigneeId = usr.Id, PermissionSetId = ps.Id );
            User_Access_Permission__c objUserAccessPerm = TestDataFactory.createUserAccessPerm(pos.Id,usr.Id,team_instance.Id);
            insert objUserAccessPerm;
                
                test.startTest();
                SalesIQTreeAndBreadcrumbCtrl.getOrgNamespace();
                Map<String,List<Team_Instance__c>> team_instance_map = SalesIQTreeAndBreadcrumbCtrl.getBreadcrumbTeamInstance(moduleName,'');
                List<String> positions = SalesIQTreeAndBreadcrumbCtrl.getParentPosition(pos1.Id,team_instance.Id, moduleName, '');
                List<String> positions2 = SalesIQTreeAndBreadcrumbCtrl.getParentPosition(pos2.Id,team_instance.id, moduleName, '');
                List<String> positions3 = SalesIQTreeAndBreadcrumbCtrl.getParentPosition(pos.Id,team_instance.Id, moduleName, '');
                SalesIQTreeAndBreadcrumbCtrl.SalesIQPositionHierarchyWrapper userRoles = SalesIQTreeAndBreadcrumbCtrl.getUserRoles(team_instance.Id,moduleName,'',new List<String>(),'','');
                SalesIQTreeAndBreadcrumbCtrl.SalesIQPositionHierarchyWrapper findPositions = SalesIQTreeAndBreadcrumbCtrl.findByName('a',team_instance.Id,moduleName,'');
                SalesIQTreeAndBreadcrumbCtrl.SalesIQPositionHierarchyWrapper userRoles2 = SalesIQTreeAndBreadcrumbCtrl.getUserRoles(team_instance.Id,moduleName,'',new List<String>(),'','');
                SalesIQTreeAndBreadcrumbCtrl.SalesIQPositionHierarchyWrapper findPositions2 = SalesIQTreeAndBreadcrumbCtrl.findByName('a',team_instance.Id,moduleName,'');
                SalesIQTreeAndBreadcrumbCtrl.SalesIQPositionHierarchyWrapper userRoles3 = SalesIQTreeAndBreadcrumbCtrl.getUserRoles('',moduleName,'',new List<String>(),'','');
                SalesIQTreeAndBreadcrumbCtrl.SalesIQPositionHierarchyWrapper findPositions3 = SalesIQTreeAndBreadcrumbCtrl.findByName('a','',moduleName,'');
                test.stopTest();
        } 




    } 

    static testMethod void testbreadrumb2() 
    {
        String moduleName = 'Employee Universe';
        TriggerContol__c tc = new TriggerContol__c();
            tc.Name = 'SecurityUtil';
            tc.IsStopTrigger__c = true ;
            insert tc;

            TriggerContol__c tc1 = new TriggerContol__c();
            tc1.Name = 'ScenarioTrigger';
            tc1.IsStopTrigger__c = true ;
            insert tc1;
        Team__c team = TestDataFactory.createTeam('Base');
        insert team;
        Team__c team2 = TestDataFactory.createTeam('Overlay');
        team2.Controlling_Team__c = team.Id;
        insert team2;

        Team_Instance__c team_instance1 = TestDataFactory.CreateTeamInstance(team.Id,'Zip','',Date.today()-100,Date.today()+100);
        insert team_instance1;

        Team_Instance__c team_instance = TestDataFactory.CreateTeamInstance(team2.Id,'Zip','',Date.today()-100,Date.today()+100);
        team_instance.Base_team_Instance__c = team_instance1.Id;
        insert team_instance;

        Position__C pos = CallPlanTestDataFactory.createRegionPosition(team);
        pos.Team_Instance__c = team_instance1.Id;
        pos.Is_Unassigned_Position__c  =false;
        pos.Hierarchy_Level__C = '5';
        insert pos;
        Position__C pos1 = CallPlanTestDataFactory.createPositionParent(pos,team);
        pos1.Team_Instance__c = team_instance1.Id;
        pos1.Is_Unassigned_Position__c  = false;
        pos1.hierarchy_level__C = '4';
        insert pos1;
        Position__C pos2 = CallPlanTestDataFactory.createPosition(pos1,team);
        pos2.Team_Instance__c = team_instance1.Id;
        pos2.Is_Unassigned_Position__c = false;
        pos2.hierarchy_level__c = '2';
        insert pos2;
        Position__C pos3 = CallPlanTestDataFactory.createPosition(pos2,team);
        pos3.Team_Instance__c = team_instance1.Id;
        pos3.Is_Unassigned_Position__c  = false;
        pos3.Hierarchy_Level__c = '1';
        insert pos3;
        Position__C pos4 = CallPlanTestDataFactory.createPosition(pos3,team);
        pos4.Team_Instance__c = team_instance1.Id;
        pos4.Is_Unassigned_Position__c  = false;
        insert pos4;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User usr = TestDataFactory.createUser(p.Id);
        insert usr;
        System.runAs(usr)
        { 
        
            PermissionSet ps = [SELECT Id, Name From PermissionSet WHERE Name = 'SalesIQ_HO'];
            insert new PermissionSetAssignment(AssigneeId = usr.Id, PermissionSetId = ps.Id );
            User_Access_Permission__c objUserAccessPerm2 = TestDataFactory.createUserAccessPerm(pos.Id,usr.Id,team_instance1.Id);
            insert objUserAccessPerm2;
                
                test.startTest();
                 
                Map<String,List<Team_Instance__c>> team_instance_map = SalesIQTreeAndBreadcrumbCtrl.getBreadcrumbTeamInstance(moduleName,'');
                List<String> positions = SalesIQTreeAndBreadcrumbCtrl.getParentPosition(pos1.Id,team_instance1.Id, moduleName, '');
                List<String> positions2 = SalesIQTreeAndBreadcrumbCtrl.getParentPosition(pos2.Id,team_instance1.id, moduleName, '');
                List<String> positions3 = SalesIQTreeAndBreadcrumbCtrl.getParentPosition(pos.Id,team_instance1.Id, moduleName, '');
                List<String> positions4 = SalesIQTreeAndBreadcrumbCtrl.getParentPosition(pos3.Id,team_instance1.Id, moduleName, '');
                List<String> positions5 = SalesIQTreeAndBreadcrumbCtrl.getParentPosition(pos4.Id,team_instance1.Id, moduleName, '');
                SalesIQTreeAndBreadcrumbCtrl.SalesIQPositionHierarchyWrapper userRoles = SalesIQTreeAndBreadcrumbCtrl.getUserRoles(team_instance1.Id,moduleName,'',new List<String>(),'','');
                SalesIQTreeAndBreadcrumbCtrl.SalesIQPositionHierarchyWrapper findPositions = SalesIQTreeAndBreadcrumbCtrl.findByName('a',team_instance1.Id,moduleName,'');
                SalesIQTreeAndBreadcrumbCtrl.SalesIQPositionHierarchyWrapper userRoles2 = SalesIQTreeAndBreadcrumbCtrl.getUserRoles(team_instance1.Id,moduleName,'',new List<String>(),'','');
                SalesIQTreeAndBreadcrumbCtrl.SalesIQPositionHierarchyWrapper findPositions2 = SalesIQTreeAndBreadcrumbCtrl.findByName('a',team_instance1.Id,moduleName,'');
                SalesIQTreeAndBreadcrumbCtrl.SalesIQPositionHierarchyWrapper userRoles3 = SalesIQTreeAndBreadcrumbCtrl.getUserRoles('',moduleName,'',new List<String>(),'','');
                SalesIQTreeAndBreadcrumbCtrl.SalesIQPositionHierarchyWrapper findPositions3 = SalesIQTreeAndBreadcrumbCtrl.findByName('a','',moduleName,'');
                test.stopTest();
        } 




    }  

    static testMethod void testbreadrumb3() 
    {
        String moduleName = 'Employee Universe';
        TriggerContol__c tc = new TriggerContol__c();
            tc.Name = 'SecurityUtil';
            tc.IsStopTrigger__c = true ;
            insert tc;

            TriggerContol__c tc1 = new TriggerContol__c();
            tc1.Name = 'ScenarioTrigger';
            tc1.IsStopTrigger__c = true ;
            insert tc1;
            test.startTest();
        try{
            Map<String,List<Team_Instance__c>> team_instance_map1 = SalesIQTreeAndBreadcrumbCtrl.getBreadcrumbTeamInstance(moduleName,''); 
            }
        catch(Exception e)
        {
            String message = e.getTypeName() + ': ' + e.getMessage() + '\n\n' + e.getStackTraceString() ;
            AuraHandledException ex = new AuraHandledException(message);
            ex.setMessage(message);
        }
        try{
            SalesIQTreeAndBreadcrumbCtrl.SalesIQPositionHierarchyWrapper findPositions = SalesIQTreeAndBreadcrumbCtrl.findByName('a','',moduleName,'');
        }catch(Exception e) {
            String message = e.getTypeName() + ': ' + e.getMessage() + '\n\n' + e.getStackTraceString() ;
            AuraHandledException ex = new AuraHandledException(message);
            ex.setMessage(message);
        }   
        try{
            List<String> positions3 = SalesIQTreeAndBreadcrumbCtrl.getParentPosition('a','b', moduleName, '');
        } catch(Exception e) {
            String message = e.getTypeName() + ': ' + e.getMessage() + '\n\n' + e.getStackTraceString() ;
            AuraHandledException ex = new AuraHandledException(message);
            ex.setMessage(message);
        }
        try{
            SalesIQTreeAndBreadcrumbCtrl.SalesIQPositionHierarchyWrapper userRoles = SalesIQTreeAndBreadcrumbCtrl.getUserRoles('',moduleName,'',new List<String>(),'','');
        }  catch(Exception e) {
            String message = e.getTypeName() + ': ' + e.getMessage() + '\n\n' + e.getStackTraceString() ;
            AuraHandledException ex = new AuraHandledException(message);
            ex.setMessage(message);
        }
        SalesIQTreeAndBreadcrumbCtrl.logException('',moduleName);

         
        test.stopTest();
    }
    
    static testMethod void pendingCRTest(){

        Team__c team = TestDataFactory.createTeam('Base');
        insert team;

        Team_Instance__c team_instance1 = TestDataFactory.CreateTeamInstance(team.Id,'Zip','',Date.today()-100,Date.today()+100);
        team_instance1.Alignment_Period__c = SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE;
        insert team_instance1;

        Position__C pos = CallPlanTestDataFactory.createRegionPosition(team);
        pos.Team_Instance__c = team_instance1.Id;
        insert pos;
        
        change_request__c cr = new  change_request__c();
        cr.source_position__c = pos.ID;
        cr.status__c = SalesIQGlobalConstants.REQUEST_STATUS_PENDING;
        //cr.change_effective_date__c = Date.newInstance(2018, 03, 01);
        cr.Team_Instance_ID__c = team_instance1.Id;
        cr.Request_Type_Change__c = SalesIQGlobalConstants.REQUEST_TYPE_CALL_PLAN;
        insert cr;

        SalesIQTreeAndBreadcrumbCtrl.getPendingChangeRequests(new list<String>{pos.ID}, SalesIQGlobalConstants.CALL_PLAN_MODULE);
        
        change_request__c cr1 = new  change_request__c();
        cr1.source_position__c = pos.ID;
        cr1.status__c = SalesIQGlobalConstants.REQUEST_STATUS_PENDING;
        //cr.change_effective_date__c = Date.newInstance(2018, 03, 01);
        cr1.Team_Instance_ID__c = team_instance1.Id;
        cr1.Request_Type_Change__c = SalesIQGlobalConstants.UPDATE_POSITION_ATTRIBUTES;
        insert cr1;
       
        SalesIQTreeAndBreadcrumbCtrl.getPendingChangeRequests(new list<String>{pos.ID}, SalesIQGlobalConstants.POSITION_UNIVERSE);
        
        change_request__c cr2 = new  change_request__c();
        cr2.source_position__c = pos.ID;
        cr2.status__c = SalesIQGlobalConstants.REQUEST_STATUS_PENDING;
        //cr.change_effective_date__c = Date.newInstance(2018, 03, 01);
        cr2.Team_Instance_ID__c = team_instance1.Id;
        cr2.Request_Type_Change__c = SalesIQGlobalConstants.EMPLOYEE_ASSIGNMENT;
        insert cr2;

        SalesIQTreeAndBreadcrumbCtrl.getPendingChangeRequests(new list<String>{pos.ID}, SalesIQGlobalConstants.EMPLOYEE_UNIVERSE);



    }
    
}