@IsTest 
public with sharing class TestAutocompctrl{
    static testMethod  void  TestfindFullname(){
        Autocompctrl obj = new Autocompctrl();
        
        Team__c Team = CallPlanTestDataFactory.createTeam('Base');
        insert Team;
        
        Team_Instance__c TeamInstace  =CallPlanTestDataFactory.CreateTeamInstance(Team,'ZIP');
        insert TeamInstace;
        
        //Create New
        Position__c sourcePosition = CallPlanTestDataFactory.createPosition(null, Team);
        insert sourcePosition;
        
        Position__c DestinationPos = CallPlanTestDataFactory.createPosition(sourcePosition,Team);
        insert DestinationPos;
       
        Autocompctrl.findFullname('Position__c',DestinationPos.name, 'Name','','',TeamInstace.id,Team.id,sourcePosition.id);
        system.assert(Team != null);
        
    }
}