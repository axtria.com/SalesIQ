/*****************************************************************************************************
    Name        :   ProcessCRAssignmentBatch.cls
    Description :   Batch Class to Update Related Objects of the Expired Position . 
                    This class updates the Position Accounts, Position Geographies of an Expired Position 
                    to that of an Unassigned Territory and End Date of the Position Employee.
    Params 		:	Change Request Id , CR Assignment Type
    Modified    :   28th March 2019 ,Lagnika Sharma | Updated constructor and methods updated for handling 
    				posGep, posAcc,posemp in cascade delete for bulk dat
*****************************************************************************************************/
global class ProcessCRAssignmentBatch implements Database.Batchable<sObject>, Database.AllowsCallouts 
{
	// deprecated variables
	global Id unassignedTerritory;
	global Id unassignedPosTeamInsId;
	global string crID;
	List<CR_Position__c> crPosList;
	global set<Id> processedRecIds;
	
	// currently using variables
	global Date endDate;
	List<Id> objList;
	String positionId;
	String teamInstance;
	global String query;
	global String type;
	global map<id,Team_Instance__c> DateTeamInstanceMap;
	global map<id,id> UnassignedPosMap;
	global set<Id> unassignedTerrIds;
	global map<id,id> UnassignedPosTeamInsMap;
	global Date processDate;
	global set<Id> inactivePosId;
	global double processDay;

	
	
	global ProcessCRAssignmentBatch(Change_Request__c changeReq,String assignmentType)
	{
		/*type = assignmentType;
		crID = changeReq.Id;
		objList = new List<Id>();
		positionId = changeReq.Source_Position__c;
		teamInstance = changeReq.Team_Instance_ID__c;
		unassignedTerritory = changeReq.Destination_Position__c;
		Position_Team_Instance__c unassignedPosTeamIns = [Select Id from Position_Team_Instance__c where Position_ID__c =: unassignedTerritory and Team_Instance_ID__c =: teamInstance limit 1];
		unassignedPosTeamInsId = unassignedPosTeamIns.Id;

		processedRecIds = new set<Id>();

		List<CR_Position__c> crPosList = [Select id,Effective_End_Date__c,Position__c from CR_Position__c where Position__c =: positionId limit 1];
		if(crPosList.size()>0)
		{
			endDate = crPosList[0].Effective_End_Date__c;
		}

		if(assignmentType == 'CR_Account')
		{
			List<CR_Account__c> scope = [Select id,Name,Account__c,Change_Request__c from CR_Account__c where Change_Request__c =: changeReq.Id];
			for(sObject tmp: scope)
			{
				CR_Account__c tmp1 = (CR_Account__c)tmp;
				objList.add(tmp1.Account__c);
			}
			query = 'Select id,Name,Account__c,Team_Instance__c,Position__c from Position_Account__c where Account__c in: objList and Position__c =: positionId and Team_Instance__c =: teamInstance and Assignment_Status__c != \''+SalesIQGlobalConstants.POSITION_ACCOUNT_INACTIVE+ '\'';

		}
		else if(assignmentType == 'CR_Geography')
		{
			List<CR_Geography__c> scope = [Select id,Name,Geography__c,Change_Request__c from CR_Geography__c where Change_Request__c =: changeReq.Id];
			for(sObject tmp: scope)
			{
				CR_Geography__c tmp1 = (CR_Geography__c)tmp;
				objList.add(tmp1.Geography__c);
			}
			query = 'Select id,Name,Geography__c,Team_Instance__c,Position__c from Position_Geography__c where Geography__c in: objList and Position__c =: positionId and Team_Instance__c =: teamInstance and Assignment_Status__c != \''+SalesIQGlobalConstants.POSITION_GEOGRAPHY_INACTIVE+'\'';
		}
		else if(assignmentType == 'CR_Employee')
		{
			List<CR_Employee_Assignment__c> scope = [Select id,Name,Employee_ID__c,Change_Request_ID__c from CR_Employee_Assignment__c where Change_Request_ID__c =: changeReq.Id];
			for(sObject tmp: scope)
			{
				CR_Employee_Assignment__c tmp1 = (CR_Employee_Assignment__c)tmp;
				objList.add(tmp1.Employee_ID__c);
			}
			query = 'Select id,Name,Effective_End_Date__c,Position__c,Employee__c from Position_Employee__c where Employee__c in: objList and Position__c =: positionId and Assignment_Status__c != \''+SalesIQGlobalConstants.EMPLOYEE_STATUS_INACTIVE+'\'';
		}*/
	}

	global ProcessCRAssignmentBatch(set<Id> scope,String assignmentType)
	{
		type = assignmentType;
		DateTeamInstanceMap = new map<id,Team_Instance__c>();
		UnassignedPosMap = new map<id,id>();
		UnassignedPosTeamInsMap = new map<id,id>();
		unassignedTerrIds = new set<Id>();
		inactivePosId = new set<Id>();
		inactivePosId = scope;

		if(TotalApproval__c.getValues('ProcessDay') != null){
            processDay = TotalApproval__c.getValues('ProcessDay').No_Of_Approval__c;
        }else{
            processDay = 0;
        }
        processDate = System.today().addDays(integer.valueOf(processDay));

        System.debug('processDate - '+processDate);
        System.debug('scope - '+scope);

		for(Team_Instance__c t : [select id, IC_EffStartDate__c, IC_EffEndDate__c,Alignment_Period__c from Team_Instance__c]){
            DateTeamInstanceMap.put(t.id,t);
        }
        for(Position__c ps : [select id,Team_Instance__c,Effective_End_Date__c from Position__c where Client_Position_Code__c=:SalesIQGlobalConstants.UNASSIGNED_TERRITORY_CODE]){
            unassignedTerrIds.add(ps.Id);
            if(!UnassignedPosMap.containsKey(ps.Team_Instance__c)){
                UnassignedPosMap.put(ps.Team_Instance__c,ps.id);
            }
        }
        for(Position_Team_Instance__c ps : [select id , Team_Instance_ID__c from Position_Team_Instance__c where Position_ID__c in : unassignedTerrIds]){
            if(!UnassignedPosTeamInsMap.containsKey(ps.Team_Instance_ID__c)){
                UnassignedPosTeamInsMap.put(ps.Team_Instance_ID__c,ps.id);
            }
        }
        System.debug('DateTeamInstanceMap -'+DateTeamInstanceMap);
        System.debug('UnassignedPosMap -'+UnassignedPosMap);
        System.debug('UnassignedPosTeamInsMap - '+UnassignedPosTeamInsMap);

		if(type == 'Position_Account__c'){
			query = 'Select Account_Alignment_Type__c,Account_Target_Type__c,Account__c,Assignment_Status__c,Change_Status__c,Comments__c,Effective_End_Date__c,Effective_Start_Date__c,Id,Metric1__c,Metric2__c,Metric3__c,Metric4__c,Metric5__c,Metric6__c,Metric7__c,Metric8__c,Metric9__c,Metric10__c,Metric_1__c,Name,Position_Team_Instance__c,Position__c,Segment_1__c,Team_Instance__c, Team_Instance__r.Alignment_Period__c,Position__r.Effective_End_Date__c From Position_Account__c where Effective_End_Date__c >=: processDate and Position__c in : inactivePosId order by LastModifiedDate desc';

		}else if(type == 'Position_Geography__c'){
			query = 'Select Assignment_Status__c,Change_Status__c,Restrict_Position_Geography_Trigger__c,Effective_End_Date__c,Effective_Start_Date__c,Geography__c,Id,Metric1__c,Metric2__c,Metric3__c,Metric4__c,Metric5__c,Metric6__c,Metric7__c,Metric8__c,Metric9__c,Metric10__c,Name,Position_Team_Instance__c,Position__c,Team_Instance__c, Team_Instance__r.Alignment_Period__c,Position__r.Effective_End_Date__c From Position_Geography__c where Effective_End_Date__c >=: processDate and Position__c in : inactivePosId order by LastModifiedDate desc';

		}else if(type == 'Position_Employee__c'){
			query = 'Select Id, Position__c, Employee__c, Position__r.Team_Instance__c, Position__r.Team_Instance__r.Alignment_Period__c,Position__r.Effective_End_Date__c From Position_Employee__c where Effective_End_Date__c >=: processDate and Employee__c != null and  Position__c in : inactivePosId order by LastModifiedDate desc';
			
		}
		else if(type == 'CR_Position__c' && scope.size() > 0){
        	query = 'select Future_Reporting_Position__c, Future_Reporting_Position__r.Client_Position_Code__c, position_team_instance__c, base_position_status__c, position_team_instance__r.team_instance_id__c, position__c, effective_end_date__c, position_name__c, Position_Change_String__c, Change_Request__r.Team_Instance_ID__c, Change_Request__r.destination_position__c, Change_Request__r.source_position__c, change_request__r.Request_Type_Change__c from CR_Position__c where Change_Request__c in: inactivePosId';
		}
		else if(type == 'CR_Position__c' && scope.size() == 0){
			// query all CR_Position__c and update position
			crID = SalesIQGlobalConstants.REQUEST_STATUS_APPROVED;
			endDate = System.today();
			query = 'select Future_Reporting_Position__c,Parent_Position__c, Position__c, Effective_end_date__c, Position_Change_String__c from CR_Position__c where Effective_Start_Date__c =: endDate and Change_Request__r.Status__c =: crID';
		}

	}



	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		System.debug('query -'+query);
		return Database.getQueryLocator(query);
	}   

	global void execute(Database.BatchableContext BC,List<sObject> scope)
	{

		System.debug('scope -'+scope.size());
		if(type == 'Position_Account__c'){
			updatePosAcc(scope);
		}else if(type == 'Position_Geography__c'){
			updatePosGeo(scope);
		}else if(type == 'Position_Employee__c'){
			updatePosEmp(scope);
		}	
		else if(type == 'CR_Position__c'){
        	updateHierarchyChange(scope);
		}
	} 

	global void finish(Database.BatchableContext BC){
	    /*if(type == 'CR_Geography'){
			//invoke sync with ESRI job to update ZIP terr on ESRI for deleted position.
			Id jobId = System.System.enqueueJob(new QueueESRIBatchProcess(new list<String>{crID}));
		}
		//Extent calculation in case of cascade delete assignments.
		if(processedRecIds.size() > 0){
			HandlerPosGeo.rollupExtentsOnTerritoryFuture(processedRecIds);
		}*/
	}


    // Update the Position Account
	public void updateHierarchyChange(List<CR_Position__c> scope)
	{
		//set<String> posIds = new Set<String>();
		Map<ID,String> crMap = new Map<ID,String>();

		for(CR_Position__c cp : scope){
        	crMap.put(cp.position__c,cp.future_reporting_position__c); 
		}
        list<Position__c> lstPos = SalesIQUtility.getPositionById(crMap.keySet());

    	for(Position__c pa :lstPos)
		{
			if(crMap.keySet().contains(pa.id)){
				pa.Parent_Position__c = crMap.get(pa.id);
		    }
		}
		if(scope.size()>0){
			update scope;
		}
	}


	// Update the Position Account
	public void updatePosAcc(List<Position_Account__c> scope)
	{
		list<Position_Account__c> PosAccntList=new list<Position_Account__c>();
		System.debug('processDay -'+processDay);
		Integer addDaysInDate = 0;
		if(processDay == 0)
			addDaysInDate = -1;
		
		for(Position_Account__c pa :scope)
		{
		    Date positionEndDate = pa.Position__r.Effective_End_Date__c;
			
            if(DateTeamInstanceMap.get(pa.Team_Instance__c).Alignment_Period__c == SalesIQGlobalConstants.FUTURE_TEAM_CYCLE_TYPE)
			    pa.Effective_Start_Date__c = positionEndDate.addDays(addDaysInDate);
            
            pa.Effective_End_Date__c = positionEndDate.addDays(addDaysInDate);
            PosAccntList.add(pa);

			Position_Account__c clonedPosAccntRecord = pa.clone(false,true,false,false);
            clonedPosAccntRecord.Position__c = UnassignedPosMap.get(pa.Team_Instance__c);
            clonedPosAccntRecord.Position_Team_Instance__c = UnassignedPosTeamInsMap.get(pa.Team_Instance__c);
            if(DateTeamInstanceMap.get(pa.Team_Instance__c).Alignment_Period__c == SalesIQGlobalConstants.FUTURE_TEAM_CYCLE_TYPE)
            	clonedPosAccntRecord.Effective_Start_Date__c = DateTeamInstanceMap.get(pa.Team_Instance__c).IC_EffstartDate__c;
            else
            	clonedPosAccntRecord.Effective_Start_Date__c = positionEndDate.addDays(1);
            
            clonedPosAccntRecord.Effective_End_Date__c = DateTeamInstanceMap.get(pa.Team_Instance__c).IC_EffEndDate__c;

            PosAccntList.add(clonedPosAccntRecord);
		}
		//update scope;
		upsert PosAccntList;
	}

	//Update Position Geography
	public void updatePosGeo(List<Position_Geography__c> scope)
	{
		list<Position_Geography__c> PosGeoList = new list<Position_Geography__c>();
		System.debug('processDay -'+processDay);
		Integer addDaysInDate = 0;
		if(processDay == 0)
			addDaysInDate = -1;
		
		for(Position_Geography__c pg :scope)
		{
		    Date positionEndDate = pg.Position__r.Effective_End_Date__c;
			
            if(DateTeamInstanceMap.get(pg.Team_Instance__c).Alignment_Period__c == SalesIQGlobalConstants.FUTURE_TEAM_CYCLE_TYPE)
			    pg.Effective_Start_Date__c = positionEndDate.addDays(addDaysInDate);
			
			pg.Effective_End_Date__c = positionEndDate.addDays(addDaysInDate);
            if(!pg.Restrict_Position_Geography_Trigger__c)
            	pg.Restrict_Position_Geography_Trigger__c = true;
            else
            	pg.Restrict_Position_Geography_Trigger__c = false;
            PosGeoList.add(pg);
            
			Position_Geography__c clonedPosGeoRecord = pg.clone(false,true,false,false);
            clonedPosGeoRecord.Position__c = UnassignedPosMap.get(pg.Team_Instance__c);
            clonedPosGeoRecord.Position_Team_Instance__c = UnassignedPosTeamInsMap.get(pg.Team_Instance__c);
            if(DateTeamInstanceMap.get(pg.Team_Instance__c).Alignment_Period__c == SalesIQGlobalConstants.FUTURE_TEAM_CYCLE_TYPE)
            	clonedPosGeoRecord.Effective_Start_Date__c = DateTeamInstanceMap.get(pg.Team_Instance__c).IC_EffstartDate__c;
            else
            	clonedPosGeoRecord.Effective_Start_Date__c = positionEndDate.addDays(1);
            clonedPosGeoRecord.Effective_End_Date__c = DateTeamInstanceMap.get(pg.Team_Instance__c).IC_EffEndDate__c;
            clonedPosGeoRecord.Restrict_Position_Geography_Trigger__c = true;

            PosGeoList.add(clonedPosGeoRecord);
		}
		//update scope;
		upsert PosGeoList;
	}

	//Update Position Employee
	public void updatePosEmp(List<Position_Employee__c> scope)
	{
		//Expiring the PositionEmployee records related to the Expired Positions 
        list<Position_Employee__c> PosEmployeeList=new list<Position_Employee__c>();
        Integer addDaysInDate = 0;
		if(processDay == 0)
			addDaysInDate = -1;
        for(Position_Employee__c pe : scope){
            Date positionEndDate = pe.Position__r.Effective_End_Date__c;
        	pe.Effective_End_Date__c = positionEndDate.addDays(addDaysInDate);
            PosEmployeeList.add(pe);
        }
        update PosEmployeeList;
		/*for(Position_Employee__c posEmp:scope)
		{
			posEmp.Effective_End_Date__c = endDate;
		}
		update scope;*/
	}
}