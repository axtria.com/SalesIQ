@IsTest
public with sharing class TestEmployeeStatusTrigger {
    
    
    static testmethod void InertEmployeeStatus(){
        ReadOnlyApplication__c setting  = new ReadOnlyApplication__c();
        setting.Update_Field_Status__c = false;
        setting.Update_HR_Status__c = false;
        insert setting;
        
        TriggerContol__c  triggerctrl =  new TriggerContol__c(IsStopTrigger__c=false , Name='AssignedEmployeeTrigger');
        insert triggerctrl;
        
        Employee__c emp = CallPlanTestDataFactory.createEmployee();
        insert emp;
        
        Employee_Status__c EmpStatus =  CallPlanTestDataFactory.CreateEmployeeStatus(emp,SalesIQGlobalConstants.EMPLOYEE_STATUS_ACTIVE, date.today()-10,date.today()+10);
        insert EmpStatus;
        
        test.StartTest();
        Employee_Status__c EmpStatusInactive =  CallPlanTestDataFactory.CreateEmployeeStatus(emp,SalesIQGlobalConstants.EMPLOYEE_STATUS_INACTIVE, date.today()-8,date.today()+10);
        insert EmpStatusInactive;
        
        Employee_Status__c EmpStatusInactiv2e =  CallPlanTestDataFactory.CreateEmployeeStatus(emp,SalesIQGlobalConstants.EMPLOYEE_STATUS_INACTIVE, date.today()-7,date.today()+9);
        insert EmpStatusInactiv2e;
        
        Employee_Status__c EmpStatusTerminate =  CallPlanTestDataFactory.CreateEmployeeStatus(emp,SalesIQGlobalConstants.EMPLOYEE_STATUS_TERMINATED, date.today()+11,date.today()+15);
        insert EmpStatusTerminate;
         
         test.StopTest();
        
        
    }
        
}