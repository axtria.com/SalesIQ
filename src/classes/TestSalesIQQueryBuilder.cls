@IsTest
public with sharing class TestSalesIQQueryBuilder {

	static testMethod void initSalesIQQueryBuilder(){
		//Creating custom setting test data
        ReadOnlyApplication__c setting  = new ReadOnlyApplication__c();
        setting.Alignment__c = false;
        setting.Secondary_Assignment_Alignment__c = true;
        insert setting;
        
        Organization_Master__c org = CallPlanTestDataFactory.createOrganizationMasterRecord();
        insert org;
        Country__c country = CallPlanTestDataFactory.createCountryMasterRecord(org.id);
        insert country;
        Geography_Type__c geotype = CallPlanTestDataFactory.createGeographyTypeRecord(country.id);
        insert geotype;
        
        
        
        list<DiplayColorCriteria__c> colorSettings = new list<DiplayColorCriteria__c>();
        colorSettings.add(new DiplayColorCriteria__c(Name='Error',Color__c='#F8CBAD'));
        colorSettings.add(new DiplayColorCriteria__c(Name='Normal',Color__c='#FFFFFF'));
        colorSettings.add(new DiplayColorCriteria__c(Name='Warning',Color__c='#ffE699'));
        insert colorSettings;
        
        Team__c baseTeam = new Team__c(Name='HTN',Type__c='Base',Effective_End_Date__c=Date.today().addMonths(50),Country__c=country.id);
        insert baseTeam;
        
        Team__c overlayTeam = new Team__c(Name='HTN Overlay',Type__c='Overlay',Controlling_Team__c= baseTeam.id,Country__c=country.id);
        insert overlayTeam;
        
        Workspace__c wrk = new Workspace__c();
        wrk.Workspace_Start_Date__c = Date.Today();
        wrk.Workspace_End_Date__c  = Date.today().addMonths(50);
        wrk.Name = 'Workspace 1';
        wrk.Workspace_Description__c = 'New Workspace';
        wrk.Country__c=country.id;
        insert wrk;
        
        
        Scenario__c scenario = new Scenario__c();
        scenario.CR_Tracking__c = false;
        scenario.Scenario_Name__c = 'HTN_Q3';
        scenario.Workspace__c = wrk.id;
        scenario.Team_Name__c = baseTeam.id;
        scenario.Request_Process_Stage__c = SalesIQGlobalConstants.READY;
        scenario.Scenario_Stage__c =    SalesIQGlobalConstants.COLLABORATION;     
        insert scenario;


        Team_Instance__c BaseTeamInstance = new Team_Instance__c();
        BaseTeamInstance.Name = 'HTN_Q1_2016';
        BaseTeamInstance.Alignment_Period__c = 'Current';
        BaseTeamInstance.Team__c = baseTeam.id;
        BaseTeamInstance.Alignment_Type__c = 'ZIP';
        BaseTeamInstance.isActiveCycle__c = 'Y';  
        BaseTeamInstance.Team_Cycle_Name__c = 'Current';
        BaseTeamInstance.Scenario__c  = scenario.id;  
        BaseTeamInstance.Team_Instance_Code__c = 'DI-00001'; 
        BaseTeamInstance.IC_EffstartDate__c = Date.today().addMonths(-1);
        BaseTeamInstance.IC_EffEndDate__c = Date.today().addYears(1);       
        BaseTeamInstance.Show_Custom_Metric__c=true;
        BaseTeamInstance.Geography_Type_Name__c = geotype.id;
        insert BaseTeamInstance;

    	Team_Instance_Object_Attribute__c teamInstAttrImpactTab =  new Team_Instance_Object_Attribute__c();
        teamInstAttrImpactTab.Interface_Name__c = 'Impact ZIP Tab';
        teamInstAttrImpactTab.Attribute_API_Name__c = 'Geography__r.Zip_Type__c';
        teamInstAttrImpactTab.Object_Name__c = 'Position_Geography__c';
        teamInstAttrImpactTab.Data_Type__c = 'Text';
        teamInstAttrImpactTab.Attribute_Display_Name__c = 'Label.GEO_Type';
        teamInstAttrImpactTab.isEnabled__c=true;
        teamInstAttrImpactTab.Enabled_Advance_Search__c = true;
        teamInstAttrImpactTab.Team_Instance__c = BaseTeamInstance.Id;
        teamInstAttrImpactTab.Display_Column_Order__c = 1;
        insert teamInstAttrImpactTab;

        Team_Instance_Object_Attribute__c teamInstAttrImpactTab2 =  new Team_Instance_Object_Attribute__c();
        teamInstAttrImpactTab2.Interface_Name__c = 'Impact ZIP Tab';
        teamInstAttrImpactTab2.Attribute_API_Name__c = 'Geography__r.Zip_Type__c';
        teamInstAttrImpactTab2.Object_Name__c = 'Position_Geography__c';
        teamInstAttrImpactTab2.Data_Type__c = 'Boolean';
        teamInstAttrImpactTab2.Attribute_Display_Name__c = 'Label.GEO_Type';
        teamInstAttrImpactTab2.isEnabled__c=true;
        teamInstAttrImpactTab2.Enabled_Advance_Search__c = true;
        teamInstAttrImpactTab2.Team_Instance__c = BaseTeamInstance.Id;
        teamInstAttrImpactTab2.Display_Column_Order__c = 1;
        insert teamInstAttrImpactTab2;

        Team_Instance_Object_Attribute__c teamInstAttrImpactTab3 =  new Team_Instance_Object_Attribute__c();
        teamInstAttrImpactTab3.Interface_Name__c = 'Impact ZIP Tab';
        teamInstAttrImpactTab3.Attribute_API_Name__c = 'Geography__r.Zip_Type__c';
        teamInstAttrImpactTab3.Object_Name__c = 'Position_Geography__c';
        teamInstAttrImpactTab3.Data_Type__c = 'Number';
        teamInstAttrImpactTab3.Attribute_Display_Name__c = 'Label.GEO_Type';
        teamInstAttrImpactTab3.isEnabled__c=true;
        teamInstAttrImpactTab3.Enabled_Advance_Search__c = true;
        teamInstAttrImpactTab3.Team_Instance__c = BaseTeamInstance.Id;
        teamInstAttrImpactTab3.Display_Column_Order__c = 1;
        insert teamInstAttrImpactTab3;
        
        SalesIQQueryBuilderCtrl obj = new SalesIQQueryBuilderCtrl();
        obj.interfaceName = 'Impact ZIP Tab';
        obj.teamInstanceId = BaseTeamInstance.Id;
        System.debug('--configJson ' + obj.configJson);
    }
}