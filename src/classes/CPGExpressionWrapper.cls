public class CPGExpressionWrapper {
    public string selectExpression{get;set;}
   // public string[] selectExpressionCategory;
    public Map<string,string> expressionCategory{get;set;}
    public string whereExpression{get;set;}
    public string groupByExpression{get;set;}
    public string columnalias{get;set;}
   // public string[] columnaliasCategory;
}