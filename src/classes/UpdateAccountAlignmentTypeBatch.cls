//This class will update the Account Alignmet Status for future active accounts when they become active.
//After updating the Account data, based on same Position Account, code will update the position geography metric data as well.
//
global class UpdateAccountAlignmentTypeBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
    
    list<string> TeamInstance=new  list<string>();
    list<Team_Instance__c> lsTeamInstance = new list<Team_Instance__c>();
    string countryID ='';
    
    global UpdateAccountAlignmentTypeBatch(){    	
        lsTeamInstance = SalesIQUtility.getTeamInstanceByAlignmentType('Hybrid');
         for(Team_Instance__c teamInst: lsTeamInstance){
                TeamInstance.add(teamInst.id);
                countryID = teamInst.Team__r.Country__c;
            }
     }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        system.debug('in start '+TeamInstance);
        Date changeReqDate = Date.Today();
       
        string soql = 'select id,Account__r.BillingPostalCode, Team_Instance__c, position__c, Account_Alignment_Type__c from Position_Account__c where Effective_Start_Date__c=:changeReqDate and  Team_Instance__c in : TeamInstance ' ;
        return Database.getQueryLocator(soql);
    }
    
     global void execute(Database.BatchableContext bc, List<Position_Account__c> scope){
         list<string> POSITION_ACCOUNT_UPDATE_ALIGNMENT_FIELD = new list<string>{'Account_Alignment_Type__c'};
        
        map<string, list<Position_Account__c>> mapTeamInstancePositionAccount = new map<string, list<Position_Account__c>>();
        set<id> setPosAcc = new set<id>();
        list<Position_Account__c> PositionAccountwithUpdatedAlignmentType = new list<Position_Account__c>();
        for(Position_Account__c PosAcc : scope){
           
            if(mapTeamInstancePositionAccount.containsKey(PosAcc.Team_Instance__c)){
                list<Position_Account__c> tempPosAcc = mapTeamInstancePositionAccount.get(PosAcc.Team_Instance__c);
                tempPosAcc.add(PosAcc);
                mapTeamInstancePositionAccount.put(PosAcc.Team_Instance__c, tempPosAcc);
                
            }else{
                mapTeamInstancePositionAccount.put(PosAcc.Team_Instance__c, new list<Position_Account__c>{ PosAcc});
            }
        }
        
        system.debug('mapTeamInstancePositionAccount '+mapTeamInstancePositionAccount);
        
        for(string TeamInstance:TeamInstance){
            list<Position_Account__c> tempPosAccount = mapTeamInstancePositionAccount.get(TeamInstance);
            for(Position_Account__c tempPosAcc: tempPosAccount){
                 setPosAcc.add(tempPosAcc.id);
            }
            PositionAccountwithUpdatedAlignmentType.addAll(ChangeRequestTriggerHandler.UpdateAccountAlignmentType(setPosAcc, TeamInstance));
            
        }
        system.debug('#### Execute method | scope : '+PositionAccountwithUpdatedAlignmentType);
        
        if(SecurityUtil.checkUpdate(Position_Account__c.SObjectType, POSITION_ACCOUNT_UPDATE_ALIGNMENT_FIELD, false)){
            update PositionAccountwithUpdatedAlignmentType;
        }
        
        if(PositionAccountwithUpdatedAlignmentType!=null && PositionAccountwithUpdatedAlignmentType.size()>0){           
            list<Position_geography__c> lstPositionGeography = new list<Position_geography__c>(); 
                        
            for(string TeamInstance:TeamInstance){
                list<Position_Account__c> tempPosAccount = mapTeamInstancePositionAccount.get(TeamInstance);
                for(Position_Account__c tempPosAcc: tempPosAccount){
                     setPosAcc.add(tempPosAcc.id);
                }
                
                ChangeRequestTriggerHandler.UpdateMetricData(TeamInstance,setPosAcc,lstPositionGeography,countryID);
                
            }
            //SPD-4290
    	    /*list<string> TEAM_INSTANCE_GEOGRAPHY_UPDATE_FIELD = new list<string>{'Metric1__c','Metric2__c','Metric3__c','Metric4__c','Metric5__c','Metric6__c','Metric7__c','Metric8__c','Metric9__c','Metric10__c'};            
            if(lstTIGeo!=null && lstTIGeo.size() > 0){
                if(SecurityUtil.checkUpdate(TEAM_INSTANCE_GEOGRAPHY__c.SObjectType, TEAM_INSTANCE_GEOGRAPHY_UPDATE_FIELD, false)){
                    update lstTIGeo;
                }
                
            }*/
            
            system.debug('after update lstPositionGeography' +lstPositionGeography);
            
            list<string> POSITION_GEOGRAPHY_UPDATE_FIELD = new list<string>{'Metric1__c','Metric2__c','Metric3__c','Metric4__c','Metric5__c','Metric6__c','Metric7__c','Metric8__c','Metric9__c','Metric10__c'};
            if(lstPositionGeography!=null && lstPositionGeography.size() > 0){
                 if(SecurityUtil.checkUpdate(Position_geography__c.SObjectType, POSITION_GEOGRAPHY_UPDATE_FIELD, false)){
                     update lstPositionGeography;
                 }
            }
        }
    }
    
     global void finish(Database.BatchableContext bc){
        system.debug('#### Finish method');
    }
    
   

}