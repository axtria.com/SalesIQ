@IsTest
public with sharing class TestPositionAccountTrigger {
    
    static testMethod void testPositionAccountInsert() {
        
        TriggerContol__c  triggerctrl =  new TriggerContol__c(IsStopTrigger__c=false , Name='PositionAccountTrigger');
        insert triggerctrl;
        
        Account acc = CallPlanTestDataFactory.CreateAccount('Direct Aligned','11788','Physician');    
        acc.type = 'Physician';
        insert acc;     
        
        Team__c accBasedTeam =  CallPlanTestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_AccountBased);      
        insert accBasedTeam;
        
        Team_Instance__c accBasedTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(accBasedTeam,SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT);
        insert accBasedTeamInstance;
        
        Position__c region = CallPlanTestDataFactory.createRegionPosition(accBasedTeam);
        region.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
        insert region;
        
        Position_Team_Instance__c regionTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(region, null, accBasedTeamInstance);
        regionTeamInstance.X_Max__c=-72.6966429900;
        regionTeamInstance.X_Min__c=-73.9625820000;
        regionTeamInstance.Y_Max__c=40.9666490000;
        regionTeamInstance.Y_Min__c=40.5821279800;
        insert regionTeamInstance;
        
        Position__c district = CallPlanTestDataFactory.createPositionParent(region,accBasedTeam);
        district.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
        insert district;
        
        Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, region, accBasedTeamInstance);
        districtTeamInstance.X_Max__c=-72.6966429900;
        districtTeamInstance.X_Min__c=-73.9625820000;
        districtTeamInstance.Y_Max__c=40.9666490000;
        districtTeamInstance.Y_Min__c=40.5821279800;
        insert districtTeamInstance;
        
        Position__c territory =  CallPlanTestDataFactory.createPosition(district,accBasedTeam);
        territory.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
        insert territory;
        
        Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, district, accBasedTeamInstance);
        territoryTeamInstance.X_Max__c=-72.6966429900;
        territoryTeamInstance.X_Min__c=-73.9625820000;
        territoryTeamInstance.Y_Max__c=40.9666490000;
        territoryTeamInstance.Y_Min__c=40.5821279800;
        insert territoryTeamInstance;
    
        
        /*Team_Instance_Account__c teamInstanceAcc = CallPlanTestDataFactory.createTeamInstanceAccount(acc,accBasedTeamInstance);
        insert teamInstanceAcc; 
*/
        
        Position_Account__c posAcc = CallPlanTestDataFactory.createPositionAccount(acc, territory, accBasedTeamInstance, territoryTeamInstance);
        
        
        test.startTest();       
        insert posAcc;
        list<Position_Team_Instance__c> acclatlng = [SELECT X_Max__c,X_Min__c,Y_Max__c,Y_Min__c from Position_Team_Instance__c where id =: territoryTeamInstance.Id];
        if(acclatlng[0].Y_Max__c != null){
        //system.assertEquals(40.5571389900 ,acclatlng[0].Y_Max__c );
        }
        test.stopTest();        
    
    }
    
    static testMethod void testPositionAccountUpdate() {
        
        TriggerContol__c  triggerctrl =  new TriggerContol__c(IsStopTrigger__c=false , Name='PositionAccountTrigger');
        insert triggerctrl;
        
        Account acc = CallPlanTestDataFactory.CreateAccount('Direct Aligned','11788','Physician');
        acc.type = 'Physician';
        insert acc; 
    
                    
         Team__c accBasedTeam =  CallPlanTestDataFactory.createTeam('AccountBased');        
        insert accBasedTeam;
        
        Team_Instance__c accBasedTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(accBasedTeam,'Account');
        insert accBasedTeamInstance;
        
        Position__c region = CallPlanTestDataFactory.createRegionPosition(accBasedTeam);  
        region.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;    
        insert region;
        
        Position_Team_Instance__c regionTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(region, null, accBasedTeamInstance);
        regionTeamInstance.X_Max__c=-72.6966429900;
        regionTeamInstance.X_Min__c=-73.9625820000;
        regionTeamInstance.Y_Max__c=40.9666490000;
        regionTeamInstance.Y_Min__c=40.5821279800;
        insert regionTeamInstance;
        
        Position__c district = CallPlanTestDataFactory.createPositionParent(region,accBasedTeam);
        district.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
        insert district;
        
        Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, region, accBasedTeamInstance);
        districtTeamInstance.X_Max__c=-72.6966429900;
        districtTeamInstance.X_Min__c=-73.9625820000;
        districtTeamInstance.Y_Max__c=40.9666490000;
        districtTeamInstance.Y_Min__c=40.5821279800;
        insert districtTeamInstance;
        
        Position__c territory =  CallPlanTestDataFactory.createPosition(district,accBasedTeam);
        territory.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
        insert territory;
        
        Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, district, accBasedTeamInstance);
        territoryTeamInstance.X_Max__c=-72.6966429900;
        territoryTeamInstance.X_Min__c=-73.9625820000;
        territoryTeamInstance.Y_Max__c=40.9666490000;
        territoryTeamInstance.Y_Min__c=40.5821279800;
        insert territoryTeamInstance;
        
                
        Position__c destTerr = CallPlanTestDataFactory.createPosition(district,accBasedTeam);
        destTerr.Name = 'Long Island West';
        destTerr.Client_Territory_Name__c = 'Long Island West';
        destTerr.Client_Position_Code__c = '1NE30012';
        destTerr.Client_Territory_Code__c='1NE30012';
        destTerr.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
        insert destTerr;
        
        Position_Team_Instance__c destTerrTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(destTerr, district, accBasedTeamInstance);
        destTerrTeamInstance.X_Max__c=-72.6966429900;
        destTerrTeamInstance.X_Min__c=-73.9625820000;
        destTerrTeamInstance.Y_Max__c=40.9666490000;
        destTerrTeamInstance.Y_Min__c=40.5821279800;
        insert destTerrTeamInstance;
        
        /*Team_Instance_Account__c teamInstanceAcc = CallPlanTestDataFactory.createTeamInstanceAccount(acc,accBasedTeamInstance);
        insert teamInstanceAcc; */

        Position_Account__c posAcc = CallPlanTestDataFactory.createPositionAccount(acc, territory, accBasedTeamInstance, territoryTeamInstance);
        
        
        test.startTest();       
        insert posAcc;
        posAcc.Position__c=destTerr.id;
        update posAcc;
        PositionAccountTriggerHandler.rollupExtentsOnTerritoryFromTalend(new list<Position_Account__c>{posAcc},new map<id,Position_Account__c>());
        list<Position_Team_Instance__c> acclatlng = [SELECT X_Max__c,X_Min__c,Y_Max__c,Y_Min__c from Position_Team_Instance__c where id =: territoryTeamInstance.Id];
        if(acclatlng[0].Y_Max__c != null){
        //system.assertEquals(40.5571389900 ,acclatlng[0].Y_Max__c );
        }
        test.stopTest();        
    
    }
    
        static testMethod void testPositionAccountDelete() {
        
        TriggerContol__c  triggerctrl =  new TriggerContol__c(IsStopTrigger__c=false , Name='PositionAccountTrigger');
        insert triggerctrl;
        
        Account acc = CallPlanTestDataFactory.CreateAccount('Direct Aligned','11788','Physician');
        acc.type = 'Physician';
        insert acc; 
                    
        Team__c accBasedTeam =  CallPlanTestDataFactory.createTeam('AccountBased');     
        insert accBasedTeam;
        
        Team_Instance__c accBasedTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(accBasedTeam,'Account');
        insert accBasedTeamInstance;
        
        Position__c region = CallPlanTestDataFactory.createRegionPosition(accBasedTeam);
        insert region;
        
        Position_Team_Instance__c regionTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(region, null, accBasedTeamInstance);
        regionTeamInstance.X_Max__c=-72.6966429900;
        regionTeamInstance.X_Min__c=-73.9625820000;
        regionTeamInstance.Y_Max__c=40.9666490000;
        regionTeamInstance.Y_Min__c=40.5821279800;
        insert regionTeamInstance;
        
        Position__c district = CallPlanTestDataFactory.createPositionParent(region,accBasedTeam);
        insert district;
        
        Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, region, accBasedTeamInstance);
        districtTeamInstance.X_Max__c=-72.6966429900;
        districtTeamInstance.X_Min__c=-73.9625820000;
        districtTeamInstance.Y_Max__c=40.9666490000;
        districtTeamInstance.Y_Min__c=40.5821279800;
        insert districtTeamInstance;
        
        Position__c territory =  CallPlanTestDataFactory.createPosition(district,accBasedTeam);  
        territory.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
        insert territory;
        
        Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, district, accBasedTeamInstance);
        territoryTeamInstance.X_Max__c=-72.6966429900;
        territoryTeamInstance.X_Min__c=-73.9625820000;
        territoryTeamInstance.Y_Max__c=40.9666490000;
        territoryTeamInstance.Y_Min__c=40.5821279800;
        insert territoryTeamInstance;
        

        
        /*Team_Instance_Account__c teamInstanceAcc = CallPlanTestDataFactory.createTeamInstanceAccount(acc,accBasedTeamInstance);
        insert teamInstanceAcc; */
        
        Account acc2 = CallPlanTestDataFactory.CreateAccount('Direct Aligned','11789','Physician');
        acc2.type = 'Physician';
        insert acc2;    
        
        Position_Account__c posAcc = CallPlanTestDataFactory.createPositionAccount(acc, territory, accBasedTeamInstance, territoryTeamInstance);
        Position_Account__c posAcc1 = CallPlanTestDataFactory.createPositionAccount(acc2, territory, accBasedTeamInstance, territoryTeamInstance);
        
        test.startTest();       
        insert posAcc;
        insert posAcc1;
        delete posAcc1;
        list<Position_Team_Instance__c> acclatlng = [SELECT X_Max__c,X_Min__c,Y_Max__c,Y_Min__c from Position_Team_Instance__c where id =: territoryTeamInstance.Id];
        if(acclatlng[0].Y_Max__c != null){
        //system.assertEquals(40.5571389900 ,acclatlng[0].Y_Max__c );
        }
        test.stopTest();        
    
    }
    
}