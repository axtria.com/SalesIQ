global class PrintMapMockGenerator implements HttpCalloutMock {

global string s;

    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response.
        // Set response values, and 
        // return response.
       if(s=='true')
       {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"jobStatus":"esriJobSucceeded","jobId":"esriJobSucceeded","Value":"esriJobSucceeded"}');
        res.setStatusCode(200);  
        return res;
       }
       else if(s=='false')
       {
           HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"jobStatus":"esriFailed"}');
        res.setStatusCode(200);
        return res; 
       }
       else
       {
           HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"jobStatus":"esriJobExecuting","jobId":"esriJobSucceeded","Value":"esriJobSucceeded"}');
        res.setStatusCode(200);  
        return res;
       }
       
    }
}