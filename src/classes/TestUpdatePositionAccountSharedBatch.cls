@IsTest
public class TestUpdatePositionAccountSharedBatch {
    
    static testmethod void updateSharedColumns(){  
        
        Organization_Master__c org = CallPlanTestDataFactory.createOrganizationMasterRecord();
        insert org;
        Country__c country = CallPlanTestDataFactory.createCountryMasterRecord(org.id);
        insert country;
        Geography_Type__c geotype = CallPlanTestDataFactory.createGeographyTypeRecord(country.id);
        insert geotype;

        Team__c baseTeam = CallPlanTestDataFactory.createTeam('Base');
        baseTeam.Country__c=country.id;
        insert baseTeam;    

        Workspace__c wrkspc = CallPlanTestDataFactory.createWorkspace();
        wrkspc.Country__c=country.id;
        insert wrkspc;

        Scenario__c scenario = new Scenario__c();
        scenario.CR_Tracking__c = true;
        scenario.Scenario_Name__c = 'HTN_Q3';
        scenario.Workspace__c = wrkspc.id;
        scenario.Team_Name__c = baseTeam.id;
        scenario.Request_Process_Stage__c = SalesIQGlobalConstants.READY;     
        scenario.Scenario_Stage__c = SalesIQGlobalConstants.COLLABORATION;   
        scenario.Scenario_Status__c = 'Active';
        insert scenario;  

        Team_Instance__c baseTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(baseTeam,'Hybrid');        
        baseTeamInstance.Geography_Type_Name__c = geotype.id;
        baseTeamInstance.Scenario__c = scenario.id;
        insert baseTeamInstance;

        Position__c  parentParent = null;


        Position__c territory  = CallPlanTestDataFactory.createPosition(parentParent,baseTeam);
        territory.Team_Instance__c = baseTeamInstance.id;
        insert territory;

        Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, parentParent, BaseTeamInstance);       
        insert territoryTeamInstance;

        Position__c destTerritory  = CallPlanTestDataFactory.createDestinationPosition(parentParent,baseTeam);
        destTerritory.Team_Instance__c = baseTeamInstance.id;
        insert destTerritory;

        Position_Team_Instance__c dstinationTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(destTerritory, parentParent, BaseTeamInstance);       
        insert dstinationTeamInstance;

        Account acc = new Account();
        acc.Name = '11788';
        acc.FirstName__c = 'Abby';
        acc.BillingCity='NJ';
        acc.BillingState='NY';
        acc.AccountNumber='11788';
        acc.type = 'Physician';
        acc.AccountType__c = 'Physician';
        acc.BillingPostalCode = '11788';
        acc.Speciality__c = '11788Oncology';
        acc.Alignment_Type__c = 'Direct Aligned';
        acc.BillingLatitude = 40.5571389900;
        acc.BillingLongitude = -74.2123329900;
        acc.External_Country_Id__c=country.id;
        insert acc;


        list<Position_Account__c> lstAllPositionAccount = new list<Position_Account__c>();

        Position_Account__c positionAcc = CallPlanTestDataFactory.createPositionAccount(acc, territory, baseTeamInstance, territoryTeamInstance);
        lstAllPositionAccount.add(positionAcc);

        Position_Account__c positionAcc2 = CallPlanTestDataFactory.createPositionAccount(acc, destTerritory, baseTeamInstance, dstinationTeamInstance);
        lstAllPositionAccount.add(positionAcc2);

        insert lstAllPositionAccount;
        
        Test.startTest();
        UpdatePositionAccountSharedBatch batchupdate = new UpdatePositionAccountSharedBatch(baseTeamInstance.id);
        Database.executeBatch(batchupdate);
        UpdatePositionAccountSharedBatch batchupdate1 = new UpdatePositionAccountSharedBatch(baseTeamInstance.id,new list<string>{acc.id});
        Database.executeBatch(batchupdate1);
        Test.stopTest();
        
    }
}