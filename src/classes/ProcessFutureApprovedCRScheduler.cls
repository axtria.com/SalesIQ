/************************************************************************************************
Name			: 	ProcessFutureApprovedCRScheduler
Description 	: 	Scheduler class to process approved future active ZIP movement Change requests
Author			: 	Raghvendra Rathore | 7th March 2016
************************************************************************************************/
global class ProcessFutureApprovedCRScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
		try{
			// Security check of query at Alignment List View component
            List<String> CHANGE_REQUEST_READ_FIELDS = new List<String>{'Execution_Status__c', 'Change_Effective_Date__c', 'Request_Type_Change__c', 'Status__c','Destination_Position__c', 'Source_Position__c', 'Source_Positions__c', 'Destination_Positions__c'};
			SecurityUtil.checkRead(Change_Request__c.SObjectType, CHANGE_REQUEST_READ_FIELDS, false) ;
			map<Id,Change_Request__c> crMap = new map<Id,Change_Request__c>([Select Id, Execution_Status__c, Source_Position__c, Destination_Position__c, Source_Positions__c, Destination_Positions__c, Request_Type_Change__c, Team_Instance_ID__r.Alignment_Type__c from Change_Request__c where 
																			 Change_Effective_Date__c =: system.today() and 
																			 (Request_Type_Change__c =: SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP OR
																			 Request_Type_Change__c =: SalesIQGlobalConstants.MOVEMENT_TYPE_ACCOUNT OR
																			 Request_Type_Change__c =: SalesIQGlobalConstants.HIERARCHY_CHANGE OR 
																			 Request_Type_Change__c =: SalesIQGlobalConstants.HIERARCHY_CHANGE_OVERLAY OR
																			 Request_Type_Change__c =: SalesIQGlobalConstants.UPDATE_OVERLAY_POSITION)and 
																			 Execution_Status__c != 'Completed' and 
																			 Status__c =: SalesIQGlobalConstants.REQUEST_STATUS_APPROVED]);
			system.debug('#### crMap '+crMap);
			if(crMap.size() > 0){
				list<Id> recIds = new list<Id>();
				list<string> reqIds = new list<string>();
				set<Id> positionIds = new set<Id>();
				set<Id> requestIds = new set<Id>();
				
				list<Change_Request__c> crListForZIPMove = new list<Change_Request__c>();
				map<Id,Change_Request__c> crMapForHierarchyChange = new map<Id,Change_Request__c>();
				map<Id,Change_Request__c> crMapForHierarchyChangeOverlay = new map<Id,Change_Request__c>();
				
				for(Change_Request__c cr : crMap.values()){
					if(cr.Team_Instance_ID__r.Alignment_Type__c == 'Hybrid' || cr.Team_Instance_ID__r.Alignment_Type__c == 'Account'){
						requestIds.add(cr.Id);
					}
		        	if(cr.Request_Type_Change__c == SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP){
		        		recIds.add(cr.Id);
		        		reqIds.add(cr.Id);
		        		crListForZIPMove.add(cr);
		        	}else if(cr.Request_Type_Change__c == SalesIQGlobalConstants.MOVEMENT_TYPE_ACCOUNT){
		        		reqIds.add(cr.Id);
		        		crListForZIPMove.add(cr);
		        	}else if(cr.Request_Type_Change__c == SalesIQGlobalConstants.HIERARCHY_CHANGE){
		        		crMapForHierarchyChange.put(cr.Id,cr);
		        		positionIds.add(cr.Source_Position__c);
		        	}else if(cr.Request_Type_Change__c == SalesIQGlobalConstants.HIERARCHY_CHANGE_OVERLAY){
		        		crMapForHierarchyChangeOverlay.put(cr.Id,cr);
		        		positionIds.add(cr.Source_Position__c);
		        	}else if(cr.Request_Type_Change__c == SalesIQGlobalConstants.UPDATE_OVERLAY_POSITION){
		        		recIds.add(cr.Id);
		        	}
		        }
		        
		        //Invoke SyncWithESRIBatch class to make callout on ESRI server to update features for future dated zip movement and update overlay request
	        	if(recIds.size() > 0){
	        		Id jobId = System.System.enqueueJob(new QueueESRIBatchProcess(recIds));
	        	}
	        	
	        	//CIM calculation for future dated zip movement request
		        if(crListForZIPMove.size() > 0){
	        		updateRequestSummary(crListForZIPMove,reqIds);
		        }

		        //Handle Account affiliation - Update Account Relationship flag with implicit/explicit for future dated Account movement request
		        system.debug('#### requestIds : '+requestIds);
		        if(requestIds.size() > 0){
		        	system.debug('#### calling processAccountAlignmentType');
		        	ChangeRequestTriggerHandler.processAccountAlignmentType(requestIds);
		        }
		        
		        
		        //Update parent on position for future active hierarchy change request
		        if(positionIds.size() > 0){
			        map<Id,Position__c> posMap = new map<Id,Position__c>([Select id, Parent_Position__c from Position__c where id in : positionIds]);
		        	for(CR_Position__c crPos : [select id, Change_Request__c, Position__c, Future_Reporting_Position__c from CR_Position__c where Change_Request__c in : crMap.keyset()]){
		        		if(posMap.containsKey(crPos.Position__c)){
		        			posMap.get(crPos.Position__c).Parent_Position__c = crPos.Future_Reporting_Position__c;
		        		}
		        	}
		        	update posMap.values();
		        }
		        
		        //Sync ESRI with future active hierarchy changes request
		        if(crMapForHierarchyChange.size() > 0){
		        	string jobId = database.executeBatch(new ProcessFutureZIPAssignmentBatch(crMapForHierarchyChange.keyset(),SalesIQGlobalConstants.HIERARCHY_CHANGE,SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE),50);
		        }
		        
		        //Sync ESRI with future active overlay hierarchy changes request
		        if(crMapForHierarchyChangeOverlay.size() > 0){
		        	string jobId = database.executeBatch(new ProcessFutureZIPAssignmentBatch(crMapForHierarchyChangeOverlay.keyset(),SalesIQGlobalConstants.HIERARCHY_CHANGE_OVERLAY,SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE),50);
		        }
		        
		        //Mark the execution status as completed on CR
	        	for(Change_Request__c cr : crMap.values()){
		        	cr.Execution_Status__c = 'Completed';
	        	}
		        if(SecurityUtil.checkUpdate(Change_Request__c.SObjectType, new list<string>{'Execution_Status__c'}, false)){
		        	update crMap.values();
		        }
		        
		        string AlignmentStatusJobId = database.executeBatch(new UpdateAccountAlignmentTypeBatch(),200);
		        
	        }
		}catch(Exception e){
			system.debug('####'+e.getMessage());
			SalesIQLogger.logCatchedException(e,'ProcessFutureApprovedCRScheduler');
		}
	}
	
	//Method to update CIM Summary data
	public static void updateRequestSummary(list<Change_Request__c> ChangeReq, list<string> RequestIds){
        set<id> TeamInstancePosition = new set<id>();
        
        for(Change_Request__c req : ChangeReq){
            TeamInstancePosition.add(req.Source_Positions__c);
            TeamInstancePosition.add(req.Destination_Positions__c);
        }
        
        // fetch source and destination position linked overlay position
        for(Related_Position__c overlayPosition : [select id, name, Related_Position_Team_Instance__c from Related_Position__c where isActive__c = true and Base_Position_Team_Instance__c in : TeamInstancePosition]) {
        	TeamInstancePosition.add(overlayPosition.Related_Position_Team_Instance__c);
        }
        
        map<string,map<string,string>> PosTeamInstance = new map<string,map<string,string>>();
        list<CIM_Change_Request_Impact__c> CIMReq = RequestUtilityClass.getChangeRequestImpactData(RequestIds, TeamInstancePosition);
        map<id,set<id>> PosCIMConfig = new map<id,set<id>>();
        
        if(CIMReq != null && CIMReq.size() > 0){
            for(id TeamInstanceId : TeamInstancePosition){
                Map<string,string> mapImpactofCIM = new Map<string,string>();
                set<id> CIMConfigSet = new set<id>();
                for(CIM_Change_Request_Impact__c CIMImpact : CIMReq){
                    if( CIMImpact.Position_Team_Instance__c == TeamInstanceId){
                        mapImpactofCIM.put(cimImpact.CIM_Config__c, (cimImpact.Impact__c == null ? '0' : cimImpact.Impact__c));
                        CIMConfigSet.add(cimImpact.CIM_Config__c);
                    }
                }
                PosTeamInstance.put(TeamInstanceId,mapImpactofCIM);
                PosCIMConfig.put(TeamInstanceId,CIMConfigSet);
            }
            
            map<id,list<CIM_Position_Metric_Summary__c>> impactSummary = ChangeRequestTriggerHandler.getImpactSummary(PosCIMConfig);
            
            if(impactSummary != null && impactSummary.size() > 0){
			    for(String key : impactSummary.keySet()){
                    for(CIM_Position_Metric_Summary__c summary : impactSummary.get(key)){
                    	Map<string,string> mapImpactCIM = PosTeamInstance.get(summary.Position_Team_Instance__c);  
                        summary.Approved__c = String.valueOf( (Decimal.valueOf(summary.Approved__c) + Decimal.valueOf(mapImpactCIM.get(summary.CIM_Config__c)) ) );
                	}
				}
            }
            list<string> CIM_POSITION_METRIC_SUMMARY_UPDATE_FIELD = new list<string>{'Proposed__c','Approved__c'};
            if(SecurityUtil.checkUpdate(CIM_Position_Metric_Summary__c.SObjectType, CIM_POSITION_METRIC_SUMMARY_UPDATE_FIELD, false)){
                List<CIM_Position_Metric_Summary__c> metricSummaryToBeUpdate = new List<CIM_Position_Metric_Summary__c>();
                for(Id keyId : impactSummary.keySet()){
                    metricSummaryToBeUpdate.addAll(impactSummary.get(keyId));
                }
                update metricSummaryToBeUpdate ;
            }
        }
    }
}