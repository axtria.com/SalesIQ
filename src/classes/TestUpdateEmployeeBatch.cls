@IsTest
public with sharing class TestUpdateEmployeeBatch {
    static testmethod void ExecuteBatch(){
        Employee__c emp = CallPlanTestDataFactory.createEmployee();
        insert emp;
        
        UpdateEmployeeBatch c = new UpdateEmployeeBatch();
        Id batchinstanceid = Database.executeBatch(c,500);    
    }
}