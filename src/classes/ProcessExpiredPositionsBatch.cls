/************************************************************************************************
Name            :   ProcessExpiredPositionsBatch
Description     :   Batch class to update Inactive and Employee Assignment status on Position.
                    Invoked through scheduler and change request trigger
Author          :   Lagnika Sharma | 12/14/2016
Modified        :   3-1-2017 | SPD-975 - Update Assignment Status on Position
Modified        :   6-25-2018| SIMPS-423 - Inactivate user position when position employee expired
************************************************************************************************/
global class ProcessExpiredPositionsBatch implements Database.Batchable<sObject>{

    static Date changeEffectiveDate;
    static set<ID> inactivePositionIDSet{get;set;}
    global ProcessExpiredPositionsBatch(){}

    global Database.QueryLocator start(Database.BatchableContext bc){
        system.debug('#### Start method');
        string soql = 'Select Id,IsMaster__c, Assignment_Status__c,inactive__c,Employee__c,Effective_Start_Date__c, Effective_End_Date__c,Team_Instance__r.Team_Cycle_Name__c, Team_Instance__r.Alignment_Period__c from Position__c where Inactive__c = false';
        return Database.getQueryLocator(soql);
    }

    //Execute method : Process all the positions end dating today.
    global void execute(Database.BatchableContext bc, List<Position__c> scope){
        system.debug('#### Execute method | scope : '+scope.size());
        updatePosition(scope);
    }
    
    //Deprecated in 4.5
    global static void updatePosition(list<Position__c> positions,  boolean isBatchProcess){
    }
    
    global static void updatePosition(list<Position__c> positions){
        system.debug(Logginglevel.ERROR,'#### updatePosition method | positions : '+positions.size());
        //Fetch configured day of batch process
        double processDay;
        if(TotalApproval__c.getValues('ProcessDay') != null){
            processDay = TotalApproval__c.getValues('ProcessDay').No_Of_Approval__c;
        }else{
            processDay = 0;
        }
        Date processDate = System.today().addDays(integer.valueOf(processDay));
        changeEffectiveDate = System.today().addDays(integer.valueOf(processDay));
        system.debug('#### processDate : '+processDate);
        
        set<Id> positionIds = new set<Id>();
         inactivePositionIDSet = new set<ID>();
        for(Position__c pos : positions){
            positionIds.add(pos.Id);
        }
        system.debug('Position Inactive code Starts');
        map<Id,list<Position_Employee__c>> positionAssignmentMap = new map<Id,list<Position_Employee__c>>();
        for(Position_Employee__c pe : [Select Id, Position__c, Employee__c From Position_Employee__c where Assignment_Type__c = 'Primary' and 
                                       Effective_End_Date__c >=: processDate and Effective_Start_Date__c <=: processDate and Employee__c != null and 
                                       Position__c in : positionIds order by LastModifiedDate desc]){
            positionAssignmentMap.put(pe.Position__c,new list<Position_Employee__c>{pe});
            system.debug('#### pe.Position__c : '+pe.Position__c);
        }
        
        set<Id> InactivePositionIds = new set<Id>();
        
        for(Position__c pos : positions){
            if(pos.Effective_End_Date__c <= processDate){
                pos.Inactive__c = true;
                InactivePositionIds.add(pos.id);
            }
            
            //Update Employee and Assignment Status on Position if active primary employee is assigned
            if(positionAssignmentMap.containsKey(pos.Id) && positionAssignmentMap.get(pos.Id).size() > 0){
                pos.Assignment_Status__c = 'Filled';
                pos.Employee__c = positionAssignmentMap.get(pos.Id)[0].Employee__c;
            }else{
                pos.Assignment_Status__c = 'Vacant';
                pos.Employee__c = null;
            }
        }
        inactivePositionIDSet.addAll(InactivePositionIds);
        update positions;
        
        system.debug('Position Inactive code ends');
        //updatePosGeoAccntEmployExprirePos(InactivePositionIds);
        
        //SIMPS-423 - Inactivate user position when position employee expired
        inactivateUserPosForExpiredAssignment(positionIds);
    }
    
    
    global static void updatePosGeoAccntEmployExprirePos(set<id> InactivePositionIds)
    {
        
        list<Position__c> positions = [select id from Position__c where Inactive__c = true  ];
        set<Id>  posIds = new set<Id>();
        for(Position__c pos : positions){
            posIds.add(pos.Id);
        }
        
        System.debug('posIds :'+posIds);
        
        if(posIds != null && posIds.size() >0){
            ProcessCRAssignmentBatch b1 = new ProcessCRAssignmentBatch(posIds,'Position_Account__c');
            ProcessCRAssignmentBatch b2 = new ProcessCRAssignmentBatch(posIds,'Position_Geography__c');
            ProcessCRAssignmentBatch b3 = new ProcessCRAssignmentBatch(posIds,'Position_Employee__c');

            Id batchinstanceid1 = Database.executeBatch(b1,9000);
            Id batchinstanceid2 = Database.executeBatch(b2,9000);
            Id batchinstanceid3 = Database.executeBatch(b3,9000);
        }

        if(InactivePositionIds != null && InactivePositionIds.size() >0){
            ProcessCRAssignmentBatch b1 = new ProcessCRAssignmentBatch(InactivePositionIds,'Position_Account__c');
            ProcessCRAssignmentBatch b2 = new ProcessCRAssignmentBatch(InactivePositionIds,'Position_Geography__c');
            ProcessCRAssignmentBatch b3 = new ProcessCRAssignmentBatch(InactivePositionIds,'Position_Employee__c');

            Id batchinstanceid1 = Database.executeBatch(b1,9000);
            Id batchinstanceid2 = Database.executeBatch(b2,9000);
            Id batchinstanceid3 = Database.executeBatch(b3,9000);
        }
        //Date processDate = System.today();
        /*Date processDate = changeEffectiveDate;
        system.debug(Logginglevel.ERROR,'#### processDate : '+processDate);
        
        map<id,Team_Instance__c> DateTeamInstanceMap = new map<id,Team_Instance__c>();
        for(Team_Instance__c t : [select id, IC_EffStartDate__c, IC_EffEndDate__c from Team_Instance__c]){
            DateTeamInstanceMap.put(t.id,t);
        }
            
        map<id,id> UnassignedPosMap = new map<id,id>();
        set<Id> unassignedTerrIds = new set<Id>();
        for(Position__c ps : [select id,Team_Instance__c from Position__c where Client_Position_Code__c='00000']){
            unassignedTerrIds.add(ps.Id);
            if(!UnassignedPosMap.containsKey(ps.Team_Instance__c)){
                UnassignedPosMap.put(ps.Team_Instance__c,ps.id);
            }
        }
        
        map<id,id> UnassignedPosTeamInsMap = new map<id,id>();
        for(Position_Team_Instance__c ps : [select id , Team_Instance_ID__c from Position_Team_Instance__c where Position_ID__c in : unassignedTerrIds]){
            if(!UnassignedPosTeamInsMap.containsKey(ps.Team_Instance_ID__c)){
                UnassignedPosTeamInsMap.put(ps.Team_Instance_ID__c,ps.id);
            }
        }
            
        system.debug(Logginglevel.ERROR,'Position Geography Inactive code Starts'); 
        //Assigning Unassigned Positions to the Expired Position Geography Record Having Expired Positions
        map<Id,list<Position_Geography__c>> geoAssignmentMap = new map<Id,list<Position_Geography__c>>();
        list<Position_Geography__c> PosGeoList = new list<Position_Geography__c>();
        list<Position_Geography__c> PosGeoListToDelete = new list<Position_Geography__c>();
        for(Position_Geography__c pg : [Select Assignment_Status__c,Change_Status__c,Effective_End_Date__c,Effective_Start_Date__c,Geography__c,Id,Metric1__c,Metric2__c,Metric3__c,Metric4__c,Metric5__c,Metric6__c,Metric7__c,Metric8__c,Metric9__c,Metric10__c,Name,Position_Team_Instance__c,Position__c,Team_Instance__c, Team_Instance__r.Alignment_Period__c From Position_Geography__c where Effective_End_Date__c >: processDate and 
                                       Position__c in : InactivePositionIds order by LastModifiedDate desc]){
            
            Position_Geography__c clonedPosGeoRecord = pg.clone(false,true,false,false);
            
            clonedPosGeoRecord.Position__c = UnassignedPosMap.get(pg.Team_Instance__c);
            clonedPosGeoRecord.Position_Team_Instance__c = UnassignedPosTeamInsMap.get(pg.Team_Instance__c);
            
            
                clonedPosGeoRecord.Effective_Start_Date__c = processDate;
                pg.Effective_End_Date__c = processDate.addDays(-1);
                PosGeoList.add(pg);
            
            clonedPosGeoRecord.Effective_End_Date__c = DateTeamInstanceMap.get(pg.Team_Instance__c).IC_EffEndDate__c;

            PosGeoList.add(clonedPosGeoRecord);
        }

        system.debug(Logginglevel.ERROR,'Position Geography Inactive code Ends'); 
        
        //Assigning Unassigned Positions to the Expired Position Account Record Having Expired Positions
        map<Id,list<Position_Account__c>> AccntAssignmentMap = new map<Id,list<Position_Account__c>>();
        list<Position_Account__c> PosAccntList=new list<Position_Account__c>();
        list<Position_Account__c> PosAccntListToDelete = new list<Position_Account__c>();
        for(Position_Account__c pa : [Select Account_Alignment_Type__c,Account_Target_Type__c,Account__c,Assignment_Status__c,Change_Status__c,Comments__c,Effective_End_Date__c,Effective_Start_Date__c,Id,Metric1__c,Metric2__c,Metric3__c,Metric4__c,Metric5__c,Metric6__c,Metric7__c,Metric8__c,Metric9__c,Metric10__c,Metric_1__c,Name,Position_Team_Instance__c,Position__c,Segment_1__c,Team_Instance__c, Team_Instance__r.Alignment_Period__c From Position_Account__c where Effective_End_Date__c >=: processDate and 
                                      Position__c in : InactivePositionIds order by LastModifiedDate desc]){
            
            Position_Account__c clonedPosAccntRecord = pa.clone(false,true,false,false);
            
            clonedPosAccntRecord.Position__c = UnassignedPosMap.get(pa.Team_Instance__c);
            clonedPosAccntRecord.Position_Team_Instance__c = UnassignedPosTeamInsMap.get(pa.Team_Instance__c);
            
                clonedPosAccntRecord.Effective_Start_Date__c = processDate;
                pa.Effective_End_Date__c = processDate.addDays(-1);
                PosAccntList.add(pa);
            //}
            clonedPosAccntRecord.Effective_End_Date__c = DateTeamInstanceMap.get(pa.Team_Instance__c).IC_EffEndDate__c;

            PosAccntList.add(clonedPosAccntRecord);
        }

        //Expiring the PositionEmployee records related to the Expired Positions 
        list<Position_Employee__c> PosEmployeeList=new list<Position_Employee__c>();
        list<Position_Employee__c> PosEmployeeListToDelete = new list<Position_Employee__c>();
        map<Id,list<Position_Employee__c>> positionAssignmentMap = new map<Id,list<Position_Employee__c>>();
        for(Position_Employee__c pe : [Select Id, Position__c, Employee__c, Position__r.Team_Instance__c, Position__r.Team_Instance__r.Alignment_Period__c From Position_Employee__c where 
                                       Effective_End_Date__c >=: processDate and Employee__c != null and 
                                       Position__c in : InactivePositionIds order by LastModifiedDate desc]){
            
            
                pe.Effective_End_Date__c = processDate.addDays(-1);
                PosEmployeeList.add(pe);
            //}
        }
        
        //Check if the total records to be updated in the process are more than 10K then throw DML exception.
        integer dmlSize = PosGeoList.size() + PosGeoListToDelete.size() + PosAccntList.size() + PosAccntListToDelete.size() + PosEmployeeList.size() + PosEmployeeListToDelete.size();
        system.debug('#### dmlSize : '+dmlSize);
        if(dmlSize >= 10000){
            throw new DMLException('My DML EXCEPTION');
        }

        system.debug(Logginglevel.ERROR,'PosGeoList : '+PosGeoList.size()); 
        if(PosGeoList != null && PosGeoList.size() > 0){
           // upsert(PosGeoList);
            

        }
        

        system.debug(Logginglevel.ERROR,'PosAccntList : '+PosAccntList.size()); 
        if(PosAccntList != null && PosAccntList.size() > 0){
            //upsert(PosAccntList);
            

        }

       
            
        system.debug(Logginglevel.ERROR,'PosEmployeeList : '+PosEmployeeList.size()); 
        if(PosEmployeeList != null && PosEmployeeList.size() > 0){
            //update(PosEmployeeList);
           
        }*/
    }
    
    public static void inactivateUserPosForExpiredAssignment(set<Id> positionIds){
        //Fetch configured day of batch process
        double processDay;
        if(TotalApproval__c.getValues('ProcessDay') != null){
            processDay = TotalApproval__c.getValues('ProcessDay').No_Of_Approval__c;
        }else{
            processDay = 0;
        }
        Date processDate = System.today().addDays(integer.valueOf(processDay));
        system.debug('#### processDate : '+processDate);

        set<Id> teamInstanceIds = new set<Id>();
        set<Id> posIds = new set<Id>();
        set<Id> userIds = new set<Id>();

        map<Id,map<Id,set<Id>>> empAssMap = new map<Id,map<Id,set<Id>>>();
        for(Position_Employee__c pe : [Select Id, Position__c, Position__r.Team_Instance__c, Employee__r.USER__c From Position_Employee__c 
                                       WHERE Assignment_Type__c = 'Primary' AND Position__c IN : positionIds AND 
                                       Effective_End_Date__c <: processDate AND Employee__c != null AND Employee__r.USER__c != null]){
            teamInstanceIds.add(pe.Position__r.Team_Instance__c);
            posIds.add(pe.Position__c);
            userIds.add(pe.Employee__r.USER__c);

            map<Id,set<Id>> posToUserMap = empAssMap.get(pe.Position__r.Team_Instance__c);
            if(posToUserMap != null){
                set<Id> userIdSet = posToUserMap.get(pe.Position__c);
                if(userIdSet == null){
                    userIdSet = new set<Id>();
                }
                userIdSet.add(pe.Employee__r.USER__c);
                posToUserMap.put(pe.Position__c,userIdSet);
            }else{
                posToUserMap = new map<Id, set<Id>>();
                posToUserMap.put(pe.Position__c, new set<Id>{pe.Employee__r.USER__c});
            }
            empAssMap.put(pe.Position__r.Team_Instance__c,posToUserMap);
        }

        list<User_Access_Permission__c> implicitUAP = new list<User_Access_Permission__c>();
        for(User_Access_Permission__c uap : [SELECT Id, Team_Instance__c, User__c,  Sharing_Type__c, Position__c, Is_Active__c FROM User_Access_Permission__c 
                                             WHERE Team_Instance__c IN : teamInstanceIds AND User__c IN : userIds AND Position__c IN : posIds AND
                                             Sharing_Type__c = 'Implicit' AND Is_Active__c = true]){
            map<Id,set<Id>> posToUserMap = empAssMap.get(uap.Team_Instance__c);
            if(posToUserMap.containsKey(uap.Position__c)){
                if(posToUserMap.get(uap.Position__c).contains(uap.User__c)){
                    uap.Is_Active__c = false;
                    implicitUAP.add(uap);
                }
            }
        }
        if(implicitUAP.size() > 0){
            update implicitUAP;
        }
    }
    
    global void finish(Database.BatchableContext bc){
        system.debug('#### Finish method');
       // if(inactivePositionIDSet != null && inactivePositionIDSet.size() >0)
        updatePosGeoAccntEmployExprirePos(new set<ID>{});
    }

    //This method will be called from Change request trigger handler for expire positions (cascade delete)
    public static void updateExpiredPosition(list<Position__c> positions, Change_Request__c cr){
        try{

            list<CR_Position__c> crPos = [SELECT Id, Effective_End_Date__c FROM CR_Position__c WHERE Change_Request__c =: cr.Id];
            
            if(crPos.size() > 0){
                changeEffectiveDate = crPos[0].Effective_End_Date__c;
            }else{
                changeEffectiveDate = system.today();
            }

            system.debug(Logginglevel.ERROR,'#### updatePosition method | positions : '+positions.size());
            
            //Date processDate = System.today().addDays(integer.valueOf(processDay));
            //SIMPS-644 - This code will only execute for delete position. Process date will be position end date.
            Date processDate = changeEffectiveDate;
            
            system.debug(Logginglevel.ERROR,'#### processDate : '+processDate);
            
            set<Id> positionIds = new set<Id>();
            for(Position__c pos : positions){
                positionIds.add(pos.Id);
            }
            system.debug('Position Inactive code Starts');
            /*map<Id,list<Position_Employee__c>> positionAssignmentMap = new map<Id,list<Position_Employee__c>>();
            for(Position_Employee__c pe : [Select Id, Position__c, Employee__c From Position_Employee__c where Assignment_Type__c = 'Primary' and 
                                           Effective_End_Date__c >: processDate and Effective_Start_Date__c <=: processDate and Employee__c != null and 
                                           Position__c in : positionIds order by LastModifiedDate desc]){
                positionAssignmentMap.put(pe.Position__c,new list<Position_Employee__c>{pe});
                system.debug(Logginglevel.ERROR,'#### pe.Position__c : '+pe.Position__c);
            }*/
            
            set<Id> InactivePositionIds = new set<Id>();
            for(Position__c pos : positions){
                //Mark the position as inactive if Effective end date is smaller than today
                if(pos.Effective_End_Date__c <= processDate || pos.Team_Instance__r.Alignment_Period__c == SalesIQGlobalConstants.FUTURE_TEAM_CYCLE_TYPE){
                    pos.inactive__c = true;
                    InactivePositionIds.add(pos.id);
                    system.debug('The Inactive Position is'+pos.id); 
                    //SIMPS-644 - Position is getting deleted and the employee assignments will be removed. Hence updating the Assignment_Status__c to vacant
                    pos.Assignment_Status__c = 'Vacant';
                    pos.Employee__c = null;
                }

            }
            update positions;
            system.debug('Position Inactive code ends');
            updatePosGeoAccntEmployExprirePos(InactivePositionIds);
            
            //SIMPS-423 - Inactivate user position when position employee expired
            set<Id> positionIdsSet = new set<Id>(positionIds);
            inactivateUserPosForExpiredAssignment(positionIdsSet);
        }catch(System.DmlException dmlEx){
            system.debug(Logginglevel.ERROR,'Error message approveRejectDeletePosition '+dmlEx.getMessage());
           // cr.AddError('Salesforce Limit Exception : Operation cannot be completed as data volume has exceeded the threshold. Please contact SalesIQ admin to process this request.');
        }
        catch(Exception ex){
            system.debug(Logginglevel.ERROR,'Error message approveRejectDeletePosition '+ex.getMessage());
            cr.AddError('Error in updateExpiredPosition : '+ex.getMessage());
        }
    }
}