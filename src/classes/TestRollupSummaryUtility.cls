@isTest
private class TestRollupSummaryUtility {
    static testMethod void testRollupForPositionAccount() {
        Team__c accBasedTeam =  CallPlanTestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_AccountBased);    	
    	insert accBasedTeam;
    	
		Team_Instance__c accBasedTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(accBasedTeam,SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT);
    	insert accBasedTeamInstance;
    	
        Account acc = CallPlanTestDataFactory.CreateAccount('Direct Aligned','11788','Physician');    
    	acc.type = 'Physician';
    	insert acc; 	
    	
    	Position__c region = CallPlanTestDataFactory.createRegionPosition(accBasedTeam);
        region.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
		insert region;
		
		Position_Team_Instance__c regionTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(region, null, accBasedTeamInstance);
		regionTeamInstance.X_Max__c=-72.6966429900;
        regionTeamInstance.X_Min__c=-73.9625820000;
        regionTeamInstance.Y_Max__c=40.9666490000;
        regionTeamInstance.Y_Min__c=40.5821279800;
		insert regionTeamInstance;
		
		Position__c district = CallPlanTestDataFactory.createPositionParent(region,accBasedTeam);
        district.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
		insert district;
		
		Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, region, accBasedTeamInstance);
		districtTeamInstance.X_Max__c=-72.6966429900;
        districtTeamInstance.X_Min__c=-73.9625820000;
        districtTeamInstance.Y_Max__c=40.9666490000;
        districtTeamInstance.Y_Min__c=40.5821279800;
		insert districtTeamInstance;
		
		Position__c territory =  CallPlanTestDataFactory.createPosition(district,accBasedTeam);
        territory.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
		insert territory;
		
		Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, district, accBasedTeamInstance);
		territoryTeamInstance.X_Max__c=-72.6966429900;
        territoryTeamInstance.X_Min__c=-73.9625820000;
        territoryTeamInstance.Y_Max__c=40.9666490000;
        territoryTeamInstance.Y_Min__c=40.5821279800;
		insert territoryTeamInstance;
	
		
    	/*Team_Instance_Account__c teamInstanceAcc = CallPlanTestDataFactory.createTeamInstanceAccount(acc,accBasedTeamInstance);
    	insert teamInstanceAcc;	*/

    	
    	Position_Account__c posAcc = CallPlanTestDataFactory.createPositionAccount(acc, territory, accBasedTeamInstance, territoryTeamInstance);
    	insert posAcc;
    	
    	test.startTest();
	    	string jobId = database.executeBatch(new RollupSummaryUtility('Position_Account__c',accBasedTeamInstance.Id));
	        list<Position_Team_Instance__c> acclatlng = [SELECT X_Max__c,X_Min__c,Y_Max__c,Y_Min__c from Position_Team_Instance__c where id =: territoryTeamInstance.Id];
	    	if(acclatlng[0].Y_Max__c != null){
	    		system.assertEquals(40.9666490000 ,acclatlng[0].Y_Max__c );
	    	}
    	test.stopTest();
    }
    
    static testMethod void testRollupForPositionGeography() {
        Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11778');
		insert geo;
		
		Team__c zipBasedTeam =  CallPlanTestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_GeoBased);    	
    	insert zipBasedTeam;
    	
		Team_Instance__c zipBasedTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(zipBasedTeam,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP);
    	insert zipBasedTeamInstance;
    	
    	Position__c region = CallPlanTestDataFactory.createRegionPosition(zipBasedTeam);
		insert region;
		
		Position_Team_Instance__c regionTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(region, null, zipBasedTeamInstance);
		regionTeamInstance.X_Max__c=-72.6966429900;
        regionTeamInstance.X_Min__c=-73.9625820000;
        regionTeamInstance.Y_Max__c=40.9666490000;
        regionTeamInstance.Y_Min__c=40.5821279800;
		insert regionTeamInstance;
		
		Position__c district = CallPlanTestDataFactory.createPositionParent(region,zipBasedTeam);
		insert district;
		
		Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, region, zipBasedTeamInstance);
		districtTeamInstance.X_Max__c=-72.6966429900;
        districtTeamInstance.X_Min__c=-73.9625820000;
        districtTeamInstance.Y_Max__c=40.9666490000;
        districtTeamInstance.Y_Min__c=40.5821279800;
		insert districtTeamInstance;
		
		Position__c territory =  CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
		territory.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
		insert territory;
		
		Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, district, zipBasedTeamInstance);
		territoryTeamInstance.X_Max__c=-72.6966429900;
        territoryTeamInstance.X_Min__c=-73.9625820000;
        territoryTeamInstance.Y_Max__c=40.9666490000;
        territoryTeamInstance.Y_Min__c=40.5821279800;
		insert territoryTeamInstance;

        /*Team_Instance_Geography__c geographyTeamInstance = CallPlanTestDataFactory.CreateTeamInstanceGeoGraphy(geo,zipBasedTeamInstance);
        insert geographyTeamInstance;*/
        
        Position_Geography__c posGeo = CallPlanTestDataFactory.createPosGeography(geo,territory,territoryTeamInstance,zipBasedTeamInstance);
        posGeo.Position_Id_External__c = territory.id;
        insert posGeo;
        
        test.startTest();
			string jobId = database.executeBatch(new RollupSummaryUtility('Position_Geography__c',zipBasedTeamInstance.Id));
	        list<Position_Team_Instance__c> acclatlng = [SELECT X_Max__c,X_Min__c,Y_Max__c,Y_Min__c from Position_Team_Instance__c where id =: territoryTeamInstance.Id];
	    	if(acclatlng[0].Y_Max__c != null){
	    		system.assertEquals(40.9666490000 ,acclatlng[0].Y_Max__c );
	    	}
		test.stopTest();
    }
}