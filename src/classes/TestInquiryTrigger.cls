@isTest
private class TestInquiryTrigger {

    static User rep, hoUser1 ;
    static Group testGroup ;
    static Profile hoProfile ;

    @future
    static void createUsers(){
        hoProfile = [SELECT Id, Name FROM Profile WHERE Name =: 'System Administrator' limit 1];
        
        testGroup = new Group(Name = 'InquiryHOQueue', Type = 'Queue');
        insert testGroup;

        hoUser1 = new User(LastName = 'ho',
                            FirstName='test',
                            Alias = 'test',
                            Email = 'test.ho@test.com',
                            Username = 'test.ho@test.com',
                            ProfileId = hoProfile.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US');
        
        rep = new User(LastName = 'rep',
                        FirstName='test',
                        Alias = 'rep',
                        Email = 'test.rep@test.com',
                        Username = 'test.rep@test.com',
                        ProfileId = hoProfile.id,
                        TimeZoneSidKey = 'GMT',
                        LanguageLocaleKey = 'en_US',
                        EmailEncodingKey = 'UTF-8',
                        LocaleSidKey = 'en_US');

        insert new User[] {hoUser1, rep}; 

        List<PermissionSet> repAndHoPS = [SELECT Id, Name From PermissionSet WHERE Name = 'SalesIQ_HO' or Name = 'SalesIQ_DM'];
        List<PermissionSetAssignment> psAssignments = new List<PermissionSetAssignment>();
        for(PermissionSet ps : repAndHoPS) {
            if(ps.Name == 'SalesIQ_HO')
                psAssignments.add(new PermissionSetAssignment(AssigneeId = hoUser1.id, PermissionSetId = ps.Id )) ;
            else
                psAssignments.add(new PermissionSetAssignment(AssigneeId = rep.id, PermissionSetId = ps.Id )) ;
        }
        insert psAssignments ;
    }


    static {
        test.startTest();
        
        createUsers();
        test.stopTest();

        Employee__c manager = new Employee__c();
        manager.isSalesforceUser__c = True;
        manager.user__c = hoUser1.id;
        manager.FirstName__c = 'emp';
        manager.Last_Name__c = 'lastName';
        insert manager;

      
        Employee__c emp = new Employee__c();
        emp.isSalesforceUser__c = True;
        emp.user__c = rep.id;
        emp.FirstName__c = 'firstName';
        emp.Last_Name__c = 'LastName';
        emp.Manager__c = manager.id;
        insert emp;

        Inquiry_SLA_Setting__c setting = new Inquiry_SLA_Setting__c();
        setting.Resolution_Days_Count__c = 5;
        setting.name = 'inquirysetting';
        setting.Priority__c = 'High';
        setting.Category__c = 'Position';
        setting.Sub_Category__c = 'Employee Universe';
        setting.Profile_Name__c = hoProfile.Name ;
        insert setting;
    }

	static testMethod void testInquiry() {
        
        Inquiry__c inqObj ;   
        System.runAs(rep){

            GroupMember member = new GroupMember();
            member.UserOrGroupId = hoUser1.id;
            member.GroupId = testGroup.Id;
            insert member;

            
            inqObj = new Inquiry__c();
            inqObj.Priority__c = 'High';
            inqObj.Category__c = 'Position';
            inqObj.Sub_Category__c = 'Employee Universe';
            
            // ownership changed from current user
            insert inqObj;

            try {
                // non-onwer trying to update the Inquiry
                update inqObj ;
            } catch(Exception e) {
                // code will come here, not allowing edit
            }
        }

        System.runAs(hoUser1) {
            // HO marked it Resolved
            inqObj.Status__c = SalesIQGlobalConstants.INQUIRY_STATUS_RESOLVED ;

            Inquiry_Comment__c comment = new Inquiry_Comment__c() ;
            comment.Inquiry__c = inqObj.Id ;
            comment.Description__c = 'Some text' ;
            insert comment ;

            update inqObj ;
        }

        System.runAs(rep){ 
            // Rep reopen an Inquiry
            inqObj.Status__c = SalesIQGlobalConstants.INQUIRY_STATUS_REOPEN ;
            update inqObj ;
        }

        try {
            // Trying to update the inquiry as a non-owner
            inqObj.Status__c = SalesIQGlobalConstants.INQUIRY_STATUS_CLOSED ;
            update inqObj ;
        } catch(Exception e) {
            // error added by trigger
        }  
        
	}

    static testMethod void testCancelInquiry() {
        Inquiry__c inqObj ;
        System.runAs(rep){ 
            GroupMember member = new GroupMember();
            member.UserOrGroupId = hoUser1.id;
            member.GroupId = testGroup.Id;
            insert member;

            
            inqObj = new Inquiry__c();
            inqObj.Priority__c = 'High';
            inqObj.Category__c = 'Position';
            inqObj.Sub_Category__c = 'Employee Universe';
            
            // ownership changed from current user
            insert inqObj;

            System.assertNotEquals(rep.Id, [select OwnerId from Inquiry__c WHERE Id = : inqObj.Id].OwnerId) ;
            System.assertNotEquals(UserInfo.getUserId(), [select OwnerId from Inquiry__c WHERE Id = : inqObj.Id].OwnerId) ;

            inqObj.Status__c = SalesIQGlobalConstants.INQUIRY_STATUS_CANCELLED ;
            inqObj.Cancellation_Comments__c = 'Cancelling Inquiry' ;
            update inqObj ;

        }

        try {
            // try to updated cancelled Inquiry
            update inqObj ;
        } catch(Exception e) {
            // control will come here
        } 

    }

}