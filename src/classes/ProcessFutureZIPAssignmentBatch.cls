/**
* Description   : Batch class containing methods to callout to ESRI API to update features on ESRI server for future active Hierarchy change request
* Author        : Raghvendra.Rathore
* Since         : 15-March-2016
* Version       : 1.0
*/
global class ProcessFutureZIPAssignmentBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
	global String query;
	global string teamInstance;
	global set<Id> positionIds;	
	global string changeReqType;
	global string unassignedPosCode;
	Date changeReqDate;
	global string alignmentCycle;
	public ProcessFutureZIPAssignmentBatch(set<Id> crIDs,string crType, string alignmentPeriod){
		system.debug('#### ProcessFutureZIPAssignmentBatch constructor');
		alignmentCycle = alignmentPeriod;
		positionIds = new set<Id>();					
		changeReqType = crType;
		unassignedPosCode = SalesIQGlobalConstants.UNASSIGNED_TERRITORY_CODE;
		changeReqDate = Date.Today();
		for(Change_Request__c cr : [Select id,Change_Effective_Date__c, Source_Position__c from Change_Request__c where id in: crIDs]){
			positionIds.add(cr.Source_Position__c);			
		}
		if(crType == SalesIQGlobalConstants.HIERARCHY_CHANGE){
			query = 'select Position__c,Position__r.Name,Position__r.Client_Position_Code__c,Position__r.Parent_Position__r.Name ,Position__r.Parent_Position__r.Client_Position_Code__c,Position__r.Parent_Position__r.Parent_Position__r.Name,'; 
			query +='Position__r.Parent_Position__r.Parent_Position__r.Client_Position_Code__c,Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Name,';
			query +='Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Client_Position_Code__c ,Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Name,';
			query +='Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Client_Position_Code__c,Geography__r.Name, Team_Instance__r.Team_Instance_Code__c, Team_Instance__r.Team__r.Name, Team_Instance__r.Team__r.Type__c, Team_Instance__r.Name from Position_Geography__c where ';
			query +='(Position__c in: positionIds or ';
			query +='Position__r.Parent_Position__c in: positionIds or ';
			query +='Position__r.Parent_Position__r.Parent_Position__c in: positionIds or ';
			query +='Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__c in: positionIds or'; 
			query +='(Position__r.Client_Position_Code__c =: unassignedPosCode and Effective_Start_Date__c =: changeReqDate)) and ';
			if(alignmentPeriod == SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE){
	            query += ' Assignment_Status__c = \'Active\'';
	        }else{
	            query += ' Assignment_Status__c = \'Future Active\'';
	        }
		}
		if(crType == SalesIQGlobalConstants.HIERARCHY_CHANGE_OVERLAY){
			query = 'select Position__c,Position__r.Name,Position__r.Client_Position_Code__c,Position__r.Parent_Position__r.Name ,Position__r.Parent_Position__r.Client_Position_Code__c,Position__r.Parent_Position__r.Parent_Position__r.Name,'; 
			query +='Position__r.Parent_Position__r.Parent_Position__r.Client_Position_Code__c,Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Name,';
			query +='Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Client_Position_Code__c ,Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Name,';
			query +='Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Client_Position_Code__c,Geography__r.Name, Team_Instance__r.Team__r.Name, Team_Instance__r.Team_Instance_Code__c, Team_Instance__r.Team__r.Type__c, Team_Instance__r.Name from Position_Geography__c where ';
			query +='Position__r.Client_Position_Code__c =: unassignedPosCode and Effective_Start_Date__c =: changeReqDate and ';
			query +='Position__r.Related_Position_Type__c = \'Overlay\' and';
			if(alignmentPeriod == SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE){
	            query += ' Assignment_Status__c = \'Active\'';
	        }else{
	            query += ' Assignment_Status__c = \'Future Active\'';
	        }
		}
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		system.debug('#### ProcessFutureZIPAssignmentBatch start'+ query );
		Database.QueryLocator dbquery;
		dbquery =  Database.getQueryLocator(query);
		return dbquery;
	}
	
	global void execute(Database.BatchableContext BC, List<Position_Geography__c> scope){
		system.debug('#### ProcessFutureZIPAssignmentBatch execute');
		system.debug('#### scope ' + scope);
		system.debug('##### Size of scope ' + scope.size());
		MapServerURL__c ESRIService = MapServerURL__c.getOrgDefaults();
		string strTeamInstance = '';
		set<String> teamInstances = new set<string>();
		map<string,list<SyncWithESRIBatchHelper.RequestDataWrapper>> zipDataMap = new map<string,list<SyncWithESRIBatchHelper.RequestDataWrapper>>();
		map<string,map<string,list<SyncWithESRIBatchHelper.RequestDataWrapper>>> newZipDataMap = new map<string,map<string,list<SyncWithESRIBatchHelper.RequestDataWrapper>>>();
		
		map<string,set<string>> teamInstanceZIPMap = new map<string,set<string>>();
		
		map<string,list<Position_Geography__c>> teamInstancePosGeoMap = new map<string,list<Position_Geography__c>>();
		for(Position_Geography__c posGeo : scope){
			if(!teamInstancePosGeoMap.containsKey(posGeo.Team_Instance__r.Name)){
				teamInstancePosGeoMap.put(posGeo.Team_Instance__r.Name,new list<Position_Geography__c>{posGeo});
			}else{
				list<Position_Geography__c> tempPosGeo = teamInstancePosGeoMap.get(posGeo.Team_Instance__r.Name);
				tempPosGeo.add(posGeo);
				teamInstancePosGeoMap.put(posGeo.Team_Instance__r.Name,tempPosGeo);
			}
		}
		
		for(string ti : teamInstancePosGeoMap.keyset()){
			zipDataMap = new map<string,list<SyncWithESRIBatchHelper.RequestDataWrapper>>();
			for(Position_Geography__c posGeo : teamInstancePosGeoMap.get(ti)){
			    string zip = posGeo.Geography__r.Name;
		    	system.debug('zip is '+zip);
		    	
		    	system.debug('#### teamInstanceZIPMap.containsKey : '+teamInstanceZIPMap.containsKey(posGeo.Team_Instance__r.Name));
		    	
		    	if(!teamInstanceZIPMap.containsKey(posGeo.Team_Instance__r.Name)){
					teamInstanceZIPMap.put(posGeo.Team_Instance__r.Name,new set<string>{zip});
				}else{
					set<string> temp = teamInstanceZIPMap.get(posGeo.Team_Instance__r.Name);
					temp.add(zip);
					teamInstanceZIPMap.put(posGeo.Team_Instance__r.Name,temp);
				}	
				
	           	SyncWithESRIBatchHelper.RequestDataWrapper reqData;             
	                	
	            reqData = new SyncWithESRIBatchHelper.RequestDataWrapper(zip,SalesIQGlobalConstants.REQUEST_STATUS_APPROVED,
		                  posGeo.Position__r.Client_Position_Code__c, 
		                  posGeo.Position__r.Name,
		                  posGeo.Position__r.Parent_Position__r.Client_Position_Code__c,
		                  posGeo.Position__r.Parent_Position__r.Name,
		                  posGeo.Position__r.Parent_Position__r.Parent_Position__r.Client_Position_Code__c,
		                  posGeo.Position__r.Parent_Position__r.Parent_Position__r.Name,
		                  posGeo.Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Client_Position_Code__c,
		                  posGeo.Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Name,
		                  posGeo.Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Client_Position_Code__c,  
		                  posGeo.Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Parent_Position__r.Name,
		                  posGeo.Position__r.Client_Position_Code__c,
		                  posGeo.Team_Instance__r.Team_Instance_Code__c,
		                  userinfo.getName(), false,
		                  changeReqType,alignmentCycle,
		                  posGeo.Team_Instance__r.Team__r.Name,
                          posGeo.Team_Instance__r.Team__r.Type__c);
	                   //Arun SPD-3330 Added two new parameter for Team Name and Team Type
				//reqDataForOverlay = SyncWithESRIBatchHelper.getRequestDataWrapperForOverlay(zip,SalesIQGlobalConstants.REQUEST_STATUS_APPROVED, overlayPositionsMapByBasePosition.get(request.Source_Position__c), overlayPositionsMapByBasePosition.get(request.Destination_Position__c), true, userinfo.getName());
	            zipDataMap.put(zip,new list<SyncWithESRIBatchHelper.RequestDataWrapper>{reqData});
			}
			newZipDataMap.put(ti,zipDataMap);
		}
		
		system.debug('********** teamInstanceZIPMap ' + teamInstanceZIPMap);
		
        system.debug('****** ZipDataMap ' + zipDataMap);    
        
        String endPoint = ESRIService.Service_URL__c+ESRIService.Territory_Layer__c +'query?';        
        //string body = 'f=json&returnGeometry=false&outFields=geography_name,objectid&where=geography_name+in+'+zipParamValue+'+and+team_instance_name+in+'+teamInstances;
        
        for(string ti : newZipDataMap.keyset()){
        	//tring body = 'f=json&returnGeometry=false&outFields=geography_name,objectid&where=';
        	string zipParamValue = '(';
        	for(string z : teamInstanceZIPMap.get(ti)){
        		zipParamValue += '\''+z+'\',';
        	}
        	zipParamValue = zipParamValue.removeEnd(',');
        	zipParamValue += ')';
        	
        	//body += 'geography_name+in+'+zipParamValue+'+and+team_instance_name=\''+ti+'\'';
        	String teamInstanceCode = '\''+teamInstancePosGeoMap.get(ti)[0].Team_Instance__r.Team_Instance_Code__c+'\'';
        	system.debug('--teamInstanceCode ' + teamInstanceCode);
        	string body = 'f=json&returnGeometry=false&outFields=geography_name,objectid,team_instance_name,level1_code,level1_name&where=geography_name+in+'+zipParamValue+'+and+team_instance_name='+teamInstanceCode;
        	system.debug('****** body ' + body);
        	
        	map<string,list<SyncWithESRIBatchHelper.RequestDataWrapper>> zipDataMapResult = SyncWithESRIBatchHelper.sendGetRequestToEsriToPopulateObjectId(endPoint, body, newZipDataMap.get(ti),new list<SalesIQ_Logger__c>(),'') ;
        	
        	system.debug('zipdata after get request ' + zipDataMapResult) ;
	        if(zipDataMapResult != null) {
		        //send post request to ESRI for base team
		        //Arun SPD-3330 
		    	SyncWithESRIBatchHelper.sendPostRequestToEsriToUpdateData(zipDataMapResult,'updateFeatures','');
	        }else {
				system.debug('Oops something went wrong in get request');
				//sendErrorEmail(requestData,res.getStatus());
			}
        }
	}
	
	global void finish(Database.BatchableContext BC){
        system.debug('#### ProcessFutureZIPAssignmentBatch finished');
    }
}