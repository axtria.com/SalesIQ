/******************************************************************************************************************************************************
 * Name         :   TestSalesIQAccountHierarchyCtrl.cls
 * Description  :   Test class for SalesIQAccountHierarchyCtrl
 * Author       :   Raghvendra Rathore
 * Created On   :   01/29/2018
******************************************************************************************************************************************************/
@isTest
private class TestSalesIQAccountHierarchyCtrl {



	static testMethod void testHierarchyJSON(){

		Organization_Master__c org = CallPlanTestDataFactory.createOrganizationMasterRecord();
        insert org;
        Country__c country = CallPlanTestDataFactory.createCountryMasterRecord(org.id);
        insert country;

        Workspace__c wrk = new Workspace__c();
        wrk.Workspace_Start_Date__c = Date.Today();
        wrk.Workspace_End_Date__c  = Date.today().addMonths(50);
        wrk.Name = 'Workspace 1';
        wrk.Workspace_Description__c = 'New Workspace';
        wrk.Country__c=country.id;
        insert wrk;

        Geography_Type__c geotype = CallPlanTestDataFactory.createGeographyTypeRecord(country.id);
        insert geotype;
        
        Team__c baseTeam = new Team__c(Name='HTN',Type__c='Base',Effective_End_Date__c=Date.today().addMonths(50),Country__c=country.id);
        insert baseTeam;
        
        Scenario__c scenario = new Scenario__c();
        scenario.CR_Tracking__c = false;
        scenario.Scenario_Name__c = 'HTN_Q3';
        scenario.Workspace__c = wrk.id;
        scenario.Team_Name__c = baseTeam.id;
        scenario.Request_Process_Stage__c = SalesIQGlobalConstants.READY;
        scenario.Scenario_Stage__c =    SalesIQGlobalConstants.COLLABORATION;    
        scenario.Scenario_Type__c = SalesIQGlobalConstants.SCENARIO_TYPE_ALIGNMENT;
        scenario.Scenario_Status__c = SalesIQGlobalConstants.SCENARIO_STATUS_ACTIVE;
        insert scenario;

		

        Account acc = new Account();
        acc.Name = '11788';
        acc.FirstName__c = 'Abby';
        acc.BillingCity='NJ';
        acc.BillingState='NY';
        acc.AccountNumber='11788';
        acc.type = 'Physician';
        acc.AccountType__c = 'Physician';
        acc.BillingPostalCode = '11788';
        acc.Speciality__c = '11788Oncology';
        acc.Alignment_Type__c = 'Direct Aligned';
        acc.BillingLatitude = 40.5571389900;
        acc.BillingLongitude = -74.2123329900;
        acc.External_Country_Id__c = country.id;
        insert acc;

        Account parentAccount = new Account();
        parentAccount.name = 'Parent Account';
        parentAccount.FirstName__c = 'Parent';
        parentAccount.LastName__c = 'Account';
        parentAccount.BillingCity = 'NJ';
        parentAccount.BillingState = 'NY';
        parentAccount.AccountNumber = '11790';
        parentAccount.type = 'Physician';
        parentAccount.Speciality__c = '11790Oncology';
        insert parentAccount;

        Account childAccount = new Account();
        childAccount.name = 'Child Account';
        childAccount.FirstName__c = 'Child';
        childAccount.LastName__c = 'Account';
        childAccount.BillingCity = 'NJ';
        childAccount.BillingState = 'NY';
        childAccount.AccountNumber = '11788';
        childAccount.type = 'Physician';
        childAccount.Speciality__c = '11788Oncology';
        insert childAccount;

        Affiliation_Network__c affNetworkObj = new Affiliation_Network__c(Name='Group Practice');
        insert affNetworkObj;

        Account_Affiliation__c accAffiliationObj = new Account_Affiliation__c(Account__c = childAccount.Id, Parent_Account__c = parentAccount.Id, Root_Account__c = parentAccount.Id, Affiliation_Network__c = affNetworkObj.Id);
        insert accAffiliationObj;

        Team_Instance__c BaseTeamInstance = new Team_Instance__c();
        BaseTeamInstance.Name = 'HTN_Q1_2016';
        BaseTeamInstance.Alignment_Period__c = 'Current';
        BaseTeamInstance.Team__c = baseTeam.id;
        BaseTeamInstance.Alignment_Type__c = 'ZIP';
        BaseTeamInstance.isActiveCycle__c = 'Y';  
        BaseTeamInstance.Team_Cycle_Name__c = 'Current';
        BaseTeamInstance.Scenario__c  = scenario.id;  
        BaseTeamInstance.Team_Instance_Code__c = 'DI-00001'; 
        BaseTeamInstance.IC_EffstartDate__c = Date.today().addMonths(-1);
        BaseTeamInstance.IC_EffEndDate__c = Date.today().addYears(1);       
        BaseTeamInstance.Show_Custom_Metric__c=true;
        BaseTeamInstance.Geography_Type_Name__c = geotype.id;
        BaseTeamInstance.doContiguityCheck__c = true;
        BaseTeamInstance.doDoughnutCheck__c = true;
        BaseTeamInstance.CIM_available__c = true;
        BaseTeamInstance.Affiliation_Network__c = affNetworkObj.Id;
        insert BaseTeamInstance;

        Team_Instance__c BaseTeamInstance1 = new Team_Instance__c();
        BaseTeamInstance1.Name = 'HTN_Q1_2016';
        BaseTeamInstance1.Alignment_Period__c = 'Current';
        BaseTeamInstance1.Team__c = baseTeam.id;
        BaseTeamInstance1.Alignment_Type__c = 'ZIP';
        BaseTeamInstance1.isActiveCycle__c = 'Y';  
        BaseTeamInstance1.Team_Cycle_Name__c = 'Current';
        BaseTeamInstance1.Scenario__c  = scenario.id;  
        BaseTeamInstance1.Team_Instance_Code__c = 'DI-00002'; 
        BaseTeamInstance1.IC_EffstartDate__c = Date.today().addMonths(-1);
        BaseTeamInstance1.IC_EffEndDate__c = Date.today().addYears(1);       
        BaseTeamInstance1.Show_Custom_Metric__c=true;
        BaseTeamInstance1.Geography_Type_Name__c = geotype.id;
        BaseTeamInstance1.doContiguityCheck__c = true;
        BaseTeamInstance1.doDoughnutCheck__c = true;
        BaseTeamInstance1.CIM_available__c = true;

        insert BaseTeamInstance1;

        Position__c nation = new Position__c();
        nation.name = 'USA';
        nation.Client_Territory_Name__c = 'North east';
        nation.Client_Position_Code__c = '1NE30000';
        nation.Client_Territory_Code__c='1NE30000';
        nation.Position_Type__c='Nation';
        nation.inactive__c = false;
        nation.RGB__c = '41,210,117';
        nation.Team_iD__c = baseTeam.id;
        nation.Hierarchy_Level__c='4';
        nation.Related_Position_Type__c  ='Base';
        nation.Effective_End_Date__c=Date.today().addMonths(50);
        nation.Team_Instance__c = BaseTeamInstance.id;
        nation.Position_Category__c = 'Field Position';
        insert nation;
        

        Position__c region = new Position__c();
        region.name = 'North east';
        region.Client_Territory_Name__c = 'North east';
        region.Client_Position_Code__c = '1NE30000';
        region.Client_Territory_Code__c='1NE30000';
        region.Position_Type__c='Region';
        region.inactive__c = false;
        region.RGB__c = '41,210,117';
        region.Team_iD__c = baseTeam.id;
        region.Effective_End_Date__c=Date.today().addMonths(50);
        region.Hierarchy_Level__c = '3';
        region.Related_Position_Type__c  ='Base';
        region.Team_Instance__c = BaseTeamInstance.id;
        region.Position_Category__c = 'Field Position';
        insert region;
        

        Position__c district = new Position__c();
        district.name = 'Newyork';
        district.Client_Territory_Name__c = 'Newyork';
        district.Client_Position_Code__c = '1NE30001';
        district.Client_Territory_Code__c='1NE30001';
        district.Position_Type__c='District';
        district.inactive__c = false;
        district.RGB__c = '41,210,117';
        district.Team_iD__c = baseTeam.id;
        district.Parent_Position__c = region.Id;
        district.Hierarchy_Level__c='2';
        district.Related_Position_Type__c  ='Base';
        district.Effective_End_Date__c=Date.today().addMonths(50);
        district.Team_Instance__c = BaseTeamInstance.id;
        district.Position_Category__c = 'Field Position';
        insert district;

        Position__c territory = new Position__c();
        territory.name = 'Long Island East';
        territory.Client_Territory_Name__c = 'Long Island East';
        territory.Client_Position_Code__c = '1NE30011';
        territory.Client_Territory_Code__c='1NE30011';
        territory.Position_Type__c='Territory';
        territory.inactive__c = false;
        territory.RGB__c = '41,210,117';
        territory.Team_iD__c = baseTeam.id;
        territory.Parent_Position__c = district.Id; 
        territory.Effective_Start_Date__c=Date.today();       
        territory.Effective_End_Date__c=Date.today().addMonths(50);
        territory.Related_Position_Type__c = 'Base';
        territory.Hierarchy_Level__c = '1'; 
        territory.Team_Instance__c =  BaseTeamInstance.id;
        territory.Position_Category__c = 'Field Position';
        insert territory;

        Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, null, BaseTeamInstance);
        territoryTeamInstance.X_Max__c=-72.6966429900;
        territoryTeamInstance.X_Min__c=-73.9625820000;
        territoryTeamInstance.Y_Max__c=40.9666490000;
        territoryTeamInstance.Y_Min__c=40.5821279800;
        insert territoryTeamInstance;

        Set<Id> accountIds = new set<Id>();
        accountIds.add(acc.Id);
        accountIds.add(childAccount.Id);
        accountIds.add(parentAccount.Id);

        Position_Account__c posAcc = CallPlanTestDataFactory.createPositionAccount(acc, territory, BaseTeamInstance, territoryTeamInstance);
        posAcc.Position__c = territory.Id;
        posAcc.Effective_Start_Date__c = System.today();
        posAcc.Effective_End_Date__c = System.today().addMonths(10);
        insert posAcc;


        SalesIQAccountHierarchyCtrl.dummyMethod();
        SalesIQAccountHierarchyCtrl.dummyMethod1();
        SalesIQAccountHierarchyCtrl.AccountHierarchyWrapper wrapper = new SalesIQAccountHierarchyCtrl.AccountHierarchyWrapper();
        wrapper.accountName = 'ABC';
        wrapper.accountId = acc.Id;
        wrapper.accountNumber = '1234';
        wrapper.parentId = territory.ID;
        wrapper.positionName = 'abcx';
        wrapper.isCurrent = 'true';

        SalesIQAccountHierarchyCtrl.nodes wrapper1 = new SalesIQAccountHierarchyCtrl.nodes('','');
        SalesIQAccountHierarchyCtrl.links wrapper2 = new SalesIQAccountHierarchyCtrl.links('','',0);
        
        
        SalesIQAccountHierarchyCtrl.hierarchyJSON(acc.ID, BaseTeamInstance1.Id);
        SalesIQAccountHierarchyCtrl.hierarchyJSON(childAccount.ID, BaseTeamInstance.Id);
		SalesIQAccountHierarchyCtrl.hierarchyJSON(parentAccount.ID, BaseTeamInstance.Id);
		SalesIQAccountHierarchyCtrl.hierarchyJSON(acc.ID, BaseTeamInstance.Id);
		

		

	}
   
}