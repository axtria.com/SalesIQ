@isTest
private class TestProductManagementUtility {

	@isTest static void testProductManagement() {

		Alignment_Global_Settings__c alignmentSetting  = new Alignment_Global_Settings__c();
        alignmentSetting.Name = 'TeamCodeMax';
        alignmentSetting.Tree_Hierarchy_Sort_Field__c = '1';
        insert alignmentSetting;
        Alignment_Global_Settings__c alignmentSetting1  = new Alignment_Global_Settings__c();
        alignmentSetting1.Name = 'TeamInstanceCodeMax';
        alignmentSetting1.Tree_Hierarchy_Sort_Field__c = '1';
        insert alignmentSetting1;

		Team__c accBasedTeam = new Team__c(Name='HTN',
		Type__c=SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT ,
		Effective_End_Date__c=Date.today().addMonths(50)
		);
        insert accBasedTeam;

		Team_Instance__c BaseTeamInstance = new Team_Instance__c();
        BaseTeamInstance.Name = 'HTN_Q1_2016';
        BaseTeamInstance.Alignment_Period__c = 'Current';
        BaseTeamInstance.Alignment_Type__c = 'ZIP';
        BaseTeamInstance.isActiveCycle__c = 'Y';
		BaseTeamInstance.IC_EffEndDate__c = Date.today().addMonths(50);
		BaseTeamInstance.IC_EffStartDate__c =  Date.today();
		BaseTeamInstance.Team__c = accBasedTeam.id;
        insert BaseTeamInstance;

		Product__c newproduct = new Product__c();
		newproduct.name = 'p1';
		newproduct.Product_Code__c =  'code1';
		newproduct.Effective_Start_Date__c = Date.today();
		newproduct.Effective_End_Date__c = Date.today().addMonths(50);
		insert newproduct;

		try{
		Product__c newproduct2 = new Product__c();
		newproduct2.name = 'p1';
		newproduct2.Product_Code__c =  'code2';
		newproduct2.Effective_Start_Date__c = Date.today();
		newproduct2.Effective_End_Date__c = Date.today().addMonths(50);
		insert newproduct2;

		}catch(DMLException e){
			System.assert(true, 'Product creation with same name failed');
		}


		list<product__c> listProduct = new list<product__c>();
		listProduct.add(newproduct);

		Set<id> prodSet = new Set<id>();
		prodSet.add(newproduct.id);

		Position__c pos = new Position__c();
		pos.Team_Instance__c = BaseTeamInstance.id;
		pos.Hierarchy_Level__c = '1';
		pos.inactive__c = false;
		pos.Team_id__c = accBasedTeam.id;
		insert pos;
		set<id> tiSet = new Set<id>();
		tiSet.add(BaseTeamInstance.id);

		List<Team_Instance_Product__c> productList = new List<Team_Instance_Product__c>();
		Team_Instance_Product__c tip = new Team_Instance_Product__c();
		tip.Team_Instance__c = BaseTeamInstance.ID;
		tip.Product_Master__c = newproduct.ID;

		productList.add(tip);
		insert productList ;



		List<Position_Product__c> posProdList = new List<Position_Product__c> ();
		posProdList.add(new Position_Product__c(
			Team_Instance__c=BaseTeamInstance.id,
			Product_Master__c=newproduct.id,
			Product_Weight__c = 0,
			Effective_Start_Date__c = Date.today(),
			Effective_End_Date__c = Date.today().addMonths(50)
			));
		insert posProdList;


		Test.startTest();
		ProductManagementUtility.getPositionList(tiSet);
		ProductManagementUtility.addPositionProducts(productList);
		ProductManagementUtility.removeTeamInstanceProducts(listProduct);
		ProductManagementUtility.removePositionProducts(productList);
		ProductManagementUtility.getListPositionProduct(tiSet,prodSet);

		Test.stopTest();

	}

}
