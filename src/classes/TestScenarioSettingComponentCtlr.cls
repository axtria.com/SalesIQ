/******************************************************************************************************************************************************
 * Name         :   TestScenarioSettingComponentCtlr.cls
 * Description  :   Controller for ScenarioSettingComponentCtlr.
 * Author       :   Sony
 * Created On   :   04/08/2017
******************************************************************************************************************************************************/
@isTest
private class TestScenarioSettingComponentCtlr{

    static testMethod void myUnitTest() {

        User user = CallPlanTestDataFactory.createUserWithRoleAndProfile();
        insert user;

        System.runAs(user){
        test.startTest();
        System.debug('user -'+user);
        Employee__c emp = CallPlanTestDataFactory.createEmployee(user);

        insert emp;
        System.debug('employeee --- '+emp);
        TriggerContol__c offSecurityUtil = new TriggerContol__c(Name='SecurityUtil', IsStopTrigger__c = true);
        insert offSecurityUtil;
        
        Team__c testTeam = new Team__c (Name='HTN', Type__c='Base', 
                                        Effective_Start_Date__c = system.today().addMonths(-5), 
                                        Effective_End_Date__c = Date.today().addMonths(5));
        insert testTeam;
        
        Team_Instance__c scenarioTeamInstance = new Team_Instance__c();
        scenarioTeamInstance.Name = 'HTN Q2';
        scenarioTeamInstance.Alignment_Period__c = 'Future';
        scenarioTeamInstance.Team__c = testTeam.id;
        scenarioTeamInstance.Alignment_Type__c = 'ZIP';
        scenarioTeamInstance.isActiveCycle__c = 'Y';
        insert scenarioTeamInstance;
        
        Position__c pos = CallPlanTestDataFactory.createPosition(null, testTeam);
        pos.Hierarchy_Level__c = '3';
        insert pos;

        Workspace__c workspace = new Workspace__c(Name = 'test workspace',
                                                  Workspace_Start_Date__c = system.today().addMonths(-5),
                                                  Workspace_End_Date__c = system.today().addMonths(5),
                                                  Workspace_Description__c = 'Test Workspace');
        insert workspace;

        Scenario__c scenario = new Scenario__c();
        scenario.Team_Instance__c = scenarioTeamInstance.Id;
        scenario.Effective_Start_Date__c = System.today();
        scenario.Effective_End_Date__c = system.today().addMonths(2);
        scenario.Scenario_Stage__c = 'Publishing';
        scenario.Request_Process_Stage__c = 'Ready';
        scenario.Scenario_Status__c = 'Active';
        scenario.Workspace__c = workspace.Id;
        scenario.Employee_Assignment__c = true;
        insert scenario;
        
        User_Access_Permission__c ua = CallPlanTestDataFactory.createUserAccessPerm(pos, user, scenarioTeamInstance);
        ua.Sharing_Type__c = 'Explicit';
        insert ua;

        User_Access_Permission__c access = CallPlanTestDataFactory.createUserAccessPerm(pos, user, scenarioTeamInstance);
        access.Sharing_Type__c = 'Implicit';
        insert access;
        
         TriggerContol__c triggerControl = new TriggerContol__c (IsStopTrigger__c = true , Name='SecurityUtil');
        insert triggerControl;
        
        ScenarioSettingComponentCtlr.getScenarioData(String.valueof(scenario.id));
        ScenarioSettingComponentCtlr.saveScenario(scenario,true);
        ScenarioSettingComponentCtlr.dummyMethod();
     
          
        test.stopTest();
    }
    }

    
}