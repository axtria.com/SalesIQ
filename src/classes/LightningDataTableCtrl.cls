public with sharing class LightningDataTableCtrl 
{
	@AuraEnabled
    public static String getEditEmployeeConfigSetting(){

        if(Alignment_Global_Settings__c.getValues('EnableCustomEditEmployee') != null)
             return String.valueOf(Alignment_Global_Settings__c.getValues('EnableCustomEditEmployee').Tree_Hierarchy_Sort_Field__c);
        else
            return 'false';


    }

    @AuraEnabled
    public static List<SObject> getPaginationData(String pageNumber, String pageSize,String objectName, String queryString){

    	/*System.debug('queryString :'+queryString);
    	System.debug('objectName :'+objectName);*/

    	list<SObject> objList = new list<SObject>();

    	Integer psize = Integer.valueOf(pageSize);
        Integer pnumber = Integer.valueOf(pageNumber)-1;

    	String query = queryString;
        if(query.indexOf('LIMIT') != -1){
            queryString = queryString.split('LIMIT')[0];
            query = queryString + ' LIMIT '+Integer.valueOf(psize) +' OFFSET '+(psize*pnumber);
        }else{
            query = queryString + ' LIMIT '+Integer.valueOf(psize) +' OFFSET '+(psize*pnumber);
        }

    	//query = queryString + ' LIMIT '+Integer.valueOf(psize) +' OFFSET '+(psize*pnumber);

    	//System.debug('query :'+query);

    	objList = Database.query(query);

    	return objList;
    }

    @AuraEnabled
    public static List<String> getDateFormat(){
        return new List<String>{SalesIQUtility.getUserDateFormat(), SalesIQUtility.getUserDateTimeFormat()};
    }

    @AuraEnabled
    public static String getSelectedTeamInstance(String employeeId)
    {
        string teamInstance = '';
        list<Position_Employee__c> posEmp =[SELECT Position__r.Team_Instance__r.Team__r.id FROM Position_Employee__c WHERE Employee__r.id = :employeeId AND Assignment_Type__c = 'Primary' ORDER BY Effective_Start_Date__c Desc LIMIT 1];
        if(posEmp.size()>0){
            teamInstance = System.Label.All_Positions+'_'+posEmp[0].Position__r.Team_Instance__r.Team__r.id;
        }
        return teamInstance;
    }

}