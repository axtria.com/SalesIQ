/************************************************************************************************
Name			:		HandlerPosGeo
Description		:		Handler class for PositionGeographyTrigger
Last Updated By	:		Raghvendra Rathore - 29/02/2016
************************************************************************************************/
public with sharing class HandlerPosGeo {

	public static list<Position_Geography__c> getPosGeoByIds(set<Id> posGeoIDs){
		if(SecurityUtil.checkRead(Position_Geography__c.SObjectType, new list<string>{'Position_Team_Instance__c', 'Position_Id_External__c'}, false) ){
			return [select id, Position_Team_Instance__c, Position_Id_External__c from Position_Geography__c where Id in: posGeoIDs];
		}

		return new list<Position_Geography__c>();
	}

	//Future Method - To tackle 101 Sql Queries Exception
	@future
    public static void rollupExtentsOnTerritoryFuture(set<Id> posGeoNewIds){
        rollupExtentsOnTerritory(posGeoNewIds, new list<set<Id>>());
    }	


    public static void rollupExtentsOnTerritoryFutureBulk(set<Id> posGeoNewIds){
        rollupExtentsOnTerritory(posGeoNewIds, new list<set<Id>>());
    }	


    /*
	* This method will Aggregate extents and update all positions at territory level
    * @return void
    */ 
    public static void rollupExtentsOnTerritory(set<Id> posGeoNewIds, list<set<Id>> deletePosGeos){
        
        set<Id> parentIds = new set<Id>();
	    set<Id> positionIds = new set<Id>();
        
        if(deletePosGeos.size() == 0){
	    	list<Position_Geography__c> posGeoList = getPosGeoByIds(posGeoNewIds);
	   
	   		for(Position_Geography__c posgeoObj : posGeoList){
	   		    if(posgeoObj.Position_Team_Instance__c!=null && posgeoObj.Position_Id_External__c!=null){
        	   		parentIds.add(posgeoObj.Position_Team_Instance__c);
    	   			positionIds.add(posgeoObj.Position_Id_External__c);
    	   		}
	   		}
   		}
   		else{
   			parentIds.addAll(deletePosGeos[0]);
   			positionIds.addAll(deletePosGeos[1]);
   		}

        system.debug('--parentIDs ' + parentIDs);
        system.debug('--positionIds ' + positionIds);
        
        
        map<Id,Position_Team_Instance__c> positionTeamInstanceMap = null;
        list<string> POSITION_TEAM_INSTANCE_READ_FIELD = new list<string>{'Position_ID__c', 'Parent_Position_ID__c', 'team_instance_id__c'};
        if(SecurityUtil.checkRead(Position_Team_Instance__c.SObjectType, POSITION_TEAM_INSTANCE_READ_FIELD, false) && SecurityUtil.checkRead(team_Instance__c.SObjectType, new list<string>{'team__c'}, false) && SecurityUtil.checkRead(team__c.SObjectType, new list<string>{'type__c'}, false)){
        	positionTeamInstanceMap = new map<Id,Position_Team_Instance__c>([SELECT Id, Position_ID__c, team_instance_id__r.team__r.type__c, Parent_Position_ID__c FROM Position_Team_Instance__c WHERE Id IN : parentIds and Position_ID__r.client_position_code__c !=: SalesIQGlobalConstants.UNASSIGNED_TERRITORY_CODE]);
        }  
             
        string teamType = SalesIQGlobalConstants.TEAM_TYPE_BASE;
        list<ID> allPositionTeamInstanceIds = new list<ID>();
        allPositionTeamInstanceIds.addAll(parentIds);
        if(positionTeamInstanceMap.size() > 0){
        	for(Id posId : parentIDs){
        		if(positionTeamInstanceMap.containsKey(posId)){
        			teamType = positionTeamInstanceMap.get(posId).team_instance_id__r.team__r.type__c;
        			break;
        		}
        	}
        }
        
        //map<Id,Position_Geography__c> posGeoMap = new map<Id,Position_Geography__c>([select id from Position_Geography__c where Position_Id_External__c IN : positionIds]);
        //System.debug('posGeoMap --'+posGeoMap.size());
        
        list<string> POSITION_GEOGRAPHY_READ_FIELD = new list<string>{'Position_Team_Instance__c'};        
        list<string> GEOGRAPHY_READ_FIELD = new list<string>{'X_Max__c','X_Min__c','Y_Max__c','Y_Min__c'};
        if(SecurityUtil.checkRead(Geography__c.SObjectType, GEOGRAPHY_READ_FIELD, false) && SecurityUtil.checkRead(Position_Geography__c.SObjectType, POSITION_GEOGRAPHY_READ_FIELD, false)){
        	//Fetch aggregated extents at Level 1 position level.
        	list<AggregateResult> aggregateResults = [SELECT Position_Team_Instance__c, 
	        										  max(Geography__r.X_Max__c) xmax, 
	        										  min(Geography__r.X_Min__c) xmin, 
	        										  max(Geography__r.Y_Max__c) ymax, 
	        										  min(Geography__r.Y_Min__c) ymin 
	                                                  FROM Position_Geography__c 
	                                                  WHERE Position_Id_External__c IN : positionIds 
	                                                  and ((Assignment_status__c =: SalesIQGlobalConstants.POSITION_GEOGRAPHY_ACTIVE and Position_Team_Instance__r.Team_Instance_ID__r.Alignment_Period__c =: SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE) or (Assignment_status__c =: SalesIQGlobalConstants.POSITION_GEOGRAPHY_FUTURE_ACTIVE and Position_Team_Instance__r.Team_Instance_ID__r.Alignment_Period__c =: SalesIQGlobalConstants.FUTURE_TEAM_CYCLE_TYPE )) 
	                                                  and Position__r.client_position_code__c !=: SalesIQGlobalConstants.UNASSIGNED_TERRITORY_CODE 
	                                                  GROUP BY Position_Team_Instance__c];        
        
	        list<Position_Team_Instance__c> PositionToRollup = new list<Position_Team_Instance__c>();
	        system.debug('--aggregateResults ' + aggregateResults);
	        
	        set<string> positionNotHavingPosGeo = new set<string>();
	        set<string> positionHavingPosGeo = new set<string>();
	        //Update extents and centroids at Level 1 position level.
	        for(AggregateResult ar : aggregateResults){
	            if(positionTeamInstanceMap.containsKey((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Position_Team_Instance__c'))){
	                Position_Team_Instance__c positionToUpdate = positionTeamInstanceMap.get((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Position_Team_Instance__c'));
	
	                positionToUpdate.X_Max__c = (decimal)ar.get('xmax');
		          	positionToUpdate.X_Min__c = (decimal)ar.get('xmin');
		            positionToUpdate.Y_Max__c = (decimal)ar.get('ymax');
		            positionToUpdate.Y_Min__c = (decimal)ar.get('ymin');
		            if((decimal)ar.get('xmax') != null && (decimal)ar.get('xmin') != null && (decimal)ar.get('ymax') != null &&(decimal)ar.get('ymin') != null){
	                	positionToUpdate.Centroid_Lattitude__c = (positionToUpdate.X_Max__c + positionToUpdate.X_Min__c)/2;
	                	positionToUpdate.Centroid_Longitude__c = (positionToUpdate.Y_Max__c + positionToUpdate.Y_Min__c)/2;	                
		            }
		            PositionToRollup.add(positionToUpdate);
		            positionHavingPosGeo.add(positionToUpdate.id);
	            }
	        }
	        
	        for(string posTeamInsId : parentIDs){
	        	if(!positionHavingPosGeo.contains(posTeamInsId)){
	        		positionNotHavingPosGeo.add(posTeamInsId);
	        	}
	        }
	        
	        system.debug('--positionNotHavingPosGeo ' + positionNotHavingPosGeo.size());
	        
	        list<position_team_instance__c> positionTeamInsList ;
	        
	        List<String> POSITION_TEAM_INSTANCE_READ_FIELDS = new List<String>{'X_Max__c', 'X_Min__c', 'Y_Max__c', 'Y_Min__c', 'Centroid_Lattitude__c', 'Centroid_Longitude__c'} ;
	        if(SecurityUtil.checkRead(Position_Team_Instance__c.SObjectType, POSITION_TEAM_INSTANCE_READ_FIELDS, false)) {
	        	positionTeamInsList = [select id, X_Max__c, X_Min__c, Y_Max__c, Y_Min__c, Centroid_Lattitude__c, Centroid_Longitude__c from Position_Team_Instance__c where id in: positionNotHavingPosGeo and Position_ID__r.client_position_code__c !=: SalesIQGlobalConstants.UNASSIGNED_TERRITORY_CODE];
			}
	        
	        for(position_team_instance__c posTeamIns : positionTeamInsList){
	        	posTeamIns.X_Max__c = null;
	        	posTeamIns.X_Min__c = null;
	        	posTeamIns.Y_Max__c = null;
	        	posTeamIns.Y_Min__c = null;
	        	posTeamIns.Centroid_Lattitude__c = null;
	        	posTeamIns.Centroid_Longitude__c = null;
	        }
	        
	        PositionToRollup.addAll(positionTeamInsList);
	        
	        system.debug('--PositionToRollup ' + PositionToRollup);
	        if(PositionToRollup.size() > 0){
	        	//Update extents and centroids on Level 1 positions
	        	list<string> POSITION_TEAM_INSTANCE_UPDATE_FIELD = new list<string>{'X_Max__c','X_Min__c','Y_Max__c','Y_Min__c','Centroid_Lattitude__c','Centroid_Longitude__c'};
	        	if(SecurityUtil.checkUpdate(Position_Team_Instance__c.SObjectType, POSITION_TEAM_INSTANCE_UPDATE_FIELD, false)){
	        		update PositionToRollup;	
	        	}
	            
	            //Calling method to rollup extents on parent positions (Level 2)
	            //SIMPS-1448 , remove overlay position check while calculating extent
	            if(teamType != null )//&& teamType != SalesIQGlobalConstants.TEAM_TYPE_OVERLAY
	            	list<Position_Team_Instance__c> districts = HandlerPosGeo.rollupExtentsOnParents(PositionToRollup);            
	        }
        }
        
    }
    
    /*
	* This method will Aggregate the extent and update it at parent position
    * @return list<Position_Team_Instance__c> of all positions updated by method
    */
    public static list<Position_Team_Instance__c> rollupExtentsOnParents(list<Position_Team_Instance__c> newRecList){
        set<Id> parentIds = new set<Id>();
        
        for(Position_Team_Instance__c s : newRecList) {
        	parentIds.add(s.Parent_Position_ID__c);
        }
        
        map<Id,Position_Team_Instance__c> positionMap = SalesIQUtility.getPositionTeamInstanceMap(parentIds);
        if(positionMap != null && positionMap.size() > 0){
        	list<string> POSITION_TEAM_INSTANCE_READ_FIELD = new list<string>{'Parent_Position_ID__c','X_Max__c','X_Min__c','Y_Max__c','Y_Min__c'};
        	if(SecurityUtil.checkRead(Position_Team_Instance__c.SObjectType, POSITION_TEAM_INSTANCE_READ_FIELD, false)){
        		list<AggregateResult> aggregateResults = [SELECT Parent_Position_ID__c, max(X_Max__c) xmax, min(X_Min__c) xmin, max(Y_Max__c) ymax, min(Y_Min__c) ymin 
		                                                  FROM Position_Team_Instance__c WHERE Parent_Position_ID__c IN : parentIds and Position_ID__r.client_position_code__c !=: SalesIQGlobalConstants.UNASSIGNED_TERRITORY_CODE 
		                                                  GROUP BY Parent_Position_ID__c];       
        
		        for(AggregateResult ar : aggregateResults){
		        	if(positionMap.containsKey((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Parent_Position_ID__c'))){
		            	positionMap.get((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Parent_Position_ID__c')).X_Max__c = (decimal)ar.get('xmax');
		                positionMap.get((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Parent_Position_ID__c')).X_Min__c = (decimal)ar.get('xmin');
		                positionMap.get((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Parent_Position_ID__c')).Y_Max__c = (decimal)ar.get('ymax');
		                positionMap.get((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Parent_Position_ID__c')).Y_Min__c = (decimal)ar.get('ymin');
		                
		                if((decimal)ar.get('xmax') != null && (decimal)ar.get('xmin') != null && (decimal)ar.get('ymax') != null &&(decimal)ar.get('ymin') != null){
			                positionMap.get((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Parent_Position_ID__c')).Centroid_Lattitude__c = ((decimal)ar.get('xmax') + (decimal)ar.get('xmin'))/2;
			                positionMap.get((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Parent_Position_ID__c')).Centroid_Longitude__c = ((decimal)ar.get('ymax') + (decimal)ar.get('ymin'))/2;
		                }
		            }
		        }
		        
		        list<string> POSITION_TEAM_INSTANCE_UPDATE_FIELD = new list<string>{'Parent_Position_ID__c','X_Max__c','X_Min__c','Y_Max__c','Y_Min__c','Centroid_Lattitude__c','Centroid_Longitude__c'};
		        if(SecurityUtil.checkUpdate(Position_Team_Instance__c.SObjectType, POSITION_TEAM_INSTANCE_UPDATE_FIELD, false)){
		        	update positionMap.values();
		        }
        	}
        	
        }
        
        //Calling method to rollup extents on parent positions above Level 2 (Iteratively)
	    list<Position_Team_Instance__c> parentPositions;
	    if(positionMap.size() > 0){
	    	parentPositions = HandlerPosGeo.rollupExtentsOnParents(positionMap.values());
	    }else{
	    	parentPositions = positionMap.values();
	    }
	    return parentPositions;
    }
}