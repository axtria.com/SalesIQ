/**
* Description   : Queueable class to enque SyncWithESRIBatch
* Author        : Raghvendra.Rathore
* Since         : 8-June-2016
* Version       : 1.0
*/

global with sharing class QueueESRIBatchProcess implements System.Queueable, Database.AllowsCallouts {
    list<string> reqIds;
    boolean crTracking;
    list<string> insertDeletePositionGeography;
    string sharingOperationType;
    Set<Id> geoList;
    Change_Request__c crr;
    string requestType;
    Position_Team_Instance__c posTeamInstanceUnassignedOverlay;
    Position__c sourcePositionObj; 
    Map<string, string> mapPosAndAssignment;

    public QueueESRIBatchProcess(list<String> requestIds){        
        reqIds = new list<String>();
        reqIds.addAll(requestIds);
        crTracking = true;
        requestType=SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP;
        system.debug('in constructor');
    }


    public QueueESRIBatchProcess(list<String> requestIds, Set<Id> geList, Change_Request__c cr){        
        reqIds = new list<String>();
        reqIds.addAll(requestIds);
        crTracking = true;
        crr = new Change_Request__c();
        crr = cr;
        requestType=SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP;
        geoList = new Set<Id>();
        geoList.addAll(geList);
        system.debug('list is'+geoList);
    } 

     /*@author : Arun Kumar
    @date : 6 July 2017  
    @description : This method will will call from AlignmentUtility when CR tracking is Off  
    @param1: list of position geography
    @param2: boolean value of cr tracking   
    @return : none
    */

    public QueueESRIBatchProcess(list<String> requestIds, boolean cRTackingEnable){
        system.debug('requestIds ' +requestIds);
        crTracking = cRTackingEnable;
        reqIds = new list<String>();
        reqIds.addAll(requestIds);
        requestType=SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP;
    }


    public QueueESRIBatchProcess(list<String> requestIds, boolean cRTackingEnable, Position_Team_Instance__c posTeamInstanceUnassignedOverlay, Position__c sourcePositionObj, Map<string, string> mapPosAndAssignment, string requestType){
        system.debug('-- batch process requestIds ' +requestIds);
        crTracking = cRTackingEnable;
        reqIds = new list<String>();

        this.posTeamInstanceUnassignedOverlay = posTeamInstanceUnassignedOverlay;
        this.sourcePositionObj = sourcePositionObj;
        this.mapPosAndAssignment = mapPosAndAssignment;
        this.requestType = requestType;

        reqIds.addAll(requestIds);
    }
    
    /*@author : Arun Kumar
    @date : 4 Aug 2017  
    @description : This method will call from AlignmentUtility when ZIP Sharing is enabled and user is sharing ZIP among different territory
    @param1: list of position geography to be updated
    @param2: boolean value of cr tracking   
    @return : none
    */

    public QueueESRIBatchProcess(list<String> tobeUpdatePositionGeography,list<String> tobeInsertDeletePositionGeography, boolean cRTackingEnable,string movementType, string operationType ){
        system.debug('requestIds ' +tobeUpdatePositionGeography);
        requestType = movementType;
        sharingOperationType = operationType;
        crTracking = cRTackingEnable;
        reqIds = new list<String>();        
        reqIds.addAll(tobeUpdatePositionGeography);
        insertDeletePositionGeography = new list<string>();
        insertDeletePositionGeography.addAll(tobeInsertDeletePositionGeography);
    }
    
    public void execute(QueueableContext context) {  
        
        system.debug('Inside execute ');
        list<SalesIQ_Logger__c> lstLogger = new list<SalesIQ_Logger__c>();
        //lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('In execute method of QueueESRIBatchProcess with id '+reqIds ,'', '' ));
        insert lstLogger;
        if(requestType== SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP){
            if(crr!=null && geoList.size()>0 && crTracking){
                system.debug('list is'+geoList);
                 Id jobId = database.executeBatch(new SyncWithESRIBatch(reqIds,geoList,crr),1500);      
            }
            else if(crTracking){
                Id jobId = database.executeBatch(new SyncWithESRIBatch(reqIds),2);  
            }else{
                Id jobId = database.executeBatch(new SyncWithESRIBatch(reqIds,crTracking),1500);  
            }
        }else if(requestType==SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP_SHARING){
            system.debug('######## Zip Sharing Esri Sync');
            Id jobId = database.executeBatch(new SyncWithESRIBatch(reqIds,InsertDeletePositionGeography,crTracking,requestType,sharingOperationType),1500);  
        }else if(requestType == SalesIQGlobalConstants.CREATE_OVERLAY_POSITION || requestType == SalesIQGlobalConstants.UPDATE_OVERLAY_POSITION){
                system.debug('--Inside overlay esri sync case');
                Id jobIdoverlay = database.executeBatch(new SyncWithESRIBatch(reqIds, crTracking, posTeamInstanceUnassignedOverlay, sourcePositionObj, mapPosAndAssignment, requestType),1500);
        } 
        else if(requestType==SalesIQGlobalConstants.REQUEST_STATUS_SUBMISSION_PENDING || requestType==SalesIQGlobalConstants.REQUEST_STATUS_DELETE_SUBMISSION_PENDING){
            system.debug('######## Submission Esri Sync');
            Id jobId = database.executeBatch(new SyncWithESRIBatch(reqIds,new list<string>(),crTracking,requestType,sharingOperationType),1500);  
        }
             
    }
}