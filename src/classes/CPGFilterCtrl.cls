public with sharing class CPGFilterCtrl {    
    static  List<string> lstExpression=new List<string>();
    static list<Data_Set_Column_Detail__c> lstColumns = new list<Data_Set_Column_Detail__c>();
    static integer expressionSequence=1;
    public static string scenarioRuleInstanceId{get;set;}
    

    @AuraEnabled
    public static List<Scenario_Rule_Instance_Details__c> getScenarioID(String ScenarioRuleInstanceId){
     
       List<Scenario_Rule_Instance_Details__c> sceID = [SELECT Id, Scenario_Id__c FROM Scenario_Rule_Instance_Details__c where id=:ScenarioRuleInstanceId];
       return sceID;
    }
     
    
    static list<Data_Set_Column_Detail__c> getColumnsData(String scenarioInstanceId){
        list<Data_Set_Rule_Map__c> lstRuleMap = [select id,dataset_id__c,table_display_name__c from Data_Set_Rule_Map__c
                                                 where ds_type__c='I' and scenario_rule_instance_id__r.Id =: scenarioInstanceId];
        //list<Data_Set_Column_Detail__c> lstcolumns;
        //system.debug(lstRuleMap[0].table_display_name__c);
        if(lstRuleMap.size()>0)
        {
            lstcolumns=[select Id,ds_col_name__c,column_id__c,datatype__c,value_type__c,variable_type__c,
                        tb_col_nm__c,Table_Data_type__c from Data_Set_Column_Detail__c where dataset_id__r.Id=:lstRuleMap[0].dataset_id__c]; //a1af4000000hFWiAAM
            
        }
        system.debug(lstcolumns);        
        return lstcolumns;
    }
    @AuraEnabled
    public static String getInputTableName(String scenarioInstanceId){
        
        list<Data_Set_Rule_Map__c> lstRuleMap = [select id,dataset_id__c,table_display_name__c from Data_Set_Rule_Map__c
                                                  where ds_type__c='i' and scenario_rule_instance_id__c=: scenarioInstanceId];
      
        system.debug(lstRuleMap);
        return lstRuleMap[0].table_display_name__c;
    }

    @AuraEnabled
    public static String getOutputTableName(String scenarioInstanceId){
        system.debug(scenarioInstanceId);
        list<Data_Set_Rule_Map__c> lstRuleMap = [select id,dataset_id__c,table_display_name__c from Data_Set_Rule_Map__c
                                                  where ds_type__c='o' and scenario_rule_instance_id__c=: scenarioInstanceId];
      
        system.debug(lstRuleMap);
        return lstRuleMap[0].table_display_name__c;
    }
    
    
    
    public static list<BRColumnWrapper> CreateColumnWrapper(String scenarioId)
    {
        list<Data_Set_Rule_Map__c> lstRuleMapOutput = [select id,dataset_id__c,table_display_name__c from Data_Set_Rule_Map__c
                                                 where ds_type__c='O' and scenario_rule_instance_id__r.Id =: scenarioId];
        list<Data_Set_Column_Detail__c> lstcolumnsOutput;
        if(lstRuleMapOutput.size()>0)
        {
            lstcolumnsOutput=[select ds_col_name__c,column_id__c,datatype__c,value_type__c,variable_type__c,
                        tb_col_nm__c,Table_Data_type__c from Data_Set_Column_Detail__c where dataset_id__r.Id=:lstRuleMapOutput[0].dataset_id__c]; //a1af4000000hFWiAAM
            
        }

        list<BRColumnWrapper> lstColumnData = new list<BRColumnWrapper>();
        list<Data_Set_Column_Detail__c> lstColumns=getColumnsData(scenarioId);
        set<String> setColumnNames = new set<String>();
        for(Data_Set_Column_Detail__c obj:lstcolumnsOutput){
            setColumnNames.add(obj.ds_col_name__c);
        }
        for(Data_Set_Column_Detail__c n : lstColumns)
          {
              BRColumnWrapper columns=new BRColumnWrapper();
              columns.Id=n.Id;
              columns.Name=n.ds_col_name__c;
              columns.variabletype=n.variable_type__c;
              columns.colDatatype=n.datatype__c;
              columns.colValuetype=n.value_type__c;
              columns.tableDataType = n.Table_Data_type__c;
              columns.tableColumnName = n.tb_col_nm__c;
              if(setColumnNames.contains(n.ds_col_name__c)){
                columns.statusFlag=true;
              }else{
                columns.statusFlag=false;
              }
              lstColumnData.add(columns);
           }
        
         return lstColumnData;  
        
    }
    @AuraEnabled
    public static void saveUpdate(String ruleName, String parsedJson,String finalQuery,String scenarioRuleInstanceId,String componentId,String columndata,String stagingQuery){
        list<BRColumnWrapper> lstColumnWrapper = (list<BRColumnWrapper>)JSON.deserialize(columndata,list<BRColumnWrapper>.class);
        list<Business_Rules__c> lstRules = [select id from Business_Rules__c where ScenarioRuleInstanceDetails__c =:scenarioRuleInstanceId];
        Id busiId;
        system.debug('Id-->'+scenarioRuleInstanceId);
        if(lstRules!=Null && lstRules.size()>0){
            UpdateBusinessRule(lstRules[0].Id, ruleName);
            deleteBusinessFilters(lstRules[0].Id);
            busiId=lstRules[0].Id;
        }
        else{
            busiId= SaveBusinessRule(ruleName,scenarioRuleInstanceId);      
        }
        saveInFilterRuleEntityDetail(parsedJson,finalQuery,busiId,stagingQuery);
        SaveColumnsToInclude(lstColumnWrapper,scenarioRuleInstanceId);
        SaveBusinessRuleFieldMap(busiId,ruleName);
    }

    public static void SaveBusinessRuleFieldMap(Id busiId,String ruleName)
    {        
        Business_Rule_FieldMap_Details__c fieldMap = new Business_Rule_FieldMap_Details__c();
        fieldMap.Business_Rule__c=busiId;
        fieldMap.IO_Flag__c='I';
        fieldMap.Param_Name__c=ruleName;
        fieldMap.Param_Type__c= 'Derived';
        insert fieldMap;
                
    }

    @AuraEnabled
    public static string getBusinessRuleId(String scenarioRuleInstanceId){
        list<Business_Rules__c> lstRules = [select id from Business_Rules__c where ScenarioRuleInstanceDetails__c =:scenarioRuleInstanceId];
        system.debug('lstRules--'+lstRules);
        if(lstRules.size()>0){
                return lstRules[0].Id;
        }else{
            return '';
        }
        
    }
    static void UpdateBusinessRule(String ruleId, String ruleName){
        Business_Rules__c rule = [select id from Business_Rules__c where id=:ruleId];  
        rule.name=ruleName;        
        update rule;        
    }
     public static Id SaveBusinessRule(String ruleName,String scenarioId){
        Business_Rule_Type_Master__c ruleMaster = [select id from Business_Rule_Type_Master__c 
                                                   where name ='Filter'
                                                   limit 1];
        Scenario_Rule_Instance_Details__c scenarioRule = [select id from Scenario_Rule_Instance_Details__c   
                                                   where id =: scenarioId
                                                   limit 1];
        Business_Rules__c rule = new Business_Rules__c();
        rule.name=ruleName;
        rule.Business_Rule_Type_Master__c=ruleMaster.id;
        rule.ScenarioRuleInstanceDetails__c=scenarioRule.id;
        rule.Is_Derived__c=true;
        rule.Is_Mandatory__c=true;
        insert rule;
        
        system.debug('rule'+rule.id);
        return rule.id;
    }   
    
    static void deleteBusinessFilters(String ruleId){
        
        list<Business_Rule_FieldMap_Details__c> lstFieldMap=[select id from Business_Rule_FieldMap_Details__c
                                                     where Business_Rule__c =: ruleId];
        delete lstFieldMap;
    
        list<Filter_Rule_Entity_Detail__c> lstRuleEntity = [SELECT Base_Filter_Rule_Entity_Detail__c,Business_Rules__c,Category_Type__c,Filter_Display_Expression__c,Entity_Type__c,Filter_Rule_Entity_Detail_Ext_Id__c,Id FROM Filter_Rule_Entity_Detail__c WHERE Business_Rules__c =: ruleId];
        list<Filter_Rule_Criteria_Detail__c> lstGrpEntity = [SELECT Criteria_operator__c,FieldDataType__c,Filter_Field_Name__c,Filter_Rule_Criteria_Detail_Ext_Id__c,Filter_Rule_Entity_Detail__c,Id,Logic_operator__c,Param_value__c,Sequence__c FROM Filter_Rule_Criteria_Detail__c  where Filter_Rule_Entity_Detail__c in: lstRuleEntity];        
        delete lstGrpEntity;
              
        delete lstRuleEntity;   
    }

    static void saveInFilterRuleEntityDetail(String parsedJson,String finalQuery,Id busiRuleId,String stagingQuery){        
            Filter_Rule_Entity_Detail__c ruleEntity = new Filter_Rule_Entity_Detail__c();
            ruleEntity.Category_Type__c='If';   
            ruleEntity.Business_Rules__c = busiRuleId;
            ruleEntity.Entity_Type__c='Block';
            ruleEntity.Filter_Display_Expression__c=finalQuery;
            ruleEntity.Json_Expression__c=parsedJson;
            ruleEntity.Filter_Exec_expression__c=stagingQuery;
            insert ruleEntity;
        
    }    
        
    
    
    static void SaveColumnsToInclude(list<BRColumnWrapper> lstColumnWrapper,string scenarioInsId)
    {   
        list<Data_Set_Rule_Map__c> lstRuleMap = [select id,dataset_id__c,table_display_name__c from Data_Set_Rule_Map__c
                                                 where ds_type__c='O' and scenario_rule_instance_id__r.Id =: scenarioInsId];

        list<Data_Set_Column_Detail__c> lstColumnDetail = new  list<Data_Set_Column_Detail__c>();
        list<Data_Set_Column_Detail__c> lstColumnDetailToInsert = new  list<Data_Set_Column_Detail__c>();
        Map<string,ID> mapColumnDetails=new Map<string,ID>();
        list<String> lstColumnDetailToDelete = new  list<String>();
        set<String> setColumnName = new set<String>();
        List<Data_Set_Column_Detail__c> lstColumnNames =[select Id,ds_col_name__c,column_id__c,datatype__c,value_type__c,variable_type__c,
                        tb_col_nm__c from Data_Set_Column_Detail__c where dataset_id__r.Id=:lstRuleMap[0].dataset_id__c];
        for(Data_Set_Column_Detail__c obj:lstColumnNames){
            setColumnName.add(obj.ds_col_name__c);
            mapColumnDetails.put(obj.ds_col_name__c,obj.Id);
        }
        system.debug(mapColumnDetails);
        for(BRColumnWrapper col : lstColumnWrapper)
        {

            if(col.statusFlag==true && !setColumnName.contains(col.Name)){
                Data_Set_Column_Detail__c  columnDetail=new Data_Set_Column_Detail__c ();
                columnDetail.ds_col_name__c= col.Name; 
                columnDetail.datatype__c=col.colDatatype;
                columnDetail.value_type__c=col.colValuetype;
                columnDetail.variable_type__c=col.variabletype;
                columnDetail.Table_Data_type__c = col.tableDataType;
                columnDetail.tb_col_nm__c = col.tableColumnName.replace('.','_');
                columnDetail.dataset_id__c=lstRuleMap[0].dataset_id__c;
                lstColumnDetailToInsert.add(columnDetail);
            }else if(col.statusFlag==false){
                if(setColumnName.contains(col.Name)){
                    lstColumnDetailToDelete.add(mapColumnDetails.get(col.Name));
                }

            }
            
        }
        system.debug(lstColumnDetailToDelete);
        List<Data_Set_Column_Detail__c> lstColumnNamesToBeDeleted =[select Id from Data_Set_Column_Detail__c where Id IN:lstColumnDetailToDelete];
        delete lstColumnNamesToBeDeleted;
        insert lstColumnDetailToInsert;
        system.debug('Columns Inserted:'+lstColumnDetailToInsert);
    }
    


    @AuraEnabled
    public static List<String> getRuleName(String scenarioId){
        Business_Rules__c rule = [select id,name from Business_Rules__c where ScenarioRuleInstanceDetails__c=: scenarioId limit 1];
        Filter_Rule_Entity_Detail__c entityRuleObj = [Select Id,Filter_Display_Expression__c,Json_Expression__c from Filter_Rule_Entity_Detail__c where Business_Rules__c=:rule.Id];
        List<String> strRuleNameAndFilterDetail = new List<String>();
        strRuleNameAndFilterDetail.add(rule.Name);
        strRuleNameAndFilterDetail.add(entityRuleObj.Json_Expression__c);
        strRuleNameAndFilterDetail.add(entityRuleObj.Filter_Display_Expression__c);
        return strRuleNameAndFilterDetail;
    }
    //Kirti
    @AuraEnabled
    public static list<BRColumnWrapper> getColumns(string scenarioId)
    {
        list<BRColumnWrapper> columns= CreateColumnWrapper(scenarioId);
        return columns;        
    }

}