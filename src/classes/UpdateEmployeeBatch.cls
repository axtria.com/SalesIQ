//This batch will update the HR Status to Active/Inactive based on Employee Status Record
global class UpdateEmployeeBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    
    global String query;
    global date todayDate  ;
    global string status ;

    
    global Database.QueryLocator start(Database.BatchableContext BC){
         Database.QueryLocator dbquery;
         
          integer processday=0;
        if(TotalApproval__c.getValues('ProcessDay') !=null){
            processday = integer.valueof(TotalApproval__c.getValues('ProcessDay').No_Of_Approval__c);
        }
        todayDate=date.today().adddays(processday);
        status= SalesIQGlobalConstants.EMPLOYEE_STATUS_TERMINATED;
        query = 'select id,HR_Status__c, (select id,Employee_Status__c,Employee__c from Employee_Status__r where Effective_Start_Date__c<=:todayDate and Effective_End_Date__c>=:todayDate ), (select id,Employee__c, Position__c from position_Employees__r where Effective_Start_Date__c<=:todayDate and Effective_End_Date__c>=:todayDate and Assignment_Type__c=\'Primary\' ) from employee__c where HR_Status__c !=:status';
        list<string> EMPLOYEE_STATUS_READ_FIELD = new list<string>{'Employee__c','Effective_Start_Date__c','Effective_End_Date__c','Employee_Status__c'};
        list<string> EMPLOYEE_READ_FIELD = new list<string>{'HR_Status__c'};
        list<string> POSITION_EMPLOYEE_READ_FIELD = new list<string>{'Employee__c','Effective_Start_Date__c','Effective_End_Date__c'};
        
        if(SecurityUtil.checkRead(Employee_Status__c.SObjectType, EMPLOYEE_STATUS_READ_FIELD, false) && SecurityUtil.checkRead(Employee__c.SObjectType, EMPLOYEE_READ_FIELD, false)){
            dbquery =  Database.getQueryLocator(query);
            system.debug('dbquery' +dbquery);
        }
         
        return dbquery;
    }
     
    global void execute(Database.BatchableContext BC,List<Employee__c> scope){
        list<Employee__c> lstEmployee = new list<Employee__c>();
        for(Employee__c Emp: scope){
            system.debug('Emp are '+Emp.id);   
            Employee__c objEmp = new Employee__c();   
            objEmp.id = Emp.id; 
            if(!ReadOnlyApplication__c.getInstance(UserInfo.getProfileId()).Update_HR_Status__c){
                if(Emp.Employee_Status__r!=null && Emp.Employee_Status__r.size() > 0){
                    for(Employee_Status__c EmpStatus:Emp.Employee_Status__r ){
                        if(EmpStatus.Employee_Status__c == SalesIQGlobalConstants.ASSIGNED){
                            objEmp.HR_Status__c = SalesIQGlobalConstants.EMPLOYEE_STATUS_ACTIVE;
                        }
                        //objEmp.HR_Status__c = EmpStatus.Employee_Status__c;
                    }
                }
                else{
                    objEmp.HR_Status__c = SalesIQGlobalConstants.EMPLOYEE_STATUS_INACTIVE;
                }
            }
            
            
            //Update the Employee Field_Status__c based on Pos_EMP Record 
            if(!ReadOnlyApplication__c.getInstance(UserInfo.getProfileId()).Update_Field_Status__c){
                if(Emp.position_Employees__r!=null && Emp.position_Employees__r.size() > 0){
                    for(position_Employee__c EmpStatus:Emp.position_Employees__r ){                     
                        if(objEmp.Field_Status__c != SalesIQGlobalConstants.ASSIGNED)
                            objEmp.Field_Status__c = SalesIQGlobalConstants.ASSIGNED;
                        if(objEmp.Current_Territory__c != EmpStatus.Position__c)
                            objEmp.Current_Territory__c = EmpStatus.Position__c;
                    }
                }
                else{               
                    objEmp.Field_Status__c = SalesIQGlobalConstants.UNASSIGNED;
                    objEmp.Current_Territory__c = null;
                } 
            }
            
            lstEmployee.add(objEmp);
        }       
        if(lstEmployee.size() > 0){
            update lstEmployee;
        }
        
    }
     
    global void finish(Database.BatchableContext BC){
        system.debug('#### SyncWithESRIBatch finished');
    }

}