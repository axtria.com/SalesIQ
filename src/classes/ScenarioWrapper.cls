public class ScenarioWrapper{
        @AuraEnabled
        public string scenarioName{get;set;}
        @AuraEnabled
        public string teamName{get;set;}
        @AuraEnabled
        public string teamInstance{get;set;}
        @AuraEnabled
        public date startDate{get;set;}
        @AuraEnabled
        public date endDate{get;set;}
        
        public ScenarioWrapper(date startDate, date endDate){
            this.scenarioName = '';
            this.teamName = '';
            this.teamInstance = '';
            this.startDate = startDate; 
            this.endDate = endDate;
        }
    }