/**
* Description 	: Handler class for PositionAccountTrigger
* Author 		: Raghvendra.Rathore
* Since 		: 29-Feb-2016
* Version 		: 1.0
*/

public with sharing class PositionAccountTriggerHandler {
	
	/*
    * This method rollup extents and centroids on Parent Territory when a position Account of Account based team is updated
    * @param set of position account. Trigger.new
    * @param map of position account. Trigger.oldMap
    * @return void
  	*/   
	public static void rollupExtentsOnTerritory(list<Position_Account__c> newRecList, map<id,Position_Account__c> oldRecMap){	
        set<Id> parentIds = new set<Id>();
        //Iterate over the Position Geography and fetch all Parent Positions (Level 1)
        Boolean isSamePosition = true;
        for(Position_Account__c s : newRecList) {
            parentIds.add(s.Position_Team_Instance__c);
            if(oldRecMap.containsKey(s.Id)){
                //SIMPS-378
                if(oldRecMap.get(s.Id).Position__c != s.Position__c){
            		isSamePosition = false;
	                parentIds.add(oldRecMap.get(s.Id).Position_Team_Instance__c);
	            }
            }
            
        }
		
		if(parentIds.size() > 0 && !isSamePosition){
			
			map<Id,Position_Team_Instance__c> positionTeamInstanceMap = null;
			list<string> POSITION_TEAM_INSTANCE_READ_FIELD = new list<string>{'Position_ID__c', 'Parent_Position_ID__c','Team_Instance_ID__c'};
			list<string> TEAM_INSTANCE_READ_FIELD = new list<string>{'Alignment_Type__c'};
			
			if(SecurityUtil.checkRead(Team_Instance__c.SObjectType, TEAM_INSTANCE_READ_FIELD, false) && SecurityUtil.checkRead(Position_Team_Instance__c.SObjectType, POSITION_TEAM_INSTANCE_READ_FIELD, false)){
				positionTeamInstanceMap = new map<Id,Position_Team_Instance__c>([SELECT Id, Position_ID__c, Parent_Position_ID__c FROM Position_Team_Instance__c WHERE Id IN : parentIds 
	                                                                             AND (Team_Instance_ID__r.Alignment_Type__c =: SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT OR Team_Instance_ID__r.Geography_Type_Name__r.Shape_Available__c=false )]);
	        	
	        	list<string> POSITION_ACCOUNT_READ_FIELD = new list<string>{'Position_Team_Instance__c'};
	        	list<string> ACCOUNT_READ_FIELD  =new list<string>{'BillingLongitude','BillingLatitude'};
		        if(SecurityUtil.checkRead(ACCOUNT.SObjectType, ACCOUNT_READ_FIELD, false) && SecurityUtil.checkRead(Position_Account__c.SObjectType, POSITION_ACCOUNT_READ_FIELD, false) && SecurityUtil.checkRead(Team_Instance__c.SObjectType, TEAM_INSTANCE_READ_FIELD, false)){
		        	//Fetch aggregated extents at Level 1 position level.
		        	list<AggregateResult> aggregateResults = [SELECT Position_Team_Instance__c,  
			                                                  min(Account__r.BillingLongitude) minLong, 
			                                                  max(Account__r.BillingLongitude) maxLong, 
			                                                  min(Account__r.BillingLatitude) minLat, 
			                                                  max(Account__r.BillingLatitude) maxLat 
			                                                  FROM Position_Account__c 
			                                                  WHERE Position_Team_Instance__c IN : parentIds 
			                                                  AND (Team_Instance__r.Alignment_Type__c =: SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT  OR (Team_Instance__r.Geography_Type_Name__r.Account_Shape_Available__c=true and Team_Instance__r.Geography_Type_Name__r.Shape_Available__c=false))
			                                                  GROUP BY Position_Team_Instance__c];
		        
			        list<Position_Team_Instance__c> PositionToRollup = new list<Position_Team_Instance__c>();
			        for(AggregateResult ar : aggregateResults){
			            if(positionTeamInstanceMap.containsKey((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Position_Team_Instance__c'))){
			                Position_Team_Instance__c positionToUpdate = positionTeamInstanceMap.get((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Position_Team_Instance__c'));
			                			                
			                positionToUpdate.X_Max__c = (decimal)ar.get('maxLong');
				          	positionToUpdate.X_Min__c = (decimal)ar.get('minLong');
				            positionToUpdate.Y_Max__c = (decimal)ar.get('maxLat');
				            positionToUpdate.Y_Min__c = (decimal)ar.get('minLat');
				            
				            if((decimal)ar.get('maxLong') != null){
				                positionToUpdate.Centroid_Lattitude__c = (positionToUpdate.X_Max__c + positionToUpdate.X_Min__c)/2;
				                positionToUpdate.Centroid_Longitude__c = (positionToUpdate.Y_Max__c + positionToUpdate.Y_Min__c)/2;
				            }
			                PositionToRollup.add(positionToUpdate);
			            }
			        }
			        
		            if(PositionToRollup.size() > 0){
		            	list<string> POSITION_TEAM_INSTANCE_UPDATE_FIELD = new list<string>{'X_Max__c','X_Min__c','Y_Max__c','Y_Min__c','Centroid_Longitude__c','Centroid_Lattitude__c'};
			            if(SecurityUtil.checkUpdate(Position_Team_Instance__c.SObjectType, POSITION_TEAM_INSTANCE_UPDATE_FIELD, false)){
			            	update PositionToRollup;
			            }
			            				            
			            
			            //Calling method to rollup extents on parent positions (District level)
			            //list<Position_Team_Instance__c> districts = PositionAccountTriggerHandler.rollupExtentsOnParents(PositionToRollup);
			        }
		        }
		    }
				        
		}
    }
    

    public static void rollupExtentsOnTerritoryFromTalend(list<Position_Account__c> newRecList, map<id,Position_Account__c> oldRecMap){	
        set<Id> parentIds = new set<Id>();
        //Iterate over the Position Geography and fetch all Parent Positions (Level 1)
        Boolean isSamePosition = true;
        for(Position_Account__c s : newRecList) {
        	system.debug('in loop' );
            parentIds.add(s.Position_Team_Instance__c);
            if(oldRecMap.containsKey(s.Id)){
            	            	
	            parentIds.add(oldRecMap.get(s.Id).Position_Team_Instance__c);
	            
            }
        }
		
		if(parentIds.size() > 0 ){
			
			map<Id,Position_Team_Instance__c> positionTeamInstanceMap = null;
			list<string> POSITION_TEAM_INSTANCE_READ_FIELD = new list<string>{'Position_ID__c', 'Parent_Position_ID__c','Team_Instance_ID__c'};
			list<string> TEAM_INSTANCE_READ_FIELD = new list<string>{'Alignment_Type__c'};
			
			if(SecurityUtil.checkRead(Team_Instance__c.SObjectType, TEAM_INSTANCE_READ_FIELD, false) && SecurityUtil.checkRead(Position_Team_Instance__c.SObjectType, POSITION_TEAM_INSTANCE_READ_FIELD, false)){
				positionTeamInstanceMap = new map<Id,Position_Team_Instance__c>([SELECT Id, Position_ID__c, Parent_Position_ID__c FROM Position_Team_Instance__c WHERE Id IN : parentIds 
	                                                                             AND (Team_Instance_ID__r.Alignment_Type__c =: SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT OR Team_Instance_ID__r.Geography_Type_Name__r.Shape_Available__c=false )]);
	        	
	        	list<string> POSITION_ACCOUNT_READ_FIELD = new list<string>{'Position_Team_Instance__c'};
	        	list<string> ACCOUNT_READ_FIELD  =new list<string>{'BillingLongitude','BillingLatitude'};
		        if(SecurityUtil.checkRead(ACCOUNT.SObjectType, ACCOUNT_READ_FIELD, false) && SecurityUtil.checkRead(Position_Account__c.SObjectType, POSITION_ACCOUNT_READ_FIELD, false) && SecurityUtil.checkRead(Team_Instance__c.SObjectType, TEAM_INSTANCE_READ_FIELD, false)){
		        	//Fetch aggregated extents at Level 1 position level.
		        	list<AggregateResult> aggregateResults = [SELECT Position_Team_Instance__c,  
			                                                  min(Account__r.BillingLongitude) minLong, 
			                                                  max(Account__r.BillingLongitude) maxLong, 
			                                                  min(Account__r.BillingLatitude) minLat, 
			                                                  max(Account__r.BillingLatitude) maxLat 
			                                                  FROM Position_Account__c 
			                                                  WHERE Position_Team_Instance__c IN : parentIds 
			                                                  AND (Team_Instance__r.Alignment_Type__c =: SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT  OR (Team_Instance__r.Geography_Type_Name__r.Account_Shape_Available__c=true and Team_Instance__r.Geography_Type_Name__r.Shape_Available__c=false))
			                                                  GROUP BY Position_Team_Instance__c];
		        
			        list<Position_Team_Instance__c> PositionToRollup = new list<Position_Team_Instance__c>();
			        for(AggregateResult ar : aggregateResults){
			            if(positionTeamInstanceMap.containsKey((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Position_Team_Instance__c'))){
			                Position_Team_Instance__c positionToUpdate = positionTeamInstanceMap.get((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Position_Team_Instance__c'));
			                			                
			                positionToUpdate.X_Max__c = (decimal)ar.get('maxLong');
				          	positionToUpdate.X_Min__c = (decimal)ar.get('minLong');
				            positionToUpdate.Y_Max__c = (decimal)ar.get('maxLat');
				            positionToUpdate.Y_Min__c = (decimal)ar.get('minLat');
				            
				            if((decimal)ar.get('maxLong') != null){
				                positionToUpdate.Centroid_Lattitude__c = (positionToUpdate.X_Max__c + positionToUpdate.X_Min__c)/2;
				                positionToUpdate.Centroid_Longitude__c = (positionToUpdate.Y_Max__c + positionToUpdate.Y_Min__c)/2;
				            }
			                PositionToRollup.add(positionToUpdate);
			            }
			        }
			        
		            if(PositionToRollup.size() > 0){
		            	list<string> POSITION_TEAM_INSTANCE_UPDATE_FIELD = new list<string>{'X_Max__c','X_Min__c','Y_Max__c','Y_Min__c','Centroid_Longitude__c','Centroid_Lattitude__c'};
			            if(SecurityUtil.checkUpdate(Position_Team_Instance__c.SObjectType, POSITION_TEAM_INSTANCE_UPDATE_FIELD, false)){
			            	update PositionToRollup;
			            }
			            				            
			            
			            //Calling method to rollup extents on parent positions (District level)
			            list<Position_Team_Instance__c> districts = PositionAccountTriggerHandler.rollupExtentsOnParents(PositionToRollup);
			        }
		        }
		    }
				        
		}
    }




    /*
    * This method rollup extents and centroids on Parent Position
    * @param  list of Position_Team_Instance__c of child to be aggregated.
    * @return list of Position_Team_Instance__c of parent updated
  	*/
  	//Commenting the code as it seems this method is not used now.
    public static list<Position_Team_Instance__c> rollupExtentsOnParents(list<Position_Team_Instance__c> newRecList){
        
        set<Id> parentIds = new set<Id>();
        
        for(Position_Team_Instance__c s : newRecList) {
            parentIds.add(s.Parent_Position_ID__c);
        }
        
        map<Id,Position_Team_Instance__c> positionMap = new map<Id,Position_Team_Instance__c>();
        list<string> POSITION_TEAM_INSTANCE_READ_FIELD  =new list<string>{'Position_ID__c', 'Parent_Position_ID__c','Team_Instance_ID__c'};
        list<string> TEAM_INSTANCE_READ_FIELD = new list<string>{'Alignment_Type__c'};
        
        if(SecurityUtil.checkRead(Team_Instance__c.SObjectType, TEAM_INSTANCE_READ_FIELD, false) && SecurityUtil.checkRead(Position_Team_Instance__c.SObjectType, POSITION_TEAM_INSTANCE_READ_FIELD, false)){
        	for(Position_Team_Instance__c pti : [SELECT Id, Position_ID__c, Parent_Position_ID__c FROM Position_Team_Instance__c WHERE Position_ID__c IN : parentIds AND Team_Instance_ID__r.Alignment_Type__c = : SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT]){
	        	positionMap.put(pti.Position_ID__c,pti);
	        }
        }
        
        
        
        list<string> AGG_POSITION_TEAM_INSTANCE_READ_FIELD  =new list<string>{'Team_Instance_ID__c','Parent_Position_ID__c','X_Max__c','X_Min__c','Y_Max__c','Y_Min__c'};
        if(SecurityUtil.checkRead(Position_Team_Instance__c.SObjectType, AGG_POSITION_TEAM_INSTANCE_READ_FIELD, false) && SecurityUtil.checkRead(Team_Instance__c.SObjectType, TEAM_INSTANCE_READ_FIELD, false)){
        	list<AggregateResult> aggregateResults = [SELECT Parent_Position_ID__c, max(X_Max__c) xmax, min(X_Min__c) xmin, max(Y_Max__c) ymax, min(Y_Min__c) ymin 
                                                  FROM Position_Team_Instance__c 
                                                  WHERE Parent_Position_ID__c IN : parentIds 
                                                  AND Team_Instance_ID__r.Alignment_Type__c = :SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT
                                                  GROUP BY Parent_Position_ID__c];       
        
        
	        for(AggregateResult ar : aggregateResults){
	            if(positionMap.containsKey((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Parent_Position_ID__c'))){
	            	
	                positionMap.get((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Parent_Position_ID__c')).X_Max__c = (decimal)ar.get('xmax');
	                positionMap.get((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Parent_Position_ID__c')).X_Min__c = (decimal)ar.get('xmin');
	                positionMap.get((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Parent_Position_ID__c')).Y_Max__c = (decimal)ar.get('ymax');
	                positionMap.get((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Parent_Position_ID__c')).Y_Min__c = (decimal)ar.get('ymin');
	                
	                if((decimal)ar.get('xmax') != null){
		                positionMap.get((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Parent_Position_ID__c')).Centroid_Lattitude__c = ((decimal)ar.get('xmax') + (decimal)ar.get('xmin'))/2;
		                positionMap.get((string)ar.get(SalesIQGlobalConstants.NAME_SPACE+'Parent_Position_ID__c')).Centroid_Longitude__c = ((decimal)ar.get('ymax') + (decimal)ar.get('ymin'))/2;
	                }
	            }
	        }
	        
	        list<string> POSITION_TEAM_INSTANCE_UPDATE_FIELD = new list<string>{'X_Max__c','X_Min__c','Y_Max__c','Y_Min__c','Centroid_Lattitude__c','Centroid_Longitude__c'};
	        if(SecurityUtil.checkUpdate(Position_Team_Instance__c.SObjectType, POSITION_TEAM_INSTANCE_UPDATE_FIELD, false)){
	        	update positionMap.values();
	        }
        }
        
        //Calling method to rollup extents on parent positions above Level 2 (Iteratively)
	    list<Position_Team_Instance__c> parentPositions;
	    if(positionMap.size() > 0){
	    	parentPositions = PositionAccountTriggerHandler.rollupExtentsOnParents(positionMap.values());
	    }else{
	    	parentPositions = positionMap.values();
	    }
	    return parentPositions;
    }
}
