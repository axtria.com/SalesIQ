/**
 * SObjectDescribe is utility class supporting SecurityUtil to get the describe information
 * We are tried to use internal caching to minimize describe calls
**/
public class SObjectDescribe {
		
	public static String namespacePrefix {
		get {
			if(namespacePrefix == null)
				namespacePrefix = SObjectType.User_Access_Permission__c.getName().replace(SObjectType.User_Access_Permission__c.getLocalName(), '') ;
			return namespacePrefix ;
		}
		set ;
	}

	Map<String, Map<String, SObjectField>> fieldCache {
		get { return fieldCache != null ? fieldCache : (fieldCache = new Map<String, Map<String, SObjectField>>()); }
		set;
	}

	public DescribeFieldResult describeField(SObjectType objectToken, String fieldName, Boolean notImplyNamespace ) {
		final String objectKey = getObjectKey(objectToken);
		if (!fieldCache.containsKey(objectKey)) {

			/*
				The map has the following characteristics:

				It is dynamic, that is, it is generated at runtime on the fields for that sObject.
				All field names are case insensitive.
				The keys use namespaces as required.
				The keys reflect whether the field is a custom object.
			*/

			fieldCache.put(objectKey, objectToken.getDescribe().fields.getMap());
		}
		if (fieldName.length() == 0) return null;

		
		String fieldKey = notImplyNamespace ? fieldName.toLowerCase() : addNSPrefix(fieldName.toLowerCase()) ;
		// remove relationship fields
		fieldKey = fieldKey.indexOf('.') != -1 ? fieldKey.substring(0, fieldKey.indexOf('.')) : fieldKey ;
		Schema.SObjectField sObjField = getField(fieldKey, objectKey) ;
		return sObjField != null ? sObjField.getDescribe() : null ;
	}

	public Schema.SObjectField getField(String fieldName, String objectKey){

		final Map<String, SObjectField> fieldMap = fieldCache.get(objectKey);
		Schema.SObjectField result = fieldMap.get( fieldName.endsWithIgnoreCase('__r') ? //resolve custom field cross-object (__r) syntax
			(fieldName.removeEndIgnoreCase('__r')+'__c') :
			fieldName
		); 
		if(result == null){
			result = fieldMap.get(fieldName+'Id'); //in case it's a standard lookup in cross-object format
		}
		return result;
	}

	String getObjectKey(SObjectType objectToken) {
		return objectToken.getDescribe().getName().toLowerCase();
	}

	public static String addNSPrefix(String strWithoutNSPrefix){
        String fieldNamesWithNamespace ;
        if((strWithoutNSPrefix.contains('__c') || strWithoutNSPrefix.contains('__r')) && !(strWithoutNSPrefix.startsWithIgnoreCase(namespacePrefix)))
            fieldNamesWithNamespace = namespacePrefix+strWithoutNSPrefix;
        else
            fieldNamesWithNamespace = strWithoutNSPrefix;

        return fieldNamesWithNamespace;
    }

	public static list<String> removeNSPrefix(list<String> strListWithNSPrefix){
        list<string> cleanedValues = new list<string>();
        
        if(strListWithNSPrefix != null && !strListWithNSPrefix.isEmpty()){
            for(String s : strListWithNSPrefix){
                cleanedValues.add(removeNSPrefix(s));
            }
        }
        return cleanedValues;
   }
   
   public static String removeNSPrefix(String strWithNSPrefix){
        return strWithNSPrefix != null ? strWithNSPrefix.trim().toLowerCase().replace(namespacePrefix, '') : strWithNSPrefix ;
   }

}