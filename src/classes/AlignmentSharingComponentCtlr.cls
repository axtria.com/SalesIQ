/******************************************************************************************************************************************************
 * Name         :   AlignmentSharingComponentCtlr.cls
 * Description  :   Controller for AlignmentSharingComponent. Contains remote methods for add and remove sharing on ZIPs and Accounts
 * Author       :   Raghvendra Rathore
 * Created On   :   19/02/2019
******************************************************************************************************************************************************/
public class AlignmentSharingComponentCtlr{
    /*
    * Method to fetch positions to be shared with and reason code for selected action
    * Param 1: Selected team instance id
    * Param 2: Root level position accessed to logged in user
    * Param 3: Selected position sf id
    * Param 4: List of possition already shared
    * Param 5: Sharing type (ZIP/Account)
    * Param 6: Action (Add/Remove)
    * Return : map<string,list<SelectOption>> [key Positions/Reason Code, Value list of select options]
    */
    @RemoteAction
    public static map<string,list<SelectOption>> getPositionsToBeShared(string teamInstanceId, string rootPosition, string selectedPosition, list<string> sharedPositions, string sharingType, string action){
    	system.debug('####  getPositionsToBeShared called for '+action+' sharing for '+sharingType);
    	map<string,list<SelectOption>> sharingDataMap = new map<string,list<SelectOption>>();
    	
    	if(action == SalesIQGlobalConstants.SHARING_OPERATION_ADD){
            /*Generate data for Positions drop down*/
            list<Id> exceptPosition = new list<Id>();
            /*Fetching positions on basis of Position Name (the positions that are currently in sharedWith column of selected ZIPs/Accounts)*/
            if(sharedPositions.size() > 0){
                list<Position__c> sharedPositionList = SalesIQUtility.getPositionByPositionName(sharedPositions);  
                for(Position__c pos : sharedPositionList){
                   exceptPosition.add(pos.ID);
                }
            }
            
            /*Selected position*/
            exceptPosition.add(selectedPosition);
            
            list<SelectOption> positionDropDown = new list<SelectOption>();
            positionDropDown.add(new SelectOption(System.Label.None,System.Label.None));
            
            /*Fetch level 5 positions in case of hierarchy level 6 and above*/
            List<String> rootPositionIds = SalesIQUtility.getRootPositionIds(rootPosition);
            positionDropDown.addAll(SalesIQUtility.getPositionsByRootPositionID(rootPositionIds,exceptPosition,teamInstanceId,false));
            sharingDataMap.put('positions',positionDropDown);
        }

        /*Generate data for Reason code drop down*/
        list<SelectOption> reasonCodeList = new list<SelectOption>();
        reasonCodeList.add(new SelectOption(System.Label.Select_Reason_Code_Label , System.Label.Select_Reason_Code_Label));
        if(sharingType == 'ZIP'){
            reasonCodeList.addAll(AlignmentUtility.getReasonCodeList(teamInstanceId,SalesIQGlobalConstants.CONFIRM_MOVEMENT_ZIP));
        }else{
            reasonCodeList.addAll(AlignmentUtility.getReasonCodeList(teamInstanceId,SalesIQGlobalConstants.CONFIRM_MOVEMENT_ACCOUNT));
        }
    	
    	sharingDataMap.put('reasonCode',reasonCodeList);
    	return sharingDataMap;
    }

    /*
    * Method to check for external validations defined outside the package for add or remove sharing
    * Param 1: Selected team instance object
    * Param 2: Selected records (Account.AccountNumber or Geography__c.Name)
    * Param 3: Selected position 
    * Param 4: Position to add/remove sharing
    * Param 5: Sharing type (ZIP/Account)
    * Param 6: Action (Add/Remove)
    * Return : 
    */
    @RemoteAction
    public static map<string,string> sharingExternalValidation(Team_Instance__c teamInstanceObj, list<string> selectedData, string selectedPosition, string destinationPosition, string sharingType, string action){
        system.debug('#### selectedPosition : ' + selectedPosition);
        system.debug('#### destinationPosition : ' + destinationPosition);
        Boolean externalSharingError = false;
        string stringballoonErrorMessage = '';
        string validationErrorType = '';
        string balloonErrorMessage = '';
        list<Package_Extension__mdt> pkgExtList = SalesIQUtility.getPackageExtensionsByLabels(new set<string>{'Alignment Validation'});
        system.debug('######## pkgExtList ' + pkgExtList);
        string className = '';
        string operation = '';
        operation += action + ' ' + sharingType;
        set<string> setSelectedRecords = new set<string>(selectedData);
        if(pkgExtList != null && pkgExtList.size() > 0){
            className = pkgExtList[0].ClassName__c;
        }
        if(className == ''){
    
        }else{
            IAlignmentValidations v = SalesIQUtility.getAlignmentInterfaceInstance(className);  
            if(v != null){
                ValidationResponse valResp = new ValidationResponse();      
                if(sharingType == SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP_SHARING){                    
                     valResp = v.checkValidationOnSharing(teamInstanceObj.Id,selectedPosition,destinationPosition,setSelectedRecords,operation);
                }else if(sharingType == SalesIQGlobalConstants.MOVEMENT_TYPE_ACCOUNT_SHARING){
                     valResp = v.checkValidationOnSharing(teamInstanceObj.Id,selectedPosition,destinationPosition,setSelectedRecords,operation);
                }
                system.debug('######## ValidationResponse ' + valResp);
                if(valResp.responseType == 'Error'){
                    externalSharingError = true;
                    validationErrorType = 'Error';
                    for(string errorMsg : valResp.responseMessage){
                        balloonErrorMessage += '<li>'+errorMsg+'</li>';
                    }
                }else if(valResp.responseType == 'Warning'){
                    externalSharingError = true;
                    validationErrorType = 'Warning';
                    for(string errorMsg : valResp.responseMessage){
                        balloonErrorMessage += '<li>'+errorMsg+'</li>';
                    }
                }
            }                                                                
        }

        list<string> BUSINESS_RULES_FIELD = new list<string>{'name','Selected_Positions__c','Rule_Level__c','Rule_Type__c'};
        if(sharingType == SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP_SHARING && SecurityUtil.checkRead(Business_Rules__c.SObjectType, BUSINESS_RULES_FIELD, false)){
            list<Business_Rules__c> lstbusinessRules  = [SELECT Business_Rule_Type_Master__r.Name,Id,Rule_Level__c,Scenario__c,Selected_Positions__c FROM Business_Rules__c WHERE Business_Rule_Type_Master__r.Name = 'Assignment Rules' and Rule_Level__c = 'Position' and Scenario__c =: teamInstanceObj.Scenario__c];
            list<Position_Team_Instance__c> selDestination = AlignmentUtility.getPositionTeamInstanceByTeamName(destinationPosition, teamInstanceObj.Id , true);
            if(!selDestination.isEmpty() && AlignmentUtility.checkPositionRulesMisMatching(selectedPosition,selDestination[0].position_id__r.Id,teamInstanceObj.Id)){
                externalSharingError = true;
                validationErrorType = 'Warning';
                balloonErrorMessage += '<li>'+System.Label.Postion_Rules_ZIP_Sharing+'</li>';
            }
        }
        return new map<string,string>{validationErrorType=>balloonErrorMessage};
    }

    /*
    * Method to add or remove sharing on selected records (Account/ZIP)
    * Param 1: Selected team instance object
    * Param 2: Selected records (Account.AccountNumber or Geography__c.Name)
    * Param 3: Selected position to add/remove sharing
    * Param 4: Source position
    * Param 5: Sharing type (ZIP/Account)
    * Param 6: Action (Add/Remove)
    * Param 7: Reason code
    * Return : map<string,string> [Success/Error as key and Message as value]
    */
    @RemoteAction
    public static map<string,string> confirmSharing(Team_Instance__c teamInstanceObj, list<string> selectedRecs, string position, string selectedPositionId, string selectedPositionName, string sharingType, string action, string reason){
    	system.debug('#### confirmSharing called to '+action+' for '+sharingType+'('+selectedRecs+') with '+position);

        try{
            string assignmentStatus = SalesIQGlobalConstants.POSITION_GEOGRAPHY_ACTIVE;
            system.debug('#### Alignment_Period__c : '+teamInstanceObj.Alignment_Period__c);
            if(teamInstanceObj.Alignment_Period__c == SalesIQGlobalConstants.FUTURE_TEAM_CYCLE_TYPE){
                assignmentStatus = SalesIQGlobalConstants.POSITION_GEOGRAPHY_FUTURE_ACTIVE;
            }

            list<Position_Geography__c> lstSharedPosGeoRecord;
            list<Position_Account__c> lsPosAccount;

            Change_Request__c req = intantiateChangeRequest(selectedRecs, sharingType, position, selectedPositionId, teamInstanceObj, reason, action);
            set<string> selectedDataSet = new set<string>(selectedRecs);

            if(sharingType == SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP_SHARING){
                if(action == SalesIQGlobalConstants.SHARING_OPERATION_ADD){
                    lstSharedPosGeoRecord = SalesIQUtility.getPosGeoByGeoIdsANDteamIns(selectedDataSet, teamInstanceObj.Id, new list<string>{assignmentStatus});
                    if(!lstSharedPosGeoRecord.isEmpty()){
                        lsPosAccount = SalesIQUtility.getPositionAccountBySelectedZips(new set<id>{teamInstanceObj.Id}, selectedDataSet, new list<string>{assignmentStatus}, new list<string>{SalesIQGlobalConstants.CR_ACCOUNT_IMPLICIT_STATUS}, position, action);
                    }
                }
                if(action == SalesIQGlobalConstants.SHARING_OPERATION_REMOVE){
                    lstSharedPosGeoRecord = SalesIQUtility.getPosGeoByGeoNameANDPos(selectedDataSet, teamInstanceObj.Id, new list<string>{assignmentStatus}, position); 
                    lsPosAccount = SalesIQUtility.getPositionAccountBySelectedZips(new set<id>{teamInstanceObj.Id}, selectedDataSet, new list<string>{assignmentStatus}, new list<string>{SalesIQGlobalConstants.CR_ACCOUNT_IMPLICIT_STATUS},position,action);
                }

                string reqID = AlignmentUtility.createSharingRequest(req, lstSharedPosGeoRecord, lsPosAccount, false, position, selectedPositionName, action, req.Position_Rules_Mismatch__c);
            }

            if(sharingType == SalesIQGlobalConstants.MOVEMENT_TYPE_ACCOUNT_SHARING){
                if(action == SalesIQGlobalConstants.SHARING_OPERATION_ADD){
                    lsPosAccount = SalesIQUtility.getPosAccByAccNumbers(selectedDataSet, teamInstanceObj.Id, new list<string>{assignmentStatus},'');
                }
                if(action == SalesIQGlobalConstants.SHARING_OPERATION_REMOVE){
                    lsPosAccount = SalesIQUtility.getPosAccByAccNumbers(selectedDataSet, teamInstanceObj.Id, new list<string>{assignmentStatus},position);
                }

                string reqID = AlignmentUtility.createSharingRequest(req, new list<Position_Geography__c>(),lsPosAccount, false, position, selectedPositionName, action, req.Position_Rules_Mismatch__c);
            }

            return new map<string,string>{'Success'=>System.Label.Changes_Successful};
        }catch(Exception e){
            return new map<string,string>{'Error'=>e.getMessage()};
        }
    }

    public static Change_Request__c intantiateChangeRequest(list<string> selectedRecs, string sharingType, string position, string selectedPosition, Team_Instance__c teamInstanceObj, string reason, string action){
        Change_Request__c req = new Change_Request__c();
        try{
            //Getting destination ID 
            list<Position_Team_Instance__c > selDestination = new list<Position_Team_Instance__c>();
            selDestination = AlignmentUtility.getPositionTeamInstanceByTeamName(position, teamInstanceObj.Id , false);
            system.debug('#### selDestination ' + selDestination);
            string destination_Id;
            if(selDestination != null && selDestination.size() > 0){ 
                destination_Id = selDestination[0].position_id__r.Id; 
            }

            req.Change_Effective_Date__c = Date.today();  
            if(req.Change_Effective_Date__c > Date.today()){
               req.Execution_Status__c = 'New';
            }
            req.Country__c = teamInstanceObj.Team__r.Country__c;
            req.Source_Position__c = selectedPosition;
            req.Destination_Position__c = destination_Id;
            set<id> allPos = new set<id>();
            allPos.add(destination_Id);
            allPos.add(selectedPosition);
            Map<id,id> MapPos = AlignmentUtility.getPositionTeamInstance(allPos, teamInstanceObj.Id);
            
            string selectedDataStr = '';
            for(string rec : selectedRecs){
                selectedDataStr += rec+',';
            }
            selectedDataStr = selectedDataStr.removeEnd(',');
            
            Map<string,string> MapZIP = AlignmentUtility.getZIPByType(selectedDataStr,teamInstanceObj.Team__r.Country__c);
            system.debug('#### MapZIP : '+MapZIP);
            req.Destination_Positions__c = MapPos.get(destination_Id);
            req.Source_Positions__c = MapPos.get(selectedPosition);
            req.Reason__c = reason;
            req.AllZips__c = MapZIP.get(SalesIQGlobalConstants.ZIP_TYPE_STANDARD);
            req.Point_Zips__c = MapZIP.get(SalesIQGlobalConstants.ZIP_TYPE_POINT);
            req.Team_Instance_ID__c = teamInstanceObj.Id;
            req.Status__c = SalesIQGlobalConstants.REQUEST_STATUS_APPROVED;
            req.Is_Auto_Approved__c = true;
            req.Sharing_Opreation__c = action;
            string movementType = '';
            if(teamInstanceObj.Alignment_Type__c == SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP){
                movementType = SalesIQUtility.getSharingMovementType(teamInstanceObj.Alignment_Type__c, teamInstanceObj.Restrict_ZIP_Share__c, false, sharingType);
            }else if(teamInstanceObj.Alignment_Type__c ==SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT){
                movementType = SalesIQUtility.getSharingMovementType(teamInstanceObj.Alignment_Type__c, false, teamInstanceObj.EnableAccountSharing__c, sharingType);
                req.Account_Moved_Id__c = selectedDataStr;
            }else if(teamInstanceObj.Alignment_Type__c == SalesIQGlobalConstants.ALIGNMENT_TYPE_HYBRID){
                movementType = SalesIQUtility.getSharingMovementType(teamInstanceObj.Alignment_Type__c, teamInstanceObj.Restrict_ZIP_Share__c, teamInstanceObj.EnableAccountSharing__c, sharingType);
            }

            system.debug('#### movementType : '+movementType);
            req.Request_Type_Change__c = movementType;
            if(teamInstanceObj.Alignment_Type__c == SalesIQGlobalConstants.ALIGNMENT_TYPE_HYBRID && sharingType == SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP_SHARING){
                req.Position_Rules_Mismatch__c = String.valueOf(AlignmentUtility.checkPositionRulesMisMatching(selectedPosition,destination_Id,teamInstanceObj.Id));
                if(req.Position_Rules_Mismatch__c == 'true'){
                    req.Rule_Execution_Status__c = SalesIQGlobalConstants.REQUEST_STATUS_PENDING;
                }
            }
        }
        catch(exception e){
            SalesIQLogger.logCatchedException(e, true, SalesIQLogger.ALIGNMENT_MODULE);
        }
        return req;
    }
}