/************************************************************************************************
Name        :   DownloadTableDataCtlr.cls
Description :   Controller for DownloadTableData vf page. 
************************************************************************************************/
public class DownloadTableDataCtlr{
	public list<SObject> downloadData {get;set;}
  public set<string> columnHeaders {get;set;}
  public map<string,Team_Instance_Object_Attribute__c> columns {get;set;}
  public string fileHeader{get;set;}
  public Integer colCount{get;set;}

  public DownloadTableDataCtlr(){
    string teamInstanceId = ApexPages.currentPage().getParameters().get('teamInstanceId');
    string selectedPositionId = ApexPages.currentPage().getParameters().get('selectedPositionId');
    fileHeader = ApexPages.currentPage().getParameters().get('listLabel');
    string selectedPositionLevel = ApexPages.currentPage().getParameters().get('selectedPositionLevel');
    string downloadSOQL = ApexPages.currentPage().getParameters().get('soql');
    string listType = ApexPages.currentPage().getParameters().get('listType');
    string isCIMavailable = ApexPages.currentPage().getParameters().get('isCIMavailable');
    
    colCount = 0;

    system.debug('teamInstanceId : '+teamInstanceId);
    system.debug('selectedPositionId : '+selectedPositionId);
    system.debug('fileHeader : '+fileHeader);
    system.debug('selectedPositionLevel : '+selectedPositionLevel);
    system.debug('listType : '+listType);

    string colConfigSQOL = 'Select Attribute_API_Name__c, Attribute_Display_Name__c, Data_Type__c From Team_Instance_Object_Attribute__c where isEnabled__c = True and ';
    colConfigSQOL += 'Team_Instance__c =: teamInstanceId ';

    if(selectedPositionLevel == '1' && listType == Label.ZIP){
   		colConfigSQOL +=  'and Interface_Name__c = \'Zip Movement\' ';
   	}else if(selectedPositionLevel == '1' && listType == Label.Account){
   		colConfigSQOL +=  'and Interface_Name__c = \'Account Movement\' ';
   	}else{
   		colConfigSQOL +=  'and Interface_Name__c = \'Position List View\' ';
   	}
    system.debug('### isCIMavailable'+isCIMavailable);
    if(isCIMavailable == 'false'){
      colConfigSQOL += 'and Data_Type__c != \'Number\' ';
    }

    colConfigSQOL += 'order by Display_Column_Order__c';

    system.debug('#### colConfigSQOL : '+colConfigSQOL);
    columns = new map<string,Team_Instance_Object_Attribute__c>();
    columnHeaders = new set<string>();
    for(Team_Instance_Object_Attribute__c col : database.query(colConfigSQOL)){
      columns.put(AlignmentUtility.getLabelString(col.Attribute_Display_Name__c),col);
      columnHeaders.add(AlignmentUtility.getLabelString(col.Attribute_Display_Name__c));
    }

    system.debug('#### Columns : '+columnHeaders);

    colCount = columns.size();

    system.debug('#### soql : '+downloadSOQL);
    downloadData = database.query(downloadSOQL);
  }
}