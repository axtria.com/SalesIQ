global Interface IPositions{
	ValidationResponse checkPositionsValidation(String operationType, Position__c oldPosition, Position__c newPosition, String teamType, String loadedTeamInstance, String baseTeamInstance,
     Date crEffectiveDate, List<OverlayBasePositionWrapper> baseOverlayWrapper);

	
}