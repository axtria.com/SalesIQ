public with Sharing class  CPGComputationDerivedFieldCtrl {
    
    /*
    Method Name :   getOrgNamespace
    Description :   Method to fetch org namespace if exist
    */   
    @AuraEnabled
    public static String getOrgNamespace()
    {
        return SObjectType.Scenario__c.getName().replace(SObjectType.Scenario__c.getLocalName(), '').replace('__', '');
    }


    @AuraEnabled
    public static List<BRFieldWrapper> GetSourceAndDerivedFields(string scenarioRuleInstanceDetailsId){
       List<BRFieldWrapper> lstWrapper;
        try{
             lstWrapper=CPGUtility.GetSourceAndDerivedFields(scenarioRuleInstanceDetailsId);
          } 
        catch(Exception ex){
             CPGUtility.InsertErrorLog(ex.getMessage(),ex.getStackTraceString());
        }
       return lstWrapper;
   }
    
   
    
    @AuraEnabled
    //This method Saves the Computation Type derived variable. It accepts 3 json strings which are constructed in the same 
    //lines as 
    //corresponding objects from client side and sent to apex method. The strings would be deserialized to appropriate objects 
    //and saved.
    public static CPGConstants.Response SaveComputationType(string strJsonBusinessRules,string strJsonComputationRuleDetail,string strJsonBusinessRuleFieldMapDetails){
        CPGConstants.Response objResponse=new CPGConstants.Response();
        Savepoint sp = Database.setSavepoint();                                                        
        try{
               Business_Rules__c objBusinessRule=(Business_Rules__c) JSON.deserialize(strJsonBusinessRules, Business_Rules__c.class);  
               List<Computation_Rule_Detail__c> lstComputationRuleDetail=(List<Computation_Rule_Detail__c>) JSON.deserialize(strJsonComputationRuleDetail, List<Computation_Rule_Detail__c>.class);                                                                              
               List<Business_Rule_FieldMap_Details__c> lstBusinessRuleFieldMapDetails=(List<Business_Rule_FieldMap_Details__c>) JSON.deserialize(strJsonBusinessRuleFieldMapDetails, List<Business_Rule_FieldMap_Details__c>.class);                                                                              
               System.debug('objBusinessRule'+objBusinessRule);   
               System.debug('lstComputationRuleDetail'+lstComputationRuleDetail);   
               System.debug('lstBusinessRuleFieldMapDetails'+lstBusinessRuleFieldMapDetails); 
              // objBusinessRule.Execution_Sequence__c= CPGUtility.GetExecutionSequence();
              // jsonBusinessRules.Business_Rules__c["Name"]
             
           boolean isExists= CPGUtility.ValidateBusinessRuleNames(objBusinessRule.ScenarioRuleInstanceDetails__c,objBusinessRule.Name,false);               
           system.debug('Duplicate Rule Name'+isExists);
            if(isExists){
                objResponse.status=CPGConstants.ERROR;
                 objResponse.msg='Business Rule Name already exists.';
                return objResponse;
            }
            
            boolean isNameExists= CPGUtility.ValidateFieldNames(objBusinessRule.ScenarioRuleInstanceDetails__c,lstComputationRuleDetail[0].Derived_field_name__c );               
           system.debug('Duplicate Field Name'+isNameExists);
            if(isNameExists){
                objResponse.status=CPGConstants.ERROR;
                 objResponse.msg=' Field Name already exists.';
                return objResponse;
            }  
            
            
               insert  objBusinessRule;
               //throw new DmlException('MyException'); 
               for(Computation_Rule_Detail__c compRuleDetail: lstComputationRuleDetail){
                     compRuleDetail.Business_Rules__c=  objBusinessRule.Id;
                     insert compRuleDetail;
                     List<Data_Set_Rule_Map__c> lstDataSetRuleMap=new List<Data_Set_Rule_Map__c>();
                    lstDataSetRuleMap=[select Id,dataset_id__c from Data_Set_Rule_Map__c 
                                       where scenario_rule_instance_id__c= :objBusinessRule.ScenarioRuleInstanceDetails__c
                                       and ds_type__c='I']; 
                      if(lstDataSetRuleMap!=null && lstDataSetRuleMap.size()>0){
                        string dataSetId=lstDataSetRuleMap[0].dataset_id__c;
                        Data_Set_Column_Detail__c dataSetColumnDetail=new Data_Set_Column_Detail__c();
                        dataSetColumnDetail.dataset_id__c=dataSetId;
                        dataSetColumnDetail.ds_col_name__c=compRuleDetail.Derived_field_name__c;
                        dataSetColumnDetail.tb_col_nm__c=compRuleDetail.Derived_field_name__c.replace(' ','_').toLowerCase();  
                        dataSetColumnDetail.datatype__c=compRuleDetail.Data_Type__c;
                        dataSetColumnDetail.Table_Data_Type__c = compRuleDetail.Data_Type__c.toLowerCase();
                        dataSetColumnDetail.value_type__c=compRuleDetail.Value_Type__c;
                        dataSetColumnDetail.variable_type__c=CPGConstants.DERIVED;
                        insert  dataSetColumnDetail; 
                    }
                }  
                for(Business_Rule_FieldMap_Details__c busRuleFieldMapDetails: lstBusinessRuleFieldMapDetails){
                     busRuleFieldMapDetails.Business_Rule__c=  objBusinessRule.Id;
                     insert busRuleFieldMapDetails;
                }  
                if(!CPGUtility.UpdateExecutionSequences(objBusinessRule.ScenarioRuleInstanceDetails__c)){
                    throw new DmlException(CPGConstants.CYCLICREFERENCEERROR); 
                }
               
           
                objResponse.status=CPGConstants.SUCCESS;
                objResponse.msg=CPGConstants.SAVEDMSG;
        }
        
        catch(Exception ex){
            System.debug('exception-->'+ex.getStackTraceString()+'~^'+ex.getMessage());
            Database.rollback(sp);
            CPGUtility.InsertErrorLog(ex.getMessage(),ex.getStackTraceString());
            objResponse.status=CPGConstants.ERROR;
            objResponse.msg=ex.getMessage();
        }
        return objResponse;                                    
    }
    
    @AuraEnabled
    public static List<Business_Rules__c> GetComputationType(string businessRuleTypeId){
        List<Business_Rules__c> lstBusinessRules=new List<Business_Rules__c>();
        lstBusinessRules=[select Id,Name,
                          (select Id,IO_Flag__c,Name,Param_Type__c from Business_Rule_FieldMap_Details__r),
                          (select Id,Display_expression__c,Derived_field_name__c,Data_Type__c,Value_Type__c from Computation_Rule_Details__r) 
                          from Business_Rules__c where id= :businessRuleTypeId];
        
        //string strBusinessRules= JSON.serialize(lstBusinessRules);
        System.debug('value=='+lstBusinessRules);
        return lstBusinessRules;
    }
    
    @AuraEnabled
    public static CPGConstants.Response UpdateComputationType(string strJsonBusinessRules,string strJsonComputationRuleDetail
                                             ,string strJsonBusinessRuleFieldMapDetails){
        System.debug('inside UpdateComputationType ');                                         
        CPGConstants.Response objResponse=new CPGConstants.Response();
        Savepoint sp = Database.setSavepoint();                                         
        try{
        	   
               Business_Rules__c objBusinessRule=(Business_Rules__c) JSON.deserialize(strJsonBusinessRules, Business_Rules__c.class);
               System.debug('Business Rule Object'+objBusinessRule); 
               String ruleName = objBusinessRule.Name;
               List<Computation_Rule_Detail__c> lstComputationRuleDetail=(List<Computation_Rule_Detail__c>) JSON.deserialize(strJsonComputationRuleDetail, List<Computation_Rule_Detail__c>.class);                                                                              
               List<Business_Rule_FieldMap_Details__c> lstBusinessRuleFieldMapDetails=(List<Business_Rule_FieldMap_Details__c>) JSON.deserialize(strJsonBusinessRuleFieldMapDetails, List<Business_Rule_FieldMap_Details__c>.class);                                                                              
              
            String businessRuleTypeID = objBusinessRule.Id;
            system.debug('===businessRuleTypeID===='+businessRuleTypeID);
             List<Business_Rules__c> lstBusinessRules=new List<Business_Rules__c>();
             lstBusinessRules = GetComputationType(businessRuleTypeID);
            system.debug('Get Computation type values');
             system.debug(lstBusinessRules[0].Computation_Rule_Details__r[0].Derived_field_name__c );
            String existingruleName = lstBusinessRules[0].Name;
            String existingfieldName = lstBusinessRules[0].Computation_Rule_Details__r[0].Derived_field_name__c; 
            boolean isExists= CPGUtility.ValidateBusinessRuleNamesUpdate(objBusinessRule.ScenarioRuleInstanceDetails__c,objBusinessRule.Name,existingruleName,false);               
            system.debug('Duplicate Rule Name'+isExists);
            if(isExists){
                objResponse.status=CPGConstants.ERROR;
                objResponse.msg='Business Rule Name already exists.';
                return objResponse;
            } 
            boolean isFieldExists= CPGUtility.ValidateFieldNamesUpdate(objBusinessRule.ScenarioRuleInstanceDetails__c,lstComputationRuleDetail[0].Derived_field_name__c ,existingfieldName );               
            system.debug('Duplicate Rule Name'+isFieldExists);
            if(isFieldExists){
                objResponse.status=CPGConstants.ERROR;
                objResponse.msg='Field Name already exists.';
                return objResponse;
            }  
            
            // delete[select id from Computation_Rule_Detail__c where Business_Rules__c= :objBusinessRule.Id] ;
               delete[select id from Business_Rule_FieldMap_Details__c where Business_Rule__c= :objBusinessRule.Id] ;                                               
               
            List<Data_Set_Rule_Map__c> lstDataSetRuleMapToDelete=new List<Data_Set_Rule_Map__c>();
            lstDataSetRuleMapToDelete=[select Id,dataset_id__c from Data_Set_Rule_Map__c 
                              where scenario_rule_instance_id__c= :objBusinessRule.ScenarioRuleInstanceDetails__c]; 
             if(lstDataSetRuleMapToDelete!=null && lstDataSetRuleMapToDelete.size()>0){
             System.debug('Business Rule Object ID'+objBusinessRule.Id); 
             String businessRuleID = objBusinessRule.Id;
             System.debug(businessRuleID);	
             string dataSetId=lstDataSetRuleMapToDelete[0].dataset_id__c;  
             System.debug('to be deleted'+dataSetId);
              
	          List<Computation_Rule_Detail__c> lstCompRuleDetailToDelete = [select Id,Derived_field_name__c from Computation_Rule_Detail__c where  business_rules__c = :businessRuleID];
	          System.debug('List before deletion'+lstCompRuleDetailToDelete);
	          if(lstCompRuleDetailToDelete!=null && lstCompRuleDetailToDelete.size()>0){
	             string columnName = lstCompRuleDetailToDelete[0].Derived_field_name__c;
	             System.debug('Field to be deleted'+columnName);
	             delete[select Id from Data_Set_Column_Detail__c  where dataset_id__c= :dataSetId and variable_type__c ='Derived'  and ds_col_name__c = :columnName]  ;
           }
             }  
               
             delete[select id from Computation_Rule_Detail__c where Business_Rules__c= :objBusinessRule.Id] ;  
             
               update  objBusinessRule;
              // CPGUtility.UpdateExecutionSequences();
               for(Computation_Rule_Detail__c compRuleDetail: lstComputationRuleDetail){
                     compRuleDetail.Business_Rules__c=objBusinessRule.Id;
                     insert compRuleDetail;
                   
                      //Updating Data set column detail
                      List<Data_Set_Rule_Map__c> lstDataSetRuleMap=new List<Data_Set_Rule_Map__c>();
                      lstDataSetRuleMap=[select Id,dataset_id__c from Data_Set_Rule_Map__c 
                                 where scenario_rule_instance_id__c= :objBusinessRule.ScenarioRuleInstanceDetails__c
                                  and  ds_type__c='I' ]; 
                                      
                      System.debug('Queried Ids inside update method'+lstDataSetRuleMap);                 
                      if(lstDataSetRuleMap!=null && lstDataSetRuleMap.size()>0){
                        string dataSetId=lstDataSetRuleMap[0].dataset_id__c;
                        delete[select Id from Data_Set_Column_Detail__c where dataset_id__c= :dataSetId 
                           and ds_col_name__c= :compRuleDetail.Derived_field_name__c ];  
                   /* Data_Set_Column_Detail__c dataSetColumnDetail=[select Id from Data_Set_Column_Detail__c 
                                                                       where dataset_id__c= :dataSetId limit 1];*/
                    //Data_Set_Column_Detail__c dataSetColumnDetail=new  Data_Set_Column_Detail__c();   
                        Data_Set_Column_Detail__c dataSetColumnDetail=new Data_Set_Column_Detail__c();
                        dataSetColumnDetail.dataset_id__c=dataSetId;
                        dataSetColumnDetail.ds_col_name__c=compRuleDetail.Derived_field_name__c;
                        dataSetColumnDetail.tb_col_nm__c=compRuleDetail.Derived_field_name__c.replace(' ','_').toLowerCase();  
                        dataSetColumnDetail.datatype__c=compRuleDetail.Data_Type__c;
                        dataSetColumnDetail.value_type__c=compRuleDetail.Value_Type__c;
                        dataSetColumnDetail.Table_Data_Type__c = compRuleDetail.Data_Type__c.toLowerCase();
                        dataSetColumnDetail.variable_type__c=CPGConstants.DERIVED;
                        insert  dataSetColumnDetail; 
                    }
                     
                }  
                for(Business_Rule_FieldMap_Details__c businessRuleFieldMapDetails: lstBusinessRuleFieldMapDetails){
                     businessRuleFieldMapDetails.Business_Rule__c=objBusinessRule.Id;
                     insert businessRuleFieldMapDetails;
                }  
                if(!CPGUtility.UpdateExecutionSequences(objBusinessRule.ScenarioRuleInstanceDetails__c)){
                    throw new DmlException(CPGConstants.CYCLICREFERENCEERROR); 
                }
                objResponse.status=CPGConstants.SUCCESS;
                objResponse.msg=CPGConstants.SAVEDMSG;
        } 
        catch(Exception ex){
                System.debug('exception-->'+ex.getStackTraceString()+'~^'+ex.getMessage());
                Database.rollback(sp);
                CPGUtility.InsertErrorLog(ex.getMessage(),ex.getStackTraceString());
               objResponse.status=CPGConstants.ERROR;
               objResponse.msg=ex.getMessage();
              // return throw AuraEnabledException(ex.getMessage());
        }
        return objResponse;                                         
                                          
    }
    
    
   
    
    @AuraEnabled
    public static CPGConstants.Response DeleteComputationType(string businessRuleId,string scenarioRuleInstanceDetailsId){
        CPGConstants.Response objResponse=new CPGConstants.Response();
       /* Savepoint sp = Database.setSavepoint();  
        try{
           if(!CPGUtility.ContainsReference(businessRuleId)){
           	//Delete in sequence
           	
           	String SRID =  [select ScenarioRuleInstanceDetails__c from Business_Rules__c where id =:businessRuleId][0].ScenarioRuleInstanceDetails__c ;
           	System.debug('ScenarioRuleInstanceDetails__c value'+SRID);
           
           	String SCID =[select Scenario_Id__c from Scenario_Rule_Instance_Details__c where id=: SRID][0].Scenario_Id__c;
            System.debug('Scenario_Id__c value'+SCID);
           	
           	List<Scenario_Rule_Instance_Details__c> SRIDList = new List<Scenario_Rule_Instance_Details__c>();
           	SRIDList = [select id from Scenario_Rule_Instance_Details__c where Scenario_Id__c=: SCID];
           	System.debug('List of ScenarioRuleInstacneDetailsID'+SRIDList);
           	
           	List<Business_Rules__c> bussnessRuleObjList = new List<Business_Rules__c>();
           	bussnessRuleObjList = [select id from Business_Rules__c where ScenarioRuleInstanceDetails__c IN:SRIDList ];
            System.debug('List of BRids'+bussnessRuleObjList);
           
           	List<Business_Rule_FieldMap_Details__c> BRFMList = new List<Business_Rule_FieldMap_Details__c>();
           	BRFMList = [select Business_Rule__c from Business_Rule_FieldMap_Details__c where Business_Rule__c IN: bussnessRuleObjList];
           	
       
             delete[select Id from Business_Rules__c where Id= :businessRuleId];
             delete[select Id from Computation_Rule_Detail__c where business_rules__c= :businessRuleId];
             delete[select Id from Business_Rule_FieldMap_Details__c where business_rule__c= :businessRuleId];
             List<Data_Set_Rule_Map__c> lstDataSetRuleMap=new List<Data_Set_Rule_Map__c>();
             lstDataSetRuleMap=[select Id,dataset_id__c from Data_Set_Rule_Map__c 
                                  where scenario_rule_instance_id__c= :scenarioRuleInstanceDetailsId]; 
             if(lstDataSetRuleMap!=null && lstDataSetRuleMap.size()>0){
                 string dataSetId=lstDataSetRuleMap[0].dataset_id__c;  
                 delete[select Id from Data_Set_Column_Detail__c  where dataset_id__c= :dataSetId limit 1]  ;
             }
             objResponse.status=CPGConstants.SUCCESS;
             objResponse.msg=CPGConstants.SAVEDMSG;  
           }    
        }
        catch(Exception ex){
            Database.rollback(sp);
            CPGUtility.InsertErrorLog(ex.getMessage(),ex.getStackTraceString());
            objResponse.status=CPGConstants.ERROR;
            objResponse.msg=CPGConstants.ERRORMSG;
               
        }*/
        return  objResponse;
        
    }
    
   
  
    /**
     * calling Rule_Types custom metatdata types for the Rule type dropdown
     *  
     *  @author  Aakash Gupta
     * @version 1.0
     * @since   2017-09-05  
    */
    
     @AuraEnabled
    public static  Rule_Types__mdt[] getRuleTypeDetails(){
     // return Rule_Types__mdt.getall().values();
        List<Rule_Types__mdt> RuleTypeNames = [Select Id,Label  from Rule_Types__mdt ];
        return  RuleTypeNames; 
    } 
    
     @AuraEnabled
    public static List_Of_Operators__mdt[]  getOperatorTypeValues(){
    	return CPGUtility.getOperatorTypeValues(); 
    }
    
   
    
     /**
     * calling List Of Operators custom metatdata types for the Operator dropdown
     *  
     *  @author  Aakash Gupta
     *  @version 1.0
     *  @since   2017-09-05  
    */
    @AuraEnabled
    public static  List_Of_Operators__mdt[] getOperators(string oprtype){
       List<List_Of_Operators__mdt> oprListData = CPGUtility.getOperatorTypeValues();
       List<List_Of_Operators__mdt> oprListDataResut=new List<List_Of_Operators__mdt>();
       List<String> dependentOperators = new List<String>();
       for(integer i=0;i<oprListData.size() ;i++){
             if(oprListData[i].Operator_Type__c==oprtype){
                oprListDataResut.add(oprListData[i]);
               // dependentOperators.add(oprListData[i].Name);
             }
        }
        return oprListDataResut;
    }
    
     /**
     * calling Types List custom metatdata types for the type lists dropdown
     *  
     *  @author  Aakash Gupta
     *  @version 1.0
     *  @since   2017-09-05  
    */
       @AuraEnabled
      public static  Types_List__mdt[] getTypesList(){ 
        List<Types_List__mdt> typesList = [Select Id,Label,	Type__c,Value__c from Types_List__mdt ];
        system.debug('typesList====>'+typesList);
          return  typesList; 
      } 
    
     @AuraEnabled
    public static List<Business_Rule_Type_Master__c> getBusinessRuleTypeMaster(string componentTypeMasterId){
         
        List<Business_Rule_Type_Master__c> lstBusinessRuleTypeMaster=[select Id,Name from  Business_Rule_Type_Master__c where Component_Type__r.Name='Derived Field'];
        return lstBusinessRuleTypeMaster;
       
    }
    
     /**
     * calling Table Name for computation screen display
     *  
     *  @author  Aakash Gupta
     *  @version 1.0
     *  @since   2017-09-05  
    */
    @AuraEnabled
    public static List<Data_Set_Rule_Map__c> getInputTableName(string scenarioRuleInstanceDetailsId){
     
    	List<Data_Set_Rule_Map__c> inputTable = [SELECT Table_Display_Name__c FROM Data_Set_Rule_Map__c WHERE scenario_rule_instance_id__c = :scenarioRuleInstanceDetailsId AND ds_type__c = 'I'  ];
    	//String inputTableName = inputTable[0].Input_Table__c;
    	system.debug('Output Value:' +inputTable);
    	return inputTable;
    }
    
    /* @AuraEnabled
    public static DerivedListWrapper getDerivedFieldListData(){
    	/*List<Business_Rules__c> businessRulesData = [Select Id from Business_Rules__c];
        List<Id> businessRules=new List<Id>();
        for(Business_Rules__c br: businessRulesData)
        {
            businessRules.add(br.id);
        }
        system.debug(businessRules);
    	List <Computation_Rule_Detail__c> ruleDetailData = [Select Business_Rules__r.Name,Business_Rules__r.Business_Rule_Type_Master__r.Name,Derived_field_name__c, Data_Type__c, Value_Type__c from Computation_Rule_Detail__c where Business_Rules__c in :businessRules];
    	system.debug('businessRulesData'+businessRulesData);
        system.debug('derived List'+ruleDetailData);
    	return ruleDetailData;
        
        List<Data_Set_Rule_Map__c> dataSetIDList = [select Id,dataset_id__c from Data_Set_Rule_Map__c where scenario_rule_instance_id__c='a1Mf4000000ohYEEAY' and ds_type__c='I'];
        List<Data_Set_Column_Detail__c> dataSetColumnDetails = [select ds_col_name__c,datatype__c,value_type__c from Data_Set_Column_Detail__c  where dataset_id__c='a1af4000000hEmsAAE' and variable_type__c='Derived'];
        List<Business_Rules__c> businessRuleDetails =[select Id,Name,Business_Rule_Type_Master__r.Name from Business_Rules__c where ScenarioRuleInstanceDetails__c='a1Mf4000000ohYEEAY' and Business_Rule_Type_Master__r.Component_Type__r.Id= 'a1Kf4000000jQD8EAM'];
        return(new DerivedListWrapper(businessRuleDetails[0].Name,dataSetColumnDetails[0].ds_col_name__c,businessRuleDetails[0].Business_Rule_Type_Master__r.Name,dataSetColumnDetails[0].datatype__c,dataSetColumnDetails[0].value_type__c));
    }
    
          public class DerivedListWrapper{
        @AuraEnabled
        public string RuleName{get;set;}
        @AuraEnabled
        public string FieldName{get;set;}
        @AuraEnabled
        public string Type1{get;set;}
        @AuraEnabled
        public string DataType{get;set;}
        @AuraEnabled
        public string ValueType{get;set;}
       
        public DerivedListWrapper(string RuleName, string FieldName, string Type1,string DataType,string ValueType){
            this.RuleName = RuleName;
            this.FieldName = FieldName;
            this.Type1 = Type1;
            this.DataType = DataType;
            this.ValueType = ValueType;
            
        }
      } */
      
      
    
}