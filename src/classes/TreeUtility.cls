/**********************************************************************************************
@author     :  Aditi Singh
@date       : 22.12.2015
@description: 
Revison(s)  : This is the utility class with methods required to display tree hierarchy on 
              various pages.
**********************************************************************************************/
public with sharing class TreeUtility {

    public static Map <Id,Position__c> allPositionMap; //map with key as position salesforce id and value as corresponding position
    public static Map <Id, List<Position__c>> parentChildPositionMap; //map with key as parent position salesforce id and value as list of its child positions
    
    public static set<Id> secondaryPositionIds;
    public static map<Id,Position__c> allSecondaryPositionMap;
    public static map<Id, List<Position__c>> secParentChildPositionMap;
        
    private static JSONGenerator gen {get; set;}// JSON generated for tree creation
    static wrapperCallingClassVariables wrapObj;// wrapper instance to return certain variables to alignment controller
    
    /*******************Method for getting all positions too be displayed on tree and formation of parent-child hierarchy********/
    public static wrapperCallingClassVariables init(String selectedTeam){
        
        list<string> USER_ACCESS_PERMISSION_READ_FIELD = new list<string>{'id','Position__c', 'Team_Instance__c', 'User__c', 'Is_Active__c'};
        list<String> POSITION_READ_FIELD = new list<String>{'id','Change_Status_del__c','name','parent_Position__c','Client_Position_Code__c','Client_Territory_Code__c','Client_Territory_Name__c','Position_Type__c','RGB__c','Team_iD__c','X_Max__c','Y_Max__c','X_Min__c','Y_Min__c','inactive__c'};
        list<String> TEAM_INSTANCE_READ_FIELD = new list<String>{'Team__c','Name','Alignment_Type__c','ReadOnlyCreatePosition__c','ReadOnlyEditPosition__c','ReadOnlyDeletePosition__c','IsAllowedManageAssignment__c', 'Restrict_Update_Position_Attribute__c'};
        
        try{
            if (SecurityUtil.checkRead(User_Access_Permission__c.SObjectType, USER_ACCESS_PERMISSION_READ_FIELD, false) && 
                SecurityUtil.checkRead(Position__c.SObjectType, POSITION_READ_FIELD, false) &&
                SecurityUtil.checkRead(Team_Instance__c.SObjectType, TEAM_INSTANCE_READ_FIELD, false)){
                
                User_Access_Permission__c access;
                
                //For this to work in Roster, It is a mandate to have current team instance record in UAP corresponding to a team.  
                list<User_Access_Permission__c> accessUserPos = [Select Id, Position__c, Position__r.X_Max__c, Position__r.X_Min__c, Position__r.Y_Max__c, 
                                                        Position__r.Y_Min__c, Position__r.Position_Type__c,Team_Instance__c, Team_Instance__r.Team__c, 
                                                        Team_Instance__r.Team__r.Name, Team_Instance__r.Name, Team_Instance__r.Alignment_Period__c,
                                                        Team_Instance__r.Alignment_Type__c ,Position__r.Client_Position_Code__c, Team_Instance__r.Team__r.Type__c,
                                                        Team_Instance__r.IC_EffstartDate__c, Team_Instance__r.Team__r.Effective_End_Date__c, Team_Instance__r.Team__r.Controlling_Team__c,
                                                        Team_Instance__r.ReadOnlyCreatePosition__c, Team_Instance__r.ReadOnlyEditPosition__c, Team_Instance__r.ReadOnlyDeletePosition__c,
                                                        Team_Instance__r.Restrict_Hierarchy_Change__c, Team_Instance__r.IsAllowedManageAssignment__c, Team_Instance__r.Restrict_Update_Position_Attribute__c
                                                        From User_Access_Permission__c where Is_Active__c = true AND User__c =: UserInfo.getUserId() 
                                                        and Team_Instance__r.Team__c =: selectedTeam and Team_Instance__r.Alignment_Period__c =: SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE
                                                        order by Team_Instance__r.Name limit 1]; 
               	if(accessUserPos.size() > 0){
               		access = accessUserPos[0];
               	}else{
               		return null;
               	}
                
               	wrapObj = new wrapperCallingClassVariables();
               	wrapObj.posType = access.Position__r.Position_Type__c;
               	wrapObj.teamType = access.Team_Instance__r.Team__r.Type__c;
               	wrapObj.selectedTeamName = access.Team_Instance__r.Team__r.Name;
               	wrapObj.selectedCycle = access.Team_Instance__c;
               	wrapObj.selectedteamID = access.Team_Instance__r.Team__c;
               	wrapObj.AlignmentType = access.Team_Instance__r.Alignment_Type__c;
               	wrapObj.rootPositionId = access.Position__c;
				wrapObj.rootPositionCode = access.Position__r.Client_Position_Code__c;
               	wrapObj.selectedCycleName = access.Team_Instance__r.Name; 
               	wrapObj.initialExtents = access.Position__r.X_Min__c+','+access.Position__r.Y_Min__c+','+access.Position__r.X_Max__c+','+access.Position__r.Y_Max__c;
               	wrapObj.controllingTeamId = access.Team_Instance__r.Team__r.Controlling_Team__c;
               	wrapObj.readOnlyCreatePosition = access.Team_Instance__r.ReadOnlyCreatePosition__c;
               	wrapObj.readOnlyEditPosition = access.Team_Instance__r.ReadOnlyEditPosition__c;
                wrapObj.readOnlyUpdateAttributes = access.Team_Instance__r.Restrict_Update_Position_Attribute__c;
               	wrapObj.readOnlyDeletePosition = access.Team_Instance__r.ReadOnlyDeletePosition__c;
               	wrapObj.readOnlyHierarchyChange = access.Team_Instance__r.Restrict_Hierarchy_Change__c;
               	wrapObj.readOnlyManageAssignment = access.Team_Instance__r.IsAllowedManageAssignment__c;
                
                if(wrapObj.AlignmentType == SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT){
                    wrapObj.selectedMovementType = SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT;
                }
                else{
                    wrapObj.selectedMovementType = SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP;
                }
                 
                allPositionMap = new map<Id,Position__c>();
                parentChildPositionMap = new Map <Id, List<Position__c>>();
                
                list<Position__c> allAccessiblePositions = new list<Position__c>();
                
                list<team_instance__c> listBaseCurrentTeamInstance;
                
                if(wrapObj.teamType == SalesIQGlobalConstants.TEAM_TYPE_OVERLAY){
                	string baseTeamId = wrapObj.controllingTeamId;
    				listBaseCurrentTeamInstance = [select id from team_instance__c where Alignment_Period__c = 'Current' and team__c =: baseTeamId];
    	        	allAccessiblePositions = SalesIqUtility.getPositionbyRootPositionIdRoster(wrapObj.rootPositionId, wrapObj.teamType, wrapObj.selectedCycle, listBaseCurrentTeamInstance[0].id);
                }else if(wrapObj.teamType == SalesIQGlobalConstants.TEAM_TYPE_BASE){
                	allAccessiblePositions = SalesIqUtility.getPositionbyRootPositionIdRoster(wrapObj.rootPositionId, wrapObj.teamType, wrapObj.selectedCycle, wrapObj.selectedCycle);
                }
                
                for(Position__c pos : allAccessiblePositions){
                    allPositionMap.put(pos.Id,pos);
                    list<Position__c> tempList = parentChildPositionMap.get(pos.Parent_Position__c);
               		
                    if(tempList == null){
                        tempList = new list<Position__c>();
                    }
                    tempList.add(pos);
                    parentChildPositionMap.put(pos.Parent_Position__c,tempList);
                }
                
                if(ReadOnlyApplication__c.getOrgDefaults().Secondary_Assignment_Roster__c){
                	secondaryPositionIds = AlignmentUtility.getSecondaryPositions(userinfo.getuserid(),access.Team_Instance__c);
                }
                
                system.debug('#### secondaryPositionIds : '+secondaryPositionIds);
                if(secondaryPositionIds != null && secondaryPositionIds.size() > 0){
                	list<Position__c> allAccessibleSecPositions;
                	if(wrapObj.teamType == SalesIQGlobalConstants.TEAM_TYPE_OVERLAY){
	    	        	allAccessibleSecPositions = SalesIqUtility.getPositionbyRootPositionIdRoster(secondaryPositionIds, wrapObj.teamType, wrapObj.selectedCycle, listBaseCurrentTeamInstance[0].id);
	                }else if(wrapObj.teamType == SalesIQGlobalConstants.TEAM_TYPE_BASE){
	                	allAccessibleSecPositions = SalesIqUtility.getPositionbyRootPositionIdRoster(secondaryPositionIds, wrapObj.teamType, wrapObj.selectedCycle, wrapObj.selectedCycle);
	                }
                	
                	secParentChildPositionMap = new Map <Id, List<Position__c>>();
                	allSecondaryPositionMap = new map<Id,Position__c>();
                	
					for(Position__c pos : allAccessibleSecPositions){
						system.debug('#### secondary pos : '+pos.Id);
						allSecondaryPositionMap.put(pos.Id,pos);
						list<Position__c> tempList = secParentChildPositionMap.get(pos.Parent_Position__c);
	                    if(tempList == null){
	                        tempList = new list<Position__c>();
	                    }
	                    tempList.add(pos);
	                    secParentChildPositionMap.put(pos.Parent_Position__c,tempList);
					}
					system.debug('#### allAccessibleSecPositions : '+allAccessibleSecPositions);
					system.debug('#### allSecondaryPositionMap : '+allSecondaryPositionMap);
                }
                
               //Get configured attributes for Geography
                return wrapObj;
            }
        }
        catch(Exception e){
            SalesIQLogger.logMessage(e);
    	}
        return null;
    }
    
    //Method returns JSON string for the tree.
    public static string getJsonString(){
        gen = JSON.createGenerator(true);
        if(allPositionMap.size() > 0){
        	GeographyUtil.GeographyNodeWrapper node = GeographyUtil.createNode(wrapObj.rootPositionId,allPositionMap,parentChildPositionMap);
	        gen.writeStartArray();
	        GeographyUtil.convertNodeToJSON(gen,node,true);
	        gen.writeEndArray();
        }
        string jsonString = gen.getAsString();
        if(allSecondaryPositionMap != null && allSecondaryPositionMap.size() > 0){
        	string primaryNodeJSON = '[{"title": "Primary Assignments","text": "Primary Assignments","key": "YYY","unselectable": true,"expand": true,"opened": true,"isFolder": true,';
			primaryNodeJSON += '"rgb": "0,0,0","TerritoryCode": "Terr0012","id": "Primary","state": {"opened": true},';//,"extents": "null,null,null,null","TeamName": "HTN",
			primaryNodeJSON += '"icon": "circle _Primary","Level": "0","children":';//,"DistrictCode": "","AreaCode": "","RegionCode": "","Type": "Territory",
        	
        	jsonString = primaryNodeJSON + jsonString+'}';
        	
	    	//Starting secondary node in JSON
	        jsonString += ', {"title": "Secondary Assignments","text": "Secondary Assignments","key": "XXX","unselectable": true,"expand": true,"opened": true,"isFolder": true,';
			jsonString += '"state": {"opened": true},"extents": "null,null,null,null","TeamName": "HTN","rgb": "0,0,0","TerritoryCode": "Terr0012",';
			jsonString += '"id": "Terr0012","icon": "circle _Secondary","DistrictCode": "","AreaCode": "","RegionCode": "","Type": "Territory","Level": "1","children": [';
			
			string secJSONString = '';
	    	for(Id secPosId : secondaryPositionIds){
	    		JSONGenerator secGen = JSON.createGenerator(true);
	        	GeographyUtil.GeographyNodeWrapper node = GeographyUtil.createNode(secPosId,allSecondaryPositionMap,secParentChildPositionMap);
	        	secGen.writeStartArray();
	            GeographyUtil.convertNodeToJSON(secGen,node,false);
	            secGen.writeEndArray();
	            secJSONString += secGen.getAsString().removeStart('[');
	            secJSONString = secJSONString.removeEnd(']');
	            secJSONString = secJSONString+',';
	        }
	        secJSONString = secJSONString.removeEnd(',');
	        //Adding secondary positions in json
			jsonString += secJSONString;
			//Ending secondary node in JSON
			jsonString += ']}]';
	    }
        system.debug('#### jsonString : '+jsonString);
        return jsonString; 
    }
    
    //Method returns RGBs for corresponding Positions to show colors on Tree
    public static map<string,string> getAllPositionsRGBs(String selectedTeam){
    	system.debug('selectedTeam getAllPositionsRGBs'+selectedTeam);
        init(selectedTeam);
        map<string,string> posRgbMap = new map<string,string>();
   
        //Fetch all Positiion Geographies related to Territories
        for(Position__c pos : allPositionMap.values()){
        	if(pos.Client_Position_Code__c != null){
	            if(pos.Client_Position_Code__c.contains('<')){
	                pos.Client_Position_Code__c = '1N000000';
	            }
        	}
            if(pos.RGB__c != null){
                 if(pos.RGB__c.contains('<')){
                    pos.RGB__c = '0,0,0';
                }
                posRgbMap.put(pos.Client_Position_Code__c,pos.RGB__c);
            }
        }
        
        if(allSecondaryPositionMap != null){
	        for(Position__c pos : allSecondaryPositionMap.values()){
	        	if(pos.Client_Position_Code__c != null){
		            if(pos.Client_Position_Code__c.contains('<')){
		                pos.Client_Position_Code__c = '1N000000';
		            }
	        	}
	            if(pos.RGB__c != null){
	                 if(pos.RGB__c.contains('<')){
	                    pos.RGB__c = '0,0,0';
	                }
	                posRgbMap.put(pos.Client_Position_Code__c,pos.RGB__c);
	            }
	        }
        }
        return posRgbMap; 
    }
    
    /***************wrapper to return variables to alignment controller***********************/
    public class wrapperCallingClassVariables{
        public String rootPositionId;
        public String rootPositionCode;
        public String selectedTeamName;
        public String selectedCycle;
        public String selectedCycleName;
        public String AlignmentType;
        public String posType;
        public String selectedteamID;
        public String selectedMovementType;
        public String initialExtents;
        public String teamType;
        public string controllingTeamId;
        public Boolean readOnlyCreatePosition;
        public Boolean readOnlyEditPosition;
        public Boolean readOnlyDeletePosition;
        public Boolean readOnlyHierarChyChange;
        public Boolean readOnlyManageAssignment;
        public boolean readOnlyUpdateAttributes;
    }
}