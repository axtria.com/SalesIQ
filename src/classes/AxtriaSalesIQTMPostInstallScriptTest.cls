/**
Name        :   AxtriaSalesIQTMPostInstallScriptTest
Author      :   Ayush Rastogi
Date        :   15/11/2019
Description :   Test class for AxtriaSalesIQTMPostInstallScript
*/
@isTest
private class AxtriaSalesIQTMPostInstallScriptTest {
    static testMethod void testPostInstallScript1() {
        test.startTest();
        //Create instance of InstallHandler
        AxtriaSalesIQTMPostInstallScript postinstall = new AxtriaSalesIQTMPostInstallScript();


        ETL_Config__c etlConfig = CallPlanTestDataFactory.createETLConfig();
        etlConfig.Name = 'BRMS';

        ETL_Config__c etlConfig2 = CallPlanTestDataFactory.createETLConfig();
        etlConfig2.Name = 'Talend';

        insert new List <ETL_Config__c> {
            etlConfig,
            etlconfig2
        };
       test.stopTest();

    }
    static testMethod void testPostInstallScript2() {
        test.startTest();
        //Create instance of InstallHandler
        AxtriaSalesIQTMPostInstallScript postinstall = new AxtriaSalesIQTMPostInstallScript();
       

        ETL_Config__c etlConfig = CallPlanTestDataFactory.createETLConfig();
        etlConfig.Name = 'ScenarioOperations';

        ETL_Config__c etlConfig2 = CallPlanTestDataFactory.createETLConfig();
        etlConfig2.Name = 'BRMS';

        ETL_Config__c etlConfig3 = CallPlanTestDataFactory.createETLConfig();
        etlConfig3.Name = 'Talend';

        insert new List <ETL_Config__c> {
            etlConfig,
            etlConfig2,
            etlconfig3
        };
        test.stopTest();
        }
        static testMethod void testPostInstallScript3(){
        test.startTest();
        
        AxtriaSalesIQTMPostInstallScript postinstall = new AxtriaSalesIQTMPostInstallScript();
        Team__c team= CallPlanTestDataFactory.createTeam('Base');
        Team_Instance__c acct= CallPlanTestDataFactory.CreateTeamInstance(team,'Hybrid');
        
        postinstall.createChangeRequestSectionForZIPAccountInfoInterface();
        postinstall.getZipAccountInfoList(acct.id);
        postinstall.getManageVacantList(acct.id);
        test.stopTest();
        
    }
}