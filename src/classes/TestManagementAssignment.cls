@IsTest 
public with sharing class TestManagementAssignment {
    static testMethod void TestManagementAssignmentCtrl1(){
        list<string> AlignmentType = new list<string>();
        AlignmentType.add('Base');  
        AlignmentType.add('Overlay');    
        
        list<string> AccountAlignmentType = new list<string>();
        AccountAlignmentType.add('ZIP Aligned');
        
        User objUser = CallPlanTestDataFactory.createUser();
        insert objUser;     
        
        //System.runAs(objUser){           
            list<Team__c> lstAllTeam = new list<Team__c>();
            for(string Type:AlignmentType){
                lstAllTeam.Add(CallPlanTestDataFactory.createTeam(Type));
            }
                     
            insert lstAllTeam;
            
            list<Team_Instance__c> lstTeamInstance = new list<Team_Instance__c>();
            integer icount;
            for(icount= 0;icount<AlignmentType.size(); icount++ ){
                lstTeamInstance.add(CallPlanTestDataFactory.CreateTeamInstance(lstAllTeam[icount],AlignmentType[icount]));
            }

            lstTeamInstance[0].team_instance_code__c = 'TeamInst000';
            lstTeamInstance[1].team_instance_code__c = 'TeamInst001';
            insert lstTeamInstance;
            
            Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11778');
            insert geo;
            
            list<Account> allAccount = new list<Account>();
            for(string AccountType:AccountAlignmentType){
                allAccount.Add(CallPlanTestDataFactory.CreateAccount(AccountType, geo.Name,'Physician'));
            }
            insert allAccount;
            
            /*list<Team_Instance_Account__c> lstTeamInstanceAccount = new list<Team_Instance_Account__c>();
            for(Team_Instance__c instance: lstTeamInstance){
                lstTeamInstanceAccount.add(CallPlanTestDataFactory.createTeamInstanceAccount(allAccount[0],instance));
            }
            insert lstTeamInstanceAccount;*/
            
            Position__c RegionPos = CallPlanTestDataFactory.createRegionPosition(lstAllTeam[0]);
            RegionPos.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            RegionPos.Hierarchy_Level__c = '1';
            insert RegionPos;
            
            Position__c positionObj2 = CallPlanTestDataFactory.createPositionParent(RegionPos, lstAllTeam[0]);
            positionObj2.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            positionObj2.Hierarchy_Level__c = '2';
            insert positionObj2;
            
            Position__c positionObj = CallPlanTestDataFactory.createPosition(positionObj2,lstAllTeam[0]);
            positionObj.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            positionObj.Hierarchy_Level__c = '3';
            insert positionObj;
            
            Position__c DestinationpositionObj = CallPlanTestDataFactory.createDestinationPosition(positionObj2,lstAllTeam[0]);
            DestinationpositionObj.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            DestinationpositionObj.Hierarchy_Level__c = '3';
            insert DestinationpositionObj;
    
            Position_Team_Instance__c posTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(positionObj,positionObj2,lstTeamInstance[0]);
            insert posTeamInstance;
            
            Position_Team_Instance__c posTeamInstanceDestination =CallPlanTestDataFactory.CreatePosTeamInstance(DestinationpositionObj,positionObj2,lstTeamInstance[0]);
            insert posTeamInstanceDestination;
            
            Position_Account__c PosAccount = CallPlanTestDataFactory.createPositionAccount(allAccount[0], positionObj, lstTeamInstance[0],posTeamInstance);
            insert PosAccount;
    
            /*list<Team_Instance_Geography__c> allTeamInstanceGeo = new list<Team_Instance_Geography__c>();
            for(Team_Instance__c TeamInst:lstTeamInstance){
                allTeamInstanceGeo.add(CallPlanTestDataFactory.CreateTeamInstanceGeoGraphy(geo,TeamInst));
            }
            insert allTeamInstanceGeo;*/
            
            Position_Geography__c Posgeo = CallPlanTestDataFactory.createPosGeography(geo,positionObj,posTeamInstance,lstTeamInstance[0] );
            insert Posgeo;      
            
            Change_Request_Type__c objZipRequest = CallPlanTestDataFactory.CreateRequestType('Employee Assignment');
            insert objZipRequest;
                       
            list<Change_Request__c> listChangeRequest = new list<Change_Request__c>();
            Integer i;
            for(i = 1 ; i < 4 ; i++){
                Change_Request__c objChangeRequest = new Change_Request__c();
                objChangeRequest.Approver1__c = UserInfo.getuserid();
                objChangeRequest.Destination_Position__c = DestinationpositionObj.id;
                objChangeRequest.Source_Position__c = positionObj.id;
                objChangeRequest.RecordTypeID__c = objZipRequest.id;
                objChangeRequest.status__c = 'Pending';
                objChangeRequest.Request_Type_Change__c = 'Employee Assignment';
                objChangeRequest.Source_Positions__c = posTeamInstance.id;
                objChangeRequest.Destination_Positions__c = posTeamInstanceDestination.id;
                objChangeRequest.Team_Instance_ID__c = lstTeamInstance[0].id;
                
                listChangeRequest.add(objChangeRequest);
            }
            insert listChangeRequest;
            
            list<CIM_Config__c> objCIM = CallPlanTestDataFactory.createCIMCOnfig(objZipRequest, lstTeamInstance[0],'Team_Instance_Geography__c');
            insert objCIM;
            
            list<CIM_Change_Request_Impact__c> objCIMcrImpact = CallPlanTestDataFactory.createCIMchangeRequestImpact(listChangeRequest[0], objCIM[0], posTeamInstance);
            insert objCIMcrImpact;
            
            list<CIM_Change_Request_Impact__c> objCIMcrDestinationImpact = CallPlanTestDataFactory.createCIMchangeRequestImpact(listChangeRequest[0], objCIM[0], posTeamInstanceDestination);
            insert objCIMcrDestinationImpact;
            
            User_Access_Permission__c objUserAccessPermTest =  CallPlanTestDataFactory.createUserAccessPerm(positionObj, objUser,lstTeamInstance[0]);
            insert objUserAccessPermTest;
                                  
            Employee__c managerEmp = CallPlanTestDataFactory.createEmployee();
            insert managerEmp;

            Employee__c empObj = CallPlanTestDataFactory.createEmployee();
            empObj.Manager__c = managerEmp.Id ;
            //insert empObj;
            
            Employee__c empnotObj = CallPlanTestDataFactory.createEmployee();
            empnotObj.Name = 'Test Emp2';
            empnotObj.Employee_ID__c = '12009';
            empnotObj.Last_Name__c = 'Empl1';
            empnotObj.FirstName__c = 'Test2';
            empnotObj.Gender__c = 'M';
            empnotObj.Change_status__c = 'Pending';
            empnotObj.Manager__c = managerEmp.Id ;
            //insert empnotObj;
            
            Employee__c emp2 = new Employee__c();
            emp2.Name = 'Test Emp4';
            emp2.Employee_ID__c = '12809';
            emp2.Last_Name__c = 'Empl1';
            emp2.FirstName__c = 'Test2';
            emp2.Gender__c = 'M';
            emp2.Change_status__c = 'Pending';
            emp2.Manager__c = managerEmp.Id ;
            
            insert new Employee__c[]{empObj, empnotObj, emp2};
            
            
            Employee_Address__c objEMPAddress = new Employee_Address__c();
            objEMPAddress.Employee__c = empObj.id;
            insert objEMPAddress;
            
            Position_Employee__c posEmployee = CallPlanTestDataFactory.createPosEmploye(positionObj,empObj);
            insert posEmployee;
            
            Position_Employee__c posEmployee2 = CallPlanTestDataFactory.createPosEmploye(positionObj2,empObj);                       
            insert posEmployee2;
             
            list<CR_Employee_Assignment__c>  objCRempAssignment = CallPlanTestDataFactory.CreateEmployeeAssignment(listChangeRequest[0], empObj, positionObj);
            insert objCRempAssignment;
            
            CR_Position__c objCRposition = CallPlanTestDataFactory.createCRPosition(listChangeRequest[0]);
            insert objCRposition;
            
            Test.startTest();
            
                PageReference pRef1 = Page.ManageAssignments;
                pRef1.getParameters().put('territory', positionObj.Id);
                pRef1.getParameters().put('eventId', objCRempAssignment[0].Id);
               
                Test.setCurrentPage(pRef1);
                ManageAssignments objManageAssignment = new ManageAssignments();
                objManageAssignment.actionInit();
                objManageAssignment.empChosen =  empObj.id;          
                objManageAssignment.addAssignment();
                objManageAssignment.showPopup();
                objManageAssignment.closeSavePopup();
                objManageAssignment.saveAssignments();
                objManageAssignment.employeeAssignmentDetails();
                objManageAssignment.cancelChanges();
                objManageAssignment.searchBoxSupport();
                objManageAssignment.backToAlignment();
                objManageAssignment.runSearch();
                objManageAssignment.currentAssignments();
                objManageAssignment.deleteId = posEmployee.id;
                objManageAssignment.deleteAssignment();
                pRef1.getParameters().put('empid', empObj.Id);
                objManageAssignment.runSearch();
                objManageAssignment.selectedID = 0;
                objManageAssignment.makeFieldsEditable();
                
                List<SelectOption> temp = objManageAssignment.assignmenttypeList;
                List<SelectOption> temp1 = objManageAssignment.reasonCodeList;
                system.assert(temp != null); 
                
                objManageAssignment.empChosen = empnotObj.id;
                objManageAssignment.selectedID = 0;
                
                ManageAssignments.namespace = '';
                objManageAssignment.showCRMsg = true;
                
                objManageAssignment.closeCRMsg();
                
               
               
                objManageAssignment.empChosen = emp2.id;
                objManageAssignment.saveAssignments();
                
                               
            Test.stopTest();
        //}
    }

    static testMethod void TestManagementAssignmentCtrl2(){
        list<string> AlignmentType = new list<string>();
        AlignmentType.add('Base');  
        AlignmentType.add('Overlay');   

        Alignment_Global_Settings__c timeoutSetting = new Alignment_Global_Settings__c(Name='notificationTimeOut', Tree_Hierarchy_Sort_Field__c = '10000'); 
        insert timeoutSetting ;

        TriggerContol__c stopSecurityCheck = new TriggerContol__c(Name='SecurityUtil', IsStopTrigger__c = true);
        insert stopSecurityCheck;
        
        list<string> AccountAlignmentType = new list<string>();
        AccountAlignmentType.add('ZIP Aligned');
        
        User objUser = CallPlanTestDataFactory.createUser();
        insert objUser;     
        
        System.runAs(objUser){           
            list<Team__c> lstAllTeam = new list<Team__c>();
            for(string Type:AlignmentType){
                lstAllTeam.Add(CallPlanTestDataFactory.createTeam(Type));
            }
                     
            insert lstAllTeam;
            
            list<Team_Instance__c> lstTeamInstance = new list<Team_Instance__c>();
            integer icount;
            for(icount= 0;icount<AlignmentType.size(); icount++ ){
                lstTeamInstance.add(CallPlanTestDataFactory.CreateTeamInstance(lstAllTeam[icount],AlignmentType[icount]));
            }

            lstTeamInstance[0].team_instance_code__c = 'TeamInst000';
            lstTeamInstance[0].IC_EffstartDate__c = Date.today().addDays(10);
            lstTeamInstance[0].IC_EffendDate__c = Date.today().addMonths(10);
            lstTeamInstance[1].team_instance_code__c = 'TeamInst001';
            insert lstTeamInstance;

            Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11778');
            insert geo;
            
            list<Account> allAccount = new list<Account>();
            for(string AccountType:AccountAlignmentType){
                allAccount.Add(CallPlanTestDataFactory.CreateAccount(AccountType, geo.Name,'Physician'));
            }
            insert allAccount;
            
            /*list<Team_Instance_Account__c> lstTeamInstanceAccount = new list<Team_Instance_Account__c>();
            for(Team_Instance__c instance: lstTeamInstance){
                lstTeamInstanceAccount.add(CallPlanTestDataFactory.createTeamInstanceAccount(allAccount[0],instance));
            }
            insert lstTeamInstanceAccount;*/
            

            // nation
            Position__c nation = CallPlanTestDataFactory.createPosition(null, lstAllTeam[0]);
            nation.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            nation.Hierarchy_Level__c = '4';
            nation.Team_Instance__c = lstTeamInstance[0].Id;
            insert nation;

            // region
            Position__c region = CallPlanTestDataFactory.createPositionParent(nation, lstAllTeam[0]);
            region.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            region.Hierarchy_Level__c = '3';
            region.Team_Instance__c = lstTeamInstance[0].Id;
            insert region;

            // area
            Position__c area = CallPlanTestDataFactory.createPositionParent(region, lstAllTeam[0]);
            area.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            area.Hierarchy_Level__c = '3';
            area.Team_Instance__c = lstTeamInstance[0].Id;
            insert area;

            // district
            Position__c district = CallPlanTestDataFactory.createPositionParent(area, lstAllTeam[0]);
            district.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            district.Hierarchy_Level__c = '2';
            district.Team_Instance__c = lstTeamInstance[0].Id;
            insert district;

            // territory
            Position__c positionObj = CallPlanTestDataFactory.createPositionParent(district, lstAllTeam[0]);
            positionObj.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            positionObj.Hierarchy_Level__c = '1';
            positionObj.Team_Instance__c = lstTeamInstance[0].Id;
            insert positionObj;

            
            Position__c DestinationpositionObj = CallPlanTestDataFactory.createDestinationPosition(district,lstAllTeam[0]);
            DestinationpositionObj.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            DestinationpositionObj.Hierarchy_Level__c = '3';
            insert DestinationpositionObj;
    
            Position_Team_Instance__c posTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(positionObj,district,lstTeamInstance[0]);
            insert posTeamInstance;
            
            Position_Team_Instance__c posTeamInstanceDestination =CallPlanTestDataFactory.CreatePosTeamInstance(DestinationpositionObj,district,lstTeamInstance[0]);
            insert posTeamInstanceDestination;  
            
            Change_Request_Type__c objZipRequest = CallPlanTestDataFactory.CreateRequestType('Employee Assignment');
            insert objZipRequest;
                       
            list<Change_Request__c> listChangeRequest = new list<Change_Request__c>();
            Integer i;
            for(i = 1 ; i < 4 ; i++){
                Change_Request__c objChangeRequest = new Change_Request__c();
                objChangeRequest.Approver1__c = UserInfo.getuserid();
                objChangeRequest.Destination_Position__c = DestinationpositionObj.id;
                objChangeRequest.Source_Position__c = positionObj.id;
                objChangeRequest.RecordTypeID__c = objZipRequest.id;
                objChangeRequest.status__c = 'Pending';
                objChangeRequest.Request_Type_Change__c = 'Employee Assignment';
                objChangeRequest.Source_Positions__c = posTeamInstance.id;
                objChangeRequest.Destination_Positions__c = posTeamInstanceDestination.id;
                objChangeRequest.Team_Instance_ID__c = lstTeamInstance[0].id;
                
                listChangeRequest.add(objChangeRequest);
            }
            insert listChangeRequest;
            
            list<CIM_Config__c> objCIM = CallPlanTestDataFactory.createCIMCOnfig(objZipRequest, lstTeamInstance[0],'Team_Instance_Geography__c');
            insert objCIM;
            
            list<CIM_Change_Request_Impact__c> objCIMcrImpact = CallPlanTestDataFactory.createCIMchangeRequestImpact(listChangeRequest[0], objCIM[0], posTeamInstance);
            insert objCIMcrImpact;
            
            list<CIM_Change_Request_Impact__c> objCIMcrDestinationImpact = CallPlanTestDataFactory.createCIMchangeRequestImpact(listChangeRequest[0], objCIM[0], posTeamInstanceDestination);
            insert objCIMcrDestinationImpact;
            
            User_Access_Permission__c objUserAccessPermTest =  CallPlanTestDataFactory.createUserAccessPerm(nation, objUser,lstTeamInstance[0]);
            insert objUserAccessPermTest;
                                  
            Employee__c empObj = CallPlanTestDataFactory.createEmployee();
            empObj.Original_Hire_Date__c = system.today().addDays(5);
            empObj.HR_Termination_Date__c = system.today().addDays(10);
            insert empObj;
            
            Employee__c empnotObj = CallPlanTestDataFactory.createEmployee();
            empnotObj.Name = 'Test Emp2';
            empnotObj.Employee_ID__c = '12009';
            empnotObj.Last_Name__c = 'Empl1';
            empnotObj.FirstName__c = 'Test2';
            empnotObj.Gender__c = 'M';
            empnotObj.Change_status__c = 'Pending';
            insert empnotObj;
            
            Employee__c emp2 = new Employee__c();
            emp2.Name = 'Test Emp4';
            emp2.Employee_ID__c = '12809';
            emp2.Last_Name__c = 'Empl1';
            emp2.FirstName__c = 'Test2';
            emp2.Gender__c = 'M';
            emp2.Change_status__c = 'Pending';
            insert emp2;
            
            
            Employee_Address__c objEMPAddress = new Employee_Address__c();
            objEMPAddress.Employee__c = empObj.id;
            insert objEMPAddress;
            
            Position_Employee__c posEmployee = CallPlanTestDataFactory.createPosEmploye(positionObj,empObj);
            Position_Employee__c posEmployeeF = CallPlanTestDataFactory.createPosEmploye(positionObj,empObj);
            posEmployeeF.Effective_start_date__c = date.today()+3;
            posEmployeeF.Effective_End_Date__c = date.today()+20;
            insert new Position_Employee__c[] {posEmployee,posEmployeeF};
            
            Position_Employee__c posEmployee2 = CallPlanTestDataFactory.createPosEmploye(district,empObj);                       
            insert posEmployee2;
             
            list<CR_Employee_Assignment__c>  objCRempAssignment = CallPlanTestDataFactory.CreateEmployeeAssignment(listChangeRequest[0], empObj, positionObj);
            insert objCRempAssignment;
            
            CR_Position__c objCRposition = CallPlanTestDataFactory.createCRPosition(listChangeRequest[0]);
            insert objCRposition;
            
            Test.startTest();
            
                PageReference pRef1 = Page.ManageAssignments;
                pRef1.getParameters().put('territory', positionObj.Id);
                pRef1.getParameters().put('eventId', objCRempAssignment[0].Id);
                pRef1.getParameters().put('teamInstanceID', lstTeamInstance[0].Id);
                pRef1.getParameters().put('teamID', lstAllTeam[0].Id);
               
                Test.setCurrentPage(pRef1);
                ManageAssignments objManageAssignment = new ManageAssignments();
                objManageAssignment.actionInit();
                objManageAssignment.empChosen =  empObj.id;          
                objManageAssignment.addAssignment();
                objManageAssignment.saveAssignments();
                objManageAssignment.employeeAssignmentDetails();
                objManageAssignment.cancelChanges();
                objManageAssignment.searchBoxSupport();
                objManageAssignment.backToAlignment();
                objManageAssignment.runSearch();
                objManageAssignment.currentAssignments();
                objManageAssignment.deleteId = posEmployee.id;
                objManageAssignment.deleteAssignment();
                pRef1.getParameters().put('empid', empObj.Id);
                objManageAssignment.runSearch();
                objManageAssignment.selectedID = 0;
                objManageAssignment.makeFieldsEditable();
                
                List<SelectOption> temp = objManageAssignment.assignmenttypeList;
                List<SelectOption> temp1 = objManageAssignment.reasonCodeList;
                system.assert(temp != null); 
                
                objManageAssignment.empChosen = empnotObj.id;
                objManageAssignment.selectedID = 0;
               
                objManageAssignment.wrapperListCurAssign[0].getStartDateFormatted();
                objManageAssignment.wrapperListCurAssign[0].getEndDateFormatted();

                objManageAssignment.empAssignmentList[0].getStartDateFormatted();
                objManageAssignment.empAssignmentList[0].getEndDateFormatted();

                //system.assertEquals(null, objManageAssignment.wrapperListCurAssign[0].pEmp.Effective_Start_Date__c);
                objManageAssignment.wrapperListCurAssign[0].pEmp.Effective_Start_Date__c = null ;
                objManageAssignment.saveAssignments();

                objManageAssignment.wrapperListCurAssign[0].pEmp.Effective_Start_Date__c = system.today().addDays(-3) ;
                objManageAssignment.wrapperListCurAssign[0].pEmp.Effective_End_Date__c = null ;
                objManageAssignment.saveAssignments();

                objManageAssignment.wrapperListCurAssign[0].pEmp.Effective_Start_Date__c = system.today().addDays(20) ;
                objManageAssignment.wrapperListCurAssign[0].pEmp.Effective_End_Date__c = system.today().addDays(3) ;
                objManageAssignment.saveAssignments();

                objManageAssignment.wrapperListCurAssign[0].pEmp.Effective_Start_Date__c = system.today().addDays(-3) ;
                objManageAssignment.wrapperListCurAssign[0].pEmp.Effective_End_Date__c = system.today().addMonths(30) ;
                objManageAssignment.saveAssignments();

                objManageAssignment.wrapperListCurAssign[0].pEmp.Effective_Start_Date__c = system.today().addDays(-3) ;
                objManageAssignment.wrapperListCurAssign[0].pEmp.Effective_End_Date__c = system.today().addDays(3) ;
                objManageAssignment.saveAssignments();

                objManageAssignment.empChosen = emp2.id;
                objManageAssignment.addAssignment();

                objManageAssignment.wrapperListCurAssign[2].pEmp.Effective_Start_Date__c = system.today().addDays(-3) ;
                objManageAssignment.wrapperListCurAssign[2].pEmp.Effective_End_Date__c = system.today().addDays(3) ;
                objManageAssignment.saveAssignments();

                
                               
            Test.stopTest();
        }
    }

    static testMethod void TestManagementAssignmentCtrl3(){
        list<string> AlignmentType = new list<string>();
        AlignmentType.add('Base');  
        AlignmentType.add('Overlay');   

        Alignment_Global_Settings__c timeoutSetting = new Alignment_Global_Settings__c(Name='notificationTimeOut', Tree_Hierarchy_Sort_Field__c = '10000'); 
        insert timeoutSetting ;

        TriggerContol__c stopSecurityCheck = new TriggerContol__c(Name='SecurityUtil', IsStopTrigger__c = true);
        insert stopSecurityCheck;
        
        User objUser = CallPlanTestDataFactory.createUser();
        insert objUser;     
        
        System.runAs(objUser){           
            list<Team__c> lstAllTeam = new list<Team__c>();
            for(string Type:AlignmentType){
                lstAllTeam.Add(CallPlanTestDataFactory.createTeam(Type));
            }
                     
            insert lstAllTeam;
            
            list<Team_Instance__c> lstTeamInstance = new list<Team_Instance__c>();
            integer icount;
            for(icount= 0;icount<AlignmentType.size(); icount++ ){
                lstTeamInstance.add(CallPlanTestDataFactory.CreateTeamInstance(lstAllTeam[icount],AlignmentType[icount]));
            }

            lstTeamInstance[0].team_instance_code__c = 'TeamInst000';
            lstTeamInstance[0].IC_EffstartDate__c = Date.today().addMonths(-5);
            lstTeamInstance[0].IC_EffendDate__c = Date.today().addMonths(10);
            lstTeamInstance[1].team_instance_code__c = 'TeamInst001';
            insert lstTeamInstance;


            // nation
            Position__c nation = CallPlanTestDataFactory.createPosition(null, lstAllTeam[0]);
            nation.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            nation.Hierarchy_Level__c = '4';
            nation.Team_Instance__c = lstTeamInstance[0].Id;
            insert nation;

            // region
            Position__c region = CallPlanTestDataFactory.createPositionParent(nation, lstAllTeam[0]);
            region.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            region.Hierarchy_Level__c = '3';
            region.Team_Instance__c = lstTeamInstance[0].Id;
            insert region;

            // area
            Position__c area = CallPlanTestDataFactory.createPositionParent(region, lstAllTeam[0]);
            area.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            area.Hierarchy_Level__c = '3';
            area.Team_Instance__c = lstTeamInstance[0].Id;
            insert area;

            // district
            Position__c district = CallPlanTestDataFactory.createPositionParent(area, lstAllTeam[0]);
            district.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            district.Hierarchy_Level__c = '2';
            district.Team_Instance__c = lstTeamInstance[0].Id;
            insert district;

            // territory
            Position__c positionObj = CallPlanTestDataFactory.createPositionParent(district, lstAllTeam[0]);
            positionObj.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            positionObj.Hierarchy_Level__c = '1';
            positionObj.Team_Instance__c = lstTeamInstance[0].Id;
            positionObj.Effective_Start_Date__c = Date.today().addMonths(-5);
            positionObj.Effective_End_Date__c = Date.today().addMonths(10);
            insert positionObj;

            
            Position__c DestinationpositionObj = CallPlanTestDataFactory.createDestinationPosition(district,lstAllTeam[0]);
            DestinationpositionObj.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            DestinationpositionObj.Hierarchy_Level__c = '3';
            insert DestinationpositionObj;
    
            Position_Team_Instance__c posTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(positionObj,district,lstTeamInstance[0]);
            insert posTeamInstance;
            
            Position_Team_Instance__c posTeamInstanceDestination =CallPlanTestDataFactory.CreatePosTeamInstance(DestinationpositionObj,district,lstTeamInstance[0]);
            insert posTeamInstanceDestination;  
            
            Change_Request_Type__c employEAssigType = CallPlanTestDataFactory.CreateRequestType('Employee Assignment');
            insert employEAssigType;
            
            User_Access_Permission__c objUserAccessPermTest =  CallPlanTestDataFactory.createUserAccessPerm(nation, objUser,lstTeamInstance[0]);
            insert objUserAccessPermTest;
                                  
            Employee__c empObj = CallPlanTestDataFactory.createEmployee();
            insert empObj;
            
            Employee__c empnotObj = CallPlanTestDataFactory.createEmployee();
            empnotObj.Name = 'Test Emp2';
            empnotObj.Employee_ID__c = '12009';
            empnotObj.Last_Name__c = 'Empl1';
            empnotObj.FirstName__c = 'Test2';
            empnotObj.Gender__c = 'M';
            empnotObj.Change_status__c = 'Pending';
            insert empnotObj;
            
            Employee__c emp2 = new Employee__c();
            emp2.Name = 'Test Emp4';
            emp2.Employee_ID__c = '12809';
            emp2.Last_Name__c = 'Empl1';
            emp2.FirstName__c = 'Test2';
            emp2.Gender__c = 'M';
            emp2.Change_status__c = 'Pending';
            insert emp2;

            
            Position_Employee__c posEmployee = CallPlanTestDataFactory.createPosEmploye(positionObj,empObj);
            insert new Position_Employee__c[] {posEmployee};

            CR_Approval_Config__c appConfig = new CR_Approval_Config__c(Team_Instance_ID__c = lstTeamInstance[0].Id, Change_Request_Type__c = employEAssigType.Id, HO_Office_User__c = 'Y', HR_User__c = 'N', IsImpactedPosition__c = false, isHigherLevelManagers__c = false, isImpactedManager__c = false);
            insert appConfig;
            
            Test.startTest();
            
                PageReference pRef1 = Page.ManageAssignments;
                pRef1.getParameters().put('territory', positionObj.Id);
                pRef1.getParameters().put('teamInstanceID', lstTeamInstance[0].Id);
                pRef1.getParameters().put('teamID', lstAllTeam[0].Id);
               
                Test.setCurrentPage(pRef1);
                ManageAssignments objManageAssignment = new ManageAssignments();
                objManageAssignment.actionInit();
                objManageAssignment.empChosen =  empObj.id;          
                objManageAssignment.addAssignment();

                objManageAssignment.wrapperListCurAssign[1].pEmp.Effective_Start_Date__c = system.today().addDays(5) ;
                objManageAssignment.wrapperListCurAssign[1].pEmp.Effective_End_Date__c = system.today().addDays(10) ;

                objManageAssignment.saveAssignments();

                objManageAssignment.empChosen =  empnotObj.id;          
                objManageAssignment.addAssignment();
                objManageAssignment.wrapperListCurAssign[1].pEmp.Assignment_Type__c = 'Primary';
                objManageAssignment.saveAssignments();
                               
            Test.stopTest();
        }
    }

    static testMethod void TestManagementAssignmentCtrl4(){
        ManageAssignments objManageAssignment ;
        try{
            objManageAssignment = new ManageAssignments();
        } catch(Exception e) {

        }

        try{
            objManageAssignment.actionInit();
        } catch(Exception e) {

        }
    }
}