public with sharing class SalesIQQueryBuilderCtrl {
	public String teamInstanceId {get;set;}
    public String interfaceName {get;set;}

    public String configJson{
    	get{
			List<Team_Instance_Object_Attribute__c> fetchAllRequiredColumns = new list<Team_Instance_Object_Attribute__c>();
            fetchAllRequiredColumns = SalesIQUtility.getTeamInstanceAttr(new List<String>{interfaceName}, new List<String>{teamInstanceId}, false, false);
			List<String> columnsData = new List<String>();
            System.debug('==fetchAllRequiredColumns ' + fetchAllRequiredColumns);
			for(Team_Instance_Object_Attribute__c attribute : fetchAllRequiredColumns){
                if(attribute.Enabled_Advance_Search__c){
                    String dataType = String.valueOf(attribute.Data_Type__c).toLowerCase();
                    if(dataType == 'text')
                        dataType = 'string';
                    else if(dataType == 'checkbox' || dataType == 'boolean')
                        dataType = 'boolean';
                    else if(dataType == 'number' || dataType == 'integer' || dataType == 'double')
                        dataType = 'double';
                    else
                        dataType = 'string';

    				String operators = getOperatorsToType(dataType);
    				operators = operators.removeStart('\'');
    				operators = operators.removeEnd('\'');

                    /*if(dataType == 'boolean'){
                        columnsData.add('{ "id": "' + attribute.Attribute_API_Name__c + '", "operators" : ' + operators + ' , "label" : "' + attribute.Attribute_Display_Name__c + '" , "input" : "radio", "values" : ["true", "false"],  "type": "'+dataType+'"}');
                    }
                    else*/
    				columnsData.add('{ "id": "' + attribute.Attribute_API_Name__c + '", "validation" : {"format" : "^[_a-z,_A-Z_,_0-9_,_ ]*$"}, "operators" : ' + operators + ' , "label" : "' + AlignmentUtility.getLabelString(attribute.Attribute_Display_Name__c) + '" ,"type": "'+dataType+'"}');
                }
            }
			configJson = '['+String.join(columnsData, ',')+']';
			System.debug('--configJson ' + configJson);
    		return configJson;
    	}
    	set;
    }

   	public String getOperatorsToType(String type){
    	if(type == 'string' || type == 'text')
    		return '["equal", "not_equal", "in", "not_in", "contains", "is_empty", "is_not_empty"]';
    	else if(type == 'Double' || type == 'integer' || type =='number' || type == 'date' || type == 'datetime')
    		return '["equal", "not_equal", "less", "less_or_equal", "greater", "greater_or_equal", "is_not_empty"]';
    	else if(type == 'Boolean')
    		return '["equal", "not_equal"]';
    	return '';
    }
}