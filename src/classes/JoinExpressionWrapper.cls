public with sharing class JoinExpressionWrapper {
    
    @AuraEnabled
    public String ruleName {get;set;}
    @AuraEnabled
    public String defaultexpression {get; set;}      
    @AuraEnabled
    public List<String> selectedPicklistValues {get; set;}        
    @AuraEnabled
    public String firstdatasource {get; set;}
    @AuraEnabled
    public String seconddatasource {get; set;}
     @AuraEnabled
    public String executeexpression {get; set;}

    public JoinExpressionWrapper() {

    }
    
    public JoinExpressionWrapper(String ruleName,string defaultexpression,List<String> selectedPicklistValues,String firstdatasource,String seconddatasource,String executeexpression){
        this.ruleName = ruleName;
        this.defaultexpression = defaultexpression;
        this.selectedPicklistValues = selectedPicklistValues;
        this.firstdatasource = firstdatasource;
        this.seconddatasource = seconddatasource;
        this.executeexpression = executeexpression;

    }
   
}