@isTest
private class TestReinitiateMetricsAndEsri {
    static testMethod void testMethod1() {

    	Workspace__c ws = CallPlanTestDataFactory.createWorkspace();
    	insert ws;

    	Team__c team = CallPlanTestDataFactory.createTeam('Base');
    	insert team;

    	Team_Instance__c teamIns = CallPlanTestDataFactory.createTeamInstance(team, 'ZIP');
    	insert teamIns;

    	Scenario__c scenario = CallPlanTestDataFactory.createScenario(ws, teamIns);
    	insert scenario;

        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();

        req.requestURI = '/services/apexrest/salesiq/reinitiateMetricsAndEsri';  
        req.httpMethod = 'GET';
        req.addParameter('scenarioId',scenario.Id);
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        String results = ReinitiateMetricsAndEsri.doGet();
        Test.stopTest();

    	//RestRequest restReq = RestContext.request;
		//String scenarioId = restReq.params.put('scenarioId', scenario.Id) ;

		//ReinitiateMetricsAndEsri.doGet();  
    }
}