public with sharing class SalesIQTreeAndBreadcrumbCtrl 
{
    @AuraEnabled
    public static String getOrgNamespace() 
    {
        return SalesIQGlobalConstants.NAME_SPACE;        
    }
    
	@AuraEnabled
    public static Map<String,List<Team_Instance__c>> getBreadcrumbTeamInstance(String moduleName, String countryId)
    {
        System.debug('================== countryId breadcrumb'+countryId);
    	try {
            Map<String,List<Team_Instance__c>> teamInstanceMap = new Map<String,List<Team_Instance__c>>();

            List<Team_Instance__c> teamInsList = SalesIQUtility.getUserAccessTeamInstance('',moduleName,countryId);
            if(teamInsList.size() < 1)
            {
                AuraHandledException ex = new AuraHandledException(System.Label.No_UAP_Assigned);
                ex.setMessage(System.Label.No_UAP_Assigned);
                throw ex;
            }

          	for (Team_Instance__c  obj : teamInsList)
          	{
                if(!teamInstanceMap.containsKey(obj.Team__c))
                    teamInstanceMap.put(obj.Team__c, new List<Team_Instance__c>{obj});
                else
                    teamInstanceMap.get(obj.Team__c).add(obj);
          	}
        
        	return teamInstanceMap;
        } catch(Exception e) {
            String message = e.getTypeName() + ': ' + e.getMessage() + '\n\n' + e.getStackTraceString() ;
            AuraHandledException ex = new AuraHandledException(message);
            ex.setMessage(message);
            throw ex;
        }
    }

    @AuraEnabled
    public static List<String> getParentPosition(Id positionid, Id selectedTeamInstance,String moduleName, String countryId)
    {
        try {

                System.debug('getParentPosition ----- :'+selectedTeamInstance);
                List<String> positions = new List<String>();
                String ns = SalesIQGlobalConstants.Name_space;        
                /*List<User_Access_Permission__c> currentUserPermission ;
                List<String> userAccessReadFields = new List<String>{'Position__c', 'User__c', 'Is_Active__c', 'Team_Instance__c'};
                if(SecurityUtil.checkRead(User_Access_Permission__c.SObjectType, userAccessReadFields, false)) 
                {    
                    currentUserPermission = SalesIQUtility.getLoggedInUserAccessRecords(true, selectedTeamInstance, null);        
                }*/

                Position__c pos ;
                pos =  [SELECT Id,Name, Client_Territory_Name__c, Team_Instance__c, Parent_Position__c,Parent_Position__r.Name,Position_Type__c FROM Position__c where id=:positionid];
                selectedTeamInstance = pos.Team_Instance__c;

                System.debug('selectedTeamInstance ' + selectedTeamInstance);
                List<Team_Instance__c> teamInsList = SalesIQUtility.getUserAccessTeamInstance(selectedTeamInstance,moduleName, countryId);
                if(teamInsList.size() < 1)
                {
                    AuraHandledException ex = new AuraHandledException(System.Label.No_UAP_Assigned);
                    ex.setMessage(System.Label.No_UAP_Assigned);
                    throw ex;
                }

                /*Position__c pos ;
                pos =  [SELECT Id,Name, Client_Territory_Name__c, Parent_Position__c,Parent_Position__r.Name,Position_Type__c FROM Position__c where id=:positionid];
*/
                String queryString = 'SELECT Name,'+ns+'Client_Territory_Name__c,'+ns+'Parent_Position__c,'+ns+'Parent_Position__r.Name,'+ns+'Position_Type__c FROM '+ns+'Position__c';
                String intermediateQuery = SalesIQUtility.getIntermediateTeamInstanceQuery(teamInsList,null,selectedTeamInstance,null, true);
                queryString = queryString + ' WHERE ' + intermediateQuery;

                List<Position__c> masterPosList = new List<Position__c>();
                List<String> positionReadFields = new List<String>{'Position_Type__c', 'Parent_Position__c'};
                if(SecurityUtil.checkRead(Position__c.SObjectType, positionReadFields, false))
                {
                    System.debug('--queryString ' + queryString);
                    masterPosList = Database.query(queryString); 
                }



                Map<String,Position__c> posMap = new Map<String,Position__c>();
                for(Position__c mypos : masterPosList)
                {
                    posMap.put(String.valueOf(mypos.id),mypos);
                }

                Position__c tempPos = pos;
                positions.add(pos.Client_Territory_Name__c);
                while(true){
                    if(tempPos.Parent_Position__c != null && posMap.containsKey(tempPos.Parent_Position__c)){
                        positions.add(posMap.get(tempPos.Parent_Position__c).Client_Territory_Name__c);
                        tempPos = posMap.get(tempPos.Parent_Position__c);
                    }
                    else
                        break;
                }
                /*do
                {   
                    tempPos = posMap.get(String.valueOf(tempPos.Parent_Position__c));
                    positions.add(String.valueOf(tempPos.Client_Territory_Name__c));
                }while((tempPos != null && tempPos.Parent_Position__c != null) && tempPos.id != currentUserPermission[0].Position__c);
                */
                return positions;
                 
                
                
           
            
        } catch(Exception e) {
            String message = e.getTypeName() + ': ' + e.getMessage() + '\n\n' + e.getStackTraceString() ;
            AuraHandledException ex = new AuraHandledException(message);
            ex.setMessage(message);
            throw ex;
        }
    }

    @AuraEnabled 
    public static SalesIQPositionHierarchyWrapper getUserRoles(String selectedTeamInstance,String moduleName, String countryId, List<String> teamProductIds, String selectedRuleLevel, String scenarioRuleInstanceId) 
    {
        try{
            String ns = SalesIQGlobalConstants.Name_space;

            System.debug('selectedTeamInstance -'+selectedTeamInstance);
            if(selectedTeamInstance.indexOf('All') != -1){
                String teamID = selectedTeamInstance.split('_')[1];
                list<Team_Instance__c> teamInstance = new list<Team_Instance__c>();
                teamInstance = [select Scenario__r.IsLatest_Employee_Assignment__c, Scenario__r.Employee_Assignment__c from Team_Instance__c where Team__c =:teamID AND Scenario__c != null limit 1];
                System.debug('teamInstance :'+teamInstance);
                if(teamInstance[0].Scenario__r.IsLatest_Employee_Assignment__c)
                    selectedTeamInstance = teamInstance[0].ID;
            }
            

            String queryString = 'SELECT Name,'+ns+'Client_Territory_Name__c,'+ns+'Related_Position_Type__c,'+ns+'Parent_Position__c,'+ns+'Hierarchy_Level__c,'+ns+'RGB__c FROM '+ns+'Position__c';
            List<Team_Instance__c> teamInsList = SalesIQUtility.getUserAccessTeamInstance(selectedTeamInstance,moduleName, countryId);
            if(teamInsList.size() < 1 && selectedTeamInstance.indexOf('All') == -1)
            {
                AuraHandledException ex = new AuraHandledException(System.Label.No_UAP_Assigned);
                ex.setMessage(System.Label.No_UAP_Assigned);
                
                throw ex;
            }

            String intermediateQuery = SalesIQUtility.getIntermediateTeamInstanceQuery(teamInsList,null,selectedTeamInstance,null, true);
            queryString = queryString + ' WHERE ' + intermediateQuery;

            String orderByClause = '';
            if(Alignment_Global_Settings__c.getValues('global') != null)
                orderByClause = Alignment_Global_Settings__c.getValues('global').Tree_Hierarchy_Sort_Field__c ;
            
            if(orderByClause == null || orderByClause == '')
                orderByClause = 'Name' ;

            queryString += ' order by ' + orderByClause ;

            System.debug('queryString :'+queryString);

            List<String> positionReadFields = new List<String>{'Team_instance__c', 'Client_Position_Code__c', 'Hierarchy_Level__c', 'Parent_Position__c', 'RGB__c'};
            List<Position__c> posList = new List<Position__c>();
            Map<Id,List<Change_Request__c>> CrMap = new Map<Id,List<Change_Request__c>>();
            if(SecurityUtil.checkRead(Position__c.SObjectType, positionReadFields, false))
               posList = Database.query(queryString); 
            else
                return null ;
            List<String> posIdList = new List<String>();
            for(Position__c pos : posList)
            {
                posIdList.add(pos.Id);
            }
            List<Change_Request__c> crList = getPendingChangeRequests(posIdList,moduleName);
            for(Change_Request__c cr: crList)
            {
                if(CrMap.containsKey(cr.Source_Position__c))
                {
                   Set<Change_Request__c> intermediateCRSet = new Set<Change_Request__c>(CrMap.get(cr.Source_Position__c));
                   intermediateCRSet.add(cr);

                   List<Change_Request__c> intermediateCRlist = new list<Change_Request__c>();
                   intermediateCRlist.addAll(intermediateCRSet);
                   CrMap.put(cr.Source_Position__c, intermediateCRlist);
                }
                else
                {
                    CrMap.put(cr.Source_Position__c, new List<Change_Request__c>{cr});
                }
                if(CrMap.containsKey(cr.destination_position__c))
                {
                   Set<Change_Request__c> intermediateCRSet = new Set<Change_Request__c>(CrMap.get(cr.destination_position__c));
                   intermediateCRSet.add(cr);


                   List<Change_Request__c> intermediateCRlist = new list<Change_Request__c>();
                   intermediateCRlist.addAll(intermediateCRSet);
                   CrMap.put(cr.destination_position__c, intermediateCRlist);

                }
                else
                {
                    CrMap.put(cr.destination_position__c, new List<Change_Request__c>{cr});
                }
            }

            SalesIQPositionHierarchyWrapper wrapper = new SalesIQPositionHierarchyWrapper();
            wrapper.positionList = posList;
            wrapper.pendingCRMap = CrMap;

            //if(selectedRuleLevel == 'Position' && selectedProductId != null && posIdList.size() > 0 ){
            if( scenarioRuleInstanceId != null && scenarioRuleInstanceId != 'undefined' && scenarioRuleInstanceId != '' )
                wrapper.mapPositonProducts = getPositionProducts( new Set<String>(teamProductIds),new Set<String>(posIdList), scenarioRuleInstanceId, countryId);
            //}

            return wrapper;
        } 
        catch(Exception e) {
            String message = e.getTypeName() + ': ' + e.getMessage() + '\n\n' + e.getStackTraceString() ;
            AuraHandledException ex = new AuraHandledException(message);
            ex.setMessage(message);
            throw ex;
        }
    }

    @AuraEnabled
    public static SalesIQPositionHierarchyWrapper findByName(String searchKey,String selectedTeamInstance,String moduleName, String countryId) 
    {
        try {
            String name = '%' + searchKey + '%';
            String ns = SalesIQGlobalConstants.Name_space;

            system.debug('========== tree hierarchy countryId'+countryId);

            // String queryString = '';
            List<Team_Instance__c> teamInsList = SalesIQUtility.getUserAccessTeamInstance(selectedTeamInstance,moduleName,countryId);
            String intermediateQuery = SalesIQUtility.getIntermediateTeamInstanceQuery(teamInsList,null,selectedTeamInstance,name, true);
            String queryString = 'SELECT Name,'+ns+'Client_Territory_Name__c,'+ns+'Parent_Position__c,'+ns+'Hierarchy_Level__c,'+ns+'RGB__c FROM '+ns+'Position__c';
            // queryString = 'SELECT Name,'+ns+'Client_Territory_Name__c,'+ns+'RGB__c,'+ns+'Parent_Position__c,'+ns+'Hierarchy_Level__c,';
            // queryString += ns+'Parent_Position__r.Name, '+ns+'Parent_Position__r.'+ns+'Hierarchy_Level__c, '+ns+'Parent_Position__r.'+ns+'RGB__c,'+ns+'Parent_Position__r.'+ns+'Client_Territory_Name__c,';
            // queryString += ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'RGB__c,'+ns+'Parent_Position__r.'+ns+'Parent_Position__c,'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.Name,'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Hierarchy_Level__c,'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Client_Territory_Name__c,' ;
            // queryString += ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Parent_Position__c,'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.Name, '+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Hierarchy_Level__c,'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'RGB__c,'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Client_Territory_Name__c,' ;
            // queryString += ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.Name,'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Hierarchy_Level__c,'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'RGB__c,'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Parent_Position__r.'+ns+'Client_Territory_Name__c'+' FROM '+ns+'Position__c';

            queryString = queryString + ' WHERE ' + intermediateQuery;

            String queryStringMaster = 'SELECT Name,'+ns+'Client_Territory_Name__c,'+ns+'Parent_Position__c,'+ns+'Hierarchy_Level__c,'+ns+'RGB__c FROM '+ns+'Position__c';
            String intermediateQueryMaster = SalesIQUtility.getIntermediateTeamInstanceQuery(teamInsList,null,selectedTeamInstance,null, true);
            queryStringMaster = queryStringMaster + ' WHERE ' + intermediateQueryMaster;

            List<Position__c> masterPosList = Database.query(queryStringMaster);
            Map<String,Position__c> posMap = new Map<String,Position__c>();
            for(Position__c pos : masterPosList)
            {
                posMap.put(String.valueOf(pos.id),pos);
            }
            System.debug(posMap);
            String orderByClause = '';
            if(Alignment_Global_Settings__c.getValues('global') != null)
                orderByClause = Alignment_Global_Settings__c.getValues('global').Tree_Hierarchy_Sort_Field__c ;
            
            if(orderByClause == null || orderByClause == '')
                orderByClause = 'Name' ;

            queryString += ' order by ' + orderByClause ;
                    
            List<String> positionReadFields = new List<String>{'Team_instance__c', 'Client_Position_Code__c', 'Hierarchy_Level__c', 'Parent_Position__c', 'RGB__c'};
            List<Position__c> posList = new List<Position__c>();
            Map<Id,List<Change_Request__c>> CrMap = new Map<Id,List<Change_Request__c>>();
            if(SecurityUtil.checkRead(Position__c.SObjectType, positionReadFields, false))
               posList = Database.query(queryString); 
            else
                return null ;

            set<String> posIdList = new set<String>();
            List<Position__c> finalPosList = new List<Position__c>(); 
            for(Position__c pos : posList)
            {
                Position__c tempPos = pos;
                do 
                {
                    system.debug(tempPos.Name);
                    if(!posIdList.contains(String.valueOf(tempPos.Id)))
                    {
                        posIdList.add(String.valueOf(tempPos.Id));
                        finalPosList.add(posMap.get(String.valueOf(tempPos.id)));
                    }
                    else
                    {
                        break;
                    }
                    tempPos = posMap.get(String.valueOf(tempPos.Parent_Position__c));
                }while(tempPos != null && tempPos.Parent_Position__c != null);
                
            }
            system.debug('outside-->');
            system.debug(finalPosList);
            system.debug(posIdList);
            list<String> myPosIdList = new List<String>();
            myPosIdList.addAll(posIdList);
            List<Change_Request__c> crList = getPendingChangeRequests(myPosIdList,moduleName);
            for(Change_Request__c cr: crList)
            {
                if(CrMap.containsKey(cr.Source_Position__c))
                {
                   Set<Change_Request__c> intermediateCRSet = new Set<Change_Request__c>(CrMap.get(cr.Source_Position__c));
                   intermediateCRSet.add(cr);

                   List<Change_Request__c> intermediateCRlist = new list<Change_Request__c>();
                   intermediateCRlist.addAll(intermediateCRSet);
                   CrMap.put(cr.Source_Position__c, intermediateCRlist);

                }
                else
                {
                    CrMap.put(cr.Source_Position__c, new List<Change_Request__c>{cr});
                }
                if(CrMap.containsKey(cr.destination_position__c))
                {
                    Set<Change_Request__c> intermediateCRSet = new Set<Change_Request__c>(CrMap.get(cr.destination_position__c));
                    intermediateCRSet.add(cr);

                    List<Change_Request__c> intermediateCRlist = new list<Change_Request__c>();
                    intermediateCRlist.addAll(intermediateCRSet);
                    CrMap.put(cr.destination_position__c, intermediateCRlist);
                }
                else
                {
                    CrMap.put(cr.destination_position__c, new List<Change_Request__c>{cr});
                }
            }
            SalesIQPositionHierarchyWrapper wrapper = new SalesIQPositionHierarchyWrapper();
            wrapper.positionList = finalPosList;
            wrapper.pendingCRMap = CrMap;

            return wrapper;
        } catch(Exception e) {
            String message = e.getTypeName() + ': ' + e.getMessage() + '\n\n' + e.getStackTraceString() ;
            AuraHandledException ex = new AuraHandledException(message);
            ex.setMessage(message);
            throw ex;
        }
    }

    @AuraEnabled
    public static List<Change_Request__c> getPendingChangeRequests(List<String> posIdList, String moduleName)
    {
        // List<String> pendingCR = new List<String>();
        List<Change_Request__c> crList;
        if(moduleName == SalesIQGlobalConstants.CALL_PLAN_MODULE)
        {
            crList = SalesIQUtility.isChangeRequestPending( posIdList, new List<String>{SalesIQGlobalConstants.REQUEST_TYPE_CALL_PLAN});
        }
        else if(moduleName == SalesIQGlobalConstants.POSITION_UNIVERSE)
        {
            crList = SalesIQUtility.isChangeRequestPending( posIdList, new List<String>{SalesIQGlobalConstants.UPDATE_POSITION_ATTRIBUTES,SalesIQGlobalConstants.UPDATE_POSITION, SalesIQGlobalConstants.HIERARCHY_CHANGE , SalesIQGlobalConstants.CREATE_POSITION, SalesIQGlobalConstants.DELETE_POSITION, SalesIQGlobalConstants.CREATE_OVERLAY_POSITION, SalesIQGlobalConstants.CREATE_OVERLAY_POSITION, SalesIQGlobalConstants.UPDATE_BASE_OVERLAY_MAPPING, SalesIQGlobalConstants.UPDATE_OVERLAY_POSITION,SalesIQGlobalConstants.DELETE_OVERLAY_POSITION,SalesIQGlobalConstants.HIERARCHY_CHANGE_OVERLAY,SalesIQGlobalConstants.EMPLOYEE_ASSIGNMENT,SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP,SalesIQGlobalConstants.MOVEMENT_TYPE_ACCOUNT});
        }
        else if(moduleName == SalesIQGlobalConstants.EMPLOYEE_UNIVERSE)
        {
            crList = SalesIQUtility.isChangeRequestPending( posIdList, new List<String>{SalesIQGlobalConstants.EMPLOYEE_ASSIGNMENT});   
        }

        return crList;
    }

    @AuraEnabled
    public static void logException(String message, String module) 
    {
        if(message.length() > 2000)
            message = message.substring(0,2000);
        SalesIQLogger.logErrorMessage(message, module);
    }

    public class SalesIQPositionHierarchyWrapper  
    {
        @AuraEnabled
        public List<Position__c> positionList {get;set;}
        @AuraEnabled
        public Map<Id,List<Change_Request__c>> pendingCRMap {get;set;}
        @AuraEnabled 
        public Map<String,Set<String>> mapPositonProducts {get;set;}
    }

    /*METHOD TO GET ALL POSTION PRODUCT ON SELECTED TEAM INSTANCE*/

    public static Map<String, Set<String>> getPositionProducts(Set<String> teamProductIds, Set<String> setPositionIds, String scenarioRuleInstanceId, String countryId){
        Map<String, Set<String>> positionToProductMap = new Map<String,Set<String>>();
        try{
             //GET PRODUCT ASSIGNMENT DATASETID
            List<Data_Set_Rule_Map__c> dataSetRuleMap = [ Select dataset_id__c from Data_Set_Rule_Map__c where File_Type__c = :CPGConstants.ASSIGNMENT_FILE_TYPE AND scenario_rule_instance_id__c = :scenarioRuleInstanceId limit 1 ] ;

            String dataSetId = dataSetRuleMap[0].dataset_id__c;

            //GET PRODUCT COLUMN API NAME
            List<Data_Set_Column_Detail__c> dataSetColumns = [ Select id, Source_Column__c FROM Data_Set_Column_Detail__c where dataset_id__c =: dataSetId and Key_Column__c = true limit 1];

            String productColumnApiName = dataSetColumns[0].Source_Column__c;

            String productsQuery = 'Select id, Position__c, '+ productColumnApiName+ ' FROM Position_Product__c where Position__c IN :setPositionIds  AND ' + productColumnApiName+' IN :teamProductIds';

            List<Position_Product__c> listPositionProducts = Database.Query(productsQuery);
            system.debug(listPositionProducts);

            for( Position_Product__c obj :  listPositionProducts) {
                if(positionToProductMap.containsKey( String.valueOf(obj.get(productColumnApiName)))){
                    if(!positionToProductMap.get(String.valueOf(obj.get(productColumnApiName))).contains(obj.Position__c)){
                        positionToProductMap.get(String.valueOf(obj.get(productColumnApiName))).add(obj.Position__c);
                    } 
                }else{
                    Set<String> setPositionId = new Set<String>();
                    setPositionId.add(obj.Position__c);
                    positionToProductMap.put( String.valueOf(obj.get(productColumnApiName)), setPositionId);
                }
               
            }

        }catch(Exception e) {
            return positionToProductMap;
        }
        return positionToProductMap;
    }

    @AuraEnabled
    public static String getAllPositionViewCustomSetting(){
        String showOnlyAllPositionView = 'true';
        if(Alignment_Global_Settings__c.getValues('Show only All Positions View') != null){
            showOnlyAllPositionView = Alignment_Global_Settings__c.getValues('Show only All Positions View').Tree_Hierarchy_Sort_Field__c;
        }

        return showOnlyAllPositionView;
    } 
}
