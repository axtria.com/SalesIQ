global with sharing class EsriSyncingWithTalendBatch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts 
{
	global String scenarioId;	

	global EsriSyncingWithTalendBatch(Id scenario)
	{
		scenarioId = scenario;
	}

	global Database.QueryLocator start(Database.BatchableContext BC){
		try{
			return Database.getQueryLocator('Select id from account limit 1');
		}
		catch(Exception ex){
			SalesIQLogger.logCatchedException(ex, false, 'Module : esriSyncingWithTalendBatch.cls');
			Database.QueryLocator dbquery;
			return dbquery;
		}
	}

	global void execute(Database.BatchableContext BC, List<Account> scope){    
	}
	
	global void finish(Database.BatchableContext BC){
        esriSyncingwithTalend(scenarioId);
    }

    public static void esriSyncingwithTalend(String scenarioId)
    {
        String s_username,s_password,s_token,s_orgtype,dest_team_instance_id;
        list<ETL_Config__c> lstETLInfo = [SELECT SF_Password__c,SF_UserName__c, S3_Security_Token__c, Org_Type__c FROM ETL_Config__c where name =: CPGConstants.talend_Instance limit 1];

        if(lstETLInfo != null && lstETLInfo.size() > 0)
        {
            s_username = lstETLInfo[0].SF_UserName__c;
            s_password = lstETLInfo[0].SF_Password__c;
            s_token = lstETLInfo[0].S3_Security_Token__c;
            s_orgtype = lstETLInfo[0].Org_Type__c;
        }

        Scenario__c scenarioGeoType = [select Id,Team_Instance__c,Team_Instance__r.Geography_Type_Name__r.Shape_Available__c,  Team_Instance__r.Geography_Type_Name__r.Account_Shape_Available__c from Scenario__c where Id =: scenarioId];


        dest_team_instance_id = scenarioGeoType.Team_Instance__c;

        string esriSyncService = CPGUtility.getBRMSConfigValues('BREsriSyncAfterPush');
        //IN CASE
        //1. ESRI SYNC SERVICE IS NOT AVAILABLE. MARK SCENARIO AS SUCCESS
        //2. IN CASE BOTH ACCOUNT SHAPE AND GEO SHAPE IS NOT AVAILABLE MARK SCENARIO AS SUCCESS
        if( !scenarioGeoType.Team_Instance__r.Geography_Type_Name__r.Account_Shape_Available__c && !scenarioGeoType.Team_Instance__r.Geography_Type_Name__r.Shape_Available__c ) 
        {
            //MARK SCENARIO AS SUCCESS
            Scenario__c mySc = new Scenario__c(id= scenarioId,Rule_Execution_Status__c = CPGConstants.SUCCESS);
            update mySc;
            return;
        }
        else
        {
            if( esriSyncService == null )
            {
                //MARK SCENARIO AS SUCCESS
                Scenario__c mySc = new Scenario__c(id= scenarioId,Rule_Execution_Status__c = CPGConstants.SUCCESS);
                update mySc;
                return;
            }
        String url = esriSyncService + '?sf_username='+s_username+'&sf_password='+s_password+'&sf_token='+s_token+'&sf_sandbox='+SalesIQUtility.isSandboxOrg()+'&namespace='+SalesIQGlobalConstants.NAME_SPACE+'&action=esri_insert&scenario_id='+scenarioId+'&dest_team_instance_id='+dest_team_instance_id+'&brms_esri_sync=true';

        system.debug(' ESRi sync service url>>>'+url);

        Http h = new Http();

        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        url = url.replaceAll( '\\s+', '%20');
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept','application/json');
        req.setTimeout(120000);
        // Send the request, and return a response
        HttpResponse res = h.send(req);
        System.debug('esriSyncingwithTalend response:--> ' + res.getBody());
        String result = res.getBody();
    }	
    }	
}