@istest
private class TestSalesIQKPICardsCtrl 
{
	static testMethod void kpiCardsTest()
	{
        String namespacePrefix = SalesIQGlobalConstants.NAME_SPACE;
		TriggerContol__c offSecurityUtil = new TriggerContol__c(Name='SecurityUtil', IsStopTrigger__c = true);
        insert offSecurityUtil;
        
        Profile p = [select id from Profile where name = 'System Administrator'];
        
        User loggedInUser = TestDataFactory.createUser(p.Id);
        insert loggedInUser;
        
        Organization_Master__c orgMaster = new Organization_Master__c();
        orgMaster.Name = 'North America';
        orgMaster.Parent_Country_Level__c = true;
        orgMaster.Org_Level__c  = 'Region';
        insert orgMaster;

        Country__c country = new Country__c();
        country.Name = 'USA';
        country.Parent_Organization__c = orgMaster.id;
        country.status__c = 'Active';
        country.Country_Code__c = 'US';
        insert country;
        
        Team__c team = TestDataFactory.createTeam('Base');
        team.Country__c = country.id;
        insert team;    
        
        Team_Instance__c teamIns = TestDataFactory.CreateTeamInstance(team.Id,'Zip','',Date.today()-100,Date.today()+100);
        insert teamIns;
        
        Position__C pos1 = TestDataFactory.createPosition(team, 'PositionRegion', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_REGION, '3', null);
        pos1.Team_Instance__c = teamIns.ID;
        insert pos1;
        Position__C pos2 = TestDataFactory.createPosition(team, 'PositionDistrict', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_DISTRICT, '2', pos1.Id);
        pos2.Team_Instance__c = teamIns.ID;
        insert pos2;
        Position__c position = TestDataFactory.createPosition(team, 'PositionTerritory', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_TERRITORY, '1', pos2.Id);
        position.Team_Instance__c = teamIns.ID;
        insert position ;

        

        Workspace__c ws = TestDataFactory.createWorkspace('WS1', system.today().addMonths(-2), system.today().addMonths(3));
        ws.Country__c = country.id;
        insert ws;
        Scenario__c callPlanScenario = TestDataFactory.createScenario(ws.Id,teamIns.Id,teamIns.Id,'Live','Ready','Active',System.today().addMonths(-1),System.today().addMonths(2));
        callPlanScenario.Scenario_type__c = SalesIQGlobalConstants.SCENARIO_TYPE_CALL_PLAN;
        callPlanScenario.KPI_Card_Guardrail_Config__c = true;
        callPlanScenario.Scenario_Source__c = SalesIQGlobalConstants.SCENARIO_SOURCE_CREATE_FROM_FILE;
        insert callPlanScenario ;

        Position_Team_Instance__c posTeam = new Position_Team_Instance__c();
        posTeam.Position_ID__c = position.id;
        posTeam.Parent_Position_ID__c = position.id;
        posTeam.Team_Instance_ID__c = teamIns.id;
        posTeam.Effective_End_Date__c = Date.today()+100;
        posTeam.Effective_Start_Date__c = Date.today()-100;
        insert posTeam;

        Change_Request_Type__c crType = CallPlanTestDataFactory.CreateCallPlanRequestType();
        insert crType;

        Change_Request__c changedRec = new Change_Request__c();                                               
        changedRec.Reason__c =  'Call Plan Changes';
        changedRec.recordtypeId = SalesIQUtility.getselctedRecordType(namespacePrefix + 'Change_Request__c', SalesIQGlobalConstants.REQUEST_TYPE_CALL_PLAN );
        changedRec.RecordTypeID__c = SalesIQUtility.getChangeRequestTypebyRequestType('Call_Plan_Change');
        changedRec.Request_Type_Change__c = SalesIQGlobalConstants.REQUEST_TYPE_CALL_PLAN;  
                // changedRec.Account_SF_Id__c = movedAccs.removeEnd(',');
        changedRec.Source_Position__c = position.Id;
        changedRec.Destination_Position__c = position.Id;  
        changedRec.Team_Instance_ID__c = teamIns.id;
        changedRec.Status__c = SalesIQGlobalConstants.REQUEST_STATUS_PENDING;
        insert changedRec;
        
        list<CIM_Config__c> cimConfigs = CallPlanTestDataFactory.createCIMCOnfig(crType, teamIns, 'Position_Account_Call_Plan__c');
        cimConfigs[0].Name = 'No of Targets';
        cimConfigs[0].MetricCalculationType__c = 'Absolute';
        cimConfigs[1].Name = 'Total Calls';
        cimConfigs[1].isOptimum__c = true;
        cimConfigs[2].Name = 'No of Tier1 Physician';
        cimConfigs[2].MetricCalculationType__c = 'Absolute';
        cimConfigs[2].isOptimum__c = true;
        cimConfigs[3].Name = 'No of Tier2 Physician';
        cimConfigs[4].Name = 'No of Tier3 Physician';
        cimConfigs[5].Name = 'No of Non-Target Physician';
        insert cimConfigs;
        
        list<CIM_Position_Metric_Summary__c> cimSummary = CallPlanTestDataFactory.createCIMMetricSummary(cimConfigs,posTeam,teamIns);
        insert cimSummary;

        DiplayColorCriteria__c criteria1 = new DiplayColorCriteria__c(name=SalesIQGlobalConstants.ERROR,Color__c='#000000');
        DiplayColorCriteria__c criteria2 = new DiplayColorCriteria__c(name=SalesIQGlobalConstants.WARNING,Color__c='#000001');
        DiplayColorCriteria__c criteria3 = new DiplayColorCriteria__c(name=SalesIQGlobalConstants.NORMAL,Color__c='#000002');
        insert new List<DiplayColorCriteria__c>{criteria1,criteria2,criteria3};

        System.runAs(loggedInUser)
        {
        	PermissionSet ps = [SELECT Id, Name From PermissionSet WHERE Name = 'SalesIQ_HO'];
	        insert new PermissionSetAssignment(AssigneeId = loggedInUser.Id, PermissionSetId = ps.Id );
	        User_Access_Permission__c objUserAccessPerm = TestDataFactory.createUserAccessPerm(position.Id,loggedInUser.Id,teamIns.Id);
            objUserAccessPerm.Default_Country__c = true;
	        insert objUserAccessPerm;
            
            test.startTest();
            List<SalesIQKPICardsCtrl.SalesIQKPICardsWrapper> wrapperList1 = SalesIQKPICardsCtrl.getKPICardsDetail('Call_Plan_Change',teamIns.Id,position.Id,'','');
            try{
                List<SalesIQKPICardsCtrl.SalesIQKPICardsWrapper> wrapperList2 = SalesIQKPICardsCtrl.getKPICardsDetail('Call_Plan_Change','','','','');
                List<SalesIQKPICardsCtrl.SalesIQKPICardsWrapper> wrapperList3 = SalesIQKPICardsCtrl.getKPICardsDetail('Call_Plan_Change','','',changedRec.Id,'');
                List<SalesIQKPICardsCtrl.SalesIQKPICardsWrapper> wrapperList4 = SalesIQKPICardsCtrl.getKPICardsDetail('Call_Plan_Change',teamIns.Id,'','','');
            }catch(Exception e){
                
            }
            
            SalesIQKPICardsCtrl.logException('',SalesIQGlobalConstants.CALL_PLAN_MODULE);
            test.stopTest();
        }    
	}
    
}