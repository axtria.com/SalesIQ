public class CPGCategoryHelperParser {   
    public static string inputCondition = '';
    public static boolean elseNotExists = false;
    public CPGCategoryHelperParser(){
        inputCondition = '';
    }
    //public static String CreateCategoryParser(String ruleId){
     public static Map<string,string> CreateCategoryParser(String ruleId){
        map<id, list<id>> mapGrp = new map<id, list<id>>();
        map<id, Category_Rule_Entity_Detail__c> mapEntity = new map<id, Category_Rule_Entity_Detail__c>();
        list<id> lstBlockId = new List<id>();
        list<id> lstGrpId = new List<id>();        
       // String expression='';
       Map<string,string> expressionMap=new Map<string,string>();
        
        map<string, string> mapMedataData= new map<string, string>();
        
        list<Category_Derived_Variable_Metadata__c> lstOutputVariables= [select id,Field_Name__c,DataType__c 
                                                                         from Category_Derived_Variable_Metadata__c
                                                                         where Business_Rule_Id__c=: ruleId];
        for(Category_Derived_Variable_Metadata__c metadata : lstOutputVariables)
        {
            mapMedataData.put(metadata.Field_Name__c, metadata.DataType__c);
        }
        
        list<Category_Rule_Entity_Detail__c> lstRuleEntity= [select id,Parent_Entity__c,Category_Type__c,Logic_Operator__c,
                                                             Entity_Type__c,FormulaPriority__c,
                                                             Priority__c from Category_Rule_Entity_Detail__c 
                                                             where Business_Rule__c=: ruleId order by FormulaPriority__c asc];
        
        for(Category_Rule_Entity_Detail__c entity : lstRuleEntity){        
            mapEntity.put(entity.id, entity);
            if(entity.Entity_Type__c =='Group'){
                lstGrpId.add(entity.id);                
                if(mapGrp.containsKey(entity.Parent_Entity__c)){
                    List<id> grpId= mapGrp.get(entity.Parent_Entity__c);
                    grpId.add(entity.Id);
                    mapGrp.put(entity.Parent_Entity__c, grpId);                     
                }
                else{
                    mapGrp.put(entity.Parent_Entity__c, new List<Id> {entity.id});                    
                }
            }
            else if(entity.Entity_Type__c =='Block'){
                lstBlockId.add(entity.id);
            }
        }
        map<id,list<Category_Rule_Criteria_Detail__c>> mapRuleCri = new map<id,list<Category_Rule_Criteria_Detail__c>>();      
        for(Category_Rule_Criteria_Detail__c criDetail : [select id,Criteria_operator__c, Field__c,Logic_Operator__c,
                                                          Sequence__c,Param_value__c,Category_Rule_Entity_Detail__c,
                                                          FieldDataType__c
                                                          from Category_Rule_Criteria_Detail__c
                                                          where Category_Rule_Entity_Detail__c in: lstGrpId 
                                                          order by Sequence__c asc])
        {        
            if(mapRuleCri.containsKey(criDetail.Category_Rule_Entity_Detail__c)){
                List<Category_Rule_Criteria_Detail__c> lstCri= mapRuleCri.get(criDetail.Category_Rule_Entity_Detail__c);
                lstCri.add(criDetail);
                mapRuleCri.put(criDetail.Category_Rule_Entity_Detail__c, lstCri);   
            }
            else
                mapRuleCri.put(criDetail.Category_Rule_Entity_Detail__c, new List<Category_Rule_Criteria_Detail__c> {criDetail});
        }
        map<id,list<Category_Rule_Output_Detail__c>> mapCateg= new map<id,list<Category_Rule_Output_Detail__c>>();
        for(Category_Rule_Output_Detail__c detail: [select id,Category_Rule_Entity_Detail__c,
                                                    Derived__c,Variable_Value__c
                                                    from Category_Rule_Output_Detail__c
                                                    where Category_Rule_Entity_Detail__c in:lstBlockId])
        {
            if(mapCateg.containsKey(detail.Category_Rule_Entity_Detail__c)){
                List<Category_Rule_Output_Detail__c> lstCri= mapCateg.get(detail.Category_Rule_Entity_Detail__c);
                lstCri.add(detail);
                mapCateg.put(detail.Category_Rule_Entity_Detail__c, lstCri);   
            }
            else
                mapCateg.put(detail.Category_Rule_Entity_Detail__c, new List<Category_Rule_Output_Detail__c> {detail});
        }
        
        integer counter=0;
        //set<string> outputCol = new set<string>();
        
        string condition='';
        string outputColValues = '';
        String outputCol='';
        integer ctr = 0;
        system.debug('lstOutputVariables'+lstOutputVariables);
        inputCondition = '';
        for(Category_Derived_Variable_Metadata__c outputVariable : lstOutputVariables)
        {
            system.debug('outputVariable'+outputVariable);
            String otputData = outputVariable.Field_Name__c.replace(' ','_');
            outputCol+=otputData+',';
            elseNotExists = false;
            String expressionData = CreateGroupExpression(lstRuleEntity,mapRuleCri,mapCateg,
                                                          outputVariable.Field_Name__c,mapGrp, 
                                                          mapEntity,mapMedataData,ctr); 
            system.debug('elseNotExists'+elseNotExists);
            if(elseNotExists == false){            
                expressionData += ' else '+ otputData.toLowerCase();                        
            }  
            outputColValues = GetOutputValues(outputColValues,outputVariable.Field_Name__c,lstRuleEntity,mapCateg);
            system.debug('outputColValues'+outputColValues);
            outputColValues+='$';
            system.debug('expressionData'+expressionData);
            ctr++;
           /* expression += otputData.toLowerCase() + ' = case ' + expressionData;
            expression += ' end';
            if(counter<lstOutputVariables.size()-1){
                expression+=' , ';
            }*/
            expressionMap.put(otputData.toLowerCase(), 'case ' + expressionData.replace('#','total')+' end');
            ++counter;
        }
        
              
        
       /* if(expression.contains('#'))
            expression = expression.replace('#','total');   */
        
        //for decrease reach
        list<Business_rule_type_master__c> ruleMaster = [select id 
                                                         from Business_rule_type_master__c
                                                         where name ='Decrease'];  
        if(ruleMaster != null && ruleMaster.size()>0)
        {
            list<Business_rules__c> lstRules = [select id
                                                from Business_rules__c
                                                where id=: ruleId
                                                and Business_Rule_Type_Master__c=: ruleMaster[0].id];   
            if(lstRules != null && lstRules.size()>0)
            {
                //This is business rule from decrease reach
                //Need to populate Call_balancing_expression_data_detail__c object
                PopulateExpressionObject(lstOutputVariables, outputColValues,outputCol,ruleId);
            }
        }
        return expressionMap;
    }
    static String GetOutputValues(String outputColValues, String derivedFieldName,
                                  list<Category_Rule_Entity_Detail__c> lstRuleEntity,
                                  map<id,list<Category_Rule_Output_Detail__c>> mapCateg)
    {
        for(Category_Rule_Entity_Detail__c ruleEntity : lstRuleEntity){            
            if(ruleEntity.Entity_Type__c == 'Block'){                
                if(ruleEntity.Category_Type__c == 'If' || ruleEntity.Category_Type__c == 'Else If'){
                    list<Category_Rule_Output_Detail__c> outPutDetails = mapCateg.get(ruleEntity.id);
                    for(Category_Rule_Output_Detail__c det : outPutDetails){                        
                        if(det.Derived__c == derivedFieldName){    
                            //system.debug('derivedFieldName-->'+derivedFieldName);
                            outputColValues += det.Variable_Value__c+'@';                                                                              
                        }
                    }
                }
            }
        }
        return outputColValues;
    }
    static void PopulateExpressionObject(list<Category_Derived_Variable_Metadata__c> lstOutputVariables, 
                                         string outputColValues,string outputCol, string ruleId)
    {
        system.debug('ruleId'+ruleId);
        system.debug('inputCondition'+inputCondition);
        outputCol = outputCol.removeEnd(',');  
        list<Call_balancing_expression_data_detail__c> lstExpression = new list<Call_balancing_expression_data_detail__c>();
        string[] arrColValues = outputColValues.split('\\$');
        inputCondition = inputCondition.removeEnd(',');  
        string[] inputCond = inputCondition.split(',');
        system.debug('inputCond'+inputCond.size());
        integer cnt = arrColValues.size();
        string newStr= '';
        string strCol ='';
        string strCond ='';
        for(integer input=0; input < inputCond.size(); input++){
            Call_balancing_expression_data_detail__c expression = new Call_balancing_expression_data_detail__c();                          
            newStr= '';
            for(string str : arrColValues){                
                string[] strValues = str.split('\\@');               
                newStr += strValues[input] + ',';
            }
            newStr= newStr.removeEnd(',');            
            
            strCol = outputCol;
            // strCol= strCol.replace('{','');
            // strCol= strCol.replace('}','');
            
            strCond = inputCond[input];
            strCond= strCond.replace('(','');
            strCond= strCond.replace(')','');
            expression.output_column_values__c = newStr;
            expression.Output_Column__c = strCol;
            expression.Business_Rules__c = ruleId;                                
            expression.Input_Condition__c = strCond;
            lstExpression.add(expression);
        }
        insert lstExpression;
    }
    
    static String CreateGroupExpression(list<Category_Rule_Entity_Detail__c> lstRuleEntity,
                                        map<id,list<Category_Rule_Criteria_Detail__c>> mapRuleCri,
                                        map<id,list<Category_Rule_Output_Detail__c>> mapCateg,
                                        String derivedFieldName,
                                        map<id, list<id>> mapGrp,map<id, Category_Rule_Entity_Detail__c> mapEntity,
                                        map<string, string> mapMedataData, integer counter)                                   
    {
        String queryStatement='';
        string expStatement='';
        String variableValue='';
        
        for(Category_Rule_Entity_Detail__c ruleEntity : lstRuleEntity){            
            if(ruleEntity.Entity_Type__c == 'Block'){ //if
                   if(ruleEntity.Category_Type__c == 'If' || ruleEntity.Category_Type__c == 'Else If')
                   {                                        
                       queryStatement += ' when'; 
                       queryStatement += '(' ;
                       for(Id grpId : mapGrp.get(ruleEntity.id)){
                           
                           Category_Rule_Entity_Detail__c EntityDet= mapEntity.get(grpId);  //1st grp  
                           if(EntityDet.Logic_Operator__c != null){                                                            
                               queryStatement += EntityDet.Logic_Operator__c+' ';  
                               expStatement =  EntityDet.Logic_Operator__c+' ';  
                           }
                           
                           list<Category_Rule_Criteria_Detail__c> lstCriteria = mapRuleCri.get(grpId);                        
                           String statement = CreateCriteriaExpression(grpId,lstCriteria);   
                           queryStatement += statement;
                           expStatement = statement;  
                       }
                       if(counter==0)
                           inputCondition+=expStatement+',';                    
                       queryStatement +=')';
                       list<Category_Rule_Output_Detail__c> outPutDetails = mapCateg.get(ruleEntity.id);
                       for(Category_Rule_Output_Detail__c det : outPutDetails){                        
                           if(det.Derived__c == derivedFieldName){    
                               String dataTypeVar = mapMedataData.get(derivedFieldName);
                               if(dataTypeVar == 'Text')
                                   variableValue='\''+det.Variable_Value__c+'\'';
                               else
                                   variableValue=det.Variable_Value__c;                               
                               queryStatement += ' then '+ variableValue;    
                           }
                       }
                   }
                   else if(ruleEntity.Category_Type__c == 'Else'){
                       system.debug('ruleEntity.Category_Type__c'+ruleEntity.Category_Type__c);                       
                       if(mapCateg.get(ruleEntity.id) != null){
                           elseNotExists = true;
                           list<Category_Rule_Output_Detail__c> outPutDetails = mapCateg.get(ruleEntity.id);
                           for(Category_Rule_Output_Detail__c det : outPutDetails){
                               if(det.Derived__c == derivedFieldName){                              
                                   String dataTypeVar = mapMedataData.get(derivedFieldName);
                                   if(dataTypeVar == 'Text' && det.Variable_Value__c != null && det.Variable_Value__c != ''){
                                       variableValue='\''+det.Variable_Value__c+'\'';
                                       queryStatement += ' else '+ variableValue;    
                                   }
                                   else if(dataTypeVar == 'Numeric'){
                                       variableValue=det.Variable_Value__c;
                                       queryStatement += ' else '+ variableValue;  
                                   }
                               }
                           }
                       }
                   }
               }
        }                
        string str = queryStatement;
        system.debug('str'+ str);
        return str;
    }
    
    static String CreateCriteriaExpression(Id grpId, list<Category_Rule_Criteria_Detail__c> lstCriteria){            
        String queryStatement = ' (' ;
        for(Category_Rule_Criteria_Detail__c cri : lstCriteria){
            if(cri.Logic_Operator__c== '-')
                cri.Logic_Operator__c='';                      
            queryStatement += FormatExpression(cri) ;            
        }
        queryStatement += ')';
        return queryStatement;
    }
    static string FormatExpression(Category_Rule_Criteria_Detail__c cri){        
        String queryStatement=''; 
        String paramValue='';
        if(cri.Param_value__c!=null && cri.Param_value__c!= ''){            
            if(cri.FieldDataType__c == 'Text'){
                if(cri.Criteria_operator__c== 'equals' || cri.Criteria_operator__c== 'not equals')
                    paramValue='\'' + cri.Param_value__c+ '\'';
                else if(cri.Criteria_operator__c== 'in' || cri.Criteria_operator__c== 'not in'){  
                    String dataArr = '';  
                    integer ctr=0;
                    for(string val : cri.Param_value__c.split(',')){                              
                        dataArr+=('\''+val+'\''); 
                        if(ctr<cri.Param_value__c.split(',').size()-1){
                            dataArr+=',';
                        }
                        ++ctr;
                        
                    }
                    paramValue='('+dataArr+')';
                }
                else if(cri.Criteria_operator__c== 'contains' || cri.Criteria_operator__c== 'does not contains'){      
                    paramValue='\'%'+cri.Param_value__c+'%\'';
                }   
                else if(cri.Criteria_operator__c== 'begins with')     
                    paramValue='\''+cri.Param_value__c+ '%\'';
                
                else if(cri.Criteria_operator__c== 'ends with')     
                    paramValue='\'%'+cri.Param_value__c+'\'';   
                else
                    paramValue='\''+cri.Param_value__c+'\'';
            }
            else if(cri.FieldDataType__c == 'Numeric'){            
                if(cri.Criteria_operator__c == 'in' || cri.Criteria_operator__c== 'not in'){                
                    paramValue='(' + cri.Param_value__c +')';
                }  
                else
                    paramValue=cri.Param_value__c;
            }
            
        }
        string criteriaOperator='';
        criteriaOperator=  ReplaceExpression(cri.Criteria_operator__c);        
        if(cri.Field__c.contains('#'))
            cri.Field__c=cri.Field__c.replace('#','total');
        cri.Field__c=cri.Field__c.replace(' ','_');
        if(cri.Param_value__c ==null || cri.Param_value__c  == '' || criteriaOperator=='is null' || criteriaOperator=='is not null')
            paramValue='';
        if(cri.Logic_Operator__c != null)
            queryStatement = cri.Logic_Operator__c+' '+ cri.Field__c.toLowerCase()+' '+ criteriaOperator+ ' '+paramValue+' ';
        else
            queryStatement =  cri.Field__c.toLowerCase() +' '+ criteriaOperator+ ' '+paramValue+' ';
        
        return queryStatement;
    }
    static String ReplaceExpression(String expression){         
        String postgresExpression='';
        list<CategoryPostgresOperands__mdt> lstMetaData = [select PostgresOperands__c, SalesforceOperands__c 
                                                           from CategoryPostgresOperands__mdt];        
        
        for(CategoryPostgresOperands__mdt obj : lstMetaData){  
            if(expression.replace(' ','_') == obj.SalesforceOperands__c){                             
                expression=obj.PostgresOperands__c;                   
            }
        }   
        return expression;
    }
}