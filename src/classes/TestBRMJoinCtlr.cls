@isTest
private class TestBRMJoinCtlr{
    static testMethod void brmJoinTest() {

    	Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        insert workSpaceObj;

        Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        insert scenarioObj;

        Business_Rule_Template__c  busRuleTemObj = new Business_Rule_Template__c();
        busRuleTemObj.isActive__c=true;
        busRuleTemObj.isInternal__c=true;
        insert busRuleTemObj;
        
        BusinessRule_Template_Details__c busRuleTemDet = new BusinessRule_Template_Details__c();
        busRuleTemDet.is_Visible__c=true;
        busRuleTemDet.Business_Rule_Template_Id__c=busRuleTemObj.id;
        insert busRuleTemDet;

         ComponentTypeMaster__c ComTypeMasObj = new ComponentTypeMaster__c();
        ComTypeMasObj.Name='Call Balancing';
        ComTypeMasObj.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj.DisplayIcon__c='add';
        ComTypeMasObj.DisplayIconClass__c='window information';
        insert ComTypeMasObj;

        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        sceRuleInsDetObj.Component_Display_Order__c=4;
        sceRuleInsDetObj.Is_Visible__c=true;
        sceRuleInsDetObj.Component_Type_Master__c=ComTypeMasObj.id;
        sceRuleInsDetObj.BRT_Details_Id__c=busRuleTemDet.id;
        insert sceRuleInsDetObj;

        Scenario_Rule_Instance_Details__c sceRuleInsDetObj1 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj1.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj1.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        sceRuleInsDetObj1.Component_Display_Order__c=4;
        sceRuleInsDetObj1.Is_Visible__c=true;
        sceRuleInsDetObj1.Component_Type_Master__c=ComTypeMasObj.id;
        sceRuleInsDetObj1.BRT_Details_Id__c=busRuleTemDet.id;
        insert sceRuleInsDetObj1;

        Data_Set__c dataSetObj = new Data_Set__c();
        dataSetObj.Name = 'Call Plan Summary';
        insert dataSetObj;
        System.debug('dataSetObj Id:: '+dataSetObj.Id);

        Data_Set__c dataSetObj1 = new Data_Set__c();
        dataSetObj1.Name = 'Final Call Plan';
        insert dataSetObj1;
        System.debug('dataSetObj1 Id:: '+dataSetObj1.Id);
        
        Data_Set_Rule_Map__c dataSetRuleObj = new Data_Set_Rule_Map__c();
        dataSetRuleObj.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj.ds_type__c='I';
        dataSetRuleObj.dataset_id__c=dataSetObj.id;
        insert dataSetRuleObj;

        Data_Set_Rule_Map__c dataSetRuleObj2 = new Data_Set_Rule_Map__c();
        dataSetRuleObj2.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj2.ds_type__c='O';
        dataSetRuleObj2.dataset_id__c=dataSetObj.id;
        insert dataSetRuleObj2;

        Data_Set_Rule_Map__c dataSetRuleObj3 = new Data_Set_Rule_Map__c();
        dataSetRuleObj3.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj3.ds_type__c='I';
        dataSetRuleObj3.dataset_id__c=dataSetObj.id;
        insert dataSetRuleObj3;


        Data_Set_Column_Detail__c dataSetColObj = new Data_Set_Column_Detail__c();
        dataSetColObj.dataset_id__c=dataSetObj.id;
        dataSetColObj.ds_col_name__c='AccountNumber';
        dataSetColObj.tb_col_nm__c = 'account_number';
        dataSetColObj.dataset_id__c = dataSetObj.id;
        dataSetColObj.datatype__c = 'Text';
        dataSetColObj.JoinStatus_Flag__c = true;
        dataSetColObj.Table_Data_Type__c = 'text';
        dataSetColObj.variable_type__c = 'Source';
        dataSetColObj.value_type__c = 'Discrete';
        dataSetColObj.Name = 'Customer Universe';
        insert dataSetColObj;

        Data_Set_Column_Detail__c dataSetColObj1 = new Data_Set_Column_Detail__c();
        // dataSetColObj1.dataset_id__c=dataSetObj.id;
        dataSetColObj1.ds_col_name__c='AcctNum';
        dataSetColObj1.tb_col_nm__c = 'account_num';
        dataSetColObj1.dataset_id__c = dataSetObj1.id;
        dataSetColObj1.datatype__c = 'Text';
        dataSetColObj1.JoinStatus_Flag__c = true;
        dataSetColObj1.Table_Data_Type__c = 'text';
        dataSetColObj1.variable_type__c = 'Source';
        dataSetColObj1.value_type__c = 'Discrete';
        dataSetColObj1.Name = 'Customer Universe';
        insert dataSetColObj1;

        Business_Rule_Type_Master__c busRuleTypeMasObj8 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj8.Name='Join';
        busRuleTypeMasObj8.Component_Type__c = ComTypeMasObj.Id;
        insert busRuleTypeMasObj8;

        Business_Rules__c busRuleObjCom = new Business_Rules__c();
        busRuleObjCom.Name='Join rule 66';
        busRuleObjCom.Business_Rule_Type_Master__c=busRuleTypeMasObj8.id;
        busRuleObjCom.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObjCom.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        insert busRuleObjCom;

        BR_Join_Rule_Condition__c joinrulecondobj = new BR_Join_Rule_Condition__c();
        joinrulecondobj.Business_Rules__c = busRuleObjCom.Id;
        joinrulecondobj.Data_Set_1_Field__c = dataSetColObj.Id;
        joinrulecondobj.Data_Set_1__c = dataSetObj.Id;
        joinrulecondobj.Data_Set_2_Field__c = dataSetColObj1.Id;
        joinrulecondobj.Data_Set_2__c = dataSetObj1.Id;
        joinrulecondobj.JoinType__c = 'INNER';
        joinrulecondobj.Scenario__c = scenarioObj.Id;
        insert joinrulecondobj;

        list<String> sceRuleDetList = new list<String>();
        sceRuleDetList.add(dataSetColObj.ds_col_name__c);
        sceRuleDetList.add(dataSetColObj1.ds_col_name__c);

         list<String> datasetList = new list<String>();
        datasetList.add(dataSetObj.id);
        datasetList.add(dataSetObj1.id);

        JoinExpressionWrapper expobj = new JoinExpressionWrapper();
        expobj.ruleName = 'dsadsa';//busRuleObjCom.Name;
        expobj.defaultexpression = 'Account Master_I INNER JOIN Zip Terr_I ON Account Master_I.AccountNumber = Zip Terr_I.ZIP';
        // expobj.selectedPicklistValues = new List<String> {"1":"AccountNumber","2":"ZIP"};
        expobj.selectedPicklistValues = new List<String>{'{\"1\":\"AccountNumber\",\"2\":\"AcctNum\"}'};
        expobj.firstdatasource = dataSetObj.id;
        expobj.seconddatasource = dataSetObj1.id;
        expobj.executeexpression = 't_acct_mst_scenario_2362 INNER JOIN t_zip_terr_scenario_2362 ON t_acct_mst_scenario_2362.accountnumber = t_zip_terr_scenario_2362.zip';
        List<JoinExpressionWrapper> joinExpList = new List<JoinExpressionWrapper>{expobj};

        System.debug(JSON.serialize(joinExpList));


        JoinFieldsWrapper fieldobj = new JoinFieldsWrapper();
        fieldobj.columnId = '';
        fieldobj.tableColumnName = dataSetColObj.tb_col_nm__c;
        fieldobj.colDatatype = dataSetColObj.datatype__c;
        fieldobj.colValuetype = dataSetColObj.value_type__c;
        fieldobj.statusFlag = dataSetColObj.JoinStatus_Flag__c;
        fieldobj.datasourceName = dataSetColObj.Name;
        fieldobj.datasourceColumnName = dataSetColObj.ds_col_name__c;
        fieldobj.datasetId = dataSetObj.id;
        fieldobj.tableDataType = dataSetColObj.Table_Data_Type__c;
        fieldobj.variableType = dataSetColObj.variable_type__c;
        fieldobj.Id = dataSetColObj.Id;
        List<JoinFieldsWrapper> joinFieldsWrapperList = new List<JoinFieldsWrapper>();
        joinFieldsWrapperList.add(fieldobj);

        JoinFieldsWrapper fieldobj1 = new JoinFieldsWrapper();
        fieldobj1.columnId = '';
        fieldobj1.tableColumnName = dataSetColObj1.tb_col_nm__c;
        fieldobj1.colDatatype = dataSetColObj1.datatype__c;
        fieldobj1.colValuetype = dataSetColObj1.value_type__c;
        fieldobj1.statusFlag = dataSetColObj1.JoinStatus_Flag__c;
        fieldobj1.datasourceName = dataSetColObj1.Name;
        fieldobj1.datasourceColumnName = dataSetColObj1.ds_col_name__c;
        fieldobj1.datasetId = dataSetObj1.id;
        fieldobj1.tableDataType = dataSetColObj1.Table_Data_Type__c;
        fieldobj1.variableType = dataSetColObj1.variable_type__c;
        fieldobj1.Id = dataSetColObj1.Id;
        joinFieldsWrapperList.add(fieldobj1);

        BRJoinRuleSelectCondition__c selectcondobj = new BRJoinRuleSelectCondition__c();
        selectcondobj.Business_Rules__c = busRuleObjCom.Id;
        selectcondobj.Data_Set_Field__c = dataSetColObj.Id;
        selectcondobj.Data_Set__c = dataSetObj.Id;
        selectcondobj.Scenario__c = scenarioObj.Id;
        insert selectcondobj;

        BR_JoinRule__c joinruleobj = new BR_JoinRule__c();
        joinruleobj.Business_Rules__c = busRuleObjCom.Id;
        joinruleobj.Scenario__c = scenarioObj.Id;
        insert joinruleobj;
        
        String whereClaus = '1=1';
        
        String joinfieldswrapListserialized = JSON.serialize(joinFieldsWrapperList);

        Test.startTest();
      
        List<Data_Set_Rule_Map__c> getInputOutputDatasourcesList = BRMJoinCtlr.getInputOutputDatasources(sceRuleInsDetObj.Id);
        List<Data_Set_Column_Detail__c> datasourcefieldsList = BRMJoinCtlr.callDatasourceFields(dataSetObj.id);
        List<Data_Set_Column_Detail__c> expressionList = BRMJoinCtlr.generateexecuteexpression(sceRuleDetList,datasetList);
        List<JoinFieldsWrapper> fieldswrapperList = BRMJoinCtlr.CreateColumnWrapper(sceRuleInsDetObj.Id,dataSetObj.id,dataSetObj1.id);
        List<BR_JoinRule__c> businessRuleList = BRMJoinCtlr.createJoinBusinessRule(busRuleObjCom.Name,sceRuleInsDetObj.Id,scenarioObj.id,joinfieldswrapListserialized,JSON.serialize(joinExpList),joinrulecondobj.JoinType__c);
        List<BR_JoinRule__c> joinValuesList = BRMJoinCtlr.saveJoinValues(joinfieldswrapListserialized,JSON.serialize(joinExpList),busRuleObjCom.Id,scenarioObj.id,joinrulecondobj.JoinType__c,sceRuleInsDetObj.Id);

        JoinExpressionWrapper initvaluesobj = BRMJoinCtlr.getinitializedValues(busRuleObjCom.Id,dataSetObj.Id,dataSetObj1.Id);
        List<JoinFieldsWrapper> updatedfieldsList = BRMJoinCtlr.getupdatedfields(sceRuleInsDetObj.Id);
        List<String> joinValuesList1 = BRMJoinCtlr.saveUpdatedValues(joinfieldswrapListserialized,JSON.serialize(joinExpList),busRuleObjCom.Id,scenarioObj.id,joinrulecondobj.JoinType__c,sceRuleInsDetObj.Id,busRuleObjCom.Name,whereClaus);
     
        Test.stopTest();
    }
   
}
