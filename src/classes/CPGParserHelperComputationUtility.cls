public class CPGParserHelperComputationUtility {
    static List<List_Of_Operators__mdt> lstOperators; 
    static List<string> lstExecuteExpression;
    static List<string> lstSplitExpression;
    static integer ctr=0;
    static boolean isCustomFunction;
    static string dbFunction;
    static Set<string> setLogicalOperators=new Set<string>{'>','<','>=','<=','='} ;
    public static string GetExecuteExpression(string expression){
        ctr=0;
        lstSplitExpression=new List<string>();
        lstSplitExpression=expression.split('~');
        lstExecuteExpression=new List<string>();
        List<string> lstTempExecExpr=new List<string>();
        lstOperators= CPGUtility.getOperatorTypeValues();
        string operator='';
        boolean isOperator=false;
        system.debug('lstOperators---->'+lstOperators);
        system.debug('lstSplitExpression---->'+lstSplitExpression);
        
       
       while(ctr<lstSplitExpression.size()){
          
           if(!isFunction(lstSplitExpression[ctr])){
               /*if(!isOperator){
                    lstExecuteExpression.add(lstSplitExpression[ctr]);
               } 
               else{
                   
               }*/
              lstExecuteExpression.add(lstSplitExpression[ctr]);
               ++ctr;
           }
           else{
               if(InsertExpressionForFunction(lstExecuteExpression)){
                   continue;
               }
           }
        } 
        System.debug(string.join(lstExecuteExpression, ''));
        ctr=0;
        string strExecExpr= string.join(lstExecuteExpression, '');
       // return strExecExpr.replace('\'','\'\'');
         return strExecExpr;
       
    } 
    
    static boolean InsertExpressionForFunction(List<string> lstExpression){
        system.debug('isCustomFunction-->'+isCustomFunction);
        if(!isCustomFunction){
            InsertExpressionForNormalFunction(lstExpression);
        }
        else{
             InsertExpressionForCustomFunction(lstExpression);  
        } 
        ++ctr; 
        return true;
    }
    
    static void InsertExpressionForCustomFunction(List<string> lstExpression){
        if (lstSplitExpression[ctr].equalsIgnoreCase('Or')){
            system.debug('Or');
            lstExpression.addAll(CreateExpressionForOrAnd('or'));
        }
        else if (lstSplitExpression[ctr].equalsIgnoreCase('And')){
            system.debug('And');
            lstExpression.addAll(CreateExpressionForOrAnd('and'));
        }
        else if (lstSplitExpression[ctr].equalsIgnoreCase('Average')){
           lstExpression.addAll(CreateExpressionForAverage());
        }
        else if (lstSplitExpression[ctr].equalsIgnoreCase('Between')){
            lstExpression.addAll(CreateExpressionForBetween());
        }
    }
    
    
    
    static List<String> CreateExpressionForOrAnd(string condition){
        integer commaCtr=0;
        List<string> lstOr=new List<string>{'cast(('};
        ++ctr;//Skip function
        ++ctr;//Skip Opening bracket
        while(ctr<lstSplitExpression.size() && lstSplitExpression[ctr]!=')'){
            //System.debug('lstSplitExpression[ctr]1-->'+lstSplitExpression[ctr]);
           if(!isFunction(lstSplitExpression[ctr])){
               system.debug('ctr Updated inside-->'+ctr); 
                if(lstSplitExpression[ctr+1]==',' || lstSplitExpression[ctr+1]==')'){
                     
                    lstOr.add(lstSplitExpression[ctr]);
                    ++ctr;//Skip current
                    if(lstSplitExpression[ctr]==','){
                       lstOr.add(' '+condition+' ');
                       ++ctr; 
                    }
                    system.debug('moved ctr-->'+ctr);
                   // ++ctr;//Skip comma or closing bracket
                   
               }
               else if (lstSplitExpression[ctr]!=')'){
                   
                   lstOr.add('(');
                   while(ctr<lstSplitExpression.size() && (lstSplitExpression[ctr]!=',' && lstSplitExpression[ctr]!=')' ) ){
                       
                       lstOr.add(lstSplitExpression[ctr]);
                       ++ctr;
                   }
                   
                   if(ctr<lstSplitExpression.size() && (lstSplitExpression[ctr]==',' || lstSplitExpression[ctr]==')')){
                      // System.debug('lstSplitExpression[ctr]-->'+lstSplitExpression[ctr]);
                       lstOr.add(')');
                        if(lstSplitExpression[ctr]==','){
                           lstOr.add(' '+condition+' ');
                            ++ctr;
                        }
                        //++ctr;//Skip comma or closing bracket
                       
                       
                   }
               }
           }
           else{
               System.debug('function');
                   lstOr.add('(');
                   if(InsertExpressionForFunction(lstOr)){
                     system.debug('ctr Updated after function returned-->'+ctr);  
                     lstOr.add(')');
                     //++ctr;//skip current; 
                      if(lstSplitExpression[ctr]==','){
                         lstOr.add(' '+condition+' ');
                         ++ctr; 
                       }  
                     // ++ctr;//skip comma or closing bracket; 
                     //continue;
                   }
                  
                
            } 
           System.debug('lstOr-->'+lstOr);
          }
        lstOr.add(') as integer )');
       // ++ctr;
        system.debug('final ctr -->'+ctr);
      return lstOr;
        
        
    }
    
    static List<string> CreateExpressionForAverage(){
        integer itemCtr=0;
        List<string> lstAverage=new List<string>{'(('};
        ++ctr;//Skip function
        ++ctr;//Skip Opening bracket
        while(ctr<lstSplitExpression.size() && lstSplitExpression[ctr]!=')'){
           if(!isFunction(lstSplitExpression[ctr])){
                if(lstSplitExpression[ctr+1]==',' || lstSplitExpression[ctr+1]==')'){
                    lstAverage.add(lstSplitExpression[ctr]);
                    lstAverage.add('+');
                    ++ctr;//Skip current
                    ++ctr;//Skip comma
                    
                }
               else{
                   lstAverage.add('(');
                   while(ctr<lstSplitExpression.size() && (lstSplitExpression[ctr]!=',' && lstSplitExpression[ctr]!=')' ) ){
                       lstAverage.add(lstSplitExpression[ctr]);
                       ++ctr;
                   }
                   if(ctr<lstSplitExpression.size() && (lstSplitExpression[ctr]==',' || lstSplitExpression[ctr]==')')){
                       lstAverage.add(')');
                       lstAverage.add('+');
                       ++ctr;//Skip comma
                       
                   }
               }
              
               ++itemCtr;
                
            
           }
            else{
                  
                   lstAverage.add('(');
                   if(InsertExpressionForFunction(lstAverage)){
                     lstAverage.add(')');
                     lstAverage.add('+');
                     ++ctr;//skip current;  
                     ++itemCtr;
                     continue;
                   }
                  
                
            } 
        }
        lstAverage.remove(lstAverage.size()-1);
        lstAverage.add(')');
        lstAverage.add('/');
        lstAverage.add(String.valueOf(itemCtr));
        lstAverage.add(')');
        ++ctr;
        
     return lstAverage;   
    }
    static List<string> CreateExpressionForBetween(){
        integer commaCtr=0;
        List<string> lstBetween=new List<string>{'cast(('};
        ++ctr;//Skip function
        ++ctr;//Skip Opening bracket
        while(ctr<lstSplitExpression.size() && lstSplitExpression[ctr]!=')'){
            System.debug('lstSplitExpression[ctr]1-->'+lstSplitExpression[ctr]);
           if(!isFunction(lstSplitExpression[ctr])){
                if(lstSplitExpression[ctr+1]==',' || lstSplitExpression[ctr+1]==')'){
                    lstBetween.add(lstSplitExpression[ctr]);
                    ++ctr;//Skip current
                    
                    if(lstSplitExpression[ctr]==','){
                         ++commaCtr;
                         ++ctr; //skip comma
                    }
                   // ++ctr;//Skip comma or closing bracket
                   
               }
               else{
                   
                   lstBetween.add('(');
                   while(ctr<lstSplitExpression.size() && (lstSplitExpression[ctr]!=',' && lstSplitExpression[ctr]!=')' ) ){
                       
                       lstBetween.add(lstSplitExpression[ctr]);
                       ++ctr;
                   }
                   
                   if(ctr<lstSplitExpression.size() && (lstSplitExpression[ctr]==',' || lstSplitExpression[ctr]==')')){
                      // System.debug('lstSplitExpression[ctr]-->'+lstSplitExpression[ctr]);
                       lstBetween.add(')');
                      
                       if(lstSplitExpression[ctr]==','){
                           ++commaCtr;
                            ++ctr;//Skip comma 
                       }
                        //++ctr;//Skip comma or closing bracket
                       
                       
                   }
               }
           }
           else{
               System.debug('function');
                   lstBetween.add('(');
                   if(InsertExpressionForFunction(lstBetween)){
                     lstBetween.add(')');
                     if(lstSplitExpression[ctr]==','){
                           ++commaCtr;
                           ++ctr;//skip comma; 
                      } 
                     // ++ctr;//skip current; 
                     //continue;
                   }
                  
                
            } 
           if(commaCtr==1){
                   lstBetween.add(' between ');
               }
           else if(commaCtr==2){
                   lstBetween.add(' and ');
                   commaCtr=0;
               }
           
        }
        lstBetween.add(') as integer )');
        //++ctr;
      return lstBetween;
        
    }
    static void InsertExpressionForNormalFunction(List<string> lstExpression){
       lstExpression.add(dbFunction);
       ++ctr; 
        while(lstSplitExpression[ctr]!=')') {
            system.debug('InsertExpressionForNormalFunction-->'+lstSplitExpression[ctr]);
            if(!isFunction(lstSplitExpression[ctr])){
                lstExpression.add(lstSplitExpression[ctr]);
                 ++ctr;
            }
            else{
                
                if(InsertExpressionForFunction(lstExpression)){
                    continue;
                }
            }
           
        }
       lstExpression.add(lstSplitExpression[ctr]);
       
    }
    
    static boolean isFunction(string item){
        boolean isFunction=false;
        for(List_Of_Operators__mdt op:lstOperators){
            if(op.isFunction__c==true && op.Operator__c ==item){
                isFunction=true;
                isCustomFunction=op.IsCustom__c;
                dbFunction=op.DbFunction_Name__c;
                break;
            }
        }
        system.debug('isFunction-->'+isFunction);
            
        return isFunction;
    }
   

}