public with sharing class CPGRunLogCtrl {
    @AuraEnabled
    public List<ComponentWrapper> CPGRunLog ;
    @AuraEnabled
    public ErrorWrapper Error ;
    
    @AuraEnabled     
    public static CPGRunLogCtrl  getCalloutResponseContents(String url,String scenarioId) {
 
     
       system.debug('url:: '+url);
       //SSL CHANGES
       Map<String,String> urlParams = new Map<String,String>();
       urlParams.put('serviceUrl',url);

       system.debug('scenarioId:: '+scenarioId);
         list<ETL_Config__c> lstETLInfo = getETLConfigByName(CPGConstants.talend_Instance);

        String pg_username,pg_password,pg_host,dbName;
        if(lstETLInfo != null && lstETLInfo.size() > 0)
        {
            pg_username = lstETLInfo[0].BR_PG_UserName__c;
            pg_password = lstETLInfo[0].BR_PG_Password__c;
            pg_host = lstETLInfo[0].BR_PG_Host__c;
            dbName = lstETLInfo[0].BR_PG_Database__c;
        }
        String func_Name = 'CGPRunLog';

        //url = url+'?pg_username='+pg_username;
        //url = url+'&pg_password='+pg_password;
        //url = url+'&pg_host='+pg_host;
        //url = url+'&db_name='+dbName;
        //url = url+'&functionName='+func_Name;
        //url = url+'&scenarioId='+scenarioId;
         
        //system.debug('url::::'+url);
        urlParams.put('pg_username',pg_username);
        urlParams.put('pg_password',pg_password);
        urlParams.put('pg_host',pg_host);
        urlParams.put('db_name',dbName);
        urlParams.put('functionName',func_Name);
        urlParams.put('scenarioId',scenarioId);

        //SSL CHANGES
        //Http h = new Http();
        //HttpRequest req = new HttpRequest();
        //req.setEndpoint(url);
        //req.setMethod('GET');
        //req.setHeader('Content-Type', 'application/json');
        //req.setHeader('Accept','application/json');
        //HttpResponse res = h.send(req);
        HttpResponse res = SalesiqUtility.getHTTPResponse(urlParams.get('serviceUrl'),urlParams,'POST',120000);

        System.debug('response:--> ' + res.getBody());
 
        // Deserialize the JSON string into collections of primitive data types.
       // Map < String,Object > resultsMap = (Map <String, Object> ) JSON.deserializeUntyped(res.getBody());
       // system.debug('resultsMap-->' + resultsMap);

       // return resultsMap;







        // Instantiate a new http object
     /*    Http h = new Http();
 
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        req.setEndpoint(url);
        req.setMethod('GET');
        //String jsonRecieved = '{"CPGRunLog": [{"name": "Inclusion / Exclusion flags and Other Fields","status": "Success","date": "2017-11-17","fields": [{"name": "GATTEX CALL PLAN ELIGIBLE FLAG","status": "Success","date": "2017-11-17"}]}, {"name": "Call Balancing","status": "Success","date": "2017-11-17","fields": [{"name": "CCBM-3903","status": null,"date": "2017-11-17"}, {"name": "CCBM-3903","status": null,"date": "2017-11-17"}]},{"final_status":"Success"}]}';
       // String receivedJson = '{"CPGRunLog":[{"name":"Final Call Plan","status":"Success","date":"2018-03-26 13:33:10","fields":[{"name":"Exclusion / Inclusion Criteria","value":"376774","type":"input"},{"name":"Call Balancing","value":"0","type":"output"}]} ,{"name":"Guard Rails","status":"Success","date":"2018-03-26 13:33:10","fields":[{"name":"Call Plan Summary","value":"0","type":"input"}]} ,{"name":"Call Balancing","status":"Success","date":"2018-03-26 13:33:10","fields":[{"name":"Final Call Plan","value":"0","type":"input"},{"name":"Call Plan Summary","value":"0","type":"output"}]} ,{"name":"Customer Universe Starting Point","status":"Success","date":"2018-03-26 13:33:10","fields":[{"name":"Inclusion / Exclusion flags and Other Fields","value":"376774","type":"output"}]} ,{"name":"Aggregation","status":"Success","date":"2018-03-26 13:33:10","fields":[{"name":"Final Call Plan","value":"0","type":"input"},{"name":"Call Plan Summary","value":"0","type":"output"}]} ,{"name":"Call Plan Summary","status":"Success","date":"2018-03-26 13:33:10","fields":[{"name":"Aggregation","value":"0","type":"input"},{"name":"Call Balancing","value":"0","type":"input"},{"name":"Guard Rails","value":"0","type":"output"}]} ,{"name":"Inclusion / Exclusion flags and Other Fields","status":"Success","date":"2018-03-26 13:33:10","fields":[{"name":"Customer Universe Starting Point","value":"376774","type":"input"}]} ,{"name":"Exclusion / Inclusion Criteria","status":"Error Occured","date":"2018-03-26 13:33:10","fields":[{"name":"Customer Universe Starting Point","value":"376774","type":"input"},{"name":"Final Call Plan","value":"Failed","type":"output"},{"name":"cust_id","value":"Error Occured","type":"2018-03-26 13:33:12"}]} ,{"name":"Frequency Allocation","status":"Success","date":"2018-03-26 13:33:10","fields":[{"name":"Final Call Plan","value":"0","type":"input"}]} ,{"final_status":"Error Occured"}], "Error":}';
       // if(!Test.isRunningTest()){  
        res = h.send(req); 
        */                                    
        if(res.getstatusCode() == 200 && res.getbody() != null)
        {
            system.debug('result-->' + res.getbody());    
            CPGRunLogCtrl clsObj = (CPGRunLogCtrl)JSON.deserialize((res.getbody()).replaceAll('date', 'callPlanDate').replaceAll('value','status') , CPGRunLogCtrl.class);

            list<Scenario_Rule_Instance_Details__c> allScnRuleInstance = [SELECT LastModifiedDate,Name FROM Scenario_Rule_Instance_Details__c WHERE Scenario_Id__c =: scenarioId];


            for (Scenario_Rule_Instance_Details__c srid : allScnRuleInstance)
            {
                for (ComponentWrapper cw : clsObj.CPGRunLog)
                {
                    if (cw.Name == srid.Name)
                    {
                        cw.callPlanDate = srid.LastModifiedDate.format();
                    }
                }
            }
            return clsObj;
        }
        else
        {
            return null;
        } 
    }
     

    public static List<ETL_Config__c> getETLConfigByName(string etlConfigName){
        List<ETL_Config__c> etlConfigList;
            etlConfigList = [SELECT BDT_DataSet_Id__c,BR_Data_Object_Id__c,BR_PG_Database__c,BR_PG_Host__c,BR_PG_Password__c,BR_PG_Port__c,BR_PG_Schema__c,BR_PG_UserName__c,BR_Secret_Key__c,CreatedById,CreatedDate,End_Point__c,ESRI_Password__c,ESRI_UserName__c,GI_Dataset_Id__c,Id,Name,S3_Access_Key__c,S3_Bucket__c,S3_Filename__c,S3_Key_Name__c,S3_Security_Token__c,Server_Type__c,SFTP_Folder__c,SFTP_Host__c,SFTP_Password__c,SFTP_Port__c,SFTP_Username__c,SF_Password__c,SF_UserName__c FROM ETL_Config__c where name =:etlConfigName limit 1];
        return etlConfigList;
    }
     
    @AuraEnabled
    public static List<Scenario_Rule_Instance_Details__c> getScenarioID(String ScenarioRuleInstanceId){
     
       List<Scenario_Rule_Instance_Details__c> sceID = [SELECT Id, Scenario_Id__c FROM Scenario_Rule_Instance_Details__c where id=:ScenarioRuleInstanceId];
       return sceID;
    }
    @AuraEnabled
    public static String getDBDetails(){
      
        List<ETL_Config__c> dbDetail = [SELECT Id, BR_PG_Database__c from ETL_Config__c where Name ='BRMS'];
        return dbDetail[0].BR_PG_Database__c;

    }
    @AuraEnabled
    public static List<String> getInstanceNameAndURL() {
       
       List<String> lstInstanceNameAndURL = new List<String>();
        string cpgInstanceName =  CPGUtility.getBRMSConfigValues('BRDatabaseName');
        lstInstanceNameAndURL.add(cpgInstanceName);
        // string cpgInstanceURL =  CPGUtility.getBRMSConfigValues('BRRunLogURL');
        // lstInstanceNameAndURL.add(cpgInstanceURL);
        string cpgInstanceURL =  CPGUtility.getBRMSConfigValues('BRUrl');
        lstInstanceNameAndURL.add(cpgInstanceURL);

        return lstInstanceNameAndURL;
    }
    
    @AuraEnabled
    public static List<String> getformatedNumbers(List<String> numberVal) {
         system.debug('numberVal'+numberVal);
         system.debug('numberVal'+numberVal.size());

         List<String> formattedNumbers = new List<String>();
         for(Integer ctr=0; ctr<numberVal.size();ctr++){
           //system.debug('numberVal inside loop'+numberVal[ctr]);
           //system.debug('formatted number' + Integer.ValueOf(numberVal[ctr]).format());
           //String randomval = '23.554455546';
           //Decimal valCheck = decimal.ValueOf(randomval);
           Decimal val = decimal.ValueOf(numberVal[ctr]);

           Decimal rounded = val.setScale(5);
           system.debug('VALUE'+ rounded);
           system.debug('FORMATED VALUE'+ rounded.format());
           formattedNumbers.add(rounded.format());
          }
        // String num = numberVal.format();
         system.debug(formattedNumbers);
         return formattedNumbers;
     }

    public class ComponentWrapper{
        @AuraEnabled
        public string Name{get;set;}
        @AuraEnabled
        public string Status{get;set;}
        @AuraEnabled
        public string callPlanDate{get;set;}
        @AuraEnabled
        public string final_status{get;set;}
        @AuraEnabled
        public Integer inputcount{get;set;}
        @AuraEnabled
        public Integer outputcount{get;set;}
        @AuraEnabled
        public List<FieldsWrapper> Fields{get;set;}
        public ComponentWrapper(string Name, string Status, string callPlanDate,List<FieldsWrapper> Fields, string finalStatus, Integer inputcount, Integer outputcount){
            this.Name = Name;
            this.Status = Status;
            this.callPlanDate = callPlanDate;
            this.Fields = Fields;
            this.final_status = finalStatus;
            this.inputcount = inputcount;
            this.outputcount = outputcount;
        }
    }
    public class FieldsWrapper{
        @AuraEnabled
        public string Name{get;set;}
        @AuraEnabled
        public string Status{get;set;}
        @AuraEnabled
        public string callPlanDate{get;set;}
        @AuraEnabled
        public string type{get;set;}
        public FieldsWrapper(string Name, string Status, string callPlanDate, string type){
            this.Name = Name;
            this.Status = Status;
            this.callPlanDate = callPlanDate;
            this.type = type;
        }
    }
    
     public class ErrorWrapper{
        @AuraEnabled
        public string proc_name{get;set;}
        @AuraEnabled
        public string error_name{get;set;}
         
        public ErrorWrapper(string proc_name, string error_name){
            this.proc_name = proc_name;
            this.error_name = error_name;
        }
     }
}