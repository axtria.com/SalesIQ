@isTest
private class TestWorkspaceTrigger {

    static testMethod void testDeleteWorkspace() {

        
        Organization_Master__c org = CallPlanTestDataFactory.createOrganizationMasterRecord();
        insert org;
        Country__c country = CallPlanTestDataFactory.createCountryMasterRecord(org.id);
        insert country;
    	
    	Workspace__c workspc = new Workspace__C();
    	workspc.Name  = 'My Workspace';
    	workspc.Workspace_Description__c = 'TestWorkspace';
    	workspc.Workspace_Start_Date__c = Date.Today();
    	workspc.Workspace_End_Date__c = Date.Today() + 100;
        workspc.Country__c = country.id;
    	insert workspc;
    	
    	Scenario__c sce = new Scenario__c();    	
    	sce.Workspace__c = workspc.id;
    	sce.Effective_Start_Date__c = Date.Today() + 5;
    	sce.Effective_End_Date__c   = Date.Today() + 50;
    	insert sce;
    	
    	test.StartTest();
    	try{
    	   delete workspc;
    	}catch(Exception e){
    	   System.Assert(e.getMessage().contains(system.Label.Workspace_Multiple_Scenarios));
    	}
    	test.StopTest();
        
    }

    static testMethod void testInsertWorkspace() {

        Organization_Master__c org = CallPlanTestDataFactory.createOrganizationMasterRecord();
        insert org;
        Country__c country = CallPlanTestDataFactory.createCountryMasterRecord(org.id);
        insert country;

        User_Persistence__c userPersistance = new User_Persistence__c();
        userPersistance.Workspace_Country__c = country.id;
        userPersistance.Default_Country__c = null;
        userPersistance.User__c = UserInfo.getUserId();
        insert userPersistance;
        
        Workspace__c workspc = new Workspace__C();
        workspc.Name  = 'My Workspace';
        workspc.Workspace_Description__c = 'TestWorkspace';
        workspc.Workspace_Start_Date__c = Date.Today();
        workspc.Workspace_End_Date__c = Date.Today() + 100;
        workspc.Country__c = country.id;
        insert workspc;
        
        test.StartTest();
        try{
            Workspace__c duplicateWorkspace = new Workspace__C();
            duplicateWorkspace.Name  = 'My Workspace';
            duplicateWorkspace.Workspace_Description__c = 'TestWorkspace';
            duplicateWorkspace.Workspace_Start_Date__c = Date.Today();
            duplicateWorkspace.Workspace_End_Date__c = Date.Today() + 100;
            duplicateWorkspace.Country__c = country.id;
            insert duplicateWorkspace;
        }catch(Exception e){
           System.Assert(e.getMessage().contains(system.Label.Workspace_Duplicate_Name));
        }
        test.StopTest();
        
    }
}