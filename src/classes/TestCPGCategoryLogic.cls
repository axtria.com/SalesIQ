/*
Author : Ankit Manendra
Description : Test class for CPGCategoryLogic
Date : 2 Novomber, 2017

*/

@isTest
private class TestCPGCategoryLogic{
    @isTest static void cpgCatLogicTest() {
    
    Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj;
        
        Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        insert scenarioObj;
        
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj;
        
         Scenario_Rule_Instance_Details__c sceRuleInsDetObj1 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj1.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj1.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj1;
        
        Data_Set_Rule_Map__c dataSetRuleObj = new Data_Set_Rule_Map__c();
        dataSetRuleObj.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj.ds_type__c='I';
        insert dataSetRuleObj;
        
        system.debug('==dataSetRuleObj===='+dataSetRuleObj);
        
        ComponentTypeMaster__c ComTypeMasObj = new ComponentTypeMaster__c();
        ComTypeMasObj.Name='Call Balancing';
        ComTypeMasObj.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj.DisplayIcon__c='add';
        ComTypeMasObj.DisplayIconClass__c='window information';
        insert ComTypeMasObj;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj.Name='Aggregate';
        busRuleTypeMasObj.Component_Type__c=ComTypeMasObj.id;
        insert busRuleTypeMasObj;
        
        ComponentTypeMaster__c ComTypeMasObj1 = new ComponentTypeMaster__c();
        ComTypeMasObj1.Name='Aggregation';
        ComTypeMasObj1.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj1.DisplayIcon__c='add';
        ComTypeMasObj1.DisplayIconClass__c='window information';
        insert ComTypeMasObj1;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj1 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj1.Name='Rank';
        busRuleTypeMasObj1.Component_Type__c=ComTypeMasObj1.id;
        insert busRuleTypeMasObj1;
        
        Business_Rules__c busRuleObj = new Business_Rules__c();
        busRuleObj.Name='Rank rule 278';
        busRuleObj.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObj.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj.Aggregate_Level__c='Test';
        busRuleObj.customer_prioritization__c='Test';
        busRuleObj.Scenario__c=scenarioObj.id;
        insert busRuleObj;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj.Param_Name__c='derived field 278';
        busRuleFieldObj.Business_Rule__c=busRuleObj.id;
        busRuleFieldObj.Param_Data_Type__c='Numeric';
        busRuleFieldObj.Param_Type__c='Derived';
        busRuleFieldObj.IO_Flag__c='O';
        
        insert busRuleFieldObj;
         
        Computation_Rule_Detail__c compRuleDetObj = new Computation_Rule_Detail__c();
        compRuleDetObj.Business_Rules__c=busRuleObj.id;
        compRuleDetObj.Data_Type__c='Numeric';
        compRuleDetObj.Derived_field_name__c='FIELD 123';
        
        insert compRuleDetObj;
        
        Data_Set__c dataSetObj = new Data_Set__c();
        dataSetObj.Name = 'Call Plan Summary';
        insert dataSetObj;
        
        Data_Set_Column_Detail__c dataSetCol = new Data_Set_Column_Detail__c();
        dataSetCol.dataset_id__c=dataSetObj.id;
        dataSetCol.variable_type__c='Derived';
        dataSetCol.ds_col_name__c='colTest';
        dataSetCol.datatype__c='Numeric';
        insert dataSetCol;
        
        list<Data_Set_Column_Detail__c> dataSetColDetList = new list<Data_Set_Column_Detail__c>();
        dataSetColDetList.add(dataSetCol);
        
        Category_Rule_Entity_Detail__c catRuleEntDet = new Category_Rule_Entity_Detail__c();
        catRuleEntDet.Entity_Type__c='Block';
        catRuleEntDet.Business_Rule__c=busRuleObj.id;
        catRuleEntDet.Priority__c='1';
        insert catRuleEntDet;
        
        Category_Derived_Variable_Metadata__c catDerMetObj = new Category_Derived_Variable_Metadata__c();
        catDerMetObj.Field_Name__c='TestCatDer';
        catDerMetObj.Business_Rule_Id__c=busRuleObj.id;
        insert catDerMetObj;
        
        Category_Rule_Entity_Detail__c catRuleEntDet1 = new Category_Rule_Entity_Detail__c();
        catRuleEntDet1.Entity_Type__c='Group';
        catRuleEntDet1.Business_Rule__c=busRuleObj.id;
        catRuleEntDet1.Priority__c='1';
        catRuleEntDet1.Parent_Entity__c=catRuleEntDet.id;
        insert catRuleEntDet1;
        
        Category_Rule_Output_Detail__c catRuleOutDet = new Category_Rule_Output_Detail__c();
        catRuleOutDet.Category_Rule_Entity_Detail__c=catRuleEntDet.id;
        catRuleOutDet.Derived__c='TestCatDer';
        insert catRuleOutDet;
        
        Category_Rule_Criteria_Detail__c catRuleCriDet = new Category_Rule_Criteria_Detail__c();
        catRuleCriDet.FieldDataType__c='Numeric';
        catRuleCriDet.Sequence__c='1';
        insert catRuleCriDet;
        
        String[] stringArr = CPGCategoryLogic.getDataOptions('Text');
        String[] stringArr1 = CPGCategoryLogic.getOperators('Add');
        BRRuleDetails ruleDetWrap = new BRRuleDetails();
        BRRuleDetails cpgRule = CPGCategoryLogic.getRuleName(sceRuleInsDetObj.id,busRuleObj.id);
        string lstCriteria='[{"categName":"xx","dataType":"Numeric","isDropdown":false,"selValue":"1","valueType":"Continuous"}]';
        //string wrapperData= '[{"criteriaLogic":"(1)","criteriaSeq":1,"lstWrapGroup":[{"lstNestedGroup":[],"lstwrapCriteria":[{"criName":"-","criNameOptions":["-"],"fieldName":[{"dataType":"Numeric","dsColName":"gattexlead_flag","tblColName":"gattexlead_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"SHIRE_ID","tblColName":"shire_id","variableType":"Source"},{"dataType":"Text","dsColName":"Master tier","tblColName":"master_tier","variableType":"Source"},{"dataType":"Numeric","dsColName":"callactivity_flag","tblColName":"callactivity_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"REMS_FLG","tblColName":"rems_flg","variableType":"Source"},{"dataType":"Text","dsColName":"ZIP","tblColName":"zip","variableType":"Source"},{"dataType":"Text","dsColName":"SPECIALTY_CODE","tblColName":"specialty_code","variableType":"Source"},{"dataType":"Text","dsColName":"PROF_TYPE","tblColName":"prof_type","variableType":"Source"},{"dataType":"Text","dsColName":"PROF_SUBTYPE","tblColName":"prof_subtype","variableType":"Source"},{"dataType":"Text","dsColName":"PROF_STATUS","tblColName":"prof_status","variableType":"Source"},{"dataType":"Text","dsColName":"SF_EXCLUSION","tblColName":"sf_exclusion","variableType":"Source"},{"dataType":"Numeric","dsColName":"NO_SAMPLE","tblColName":"no_sample","variableType":"Source"},{"dataType":"Numeric","dsColName":"NO_ACCESS","tblColName":"no_access","variableType":"Source"},{"dataType":"Numeric","dsColName":"HR_EXCLUSION","tblColName":"hr_exclusion","variableType":"Source"},{"dataType":"Numeric","dsColName":"RUL","tblColName":"rul","variableType":"Source"},{"dataType":"Numeric","dsColName":"Gattex_XS_Final","tblColName":"gattex_xs_final","variableType":"Source"},{"dataType":"Numeric","dsColName":"Gattex_XT","tblColName":"gattex_xt","variableType":"Source"},{"dataType":"Text","dsColName":"territory_ID","tblColName":"territory_id","variableType":"Source"},{"dataType":"Text","dsColName":"GATTEX CALL PLAN ELIGIBLE FLAG","tblColName":"gattex_call_plan_eligible_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"targettype_flag","tblColName":"targettype_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"comp_name","tblColName":"comp_name","variableType":"Derived"},{"dataType":"Numeric","dsColName":"frcomp1","tblColName":"frcomp1","variableType":"Derived"},{"dataType":"Numeric","dsColName":"fff1","tblColName":"fff1","variableType":"Derived"},{"dataType":"Numeric","dsColName":"f1cat","tblColName":"f1cat","variableType":"Derived"},{"dataType":"Numeric","dsColName":"f1","tblColName":"f1","variableType":"Derived"}],"fieldType":"","fieldVal":"12","opVal":["equals","not equals","in","not in","is null","is not null","less than","less or equal","greater than","greater or equal","ends with","between"],"selFieldName":"0@0@0@Numeric@gattexlead_flag@Source","selOpVal":"0@0@0@equals","seqNo":1}],"selGroupOp":"AND"}],"priority":[1],"selPriority":1,"lstOutputCriteria":[{"categName":"xx","dataType":"Numeric","isDropdown":false,"selValue":"1","valueType":"Continuous"}],"lstElseCriteria":[{"categName":"xx","dataType":"Numeric","isDropdown":false,"selValue":"","valueType":"Continuous"}]}]';
        string wrapperData= '[{"criteriaLogic":"(1)","criteriaSeq":1,"lstWrapGroup":[{"lstNestedGroup":[],"lstwrapCriteria":[{"criName":"-","criNameOptions":["-"],"fieldName":[{"dataType":"Numeric","dsColName":"SHIREID","tblColName":"shireid","variableType":"Source"},{"dataType":"Numeric","dsColName":"NOSAMPLE","tblColName":"nosample","variableType":"Source"},{"dataType":"Numeric","dsColName":"NOACCESS","tblColName":"noaccess","variableType":"Source"},{"dataType":"Numeric","dsColName":"HREXCLUSION","tblColName":"hrexclusion","variableType":"Source"}],"fieldType":"","fieldVal":"1","opVal":["equals","not equals","in","not in","is null","is not null","less than","less or equal","greater than","greater or equal","ends with","between"],"selFieldName":"0@0@0@Numeric@shireid@Source","selOpVal":"0@0@0@equals","seqNo":1}],"selGroupOp":"AND"}],"priority":[1],"selPriority":1,"lstOutputCriteria":[{"categName":"myfield1","dataType":"Numeric","isDropdown":false,"selValue":"1","valueType":"Continuous"},{"categName":"my_field2","dataType":"Text","isDropdown":true,"selValue":"a","values":["---","a","b"],"valueType":"Discrete"}],"lstElseCriteria":[{"categName":"myfield1","dataType":"Numeric","isDropdown":false,"selValue":"3","valueType":"Continuous"},{"categName":"myfield2","dataType":"Text","isDropdown":true,"selValue":"a","values":["---","a","b"],"valueType":"Discrete"}]}]';
        BROutputCriteria outputCri = CPGCategoryLogic.AddDynamicData(lstCriteria,sceRuleInsDetObj.id,'FIELD 123','Text','Discrete','Test','true');
        BROutputCriteria outputCri1 = CPGCategoryLogic.getCategoryFieldData('FIELD 123',lstCriteria);
        BRWrapBlock.ElseCriteria elseCri = CPGCategoryLogic.AddElseDataCriteria('FIELD 123','Text','Discrete','Test');
        List<Business_Rule_Type_Master__c> busRuleTypeMasList = CPGCategoryLogic.getBusinessRuleTypeMaster();
        Types_List__mdt[] typeList = CPGCategoryLogic.getTypesList();
        list<BROutputCriteria> outCriList = CPGCategoryLogic.AddOutputField('Rank');
        list<BRWrapBlock> wrapCpgList = CPGCategoryLogic.initialiseBlock(sceRuleInsDetObj.id,busRuleObj.id,'Rank');
        list<BRWrapBlock> wrapCpgList1 = CPGCategoryLogic.initialiseBlock(sceRuleInsDetObj.id,'','Rank');
        BRRuleDetails ruleDet = CPGCategoryLogic.GetRuleDetails(sceRuleInsDetObj.id);
        list<Data_Set_Rule_Map__c> dataSetRuleList = CPGCategoryLogic.getInputTableName(sceRuleInsDetObj.id);
        Id stringId = CPGCategoryLogic.SaveBusinessRule(busRuleObj.Name,sceRuleInsDetObj.id,'Rank','TestLevel','TestCustPrior',null);
        list<BROutputCriteria> outPutcatList = new list<BROutputCriteria>();
        BROutputCriteria outCri = new BROutputCriteria();
        outCri.categName='TerrTargetConstraint';
        outCri.isDropdown=true;
        outCri.dataType='Text';
        outCri.selValue='Over';
        outCri.valueType='Discreet';    
        outPutcatList.add(outCri);  
        CPGCategoryLogic.AddCategoryToFeilds(outPutcatList,sceRuleInsDetObj.id,dataSetColDetList,dataSetObj.id);
        
        BRWrapBlock.ElseCriteria elseCri1 = CPGCategoryLogic.CreateElseCriteria(catRuleOutDet,catDerMetObj);
        list<BROutputCriteria> outCriList1 = CPGCategoryLogic.DisplayCategories(busRuleObj.id);
        CPGCategoryLogic.DeleteFromDataSetColumnDetails(sceRuleInsDetObj.id,'colTest');
        list<BROutputCriteria> outCriList2= CPGCategoryLogic.deleteListFieldCategory(lstCriteria,'TerrTargetConstraint');
        BRWrapCriteria  wrapCri = CPGCategoryLogic.CreateCriteria(catRuleCriDet,dataSetObj.id,1,1,1);
        list<BRWrapBlock> wrapBlockList = new list<BRWrapBlock>();
        list<Data_Set_Column_Detail__c> dataSelCat = CPGCategoryLogic.getColumnsDataByScenarioInstId(sceRuleInsDetObj.id);
        map<Integer,Category_Rule_Entity_Detail__c> mapInCat = new map<Integer,Category_Rule_Entity_Detail__c>();
        boolean bool1 = CPGCategoryLogic.ValidateCategory('TerrTargetConstraint',sceRuleInsDetObj.id);
        BRWrapBlock wrapBlockList1=CPGCategoryLogic.AddNewBlock(lstCriteria,dataSetObj.id);
        BRWrapBlock.WrapGroup wrapGrp =CPGCategoryLogic.AddAGroup(dataSetObj.id);
        BRWrapCriteria wrapCriteria =CPGCategoryLogic.AddCriteria(dataSetObj.id);

        Test.startTest();
        string retStr = CPGCategoryLogic.SaveCheck(lstCriteria,wrapperData.replaceAll('""','"\"'),busRuleObj.name,sceRuleInsDetObj.id,busRuleObj.id,dataSetObj.id,'Rank','Test','Test',lstCriteria);
        Test.stopTest();
    }
}