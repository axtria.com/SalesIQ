@isTest
private class TestSalesIQLogger {
    @isTest
    static void testMethod1() {
        List<SalesIQ_Logger__c> logs = [select Id, Line_Number__c, Message__c, Exception_Type__c from SalesIQ_Logger__c] ;
        System.assertEquals(0, logs.size());

    	SalesIQLogger.logCatchedException(new DmlException(), true, SalesIQLogger.ALIGNMENT_MODULE);
    	SalesIQLogger.dummyMethod();
        SalesIQLogger.dummyMethod1();
        SalesIQLogger.dummyMethod2();

    	logs = [select Id, Line_Number__c, Message__c, Exception_Type__c from SalesIQ_Logger__c] ;
    	System.assertEquals(1, logs.size());
    	System.assertEquals('System.DmlException', logs.get(0).Exception_Type__c);
    }

    @isTest
    static void testMethod2() {
        List<SalesIQ_Logger__c> logs = [select Id, Line_Number__c, Message__c, Exception_Type__c from SalesIQ_Logger__c] ;
        System.assertEquals(0, logs.size());
        
        SalesIQLogger.logCatchedException(new NullPointerException(), true, SalesIQLogger.ALIGNMENT_MODULE);
        
        logs = [select Id, Line_Number__c, Message__c, Exception_Type__c from SalesIQ_Logger__c] ;
        System.assertEquals(1, logs.size());
        System.assertEquals('System.NullPointerException', logs.get(0).Exception_Type__c);
    }

    @isTest
    static void testMethod3() {
        List<SalesIQ_Logger__c> logs = [select Id, Line_Number__c, Message__c, Exception_Type__c from SalesIQ_Logger__c] ;
        System.assertEquals(0, logs.size());

        Test.startTest();
        SalesIQLogger.logCatchedExceptionFuture(new DmlException(), true, SalesIQLogger.CALL_MODULE);
        Test.stopTest();

        logs = [select Id, Line_Number__c, Message__c, Exception_Type__c from SalesIQ_Logger__c] ;
        System.assertEquals(1, logs.size());
        System.assertEquals('System.DmlException', logs.get(0).Exception_Type__c);

    }

    @isTest
    static void testMethod4() {
        List<SalesIQ_Logger__c> logs = [select Id, Line_Number__c, Message__c, Exception_Type__c from SalesIQ_Logger__c] ;
        System.assertEquals(0, logs.size());

        Test.startTest();
        SalesIQLogger.logCatchedExceptionFuture(new NullPointerException(), true, SalesIQLogger.CALL_MODULE);
        Test.stopTest();

        logs = [select Id, Line_Number__c, Message__c, Exception_Type__c from SalesIQ_Logger__c] ;
        System.assertEquals(1, logs.size());
        System.assertEquals('System.NullPointerException', logs.get(0).Exception_Type__c);

    }

    @isTest
    static void testMethod5() {
        Exception e = new NullPointerException() ;
        SalesIQLogger.logMessage(e) ;
    }

    @isTest
    static void testMethod6() {
        List<SalesIQ_Logger__c> logs = [select Id, Line_Number__c, Message__c, Exception_Type__c from SalesIQ_Logger__c] ;
        System.assertEquals(0, logs.size());

        Test.startTest();
        SalesIQLogger.logCatchedException(new NullPointerException(), SalesIQLogger.CALL_MODULE);
        Test.stopTest();

        logs = [select Id, Line_Number__c, Message__c, Exception_Type__c from SalesIQ_Logger__c] ;
        System.assertEquals(1, logs.size());
        System.assertEquals('System.NullPointerException', logs.get(0).Exception_Type__c);
    }

    @isTest
    static void testMethod7() {
        List<SalesIQ_Logger__c> logs = [select Id, Line_Number__c, Message__c, Exception_Type__c from SalesIQ_Logger__c] ;
        System.assertEquals(0, logs.size());

        Test.startTest();
        SalesIQLogger.logErrorMessage('Null Pointer exception occured', SalesIQLogger.CALL_MODULE);
        Test.stopTest();

        logs = [select Id, Line_Number__c, Message__c, Exception_Type__c from SalesIQ_Logger__c] ;
        System.assertEquals(1, logs.size());
        System.assertEquals('Null Pointer exception occured', logs.get(0).Message__c);
    }
}