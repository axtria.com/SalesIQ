/*@author : Arun Kumar
  @date : 24 April 2018  
  @description : This batch will execute based on scheduler. This batch will getch the excluded accounts and delete those
  accounts from position account and CR accounts  
  return : void 
 */ 
 

global class ProcessAccountExclusionBatch implements Database.Batchable<sObject>, Database.AllowsCallouts,Database.Stateful{
    public final String strPositionAccountquery;
    public set<id> account;
    //below variable is used to update the Is_Exclusion_Batch_Run__c field, which will track that these excluded accounts won't be processed in next batch run
    public list<Account_Exclusion__c> tobeUpdatedExcludedAccounts;

    map<string,list<string>> mapTeamInstanceAccountNumber  = new map<string,list<string>>();   
    list<Id> CRToBeRejected = new list<Id>(); 
    
    global ProcessAccountExclusionBatch(Boolean runOnAllRecords){
        tobeUpdatedExcludedAccounts = new list<Account_Exclusion__c>();
        account = new set<id>();
        system.debug('excludedAccount before'+tobeUpdatedExcludedAccounts);
       
        account = getExcludedAccounts(runOnAllRecords);
        system.debug('account '+account);

        if(account.size()>0){
            system.debug('in if '+account);
            strPositionAccountquery='select id, account__c,account__r.name,account__r.AccountNumber,Position__r.Name, position__c, team_instance__r.Name,team_instance__r.Alignment_Type__c,team_instance__r.Team_Instance_Code__c,team_instance__r.Geography_Type_Name__r.Account_Shape_Available__c ,Metric1__c,Metric2__c,metric3__c,metric4__c,metric5__c,metric6__c,metric7__c,metric8__c,metric9__c,metric10__c from position_account__c where account__c in:account and Team_instance__r.Scenario__r.Scenario_Status__c = \'Active\' and Team_instance__r.Allow_Account_Exclusion__c=true ';
        }else{
            strPositionAccountquery = 'SELECT Id, Team_Instance__c, Position__c, Account__c FROM Position_Account__c WHERE Name = null';
        }
    }

    
    global Database.QueryLocator start(Database.BatchableContext BC){
       
        return Database.getQueryLocator(strPositionAccountquery);
               
    }
    
    global void execute(Database.BatchableContext BC,List<position_Account__c> scope){
        
        //PostProcessAccountHandler.processCIMRequestImpact(BC, scope,account);    
        //We required below maaping becasue we need to delete team instance and account from the ESRI as well
        list<Exclusion_Logger__c> lstAccountLogger = new list<Exclusion_Logger__c>();
        
        try{
            list<position_Account__c> positionAccountId = new list<position_Account__c>();
            set<id> crAccountId = new set<id>();
            set<Id> crIds = new set<Id>();
            list<CIM_Change_Request_Impact__c> lstUpdatedRequestImpact = new list<CIM_Change_Request_Impact__c>();
            map<id,position_Account__c> mapPosAccIsandPositionAccount = new map<id,position_Account__c>(); 
            map<id,list<CR_Account__c>> mapAccountCRAccount = new map<id,list<CR_Account__c>>();
            map<id,id> mapCRIdTeamInstance  = new map<id,id>();
            //below map will create a data mapping for TeamInanceCOde and their Mapped Account number. 
           
            //below will have set of positions whose metric data will be updated in CIM Position Metric Summary
            set<string> setPositions = new set<string>();
            set<string> sourcePositions = new set<string>();
            set<string> destinationPositions = new set<string>();

            list<CR_Account__c> crAccToDelete = new list<CR_Account__c>();
            
             
             
            //Getting CR_Account of selected account. 
            list<CR_Account__c> lstCRAccout = [select id, account__c, Change_Request__c,Change_Request__r.Team_Instance_id__c,Destination_Position__c,Source_Position__c from CR_Account__c where account__c in:account and Change_Request__r.Status__c='Pending' ];
            for(CR_Account__c crAcc: lstCRAccout){                 
                crAccountId.add(crAcc.id);
                mapCRIdTeamInstance.put(crAcc.Change_Request__c,crAcc.Change_Request__r.Team_Instance_id__c);
                crIds.add(crAcc.Change_Request__c);
                if(mapAccountCRAccount.containsKey(crAcc.account__c)){
                    list<CR_Account__c> tempCRAccount = mapAccountCRAccount.get(crAcc.account__c);
                    tempCRAccount.add(crAcc);
                    mapAccountCRAccount.put(crAcc.account__c, tempCRAccount);
                    
                }else{
                    mapAccountCRAccount.put(crAcc.account__c, new list<CR_Account__c>{crAcc});
                     
                }                
                
            }
             
            system.debug('mapCRIdTeamInstance: '+mapCRIdTeamInstance);

            if(mapCRIdTeamInstance.size() > 0){
                //Find out CRs if there is no accounts associated other than affiliated accounts and mark it rejected.
                
                
                for(Change_Request__c cr : [SELECT Id, (SELECT Id FROM CR_Accounts__r WHERE Account__c NOT IN : account) FROM Change_Request__c WHERE Id IN : mapCRIdTeamInstance.keyset()]){
                    if(cr.CR_Accounts__r.size() == 0){
                        cr.Status__c = SalesIQGlobalConstants.REQUEST_STATUS_REJECTED;
                        crToBeRejected.add(cr.Id);
                        crIds.remove(cr.Id);
                    }
                }
                
            }
             
             
            //Getting the CIM request Impact for all CR which are part of CR Account
            list<CIM_Change_Request_Impact__c> lstCIMReqImpact = [select id,Impact__c,Change_Request__c,CIM_Config__r.Attribute_API_Name__c,Change_Request__r.Source_Position__c, Change_Request__r.Destination_Position__c from CIM_Change_Request_Impact__c where Change_Request__c in:crIds ];
            map<id, list<CIM_Change_Request_Impact__c>> mapCRidandCIMReqImpact = new map<id, list<CIM_Change_Request_Impact__c>>();
            for(CIM_Change_Request_Impact__c cimreqImpact: lstCIMReqImpact){
                sourcePositions.add(cimreqImpact.Change_Request__r.Source_Position__c);
                destinationPositions.add(cimreqImpact.Change_Request__r.Destination_Position__c);

                system.debug('sourcePositions '+sourcePositions);
                system.debug('destinationPositions '+destinationPositions);

                if(mapCRidandCIMReqImpact.containsKey(cimreqImpact.Change_Request__c)){
                    list<CIM_Change_Request_Impact__c> temReqImpact = mapCRidandCIMReqImpact.get(cimreqImpact.Change_Request__c);
                    temReqImpact.add(cimreqImpact);
                    mapCRidandCIMReqImpact.put(cimreqImpact.Change_Request__c,temReqImpact );
                     
                }else{
                    mapCRidandCIMReqImpact.put(cimreqImpact.Change_Request__c,new list<CIM_Change_Request_Impact__c>{cimreqImpact} );
                }
            }
             
            list<CIM_Change_Request_Impact__c> tobeUpdatedCIMRequestImpact = null;
            /*below variable will be used to track that excluion logger list doest have duplicate records becasue I'm using single objlogger which will add account info from two diffrent case.
            Case 1: when there is NO CR account avalbale for excluded accounts
            Case 2: when excluded accounts have CR avalable

            so I'm preparering list<Exclusion_Logger__c> which will insert in logging object. but this list might have duplicate variable for those account
            which have CR account as well. SO I'm using loggedRecordAvailable which will track that list.add should not execute twice for those accounts which has CR account as well

            */

            Boolean loggedRecordAvailable = false;
            map<string, list<CIM_Position_Metric_Summary__c>> mapDestinationPositionAndMetricSummary = getCIMPositionMetricSummary(destinationPositions);
            system.debug('mapDestinationPositionAndMetricSummary '+mapDestinationPositionAndMetricSummary);
            list<CIM_Position_Metric_Summary__c> cimDestinationSummaryToUpdate = new list<CIM_Position_Metric_Summary__c>();
            for(position_Account__c pos: scope){
                system.debug('in loop ' +pos);
                Exclusion_Logger__c objPosAcclogger = new Exclusion_Logger__c();
                objPosAcclogger.Account__c = pos.Account__c;
                objPosAcclogger.Source_Position__c = pos.Position__c;
                objPosAcclogger.Batch_Job_Id__c = BC.getJobId();
                objPosAcclogger.Batch_Type__c='Account Exclusion';
                objPosAcclogger.Source_object__c = 'Position_Account__c';
                system.debug('in loop Position Account ' +objPosAcclogger);
                lstAccountLogger.add(objPosAcclogger);
               
                positionAccountId.add(pos);
                

                //system.debug('BC.getJobId() in loop '+BC.getJobId());

                //below logic will be used in case of deleting the accounts from ESRI. Logic is prepareing 
                //mapTeamInstanceAccountNumber only for those team Instacne whose shapes are avalable nin ESRI side

                if(pos.team_instance__r.Geography_Type_Name__r.Account_Shape_Available__c){
                    if(mapTeamInstanceAccountNumber.containsKey(pos.team_instance__r.Team_Instance_Code__c)){
                        list<string> tempAccnumber = mapTeamInstanceAccountNumber.get(pos.team_instance__r.Team_Instance_Code__c);
                        tempAccnumber.add(pos.Account__r.AccountNumber);
                        mapTeamInstanceAccountNumber.put(pos.team_instance__r.Team_Instance_Code__c,tempAccnumber);
                        
                    }else{
                        mapTeamInstanceAccountNumber.put(pos.team_instance__r.Team_Instance_Code__c,new list<string>{pos.Account__r.AccountNumber});
                    }

                }
                
                //Below CIM calculation will not work in cas of ZIP team. It will work on Account and HYbrid Team.
                //Although deletion of excluded accounts are still valid for ZIP based team
                if(pos.team_instance__r.Alignment_Type__c!='ZIP'){

                    system.debug('pos.Position__c '+pos.Position__c);
                    setPositions.add(pos.Position__c);
                    system.debug('Team TYpe '+pos.team_instance__r.Alignment_Type__c);
                
                    list<CR_Account__c> lstCRAccount = mapAccountCRAccount.get(pos.Account__c);
                    if(lstCRAccount!=null){
                        for(CR_Account__c crAccount:lstCRAccount){
                            string TeamInstance = mapCRIdTeamInstance.get(crAccount.Change_Request__c);
                            if(!string.isBlank(TeamInstance) && pos.Team_Instance__c==TeamInstance){
                                Exclusion_Logger__c objCRAccountlogger = new Exclusion_Logger__c();
                                objCRAccountlogger.Account__c = crAccount.Account__c;                                
                                objCRAccountlogger.Change_Request__c = crAccount.Change_Request__c;
                                objCRAccountlogger.Source_Position__c = crAccount.Source_Position__c;
                                objCRAccountlogger.Destination_Position__c = crAccount.Destination_Position__c;
                                objCRAccountlogger.Batch_Job_Id__c = BC.getJobId();
                                objCRAccountlogger.Batch_Type__c='Account Exclusion';
                                objCRAccountlogger.Source_object__c = 'CR_Account__c';
                                lstAccountLogger.add(objCRAccountlogger);
                                
                               
                                crAccToDelete.add(crAccount);
                                system.debug('pos team_instance__r.Name '+pos.team_instance__r.Name);
                                system.debug('pos.Account__c '+pos.Account__r.Name);
                                
                                tobeUpdatedCIMRequestImpact = new list<CIM_Change_Request_Impact__c>();
                                list<CIM_Change_Request_Impact__c> cimReqImpactSelectedCRid = mapCRidandCIMReqImpact.get(crAccount.Change_Request__c);
                                system.debug('cimReqImpactSelectedCRid before loop '+cimReqImpactSelectedCRid);
                                if(cimReqImpactSelectedCRid!=null ){
                                    for(CIM_Change_Request_Impact__c reqImpact: cimReqImpactSelectedCRid){
                                        system.debug('reqImpact '+reqImpact);
                                        //system.debug('Pre impacted val '+reqImpact.Impact__c);
                                        if(!string.isEmpty(reqImpact.CIM_Config__r.Attribute_API_Name__c)){
                                            system.debug('API name '+reqImpact.CIM_Config__r.Attribute_API_Name__c);
                                            system.debug('Pre impacted val '+reqImpact.Impact__c);
                                            decimal accMetric = double.valueOf(pos.get(reqImpact.CIM_Config__r.Attribute_API_Name__c)) ;
                                            system.debug(accMetric);
                                            decimal newval=0;
                                            if(reqImpact.Impact__c.contains('-')){
                                                newval = double.valueOf(reqImpact.Impact__c)+accMetric;
                                                system.debug('newval in - '+newval);
                                                reqImpact.Impact__c = string.valueOf(newval);
                                            }else{
                                                newval = double.valueOf(reqImpact.Impact__c)-accMetric;
                                                system.debug('newval in + '+newval);
                                                reqImpact.Impact__c = '+'+string.valueOf(newval);
                                                
                                            }
                                            system.debug('reqImpact after '+reqImpact);
                                            tobeUpdatedCIMRequestImpact.add(reqImpact);
                                        }                                    
                                    }
                                }

                                if(mapDestinationPositionAndMetricSummary!=null && mapDestinationPositionAndMetricSummary.size() > 0){
                                    list<CIM_Position_Metric_Summary__c> lstDestinationCIMSummary = mapDestinationPositionAndMetricSummary.get(crAccount.Destination_Position__c);
                                    system.debug('lstDestinationCIMSummary '+lstDestinationCIMSummary);
                                    if(lstDestinationCIMSummary!=null){
                                        for(CIM_Position_Metric_Summary__c tempDestinationSummary:lstDestinationCIMSummary){
                                            Decimal accMetricVal = double.valueOf(pos.get(tempDestinationSummary.CIM_Config__r.Attribute_API_Name__c));
                                            system.debug('accMetricVal '+accMetricVal);

                                            Decimal summaryDestinationProposedVal = double.valueOf(tempDestinationSummary.Proposed__c);
                                            system.debug('summaryDestinationProposedVal '+summaryDestinationProposedVal);

                                            summaryDestinationProposedVal = summaryDestinationProposedVal-accMetricVal;
                                            if(summaryDestinationProposedVal >0){
                                                tempDestinationSummary.Proposed__c = string.valueOf(summaryDestinationProposedVal);
                                            }
                                            system.debug('tempSummary in Destination after '+tempDestinationSummary);

                                            cimDestinationSummaryToUpdate.add(tempDestinationSummary);


                                        }

                                    }
                                    

                                }

                            }
                        }

                    }
                                 
                }                
            }

            system.debug('tobeUpdatedCIMRequestImpact '+tobeUpdatedCIMRequestImpact);

            if(tobeUpdatedCIMRequestImpact!=null && tobeUpdatedCIMRequestImpact.size() > 0){
                update tobeUpdatedCIMRequestImpact;

            }

            system.debug('cimDestinationSummaryToUpdate '+cimDestinationSummaryToUpdate);

            if(cimDestinationSummaryToUpdate!=null && cimDestinationSummaryToUpdate.size() > 0){
                update cimDestinationSummaryToUpdate;

            }            

            map<string, list<CIM_Position_Metric_Summary__c>> mapPositionAndMetricSummary = getCIMPositionMetricSummary(setPositions);
            system.debug('summary before '+mapPositionAndMetricSummary.values());

            //list<CIM_Position_Metric_Summary__c> cimSummaryToUpdate = new list<CIM_Position_Metric_Summary__c>();
            map<id,CIM_Position_Metric_Summary__c> mapCIMSummaryToUpdate = new map<id,CIM_Position_Metric_Summary__c>();
            if(mapPositionAndMetricSummary.size()>0){
                for(Position_Account__c posacc: scope){
                    
                    if(posacc.team_instance__r.Alignment_Type__c!='ZIP'){
                        system.debug('selected position in another loop '+posacc.Position__c);
                        system.debug('position Name '+posacc.Position__r.Name);
                        system.debug('Acc Name '+posacc.Account__r.Name);
                        list<CIM_Position_Metric_Summary__c> lstSummary = mapPositionAndMetricSummary.get(posacc.Position__c);                       
                        if(lstSummary!=null){

                            Boolean accountProcessedEitherOfPosition = false;

                            for(CIM_Position_Metric_Summary__c tempSummary:lstSummary){
                                system.debug('tempSummary before '+tempSummary);

                                Decimal accMetricVal = double.valueOf(posacc.get(tempSummary.CIM_Config__r.Attribute_API_Name__c));
                                system.debug('accMetricVal '+accMetricVal);

                                //This variable is used to subtract the deleted account level metric from position metric summary data
                                Decimal summaryProposedVal = double.valueOf(tempSummary.Proposed__c);
                                Decimal summaryApprovedVal = double.valueOf(tempSummary.Approved__c);

                                //While excluding Accounts, we need to update the Approved/Proposed value based on whather Accounts are part of CR Account or not.
                                //There might be chnace that some accounts are not a part of any of the CR. In that case account level metric should be deducted from Proposed and Approved 
                                //fields. To track that, I have used accountProcessedEitherOfPosition variable
                                if(sourcePositions.contains(posacc.Position__c)){
                                    accountProcessedEitherOfPosition = true;
                                    //update Approved field(with minus) value from CIM position metric summary becasue now that account is going to delete from
                                    //source position and we need to re calclulate Aprppoved value
                                    system.debug('in source posacc.Position__c '+posacc.Position__c);
                                    summaryApprovedVal = summaryApprovedVal-accMetricVal;
                                    if(summaryApprovedVal >0){
                                        tempSummary.Approved__c = string.valueOf(summaryApprovedVal);
                                    }
                                    system.debug('tempSummary in source '+tempSummary);

                                }
                                if(destinationPositions.contains(posacc.Position__c)){
                                    accountProcessedEitherOfPosition = true;
                                    //update proposed - from acc level
                                    system.debug('in destina tion posacc.Position__c '+posacc.Position__c);
                                    summaryProposedVal = summaryProposedVal-accMetricVal;
                                    if(summaryProposedVal >0){
                                        tempSummary.Proposed__c = string.valueOf(summaryProposedVal);
                                    }
                                    system.debug('tempSummary in destination '+tempSummary);

                                }
                                if(!accountProcessedEitherOfPosition){
                                    summaryApprovedVal = summaryApprovedVal-accMetricVal;
                                    summaryProposedVal = summaryProposedVal-accMetricVal;

                                    tempSummary.Approved__c = string.valueOf(summaryApprovedVal);
                                    tempSummary.Proposed__c = string.valueOf(summaryProposedVal);

                                    system.debug('tempSummary accountProcessedEitherOfPosition '+tempSummary);

                                }
                                
                                //cimSummaryToUpdate.add(tempSummary);
                                mapCIMSummaryToUpdate.put(tempSummary.id, tempSummary);
                                system.debug('tempSummary after '+tempSummary);
                                
                            }                           

                        }

                    }

                }

            }
            //system.debug('cimSummaryToUpdate '+cimSummaryToUpdate );
            system.debug('lstAccountLogger '+lstAccountLogger);

            if(lstAccountLogger.size() > 0){
                insert lstAccountLogger;

            }

             
            if(crAccToDelete.size() > 0){
                system.debug('crAccToDelete '+crAccToDelete);
                delete crAccToDelete;

            }
             

            if(mapCIMSummaryToUpdate.size()>0){
                //update cimSummaryToUpdate;
                update mapCIMSummaryToUpdate.values();
            }                         
             
            //deleting position account records
            if(positionAccountId.size() > 0){
                delete positionAccountId;
            }
                                     
             
        }catch(exception e){
            //SalesIQLogger.logCatchedException(e, true, 'processCIMRequestImpact'); 
            system.debug('error statc trace '+e.getStackTraceString());
             
        }   
    
    }    
    
    global void finish(Database.BatchableContext BC){
        try{
            system.debug('#### ProcessAccountExclusionBatch finished');   
                            
            for(Account_Exclusion__c exAccount: tobeUpdatedExcludedAccounts){
                exAccount.Is_Exclusion_Batch_Run__c = true;
            }
            
            if(mapTeamInstanceAccountNumber.size()> 0){

                Integer statusCode = deleteESRIAccountData(mapTeamInstanceAccountNumber);
                if(statusCode == 200){
                    system.debug('in if ');
                                        
                }else{
                    system.debug('in else ');
                    
                }
                //delete code end
            }
            if(tobeUpdatedExcludedAccounts.size()>0){
                update tobeUpdatedExcludedAccounts;
            }
            system.debug('#### CRs to be rejected : in finish '+crToBeRejected);
            if(crToBeRejected.size() > 0){
                //update crToBeRejected;
                SalesIQUtility.submitForRejection(crToBeRejected,System.Label.Exclusion_Rejection);
            } 

        }
        catch(exception e){
            //SalesIQLogger.logCatchedException(e, true, 'processCIMRequestImpact'); 
            system.debug('e statc trace in finish '+e.getStackTraceString());
             
        }  
        
    }    

    //runOnAllRecords will return all accounts which are in exclusion object or return detla.
    private set<id> getExcludedAccounts(Boolean runOnAllRecords){
        set<id> excludedAccount = new set<id>();
        list<Account_Exclusion__c> lstExcludedAccount =null;
        if(runOnAllRecords){
            lstExcludedAccount = [select id,account__c,Is_Exclusion_Batch_Run__c from Account_Exclusion__c where Status__c='Active' limit 50000 ];

        }else{
            lstExcludedAccount = [select id,account__c,Is_Exclusion_Batch_Run__c from Account_Exclusion__c where Status__c='Active' and Is_Exclusion_Batch_Run__c  = false limit 50000];

        }
        if(lstExcludedAccount!=null){
            for(Account_Exclusion__c accExclusion: lstExcludedAccount){
                excludedAccount.add(accExclusion.account__c);
            }
            tobeUpdatedExcludedAccounts.addAll(lstExcludedAccount);
        }
        
        return excludedAccount;
    }

    public static HttpResponse getHTTPResponse(string endPoint, string httpBody){
        Http http = new Http();
        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setEndpoint(endPoint);
        httpRequest.setMethod('POST');
        httpRequest.setHeader('X-Frame-Options', 'ALLOW-FROM');
        httpRequest.setHeader('Pragma', 'no-cache');
        httpRequest.setHeader('Cache-Control', 'no-cache');
        httpRequest.setHeader('X-XSS-Protection', '1');
        httpRequest.setHeader('x-content-type-options', 'nosniff');
        httpRequest.setHeader('SET-COOKIE', 'HttpOnly');
        httpRequest.setBody(httpBody);
         
        HTTPResponse httpResponse = new HTTPResponse();
        try {
             
            if(Test.isRunningTest()){
                ESRIServicesMockGenerator obj = new ESRIServicesMockGenerator();
                httpResponse = obj.respond(httpRequest);
            }
            else{
                httpResponse = http.send(httpRequest);
            }
                
                
        }catch(exception e){
            //SalesIQLogger.logCatchedException(e, true, 'deleteESRIAccountData'); 
            system.debug('exception is getHTTPResponse '+e.getStacktracestring());
        }
        return httpResponse;
         
    }
     
     
    public static Integer deleteESRIAccountData(map<string,list<string>> mapTeamInstanceAccountNumber){
        Integer iSucess = 0;
        try{
            string strQuery='';
            for(string strTeamInstanceCode:mapTeamInstanceAccountNumber.keyset()){
                list<string> lstAccNumber = mapTeamInstanceAccountNumber.get(strTeamInstanceCode);
                string accountNumber = '(';
                string teamIntanceCode = '\''+strTeamInstanceCode+'\'';
                for(string accNumber:lstAccNumber){
                    accountNumber += '\''+accNumber+'\',';
                }
                accountNumber = accountNumber.removeEnd(',');
                accountNumber += ')';
                
                strQuery+='(account_number+in+'+accountNumber+'+and+team_instance='+teamIntanceCode+') OR ';
                 
            }
             
            system.debug('strQuery ' +strQuery);
            strQuery = strQuery.removeEnd(' OR ');
            system.debug('after strQuery ' +strQuery);
             
            MapServerURL__c ESRIService = MapServerURL__c.getOrgDefaults();
            String endPoint = ESRIService.Service_URL__c+ESRIService.Account_Layer__c +'/query?';
            //string body = 'f=json&returnGeometry=false&returnIdsOnly=true&where=geography_name+in+'+removeZipParamValue+'+and+team_instance_name='+teamInstance;
            string body = 'f=json&returnGeometry=false&returnIdsOnly=true&where='+strQuery;
             
            system.debug('body '+body);
            
            //below method will be use to send the account which need to delete.
            //It will give the objectid of accounts which need to be delete from ESRI table
            HTTPResponse httpResponseAccountObjectId = getHTTPResponse(endPoint,body);
             
            
            if(httpResponseAccountObjectId.getStatusCode() == 200) {
                JSONParser parser = JSON.createParser(httpResponseAccountObjectId.getBody());
              
                System.debug('httpResponse.getBody() ---'+httpResponseAccountObjectId.getBody());
                Map<String, Object> jsonResponse = (Map<String, Object>) JSON.deserializeUntyped(httpResponseAccountObjectId.getBody());
               
                List<Object> myMapObjects = (List<Object>) jsonResponse.get('objectIds');                               
                string accObjectid='';
                //if selected accounts are not availble at ESRI end
                if(myMapObjects!=null){
                    for(Object selectedObjectid:myMapObjects){
                        accObjectid+= string.valueOf(selectedObjectid)+',';
                    }

                    accObjectid = accObjectid.removeEnd(',');
                    system.debug('accObjectid '+accObjectid);                
                   
                    string paramValue ='objectIds='+accObjectid;
                    
                    String bodyDelete = paramValue+'&f=json';
                    system.debug('body delete '+bodyDelete);
                    
                    HTTPResponse httpResponseDeleteAccountObjectId = getHTTPResponse(ESRIService.Service_URL__c+ESRIService.Account_Layer__c +'/deleteFeatures?',bodyDelete);
                    System.debug('httpResponse.getBody() delete response---'+httpResponseDeleteAccountObjectId.getBody());
                    iSucess = httpResponseDeleteAccountObjectId.getStatusCode();  

                }                             
                
            }
             
        }catch(exception e){
            system.debug('exception is '+e.getStacktracestring());
            //SalesIQLogger.logCatchedException(e, true, 'deleteESRIAccountData'); 
        }
         return iSucess;
         
    }

    //Below method will create a map of Position id and list<CIMPOsitionMetrict Summary>. Which will be used to calculate the CIm at Territory level
    //Raghav is also using this method while creating sharing record using Affiliation
    public static map<string, list<CIM_Position_Metric_Summary__c>> getCIMPositionMetricSummary(set<string> positions){
        map<string, list<CIM_Position_Metric_Summary__c>> mapPositionAndMetricSummary = new map<string, list<CIM_Position_Metric_Summary__c>>();
        try{
            if(positions!=null && positions.size() > 0){
                system.debug('positions '+positions);
                system.debug('positions '+positions.size());

                                
                string strnewQuery = 'select id,approved__c, proposed__c, position_team_instance__r.position_id__c, CIM_Config__r.Attribute_API_Name__c,CIM_Config__r.Metric_Name__c from CIM_Position_Metric_Summary__c where position_team_instance__r.position_id__c in  :positions and CIM_config__r.Enable__c=true ';

                
                list<CIM_Position_Metric_Summary__c> lstCIMSummary = Database.query(strnewQuery);

                system.debug('lst '+lstCIMSummary.size());
                
                if(lstCIMSummary!=null && lstCIMSummary.size() > 0){
                    for(CIM_Position_Metric_Summary__c summary: lstCIMSummary){
                        if(mapPositionAndMetricSummary.containsKey(summary.position_team_instance__r.position_id__c)){
                            list<CIM_Position_Metric_Summary__c> tempSummary = mapPositionAndMetricSummary.get(summary.position_team_instance__r.position_id__c);
                            tempSummary.add(summary);
                            mapPositionAndMetricSummary.put(summary.position_team_instance__r.position_id__c, tempSummary);

                        }else{
                            mapPositionAndMetricSummary.put(summary.position_team_instance__r.position_id__c, new list<CIM_Position_Metric_Summary__c>{summary});
                        }
                        system.debug('mapPositionAndMetricSummary '+mapPositionAndMetricSummary);

                    }


                }

            }

        }catch(exception e){
            system.debug('exception is getCIMPositionMetricSummary '+e.getStacktracestring());
            //SalesIQLogger.logCatchedException(e, true, 'deleteESRIAccountData'); 
        }
        return mapPositionAndMetricSummary;

    }

}