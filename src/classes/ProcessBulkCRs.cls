global class ProcessBulkCRs implements Schedulable
{
    public String action;
    public String comments;
    
    public ProcessBulkCRs(string argAction, string cmnts){
        action = argAction;
        comments = cmnts;
    }
  

    /*public static void submitForApproval(List<Id> objectIds, String status, String reason){
        List<Approval.ProcessSubmitRequest> approvalRequestList = new List<Approval.ProcessSubmitRequest>();
        for(Id recordId : objectIds) {
            Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();              
            approvalRequest.setComments(reason);                 
            approvalRequest.setObjectId(recordId); 
            approvalRequestList.add(approvalRequest);     
        }  
        System.debug('----Submit run----');        
        List<Approval.ProcessResult> results = Approval.process(approvalRequestList);
        system.debug('#### approvers : '+results.size());
        if(status==SalesIQGlobalConstants.REQUEST_STATUS_APPROVED){
            List<Approval.ProcessWorkitemRequest> autoApproveRequests = new List<Approval.ProcessWorkitemRequest>();
            for(Approval.ProcessResult processResult : results) {
                Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                req.setComments(reason);
                req.setAction('Approve');
               
                List<Id> newWorkItemIds = processResult.getNewWorkitemIds() ;
                if(newWorkItemIds.isEmpty()) continue;
                req.setWorkitemId(newWorkItemIds[0]);
                
                autoApproveRequests.add(req);
            }
            List<Approval.ProcessResult> result2 =  Approval.process(autoApproveRequests);
        }
    }*/
   

    public static void submitCR(List<Id> objectIds, string rejectionComment, String varAction){
            list<Approval.ProcessWorkitemRequest> rejectionRequest = new list<Approval.ProcessWorkitemRequest>();
            for(ProcessInstance pItem : [SELECT Id, (Select Id from Workitems limit 1) FROM ProcessInstance WHERE TargetObjectId IN : objectIds]){
                Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                req.setComments(rejectionComment);
                //Approve or Reject Record
                System.debug('Starting to approve');
                if(varAction=='Approved'){
                    req.setAction('Approve');
                }
                else if(varAction=='Rejected'){
                    req.setAction('Reject');
                }
                else if(varAction=='Cancelled'){
                    req.setAction('Removed');
                }
                if(pItem.Workitems.size() > 0){
                    System.debug('pId' + pItem.Workitems[0].Id);
                    req.setWorkitemId(pItem.Workitems[0].Id);
                    rejectionRequest.add(req);
                }
                
            }
 
            // Submit the request for rejection
            if(rejectionRequest.size () > 0){
                System.debug('rejectionRequest.size ()' + rejectionRequest.size ());
                Approval.ProcessResult[] results = Approval.process(rejectionRequest);
            }
            
    } 


    global void execute(SchedulableContext ctx)
    {
        List<CR_Queue__c> crq_prog = [SELECT Name, Status__c FROM CR_Queue__c WHERE Status__c = 'In Progress' LIMIT 1];
        
        if (crq_prog.size() == 1)
        {
            System.debug('------ In Progress');
            // code for reschedule
                String day = string.valueOf(system.now().day());
				String month = string.valueOf(system.now().month());
				String hour = string.valueOf(system.now().hour());
				String minute = string.valueOf(system.now().minute());
				String second = string.valueOf(system.now().second()+60);
				String year = string.valueOf(system.now().year());
				Integer min =Integer.valueOf(minute);
				Integer hourvar=Integer.valueOf(system.now().hour());
				Integer sec=Integer.valueOf(second);
				if(sec>59){
						sec=sec-60;
						min=min+1;
				}
				second=string.valueOf(sec);
				minute=string.valueOf(min);
				if(min>59){
					Integer x=min-60;
					minute=string.valueOf(x);
					Integer hour1=(hourvar+1);
					hour=string.valueOf(hour1);
					if(Integer.valueof(hour)>=24){
							minute=string.valueOf(0);
							hour=string.valueOf(0);
					}
				}
				String strJobName = 'ProcessBulk-' + second + '_' + minute + '_' + hour + '_' + day + '_' + month + '_' + year;
				String strSchedule = second+ ' ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
				if(!test.isRunningTest()){
						System.schedule(strJobName, strSchedule, new ProcessBulkCRs(action, comments));
				}
        }
        
        else
        {
            List<CR_Queue__c> crq_pending = [SELECT Name, Status__c, Action__c FROM CR_Queue__c WHERE Status__c = 'Pending' LIMIT 1];
            
            System.debug('----- Pending');
            if (crq_pending.size() == 1)
            {
                crq_pending[0].Status__c = 'In Progress';
                update crq_pending;
                System.debug('----- Status update to In progress');
                List<Change_Request__c> cr = new List<Change_Request__c>();
                cr = [SELECT Id, Status__c FROM Change_Request__c WHERE Id =: crq_pending[0].Name LIMIT 1];
                System.debug('##### cr.size : '+cr.size());
                if(cr.size()>0){
                    system.debug('#### action : '+action);
                    system.debug('#### id : '+cr[0].id);
                    /*if (action == 'Approve'){
                        ProcessBulkCRs.submitForApproval(new List<id>{cr[0].id},SalesIQGlobalConstants.REQUEST_STATUS_APPROVED,'Bulk Approval');
                    }

                    else if(action == 'Reject'){*/
                    try{
                        if(cr[0].Status__c == 'Pending'){
                            ProcessBulkCRs.submitCR(new List<id>{cr[0].id}, comments, crq_pending[0].Action__c);
                        }
                        crq_pending[0].Status__c = 'Completed';
                        update crq_pending;
                    }
                    catch(exception e){
                        crq_pending[0].Status__c = 'Error Occurred';
                        update crq_pending;
                    }

                }
                
                String day = string.valueOf(system.now().day());
				String month = string.valueOf(system.now().month());
				String hour = string.valueOf(system.now().hour());
				String minute = string.valueOf(system.now().minute());
				String second = string.valueOf(system.now().second()+60);
				String year = string.valueOf(system.now().year());
				Integer min =Integer.valueOf(minute);
				Integer hourvar=Integer.valueOf(system.now().hour());
				Integer sec=Integer.valueOf(second);
				if(sec>59){
						sec=sec-60;
						min=min+1;
				}
				second=string.valueOf(sec);
				minute=string.valueOf(min);
				if(min>59){
					Integer x=min-60;
					minute=string.valueOf(x);
					Integer hour1=(hourvar+1);
					hour=string.valueOf(hour1);
					if(Integer.valueof(hour)>=24){
							minute=string.valueOf(0);
							hour=string.valueOf(0);
					}
				}
				String strJobName = 'ProcessBulk-' + second + '_' + minute + '_' + hour + '_' + day + '_' + month + '_' + year;
				String strSchedule = second+ ' ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
				if(!test.isRunningTest()){
						System.schedule(strJobName, strSchedule, new ProcessBulkCRs(action, comments));
				}
                // Query for Change Request
            }
        }
    }

}