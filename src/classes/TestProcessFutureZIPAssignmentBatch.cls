@IsTest
private class TestProcessFutureZIPAssignmentBatch {
    
    static {
        MapServerURL__c esriService=MapServerURL__c.getOrgDefaults();
        esriService.SetupOwnerId=UserInfo.getOrganizationId();
        esriService.Endpoint__c='https://uat.align-max.com/arcgis/rest/services/USA/ZIP_CODES/MapServer/';
        esriService.Service_URL__c='https://uat.align-max.com/arcgis/rest/services/Nextgen/Nextgen_Dev_28Oct16';
        esriService.Territory_Layer__c='/FeatureServer/1/';
        upsert esriService; 
    }
    
    static testMethod void testHierarchyChange(){   
        try{   
                
            TriggerContol__c  triggerctrl =  new TriggerContol__c(IsStopTrigger__c=false , Name='ChangeRequestTrigger');
            insert triggerctrl; 
            
            Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11788');
            insert geo;
            
            Geography__c pointGeo = CallPlanTestDataFactory.createGeography('Point',geo,'11789');       
            insert pointGeo;
            
            
            Profile hoProfile = [SELECT Id FROM Profile WHERE Name =: 'System Administrator' limit 1];
            
            User hoUser1 = new User(LastName = 'ho',
                                FirstName='test',
                                Alias = 'test',
                                Email = 'test.ho@test.com',
                                Username = 'test.ho@test.com',
                                ProfileId = hoProfile.id,
                                TimeZoneSidKey = 'GMT',
                                LanguageLocaleKey = 'en_US',
                                EmailEncodingKey = 'UTF-8',
                                LocaleSidKey = 'en_US');
            insert hoUser1;     
                    
            Team__c zipBasedTeam =  CallPlanTestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_BASE);      
            insert zipBasedTeam;
            
            Team_Instance__c zipBasedTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(zipBasedTeam,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP);
            insert zipBasedTeamInstance;      
            
            /*Team_Instance_Geography__c TeaminstanceGeo = CallPlanTestDataFactory.CreateTeamInstanceGeoGraphy(geo, zipBasedTeamInstance); 
            insert TeaminstanceGeo;*/
            
            Position__c district = CallPlanTestDataFactory.createPositionParent(null,zipBasedTeam);
            district.Related_Position_Type__c = 'Base';
            insert district;
            
            Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, null, zipBasedTeamInstance);
            districtTeamInstance.X_Max__c=-72.6966429900;
            districtTeamInstance.X_Min__c=-73.9625820000;
            districtTeamInstance.Y_Max__c=40.9666490000;
            districtTeamInstance.Y_Min__c=40.5821279800;
            insert districtTeamInstance;
            
            Position__c territory =  CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
            insert territory;
            
            Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, district, zipBasedTeamInstance);
            territoryTeamInstance.X_Max__c=-72.6966429900;
            territoryTeamInstance.X_Min__c=-73.9625820000;
            territoryTeamInstance.Y_Max__c=40.9666490000;
            territoryTeamInstance.Y_Min__c=40.5821279800;
            insert territoryTeamInstance;
            
            Position_Geography__c sourcePosGeo = CallPlanTestDataFactory.createPosGeography(geo, territory ,territoryTeamInstance, zipBasedTeamInstance );
            insert sourcePosGeo;
            
            Position__c destTerr = CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
            destTerr.Name = 'Long Island West';
            destTerr.Client_Territory_Name__c = 'Long Island West';
            destTerr.Client_Position_Code__c = '1NE30012';
            destTerr.Client_Territory_Code__c='1NE30012';
            insert destTerr;
            
            Position__c futurePos = CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
            futurePos.Name = 'Island West';
            futurePos.Client_Territory_Name__c = 'Island West';
            futurePos.Client_Position_Code__c = '1NE30044';
            futurePos.Client_Territory_Code__c='1NE30044';
            insert futurePos;
            
            Position_Team_Instance__c destTerrTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(destTerr, district, zipBasedTeamInstance);
            destTerrTeamInstance.X_Max__c=-72.6966429900;
            destTerrTeamInstance.X_Min__c=-73.9625820000;
            destTerrTeamInstance.Y_Max__c=40.9666490000;
            destTerrTeamInstance.Y_Min__c=40.5821279800;
            insert destTerrTeamInstance;  
            
            Position_Geography__c DestinationPosGeo = CallPlanTestDataFactory.createPosGeography(geo, destTerr ,destTerrTeamInstance, zipBasedTeamInstance );
            insert DestinationPosGeo;
            
            Change_Request_Type__c hierarchychange = CallPlanTestDataFactory.CreateZipRequestType();
            hierarchychange.CR_Type_Name__c ='Hierarchy Change';        
            insert hierarchychange;     
            
            Change_Request__c ApprovehierarchyChangeRequest = new Change_Request__c();
            ApprovehierarchyChangeRequest.AllZips__c = geo.name;
            ApprovehierarchyChangeRequest.Point_Zips__c = pointGeo.name;
            ApprovehierarchyChangeRequest.Approver3__c = hoUser1.id;
            ApprovehierarchyChangeRequest.Approver1__c = hoUser1.id;
            ApprovehierarchyChangeRequest.Approver2__c = hoUser1.id;
            ApprovehierarchyChangeRequest.Status__c = 'Approved';
            ApprovehierarchyChangeRequest.Destination_Position__c = destTerr.id;
            ApprovehierarchyChangeRequest.Destination_Positions__c= destTerrTeamInstance.id;
            ApprovehierarchyChangeRequest.Source_Positions__c= destTerrTeamInstance.id;
            ApprovehierarchyChangeRequest.Source_Position__c = territory.id;
            ApprovehierarchyChangeRequest.Team_Instance_ID__c = zipBasedTeamInstance.id;
            ApprovehierarchyChangeRequest.Change_Effective_Date__c = date.today();
            ApprovehierarchyChangeRequest.RecordTypeID__c= hierarchychange.id;
            ApprovehierarchyChangeRequest.Request_Type_Change__c = 'Hierarchy Change';        
            insert ApprovehierarchyChangeRequest;
            
            
            Change_Request__c ApprovehierarchyOverlayChangeRequest = new Change_Request__c();
            ApprovehierarchyOverlayChangeRequest.AllZips__c = geo.name;
            ApprovehierarchyOverlayChangeRequest.Point_Zips__c = pointGeo.name;
            ApprovehierarchyOverlayChangeRequest.Approver3__c = hoUser1.id;
            ApprovehierarchyOverlayChangeRequest.Approver1__c = hoUser1.id;
            ApprovehierarchyOverlayChangeRequest.Approver2__c = hoUser1.id;
            ApprovehierarchyOverlayChangeRequest.Status__c = 'Approved';
            ApprovehierarchyOverlayChangeRequest.Destination_Position__c = destTerr.id;
            ApprovehierarchyOverlayChangeRequest.Destination_Positions__c= destTerrTeamInstance.id;
            ApprovehierarchyOverlayChangeRequest.Source_Positions__c= destTerrTeamInstance.id;
            ApprovehierarchyOverlayChangeRequest.Source_Position__c = territory.id;
            ApprovehierarchyOverlayChangeRequest.Team_Instance_ID__c = zipBasedTeamInstance.id;
            ApprovehierarchyOverlayChangeRequest.Change_Effective_Date__c = date.today();
            ApprovehierarchyOverlayChangeRequest.RecordTypeID__c= hierarchychange.id;
            ApprovehierarchyOverlayChangeRequest.Request_Type_Change__c = 'Hierarchy Change Overlay';        
            insert ApprovehierarchyOverlayChangeRequest;
            
            
            CR_Position__c CRPos = CallPlanTestDataFactory.createCRPosition(ApprovehierarchyChangeRequest);  
            CRPos.position__c = territory.id;  
            CRPos.Future_Reporting_Position__c = futurePos.id;
            insert CRPos;
            
            CR_Position__c CRPosOverlay = CallPlanTestDataFactory.createCRPosition(ApprovehierarchyOverlayChangeRequest);  
            CRPosOverlay.position__c = territory.id;  
            CRPosOverlay.Future_Reporting_Position__c = futurePos.id;
            insert CRPosOverlay;

                           
            Test.setMock(HttpCalloutMock.class, new ESRIServicesMockGenerator());
            test.StartTest();
            
            ChangeRequestTriggerHandler.setAlreadyExecuted();
            Boolean b = ChangeRequestTriggerHandler.isAlreadyExecuted(); 
            ApprovehierarchyChangeRequest.Status__c = 'Approved';
            update ApprovehierarchyChangeRequest;
            
            ApprovehierarchyOverlayChangeRequest.Status__c = 'Approved';
            update ApprovehierarchyOverlayChangeRequest;
            test.StopTest();
        }
        catch(Exception ex){
            system.debug(ex);
        }
    }
    
    static testMethod void testProcessFutureApprovedCRScheduler(){     
        try{
                
            TriggerContol__c  triggerctrl =  new TriggerContol__c(IsStopTrigger__c=false , Name='ChangeRequestTrigger');
            insert triggerctrl; 
            
            Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11789');
            insert geo;
            
            Geography__c pointGeo = CallPlanTestDataFactory.createGeography('Point',geo,'11789');       
            insert pointGeo;
            
            
            Profile hoProfile = [SELECT Id FROM Profile WHERE Name =: 'System Administrator' limit 1];
            
            User hoUser1 = new User(LastName = 'ho',
                                FirstName='test',
                                Alias = 'test',
                                Email = 'test.ho@test.com',
                                Username = 'test.ho@test.com',
                                ProfileId = hoProfile.id,
                                TimeZoneSidKey = 'GMT',
                                LanguageLocaleKey = 'en_US',
                                EmailEncodingKey = 'UTF-8',
                                LocaleSidKey = 'en_US');
            insert hoUser1;     
                    
            Team__c zipBasedTeam =  CallPlanTestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_BASE);      
            insert zipBasedTeam;
            
            Team_Instance__c zipBasedTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(zipBasedTeam,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP);
            insert zipBasedTeamInstance;      
            
            /*Team_Instance_Geography__c TeaminstanceGeo = CallPlanTestDataFactory.CreateTeamInstanceGeoGraphy(geo, zipBasedTeamInstance); 
            insert TeaminstanceGeo;*/
            
            Position__c district = CallPlanTestDataFactory.createPositionParent(null,zipBasedTeam);
            district.Related_Position_Type__c = 'Base';
            insert district;
            
            Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, null, zipBasedTeamInstance);
            districtTeamInstance.X_Max__c=-72.6966429900;
            districtTeamInstance.X_Min__c=-73.9625820000;
            districtTeamInstance.Y_Max__c=40.9666490000;
            districtTeamInstance.Y_Min__c=40.5821279800;
            insert districtTeamInstance;
            
            Position__c territory =  CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
            insert territory;
            
            Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, district, zipBasedTeamInstance);
            territoryTeamInstance.X_Max__c=-72.6966429900;
            territoryTeamInstance.X_Min__c=-73.9625820000;
            territoryTeamInstance.Y_Max__c=40.9666490000;
            territoryTeamInstance.Y_Min__c=40.5821279800;
            insert territoryTeamInstance;
            
            Position_Geography__c sourcePosGeo = CallPlanTestDataFactory.createPosGeography(geo, territory ,territoryTeamInstance, zipBasedTeamInstance );
            insert sourcePosGeo;
            
            Position__c destTerr = CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
            destTerr.Name = 'Long Island West';
            destTerr.Client_Territory_Name__c = 'Long Island West';
            destTerr.Client_Position_Code__c = '1NE30012';
            destTerr.Client_Territory_Code__c='1NE30012';
            insert destTerr;
            
            Position__c futurePos = CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
            futurePos.Name = 'Island West';
            futurePos.Client_Territory_Name__c = 'Island West';
            futurePos.Client_Position_Code__c = '1NE30044';
            futurePos.Client_Territory_Code__c='1NE30044';
            insert futurePos;
            
            Position_Team_Instance__c destTerrTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(destTerr, district, zipBasedTeamInstance);
            destTerrTeamInstance.X_Max__c=-72.6966429900;
            destTerrTeamInstance.X_Min__c=-73.9625820000;
            destTerrTeamInstance.Y_Max__c=40.9666490000;
            destTerrTeamInstance.Y_Min__c=40.5821279800;
            insert destTerrTeamInstance;  
            
            Position_Geography__c DestinationPosGeo = CallPlanTestDataFactory.createPosGeography(geo, destTerr ,destTerrTeamInstance, zipBasedTeamInstance );
            insert DestinationPosGeo;
            
            Change_Request_Type__c hierarchychange = CallPlanTestDataFactory.CreateZipRequestType();
            hierarchychange.CR_Type_Name__c ='Hierarchy Change';        
            insert hierarchychange;     
            
            Change_Request__c ApprovehierarchyChangeRequest = new Change_Request__c();
            ApprovehierarchyChangeRequest.AllZips__c = geo.name;
            ApprovehierarchyChangeRequest.Point_Zips__c = pointGeo.name;
            ApprovehierarchyChangeRequest.Approver3__c = hoUser1.id;
            ApprovehierarchyChangeRequest.Approver1__c = hoUser1.id;
            ApprovehierarchyChangeRequest.Approver2__c = hoUser1.id;
            ApprovehierarchyChangeRequest.Status__c = 'Approved';
            ApprovehierarchyChangeRequest.Destination_Position__c = destTerr.id;
            ApprovehierarchyChangeRequest.Destination_Positions__c= destTerrTeamInstance.id;
            ApprovehierarchyChangeRequest.Source_Positions__c= destTerrTeamInstance.id;
            ApprovehierarchyChangeRequest.Source_Position__c = territory.id;
            ApprovehierarchyChangeRequest.Team_Instance_ID__c = zipBasedTeamInstance.id;
            ApprovehierarchyChangeRequest.Change_Effective_Date__c = date.today();
            ApprovehierarchyChangeRequest.RecordTypeID__c= hierarchychange.id;
            ApprovehierarchyChangeRequest.Request_Type_Change__c = 'Hierarchy Change';        
            insert ApprovehierarchyChangeRequest;
            
            Change_Request__c ApprovehierarchyOverlayChangeRequest = new Change_Request__c();
            ApprovehierarchyOverlayChangeRequest.AllZips__c = geo.name;
            ApprovehierarchyOverlayChangeRequest.Point_Zips__c = pointGeo.name;
            ApprovehierarchyOverlayChangeRequest.Approver3__c = hoUser1.id;
            ApprovehierarchyOverlayChangeRequest.Approver1__c = hoUser1.id;
            ApprovehierarchyOverlayChangeRequest.Approver2__c = hoUser1.id;
            ApprovehierarchyOverlayChangeRequest.Status__c = 'Approved';
            ApprovehierarchyOverlayChangeRequest.Destination_Position__c = destTerr.id;
            ApprovehierarchyOverlayChangeRequest.Destination_Positions__c= destTerrTeamInstance.id;
            ApprovehierarchyOverlayChangeRequest.Source_Positions__c= destTerrTeamInstance.id;
            ApprovehierarchyOverlayChangeRequest.Source_Position__c = territory.id;
            ApprovehierarchyOverlayChangeRequest.Team_Instance_ID__c = zipBasedTeamInstance.id;
            ApprovehierarchyOverlayChangeRequest.Change_Effective_Date__c = date.today();
            ApprovehierarchyOverlayChangeRequest.RecordTypeID__c= hierarchychange.id;
            ApprovehierarchyOverlayChangeRequest.Request_Type_Change__c = 'Hierarchy Change Overlay';        
            insert ApprovehierarchyOverlayChangeRequest;
            
            CR_Position__c CRPos = CallPlanTestDataFactory.createCRPosition(ApprovehierarchyChangeRequest);  
            CRPos.position__c = territory.id;  
            CRPos.Future_Reporting_Position__c = futurePos.id;
            insert CRPos;
            
            CR_Position__c CRPosOverlay = CallPlanTestDataFactory.createCRPosition(ApprovehierarchyOverlayChangeRequest);  
            CRPosOverlay.position__c = territory.id;  
            CRPosOverlay.Future_Reporting_Position__c = futurePos.id;
            insert CRPosOverlay;
            
            Test.setMock(HttpCalloutMock.class, new ESRIServicesMockGenerator());        
            ApprovehierarchyChangeRequest.Execution_Status__c = 'New';
            ApprovehierarchyOverlayChangeRequest.Execution_Status__c = 'New';
            update new list<Change_Request__c>{ApprovehierarchyChangeRequest,ApprovehierarchyOverlayChangeRequest};
            test.StartTest();
            ProcessFutureApprovedCRScheduler sh1 = new ProcessFutureApprovedCRScheduler();
            String sch = '0 0 23 * * ?';
            system.schedule('Test Future active cr', sch, sh1);
            
            test.stopTest();
        }
        catch(Exception ex){
            system.debug(ex);
        }
    }
    
    static testMethod void testProcessFutureApprovedCRSchedulerAccountMovement() {
                
        try{
            TriggerContol__c  triggerctrl =  new TriggerContol__c(IsStopTrigger__c=false , Name='ChangeRequestTrigger');
            insert triggerctrl; 
            
            Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11788');
            insert geo;
            
            Geography__c pointGeo = CallPlanTestDataFactory.createGeography('Point',geo,'11789');       
            insert pointGeo;
            
            
            Profile hoProfile = [SELECT Id FROM Profile WHERE Name =: 'System Administrator' limit 1];
            
            User hoUser1 = new User(LastName = 'ho',
                                FirstName='test',
                                Alias = 'test',
                                Email = 'test.ho@test.com',
                                Username = 'test.ho@test.com',
                                ProfileId = hoProfile.id,
                                TimeZoneSidKey = 'GMT',
                                LanguageLocaleKey = 'en_US',
                                EmailEncodingKey = 'UTF-8',
                                LocaleSidKey = 'en_US');
            insert hoUser1;     
                    
            Team__c zipBasedTeam =  CallPlanTestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_BASE);      
            insert zipBasedTeam;
            
            Team_Instance__c zipBasedTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(zipBasedTeam,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP);
            insert zipBasedTeamInstance;      
            
            /*Team_Instance_Geography__c TeaminstanceGeo = CallPlanTestDataFactory.CreateTeamInstanceGeoGraphy(geo, zipBasedTeamInstance); 
            insert TeaminstanceGeo;*/
            
            Position__c district = CallPlanTestDataFactory.createPositionParent(null,zipBasedTeam);
            district.Related_Position_Type__c = 'Base';
            insert district;
            
            Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, null, zipBasedTeamInstance);
            districtTeamInstance.X_Max__c=-72.6966429900;
            districtTeamInstance.X_Min__c=-73.9625820000;
            districtTeamInstance.Y_Max__c=40.9666490000;
            districtTeamInstance.Y_Min__c=40.5821279800;
            insert districtTeamInstance;
            
            Position__c territory =  CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
            insert territory;
            
            Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, district, zipBasedTeamInstance);
            territoryTeamInstance.X_Max__c=-72.6966429900;
            territoryTeamInstance.X_Min__c=-73.9625820000;
            territoryTeamInstance.Y_Max__c=40.9666490000;
            territoryTeamInstance.Y_Min__c=40.5821279800;
            insert territoryTeamInstance;
            
            Position_Geography__c sourcePosGeo = CallPlanTestDataFactory.createPosGeography(geo, territory,territoryTeamInstance, zipBasedTeamInstance );
            insert sourcePosGeo;
            
            Position__c destTerr = CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
            destTerr.Name = 'Long Island West';
            destTerr.Client_Territory_Name__c = 'Long Island West';
            destTerr.Client_Position_Code__c = '1NE30012';
            destTerr.Client_Territory_Code__c='1NE30012';
            insert destTerr;
            
            Position__c futurePos = CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
            futurePos.Name = 'Island West';
            futurePos.Client_Territory_Name__c = 'Island West';
            futurePos.Client_Position_Code__c = '1NE30044';
            futurePos.Client_Territory_Code__c='1NE30044';
            insert futurePos;
            
            Position_Team_Instance__c destTerrTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(destTerr, district, zipBasedTeamInstance);
            destTerrTeamInstance.X_Max__c=-72.6966429900;
            destTerrTeamInstance.X_Min__c=-73.9625820000;
            destTerrTeamInstance.Y_Max__c=40.9666490000;
            destTerrTeamInstance.Y_Min__c=40.5821279800;
            insert destTerrTeamInstance;  
            
            Position_Geography__c DestinationPosGeo = CallPlanTestDataFactory.createPosGeography(geo, destTerr ,destTerrTeamInstance, zipBasedTeamInstance );
            insert DestinationPosGeo;
            
            Account acc = new Account();
            acc.Name = 'test Acc';
            acc.FirstName__c = 'Abby';
            acc.BillingCity='NJ';
            acc.BillingState='NY';
            acc.AccountNumber='11788';
            acc.type = 'Physician';
            acc.AccountType__c = 'Physician';
            acc.BillingPostalCode = '122001';
            acc.Speciality__c = '11788Oncology';
            acc.Alignment_Type__c = 'Direct Aligned';
            acc.BillingLatitude = 40.5571389900;
            acc.BillingLongitude = -74.2123329900;
            insert acc;
            
            Team__c accBasedTeam = new Team__c(Name='HTN',Type__c=SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT ,Effective_End_Date__c=Date.today().addMonths(50));
            insert accBasedTeam;
            
            Team_Instance__c accBasedTeamInstance = new Team_Instance__c();
            accBasedTeamInstance.Name = 'HTN_Q1_2016';
            accBasedTeamInstance.Alignment_Period__c = 'Current';
            accBasedTeamInstance.Team__c = accBasedTeam.id;
            accBasedTeamInstance.Alignment_Type__c = 'Hybrid';
            accBasedTeamInstance.isActiveCycle__c = 'Y';
            accBasedTeamInstance.IC_EffstartDate__c=Date.Today();
            insert accBasedTeamInstance;
            
            /*Team_Instance_Account__c teamInstanceAcc = CallPlanTestDataFactory.createTeamInstanceAccount(acc,accBasedTeamInstance);
            insert teamInstanceAcc;*/
            
            Position_Account__c posAcc = CallPlanTestDataFactory.createPositionAccount(acc, destTerr, accBasedTeamInstance, destTerrTeamInstance);
            posAcc.Effective_Start_Date__c = system.today();
            insert posAcc;
            
            Change_Request_Type__c hierarchychange = CallPlanTestDataFactory.CreateZipRequestType();
            hierarchychange.CR_Type_Name__c ='Account Movement';        
            insert hierarchychange;     
            
            Change_Request__c ApprovehierarchyChangeRequest = new Change_Request__c();
            ApprovehierarchyChangeRequest.AllZips__c = geo.name;
            ApprovehierarchyChangeRequest.Point_Zips__c = pointGeo.name;
            ApprovehierarchyChangeRequest.Approver3__c = hoUser1.id;
            ApprovehierarchyChangeRequest.Approver1__c = hoUser1.id;
            ApprovehierarchyChangeRequest.Approver2__c = hoUser1.id;
            ApprovehierarchyChangeRequest.Status__c = 'Approved';
            ApprovehierarchyChangeRequest.Destination_Position__c = destTerr.id;
            ApprovehierarchyChangeRequest.Destination_Positions__c= destTerrTeamInstance.id;
            ApprovehierarchyChangeRequest.Source_Positions__c= destTerrTeamInstance.id;
            ApprovehierarchyChangeRequest.Source_Position__c = territory.id;
            ApprovehierarchyChangeRequest.Team_Instance_ID__c = zipBasedTeamInstance.id;
            ApprovehierarchyChangeRequest.Change_Effective_Date__c = date.today();
            ApprovehierarchyChangeRequest.RecordTypeID__c= hierarchychange.id;
            ApprovehierarchyChangeRequest.Request_Type_Change__c = 'Account Movement';        
            insert ApprovehierarchyChangeRequest;
            
            CR_Position__c CRPos = CallPlanTestDataFactory.createCRPosition(ApprovehierarchyChangeRequest);  
            CRPos.position__c = territory.id;  
            CRPos.Future_Reporting_Position__c = futurePos.id;
            insert CRPos;
            
            Test.setMock(HttpCalloutMock.class, new ESRIServicesMockGenerator());
            test.StartTest();
            ApprovehierarchyChangeRequest.Execution_Status__c = 'New';        
            update new list<Change_Request__c>{ApprovehierarchyChangeRequest};
            
            ProcessFutureApprovedCRScheduler sh1 = new ProcessFutureApprovedCRScheduler();
            String sch = '0 0 23 * * ?';
            system.schedule('Test Future active cr', sch, sh1);
            
            test.stopTest();
        }
        catch(Exception ex){
            system.debug(ex);
        }
    
    }
    static testMethod void testProcessFutureApprovedCRSchedulerZipMovement() {
        try{
                
            TriggerContol__c  triggerctrl =  new TriggerContol__c(IsStopTrigger__c=false , Name='ChangeRequestTrigger');
            insert triggerctrl; 
            
            Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11788');
            insert geo;
            
            Geography__c pointGeo = CallPlanTestDataFactory.createGeography('Point',geo,'11789');       
            insert pointGeo;
            
            
            Profile hoProfile = [SELECT Id FROM Profile WHERE Name =: 'System Administrator' limit 1];
            
            User hoUser1 = new User(LastName = 'ho',
                                FirstName='test',
                                Alias = 'test',
                                Email = 'test.ho@test.com',
                                Username = 'test.ho@test.com',
                                ProfileId = hoProfile.id,
                                TimeZoneSidKey = 'GMT',
                                LanguageLocaleKey = 'en_US',
                                EmailEncodingKey = 'UTF-8',
                                LocaleSidKey = 'en_US');
            insert hoUser1;     
                    
            Team__c zipBasedTeam =  CallPlanTestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_BASE);      
            insert zipBasedTeam;
            
            Team_Instance__c zipBasedTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(zipBasedTeam,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP);
            zipBasedTeamInstance.IC_EffEndDate__c=Date.today().addMonths(50);
            insert zipBasedTeamInstance;      
            
            /*Team_Instance_Geography__c TeaminstanceGeo = CallPlanTestDataFactory.CreateTeamInstanceGeoGraphy(geo, zipBasedTeamInstance); 
            insert TeaminstanceGeo;*/
            
            Position__c district = CallPlanTestDataFactory.createPositionParent(null,zipBasedTeam);
            district.Related_Position_Type__c = 'Base';
            insert district;
            
            Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, null, zipBasedTeamInstance);
            districtTeamInstance.X_Max__c=-72.6966429900;
            districtTeamInstance.X_Min__c=-73.9625820000;
            districtTeamInstance.Y_Max__c=40.9666490000;
            districtTeamInstance.Y_Min__c=40.5821279800;
            insert districtTeamInstance;
            
            Position__c territory =  CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
            insert territory;
            
            Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, district, zipBasedTeamInstance);
            territoryTeamInstance.X_Max__c=-72.6966429900;
            territoryTeamInstance.X_Min__c=-73.9625820000;
            territoryTeamInstance.Y_Max__c=40.9666490000;
            territoryTeamInstance.Y_Min__c=40.5821279800;
            insert territoryTeamInstance;
            
            Position_Geography__c sourcePosGeo = CallPlanTestDataFactory.createPosGeography(geo, territory,territoryTeamInstance, zipBasedTeamInstance );
            sourcePosGeo.Effective_End_Date__c = Date.today()+4;
            insert sourcePosGeo;
            
            Position__c destTerr = CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
            destTerr.Name = 'Long Island West';
            destTerr.Client_Territory_Name__c = 'Long Island West';
            destTerr.Client_Position_Code__c = '1NE30012';
            destTerr.Client_Territory_Code__c='1NE30012';
            insert destTerr;
            
            Position__c futurePos = CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
            futurePos.Name = 'Island West';
            futurePos.Client_Territory_Name__c = 'Island West';
            futurePos.Client_Position_Code__c = '1NE30044';
            futurePos.Client_Territory_Code__c='1NE30044';
            insert futurePos;
            
            Position_Team_Instance__c destTerrTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(destTerr, district, zipBasedTeamInstance);
            destTerrTeamInstance.X_Max__c=-72.6966429900;
            destTerrTeamInstance.X_Min__c=-73.9625820000;
            destTerrTeamInstance.Y_Max__c=40.9666490000;
            destTerrTeamInstance.Y_Min__c=40.5821279800;
            insert destTerrTeamInstance;  
            
            Position_Geography__c DestinationPosGeo = CallPlanTestDataFactory.createPosGeography(geo, destTerr,destTerrTeamInstance, zipBasedTeamInstance );
            DestinationPosGeo.Effective_End_Date__c=Date.today()+4;
            insert DestinationPosGeo;
            
            Change_Request_Type__c ziptype = CallPlanTestDataFactory.CreateZipRequestType();                
            insert ziptype;     
            
            Change_Request__c ApprovehierarchyChangeRequest = new Change_Request__c();
            ApprovehierarchyChangeRequest.AllZips__c = geo.name;
            ApprovehierarchyChangeRequest.Point_Zips__c = pointGeo.name;
            ApprovehierarchyChangeRequest.Approver3__c = hoUser1.id;
            ApprovehierarchyChangeRequest.Approver1__c = hoUser1.id;
            ApprovehierarchyChangeRequest.Approver2__c = hoUser1.id;
            ApprovehierarchyChangeRequest.Status__c = 'Approved';
            ApprovehierarchyChangeRequest.Destination_Position__c = destTerr.id;
            ApprovehierarchyChangeRequest.Destination_Positions__c= destTerrTeamInstance.id;
            ApprovehierarchyChangeRequest.Source_Positions__c= destTerrTeamInstance.id;
            ApprovehierarchyChangeRequest.Source_Position__c = territory.id;
            ApprovehierarchyChangeRequest.Team_Instance_ID__c = zipBasedTeamInstance.id;
            ApprovehierarchyChangeRequest.Change_Effective_Date__c = date.today();
            ApprovehierarchyChangeRequest.RecordTypeID__c= ziptype.id;
            ApprovehierarchyChangeRequest.Request_Type_Change__c = 'ZIP Movement';        
            insert ApprovehierarchyChangeRequest;
            
            CR_Position__c CRPos = CallPlanTestDataFactory.createCRPosition(ApprovehierarchyChangeRequest);  
            CRPos.position__c = territory.id;  
            CRPos.Future_Reporting_Position__c = futurePos.id;
            insert CRPos;
            
            ApprovehierarchyChangeRequest.Execution_Status__c = 'New';        
            update new list<Change_Request__c>{ApprovehierarchyChangeRequest};
            
            Test.setMock(HttpCalloutMock.class, new ESRIServicesMockGenerator());
            test.StartTest();
            
            
            ProcessFutureApprovedCRScheduler sh1 = new ProcessFutureApprovedCRScheduler();
            String sch = '0 0 23 * * ?';
            system.schedule('Test Future active cr', sch, sh1);
            
            test.stopTest();
        }
        catch(Exception ex){
            system.debug(ex);
        }
    
    }
    
}