@IsTest
public class TestCIMPositionMatrixSummaryUpdateCtlr {
    static testMethod void TestCIMPositionMatrixSummaryUpdateCtlr(){
        Team__c objTeam = new Team__c(Name='HTN',Type__c=SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP ,Effective_End_Date__c=Date.today().addMonths(50));
        insert objTeam;
        
        Affiliation_Network__c affNet = new Affiliation_Network__c(Name = 'test aff');
        insert affNet;
        
        Organization_Master__c org=new Organization_Master__c();
        org.Name='Asia';
        org.Org_Level__c='Global';
        org.Parent_Country_Level__c=true;
        insert org;
        
        Country__c ctr=new Country__c();
        ctr.name='USA';
        ctr.Country_Code__c='US';
        ctr.Status__c='Active';
        ctr.Parent_Organization__c=org.id;
        insert ctr;
        
        Geography_Type__c gType=new Geography_Type__c();
        gType.Name='ZIP';
        gType.Country__c=ctr.id;
        gType.Account_Shape_Available__c=true;
        gType.Shape_Available__c=true;
        insert gType;
        
        Team_Instance__c objCurrentTeamInstance = new Team_Instance__c();
        objCurrentTeamInstance.Name = 'HTN_Q1_2016';
        objCurrentTeamInstance.Alignment_Period__c = 'Current';
        objCurrentTeamInstance.Team__c = objTeam.id;
        objCurrentTeamInstance.Alignment_Type__c='ZIP';
        objCurrentTeamInstance.Alignment_Type__c = SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP;
        objCurrentTeamInstance.isActiveCycle__c = 'Y';        
        objCurrentTeamInstance.IC_EffstartDate__c=Date.Today();
        objCurrentTeamInstance.Restrict_ZIP_Share__c = true;
        objCurrentTeamInstance.Geography_Type_Name__c=gType.id;
        objCurrentTeamInstance.Affiliation_Network__c=affNet.id;
        objCurrentTeamInstance.Enable_Affiliation_Movement__c = true;
        insert objCurrentTeamInstance;
        
        Business_Rule_Template__c br=new Business_Rule_Template__c();
        br.name='Product Assignment Template';
        insert br;
        
        Workspace__c wspace=CallPlanTestDataFactory.createWorkspace();
        insert wspace;
        
        Scenario__c curScenario=CallPlanTestDataFactory.createScenario(wspace, objCurrentTeamInstance);
        curScenario.Scenario_Type__c='Alignment Scenario';
        curScenario.Business_Rule_Template__c=br.id;
        curScenario.Request_Process_Stage__c='CIM Ready';
        insert curScenario;
        
        Geography__c geo1 = new Geography__c();
        geo1.Name = '11788';
        geo1.City__c='Test 11788';
        geo1.State__c = '11788';
        geo1.Neighbor_Geography__c = '11789';
        geo1.Zip_Type__c = 'Standard';
        geo1.Centroid_Latitude__c = 0.0;
        geo1.Centroid_Longitude__c = 0.0;
        insert geo1;
        
        Account acc = CallPlanTestDataFactory.CreateAccount('Direct Aligned','11788','Physician');    
        acc.type = 'Physician';
        insert acc;
        
        /*Team_Instance_Account__c teamInstanceAcc = CallPlanTestDataFactory.createTeamInstanceAccount(acc,objCurrentTeamInstance);
        insert teamInstanceAcc; */
        
        /*Team_Instance_Geography__c teamInsGeo1 = new Team_Instance_Geography__c();
        teamInsGeo1.Geography__c = geo1.id;
        teamInsGeo1.Team_Instance__c = objCurrentTeamInstance.id;
        teamInsGeo1.Effective_Start_Date__c = Date.today().addMonths(1);
        teamInsGeo1.Effective_End_Date__c = Date.today().addYears(1);
        teamInsGeo1.Metric1__c  =2.010000000000;
        teamInsGeo1.Metric2__c  =29.000000000000;
        teamInsGeo1.Metric3__c  =2.000000000000;
        teamInsGeo1.Metric4__c  =1.431479545000;
        teamInsGeo1.Metric5__c  =1.490000000000;
        teamInsGeo1.Metric6__c  =2.010000000000;
        teamInsGeo1.Metric7__c  =29.000000000000;
        teamInsGeo1.Metric8__c  =2.000000000000;
        teamInsGeo1.Metric9__c  =1.431479545000;
        teamInsGeo1.Metric10__c =1.490000000000;
        insert teamInsGeo1;*/

        Position__c CurTerr = new Position__c();
        CurTerr.Name = 'Long Island East';
        CurTerr.Client_Territory_Name__c = 'Long Island East';
        CurTerr.Client_Position_Code__c = '1NE30012';
        CurTerr.Client_Territory_Code__c='1NE30012';
        CurTerr.Position_Type__c='Territory';
        CurTerr.inactive__c = false;
        CurTerr.RGB__c = '41,210,117';
        CurTerr.Team_iD__c = objTeam.id;
        CurTerr.Team_Instance__c=objCurrentTeamInstance.id;
        //destTerr.Parent_Position__c = district.Id;
        CurTerr.Hierarchy_Level__c = '1';
        CurTerr.Related_Position_Type__c  ='Base';
        CurTerr.Effective_Start_Date__c=Date.today();
        CurTerr.Effective_End_Date__c=Date.today().addMonths(50);
        insert CurTerr;
        
        Position_Team_Instance__c curTerrTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(CurTerr, null, objCurrentTeamInstance);
        curTerrTeamInstance.X_Max__c=-72.6966429900;
        curTerrTeamInstance.X_Min__c=-73.9625820000;
        curTerrTeamInstance.Y_Max__c=40.9666490000;
        curTerrTeamInstance.Y_Min__c=40.5821279800;
        insert curTerrTeamInstance;
        
        Position_Geography__c posGeo1  = new Position_Geography__c();
        posGeo1.Effective_Start_Date__c  = Date.today().addMonths(1);
        posGeo1.Effective_End_Date__c  = Date.today().addYears(1);
        posGeo1.IsShared__c = true;  
        posGeo1.SharedWith__c = 'Long Island West';
        posGeo1.Geography__c = geo1.id;     
        posGeo1.Position__c = CurTerr.id;
        posGeo1.Team_Instance__c = objCurrentTeamInstance.id ;
        //posGeo1.Team_Instance_Geography__c = teamInsGeo1.id;
        posGeo1.Position_Team_Instance__c = curTerrTeamInstance.id;
        posGeo1.Change_Status__c = 'No Change';
        posGeo1.Metric1__c  =2.010000000000;
        posGeo1.Metric2__c  =29.000000000000;
        posGeo1.Metric3__c  =2.000000000000;
        posGeo1.Metric4__c  =1.431479545000;
        posGeo1.Metric5__c  =1.490000000000;
        posGeo1.Metric6__c  =2.010000000000;
        posGeo1.Metric7__c  =29.000000000000;
        posGeo1.Metric8__c  =2.000000000000;
        posGeo1.Metric9__c  =1.431479545000;
        posGeo1.Metric10__c =1.490000000000;
        insert posGeo1;
        
       /* Position_Account__c posAccnt  = new Position_Account__c();
        posAccnt.Effective_Start_Date__c  = Date.today().addMonths(1);
        posGeo1.Effective_End_Date__c  = Date.today().addYears(1);
        posGeo1.IsShared__c = true;  
        posGeo1.SharedWith__c = 'Long Island West';
        posGeo1.Geography__c = geo1.id;     
        posGeo1.Position__c = CurTerr.id;
        posGeo1.Team_Instance__c = objCurrentTeamInstance.id ;
        posGeo1.Team_Instance_Geography__c = teamInsGeo1.id;
        posGeo1.Position_Team_Instance__c = curTerrTeamInstance.id;
        posGeo1.Change_Status__c = 'No Change';
        posGeo1.Metric1__c  =2.010000000000;
        posGeo1.Metric2__c  =29.000000000000;
        posGeo1.Metric3__c  =2.000000000000;
        posGeo1.Metric4__c  =1.431479545000;
        posGeo1.Metric5__c  =1.490000000000;
        posGeo1.Metric6__c  =2.010000000000;
        posGeo1.Metric7__c  =29.000000000000;
        posGeo1.Metric8__c  =2.000000000000;
        posGeo1.Metric9__c  =1.431479545000;
        posGeo1.Metric10__c =1.490000000000;
        insert posGeo1;
        */
       
        Account accChild = CallPlanTestDataFactory.CreateAccount('Direct Aligned','11789','Physician');    
        accChild.type = 'Physician';
        insert accChild;
        Account_Affiliation__c accAffList = new Account_Affiliation__c(Affiliation_Network__c = affNet.Id, Account__c = acc.Id, Root_Account__c = accChild.Id);
        insert accAffList;
        
        
        
        
        Position_Account_Call_Plan__c PosCallPlan=new Position_Account_Call_Plan__c();
        PosCallPlan.Effective_Start_Date__c  = Date.today().addMonths(1);
        PosCallPlan.Effective_End_Date__c  = Date.today().addYears(1);
        PosCallPlan.Account__c = acc.id;  
        PosCallPlan.Picklist1_Segment__c='Class 3';
        //PosCallPlan.isincludedCallPlan__c=TRUE;
        PosCallPlan.Position__c = CurTerr.id;
        PosCallPlan.Team_Instance__c = objCurrentTeamInstance.id ;
        //PosCallPlan.Team_Instance_Account__c = teamInstanceAcc.id;
        PosCallPlan.Position_Team_Instance__c = curTerrTeamInstance.id;
        PosCallPlan.Change_Status__c = 'No Change';
        PosCallPlan.Metric1__c  =2.010000000000;
        PosCallPlan.Metric2__c  =29.000000000000;
        PosCallPlan.Metric3__c  =2.000000000000;
        PosCallPlan.Metric4__c  =1.431479545000;
        PosCallPlan.Metric5__c  =1.490000000000;
        PosCallPlan.Metric6__c  =2.010000000000;
        PosCallPlan.Metric7__c  =29.000000000000;
        PosCallPlan.Metric8__c  =2.000000000000;
        PosCallPlan.Metric9__c  =1.431479545000;
        PosCallPlan.Metric10__c =1.490000000000;
        insert PosCallPlan;
        
        Change_Request_Type__c ZipRequestType = CallPlanTestDataFactory.CreateZipRequestType();     
        insert ZipRequestType;
        Change_Request_Type__c CallPlanType=CallPlanTestDataFactory.CreateCallPlanRequestType();
        insert CallPlanType;
        
        Map<String,String> cimId=new Map<String,String>(); 
        list<CIM_Config__c> lstCIM = new list<CIM_Config__c>();
        Integer iCount;
        try{
        for(iCount=1;iCount<=3;iCount++){
            
            CIM_Config__c obj = new CIM_Config__c();
            obj.Name = 'Test CIM'+iCount;
            obj.Aggregation_Type__c = 'Sum';
            obj.Hierarchy_Level__c='1';
            obj.Is_Custom_Metric__c=false; 
            obj.Attribute_API_Name__c = SalesIQGlobalConstants.NAME_SPACE+'Metric'+iCount+'__c';
            obj.Attribute_Display_Name__c = 'Test Display'+iCount;
                if(iCount==1)
                {
                    obj.Object_Name__c = SalesIQGlobalConstants.NAME_SPACE+'Position_geography__c';
                    obj.Change_Request_Type__c = ZipRequestType.id;
                    obj.Aggregation_Object_Name__c=SalesIQGlobalConstants.NAME_SPACE+'Position_geography__c';
                }
                if(iCount==2)
                {
                    obj.Change_Request_Type__c = CallPlanType.id;
                    obj.Object_Name__c = 'Position_Account_Call_plan__c';
                    obj.Aggregation_Object_Name__c='Position_Account_Call_plan__c';
                    obj.Aggregation_Condition_Attribute_API_Name__c='Picklist1_Segment__c=\'Class 3\''; // and isIncludedCallPlan__c=TRUE';
                }
                
                if(iCount==3)
                {
                    obj.Object_Name__c = SalesIQGlobalConstants.NAME_SPACE+'Position_geography__c';
                    obj.Change_Request_Type__c = ZipRequestType.id;
                    obj.Aggregation_Object_Name__c=SalesIQGlobalConstants.NAME_SPACE+'Position_geography__c';                    
                }
            obj.Aggregation_Attribute_API_Name__c= 'Position_Team_Instance__c';
            obj.MetricCalculationType__c = 'Percentage';
            obj.isOptimum__c = false;
            obj.Enable__c = true;
            obj.Team_Instance__c = objCurrentTeamInstance.id;
            
            obj.Threshold_Min__c = '-40';
            obj.Threshold_Max__c = '40';
            obj.Threshold_Warning_Min__c = '-20';
            obj.Threshold_Warning_Max__c = '20';
            obj.Acceptable_Max__c = '5';
            obj.Acceptable_Min__c = '-5';
            System.debug('obj :'+obj);
            lstCIM.add(obj);
        }
        
        insert lstCIM;
        for(CIM_Config__c obj : lstCIM){
            cimId.put(obj.id,obj.id);
        }
        
        
        
        test.startTest();
        string currenteaminsId = (string) objCurrentTeamInstance.id;
        string scenarioId = (string) curScenario.id;
        
            CIMPositionMatrixSummaryUpdateCtlr cimctrl=new CIMPositionMatrixSummaryUpdateCtlr(currenteaminsId,scenarioId);
        }catch(Exception e){
            System.debug(e.getLineNumber() + ', error -'+e.getMessage());
        }
        
        test.stopTest();
    }
    
    static testMethod void TestCIMPositionMatrixSummaryUpdateCtlrCallPlan(){
        Team__c objTeam = new Team__c(Name='HTN',Type__c=SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP ,Effective_End_Date__c=Date.today().addMonths(50));
        insert objTeam;
        
        Affiliation_Network__c affNet = new Affiliation_Network__c(Name = 'test aff');
        insert affNet;
        
        Organization_Master__c org=new Organization_Master__c();
        org.Name='Asia';
        org.Org_Level__c='Global';
        org.Parent_Country_Level__c=true;
        insert org;
        
        Country__c ctr=new Country__c();
        ctr.name='USA';
        ctr.Country_Code__c='US';
        ctr.Status__c='Active';
        ctr.Parent_Organization__c=org.id;
        insert ctr;
        
        Geography_Type__c gType=new Geography_Type__c();
        gType.Name='ZIP';
        gType.Country__c=ctr.id;
        gType.Account_Shape_Available__c=true;
        gType.Shape_Available__c=true;
        insert gType;
        
        Team_Instance__c objCurrentTeamInstance = new Team_Instance__c();
        objCurrentTeamInstance.Name = 'HTN_Q1_2016';
        objCurrentTeamInstance.Alignment_Period__c = 'Current';
        objCurrentTeamInstance.Team__c = objTeam.id;
        objCurrentTeamInstance.Alignment_Type__c='ZIP';
        objCurrentTeamInstance.Alignment_Type__c = SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP;
        objCurrentTeamInstance.isActiveCycle__c = 'Y';        
        objCurrentTeamInstance.IC_EffstartDate__c=Date.Today();
        objCurrentTeamInstance.Restrict_ZIP_Share__c = true;
        objCurrentTeamInstance.Geography_Type_Name__c=gType.id;
        objCurrentTeamInstance.Affiliation_Network__c=affNet.id;
        objCurrentTeamInstance.Enable_Affiliation_Movement__c = true;
        insert objCurrentTeamInstance;
        
        Workspace__c wspace=CallPlanTestDataFactory.createWorkspace();
        insert wspace;
        
        Business_Rule_Template__c br=new Business_Rule_Template__c();
        br.name='Product Assignment Template';
        Scenario__c curScenario1=CallPlanTestDataFactory.createScenario(wspace, objCurrentTeamInstance);
        curScenario1.Scenario_Type__c='Call_Plan_Change';
        curScenario1.Business_Rule_Template__c=br.id;
        curScenario1.Request_Process_Stage__c='CIM Ready';
        insert curScenario1;
        
        
        Geography__c geo1 = new Geography__c();
        geo1.Name = '11788';
        geo1.City__c='Test 11788';
        geo1.State__c = '11788';
        geo1.Neighbor_Geography__c = '11789';
        geo1.Zip_Type__c = 'Standard';
        geo1.Centroid_Latitude__c = 0.0;
        geo1.Centroid_Longitude__c = 0.0;
        insert geo1;
        
        Account acc = CallPlanTestDataFactory.CreateAccount('Direct Aligned','11788','Physician');    
        acc.type = 'Physician';
        insert acc;
        
        /*Team_Instance_Account__c teamInstanceAcc = CallPlanTestDataFactory.createTeamInstanceAccount(acc,objCurrentTeamInstance);
        insert teamInstanceAcc; */
        
        /*Team_Instance_Geography__c teamInsGeo1 = new Team_Instance_Geography__c();
        teamInsGeo1.Geography__c = geo1.id;
        teamInsGeo1.Team_Instance__c = objCurrentTeamInstance.id;
        teamInsGeo1.Effective_Start_Date__c = Date.today().addMonths(1);
        teamInsGeo1.Effective_End_Date__c = Date.today().addYears(1);
        teamInsGeo1.Metric1__c  =2.010000000000;
        teamInsGeo1.Metric2__c  =29.000000000000;
        teamInsGeo1.Metric3__c  =2.000000000000;
        teamInsGeo1.Metric4__c  =1.431479545000;
        teamInsGeo1.Metric5__c  =1.490000000000;
        teamInsGeo1.Metric6__c  =2.010000000000;
        teamInsGeo1.Metric7__c  =29.000000000000;
        teamInsGeo1.Metric8__c  =2.000000000000;
        teamInsGeo1.Metric9__c  =1.431479545000;
        teamInsGeo1.Metric10__c =1.490000000000;
        insert teamInsGeo1;*/

        Position__c CurTerr = new Position__c();
        CurTerr.Name = 'Long Island East';
        CurTerr.Client_Territory_Name__c = 'Long Island East';
        CurTerr.Client_Position_Code__c = '1NE30012';
        CurTerr.Client_Territory_Code__c='1NE30012';
        CurTerr.Position_Type__c='Territory';
        CurTerr.inactive__c = false;
        CurTerr.RGB__c = '41,210,117';
        CurTerr.Team_iD__c = objTeam.id;
        CurTerr.Team_Instance__c=objCurrentTeamInstance.id;
        //destTerr.Parent_Position__c = district.Id;
        CurTerr.Hierarchy_Level__c = '1';
        CurTerr.Related_Position_Type__c  ='Base';
        CurTerr.Effective_Start_Date__c=Date.today();
        CurTerr.Effective_End_Date__c=Date.today().addMonths(50);
        insert CurTerr;
        
        Position_Team_Instance__c curTerrTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(CurTerr, null, objCurrentTeamInstance);
        curTerrTeamInstance.X_Max__c=-72.6966429900;
        curTerrTeamInstance.X_Min__c=-73.9625820000;
        curTerrTeamInstance.Y_Max__c=40.9666490000;
        curTerrTeamInstance.Y_Min__c=40.5821279800;
        insert curTerrTeamInstance;
        
        Position_Geography__c posGeo1  = new Position_Geography__c();
        posGeo1.Effective_Start_Date__c  = Date.today().addMonths(1);
        posGeo1.Effective_End_Date__c  = Date.today().addYears(1);
        posGeo1.IsShared__c = true;  
        posGeo1.SharedWith__c = 'Long Island West';
        posGeo1.Geography__c = geo1.id;     
        posGeo1.Position__c = CurTerr.id;
        posGeo1.Team_Instance__c = objCurrentTeamInstance.id ;
        //posGeo1.Team_Instance_Geography__c = teamInsGeo1.id;
        posGeo1.Position_Team_Instance__c = curTerrTeamInstance.id;
        posGeo1.Change_Status__c = 'No Change';
        posGeo1.Metric1__c  =2.010000000000;
        posGeo1.Metric2__c  =29.000000000000;
        posGeo1.Metric3__c  =2.000000000000;
        posGeo1.Metric4__c  =1.431479545000;
        posGeo1.Metric5__c  =1.490000000000;
        posGeo1.Metric6__c  =2.010000000000;
        posGeo1.Metric7__c  =29.000000000000;
        posGeo1.Metric8__c  =2.000000000000;
        posGeo1.Metric9__c  =1.431479545000;
        posGeo1.Metric10__c =1.490000000000;
        insert posGeo1;
        
       /* Position_Account__c posAccnt  = new Position_Account__c();
        posAccnt.Effective_Start_Date__c  = Date.today().addMonths(1);
        posGeo1.Effective_End_Date__c  = Date.today().addYears(1);
        posGeo1.IsShared__c = true;  
        posGeo1.SharedWith__c = 'Long Island West';
        posGeo1.Geography__c = geo1.id;     
        posGeo1.Position__c = CurTerr.id;
        posGeo1.Team_Instance__c = objCurrentTeamInstance.id ;
        posGeo1.Team_Instance_Geography__c = teamInsGeo1.id;
        posGeo1.Position_Team_Instance__c = curTerrTeamInstance.id;
        posGeo1.Change_Status__c = 'No Change';
        posGeo1.Metric1__c  =2.010000000000;
        posGeo1.Metric2__c  =29.000000000000;
        posGeo1.Metric3__c  =2.000000000000;
        posGeo1.Metric4__c  =1.431479545000;
        posGeo1.Metric5__c  =1.490000000000;
        posGeo1.Metric6__c  =2.010000000000;
        posGeo1.Metric7__c  =29.000000000000;
        posGeo1.Metric8__c  =2.000000000000;
        posGeo1.Metric9__c  =1.431479545000;
        posGeo1.Metric10__c =1.490000000000;
        insert posGeo1;
        */
        Account accChild = CallPlanTestDataFactory.CreateAccount('Direct Aligned','11789','Physician');    
        accChild.type = 'Physician';
        insert accChild;
        Account_Affiliation__c accAffList = new Account_Affiliation__c(Affiliation_Network__c = affNet.Id, Account__c = acc.Id, Root_Account__c = accChild.Id);
        insert accAffList;
        
        Position_Account_Call_Plan__c PosCallPlan=new Position_Account_Call_Plan__c();
        PosCallPlan.Effective_Start_Date__c  = Date.today().addMonths(1);
        PosCallPlan.Effective_End_Date__c  = Date.today().addYears(1);
        PosCallPlan.Account__c = acc.id;  
        PosCallPlan.Picklist1_Segment__c='Class 3';
        //PosCallPlan.isincludedCallPlan__c=TRUE;
        PosCallPlan.Position__c = CurTerr.id;
        PosCallPlan.Team_Instance__c = objCurrentTeamInstance.id ;
        //PosCallPlan.Team_Instance_Account__c = teamInstanceAcc.id;
        PosCallPlan.Position_Team_Instance__c = curTerrTeamInstance.id;
        PosCallPlan.Change_Status__c = 'No Change';
        PosCallPlan.Metric1__c  =2.010000000000;
        PosCallPlan.Metric2__c  =29.000000000000;
        PosCallPlan.Metric3__c  =2.000000000000;
        PosCallPlan.Metric4__c  =1.431479545000;
        PosCallPlan.Metric5__c  =1.490000000000;
        PosCallPlan.Metric6__c  =2.010000000000;
        PosCallPlan.Metric7__c  =29.000000000000;
        PosCallPlan.Metric8__c  =2.000000000000;
        PosCallPlan.Metric9__c  =1.431479545000;
        PosCallPlan.Metric10__c =1.490000000000;
        insert PosCallPlan;
        
        
        
        Change_Request_Type__c ZipRequestType = CallPlanTestDataFactory.CreateZipRequestType();     
        insert ZipRequestType;
        Change_Request_Type__c CallPlanType=CallPlanTestDataFactory.CreateCallPlanRequestType();
        insert CallPlanType;
        //list<CIM_Config__c> Cimconfig=CallPlanTestDataFactory.createCIMCOnfig(ZipRequestType, objCurrentTeamInstance, 'Team_instance_geography__c');
        Map<String,String> cimId=new Map<String,String>(); 
        list<CIM_Config__c> lstCIM = new list<CIM_Config__c>();
        Integer iCount;
        for(iCount=1;iCount<=3;iCount++){
            
            CIM_Config__c obj = new CIM_Config__c();
            obj.Name = 'Test CIM'+iCount;
            obj.Aggregation_Type__c = 'Sum';
            obj.Hierarchy_Level__c='1';
            obj.Is_Custom_Metric__c=false; 
            obj.Attribute_API_Name__c = SalesIQGlobalConstants.NAME_SPACE+'Metric'+iCount+'__c';
            obj.Attribute_Display_Name__c = 'Test Display'+iCount;
                if(iCount==1)
                {
                    obj.Object_Name__c = 'Team_instance_geography__c';
                    obj.Change_Request_Type__c = ZipRequestType.id;
                    obj.Aggregation_Object_Name__c='Team_instance_geography__c';
                }
                if(iCount==2)
                {
                    obj.Change_Request_Type__c = CallPlanType.id;
                    obj.Object_Name__c = 'Position_Account_Call_plan__c';
                    obj.Aggregation_Object_Name__c='Position_Account_Call_plan__c';
                    obj.Aggregation_Condition_Attribute_API_Name__c='Picklist1_Segment__c=\'Class 3\''; // and isIncludedCallPlan__c=TRUE';
                }
                
                if(iCount==3)
                {
                    obj.Object_Name__c = 'Position_geography__c';
                    obj.Change_Request_Type__c = ZipRequestType.id;
                    obj.Aggregation_Object_Name__c='Position_geography__c';                    
                }
            obj.Aggregation_Attribute_API_Name__c='Position_Team_Instance__c';
            obj.MetricCalculationType__c = 'Percentage';
            obj.isOptimum__c = false;
            obj.Enable__c = true;
            obj.Team_Instance__c = objCurrentTeamInstance.id;
            
            obj.Threshold_Min__c = '-40';
            obj.Threshold_Max__c = '40';
            obj.Threshold_Warning_Min__c = '-20';
            obj.Threshold_Warning_Max__c = '20';
            obj.Acceptable_Max__c = '5';
            obj.Acceptable_Min__c = '-5';
            lstCIM.add(obj);
        }
        
        insert lstCIM;
        for(CIM_Config__c obj : lstCIM){
            cimId.put(obj.id,obj.id);
        }
        
        
        test.startTest();
        string currenteaminsId = (string) objCurrentTeamInstance.id;
        string scenarioId1 = (string) curScenario1.id;
        CIMPositionMatrixSummaryUpdateCtlr cimctrl=new CIMPositionMatrixSummaryUpdateCtlr(currenteaminsId,scenarioId1);
        test.stopTest();
    }
}