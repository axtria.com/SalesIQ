public class CPGDerivedFieldsListCtrl {
  
     /**
     * calling derived data list on click of canvas + sign
     *  
     *  @author  Aakash Gupta
     *  @version 1.0
     *  @since   2017-09-19  
    */
    @AuraEnabled
    public static List<Wrapper>  getDerivedFieldListData(string ScenarioRuleInstanceId){
    List<Wrapper> lstWrapper=new List<Wrapper>();
    List<Scenario_Rule_Instance_Details__c> lstScenarioRuleInstanceDetails=new  List<Scenario_Rule_Instance_Details__c>(); 
    //lstScenarioRuleInstanceDetails=[select Id from Scenario_Rule_Instance_Details__c where Scenario_Id__c= :scenarioId 
                                  // and ]    
     List<Business_Rule_FieldMap_Details__c> derivedListData = [Select Business_Rule__r.Name, Param_Name__c,
      Business_Rule__r.Business_Rule_Type_Master__r.Name, Param_Data_Type__c, Param_Type__c, Business_Rule__r.Id 
     FROM Business_Rule_FieldMap_Details__c 
     WHERE IO_Flag__c='O' and Business_Rule__r.ScenarioRuleInstanceDetails__c =:ScenarioRuleInstanceId ];
     
     for(Business_Rule_FieldMap_Details__c fieldMap:derivedListData){
     	lstWrapper.add(new Wrapper(fieldMap.Business_Rule__r.Name,fieldMap.Param_Name__c,
      fieldMap.Business_Rule__r.Business_Rule_Type_Master__r.Name,fieldMap.Param_Data_Type__c,fieldMap.Param_Type__c,
       fieldMap.Business_Rule__r.Id));
     }
      return  lstWrapper;                                                          
                                                                 
    }
    
    public  class Wrapper {
    	@AuraEnabled
    	public string businessRuleName{get;set;}
    	@AuraEnabled
    	public string paramName{get;set;}
    	@AuraEnabled
    	public string businessRuleTypeMasterName{get;set;}
    	@AuraEnabled
    	public string paramDataType{get;set;}
    	@AuraEnabled
    	public string paramType{get;set;}
    	@AuraEnabled
    	public string businessRuleId{get;set;}
    	
    	public Wrapper(string businessRuleName,string paramName,string businessRuleTypeMasterName,string paramDataType,string paramType,string businessRuleId){
    		this.businessRuleName=businessRuleName;
    		this.paramName=paramName;
    		this.businessRuleTypeMasterName=businessRuleTypeMasterName;
    		this.paramDataType=paramDataType;
    		this.paramType=paramType;
    		this.businessRuleId=businessRuleId;
    	}
    }
    
     /**
     * calling scenarioRuleInstanceID using scenario id
     *  
     *  @author  Aakash Gupta
     *  @version 1.0
     *  @since   2017-09-20  
    */
    @AuraEnabled
    public static List<Scenario_Rule_Instance_Details__c>  getScenrioRuleInstanceId(String scenarioId){
    
     List<Scenario_Rule_Instance_Details__c> scenarioRuleInsId = [SELECT Id,Scenario_Id__c,componenttypeLabel__c FROM Scenario_Rule_Instance_Details__c where scenario_Id__c=:scenarioId and componentTypeLabel__c = 'Flags for Inclusion / Exclusion and Other Fields' ];
      return  scenarioRuleInsId;                                                          
                                                                 
    }
    
    
    /**
     * calling Table Name for CPDDerivedFieldsList screen display
     *  
     *  @author  Aakash Gupta
     *  @version 1.0
     *  @since   2017-09-05  
    */
    @AuraEnabled
    public static List<Data_Set_Rule_Map__c> getInputTableName(String scenarioRuleInstanceDetailsId){
     
    	List<Data_Set_Rule_Map__c> inputTable = [SELECT Table_Display_Name__c FROM Data_Set_Rule_Map__c WHERE scenario_rule_instance_id__c = :scenarioRuleInstanceDetailsId AND ds_type__c = 'I'  ];
    	//String inputTableName = inputTable[0].Input_Table__c;
    	system.debug('Output Value:' +inputTable);
    	return inputTable;
    }
    
     /**
     * calling scenarioID for navigation to canvas
     *  
     *  @author  Aakash Gupta
     *  @version 1.0
     *  @since   2017-09-05  
    */
    @AuraEnabled
    public static List<Scenario_Rule_Instance_Details__c> getScenarioID(String ScenarioRuleInstanceId){
     
    	List<Scenario_Rule_Instance_Details__c> sceID = [SELECT Id, Scenario_Id__c FROM Scenario_Rule_Instance_Details__c where id=:ScenarioRuleInstanceId];
    	//String inputTableName = inputTable[0].Input_Table__c;
    	//system.debug('Output Value:' +inputTable);
    	return sceID;
    }
    
     
}