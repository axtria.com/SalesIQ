/*
Author : Ankit Manendra
Description : Test class for CPGCallBalancingCtrl
Date : 9 Novomber, 2017

*/

@isTest
private class TestCPGCallBalancingCtrl{
    static testMethod void cpgCallBalTest() {

    Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj;
        
        ComponentTypeMaster__c ComTypeMasObj1 = new ComponentTypeMaster__c();
        ComTypeMasObj1.Name='Aggregation';
        ComTypeMasObj1.Component_Definition__c='CPGCallPlanBalancingRules';
        ComTypeMasObj1.DisplayIcon__c='add';
        ComTypeMasObj1.DisplayIconClass__c='window information';
        insert ComTypeMasObj1;
        
        ComponentTypeMaster__c ComTypeMasObj2 = new ComponentTypeMaster__c();
        ComTypeMasObj2.Name='Aggregation';
        ComTypeMasObj2.Component_Definition__c='CPGAggregation';
        ComTypeMasObj2.DisplayIcon__c='add';
        ComTypeMasObj2.DisplayIconClass__c='window information';
        insert ComTypeMasObj2;
        
        Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        insert scenarioObj;
        
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        sceRuleInsDetObj.Component_Type_Master__c=ComTypeMasObj2.id;
        sceRuleInsDetObj.Exec_Seq__c=2;
        insert sceRuleInsDetObj;
        
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj3 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj3.Scenario_Id__c= scenarioObj.id;
        insert sceRuleInsDetObj3;
        
         Scenario_Rule_Instance_Details__c sceRuleInsDetObj1 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj1.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj1.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        sceRuleInsDetObj1.Exec_Seq__c=4;
        insert sceRuleInsDetObj1;
        
        Data_Set_Rule_Map__c dataSetRuleObj = new Data_Set_Rule_Map__c();
        dataSetRuleObj.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj.ds_type__c='I';
        insert dataSetRuleObj;
        
        system.debug('==dataSetRuleObj===='+dataSetRuleObj);
        
        Business_Rule_Type_Master__c busRuleTypeMasObj = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj.Name='Rank';
        busRuleTypeMasObj.Component_Type__c=ComTypeMasObj1.id;
        insert busRuleTypeMasObj;
        
        Business_Rules__c busRuleObj = new Business_Rules__c();
        busRuleObj.Name='Rank rule 278';
        busRuleObj.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObj.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        insert busRuleObj;
        
        Business_Rules__c busRuleObj1 = new Business_Rules__c();
        busRuleObj1.Name='Rank rule 278';
        busRuleObj1.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObj1.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj1.id;
        insert busRuleObj1;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj.Param_Name__c='derived field 278';
        busRuleFieldObj.Business_Rule__c=busRuleObj.id;
        busRuleFieldObj.Param_Data_Type__c='Numeric';
        busRuleFieldObj.Param_Type__c='Derived';
        busRuleFieldObj.IO_Flag__c='O';
        
        insert busRuleFieldObj;
        
        Computation_Rule_Detail__c compRuleDetObj = new Computation_Rule_Detail__c();
        compRuleDetObj.Business_Rules__c=busRuleObj.id;
        compRuleDetObj.Data_Type__c='Numeric';
        compRuleDetObj.Derived_field_name__c='FIELD 123';
        
        insert compRuleDetObj;
        
        Category_Derived_Variable_Metadata__c catDerMetObj = new Category_Derived_Variable_Metadata__c();
        catDerMetObj.Business_Rule_Id__c=busRuleObj1.id;
        catDerMetObj.Value_Type__c='Discrete';
        insert catDerMetObj;
        
        
        Test.startTest();
         
        List<Scenario_Rule_Instance_Details__c> sceRuleList = CPGCallBalancingCtrl.getScenarioID(sceRuleInsDetObj.id);
        List<BRCallBalancingWrapper> callBalWrap = CPGCallBalancingCtrl.getcallBalancingRuleName(sceRuleInsDetObj.id);
        List<BRCallBalancingParamsWrapper> callBalWrap1 = CPGCallBalancingCtrl.getConstraintValues(sceRuleInsDetObj.id);
        string arrConfigCallBalancingMap='[{"Id":"a1Jf4000000pHIzEAM","'+SObjectDescribe.namespacePrefix+'Run_order__c":"1","'+SObjectDescribe.namespacePrefix+'Constraint_1__c":"TerrCallConstraint","'+SObjectDescribe.namespacePrefix+'Constraint_2__c":"TerrTargetConstraint","'+SObjectDescribe.namespacePrefix+'aggregate_level__c":"territory_ID","'+SObjectDescribe.namespacePrefix+'Scenario_Rule_Instance_Details__c":"a1Mf4000000ov9qEAA"}]';
        string arrCallBalancingConstraintDetails = '[{"CallBalancingConstraintDetails":[{"'+SObjectDescribe.namespacePrefix+'Constraint_1_Value__c":"Under","'+SObjectDescribe.namespacePrefix+'Constraint_2_Value__c":"Under","'+SObjectDescribe.namespacePrefix+'Constraint_1_Goal__c":"Balanced","'+SObjectDescribe.namespacePrefix+'Constraint_2_Goal__c":"Balanced"},{"'+SObjectDescribe.namespacePrefix+'Constraint_1_Value__c":"Under","'+SObjectDescribe.namespacePrefix+'Constraint_2_Value__c":"Balanced","'+SObjectDescribe.namespacePrefix+'Constraint_1_Goal__c":"Balanced","'+SObjectDescribe.namespacePrefix+'Constraint_2_Goal__c":"Balanced"},{"'+SObjectDescribe.namespacePrefix+'Constraint_1_Value__c":"Under","Constraint_2_Value__c":"Over","'+SObjectDescribe.namespacePrefix+'Constraint_1_Goal__c":"Balanced","'+SObjectDescribe.namespacePrefix+'Constraint_2_Goal__c":"Balanced"},{"'+SObjectDescribe.namespacePrefix+'Constraint_1_Value__c":"Balanced","'+SObjectDescribe.namespacePrefix+'Constraint_2_Value__c":"Under","'+SObjectDescribe.namespacePrefix+'Constraint_1_Goal__c":"Balanced","'+SObjectDescribe.namespacePrefix+'Constraint_2_Goal__c":"Balanced"},{"C'+SObjectDescribe.namespacePrefix+'onstraint_1_Value__c":"Balanced","C'+SObjectDescribe.namespacePrefix+'onstraint_2_Value__c":"Balanced","'+SObjectDescribe.namespacePrefix+'Constraint_1_Goal__c":"Balanced","'+SObjectDescribe.namespacePrefix+'Constraint_2_Goal__c":"Balanced"},{"'+SObjectDescribe.namespacePrefix+'Constraint_1_Value__c":"Balanced","'+SObjectDescribe.namespacePrefix+'Constraint_2_Value__c":"Over","C'+SObjectDescribe.namespacePrefix+'onstraint_1_Goal__c":"Balanced","C'+SObjectDescribe.namespacePrefix+'onstraint_2_Goal__c":"Balanced"},{"C'+SObjectDescribe.namespacePrefix+'onstraint_1_Value__c":"Over","C'+SObjectDescribe.namespacePrefix+'onstraint_2_Value__c":"Under","'+SObjectDescribe.namespacePrefix+'Constraint_1_Goal__c":"Balanced","'+SObjectDescribe.namespacePrefix+'Constraint_2_Goal__c":"Balanced"},{"'+SObjectDescribe.namespacePrefix+'Constraint_1_Value__c":"Over","'+SObjectDescribe.namespacePrefix+'Constraint_2_Value__c":"Balanced","'+SObjectDescribe.namespacePrefix+'Constraint_1_Goal__c":"Balanced","'+SObjectDescribe.namespacePrefix+'Constraint_2_Goal__c":"Balanced"},{"'+SObjectDescribe.namespacePrefix+'Constraint_1_Value__c":"Over","'+SObjectDescribe.namespacePrefix+'Constraint_2_Value__c":"Over","'+SObjectDescribe.namespacePrefix+'Constraint_1_Goal__c":"Balanced","'+SObjectDescribe.namespacePrefix+'Constraint_2_Goal__c":"Balanced"}]}]';
        string arrConfigCallBalancingConstraintRuleMap = '[[{"CallBalancingConstraintRuleMaps":[{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFfAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":1},{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFgAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":2},{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFjAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":3}]},{"CallBalancingConstraintRuleMaps":[{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFfAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":1}]},{"CallBalancingConstraintRuleMaps":[{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFfAAI","Exec_Seq__c":1},{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFiAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":2},{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFkAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":3}]},{"CallBalancingConstraintRuleMaps":[{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFgAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":1},{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFjAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":2}]},{"CallBalancingConstraintRuleMaps":[]},{"CallBalancingConstraintRuleMaps":[{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFiAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":1},{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFkAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":2}]},{"CallBalancingConstraintRuleMaps":[{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFgAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":1},{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFhAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":2},{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFjAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":3}]},{"CallBalancingConstraintRuleMaps":[{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFhAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":1}]},{"CallBalancingConstraintRuleMaps":[{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFhAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":1},{"Business_Rules__c":"a1hf4000000huFiAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":2},{"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":"a1hf4000000huFkAAI","'+SObjectDescribe.namespacePrefix+'Exec_Seq__c":3}]}]]';
        try{
            CPGCallBalancingCtrl.ValidateRuleData(sceRuleInsDetObj3.Id);
            CPGCallBalancingCtrl.ValidateGuardRails(sceRuleInsDetObj.Id);
        }catch(Exception e){
        }
        
        
       
        CPGConstants.Response res = CPGCallBalancingCtrl.saveCallBalanceConfigRulesServer(arrConfigCallBalancingMap,arrCallBalancingConstraintDetails,arrConfigCallBalancingConstraintRuleMap,sceRuleInsDetObj.id);
        CPGCallBalancingCtrl.ConstraintValuesWrapper consWrap = CPGCallBalancingCtrl.fillConstraintWrapper(sceRuleInsDetObj.id);
        
        Test.stopTest();
        }
        
 }