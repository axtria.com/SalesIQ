/******************************************************************************************************************************************************
 * Name         :   AlignmentPeriodManager.cls
 * Description  :   Scheduler class to be executed daily to make scenario team instance operational and operational to past.
 * Author       :   Raghvendra Rathore
 * Created On   :   06/16/2017
******************************************************************************************************************************************************/
global class AlignmentPeriodManager implements Schedulable {
    global void execute(SchedulableContext SC) {
        try{
            list<Scenario__c> scenariosToUpdate = new list<Scenario__c>();
            list<Team_Instance__c> TeamInstancesToUpdate = new list<Team_Instance__c>();

            //Fetch the published scenarios with start date as today
            List<String> SCENARIO_READ_FIELD = new List<String>{'Scenario_Stage__c', 'Team_Name__c','Team_Instance__c','Request_Process_Stage__c','Effective_Start_Date__c','Scenario_Status__c'};
            SecurityUtil.checkRead(Scenario__c.SObjectType, SCENARIO_READ_FIELD, false) ;
            list<Scenario__c> scenarioList = [SELECT id, Scenario_Stage__c, Team_Name__c, Team_Instance__c FROM Scenario__c WHERE 
                                              Scenario_Stage__c =: SalesIQGlobalConstants.PUBLISHING AND 
                                              Request_Process_Stage__c =: SalesIQGlobalConstants.READY AND 
                                              Effective_Start_Date__c =: System.today() AND 
                                              Scenario_Status__c = 'Active'];

            //Mark the published scenarios to live
            if(scenarioList.size() > 0){
                set<Id> teamIds = new set<Id>();
                set<Id> teamInstanceIds = new set<Id>();
                for(Scenario__c s : scenarioList){
                    s.Scenario_Stage__c = SalesIQGlobalConstants.LIVE;
                    scenariosToUpdate.add(s);
                    teamIds.add(s.Team_Name__c);
                    teamInstanceIds.add(s.Team_Instance__c);
                }

                //Fetch the team instance of new live scenario and mark it current
                List<String> TEAM_INSTANCE_READ_FIELD = new List<String>{'Alignment_Period__c'};
                SecurityUtil.checkRead(Team_Instance__c.SObjectType, TEAM_INSTANCE_READ_FIELD, false) ;
                list<Team_Instance__c> teamInstances = [SELECT id, Alignment_Period__c FROM Team_Instance__c WHERE id IN : teamInstanceIds];
                for(Team_Instance__c t : teamInstances){
                    t.Alignment_Period__c = SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE;
                    t.AccountMovement__c = false;
                    t.ReadOnlyCreatePosition__c = false;
                    t.ReadOnlyDeletePosition__c = false;
                    t.ReadOnlyEditPosition__c = false;
                    t.Restrict_Hierarchy_Change__c = false;
                    t.IsAllowedManageAssignment__c = false;
                    t.Restrict_Update_Position_Attribute__c = false;
                    t.ZIPMovement__c = false;
                    t.Restrict_ZIP_Share__c = false;
                    TeamInstancesToUpdate.add(t);
                }

                //Fetch existing active live scenario and mark it past
                set<Id> currentTeamInstanceIds = new set<Id>();
                SCENARIO_READ_FIELD = new List<String>{'Scenario_Stage__c', 'Team_Name__c','Scenario_Status__c'};
                SecurityUtil.checkRead(Scenario__c.SObjectType, SCENARIO_READ_FIELD, false) ;
                list<Scenario__c> currentLiveScenarioList = [SELECT id, Team_Instance__c, Scenario_Stage__c FROM Scenario__c WHERE Team_Name__c IN : teamIds AND Scenario_Stage__c =: SalesIQGlobalConstants.LIVE AND Scenario_Status__c = 'Active'];
                for(Scenario__c s : currentLiveScenarioList){
                    s.Scenario_Stage__c = SalesIQGlobalConstants.PAST;
                    scenariosToUpdate.add(s);
                    currentTeamInstanceIds.add(s.Team_Instance__c);
                }

                //Fetch existing current team instance of new past scenario and mark it past
                TEAM_INSTANCE_READ_FIELD = new List<String>{'Alignment_Period__c','Team__c','Alignment_Period__c'};
                SecurityUtil.checkRead(Team_Instance__c.SObjectType, TEAM_INSTANCE_READ_FIELD, false) ;
                list<Team_Instance__c> currentTeamInstances = [SELECT id, Alignment_Period__c, AccountMovement__c, ReadOnlyCreatePosition__c, ReadOnlyDeletePosition__c, ReadOnlyEditPosition__c, 
                                                               Restrict_Hierarchy_Change__c, IsAllowedManageAssignment__c, Restrict_Update_Position_Attribute__c, ZIPMovement__c, Restrict_ZIP_Share__c 
                                                               FROM Team_Instance__c WHERE Team__c IN : teamIds AND Id IN : currentTeamInstanceIds AND 
                                                               Alignment_Period__c =: SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE];
                for(Team_Instance__c t : currentTeamInstances){
                    t.Alignment_Period__c = SalesIQGlobalConstants.PAST_TEAM_CYCLE_TYPE;
                    t.AccountMovement__c = true;
                    t.ReadOnlyCreatePosition__c = true;
                    t.ReadOnlyDeletePosition__c = true;
                    t.ReadOnlyEditPosition__c = true;
                    t.Restrict_Hierarchy_Change__c = true;
                    t.IsAllowedManageAssignment__c = false;
                    t.Restrict_Update_Position_Attribute__c = true;
                    t.ZIPMovement__c = true;
                    //t.Restrict_ZIP_Share__c = true;
                    TeamInstancesToUpdate.add(t);
                }

                List<String> SCENARIO_UPDATE_FIELD = new List<String>{'Scenario_Stage__c'};
                SecurityUtil.checkUpdate(Scenario__c.SObjectType, SCENARIO_UPDATE_FIELD, false) ;
                update scenariosToUpdate;

                List<String> TEAM_INSTANCE_UPDATE_FIELD = new List<String>{'Alignment_Period__c'};
                SecurityUtil.checkUpdate(Team_Instance__c.SObjectType, TEAM_INSTANCE_UPDATE_FIELD, false) ;
                update TeamInstancesToUpdate;
            }
        }catch(Exception e){
            system.debug('#### ERROR : '+e.getMessage());
        }
    }
}