/**
Name        :   AxtriaSalesIQTMPostInstallScript
Author      :   <Lead Developer>
Created     :   18/11/2019
Updated     :   27/11/2019
Description :   InstallHandler class to Create new ETL config for ScenarioOperations and merge data of existing records.
*/

global class AxtriaSalesIQTMPostInstallScript implements InstallHandler {    
    string ns =SalesIQGlobalConstants.NAME_SPACE; 
    global void onInstall(InstallContext context){
        if( (context.previousVersion() != null))
        {
        mergeETLConfigs();
        deleteRedundantConfigs();
        createChangeRequestSectionForZIPAccountInfoInterface();
        pushTopic();
        }
    }

    // Create new ETL config for ScenarioOperations and merge data of existing records with new one as supported in SalesIQ 7.3 version of SalesIQ.
   public void mergeETLConfigs(){
        
        //return if ScenarioOperations record already exists
        if (null!=getEtlConfigForName('ScenarioOperations')) {return;}
    
        ETL_Config__c scenarioOpsEtlConfig = new ETL_Config__c();
        scenarioOpsEtlConfig.NAme='ScenarioOperations';
        
        // Fetching data from existing configs
        ETL_Config__c  brmsEtlConfig = getEtlConfigForName('BRMS');     
        ETL_Config__c  talendEtlConfig = getEtlConfigForName('Talend');

        
        // Service end point
        scenarioOpsEtlConfig.End_Point__c = '';
        
        // ESRI server details
        scenarioOpsEtlConfig.Esri_DB__c = talendEtlConfig.Esri_DB__c;
        scenarioOpsEtlConfig.ESRI_UserName__c = talendEtlConfig.ESRI_UserName__c; 
        scenarioOpsEtlConfig.ESRI_Password__c = talendEtlConfig.ESRI_Password__c;
        scenarioOpsEtlConfig.Esri_Server_IP__c = talendEtlConfig.Esri_Server_IP__c;
        
        // SFTP Storage details
        scenarioOpsEtlConfig.Server_Type__c = talendEtlConfig.Server_Type__c;
        scenarioOpsEtlConfig.SFTP_Folder__c = talendEtlConfig.SFTP_Folder__c;
        scenarioOpsEtlConfig.SFTP_Host__c = talendEtlConfig.SFTP_Host__c;
        scenarioOpsEtlConfig.SFTP_Password__c = talendEtlConfig.SFTP_Password__c;
        scenarioOpsEtlConfig.SFTP_Port__c = talendEtlConfig.SFTP_Port__c;
        scenarioOpsEtlConfig.SFTP_Username__c = talendEtlConfig.SFTP_Username__c;
        
        // SFDC integration details
        scenarioOpsEtlConfig.SF_Password__c = talendEtlConfig.SF_Password__c;
        scenarioOpsEtlConfig.SF_UserName__c = talendEtlConfig.SF_UserName__c;
        scenarioOpsEtlConfig.S3_Security_Token__c = brmsEtlConfig.S3_Security_Token__c;

        
        // Postgres DB details
        scenarioOpsEtlConfig.BR_PG_Database__c = brmsEtlConfig.BR_PG_Database__c;
        scenarioOpsEtlConfig.BR_PG_Host__c = brmsEtlConfig.BR_PG_Host__c;
        scenarioOpsEtlConfig.BR_PG_Password__c = brmsEtlConfig.BR_PG_Password__c;
        scenarioOpsEtlConfig.BR_PG_UserName__c = brmsEtlConfig.BR_PG_UserName__c;
        //scenarioOpsEtlConfig.CurrencyIsoCode = brmsEtlConfig.CurrencyIsoCode;
        
        // S3 bucket details and Org type
        scenarioOpsEtlConfig.S3_Access_Key__c = brmsEtlConfig.S3_Access_Key__c;
        scenarioOpsEtlConfig.S3_Bucket__c = brmsEtlConfig.S3_Bucket__c;
        scenarioOpsEtlConfig.S3_Filename__c = brmsEtlConfig.S3_Filename__c;
        scenarioOpsEtlConfig.S3_Key_Name__c  = brmsEtlConfig.S3_Key_Name__c;
        scenarioOpsEtlConfig.Org_Type__c = brmsEtlConfig.Org_Type__c;
    
        
        insert scenarioOpsEtlConfig;


    }

    // PushTopic
    public void pushTopic(){
        List<PushTopic> pushTopicRecordBulkUpdates = [Select id, name, ApiVersion, NotifyForOperationCreate from PushTopic where name = 'ChangeRequestBulkUpdates'];
        if(pushTopicRecordBulkUpdates.size()==0){
            PushTopic pushTopicReq = new PushTopic();
            pushTopicReq.Name = 'ChangeRequestBulkUpdates';
            pushTopicReq.Query = 'SELECT Id,CreatedById,axtriasalesiqtm__execution_Status__c FROM axtriasalesiqtm__Change_Request__c WHERE axtriasalesiqtm__Request_Type_Change__c= \'Bulk ZIP Movement\' and axtriasalesiqtm__execution_Status__c=\'Completed\'';
            pushTopicReq.ApiVersion = 36.0;
            pushTopicReq.NotifyForOperationCreate = false;
            pushTopicReq.NotifyForOperationUpdate = true;
            pushTopicReq.NotifyForOperationUndelete = false;
            pushTopicReq.NotifyForOperationDelete = false;
            pushTopicReq.NotifyForFields = 'Referenced';
            insert pushTopicReq;
        }
        List<PushTopic> pushTopicRecordBulkFails = [Select id, name, ApiVersion, NotifyForOperationCreate from PushTopic where name = 'ChangeRequestFails'];
        if(pushTopicRecordBulkFails.size()==0){
            PushTopic pushTopicReq = new PushTopic();
            pushTopicReq.Name = 'ChangeRequestFails';
            pushTopicReq.Query = 'SELECT Id,CreatedById,axtriasalesiqtm__execution_Status__c FROM axtriasalesiqtm__Change_Request__c WHERE axtriasalesiqtm__Request_Type_Change__c= \'ZIP Movement\' and axtriasalesiqtm__execution_Status__c=\'Error Occurred\'';
            pushTopicReq.ApiVersion = 36.0;
            pushTopicReq.NotifyForOperationCreate = false;
            pushTopicReq.NotifyForOperationUpdate = true;
            pushTopicReq.NotifyForOperationUndelete = false;
            pushTopicReq.NotifyForOperationDelete = false;
            pushTopicReq.NotifyForFields = 'Referenced';
            insert pushTopicReq;

        }

    }

    // delete redundant configs
    public void deleteRedundantConfigs(){
    
    //return if ScenarioOperations record does not exist
        if (null == getEtlConfigForName('ScenarioOperations')) {return;}
        
    
    // Fetching data from existing configs
        ETL_Config__c  brmsEtlConfig = getEtlConfigForName('BRMS');     
        ETL_Config__c  talendEtlConfig = getEtlConfigForName('Talend');
        
        if(brmsEtlConfig!=null){
           delete brmsEtlConfig;}
        
        if(talendEtlConfig!=null){
            delete talendEtlConfig;
        }
    }
    
    // get ETL Config for Name
     public ETL_Config__c getEtlConfigForName(String etlConfigName)
    
    {
        List<ETL_Config__c> etlConfigList =  [SELECT BDT_DataSet_Id__c,BR_Data_Object_Id__c,BR_PG_Database__c,BR_PG_Host__c,BR_PG_Password__c,BR_PG_Port__c,BR_PG_Schema__c,BR_PG_UserName__c,BR_Secret_Key__c,CreatedById,CreatedDate,End_Point__c,Esri_DB__c,ESRI_Password__c,Esri_Server_IP__c,ESRI_UserName__c,GI_Dataset_Id__c,Id,IsDeleted,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Name,Org_Type__c,OwnerId,S3_Access_Key__c,S3_Bucket__c,S3_Filename__c,S3_Key_Name__c,S3_Security_Token__c,Server_Type__c,SFTP_Folder__c,SFTP_Host__c,SFTP_Password__c,SFTP_Port__c,SFTP_Username__c,SF_Password__c,SF_UserName__c FROM ETL_Config__c where Name = :etlConfigName];
        
        if(etlConfigList.size()==0)
         return null;
        
        ETL_Config__c etlConfigForName = etlConfigList[0];
        return etlConfigForName;
    }
        
     public List<Team_Instance_Object_Attribute__c> getZipAccountInfoList(id teamInsId)
    {
        
        List<Team_Instance_Object_Attribute__c> changerequestSectionList=new List<Team_Instance_Object_Attribute__c>();
        //string nsone =SalesIQGlobalConstants.NAME_SPACE;
        string objNameone='CR_Account__c';
        string objNametwo='CR_Geography__c';
        if(!string.isBlank(ns)){
            objNameone=ns+objNameone;
            objNametwo=ns+objNametwo;
        }
        Team_Instance_Object_Attribute__c createAccinfo =new Team_Instance_Object_Attribute__c();
        createAccinfo.Attribute_API_Name__c='Name';
        createAccinfo.Object_Name__c= objNameone;
        createAccinfo.Team_Instance__c = teamInsId;
        createAccinfo.Section__c='Label.ChangeRequestInformation';
        createAccinfo.Display_Column_Order__c = 0;
        createAccinfo.Interface_Name__c= 'AccInfo';
        createAccinfo.isEnabled__c= True;
        createAccinfo.Data_Type__c='Text';
        createAccinfo.Attribute_Display_Name__c= 'Label.ChangeRequestInformation';
        
        Team_Instance_Object_Attribute__c createZIPInfo =new Team_Instance_Object_Attribute__c();
        createZIPInfo.Interface_Name__c='ZIPInfo';
        createZIPInfo.Attribute_API_Name__c='Name';
        createZIPInfo.Object_Name__c= objNametwo;
        createZIPInfo.Team_Instance__c = teamInsId;
        createZIPInfo.Section__c='Label.ChangeRequestInformation';
        createZIPInfo.Display_Column_Order__c = 0;
        createZIPInfo.isEnabled__c= True;
        createZIPInfo.Data_Type__c='Text';
        createZIPInfo.Attribute_Display_Name__c= 'Label.ChangeRequestInformation';
        
        
        changerequestSectionList.add(createAccinfo);
        changerequestSectionList.add(createZIPInfo);
        return changerequestSectionList;
        
    }
    public List<Team_Instance_Object_Attribute__c> getManageVacantList(id teamInsId){
        
        List<Team_Instance_Object_Attribute__c> manageVacantList=new List<Team_Instance_Object_Attribute__c>();
        String objNamethree='Position__c';
        String attribDisplayName='Manage_Vacant__c';
        //String position='Position__r.';
        if(!string.isBlank(ns)){
        objNamethree=ns+objNamethree;
        attribDisplayName=ns+attribDisplayName;
        }
        //else{
        //attribDisplayName=position+attribDisplayName;
        //}
        //String objNamefour=;
        Team_Instance_Object_Attribute__c alignmentcreatePositionPopup = new Team_Instance_Object_Attribute__c();
        alignmentcreatePositionPopup.Team_Instance__c = teamInsId;
        alignmentcreatePositionPopup.Attribute_Display_Name__c='Manage Vacant';
        alignmentcreatePositionPopup.Attribute_API_Name__c=attribDisplayName;
        alignmentcreatePositionPopup.Data_Type__c='Boolean';
        alignmentcreatePositionPopup.Display_Column_Order__c=7;
        alignmentcreatePositionPopup.Interface_Name__c='Alignment: Create Position Popup';
        alignmentcreatePositionPopup.Object_Name__c=objNamethree;
        alignmentcreatePositionPopup.isEditable__c=TRUE;
        alignmentcreatePositionPopup.isEnabled__c=TRUE;
        alignmentcreatePositionPopup.isRequired__c=TRUE;
        alignmentcreatePositionPopup.Lookup_Detail_String__c='158';
        
        Team_Instance_Object_Attribute__c alignmentupdatePositionAttributesPopup = new Team_Instance_Object_Attribute__c();
        alignmentupdatePositionAttributesPopup.Team_Instance__c = teamInsId;
        alignmentupdatePositionAttributesPopup.Attribute_Display_Name__c='Manage Vacant';
        alignmentupdatePositionAttributesPopup.Attribute_API_Name__c=attribDisplayName;
        alignmentupdatePositionAttributesPopup.Data_Type__c='Boolean';
        alignmentupdatePositionAttributesPopup.Display_Column_Order__c=7;
        alignmentupdatePositionAttributesPopup.Interface_Name__c='Alignment: Update Position Attributes Popup';
        alignmentupdatePositionAttributesPopup.Object_Name__c=objNamethree;
        alignmentupdatePositionAttributesPopup.isEditable__c=TRUE;
        alignmentupdatePositionAttributesPopup.isEnabled__c=TRUE;
        alignmentupdatePositionAttributesPopup.isRequired__c=TRUE;
        alignmentupdatePositionAttributesPopup.Lookup_Detail_String__c='159';
        
        Team_Instance_Object_Attribute__c rosterCreatePositionPopup= new Team_Instance_Object_Attribute__c();
        rosterCreatePositionPopup.Team_Instance__c = teamInsId;
        rosterCreatePositionPopup.Attribute_Display_Name__c='Manage Vacant';
        rosterCreatePositionPopup.Attribute_API_Name__c=attribDisplayName;
        rosterCreatePositionPopup.Data_Type__c='Boolean';
        rosterCreatePositionPopup.Display_Column_Order__c=7;
        rosterCreatePositionPopup.Interface_Name__c='Roster: Create Position Popup';
        rosterCreatePositionPopup.Object_Name__c=objNamethree;
        rosterCreatePositionPopup.isEditable__c=TRUE;
        rosterCreatePositionPopup.isEnabled__c=TRUE;
        rosterCreatePositionPopup.isRequired__c=TRUE;
        rosterCreatePositionPopup.Lookup_Detail_String__c='160';
        
        Team_Instance_Object_Attribute__c rosterUpdatePositionAttributesPopup= new Team_Instance_Object_Attribute__c();
        rosterUpdatePositionAttributesPopup.Team_Instance__c = teamInsId;
        rosterUpdatePositionAttributesPopup.Attribute_Display_Name__c='Manage Vacant';
        rosterUpdatePositionAttributesPopup.Attribute_API_Name__c=attribDisplayName;
        rosterUpdatePositionAttributesPopup.Data_Type__c='Boolean';
        rosterUpdatePositionAttributesPopup.Display_Column_Order__c=7;
        rosterUpdatePositionAttributesPopup.Interface_Name__c='Roster: Update Position Attributes Popup';
        rosterUpdatePositionAttributesPopup.Object_Name__c=objNamethree;
        rosterUpdatePositionAttributesPopup.isEditable__c=TRUE;
        rosterUpdatePositionAttributesPopup.isEnabled__c=TRUE;
        rosterUpdatePositionAttributesPopup.isRequired__c=TRUE;
        rosterUpdatePositionAttributesPopup.Lookup_Detail_String__c='161';
        
        manageVacantList.add(alignmentcreatePositionPopup);
        manageVacantList.add(alignmentupdatePositionAttributesPopup);
        manageVacantList.add(rosterCreatePositionPopup);
        manageVacantList.add(rosterUpdatePositionAttributesPopup);
        return manageVacantList;
    
    }
    
     public void createChangeRequestSectionForZIPAccountInfoInterface()
    {
        List<Team_Instance_Object_Attribute__c> returningValue=new List<Team_Instance_Object_Attribute__c>();
        List<String> getvalidList = new List<String>();
        List<String> getInterfaceNameList= new List<String>{'Alignment: Create Position Popup','Alignment: Update Position Attributes Popup','Roster: Create Position Popup','Roster: Update Position Attributes Popup','AccInfo','ZipInfo'};
        getvalidList.addall(SalesIQUtility.getValidScenarios());
        //string ns =SalesIQGlobalConstants.NAME_SPACE;
        string attApiName='Manage_Vacant__c';
        if(!string.isBlank(ns)){
            attApiName=ns+attApiName;
        }
        List<Team_instance__C> teamInstanceList =  [Select id,name, scenario__r.name,scenario__c from Team_instance__C where scenario__r.Request_Process_Stage__c=:getvalidList];
        List<Team_Instance_Object_Attribute__c> teamInstanceObjectAttributeList =  [SELECT id,Team_Instance__c,Display_Column_Order__c,Attribute_API_Name__c,Team_Instance__r.Alignment_Type__c,Interface_Name__c,Section__c FROM Team_Instance_Object_Attribute__c where Team_instance__C IN :teamInstanceList AND Interface_Name__c IN :getInterfaceNameList];
        Map<Id,List<Team_Instance_Object_Attribute__c>> mapTeamInstanceObjectAttribute=new Map<Id,List<Team_Instance_Object_Attribute__c>>();
        
         for(Team_Instance_Object_Attribute__c teamone: teamInstanceObjectAttributeList)
        {
          if(mapTeamInstanceObjectAttribute.containsKey(teamone.Team_Instance__c))
            {
           list<Team_Instance_Object_Attribute__c> updatedTioaList = mapTeamInstanceObjectAttribute.get(teamone.Team_Instance__c);
           updatedTioaList.add(teamone);
           mapTeamInstanceObjectAttribute.put(teamone.Team_Instance__c,updatedTioaList);
            }
          else
            {
            //List<Team_Instance_Object_Attribute__c> newTioaList =new List<Team_Instance_Object_Attribute__c>();
            //newTioaList.add(teamone);
            mapTeamInstanceObjectAttribute.put(teamone.Team_Instance__c,new list<Team_Instance_Object_Attribute__c>{teamone});
            }
        }
         for(Id teamInsId : mapTeamInstanceObjectAttribute.keySet())
        {
                  boolean changeReqSec=false;
                  boolean manageVacant=false;
                  for(Team_Instance_Object_Attribute__c teamInsObj : mapTeamInstanceObjectAttribute.get(teamInsId))
                  {    if(teamInsObj.Interface_Name__c== SalesIQGlobalConstants.INFO_TYPE_ZIP && teamInsObj.Section__c!='Label.ChangeRequestInformation' && teamInsObj.Display_Column_Order__c==0)
                              {
                              teamInsObj.Display_Column_Order__c=1;
                              returningValue.add(teamInsObj);
                              }
                      if(teamInsObj.Section__c=='Label.ChangeRequestInformation' && (teamInsObj.Interface_Name__c==SalesIQGlobalConstants.INFO_TYPE_ACCOUNT || teamInsObj.Interface_Name__c==SalesIQGlobalConstants.INFO_TYPE_ZIP))
                      {
                          changeReqSec=true;
                          //create teaminstance object attribute with interfacr name accountinfo
                      }
                      else if(teamInsObj.Attribute_API_Name__c==attApiName)
                      {
                          manageVacant=true;
                      }
                  }
                  if(changeReqSec==false)
                     {
                  returningValue.addAll(getZipAccountInfoList(teamInsId));
                     }
                  if(manageVacant==false)
                    {
                   returningValue.addAll(getManageVacantList(teamInsId));
                    }
        } 
            upsert returningValue;
            
    }
}