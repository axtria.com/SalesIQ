@isTest
public with sharing class TestProcessAccountExclusionBatch{
    
    //Below method is used when 1 accounts is part of CR Account and another account is part of Account_exclusion__c object
    static testMethod void accountExclusionforHybridAccountMovementMultipleCRAccount(){ 

        list<Profile> hoProfile = [SELECT Id FROM Profile WHERE Name =: SalesIQGlobalConstants.HR_PROFILE limit 1];
        if(hoProfile.size() == 0){
            hoProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1];
        }
        
        User usr = new User(LastName = 'ho',
                            FirstName='test',
                            Alias = 'test',
                            Email = 'test.ho@test.com',
                            Username = 'test.ho@test.com',
                            ProfileId = hoProfile[0].id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US');
        insert usr;


        Organization_Master__c org = CallPlanTestDataFactory.createOrganizationMasterRecord();
        insert org;
        Country__c country = CallPlanTestDataFactory.createCountryMasterRecord(org.id);
        insert country;
        Geography_Type__c geotype = CallPlanTestDataFactory.createGeographyTypeRecord(country.id);
        insert geotype;


        Team__c baseTeam = CallPlanTestDataFactory.createTeam('Base');
        baseTeam.Country__c=country.id;
        insert baseTeam;        

        

        Workspace__c wrkspc = CallPlanTestDataFactory.createWorkspace();
        wrkspc.Country__c=country.id;
        insert wrkspc;

        Scenario__c scenario = new Scenario__c();
        scenario.CR_Tracking__c = true;
        scenario.Scenario_Name__c = 'HTN_Q3';
        scenario.Workspace__c = wrkspc.id;
        scenario.Team_Name__c = baseTeam.id;
        scenario.Request_Process_Stage__c = SalesIQGlobalConstants.READY;     
        scenario.Scenario_Stage__c = SalesIQGlobalConstants.COLLABORATION;   
        scenario.Scenario_Status__c = 'Active';
        insert scenario;  

        Team_Instance__c baseTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(baseTeam,'Hybrid');        
        baseTeamInstance.Geography_Type_Name__c = geotype.id;
        baseTeamInstance.Allow_Account_Exclusion__c = true;
        baseTeamInstance.Scenario__c = scenario.id;
        insert baseTeamInstance;

        Position__c  parentParent = null;


        Position__c territory  = CallPlanTestDataFactory.createPosition(parentParent,baseTeam);
        territory.Team_Instance__c = baseTeamInstance.id;
        insert territory;



        Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, parentParent, BaseTeamInstance);       
        insert territoryTeamInstance;

        Position__c destTerritory  = CallPlanTestDataFactory.createDestinationPosition(parentParent,baseTeam);
        destTerritory.Team_Instance__c = baseTeamInstance.id;
        insert destTerritory;

        Position_Team_Instance__c dstinationTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(destTerritory, parentParent, BaseTeamInstance);       
        insert dstinationTeamInstance;

        Account acc = new Account();
        acc.Name = '11788';
        acc.FirstName__c = 'Abby';
        acc.BillingCity='NJ';
        acc.BillingState='NY';
        acc.AccountNumber='11788';
        acc.type = 'Physician';
        acc.AccountType__c = 'Physician';
        acc.BillingPostalCode = '11788';
        acc.Speciality__c = '11788Oncology';
        acc.Alignment_Type__c = 'Direct Aligned';
        acc.BillingLatitude = 40.5571389900;
        acc.BillingLongitude = -74.2123329900;
        acc.External_Country_Id__c=country.id;
        //insert acc;

        Account acc2 = new Account();
        acc2.Name = '11789';
        acc2.FirstName__c = 'Abby';
        acc2.BillingCity='NJ';
        acc2.BillingState='NY';
        acc2.AccountNumber='11788';
        acc2.type = 'Physician';
        acc2.AccountType__c = 'Physician';
        acc2.BillingPostalCode = '11788';
        acc2.Speciality__c = '11788Oncology';
        acc2.Alignment_Type__c = 'Direct Aligned';
        acc2.BillingLatitude = 40.5571389900;
        acc2.BillingLongitude = -74.2123329900;
        acc2.External_Country_Id__c=country.id;
        //insert acc2;

        Account acc3 = new Account();
        acc3.Name = '11788';
        acc3.FirstName__c = 'Abby';
        acc3.BillingCity='NJ';
        acc3.BillingState='NY';
        acc3.AccountNumber='11780';
        acc3.type = 'Physician';
        acc3.AccountType__c = 'Physician';
        acc3.BillingPostalCode = '11788';
        acc3.Speciality__c = '11788Oncology';
        acc3.Alignment_Type__c = 'Direct Aligned';
        acc3.BillingLatitude = 40.5571389900;
        acc3.BillingLongitude = -74.2123329900;
        acc3.External_Country_Id__c=country.id;
        //insert acc3;

        list<Account> lstAllAccount = new list<Account>();
        lstAllAccount.add(acc);
        lstAllAccount.add(acc2);
        lstAllAccount.add(acc3);
        insert lstAllAccount;

        list<Position_Account__c> lstAllPositionAccount = new list<Position_Account__c>();

        Position_Account__c positionAcc = CallPlanTestDataFactory.createPositionAccount(acc, territory, baseTeamInstance, territoryTeamInstance);
        lstAllPositionAccount.add(positionAcc);

        Position_Account__c positionAcc2 = CallPlanTestDataFactory.createPositionAccount(acc2, destTerritory, baseTeamInstance, dstinationTeamInstance);
        //insert positionAcc2;
        lstAllPositionAccount.add(positionAcc2);

        Position_Account__c positionAcc3 = CallPlanTestDataFactory.createPositionAccount(acc3, territory, baseTeamInstance, territoryTeamInstance);
        //insert positionAcc3;
        lstAllPositionAccount.add(positionAcc3);

        insert lstAllPositionAccount;

        TotalApproval__c totalApp = new TotalApproval__c(Name='Approvals',No_Of_Approval__c=3);
        insert totalApp;
        
        TotalApproval__c QuaterName = new TotalApproval__c(Name='Quarter',Subscriber_Profile__c='Orignal');
        insert QuaterName;


        Change_Request__c objChangeRequest = new Change_Request__c();
        objChangeRequest.Approver1__c = usr.id;
        objChangeRequest.Approver2__c = usr.id;
        objChangeRequest.Approver3__c = usr.id;
        objChangeRequest.Destination_Position__c = destTerritory.id;
        objChangeRequest.Source_Position__c = territory.id;
        objChangeRequest.Account_Moved_Id__c = acc.AccountNumber+','+acc3.AccountNumber;   
        objChangeRequest.Account_SF_Id__c = acc.id+','+acc3.id;
        objChangeRequest.Status__c = 'Pending';
        objChangeRequest.OwnerID  =usr.id;
        objChangeRequest.Team_Instance_ID__c = baseTeamInstance.id;
        objChangeRequest.scenario_name__c='Scenario';
        
        insert objChangeRequest;

        list<CR_Account__c> allCRAccount = new list<CR_Account__c>();

        CR_Account__c objCRAccount = new CR_Account__c();
        objCRAccount.Account__c = acc.id;
        objCRAccount.Destination_Position__c = destTerritory.id;
        objCRAccount.Source_Position__c = territory.id;
        objCRAccount.Change_Request__c = objChangeRequest.Id;
        objCRAccount.IsImpactCallPlan__c = true;
        allCRAccount.add(objCRAccount);

        CR_Account__c objCRAccount3 = new CR_Account__c();
        objCRAccount3.Account__c = acc3.id;
        objCRAccount3.Destination_Position__c = destTerritory.id;
        objCRAccount3.Source_Position__c = territory.id;
        objCRAccount3.Change_Request__c = objChangeRequest.Id;
        objCRAccount3.IsImpactCallPlan__c = true;        
        allCRAccount.add(objCRAccount3);

        insert allCRAccount;



        Change_Request_Type__c objRequestType = CallPlanTestDataFactory.CreateAccountRequestType();
        insert objRequestType;

        list<CIM_Config__c> cimList = CallPlanTestDataFactory.createCIMCOnfig(objRequestType, baseTeamInstance,'Team_Instance_Geography__c');
        //cimList[0].isOptimum__c = true;
        //cimList[0].MetricCalculationType__c = 'Absolute';
        insert cimList;

        list<CIM_Position_Metric_Summary__c> allPostionSummary = new list<CIM_Position_Metric_Summary__c>();
        list<CIM_Position_Metric_Summary__c> sourceCIMSummary =  CallPlanTestDataFactory.createCIMMetricSummary(cimList,territoryTeamInstance,baseTeamInstance);
        allPostionSummary.addAll(sourceCIMSummary);

        list<CIM_Position_Metric_Summary__c> destCIMSummary =  CallPlanTestDataFactory.createCIMMetricSummary(cimList,dstinationTeamInstance,baseTeamInstance);
        //insert destCIMSummary;
        allPostionSummary.addAll(destCIMSummary);

        insert allPostionSummary;

        list<CIM_Change_Request_Impact__c> lstAllReqImpact = new list<CIM_Change_Request_Impact__c> ();
        lstAllReqImpact.addAll(CallPlanTestDataFactory.createCIMchangeRequestImpact(objChangeRequest,cimList[0], territoryTeamInstance));
        lstAllReqImpact.addAll(CallPlanTestDataFactory.createDestinationCIMchangeRequestImpact(objChangeRequest,cimList[0], dstinationTeamInstance));
        insert lstAllReqImpact;

        list<Account_Exclusion__c> lstAllExclued = new list<Account_Exclusion__c>();
        Account_Exclusion__c objExcludedAccount = new Account_Exclusion__c();
        objExcludedAccount.Account__c = acc.id;
        objExcludedAccount.Is_Exclusion_Batch_Run__c = false;
        objExcludedAccount.Status__c = 'Active';
        lstAllExclued.add(objExcludedAccount);

        Account_Exclusion__c objExcludedAccount2 = new Account_Exclusion__c();
        objExcludedAccount2.Account__c = acc2.id;
        objExcludedAccount2.Is_Exclusion_Batch_Run__c = false;
        objExcludedAccount2.Status__c = 'Active';
        lstAllExclued.add(objExcludedAccount2);

        Account_Exclusion__c objExcludedAccount3 = new Account_Exclusion__c();
        objExcludedAccount3.Account__c = acc3.id;
        objExcludedAccount3.Is_Exclusion_Batch_Run__c = false;
        objExcludedAccount3.Status__c = 'Active';

        //lstAllExclued.add(objExcludedAccount3);   

        insert lstAllExclued;     

        Test.startTest();

        ProcessAccountExclusionBatch objMultiple = new ProcessAccountExclusionBatch(false);      
        Database.executeBatch(objMultiple,10);
        Test.stopTest();

        

    }
    

}