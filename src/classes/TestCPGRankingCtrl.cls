/*
Author : Ankit Manendra
Description : Test class for CPGRankingCtrl
Date : 7 Novomber, 2017

*/

@isTest
private class TestCPGRankingCtrl{
    static testMethod void cpgRankingTest() {
    
    Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj;
        
        Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        insert scenarioObj;
        
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj;
        
         Scenario_Rule_Instance_Details__c sceRuleInsDetObj1 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj1.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj1.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj1;
        
        Data_Set_Rule_Map__c dataSetRuleObj = new Data_Set_Rule_Map__c();
        dataSetRuleObj.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj.ds_type__c='I';
        insert dataSetRuleObj;
        
        system.debug('==dataSetRuleObj===='+dataSetRuleObj);
        
        ComponentTypeMaster__c ComTypeMasObj = new ComponentTypeMaster__c();
        ComTypeMasObj.Name='Call Balancing';
        ComTypeMasObj.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj.DisplayIcon__c='add';
        ComTypeMasObj.DisplayIconClass__c='window information';
        insert ComTypeMasObj;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj.Name='Rank';
        busRuleTypeMasObj.Component_Type__c=ComTypeMasObj.id;
        insert busRuleTypeMasObj;
        
        ComponentTypeMaster__c ComTypeMasObj1 = new ComponentTypeMaster__c();
        ComTypeMasObj1.Name='Aggregation';
        ComTypeMasObj1.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj1.DisplayIcon__c='add';
        ComTypeMasObj1.DisplayIconClass__c='window information';
        insert ComTypeMasObj1;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj1 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj1.Name='Rank';
        busRuleTypeMasObj1.Component_Type__c=ComTypeMasObj1.id;
        insert busRuleTypeMasObj1;
        
        Business_Rules__c busRuleObj = new Business_Rules__c();
        busRuleObj.Name='Rank rule 278';
        busRuleObj.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObj.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj.Aggregate_Level__c='Test';
        busRuleObj.customer_prioritization__c='Test';
        insert busRuleObj;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj.Param_Name__c='derived field 278';
        busRuleFieldObj.Business_Rule__c=busRuleObj.id;
        busRuleFieldObj.Param_Data_Type__c='Numeric';
        busRuleFieldObj.Param_Type__c='Derived';
        busRuleFieldObj.IO_Flag__c='O';
        
        insert busRuleFieldObj;
        
        Computation_Rule_Detail__c compRuleDetObj = new Computation_Rule_Detail__c();
        compRuleDetObj.Business_Rules__c=busRuleObj.id;
        compRuleDetObj.Data_Type__c='Numeric';
        compRuleDetObj.Derived_field_name__c='FIELD 123';
        
        insert compRuleDetObj;
        
        Data_Set__c dataSetObj = new Data_Set__c();
        dataSetObj.Name = 'Call Plan Summary';
        insert dataSetObj;
        
        Data_Set_Column_Detail__c dataSetCol = new Data_Set_Column_Detail__c();
        dataSetCol.dataset_id__c=dataSetObj.id;
        insert dataSetCol;
        
        list<Data_Set_Column_Detail__c> dataSetColDetList = new list<Data_Set_Column_Detail__c>();
        dataSetColDetList.add(dataSetCol);
        
        Category_Rule_Entity_Detail__c catRuleEntDet = new Category_Rule_Entity_Detail__c();
        catRuleEntDet.Entity_Type__c='Block';
        catRuleEntDet.Business_Rule__c=busRuleObj.id;
        catRuleEntDet.Priority__c='1';
        insert catRuleEntDet;
        
        Category_Derived_Variable_Metadata__c catDerMetObj = new Category_Derived_Variable_Metadata__c();
        catDerMetObj.Field_Name__c='TestCatDer';
        catDerMetObj.Business_Rule_Id__c=busRuleObj.id;
        insert catDerMetObj;
        
        Category_Rule_Entity_Detail__c catRuleEntDet1 = new Category_Rule_Entity_Detail__c();
        catRuleEntDet1.Entity_Type__c='Group';
        catRuleEntDet1.Business_Rule__c=busRuleObj.id;
        catRuleEntDet1.Priority__c='1';
        catRuleEntDet1.Parent_Entity__c=catRuleEntDet.id;
        insert catRuleEntDet1;
        
        Category_Rule_Output_Detail__c catRuleOutDet = new Category_Rule_Output_Detail__c();
        catRuleOutDet.Category_Rule_Entity_Detail__c=catRuleEntDet.id;
        catRuleOutDet.Derived__c='TestCatDer';
        insert catRuleOutDet;
        
        Rank_Rule__c rankRule = new Rank_Rule__c();
        rankRule.Field_Name__c='TestName';
        rankRule.Value_Type__c='Continuous';
        rankRule.Data_Type__c='Numeric';
        rankRule.Business_Rules__c=busRuleObj.id;
        insert rankRule;
        
        Test.startTest();
        
        //File_Columns__c[] fileColList = CPGRankingCtrl.getDatasourceFields(scenarioObj.id);
        //Rule_Type__c[] ruleTpyeList = CPGRankingCtrl.getRuleTypeDetails();
        Types_List__mdt[] typeList = CPGRankingCtrl.getTypesList();
        List<BRFieldWrapper> fieldWrapList = CPGRankingCtrl.GetSourceAndDerivedFields(sceRuleInsDetObj.id);
        List<Data_Set_Rule_Map__c> dataSetRuleList = CPGRankingCtrl.getInputTabName(sceRuleInsDetObj.id);
        string strJsonBusinessRules = '{"Business_Rules__c":{"'+SObjectDescribe.namespacePrefix+'Business_Rule_Type_Master__c":"'+busRuleTypeMasObj.Id+'","Name":"NEW RULE","'+SObjectDescribe.namespacePrefix+'Is_Derived__c":true,"'+SObjectDescribe.namespacePrefix+'Is_Mandatory__c":true,"'+SObjectDescribe.namespacePrefix+'ScenarioRuleInstanceDetails__c":' + '"'+ sceRuleInsDetObj.id + '"}}';
        string strJsonRankRuleDetail = '[{"'+SObjectDescribe.namespacePrefix+'Excecute_Expression__c":"REMS_FLG +7","'+SObjectDescribe.namespacePrefix+'Field_Name__c":"TestName","'+SObjectDescribe.namespacePrefix+'Value_Type__c":"Continuous","'+SObjectDescribe.namespacePrefix+'Data_Type__c":"Numeric"'+ ',"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":'+'"'+busRuleObj.id+'"'  +'}]';
        string strJsonRankPartitionDetails = '[{"'+SObjectDescribe.namespacePrefix+'Partition_Field__c":"Test"'+ '}]'; //',"Rank_Rule__c":'+'"'+busRuleObj.id+'"'  +'}]';
        string strJsonRankOrderDetails = '[{"'+SObjectDescribe.namespacePrefix+'Excecution_Order__c":"1"'+ '}]'; 
        string strJsonBusinessRuleFieldMapDetails='[{"'+SObjectDescribe.namespacePrefix+'IO_flag__c":"O","Param_Name__c":"FIELD 123","'+SObjectDescribe.namespacePrefix+'Param_Data_Type__c":"Numeric","'+SObjectDescribe.namespacePrefix+'Param_type__c":"Derived"}]';
        
        string strJsonBusinessRulesUpdate = '{"Business_Rules__c":{"'+SObjectDescribe.namespacePrefix+'Business_Rule_Type_Master__c":"'+busRuleTypeMasObj.Id+'","Name":"UPDATE RULE","'+SObjectDescribe.namespacePrefix+'Is_Derived__c":true,"'+SObjectDescribe.namespacePrefix+'Is_Mandatory__c":true,"'+SObjectDescribe.namespacePrefix+'ScenarioRuleInstanceDetails__c":' + '"'+ sceRuleInsDetObj.id +'"'+ ',"Id":'+'"'+ busRuleObj.id +'"'+  '}}';
        string strJsonRankRuleUpdate = '[{"'+SObjectDescribe.namespacePrefix+'Excecute_Expression__c":"REMS_FLG +7","'+SObjectDescribe.namespacePrefix+'Field_Name__c":"TestName","'+SObjectDescribe.namespacePrefix+'Value_Type__c":"Continuous","'+SObjectDescribe.namespacePrefix+'Data_Type__c":"Numeric"'+ ',"'+SObjectDescribe.namespacePrefix+'Business_Rules__c":'+'"'+busRuleObj.id+'"'  +'}]';
        
        CPGConstants.Response res = CPGRankingCtrl.insertRule(strJsonBusinessRules,strJsonRankRuleDetail,strJsonRankPartitionDetails,strJsonRankOrderDetails,strJsonBusinessRuleFieldMapDetails,sceRuleInsDetObj.id);
        CPGConstants.Response res1 = CPGRankingCtrl.UpdateRule(strJsonBusinessRulesUpdate,strJsonRankRuleUpdate,strJsonRankPartitionDetails,strJsonRankOrderDetails,strJsonBusinessRuleFieldMapDetails);
         
        Test.stopTest();
        
        
        }
        
  }