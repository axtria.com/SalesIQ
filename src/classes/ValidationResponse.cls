global class ValidationResponse{
	global string responseType{get;set;}
	global list<string> responseMessage{get;set;}
	
	global ValidationResponse(){
		responseType = '';
	    responseMessage = new list<string>(); 
	}
}