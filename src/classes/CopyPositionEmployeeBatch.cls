/*******************************************************************************************************************************************************************************
 * Name         :   CopyPositionEmployeeBatch.cls
 * Description  :   This batch will execute when scenario record get edit and copy the Position employee record for source team Instance and copy it in destination TeamInstance
 * Author       :   Arun Kumar
 * Created On   :   July 2017
 * Modified By  :   Raghvendra Rathore | 11/22/2018
 * Modification :   Modified the batch class to update the master position reference in Position Employee records when employee assignment is enabled.
********************************************************************************************************************************************************************************/
global class CopyPositionEmployeeBatch implements Database.Batchable<sObject>, Database.AllowsCallouts,Database.Stateful {   
    global Set<Id> teamInstanceSet = new Set<Id>();
    global Team_Instance__c teamInsObj;
    //Id sourceTeamInstance;
    
    global CopyPositionEmployeeBatch(list<Scenario__c> scenarioId){
        for(Scenario__c sc : scenarioId){
            teamInstanceSet.add(sc.Team_Instance__c);
            /*if(sc.Scenario_Source__c == 'Existing Scenario'){
                teamInstanceSet.add(sc.Source_Team_Instance__c);
                sourceTeamInstance = sc.Source_Team_Instance__c;
            }*/
        }
        teamInsObj = [select Id, Team__c, IC_EffstartDate__c from Team_Instance__c where id IN : teamInstanceSet limit 1];
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        string teamId = teamInsObj.Team__c;
        return Database.getQueryLocator('Select Id, Name, Client_Position_Code__c, Team_Instance__c, Effective_End_Date__c, IsMaster__c, Master_Position_Reference__c From Position__c Where Team_iD__c =: teamId AND (Team_Instance__c IN : teamInstanceSet OR IsMaster__c = true)');
    }

    //Execute method : Process all the positions end dating today.
    @Deprecated
    global void execute(Database.BatchableContext bc, List<Position_Employee__c> scope){}

    global void execute(Database.BatchableContext bc, List<sObject> sObjectList){
        System.debug('teamInstanceSet in execute  --- '+teamInstanceSet);

        //System.debug('sourceTeamInstance :: '+sourceTeamInstance);
        List<Position__c> scope = (List<Position__c>) sObjectList;
        Map<String, Position__c> masterPosIdAndPosMap = new Map<String, Position__c>();
        Set<String> setPosids = new Set<String>();
        Set<String> newPositions = new Set<String>();
        
        for(Position__c pos : scope){
            if(teamInsObj.Id == pos.Team_Instance__c){
                newPositions.add(pos.Client_Position_Code__c);
            }
        }
        
        for(Position__c pos : scope){
            setPosids.add(String.valueOf(pos.Id));
            system.debug('#### Position : '+pos.Name + ' | ' + pos.Client_Position_Code__c);
            system.debug('#### Position end date : '+pos.Effective_End_Date__c);
            system.debug('#### Scenario start date : '+teamInsObj.IC_EffstartDate__c);
            
            /*If existing scenario position is expired, mark it Master else Non master. All the positions in new scenario will be master*/
            if(teamInsObj.Id != pos.Team_Instance__c && pos.Effective_End_Date__c >= teamInsObj.IC_EffstartDate__c && newPositions.contains(pos.Client_Position_Code__c)){
                pos.IsMaster__c = false;
            }else{
                pos.IsMaster__c = true;
                masterPosIdAndPosMap.put(pos.Client_Position_Code__c, pos);
            }
            system.debug('#### Master flag : '+pos.IsMaster__c);
        }

        //scope.addAll(oldMasterPositionList);

        System.debug('scope -- '+scope.size());

        map<Id,Position__c> positionMap = new map<Id,Position__c>(scope);
        map<Id,Employee__c> employeeMap = new map<Id,Employee__c>();

        String AssignmentStatus = SalesIQGlobalConstants.EMPLOYEE_STATUS_ACTIVE;
        
        System.debug('setPosids : '+setPosids);
        System.debug('size - '+setPosids.size());
        String soql = 'Select Id, Assignment_Type__c, Position__r.Master_Position_Reference__c, Change_Status__c, Effective_Start_Date__c, Effective_End_Date__c, Employee__c, Employee__r.User__c, Position__c, Position__r.Client_Position_Code__c, Status__c, Position__r.Team_Instance__c, Position__r.IsMaster__c From Position_employee__c Where Position__c In : setPosids';
        System.debug('#### soql ' + soql);
        System.debug('#### masterPosIdAndPosMap ' + masterPosIdAndPosMap.keySet());
        List<Position_Employee__c> lstPosEmp = Database.Query(soql);

        list<User_Access_Permission__c> scenarioUserAccess = new list<User_Access_Permission__c>();
        for(Position_Employee__c pEmp : lstPosEmp){
            /*Update master position reference on employee assignment*/
            if(masterPosIdAndPosMap.get(pEmp.Position__r.Client_Position_Code__c) != null){
                system.debug('#### Old Position : '+pEmp.Position__c);
                system.debug('#### New Position : '+masterPosIdAndPosMap.get(pEmp.Position__r.Client_Position_Code__c).Id);
                if(pEmp.Position__c != masterPosIdAndPosMap.get(pEmp.Position__r.Client_Position_Code__c).Id){
                    pEmp.Position__c = masterPosIdAndPosMap.get(pEmp.Position__r.Client_Position_Code__c).Id;
                }
            }
            
            /*Create user access permission for primary employees*/
            if(pEmp.Employee__r.User__c != null && pEmp.Assignment_Type__c == 'Primary'){
                scenarioUserAccess.add(new User_Access_Permission__c(User__c = pEmp.Employee__r.User__c, 
                                                                     Position__c = pEmp.Position__c,
                                                                     Map_Access_Position__c = pEmp.Position__c,
                                                                     Team_Instance__c = masterPosIdAndPosMap.get(pEmp.Position__r.Client_Position_Code__c).Team_Instance__c,
                                                                     Is_Active__c = false,
                                                                     Effective_Start_Date__c = pEmp.Effective_Start_Date__c,
                                                                     Effective_End_Date__c = pEmp.Effective_End_Date__c,
                                                                     Sharing_Type__c = 'Implicit'));
            }
        
            if(pEmp.Assignment_Type__c == 'Primary'){
                Position__c tempPos = positionMap.get(pEmp.Position__c);
                tempPos.Employee__c = pEmp.Employee__c;
                tempPos.Assignment_status__c = 'Filled';
                positionMap.put(tempPos.Id,tempPos);
                
                // update employee status when assignment start date is less or equal to today date
                //assignments will not reflect for future scenarios
                if(pEmp.Effective_Start_Date__c <= System.today()){
                    Employee__c tempEmp = new Employee__c(id = pEmp.Employee__c);
                    tempEmp.Current_Territory__c = pEmp.Position__c;
                    tempEmp.Field_Status__c = 'Assigned';
                    employeeMap.put(tempEmp.Id,tempEmp);
                }
            }
        }

        System.debug('positionMap.values() -- '+positionMap.values());
        if(positionMap.size() > 0){
            update positionMap.values();
        }

        if(scenarioUserAccess.size() > 0){
            insert scenarioUserAccess;
        }

        if(lstPosEmp.size() > 0){
            update lstPosEmp;
        }

        /*Update scenario status after batch completion*/
        list<Scenario__c> scenarioToUpdate = [SELECT Id, Request_Process_Stage__c FROM Scenario__c WHERE Team_Instance__c in : teamInstanceSet];
        for(Scenario__c sc : scenarioToUpdate){
            sc.Request_Process_Stage__c = SalesIQGlobalConstants.READY;
        }
        list<string> SCENARIO_UPDATE_FIELD = new list<string>{'Request_Process_Stage__c'};
        if(SecurityUtil.checkUpdate(Scenario__c.SObjectType, SCENARIO_UPDATE_FIELD , false)){
            system.debug('#### sc.Request_Process_Stage__c : '+ScenarioToUpdate[0].Request_Process_Stage__c);
            update ScenarioToUpdate;
        }
    }

    global void finish(Database.BatchableContext bc){
        system.debug('finish executed');       
    }
}
