@isTest
public class TestTerritorySummaryComponentCtlr {

    static testMethod void validateTerritorySummaryComponentCtlr() {
 
    
     
        //Creating custom setting test data
        ReadOnlyApplication__c setting  = new ReadOnlyApplication__c();
        setting.Alignment__c = false;
        setting.Secondary_Assignment_Alignment__c = true;
        insert setting;
        
        Organization_Master__c org = CallPlanTestDataFactory.createOrganizationMasterRecord();
        insert org;
        Country__c country = CallPlanTestDataFactory.createCountryMasterRecord(org.id);
        insert country;
        Geography_Type__c geotype = CallPlanTestDataFactory.createGeographyTypeRecord(country.id);
        insert geotype;

        Team__c baseTeam = TestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_BASE);
        insert baseTeam ;
        
        Change_Request_Type__c objRequest = CallPlanTestDataFactory.CreateZipRequestType();
        insert objRequest;

        Team__c overlayTeam = TestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_OVERLAY);
        overlayTeam.Name='HTN Overlay';
        overlayTeam.Controlling_Team__c=baseTeam.id;
        insert overlayTeam ;

        Team_Instance__c baseTeamInstance = TestDataFactory.createTeamInstance(baseTeam.Id,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP,'',Date.today()-100,Date.today()+100);
        insert baseTeamInstance;
        
        list<CIM_Config__c> cimList = CallPlanTestDataFactory.createCIMCOnfig(objRequest, BaseTeamInstance,'ZIP Movement');
        cimList[0].isOptimum__c = true;
        cimList[0].MetricCalculationType__c = 'Absolute';
        cimList[0].Aggregation_Type__c = 'Count';
        cimList[0].Hierarchy_Level__c = '2';
        cimList[0].Enable__c  = true;
        cimList[0].Visible__c = true; 
        cimList[0].Is_Custom_Metric__c=false;
        cimList[1].Hierarchy_Level__c = '3';
        cimList[1].Enable__c  = true;
        cimList[1].Visible__c = true; 
        cimList[1].Is_Custom_Metric__c=false;
        cimList[1].Aggregation_Type__c = 'Sum';
        cimList[1].Roll_Up_Summary_Column__c = 'Proposed';
        insert cimList;
        
        Team_Instance__c overlayTeamInstance = TestDataFactory.createTeamInstance(overlayTeam.Id,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP,'',Date.today()-100,Date.today()+100);

        Position__c districtPosition = TestDataFactory.createPosition(baseTeam, 'District', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_DISTRICT, '2', null);
        insert districtPosition;
       
        Position__c position = TestDataFactory.createPosition(overlayTeam, 'Territory', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_TERRITORY, '1', districtPosition.Id);
        insert position;
        
        Position__c nation = new Position__c();
        nation.name = 'USA';
        nation.Client_Territory_Name__c = 'North east';
        nation.Client_Position_Code__c = '1NE30000';
        nation.Client_Territory_Code__c='1NE30000';
        nation.Position_Type__c='Nation';
        nation.inactive__c = false;
        nation.RGB__c = '41,210,117';
        nation.Team_iD__c = baseTeam.id;
        nation.Hierarchy_Level__c='4';
        nation.Related_Position_Type__c  ='Base';
        nation.Effective_End_Date__c=Date.today().addMonths(50);
        insert nation;
        
        Position__c region = new Position__c();
        region.name = 'North east';
        region.Client_Territory_Name__c = 'North east';
        region.Client_Position_Code__c = '1NE30000';
        region.Client_Territory_Code__c='1NE30000';
        region.Position_Type__c='Region';
        region.inactive__c = false;
        region.RGB__c = '41,210,117';
        region.Team_iD__c = baseTeam.id;
        region.Effective_End_Date__c=Date.today().addMonths(50);
        region.Hierarchy_Level__c = '3';
        region.Related_Position_Type__c  ='Base';
        insert region;
        
        Position_Team_Instance__c regionTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(region, null, BaseTeamInstance);
        regionTeamInstance.X_Max__c=-72.6966429900;
        regionTeamInstance.X_Min__c=-73.9625820000;
        regionTeamInstance.Y_Max__c=40.9666490000;
        regionTeamInstance.Y_Min__c=40.5821279800;
        insert regionTeamInstance;
        
        Position__c district = new Position__c();
        district.name = 'Newyork';
        district.Client_Territory_Name__c = 'Newyork';
        district.Client_Position_Code__c = '1NE30001';
        district.Client_Territory_Code__c='1NE30001';
        district.Position_Type__c='District';
        district.inactive__c = false;
        district.RGB__c = '41,210,117';
        district.Team_iD__c = baseTeam.id;
        district.Parent_Position__c = region.Id;
        district.Hierarchy_Level__c='2';
        district.Related_Position_Type__c  ='Base';
        district.Effective_End_Date__c=Date.today().addMonths(50);
        insert district;
        
        Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, region, BaseTeamInstance);
        districtTeamInstance.X_Max__c=-72.6966429900;
        districtTeamInstance.X_Min__c=-73.9625820000;
        districtTeamInstance.Y_Max__c=40.9666490000;
        districtTeamInstance.Y_Min__c=40.5821279800;
        insert districtTeamInstance;
        
        Position__c territory = new Position__c();
        territory.name = 'Long Island East';
        territory.Client_Territory_Name__c = 'Long Island East';
        territory.Client_Position_Code__c = '1NE30011';
        territory.Client_Territory_Code__c='1NE30011';
        territory.Position_Type__c='Territory';
        territory.inactive__c = false;
        territory.RGB__c = '41,210,117';
        territory.Team_iD__c = baseTeam.id;
        territory.Parent_Position__c = district.Id; 
        territory.Effective_Start_Date__c=Date.today();       
        territory.Effective_End_Date__c=Date.today().addMonths(50);
        territory.Related_Position_Type__c = 'Base';
        territory.Hierarchy_Level__c = '1'; 
        territory.Team_Instance__c =  BaseTeamInstance.id;
        insert territory;
        
         Position_Team_Instance__c terrTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, null, BaseTeamInstance);
        regionTeamInstance.X_Max__c=-72.6966429900;
        regionTeamInstance.X_Min__c=-73.9625820000;
        regionTeamInstance.Y_Max__c=40.9666490000;
        regionTeamInstance.Y_Min__c=40.5821279800;
        insert terrTeamInstance;
        
        list<DiplayColorCriteria__c> colorSettings = new list<DiplayColorCriteria__c>();
        colorSettings.add(new DiplayColorCriteria__c(Name='Error',Color__c='#F8CBAD'));
        colorSettings.add(new DiplayColorCriteria__c(Name='Normal',Color__c='#FFFFFF'));
        colorSettings.add(new DiplayColorCriteria__c(Name='Warning',Color__c='#ffE699'));
        insert colorSettings;
        
         list<CIM_Position_Metric_Summary__c> sourceCIMSummary =  CallPlanTestDataFactory.createCIMMetricSummary(cimList,regionTeamInstance,baseTeamInstance);
        insert sourceCIMSummary;
        
        list<CIM_Position_Metric_Summary__c> sourceCIMSummary1 =  CallPlanTestDataFactory.createCIMMetricSummary(cimList,terrTeamInstance,baseTeamInstance);
        insert sourceCIMSummary1;
     
        test.startTest();
        List<String> rootPositionIds = SalesIQUtility.getRootPositionIds(territory.id);
        system.debug(rootPositionIds);
        TerritorySummaryComponentCtlr.PositionSummaryWrapper positionWrapperObj = new TerritorySummaryComponentCtlr.PositionSummaryWrapper();
        positionWrapperObj.nationalAverageMap = new Map<String,String>();
        Map<String,List<TerritorySummaryComponentCtlr.PositionSummaryWrapper>> nationalAvg = TerritorySummaryComponentCtlr.getNationalAverageData(String.Valueof(baseTeamInstance.id),'ZIP Movement','1',String.Valueof(territory.id),false);
        try{
            TerritorySummaryComponentCtlr.getTerritorySummaryData(String.Valueof(baseTeamInstance.id),String.valueOf(position.id),'1','ZIP Movement',new map<String,String>(),false);
        }catch(Exception e){
        }

        TerritorySummaryComponentCtlr.getHierarchyLevels('1',String.Valueof(baseTeamInstance.id));
        TerritorySummaryComponentCtlr.getHierarchyLevels('2',String.Valueof(baseTeamInstance.id));     
 
        test.stopTest();
     
 }
}
