global class BulkZIporAccountBatch implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful{
     
    public List<CR_Geography__c> crGeoList {get;set;}
    public List<String> geoList;
    public set<ID> posgeoIds;
    Public List<String> crIds {get;set;}
    public Change_Request__c crReq {get;set;}
    public List<String> crAccIds;
    public List<String> pos;
    public string teamInstance;
    public list<SalesIQ_Logger__c> lstLogger = new list<SalesIQ_Logger__c>();
    public Boolean isError = false;//this variable is used to track whether any error get occured in execute method or inner methd of that execute method i.e CreateCRAccountBulk
    //So we need to stop further execution of finish method, which is further executing other batch/method. in that case other method will mark CR status to completed , which leads to
    //inconsitency
    
    public BulkZIporAccountBatch(List<String> crgIDs,Change_Request__C cr,LIst<String> geList, List<String> crAccId)
    {
        crgeoList = new List<CR_Geography__c>();
        crIds = crgIDs;  
        crReq  = cr;
        geoList = new List<String>();
        posgeoIds = new Set<ID>();
        geoList.addAll(geList);
        crAccIds = crAccId;
        pos = new List<String>();
        pos.addAll(cr.Sources_Bulk__c.split(','));
        teamInstance = cr.Team_Instance_ID__c;
    }
     
    global  Database.QueryLocator start(Database.BatchableContext BC)
    {    
       String query =  'select id,Geography__r.name,Change_Request__c,Destination_Position__c,Destination_Position_Team_Instance__c,Geography__c,isAccount__c,Source_Position__c,Source_Position_Team_Instance__c from CR_Geography__c where ID IN: crIds ';   
       return Database.getQueryLocator(query);

    }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {   
        lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('start Executeing Batch BulkZIporAccountBatch ' ,'', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,crReq.id ));
        try{
            bulktriggerStop.stopRun = true;
            insert lstLogger;
            Set<ID> updatedZips = ChangeRequestTriggerHandler.ApproveRejectRequestBulk((List<CR_Geography__c>)scope,0,SalesIQGlobalConstants.REQUEST_STATUS_APPROVED,crReq);   
           
            posgeoIds.addAll(updatedZips);
            system.debug('zip is'+updatedZips);
            
            //system.debug('########HSM2'+System.Limits.getFutureCalls());

        }catch(Exception e){
            system.debug('Exception :: ' + e.getMessage());
            
            //clearing log error coz if handler msg throw error, den inserting in logger will give error, because list has SF id, which received while inserting log in execute method
            if(lstLogger.size() > 0){
                lstLogger.clear();
            }
            
            lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('Error: '+e.getMessage()+' in Execute method of BulkZIporAccountBatch ','', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,crReq.id ));
            insert lstLogger;

            SalesIQUtility.updateChangeRequestStatus(new set<id>{crReq.id}, true);
            isError = true;
        }
        finally{
            //Clearing logger because in case of 2 batch ,it will clear the log for secod time to evade error "cannot specify Id in an insert call"
            if(lstLogger.size() > 0){
                lstLogger.clear();
            }
            
        } 
        
       //List<String> updatedZips = ChangeRequestTriggerHandler.ApproveRejectRequestBulk(scope,0,SalesIQGlobalConstants.REQUEST_STATUS_APPROVED,crReq);   

    }
    
    global void finish(Database.BatchableContext BC)
    {
        if(lstLogger.size() > 0){
            lstLogger.clear();
        }
        try{
            lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('Finished Batch execution of BulkZIporAccountBatch ' ,'', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,crReq.id ));
            lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('In Finishing method of BulkZIporAccountBatch ' ,'', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,crReq.id ));
            if(!isError){                
                
                Team_Instance__c objTeamInstance= SalesIQUtility.getTeamInstanceById(teamInstance);

            if(objTeamInstance.CIM_Available__c){
                ChangeRequestTriggerHandler.updateRequestSummaryBulk(crReq);
            }

                 
                list<string> reqIds = new list<string>();
                reqIds.add(crReq.id);
                system.debug('zip in finish'+geoList);
                system.debug('zip in finish'+posgeoIds);
                HandlerPosGeo.rollupExtentsOnTerritoryFutureBulk(posgeoIds);
                if(crAccIds.size()>0){
                    //lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('preparing BulkAccountMovementBatch with  id '+crReq.id+' and '+crAccIds.size() +' Account ' ,'', 'Bulk Movement' ));
                    insert lstLogger;
                    Database.executeBatch(new BulkAccountMovementBatch(crAccIds,crReq),1500);
                }

                system.debug('Restrict_ZIP_Share__c '+objTeamInstance.Restrict_ZIP_Share__c);
                if(!objTeamInstance.Restrict_ZIP_Share__c){
                    database.executeBatch(new UpdatePositionGeographySharedBatch(crReq.Team_Instance_ID__c,new set<id>{crReq.id}, true),1500);  
                }

                if(objTeamInstance.Geography_Type_Name__r.Shape_Available__c){
                    if(!lstLogger.isEmpty()){
                        lstLogger.clear();
                    }
                    lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('preparing QueueESRIBatchProcess with '+posgeoIds.size() +' ZIPs ' ,'', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,crReq.id ));
                    insert lstLogger;
                    Id jobId = System.System.enqueueJob(new QueueESRIBatchProcess(reqIds,posgeoIds,crReq));
                }          

               

                 /*We need to update CR status=completed. 
                If geo shapes are not availble then CR status should be marked completed here.
                */

                if(crAccIds.size()==0 && !objTeamInstance.Geography_Type_Name__r.Shape_Available__c){

                    crReq.Execution_Status__c = SalesIQGlobalConstants.BULK_CR_COMPLETED;
                    update crReq;

                    List<Position__c> pList = SalesIQUtility.getPositions(pos,false);
                    if(pos.size()>0)
                        update pList;
                    
                }
                 
                bulktriggerStop.stopRun = false;
            }
            

        }catch(Exception e){
            SalesIQUtility.updateChangeRequestStatus(new set<id>{crReq.id}, true);
            if(lstLogger.size() > 0){
                lstLogger.clear();
            }
            lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('Error: '+e.getMessage()+' in Finish method of BulkZIporAccountBatch ' ,'', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,crReq.id ));
            insert lstLogger;

        }
        finally{
                       
            isError = false;//resetting variable again
        }  
    }
}