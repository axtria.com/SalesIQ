/**********************************************************************
* Rest Webservice, called after Rule Run and Promote in case the template output is Position Account 
*************************************************************************/
@RestResource(urlMapping='/salesiq/reinitiateMetricsAndEsri/*')
global class ReinitiateMetricsAndEsri 
{
    @HttpGet
    global static String doGet() 
    {

    	RestRequest restReq = RestContext.request;
		String scenarioId = restReq.params.get('scenarioId') ;
    	String teamInstanceId;
    	Scenario__c sc = [select Team_Instance__c,Scenario_Type__c from Scenario__c where id=:scenarioId];
        List<Data_Set_Rule_Map__c> dsrmList = [select id,dataset_id__r.Data_Set_Object_Name__c from Data_Set_Rule_Map__c where scenario_rule_instance_id__r.Scenario_Id__c=:scenarioId and dataset_id__r.Is_Upstream__c = true];
    	teamInstanceId = sc.Team_Instance__c;
        if(teamInstanceId != null && dsrmList.size() > 0 && dsrmList[0].dataset_id__r.Data_Set_Object_Name__c == SObjectType.Position_Account__c.getName())
        {
            CIMPositionMatrixSummaryUpdateCtlr cim=new CIMPositionMatrixSummaryUpdateCtlr(teamInstanceId,scenarioId,true);
        }
        
        return null;
    }
}