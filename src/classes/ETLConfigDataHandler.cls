/******************************************
Name        :   ETLConfigDataHandler.cls
Description :   Controller to send parameters to postgres service upon creation or update of ET record.
Created by  :   Ayush Rastogi A2412
******************************************/

public class ETLConfigDataHandler {

    @future(callout = true)

    public static void getETLConfig(ID id, String Uname, String Pwrd, String Stoken) {

        Id OrgId = UserInfo.getOrganizationId();
        string statusCode = '';
        try {

            list < ETL_Config__c > lstETLInfo = ScenarioUtility.getETLConfigByName(SalesIQGlobalConstants.ETLCONFIG_MAP_DOWNLOAD);
            string endPoint = '';
            //Map <string, string> jsonBody = new Map <string, string> ();

            if (lstETLInfo != null && lstETLInfo.size() > 0) {
                for (ETL_Config__c config: lstETLInfo) {
                    endPoint = config.End_Point__c + '/etl_sync?';
                    system.debug('endPoint ====>' + endPoint);

                    //jsonBody =  'sf_username=' + config.SF_UserName__c + 'sf_password=' + config.SF_Password__c + '&sf_orgid=' + OrgId + '&etl_id=' + config.Id + '&sf_securitytoken=' + config.S3_Security_Token__c;

                }
                string ns=SalesIQGlobalConstants.NAME_SPACE;
                list<Organization> domain = [Select IsSandbox from Organization Limit 1];
                Http h = new Http();
                String JsonBodys = 'sf_orgid=' + OrgId + '&etl_id=' + id + '&sf_username=' + Uname + '&sf_password=' + Pwrd + '&sf_securitytoken=' + Stoken + '&namespace=' + ns + '&sandbox=' + domain[0].IsSandbox;
                HttpRequest request = new HttpRequest();
                request.setEndPoint(endPoint);
                request.setmethod('POST');
                request.setbody(JsonBodys);
                HttpResponse response = h.send(request);
                //HttpResponse reponse = SalesIQUtility.getHTTPResponse(endpoint, jsonbody, 'POST', 12000);
                statusCode = string.valueOf(response.getStatusCode());
                system.debug('JsonBodys====>' + JsonBodys);
                system.debug('statusCode=====>' + statusCode);

            } else {
                statusCode = SalesIQGlobalConstants.ERROR_CODE_107;

            }

        } catch (Exception e) {
            if (e.getMessage().contains('Remote site settings')) {
                system.debug('in if condition');
                //successErrorMap.put('Error',System.Label.No_Remote_Site_Setting);
                statusCode = SalesIQGlobalConstants.ERROR_CODE_106;
            } else {
                statusCode = 'SalesIQGlobalConstants.ERROR_CODE_404';
            }
            String message = e.getTypeName() + ': ' + e.getMessage() + '\n\n' + e.getStackTraceString();
        }
    }
}
