@IsTest 
public with sharing class TestRepRequestCtrl{
    static testMethod void TestgetRepRequest(){
        string AlignmentType = 'Base';
        
        User objUser = CallPlanTestDataFactory.createUser();
        insert objUser;
           
        Team__c objTeam = CallPlanTestDataFactory.createTeam(AlignmentType);
        insert objTeam;
        
        Position__c sourcePosition = CallPlanTestDataFactory.createPosition(null, objTeam);
        insert sourcePosition;
        
        Position__c DestinationPosition = CallPlanTestDataFactory.createDestinationPosition(null, objTeam);
        DestinationPosition.Related_Position_Type__c = 'Base';
        insert DestinationPosition;
        
        Team_Instance__c  teamInstance = CallPlanTestDataFactory.CreateTeamInstance(objTeam, AlignmentType);
        insert teamInstance;
        
        User_Access_Permission__c objAccessPermission =  CallPlanTestDataFactory.createUserAccessPerm(sourcePosition,objUser ,teamInstance);
        insert objAccessPermission;
        
        Position_Team_Instance__c posTeamInstance =CallPlanTestDataFactory.CreatePosTeamInstance(sourcePosition,null,teamInstance);
        insert posTeamInstance;
        
        Position_Team_Instance__c DestinationTeamInstance =CallPlanTestDataFactory.CreatePosTeamInstance(DestinationPosition,null,teamInstance);
        insert DestinationTeamInstance;
         
        Change_Request__c ChngeReq =  CallPlanTestDataFactory.createChangeRequest(objUser, sourcePosition, teamInstance,'Pending' );
        insert ChngeReq;
        
        Change_Request_Approver_Detail__c ApprovalDetails  =CallPlanTestDataFactory.createCRADsecond(sourcePosition, objUser);
        insert ApprovalDetails;
        
		test.startTest();            
        CallPlanRequestCtrl objReq = new CallPlanRequestCtrl();            
        test.stopTest();
    }
}