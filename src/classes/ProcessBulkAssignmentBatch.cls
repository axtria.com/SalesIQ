global class ProcessBulkAssignmentBatch implements Database.Batchable<sObject>, Database.AllowsCallouts 
{

    global map<string, list<string>> mapGeographyIdANDrelatedPositionID;
    
    global set<string> geographyIds;
    
    global Id teamInstanceId;
    global String assignmentStatus;
    global String operationType;
    global Date changeEffectiveDate;
    global Date positionTeamInstanceEndDate;
    map<string, list<string>> dataMap;
    global String query;

    global ProcessBulkAssignmentBatch(list<Sobject> dataList, set<Id> dataIds,map<string, list<string>> dataMap, String operationType, Date assignmentEndDate, Date positionTeamInstanceEndDate){
        operationType = operationType;
            
            dataMap = new map<string, list<string>>();
            dataMap = dataMap;
            changeEffectiveDate = assignmentEndDate;
            
            assignmentStatus = '';
            positionTeamInstanceEndDate = positionTeamInstanceEndDate;
            Team_Instance__c objTeamInstance = [select id, Alignment_Period__c from Team_Instance__c where id in: dataIds ];
            String alignmentPeriod = '';
            if(!String.isBlank(objTeamInstance.id)){
                 alignmentPeriod = objTeamInstance.Alignment_Period__c;
            }
            else{
                 alignmentPeriod = SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE;
            }

            if(alignmentPeriod == SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE){
                assignmentStatus = SalesIQGlobalConstants.POSITION_GEOGRAPHY_ACTIVE;
            }else{
                assignmentStatus = SalesIQGlobalConstants.POSITION_GEOGRAPHY_FUTURE_ACTIVE;
            }

        if(operationType == 'createOverlayAssignments'){
            geographyIds = new set<string>();
            mapGeographyIdANDrelatedPositionID = new map<string, list<string>>();
            list<Position_Geography__c> posGeographyList = dataList;
            for(Position_Geography__c posGeo : posGeographyList){
                geographyIds.add(posGeo.Geography__c);
                mapGeographyIdANDrelatedPositionID.put(posGeo.Geography__c, dataMap.get(posGeo.position__c));
            }

            query = 'select id, name, Change_Status__c, Effective_End_Date__c, Effective_Start_Date__c, Geography__c,Geography__r.name, Metric1__c, Metric2__c, Metric3__c, Metric4__c, Metric5__c, Metric6__c, Metric7__c, Metric8__c, Metric9__c, Metric10__c, Position__c,Position__r.Change_Status_del__c, Position_Team_Instance__c, Team_Instance__c from position_geography__c where geography__c in: geographyIds and team_instance__c =:teamInstanceId and Assignment_status__c =: assignmentStatus';

        }
        if(operationType == 'createOverlayAccountAssignments'){
            geographyIds = new set<String>();
            mapGeographyIdANDrelatedPositionID = new map<string, list<string>>();
            list<Position_Account__c> posAccountList = dataList;
            for(Position_Account__c posAcc : posAccountList){
                geographyIds.add(posAcc.Account__r.AccountNumber);
                mapGeographyIdANDrelatedPositionID.put(posAcc.Account__c, dataMap.get(posAcc.position__c));
            }

            query = 'select id, name, Change_Status__c, Effective_End_Date__c, Effective_Start_Date__c, Account__r.AccountNumber,Account__c, Metric1__c, Metric2__c, Metric3__c, Metric4__c, Metric5__c, Metric6__c, Metric7__c, Metric8__c, Metric9__c, Metric10__c, Position__c, Position_Team_Instance__c, Team_Instance__c,SharedWith__c, IsShared__c,Team_Instance__r.Alignment_Period__c,Team_Instance__r.IC_EffstartDate__c, Team_Instance__r.IC_EffendDate__c ,Account_Alignment_Type__c,Team_Instance__r.Alignment_Type__c from Position_Account__c where Account__r.AccountNumber in: geographyIds and team_instance__c =:teamInstanceId and Assignment_status__c =: assignmentStatus';

        }
    
    }
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.debug('query -'+query);
        return Database.getQueryLocator(query);
    }   
    global void execute(Database.BatchableContext BC,List<sObject> dataList)
    {
        if(operationType == 'createOverlayAssignments'){
            createOverlayAssignments(dataList);
        }
        if(operationType == 'createOverlayAccountAssignments'){

        }
        
    }
    global void finish(Database.BatchableContext BC){}

    public void createOverlayAssignments(list<position_geography__c> dataList){

        list<Position_Geography__c> listPosGeoUnassigned = dataList;

        list<Position_Geography__c> newPositionGeoList = new list<Position_Geography__c>();

        for(Position_Geography__c posGeo : listPosGeoUnassigned){   
            if(assignmentStatus == SalesIQGlobalConstants.RELATED_POSIITON_ACTIVE){
                posGeo.Effective_End_Date__c = changeEffectiveDate - 1;
                posGeo.Position_Id_External__c = posGeo.Position__c;

                Position_Geography__c newRelatedPosObj = posGeo.clone(false, true);
                newRelatedPosObj.position_Team_instance__c = mapGeographyIdANDrelatedPositionID.get(posGeo.geography__c)[0];
                newRelatedPosObj.position__c = mapGeographyIdANDrelatedPositionID.get(posGeo.geography__c)[1];
                newRelatedPosObj.Effective_Start_Date__c = changeEffectiveDate;
                newRelatedPosObj.Effective_End_Date__c = positionTeamInstanceEndDate;
                
                newRelatedPosObj.Position_Id_External__c = newRelatedPosObj.Position__c;
                newPositionGeoList.add(newRelatedPosObj);
            }else{
                //Update position instead of end dating current assignment and creating new assignments in case of future team instance
                posGeo.position_Team_instance__c = mapGeographyIdANDrelatedPositionID.get(posGeo.geography__c)[0];
                posGeo.position__c = mapGeographyIdANDrelatedPositionID.get(posGeo.geography__c)[1];
                posGeo.Position_Id_External__c = posGeo.Position__c;
            }
        }

        listPosGeoUnassigned.addAll(newPositionGeoList);

        if(listPosGeoUnassigned.size() > 0){
            upsert listPosGeoUnassigned;
        }
    } 

    public void createOverlayAccountAssignments(list<Position_Account__c> dataList){

        list<Position_Account__c> listPosAccUnassigned = dataList;

        list<Position_Account__c> newPositionAccList = new list<Position_Account__c>();
        for(Position_Account__c posAcc : listPosAccUnassigned){
            if(assignmentStatus == SalesIQGlobalConstants.RELATED_POSIITON_ACTIVE){
                posAcc.Effective_End_Date__c = changeEffectiveDate - 1;

                Position_Account__c newRelatedPosObj = posAcc.clone(false, true);
                newRelatedPosObj.position_Team_instance__c = mapGeographyIdANDrelatedPositionID.get(posAcc.Account__c)[0];
                newRelatedPosObj.position__c = mapGeographyIdANDrelatedPositionID.get(posAcc.Account__c)[1];
                newRelatedPosObj.Effective_Start_Date__c = changeEffectiveDate;
                newRelatedPosObj.Effective_End_Date__c = positionTeamInstanceEndDate;
                
                newPositionAccList.add(newRelatedPosObj);
            }else{
                //Update position instead of end dating current assignment and creating new assignments in case of future team instance
                posAcc.position_Team_instance__c = mapGeographyIdANDrelatedPositionID.get(posAcc.Account__c)[0];
                posAcc.position__c = mapGeographyIdANDrelatedPositionID.get(posAcc.Account__c)[1];
            }

        }
        listPosAccUnassigned.addAll(newPositionAccList);
        if(listPosAccUnassigned.size() > 0){
            upsert listPosAccUnassigned;
        }


    }

    
}