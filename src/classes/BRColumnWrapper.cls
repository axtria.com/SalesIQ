public class BRColumnWrapper{  
    @AuraEnabled
    public String Id {get;set;}      
    @AuraEnabled
    public String Name {get;set;}        
    @AuraEnabled
    public String variabletype {get; set;}  
    @AuraEnabled
    public String tableDataType {get; set;}
    @AuraEnabled
    public String tableColumnName {get; set;}      
    @AuraEnabled
    public String colDatatype {get; set;}        
    @AuraEnabled
    public String colValuetype {get; set;}
    @AuraEnabled
    public boolean statusFlag {get; set;}
    @AuraEnabled
    public String sourceColumn {get; set;}
           
}