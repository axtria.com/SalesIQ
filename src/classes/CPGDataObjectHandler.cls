public with sharing class CPGDataObjectHandler {
	 /*@future(callout=true)
     public static void runDataManagementJob(String dataObjId){
        string pg_host; 
        string pg_username;
        string pg_port;
        string pg_password;
        string pg_database;
        string secret_key;
        string s3_folder_name;
        string access_key;
        string s_password;
        string s_username;
        string ftp_host;
        string ftp_password;
        string ftp_username;
        string ftp_folder;
        string GI_dataset_ID;
        string BDT_dataset_ID;
        string br_pg_schema;
        list<ETL_Config__c> lstETLInfo = getETLConfigByName(CPGConstants.talend_Instance);
        if(lstETLInfo != null && lstETLInfo.size() > 0){
          for(ETL_Config__c objETL : lstETLInfo){
                s_username = objETL.SF_UserName__c;
                s_password = objETL.SF_Password__c;
                pg_host = objETL.BR_PG_Host__c;
                pg_username = objETL.BR_PG_UserName__c;
                pg_port = objETL.BR_PG_Port__c;
                pg_password = objETL.BR_PG_Password__c;
                pg_database = objETL.BR_PG_Database__c;
                secret_key = objETL.S3_Security_Token__c;
                s3_folder_name = objETL.S3_Filename__c;
                access_key = objETL.S3_Access_Key__c;
                ftp_host = objETL.SFTP_Host__c;
                ftp_password = objETL.SFTP_Password__c;
                ftp_username = objETL.SFTP_Username__c;
                ftp_folder = objETL.SFTP_Folder__c;
                br_pg_schema = objETL.BR_PG_Schema__c;
            }

        }
     	DataManagement.CPG_QA_DataMngmnt obj=new DataManagement.CPG_QA_DataMngmnt(); 
        obj.runJob(new string[]{'--context_param data_object_id='+dataObjId,'--context_param s_username='+s_username,'--context_param s_password='+s_password,'--context_param pg_host='+pg_host,'--context_param pg_username='+pg_username,'--context_param pg_port='+pg_port,'--context_param pg_password='+pg_password,'--context_param pg_database='+pg_database,'--context_param secret_key='+secret_key,'--context_param s3_folder_name='+s3_folder_name,'--context_param access_key='+access_key,'--context_param ftp_host='+ftp_host,'--context_param ftp_password='+ftp_password,'--context_param ftp_username='+ftp_username,'--context_param ftp_folder='+ftp_folder,'--context_param br_pg_schema='+br_pg_schema});
     }*/
      /*@author : Pooja Dhiman 
    @description : This method will fetch ETL_Config__c records based on  etl Config Names
    @param1: etlConfigName : etl config name
    @return : ETL_Config__c list
    */
    
    public static List<ETL_Config__c> getETLConfigByName(string etlConfigName){
        List<ETL_Config__c> etlConfigList;
            etlConfigList = [SELECT BDT_DataSet_Id__c,BR_Data_Object_Id__c,BR_PG_Database__c,BR_PG_Host__c,BR_PG_Password__c,BR_PG_Port__c,BR_PG_Schema__c,BR_PG_UserName__c,BR_Secret_Key__c,CreatedById,CreatedDate,End_Point__c,ESRI_Password__c,ESRI_UserName__c,GI_Dataset_Id__c,Id,Name,S3_Access_Key__c,S3_Bucket__c,S3_Filename__c,S3_Key_Name__c,S3_Security_Token__c,Server_Type__c,SFTP_Folder__c,SFTP_Host__c,SFTP_Password__c,SFTP_Port__c,SFTP_Username__c,SF_Password__c,SF_UserName__c FROM ETL_Config__c where name =:etlConfigName limit 1];
        return etlConfigList;
    }
}