/******************************************************************************************************************************************************
 * Name         :   TestLookupComponentCtlr.cls
 * Description  :   Test Class for for LookupComponentCtlr lightning component 
 * Author       :   Lagnika Sharma
 * Created On   :   14/09/2017
******************************************************************************************************************************************************/
@isTest
public class TestLookupComponentCtlr{

    static testMethod void testLookup(){
    	
    	 String ns = SalesIQGlobalConstants.getOrgNameSpace();

        

    	TriggerContol__c triggerControl = new TriggerContol__c (IsStopTrigger__c = true , Name='SecurityUtil');
    	insert triggerControl;

    	Team__c baseTeam = new Team__c(Name='HTN',Type__c='Base',Effective_start_Date__c = Date.newInstance(2015, 1, 1), Effective_End_Date__c = Date.newInstance(2018, 12, 31));
        insert baseTeam;

    	Team_Instance__c baseTeamInstance =  new Team_Instance__c();
        BaseTeamInstance.Name = 'HTN_Q1_2016';
        BaseTeamInstance.Alignment_Period__c = 'Current';
        BaseTeamInstance.Team__c = baseTeam.id;
        BaseTeamInstance.IC_EffstartDate__c = Date.newInstance(2015, 1, 1);
        BaseTeamInstance.IC_EffendDate__c = Date.newInstance(2022, 12, 31);
        BaseTeamInstance.Alignment_Type__c = 'ZIP';
        BaseTeamInstance.isActiveCycle__c = 'Y'; 
        BaseTeamInstance.IsCascadeDeleteAssignments__c = false;
        insert baseTeamInstance;

        

    	Team__c overlayTeam = new Team__c(Name='HTN Overlay',Type__c='Overlay',Effective_start_Date__c = Date.newInstance(2015, 1, 1), Effective_End_Date__c = Date.newInstance(2018, 12, 31), Controlling_Team__c= baseTeam.id);
        overlayTeam.Is_Enforced__c = true;
        insert overlayTeam;

    	Team_Instance__c overlayTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(overlayTeam, 'Zip');
        overlayTeamInstance.Name = 'HTN_Overlay_2016';
        overlayTeamInstance.Alignment_Period__c= 'Current';
    	overlayTeamInstance.Base_Team_Instance__c = baseTeamInstance.Id;
        overlayTeamInstance.IC_EffstartDate__c = Date.newInstance(2015, 01, 01);
        overlayTeamInstance.IC_EffEndDate__c = Date.newInstance(2025, 01, 01);
        overlayTeamInstance.isActiveCycle__c = 'Y'; 
        overlayTeamInstance.IsCascadeDeleteAssignments__c = true;
    	insert overlayTeamInstance;

    	Position__c terrPos = new  Position__c();
        terrPos.Name = 'Albany';
    	terrPos.Hierarchy_Level__c = '1';
        terrPos.Client_Position_Code__c = 'POS-0009';
        terrPos.inactive__c = false;
        terrPos.Effective_Start_Date__c = System.today();
        terrPos.Effective_End_Date__c = Date.newInstance(2018, 01, 01);
        terrPos.Related_Position_Type__c = 'Base';
        terrPos.Team_iD__c = baseTeam.Id;
        terrPos.Team_Instance__c = baseTeamInstance.Id;
        insert terrPos;

        Position__c overlayPos = new  Position__c();
        overlayPos.Name = 'Albany';
    	overlayPos.Hierarchy_Level__c = '1';
        overlayPos.Client_Position_Code__c = 'POS-0009';
        overlayPos.inactive__c = false;
        overlayPos.Effective_Start_Date__c = System.today();
        overlayPos.Effective_End_Date__c = Date.newInstance(2018, 01, 01);
        overlayPos.Related_Position_Type__c = 'Base';
        overlayPos.Team_iD__c = overlayTeam.Id;
        overlayPos.Team_Instance__c = overlayTeamInstance.Id;
        insert overlayPos;

        CR_Team_Instance_Config__c config  = new CR_Team_Instance_Config__c();
        config.Team_Instance__c = baseTeamInstance.Id;
        config.Configuration_Type__c = 'Hierarchy Configuration';
        insert config;

    	String filter = '1;'+baseTeamInstance.Id + ';Base';
    	LookupComponentCtlr.searchDB(ns+'Position__c', 'Id', 'Name', 1,'Name','Albany', filter,'Edit Position','');

         Alignment_Global_Settings__c setting = new Alignment_Global_Settings__c(Name = 'ExceutePositionTriggerValidation', Tree_Hierarchy_Sort_Field__c = 'true');
         insert setting;

         LookupComponentCtlr.searchDB(ns+'Position__c', 'Id', 'Name', 1,'Name','Albany', filter,'Create Position','');

    	String filterOverlay = '0;'+overlayTeamInstance.Id + ';Overlay';
    	LookupComponentCtlr.searchDB(ns+'Position__c', 'Id', 'Name', 1,'Name','Albany', filterOverlay,'Create Position','');

    }

}