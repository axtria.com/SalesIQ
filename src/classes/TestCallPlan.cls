@istest
private class TestCallPlan {
    //CALL PLAN class DEPRECATED
    /*
    static testMethod void callPlanTest(){
        try{
        TriggerContol__c offSecurityUtil = new TriggerContol__c(Name='SecurityUtil', IsStopTrigger__c = true);
        insert offSecurityUtil;
        
        Profile p = [select id from Profile where name = 'System Administrator'];
        
        User tUser = new User(Alias = 'Rep', Email='repuser@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='repuserQuset@testorg.com');
        
        User tUser1 = new User(Alias = 'Rep', Email='repuser1@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='repuserQuset1@testorg.com');
        
        User loggedInUser = new User(id=UserInfo.getUserId());
        
        User tUser2 = new User(Alias = 'Rep', Email='repuser1@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='repuserQuset2@testorg.com');
                                
        User tUser3 = new User(Alias = 'Rep', Email='repuser1@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='repuserQuset3@testorg.com');
        
        insert new User[]{tUser, tUser1, tUser2, tUser3}; 
        
        
        Team__c team = new Team__c();
        team.Name = 'Specialty';
        insert team;      
        
        
        Position__c posNation = new Position__c();
        posNation.Position_Type__c = 'Nation';
        posNation.Hierarchy_Level__c ='4';
        posNation.Name = 'Chico CA_SPEC';
        posNation.Team_iD__c     = team.id;
        
        insert posNation;
        
        Position__c pos2 = new Position__c();
        pos2.Position_Type__c = 'Region';
        pos2.Name = 'Chicago';
        pos2.Hierarchy_Level__c ='3';
        pos2.Team_iD__c = team.id;
        pos2.Parent_Position__c = posNation.id;
        
        insert pos2;
        
        Position__c pos = new Position__c();
        pos.Position_Type__c = 'Territory';
        pos.Name = 'Chico CA_SPEC';
        pos.Hierarchy_Level__c ='1';
        pos.Team_iD__c   = team.id;
        pos.Parent_Position__c = pos2.id;
        
        insert pos;
        
        Position__c posDM = new Position__c();
        posDM.Position_Type__c = 'District';
        posNation.Hierarchy_Level__c ='2';
        posDM.Name = 'Chico CA_SPEC';
        posDM.Team_iD__c     = team.id;
        posDM.Parent_Position__c = pos2.id;
        
        insert posDM;
        
        system.debug('pos :::'+pos);
        
        Team_Instance__c teamIns = new Team_Instance__c();
        teamIns.Team__c = team.id;
        insert teamIns;
        
        Position_Team_Instance__c posTeam = new Position_Team_Instance__c();
        posTeam.Position_ID__c = pos.id;
        posTeam.Parent_Position_ID__c = pos.id;
        posTeam.Team_Instance_ID__c = teamIns.id;
        posTeam.Effective_End_Date__c = Date.newInstance(2022,11,09);
        posTeam.Effective_Start_Date__c = Date.newInstance(2016,02,29);
        insert posTeam;
        
        Team_Instance_Object_Attribute__c teamAtt = new Team_Instance_Object_Attribute__c();
        teamAtt.Team_Instance__c = teamIns.id;
        teamAtt.Interface_Name__c = 'Call Plan';
        teamAtt.isRequired__c = true;
        teamAtt.Attribute_API_Name__c = 'name';
        teamAtt.Object_Name__c = 'Position_Account_Call_Plan__c';
        //insert teamatt;
        
        Team_Instance_Object_Attribute__c objAttrtbutes = CallPlanTestDataFactory.createObjectAttributes(teamIns,'Position_Account_Call_Plan__c');
        objAttrtbutes.Interface_Name__c = SalesIQGlobalConstants.MOVEMENT_TYPE_CALL_PLAN;
        objAttrtbutes.isRequired__c = true;
        objAttrtbutes.Data_Type__c = 'Picklist';
        
        insert new Team_Instance_Object_Attribute__c[]{teamatt, objAttrtbutes};
        
        Team_Instance_Object_Attribute_Detail__c objAttrtbutesDetails = CallPlanTestDataFactory.createAttributesDetails(objAttrtbutes, teamIns);
        //insert objAttrtbutesDetails;
        
        Team_Instance_Object_Attribute_Detail__c teamObjDetail = new Team_Instance_Object_Attribute_Detail__c();
        teamObjDetail.Object_Attibute_Team_Instance__c = teamAtt.id;
        
        insert new Team_Instance_Object_Attribute_Detail__c[]{objAttrtbutesDetails, teamObjDetail};
        
        Account acc = new Account();
        acc.Name = 'Chelsea Parson';
        insert acc;
        
        Position_Account_Call_Plan__c posAcc = new Position_Account_Call_Plan__c();
        //posAcc.isAccountTarget__c = true;
        posAcc.Position__c = pos.id;
        posAcc.Team_Instance__c = teamIns.id;
        posAcc.Account__c = acc.id;
        posAcc.Effective_End_Date__c = Date.newInstance(2016,08,09);
        posAcc.Effective_Start_Date__c = Date.newInstance(2016,04,04);
        //posAcc.isIncludedCallPlan__c = true;
        posAcc.Picklist1_Updated__c = 'Class 5';
        posAcc.Picklist1_Segment_Approved__c = '';
        posAcc.Picklist1_Segment__c = '6';
        insert posAcc;
        
        Position_Account_Call_Plan__c posAcc1 = new Position_Account_Call_Plan__c();
        posAcc1.Position__c = pos.id;
        posAcc1.isAccountTarget__c = false;
        posAcc1.Team_Instance__c = teamIns.id;
        posAcc1.Account__c = acc.id;
        posAcc1.Effective_End_Date__c = Date.newInstance(2016,08,09);
        posAcc1.Effective_Start_Date__c = Date.newInstance(2016,04,04);
        posAcc1.isincludedCallPlan__c = true;
        posAcc1.Picklist1_Updated__c = 'Class 5';
        posAcc1.Picklist1_Segment_Approved__c = '1';
        insert posAcc1;
        
        User_Access_Permission__c accessRecs = new User_Access_Permission__c();
        accessRecs.Is_Active__c = true;
        accessRecs.User__c = loggedInUser.id;
        accessRecs.Team_Instance__c = teamIns.id;
        accessRecs.Position__c =  pos.id;
        
        User_Access_Permission__c accessRecs1 = new User_Access_Permission__c();
        accessRecs1.Is_Active__c = true;
        accessRecs1.User__c = tuser.id;
        accessRecs1.Team_Instance__c = teamIns.id;
        accessRecs1.Position__c =  pos2.id;
        
        User_Access_Permission__c accessRecs2 = new User_Access_Permission__c();
        accessRecs2.Is_Active__c = true;
        accessRecs2.User__c = tuser1.id;
        accessRecs2.Team_Instance__c = teamIns.id;
        accessRecs2.Position__c =  posNation.id;
        
        User_Access_Permission__c accessRecs3 = new User_Access_Permission__c();
        accessRecs3.Is_Active__c = true;
        accessRecs3.User__c = tuser2.id;
        accessRecs3.Team_Instance__c = teamIns.id;
        accessRecs3.Position__c =  posNation.id;
        
        User_Access_Permission__c accessRecs4 = new User_Access_Permission__c();
        accessRecs3.Is_Active__c = true;
        accessRecs3.User__c = tUser3.id;
        accessRecs3.Team_Instance__c = teamIns.id;
        accessRecs3.Position__c =  posDM.id;
        
        insert new User_Access_Permission__c[]{accessRecs, accessRecs1, accessRecs2, accessRecs3,accessRecs4} ;
        
        Team_Instance_Account__c lstTeamInstanceAccount = CallPlanTestDataFactory.createTeamInstanceAccount(acc,teamIns);           
        insert lstTeamInstanceAccount;
        
        CR_Team_Instance_Config__c crTeamInst = new CR_Team_Instance_Config__c();
        crTeamInst.Configuration_Name__c = '4';
        crTeamInst.Configuration_Type__c = SalesIQGlobalConstants.HIERARCHY_CONFIG;
        crTeamInst.Configuration_Value__c = 'Nation';
        crTeamInst.Config_Value__c = false;
        crTeamInst.Team_Instance__c = teamIns.id;
        insert crTeamInst;
        
        set<string> allFieldsTest = new set<string>();
        allFieldsTest.add('Account__r.FirstName__c');
        allFieldsTest.add('Account__r.LastName__c');
        allFieldsTest.add('Account__c');
        allFieldsTest.add('Account__r.Name__c');
        allFieldsTest.add('Change_Status__c');
        allFieldsTest.add('Account__r.Speciality__c');
        allFieldsTest.add('Account__r.AccountNumber');
        allFieldsTest.add('Picklist2_Segment__c');
        allFieldsTest.add('Picklist1_Segment__c');
        allFieldsTest.add('Picklist3_Updated__c');
        allFieldsTest.add('Metric3__c');
        allFieldsTest.add('isIncludedCallPlan__c');
        allFieldsTest.add('Account__r.BillingCity');
        allFieldsTest.add('Account__r.BillingState');
        allFieldsTest.add('Account__r.BillingPostalCode');
        allFieldsTest.add('Segment6__c');
        allFieldsTest.add('Segment7__c');
        allFieldsTest.add('Segment8__c');
        allFieldsTest.add('Segment9__c');
        allFieldsTest.add('Segment10__c');
        allFieldsTest.add('Metric6__c');
        allFieldsTest.add('Metric7__c');
        allFieldsTest.add('Metric8__c');
        allFieldsTest.add('Metric9__c');
        allFieldsTest.add('Metric10__c');
        allFieldsTest.add('Source__c');
        
        Change_Request_Type__c crType = CallPlanTestDataFactory.CreateCallPlanRequestType();
        insert crType;
        
        list<CIM_Config__c> cimConfigs = CallPlanTestDataFactory.createCIMCOnfig(crType, teamIns, 'Position_Account_Call_Plan__c');
        cimConfigs[0].Name = 'No of Targets';
        cimConfigs[1].Name = 'Total Calls';
        cimConfigs[2].Name = 'No of Tier1 Physician';
        cimConfigs[3].Name = 'No of Tier2 Physician';
        cimConfigs[4].Name = 'No of Tier3 Physician';
        cimConfigs[5].Name = 'No of Non-Target Physician';
        insert cimConfigs;
        
        list<CIM_Position_Metric_Summary__c> cimSummary = CallPlanTestDataFactory.createCIMMetricSummary(cimConfigs,posTeam,teamIns);
        insert cimSummary;
        
        map<String, map<String,String>> changedMap = new map<String, map<String,String>>();
        map<String,String> temp = new map<String,String>();
        temp.put('tierChangedFlag','true');
        temp.put('originalTier','Non Target');
        temp.put('tier','Non Target');
        
       
        System.runAs(loggedinuser){
            CallPlan cons = new CallPlan();
            
            cons.selectedPosition = pos.Id;
            cons.selectedTeamInstance = teamIns.Id;
            cons.userType = 'Rep';
            cons.lock = true;
            
            cons.showReasonCodeError();
            cons.showSalesDirectionError();
            cons.callSaveError();
            
            
            CallPlan.phyWrapper testWrap = new CallPlan.phyWrapper(posAcc, cons.segmentToValueMap, allFieldsTest);
            
            teamIns.Rep_End_Date__c = system.today().addDays(10);
            teamIns.DM_End_Date__c = system.today().addDays(10);
            update teamIns;
            //cons.submit();
            
            list<Profile> testProfile = [select id from Profile where Name = 'DM' limit 1];
            if(testProfile.size() >  0){
                testProfile = [select id from Profile where Name = 'System Administrator' limit 1];
            }
            
            posacc.Change_Status__c = 'Pending for Submission';
            update posAcc;
            
            cons.allFields += 'Picklist2_Segment__c , Picklist1_Segment__c';
            
            CallPlan.runQuery(cons.soql,pos.Id,teamIns.Id,cons.segmentToValueMap,cons.allFields);
            
            changedMap.put(posAcc.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','Class 1');
            temp.put('tier','Class 5');
            changedMap.put(posAcc1.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','Class 2');
            temp.put('tier','Class 5');
            changedMap.put(posAcc1.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','Class 3');
            temp.put('tier','Class 5');
            changedMap.put(posAcc1.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','Class 5');
            temp.put('tier','Class 5');
            changedMap.put(posAcc1.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','Class 1');
            temp.put('tier','Class 1');
            changedMap.put(posAcc.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','Class 2');
            temp.put('tier','Class 1');
            changedMap.put(posAcc.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','Class 3');
            temp.put('tier','Class 1');
            changedMap.put(posAcc.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','Class 5');
            temp.put('tier','Class 1');
            changedMap.put(posAcc.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','Class 1');
            temp.put('tier','Class 2');
            changedMap.put(posAcc.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','Class 2');
            temp.put('tier','Class 2');
            changedMap.put(posAcc.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','Class 3');
            temp.put('tier','Class 2');
            changedMap.put(posAcc.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','Class 5');
            temp.put('tier','Class 2');
            changedMap.put(posAcc.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','Class 1');
            temp.put('tier','Class 3');
            changedMap.put(posAcc.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','Class 2');
            temp.put('tier','Class 3');
            changedMap.put(posAcc.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','Class 3');
            temp.put('tier','Class 3');
            changedMap.put(posAcc.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','Class 5');
            temp.put('tier','Class 3');
            changedMap.put(posAcc.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('tierChangedFlag','true');
            temp.put('originalTier','3');
            temp.put('tier','3');
            temp.put('reasonCode','A');
            temp.put('SalesDirectionChangeFlag','true');
            temp.put('UpdatedSalesDirectionVal','1');
            changedMap.put(posAcc.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            changedMap = new map<String, map<String,String>>();
            temp = new map<String,String>();
            temp.put('SalesDirectionChangeFlag','true');
            temp.put('UpdatedSalesDirectionVal','1');
            changedMap.put(posAcc.Id,temp);
            CallPlan.savefunc(changedMap, pos.Id, teamIns.Id);
            
            CallPlan.undo(pos.Id,teamIns.Id);
            
            test.startTest();
            cons.mapParentIdPositionId = new map<ID, list<Position__c>>();
            cons.territoryListChange();
            cons.territoryTableChange();
            //cons.changeTerritory();
            cons.showSalesDirectionErrorClass5();
            cons.horizontalBar();
            cons.regionList = new list<SelectOption>();
            cons.districtSelected = '';
            cons.allTerritory = new list<SelectOption>();
            
            //cons.submit();
        }
        
        System.runAs(tuser){
            CallPlan cons = new CallPlan();
        }
        
        System.runAs(tuser1){
            CallPlan cons = new CallPlan();
        }
        
        System.runAs(tuser2){
            CallPlan cons = new CallPlan();
        }
        
        test.stopTest();
    }
    catch(Exception ex){

    }
    }

    */
}