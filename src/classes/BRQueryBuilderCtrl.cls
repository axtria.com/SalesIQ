public class BRQueryBuilderCtrl {
    
    static String OPERATOR_NOT_IN_NULL = 'is_not_null';
    static String OPERATOR_IS_NULL = 'is_null';
    static String OPERATOR_BETWEEN = 'between';
    static String OPERATOR_ENDS_WITH = 'ends_with';
    static String OPERATOR_BEGINS_WITH = 'begins_with';
    static String OPERATOR_CONTAINS = 'contains';
    static String OPERATOR_DOESNOTCONTAINS = 'does_not_contains';
    static String OPERATOR_NOT_IN = 'not_in';
    static String OPERATOR_IN = 'in';
    static String OPERATOR_EQUALS = 'equals';
    static String OPERATOR_NOT_EQUALS = 'not_equals';


    @AuraEnabled 
    public static BRFilterConditionWrapper initializeBlock(String queryJSON){
        if(queryJSON==null || queryJSON=='')
           return createGroup(); 
        else
            return (BRFilterConditionWrapper)JSON.deserialize(queryJSON,BRFilterConditionWrapper.class);
    }

    @AuraEnabled
    public static BRFilterConditionWrapper.ConditionWrapper createCriteria(){
        BRFilterConditionWrapper.ConditionWrapper wrapperObj = new BRFilterConditionWrapper.ConditionWrapper();
        wrapperObj.field = '';
        wrapperObj.dataType = '';
        wrapperObj.operator = '';
        wrapperObj.value = '';    
        wrapperObj.anotherValue = '';
        wrapperObj.colValueType = '';
        wrapperObj.isGroup = false;
        wrapperObj.innerConditionWrapper = new BRFilterConditionWrapper();
        return wrapperObj;
    }

    @AuraEnabled
    public static BRFilterConditionWrapper addInnerGroup(String filterWrap){
        BRFilterConditionWrapper filterWrapper = (BRFilterConditionWrapper)JSON.deserialize(filterWrap,BRFilterConditionWrapper.class);
        system.debug('--'+filterWrapper);
        BRFilterConditionWrapper.ConditionWrapper wrapperObj = new BRFilterConditionWrapper.ConditionWrapper();
        wrapperObj.isGroup = true;
        wrapperObj.innerConditionWrapper = createGroup();
        filterWrapper.criterias.add(wrapperObj); 
        
        return filterWrapper ;
    }

    private static BRFilterConditionWrapper createGroup(){
        BRFilterConditionWrapper brFilterCondtionWrap = new BRFilterConditionWrapper();
        brFilterCondtionWrap.bindClause = 'AND';

        List<BRFilterConditionWrapper.ConditionWrapper> conditionWrapperListCriteria = new List<BRFilterConditionWrapper.ConditionWrapper>{createCriteria()};
        brFilterCondtionWrap.criterias = conditionWrapperListCriteria;
        return brFilterCondtionWrap;
    }

    @AuraEnabled
    public static BRFilterConditionWrapper addInnerCriteria(String filterWrap){
        BRFilterConditionWrapper filterWrapper = (BRFilterConditionWrapper)JSON.deserialize(filterWrap,BRFilterConditionWrapper.class);
        BRFilterConditionWrapper.ConditionWrapper wrapperObj = createCriteria();
        filterWrapper.criterias.add(wrapperObj);
        return filterWrapper;
    }

    @AuraEnabled 
    public static BRFilterConditionWrapper deleteInnerCriteria(String filterWrapper, Integer idx){
        BRFilterConditionWrapper wrapFilter = (BRFilterConditionWrapper)JSON.deserialize(filterWrapper,BRFilterConditionWrapper.class); 
        wrapFilter.criterias.remove(idx);
        return wrapFilter;
        
    }

    @AuraEnabled 
    public static BRFilterConditionWrapper deleteInnerGroup(String filterWrapper){
        BRFilterConditionWrapper wrapFilter = (BRFilterConditionWrapper)JSON.deserialize(filterWrapper,BRFilterConditionWrapper.class); 

        Integer index;
        Integer counter = 0;
        if(wrapFilter.criterias!=null ){
            for(BRFilterConditionWrapper.ConditionWrapper condWrap : wrapFilter.criterias) {
                if(condWrap.isGroup==true && condWrap.innerConditionWrapper==null){
                    index = counter;
                }
                counter++;
            }
        }

        if(index != null)
            wrapFilter.criterias.remove(index);
        
        return wrapFilter;
    }

    @AuraEnabled
    public static String[] generateQuery(String filterWrapper,String columnNames,String listOfOperators){
        system.debug('filterWrapper>>'+filterWrapper);
        system.debug('columnNames>>'+(List<BRColumnWrapper>)JSON.deserialize(columnNames, List<BRColumnWrapper>.class));
        system.debug('listOfOperators>>'+(List<BROperatorsWrapper>)JSON.deserialize(listOfOperators, List<BROperatorsWrapper>.class));
        String result = '';
        String stagingResult = '';
        String sourceResult = '';
        List<String> results = new List<String>();
        BRFilterConditionWrapper wrapFilter = (BRFilterConditionWrapper)JSON.deserialize(filterWrapper,BRFilterConditionWrapper.class);
        List<BRColumnWrapper> lstOfFields = (List<BRColumnWrapper>)JSON.deserialize(columnNames, List<BRColumnWrapper>.class);
        List<BROperatorsWrapper> lstofOperators = (List<BROperatorsWrapper>)JSON.deserialize(listOfOperators, List<BROperatorsWrapper>.class);
        System.debug(wrapFilter);

        Map<String,String> mapOfFields = new Map<String,String>();
        Map<String,String> mapOfSourceColumn = new Map<String,String>();
        Map<String,String> mapOfColumnValueType = new Map<String,String>();
        Map<String,String> mapOfOperators = new Map<String,String>();
        Map<String,String> mapOfStagingOperators = new Map<String,String>();
        Map<String,String> mapOfSourceOperators = new Map<String,String>();
        
        for(BRColumnWrapper obj:lstOfFields){
            mapOfFields.put(obj.tableColumnName,obj.Name);
            mapOfSourceColumn.put(obj.tableColumnName,obj.sourceColumn); //FOR LOOP BELOW COMMENTED
            mapOfColumnValueType.put(obj.tableColumnName,obj.colValuetype);//FOR LOOP BELOW COMMENTED
        }

        //for(BRColumnWrapper obj:lstOfFields){
        //    mapOfSourceColumn.put(obj.tableColumnName,obj.sourceColumn);
        //}
        
        //for(BRColumnWrapper obj:lstOfFields){
        //    mapOfColumnValueType.put(obj.tableColumnName,obj.colValuetype);
        //}

        for(BROperatorsWrapper obj:lstofOperators){
            if(obj.dataType == CPGConstants.DATATYPE_TEXT || obj.dataType == CPGConstants.DATATYPE_NUMERIC){
                for(BROperatorsWrapper.OperatorWrapper innerObj:obj.lstOfOperators){
                    mapOfOperators.put(innerObj.operatorsName,innerObj.operatorTranslated);
                    mapOfStagingOperators.put(innerObj.operatorsName,innerObj.operatorStaging);
                    mapOfSourceOperators.put(innerObj.operatorsName,innerObj.operatorSource);
                }
            }
        }
        system.debug('mapOfFields::'+mapOfFields);
        system.debug('mapOfOperators::'+mapOfOperators);
        system.debug('mapOfStagingOperators::'+mapOfStagingOperators);


        if(wrapFilter.criterias!=null ){
            result = result + generateInnerQuery(wrapFilter.criterias, wrapFilter.bindClause,mapOfFields,mapOfOperators,mapOfSourceOperators); 
            stagingResult = stagingResult + generateStagingInnerQuery(wrapFilter.criterias, wrapFilter.bindClause,mapOfFields,mapOfStagingOperators);
            sourceResult = sourceResult + generateSourceInnerQuery(wrapFilter.criterias, wrapFilter.bindClause,mapOfSourceColumn,mapOfSourceOperators);
        }  
        results.add(result);
        results.add(stagingResult);
        results.add(sourceResult);
        System.debug('--result ' + results);
        return results;
    }

    private static String generateInnerQuery(List<BRFilterConditionWrapper.ConditionWrapper> criterias, String bindClause,Map<String,String> mapOfFields,Map<String,String> mapOfOperators,Map<String,String>  mapOfSourceOperators){
        system.debug('criterias>>'+criterias);
        system.debug('bindClause>>'+bindClause);
        system.debug('mapOfFields>>'+mapOfFields);
        system.debug('mapOfOperators>>'+mapOfOperators);
        String result = '';
        String stagingResult ='';
        Integer index = 0;

        
        if(criterias == null || criterias.size() == 0 )
            return '' ;
        else {
            List<String> criteriaExpandedList = new List<String>();
            for(BRFilterConditionWrapper.ConditionWrapper condWrap : criterias) 
            {
                if(condWrap.isGroup) {
                    String intermediateQuery = generateInnerQuery(condWrap.innerConditionWrapper.criterias, condWrap.innerConditionWrapper.bindClause,mapOfFields,mapOfOperators, mapOfSourceOperators);
                    if(intermediateQuery.trim() != '')
                        criteriaExpandedList.add(intermediateQuery);
                } 
                else
                { 
                    system.debug(':::::::: condWrap.dataType :::::::'+ condWrap.dataType);
                    String operatorVal = mapOfOperators.get(condWrap.operator);
                    operatorVal = operatorVal == null ? '' : operatorVal ;
                        
                    String fieldVal = mapOfFields.get(condWrap.field);
                    fieldVal = fieldVal == null ? '' : fieldVal;
                    //FOR NUMERIC AND TEXT FIELDS
                    if(condWrap.operator != OPERATOR_IS_NULL && condWrap.operator != OPERATOR_NOT_IN_NULL && condWrap.operator != OPERATOR_BETWEEN ){
                        criteriaExpandedList.add(fieldVal + ' ' + operatorVal + ' ' +  condWrap.value);
                    }
                    else if(condWrap.operator == OPERATOR_BETWEEN){
                        criteriaExpandedList.add(fieldVal + ' ' + operatorVal + ' ' +  condWrap.value + ' and ' + condWrap.anotherValue);
                    }
                    else if(condWrap.operator == OPERATOR_IS_NULL || condWrap.operator == OPERATOR_NOT_IN_NULL){
                        criteriaExpandedList.add(fieldVal + ' ' + operatorVal + ' ');
                    }
                }
            }
            if(!criteriaExpandedList.isEmpty())
                result = String.join(criteriaExpandedList, ' ' + bindClause + ' ');
            if(result.trim() == '')
                return '';
            else
                return '( ' + result + ' )' ;
        }
    }
    private static String generateStagingInnerQuery(List<BRFilterConditionWrapper.ConditionWrapper> criterias, String bindClause,Map<String,String> mapOfFields,Map<String,String> mapOfOperators){
        String stagingResult ='';
        Integer index = 0;

        
        if(criterias == null || criterias.size() == 0 )
            return '' ;
        else {
            List<String> criteriaStagingExpandedList = new List<String>();
            for(BRFilterConditionWrapper.ConditionWrapper condWrap : criterias) 
            {
                if(condWrap.isGroup) {
                    String intermediateQuery = generateStagingInnerQuery(condWrap.innerConditionWrapper.criterias, condWrap.innerConditionWrapper.bindClause,mapOfFields,mapOfOperators);
                    if(intermediateQuery.trim() != '')
                        criteriaStagingExpandedList.add(intermediateQuery);
                } 
                else
                { 
                    String operatorVal = mapOfOperators.get(condWrap.operator);
                    operatorVal = operatorVal == null ? '' : operatorVal ;
                    String postgresFormatWhereClause = '';
                    
                    if( condWrap.dataType == CPGConstants.DATATYPE_MULTIVALUE ){
                        //MULTIVALUE CUSTOM METADATA VALUES
                        List<CategoryPostgresOperands__mdt> lstMetaData = [select PostgresOperands__c, SalesforceOperands__c,SourceOperands__c, sequence__c, datatype__c,Operator_Label__c from CategoryPostgresOperands__mdt where datatype__c = 'Multivalue'];
                        map<String,String> mapPostgresOperand = new map<String,String>();

                        for(CategoryPostgresOperands__mdt md :lstMetaData){
                            mapPostgresOperand.put(md.SalesforceOperands__c, md.PostgresOperands__c);
                        }
                        //FOR MULTIVALUE FIELDS
                        postgresFormatWhereClause = mapPostgresOperand.get(condWrap.operator);

                        String formattedValues = '';
                        List<String> valueList = condWrap.value.split(',');
                        for(String val : valueList){
                            formattedValues += '\''+val.toLowerCase()+'\''+',';
                        }
                        formattedValues = formattedValues.removeEnd(',');
                        //if(( condWrap.operator == OPERATOR_NOT_IN  ||  condWrap.operator == OPERATOR_NOT_EQUALS ) ){
                        //    formattedValues +=  ','+'\'\'';
                        //}
                        postgresFormatWhereClause = postgresFormatWhereClause.replace('0','LOWER('+condWrap.field+')');
                        postgresFormatWhereClause = postgresFormatWhereClause.replace('1',formattedValues);
                    }
                   



                    if(condWrap.operator == OPERATOR_ENDS_WITH){
                        criteriaStagingExpandedList.add('LOWER('+condWrap.field+')' + ' '+ operatorVal + ' ' +  '\'%' + (condWrap.value).toLowerCase() +'%\'');
                    }
                    else if(condWrap.operator == OPERATOR_BEGINS_WITH){
                        criteriaStagingExpandedList.add('LOWER('+condWrap.field+')' + ' '+ operatorVal + ' ' +  '\'' + (condWrap.value).toLowerCase() +'%\'');
                    }
                    else if(condWrap.operator == OPERATOR_BETWEEN){
                        criteriaStagingExpandedList.add(condWrap.field + ' '+ operatorVal + ' ' +  condWrap.value + ' and ' + condWrap.anotherValue);
                    }
                    else if(condWrap.operator == OPERATOR_CONTAINS){
                        criteriaStagingExpandedList.add('LOWER('+condWrap.field+')' + ' '+ operatorVal + ' ' +  '\'%' + (condWrap.value).toLowerCase() +'%\'');
                    }
                    else if(condWrap.operator == OPERATOR_DOESNOTCONTAINS){
                        criteriaStagingExpandedList.add('LOWER('+condWrap.field+')' + ' '+ operatorVal + ' ' +  '\'%' + (condWrap.value).toLowerCase() +'%\'');
                    }
                    else if(condWrap.operator == OPERATOR_IS_NULL || condWrap.operator == OPERATOR_NOT_IN_NULL){
                        criteriaStagingExpandedList.add(condWrap.field + ' ' + operatorVal + ' ');
                    }
                    else if(condWrap.operator == OPERATOR_NOT_EQUALS || condWrap.operator == OPERATOR_EQUALS){
                        if(condWrap.dataType == CPGConstants.DATATYPE_MULTIVALUE){
                            criteriaStagingExpandedList.add(postgresFormatWhereClause);
                        }
                        else{
                            criteriaStagingExpandedList.add(condWrap.field + ' '+ operatorVal + ' ' + '\''+condWrap.value+'\'');
                        }
                    }  
                    else if(condWrap.operator == OPERATOR_NOT_IN || condWrap.operator == OPERATOR_IN) {
                        if(condWrap.dataType == CPGConstants.DATATYPE_TEXT) {
                            List<String> values = condWrap.value.split(',');
                            List<String> outputValues = new List<String>();
                            for(String eachVal : values) {
                                outputValues.add('\'' + eachVal.trim().toLowerCase() +'\'');
                            }

                            criteriaStagingExpandedList.add('LOWER('+condWrap.field+')' + ' '+ operatorVal + ' ' +  '(' + String.join(outputValues, ',') + ')');
                        }else if(condWrap.dataType == CPGConstants.DATATYPE_MULTIVALUE) {
                            criteriaStagingExpandedList.add(postgresFormatWhereClause);
                        }
                        else {
                            criteriaStagingExpandedList.add(condWrap.field + ' '+ operatorVal + ' ' +  '(' + condWrap.value + ')');
                        }
                    }
                    else {
                        if(condWrap.dataType == CPGConstants.DATATYPE_TEXT){
                            criteriaStagingExpandedList.add('LOWER('+condWrap.field+')' + ' '+ operatorVal + ' ' +  '\''+(condWrap.value).toLowerCase()+'\'');
                        } else {
                            criteriaStagingExpandedList.add(condWrap.field + ' '+ operatorVal + ' ' +  condWrap.value);
                        }
                    }
                }
            }
            system.debug('criteriaStagingExpandedList>>'+criteriaStagingExpandedList);
            if(!criteriaStagingExpandedList.isEmpty())
                stagingResult = String.join(criteriaStagingExpandedList, ' ' + bindClause + ' ');
                system.debug('stagingResult>>>'+stagingResult);
            if(stagingResult.trim() == '')
                return '';
            else
                return '( ' + stagingResult + ' )' ;
        }
    }

    private static String generateSourceInnerQuery(List<BRFilterConditionWrapper.ConditionWrapper> criterias, String bindClause,Map<String,String> mapOfFields,Map<String,String> mapOfOperators){
        String sourceResult ='';
        Integer index = 0;

        
        if(criterias == null || criterias.size() == 0 )
            return '' ;
        else {
            List<String> criteriaSourceExpandedList = new List<String>();
            for(BRFilterConditionWrapper.ConditionWrapper condWrap : criterias) 
            {
                if(condWrap.isGroup) {
                    String intermediateQuery = generateSourceInnerQuery(condWrap.innerConditionWrapper.criterias, condWrap.innerConditionWrapper.bindClause,mapOfFields,mapOfOperators);
                    if(intermediateQuery.trim() != '')
                        criteriaSourceExpandedList.add(intermediateQuery);
                } 
                else
                { 
                    String operatorVal = mapOfOperators.get(condWrap.operator);
                    operatorVal = operatorVal == null ? '' : operatorVal ;

                    String fieldVal = mapOfFields.get(condWrap.field);
                    fieldVal = fieldVal == null ? '' : fieldVal ;

                    //MULTIVALUE CUSTOM METADATA VALUES
                    List<CategoryPostgresOperands__mdt> lstMetaData = [select PostgresOperands__c, SalesforceOperands__c,SourceOperands__c, sequence__c, datatype__c,Operator_Label__c from CategoryPostgresOperands__mdt where datatype__c = 'Multivalue'];
                    map<String,String> multiValueOperands = new map<String,String>();

                    for(CategoryPostgresOperands__mdt md :lstMetaData){
                        multiValueOperands.put(md.SalesforceOperands__c, md.SourceOperands__c);
                    }

                    if(condWrap.operator == OPERATOR_ENDS_WITH){
                        criteriaSourceExpandedList.add(fieldVal + ' '+ operatorVal + ' ' +  '\'%' +condWrap.value +'\'');
                    }
                    else if(condWrap.operator == OPERATOR_BEGINS_WITH){
                        criteriaSourceExpandedList.add(fieldVal + ' '+ operatorVal + ' ' +  '\'' +condWrap.value +'%\'');
                    }
                    else if(condWrap.operator == OPERATOR_BETWEEN){
                        criteriaSourceExpandedList.add('( '+fieldVal + ' >= ' +  condWrap.value + ' and ' + fieldVal + ' <= ' + condWrap.anotherValue+' )');
                    }
                    else if(condWrap.operator == OPERATOR_CONTAINS){
                        criteriaSourceExpandedList.add(fieldVal + ' '+ operatorVal + ' ' +  '\'%' + condWrap.value +'%\'');
                    }
                    else if(condWrap.operator == OPERATOR_IS_NULL || condWrap.operator == OPERATOR_NOT_IN_NULL){
                        criteriaSourceExpandedList.add(fieldVal + ' ' + operatorVal + ' ');
                    } 
                    else if(condWrap.operator == OPERATOR_NOT_IN || condWrap.operator == OPERATOR_IN) {
                        if(condWrap.dataType == CPGConstants.DATATYPE_TEXT || condWrap.dataType == CPGConstants.DATATYPE_MULTIVALUE) {
                            List<String> values = condWrap.value.split(',');
                            List<String> outputValues = new List<String>();
                            for(String eachVal : values) {
                                outputValues.add('\'' + eachVal.trim() +'\'');
                            }

                            if(condWrap.dataType == CPGConstants.DATATYPE_MULTIVALUE && condWrap.operator == OPERATOR_IN)
                                criteriaSourceExpandedList.add(fieldVal + ' '+ multiValueOperands.get(condWrap.operator) + ' ' +  '(' + String.join(outputValues, ',') + ')');

                            else if(condWrap.dataType == CPGConstants.DATATYPE_MULTIVALUE && condWrap.operator == OPERATOR_NOT_IN)
                                criteriaSourceExpandedList.add(fieldVal + ' '+ multiValueOperands.get(condWrap.operator) + ' ' +  '(\'' + String.join(values, ';') + '\')');

                            else
                                criteriaSourceExpandedList.add(fieldVal + ' '+ operatorVal + ' ' +  '(' + String.join(outputValues, ',') + ')');

                        } else {
                            criteriaSourceExpandedList.add(fieldVal + ' '+ operatorVal + ' ' +  '(' + condWrap.value + ')');
                        }
                    }
                    else {
                        if(condWrap.dataType == CPGConstants.DATATYPE_TEXT){
                            criteriaSourceExpandedList.add(fieldVal + ' '+ operatorVal + ' ' +  '\''+condWrap.value+'\'');
                        } 
                        else if(condWrap.dataType == CPGConstants.DATATYPE_MULTIVALUE) {
                            criteriaSourceExpandedList.add(fieldVal + ' '+ multiValueOperands.get(condWrap.operator) + ' (' +  '\''+condWrap.value+'\')');
                        } 
                        else {
                            criteriaSourceExpandedList.add(fieldVal + ' '+ operatorVal + ' ' +  condWrap.value);
                        }
                    }
                }
            }
            system.debug('criteriaSourceExpandedList>>'+criteriaSourceExpandedList);
            if(!criteriaSourceExpandedList.isEmpty())
                sourceResult = String.join(criteriaSourceExpandedList, ' ' + bindClause + ' ');
                system.debug('sourceResult>>>'+sourceResult);
            if(sourceResult.trim() == '')
                return '';
            else
                return '( ' + sourceResult + ' )' ;
        }
    }

    @AuraEnabled
    public static List<BROperatorsWrapper> getOperatorValues(){           
        List<BROperatorsWrapper> lstStrOperator= new List<BROperatorsWrapper>();  

        List<CategoryPostgresOperands__mdt> lstMetaData = [select PostgresOperands__c, SalesforceOperands__c,SourceOperands__c, sequence__c, datatype__c,Operator_Label__c from CategoryPostgresOperands__mdt order by sequence__c];
        
        Map<String, List<BROperatorsWrapper.OperatorWrapper>> dataTypeToOperators = new Map<String, List<BROperatorsWrapper.OperatorWrapper>>();
        
        if(lstMetaData!= null && lstMetaData.size()>0){
            for(CategoryPostgresOperands__mdt operands : lstMetaData){
                
                String dataType ;
                if(operands.datatype__c.equalsIgnoreCase(CPGConstants.DATATYPE_TEXTNUMERIC) || operands.datatype__c.equalsIgnoreCase(CPGConstants.DATATYPE_TEXT)){
                    dataType = CPGConstants.DATATYPE_TEXT.toLowerCase();

                    if(dataTypeToOperators.containsKey(dataType))
                        dataTypeToOperators.get(dataType).add(new BROperatorsWrapper.OperatorWrapper(operands.SalesforceOperands__c, operands.Operator_Label__c, operands.PostgresOperands__c,operands.SourceOperands__c));
                    else
                        dataTypeToOperators.put(dataType, new List<BROperatorsWrapper.OperatorWrapper>{new BROperatorsWrapper.OperatorWrapper(operands.SalesforceOperands__c, operands.Operator_Label__c, operands.PostgresOperands__c,operands.SourceOperands__c)});
                }

                if(operands.datatype__c.equalsIgnoreCase(CPGConstants.DATATYPE_TEXTNUMERIC) || operands.datatype__c.equalsIgnoreCase(CPGConstants.DATATYPE_NUMERIC)){
                    dataType = CPGConstants.DATATYPE_NUMERIC.toLowerCase();

                    if(dataTypeToOperators.containsKey(dataType))
                        dataTypeToOperators.get(dataType).add(new BROperatorsWrapper.OperatorWrapper(operands.SalesforceOperands__c, operands.Operator_Label__c, operands.PostgresOperands__c,operands.SourceOperands__c));
                    else
                        dataTypeToOperators.put(dataType, new List<BROperatorsWrapper.OperatorWrapper>{new BROperatorsWrapper.OperatorWrapper(operands.SalesforceOperands__c, operands.Operator_Label__c, operands.PostgresOperands__c,operands.SourceOperands__c)});
                }

                if(operands.datatype__c.equalsIgnoreCase(CPGConstants.DATATYPE_MULTIVALUE) || operands.datatype__c.equalsIgnoreCase(CPGConstants.DATATYPE_MULTIVALUE)){
                    dataType = CPGConstants.DATATYPE_MULTIVALUE.toLowerCase();
                    if(dataTypeToOperators.containsKey(dataType))
                        dataTypeToOperators.get(dataType).add(new BROperatorsWrapper.OperatorWrapper(operands.SalesforceOperands__c, operands.Operator_Label__c, operands.PostgresOperands__c,operands.SourceOperands__c));
                    else
                        dataTypeToOperators.put(dataType, new List<BROperatorsWrapper.OperatorWrapper>{new BROperatorsWrapper.OperatorWrapper(operands.SalesforceOperands__c, operands.Operator_Label__c, operands.PostgresOperands__c,operands.SourceOperands__c)});
                }
            }        
        }

        for(String key : dataTypeToOperators.keySet()){
            lstStrOperator.add(new BROperatorsWrapper(key, dataTypeToOperators.get(key)));
        }
        system.debug(lstStrOperator);
        return lstStrOperator;
    }
    @AuraEnabled
    public static list<BRColumnWrapper> createColumnWrapperData(String scenarioRuleInstanceId,String fileType){
        list<BRColumnWrapper> lstColumnData = new list<BRColumnWrapper>();    
        list<Data_Set_Rule_Map__c> lstRuleMapOutput = [select id,dataset_id__c,table_display_name__c from Data_Set_Rule_Map__c
                                                 where ds_type__c='I' and scenario_rule_instance_id__r.Id =: scenarioRuleInstanceId and File_Type__c =: fileType];
        list<Data_Set_Column_Detail__c> lstcolumnsOutput;
        System.debug('fileType ::: '+fileType);
        System.debug('lstRuleMapOutput ::: '+lstRuleMapOutput);
        if(lstRuleMapOutput.size()>0)
        {
            lstcolumnsOutput=[select Id,ds_col_name__c,column_id__c,datatype__c,Source_column__c,value_type__c,variable_type__c,
                        tb_col_nm__c,Table_Data_type__c from Data_Set_Column_Detail__c 
                        where dataset_id__r.Id=:lstRuleMapOutput[0].dataset_id__c Order by ds_col_name__c]; //a1af4000000hFWiAAM
            
        }
        for(Data_Set_Column_Detail__c n : lstcolumnsOutput)
        {
              BRColumnWrapper columns=new BRColumnWrapper();
              columns.Id=n.Id;
              columns.Name=n.ds_col_name__c;
              columns.variabletype=n.variable_type__c;
              columns.colDatatype=n.datatype__c;
              columns.colValuetype=n.value_type__c;
              columns.tableDataType = n.Table_Data_type__c;
              columns.tableColumnName = n.tb_col_nm__c;
              columns.sourceColumn = n.Source_column__c;
              lstColumnData.add(columns);
        }
        return lstColumnData;
    }

     @AuraEnabled
    public static String getColumnValues(Map<String,String> urlParamsMap) 
    {

        System.debug('urlParamsMap');
        System.debug(urlParamsMap);

        //Http h = new Http();
        ETL_Config__c config = CPGUtility.getETLConfigByName(CPGConstants.talend_Instance);
        // changes to send postgres cred to service
        //SSL 
        //url += '&pg_username=' + config.BR_PG_UserName__c + '&pg_password=' + config.BR_PG_Password__c ;
        system.debug('config'+config);
        urlParamsMap.put('pg_username', config.BR_PG_UserName__c);
        urlParamsMap.put('pg_password', config.BR_PG_Password__c);
        urlParamsMap.put('sf_username', config.SF_UserName__c);
        urlParamsMap.put('sf_password', config.SF_Password__c);
        urlParamsMap.put('sf_securitytoken', config.S3_Security_Token__c);
        String isSandbox = (SalesIQUtility.isSandboxOrg()) ? 'true' : 'false';
        urlParamsMap.put('sandbox', isSandbox);
 


        //String sf_UserName = config.SF_UserName__c;
        //String sf_Passwd = config.SF_Password__c;
        //String sf_SecurityToken = config.S3_Security_Token__c;


        //url += '&sf_username='+sf_UserName+'&sf_password='+config.SF_Password__c+'&sf_securitytoken='+sf_SecurityToken+'&sandbox='+SalesIQUtility.isSandboxOrg(); 
       
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        //HttpRequest req = new HttpRequest();
        //req.setEndpoint(url);
        //req.setMethod('GET');
        //req.setTimeout(120000);
        //req.setHeader('Content-Type', 'application/json');
        //req.setHeader('Accept','application/json');
        //// Send the request, and return a response
        //HttpResponse res = h.send(req);
        //System.debug('response:--> ' + res.getBody());
        //String result = res.getBody();
        //result = result.replace('None', 'null');
        //result = result.replace('\'', '"');
        //return result;


        HttpResponse response = SalesiqUtility.getHTTPResponse(urlParamsMap.get('finalUrl'),urlParamsMap,'POST',120000);
        String result = response.getBody();
        result = result.replace('None', 'null');
        result = result.replace('\'', '"');
        return result;
    }

    @AuraEnabled
    public static List<String> getInstanceNameAndURL(String scenarioRuleInstanceId){

        List<String> lstInstanceNameAndURL = new List<String>();
        //string brInstanceName =  CPGUtility.getBRMSConfigValues('BRGetDistinctValues');
        string brInstanceName =  CPGUtility.getBRMSConfigValues('BRGetDistinctValues');
        lstInstanceNameAndURL.add(brInstanceName);

        list<Data_Set_Rule_Map__c> lstRuleMap = [select id,dataset_id__c,dataset_id__r.base_data_set__c,table_display_name__c,Scenario__c,table_name__c from Data_Set_Rule_Map__c
                                                  where ds_type__c='i' and scenario_rule_instance_id__c=: scenarioRuleInstanceId and File_Type__c = 'Base'];
      
        if(lstRuleMap.size() == 0)
        {
            lstRuleMap = [select id,dataset_id__c,table_display_name__c,dataset_id__r.base_data_set__c,Scenario__c,table_name__c from Data_Set_Rule_Map__c
                                                  where ds_type__c='i' and scenario_rule_instance_id__c=: scenarioRuleInstanceId];
        }       
        lstInstanceNameAndURL.add(lstRuleMap[0].table_name__c);

        String scenarioId = [select Scenario_id__c from Scenario_Rule_Instance_Details__c where id=: scenarioRuleInstanceId].Scenario_id__c;

        string brdbname = CPGUtility.getBRMSConfigValues('BRDatabaseName');
        lstInstanceNameAndURL.add(brdbname);
        List<Scenario_Data_Object_Map__c> scDataObjMapList = [Select id,Final_table__c,Scenario__r.Country__c from Scenario_Data_Object_Map__c where Data_Object__c != null and Scenario__c =: scenarioId];
        String finalTables = '';
        list<String> finalTablesList = new List<string>();
        for(Scenario_Data_Object_Map__c sdom:scDataObjMapList)
        {
            finalTablesList.add(sdom.Final_table__c);
        }
        finalTables = string.join(finalTablesList,',');
        lstInstanceNameAndURL.add(finalTables);
        if( scDataObjMapList != null && scDataObjMapList.size() > 0)
            lstInstanceNameAndURL.add(scDataObjMapList[0].Scenario__r.Country__c);
        if( lstRuleMap != null && lstRuleMap.size() > 0)
            lstInstanceNameAndURL.add(lstRuleMap[0].dataset_id__r.base_data_set__c);
        lstInstanceNameAndURL.add(scenarioId);
        return lstInstanceNameAndURL;  
    }

}