/******************************************************************************************************************************************************
 * Name         :   AlignmentPeriodManagerTest.cls
 * Description  :   Test class for AlilgnmentPeriodManager.
 * Author       :   Raghvendra Rathore
 * Created On   :   06/16/2017
******************************************************************************************************************************************************/
@isTest
private class AlignmentPeriodManagerTest {
    
    static testMethod void testExecute() {
        
       	TriggerContol__c triggerControl = new TriggerContol__c (IsStopTrigger__c = true , Name='ScenarioTrigger');
        insert triggerControl;

        Team__c testTeam = new Team__c (Name='HTN', Type__c='Base', 
                                        Effective_Start_Date__c = system.today().addMonths(-5), 
                                        Effective_End_Date__c = Date.today().addMonths(5));
        insert testTeam;
        
        Team_Instance__c currentTeamInstance = new Team_Instance__c();
        currentTeamInstance.Name = 'HTN Q1';
        currentTeamInstance.Alignment_Period__c = 'Current';
        currentTeamInstance.Team__c = testTeam.id;
        currentTeamInstance.Alignment_Type__c = 'ZIP';
        currentTeamInstance.isActiveCycle__c = 'Y';           
        insert currentTeamInstance;

        Team_Instance__c scenarioTeamInstance = new Team_Instance__c();
        scenarioTeamInstance.Name = 'HTN Q2';
        scenarioTeamInstance.Alignment_Period__c = 'Future';
        scenarioTeamInstance.Team__c = testTeam.id;
        scenarioTeamInstance.Alignment_Type__c = 'ZIP';
        scenarioTeamInstance.isActiveCycle__c = 'Y';
        insert scenarioTeamInstance;

        Workspace__c workspace = new Workspace__c(Name = 'test workspace',
                                                  Workspace_Start_Date__c = system.today().addMonths(-5),
                                                  Workspace_End_Date__c = system.today().addMonths(5),
                                                  Workspace_Description__c = 'Test Workspace');
        insert workspace;
        
        Scenario__c scenario = new Scenario__c();
        scenario.Team_Name__c = testTeam.Id;
        scenario.Team_Instance__c = scenarioTeamInstance.Id;
        scenario.Effective_Start_Date__c = System.today();
        //scenario.Effective_End_Date__c = system.today().addMonths(2);
        scenario.Scenario_Stage__c = SalesIQGlobalConstants.PUBLISHING;
        scenario.Request_Process_Stage__c = SalesIQGlobalConstants.READY;
        scenario.Scenario_Status__c = 'Active';
        scenario.Workspace__c = workspace.Id;
        insert scenario;
	
       	Scenario__c liveScenario = new Scenario__c();
        liveScenario.Team_Name__c = testTeam.Id;
        liveScenario.Team_Instance__c = currentTeamInstance.Id;
        liveScenario.Effective_Start_Date__c = System.today();
        liveScenario.Effective_End_Date__c = system.today().addMonths(2);
        liveScenario.Scenario_Stage__c = 'Live';
        liveScenario.Request_Process_Stage__c = 'Ready';
        liveScenario.Scenario_Status__c = 'Active';
        liveScenario.Workspace__c = workspace.Id;
        insert liveScenario;

        
        
        test.startTest();
            AlignmentPeriodManager sh = new AlignmentPeriodManager();      
            String sch = '0 0 23 * * ?';
            system.schedule('Test check', sch, sh);
        test.stopTest();

       system.assertEquals('Past',[SELECT id, Scenario_Stage__c from Scenario__c where id =: liveScenario.Id].Scenario_Stage__c);
       system.assertEquals('Live',[SELECT id, Scenario_Stage__c from Scenario__c where id =: scenario.Id].Scenario_Stage__c);
    }
}