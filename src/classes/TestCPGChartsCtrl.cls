/*
Author : Ankit Manendra
Description : Test class for CPGChartsCtrl
Date : 8 Novomber, 2017

*/

@isTest
private class TestCPGChartsCtrl{
    static testMethod void cpgChartsTest() {
    
    Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj;
        
        Team__c teamObj = new Team__c();
        teamObj.Name='TestTeam';
        insert teamObj;
        
        Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        scenarioObj.Team_Name__c=teamObj.id;
        scenarioObj.Request_Process_Stage__c='Success';
        insert scenarioObj;
        
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj;
        
         Scenario_Rule_Instance_Details__c sceRuleInsDetObj1 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj1.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj1.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj1;
        
        Data_Set_Rule_Map__c dataSetRuleObj = new Data_Set_Rule_Map__c();
        dataSetRuleObj.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj.ds_type__c='i';
        insert dataSetRuleObj;
        
        Data_Set_Rule_Map__c dataSetRuleObj1 = new Data_Set_Rule_Map__c();
        dataSetRuleObj1.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj1.ds_type__c='o';
        insert dataSetRuleObj1;
        
        system.debug('==dataSetRuleObj===='+dataSetRuleObj);
        
        ComponentTypeMaster__c ComTypeMasObj = new ComponentTypeMaster__c();
        ComTypeMasObj.Name='Call Balancing';
        ComTypeMasObj.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj.DisplayIcon__c='add';
        ComTypeMasObj.DisplayIconClass__c='window information';
        insert ComTypeMasObj;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj.Name='Rank';
        busRuleTypeMasObj.Component_Type__c=ComTypeMasObj.id;
        insert busRuleTypeMasObj;
        
        ComponentTypeMaster__c ComTypeMasObj1 = new ComponentTypeMaster__c();
        ComTypeMasObj1.Name='Aggregation';
        ComTypeMasObj1.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj1.DisplayIcon__c='add';
        ComTypeMasObj1.DisplayIconClass__c='window information';
        insert ComTypeMasObj1;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj1 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj1.Name='Rank';
        busRuleTypeMasObj1.Component_Type__c=ComTypeMasObj1.id;
        insert busRuleTypeMasObj1;
        
        Business_Rules__c busRuleObj = new Business_Rules__c();
        busRuleObj.Name='Rank rule 278';
        busRuleObj.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObj.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj.Aggregate_Level__c='Test';
        busRuleObj.customer_prioritization__c='Test';
        insert busRuleObj;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj.Param_Name__c='derived field 278';
        busRuleFieldObj.Business_Rule__c=busRuleObj.id;
        busRuleFieldObj.Param_Data_Type__c='Numeric';
        busRuleFieldObj.Param_Type__c='Derived';
        busRuleFieldObj.IO_Flag__c='O';
        
        insert busRuleFieldObj;
        
        Computation_Rule_Detail__c compRuleDetObj = new Computation_Rule_Detail__c();
        compRuleDetObj.Business_Rules__c=busRuleObj.id;
        compRuleDetObj.Data_Type__c='Numeric';
        compRuleDetObj.Derived_field_name__c='FIELD 123';
        
        insert compRuleDetObj;
        
        Data_Set__c dataSetObj = new Data_Set__c();
        dataSetObj.Name = 'Call Plan Summary';
        insert dataSetObj;
        
        Data_Set_Column_Detail__c dataSetCol = new Data_Set_Column_Detail__c();
        dataSetCol.dataset_id__c=dataSetObj.id;
        dataSetCol.variable_type__c='Derived';
        dataSetCol.ds_col_name__c='colTest';
        insert dataSetCol;
        
        list<Data_Set_Column_Detail__c> dataSetColDetList = new list<Data_Set_Column_Detail__c>();
        dataSetColDetList.add(dataSetCol);
        
        Category_Rule_Entity_Detail__c catRuleEntDet = new Category_Rule_Entity_Detail__c();
        catRuleEntDet.Entity_Type__c='Block';
        catRuleEntDet.Business_Rule__c=busRuleObj.id;
        catRuleEntDet.Priority__c='1';
        insert catRuleEntDet;
        
        Category_Derived_Variable_Metadata__c catDerMetObj = new Category_Derived_Variable_Metadata__c();
        catDerMetObj.Field_Name__c='TestCatDer';
        catDerMetObj.Business_Rule_Id__c=busRuleObj.id;
        insert catDerMetObj;
        
        Category_Rule_Entity_Detail__c catRuleEntDet1 = new Category_Rule_Entity_Detail__c();
        catRuleEntDet1.Entity_Type__c='Group';
        catRuleEntDet1.Business_Rule__c=busRuleObj.id;
        catRuleEntDet1.Priority__c='1';
        catRuleEntDet1.Parent_Entity__c=catRuleEntDet.id;
        insert catRuleEntDet1;
        
        Category_Rule_Output_Detail__c catRuleOutDet = new Category_Rule_Output_Detail__c();
        catRuleOutDet.Category_Rule_Entity_Detail__c=catRuleEntDet.id;
        catRuleOutDet.Derived__c='TestCatDer';
        insert catRuleOutDet;
        
        Category_Rule_Criteria_Detail__c catRuleCriDet = new Category_Rule_Criteria_Detail__c();
        catRuleCriDet.FieldDataType__c='Numeric';
        catRuleCriDet.Sequence__c='1';
        insert catRuleCriDet;
        
        Test.startTest();
        
        List<String> strList = CPGChartsCtrl.getTeamInstanceDetails();
        List<Scenario__c> sceList = CPGChartsCtrl.getScenarioInstances('TestTeam');
        List<Scenario__c> sceList1 = CPGChartsCtrl.getPreviousScenarioValues(scenarioObj.id,'TestTeam');
        string url = 'https://login.salesforce.com';
        Map < String,Object > mapStr2Obj = CPGChartsCtrl.generateVisualizationReport(url);
        CPGChartsCtrl.getOrgNamespaceServer();
        CPGChartsCtrl.ChartsWrapper chartsWrap = new CPGChartsCtrl.ChartsWrapper('instance','url');
        chartsWrap=CPGChartsCtrl.getInstanceNameAndUrl();    
        Test.stopTest(); 
        
        
        }
        
     }