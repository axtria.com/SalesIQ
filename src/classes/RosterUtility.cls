/**********************************************************************************************
@author     : 
@date       : 
@description: 
Revison(s)  : Utility for Roster Management
**********************************************************************************************/

public with sharing class RosterUtility {

    public static list<Position_Employee__c> getPositionEmployeByPoistionId(set<id> TerrId){
        list<String> POSITION_EMPLOYEE_READ_FIELD = new list<String>{'name','isnew__c','Employee__c','Last_Approved_End_Date__c','Last_Approved_Assignment_Type__c', 'Last_Approved_Start_Date__c', 'Position__c','Assignment_Type__c', 'Effective_Start_Date__c', 'Effective_End_Date__c', 'Reason_Code__c'};
        list<String> POSITION_READ_FIELD = new list<String>{'id','Change_Status_del__c','name','parent_Position__c','Client_Position_Code__c','Position_Type__c'};
        list<String> EMPLOYEE_READ_FIELD = new List<String>{'Name', 'employee_id__c' };
        
        list<Position_Employee__c> curAssignments  =   new list<Position_Employee__c>();
        
        if (SecurityUtil.checkRead(Position_Employee__c.SObjectType, POSITION_EMPLOYEE_READ_FIELD, false) &&
            SecurityUtil.checkRead(Position__c.SObjectType, POSITION_READ_FIELD, false) &&
            SecurityUtil.checkRead(Employee__c.SObjectType, EMPLOYEE_READ_FIELD, false)){
            curAssignments  =   [select Employee__r.name,Employee__r.employee_id__c,Position__c,employee__c, Employee__r.Manager__c, Employee__r.Manager__r.Name, 
                                Position__r.Parent_Position__c, Assignment_Type__c,Last_Approved_Assignment_Type__c, Effective_Start_Date__c, Last_Approved_Start_Date__c, 
                                Effective_End_Date__c, Last_Approved_End_Date__c, Reason_Code__c,status__c, isnew__c,position__r.parent_position__r.name, 
                                Employee__r.Original_Hire_Date__c,Employee__r.HR_Termination_Date__c 
                                from Position_Employee__c where position__c in:TerrId];
            }
            
        return curAssignments;
    }
    
    public static Position_Employee__c getPositionEmployeByPositionAndEmployeeId(string PositionId, string EmployeeId){
        Position_Employee__c posEmp = null;
        list<string> POSITION_EMPLOYEE_READ_FIELD = new list<string>{'position__c', 'employee__c','Effective_Start_Date__c','Effective_End_Date__c'};
        list<String> POSITION_READ_FIELD = new list<String>{'name','Effective_End_Date__c','Effective_Start_Date__c'};
        list<String> EMPLOYEE_READ_FIELD = new List<String>{'Name'};
        if (SecurityUtil.checkRead(Position_Employee__c.SObjectType, POSITION_EMPLOYEE_READ_FIELD, false) &&
            SecurityUtil.checkRead(Position__c.SObjectType, POSITION_READ_FIELD, false) &&
            SecurityUtil.checkRead(Employee__c.SObjectType, EMPLOYEE_READ_FIELD, false)){
    
            posEmp = [select Position__r.name, position__r.Effective_End_Date__c, position__r.Effective_Start_Date__c,  Effective_Start_Date__c,
                                        Effective_End_Date__c, employee__r.name
                                        from position_employee__c 
                                        where employee__c=: EmployeeId and position__c=: PositionId];
        }
        
        return posEmp;
        
    }
    
    public static list<Employee_status__c> getEmployeeStatusByEmpID(set<string> EmpId, list<string> Status){
        list<Employee_Status__c> lstEmployee =null;
        list<string> EMPLOYEE_STATUS_READ_FIELD = new list<string>{'Employee__c','Effective_Start_Date__c','Effective_End_Date__c','Employee_Status__c'};
        if(SecurityUtil.checkRead(Employee_Status__c.SObjectType, EMPLOYEE_STATUS_READ_FIELD, false)){
            lstEmployee = [select id,Employee__c,Effective_Start_Date__c,Effective_End_Date__c from Employee_Status__c where Employee__c in:EmpId and Employee_Status__c in:Status];
        }
        return lstEmployee;
    }
    
    
    public static CR_Position__c getCRpositionById(string crPosId){
         CR_Position__c event;
         if((SecurityUtil.checkRead(CR_Position__c.SObjectType, new List<String>{'id'}, false)) ) { 
            event = [select id from CR_Position__c where id=:crPosId limit 1];
        } 
             
        
        return event;  
    }
    
    public static list<CR_Position__c> getCRpositionByStatus(string crPosStatus, string TeamId){
        list<CR_Position__c> crPosPendingList;
        list<string> CHANGE_REQUEST_READ_FIELD = new list<string>{'status__c','Request_Type_Change__c'};
        list<string> POSITION_READ_FIELD = new list<string>{'Team_iD__c','inactive__c','Hierarchy_Level__c'};
        list<string> CR_POSITION_READ_FIELD = new list<string>{'position__c', 'change_request__c'};
        if(SecurityUtil.checkRead(Change_request__c.SObjectType, CHANGE_REQUEST_READ_FIELD, false) && SecurityUtil.checkRead(Position__c.SObjectType, POSITION_READ_FIELD, false) && SecurityUtil.checkRead(CR_Position__c.SObjectType, CR_POSITION_READ_FIELD, false) ) { 
            crPosPendingList = [select position__c, change_request__r.Request_Type_Change__c from cr_position__c where position__r.Team_iD__c =: TeamId and position__r.Hierarchy_Level__c ='1' and change_request__r.status__c =:crPosStatus and position__r.inactive__c = false];
        }
         
        
        return crPosPendingList;
    }
    
    public static CR_Position__c getCRPositionByChangeRequestId(string ChangeReqId){
        CR_Position__c objCRpos = null;
        list<string> CR_POSITION_READ_FIELD = new list<string>{'position__c','Change_Request__c', 'position_name__c', 'Position_Change_String__c', 'Future_Reporting_Position__c'};
        if(SecurityUtil.checkRead(CR_Position__c.SObjectType, CR_POSITION_READ_FIELD, false)){
            objCRpos = [select position__c, position_name__c, Position_Change_String__c, Future_Reporting_Position__c, Future_Reporting_Position__r.Client_Position_Code__c from CR_Position__c where Change_Request__c =: ChangeReqId];
        }
            
        return objCRpos;   
        
    }
    
    
     public static list<CR_Position__c> getCRPositionByChangeRequestId(set<id> ChangeReqId){
        list<CR_Position__c> objCRpos = null;
        list<string> CR_POSITION_READ_FIELD = new list<string>{'Future_Reporting_Position__c', 'position__c','base_position_status__c', 'Change_Request__c','position_name__c','effective_end_date__c', 'Position_Change_String__c', 'position_team_instance__c'};
        if(SecurityUtil.checkRead(CR_Position__c.SObjectType, CR_POSITION_READ_FIELD, false) && SecurityUtil.checkRead(Change_request__c.SObjectType, new list<string>{'Team_Instance_ID__c','destination_position__c', 'source_position__c', 'Request_Type_Change__c'}, false) && SecurityUtil.checkRead(Position_Team_Instance__c.SObjectType, new list<string>{'team_instance_id__c'}, false)){
            objCRpos = [select Future_Reporting_Position__c, Future_Reporting_Position__r.Client_Position_Code__c, position_team_instance__c, base_position_status__c, position_team_instance__r.team_instance_id__c, position__c, effective_end_date__c, position_name__c, Position_Change_String__c, Change_Request__r.Team_Instance_ID__c, Change_Request__r.destination_position__c, Change_Request__r.source_position__c, change_request__r.Request_Type_Change__c from CR_Position__c where Change_Request__c in: ChangeReqId];
        }
            
        return objCRpos;  
        
    }
    
    public static list<Employee__c> getEmployeeById(set<string> EmployeeId){
        list<employee__c> listEmp =null;
        list<string> EMPLOYEE_READ_FIELD = new list<string>{'change_status__c', 'Original_Hire_Date__c', 'HR_Termination_Date__c', 'name', 'employee_id__c', 'Manager__c'};
        
        if(SecurityUtil.checkRead(employee__c.SObjectType, EMPLOYEE_READ_FIELD, false)){
            listEmp = [select id, change_status__c, Original_Hire_Date__c,HR_Termination_Date__c, name, employee_id__c, Manager__c, Manager__r.Name from employee__c where id in: EmployeeId];
        }
            
        return listEmp;
    }
    
    
    
    public static CR_Employee_Assignment__c getEmployeeAssignmentById(string EventId){
        CR_Employee_Assignment__c objEmpAssignment =null;
        list<string> CR_EMPLOYEE_ASSIGNMENT_READ_FIELD = new list<string>{'id','Position_ID__c'};
        if(SecurityUtil.checkRead(CR_Employee_Assignment__c.SObjectType, CR_EMPLOYEE_ASSIGNMENT_READ_FIELD, false)){
            objEmpAssignment =  [select id,Position_ID__c from CR_Employee_Assignment__c where id=:EventId];
        }
        return objEmpAssignment;
    }

    public static Set<Id> getEmployeeAssignmentsByEmployeeId(Set<String> empIds)
    {
        List<CR_Employee_Assignment__c> objEmpAssignment;
        // Map<String,CR_Employee_Assignment__c> empAssignmentMap = new Map<String,CR_Employee_Assignment__c>();
        Set<Id> pendingEmpIds = new Set<Id>();
        list<string> CR_EMPLOYEE_ASSIGNMENT_READ_FIELD = new list<string>{'id','Change_Request_ID__c','Employee_ID__c'};
        List<String> CHANGE_REQUEST_READ_FIELD = new List<String>{'Status__c'};
        if(SecurityUtil.checkRead(CR_Employee_Assignment__c.SObjectType, CR_EMPLOYEE_ASSIGNMENT_READ_FIELD, false) && SecurityUtil.checkRead(Change_Request__c.SObjectType, CHANGE_REQUEST_READ_FIELD, false)){
            objEmpAssignment =  [select id,Employee_ID__c,Change_Request_ID__c,Change_Request_ID__r.Status__c from CR_Employee_Assignment__c where Employee_ID__c in: empIds and Change_Request_ID__r.Status__c =: SalesIQGlobalConstants.REQUEST_STATUS_PENDING];
        }
        for(CR_Employee_Assignment__c crEmp : objEmpAssignment)
        {
           pendingEmpIds.add(crEmp.Employee_ID__c);
        }
        return pendingEmpIds;
    }
    
     /**
    * This method return the list of  CR_Employee_Assignment__c based on ChangeReq ID. Used in Change Request Trigger Handler
    * @param set<id> of Change Request
    * @param list<CR_Employee_Assignment__c>
    */   
    public static list<CR_Employee_Assignment__c> getEmployeeAssignmentByChangeReqId(set<id> ChangeReqId){
        list<CR_Employee_Assignment__c> lstEmpAssignment = null;
        list<string> CR_EMPLOYEE_ASSIGNMENT_READ_FIELD = new list<string>{'Employee_ID__c','Position_ID__c','Change_Request_ID__c'};
        if(SecurityUtil.checkRead(CR_Employee_Assignment__c.SObjectType, CR_EMPLOYEE_ASSIGNMENT_READ_FIELD, false)){
            lstEmpAssignment = [select id, Position_ID__c, Employee_ID__c from CR_Employee_Assignment__c where Change_Request_ID__c in: ChangeReqId];
        }
        return lstEmpAssignment;
    }
    
    
    public static Change_Request__c getChangeRequestforUpdate(string id, string SourcePosition, string DestinationPosition, string Status, datetime ExecutionDate){
        Change_Request__c objChangeRequest = new Change_Request__c (id=id); 
        objChangeRequest.Source_Position__c         = SourcePosition;
        if(DestinationPosition!='')
            objChangeRequest.Destination_Position__c    = DestinationPosition ;
        objChangeRequest.Status__c                  = Status;
        objChangeRequest.Execution_Date__c          = ExecutionDate;    
        return objChangeRequest; 
        
    }
    
    public static CR_Employee_Assignment__c getEmployeeAssignmentRosterById(string EventId){
        CR_Employee_Assignment__c objEmpAssignment =null;
        list<string> POSITION_READ_FIELD = new list<string>{'id', 'Name', 'Client_Position_Code__c','Employee__c','parent_position__c','Effective_Start_Date__c','Effective_End_Date__c'};
        list<string> CR_Employee_Assignment_READ_FIELD = new list<string>{'After_Effective_Start_Date__c','Position_ID__c', 'Employee_ID__c', 'Change_Request_ID__c','After_Effective_End_Date__c'};
        list<string> EMPLOYEE_READ_FIELD = new list<string>{'name'};
        list<string> CHANGE_REQUEST_READ_FIELD = new list<string>{'execution_date__c'};
        if(SecurityUtil.checkRead(Change_Request__c.SObjectType, CHANGE_REQUEST_READ_FIELD, false) && SecurityUtil.checkRead(Employee__c.SObjectType, EMPLOYEE_READ_FIELD, false) && SecurityUtil.checkRead(Position__c.SObjectType, POSITION_READ_FIELD, false) && SecurityUtil.checkRead(CR_Employee_Assignment__c.SObjectType, CR_EMPLOYEE_ASSIGNMENT_READ_FIELD, false)){
            objEmpAssignment = [select Employee_ID__r.name,Position_ID__c, Change_Request_ID__c, Position_ID__r.name, 
                            Change_Request_ID__r.execution_date__c, Position_ID__r.parent_position__c,After_Effective_End_Date__c,
                            After_Effective_Start_Date__c, Before_Effective_End_Date__c, Position_ID__r.Effective_End_Date__c,
                            Position_ID__r.Effective_Start_Date__c from CR_Employee_Assignment__c
                            where id=: eventId];
        }
        
        return objEmpAssignment;
        
    }
}