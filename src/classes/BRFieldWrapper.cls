public class BRFieldWrapper{
    @AuraEnabled
    public string Id{get;set;}
    @AuraEnabled
    public string fieldName{get;set;}
    @AuraEnabled
    public string variableType{get;set;}
    @AuraEnabled
    public string valueType{get;set;}
    @AuraEnabled
    public string dataType{get;set;}
    @AuraEnabled
    public string tblColName{get;set;}
    @AuraEnabled
    public string sourceColumnName{get;set;}
    
    public BRFieldWrapper(string Id,string fieldName,string variableType,string valueType,string dataType,string tblColName,String sourceColumnName){
        this.Id=Id;
        this.fieldName = fieldName;
        this.variableType = variableType;
        this.valueType = valueType; 
        this.dataType=dataType; 
        this.tblColName=tblColName; 
        this.sourceColumnName = sourceColumnName;
    }
}