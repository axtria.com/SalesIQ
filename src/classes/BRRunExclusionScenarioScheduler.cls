global class BRRunExclusionScenarioScheduler implements Schedulable{
    global void execute(SchedulableContext SC) {
      BRScenarioSchedulerHandler.queueExclusionScenarioForDeltaRun();
    } 
}