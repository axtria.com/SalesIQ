public with sharing class SalesIQListViewCtrl {

    public static String sessionId ;

    public static void login() 
    {
        PageReference newPage = Page.SessionDetails;
        String content = '';
        if (!Test.isRunningTest()){
            content = newPage.getContent().toString();
            Integer s = content.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
                e = content.indexOf('End_Of_Session_Id');
            sessionId = content.substring(s, e);
        }
        else
            sessionId = '';         
    }

    @AuraEnabled
    public static String getOrgNamespace() 
    {
        return SalesIQGlobalConstants.NAME_SPACE;        
    }

    public static Boolean isAllPositionsScenario = false;
    public static String teamIdAallPositions = '';

    @AuraEnabled
    public static List<SalesIQListViewWrapper> getListViews(String objectName,String selectedTeamInstance,String selectedPosition, String moduleName,String countryId) 
    {
        try {
            List<SalesIQListViewWrapper> wrapperList = new List<SalesIQListViewWrapper>();
            String ns = SalesIQGlobalConstants.Name_space;
            if(!objectName.startsWithIgnoreCase(ns))
                objectName = ns + objectName;

            System.debug('countryId : '+countryId);
            ApexPages.StandardSetController setCon;

            if(objectName == ns+'Position_Account_Call_Plan__c')
            {
                if(selectedTeamInstance == '' || selectedTeamInstance == null)
                {
                    List<Team_Instance__c> teamInsList = SalesIQUtility.getUserAccessTeamInstance('',moduleName, countryId);
                    if(teamInsList.isEmpty()) {
                        AuraHandledException ex = new AuraHandledException(System.Label.No_UAP_Assigned);
                        ex.setMessage(System.Label.No_UAP_Assigned);
                        throw ex;
                    }
                    selectedTeamInstance = teamInsList[0].Id;
                    if(selectedPosition == '' || selectedPosition == null)
                    {
                        String tempSelectedPosition = teamInsList[0].User_Access_Permissions__r[0].Position__c;
                        List<Position__c> territoryPositions = SalesIQUtility.getPositionWithSortFieldDynamic('1', tempSelectedPosition, true, 1);

                        if(territoryPositions.isEmpty()) {
                            AuraHandledException ex = new AuraHandledException(System.Label.Territory_Position_Not_Found);
                            ex.setMessage(System.Label.Territory_Position_Not_Found);
                            throw ex;
                        }

                        selectedPosition = territoryPositions[0].Id;
                    }
                }
                else
                {
                    List<Team_Instance__c> teamInsList = SalesIQUtility.getUserAccessTeamInstance(selectedTeamInstance, SalesIQGlobalConstants.CALL_PLAN_MODULE, countryId);
                    if(selectedPosition == null || selectedPosition == '')
                    {
                        selectedPosition = teamInsList[0].User_Access_Permissions__r[0].Position__c;
                        List<Position__c> territoryPositions = SalesIQUtility.getPositionWithSortFieldDynamic('1', selectedPosition, true, 1);
                        
                        if(territoryPositions.isEmpty()) {
                            AuraHandledException ex = new AuraHandledException(System.Label.Territory_Position_Not_Found);
                            ex.setMessage(System.Label.Territory_Position_Not_Found);
                            throw ex;
                        }

                        selectedPosition = territoryPositions[0].Id;  
                    }
                }
                if(sessionId == null){
                    login();
                }

                Http http = new Http();
                HttpRequest request = new HttpRequest();
                String urlString = URL.getSalesforceBaseUrl().toExternalForm() + '/services/data/v32.0/sobjects/' + objectName + '/listviews';
                request.setEndpoint(urlString);
                request.setMethod('GET');
                request.setHeader('Authorization', 'Bearer ' + sessionId);

                HttpResponse response = http.send(request);
                String jsonString = response.getBody();
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
                list<Object> listViewResults = (list<Object>)results.get('listviews');

                for(Object obj : listViewResults){
                    Map<String, Object> singleView = (Map<String, Object>) obj;
                    SalesIQListViewWrapper wrapper = new SalesIQListViewWrapper();
                    
                    String listViewName = (String)singleView.get('developerName');
                    String teamInstanceId = listViewName.split('_')[0];

                    String queryString;
                    if(teamInstanceId.startsWith(Team_Instance__c.SObjectType.getDescribe().getKeyPrefix())){
                        if(teamInstanceId.substring(0, 15) == selectedTeamInstance.substring(0, 15)){
                            // add the list view only when it is matching with the selected team instance
                            wrapper.listViewId = (String)singleView.get('id');
                            wrapper.listViewName = (String)singleView.get('label');
                            queryString = getListViewQuery(objectName,wrapper.listViewId,selectedTeamInstance,selectedPosition, moduleName, countryId);
                        }
                    }else{
                        wrapper.listViewId = (String)singleView.get('id');
                        wrapper.listViewName = (String)singleView.get('label');
                        queryString = getListViewQuery(objectName,wrapper.listViewId,selectedTeamInstance,selectedPosition, moduleName,countryId);
                        
                    }
                    system.debug('#### querystring : '+querystring);
                    if(queryString != null){
                        
                        String query = 'select count(Id) Records FROM '+ querystring.split('FROM')[1].split('ORDER BY')[0];
                        List<AggregateResult> queryResult = Database.query(query);
                        wrapper.totalRecords = Integer.valueOf(queryResult[0].get('Records'));
                        
                        String whereClause = querystring.split('WHERE')[1];
                        whereClause = whereClause.split('ORDER BY')[0];
                        
                        list<String> whereClauseList = new list<String>();
                        whereClauseList.addAll(whereClause.split('AND'));
                        whereClause = '';
                        for(String condition : whereClauseList)
                        {
                            if(condition.contains('Position__c')){
                               continue;
                            }
                            if(!condition.contains('Team_Instance__c')){
                               whereClause += condition +' AND ';
                            }  
                        }

                        wrapper.whereClausePACP = whereClause.removeEnd('AND ');
                        wrapper.queryString = queryString;
                        wrapperList.add(wrapper);
                    }
                }
            }
            else
            {
                set<String> restrictedViews = new set<String>();
                isAllPositionsScenario = false;
                if(selectedTeamInstance != '' && selectedTeamInstance != null && selectedTeamInstance.indexOf(SalesIqGlobalConstants.ALL_POSITION_TEXT) == -1){
                    List<Team_Instance__c> teamInsList = SalesIQUtility.getUserAccessTeamInstance('',moduleName, countryId);
                    if(teamInsList.isEmpty()) {
                        AuraHandledException ex = new AuraHandledException(System.Label.No_UAP_Assigned);
                        ex.setMessage(System.Label.No_UAP_Assigned);
                        throw ex;
                    }
                    system.debug('selectedTeamInstance : '+selectedTeamInstance);
                    if(selectedTeamInstance == '' || selectedTeamInstance == null){
                        selectedTeamInstance = teamInsList[0].Id;
                    }

                    system.debug('selectedTeamInstance : '+selectedTeamInstance);
                    system.debug('moduleName : '+moduleName);

                    for(CR_Team_Instance_Config__c summaryCardConfig : [SELECT Id, Configuration_Value__c FROM CR_Team_Instance_Config__c 
                                                                        WHERE Team_Instance__c =: selectedTeamInstance AND Configuration_Name__c =: moduleName 
                                                                        AND Configuration_Type__c = 'Summary Card Configuration' AND Configuration_Value__c != null]){
                        restrictedViews.add(summaryCardConfig.Configuration_Value__c.substring(0, 15));
                    }
                    system.debug('restrictedViews : '+restrictedViews);
                }
                else{
                    String teamId = '';
                    isAllPositionsScenario = true;
                    if(selectedTeamInstance == '' || selectedTeamInstance == null){
                        List<Team_Instance__c> teamInsList = SalesIQUtility.getUserAccessTeamInstance('',moduleName, countryId);
                        if(teamInsList.isEmpty()) {
                            AuraHandledException ex = new AuraHandledException(System.Label.No_UAP_Assigned);
                            ex.setMessage(System.Label.No_UAP_Assigned);
                            throw ex;
                        }

                        teamId = teamInsList[0].Team__c;
                    }
                    else{
                        teamId = selectedTeamInstance.split('_')[1];
                    }

                    teamIdAallPositions = teamId;

                    List<Scenario__c> lstScenario = SalesIQUtility.lastestEmpAssignmnetEnabledScenario(teamId, '');
                    if(lstScenario != null && lstScenario.size() > 0)
                        selectedTeamInstance = lstScenario[0].Team_Instance__c;
                    /*else{
                        AuraHandledException ex = new AuraHandledException(System.Label.No_Latest_Employee_Assignment_Scenario);
                        ex.setMessage(System.Label.No_Latest_Employee_Assignment_Scenario);
                        throw ex;
                    }*/

                    for(CR_Team_Instance_Config__c summaryCardConfig : [SELECT Id, Configuration_Value__c FROM CR_Team_Instance_Config__c 
                                                                        WHERE Team_Instance__c =: selectedTeamInstance AND Configuration_Name__c =: moduleName 
                                                                        AND Configuration_Type__c = 'Summary Card Configuration' AND Configuration_Value__c != null]){
                        restrictedViews.add(summaryCardConfig.Configuration_Value__c.substring(0, 15));
                    }
                }
                
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator('SELECT Id FROM '+ objectName + ' LIMIT 1'));

                List<SelectOption> selectList =  setCon.getListViewOptions();
                for(SelectOption option : selectList)
                {
                    if(!restrictedViews.contains(option.getValue().substring(0, 15))){
                        SalesIQListViewWrapper wrapper = new SalesIQListViewWrapper();
                        wrapper.listViewId = option.getValue();
                        wrapper.listViewName = option.getLabel();
                        if(objectName == ns+'Position__c' || objectName == ns+'Employee__c') // && selectedTeamInstance != '' (commented for SPD-3393)
                        {
                            String queryString = getListViewQuery(objectName,wrapper.listViewId,selectedTeamInstance,selectedPosition, moduleName, countryId);
                            String query = 'select count(Id) Records FROM '+ querystring.split('FROM')[1].split('ORDER BY')[0];
                            System.debug('querystring : '+querystring);
                            // this condition is added to remove usassigned territory from summary card count
                            if(objectName == ns+'Position__c'){
                               query = query + ' AND Client_Position_Code__c !=\'' + SalesIQGlobalConstants.UNASSIGNED_TERRITORY_CODE + '\''; 
                            }
                            System.debug('query : '+query);
                            List<AggregateResult> queryResult = Database.query(query);
                            wrapper.totalRecords = Integer.valueOf(queryResult[0].get('Records'));
                            wrapper.queryString = queryString;

                        }
                        else
                        {
                            setCon.setFilterID(option.getValue());
                            wrapper.totalRecords = setCon.getResultSize();
                            wrapper.queryString = '';
                        }
                        
                        
                        wrapperList.add(wrapper);
                    }
                }

                
            }
            
            return wrapperList;
        } catch(Exception e) {
            String message = e.getTypeName() + ': ' + e.getMessage() + '\n\n' + e.getStackTraceString() ;
            System.debug('message : '+message);
            AuraHandledException ex = new AuraHandledException(message);
            ex.setMessage(message);
            throw ex;
        }
    }

    //Method to fetch query string from describe result of a single list view.
    private static string getListViewQuery(string objectName, string viewId, string selectedTeamInstance,String selectedPosition, string moduleName,String countryId){
        String ns = SalesIQGlobalConstants.Name_space;
        if(sessionId == null)
            login();

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        String urlString = URL.getSalesforceBaseUrl().toExternalForm() + '/services/data/v32.0/sobjects/' + objectName + '/listviews/' + viewId + '/describe';
        request.setEndpoint(urlString);
        request.setMethod('GET');
        request.setHeader('Authorization', 'Bearer ' + sessionId);

        HttpResponse response = http.send(request);
        String jsonString = response.getBody();
        Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
        string querystring = String.valueOf(results.get('query'));
        
        // todo: handling of adding brackets for all three objects to handle "OR" clause coming from list views
        if(objectName == ns+'Position_Account_Call_Plan__c')
        {
            String intermediateQuery = '';
            if(querystring.indexOfIgnoreCase('WHERE')!=-1)
                intermediateQuery = ' AND ' + 'Team_Instance__c =: selectedTeamInstance AND Position__c =: selectedPosition' ;
            else
                intermediateQuery = ' WHERE ' + 'Team_Instance__c =: selectedTeamInstance AND Position__c =: selectedPosition' ;
            querystring = querystring.split('ORDER BY')[0] + intermediateQuery + ' ORDER BY' + querystring.split('ORDER BY')[1];

        }
        else if(objectName == ns+'Employee__c')
        {
            String intermediateQuery = '';
            if(querystring.indexOfIgnoreCase('WHERE')!=-1)
                intermediateQuery = ' AND ' + 'Country_Name__c =: countryId' ;
            else
                intermediateQuery = ' WHERE ' + 'Country_Name__c =: countryId' ;
            querystring = querystring.split('ORDER BY')[0] + intermediateQuery + ' ORDER BY' + querystring.split('ORDER BY')[1];

        }
        else
        {
            String intermediateQuery = '';
            if(!isAllPositionsScenario){
                List<Team_Instance__c> teamInsList = SalesIQUtility.getUserAccessTeamInstance(selectedTeamInstance,moduleName, countryId);
                if(teamInsList.size() < 1)
                {
                    AuraHandledException e = new AuraHandledException(System.Label.No_UAP_Assigned);
                    e.setMessage(System.Label.No_UAP_Assigned);
                    throw e;
                }   
                intermediateQuery = SalesIQUtility.getIntermediateTeamInstanceQuery(teamInsList,null,selectedTeamInstance,null);intermediateQuery = SalesIQUtility.getIntermediateTeamInstanceQuery(teamInsList,null,selectedTeamInstance,null);
            }
            else{
                intermediateQuery = ns+'Team_iD__c = \''+String.escapeSingleQuotes(teamIdAallPositions)+'\' AND ' + ns+'IsMaster__c = true ';    
            }

            if(querystring.indexOfIgnoreCase('WHERE')!=-1)
                intermediateQuery = ' AND ' + intermediateQuery ;
            else
                intermediateQuery = ' WHERE ' + intermediateQuery;
            querystring = querystring.split('ORDER BY')[0] + intermediateQuery + ' ORDER BY' + querystring.split('ORDER BY')[1];
        }

        System.debug('querystring : '+querystring);
        return querystring;
    }

   //Method to check Country Access
    @AuraEnabled
    public static map<string,string> checkCountryAccess(string country){
        map<string,string> response =  new map<string,string>();
        response = SalesIQUtility.checkCountryAccess(country);
        return response;
    }
    

    public class SalesIQListViewWrapper  
    {
        @AuraEnabled
        public String listViewId {get;set;}
        @AuraEnabled
        public String listViewName {get;set;}
        @AuraEnabled
        public Integer totalRecords {get;set;}
        @AuraEnabled
        public String whereClausePACP {get;set;}
        @AuraEnabled
        public String queryString {get;set;}

    }

    @AuraEnabled
    public static void logException(String message, String module) 
    {
        if(message.length() > 2000)
            message.substring(0,2000);
        SalesIQLogger.logErrorMessage(message, module);
    }
}