 public class BRCallBalancingParamsWrapper{
        @AuraEnabled
        public string aggrLevel{get;set;}
        
        @AuraEnabled
        public string aggrScenarioRuleInstance{get;set;}
        
        @AuraEnabled
        public List<Category_Derived_Variable_Metadata__c> lstCatDerivedVariableMetadata{get;set;}
        
        public BRCallBalancingParamsWrapper(string aggrLevel,string aggrScenarioRuleInstance,List<Category_Derived_Variable_Metadata__c> lstCatDerivedVariableMetadata){
            this.aggrLevel=aggrLevel;
            this.aggrScenarioRuleInstance=aggrScenarioRuleInstance;
            this.lstCatDerivedVariableMetadata=lstCatDerivedVariableMetadata;
        }
}