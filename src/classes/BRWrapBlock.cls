public class BRWrapBlock{
    @AuraEnabled
    public string ruleName{get;set;}
    @AuraEnabled
    public List<Integer> priority{get;set;}
    @AuraEnabled
    public Integer selPriority{get;set;}
    @AuraEnabled
    public string thanXpression{get;set;}
    @AuraEnabled
    public Integer seq{get;set;}
    @AuraEnabled
    public Integer criteriaSeq{get;set;} 
    @AuraEnabled
    public string criteriaLogic{get;set;}        
    @AuraEnabled
    public string executionLogic{get;set;}  
    @AuraEnabled
    public string query{get;set;}      
    @AuraEnabled
    public list<WrapGroup> lstWrapGroup{get;set;}
    @AuraEnabled
    public list<BROutputCriteria> lstOutputCriteria{get;set;}
    @AuraEnabled
    public list<ElseCriteria> lstElseCriteria{get;set;}
        
    public BRWrapBlock parse(String json) {
        return (BRWrapBlock) System.JSON.deserialize(json, BRWrapBlock.class);
    }
    
    public class WrapGroup{
        @AuraEnabled
        public string groupName{get;set;}        
        @AuraEnabled
        public string selGroupOp{get;set;}  
        @AuraEnabled
        public list<WrapGroup> lstNestedGroup{get;set;}      
        @AuraEnabled
        public list<BRWrapCriteria> lstwrapCriteria{get;set;} 

        public WrapGroup parse(String json) {
            return (WrapGroup) System.JSON.deserialize(json, WrapGroup.class);
        }
    }

    public class DataSetColumnDetails{
        @AuraEnabled
        public string dsColName {get; set;}  
        @AuraEnabled
        public string dataType {get; set;} 
        @AuraEnabled
        public string tblColName {get; set;} 
        @AuraEnabled
        public string variableType {get; set;} 
        @AuraEnabled
        public string valueType {get; set;}
       
        public  DataSetColumnDetails(string dsColName,string dataType,string tblColName,string variableType){
            this.dsColName=dsColName;
            this.dataType=dataType;
            this.tblColName=tblColName;
            this.variableType=variableType;
        }
    }

    public class selectoption{
        @AuraEnabled
        public string label {get; set;}  
        @AuraEnabled
        public string value {get; set;} 
   
       
        public  selectoption(string label,string value){
            this.label=label;
            this.value=value;
            
        }
    }

    public class ElseCriteria {
        @AuraEnabled
        public String categName{get;set;}        
        @AuraEnabled
        public String dataType {get; set;}        
        @AuraEnabled
        public Boolean isDropdown {get; set;}        
        @AuraEnabled
        public String valueType {get; set;}        
        @AuraEnabled
        public String[] values {get; set;}        
        @AuraEnabled
        public String selValue {get; set;}  
        @AuraEnabled
        public Boolean isNew {get; set;}         
    }

}