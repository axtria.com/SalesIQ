public with sharing class CIMPositionMatrixSummaryUpdateCtlr {
    public list<CIM_Config__c> listCIMconfig{get; set;}
    public list<CIM_Position_Metric_Summary__c> listCIMPosmetric{get;set;}
    public list<string> queryList_CallPlan=new list<string>();
    public string teamInstanceID{get; set;}
    public string theQuery{get; set;}
    public list<CIM_Position_Metric_Summary__c> updateListPostionMetric{get; set;}
    public Id BC {get;set;}
    public Id BCcallPlan {get;set;}
    public string scenarioId;
    public string scType;
    public list<Scenario__c> sc=new list<Scenario__c>();
    public List<string> scenarioType=new List<string>();
    public string teamType;
    public list<Geography_Type__c> geoType=new list<Geography_Type__c>();
    public string geoTypeId;
    public Boolean shapeAvailable;
    public Boolean accountShapeAvailable;
    public Boolean syncEsri = false;
	
    
    public CIMPositionMatrixSummaryUpdateCtlr(string Team_Instance_Id,string scenarioIdx){     
        theQuery = '';
        teamInstanceID=Team_Instance_Id;
        scenarioId=scenarioIdx;
        scType=[select Scenario_Type__c from Scenario__c where id =:scenarioIdx].Scenario_Type__c;
        teamType=[select Alignment_Type__c from Team_Instance__c where id=:Team_Instance_Id].Alignment_Type__c;
        geoTypeId=[select Geography_Type_Name__c from Team_Instance__c where id=:Team_Instance_Id].Geography_Type_Name__c;
        geoType=[select Account_Shape_Available__c,Shape_Available__c from Geography_Type__c where id=:geoTypeId];
        for(Geography_Type__c gType:geoType){
            accountShapeAvailable=gType.Account_Shape_Available__c;
            shapeAvailable=gType.Shape_Available__c;
        }
        if(scType=='Alignment Scenario')
        {
            scenarioType.add('ZIP Movement');
            scenarioType.add('Account Movement');
            populateExtents();
        }
        else
            scenarioType.add('Call_Plan_Change');
        system.debug('Team_Instance id ='+teamInstanceID);
        system.debug('Scenario id ='+scenarioIdx);
        system.debug('Scenariotype='+scenarioType);
      
        deleteCIMPosMetricSummaryRecord();
        updatePostionMatrxiSummary();
    }

    public CIMPositionMatrixSummaryUpdateCtlr(string Team_Instance_Id,string scenarioIdx,Boolean syncEsriVar){     
        theQuery = '';
        teamInstanceID=Team_Instance_Id;
        scenarioId=scenarioIdx;
        syncEsri = syncEsriVar;
        scType=[select Scenario_Type__c from Scenario__c where id =:scenarioIdx].Scenario_Type__c;
        teamType=[select Alignment_Type__c from Team_Instance__c where id=:Team_Instance_Id].Alignment_Type__c;
        geoTypeId=[select Geography_Type_Name__c from Team_Instance__c where id=:Team_Instance_Id].Geography_Type_Name__c;
        geoType=[select Account_Shape_Available__c,Shape_Available__c from Geography_Type__c where id=:geoTypeId];
        for(Geography_Type__c gType:geoType){
            accountShapeAvailable=gType.Account_Shape_Available__c;
            shapeAvailable=gType.Shape_Available__c;
        }
        if(scType=='Alignment Scenario')
        {
            scenarioType.add('ZIP Movement');
            scenarioType.add('Account Movement');
            populateExtents();
        }
        else
            scenarioType.add('Call_Plan_Change');
        system.debug('Team_Instance id ='+teamInstanceID);
        system.debug('Scenario id ='+scenarioIdx);
        system.debug('Scenariotype='+scenarioType);
      
        deleteCIMPosMetricSummaryRecord();
        updatePostionMatrxiSummary();
    }

    public void deleteCIMPosMetricSummaryRecord(){
        listCIMPosmetric = [select id from CIM_Position_Metric_Summary__c where Team_Instance__c =:teamInstanceID and CIM_Config__r.Change_Request_Type__r.CR_Type_Name__c IN :scenarioType];
        System.debug('IN DELETE CIM');
        System.debug(listCIMPosmetric);
        delete listCIMPosmetric;
    }
    public void updatePostionMatrxiSummary(){
        
        
        displayQuery();
    }
    
    public void populateExtents(){
        RollupSummaryUtility m;
        if(teamType=='ZIP'||teamType=='Hybrid')
        {
                
            if(shapeAvailable)  
                m = new RollupSummaryUtility('Position_Geography__c',teamInstanceID);
            else if(!shapeAvailable&&accountShapeAvailable){
                m = new RollupSummaryUtility('Position_Account__c',teamInstanceID);
            }
        }
        else
        {
            if(accountShapeAvailable)
            m = new RollupSummaryUtility('Position_Account__c',teamInstanceID);
        }
        if(m!=null)
        Database.executeBatch(m);
    }
    

    
    public void displayQuery(){
        updateListPostionMetric= new list<CIM_Position_Metric_Summary__c>();
        listCIMconfig = [select id, name, Object_Name__c, Attribute_API_Name__c, Aggregation_Type__c, Aggregation_Object_Name__c, Aggregation_Attribute_API_Name__c, Aggregation_Condition_Attribute_API_Name__c, Aggregation_Condition_Attribute_Value__c,Attribute_Display_Name__c from CIM_Config__c where Team_Instance__c =:teamInstanceID and Enable__c=true and Change_Request_Type__r.CR_Type_Name__c IN :scenarioType and Is_Custom_Metric__c=false and Hierarchy_Level__c='1'];
            system.debug('List of Cim Config'+listCIMconfig);
            Map<String,String> cimId=new Map<String,String>();
            Map<String,String> cimIdCallPlan=new Map<String,String>();
            string aggregationType_objectName;
            string theQueryFirst;
            string theQueryMid;
            string aggregationAttributeAPIName;
            string aggregationObjectName;
            string aggregationConditionAttributeAPIName;
            string theQuerySecond;
            string attributeDisplayName;
        
            for(CIM_Config__c obj : listCIMconfig){
            
             attributeDisplayName=obj.Attribute_Display_Name__c;
             
             aggregationAttributeAPIName = obj.Aggregation_Attribute_API_Name__c;
             aggregationObjectName = obj.Aggregation_Object_Name__c;
             aggregationConditionAttributeAPIName = obj.Aggregation_Condition_Attribute_API_Name__c;
             
             string aggregationType = obj.Aggregation_Type__c;           
             string objectName = obj.Object_Name__c;
             string attributeAPIname = obj.Attribute_API_Name__c;
            
                        //string aggregationConditionAttributeValue = obj.Aggregation_Condition_Attribute_Value__c;
            
             string objectToBeQueried;
                    if(aggregationAttributeAPIName!= null && aggregationObjectName!=null ){
                             system.debug('the Query List Call Plan hiii '+ theQuery);  
                                if(objectName != aggregationObjectName){
                                            system.debug('the Query List Call Plan hiii inside 2nd loop '+ aggregationConditionAttributeAPIName); 
                                            objectToBeQueried =  objectName.removeEnd('c');
                                            objectToBeQueried = objectToBeQueried + 'r.'+attributeAPIname;
                                            if(aggregationType_objectName!=NULL)
                                            aggregationType_objectName+=obj.Aggregation_Type__c+'('+objectToBeQueried+')'+Obj.id+',';
                                            else
                                            aggregationType_objectName=obj.Aggregation_Type__c+'('+objectToBeQueried+')'+Obj.id+',';
                                            
                                            system.debug('the aggregationType_objectName = \n'+ aggregationType_objectName);
                                            if(aggregationConditionAttributeAPIName != null||scenarioType[0]=='Call_Plan_Change'){
                                            theQuery = 'select '+aggregationType+'(' + objectToBeQueried + ')'+Obj.id+', ' + aggregationAttributeAPIName + ' aggregationAtrributeAPIName from ' +aggregationObjectName + ' where ' + aggregationConditionAttributeAPIName +' and';
                                            }
                                            else
                                            theQuery= 'select '+aggregationType+'(' + objectToBeQueried + ')'+Obj.id+', ' + aggregationAttributeAPIName + ' aggregationAtrributeAPIName from ' +aggregationObjectName +' where';
                                            system.debug('the Query List Call Plan inside Loop '+ theQuery);
                                               
                        }
                    else    
                        {
                                            objectToBeQueried = attributeAPIname;
                                            if(aggregationType_objectName!=NULL)
                                            aggregationType_objectName+=obj.Aggregation_Type__c+'('+objectToBeQueried+')'+Obj.id+',';
                                            else
                                            aggregationType_objectName=obj.Aggregation_Type__c+'('+objectToBeQueried+')'+Obj.id+',';
                                            system.debug('the aggregationType_objectName = \n'+ aggregationType_objectName);
                                            if(aggregationConditionAttributeAPIName != null ||scenarioType[0]=='Call_Plan_Change'){
                                                if(aggregationConditionAttributeAPIName!=null)
                                                theQuery = 'select '+aggregationType+'(' + objectToBeQueried + ')'+Obj.id+', ' + aggregationAttributeAPIName + ' aggregationAtrributeAPIName from ' +aggregationObjectName + ' where ' + aggregationConditionAttributeAPIName+' and';
                                                else
                                                theQuery= 'select '+aggregationType+'(' + objectToBeQueried + ')'+Obj.id+', ' + aggregationAttributeAPIName + ' aggregationAtrributeAPIName from ' +aggregationObjectName+' where';
                                            system.debug('the Query List Call Plan '+ theQuery);
                                            }
                
                            system.debug('the Query List Call Plan '+ theQuery);
                        }
                        if(aggregationConditionAttributeAPIName != null||scenarioType[0]=='Call_Plan_Change'){
                        queryList_CallPlan.add(theQuery);
                            cimIdCallPlan.put(obj.id,obj.id);
                        }
                        else
                        cimId.put(obj.id,obj.id);
                    
                    }
                }
                if(aggregationConditionAttributeAPIName == null && scenarioType[0]!='Call_Plan_Change'){
                                theQueryFirst = 'select '+aggregationType_objectName+' ' +aggregationAttributeAPIName + ' aggregationAtrributeAPIName from ';
                                theQueryFirst += aggregationObjectName;
                    
                    }
            
                    
                    theQuerySecond=' group by '+  aggregationAttributeAPIName;
                    system.debug('the Query Second \n'+ theQuerySecond);
                    system.debug('the Query First '+ theQueryFirst);
                    system.debug('the Query List Call Plan '+ queryList_CallPlan);
                system.debug('Team_Instance_Check_Controller ='+teamInstanceID);
                if(theQueryFirst!=null)
                    BC=Database.executeBatch(new CIMPopulateBatch(teamInstanceID,theQueryFirst,theQuerySecond,cimId,scenarioId,syncEsri),1);
                    system.debug('Before Call Plan Batch Call= '+ queryList_CallPlan.size());
                if(queryList_CallPlan.size()>0)
                {
                    BCcallPlan=Database.executeBatch(new CIMPopulateCallPlanBatch(teamInstanceID,queryList_CallPlan,theQuerySecond,cimIdCallPlan,scenarioId),1);
                }
               else
                {
                    system.debug('In Call Plan Batch Call Scenario Id= '+ scenarioId);
                    sc=[select id,Request_Process_Stage__c from Scenario__c where id=:scenarioId];
                  for(Scenario__c scf:sc)
                  {
                      scf.Request_Process_Stage__c='Ready';
                  }
                  update sc;
                }
                
               
        
    }
}