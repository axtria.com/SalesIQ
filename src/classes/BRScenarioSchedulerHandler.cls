public class BRScenarioSchedulerHandler{

    public  static void syncMasterData(){
        List<Data_Set__c> dataSetList = [SELECT Data_Set_Object_Name__c,destination__c,is_internal__c,Is_Master__c,SalesIQ_Internal__c, (select Id from Data_Objects__r LIMIT 1) FROM Data_Set__c WHERE Is_Master__c = true AND is_internal__c = false AND SalesIQ_Internal__c = true AND Data_Set_Object_Name__c != null];

        List<String> dataObjectIds = new List<String>();
        for(Data_Set__c dataSet : dataSetList){
            if(!dataSet.Data_Objects__r.isEmpty())
                dataObjectIds.add(dataSet.Data_Objects__r[0].Id);
        }
        
        String dataObjectIdsString = String.join(dataObjectIds, ',');
        system.debug('DO Ids: ' + dataObjectIdsString);
        
        List<Data_Object__c> dataObjectList=[Select id from Data_Object__c WHERE id in :dataObjectIds];
        for(Data_Object__c dataObject:dataObjectList){
            dataObject.status__c=SalesIQGlobalConstants.QUEUED;
            dataObject.Current_Sync_Date__c=System.now();
        }
        update dataObjectList;
        
        Id recordId = Schema.SObjectType.SalesIQ_Logger__c.getRecordTypeInfosByName().get(CPGConstants.QUEUE_LOGGER_RECORDTYPE).getRecordTypeId();
        SalesIQ_Logger__c masterSynclog = new SalesIQ_Logger__c(Module__c=SalesIQLogger.BRMS_MODULE, Status__c=SalesIQGlobalConstants.IN_PROGRESS, Type__c= CPGConstants.QUEUE_LOGGER_TYPE_MASTER);
        masterSynclog.RecordtypeId= recordId;
        insert masterSynclog;

        callpythonService(masterSyncLog.id,dataObjectIdsString);
    }

    @future(callout=true)
    public static void  callpythonService(String masterSynclogId,String dataObjectIdsString){

        string pythonService = CPGUtility.getBRMSConfigValues('BRDataSync');
        //SSL CHANGES
        Map<String,String> urlParams = new Map<String,String>();
        urlParams.put('serviceUrl',pythonService);

        ETL_Config__c etlObj = CPGUtility.getETLConfigByName(CPGConstants.talend_Instance);
        String sfUserName = etlObj.SF_UserName__c;
        String sfPassword = etlObj.SF_Password__c;
        String sfToken = etlObj.S3_Security_Token__c;
        
        try{
            ///String url = pythonService+'?user_name='+sfUserName+'&password='+sfPassword+'&security_token='+sfToken+'&scenario_id=&dataObjectIds='+dataObjectIdsString+'&namespace='+SalesIQGlobalConstants.NAME_SPACE+'&sandbox='+SalesIQUtility.isSandboxOrg()+'&logger_id='+MasterSynclogId+'&logger_object_name='+SalesIQGlobalConstants.getOrgNameSpace()+'SalesIQ_Logger__c';
            //system.debug('data sync url --  ' + url);
            String isSandbox = ( SalesIQUtility.isSandboxOrg() ) ? 'true' : 'false';
            urlParams.put('user_name',sfUserName);
            urlParams.put('password',sfPassword);
            urlParams.put('security_token',sfToken);
            urlParams.put('scenario_id','');
            urlParams.put('dataObjectIds',dataObjectIdsString);
            urlParams.put('namespace',SalesIQGlobalConstants.NAME_SPACE);
            urlParams.put('sandbox',isSandbox);
            urlParams.put('logger_id', masterSynclogId);
            urlParams.put('logger_object_name', SalesIQGlobalConstants.getOrgNameSpace()+'SalesIQ_Logger__c');


            HttpResponse response = SalesiqUtility.getHTTPResponse(urlParams.get('serviceUrl'),urlParams,'POST',120000);

            system.debug('#### Python Response : '+response);
            if(response.getStatusCode() != 200) {
                SalesIQ_Logger__c updateLoggerStatus = new SalesIQ_Logger__c(Id = masterSynclogId);
                updateLoggerStatus.Status__c = SalesIQGlobalConstants.ERROR;
                updateLoggerStatus.Stack_Trace__c = response.getStatus();
                update updateLoggerStatus;
            }
        }
        catch(exception ex){
            system.debug('Error in master sync : '+ex.getMessage());
        }
    }

    @future(callout=true)
    public static void pythonAffliation(String finalTable,String networkfield,String parentfield,String accountfield,String accountNetworkDepthLogId){
       string pythonService = CPGUtility.getBRMSConfigValues('BRAccountAffiliation');
       
       ETL_Config__c etlObj = CPGUtility.getETLConfigByName(CPGConstants.talend_Instance);
       String pgUserName = etlObj.BR_PG_UserName__c;
       String pgPassword = etlObj.BR_PG_Password__c;  
       String dbname = etlObj.BR_PG_Database__c;

        try{ 
            String url = pythonService+'?hierarchy_table='+finalTable+'&network_field='+networkfield+'&parent_field='+parentfield+'&account_field='+accountfield+'&dbname='+dbname+'&pg_username='+pgUserName+'&pg_password='+pgPassword;
            system.debug('data sync url --  ' + url);
            Http h = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndPoint(url);
            request.setHeader('Content-type', 'application/json');
            request.setMethod('GET');
            request.setTimeout(120000);
            system.debug('request '+request);
            HttpResponse response = h.send(request);
            system.debug('#### Python Response : '+response.getBody());
         

            List<Object> serializedJson = (List<Object>)JSON.deserializeUntyped(response.getBody());

            Map<String, String> affiliationNwLevelMap = new Map<String, String>();
            for(Object levelJson : serializedJson) {
                Map<String, Object> levelMap = (Map<String, Object>)levelJson ;
                String sfdcId, hierarchylevel;
                
                for(String key : levelMap.keySet()) {
                    if(key == 'Id')
                        sfdcId = (String) levelMap.get(key);
                    else 
                        hierarchylevel = String.valueOf((Integer) levelMap.get(key));
                }
                affiliationNwLevelMap.put(sfdcId, hierarchylevel);
            }

            system.debug(affiliationNwLevelMap);

            List<SalesIQ_Logger__c> salesIQLog=[Select id,Status__c FROM SalesIQ_Logger__c WHERE id=:accountNetworkDepthLogId];
            Map<Id, Affiliation_Network__c> affiliationNetworkMap = new Map<Id, Affiliation_Network__c>([Select id,Hierarchy_Level__c FROM Affiliation_Network__c WHERE id in :affiliationNwLevelMap.keySet()]);
         
            if(salesIQLog.isEmpty())
                return ;
            
            List<Affiliation_Network__c> toBeUpdatedAffNw = new List<Affiliation_Network__c>();

            for(String nwId : affiliationNwLevelMap.keySet()) {
                if(affiliationNetworkMap.containsKey(nwId)) {
                    Affiliation_Network__c affNw = affiliationNetworkMap.get(nwId);
                    if(affNw.Hierarchy_Level__c != affiliationNwLevelMap.get(nwId)) {
                        affNw.Hierarchy_Level__c = affiliationNwLevelMap.get(nwId);
                        toBeUpdatedAffNw.add(affNw);
                    }
                }
            }

            if(!affiliationNetworkMap.isEmpty()){
                salesIQLog[0].Status__c=SalesIQGlobalConstants.SUCCESS;
            }else{
                salesIQLog[0].Status__c=SalesIQGlobalConstants.ERROR;
                salesIQLog[0].Stack_Trace__c = 'No affiliation network found';
            }

            update toBeUpdatedAffNw;
            update salesIQLog; 
        } catch(exception ex) {
            system.debug('Error in Affiliation Depth Calcualtion : '+ex.getMessage());
            List<SalesIQ_Logger__c> salesIQLog=[Select id,Status__c FROM SalesIQ_Logger__c WHERE id=:accountNetworkDepthLogId];
            if(!salesIQLog.isEmpty()) {
                salesIQLog[0].Status__c = SalesIQGlobalConstants.ERROR;
                salesIQLog[0].Stack_Trace__c = ex.getMessage();
                update salesIQLog[0];
            }
        }
    }

    public static void queueExclusionScenarioForDeltaRun(){
        List<Scenario_Execution_Setting__c> executionList=[SELECT Additional_Where_Clause__c,Name,Record_Id__c,Scenario_Where_Clause__c,Schedule_Run_Enable__c,Setting_Level__c,Type__c FROM Scenario_Execution_Setting__c WHERE Type__c =:SalesIQGlobalConstants.SCENARIO_RECORDTYPE_BLOCK and Schedule_Run_Enable__c = true];
        
        if(executionList.size()>0){
            Map<Id, Scenario__c> tobeUpdatedScenarios = new Map<Id, Scenario__c>();
            for(Scenario_Execution_Setting__c scenarioExec:executionList){
                
                String query='Select Last_Promote_Success_Date__c, Promote_Mode__c,Rule_Execution_Status__c,Country__c FROM Scenario__c WHERE RecordType.DeveloperName = \''+SalesIQGlobalConstants.SCENARIO_RECORDTYPE_BLOCK+'\' and Country__c =\''+scenarioExec.Record_Id__c +'\'';
             
                if(scenarioExec.Scenario_Where_Clause__c!=null)
                    query+=' and '+scenarioExec.Scenario_Where_Clause__c;
            
                if(scenarioExec.Additional_Where_Clause__c!=null)
                    query+=' and '+scenarioExec.Additional_Where_Clause__c;
                
                system.debug('Excluusion Scenario Query--> ' + query);
                List<Scenario__c> scenarioList=Database.query(query);
                tobeUpdatedScenarios.putAll(queueSceanrios(scenarioList));
            }

            if(!tobeUpdatedScenarios.isEmpty()) {
                system.debug('Queued Exclusion scenarios count:: ' + tobeUpdatedScenarios.size());
                update tobeUpdatedScenarios.values();
            }
        }
    }

    public static void queueScenariosForDeltaRun(){
        List<Scenario_Execution_Setting__c> executionList=[SELECT Additional_Where_Clause__c,Name,Record_Id__c,Scenario_Where_Clause__c,Schedule_Run_Enable__c,Setting_Level__c,Enable_Org_Level_Execution__c,Type__c FROM Scenario_Execution_Setting__c WHERE Type__c =: SalesIQGlobalConstants.SCENARIO_RECORDTYPE_ALIGNMENT and Schedule_Run_Enable__c = true];
        
        if(executionList.size()>0){
            Map<Id, Scenario__c> tobeUpdatedScenarios = new Map<Id, Scenario__c>();
            for(Scenario_Execution_Setting__c scenarioExec:executionList){
                String query='Select Last_Promote_Success_Date__c,Promote_Mode__c,Rule_Execution_Status__c,Country__c, RecordType.DeveloperName, Team_Instance__r.Alignment_Period__c  FROM Scenario__c WHERE RecordType.DeveloperName = \'' + SalesIQGlobalConstants.SCENARIO_RECORDTYPE_ALIGNMENT +'\'';

                if(!scenarioExec.Enable_Org_Level_Execution__c){
                    query+= ' and Team_Name__c =\''+scenarioExec.Record_Id__c +'\'';
                }

                if(scenarioExec.Scenario_Where_Clause__c!=null){
                    query+=' and '+scenarioExec.Scenario_Where_Clause__c;
                }
            
                if(scenarioExec.Additional_Where_Clause__c!=null){
                    query+=' and '+scenarioExec.Additional_Where_Clause__c;
                }

                system.debug('Alig Scenario Query--> ' + query);
                List<Scenario__c> scenarioList=Database.query(query);
                tobeUpdatedScenarios.putAll(queueSceanrios(scenarioList));
            }

            Map<Id,Activity_Log__c> scenarioIdToQueueMap = new Map<Id,Activity_Log__c>();
            scenarioIdToQueueMap = SalesIQUtility.insertActivityLog(true,tobeUpdatedScenarios);
            /*if(!tobeUpdatedScenarios.isEmpty()) {
                system.debug('Queued Alignment scenarios count:: ' + tobeUpdatedScenarios.size());
                update tobeUpdatedScenarios.values();
            }*/

        }
    }

    private static Map<Id, Scenario__c> queueSceanrios(List<Scenario__c> scenarioList) {
        Map<Id, Scenario__c> scenarioMap = new Map<Id, Scenario__c>();
        for(Scenario__c scenario:scenarioList){
            //if(scenario.Rule_Execution_Status__c != SalesIQGlobalConstants.IN_PROGRESS) {

                if(!(scenario.RecordType.DeveloperName == SalesIQGlobalConstants.SCENARIO_RECORDTYPE_ALIGNMENT && scenario.Team_Instance__r.Alignment_Period__c == SalesIQGlobalConstants.PAST_TEAM_CYCLE_TYPE)) {
                    //scenario.Rule_Execution_Status__c=SalesIQGlobalConstants.QUEUED;
                    /*if(scenario.Last_Promote_Success_Date__c != null)
                        scenario.Promote_Mode__c=SalesIQGlobalConstants.TEXT_RUN_DELTA_PROMOTE;
                    else
                        scenario.Promote_Mode__c=SalesIQGlobalConstants.TEXT_RUN_FULL_PROMOTE;*/

                    scenarioMap.put(scenario.Id, scenario);
                }
            //}
        }

        system.debug('Return Queued Map size:: ' + scenarioMap.size());
        return scenarioMap;
    }
}