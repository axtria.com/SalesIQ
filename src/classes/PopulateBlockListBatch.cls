global class PopulateBlockListBatch implements Database.Batchable<sObject>{
/* BATCH CLASS DEPRECATED RELEASE 5.6*/
global final String query;global PopulateBlockListBatch(){}global Database.QueryLocator start(Database.BatchableContext BC){return Database.getQueryLocator(query);}global void execute(Database.BatchableContext BC, List<Country__c> countryList){}global void finish(Database.BatchableContext BC){}   
}