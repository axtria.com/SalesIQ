@isTest
public class TestDataFactory {
    
	public static Team__c createTeam(String teamType){
        Team__c team = new Team__c();
        team.Name='HTN';
        team.Type__c = teamType; 
        
        return team;
    } 

    public static User createUser(String profileId){
            // This code runs as the system user
             
            User tUser = new User(Alias = 'Rep', Email='repuser@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = profileId, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='repuserQuset@testorg.com');
            return tUser;
    }

    public static Workspace__c createWorkspace(String workspaceName, Date startDate, Date endDate) {
        Workspace__c workspace = new Workspace__c();
        workspace.Name = workspaceName;
        workspace.Workspace_Description__c = 'Some Description';
        workspace.Workspace_Start_Date__c = startDate;
        workspace.Workspace_End_Date__c = endDate;

        return workspace ;
    }

    public static Scenario__c createScenario(Id workspaceId, Id scenarioTeamInstanceId, Id sourceTeamInstanceId, String stage, String processStage, String status, Date startDate, Date endDate) {
        Scenario__c scenario = new Scenario__c();
        if(scenarioTeamInstanceId != null)
            scenario.Team_Instance__c = scenarioTeamInstanceId;
        if(sourceTeamInstanceId != null)
            scenario.Source_Team_Instance__c = sourceTeamInstanceId;
        scenario.Effective_Start_Date__c = startDate;
        scenario.Effective_End_Date__c = endDate;
        scenario.Scenario_Stage__c = stage;
        scenario.Request_Process_Stage__c = processStage;
        scenario.Scenario_Status__c = status;
        scenario.Workspace__c = workspaceId;

        return scenario ;
    }

    public static Position__c createPosition(Team__c team, String positionName, Date startDate, Date endDate, String positionType, String hierarchyLevel, String parentPositionId){
        
        Position__c pos= new Position__c();
        pos.name = positionName;
        pos.Client_Territory_Name__c = positionName;
        pos.Parent_Position__c = parentPositionId;
        pos.Client_Position_Code__c = '1NE30002';
        pos.Client_Territory_Code__c='1NE30002';
        pos.Position_Type__c=positionType;
       	pos.Related_Position_Type__c=team.Type__c;
        pos.Inactive__c = false;
        pos.Effective_Start_Date__c = startDate;
        pos.Effective_End_Date__c = endDate;
        pos.RGB__c = '41,210,117';
        pos.Team_iD__c = team.Id;
        pos.X_Max__c=-72.6966429900;
        pos.X_Min__c=-73.9625820000;
        pos.Y_Max__c=40.9666490000;
        pos.Y_Min__c=40.5821279800;
        pos.Hierarchy_Level__c = hierarchyLevel;
        
        return pos;
    }

    public Static Team_Instance__c createTeamInstance(String teamId,String type,String scenarioId,Date startDate, Date endDate){
        Team_Instance__c objTeamInstance = new Team_Instance__c();
        objTeamInstance.name = 'HTN_Q1_2016';
        if(scenarioId != null && scenarioId != '')
        {
            objTeamInstance.Scenario__c = scenarioId;
            objTeamInstance.Scenario__r.Employee_Assignment__c = true;
        }
       
        objTeamInstance.Alignment_Period__c = 'Current';
        objTeamInstance.Team__c = teamId;
        objTeamInstance.IC_EffstartDate__c = startDate;
        objTeamInstance.IC_EffEndDate__c = endDate;
        objTeamInstance.Alignment_Type__c = type;
        objTeamInstance.isActiveCycle__c = 'Y';
        return objTeamInstance;
    }

    public static Position_Team_Instance__c createPositionTeamInstance(Id positionId, Id parentPositionId, Id teamInstanceId){
        Position_Team_Instance__c positionTeamInstance = new Position_Team_Instance__c();
        positionTeamInstance.Position_ID__c = positionId;
        if(parentPositionId!=null)
            positionTeamInstance.Parent_Position_ID__c = parentPositionId;
        positionTeamInstance.Effective_End_Date__c  = Date.newInstance(2018,1,1);
        positionTeamInstance.Effective_Start_Date__c  = Date.newInstance(2015,1,1);
        positionTeamInstance.Team_Instance_ID__c = teamInstanceId;
        positionTeamInstance.X_Max__c=-72.6966429900;
        positionTeamInstance.X_Min__c=-73.9625820000;
        positionTeamInstance.Y_Max__c=40.9666490000;
        positionTeamInstance.Y_Min__c=40.5821279800;     
        
        return positionTeamInstance;
    }

    public static User_Access_Permission__c createUserAccessPerm(String posId, String userObjId, String teamIntanceId )
    {
        User_Access_Permission__c objUserAccessPerm = new User_Access_Permission__c();
        objUserAccessPerm.Position__c = posId;
        objUserAccessPerm.name='access';
        objUserAccessPerm.Team_Instance__c =teamIntanceId;   
        if(userObjId != null){
            objUserAccessPerm.User__c = userObjId;
        }
        objUserAccessPerm.Is_Active__c = true;
        
        return objUserAccessPerm;
    }

    public static Employee__c createEmployee(String firstName, String lastName){
        Employee__c employee = new Employee__c();
        employee.Name = firstName + ' ' + lastName ;
        employee.Employee_ID__c = '12345';
        employee.Last_Name__c = lastName;
        employee.FirstName__c = firstName;
        employee.Gender__c = 'M';
        return employee;
    }

    public static Position_Employee__c createPositionEmployee(String positionId, String employeeId, String assignmentType, Date startDate, Date endDate){
        Position_Employee__c positionEmployee = new Position_Employee__c();       
        positionEmployee.Assignment_Type__c = assignmentType;
        positionEmployee.Position__c = positionId;
        positionEmployee.Employee__c = employeeId;        
        positionEmployee.Status__c ='Approved';
        positionEmployee.Effective_start_date__c = startDate;
        positionEmployee.Effective_End_Date__c = endDate;
        
        return positionEmployee;
    }

    public static Change_Request__c createChangeRequest(String userId, Id sourcePositionId, Id destinationPositionId, Id teamInstanceId, String status){
        Change_Request__c objChangeRequest = new Change_Request__c();
        objChangeRequest.Approver1__c = userId;
        objChangeRequest.Destination_Position__c = destinationPositionId;
        
        if(sourcePositionId != null)
            objChangeRequest.Source_Position__c = sourcePositionId;
   
        objChangeRequest.Change_String_Value__c = '11788';
        objChangeRequest.Status__c = status;
        objChangeRequest.OwnerID = userId;
        objChangeRequest.Team_Instance_ID__c = teamInstanceId;
        objChangeRequest.scenario_name__c='Scenario';
        objChangeRequest.Execution_Date__c = date.today();
        
        return objChangeRequest;
    }

    @future
    public static void assignPermissionSet(Id userId, String permissionSetName) {
        PermissionSet ps = [SELECT Id, Name From PermissionSet WHERE Name = :permissionSetName];
        
        List<PermissionSetAssignment> existingPS = [SELECT Id from PermissionSetAssignment WHERE AssigneeId =: userId and PermissionSetId =: ps.Id];
        if(existingPS.isEmpty())
            insert new PermissionSetAssignment(AssigneeId = userId, PermissionSetId = ps.Id );
    }

    @future
    public static void createUserAccessPermissionFuture(String posId, String userObjId, String teamIntanceId,String permissionSetName)
    {
        PermissionSet ps = [SELECT Id, Name From PermissionSet WHERE Name = :permissionSetName];

        List<PermissionSetAssignment> existingPS = [SELECT Id from PermissionSetAssignment WHERE AssigneeId =: userObjId and PermissionSetId =: ps.Id];
        if(existingPS.isEmpty())
            insert new PermissionSetAssignment(AssigneeId = userObjId, PermissionSetId = ps.Id );
        
        User_Access_Permission__c objUserAccessPerm = createUserAccessPerm(posId,userObjId,teamIntanceId);
        insert objUserAccessPerm;
    }

}