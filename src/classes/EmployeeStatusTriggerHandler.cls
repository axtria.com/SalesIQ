public with sharing class EmployeeStatusTriggerHandler{

    public static boolean runtrigger = true;
    
    
    public static EmployeeStatusResponse UpdateEndDate(list<Employee_Status__c> lstEmployeeStatus){
        string ErrorMessage='';
        EmployeeStatusResponse objTesponse = new EmployeeStatusResponse(ErrorMessage);
        for(Employee_Status__c emp: lstEmployeeStatus ){
            if(emp.Effective_Start_Date__c!=null && emp.Effective_End_Date__c!=null){
                if(emp.Effective_Start_Date__c> emp.Effective_End_Date__c){
                    ErrorMessage = system.label.Start_Date;
                    objTesponse= new EmployeeStatusResponse(ErrorMessage);
                }else{
                    Map<string, Employee_Status__c > EmpId = getEmployeeMap(lstEmployeeStatus); 
                    list<Employee_Status__c> lstEmployee = RosterUtility.getEmployeeStatusByEmpID(EmpId.keyset(),new list<string>{SalesIQGlobalConstants.EMPLOYEE_STATUS_ACTIVE});//[select id,Employee__c,Effective_Start_Date__c,Effective_End_Date__c from Employee_Status__c where Employee__c in:EmpId.keyset() and Employee_Status__c='Active'];
                    if(lstEmployee!=null && lstEmployee.size() > 0){
                        for(Employee_Status__c EmpStatus: lstEmployee){
                        	EmpStatus.Effective_End_Date__c = EmpId.get(EmpStatus.Employee__c).Effective_Start_Date__c-1;
                        }           
                        update lstEmployee;
                        //InsertHighEndRecord(lstEmployeeStatus);
                    }
                }
            }
        }
        return objTesponse;        
    }
    
    public static Map<string, list<Employee_Status__c> > getStatusMapwithEmployeeStatus(list<Employee_Status__c> lstEmployeeStatus){
        Map<string, list<Employee_Status__c> > EmpStatusMap = new Map<string, list<Employee_Status__c> >();
        for(Employee_Status__c empStatus: lstEmployeeStatus){
            if(EmpStatusMap.containskey(empStatus.Employee_Status__c)){
                list<Employee_Status__c> LocalEmpStatus = EmpStatusMap.get(empStatus.Employee_Status__c);
                LocalEmpStatus.add(empStatus);
                EmpStatusMap.put(empStatus.Employee_Status__c, LocalEmpStatus);
            }else{
                EmpStatusMap.put(empStatus.Employee_Status__c, new list<Employee_Status__c>{empStatus});
            }
            
        }
        system.debug('EmpStatusMap '+EmpStatusMap);
        system.debug('EmpStatusMap '+EmpStatusMap.size());
        return EmpStatusMap;
    }
    
    public static void InsertHighEndRecord(list<Employee_Status__c> lstEmployeeStatus){
        
        list<Employee_Status__c> lstHighEndDateRecord = new list<Employee_Status__c>();
        for(Employee_Status__c empStatus:lstEmployeeStatus ){
            
            Employee_Status__c objEmpStatus = new Employee_Status__c();
            objEmpStatus.Employee__c = empStatus.Employee__c;
            objEmpStatus.Employee_Status__c = SalesIQGlobalConstants.EMPLOYEE_STATUS_ACTIVE;
            objEmpStatus.Effective_Start_Date__c = empStatus.Effective_End_Date__c+1;
            objEmpStatus.Effective_End_Date__c = Date.newInstance(4000,12,31);
            lstHighEndDateRecord.add(objEmpStatus);
        }
        
        insert lstHighEndDateRecord;
    }
    
    public static EmployeeStatusResponse CheckOverlappingRecord(list<Employee_Status__c> lstEmployeeStatus){
        
        string ErrorMessage='';
       
        system.debug('newly lstEmployee '+lstEmployeeStatus);
        //This is newly created Record, which is coming when user is inserting record from UI
        Map<string, Employee_Status__c > MapNewEmpStatusRecord = getEmployeeMap(lstEmployeeStatus);
        
        //Getting old record with active status
        //These are the record in which i have to check Overlapping
        list<Employee_Status__c> lstEmployee = RosterUtility.getEmployeeStatusByEmpID(MapNewEmpStatusRecord.keyset(),new list<string>{SalesIQGlobalConstants.EMPLOYEE_STATUS_INACTIVE});
        system.debug('CheckOverlappingRecord lstEmployee '+lstEmployee);
        if(lstEmployee!=null && lstEmployee.size() > 0){
            for(Employee_Status__c empStatus:lstEmployee ){
                Employee_Status__c NewEmpStatusRecord = MapNewEmpStatusRecord.get(empStatus.Employee__c);
                system.debug('NewEmpStatusRecord '+NewEmpStatusRecord);
                if(empStatus.Effective_Start_Date__c<=NewEmpStatusRecord.Effective_Start_Date__c || empStatus.Effective_End_Date__c<=NewEmpStatusRecord.Effective_Start_Date__c  ){
                    system.debug('first if');
                    
                    ErrorMessage = system.label.Overlapping_Date_Message;
                    if(empStatus.Effective_Start_Date__c<=NewEmpStatusRecord.Effective_End_Date__c || empStatus.Effective_End_Date__c<=NewEmpStatusRecord.Effective_End_Date__c  ){
                        system.debug('2nd if');
                        
                        ErrorMessage = system.label.Overlapping_Date_Message;
                    }
                }
                                
            }            
        }
        
        EmployeeStatusResponse objResponse = new EmployeeStatusResponse(ErrorMessage);
       
        system.debug('overlapping value is '+objResponse);
        return objResponse;
    }
    
    private static Map<string, Employee_Status__c> getEmployeeMap(list<Employee_Status__c> lstEmployeeStatus){
        Map<string, Employee_Status__c > MapEmployeeStatus = new Map<string, Employee_Status__c>();
        for(Employee_Status__c empStatus: lstEmployeeStatus){
            MapEmployeeStatus.put(empStatus.Employee__c, empStatus);
        }
        return MapEmployeeStatus;
    }
    
    public static EmployeeStatusResponse EmployeeStatusEvent(list<Employee_Status__c> lstEmployeeStatus){
        EmployeeStatusResponse Response = new EmployeeStatusResponse();
         Map<string, list<Employee_Status__c> > EmpStatusMap = getStatusMapwithEmployeeStatus(lstEmployeeStatus);
         for(string EmpStaus: EmpStatusMap.keyset()){
            if(EmpStaus==SalesIQGlobalConstants.EMPLOYEE_STATUS_INACTIVE){
                Response =  CheckOverlappingRecord(EmpStatusMap.get(EmpStaus));
                system.debug('Response is '+Response);
                if(Response.Error!=null && Response.Error!=''){
                    return Response;
                }else{                  
                    Response = UpdateEndDate(EmpStatusMap.get(EmpStaus));
                    
                    if(Response.Error==''){
                        List<Employee_Status__c> validEmpStatus = new List<Employee_Status__c>();
                        for(Employee_Status__c obj : EmpStatusMap.get(EmpStaus)){
                              if(obj.Effective_End_Date__c != null && obj.Effective_End_Date__c != Date.newInstance(4000,12,31))
                                    validEmpStatus.add(obj);
                        }
                        if(validEmpStatus.size() > 0)
                         InsertHighEndRecord(validEmpStatus);
                    }
                    
                }               
                
            }else if(EmpStaus==SalesIQGlobalConstants.EMPLOYEE_STATUS_TERMINATED){
                EmployeeStatusTermination(EmpStatusMap.get(EmpStaus));
            }
         }
         return Response;
    }
    
    ////lstEmployeeStatus is the employee which is going to be terminated from the system
    public static EmployeeStatusResponse EmployeeStatusTermination(list<Employee_Status__c> lstEmployeeStatus){
    	
        Map<string, Employee_Status__c > MapNewTerminationRecord = getEmployeeMap(lstEmployeeStatus);
        //list<Employee_Status__c> lstActiveEmployee = RosterUtility.getEmployeeStatusByEmpID(MapNewTerminationRecord.keyset(),new list<string>{SalesIQGlobalConstants.EMPLOYEE_STATUS_ACTIVE});
        list<Employee_status__c> lstAllEmp = [select id,Employee__c,Effective_Start_Date__c,Effective_End_Date__c from Employee_Status__c where employee__c in:MapNewTerminationRecord.keyset()  ];
        list<Employee_Status__c> lstDeletedRecord = null;
                
        system.debug('lstEmployee '+lstAllEmp);
        if(lstAllEmp!=null && lstAllEmp.size() > 0){
            for(Employee_Status__c Emp:lstAllEmp){
            	
            	if((emp.Effective_Start_Date__c>MapNewTerminationRecord.get(Emp.Employee__c).Effective_Start_Date__c && emp.Effective_End_Date__c >MapNewTerminationRecord.get(Emp.Employee__c).Effective_Start_Date__c)){
			        lstDeletedRecord = new list<Employee_Status__c>();
			        system.debug('deleted Record ' + emp.id);    
			        lstDeletedRecord.add(Emp);
			        //Delete this record
			    }                    
            }
            system.debug('lstDeletedRecord '+lstDeletedRecord);
            if(lstDeletedRecord!=null && lstDeletedRecord.size()> 0){
            	delete lstDeletedRecord;
            }
           
           list<Employee_status__c> lstAllEmpToUpdate = [select id,Employee__c,Effective_Start_Date__c,Effective_End_Date__c from Employee_Status__c where employee__c in:MapNewTerminationRecord.keyset()  ];
           system.debug('lstAllEmpToUpdate '+lstAllEmpToUpdate);
           if(lstAllEmpToUpdate!=null && lstAllEmpToUpdate.size() > 0){
           	list<Employee_Status__c> lstUpdateEndDate = new list<Employee_Status__c>();    
           		for(Employee_status__c empUpdate: lstAllEmpToUpdate){
	           	  if(MapNewTerminationRecord.get(empUpdate.Employee__c).Effective_Start_Date__c>empUpdate.Effective_Start_Date__c && MapNewTerminationRecord.get(empUpdate.Employee__c).Effective_Start_Date__c<empUpdate.Effective_End_Date__c){
			        system.debug('future date ' + empUpdate.id); 
			        Employee_status__c objUpdate = new Employee_status__c();
			        objUpdate.id = empUpdate.id;
			        objUpdate.Effective_Start_Date__c = empUpdate.Effective_Start_Date__c;
			        objUpdate.Effective_End_Date__c = MapNewTerminationRecord.get(empUpdate.Employee__c).Effective_End_Date__c-1;
			        objUpdate.Employee__c =empUpdate.Employee__c;
			        //now update the end date of this record to Termination Starta date
			        lstUpdateEndDate.add(objUpdate);
			      }
	           	
	           }
	           system.debug('lstUpdateEndDate '+lstUpdateEndDate);
	           if(lstUpdateEndDate.size() > 0){
	           	  update lstUpdateEndDate;
	           }
           	
           }
           
           
            
        }
        return new EmployeeStatusResponse();
    }
    
    public class EmployeeStatusResponse{
        public string Error{get;set;}
        public EmployeeStatusResponse(){
            
        }
        public EmployeeStatusResponse(string ErrorMessage){
            this.Error = ErrorMessage;
        }
    } 
    
    
    
}