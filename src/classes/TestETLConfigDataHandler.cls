@isTest
public class TestETLConfigDataHandler {
    public static testMethod void testCallout() {
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new SalesIQHTTPMockResponseGenerator());

        ETL_Config__c a = new ETL_Config__c();
        a.Name = 'MapDownload';
        a.end_point__c = 'https://gisdev.axtria.com/map_download';
        insert a;
        Test.startTest();
        ETLConfigDataHandler.getETLConfig(a.id, a.SF_UserName__c, a.SF_Password__c, a.S3_Security_Token__c);
        Test.stopTest();



    }
}