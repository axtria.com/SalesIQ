@isTest
public class TestSalesIQDataTableCtrl_V2 
{
    
    Static String namespacePrefix = SalesIQGlobalConstants.NAME_SPACE ;

    static testMethod void testMethod1()
    {

        TriggerContol__c triggerControlPosition = new TriggerContol__c (IsStopTrigger__c = true , Name='PositionTrigger');
        TriggerContol__c triggerControlCRtrigger = new TriggerContol__c (IsStopTrigger__c = true , Name='ChangeRequestTrigger');
        TriggerContol__c triggerControlSecurityUtil = new TriggerContol__c (IsStopTrigger__c = true , Name='SecurityUtil');
        TriggerContol__c triggerControlScenariotrigger = new TriggerContol__c (IsStopTrigger__c = true , Name='ScenarioTrigger');
        insert new TriggerContol__c[] {triggerControlPosition, triggerControlCRtrigger,triggerControlSecurityUtil,triggerControlScenariotrigger};

        TotalApproval__c totalApp = new TotalApproval__c(Name='Approvals',No_Of_Approval__c=1);
        insert totalApp;

        TotalApproval__c totalApp1 = new TotalApproval__c(Name='System Administrator', Subscriber_Profile__c = 'HO',No_Of_Approval__c=1);
        insert totalApp1; 

        Organization_Master__c orgMaster = new Organization_Master__c();
        orgMaster.Name = 'North America';
        orgMaster.Parent_Country_Level__c = true;
        orgMaster.Org_Level__c  = 'Region';
        insert orgMaster;

        Country__c country = new Country__c();
        country.Name = 'USA';
        country.Parent_Organization__c = orgMaster.id;
        country.status__c = 'Active';
        country.Country_Code__c = 'US';
        insert country;

        Employee__c emp = CallPlanTestDataFactory.createEmployee();
        insert emp;

        Team__c team = TestDataFactory.createTeam('Base');
        team.Country__c = country.id;
        insert team;

        Team__c team2 = TestDataFactory.createTeam('Overlay');
        team2.Controlling_Team__c = team.Id;
        team2.Country__c = country.id;
        insert team2;

        Team_Instance__c team_instance1 = TestDataFactory.CreateTeamInstance(team2.Id,'Zip','',Date.today()-100,Date.today()+100);
        insert team_instance1;

        Team_Instance__c team_instance = TestDataFactory.CreateTeamInstance(team.Id,'Zip','',Date.today()-100,Date.today()+100);
        insert team_instance;

        // Position__C pos = CallPlanTestDataFactory.createPosition(null,team);
        // pos.Hierarchy_Level__c = '2';
        // pos.Team_Instance__c = team_instance.Id;
        // pos.Position_Type__c = 'District';
        // insert pos;
        // Position__C pos1 = CallPlanTestDataFactory.createPosition(pos,team);
        // pos1.Team_Instance__c = team_instance.Id;
        // pos1.Position_Type__c = 'Territory';
        // pos1.Hierarchy_Level__c = '1';
        // insert pos1;

        Position__C pos1 = TestDataFactory.createPosition(team, 'PositionRegion', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_REGION, '3', null);
        insert pos1;
        Position__C pos2 = TestDataFactory.createPosition(team, 'PositionDistrict', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_DISTRICT, '2', pos1.Id);
        insert pos2;
        Position__c pos = TestDataFactory.createPosition(team, 'PositionTerritory', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_TERRITORY, '1', pos2.Id);
        insert pos ;

        Workspace__c ws = TestDataFactory.createWorkspace('WS1', system.today().addMonths(-2), system.today().addMonths(3));
        ws.Country__c = country.id;
        insert ws;
        Scenario__c callPlanScenario = TestDataFactory.createScenario(ws.Id,team_instance.Id,team_instance.Id,'Live','Ready','Active',System.today().addMonths(-1),System.today().addMonths(2));
        callPlanScenario.Scenario_type__c = SalesIQGlobalConstants.SCENARIO_TYPE_CALL_PLAN;
        callPlanScenario.Scenario_Source__c = SalesIQGlobalConstants.SCENARIO_SOURCE_CREATE_FROM_FILE;
        callPlanScenario.CR_Tracking__c = false;
        insert callPlanScenario ;

        team_instance.Scenario__c = callPlanScenario.ID;
        update team_instance;

        Team_Instance_Configuration__c teamInsConfTerr = new Team_Instance_Configuration__c();
        teamInsConfTerr.name = 'TerrConfig';
        teamInsConfTerr.Hierarchy_Level__c = '1';
        teamInsConfTerr.Pattern_Value__c = 'Terr{0000}';
        teamInsConfTerr.Position_Type__c = 'Territory';
        
        string teamID = team.id;
        if(teamId.length() > 15){
            teamID = teamID.left(15);
        }
        
        teamInsConfTerr.Team__c = teamID;
        teamInsConfTerr.Team_Instance__c = team_instance.id;
        //insert teamInsConfTerr;
         
        Team_Instance_Configuration__c teamInsConfDistr = new Team_Instance_Configuration__c();
        //teamInsConf.Configuration_Type__c = 
        teamInsConfDistr.name = 'DistrictConfig';
        teamInsConfDistr.Hierarchy_Level__c = '2';
        teamInsConfDistr.Pattern_Value__c = 'Dist{0000}';
        teamInsConfDistr.Position_Type__c = 'District';
        teamInsConfDistr.Team__c = teamID;
        teamInsConfDistr.Team_Instance__c = team_instance.id;

        insert new Team_Instance_Configuration__c[]{teamInsConfTerr, teamInsConfDistr};

        Team_Instance_Object_Attribute__c tioa = new Team_Instance_Object_Attribute__c();
        tioa.Attribute_API_Name__c = namespacePrefix+'Change_Status__c';
        tioa.Attribute_Display_Name__c = 'Change Status';
        tioa.Data_Type__c = 'Text';
        tioa.Display_Column_Order__c = 1;
        tioa.Interface_Name__c = SalesIQGlobalConstants.CALL_PLAN_MODULE;
        tioa.isEditable__c = false;
        tioa.isEnabled__c = true;
        tioa.Object_Name__c = namespacePrefix+'Position_Account_Call_Plan__c';
        tioa.IsEnabled_Account_Universe__c = true;
        tioa.Team_Instance__c  = team_instance.Id;
        insert(tioa);

        Team_Instance_Object_Attribute__c tioa1 = new Team_Instance_Object_Attribute__c();
        tioa1.Attribute_API_Name__c = namespacePrefix+'Account__r.'+namespacePrefix+'FirstName__c';
        tioa1.Attribute_Display_Name__c = 'First Name';
        tioa1.Data_Type__c = 'Reference';
        tioa1.Display_Column_Order__c = 2;
        tioa1.Interface_Name__c = SalesIQGlobalConstants.CALL_PLAN_MODULE;
        tioa1.isEditable__c = false;
        tioa1.isEnabled__c = true;
        tioa1.Object_Name__c = namespacePrefix+'Position_Account_Call_Plan__c';
        tioa1.Team_Instance__c  = team_instance.Id;
        insert(tioa1);

        Team_Instance_Object_Attribute__c tioa2 = new Team_Instance_Object_Attribute__c();
        tioa2.Attribute_API_Name__c = namespacePrefix+'Picklist1_Segment__c';
        tioa2.Attribute_Display_Name__c = 'Original Call Class';
        tioa2.Data_Type__c = 'Text';
        tioa2.Display_Column_Order__c = 3;
        tioa2.Interface_Name__c = SalesIQGlobalConstants.CALL_PLAN_MODULE;
        tioa2.isEditable__c = false;
        tioa2.isEnabled__c = true;
        tioa2.Object_Name__c = namespacePrefix+'Position_Account_Call_Plan__c';
        tioa2.Team_Instance__c  = team_instance.Id;
        insert(tioa2);

        Team_Instance_Object_Attribute__c tioa3 = new Team_Instance_Object_Attribute__c();
        tioa3.Attribute_API_Name__c = namespacePrefix+'Picklist1_Segment_Updated__c';
        tioa3.Attribute_Display_Name__c = 'Call Class';
        tioa3.Data_Type__c = 'Picklist';
        tioa3.Display_Column_Order__c = 4;
        tioa3.Interface_Name__c = SalesIQGlobalConstants.CALL_PLAN_MODULE;
        tioa3.isEditable__c = true;
        tioa3.isEnabled__c = true;
        tioa3.Object_Name__c = namespacePrefix+'Position_Account_Call_Plan__c';
        tioa3.Team_Instance__c = team_instance.Id;
        insert(tioa3);

        Team_Instance_Object_Attribute__c tioa6 = new Team_Instance_Object_Attribute__c();
        tioa6.Attribute_API_Name__c = namespacePrefix+'Assignment_Type__c';
        tioa6.Attribute_Display_Name__c = ' Assign. Type';
        tioa6.Data_Type__c = 'Picklist';
        tioa6.Display_Column_Order__c = 2;
        tioa6.Interface_Name__c = SalesIQGlobalConstants.INTERFACE_NAME_POSITION_EMPLOYEE;
        tioa6.isEditable__c = false;
        tioa6.isEnabled__c = true;
        tioa6.Object_Name__c = namespacePrefix+'Position_Employee__c';
        tioa6.Team_Instance__c = team_instance.Id;
        insert(tioa6);

        Team_Instance_Object_Attribute__c tioa4 = new Team_Instance_Object_Attribute__c();
        tioa4.Attribute_API_Name__c = namespacePrefix+'Effective_End_Date__c';
        tioa4.Attribute_Display_Name__c = 'Assign. End Date';
        tioa4.Data_Type__c = 'Date';
        tioa4.Display_Column_Order__c = 3;
        tioa4.Interface_Name__c = SalesIQGlobalConstants.INTERFACE_NAME_POSITION_EMPLOYEE;
        tioa4.isEditable__c = false;
        tioa4.isEnabled__c = true;
        tioa4.Object_Name__c = namespacePrefix+'Position_Employee__c';
        tioa4.isRequired__c = true;
        tioa4.Team_Instance__c  = team_instance.Id;
        insert(tioa4);

        Team_Instance_Object_Attribute__c tioa5 = new Team_Instance_Object_Attribute__c();
        tioa5.Attribute_API_Name__c = namespacePrefix+'Effective_Start_Date__c';
        tioa5.Attribute_Display_Name__c = 'Assign. Start Date';
        tioa5.Data_Type__c = 'Date';
        tioa5.Display_Column_Order__c = 4;
        tioa5.Interface_Name__c = SalesIQGlobalConstants.INTERFACE_NAME_POSITION_EMPLOYEE;
        tioa5.isEditable__c = false;
        tioa5.isEnabled__c = true;
        tioa5.Object_Name__c = namespacePrefix+'Position_Employee__c';
        tioa5.Team_Instance__c  = team_instance.Id;
        insert(tioa5);

        Team_Instance_Object_Attribute__c tioa7 = new Team_Instance_Object_Attribute__c();
        tioa7.Attribute_API_Name__c = namespacePrefix+'Position__r.'+namespacePrefix+'Client_Position_code__c';
        tioa7.Attribute_Display_Name__c = ' Territory ID';
        tioa7.Data_Type__c = 'Text';
        tioa7.Display_Column_Order__c = 1;
        tioa7.Interface_Name__c = SalesIQGlobalConstants.INTERFACE_NAME_POSITION_EMPLOYEE;
        tioa7.isEditable__c = false;
        tioa7.isEnabled__c = true;
        tioa7.Object_Name__c = namespacePrefix+'Position_Employee__c';
        tioa7.Team_Instance__c  = team_instance.Id;
        insert(tioa7);

        Account acc = new Account();
        acc.Name = 'Chelsea Parson';
        acc.FirstName__c = 'Chelsea';
        acc.LastName__c = 'Parson';
        insert acc;

        Position_Team_Instance__c posTeam = new Position_Team_Instance__c();
        posTeam.Position_ID__c = pos.id;
        posTeam.Parent_Position_ID__c = pos.id;
        posTeam.Team_Instance_ID__c = team_instance.id;
        posTeam.Effective_End_Date__c = Date.today()+100;
        posTeam.Effective_Start_Date__c = Date.today()-100;
        insert posTeam;
        
        Position_Account_Call_Plan__c posAcc = new Position_Account_Call_Plan__c();
        posAcc.Position__c = pos.id;
        posAcc.Position_Team_Instance__c = posTeam.Id;
        posAcc.Team_Instance__c = team_instance.id;
        posAcc.Account__c = acc.id;
        posAcc.Effective_End_Date__c = Date.newInstance(2018,08,09);
        posAcc.Effective_Start_Date__c = Date.newInstance(2016,04,04);
        posAcc.Picklist1_Segment_Approved__c = '';
        posAcc.Picklist1_Segment__c = '6';
        posAcc.Change_Status__c = SalesIQGlobalConstants.REQUEST_STATUS_SUBMISSION_PENDING;
        insert posAcc;
        
        Position_Account_Call_Plan__c posAcc1 = new Position_Account_Call_Plan__c();
        posAcc1.Position__c = pos.id;
        posAcc1.Position_Team_Instance__c = posTeam.Id;
        posAcc1.Team_Instance__c = team_instance.id;
        posAcc1.Account__c = acc.id;
        posAcc1.Effective_End_Date__c = Date.newInstance(2018,08,09);
        posAcc1.Effective_Start_Date__c = Date.newInstance(2016,04,04);
        posAcc1.Picklist1_Segment_Approved__c = '1';
        posAcc1.Change_Status__c = SalesIQGlobalConstants.REQUEST_STATUS_SUBMISSION_PENDING;
        insert posAcc1;
        

        Change_Request_Type__c crType = CallPlanTestDataFactory.CreateCallPlanRequestType();
        insert crType;

        Change_Request__c changedRec = new Change_Request__c();                                               
        changedRec.Reason__c =  'Call Plan Changes';
        changedRec.recordtypeId = SalesIQUtility.getselctedRecordType(namespacePrefix + 'Change_Request__c', SalesIQGlobalConstants.REQUEST_TYPE_CALL_PLAN );
        changedRec.RecordTypeID__c = SalesIQUtility.getChangeRequestTypebyRequestType('Call_Plan_Change');
        changedRec.Request_Type_Change__c = SalesIQGlobalConstants.REQUEST_TYPE_CALL_PLAN;  
                // changedRec.Account_SF_Id__c = movedAccs.removeEnd(',');
        changedRec.Source_Position__c = pos.Id;
        changedRec.Destination_Position__c = pos.Id;  
        changedRec.Team_Instance_ID__c = team_instance.id;
        changedRec.Status__c = SalesIQGlobalConstants.REQUEST_STATUS_PENDING;
        insert changedRec;
        
        list<CIM_Config__c> cimConfigs = CallPlanTestDataFactory.createCIMCOnfig(crType, team_instance, 'Position_Account_Call_Plan__c');
        cimConfigs[0].Name = 'No of Targets';
        cimConfigs[0].MetricCalculationType__c = 'Absolute';
        cimConfigs[1].Name = 'Total Calls';
        cimConfigs[1].isOptimum__c = true;
        cimConfigs[2].Name = 'No of Tier1 Physician';
        cimConfigs[2].MetricCalculationType__c = 'Absolute';
        cimConfigs[2].isOptimum__c = true;
        cimConfigs[3].Name = 'No of Tier2 Physician';
        cimConfigs[4].Name = 'No of Tier3 Physician';
        cimConfigs[5].Name = 'No of Non-Target Physician';
        insert cimConfigs;

        Dependency_Control__c depControl = new Dependency_Control__c();
        depControl.Name = 'Dependency0';
        depControl.Controlling_Field_1__c = 'Picklist1_Segment_Updated__c';
        depControl.Dependent_Field_1__c = 'Call_Frequency_Updated__c';
        depControl.IsActive__c = true;
        depControl.Team_Instance__c = team_instance.Id;
        // insert depControl;

        Dependency_Control__c depControl1 = new Dependency_Control__c();
        depControl1.Name = 'Dependency1';
        depControl1.Controlling_Field_1__c = 'Picklist10_Segment_Updated__c';
        depControl1.Controlling_Field_2__c = 'Picklist7_Segment_Updated__c';
        depControl1.Dependent_Field_1__c = 'Picklist9_Segment_Updated__c';
        depControl1.IsActive__c = true;
        depControl1.Team_Instance__c = team_instance.Id;
        // insert depControl2;

        Dependency_Control__c depControl2 = new Dependency_Control__c();
        depControl2.Name = 'Dependency2';
        depControl2.Controlling_Field_1__c = 'Picklist1_Segment_Updated__c';
        // depControl2.Controlling_Field_2__c = 'Picklist7_Segment_Updated__c';
        depControl2.Dependent_Field_1__c = 'Picklist9_Segment_Updated__c';
        depControl2.IsActive__c = true;
        depControl2.Team_Instance__c = team_instance.Id;
        // insert depControl2;
        insert new List<Dependency_Control__c>{depControl,depControl1,depControl2};

        Dependency_Control_Condition__c depCondition = new Dependency_Control_Condition__c();
        depCondition.Name = 'DepCondition1';
        depCondition.Dependency_Control__c = depControl.Id;
        depCondition.IsActive__c = true;
        depCondition.Entry_Criteria_1__c = 'Picklist1_Segment_Updated__c = 9';
        depCondition.Dependent_Field_1_Values__c = '5';
        depCondition.Team_Instance__c = team_instance.Id;
        depCondition.Error__c = 'Wrong Class';
        insert depCondition;

        Dependency_Control_Condition__c depCondition2 = new Dependency_Control_Condition__c();
        depCondition2.Name = 'DepCondition2';
        depCondition2.Dependency_Control__c = depControl1.Id;
        depCondition2.IsActive__c = true;
        depCondition2.Entry_Criteria_1__c = 'Picklist10_Segment_Updated__c = Class 5';
        depCondition2.Dependent_Field_1_Values__c = '5';
        depCondition2.Team_Instance__c = team_instance.Id;
        depCondition2.Error__c = 'Wrong Class';
        insert depCondition2;

        Dependency_Control_Condition__c depCondition3= new Dependency_Control_Condition__c();
        depCondition3.Name = 'DepCondition3';
        depCondition3.Dependency_Control__c = depControl2.Id;
        depCondition3.IsActive__c = true;
        depCondition3.Entry_Criteria_1__c = 'Picklist1_Segment_Updated__c = 9';
        depCondition3.Dependent_Field_1_Values__c = '5';
        depCondition3.Team_Instance__c = team_instance.Id;
        depCondition3.Error__c = 'Wrong Class';
        insert depCondition3;




        CR_Approval_Config__c appConfig = new CR_Approval_Config__c(Team_Instance_ID__c = team_instance.Id, Change_Request_Type__c = crType.Id, HO_Office_User__c = 'Y', HR_User__c = 'N', IsImpactedPosition__c = false, isHigherLevelManagers__c = false, isImpactedManager__c = false);
        insert appConfig;

        

        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User usr = TestDataFactory.createUser(p.Id);
        usr.ManagerId = UserInfo.getUserId();
        insert usr;

        test.startTest();
        System.runAs(usr)
        { 

            PermissionSet ps = [SELECT Id, Name From PermissionSet WHERE Name = 'SalesIQ_HO'];
            insert new PermissionSetAssignment(AssigneeId = usr.Id, PermissionSetId = ps.Id );

            User_Access_Permission__c objUserAccessPerm = TestDataFactory.createUserAccessPerm(pos.Id,usr.Id,team_instance.Id);
            insert objUserAccessPerm;
            String datatableJson = '';
            try{
                datatableJson = SalesIQDataTableCtrl_V2.initialiseDataTable(SalesIQGlobalConstants.CALL_PLAN_MODULE,team_instance.id,(string)pos.Id,'','',(string)country.Id);
            }
            catch(Exception e){
                
            }
            try{
                datatableJson = SalesIQDataTableCtrl_V2.initialiseDataTable(SalesIQGlobalConstants.CALL_PLAN_MODULE,'','','','',(string)country.Id);
            }
            catch(Exception e){
                
            }
            try{
                datatableJson = SalesIQDataTableCtrl_V2.initialiseDataTable(SalesIQGlobalConstants.POSITION_UNIVERSE,'','','','',(string)country.Id);
            }
            catch(Exception e){
                
            }
            

            CR_Team_Instance_Config__c crTeamInsConfig = new CR_Team_Instance_Config__c();
            crTeamInsConfig.Configuration_Name__c = 'Call Plan';
            crTeamInsConfig.Configuration_Type__c = 'Date Table Configuration';
            crTeamInsConfig.Configuration_Value__c = '';
            crTeamInsConfig.Fixed_Column_From_Left__c = 2.0;
            crTeamInsConfig.Fixed_Column_From_Right__c = 2.0;
            crTeamInsConfig.Page_Size__c = 12.0;
            crTeamInsConfig.Bulk_Edit_Allowed__c = false;
            crTeamInsConfig.Pagination_on__c = true;
            insert crTeamInsConfig;

            SalesIQDataTableCtrl_V2.getOrgNamespace();
            map<String,list<String>> profileMap = SalesIQDataTableCtrl_V2.getProfileNameMap();
            SalesIQDataTableCtrl_V2.isScenarioLock(callPlanScenario.Id);
            String datatableJson4 = '';
            String datatableJson3 = '';
            String datatableJsonTest = '';
            String datatableJson2 = '';
            String datatableJson5 = '';
            try{
                 datatableJson4 = SalesIQDataTableCtrl_V2.initialiseDataTable(SalesIQGlobalConstants.CALL_PLAN_MODULE,team_instance.id,(string)pos.Id,SalesIQGlobalConstants.ELIGIBLE_TARGETS,'abc',(string)country.Id);
                 datatableJson3 = SalesIQDataTableCtrl_V2.initialiseDataTable(SalesIQGlobalConstants.CALL_PLAN_MODULE,'',(string)pos.Id,'','', (string)country.id);
                 datatableJsonTest = SalesIQDataTableCtrl_V2.initialiseDataTable(SalesIQGlobalConstants.POSITION_UNIVERSE,team_instance.id,(string)pos.Id,'','',(string)country.id);
                 datatableJson2 = SalesIQDataTableCtrl_V2.initialiseDataTable(SalesIQGlobalConstants.POSITION_UNIVERSE,'',(string)pos.Id,'','',(string)country.id);
                 datatableJson5 = SalesIQDataTableCtrl_V2.initialiseDataTable(SalesIQGlobalConstants.MODULE_TYPE_CR_CALL_PLAN,team_instance.id,(string)pos.Id,'',(String)changedRec.id,(string)country.id);
            }catch(Exception e){
            }
            
            SalesIQDataTableCtrl_V2.undoCallPlan(pos.Id,team_instance.Id);

            SalesIQDataTableCtrl_V2.ColumnsHeaderResponse columnsresponse;
            SalesIQDataTableCtrl_V2.submissionPendingRecords(team_instance.Id,pos.Id);
            try{
                SalesIQDataTableCtrl_V2.Response response = SalesIQDataTableCtrl_V2.runQuery(SalesIQGlobalConstants.CALL_PLAN_MODULE, '', '{}',pos.Id,team_instance.Id,((SalesIQDataTableCtrl_V2.ColumnsHeaderResponse )JSON.deserialize(datatableJson,SalesIQDataTableCtrl_V2.ColumnsHeaderResponse.class)).allFieldsAPI,'', '','');
            }catch(Exception e){}

            try{
                SalesIQDataTableCtrl_V2.Response response1 = SalesIQDataTableCtrl_V2.runQuery(SalesIQGlobalConstants.CALL_PLAN_MODULE, '', '{}',pos.Id,team_instance.Id,((SalesIQDataTableCtrl_V2.ColumnsHeaderResponse )JSON.deserialize(datatableJson,SalesIQDataTableCtrl_V2.ColumnsHeaderResponse.class)).allFieldsAPI,'', SalesIQGlobalConstants.WHOLE_UNIVERSE_TARGETS,'');    
            }catch(Exception e){}
            
            map<String, list<Dependency_Control_Condition__c>> dependencyResult1 = SalesIQDataTableCtrl_V2.getDependencyControlData(team_instance.Id);
            Map<String,List<String>> saveResult = SalesIQDataTableCtrl_V2.saveCallPlan('{"'+ posAcc.Id +'":{"'+namespacePrefix+'Picklist1_Segment_Updated__c":"9","rowIndex":0}}',team_instance.Id,pos.Id,'');
            map<String, list<Dependency_Control_Condition__c>> dependencyResult2 = SalesIQDataTableCtrl_V2.getDependencyControlData(team_instance.Id);
            
            SalesIQDataTableCtrl_V2.undoCallPlan(pos.Id,team_instance.Id);
            // try catch is added to avoid errors due to approval
            try{
                SalesIQDataTableCtrl_V2.undoRow(pos.Id,team_instance.Id,posAcc.Id,((SalesIQDataTableCtrl_V2.ColumnsHeaderResponse )JSON.deserialize(datatableJson4,SalesIQDataTableCtrl_V2.ColumnsHeaderResponse.class)).allFieldsAPI);
                
            }catch(Exception e){
                System.debug(e);
            }
            try{
                List<String> submitResult = SalesIQDataTableCtrl_V2.submitCallPlan('{"'+ posAcc.Id +'":{"'+namespacePrefix+'Picklist1_Segment_Updated__c":"9","rowIndex":0}}',team_instance.Id,pos.Id);
            }catch(Exception e){}
            try{
                list<String> confirmResponse = SalesIQDataTableCtrl_V2.confirmSubmitCallPlan(team_instance.Id,pos.Id,JSON.serialize(profileMap));
            }catch(Exception e){}
            try{
                SalesIQDataTableCtrl_V2.Response response4 = SalesIQDataTableCtrl_V2.runQuery(SalesIQGlobalConstants.MODULE_TYPE_CR_CALL_PLAN,(String) changedRec.Id, '{}',pos.Id,team_instance.Id,((SalesIQDataTableCtrl_V2.ColumnsHeaderResponse )JSON.deserialize(datatableJson5,SalesIQDataTableCtrl_V2.ColumnsHeaderResponse.class)).allFieldsAPI,'', '','');
                SalesIQDataTableCtrl_V2.Response response2 = SalesIQDataTableCtrl_V2.runQuery(SalesIQGlobalConstants.CALL_PLAN_MODULE, '', '{}',pos.Id,team_instance.Id,((SalesIQDataTableCtrl_V2.ColumnsHeaderResponse )JSON.deserialize(datatableJson4,SalesIQDataTableCtrl_V2.ColumnsHeaderResponse.class)).allFieldsAPI,'', SalesIQGlobalConstants.ELIGIBLE_TARGETS,'');
                SalesIQDataTableCtrl_V2.Response response3 = SalesIQDataTableCtrl_V2.runQuery(SalesIQGlobalConstants.CALL_PLAN_MODULE, '', '{}',pos.Id,team_instance.Id,((SalesIQDataTableCtrl_V2.ColumnsHeaderResponse )JSON.deserialize(datatableJson,SalesIQDataTableCtrl_V2.ColumnsHeaderResponse.class)).allFieldsAPI,'', SalesIQGlobalConstants.ELIGIBLE_ACROSS_TEAM_TARGETS,'');
            }catch(Exception e){}
            
            List<ObjectDetailWrapper> wrap3 = SalesIQManageAssignmentCtrl.addAssignments(new List<String>{pos.Id},emp.Id, SalesIQGlobalConstants.EMPLOYEE_UNIVERSE,'Primary');

            String datatableJsonPEMP = SalesIQDataTableCtrl_V2.initialiseDataTable(SalesIQGlobalConstants.INTERFACE_NAME_POSITION_EMPLOYEE,team_instance.id,(string)pos.Id,'','',(string)country.Id);

            SalesIQDataTableCtrl_V2.Response responsePemp = SalesIQDataTableCtrl_V2.runQuery(SalesIQGlobalConstants.INTERFACE_NAME_POSITION_EMPLOYEE, '', '{}',pos.Id,team_instance.Id,((SalesIQDataTableCtrl_V2.ColumnsHeaderResponse )JSON.deserialize(datatableJsonPEMP,SalesIQDataTableCtrl_V2.ColumnsHeaderResponse.class)).allFieldsAPI,'','',JSON.serialize(wrap3));

           
            // SalesIQDataTableCtrl_V2.Response response4 = SalesIQDataTableCtrl_V2.runQuery(SalesIQGlobalConstants.CALL_PLAN_MODULE, '', '{}',pos.Id,team_instance.Id,((SalesIQDataTableCtrl_V2.ColumnsHeaderResponse )JSON.deserialize(datatableJson,SalesIQDataTableCtrl_V2.ColumnsHeaderResponse.class)).allFieldsAPI,'', SalesIQGlobalConstants.WHOLE_UNIVERSE_TARGETS);
            SalesIQDataTableCtrl_V2.logException('',SalesIQGlobalConstants.CALL_PLAN_MODULE);
        }   

        test.stopTest();

    }
}