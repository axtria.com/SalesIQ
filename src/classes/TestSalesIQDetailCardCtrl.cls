@isTest
private class TestSalesIQDetailCardCtrl {

		Static Employee__c emp;
		Static Position__c pos;
		static void testData() 
    	{
			Team__c team = CallPlanTestDataFactory.createTeam('Base');
	        insert team;

	        String ns = SalesIQGlobalConstants.NAME_SPACE;

	    	emp = CallPlanTestDataFactory.createEmployee();
	        insert emp;

	        Employee__c emp2 = CallPlanTestDataFactory.createEmployee();
	        insert emp2;

	        Team_Instance__c team_instance = CallPlanTestDataFactory.CreateTeamInstance(team,'Zip');
	        team_instance.IC_EffstartDate__c = date.today() - 5;
	        team_instance.IC_EffEndDate__c = date.today() + 5;
	        insert team_instance;

	        pos = CallPlanTestDataFactory.createPosition(null,team);
	        pos.Team_Instance__c = team_instance.Id;
	        insert pos;

	        Team_Instance_Object_Attribute__c tioa = new Team_Instance_Object_Attribute__c();
			tioa.Attribute_API_Name__c = ns+'Manager__r.Name';
			tioa.Attribute_Display_Name__c = 'Manager';
			tioa.Data_Type__c = 'REFERENCE';
			tioa.Display_Column_Order__c = 2;
			tioa.Interface_Name__c = SalesIQGlobalConstants.MANAGE_ASSIGNMENT_EMPLOYEE_DETAILS;
			tioa.isEditable__c = false;
			tioa.isEnabled__c = true;
			tioa.Object_Name__c = ns+'Employee__c';
			tioa.Team_Instance__c  = team_instance.Id;
			insert(tioa);

			Team_Instance_Object_Attribute__c tioa1 = new Team_Instance_Object_Attribute__c();
			tioa1.Attribute_API_Name__c = ns+'Employee_Type__c';
			tioa1.Attribute_Display_Name__c = 'Employee Type';
			tioa1.Data_Type__c = 'Text';
			tioa1.Display_Column_Order__c = 1;
			tioa1.Interface_Name__c = SalesIQGlobalConstants.MANAGE_ASSIGNMENT_EMPLOYEE_DETAILS;
			tioa1.isEditable__c = false;
			tioa1.isEnabled__c = true;
			tioa1.Object_Name__c = ns+'Employee__c';
			tioa1.Team_Instance__c  = team_instance.Id;
			insert(tioa1);

			Team_Instance_Object_Attribute__c tioa2 = new Team_Instance_Object_Attribute__c();
			tioa2.Attribute_API_Name__c = ns+'Employee__r.Name';
			tioa2.Attribute_Display_Name__c = 'Employee Name';
			tioa2.Data_Type__c = 'Reference';
			tioa2.Display_Column_Order__c = 1;
			tioa2.Interface_Name__c = SalesIQGlobalConstants.MANAGE_ASSIGNMENT_POSITION_EMPLOYEE_DETAILS;
			tioa2.isEditable__c = false;
			tioa2.isEnabled__c = true;
			tioa2.Object_Name__c = ns+'Position_Employee__c';
			tioa2.Team_Instance__c  = team_instance.id;
			insert(tioa2);

			Team_Instance_Object_Attribute__c tioa3 = new Team_Instance_Object_Attribute__c();
			tioa3.Attribute_API_Name__c = ns+'Effective_Start_Date__c';
			tioa3.Attribute_Display_Name__c = 'Start Date';
			tioa3.Data_Type__c = 'Date';
			tioa3.Display_Column_Order__c = 2;
			tioa3.Interface_Name__c = SalesIQGlobalConstants.MANAGE_ASSIGNMENT_POSITION_EMPLOYEE_DETAILS;
			tioa3.isEditable__c = false;
			tioa3.isEnabled__c = true;
			tioa3.Object_Name__c = ns+'Position_Employee__c';
			tioa3.Team_Instance__c  = team_instance.id;
			insert(tioa3);

			Team_Instance_Object_Attribute__c tioa4 = new Team_Instance_Object_Attribute__c();
			tioa4.Attribute_API_Name__c = ns+'Position_Type__c';
			tioa4.Attribute_Display_Name__c = 'Position Type';
			tioa4.Data_Type__c = 'Text';
			tioa4.Display_Column_Order__c = 1;
			tioa4.Interface_Name__c = SalesIQGlobalConstants.MANAGE_ASSIGNMENT_POSITION_DETAILS;
			tioa4.isEditable__c = false;
			tioa4.isEnabled__c = true;
			tioa4.Object_Name__c = ns+'Position__c';
			tioa4.Team_Instance__c  = team_instance.Id;
			insert(tioa4);

			Team_Instance_Object_Attribute__c tioa5 = new Team_Instance_Object_Attribute__c();
			tioa5.Attribute_API_Name__c = ns+'Effective_Start_Date__c';
			tioa5.Attribute_Display_Name__c = 'Start Date';
			tioa5.Data_Type__c = 'DATE';
			tioa5.Display_Column_Order__c = 2;
			tioa5.Interface_Name__c = SalesIQGlobalConstants.MANAGE_ASSIGNMENT_POSITION_DETAILS;
			tioa5.isEditable__c = false;
			tioa5.isEnabled__c = true;
			tioa5.Object_Name__c = ns+'Position__c';
			tioa5.Team_Instance__c  = team_instance.Id;
			insert(tioa5);
		}
		
		static testMethod void testSaveAssignments()    
    	{
	    	testData();
	        test.startTest();
	        SalesIQDetailCardWrapper wrap = SalesIQDetailCardCtrl.getRecordData(emp.Id,'Employee__c','false','','EMPLOYEE');
	        SalesIQDetailCardWrapper wrap2 = SalesIQDetailCardCtrl.getRecordData(pos.Id,'Position_Employee__c','true','Position__c','POSITION EMPLOYEE');
	        SalesIQDetailCardWrapper wrap3 = SalesIQDetailCardCtrl.getRecordData(pos.Id,'Position__c','false','','POSITION');
	        SalesIQDetailCardCtrl.logException('hello','world');
	        test.stopTest();
	    }    	
    
}
