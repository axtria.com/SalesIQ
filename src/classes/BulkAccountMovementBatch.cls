global class BulkAccountMovementBatch implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful{
     
    public List<CR_Account__c> crAccList {get;set;}
    public List<String> geoList;
    public set<ID> posgeoIds;
    Public List<String> crIds {get;set;}
    public Change_Request__c crReq {get;set;}
    public List<String> pos;
    public string teamInstance;
    public Boolean isError = false;//this variable is used to track whether any error get occured in execute method or inner methd of that execute method i.e CreateCRAccountBulk
    //So we need to stop further execution of finish method, which is further executing other batch/method. in that case other method will mark CR status to completed , which leads to
    //inconsitency
    
    public BulkAccountMovementBatch(List<String> crAccids,Change_Request__C cr)
    {
        crAccList = new List<CR_Account__c>();
        crIds = crAccids;  
        crReq  = cr;
        pos = new List<String>();
        pos.addAll(cr.Sources_Bulk__c.split(','));
        teamInstance = cr.Team_Instance_ID__c;
       
    }
     
    global  Database.QueryLocator start(Database.BatchableContext BC)
    {    
       String query = 'Select Destination_Position__c,Source_Position__c,Destination_Position__r.Name , Account__c,IsImpactCallPlan__c, Destination_Position_Team_Instance__c,Account__r.billingPostalCode From CR_Account__c where id IN: crIDs';
       return Database.getQueryLocator(query);

    }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        try{
            ChangeRequestTriggerHandler.ApproveRejectRequestAccBulk((List<CR_Account__c>)scope,crReq,SalesIQGlobalConstants.REQUEST_STATUS_APPROVED);  

        }catch(Exception e){
            isError = true;
            SalesIQUtility.updateChangeRequestStatus(new set<id>{crReq.id}, true);

        } 

    }
    
    global void finish(Database.BatchableContext BC)
    {
        //Team_Instance__c objTeamInstance= SalesIQUtility.getTeamInstanceById(teamInstance); 
        //System.debug('objTeamInstance.EnableAccountSharing__c '+objTeamInstance.EnableAccountSharing__c);
        if(!isError){       
       
            
            /*If moved ZIP have accounts then CR status should be updated to "Completed" here just after update Position geography. If there is no accounts in CR
            then Completion of CR object has marked in BUlkZIPorAccountBatch        
            */

            if(crIds.size() > 0 ){
                // no need to chkeck account sharing condition , coz ZIP shared ZIP has shared account then those position of those shared account should be updated
                
                database.executeBatch(new UpdatePositionAccountSharedBatch(crReq.Team_Instance_ID__c ,new set<id>{crReq.id}, true),1500);  

                if(crReq.Execution_Status__c==SalesIQGlobalConstants.BULK_CR_IN_PROGRESS){
                    crReq.Execution_Status__c = SalesIQGlobalConstants.BULK_CR_COMPLETED;
                    update crReq;
                }

                List<Position__c> pList = SalesIQUtility.getPositions(pos,false);
                if(pos.size()>0)
                   update pList;

            } 
             isError = false; 
        }     

       
        System.debug('crReq after query '+crReq);        
       
    }
}