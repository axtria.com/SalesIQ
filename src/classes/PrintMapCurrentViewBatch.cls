/**********************************************************************************************
@author     :  Harkirat Singh
@date       : 
@description: This is the controller for Printing Current View of the map
Revison(s)  : 
**********************************************************************************************/

global class PrintMapCurrentViewBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {
    public string logid{get;set;}
    public string jsonparam{get;set;}
  
    global PrintMapCurrentViewBatch(string id1,string j)
    { 
        logid=id1;	
        jsonparam=j;
    }
   
    global Database.QueryLocator start(Database.BatchableContext BC)
    {    
       String query = 'SELECT CreatedDate,Id,Interface_Name__c,Job_Status__c,Name,Output__c,Position__r.Client_Position_Code__c, Position__r.Hierarchy_Level__c,Request_Type__c,Team_Instance__r.Name,Position__r.Position_Type__c FROM Activity_Log__c WHERE id=:logid';
       return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Activity_Log__c> logs)
    {  
    	try
    	{
    	 MapServerURL__c ESRIService = MapServerURL__c.getOrgDefaults();   
         string  url;
         string table= ESRIService.ZIPTERR_Table__c; //'alignmax.sde.Nextgen_Dev_Admin_28112016';
         string printURL=ESRIService.Print_URL__c;
         string printstring;
         string   printurl1;
         Integer index=25;
         if(printURL!=null)
            string tempprint=printURL.substring(0,8);
         
         if(printURL!=null)
            printstring=printURL.substring(8,40);
         if(printstring!=null)
            index=printstring.indexOf('/');
         if(printURL!=null)
            printurl1=printURL.substring(0,index+8);
           
         logs[0].Job_Status__c='In_Progress';
      
         Http h = new Http();
         HttpRequest req = new HttpRequest();
         String  address; 
         if(printurl1!=null)
          address = printurl1+'/arcgis/rest/services/PrinitingTools2/GPServer/Export%20Web%20Map/submitJob?';
         else
          address ='https://uat.align-max.com/arcgis/rest/services/PrinitingTools2/GPServer/Export%20Web%20Map/submitJob?';
         String body='Web_Map_as_JSON='+jsonparam+'&Format=PDF'+'&Layout_Template=A3 Landscape' +'&f=json';
         
         String address1;
         req.setEndPoint(address);
         req.setMethod('POST');
         req.setTimeout(120000);
         req.setHeader('X-Frame-Options', 'ALLOW-FROM');
	     req.setHeader('Pragma', 'no-cache');
	     req.setHeader('Cache-Control', 'no-cache');
	     req.setHeader('X-XSS-Protection', '1');
	     req.setHeader('x-content-type-options', 'nosniff');
	     req.setHeader('SET-COOKIE', 'HttpOnly');
	     req.setHeader('Content-Length', '512');
         req.setBody(body);
        
         HttpResponse res = h.send(req);
         String loc = res.getHeader('Location');
         String status;
         Long time1 = System.now().getTime();
         System.debug('---time--'+time1);
         while((System.now().getTime()-time1)<60000) 
         {
            Integer start = System.Now().millisecond();
            Long startingTime = System.now().getTime();
            Integer delayInMilliseconds = 10000; // One-second delay
		    while (System.now().getTime() - startingTime < delayInMilliseconds)  {
           						 // Do nothing until desired delay has passed
			}
            try
            {
              if(res.getStatusCode() ==200)
	          {
	             JSONParser parser = JSON.createParser(res.getBody());
		         System.JSONToken token,token2, token3;
	             parser.nextToken();    // Eat first START_OBJECT 
				 string  jobStatus;
				 string  jobId;
  				 while((token=parser.nextToken()) != null)
  				 {
  				   if(((token=parser.getCurrentToken()) == JSONToken.FIELD_NAME )&&(parser.getText()=='jobStatus')){
  					     parser.nextToken();
  						 jobStatus=Parser.getText();
  					}
				   if(((token=parser.getCurrentToken()) == JSONToken.FIELD_NAME )&&(parser.getText()=='jobId')){
						parser.nextToken();
						jobId=Parser.getText();
				    }
				 }
    			 if(jobStatus=='esriJobFailed')
    			 {
    				logs[0].Job_Status__c='Failed';
    				update logs;    
    				break;
    			 } 
    			 if(jobStatus=='esriJobSucceeded')
    			 {
    				status='esriJobSucceeded';
    				break;
    			 }
				 else
				 {
				   if(printurl1!=null)
				     address = printurl1+'/arcgis/rest/services/PrinitingTools2/GPServer/Export%20Web%20Map/jobs/'+jobId;
				   else
	       			 address = 'https://uat.align-max.com/arcgis/rest/services/PrinitingTools2/GPServer/Export%20Web%20Map/jobs/'+jobId;
	       		   address1=address;
	       		   req.setEndPoint(address);
	               res = h.send(req); 
	             }
	           }
	           else
	           {
	         	req.setEndpoint(loc);
		        res = h.send(req);
	           }
             }
             catch(System.CalloutException e)
             {
               logs[0].Job_Status__c='Failed';
	           update logs;    
             }
          }
          if((System.now().getTime()-time1)<600000)
          {
            if(status!='esriJobSucceeded')
            {
              logs[0].Job_Status__c='Failed';
	          update logs;   
	        }
          }
          if(status=='esriJobSucceeded')
          {
	        address=address1+'/results/Output_File';
	        req.setMethod('POST');
	     	req.setEndPoint(address);
	        res = h.send(req);
	        if(res.getStatusCode() ==200)
	        {
		      JSONParser parser = JSON.createParser(res.getBody());
		      System.JSONToken token,token2, token3;
	          parser.nextToken();    // Eat first START_OBJECT 
			  string  jobId;
			  while((token=parser.nextToken()) != null)
			  {
				if(((token=parser.getCurrentToken()) == JSONToken.FIELD_NAME )&&(parser.getText()=='url')){
					 parser.nextToken();
					 url=Parser.getText(); 
				}
	          }
			 }                    
	         logs[0].Output__c=url;
	         logs[0].Job_Status__c='Completed';
	         update logs;                
	       }
	             
    	}
    	catch(System.LimitException e)
    	{
    		logs[0].Job_Status__c='Failed';
    		update logs;
    	}
    }
    
    global void finish(Database.BatchableContext BC)
    {
    	
    }
}
