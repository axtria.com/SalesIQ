@IsTest
public with sharing class TestPositionGeographyTrigger {
	
	static testMethod void testPositionGeographyInsert(){
		
		TriggerContol__c  triggerctrl =  new TriggerContol__c(IsStopTrigger__c=false , Name='PositionGeographyTrigger');
		insert triggerctrl;
		
		Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11778');
		insert geo;
		
		Team__c zipBasedTeam =  CallPlanTestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_GeoBased);    	
    	insert zipBasedTeam;
    	
		Team_Instance__c zipBasedTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(zipBasedTeam,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP);
    	insert zipBasedTeamInstance;
    	
    	Position__c region = CallPlanTestDataFactory.createRegionPosition(zipBasedTeam);
		insert region;
		
		Position_Team_Instance__c regionTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(region, null, zipBasedTeamInstance);
		regionTeamInstance.X_Max__c=-72.6966429900;
        regionTeamInstance.X_Min__c=-73.9625820000;
        regionTeamInstance.Y_Max__c=40.9666490000;
        regionTeamInstance.Y_Min__c=40.5821279800;
		insert regionTeamInstance;
		
		Position__c district = CallPlanTestDataFactory.createPositionParent(region,zipBasedTeam);
		insert district;
		
		Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, region, zipBasedTeamInstance);
		districtTeamInstance.X_Max__c=-72.6966429900;
        districtTeamInstance.X_Min__c=-73.9625820000;
        districtTeamInstance.Y_Max__c=40.9666490000;
        districtTeamInstance.Y_Min__c=40.5821279800;
		insert districtTeamInstance;
		
		Position__c territory =  CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
		territory.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
		insert territory;
		
		Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, district, zipBasedTeamInstance);
		territoryTeamInstance.X_Max__c=-72.6966429900;
        territoryTeamInstance.X_Min__c=-73.9625820000;
        territoryTeamInstance.Y_Max__c=40.9666490000;
        territoryTeamInstance.Y_Min__c=40.5821279800;
		insert territoryTeamInstance;

        /*Team_Instance_Geography__c geographyTeamInstance = CallPlanTestDataFactory.CreateTeamInstanceGeoGraphy(geo,zipBasedTeamInstance);
        insert geographyTeamInstance;*/
        
        Position_Geography__c posGeo = CallPlanTestDataFactory.createPosGeography(geo,territory,territoryTeamInstance,zipBasedTeamInstance);
        test.startTest();
        insert posGeo;
		test.stopTest();	
	}
	
	static testMethod void testPositionGeographyUpdate(){
		
		TriggerContol__c  triggerctrl =  new TriggerContol__c(IsStopTrigger__c=false , Name='PositionGeographyTrigger');
		insert triggerctrl;
		
		Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11778');
		insert geo;
		
		Team__c zipBasedTeam =  CallPlanTestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_GeoBased);    	
    	insert zipBasedTeam;
    	
		Team_Instance__c zipBasedTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(zipBasedTeam,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP);
    	insert zipBasedTeamInstance;
    	
    	Position__c region = CallPlanTestDataFactory.createRegionPosition(zipBasedTeam);
		insert region;
		
		Position_Team_Instance__c regionTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(region, null, zipBasedTeamInstance);
		regionTeamInstance.X_Max__c=-72.6966429900;
        regionTeamInstance.X_Min__c=-73.9625820000;
        regionTeamInstance.Y_Max__c=40.9666490000;
        regionTeamInstance.Y_Min__c=40.5821279800;
		insert regionTeamInstance;
		
		Position__c district = CallPlanTestDataFactory.createPositionParent(region,zipBasedTeam);
		insert district;
		
		Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, region, zipBasedTeamInstance);
		districtTeamInstance.X_Max__c=-72.6966429900;
        districtTeamInstance.X_Min__c=-73.9625820000;
        districtTeamInstance.Y_Max__c=40.9666490000;
        districtTeamInstance.Y_Min__c=40.5821279800;
		insert districtTeamInstance;
		
		Position__c territory =  CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
		territory.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
		insert territory;
		
		Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, district, zipBasedTeamInstance);
		territoryTeamInstance.X_Max__c=-72.6966429900;
        territoryTeamInstance.X_Min__c=-73.9625820000;
        territoryTeamInstance.Y_Max__c=40.9666490000;
        territoryTeamInstance.Y_Min__c=40.5821279800;
		insert territoryTeamInstance;
		
		Position__c destTerr = CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
		destTerr.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
        destTerr.Name = 'Long Island West';
        destTerr.Client_Territory_Name__c = 'Long Island West';
     	destTerr.Client_Position_Code__c = '1NE30012';
      	destTerr.Client_Territory_Code__c='1NE30012';
		insert destTerr;
		
		Position_Team_Instance__c destTerrTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(destTerr, district, zipBasedTeamInstance);
		destTerrTeamInstance.X_Max__c=-72.6966429900;
        destTerrTeamInstance.X_Min__c=-73.9625820000;
        destTerrTeamInstance.Y_Max__c=40.9666490000;
        destTerrTeamInstance.Y_Min__c=40.5821279800;
		insert destTerrTeamInstance;

        /*Team_Instance_Geography__c geographyTeamInstance = CallPlanTestDataFactory.CreateTeamInstanceGeoGraphy(geo,zipBasedTeamInstance);
        insert geographyTeamInstance;*/
        
        Position_Geography__c posGeo = CallPlanTestDataFactory.createPosGeography(geo,territory,territoryTeamInstance,zipBasedTeamInstance);
        test.startTest();
        insert posGeo;
        posGeo.Position__c = destTerr.id;
        update posGeo;
		test.stopTest();	
	}
		
 	static testMethod void testPositionGeographyDelete(){
		
		TriggerContol__c  triggerctrl =  new TriggerContol__c(IsStopTrigger__c=false , Name='PositionGeographyTrigger');
		insert triggerctrl;
		
		Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11778');
		insert geo;
		
		Team__c zipBasedTeam =  CallPlanTestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_GeoBased);    	
    	insert zipBasedTeam;
    	
		Team_Instance__c zipBasedTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(zipBasedTeam,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP);
    	insert zipBasedTeamInstance;
    	
    	Position__c region = CallPlanTestDataFactory.createRegionPosition(zipBasedTeam);
		insert region;
		
		Position_Team_Instance__c regionTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(region, null, zipBasedTeamInstance);
		regionTeamInstance.X_Max__c=-72.6966429900;
        regionTeamInstance.X_Min__c=-73.9625820000;
        regionTeamInstance.Y_Max__c=40.9666490000;
        regionTeamInstance.Y_Min__c=40.5821279800;
		insert regionTeamInstance;
		
		Position__c district = CallPlanTestDataFactory.createPositionParent(region,zipBasedTeam);
		insert district;
		
		Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, region, zipBasedTeamInstance);
		districtTeamInstance.X_Max__c=-72.6966429900;
        districtTeamInstance.X_Min__c=-73.9625820000;
        districtTeamInstance.Y_Max__c=40.9666490000;
        districtTeamInstance.Y_Min__c=40.5821279800;
		insert districtTeamInstance;
		
		Position__c territory =  CallPlanTestDataFactory.createPosition(district,zipBasedTeam);
		territory.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
		insert territory;
		
		Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, district, zipBasedTeamInstance);
		territoryTeamInstance.X_Max__c=-72.6966429900;
        territoryTeamInstance.X_Min__c=-73.9625820000;
        territoryTeamInstance.Y_Max__c=40.9666490000;
        territoryTeamInstance.Y_Min__c=40.5821279800;
		insert territoryTeamInstance;
		
		Geography__c geo1 = CallPlanTestDataFactory.createGeography('Standard',null,'11778');
		insert geo1;	

        /*Team_Instance_Geography__c geographyTeamInstance = CallPlanTestDataFactory.CreateTeamInstanceGeoGraphy(geo,zipBasedTeamInstance);
        insert geographyTeamInstance;*/
        
        Position_Geography__c posGeo = CallPlanTestDataFactory.createPosGeography(geo,territory,territoryTeamInstance,zipBasedTeamInstance);
        Position_Geography__c posGeo1 = CallPlanTestDataFactory.createPosGeography(geo1,territory,territoryTeamInstance,zipBasedTeamInstance);
                
        test.startTest();
        insert posGeo;
        insert posGeo1;
        delete posGeo1;
		test.stopTest();	
	}   
}