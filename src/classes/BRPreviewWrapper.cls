public class BRPreviewWrapper{
    @AuraEnabled
    public string dataSetName{get;set;}
    @AuraEnabled
    public string tableName{get;set;}
    @AuraEnabled
    public string lastModifiedBy{get;set;}
    @AuraEnabled
    public string createdBy{get;set;}
    @AuraEnabled
    public string numberOfRows{get;set;}
    @AuraEnabled
    public string numberOfColumns{get;set;}
    
    public BRPreviewWrapper(string dataSetName, string tableName, string lastModifiedBy,string createdBy,string numberOfRows,string numberOfColumns){
        this.dataSetName = dataSetName;
        this.tableName = tableName;
        this.lastModifiedBy = lastModifiedBy;
        this.createdBy = createdBy;
        this.numberOfRows = numberOfRows;
        this.numberOfColumns = numberOfColumns;
    }
}