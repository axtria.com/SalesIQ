public interface CPGParserHelper {
     void GetExpressionForComputation(CPGExpressionWrapper exprwrapper,String expression,string derivedFieldName);
     void GetExpressionForCategory(CPGExpressionWrapper exprwrapper, String businessRuleId); 
     void GetExpressionForRank(CPGExpressionWrapper exprwrapper,string partitionName,List<Rank_Order__c> lstRankOrder
                               ,string derivedFieldName,List<Data_Set_Column_Detail__c> lstDataSetColumnDetails );
     void GetExpressionForAggregate(CPGExpressionWrapper exprwrapper
                                    ,List<Data_Set_Column_Detail__c> lstDataSetColumnDetails
                                       ,List<Aggregate_Rule_Level__c> lstAggrRuleLevel
                                       ,List<Aggregate_Rule_Detail__c> lstAggrRuleDetails); 
     void GetExpressionForFilter(CPGExpressionWrapper exprwrapper,List<Data_Set_Column_Detail__c> lstDataSetColumnDetails
                                 ,List<Filter_Rule_Entity_Detail__c> lstFilterRuleEntityDetail);
}