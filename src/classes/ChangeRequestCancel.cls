public class ChangeRequestCancel {
   private ApexPages.StandardSetController standardController;
    public String comments{get;set;}
    public boolean showError{get;set;}
    public String message {get;set;} 

    public ChangeRequestCancel (ApexPages.StandardSetController standardController)
    {
        this.standardController = standardController;
        this.showError = False;
    }

    public PageReference cancelRequest()
    {       
        // Get the selected records (optional, you can use getSelected to obtain ID's and do your own SOQL)
        List<Change_request__c> obj = (List<Change_request__c>) standardController.getSelected();
        List<Change_request__c> selectedCases = [select Id, Approver1__c, Approver2__c, Approver3__c, Approver4__c,
        Approver5__c, Status__c from Change_request__c where id IN : obj];
        String currentUser = UserInfo.getUserId();
        Integer check = 3;
        
             
        List<CR_Queue__c> crqList = new List<CR_Queue__c>();
        // Update records      
        if(selectedCases.size() < 11)
        { 
            for(Change_request__c cr1: selectedCases)
            {
                check = 1;

                if (cr1.Status__c != 'Pending')
                {
                    check = 4;
                    break;
                }
                
                List<ProcessInstance> proList = [SELECT SubmittedById FROM ProcessInstance WHERE TargetObjectId =: cr1.Id LIMIT 1];
                /*List<ProcessInstance> proList = [SELECT (select actor.id from Workitems) FROM ProcessInstance where TargetObjectId =: cr1.Id];
                for(ProcessInstance pi:proList)
                { 
                    list<ProcessInstanceWorkitem> lst = pi.Workitems;
                           
                    System.debug('ites r '+ lst );
                    for (ProcessInstanceWorkitem piw:lst)
                    {
                        System.debug('piw val' + piw.ActorId);
                        if (piw.ActorId == currentUser)
                        {
                            check = 1;
                            break;
                        }
                    }
               
                }*/
                if (proList[0].SubmittedById != currentUser)
                {
                    check = 0;
                    break;
                }

                List<CR_Queue__c> crPending = [SELECT Id, Name FROM CR_Queue__c where Name =: cr1.Id];
                
                if(crPending.size() > 0)
                {
                    check = 2;
                    break;
                }

                if (check == 1)
                {
                    CR_Queue__c cr = new CR_Queue__c(Name = cr1.Id, Status__c='Pending', Action__c = 'Cancelled');
                    crqList.add(cr);
                }
                else
                {
                    break;
                }
                
            }  
        }
        if(check == 1)
        {
            insert crqList;
        }   
        else if(check == 0)
        {
            //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'You don’t have enough permission to approve, cancel or reject the request(s).');
            //ApexPages.addMessage(myMsg);
            showError = True;
            setMessageParams(System.Label.Not_have_permission_to_approve_cancel_reject);
            System.debug('showError value : ' + showError);
            return null;
        } 
        else if(check == 2)
        {
            //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'The request(s) has been scheduled to be processed. Please check again later.');
            //ApexPages.addMessage(myMsg);
            showError = True;
            setMessageParams(System.Label.Request_Schedule_for_Process);

            System.debug('showError value : ' + showError);
            return null;
        } 
        else if(check == 3)
        {
            //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'We cannot approve, cancel or reject more than 10 request at the same time.');
            //ApexPages.addMessage(myMsg);
            showError = True;
            setMessageParams(System.Label.Cannot_greater_than_ten);

            System.debug('showError value : ' + showError);
            return null;
        }
        else if(check == 4)
        {
            showError = True;
            setMessageParams(System.Label.Already_Approve_Cancel_Reject);
            System.debug('showError value : ' + showError);
            return null;
        }

        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf(system.now().hour());
        String minute = string.valueOf(system.now().minute());
        String second = string.valueOf(system.now().second()+60);
        String year = string.valueOf(system.now().year());
        Integer min =Integer.valueOf(minute);
        Integer hourvar=Integer.valueOf(system.now().hour());
        Integer sec=Integer.valueOf(second);

        if(sec>59){
            sec=sec-60;
            min=min+1;
        }
        second=string.valueOf(sec);
        minute=string.valueOf(min);
        if(min>59){
            Integer x=min-60;
            minute=string.valueOf(x);
            Integer hour1=(hourvar+1);
            hour=string.valueOf(hour1);
            if(Integer.valueof(hour)>=24){
            minute=string.valueOf(0);
            hour=string.valueOf(0);
            }
        }
        String strJobName = 'ProcessBulk-' + second + '_' + minute + '_' + hour + '_' + day + '_' + month + '_' + year;
        String strSchedule = second+ ' ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
        if(!test.isRunningTest()){
            System.schedule(strJobName, strSchedule, new ProcessBulkCRs('Cancel', comments));
        }
        PageReference pageRef = new PageReference('/a0H');
        pageRef.setRedirect(true);
        return pageRef; 
    }

    public PageReference updateCases()
    {       
        // Call StandardSetController 'save' method to update (optional, you can use your own DML)
       return standardController.save();   
        
    }

    public void setMessageParams(String messageText){
        message = messageText;
    }
}