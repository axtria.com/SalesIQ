@istest
public with sharing class TestValidateExistingSharedZIPTrigger{
	static testMethod void validateCurrentTeamInsatnce(){

		TriggerContol__c  triggerctrl =  new TriggerContol__c(IsStopTrigger__c=false , Name='ValidateExistingSharedZIPTrigger');
		insert triggerctrl;

		Team__c objTeam = new Team__c(Name='HTN',Type__c=SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP ,Effective_End_Date__c=Date.today().addMonths(50));
        insert objTeam;

        Team_Instance__c objCurrentTeamInstance = new Team_Instance__c();
        objCurrentTeamInstance.Name = 'HTN_Q1_2016';
        objCurrentTeamInstance.Alignment_Period__c = 'Current';
        objCurrentTeamInstance.Team__c = objTeam.id;
        objCurrentTeamInstance.Alignment_Type__c = SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP;
        objCurrentTeamInstance.isActiveCycle__c = 'Y';        
        objCurrentTeamInstance.IC_EffstartDate__c=Date.Today();
        objCurrentTeamInstance.Restrict_ZIP_Share__c = true;
        insert objCurrentTeamInstance;
        

        test.startTest();
        try{
        	objCurrentTeamInstance.Restrict_ZIP_Share__c = false;
         	update objCurrentTeamInstance;

        }catch(exception ex){
        	system.assert(true);

        }       

        test.stopTest();

	}

	static testMethod void validateFutureTeamInstance(){

		TriggerContol__c  triggerctrl =  new TriggerContol__c(IsStopTrigger__c=false , Name='ValidateExistingSharedZIPTrigger');
		insert triggerctrl;

		Team__c objTeam = new Team__c(Name='HTN',Type__c=SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP ,Effective_End_Date__c=Date.today().addMonths(50));
        insert objTeam;

        Team_Instance__c objFutureTeamInstance = new Team_Instance__c();
        objFutureTeamInstance.Name = 'HTN_Q1_2016';
        objFutureTeamInstance.Alignment_Period__c = 'Future';
        objFutureTeamInstance.Team__c = objTeam.id;
        objFutureTeamInstance.Alignment_Type__c = SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP;
        objFutureTeamInstance.isActiveCycle__c = 'Y';        
        objFutureTeamInstance.IC_EffstartDate__c=Date.today().addMonths(1);
        objFutureTeamInstance.IC_EffEndDate__c=Date.today().addYears(1);
        objFutureTeamInstance.Restrict_ZIP_Share__c = false;
        insert objFutureTeamInstance;

        Geography__c geo1 = new Geography__c();
        geo1.Name = '11788';
        geo1.City__c='Test 11788';
        geo1.State__c = '11788';
        geo1.Neighbor_Geography__c = '11789';
        geo1.Zip_Type__c = 'Standard';
        geo1.Centroid_Latitude__c = 0.0;
        geo1.Centroid_Longitude__c = 0.0;
        insert geo1; 

        /*Team_Instance_Geography__c teamInsGeo1 = new Team_Instance_Geography__c();
        teamInsGeo1.Geography__c = geo1.id;
        teamInsGeo1.Team_Instance__c = objFutureTeamInstance.id;
        teamInsGeo1.Effective_Start_Date__c = Date.today().addMonths(1);
        teamInsGeo1.Effective_End_Date__c = Date.today().addYears(1);
        teamInsGeo1.Metric1__c  =2.010000000000;
        teamInsGeo1.Metric2__c  =29.000000000000;
        teamInsGeo1.Metric3__c  =2.000000000000;
        teamInsGeo1.Metric4__c  =1.431479545000;
        teamInsGeo1.Metric5__c  =1.490000000000;
        teamInsGeo1.Metric6__c  =2.010000000000;
        teamInsGeo1.Metric7__c  =29.000000000000;
        teamInsGeo1.Metric8__c  =2.000000000000;
        teamInsGeo1.Metric9__c  =1.431479545000;
        teamInsGeo1.Metric10__c =1.490000000000;
        insert teamInsGeo1;*/

        Position__c destTerr = new Position__c();
        destTerr.Name = 'Long Island East';
        destTerr.Client_Territory_Name__c = 'Long Island East';
        destTerr.Client_Position_Code__c = '1NE30012';
        destTerr.Client_Territory_Code__c='1NE30012';
        destTerr.Position_Type__c='Territory';
        destTerr.inactive__c = false;
        destTerr.RGB__c = '41,210,117';
        destTerr.Team_iD__c = objTeam.id;
        //destTerr.Parent_Position__c = district.Id;
        destTerr.Hierarchy_Level__c = '1';
        destTerr.Related_Position_Type__c  ='Base';
        destTerr.Effective_End_Date__c=Date.today().addMonths(50);
        insert destTerr;

        Position_Team_Instance__c destTerrTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(destTerr, null, objFutureTeamInstance);
        destTerrTeamInstance.X_Max__c=-72.6966429900;
        destTerrTeamInstance.X_Min__c=-73.9625820000;
        destTerrTeamInstance.Y_Max__c=40.9666490000;
        destTerrTeamInstance.Y_Min__c=40.5821279800;
        insert destTerrTeamInstance;  

        Position__c destTerr1 = new Position__c();
        destTerr1.Name = 'Long Island West';
        destTerr1.Client_Territory_Name__c = 'Long Island West';
        destTerr1.Client_Position_Code__c = '1NE30013';
        destTerr1.Client_Territory_Code__c='1NE30013';
        destTerr1.Position_Type__c='Territory';
        destTerr1.inactive__c = false;
        destTerr1.RGB__c = '41,210,117';
        destTerr1.Team_iD__c = objTeam.id;
        //destTerr.Parent_Position__c = district.Id;
        destTerr1.Hierarchy_Level__c = '1';
        destTerr1.Related_Position_Type__c  ='Base';
        destTerr1.Effective_End_Date__c=Date.today().addMonths(50);
        insert destTerr1;   

        Position_Team_Instance__c destTerrTeamInstance1 = CallPlanTestDataFactory.CreatePosTeamInstance(destTerr1, null, objFutureTeamInstance);
        destTerrTeamInstance1.X_Max__c=-72.6966429900;
        destTerrTeamInstance1.X_Min__c=-73.9625820000;
        destTerrTeamInstance1.Y_Max__c=40.9666490000;
        destTerrTeamInstance1.Y_Min__c=40.5821279800;
        insert destTerrTeamInstance1;  
   

        Position_Geography__c posGeo1  = new Position_Geography__c();
        posGeo1.Effective_Start_Date__c  = Date.today().addMonths(1);
        posGeo1.Effective_End_Date__c  = Date.today().addYears(1);
        posGeo1.IsShared__c = true;  
        posGeo1.SharedWith__c = 'Long Island West';
        posGeo1.Geography__c = geo1.id;     
        posGeo1.Position__c = destTerr.id;
        posGeo1.Team_Instance__c = objFutureTeamInstance.id ;
        //posGeo1.Team_Instance_Geography__c = teamInsGeo1.id;
        posGeo1.Position_Team_Instance__c = destTerrTeamInstance.id;
        posGeo1.Change_Status__c = 'No Change';
        posGeo1.Metric1__c  =2.010000000000;
        posGeo1.Metric2__c  =29.000000000000;
        posGeo1.Metric3__c  =2.000000000000;
        posGeo1.Metric4__c  =1.431479545000;
        posGeo1.Metric5__c  =1.490000000000;
        posGeo1.Metric6__c  =2.010000000000;
        posGeo1.Metric7__c  =29.000000000000;
        posGeo1.Metric8__c  =2.000000000000;
        posGeo1.Metric9__c  =1.431479545000;
        posGeo1.Metric10__c =1.490000000000;
        insert posGeo1;

        Position_Geography__c posGeo2  = new Position_Geography__c();
        posGeo2.Effective_Start_Date__c  = Date.today().addMonths(1);
        posGeo2.Effective_End_Date__c  = Date.today().addYears(1);
        posGeo2.Geography__c = geo1.id;
        posGeo2.IsShared__c = true;   
        posGeo1.SharedWith__c = 'Long Island East';
        posGeo2.Position__c = destTerr1.id;
        posGeo2.Team_Instance__c = objFutureTeamInstance.id ;
        //posGeo2.Team_Instance_Geography__c = teamInsGeo1.id;
        posGeo2.Position_Team_Instance__c = destTerrTeamInstance1.id;
        posGeo2.Change_Status__c = 'No Change';
        posGeo2.Metric1__c  =2.010000000000;
        posGeo2.Metric2__c  =29.000000000000;
        posGeo2.Metric3__c  =2.000000000000;
        posGeo2.Metric4__c  =1.431479545000;
        posGeo2.Metric5__c  =1.490000000000;
        posGeo2.Metric6__c  =2.010000000000;
        posGeo2.Metric7__c  =29.000000000000;
        posGeo2.Metric8__c  =2.000000000000;
        posGeo2.Metric9__c  =1.431479545000;
        posGeo2.Metric10__c =1.490000000000;
        insert posGeo2;

        test.startTest();
        try{
        	
        	objFutureTeamInstance.Restrict_ZIP_Share__c = true;
         	update objFutureTeamInstance;

        }catch(exception ex){
        	system.assert(true);

        }       

        test.stopTest();

	}

}