/**
* Description   : Batch class to update isShared and SharedWith on Position Geography 
* Author        : Gaurav 
* Constructor 1 Parameters : Team Instance ID (String)and Boolean varibale if shared geos exceed more than 2000
* Constructor 2 Parameters : Team Instance ID (String) 
* Date        : 23-May-2018
*/

global class UpdatePositionGeographySharedBatch implements Database.Batchable<sObject> ,Database.Stateful{
        
    // This variable will remain stateful throughout batch execution to maintain Account Ids with their list of Position Accounts.
    global map<ID,list<Position_Geography__c>>  mapOfGeographyIdAndPositionGeography = new map<ID,list<Position_Geography__c>>();
    list<string> updatedPositionGeographyIds = new list<string>();
    global String query;
    string teamInstanceId;
    list<string> geoIDs;
    list<string> geoNames;
    private map<String,Position_Geography__c> toBeUpdatedPosGeoMap;
    set<string> sharedPositions ;
    set<Id> geographyIds;

    //Below two variable used for storing the info when Bulk movement takes place
    boolean isBulkMovement;
    set<id> changeReqId;
     
    public UpdatePositionGeographySharedBatch (string teamInstance,Boolean higherNumberOfRecords){
        isBulkMovement = false;
        teamInstanceId = teamInstance;
        geoIDs = new list<string>();
        sharedPositions = new set<string>();
        geographyIds = new set<Id>();
        toBeUpdatedPosGeoMap = new map<String,Position_Geography__c>();
        query = ''; 
        string namespace = SalesIQGlobalConstants.getOrgNameSpace();
        list<AggregateResult> Results = [select Geography__c, count(ID) from Position_Geography__c where Team_Instance__c =: teamInstanceId group by Geography__c having count(ID)>1 limit 2000];
        system.debug('######### Aggregate Result ' + Results);
        for(AggregateResult sharedGeos : Results){
            system.debug('########## geography id ' + sharedGeos.get(namespace+'Geography__c'));
            if(sharedGeos.get(namespace+'Geography__c') != null){
                geoIDs.add(string.valueOf(sharedGeos.get(namespace+'Geography__c')));
            }
        }
        system.debug('########## geoIDs ' + geoIDs);
        query = 'select id,SharedWith__c,IsShared__c,Position__c,Geography__c,Position__r.Name from Position_Geography__c where Team_Instance__c =: teamInstanceId and Geography__c in: geoIDs and Assignment_Status__c != \''+ SalesIQGlobalConstants.POSITION_GEOGRAPHY_INACTIVE+'\' and Position__r.Related_Position_Type__c != \''+ SalesIQGlobalConstants.TEAM_TYPE_BASE+'\'';
    }
     
    public UpdatePositionGeographySharedBatch (string teamInstance){
        teamInstanceId = teamInstance;
        toBeUpdatedPosGeoMap = new map<String,Position_Geography__c>();
        sharedPositions = new set<string>();
        query = 'select id,SharedWith__c,IsShared__c,Position__c,Geography__c,Position__r.Name from Position_Geography__c where Team_Instance__c =: teamInstanceId and  Assignment_Status__c != \''+ SalesIQGlobalConstants.POSITION_GEOGRAPHY_INACTIVE+'\' and Position__r.Related_Position_Type__c != \''+ SalesIQGlobalConstants.TEAM_TYPE_BASE+'\'';

    }
    
    public UpdatePositionGeographySharedBatch (string teamInstance,list<String> geos){
    	system.debug('######## UpdatePositionGeographySharedBatch called');
        isBulkMovement = false;
        teamInstanceId = teamInstance;
        toBeUpdatedPosGeoMap = new map<String,Position_Geography__c>();
        sharedPositions = new set<string>();
        geoNames =new list<string>();
        geoIDs = new list<string>();
        sharedPositions = new set<string>();
        geographyIds = new set<Id>();
        toBeUpdatedPosGeoMap = new map<String,Position_Geography__c>();
        query = ''; 
        string namespace = SalesIQGlobalConstants.getOrgNameSpace();
        if(geos != null && geos.size() > 0){
        	geoNames.addAll(geos);
            system.debug('#########geoNames'+geoNames);
        	query = 'select id,SharedWith__c,IsShared__c,Position__c,Geography__c,Position__r.Name from Position_Geography__c where Team_Instance__c =: teamInstanceId and Geography__r.Name in: geoNames and  Assignment_Status__c != \''+ SalesIQGlobalConstants.POSITION_GEOGRAPHY_INACTIVE+'\' and Position__r.Related_Position_Type__c = \''+ SalesIQGlobalConstants.TEAM_TYPE_BASE+'\'';
        }
    }

    public UpdatePositionGeographySharedBatch (string teamInstance,set<id> crID, boolean bulkMovement){
        changeReqId = new set<id>();
        changeReqId.addAll(crID);
        isBulkMovement = bulkMovement;
        geoIDs = new list<string>();
        teamInstanceId = teamInstance;
        sharedPositions = new set<string>();
        geographyIds = new set<Id>();
        toBeUpdatedPosGeoMap = new map<String,Position_Geography__c>();


        set<id> setGeoID = new set<id>();//this id will store the Geography SF id from CR_geography object, so that those ids can be use to get records from Pos geo later.
        list<CR_Geography__C> lstCRGeoGraphy = [select id,Geography__c from CR_Geography__C where Change_Request__c in:crID limit 50000];
        system.debug('lstCRGeoGraphy '+lstCRGeoGraphy);
        for(CR_Geography__C crGeo: lstCRGeoGraphy){
            geoIDs.add(crGeo.Geography__c);

        }
        system.debug('teamInstance '+teamInstance);
        system.debug('geoIDs '+geoIDs);
        system.debug('teamInstanceId '+teamInstanceId);

        query = 'select id,SharedWith__c,IsShared__c,Position__c,Geography__c,Position__r.Name from Position_Geography__c where Team_Instance__c =: teamInstanceId and Geography__c in: geoIDs and IsShared__c=true and Assignment_Status__c != \''+ SalesIQGlobalConstants.POSITION_GEOGRAPHY_INACTIVE+'\' and Position__r.Related_Position_Type__c = \''+ SalesIQGlobalConstants.TEAM_TYPE_BASE+'\'';
        system.debug('query is '+query);



    }
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        system.debug('query ' + query);
        return Database.getQueryLocator(query);
    }
     
    public void Execute (Database.BatchableContext bc , list<Position_Geography__c> scope){
        system.debug('######## scope ' + scope);
        for(Position_Geography__c posGeo : scope){
            if(!string.isBlank(posGeo.SharedWith__c)){
                sharedPositions.addAll(posGeo.SharedWith__c.split(';'));
            }
            sharedPositions.add(posGeo.Position__r.Name);
            geographyIds.add(posGeo.Geography__c);
        }
        system.debug('#### sharedPositions : '+sharedPositions);
        system.debug('#### geographyIds: '+geographyIds);
        
        // Find active shared assignments for selected geographies
        map<string,list<Position_Geography__c>> sharedAssignmentMap = new map<string,list<Position_Geography__c>>();
        for(Position_Geography__c pa : [SELECT Id, Position__r.Name, Geography__c FROM Position_Geography__c WHERE Position__r.Name IN : sharedPositions AND 
                                      Geography__c IN : geographyIds AND Assignment_Status__c !=: SalesIQGlobalConstants.POSITION_GEOGRAPHY_INACTIVE AND 
                                      Team_Instance__c =: teamInstanceId]){
            list<Position_Geography__c> temp = sharedAssignmentMap.get(pa.Position__r.Name);
            if(temp == null){
                temp = new list<Position_Geography__c>();
            }
            temp.add(pa);
            sharedAssignmentMap.put(pa.Position__r.Name,temp);
        }
        system.debug('#### sharedAssignmentMap : '+sharedAssignmentMap);

        /*In this loop we are updating current record as well as records that we have maintained inside the stateful map.*/
        for(Position_Geography__c posGeo : scope){
            system.debug('#### posGeo : '+posGeo.Id);
            system.debug('#### mapOfGeographyIdAndPositionGeography : '+mapOfGeographyIdAndPositionGeography);
            if(mapOfGeographyIdAndPositionGeography.containsKey(posGeo.Geography__c)){
                list<Position_Geography__c> lsposGeo = mapOfGeographyIdAndPositionGeography.get(posGeo.Geography__c);               
                for(Position_Geography__c pGeo : lsposGeo){
                    //RR : Remove position from pGeo.SharedWith__c if no active assignment found
                	string sharedWithStr = '';
                    if(!string.isBlank(pGeo.SharedWith__c)){
                        for(string pos : pGeo.SharedWith__c.split(';')){
                        	system.debug('#### shared pos 1 '+ pos);
                            if(sharedAssignmentMap.containsKey(pos)){
                                sharedWithStr += pos+';';
                            }
                        }
                        sharedWithStr = sharedWithStr.removeEnd(';');
                        pGeo.SharedWith__c = sharedWithStr;
                        system.debug('#### pGeo.SharedWith__c : '+pGeo.SharedWith__c);
                        system.debug('#### sharedWithStr : '+sharedWithStr);
                    }
                    
                    //Update pGeo.SharedWith__c
                    if(!string.isBlank(pGeo.SharedWith__c) && !pGeo.SharedWith__c.contains(posGeo.Position__r.Name)){                  
                        pGeo.SharedWith__c += ';'+posGeo.Position__r.Name;
                    }else{
                        pGeo.SharedWith__c = posGeo.Position__r.Name;
                    }
                    system.debug('#### pGeo.SharedWith__c : '+pGeo.SharedWith__c);
                    system.debug('#### sharedWithStr : '+sharedWithStr);

                    pGeo.IsShared__c = true;
                    toBeUpdatedPosGeoMap.put(pGeo.ID,pGeo);
                    
                    //RR : Remove position from posGeo.SharedWith__c if no active assignment found
                    sharedWithStr = '';
                    if(!string.isBlank(posGeo.SharedWith__c)){
                        for(string pos : posGeo.SharedWith__c.split(';')){
                        	system.debug('#### shared pos : '+pos);
                            if(sharedAssignmentMap.containsKey(pos)){
                                sharedWithStr += pos+';';
                            }                            
                        }
                        sharedWithStr = sharedWithStr.removeEnd(';');
                        posGeo.SharedWith__c = sharedWithStr;
                        system.debug('#### posGeo.SharedWith__c : '+posGeo.SharedWith__c);
                        system.debug('#### sharedWithStr : '+sharedWithStr);
                    }

                    if(!string.isBlank(posGeo.SharedWith__c) && !posGeo.SharedWith__c.contains(pGeo.Position__r.Name)){
                        posGeo.SharedWith__c += ';'+pGeo.Position__r.Name;
                    }else{
                        posGeo.SharedWith__c = pGeo.Position__r.Name;
                    }
                    posGeo.IsShared__c = true;
                    toBeUpdatedPosGeoMap.put(posGeo.ID,posGeo);
                }
                system.debug('############ toBeUpdatedPosGeoMap ' + toBeUpdatedPosGeoMap);
                mapOfGeographyIdAndPositionGeography.get(posGeo.Geography__c).add(posGeo);
            }else{
                mapOfGeographyIdAndPositionGeography.put(posGeo.Geography__c,new list<Position_Geography__c>{posGeo});
            }
            system.debug('############ mapOfGeographyIdAndPositionGeography ' + mapOfGeographyIdAndPositionGeography);
        }
        updatedPositionGeographyIds.addAll(toBeUpdatedPosGeoMap.keySet());
        update toBeUpdatedPosGeoMap.values();
    }
     
    public void Finish(Database.BatchableContext bc){
        system.debug('######### Batch Execution finished ' + updatedPositionGeographyIds);
        //Below condition will execute only when lighting mode is on, because in that cae we need to update CR status to complete
        if(isBulkMovement){
            //SalesIQUtility.getChangeRequestbyId(changeReqId);
        }
    }
}