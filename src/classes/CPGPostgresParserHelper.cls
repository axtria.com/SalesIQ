public class CPGPostgresParserHelper implements CPGParserHelper {
    static string dataType='';
        
    public void GetExpressionForComputation(CPGExpressionWrapper exprwrapper,string expression,string derivedFieldName){
       // exprwrapper.selectExpression=derivedFieldName+' = '+expression;
       System.debug('expression-->'+expression);
        if(expression!='' && expression!=null){
           expression= CPGParserHelperComputationUtility.GetExecuteExpression(expression) ;
        }
      exprwrapper.selectExpression=expression.replace('.','_') ;
      exprwrapper.columnalias=derivedFieldName.replace('.','_');
   }
    
    
    
    public void GetExpressionForCategory(CPGExpressionWrapper exprwrapper, String businessRuleId){
       // exprwrapper.selectExpression =  CPGCategoryHelperParser.CreateCategoryParser(businessRuleId);
       // exprwrapper.columnaliasCategory =  CPGCategoryHelperParser.columnAliasToBePassed(businessRuleId);
       exprwrapper.expressionCategory= CPGCategoryHelperParser.CreateCategoryParser(businessRuleId);
       
    }
    public void GetExpressionForRank(CPGExpressionWrapper exprwrapper,string partitionName,List<Rank_Order__c> lstRankOrder
                                     ,string derivedFieldName,List<Data_Set_Column_Detail__c> lstDataSetColumnDetails){
        string query='rank() over(partition by ';
        query+=GetTableColumn(partitionName,lstDataSetColumnDetails)+' order by ';
        integer count=0;
        for(Rank_Order__c rankOrder:lstRankOrder){
            system.debug(' rankOrder.Order_Clause__c-->'+ rankOrder.Order_Field__c);
            query+=GetTableColumn(rankOrder.Order_Field__c,lstDataSetColumnDetails) + ' '+ rankOrder.Order_Clause__c +' '; 
            if(count<lstRankOrder.size()-1){
                query+=', ';
            }
             ++count;
        }
        query+=')';
        
         exprwrapper.selectExpression=query.replace('.','_');
         exprwrapper.columnalias=derivedFieldName.replace('.','_');
    }
    
    public void GetExpressionForAggregate(CPGExpressionWrapper exprwrapper,List<Data_Set_Column_Detail__c> lstDataSetColumnDetails,List<Aggregate_Rule_Level__c> lstAggrRuleLevel,List<Aggregate_Rule_Detail__c> lstAggrRuleDetails){
     string query='';
     string groupByColumn='';
     string aliasColumn=''; 
     string function=''; 
     integer count1=0;
     integer count2=0;
     string groupByExpression=''; 
     System.debug('lstDataSetColumnDetails inside aggregate-->'+lstDataSetColumnDetails);   
     for(Aggregate_Rule_Level__c aggrRuleLevel:lstAggrRuleLevel){
           groupByColumn= GetTableColumn(aggrRuleLevel.group_field__c,lstDataSetColumnDetails);  
           //groupByColumn=aggrRuleLevel.group_field__c;
           aliasColumn=groupByColumn;
           query=query+' '+groupByColumn+ ' as ' +aliasColumn+',';
           groupByExpression=groupByExpression+groupByColumn;
           if(count1<lstAggrRuleLevel.size()-1){
                groupByExpression+=',';
            }
             ++count1; 
     }    
     for(Aggregate_Rule_Detail__c aggrRuleDetail:lstAggrRuleDetails){
           system.debug('Id-->'+aggrRuleDetail.Id);
           System.debug('aggrRuleDetail.Aggregate_Field__c-->'+aggrRuleDetail.Aggregate_Field__c);
           groupByColumn= GetTableColumn(aggrRuleDetail.Field_Name__c,lstDataSetColumnDetails); 
           System.debug('groupByColumn-->'+groupByColumn);
           //groupByColumn= aggrRuleDetail.Aggregate_Field__c;
           aliasColumn=aggrRuleDetail.Aggregate_Field__c.replace(' ','_').replace('#','total').toLowerCase();
           function=aggrRuleDetail.Aggregate_Function__c;
           query=query+function+'('+ groupByColumn+')'+' as '+aliasColumn ;
           if(count2<lstAggrRuleDetails.size()-1){
                query+=' ,';
            }
             ++count2;
     }
       
      exprwrapper.groupByExpression=groupByExpression.replace('.','_');
      exprwrapper.selectExpression=query.replace('.','_');  
                                              
        
    }
    
    public void GetExpressionForFilter(CPGExpressionWrapper exprwrapper,List<Data_Set_Column_Detail__c> lstDataSetColumnDetails
    ,List<Filter_Rule_Entity_Detail__c> lstFilterRuleEntityDetail){
        string selectExpression='';
        string whereExpression='';
        integer ctrColumnDetails=0;
        integer ctrEntityDetails=0;
        string parentEntityId='';
        System.debug('lstDataSetColumnDetails inside filter-->'+lstDataSetColumnDetails);
        for(Data_Set_Column_Detail__c columnDetail:lstDataSetColumnDetails){
            selectExpression=selectExpression+columnDetail.tb_col_nm__c;
            if(ctrColumnDetails<lstDataSetColumnDetails.size()-1){
                selectExpression=selectExpression+',';
            }
            ++ctrColumnDetails;
        }
         
        for(Filter_Rule_Entity_Detail__c entityDetail:lstFilterRuleEntityDetail){
               
                whereExpression=entityDetail.Filter_Exec_expression__c;
                
        }
        exprwrapper.whereExpression=whereExpression;
        exprwrapper.selectExpression=selectExpression.replace('.','_');
        
   }
    
    public static string GetTableColumn(string column,List<Data_Set_Column_Detail__c> lstDataSetColumnDetails){
       string  tableColumnName;
         for(Data_Set_Column_Detail__c datasetColumnDetails:lstDataSetColumnDetails){
                    if(datasetColumnDetails.ds_col_name__c.toLowerCase()==column.toLowerCase()){
                        tableColumnName=datasetColumnDetails.tb_col_nm__c;
                        dataType=datasetColumnDetails.datatype__c;
                        break;
                    }
             }
        return tableColumnName;
    }
    
}
