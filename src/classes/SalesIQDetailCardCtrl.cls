public class SalesIQDetailCardCtrl 
{
	public static final String namespace = SalesIQGlobalConstants.Name_space;

	@AuraEnabled
    public static SalesIQDetailCardWrapper getRecordData(String recordId, String objectName, String isRelated, String referenceField, String interfaceName) 
    {

    	objectName = namespace + objectName;
    	Map<String,SObjectField> globalSchema = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();

    	if(interfaceName == 'EMPLOYEE')
    		interfaceName = SalesIQGlobalConstants.MANAGE_ASSIGNMENT_EMPLOYEE_DETAILS;
    	else if(interfaceName == 'POSITION')
    		interfaceName = SalesIQGlobalConstants.MANAGE_ASSIGNMENT_POSITION_DETAILS;
    	else if(interfaceName == 'POSITION EMPLOYEE')
    		interfaceName = SalesIQGlobalConstants.MANAGE_ASSIGNMENT_POSITION_EMPLOYEE_DETAILS;

    	List<Team_Instance_Object_Attribute__c> cardDetailFields = SalesIQUtility.getTeamInstanceAttrFromInterfaceName(interfaceName);

    	String fields = getSOQL(new List<String>{'Name'},cardDetailFields);
    	List<String> fieldsList = fields.split(',');
    	List<String> labelsList = new List<String>();
    	Map<String,String> apiLabelMap = new Map<String,String>();
        Map<String,String> typeMap = new Map<String,String>();
        Map<String,String> formattedDataMap = new Map<String,String>();
        Map<String,Boolean> refMap = new Map<String,Boolean>();

    	String queryString = 'Select ';
        queryString = queryString + fields;

        List<String> dateFields = new List<String>();
        List<String> dateTimeFields = new List<String>();

    	
        for(Team_Instance_Object_Attribute__c attribute : cardDetailFields)
        {   
            labelsList.add(attribute.Attribute_Display_Name__c);
            apiLabelMap.put(attribute.Attribute_API_Name__c,attribute.Attribute_Display_Name__c);
            typeMap.put(attribute.Attribute_API_Name__c, attribute.Data_Type__c);

            if(attribute.Data_Type__c == Schema.DisplayType.REFERENCE.name())
            {
                refMap.put(attribute.Attribute_API_Name__c,true);
                if(attribute.Attribute_API_Name__c.indexOf('__r') != -1)
                    queryString = queryString + ',' + attribute.Attribute_API_Name__c.split('__r')[0] + '__c';
                else
                    queryString = queryString + ',' + attribute.Attribute_API_Name__c.split('\\.')[0] + 'Id';
            }
            else
            {
                refMap.put(attribute.Attribute_API_Name__c, false);
            }

            if(attribute.Data_Type__c == Schema.DisplayType.DATE.name())
            {
                dateFields.add(attribute.Attribute_API_Name__c);
            }
            else if(attribute.Data_Type__c == Schema.DisplayType.DATETIME.name())
            {
                dateTimeFields.add(attribute.Attribute_API_Name__c);
            }
        }

        if(isRelated == 'true' && interfaceName == SalesIQGlobalConstants.MANAGE_ASSIGNMENT_POSITION_EMPLOYEE_DETAILS)
        {
            if(queryString.indexOf('Employee__r.Name') == -1 && queryString.indexOf('Employee__c') == -1)
            {
                queryString += ',Employee__r.Name,Employee__c ';
            }
            if(queryString.indexOf('Position__r.Name') == -1 && queryString.indexOf('Position__c') == -1)
            {
                queryString += ',Position__r.Name,Position__c ';   
            }

            queryString = queryString + ' from ' + objectName +' where '+ referenceField +'=\'' + String.escapeSingleQuotes(recordId) + '\' AND Employee__c != null'  ;
        }    
        else
            queryString = queryString + ' from ' + objectName +' where Id=\'' + String.escapeSingleQuotes(recordId) + '\'';

        
        System.debug('==queryString==' + queryString);
        if(queryString != '' && queryString != null)
        {   

            if(queryString.indexOf('effective_end_date__c') != -1)
            {   
                queryString = queryString + ' ORDER BY effective_end_date__c DESC';
                System.debug('AKqueryString' + queryString);
            }
            
        }

        List<SObject> recordDetails = new List<SObject>();

        if(SecurityUtil.checkRead(Schema.GetGlobalDescribe().get(objectName), fieldsList, true))
        {
            recordDetails = Database.query(queryString);
        }    

        Map<Id, Map<String, String>> formattedDateMap = new Map<Id, Map<String, String>>();
        for(SObject record: recordDetails)
        {
            Map<String, String> tempMap = new Map<String, String>();
            if(dateFields.size() > 0)
            {
                
                for(String field : dateFields)
                {
                    if(record.get(field) != null){
                        String recDate = Date.valueOf(record.get(field)).format();
                        tempMap.put(field,recDate);
                    }
                }
            }
            if(dateTimeFields.size() > 0)
            {
                for(String field : dateTimeFields)
                {
                    if(record.get(field) != null){
                        String recDate = Datetime.valueOf(record.get(field)).format();
                        tempMap.put(field,recDate);
                    }
                }
            }
            formattedDateMap.put(String.valueOf(record.get('Id')),tempMap);
           
        }

    	SalesIQDetailCardWrapper wrapper = new SalesIQDetailCardWrapper();
        wrapper.formattedDateMap = formattedDateMap;
    	wrapper.recordDetails = recordDetails;
    	wrapper.apiLabelMap = apiLabelMap;
    	wrapper.apiNames = fieldsList;
    	wrapper.labelNames = labelsList;
        wrapper.refMap = refMap;
        wrapper.ns = namespace;
    	return wrapper;
        
    }    

    private static String getSOQL(List<String> mandatoryFields, List<Team_Instance_Object_Attribute__c> assignmentPopupFields) {
        Set<String> queryFields = new Set<String>();
        if(mandatoryFields != null && !mandatoryFields.isEmpty())
        {
        	for(String fieldName : mandatoryFields) 
        	{
            	queryFields.add(fieldName.trim().toLowerCase());
        	}
        }

        for(Team_Instance_Object_Attribute__c attribite : assignmentPopupFields) {
            queryFields.add(attribite.Attribute_API_Name__c.trim().toLowerCase());
        }

        return String.join(new List<String>(queryFields), ',') ;
    }
    
    @AuraEnabled
    public static String getPositionEditEnableCustomSetting(){
      ReadOnlyApplication__c readOnlyApplication = ReadOnlyApplication__c.getInstance(UserInfo.getProfileId());
        if(readOnlyApplication != null){
            if(readOnlyApplication.Disable_Edit_Position_ManageAssignment__c)
                return 'false';
            else
                return 'true';
        }else{
            return 'true';
        }
    }

    @AuraEnabled
    public static void logException(String message, String module) 
    {
        if(message.length() > 2000)
            message.substring(0,2000);
        SalesIQLogger.logErrorMessage(message, module);
    }
}