public class SalesIQDetailCardWrapper 
{	
	@AuraEnabled
	public List<SObject> recordDetails{get;set;}

	@AuraEnabled
	public Map<Id, Map<String, String>> formattedDateMap {get; set;}

	@AuraEnabled
	public Map<String,String> apiLabelMap{get;set;}

	@AuraEnabled
	public List<String> apiNames{get;set;}

	@AuraEnabled
	public List<String> labelNames{get;set;}

    @AuraEnabled
    public Map<String,Boolean> refMap{get;set;}

    @AuraEnabled
    public String ns{get;set;}
    
}