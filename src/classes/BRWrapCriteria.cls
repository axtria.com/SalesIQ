public class BRWrapCriteria {  
	@AuraEnabled
	public Boolean isDiscreet{get;set;}
	@AuraEnabled
	public Integer BlockSeq{get;set;} //BlockSeq        
	@AuraEnabled
	public Integer GroupSeq{get;set;} //GroupSeq        
	@AuraEnabled
	public Integer seqNo{get;set;} //criteria seq        
	@AuraEnabled
	//Selected criteria Operator
	public String criName {get; set;}                
	@AuraEnabled
	//criteriaOperator - AND OR
	public String[] criNameOptions {get; set;}       
	@AuraEnabled
	public list<BRWrapBlock.DataSetColumnDetails> fieldName {get; set;}        
	@AuraEnabled
	public String[] opVal {get; set;}        
	@AuraEnabled
	public String selOpVal {get; set;}        
	@AuraEnabled
	public String fieldVal {get; set;}  
	@AuraEnabled
	public String fieldType {get; set;}
	@AuraEnabled
	public String selValueType {get; set;}  
	@AuraEnabled
	public String valueType {get; set;}  
	@AuraEnabled
	public String selFieldName {get; set;}
	@AuraEnabled
	public String selColValue {get; set;}
	@AuraEnabled
	public list<BRWrapBlock.selectoption> optionType {get; set;}   

	public BRWrapCriteria parse(String json) {
		return (BRWrapCriteria) System.JSON.deserialize(json, BRWrapCriteria.class);
	}
}