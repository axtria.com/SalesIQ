/*
Author : Ankit Manendra
Description : Test class for CPGAggregationCtrl
Date : 6 Novomber, 2017

*/

@isTest
private class TestCPGAggregationCtrl{
    static testMethod void cpgAggrTest() {
    
        Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj;
        
        ComponentTypeMaster__c ComTypeMasObj = new ComponentTypeMaster__c();
        ComTypeMasObj.Name='Call Balancing';
        ComTypeMasObj.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj.DisplayIcon__c='add';
        ComTypeMasObj.DisplayIconClass__c='window information';
        insert ComTypeMasObj;

        Business_Rule_Template__c  busRuleTemObj = new Business_Rule_Template__c();
        busRuleTemObj.isActive__c=true;
        busRuleTemObj.isInternal__c=true;
        insert busRuleTemObj;
        
        BusinessRule_Template_Details__c busRuleTemDet = new BusinessRule_Template_Details__c();
        busRuleTemDet.is_Visible__c=true;
        busRuleTemDet.Business_Rule_Template_Id__c=busRuleTemObj.id;
        insert busRuleTemDet;
        
        BusinessRule_Template_Details__c busRuleTemDet1 = new BusinessRule_Template_Details__c();
        busRuleTemDet1.is_Visible__c=true;
        busRuleTemDet1.Business_Rule_Template_Id__c=busRuleTemObj.id;
        insert busRuleTemDet1;
        
        Config_Call_Balancing_Map__c callBalObj = new Config_Call_Balancing_Map__c();
        callBalObj.Business_Rule_Template_Details__c=busRuleTemDet.id;
        insert callBalObj;
        
        Config_Call_Balancing_Map__c callBalObj1 = new Config_Call_Balancing_Map__c();
        callBalObj1.Business_Rule_Template_Details__c=busRuleTemDet1.id;
        callBalObj1.Base_Config_Call_Balancing_Map_Id__c=callBalObj.id;
        insert callBalObj1;
        
        
        Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        insert scenarioObj;
        
        Config_Call_Balancing_Constraint_Detail__c configCallBalObj = new Config_Call_Balancing_Constraint_Detail__c();
        configCallBalObj.Config_Call_Balancing_Map_Id__c = callBalObj.id;
        insert configCallBalObj;
         
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        sceRuleInsDetObj.Component_Display_Order__c=4;
        sceRuleInsDetObj.Is_Visible__c=true;
        sceRuleInsDetObj.Component_Type_Master__c=ComTypeMasObj.id;
        sceRuleInsDetObj.BRT_Details_Id__c=busRuleTemDet.id;
        insert sceRuleInsDetObj;
        
        Data_Set__c dataSetObj = new Data_Set__c();
        dataSetObj.Name = 'Call Plan Summary';
        insert dataSetObj;
        
        Data_Set_Rule_Map__c dataSetRuleObj = new Data_Set_Rule_Map__c();
        dataSetRuleObj.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj.ds_type__c='I';
        dataSetRuleObj.Business_Rule_Template_Details__c=busRuleTemDet.id;
        dataSetRuleObj.dataset_id__c=dataSetObj.id;
        insert dataSetRuleObj;
        
        Data_Set_Rule_Map__c dataSetRuleObj1 = new Data_Set_Rule_Map__c();
        dataSetRuleObj1.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj1.ds_type__c='O';
        dataSetRuleObj1.dataset_id__c=dataSetObj.id;
        dataSetRuleObj1.Business_Rule_Template_Details__c=busRuleTemDet.id;
        insert dataSetRuleObj1;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj.Name='Aggregate';
        insert busRuleTypeMasObj;
        
        Business_Rules__c busRuleObj = new Business_Rules__c();
        busRuleObj.Name='Rank rule 278';
        busRuleObj.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObj.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        insert busRuleObj;
        
        Business_Rules__c busRuleObj1 = new Business_Rules__c();
        busRuleObj1.Name='Rank rule 278';
        busRuleObj1.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObj1.ScenarioRuleInstanceDetails__c=null;
        busRuleObj1.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObj1.Aggregate_Business_Rule_Template_Details__c='Test';
        insert busRuleObj1;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj.Param_Name__c='derived field 278';
        busRuleFieldObj.Business_Rule__c=busRuleObj.id;
        busRuleFieldObj.Param_Data_Type__c='Numeric';
        busRuleFieldObj.Param_Type__c='Derived';
        busRuleFieldObj.IO_Flag__c='O';
        
        insert busRuleFieldObj;
        
        list<BusinessRule_Template_Details__c> busRuleTemporaryList = new list<BusinessRule_Template_Details__c>();
        busRuleTemporaryList.add(busRuleTemDet);
        
        list<Scenario_Rule_Instance_Details__c> sceRuleDetList = new list<Scenario_Rule_Instance_Details__c>();
        sceRuleDetList.add(sceRuleInsDetObj);
        
        Data_Set_Column_Detail__c dataSetColObj = new Data_Set_Column_Detail__c();
        dataSetColObj.dataset_id__c=dataSetObj.id;
        dataSetColObj.value_type__c ='Discrete';
        dataSetColObj.ds_col_name__c='callactivity_flag';
        dataSetColObj.dataset_id__c=dataSetObj.id;
        dataSetColObj.datatype__c='Text';
        insert dataSetColObj;
        
        Category_Derived_Variable_Metadata__c catDerMetObj = new Category_Derived_Variable_Metadata__c();
        catDerMetObj.Business_Rule_Id__c=busRuleObj.id;
        insert catDerMetObj;
        
        Aggregate_Rule_Detail__c aggRuleDet = new Aggregate_Rule_Detail__c();
        aggRuleDet.Field_Name__c='ASAS1';
        aggRuleDet.Business_Rules__c=busRuleObj.id;
        insert aggRuleDet;
        
        Aggregate_Rule_Level__c aggLevelDet = new Aggregate_Rule_Level__c();
        aggLevelDet.Business_Rules__c=busRuleObj.id;
        insert aggLevelDet;
        
        
        Test.startTest();
        
        List<BRFieldWrapper> fieldWrapList = new List<BRFieldWrapper>();
        fieldWrapList=CPGAggregationCtrl.GetSourceAndDerivedFields(sceRuleInsDetObj.id);
        List<Data_Set_Rule_Map__c> dataSetRuleList = CPGAggregationCtrl.getInputTableName(sceRuleInsDetObj.id);
        CPG_Aggregate_Functions__mdt[] cpgAggList = CPGAggregationCtrl.getAggregateFunctionsList();
        List<Business_Rule_Type_Master__c > busRuleTypeMasList = CPGAggregationCtrl.getRuleIdDetails();
        List<Business_Rule_FieldMap_Details__c > busRuleFieldList = CPGAggregationCtrl.getBRFMDetailsParams(sceRuleInsDetObj.id);
        List<CPGAggregationCtrl.FieldWrapper> fieldWraplst = CPGAggregationCtrl.getFieldToAggregateValues(sceRuleInsDetObj.id);
        
        String namespace = SalesIQGlobalConstants.NAME_SPACE;
        string strJsonBusinessRules='{"' + namespace + 'Business_Rules__c":{"' + namespace + 'Business_Rule_Type_Master__c":"a1kf4000000ovR3AAI","Name":"NEW RULE","' + namespace + 'Is_Derived__c":true,"' + namespace + 'Is_Mandatory__c":true,"' + namespace + 'ScenarioRuleInstanceDetails__c":' + '"'+ sceRuleInsDetObj.id + '"}}';
        string strJsonBRFMDetails = '[{"' + namespace + 'IO_flag__c":"O","' + namespace + 'Param_Name__c":"FIELD 123","' + namespace + 'Param_Data_Type__c":"Numeric","' + namespace + 'Param_type__c":"Derived"}]';
        string strJsonAggrRuleDetails= '[{"' + namespace + 'Field_Name__c":"ASAS","' + namespace + 'Aggregate_Field__c":"Master tier","' + namespace + 'Data_Type__c":"Text","' + namespace + 'Aggregate_Function__c":"Count","' + namespace + 'Business_Rules__c":'   + '"'+ busRuleObj.id + '"}]';
        string strJsonRuleLevelDetails='[{"' + namespace + 'group_field__c":"callactivity_flag","' + namespace + 'Is_selected__c":"true","' + namespace + 'Display_Seq_No__c":"1","' + namespace + 'Business_Rules__c":'   + '"'+ busRuleObj.id + '"}]';
        CPGConstants.Response res = CPGAggregationCtrl.saveAgg(strJsonBusinessRules,strJsonAggrRuleDetails,strJsonRuleLevelDetails,strJsonBRFMDetails);
        List<CPGAggregationCtrl.AggregationInfoWrapper>  agginfoWrapList = CPGAggregationCtrl.GetAggregationType(busRuleObj.id);
        string strJsonBusinessRulesUpdate = '{"' + namespace + 'Business_Rules__c":{"' + namespace + 'Business_Rule_Type_Master__c":"a1kf4000000ovR3AAI","Name":"UPDATE RULE","' + namespace + 'Is_Derived__c":true,"' + namespace + 'Is_Mandatory__c":true,"' + namespace + 'ScenarioRuleInstanceDetails__c":' + '"'+ sceRuleInsDetObj.id +'"'+ ',"Id":'+'"'+ busRuleObj.id +'"'+  '}}';
        CPGConstants.Response res1= CPGAggregationCtrl.updateAgg(strJsonBusinessRulesUpdate,strJsonAggrRuleDetails,strJsonRuleLevelDetails,strJsonBRFMDetails);
        
        Test.stopTest();
        
        
        }
        
   }