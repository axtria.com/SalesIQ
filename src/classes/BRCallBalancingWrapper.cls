public  class BRCallBalancingWrapper {
        @AuraEnabled
        public Id businessRuleId{get;set;}
        @AuraEnabled
        public string ruleName{get;set;}
        @AuraEnabled
        public string ruleType{get;set;}
        @AuraEnabled
        public string level{get;set;}
        
        public BRCallBalancingWrapper(Id businessRuleId,string ruleName,string ruleType,string level){
            this.businessRuleId = businessRuleId;
            this.ruleName = ruleName;
            this.ruleType = ruleType;
            this.level = level;
        }
    }