global class BRRunScenarioScheduler implements Schedulable{
    global void execute(SchedulableContext SC) {
        BRScenarioSchedulerHandler.queueScenariosForDeltaRun();
    } 
}