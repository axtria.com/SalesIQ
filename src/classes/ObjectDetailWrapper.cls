public class ObjectDetailWrapper {

	@AuraEnabled
    public Position_Employee__c record {get;set;}

    @AuraEnabled
    public String name {get;set;}

    @AuraEnabled
    public boolean manageVacant {get;set;}

    @AuraEnabled
    public String type {get;set;}

    @AuraEnabled
    public Boolean isNew {get; set;}

    @AuraEnabled
    public Boolean isRowValidationFailed {
    	get {
    		return isRowValidationFailed == null ? false : isRowValidationFailed ;
    	} set;}

    @AuraEnabled
    public String clientPositionCode {get;set;}

    @AuraEnabled
    public List<FieldDetailWrapper> recordDetails {get;set;}

    public class FieldDetailWrapper {
    	@AuraEnabled
	    public List<String> setOfAvailableValues {get;set;}

	    @AuraEnabled
	    public String fieldValue {get;set;}

        @AuraEnabled
        public String lookupDataValue {get;set;}

        @AuraEnabled
        public String oldFieldValue {get;set;}

	    @AuraEnabled
	    public String fieldLabel {get;set;}

	    @AuraEnabled
	    public String fieldDataType {get;set;}

	    @AuraEnabled
	    public String fieldApiName {get;set;}

	    @AuraEnabled 
	    public Boolean isEditable {get; set;}

	    @AuraEnabled 
	    public Boolean isRequired {get; set;}

        @AuraEnabled 
        public String parentObject {get; set;}
        
        @AuraEnabled 
        public Boolean invokeExternalCall {get; set;}

        @AuraEnabled
        public list<String> picklistValues {get;set;}


	    @AuraEnabled
	    public Boolean isValidationFailed {
	    	get {
	    		return isValidationFailed == null ? false : isValidationFailed;
	    	} set;}

	    @AuraEnabled
	    public Set<String> validationErrorMessage {get; set;}

        @AuraEnabled
        public LookupDataWrapper lookupData {get;set;}


        @AuraEnabled
        public String date15Or16Rule {get;set;}  //for J&J

        @AuraEnabled
        public list<LookupDataWrapper> lookupDataList {get;set;}  //for J&J

        @AuraEnabled
        public Boolean changeEffectiveDateValidation{get;set;} //for J&J

        @AuraEnabled
        public Boolean checkboxValue {get;set;}
           

        //this map contains data for lookup fields in edit manage assignment popup(id,fieldvalue)

        /*@AuraEnabled
        public map<String,String> referenceMap {get;set;}*/

    }

    @AuraEnabled
    public Position__c position {get;set;}

    @AuraEnabled
    public Employee__c employee {get;set;}
    
    // added for field/nonfield condition
    @AuraEnabled
    public String positionCategoryEditable {get;set;}

    @AuraEnabled
    public list<String> positionCategoryList {get;set;}

    @AuraEnabled
    public list<String> nonfieldPositionType {get;set;}

    @AuraEnabled 
    public String fieldPositionType {get;set;}

    @AuraEnabled
    public String positionTypeEditable {get;set;}

    @AuraEnabled
    public String errorMessage {get;set;}

    @AuraEnabled
    public Boolean isEnforced {get;set;}

    @AuraEnabled
    public Boolean isMirror {get;set;}

    @AuraEnabled
    public Boolean isCascadeDeleteEnabled {get;set;}

    @AuraEnabled
    public String footPrintMessage {get;set;}
    
    @AuraEnabled
    public String overlayNaming {get;set;}
    
    @AuraEnabled
    public String baseNaming {get;set;}

    @AuraEnabled
    public list<PositionAttributeWrapper> positionAttributes{get;set;}

    public class PositionAttributeWrapper{
    	@AuraEnabled
    	public List<ObjectDetailWrapper.FieldDetailWrapper> rowDataConfig {get;set;}

    	public PositionAttributeWrapper(List<ObjectDetailWrapper.FieldDetailWrapper>rowDataConfig){
            this.rowDataConfig = rowDataConfig;
        }
    }

    @AuraEnabled
    public list<BasePositionsWrapper> basePositionsWrapper {get;set;}

    public class BasePositionsWrapper{
        @AuraEnabled
        public Position_Team_Instance__c basePositionTeamInstance {get; set;}

        @AuraEnabled
        public String relatedOverlayPositionName{get;set;}

        @AuraEnabled
        public String basePositionName {get;set;}

        @AuraEnabled
        public String basePositionId {get;set;}

        @AuraEnabled
        public Boolean isChecked {get;set;}

        @AuraEnabled
        public String basePositionCode {get;set;}

        @AuraEnabled
        public String basePositionEffectiveStartDate{get;set;}

        @AuraEnabled
        public String basePositionEffectiveEndDate{get;set;}

        @AuraEnabled
        public String baseTeam {get;set;}

        @AuraEnabled
        public boolean isDisabled {get;set;}

        @AuraEnabled
        public boolean isDefaultDisabled {get;set;}

        @AuraEnabled
        public boolean isDefaultChecked {get;set;}
    }

    public class LookupDataWrapper{
        @AuraEnabled
        public String objName {get;set;}
        @AuraEnabled
        public String text{get;set;}
        @AuraEnabled
        public String val{get;set;}
    }

     @AuraEnabled
    public DependencyControlDataWrapper dependencyControlWrapper {get;set;}


    public class DependencyControlDataWrapper {

        @AuraEnabled
        public set<String> dependentFields {get;set;}
        @AuraEnabled
        public map<String, list<Dependency_Control_Condition__c>> dependencyMap {get;set;}

        /*public DependencyControlDataWrapper(set<String> dependentFields, map<String, list<Dependency_Control_Condition__c>> dependencyMap){
            this.dependentFields = dependentFields;
            this.dependencyMap = dependencyMap;
        }*/

    }
}