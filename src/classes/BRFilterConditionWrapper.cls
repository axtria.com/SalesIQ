public class BRFilterConditionWrapper {
    @AuraEnabled
    public String bindClause;
    @AuraEnabled
    public List<ConditionWrapper> criterias;
    
    public class ConditionWrapper{
        @AuraEnabled
        public String field;
        @AuraEnabled
        public String dataType;
        @AuraEnabled
        public String colValueType;
        @AuraEnabled
        public String operator;
        @AuraEnabled
        public string value;        
        @AuraEnabled
        public string anotherValue;
        @AuraEnabled
        public Boolean isGroup;

        @AuraEnabled
        public BRFilterConditionWrapper innerConditionWrapper;
    }
}