@isTest
private class TestCPGDataObjectTrigger 
{
    static testMethod void testMethod1() 
    {
    	Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        insert workSpaceObj;

        Team__c teamObj = new Team__c();
        teamObj.Name = 'OPH-Spain';
        insert teamObj;

        Data_Set__c dataSetObj = new Data_Set__c();
        dataSetObj.Name = 'Call Plan Summary';
        dataSetObj.Is_Master__c = true;
        insert dataSetObj;

        Data_Object__c dataObj = new Data_Object__c();
        dataObj.dataset_id__c=dataSetObj.id;
        insert dataObj;

        Data_Object__c dataObj2 = new Data_Object__c();
        dataObj2.dataset_id__c=dataSetObj.id;
        insert dataObj2;

        Test.startTest();
        dataObj.status__c = CPGConstants.SUCCESS;
        update dataObj;
        dataObj.status__c = CPGConstants.SUCCESS;
        dataObj2.status__c = CPGConstants.SUCCESS;
        update new List<Data_Object__c>{dataObj,dataObj2};
        dataObj2.status__c = CPGConstants.READY;
        update dataObj2;
        Scenario_Execution_Setting__c scenObj = new Scenario_Execution_Setting__c();
        scenObj.Type__c = SalesIQGlobalConstants.SCENARIO_RECORDTYPE_ALIGNMENT;
        scenObj.Name = 'Scheduler Job';
        scenObj.Record_Id__c = teamObj.Id;
        scenObj.Scenario_Where_Clause__c = 'Request_Process_Stage__c = \'Ready\'';
        scenObj.Additional_Where_Clause__c = '(Rule_Execution_Status__c = \'Success\' or Rule_Execution_Status__c = \'Error\')';
        insert scenObj;
        
        Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        scenarioObj.Last_Promote_Success_Date__c = Date.parse('1/1/2014');
        List<RecordType> alignmentRT = [select Id from RecordType where DeveloperName =: SalesIQGlobalConstants.SCENARIO_RECORDTYPE_ALIGNMENT];
        scenarioObj.RecordTypeId = alignmentRT.isEmpty() ? null : alignmentRT[0].Id ;
        scenarioObj.Team_Name__c = scenObj.Record_Id__c;
        insert scenarioObj;
        
        BRScenarioSchedulerHandler.queueScenariosForDeltaRun();
        Test.stopTest();
        
    }

    public static testMethod void testAffiliationNetworkDepth() {
        
        ETL_Config__c etl = new ETL_Config__c(Name = 'BRMS');
        insert etl;
            
        Data_Set__c dataSet  =new Data_Set__c(Name='Data Set',Data_Set_Object_Name__c='Account_Affiliation__c',Is_Master__c=true,is_internal__c=false,SalesIQ_Internal__c=true);
        insert dataSet;
        
        Data_Set_Column_Detail__c  columnDetail =new Data_Set_Column_Detail__c(Name='columnDetail',dataset_id__c=dataSet.id,Source_Column__c='Affiliation_Network__c');
        insert columnDetail;
        Data_Object__c dataObject = new Data_Object__c(Name='Data Object',status__c=SalesIQGlobalConstants.IN_PROGRESS,dataset_id__c=dataSet.id);
        insert dataObject;
        dataObject.status__c=SalesIQGlobalConstants.SUCCESS;
        update dataObject;
        Scenario_Execution_Setting__c scenObj = new Scenario_Execution_Setting__c();
        scenObj.Type__c = SalesIQGlobalConstants.SCENARIO_RECORDTYPE_BLOCK;
        scenObj.Name = 'Scheduler Job';
        scenObj.Scenario_Where_Clause__c = 'Request_Process_Stage__c = \'Ready\'';
        scenObj.Additional_Where_Clause__c = '(Rule_Execution_Status__c = \'Success\' or Rule_Execution_Status__c = \'Error\')';
        insert scenObj;
        BRScenarioSchedulerHandler.syncMasterData();
        BRScenarioSchedulerHandler.queueExclusionScenarioForDeltaRun();
        //List<Data_Set__c> datasetList=[Select id,Data_Set_Object_Name__c FROM Data_Set__c WHERE id=:Trigger.new[0].dataset_id__c];
        
    }
}