@isTest
public class TestSalesIQCallPlanScenarioCtrl{
    
    static testMethod void callPlanSettingTest(){

    		Team__c team = new Team__c();
    		team.Name = 'HTN';
    		insert team;

    		Team_Instance__c teamInstance = new Team_Instance__c();
    		teamInstance.Team__c = team.id;
    		insert teamInstance;

    		Workspace__c workspace = new Workspace__c();
    		workspace.Name = 'W1';
    		workspace.Workspace_Description__c = 'this is testing workspace';
    		workspace.Workspace_Start_Date__c = System.today().addMonths(-1);
    		workspace.Workspace_End_Date__c = System.today().addMonths(+1);
    		insert workspace;

            //RecordType type = [select id,Name from RecordType where Name = 'Mapping'];

            string NAME_SPACE = SObjectDescribe.namespacePrefix ;
    		Scenario__c scenario = new Scenario__c();
    		scenario.Scenario_Type__c = 'Call Plan Generation';
    		scenario.Team_Instance__c = teamInstance.Id;
    		scenario.Scenario_Name__c = 'Scenario 111';
    		scenario.Scenario_Stage__c = 'Design';
    		scenario.Scenario_Status__c = 'Active';
    		scenario.Workspace__c = workspace.Id;
            scenario.Team_Instance__c = teamInstance.Id;
            scenario.Scenario_Source__c = SalesIQGlobalConstants.SCENARIO_SOURCE_CREATE_FROM_FILE;
            scenario.KPI_Card_Guardrail_Config__C = true;
    		insert scenario;

            Scenario_Data_Object_Map__c dataObjMap = new Scenario_Data_Object_Map__c();
            dataObjMap.Scenario__c = scenario.ID;
            dataObjMap.RecordTypeId = SalesIQUtility.getselctedRecordType(NAME_SPACE+'Scenario_Data_Object_Map__c', 'Mapping');
            insert dataObjMap;


    		teamInstance.Scenario__c = scenario.Id;
    		update teamInstance;

            SalesIQCallPlanScenarioCtrl.MappingFieldsWrapper wrapper = new SalesIQCallPlanScenarioCtrl.MappingFieldsWrapper();
            wrapper.pacpField = 'Picklist__c';
            wrapper.systemField = 'Dummy1__c';
            wrapper.isRequired = true;
    
    		SalesIQCallPlanScenarioCtrl.initCallPlan(scenario.Id);
            
            SalesIQCallPlanScenarioCtrl.getOrgNamespace();
            SalesIQCallPlanScenarioCtrl.getListViewID(NAME_SPACE+'Team_Instance_Object_Attribute__c','Call Plan');
            try{
                SalesIQCallPlanScenarioCtrl.checkSaveValidations(JSON.serialize(new list<SalesIQCallPlanScenarioCtrl.MappingFieldsWrapper>{wrapper}),'Collaboration',scenario.Id);
                SalesIQCallPlanScenarioCtrl.confirmScenarioRequest(scenario, 'Collaboration','');
                SalesIQCallPlanScenarioCtrl.confirmScenarioRequest(scenario, 'Collaboration',JSON.serialize(new list<SalesIQCallPlanScenarioCtrl.MappingFieldsWrapper>{wrapper}));
                SalesIQCallPlanScenarioCtrl.confirmScenarioRequest(scenario, 'Collaboration',JSON.serialize(new list<SalesIQCallPlanScenarioCtrl.MappingFieldsWrapper>{wrapper}));
                
           }catch(Exception e){
                
            }
    		
    }

       
}