@IsTest 
private class TestRequestController {
    
    static testMethod void testRequestController() {
        try{
                
            TotalApproval__c setting = new TotalApproval__c(Name='Quarter',Subscriber_Profile__c='Q3-2016');
            insert setting;
                    
            TriggerContol__c  triggerctrl =  new TriggerContol__c(IsStopTrigger__c=false , Name='ChangeRequestTrigger');
            insert triggerctrl;

            DiplayColorCriteria__c  diplayColorCriteria  = new DiplayColorCriteria__c (Color__c ='Normal' , Name='Normal');
            insert diplayColorCriteria;
            
            DiplayColorCriteria__c  diplayColorCriteriaError  = new DiplayColorCriteria__c (Color__c ='#F8CBAD' , Name='Error');
            insert diplayColorCriteriaError;  
            
            DiplayColorCriteria__c  diplayColorCriteriaWarning  = new DiplayColorCriteria__c (Color__c ='#ffE699' , Name='Warning');
            insert diplayColorCriteriaWarning;
            
            Account acc = CallPlanTestDataFactory.CreateAccount('Direct Aligned','11788','Physician');    
            
            insert acc; 
      
            Account acc2 = CallPlanTestDataFactory.CreateAccount('Direct Aligned','11789','Physician');
            
            insert acc2;
            
            Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11788');
            insert geo;
            
            Geography__c pointGeo = CallPlanTestDataFactory.createGeography('Point',geo,'11789');       
            insert pointGeo;
            
            
            
            
            Profile hoProfile = [SELECT Id FROM Profile WHERE Name =: 'System Administrator' limit 1];
            
            User hoUser1 = new User(LastName = 'ho',
                                FirstName='test',
                                Alias = 'test',
                                Email = 'test.ho@test.com',
                                Username = 'test.ho@test.com',
                                ProfileId = hoProfile.id,
                                TimeZoneSidKey = 'GMT',
                                LanguageLocaleKey = 'en_US',
                                EmailEncodingKey = 'UTF-8',
                                LocaleSidKey = 'en_US');
            insert hoUser1;     
                    
            Team__c ZipBasedTeam_Base =  CallPlanTestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_BASE);     
            insert ZipBasedTeam_Base;
            
            Team__c ZipBasedTeam_Overlay =  CallPlanTestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_OVERLAY);       
            ZipBasedTeam_Overlay.Name='HTN Overlay';
            ZipBasedTeam_Overlay.Controlling_Team__c=ZipBasedTeam_Base.id;
            insert ZipBasedTeam_Overlay;
            
            Team_Instance__c zipBasedTeamInstance_Base = CallPlanTestDataFactory.CreateTeamInstance(ZipBasedTeam_Base,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP);
            zipBasedTeamInstance_Base.CIM_available__c = true;
            zipBasedTeamInstance_Base.Restrict_ZIP_Share__c = true;
            insert zipBasedTeamInstance_Base;

            Team_Instance__c zipBasedTeamInstance_Overlay = CallPlanTestDataFactory.CreateTeamInstance(ZipBasedTeam_Overlay,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP);
            zipBasedTeamInstance_Overlay.Team_Instance_Code__c = 'DI-00002'; 
            insert zipBasedTeamInstance_Overlay;     
            
            /*Team_Instance_Geography__c TeaminstanceGeo_Base = CallPlanTestDataFactory.CreateTeamInstanceGeoGraphy(geo,zipBasedTeamInstance_Base); 
            Team_Instance_Geography__c TeaminstanceGeo_Overlay = CallPlanTestDataFactory.CreateTeamInstanceGeoGraphy(geo,zipBasedTeamInstance_Overlay); 
            insert new Team_Instance_Geography__c[]{TeaminstanceGeo_Base,TeaminstanceGeo_Overlay};*/
            
            Position__c district = CallPlanTestDataFactory.createPositionParent(null,ZipBasedTeam_Base);
            district.Related_Position_Type__c = 'Base';
            insert district;
            
            Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, null, zipBasedTeamInstance_Base);
            districtTeamInstance.X_Max__c=-72.6966429900;
            districtTeamInstance.X_Min__c=-73.9625820000;
            districtTeamInstance.Y_Max__c=40.9666490000;
            districtTeamInstance.Y_Min__c=40.5821279800;
            insert districtTeamInstance;
            
            // Territory position - base
            Position__c territory_base =  CallPlanTestDataFactory.createPosition(district,ZipBasedTeam_Base);
            territory_base.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE;
            // Territory position - overlay
            Position__c territory_overlay =  CallPlanTestDataFactory.createPosition(district,ZipBasedTeam_Overlay);
            territory_overlay.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_OVERLAY;
            
            insert new Position__c[]{ territory_base, territory_overlay};
            
            Position_Team_Instance__c territoryTeamInstance_base = CallPlanTestDataFactory.CreatePosTeamInstance(territory_base, district, zipBasedTeamInstance_Base);
            territoryTeamInstance_base.X_Max__c=-72.6966429900;
            territoryTeamInstance_base.X_Min__c=-73.9625820000;
            territoryTeamInstance_base.Y_Max__c=40.9666490000;
            territoryTeamInstance_base.Y_Min__c=40.5821279800;
            
            
            Position_Team_Instance__c territoryTeamInstance_overlay = CallPlanTestDataFactory.CreatePosTeamInstance(territory_overlay, district,zipBasedTeamInstance_Overlay);
            territoryTeamInstance_overlay.X_Max__c=-72.6966429900;
            territoryTeamInstance_overlay.X_Min__c=-73.9625820000;
            territoryTeamInstance_overlay.Y_Max__c=40.9666490000;
            territoryTeamInstance_overlay.Y_Min__c=40.5821279800;
            
            insert new Position_Team_Instance__c[] {territoryTeamInstance_base, territoryTeamInstance_overlay};
            
            /*Team_Instance_Account__c teamInsAccount = CallPlanTestDataFactory.createTeamInstanceAccount(acc,zipBasedTeamInstance_Base);
            insert teamInsAccount;*/
            
            Position_Account__c SourcepositionAccount = CallPlanTestDataFactory.createPositionAccountPending(acc, territory_base, zipBasedTeamInstance_Base, territoryTeamInstance_base);
            insert SourcepositionAccount;
            
            Position_Geography__c sourcePosGeo = CallPlanTestDataFactory.createPosGeography(geo, territory_base ,territoryTeamInstance_base, zipBasedTeamInstance_Base);
            sourcePosGeo.Metric1__c = 2.444 ;
            sourcePosGeo.Metric2__c = 43.555 ;
            sourcePosGeo.Metric3__c = 232.22 ;
            sourcePosGeo.Metric4__c = 321.99 ;
            sourcePosGeo.Metric5__c = 211.22 ;
            sourcePosGeo.Metric6__c = 3311.211 ;
            sourcePosGeo.Metric7__c = 332.11 ;
            sourcePosGeo.Metric8__c = 98.08 ;
            sourcePosGeo.Metric9__c= 976.56 ;
            sourcePosGeo.Metric10__c = 6728.32 ;
            insert sourcePosGeo;
            
            Position_Geography__c overlaySourcePosGeo = CallPlanTestDataFactory.createPosGeography(geo, territory_overlay ,territoryTeamInstance_overlay, zipBasedTeamInstance_Overlay );
            overlaySourcePosGeo.Metric1__c = 2.444 ;
            overlaySourcePosGeo.Metric2__c = 43.555 ;
            overlaySourcePosGeo.Metric3__c = 232.22 ;
            overlaySourcePosGeo.Metric4__c = 321.99 ;
            overlaySourcePosGeo.Metric5__c = 211.22 ;
            overlaySourcePosGeo.Metric6__c = 3311.211 ;
            overlaySourcePosGeo.Metric7__c = 332.11 ;
            overlaySourcePosGeo.Metric8__c = 98.08 ;
            overlaySourcePosGeo.Metric9__c= 976.56 ;
            overlaySourcePosGeo.Metric10__c = 6728.32 ;
            insert overlaySourcePosGeo ;
            
            Related_Position__c relatedposSource=CallPlanTestDataFactory.createRelatedPos(territoryTeamInstance_base,territoryTeamInstance_overlay,territory_overlay, territory_base);
            insert relatedposSource ;
            
            Position__c destTerr_base = CallPlanTestDataFactory.createPosition(district,ZipBasedTeam_Base);
            destTerr_base.Name = 'Long Island West';
            destTerr_base.Client_Territory_Name__c = 'Long Island West';
            destTerr_base.Client_Position_Code__c = '1NE30012';
            destTerr_base.Client_Territory_Code__c='1NE30012';
            destTerr_base.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE;
            insert destTerr_base;
            Position__c destTerr_overlay = CallPlanTestDataFactory.createPosition(district,ZipBasedTeam_Overlay);
            destTerr_overlay.Name = 'Long Island West';
            destTerr_overlay.Client_Territory_Name__c = 'Long Island West';
            destTerr_overlay.Client_Position_Code__c = '1NE30012';
            destTerr_overlay.Client_Territory_Code__c='1NE30012';
            destTerr_overlay.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_OVERLAY;
            insert destTerr_overlay;
            
            
            
            Position_Team_Instance__c destTerrTeamInstance_base = CallPlanTestDataFactory.CreatePosTeamInstance(destTerr_base, district, zipBasedTeamInstance_Base);
            destTerrTeamInstance_base.X_Max__c=-72.6966429900;
            destTerrTeamInstance_base.X_Min__c=-73.9625820000;
            destTerrTeamInstance_base.Y_Max__c=40.9666490000;
            destTerrTeamInstance_base.Y_Min__c=40.5821279800;
            insert destTerrTeamInstance_base;
            
            Position_Team_Instance__c destTerrTeamInstance_overlay = CallPlanTestDataFactory.CreatePosTeamInstance(destTerr_overlay, district, zipBasedTeamInstance_Overlay);
            destTerrTeamInstance_overlay.X_Max__c=-72.6966429900;
            destTerrTeamInstance_overlay.X_Min__c=-73.9625820000;
            destTerrTeamInstance_overlay.Y_Max__c=40.9666490000;
            destTerrTeamInstance_overlay.Y_Min__c=40.5821279800;
            insert destTerrTeamInstance_overlay;  
            
            Position_Account__c DestinationpositionAccount = CallPlanTestDataFactory.createPositionAccountPending(acc, destTerr_base, zipBasedTeamInstance_Base, destTerrTeamInstance_base);
            insert DestinationpositionAccount;
            
            Position_Geography__c DestinationPosGeo = CallPlanTestDataFactory.createPosGeography(geo, destTerr_base ,destTerrTeamInstance_base, zipBasedTeamInstance_Base);
            insert DestinationPosGeo;
            
            Position_Geography__c overlayDestinationPosGeo = CallPlanTestDataFactory.createPosGeography(geo, destTerr_overlay ,destTerrTeamInstance_overlay, zipBasedTeamInstance_Overlay);
            insert overlayDestinationPosGeo;
            
            Related_Position__c relatedposDest=CallPlanTestDataFactory.createRelatedPos(destTerrTeamInstance_base,destTerrTeamInstance_overlay,destTerr_overlay, destTerr_base);
            insert relatedposDest ;
            
            
            Change_Request_Type__c zipType = CallPlanTestDataFactory.CreateZipRequestType();        
            insert zipType;
            
            list<CIM_Config__c> cimConfigListBaseOverlay=new list<CIM_Config__c>();
            list<CIM_Config__c> cimConfigList = CallPlanTestDataFactory.createCIMCOnfig(zipType,zipBasedTeamInstance_Base,'Team_instance_geography__c');
            list<CIM_Config__c> cimConfigListForOverlay = CallPlanTestDataFactory.createCIMCOnfig(zipType,zipBasedTeamInstance_Overlay,'Team_instance_geography__c');
            cimConfigListBaseOverlay.addAll(cimConfigList);
            cimConfigListBaseOverlay.addAll(cimConfigListForOverlay);
            insert cimConfigListBaseOverlay;

            
            CR_Team_Instance_Config__c crTeamInsConf = new CR_Team_Instance_Config__c();
            crTeamInsConf.Change_Request_Types__c = zipType.id;
            crTeamInsConf.Team_Instance__c = zipBasedTeamInstance_Base.id;
            crTeamInsConf.Configuration_Name__c='Is Account Move';
            insert crTeamInsConf;
            
            list<CIM_Position_Metric_Summary__c> impactSummaryList = CallPlanTestDataFactory.createCIMMetricSummary(cimConfigList,destTerrTeamInstance_base,zipBasedTeamInstance_Base);
            insert impactSummaryList;

            list<CIM_Position_Metric_Summary__c> impactSummaryList_overlay = CallPlanTestDataFactory.createCIMMetricSummary(cimConfigListForOverlay,destTerrTeamInstance_overlay,zipBasedTeamInstance_Overlay);
            insert impactSummaryList_overlay;
            
            Change_Request__c zipChangeRequest = new Change_Request__c();
            zipChangeRequest.AllZips__c = geo.name;
            zipChangeRequest.Point_Zips__c = pointGeo.name;
            zipChangeRequest.Approver3__c = hoUser1.id;
            zipChangeRequest.Approver1__c = hoUser1.id;
            zipChangeRequest.Approver2__c = hoUser1.id;
            zipChangeRequest.Status__c = 'Pending';
            zipChangeRequest.Destination_Position__c = destTerr_base.id;
            zipChangeRequest.Destination_Positions__c= destTerrTeamInstance_base.id;
            zipChangeRequest.Source_Positions__c= territoryTeamInstance_base.id;
            zipChangeRequest.Source_Position__c = territory_base.id;
            zipChangeRequest.Team_Instance_ID__c = zipBasedTeamInstance_Base.id;
            zipChangeRequest.Execution_Date__c = date.today();
            zipChangeRequest.RecordTypeID__c= zipType.id;
            zipChangeRequest.Request_Type_Change__c = SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP;
            zipChangeRequest.Error_Type__c='Error';
            insert zipChangeRequest;

            list<CIM_Change_Request_Impact__c> lstRequestImpact = new list<CIM_Change_Request_Impact__c>();
            lstRequestImpact = [select id from CIM_Change_Request_Impact__c where  Change_Request__c =: zipChangeRequest.id];


            CR_Geography__c crGeo = new CR_Geography__c();
            crGeo.Geography__c = geo.id;
            crGeo.Destination_Position__c = destTerr_base.id;
            crGeo.Source_Position__c = territory_base.id;
            crGeo.Destination_Position_Team_Instance__c = destTerrTeamInstance_overlay.id;
            crGeo.Source_Position_Team_Instance__c = territoryTeamInstance_base.id;
            crGeo.Change_Request__c = zipChangeRequest.Id;
            insert crGeo;
            
            //Test.setMock(HttpCalloutMock.class, new ESRIServicesMockGenerator());
            ApexPages.StandardController sc = new ApexPages.StandardController(zipChangeRequest);
            RequestCtlr onjMApinline = new RequestCtlr(sc);
            onjMApinline.selectedOverlayTeamInstance = zipBasedTeamInstance_Overlay.id;
            test.startTest();
            onjMApinline.getSelectedOverlayCIMRequest();  
            onjMApinline.enableCustomMetric();
            test.stopTest();
        }
        catch(Exception ex){
            System.debug(ex);
        }
    }
    
}