global class CPGExpressionParserBatch implements Database.Batchable<sObject>,Database.AllowsCallouts  {
    global String query ;
    global String scenarioId;
    global string mode;
    global Boolean promoteResult;
    global Boolean runFull;
   
    
    global CPGExpressionParserBatch(string scenarioId){
        this.scenarioId=scenarioId;
        this.promoteResult = false;
        this.runFull = true;
    }

    global CPGExpressionParserBatch(string scenarioId, Boolean promoteResult,Boolean runFull){
        this.scenarioId=scenarioId;
        this.promoteResult = promoteResult;
        this.runFull = runFull;
    }
     
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {   
        return Database.getQueryLocator('select Id from Scenario_Rule_Instance_Details__c where  Scenario_Id__c= :scenarioId');  
    }
    
    global void execute(Database.BatchableContext BC, List<Scenario_Rule_Instance_Details__c> lstScenarioRuleInstanceDetails){
        system.debug('executing-->'+lstScenarioRuleInstanceDetails.size());
        system.debug('lstScenarioRuleInstanceDetails-->'+lstScenarioRuleInstanceDetails);
        delete[select Id from Business_Rules_Expression__c where Business_Rules__r.ScenarioRuleInstanceDetails__c in :lstScenarioRuleInstanceDetails];
        delete[select Id from Call_balancing_expression_data_detail__c where Business_Rules__r.ScenarioRuleInstanceDetails__c in :lstScenarioRuleInstanceDetails];
        map<ID, list<Business_Rules__c>> mapScenarioRules = new map<ID,  list<Business_Rules__c>>();
        for(Business_Rules__c rules : [select Id,Name,Business_Rule_Type_Master__r.Name,Business_Rule_Type_Master__r.Component_Type__r.Name,ScenarioRuleInstanceDetails__c from Business_Rules__c 
                                      where ScenarioRuleInstanceDetails__c in:lstScenarioRuleInstanceDetails])
        {
            if(rules!=null){
                if(mapScenarioRules.containsKey(rules.ScenarioRuleInstanceDetails__c)){
                    List<Business_Rules__c> lstRules= mapScenarioRules.get(rules.ScenarioRuleInstanceDetails__c);
                    lstRules.add(rules);
                    mapScenarioRules.put(rules.ScenarioRuleInstanceDetails__c, lstRules);                     
                }
                else{
                    mapScenarioRules.put(rules.ScenarioRuleInstanceDetails__c, new List<Business_Rules__c> {rules});                    
                }
           }
        }
        map<ID, Id> mapScenarioDataset = new map<ID, Id>();
        set<id> setDataSetIds = new set<id>();
        List<Data_Set_Rule_Map__c> lstRuleMap=[select Id,dataset_id__c,scenario_rule_instance_id__c from Data_Set_Rule_Map__c 
                                               where scenario_rule_instance_id__c in: lstScenarioRuleInstanceDetails and ds_type__c='O'];
        System.debug('lstRuleMap-->'+lstRuleMap);
        for(Data_Set_Rule_Map__c ruleMap :lstRuleMap )
        {
            if(ruleMap!=null){               
               mapScenarioDataset.put(ruleMap.scenario_rule_instance_id__c,ruleMap.dataset_id__c);  
               setDataSetIds.add(ruleMap.dataset_id__c);
            }
        }
        system.debug('mapScenarioDataset--->'+mapScenarioDataset);
        map<Id, list<Data_Set_Column_Detail__c>> mapScerioColmn = new map<Id, list<Data_Set_Column_Detail__c>>();
        string scenarioInstanceDetailsId;
        for(Data_Set_Column_Detail__c colDetail : [select Id,ds_col_name__c,tb_col_nm__c,dataset_id__c,datatype__c 
                                                  from Data_Set_Column_Detail__c 
                                                  where dataset_id__c in: setDataSetIds])
        {
            for(string scenarioInstanceId:mapScenarioDataset.keySet()){
                if(mapScenarioDataset.get(scenarioInstanceId)==colDetail.dataset_id__c){
                    scenarioInstanceDetailsId=scenarioInstanceId;
                    if(mapScerioColmn.containsKey(scenarioInstanceDetailsId)){
                        list<Data_Set_Column_Detail__c> lstCol = mapScerioColmn.get(scenarioInstanceDetailsId);
                        lstCol.add(colDetail);
                        mapScerioColmn.put(scenarioInstanceDetailsId,lstCol);               
                    }
                    else{
                        mapScerioColmn.put(scenarioInstanceDetailsId,new list<Data_Set_Column_Detail__c>{colDetail});               
                    }
                }
            }
        }
       
        system.debug('mapScerioColmn--->'+mapScerioColmn);     
        processBusinessRules(mapScenarioRules,mapScerioColmn);
    }
    
    private void processBusinessRules(map<ID, list<Business_Rules__c>> mapScenarioRules,map<Id, list<Data_Set_Column_Detail__c>> mapScerioColmn)
    {
        for(Id scenarioInstance : mapScenarioRules.keySet()){
            list<Business_Rules__c> lstBusinessRules = mapScenarioRules.get(scenarioInstance);            
            list<Data_Set_Column_Detail__c> lstColmns= mapScerioColmn.get(scenarioInstance);
            for(Business_Rules__c businessRule:lstBusinessRules){
                CPGParserFactory factory=new CPGParserFactory(businessRule);
                factory.ParseExpression(scenarioInstance,lstColmns); 
            }
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
        
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
                          TotalJobItems, CreatedBy.Email,ExtendedStatus,JobType,ApexClassId,MethodName
                          FROM AsyncApexJob WHERE Id =
                          :BC.getJobId()];

        if(a.Status == 'Completed') 
        { 
          runMainJob(scenarioId,promoteResult,runFull);
        }
    }

    
    /* Python integrated job */
    public static String runMainJob(String scenarioId, Boolean promoteResult,Boolean runFull)
    {
        try
        {
            String s_username,s_password,access_key;
            list<ETL_Config__c> lstETLInfo = getETLConfigByName(CPGConstants.talend_Instance);
            if(lstETLInfo != null && lstETLInfo.size() > 0)
            {
                s_username = lstETLInfo[0].SF_UserName__c;
                s_password = lstETLInfo[0].SF_Password__c;
                access_key = lstETLInfo[0].S3_Security_Token__c;
            }  

            String upStreamDataSet = '',upstreamTableName = '' ;
            String compareTableName ='', compareFilter = '', compareDataSetId = '', customerUniverseTable = '', customerUniverseDataSet = '' ;
            Boolean isDelta = !runFull;

            String dataobjListString = getDataObjectList(scenarioId,false);
            List<Data_Set_Rule_Map__c> dsrmList = [Select File_Type__c,dataset_id__c,dataset_id__r.Is_Upstream__c, table_name__c,dataset_id__r.Base_Data_Set__c, scenario_rule_instance_id__r.Scenario_Id__r.Team_Instance__c from Data_Set_Rule_Map__c where scenario_rule_instance_id__r.Scenario_Id__c =: scenarioId and (dataset_id__r.is_upstream__c = true OR (File_Type__c = 'Cust Terr' and ds_type__c = 'I' and dataset_id__r.SalesIQ_Internal__c = true) OR (File_Type__c = 'Block' and ds_type__c = 'I' and dataset_id__r.SalesIQ_Internal__c = true) OR (File_Type__c = 'Base' and ds_type__c = 'O'))];

            String exclusionObjectName =  SalesIQGlobalConstants.NAME_SPACE + 'Account_Exclusion__c' ;
            List<Scenario__c> isExclusion = [select IsUniversal__c, (select Filter_Expression_Destination__c from Scenario_Data_Object_Map__r where Data_Set__r.SalesIQ_Internal__c = true and Data_Set__r.Data_Set_Object_Name__c =: exclusionObjectName) from Scenario__c where IsUniversal__c = true and Id =: scenarioId ];
            for(Data_Set_Rule_Map__c dsrm : dsrmList) {
                if(dsrm.dataset_id__r.is_upstream__c) {
                    upStreamDataSet = dsrm.dataset_id__c;
                    upstreamTableName = dsrm.table_name__c;
                } 
                else if (dsrm.File_Type__c == 'Base') {
                    customerUniverseTable = dsrm.table_name__c;
                    customerUniverseDataSet = dsrm.dataset_id__c;
                }

                if(isExclusion.isEmpty()) {
                    if (dsrm.File_Type__c == 'Cust Terr') {
                        compareTableName = dsrm.table_name__c;
                        compareDataSetId = dsrm.dataset_id__c;
                    } 
                } else {
                    if (dsrm.File_Type__c == 'Block') {
                        compareTableName = dsrm.table_name__c;
                        compareDataSetId = dsrm.dataset_id__c;
                        if(isExclusion[0].Scenario_Data_Object_Map__r.size() == 1)
                            compareFilter = isExclusion[0].Scenario_Data_Object_Map__r[0].Filter_Expression_Destination__c;
                    }
                }
            }

            if(isExclusion.isEmpty()) {
                String fieldName = SalesIQGlobalConstants.NAME_SPACE + 'Team_Instance__c';
                List<Data_Set_Column_Detail__c> cols = [select tb_col_nm__c from Data_Set_Column_Detail__c where dataset_id__c =: compareDataSetId and source_column__c =: fieldName ];

                if(!cols.isEmpty())
                  compareFilter = cols[0].tb_col_nm__c + '= \'' + dsrmList[0].scenario_rule_instance_id__r.Scenario_Id__r.Team_Instance__c + '\'';
            }

            String serverUrl = CPGUtility.getBRMSConfigValues('BRScenarioJob');
             //SSL CHANGES
            Map<String,String> urlParams = new Map<String,String>();
            urlParams.put('serviceUrl',serverUrl);

            // debugging purpose
            String url = serverUrl+'?user_name='+s_username+'&password='+s_password+'&security_token='+access_key+'&scenario_id='+scenarioId+'&dataObjects_ids='+dataobjListString+'&do_promote='+promoteResult+'&upstream_dataset_id='+upStreamDataSet+'&upstream_table_name='+upstreamTableName+'&namespace='+SalesIQGlobalConstants.NAME_SPACE+'&isDelta='+isDelta+'&sandbox='+SalesIQUtility.isSandboxOrg()+'&compare_table_name=' + compareTableName +  '&compare_table_filter='+ EncodingUtil.urlEncode(compareFilter, 'UTF-8') + '&compare_dataset_id=' + compareDataSetId + '&customer_univ_table_name=' + customerUniverseTable + '&customer_univ_dataset_id=' + customerUniverseDataSet;
            system.debug('url>>>'+url);

            String doPromote = promoteResult ? 'true' : 'false';
            String is_delta = isDelta ? 'true' : 'false';
            String isSandbox = (SalesIQUtility.isSandboxOrg()) ? 'true' : 'false';

            urlParams.put('user_name',s_username);
            urlParams.put('password',s_password);
            urlParams.put('security_token',access_key);
            urlParams.put('scenario_id',scenarioId);
            urlParams.put('dataObjects_ids',dataobjListString);
            urlParams.put('upstream_dataset_id',upStreamDataSet);
            urlParams.put('upstream_table_name',upstreamTableName);
            urlParams.put('namespace',SalesIQGlobalConstants.NAME_SPACE);
            urlParams.put('isDelta',is_delta);
            urlParams.put('sandbox',isSandbox);
            urlParams.put('do_promote',doPromote);
            urlParams.put('compare_table_name',compareTableName);
            urlParams.put('compare_dataset_id',compareDataSetId);
            urlParams.put('compare_table_filter', EncodingUtil.urlEncode(compareFilter, 'UTF-8') );
            urlParams.put('customer_univ_table_name',customerUniverseTable);
            urlParams.put('customer_univ_dataset_id',customerUniverseDataSet);

            System.debug('urlParams:--> ');
            System.debug(urlParams);



            /*Http h = new Http();*/

            //// Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
            //HttpRequest req = new HttpRequest();
            //req.setEndpoint(url);
            //req.setMethod('GET');
            //req.setHeader('Content-Type', 'application/json');
            //req.setHeader('Accept','application/json');
            //req.setTimeout(120000);
            //// Send the request, and return a response
            //HttpResponse res = h.send(req);
            //System.debug('response:--> ' + res.getBody());
            HttpResponse res = SalesiqUtility.getHTTPResponse(urlParams.get('serviceUrl'),urlParams,'POST',120000);
            String result = res.getBody();
            if(res.getstatusCode() != 200)
            {
              Scenario__c mySc = [Select id,Run_Response_Details__c,Rule_Execution_Status__c from Scenario__c where id=: scenarioId];
              mySc.Rule_Execution_Status__c = CPGConstants.ERROR;
              mySc.Run_Response_Details__c = res.getStatus();
              update mySc;
            }
            return res.getStatus();
        }
        catch(System.CalloutException e) {
            if(e.getMessage() == 'Read timed out') {
                // ignore in case of Read timeout
            }
            else {
                Scenario__c mySc = new Scenario__c(Id = scenarioId);
                mySc.Rule_Execution_Status__c = CPGConstants.ERROR;
                mySc.Run_Response_Details__c = e.getMessage();
                update mySc;
            }
            return null;
        }
        catch(Exception ex)
        {
            Scenario__c mySc = new Scenario__c(Id = scenarioId);
            mySc.Rule_Execution_Status__c = CPGConstants.ERROR;
            mySc.Run_Response_Details__c = ex.getMessage();
            update mySc;
            return null;
        }  
    }

    public static String getDataObjectList(String scenarioId,Boolean isUpstream)
    {
        String ns = SalesIQGlobalConstants.NAME_SPACE;
        List<Data_Set_Rule_Map__c> dsrmList = new List<Data_Set_Rule_Map__c>();
        if(isUpstream == true)
        {
            dsrmList = [Select dataset_id__c,dataset_id__r.Base_Data_Set__c from Data_Set_Rule_Map__c where scenario_rule_instance_id__r.Scenario_Id__c =: scenarioId and dataset_id__r.is_upstream__c = true];
        }
        else
        {
            Scenario__c currentScenario = [select Business_Rule_Template__c from Scenario__c where Id =: scenarioId];
            system.debug('currentScenario'+currentScenario);
            dsrmList = [Select dataset_id__c,dataset_id__r.Base_Data_Set__c from Data_Set_Rule_Map__c where scenario_rule_instance_id__c = null and Business_Rule_Template_Details__r.Business_Rule_Template_Id__c = :currentScenario.Business_Rule_Template__c and ds_type__c = 'I' and dataset_id__r.Is_Master__c = false and dataset_id__r.SalesIQ_Internal__c = true];

            system.debug('dsrmList'+dsrmList);
        }
      
        List<string> baseDataSetIdList = new List<String>();
        for(Data_Set_Rule_Map__c dsrm : dsrmList)
        {
           /* if(dsrm.dataset_id__r.Base_Data_Set__c != null) {
                baseDataSetIdList.add(String.valueOf(dsrm.dataset_id__r.Base_Data_Set__c));
            }*/
            //siq1022
            if(dsrm.dataset_id__c != null) {
                baseDataSetIdList.add(String.valueOf(dsrm.dataset_id__c));
            }
        }
      
        List<Data_Object__c> dataObjList = [select id from Data_Object__c where dataset_id__c in:baseDataSetIdList];
        List<String> dataObjectIds = new List<String>();
        for(Data_Object__c dataObj : dataObjList)
        {
          dataObjectIds.add(dataObj.Id);
        }

        String dataobjListString ;
        dataobjListString = String.join(dataObjectIds, ',') ;
      
        return dataobjListString;
    }


    /*@author : Pooja Dhiman 
    @description : This method will fetch ETL_Config__c records based on  etl Config Names
    @param1: etlConfigName : etl config name
    @return : ETL_Config__c list
    */
    public static List<ETL_Config__c> getETLConfigByName(string etlConfigName)
    {
        List<ETL_Config__c> etlConfigList;
            etlConfigList = [SELECT BDT_DataSet_Id__c,BR_Data_Object_Id__c,BR_PG_Database__c,BR_PG_Host__c,BR_PG_Password__c,BR_PG_Port__c,BR_PG_Schema__c,BR_PG_UserName__c,BR_Secret_Key__c,CreatedById,CreatedDate,End_Point__c,ESRI_Password__c,ESRI_UserName__c,GI_Dataset_Id__c,Id,Name,S3_Access_Key__c,S3_Bucket__c,S3_Filename__c,S3_Key_Name__c,S3_Security_Token__c,Server_Type__c,SFTP_Folder__c,SFTP_Host__c,SFTP_Password__c,SFTP_Port__c,SFTP_Username__c,SF_Password__c,SF_UserName__c FROM ETL_Config__c where name =:etlConfigName limit 1];
        return etlConfigList;
    }
}