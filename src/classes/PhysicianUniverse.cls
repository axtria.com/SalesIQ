/**
 * DEPRECATED Class
 */
 

public with sharing class PhysicianUniverse {

	/*
  Set<Id> pos{get;set;} 									// All unique positions of Rep
    public List<select_wrap> displayList {get;set;}			// List to be displayed on Page
	List<User_Access_Permission__c> positions {get;set;}	// All the positions of the Rep
    List<Position_Account_Call_Plan__c> ListUniverseAcc{get;set;}	// Temporary List for all Records
    string sortField {get;set;}								
    string sortdir {get;set;}
    string soql {get;set;}					

	List<String> posRead = new List<String>{'Name','Client_Position_Code__c'};
	List<String> userAccessPermissionRead = new List<String>{'Position__c','Team_Instance__c', 'user__c', 'Is_Active__c'};
	List<String> accountRead = new List<String>{'ID','FirstName__c','LastName__c','AccountNumber','AccountType__c','Speciality__c','BillingCity','BillingState','BillingPostalCode','Name'};
	List<String> pacpRead = new List<String>{'Calls_Approved__c','Calls_Updated__c','Calls_Original__c','Call_Sequence_Approved__c','Segment1__c','Segment2__c',   'ReasonAdd__c','ReasonDrop__c','Comments__c','isModified__c','Change_Status__c','Position__c','Team_Instance__c','isIncludedCallPlan__c','isAccountTarget__c', 'Rank__c','Account__c', 'ReasonAdd__c'};
	List<String> pacpUpdate = new List<String>{'Calls_Approved__c','Calls_Updated__c','Calls_Original__c','Call_Sequence_Approved__c','Segment1__c','Segment2__c',   'ReasonAdd__c','ReasonDrop__c','Comments__c','isModified__c','Change_Status__c','Position__c','Team_Instance__c','isIncludedCallPlan__c','isAccountTarget__c', 'Rank__c','Account__c', 'ReasonAdd__c'};
	
	public PhysicianUniverse()
	{
		//securityReview();
		// Initialising Variables and Lists
		sortField = 'Account__r.FirstName__c';
        sortdir = 'asc';
		soql = 'select Account__r.FirstName__c,Account__r.LastName__c,Account__r.Name,Account__r.Speciality__c,Account__r.AccountType__c,Account__r.BillingCity,Account__r.BillingState,Account__r.BillingPostalCode,ReasonDrop__c,ReasonAdd__c,Calls_Approved__c,Calls_Updated__c,Call_Sequence_Approved__c,Comments__c,Rank__c from Position_Account_Call_Plan__c where Position__c in :pos and isIncludedCallPlan__c = False';
        pos = new Set<Id>();
        
        if(SecurityUtil.checkRead(User_Access_Permission__c.SObjectType, userAccessPermissionRead, false))
		{
			 positions = [select Position__c from User_Access_Permission__c where User__c = :Userinfo.getUserId()];                
		}
		   
 
        for (User_Access_Permission__c p1 :positions)
        {
            pos.add(p1.Position__c);
        }
        runquery();  		

	}
	
	
	// Wrapper Class for displaying Records	
	public class select_wrap
    {
        
       public boolean selected{get;set;}
       public  Position_Account_Call_Plan__c rec{get;set;}
       
       public select_wrap(Position_Account_Call_Plan__c rec)
       {
            selected = false;
            this.rec = rec;
            
       }
    }
    
    
    public pagereference redirectToReasonPage()
    {
    	//PageReference
    	PageReference pg = new PageReference('/apex/ReasonAdd');
    	//pg.setRedirect(true);
    	return pg;	
    }
    
    List<Position_Account_Call_Plan__c> pacp;
    
    
    // Function executed when a physician is to be moved from Universe to Call Plan 
    public void IncludeInCallplan()
    {
    	pacp = new List<Position_Account_Call_Plan__c>();
    	
    	for(Position_Account_Call_Plan__c sw:reasonPacp)
    	{	
			sw.isIncludedCallPlan__c=True;
			
			if(sw.isModified__c==True)  // If originally (after last Approved ) the record was in Call Plan
			{
				sw.isModified__c = False;
				sw.Change_Status__c='Original';	
				sw.ReasonAdd__c = 'None';			
				
			}
			else
			{
				sw.isModified__c = True;
				sw.Change_Status__c='Pending for Submission';
			}
			sw.ReasonDrop__c = 'None';
			pacp.add(sw);
    	}
    	
    	if(SecurityUtil.checkUpdate(Position_Account_Call_Plan__c.SObjectType, pacpUpdate, false))
		{
			 update pacp;                
		}
		
    }
    
	List<Position_Account_Call_Plan__c> reasonPacp; 
	
	// Function to get Add Reason of Physician
	public List<Position_Account_Call_Plan__c> getphyAddDrop()
	{
		reasonPacp = new List<Position_Account_Call_Plan__c>();
    	for(select_wrap sw:displayList)
    	{
    		if(sw.selected)
    		{
    			reasonPacp.add(sw.rec);
    		}
    	}	
    	return reasonPacp;
	}
	
	// Function Saves the Add Reason, Adds Physician to Call Plan and then redirect back to Phsyician Universe Page    
    public pagereference saveReason()
    {
    	IncludeInCallplan();
    	PageReference pg = new PageReference('/apex/CallMax');
    	pg.setRedirect(true);
    	return pg;
    }
    
    public pagereference cancelReason()
    {
    	PageReference pg = new PageReference('/apex/PhysicianUniverse');
    	pg.setRedirect(true);
    	return pg;
    }
    
    // Function to run SOQL Queries
   public void runquery()
   {
   		displayList = new List<select_wrap>();
        ListUniverseAcc = new List<Position_Account_Call_Plan__c>();
        if(SecurityUtil.checkRead(Position_Account_Call_Plan__c.SObjectType, pacpRead, false) 
        	&& SecurityUtil.checkRead(Account.SObjectType, accountRead, false))
		{
       		ListUniverseAcc = Database.query(soql+' order by '+ String.escapeSingleQuotes(sortField) + ' ' + String.escapeSingleQuotes(sortDir) + ' NULLS LAST');
		}
		
		
        for(Position_Account_Call_Plan__c c1 : ListUniverseAcc)
        {
            displayList.add(new select_wrap(c1));
        }
   }
   
   public void toggleSort() 
   {
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
        runquery();
   }
   
       

       // Executed when Search is Called
       public void  runSearch()
        {  
           displayList = new List<select_wrap>();
           
           //Fetching parameters from Page  
           String ids = Apexpages.currentPage().getParameters().get('id');
           String firstName = Apexpages.currentPage().getParameters().get('firstname');
           String city= Apexpages.currentPage().getParameters().get('city');       
           String state= Apexpages.currentPage().getParameters().get('state');
           String specialty= Apexpages.currentPage().getParameters().get('specialty');
		
		   soql = 'select Account__r.FirstName__c,Account__r.LastName__c,Account__r.Name,Account__r.Speciality__c,Account__r.BillingCity,Account__r.AccountType__c,Account__r.BillingState,Account__r.BillingPostalCode,ReasonDrop__c,ReasonAdd__c,Calls_Approved__c,Calls_Updated__c,Call_Sequence_Approved__c,Comments__c,Rank__c from Position_Account_Call_Plan__c where Position__c in :pos and isIncludedCallPlan__c = False';
			
		   if (ids != null && !ids.equals(''))
                 soql += ' and account__r.AccountNumber LIKE \''+String.escapeSingleQuotes(ids)+'%\'  ';
                 
           if (firstname != null && !firstName.equals(''))
                 soql += ' and account__r.FirstName__c LIKE \''+String.escapeSingleQuotes(firstName)+'%\'  ';

            if (city!= null && !city.equals(''))
                 soql += ' and account__r.BillingCity LIKE \''+String.escapeSingleQuotes(city)+'%\'  ';
                 
            if (state!= null && !state.equals(''))
                soql += ' and account__r.BillingState LIKE \''+String.escapeSingleQuotes(state)+'%\'   ';   
            if (specialty!= null && !specialty.equals(''))
                soql += ' and account__r.Speciality__c LIKE \''+String.escapeSingleQuotes(specialty)+'%\'   ';     

           runQuery(); 
	}
		
   	public String searchString{get; set;}
    public String city{get; set;}
    public String state{get; set;}
    public String specialty{get; set;}
    public String ids{get; set;}
    public void clearSearch()
    {
        searchString='';
        city='';
        ids='';
        state='';
        specialty='';
        sortField ='account__r.name';
       runSearch();
    }

    */
}