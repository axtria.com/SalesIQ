/******************************************************************************************************************************************************
 * Name         :   TestScenarioCtlr.cls
 * Description  :   Test class for ScenarioCtlr.
 * Author       :   Raghvendra Rathore
 * Created On   :   05/22/2017
******************************************************************************************************************************************************/
@isTest
private class TestScenarioCtlr {
    public static ETL_Config__c etlObj;
    static testMethod void testInitScenario() {
        Workspace__c workspace = new Workspace__c(Name = 'test workspace',
                              Workspace_Start_Date__c = system.today().addMonths(-5),
                              Workspace_End_Date__c = system.today().addMonths(5),
                              Workspace_Description__c = 'Test Workspace');
        insert workspace;

        Organization_Master__c org = new Organization_Master__c();
        org.Name = 'North America';
        org.Org_Level__c = 'Region';
        org.Parent_Country_Level__c = true;
        insert org;

        Country__c country = new Country__c();
        country.name ='USA';
        country.Country_Code__c = 'US';
        country.Status__c = 'Active';
        country.Parent_Organization__c = org.id;
        insert country;

        ScenarioWrapper scenarioObj;
        test.startTest();
        ScenarioCtlr.getOrgNamespace();
            scenarioObj = ScenarioCtlr.initScenario(workspace.Id);
            ScenarioCtlr.getGeoLayerOptions(country.id);

        try{
            ScenarioWrapper scenarioObjError = ScenarioCtlr.initScenario('');
        }catch(Exception e){}
        
        test.stopTest();
        
        system.assert(scenarioObj != null);
    }
    
    static testMethod void testGetTeamOptions() {

        Team__c testTeam = new Team__c (Name='HTN', Type__c='Base', 
                        Effective_Start_Date__c = system.today().addMonths(-5), 
                        Effective_End_Date__c = Date.today().addMonths(5));
        insert testTeam;
        
        Team_Instance__c testTeamInstance = new Team_Instance__c();
        testTeamInstance.Name = 'HTN Q1';
        testTeamInstance.Alignment_Period__c = 'Operational';
        testTeamInstance.Team__c = testTeam.id;
        testTeamInstance.Alignment_Type__c = 'ZIP';
        testTeamInstance.isActiveCycle__c = 'Y';           
        insert testTeamInstance;
        
        map<String,String> options;
        
        test.startTest();
        options = ScenarioCtlr.getTeamOptions('');
        Team_Instance__c objTeamInst = ScenarioCtlr.getTeamInstanceById(testTeamInstance.Id);
        test.stopTest();
        
        system.assert(options != null);
    }
    
    static testMethod void testGetTeamInstanceOptions() {
        Team__c testTeam = new Team__c (Name='HTN', Type__c='Base', 
                        Effective_Start_Date__c = system.today().addMonths(-5), 
                        Effective_End_Date__c = Date.today().addMonths(5));
        insert testTeam;
        
        Team_Instance__c testTeamInstance = new Team_Instance__c();
        testTeamInstance.Name = 'HTN Q1';
        testTeamInstance.Alignment_Period__c = 'Operational';
        testTeamInstance.Team__c = testTeam.id;
        testTeamInstance.Alignment_Type__c = 'ZIP';
        testTeamInstance.isActiveCycle__c = 'Y';           
        insert testTeamInstance;
        
        map<String,String> options;
        
        test.startTest();
        options = ScenarioCtlr.getTeamInstanceOptions(testTeam.Id, SalesIQGlobalConstants.SCENARIO_TYPE_CALL_PLAN);
        try{
            options = ScenarioCtlr.getTeamInstanceOptions('', '');
        }catch(Exception e){}
        test.stopTest();
        
        system.assert(options != null);
    }

    @isTest(SeeAllData=true)
    static void testGetScenarioTypeOptions(){
        try{
            ScenarioCtlr.getScenarioTypeOptions();
        }catch(Exception e){
            
        }
        
    }
    @isTest(SeeAllData=true)
    static void testGetAlignmentTypeOptions(){
        try{
            ScenarioCtlr.getAlignmentType([SELECT ID FROM TEAM__c][0].id);
        }catch(Exception e){
            
        }
        
    }
    

    static testMethod void testGetScenarioListByTeamInstance(){

    }
    
    static testMethod void testGetSummary() {

         Workspace__c workspace = new Workspace__c(Name = 'test workspace',
                              Workspace_Start_Date__c = system.today().addMonths(-5),
                              Workspace_End_Date__c = system.today().addMonths(5),
                              Workspace_Description__c = 'Test Workspace');
        insert workspace;

        Team__c testTeam = new Team__c (Name='HTN', Type__c='Base', 
                        Effective_Start_Date__c = system.today().addMonths(-5), 
                        Effective_End_Date__c = Date.today().addMonths(5));
        insert testTeam;
        
        Team_Instance__c testTeamInstance = new Team_Instance__c();
        testTeamInstance.Name = 'HTN Q1';
        testTeamInstance.Alignment_Period__c = 'Operational';
        testTeamInstance.Team__c = testTeam.id;
        testTeamInstance.Alignment_Type__c = 'ZIP';
        testTeamInstance.isActiveCycle__c = 'Y';           
        insert testTeamInstance;
        
        CR_Team_Instance_Config__c configData = new CR_Team_Instance_Config__c(Configuration_Value__c = 'Territory', Configuration_Name__c = '1', 
                                                                                Configuration_Type__c = 'Hierarchy Configuration', Team_Instance__c = testTeamInstance.Id);
        insert configData;
        
        Position__c territory = new Position__c(Name = 'Terr 1', Team_Instance__c = testTeamInstance.Id, Team_iD__c = testTeam.Id, Hierarchy_Level__c = '1');
        insert territory;
        
        list<SummaryWrapper> summaryWrapperList = ScenarioCtlr.getSummary(testTeamInstance.Id,workspace.ID);
        
        Position_Account__c posAcc = new Position_Account__c(Team_Instance__c = testTeamInstance.Id, Position__c = territory.Id, 
                                                             Effective_Start_Date__c = system.today().addMonths(-5), Effective_End_Date__c = Date.today().addMonths(5));
        insert posAcc;
        
        Position_Employee__c posEmp = new Position_Employee__c(Position__c = territory.Id, 
                                                               Effective_Start_Date__c = system.today().addMonths(-5), Effective_End_Date__c = Date.today().addMonths(5));
        insert posEmp;

       
        
        
        test.startTest();
        summaryWrapperList = ScenarioCtlr.getSummary(testTeamInstance.Id,workspace.ID);
        try{
            summaryWrapperList = ScenarioCtlr.getSummary('',workspace.ID);
        }catch(Exception e){}
        test.stopTest();
        
        system.assert(summaryWrapperList != null);
    }
    
    static testMethod void testCreateScenario() {

        Organization_Master__c org = new Organization_Master__c();
        org.Name = 'North America';
        org.Org_Level__c = 'Region';
        org.Parent_Country_Level__c = true;
        insert org;
        
        Country__c country = new Country__c();
        country.name ='USA';
        country.Country_Code__c = 'US';
        country.Status__c = 'Active';
        country.Parent_Organization__c = org.id;
        insert country;
        
        Geography_Type__c geotype = new Geography_Type__c();
        geotype.Name = 'ZIP';
        geotype.Country__c = country.id;
        insert geotype;

        Affiliation_Network__c affiliationNetwork = new Affiliation_Network__c();
        affiliationNetwork.name = 'IDN';
        affiliationNetwork.Country__c = country.id;
        insert affiliationNetwork;


        Workspace__c workspace = new Workspace__c(Name = 'test workspace',
                              Workspace_Start_Date__c = system.today().addMonths(-5),
                              Workspace_End_Date__c = system.today().addMonths(5),
                              Workspace_Description__c = 'Test Workspace');
        insert workspace;
        
        Team__c testTeam = new Team__c (Name='HTN', Type__c='Base', 
                        Effective_Start_Date__c = system.today().addMonths(-5), 
                        Effective_End_Date__c = Date.today().addMonths(5));
        insert testTeam;
        
        Team_Instance__c testTeamInstance = new Team_Instance__c();
        testTeamInstance.Name = 'HTN Q1';
        testTeamInstance.Alignment_Period__c = 'Operational';
        testTeamInstance.Team__c = testTeam.id;
        testTeamInstance.Alignment_Type__c = 'ZIP';
        testTeamInstance.isActiveCycle__c = 'Y';           
        insert testTeamInstance;
        String dataSourceWrapper = '[{"dataSetId":"a0i1N000007fYPqQAM","dataSetName":"ZIP-Terr","filePath":""},{"dataSetId":"a0i1N000007fYPsQAM","dataSetName":"Account Master","filePath":""}]';
        test.startTest();
        map<string,string> newScenario = ScenarioCtlr.createScenario(string.valueOf(workspace.Id), 'new scenario', string.valueOf(testTeam.Id), string.valueOf(testTeamInstance.Id),'','Alignment Scenario','Alignment Scenario',geotype.id,'',false,'','','',false);
        map<string,string> scenarioFromFile2 = ScenarioCtlr.createScenarioFromFile(string.valueOf(workspace.Id),'Scenario 111', string.valueOf(testTeam.Id), '','','Alignment Scenario','Alignment Scenario',testTeamInstance.Id,geotype.id,'','',dataSourceWrapper,true,true,false,false,'','');
        test.stopTest();
        
        TotalApproval__c ho = new TotalApproval__c();
        ho.Name = 'HO';
        ho.Subscriber_Profile__c = 'HO';
        insert ho;

        TotalApproval__c admin = new TotalApproval__c();
        admin.Name = 'System Administrator';
        admin.Subscriber_Profile__c = 'System Administrator';
        insert admin;
        
        Scenario__c result = [select Name, Scenario_Name__c, id from Scenario__c where Workspace__c =: workspace.Id limit 1];
        system.assertEquals(result.Scenario_Name__c,'new scenario');

        map<string,string> scenarioFromFile = ScenarioCtlr.createScenarioFromFile(string.valueOf(workspace.Id),'Scenario 111', string.valueOf(testTeam.Id), '','','Alignment Scenario','Alignment Scenario',testTeamInstance.Id,geotype.id,'','',dataSourceWrapper,false,true,false,false,'','');


        String scenarioValidationCPG = ScenarioCtlr.duplicateScenarioValidation(string.valueOf(testTeamInstance.Id), 'Scenario 12','Call Plan Generation', '',string.valueOf(country.id),false,string.valueOf(testTeam.id),string.valueOf(workspace.id));
        String scenarioValidation = ScenarioCtlr.duplicateScenarioValidation(string.valueOf(testTeamInstance.Id), 'Scenario 12','', '',String.valueOf(country.id),false,string.valueOf(testTeam.id),string.valueOf(workspace.id));
        //string Isscenario = ScenarioCtlr.getScenarioListByTeamInstance(testTeamInstance.id);

        

        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
         User tUser = new User(Alias = 'Rep', Email='repuser@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='repuserQuset@testorg.com');
        insert tUser;

        Alignment_Global_Settings__c settings = new Alignment_Global_Settings__c();
        settings.Name =  result.Name;
        settings.Tree_Hierarchy_Sort_Field__c = 'Call Plan Generation;Alignment Scenario';
        insert settings;

        Business_Rule_Template__c template = new Business_Rule_Template__c();
        template.Name = 'Alignment Template';
        insert template;

        BusinessRule_Template_Details__c businessRuletempDetails = new BusinessRule_Template_Details__c();
        businessRuletempDetails.Business_Rule_Template_Id__c = template.Id;
        businessRuletempDetails.Name = 'Customer Product Block';
        insert businessRuletempDetails;

        Data_Object__c dataObj = new Data_Object__c();
        dataObj.Name = 'File';
        insert dataObj;

        Data_Set__c dataSetObj = new Data_Set__c();
        dataSetObj.Name ='ZIP-Terr';
        dataSetObj.SalesIQ_Internal__c = false;
        dataSetObj.Data_Filter_Value__c = 'samplevalue';
        dataSetObj.is_internal__c = false;
        insert dataSetObj;

        Data_Set__c dataSetObj2 = new Data_Set__c();
        dataSetObj.Name ='ZIP-Terr';
        dataSetObj.SalesIQ_Internal__c = true;
        dataSetObj.Data_Filter_Value__c = 'samplevalue';
        dataSetObj.is_internal__c = true;
        insert dataSetObj2;

        Data_Set_Rule_Map__c dataSetRuleMap = new Data_Set_Rule_Map__c();
        dataSetRuleMap.ds_type__c = 'I';
        dataSetRuleMap.Business_Rule_Template_Details__c = businessRuletempDetails.Id;
        dataSetRuleMap.dataset_id__c = dataSetObj.Id;
        insert dataSetRuleMap;

        Data_Set_Rule_Map__c dataSetRuleMap2 = new Data_Set_Rule_Map__c();
        dataSetRuleMap.ds_type__c = 'I';
        dataSetRuleMap.Business_Rule_Template_Details__c = businessRuletempDetails.Id;
        dataSetRuleMap.dataset_id__c = dataSetObj2.Id;
        insert dataSetRuleMap2;

        System.debug('template -- '+template);

        ETL_Config__c etlObj1 = new ETL_Config__c();
        etlObj1.SF_UserName__c = 'abc';
        etlObj1.S3_Security_Token__c= 'abc';
        etlObj1.SF_Password__c= 'abc';
        etlObj1.BR_PG_Database__c= 'abc';
        insert etlObj1;
       // String dataSourceWrapper = '[{"dataSetId":"a0i1N000007fYPqQAM","dataSetName":"ZIP-Terr","filePath":""},{"dataSetId":"a0i1N000007fYPsQAM","dataSetName":"Account Master","filePath":""}]"';
        map<String,String> newScenarioFromRule = ScenarioCtlr.createScenarioFromRule(string.valueOf(workspace.Id),'Rule Scenario','This is a test Scenario', string.valueOf(testTeam.Id), string.valueOf(testTeamInstance.Id),dataObj.Id,'','Call Plan Generation','Create scenario from rule',template.Id);
        
        try{

            map<string,string> scenarioFromFileError = ScenarioCtlr.createScenarioFromFile(string.valueOf(workspace.Id),'Scenario 111', string.valueOf(testTeam.Id), '','','Alignment Scenario','Alignment Scenario','','','','',dataSourceWrapper,false,true,false,false,'','');
            map<string,string> newScenarioError = ScenarioCtlr.createScenario(string.valueOf(workspace.Id), 'new scenario', string.valueOf(testTeam.Id), '','','','','','',false,'','','',false);
        }catch(Exception e){}
        ScenarioCtlr.getCustomerMetricsOptions(template.Id);
        ScenarioCtlr.getLocaleDateFormats();
        ScenarioCtlr.getCreationTypeOptions(result.Name);
        ScenarioCtlr.logException('No Error Found', 'Scenario');
        ScenarioCtlr.getGeoLayerOptions(country.id);
        ScenarioCtlr.getAffiliationNetworkOptions(country.id);
        Team_Instance__c ExistingTeamInstanceAttributes = ScenarioCtlr.getExistingTeamInstanceAttributes(string.valueOf(testTeamInstance.Id));
        Business_Rule_Template__c brTemplateOpts =    ScenarioCtlr.getBusinessRuleTemplateOptionsFromTeamInstance(string.valueOf(testTeamInstance.Id));
        ScenarioCtlr.copyExternalDataSourcesInPostgres('SourceTableName','DestTableName',result.id);
        
       
        ComponentTypeMaster__c ctm = new ComponentTypeMaster__c();
        ctm.Name = 'Affiliation';
        insert ctm;

        Scenario_Rule_Instance_Details__c srid = new Scenario_Rule_Instance_Details__c();
        srid.Scenario_Id__c = result.id;
        srid.Component_Type_Master__c = ctm.id;
        insert srid;

        ScenarioCtlr.removeAffiliationComponents(true,result.id);


       
    }

    static testMethod void testGetBusinessRuleTemplate(){
        Business_Rule_Template__c template = new Business_Rule_Template__c();
        template.Name = 'Alignment Template';
        insert template;

        ScenarioCtlr.getBusinessRuleTemplateOptions();
        ScenarioCtlr.getFileMetaDataOptions(template.Id);
        try{
            ScenarioCtlr.getDocumentTemplate('Call Plan Colaboration');
        }catch(Exception e){

        }
    }
    @isTest(SeeAllData=true)
    static void testGetBusinessRuleTemplate1(){
        try{
            ScenarioCtlr.getDocumentTemplate('Call Plan Generation');
            etlObj = CPGUtility.getETLConfigByName(CPGConstants.talend_Instance);
            
        }catch(Exception e){

        }
    }

    static testMethod void testScenarioWrappers() {
        test.startTest();
        ScenarioWrapper scWrap = new ScenarioWrapper(system.today().addMonths(-1),system.today().addMonths(1));
        SummaryWrapper smWrap = new SummaryWrapper('No of Accounts',10);
        test.stopTest();
    }


    static testMethod void testConvergesLevel() {

        test.startTest();

         Workspace__c workspace = new Workspace__c(Name = 'test workspace',
                              Workspace_Start_Date__c = system.today().addMonths(-5),
                              Workspace_End_Date__c = system.today().addMonths(5),
                              Workspace_Description__c = 'Test Workspace');
        insert workspace;

        Team__c testTeam = new Team__c (Name='HTN', Type__c='Base', 
                        Effective_Start_Date__c = system.today().addMonths(-5), 
                        Effective_End_Date__c = Date.today().addMonths(5));
        insert testTeam;
        
        Team_Instance__c testTeamInstance = new Team_Instance__c();
        testTeamInstance.Name = 'HTN Q1';
        testTeamInstance.Alignment_Period__c = 'Operational';
        testTeamInstance.Team__c = testTeam.id;
        testTeamInstance.Alignment_Type__c = 'ZIP';
        testTeamInstance.isActiveCycle__c = 'Y';           
        insert testTeamInstance;

        list<CR_Team_Instance_Config__c> lstConfig = new list<CR_Team_Instance_Config__c>();
        
        CR_Team_Instance_Config__c configData1 = new CR_Team_Instance_Config__c(Configuration_Value__c = 'Territory', Configuration_Name__c = '1', 
                                                                                Configuration_Type__c = SalesIQGlobalConstants.HIERARCHY_CONFIG, Team_Instance__c = testTeamInstance.Id);
        
        CR_Team_Instance_Config__c configData2 = new CR_Team_Instance_Config__c(Configuration_Value__c = 'District', Configuration_Name__c = '2', 
                                                                                Configuration_Type__c = SalesIQGlobalConstants.HIERARCHY_CONFIG, Team_Instance__c = testTeamInstance.Id);
        lstConfig.add(configData1);
        lstConfig.add(configData2);

        insert lstConfig;
        
                
        list<CR_Team_Instance_Config__c> summaryWrapperList = ScenarioCtlr.getConvergesLevel(testTeamInstance.Id);           
       
        test.stopTest();
        
        system.assert(summaryWrapperList != null);
    }


    static testMethod void testTeamInstanceByBaseTeam() {

        test.startTest();

         Workspace__c workspace = new Workspace__c(Name = 'test workspace',
                              Workspace_Start_Date__c = system.today().addMonths(-5),
                              Workspace_End_Date__c = system.today().addMonths(5),
                              Workspace_Description__c = 'Test Workspace');
        insert workspace;

        Team__c testTeam = new Team__c (Name='HTN', Type__c='Base', 
                        Effective_Start_Date__c = system.today().addMonths(-5), 
                        Effective_End_Date__c = Date.today().addMonths(5));
        insert testTeam;        
        
        Team_Instance__c testTeamInstance = new Team_Instance__c();
        testTeamInstance.Name = 'HTN Q1';
        testTeamInstance.Alignment_Period__c = 'Operational';
        testTeamInstance.Team__c = testTeam.id;
        testTeamInstance.Alignment_Type__c = 'ZIP';
        testTeamInstance.isActiveCycle__c = 'Y';           
        insert testTeamInstance;

        Team__c ChildtestTeam = new Team__c (Name='HTN', Type__c='Base', 
                        Effective_Start_Date__c = system.today().addMonths(-5), 
                        Effective_End_Date__c = Date.today().addMonths(5), Controlling_Team__c=testTeam.id);
        insert ChildtestTeam;

        Team_Instance__c childtestTeamInstance = new Team_Instance__c();
        childtestTeamInstance.Name = 'HTN Q1';
        childtestTeamInstance.Alignment_Period__c = 'Operational';
        childtestTeamInstance.Team__c = ChildtestTeam.id;
        childtestTeamInstance.Alignment_Type__c = 'ZIP';
        childtestTeamInstance.isActiveCycle__c = 'Y';           
        insert childtestTeamInstance;       
                
        map<string,string> summaryWrapperList = ScenarioCtlr.getTeamInstanceByBaseTeam(ChildtestTeam.Id);           
       
        test.stopTest();
        
        system.assert(summaryWrapperList != null);
    }

    
}