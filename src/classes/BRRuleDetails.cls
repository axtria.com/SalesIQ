public class BRRuleDetails{
    @AuraEnabled
    public list<business_rule_type_master__c> ruleType {get; set;}  
    @AuraEnabled
    public String selRuleType {get; set;}  
    @AuraEnabled
    public String ruleName {get; set;} 
	@AuraEnabled
    public list<String> level {get; set;}  
    @AuraEnabled
    public String selLevel {get; set;}  
    @AuraEnabled
    public list<String> custPrior {get; set;}  
    @AuraEnabled
    public String selCustPrior {get; set;}  
}