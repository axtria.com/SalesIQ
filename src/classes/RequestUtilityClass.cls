/******************************************
Name		:	RequesttUtility.cls
Description	:	Utility class for Request and Approve.
Created by  : 	Praveen Kumar A0616
******************************************/

public with sharing class RequestUtilityClass {	
	/*
	 * This method will return the list of wrapper of CIM value(Before and after) based on position team instance from CIM_Position_Metric_Summary__c.
	 * CIM_Position_Metric_Summary__c obaject has pre calculated value of Position of different different CIM
	 * @param  Wrapper of CIM values of Source Position
	 * @param  Wrapper of CIM values of Destination Position	 
     * @return list of wrrapper RequestCIMWrapper 
     */ 
	
	public static Map<String, List<RequestCIMWrapper>> getCIMKPIDatabyPositionTeamInstance(Set<id> cimIds, Set<id> positionTeamInstanceIds, Map<string,string> ImpactValue, String sourcePositionTeamInstances, String reqStatus, string destinationPositionTeamInsatnce){   
	    Map<String, List<RequestCIMWrapper>> lstResquestKPIData = new Map<String, List<RequestCIMWrapper>>();
		list<CIM_Position_Metric_Summary__c> latRequestSUmmary;
		
		list<String> CIM_POSITION_SUMMARY_READ_FIELD = new List<String>{'id','CIM_Config__c','Original__c','Optimum__c','Approved__c','Proposed__c','Position_Team_Instance__c'};
		list<String> CIM_CONFIG_READ_FIELD  = new List<String>{'isOptimum__c'};
		
		if (SecurityUtil.checkRead(CIM_Position_Metric_Summary__c.SObjectType, CIM_POSITION_SUMMARY_READ_FIELD, false) && 
		    SecurityUtil.checkRead(Position_Team_Instance__c.SObjectType, new List<String>{'Position_ID__c'}, false) &&
		    SecurityUtil.checkRead(Position__c.SObjectType, new List<String>{'name', 'Related_Position_Type__c'} , false) &&
		    SecurityUtil.checkRead(CIM_Config__c.SObjectType, CIM_CONFIG_READ_FIELD, false))
		{
			/*
			latRequestSUmmary=[select id,CIM_Config__c,Original__c, Approved__c,Proposed__c,Position_Team_Instance__r.Position_ID__r.Name, Position_Team_Instance__r.Position_ID__r.Related_Position_Type__c from CIM_Position_Metric_Summary__c where CIM_Config__c in :CIMid and Position_Team_Instance__c =: PositionTeamInstanceID order by CIM_Config__c asc];
			*/

			latRequestSUmmary=[select id,CIM_Config__c,CIM_Config__r.isOptimum__c,Original__c, Optimum__c,Approved__c,Proposed__c,Position_Team_Instance__r.Position_ID__r.Name, Position_Team_Instance__r.Position_ID__r.Related_Position_Type__c from CIM_Position_Metric_Summary__c where CIM_Config__c in :cimIds and Position_Team_Instance__c in :positionTeamInstanceIds order by CIM_Config__r.Attribute_API_Name__c asc];
			
		}
	
		
		if(latRequestSUmmary.size() >0){
			
			string sPositionMessage='';
            for(CIM_Position_Metric_Summary__c sumary:latRequestSUmmary){
            	//SIMPS-207 Change impact should be negative for source
               string sign='';
               decimal Current;
               decimal Impact;
               decimal Original = double.valueof(sumary.Original__c);
               RequestCIMWrapper objWraper = new RequestCIMWrapper();              
               if(sumary.CIM_Config__r.isOptimum__c){               		
               		if(sumary.Optimum__c!=null){
               			Current = double.valueof(sumary.Optimum__c);
               		}else{
               			Current = 0;
               		}	                
	                
               }else{
                    Current = double.valueof(sumary.Original__c);
               }
               if(Alignment_Global_Settings__c.getValues('CIMColumnConfiguration') != null && Alignment_Global_Settings__c.getValues('CIMColumnConfiguration').Tree_Hierarchy_Sort_Field__c == 'Proposed'){               	 
                 Impact  = double.valueof(sumary.Proposed__c);
                 if(sourcePositionTeamInstances == sumary.Position_Team_Instance__c){    				        				
    				sign='-';
        		 }
               }else{               	
        		Impact  = double.valueof(sumary.Approved__c);
        		//arun added below condition
        		//if source overlay and destination overlay same, then there is no impact
        		decimal CIMImpactedValue =0;
        		if(destinationPositionTeamInsatnce!=sourcePositionTeamInstances){
        		     CIMImpactedValue = double.valueof(ImpactValue.get(sumary.CIM_Config__c));
        		}

        		        		
        		if(ImpactValue.get(sumary.CIM_Config__c).contains('-')){
        			if(reqStatus != SalesIQGlobalConstants.REQUEST_STATUS_APPROVED){
	        			if(sourcePositionTeamInstances == sumary.Position_Team_Instance__c){
	        				//sPositionMessage='below';
	        				Impact = Impact+CIMImpactedValue;
	        				sign='-';
		        		}
		        		else{
		        			//sPositionMessage='above';
		        			Impact = Impact-CIMImpactedValue;
		        		}
	        		}
        		}
        		else{
        			if(reqStatus != SalesIQGlobalConstants.REQUEST_STATUS_APPROVED){
	        			if(sourcePositionTeamInstances == sumary.Position_Team_Instance__c){
	        				
	        				Impact = Impact-CIMImpactedValue;
	        				sign='-';
		        		}
		        		else{
		        			
		        			Impact = Impact+CIMImpactedValue;
		        		}
	        		}else{
	        		    if(sourcePositionTeamInstances == sumary.Position_Team_Instance__c){ 
        			        sign='-';
	        		    }
	        		}
        		}
                }
        		objWraper.Original = Original.setscale(SalesIQGlobalConstants.SCALE_VALUE);
               	objWraper.Current =Current.setscale(SalesIQGlobalConstants.SCALE_VALUE);
               	objWraper.Impact = Impact.setscale(SalesIQGlobalConstants.SCALE_VALUE);
                string strNetImpact = string.valueof(ImpactValue.get(sumary.CIM_Config__c));
                decimal Removenegetivesign ;
                if(strNetImpact.contains('-')){
                	sPositionMessage=System.Label.Tooltip_Guardrail_Below;
                	Removenegetivesign = double.valueof(strNetImpact.replace('-',''));
                	//Removenegetivesign = double.valueof(strNetImpact);
                }else{
                	sPositionMessage=System.Label.Tooltip_Guardrail_Above;
                    Removenegetivesign = double.valueof(ImpactValue.get(sumary.CIM_Config__c));
                	//Removenegetivesign = Removenegetivesign.setscale(SalesIQGlobalConstants.SCALE_VALUE);
                }
                decimal concatinatesign = double.valueof(sign+''+Removenegetivesign);                
                
                objWraper.NetImpact = concatinatesign.setscale(SalesIQGlobalConstants.SCALE_VALUE);
                
                objWraper.TextColor = AlignmentUtility.GraudrailColor(double.valueof(objWraper.Current) ,double.valueof(objWraper.Impact), getCIMConfigDatabyCIMID(sumary.CIM_Config__c));
                objWraper.ErrorType = AlignmentUtility.GraudrailType(objWraper.TextColor);   
                objWraper.graudrailToolTipMessage = AlignmentUtility.generateGrauidrailTooltipMessage(objWraper.textColor,sPositionMessage,objWraper.Current, objWraper.Impact,getCIMConfigDatabyCIMID(sumary.CIM_Config__c));
                objWraper.graudrailRange = AlignmentUtility.generateGraudrailRange(getCIMConfigDatabyCIMID(sumary.CIM_Config__c));
                objWraper.positionName =  sumary.Position_Team_Instance__r.Position_ID__r.Name ;    
                objWraper.graudrailHeader = AlignmentUtility.GraudrailHeader(objWraper.TextColor);   

                objWraper.cultureOriginal  = objWraper.Original.format();
                objWraper.cultureNetImpact  = objWraper.NetImpact.format();
                objWraper.cultureImpact  = objWraper.Impact.format();




               
				if(lstResquestKPIData.containsKey(sumary.Position_Team_Instance__c)) {
					lstResquestKPIData.get(sumary.Position_Team_Instance__c).add(objWraper);
               	} else {
               		lstResquestKPIData.put(sumary.Position_Team_Instance__c, new List<RequestCIMWrapper>{objWraper});
               	}
            }
		}
		return lstResquestKPIData;
	}
	
	/*
	 * This method will return the CIM Object based on CIM id	 
	 * @param  Wrapper of CIM values of Source Position 
     * @return attributes of requested CIM 
     */ 
	
	public static CIM_Config__c getCIMConfigDatabyCIMID(string CIMID){
		list<string> CIM_CONFIG_READ_FIELD = new list<string>{'id','Threshold_Warning_Min__c','Threshold_Warning_Max__c','Threshold_Min__c', 'Threshold_Max__c','Acceptable_Max__c','Acceptable_Min__c'};
		CIM_Config__c ConfigData = null;
		if(SecurityUtil.checkRead(CIM_Config__c.SObjectType, CIM_CONFIG_READ_FIELD, false)){
			ConfigData  = [select id,Threshold_Warning_Min__c,Threshold_Warning_Max__c,Threshold_Min__c, Threshold_Max__c,Acceptable_Max__c,Acceptable_Min__c,MetricCalculationType__c,Display_Name__c,isOptimum__c from CIM_Config__c where id=:CIMID ];
		}
		
				
		return ConfigData;
	}	

	
	/*
	 * This method will return list of CIM_Change_Request_Impact__c object based on Change request salesforce id	
	 * @param string of Chnage Request ID
     * @return list of CIM_Change_Request_Impact__c
     */ 
	public static list<CIM_Change_Request_Impact__c> getChangeRequestImpactData(string ChangeRequestId, string PositionTeamInstance){
		list<CIM_Change_Request_Impact__c> lstRequestImpact = new list<CIM_Change_Request_Impact__c>();
		List<String> CIM_CHANGE_REQUEST_IMPACT_READ_FIELD = new List<String>{'id','Position_Team_Instance__c','CIM_Config__c','Change_Request__c','Impact__c', 'Team_Instance__c', 'Position__c'};	
	    List<String> CHANGE_REQUEST_READ_FIELD = new List<String>{'Status__c','Source_Positions__c','Destination_Positions__c','Source_Position__c','Destination_Position__c'};
	    list<string> POSITION_READ_FIELD = new list<string>{'name'};
	    
	    
		if (SecurityUtil.checkRead(Position__c.SObjectType, POSITION_READ_FIELD, false) && SecurityUtil.checkRead(CIM_Change_Request_Impact__c.SObjectType, CIM_CHANGE_REQUEST_IMPACT_READ_FIELD, false) &&
		   SecurityUtil.checkRead(Change_Request__c.SObjectType, CHANGE_REQUEST_READ_FIELD, false) &&
		   SecurityUtil.checkRead(CIM_Config__c.SObjectType, new List<String>{'display_name__c'}, false)){
		   	if(PositionTeamInstance!=''){
		   		lstRequestImpact = [select id,CIM_Config__c, CIM_Config__r.display_Name__c, CIM_Config__r.Team_Instance__r.Team__r.Type__c, Impact__c, Team_Instance__c, Position__c, Position_Team_Instance__c, Position_Team_Instance__r.Team_Instance_ID__c,Change_Request__r.AllZips__c from CIM_Change_Request_Impact__c where Change_Request__c=:ChangeRequestId and Position_Team_Instance__c=:PositionTeamInstance and CIM_Config__r.Is_Custom_Metric__c  =false and CIM_config__r.Hierarchy_Level__c = '1'];
		   	}else{
		   		lstRequestImpact = [select id, CIM_Config__c,Team_Instance__c, Team_Instance__r.Name, CIM_Config__r.Team_Instance__r.Team__r.Type__c,Team_Instance__r.Team_Cycle_Name__c,CIM_Config__r.display_name__c,CIM_Config__r.Attribute_API_Name__c, Position_Team_Instance__r.Team_Instance_ID__c,Position_Team_Instance__c,Change_Request__r.Source_Positions__c,Change_Request__r.Destination_Positions__c,Impact__c,Change_Request__r.Source_Position__r.Name,Change_Request__r.Destination_Position__r.Name, Change_Request__r.Status__c, Position__c,Change_Request__r.AllZips__c,Change_Request__r.Team_Instance_id__c,Change_Request__r.Team_Instance_id__r.Show_Custom_Metric__c,Change_Request__r.Request_Type_Change__c,Change_Request__r.Team_Instance_id__r.Restrict_ZIP_Share__c,Change_Request__r.Point_Zips__c, Change_Request__r.Team_Instance_id__r.CIM_Available__c from CIM_Change_Request_Impact__c where CIM_Config__r.Is_Custom_Metric__c  =false and  Change_Request__c =: String.escapeSingleQuotes(ChangeRequestId) and CIM_config__r.Hierarchy_Level__c = '1'  order by CIM_Config__r.Attribute_API_Name__c asc ]; 
		   	}
			
		}
		return lstRequestImpact;
	}
	
	
	public static list<CIM_Change_Request_Impact__c> getChangeRequestImpactData(list<string> RequestID, set<id> PositionTeamInstance){
		list<CIM_Change_Request_Impact__c> lstRequestImpact = new list<CIM_Change_Request_Impact__c>();
		list<String> CIM_CHANGE_REQUEST_IMPACT_READ_FIELD = new List<String>{'id','Position_Team_Instance__c','CIM_Config__c','Change_Request__c','Impact__c'};	
	    if(SecurityUtil.checkRead(CIM_Change_Request_Impact__c.SObjectType, CIM_CHANGE_REQUEST_IMPACT_READ_FIELD, false)){
	    	lstRequestImpact = [select id,CIM_Config__c, Impact__c, CIM_Config__r.Team_Instance__r.Team__r.Type__c,Position_Team_Instance__c,Position_Team_Instance__r.Team_Instance_ID__c from CIM_Change_Request_Impact__c where Change_Request__c in:RequestID and Position_Team_Instance__c in:PositionTeamInstance ];
	    }
	    
	    	    
	    return lstRequestImpact;
		
	}

	public static list<CIM_Change_Request_Impact__c> getChangeRequestImpactData(list<string> RequestID){
		list<CIM_Change_Request_Impact__c> lstRequestImpact = new list<CIM_Change_Request_Impact__c>();
		list<String> CIM_CHANGE_REQUEST_IMPACT_READ_FIELD = new List<String>{'id','Position_Team_Instance__c','CIM_Config__c','Change_Request__c','Impact__c'};	
	    if(SecurityUtil.checkRead(CIM_Change_Request_Impact__c.SObjectType, CIM_CHANGE_REQUEST_IMPACT_READ_FIELD, false)){
	    	lstRequestImpact = [select id,CIM_Config__c,CIM_Config__r.Metric_Name__c, Impact__c, Position_Team_Instance__c,Position_Team_Instance__r.Team_Instance_ID__c,Change_Request__r.Source_Position__c,Change_Request__r.Destination_Position__c,Position_Team_Instance__r.Position_id__c,Change_Request__r.Status__c,Position_Team_Instance__r.Position_id__r.Name,Change_Request__r.Is_Auto_Approved__c, Change_Request__r.Position_Rules_Mismatch__c from CIM_Change_Request_Impact__c where Change_Request__c in:RequestID ];
	    }
	    return lstRequestImpact;
	}	

    
	public static list<Change_Request__c> getChangeRequestByRequestID(list<string> RequestID){
		list<Change_Request__c> lstChangeReq = new list<Change_Request__c>();
		List<String> CHANGE_REQUEST_READ_FIELD = new List<String>{'Account_Moved_Id__c','Team_Instance_ID__c','Request_Type_Change__c','Source_Positions__c','Destination_Positions__c','name','Status__c','AllZips__c','Source_Position__c','Destination_Position__c'};
		List<String> POSITION_READ_FIELD = new List<String>{'Client_Position_Code__c','name','Parent_Position__c'};
		
		if (SecurityUtil.checkRead(Change_Request__c.SObjectType, CHANGE_REQUEST_READ_FIELD, false) &&
		   SecurityUtil.checkRead(Position__c.SObjectType, POSITION_READ_FIELD, false))
		{
			lstChangeReq  = [select c.id,c.name,c.Request_Type_Change__c,c.Account_Moved_Id__c, c.Status__c,c.Team_Instance_ID__c,
							 c.Team_Instance_ID__r.Name, c.Team_Instance_ID__r.Team_Instance_Code__c, c.AllZips__c, c.Source_Position__r.Client_Position_Code__c,
							 c.Source_Position__r.Name, c.Source_Position__c, c.Source_Position__r.Parent_Position__r.Name, c.Source_Position__r.Parent_Position__r.Client_Position_Code__c, 
							 c.Source_Position__r.Parent_Position__r.Parent_Position__r.Name, c.Source_Position__r.Parent_Position__r.Parent_Position__r.Client_Position_Code__c,
							 c.Destination_Position__r.Client_Position_Code__c, c.Destination_Position__r.Parent_Position__c, c.Destination_Position__r.Name,
							 c.Destination_Position__c,  c.Destination_Position__r.Parent_Position__r.Name, c.Destination_Position__r.Parent_Position__r.Client_Position_Code__c,
							 c.Destination_Position__r.Parent_Position__r.Parent_Position__r.Name, c.Destination_Position__r.Parent_Position__r.Parent_Position__r.Client_Position_Code__c,
							 Team_Instance_ID__r.Alignment_Type__c,Team_Instance_ID__r.Alignment_Period__c,Team_Instance_ID__r.Geography_Type_Name__r.Geography_Layer__c,Team_Instance_ID__r.Geography_Type_Name__r.Geography_End_Point__c,
							 Team_Instance_ID__r.Geography_Type_Name__r.Geography_Table__c,Team_Instance_ID__r.Geography_Type_Name__r.City_Layer__c,Team_Instance_ID__r.Geography_Type_Name__r.Shape_Available__c,Team_Instance_ID__r.Geography_Type_Name__r.Account_Shape_Available__c,
							 Team_Instance_ID__r.Team__r.Country__r.Country_Code__c from Change_Request__c c where id in:RequestID];
		}
		return lstChangeReq;
	}
	

	/*
	 * This method will related position map by base position team instance id	
	 * @param   string of Chnage Request ID
     * @return  list of CIM_Change_Request_Impact__c
     * @author  aditi
     */
	public static Map<Id, Related_Position__c> getRelatesPositionsGroupByPositionTeamInstance(Set<Id> positionIds, String selectedTeamInstance) {

		List<String> RELATED_POSITION_READ_FIELDS = new List<String>{'Base_Position_Team_Instance__c', 'Related_Position_Team_Instance__c'} ;

		Map<Id, Related_Position__c> overlayPositionsMapByBasePosition = new Map<Id, Related_Position__c>();
        if(SecurityUtil.checkRead(Related_Position__c.SObjectType, RELATED_POSITION_READ_FIELDS, false)){
            
            // fetch all the related overlay positions from Source and Destnation base position if any
            for(Related_Position__c relatedPos : [Select Base_Position_Team_Instance__c, Related_Position_Team_Instance__c
                                                from Related_Position__c 
                                                where isActive__c = true 
                                                and Base_Position__c in :positionIds
                                                and Related_Position_Team_Instance__r.Team_Instance_ID__c =: selectedTeamInstance ]) {

                // Add base position as key and overlay position
                overlayPositionsMapByBasePosition.put(relatedPos.Base_Position_Team_Instance__c, relatedPos) ;
            }
        } 
        return overlayPositionsMapByBasePosition ;
	}
	
	public class RequestCIMWrapper{
        public Decimal Current{get;set;}
        public Decimal Original{get;set;}
        public Decimal Impact{get;set;}
        public Decimal NetImpact{get;set;}//This will 
        public String TextColor{get;set;} 
        public String ErrorType{get;set;}   
        public String graudrailToolTipMessage{get;set;}
        public String positionName {get; set;}    
        public String graudrailRange {get;set;}    
        public string graudrailHeader {get;set;}  
        //below variables is used in order to display the orignal/current/impacted values based on culture
        public string cultureNetImpact{get;set;}
        public string cultureOriginal {get;set;} 
        public string cultureImpact {get;set;}
        
    }
    
   
    public class GoogleChartDataWrapper{
    	public String timetype{get;set;}//it will contain Before/After
    	public String value{get;set;}
    	public String color{get;set;}
    	public GoogleChartDataWrapper(String timetype,String value,String color){
    		this.timetype = timetype;
    		this.value = value;
    		this.color = color;
    	}
    }
        
	
}