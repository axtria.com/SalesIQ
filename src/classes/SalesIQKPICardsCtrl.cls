public class SalesIQKPICardsCtrl 
{
	public static final String namespace = SalesIQGlobalConstants.Name_space;

	@AuraEnabled
    public static List<SalesIQKPICardsWrapper> getKPICardsDetail(String crType, String selectedTeamInstance,String selectedPosition, String recordId, String countryId) 
    {
        list<Team_Instance__c> callPlanTeamInstances = new List<Team_Instance__c>();
        if(recordId != null && recordId != '')
        {
            Change_Request__c selectedCR = [Select Id,Name,Team_Instance_ID__c,Source_Position__c,Destination_Position__c from Change_Request__c where Id =: recordId];
            selectedTeamInstance = selectedCR.Team_Instance_ID__c;
            selectedPosition = selectedCR.Destination_Position__c;
        }
        
        System.debug('selectedPosition1:::::' + selectedPosition);
        System.debug('countryId:::::' + countryId);
        

        //@todo: KPI card should also get country Id too like all other controllers
        //String countryId = SalesIQUtility.getDefaultCountryID(UserInfo.getUserId());

    	if(selectedTeamInstance == '' || selectedTeamInstance == null)
    	{
    		callPlanTeamInstances = SalesIQUtility.getUserAccessTeamInstance('', SalesIQGlobalConstants.CALL_PLAN_MODULE,countryId);
            if(callPlanTeamInstances.size() < 1)
            {
                AuraHandledException ex = new AuraHandledException(System.Label.No_UAP_Assigned);
                ex.setMessage(System.Label.No_UAP_Assigned);
                throw ex;
            }

            selectedTeamInstance = callPlanTeamInstances[0].Id;
            selectedPosition = callPlanTeamInstances[0].User_Access_Permissions__r[0].Position__c;
            List<Position__c> territoryPositions = SalesIQUtility.getPositionWithSortFieldDynamic('1', selectedPosition, true, 1);
            
            selectedPosition = territoryPositions[0].Id;
        }    
        else
        {
            callPlanTeamInstances = SalesIQUtility.getUserAccessTeamInstance(selectedTeamInstance, SalesIQGlobalConstants.CALL_PLAN_MODULE, countryId);
        	if(selectedPosition == null || selectedPosition == '')
            {
        		selectedPosition = callPlanTeamInstances[0].User_Access_Permissions__r[0].Position__c;
                List<Position__c> territoryPositions = SalesIQUtility.getPositionWithSortFieldDynamic('1', selectedPosition, true, 1);
            
                selectedPosition = territoryPositions[0].Id;  
            }
        }
        if(!callPlanTeamInstances[0].Scenarios__r[0].KPI_Card_Guardrail_Config__c)
            return null;
        
    	String ns = SalesIQGlobalConstants.NAME_SPACE;

    	List<CIM_Config__c> cimConfigList = [SELECT Change_Request_Type__c,Threshold_Warning_Max__c,Threshold_Warning_Min__c,Display_Name__c,Enable__c,Name,Team_Instance__c,Threshold_Max__c,MetricCalculationType__c,Threshold_Min__c FROM CIM_Config__c WHERE Change_Request_Type__r.CR_Type_Name__c = 'Call_Plan_Change' AND Team_Instance__c =: selectedTeamInstance AND Enable__c = true];
    	Map<Id,CIM_Config__c> CIMIds = new Map<Id,CIM_Config__c>();
    	for(CIM_Config__c cimRecord : CIMConfigList)
    	{
    		CIMIds.put(cimRecord.Id,cimRecord);
    	}
		
		List<SalesIQKPICardsWrapper> wrapperList = new List<SalesIQKPICardsWrapper>();

		List<CIM_Position_Metric_Summary__c> metricSummaryList = [SELECT Approved__c,CIM_Config__c,Id,Name,Original__c,Position_Team_Instance__c,Position_Team_Instance__r.Position_ID__r.Name,Proposed__c,CIM_Config__r.Display_Name__c,CIM_Config__r.Metric_Name__c,CIM_Config__r.Threshold_Max__c,CIM_Config__r.Threshold_Min__c,CIM_Config__r.Threshold_Warning_Max__c,CIM_Config__r.Threshold_Warning_Min__c,CIM_Config__r.isOptimum__c,
            CIM_Config__r.MetricCalculationType__c, Optimum__c FROM CIM_Position_Metric_Summary__c where CIM_Config__c in: cimConfigList and Position_Team_Instance__r.Position_ID__c =: selectedPosition];  

		for(CIM_Position_Metric_Summary__c metricSummary : metricSummaryList)
		{
			SalesIQKPICardsWrapper wrapperRecord = new SalesIQKPICardsWrapper();
			wrapperRecord.color = AlignmentUtility.GraudrailColor(Decimal.valueOf(metricSummary.Original__c),Decimal.valueOf(metricSummary.Proposed__c),CIMIds.get(metricSummary.CIM_Config__c));
			wrapperRecord.guardrailType = AlignmentUtility.GraudrailType(wrapperRecord.color);
			wrapperRecord.metricName = String.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'Display_Name__c'));
            String tooltipMessage = System.label.Kpi_Cards_Warning_Message;


            if(Boolean.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'isOptimum__c')) == true && String.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'MetricCalculationType__c')) == SalesIQGlobalConstants.ABSOLUTE)
            {
                wrapperRecord.originalValue = Decimal.valueOf(String.valueOf(metricSummary.Optimum__c));
                wrapperRecord.thresholdMin = ((Decimal.valueOf(String.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'Threshold_Min__c'))) - Decimal.valueOf(String.valueOf(metricSummary.Optimum__c)))/(Decimal.valueOf(String.valueOf(metricSummary.Optimum__c))))*100;
                wrapperRecord.thresholdMax = ((Decimal.valueOf(String.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'Threshold_Max__c'))) - Decimal.valueOf(String.valueOf(metricSummary.Optimum__c)))/(Decimal.valueOf(String.valueOf(metricSummary.Optimum__c))))*100;
                wrapperRecord.thresholdWarningMin = ((Decimal.valueOf(String.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'Threshold_Warning_Min__c'))) - Decimal.valueOf(String.valueOf(metricSummary.Optimum__c)))/(Decimal.valueOf(String.valueOf(metricSummary.Optimum__c))))*100; 
                wrapperRecord.thresholdWarningMax = ((Decimal.valueOf(String.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'Threshold_Warning_Max__c'))) - Decimal.valueOf(String.valueOf(metricSummary.Optimum__c)))/(Decimal.valueOf(String.valueOf(metricSummary.Optimum__c))))*100;
            }
            else
            {
                wrapperRecord.originalValue = Decimal.valueOf(String.valueOf(metricSummary.Original__c));
                if(String.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'MetricCalculationType__c')) == SalesIQGlobalConstants.ABSOLUTE)
                {
                    wrapperRecord.thresholdMin = ((Decimal.valueOf(String.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'Threshold_Min__c'))) - Decimal.valueOf(String.valueOf(metricSummary.Original__c)))/(Decimal.valueOf(String.valueOf(metricSummary.Original__c))))*100;
                    wrapperRecord.thresholdMax = ((Decimal.valueOf(String.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'Threshold_Max__c'))) - Decimal.valueOf(String.valueOf(metricSummary.Original__c)))/(Decimal.valueOf(String.valueOf(metricSummary.Original__c))))*100;
                    wrapperRecord.thresholdWarningMin = ((Decimal.valueOf(String.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'Threshold_Warning_Min__c'))) - Decimal.valueOf(String.valueOf(metricSummary.Original__c)))/(Decimal.valueOf(String.valueOf(metricSummary.Original__c))))*100; 
                    wrapperRecord.thresholdWarningMax = ((Decimal.valueOf(String.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'Threshold_Warning_Max__c'))) - Decimal.valueOf(String.valueOf(metricSummary.Original__c)))/(Decimal.valueOf(String.valueOf(metricSummary.Original__c))))*100;
                }
                else
                {
                    wrapperRecord.thresholdMin = Decimal.valueOf(String.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'Threshold_Min__c')));
                    wrapperRecord.thresholdMax = Decimal.valueOf(String.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'Threshold_Max__c')));
                    wrapperRecord.thresholdWarningMin = Decimal.valueOf(String.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'Threshold_Warning_Min__c')));   
                    wrapperRecord.thresholdWarningMax = Decimal.valueOf(String.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'Threshold_Warning_Max__c')));
                }
            }
			
			wrapperRecord.approvedValue = Decimal.valueOf(String.valueOf(metricSummary.Approved__c));
			wrapperRecord.proposedValue = Decimal.valueOf(String.valueOf(metricSummary.Proposed__c));
			
			wrapperRecord.position = String.valueOf(metricSummary.getSObject(ns+'Position_Team_Instance__r').getSObject(ns+'Position_ID__r').get('Name'));

            // if(wrapperRecord.guardrailType == SalesIQGlobalConstants.WARNING)
            // {
            //     wrapperRecord.tooltipMessage = String.format(tooltipMessage, new List<String>{String.valueOf(wrapperRecord.thresholdWarningMax),'warning'});
            // }
            // else if(wrapperRecord.guardrailType == SalesIQGlobalConstants.ERROR)
            // {
            //     wrapperRecord.tooltipMessage = String.format(tooltipMessage, new List<String>{String.valueOf(wrapperRecord.thresholdMax),'error'});
            // }
            // else 
            // {
            //     wrapperRecord.tooltipMessage = '';
            // }
            if(Decimal.valueOf(String.valueOf(metricSummary.Original__c)) == 0)
            {
                wrapperRecord.tooltipMessage = '';
            }
            else
            {
                String tempMessage;
                tempMessage = String.format(tooltipMessage, new List<String>{String.valueOf(metricSummary.getSObject(ns+'CIM_Config__r').get(ns+'Display_Name__c')),String.valueOf(Math.abs(Math.round((Decimal.valueOf(String.valueOf(metricSummary.Proposed__c)) - Decimal.valueOf(String.valueOf(metricSummary.Original__c)))*100/Decimal.valueOf(String.valueOf(metricSummary.Original__c))))),String.valueOf(Math.round(Decimal.valueOf(metricSummary.Original__c)))});
                if(Math.round(Decimal.valueOf(String.valueOf(metricSummary.Proposed__c))) < Math.round(Decimal.valueOf(String.valueOf(metricSummary.Original__c))))
                {
                   tempMessage = tempMessage.replace('above', 'below');
                }
                wrapperRecord.tooltipMessage = tempMessage;
            }
			wrapperList.add(wrapperRecord);
		}	

    	return wrapperList;
    }

    public class SalesIQKPICardsWrapper  
    {
        @AuraEnabled
        public String metricName {get;set;}
        @AuraEnabled
        public Decimal originalValue {get;set;}
        @AuraEnabled
        public Decimal approvedValue {get;set;}
        @AuraEnabled
        public Decimal proposedValue {get;set;}
        @AuraEnabled
        public String position {get;set;}
        @AuraEnabled
        public Decimal thresholdMin {get;set;}
        @AuraEnabled
        public Decimal thresholdMax {get;set;}
        @AuraEnabled
        public Decimal thresholdWarningMin {get;set;}
        @AuraEnabled
        public Decimal thresholdWarningMax {get;set;}
        @AuraEnabled
        public String color{get;set;}
        @AuraEnabled
        public String guardrailType{get;set;}
        @AuraEnabled
        public String tooltipMessage{get;set;}
    }    

    @AuraEnabled
    public static void logException(String message, String module) 
    {
        if(message.length() > 2000)
            message.substring(0,2000);
        SalesIQLogger.logErrorMessage(message, module);
    }
    
}