public with sharing class InquiryUtility {

    private static String hoQueueId ;
    private static String systemAdminProfile ;
    private static Set<Id> queueMembers ;
    private static Id inquiryAssignee ;

    public static Id assignEmployee(){
        if(inquiryAssignee == null) {
            Id loggedInUser = UserInfo.getUserId();
            //Id userid;
            // ASSIGN A MANAGER
            List<Employee__c> empList = Database.query('Select Manager__r.User__c from Employee__c where isSalesforceUser__c = True AND USER__c = :loggedInUser and Manager__r.User__c <> null limit 1');
            if (empList.size() > 0){
                if(empList[0].Manager__c != null){
                    inquiryAssignee = empList[0].Manager__r.User__c;
                }else{
                    // Inquiry queue
                   inquiryAssignee = getInquiryHOQueue();
                }
            }
        }
        if(inquiryAssignee == null)
            return UserInfo.getUserId();
        else 
            return inquiryAssignee;
    }
    
    public static Date assignDate(Inquiry__c inquiry){
        List<Inquiry_SLA_Setting__c> customSetting = new List<Inquiry_SLA_Setting__c>();

        String profileName = [select Name from profile where id = :userinfo.getProfileId()].Name;
        String priority = inquiry.Priority__c;
        String category = inquiry.Category__c;
        String subCategory = inquiry.Sub_Category__c;
        
        string settingquery = 'SELECT Resolution_Days_Count__c FROM Inquiry_SLA_Setting__c';
        
        List<String> whereClause = new List<String>() ;
        if(priority != null)
            whereClause.add('Priority__c = :priority');
        
        if(category != null)
            whereClause.add('Category__c = :category');

        if(subCategory != null)
            whereClause.add('Sub_Category__c = :subCategory');
            
        if(profileName != null)    
            whereClause.add('Profile_Name__c = :profileName');
        
        string whereClause2 = ' WHERE Priority__c = :priority';
        whereClause2 += ' AND Category__c = :category';
        whereClause2 += ' AND Sub_Category__c = :subCategory';

        if(!whereClause.isEmpty())
            settingquery = settingquery + ' WHERE ' + String.join(whereClause, ' AND ') ;

        customSetting = Database.query(settingquery);
        
        if(customSetting.size() > 0 && customSetting[0].Resolution_Days_Count__c != null)
            return Date.today().addDays(Integer.valueOf(customSetting[0].Resolution_Days_Count__c));
        
        return null;
    }
    
    public static id getRecordType(String recordTypeName) {
        return Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId() ;
    }
    
    public static Id getInquiryHOQueue() {
        if(hoQueueId == null) {
            List<Group> inquiryQueue = [select Id, (select userorgroupid from GroupMembers) from Group WHERE  name = 'InquiryHOQueue' limit 1] ;
            if(!inquiryQueue.isEmpty()) {
                hoQueueId = inquiryQueue.get(0).Id ;

                queueMembers = new Set<Id>();
                for(GroupMember g : inquiryQueue.get(0).GroupMembers){
                    if(g.UserOrGroupID.getsObjectType() == Schema.User.sObjectType)
                        queueMembers.add(g.UserOrGroupID);
                }
            }
        }
        return hoQueueId ;
    }

    public static Set<Id> getInquiryHOQueueMembers() {
        if(queueMembers == null) getInquiryHOQueue() ;
        return queueMembers ;
    }

    public static Id getSystemAdminProfile() {
        if(systemAdminProfile == null) {
            systemAdminProfile = [select Id from Profile where Name = 'System Administrator'].Id ;
        }
        return systemAdminProfile ;

    }

    public static Map<Id, Inquiry__c> getInquiryDetails(Set<Id> inquiryIds) {

        if(SecurityUtil.checkRead(Inquiry__c.SObjectType, new list<string>{'Status__c'}, false)) {
            return new Map<Id, Inquiry__c>([select Status__c from Inquiry__c where Id =: inquiryIds]) ;
        } 
        return null ;
    }
    
}