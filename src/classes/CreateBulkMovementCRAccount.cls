global class CreateBulkMovementCRAccount implements Database.Batchable<sObject>, Database.Stateful{
    
    
    public string theQuery {get;set;}
    public List<string> crgIds; 
    public Change_Request__c changeRequest;
    public string destinationPosition {get;set;}
    public string selectedCycle {get;set;}
    Map<id,id> mapPositionTeamInstancePosition;
    public List<String> posgeList;
    public list<string> crAccIds;
    public Map<id,ID> teamInstMap;
    public List<string> accountSFIDs;
    public List<Position__C> posList;
    public List<String> zips;
    public list<SalesIQ_Logger__c> lstLogger = new list<SalesIQ_Logger__c>();
    public Boolean isError = false;//this variable is used to track whether any error get occured in execute method or inner methd of that execute method i.e CreateCRAccountBulk
    //So we need to stop further execution of finish method, which is further executing other batch/method. in that case other method will mark CR status to completed , which leads to
    //inconsitency
    
    global CreateBulkMovementCRAccount(Map<id,id> teamInsMap,Change_Request__c changeReq, List<String> geList,List<String> gIds,List<String> zipList){
        
        
        if(changeReq!=null){
            posList = new List<Position__c>();           
            changeRequest = changeReq;
            destinationPosition = changeReq.Destination_Position__c;
            selectedCycle  =  changeReq.Team_Instance_ID__c;
            zips = zipList;           
            theQuery = 'select id , account__C from Account_Address__c WHERE Pincode__c IN: zips and Status__c = \'Active\' ';
            teamInstMap = teamInsMap;
            crAccIds  = new List<String>();
            crgIds = geList;
            posgeList = gIds;
            accountSFIDs = new List<String>();
           
        }
    }

    global Database.Querylocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(theQuery);
    }

    global void execute (Database.BatchableContext BC,list<Account_Address__c> adList){
        List<Position_Account__C> listPosAcc  = new List<position_Account__c>();
        Set<String> accSet = new Set<String>();

        try{
            lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('start Executeing Batch CreateBulkMovementCRAccount'  ,'', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,changeRequest.id  ));
            for(Account_Address__c ad : adList){
              accSet.add(ad.Account__C);
            }

            list<string> lstSourcePos = changeRequest.Sources_Bulk__c.split(',');
          
            listPosAcc = [Select id,Account__c,Account__r.Name,Position__c FROM Position_Account__c WHERE Account_Alignment_Type__c ='Implicit' and Team_Instance__c =:selectedCycle and account__c  in: accSet and Position__c in:lstSourcePos and assignment_status__c!=:SalesIQGlobalConstants.POSITION_ACCOUNT_INACTIVE];
           
            list<CR_Account__C> crAccList= new list<CR_Account__C>();

            system.debug('before listPosAcc '+listPosAcc);
            insert lstLogger;
           
            crAccList = ChangerequestTriggerhandler.CreateCRAccountBulk(changeRequest,listPosAcc,teamInstMap);
           
            for(Position_Account__C p : listPosAcc){
                accountSFIDs.add(p.id);
            }
            for(CR_Account__C crGeo: crAccList){
                crAccIds.add(crGeo.id);            
            }    

        }catch(Exception e){
            SalesIQUtility.updateChangeRequestStatus(new set<id>{changeRequest.id}, true);
            isError = true;
            //Clearing lst because till this line record have been insrted in Logger table
            if(lstLogger.size() > 0){
                lstLogger.clear();
            }
            system.debug('Exception :: ' + e.getMessage());
            lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('Error: '+e.getMessage()+' in Execute method of CreateBulkMovementCRAccount ' ,'', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,changeRequest.id ));
            insert lstLogger;            
        }
        finally{
            
            //Clearing logger because in case of 2 batch ,it will clear the log for secod time to evade error "cannot specify Id in an insert call"
            if(lstLogger.size() > 0){
                lstLogger.clear();
            }
            
        }    

    }

    global void finish(Database.BatchableContext BC)
    {
        if(lstLogger.size() > 0){
            lstLogger.clear();
        }
        //lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('Finished Batch execution of CreateBulkMovementCRAccount ' ,'', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,changeRequest.id ));
        lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('In Finishing method of CreateBulkMovementCRAccount ' ,'', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,changeRequest.id ));
        try{
            system.debug('crAccIds CreateBulkMovementCRAccount'+crAccIds);
            system.debug('accountSFIDs CreateBulkMovementCRAccount '+accountSFIDs);
            System.debug('######posgeList '+posgeList);
            System.debug('Error in execute method '+isError);
            if(!isError){

                Team_Instance__c objTeamInstance= SalesIQUtility.getTeamInstanceById(selectedCycle);
                system.debug('CIM_Available__c '+objTeamInstance.CIM_Available__c);
                list<string> lstSourcePos = changeRequest.Sources_Bulk__c.split(',');

                if(objTeamInstance.CIM_Available__c){            
                    String MovementType;
                    if(changeRequest.Alignment_Type__c==SalesIQGlobalConstants.ALIGNMENT_TYPE_HYBRID || changeRequest.Alignment_Type__c== SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT)
                        MovementType = SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT +' Movement';
                    else
                        MovementType= 'ZIP Movement';
                       
                    list<CIM_Change_Request_Impact__c> lstImpact = new list<CIM_Change_Request_Impact__c>();
                    system.debug('MovementType '+MovementType);

                   
                    list<CIM_Config__c> lstConfig = AlignmentUtility.getCIMConfigFields(MovementType, selectedCycle);
                    Map<string,string> MetricName = new Map<string,string>();
                    if(lstConfig != null && lstConfig.size() > 0){
                        for(CIM_Config__c CIM : lstConfig ){
                            MetricName.put(CIM.Attribute_API_Name__c, CIM.id);
                        }
                    }
                    system.debug('cim is'+MetricName);
                    List<AggregateResult> results =null ;
                    String SOQL;

                    string relatedPosType = SalesIQGlobalConstants.TEAM_TYPE_BASE;
                    string assignmentStatus = SalesIQUtility.getAssignmentStatusbyTeamInstance(selectedCycle);
                    Soql = 'Select Position__c, sum(Metric1__c) '+SalesIQGlobalConstants.NAME_SPACE+'Metric1__c ,sum(Metric2__c) '+SalesIQGlobalConstants.NAME_SPACE+'Metric2__c, sum(Metric3__c) '+SalesIQGlobalConstants.NAME_SPACE+'Metric3__c, sum(Metric4__c) '+SalesIQGlobalConstants.NAME_SPACE+'Metric4__c,  sum(Metric5__c) '+SalesIQGlobalConstants.NAME_SPACE+'Metric5__c, sum(Metric6__c) '+SalesIQGlobalConstants.NAME_SPACE+'Metric6__c, sum(Metric7__c) '+SalesIQGlobalConstants.NAME_SPACE+'Metric7__c, sum(Metric8__c) '+SalesIQGlobalConstants.NAME_SPACE+'Metric8__c, sum(Metric9__c) '+SalesIQGlobalConstants.NAME_SPACE+'Metric9__c, sum(Metric10__c) '+SalesIQGlobalConstants.NAME_SPACE+'Metric10__c from ';
         
                    
                    if(MovementType == SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT +' Movement'){
                         Soql = Soql+ ' position_Account__c where ID in :accountSFIDs and Team_Instance__c =: selectedCycle and Assignment_Status__c =:assignmentStatus and Position_Team_Instance__r.Position_ID__r.Related_Position_Type__c =: relatedPosType and Position_Team_Instance__r.Position_ID__r.inactive__c =false and Position__c in:lstSourcePos  group by Position__c ';                
                        list<string> POSITION_ACCOUNT_READ_FIELD = new list<string>{'Metric1__c', 'Metric2__c','Metric3__c', 'Metric4__c', 'Metric5__c', 'Metric6__c', 'Metric7__c', 'Metric8__c', 'Metric9__c', 'Metric10__c', 'Team_Instance__c'};
                        if(SecurityUtil.checkRead(Position_Account__c.SObjectType, POSITION_ACCOUNT_READ_FIELD, false)){
                            results = Database.query(Soql);
                            
                        }
                    }else{ 
                        String sourceBulkIds = changeRequest.Sources_Bulk__c;
                        set<string> posList= new set<string>();
                        posList.addAll(sourceBulkIds.split(','));
                        System.debug(posList);
                        
                        //List<AggregateResult> results =null ;
                        list<String> GEOGRAPHY_READ_FIELD = new list<String>{'Name'};  
                        list<string> TEAM_INSTANCE_GEO_READ_FIELD = new list<string>{'Metric1__c', 'Metric2__c','Metric3__c', 'Metric4__c', 'Metric5__c', 'Metric6__c', 'Metric7__c', 'Metric8__c', 'Metric9__c', 'Metric10__c', 'Geography__c', 'Team_Instance__c'};
                    
                        Soql = Soql+ ' Position_geography__c where id in :posgeList  and Team_Instance__c = :selectedCycle  and Position_Team_Instance__r.Position_ID__r.Related_Position_Type__c =:relatedPosType and Position_Team_Instance__r.Position_ID__r.inactive__c =false and Assignment_Status__c =:assignmentStatus  group by Position__c';
                                        
                        if (SecurityUtil.checkRead(Position_Geography__c.SObjectType, TEAM_INSTANCE_GEO_READ_FIELD, false) && SecurityUtil.checkRead(Geography__c.SObjectType, GEOGRAPHY_READ_FIELD, false)){
                            results = Database.query(Soql);                    
                        }
                    }

                    
                    System.debug('#######results');
                    System.debug(results);

                    System.debug('teamInstMap '+teamInstMap);
                    System.debug('teamInstMap '+teamInstMap.keySet());

                    

                    for(string cimID : MetricName.keyset()){
                        double cumulativeImpactVal = 0;
                        system.debug('first loop '+cimId);
                        for(AggregateResult result:results){
                            system.debug('in 2nd for loop '+cimId);                        
                            string ImpactValue = '';
                            
                            
                            if(result.get(cimID)!=null){
                                system.debug('if CIM value is null  ');   
                                ImpactValue = string.valueof(result.get(cimID));
                                cumulativeImpactVal = cumulativeImpactVal + Double.valueOf(result.get(cimID));
                            }
                            system.debug('cumulativeImpactVal '+cumulativeImpactVal); 
                            system.debug('ImpactValue '+ImpactValue); 
                           
                            Id pos = String.valueOf(result.get(SalesIQGlobalConstants.NAME_SPACE+'Position__c'));
                            system.debug('pos id is '+pos);
                            System.debug(teamInstMap.get(pos));
                            System.debug(teamInstMap.get(destinationPosition));
                            if(ImpactValue != null) {
                                lstImpact.add(ChangeRequestTriggerHandler.CreateCIMRequestImpact(MetricName.get(cimID), changeRequest.id, teamInstMap.get(pos), '-' + ImpactValue,selectedCycle));
                                
                            } else {
                                lstImpact.add(ChangeRequestTriggerHandler.CreateCIMRequestImpact(MetricName.get(cimID), changeRequest.Id, teamInstMap.get(pos),'',selectedCycle));
                                
                            }
                        }
                        /*Below condition will execute when there is no accounts in ZIPs and Team type is hybrid. 
                        Because of no accunts, query is not able to fectch data for source position, so we need to explict create Request CIM imapct for those soruce positions

                        */
                        system.debug('end of 2nd loop loop ');
                        if(accountSFIDs.size() == 0 && changeRequest.Alignment_Type__c==SalesIQGlobalConstants.ALIGNMENT_TYPE_HYBRID){
                            list<string> sourcePositions = changeRequest.Sources_Bulk__c.split(',');
                            for(string sourcePos: sourcePositions){
                                lstImpact.add(ChangeRequestTriggerHandler.CreateCIMRequestImpact(MetricName.get(cimID), changeRequest.Id, teamInstMap.get(sourcePos),'-' + String.valueOf(cumulativeImpactVal),selectedCycle));

                            }
                        }

                        lstImpact.add(ChangeRequestTriggerHandler.CreateCIMRequestImpact(MetricName.get(cimID), changeRequest.Id, teamInstMap.get(destinationPosition),'+' + String.valueOf(cumulativeImpactVal),selectedCycle));
                    }
                    System.debug('Request Imapct '+ lstImpact);
                    System.debug('Request Imapct Size '+ lstImpact.size());
                    if(lstImpact.size() > 0){                    
                        insert lstImpact;
                    }
                    lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('Inserted '+lstImpact.size() +' records in CIM Request Impact data ' ,'', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,changeRequest.id));
                }    
                         
                system.debug('ids in finish'+crGIds);
                
                System.debug('just befoire bacth ');
               
                Database.executeBatch(new BulkZIporAccountBatch(crGIds,changeRequest,posgeList,crAccIds),1500);
                //SalesiqUtility.updatePositionChangeStatus(new List<id>{destinationPosition},SalesIQGlobalConstants.REQUEST_STATUS_NO_CHANGE);
            }
        }catch(Exception e){
            system.debug('Exception :: ' + e.getMessage());
            //SalesIQLogger.logCatchedException(e, 'Module : CreateBulkMovementRequestChange.cls');
            lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('Error: '+e.getMessage()+' in Finish method of CreateBulkMovementCRAccount' ,'', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,changeRequest.id));
            SalesIQUtility.updateChangeRequestStatus(new set<id>{changeRequest.id}, true);
        }
        finally{
            insert lstLogger;
            isError = false;//resetting variable again
        }  
    }

    
}