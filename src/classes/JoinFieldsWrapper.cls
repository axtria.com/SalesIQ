public class JoinFieldsWrapper {
        
    @AuraEnabled
    public String columnId {get;set;}
    @AuraEnabled
    public String tableColumnName {get; set;}      
    @AuraEnabled
    public String colDatatype {get; set;}        
    @AuraEnabled
    public String colValuetype {get; set;}
    @AuraEnabled
    public boolean statusFlag {get; set;}
    @AuraEnabled
    public String datasourceName {get; set;} 
    @AuraEnabled
    public String datasourceColumnName {get; set;}
    @AuraEnabled
    public String Id {get; set;}
     @AuraEnabled
    public String datasetId {get; set;}
     @AuraEnabled
    public String tableDataType {get; set;}
     @AuraEnabled
    public String variableType {get; set;}
    @AuraEnabled
    public Boolean isVisible {get; set;}


    public JoinFieldsWrapper parse(String json) {
        return (JoinFieldsWrapper) System.JSON.deserialize(json, JoinFieldsWrapper.class);
    }
}