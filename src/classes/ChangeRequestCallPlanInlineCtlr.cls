//DEPRECTED CLASS
public with sharing class ChangeRequestCallPlanInlineCtlr {
/*
private ApexPages.StandardController ctrl{get; set;}
    private Change_Request__c changeRequest;
    public Integer maxTotalHCPs {get;set;}
   public Integer maxnexavarCalls {get;set;}
   
   public Integer maxstivargaCalls {get;set;}
   public Integer maxtotalCallsHCP {get;set;}
    
     
   public Integer percentageTotalHcps {get;set;}
   public Integer percentagenexavarCalls {get;set;}
   public Integer percentagestivargaCalls {get;set;}
   public Integer percentagetotalCallsHCP {get;set;}
   
   public integer phyAdded {get;set;}
   public integer phyDropped {get;set;}
   
   
   public string colorNexavarCalls {get;set;}
   public string colorTotalHCPs {get;set;}
   public string colorStivargaCalls {get;set;}
   public string colortotalCallsHCP {get;set;}
   public boolean lock {get;set;}  
   public boolean savelock {get;set;}
   public string selectedPosition {get;set;} 
   public string selectedTeamInstance {get;set;} 
   
   public Integer totalCallsHCP {get;set;}
  public Integer totalHCPs {get;set;}
  public Integer nexavarCalls {get;set;}
  public Integer stivargaCalls {get;set;} 
    public string genericCssForBar {get;set;}
    
    
    public ChangeRequestCallPlanInlineCtlr (ApexPages.StandardController ctrl){
      Change_Request__c obj;
      list<string> CHANGE_REQUEST_READ_FIELD = new list<string>{'id','destination_position__c','Team_Instance_ID__c'};
        lock = false;
        this.ctrl = ctrl;
        this.changeRequest = (Change_Request__c)ctrl.getRecord();
        if(SecurityUtil.checkRead(Change_Request__c.SObjectType, CHANGE_REQUEST_READ_FIELD, false)){
           obj = [select id, destination_position__c, Team_Instance_ID__c from change_request__c where id=: this.changeRequest.id];
        }
        
        selectedPosition = obj.destination_position__c;
        selectedTeamInstance = obj.Team_Instance_ID__c;
        genericCssForBar = 'left,#FF0000 ' + 12.50 + '%,#ffbf00 ' + 25.00 + '%,#00FF00 ' + 75.00 + '%,#ffbf00 ' + 87.50 + '%,#FF0000 100%';
        cimConfig();
    }
    
    
    public void cimConfig()
   {  
     
        system.debug('Hey user position is '+selectedPosition); 
        system.debug('--selectedTeamInstance '+selectedTeamInstance);
     //   list<string> displayNameList = new list<string>{'No of Targets', 'Nexavar Calls', 'Stivarga Calls', 'Total Calls'};
        list<string> CIM_POSITION_METRIC_SUMMARY_READ_FIELD = new list<string>{'id','Original__c','Proposed__c','Optimum__c','Position_Team_Instance__c'};
        list<string> POSITION_TEAM_INSTANCE_READ_FIELD = new list<string>{'Position_ID__c'};
        list<string> CIM_CONFIG_READ_FIELD = new list<string>{'id','Name','Display_Name__c','Enforcable__c','Error_Message__c','isOptimum__c','Threshold_Max__c','Threshold_Warning_Max__c','Threshold_Warning_Min__c','Tooltip_Message__c','Visible__c','Team_Instance__c','Enable__c'};
        string cimQuery = 'select id,Name,(select id, Original__c,Approved__c,Proposed__c,Optimum__c from CIM_Position_Metric_Summary__r where Position_Team_Instance__r.Position_ID__c =:selectedPosition LIMIT 1),Display_Name__c,Enforcable__c,Error_Message__c,isOptimum__c ,Threshold_Max__c, Threshold_Min__c, Threshold_Warning_Max__c, Threshold_Warning_Min__c, Tooltip_Message__c, Visible__c from CIM_Config__c where Team_Instance__c = :selectedTeamInstance and Enable__c= true ';
        List<CIM_Config__c> cimConfigRecs = new list<CIM_Config__c>();
        
        system.debug('cimQuery ::::::'+cimQuery);
        
        if (SecurityUtil.checkRead(CIM_Config__c.SObjectType, CIM_CONFIG_READ_FIELD, false) && SecurityUtil.checkRead(CIM_Position_Metric_Summary__c.SObjectType, CIM_POSITION_METRIC_SUMMARY_READ_FIELD, false) && SecurityUtil.checkRead(Position_Team_Instance__c.SObjectType, POSITION_TEAM_INSTANCE_READ_FIELD, false)){
          cimConfigRecs = Database.query(cimQuery);
        }
        

        //percentageTotalHcps = 200;
        //percentagenexavarCalls = 200;
        //percentagestivargaCalls = 200;
        //percentagetotalCallsHCP = 200;
        
        maxTotalHCPs = 0;
        maxtotalCallsHCP = 0;
        maxnexavarCalls = 0; 
        maxstivargaCalls = 0;
        
        for(CIM_Config__c cimRec : cimConfigRecs)
        {
          if(cimRec.CIM_Position_Metric_Summary__r != null && cimRec.CIM_Position_Metric_Summary__r.size() > 0){            
              
              // Total HCP's
              if(cimRec.Name == 'No of Targets'){
                totalHCPs = Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].proposed__c);
          
          Decimal updateHCps = totalHCPs;
          Decimal orginalHCps = Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].original__c);
          
                if(Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].original__c) != 0 && cimRec.CIM_Position_Metric_Summary__r[0].original__c != null) {
                  percentageTotalHcps =   Integer.valueOf((updateHCps/orginalHCps)*100);
                }else{
                  percentageTotalHcps = 200;  
                }
                
                maxTotalHCPs = Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].original__c) * 2;
                
                if(percentageTotalHcps >= 130 || percentageTotalHcps <= 70){
                  colorTotalHCPs = 'red';
                }
                else if( (percentageTotalHcps > 70 && percentageTotalHcps < 80) || (percentageTotalHcps > 120 && percentageTotalHcps < 130)){
                  colorTotalHCPs = 'yellow';  
                }
                else if(percentageTotalHcps >= 80 && percentageTotalHcps <= 120){
                  colorTotalHCPs = 'green';
                }
                
                system.debug('totalHCPs');
                system.debug('-- original value '+ Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].original__c));
                system.debug('-- proposed value '+ Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].Proposed__c));
                
                system.debug('-- approved value '+ Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].approved__c));
                system.debug('-- totalHCPs '+totalHCPs );
                system.debug('-- percentageTotalHcps '+percentageTotalHcps );
                system.debug('-- maxTotalHCPs '+maxTotalHCPs);
                
                
                Integer error_max = Integer.valueOf( 1.30 * Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].original__c));
                Integer error_min = Integer.valueOf( 0.70 * Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].original__c));
          
          if(totalHCPs < error_min || totalHCPs > error_max)
          {
            savelock = true;
            
          }
            
              }
              else if(cimRec.Name == 'Total Calls'){ // Total Calls
                
                totalCallsHCP = Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].proposed__c);
                
                
                Decimal updateCallsHcps = totalCallsHCP;
          Decimal orginalCallscps = Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].original__c);
          
                if(Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].original__c) != 0 && cimRec.CIM_Position_Metric_Summary__r[0].original__c != null){
                  percentagetotalCallsHCP = Integer.valueOf((updateCallsHcps/orginalCallscps)*100);        
                }else{
                  percentagetotalCallsHCP = 200;  
                }
                
                maxtotalCallsHCP = Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].original__c) * 2;
          
          if(percentagetotalCallsHCP >= 130 || percentagetotalCallsHCP <= 70){
                  colortotalCallsHCP = 'red';
                }
                else if((percentagetotalCallsHCP > 70 && percentagetotalCallsHCP < 80) || (percentagetotalCallsHCP > 120 && percentagetotalCallsHCP < 130)){
                  colortotalCallsHCP = 'yellow';  
                }
                else if(percentagetotalCallsHCP >= 80 && percentagetotalCallsHCP <= 120){
                  colortotalCallsHCP = 'green';
                }
          
                
          system.debug('totalCallsHCP');
                system.debug('-- original value '+ Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].original__c));
                system.debug('-- proposed value '+ Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].Proposed__c));
                
                system.debug('-- approved value '+ Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].approved__c));
                system.debug('-- totalCallsHCP '+totalCallsHCP );
                system.debug('-- percentagetotalCallsHCP '+percentagetotalCallsHCP );
                system.debug('-- maxtotalCallsHCP '+maxtotalCallsHCP);
          
          Integer error_max = Integer.valueOf( 1.30 * Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].original__c));
                Integer error_min = Integer.valueOf( 0.70 * Integer.valueOf(cimRec.CIM_Position_Metric_Summary__r[0].original__c));
          
          if(totalCallsHCP < error_min || totalCallsHCP > error_max)
          {
            savelock = true;
            
          }
              }
              
          }
          
        }
        
          system.debug('+++++++++++++ In cim selected Position and team instance is '+selectedPosition+' '+selectedTeamInstance);
      
      AggregateResult aggAdded;   
      list<string> POSITION_ACCOUNT_CALL_PLAN_READ_FIELD = new list<string>{'id','isAccountTarget__c','isIncludedCallPlan__c','Position__c','Team_Instance__c','isAccountTarget__c','Nexavar_Original__c','Stivarga_Original__c'};
      if(SecurityUtil.checkRead(Position_Account_Call_Plan__c.SObjectType, POSITION_ACCOUNT_CALL_PLAN_READ_FIELD, false)){
       aggAdded = [select count(id) countRecs from Position_Account_Call_Plan__c where isAccountTarget__c = false and isIncludedCallPlan__c = true and Position__c = :selectedPosition and Team_Instance__c = :selectedTeamInstance];
      }
      
      phyAdded = Integer.valueof(aggAdded.get('countRecs'));        
      
      AggregateResult aggDropped;
      if(SecurityUtil.checkRead(Position_Account_Call_Plan__c.SObjectType, POSITION_ACCOUNT_CALL_PLAN_READ_FIELD, false)){
       aggDropped = [select count(id) countRecs from Position_Account_Call_Plan__c where isAccountTarget__c = true and isIncludedCallPlan__c = false and Position__c = :selectedPosition and Team_Instance__c = :selectedTeamInstance];
      }
      
      phyDropped = Integer.valueof(aggDropped.get('countRecs'));
      
      system.debug('+++++++++++++++++++ phy Added and Phy Dropped '+ phyAdded + ' '+aggDropped);
      
      AggregateResult originalDirectionCalls;
      if(SecurityUtil.checkRead(Position_Account_Call_Plan__c.SObjectType, POSITION_ACCOUNT_CALL_PLAN_READ_FIELD, false)){
       originalDirectionCalls = [select sum(Nexavar_Original__c) nexavarCalls, sum(Stivarga_Original__c) stivargaCalls from Position_Account_Call_Plan__c where isAccountTarget__c = true and Position__c = :selectedPosition and Team_Instance__c = :selectedTeamInstance];        
      }
      
      Decimal originalNexavarCalls = Integer.valueof(originalDirectionCalls.get('nexavarCalls'));
      Decimal originalStivargaCalls = Integer.valueof(originalDirectionCalls.get('stivargaCalls'));
      
      AggregateResult updatedDirectionCalls;
      if(SecurityUtil.checkRead(Position_Account_Call_Plan__c.SObjectType, POSITION_ACCOUNT_CALL_PLAN_READ_FIELD, false)){
        updatedDirectionCalls = [select sum(NexavarCalls__c) nexavarCalls, sum(StivargaCalls__c) stivargaCalls from Position_Account_Call_Plan__c where isIncludedCallPlan__c = true and Position__c = :selectedPosition and Team_Instance__c = :selectedTeamInstance];
      }
      
      
      Decimal updatedNexavarCalls = Integer.valueof(updatedDirectionCalls.get('nexavarCalls'));
      Decimal updatedStivargaCalls = Integer.valueof(updatedDirectionCalls.get('stivargaCalls'));
      
      if(originalNexavarCalls != 0 && originalNexavarCalls != null && originalStivargaCalls != 0 && originalStivargaCalls != null){
      
      if(originalNexavarCalls != 0 && originalNexavarCalls != null){
        system.debug( '-- updatedNexavarCalls for now ' +updatedNexavarCalls );
        system.debug( '-- originalNexavarCalls for now ' +originalNexavarCalls );
        system.debug(' value '+ updatedNexavarCalls / originalNexavarCalls );
        percentagenexavarCalls = Integer.valueOf( (updatedNexavarCalls/originalNexavarCalls)* 100 );  
      }
      else{
        percentagenexavarCalls = 200;
      }
      
      nexavarCalls = Integer.valueOf(updatedNexavarCalls);  
      maxnexavarCalls = Integer.valueOf(originalNexavarCalls) * 2;
      
      if(percentagenexavarCalls >= 130 || percentagenexavarCalls <= 70){
              colorNexavarCalls = 'red';
              savelock = true;
            }
      else if((percentagenexavarCalls > 70 && percentagenexavarCalls < 80) || (percentagenexavarCalls > 120 && percentagenexavarCalls < 130)){
              colorNexavarCalls = 'yellow';  
            }
            else if(percentagenexavarCalls >= 80 && percentagenexavarCalls <= 120){
              colorNexavarCalls = 'green';
            }
            
            system.debug('nexavarCalls');
          system.debug('-- original value '+ originalNexavarCalls);
          system.debug('-- proposed value '+ updatedNexavarCalls);
          system.debug('-- nexavarCalls '+nexavarCalls );
          system.debug('-- percentagenexavarCalls '+percentagenexavarCalls );
          system.debug('-- maxnexavarCalls '+maxnexavarCalls);
      
      
      if(originalStivargaCalls != 0 && originalStivargaCalls != null){
        percentagestivargaCalls = Integer.valueOf((updatedStivargaCalls/originalStivargaCalls)* 100);  
      }
      else{
        percentagestivargaCalls = 200;
      }
      
      stivargaCalls = Integer.valueOf(updatedStivargaCalls); 
      maxstivargaCalls = Integer.valueOf(originalStivargaCalls) * 2;

            if(percentagestivargaCalls >= 130 || percentagestivargaCalls <= 70){
              savelock = true;
              colorStivargaCalls = 'red';
            }
            else if((percentagestivargaCalls > 70 && percentagestivargaCalls < 80) || (percentagestivargaCalls > 120 && percentagestivargaCalls < 130)){
              colorStivargaCalls = 'yellow';  
            } 
            else if(percentagestivargaCalls >= 80 && percentagestivargaCalls <= 120){
              colorStivargaCalls = 'green';
            }
            
            system.debug('-- colorStivargaCalls '+colorStivargaCalls);
      system.debug('stivargaCalls');
          system.debug('-- original value '+ originalStivargaCalls);
          system.debug('-- proposed value '+ updatedStivargaCalls);
          system.debug('-- stivargaCalls '+stivargaCalls );
          system.debug('-- percentagestivargaCalls '+percentagestivargaCalls );
          system.debug('-- maxstivargaCalls '+maxstivargaCalls);
      
      Integer error_max = Integer.valueOf( 1.30 * originalNexavarCalls);
          Integer error_min = Integer.valueOf( 0.70 * originalNexavarCalls);
          
      
      if(updatedNexavarCalls < error_min || updatedNexavarCalls > error_max)
      {
        savelock = true;
        
      }
      
      error_max = Integer.valueOf( 1.30 * originalStivargaCalls);
          error_min = Integer.valueOf( 0.70 * originalStivargaCalls);
          
      
      if(updatedStivargaCalls < error_min || updatedStivargaCalls > error_max)
      {
        savelock = true;
        
      }
      
    }
   } 

   */ 
}
