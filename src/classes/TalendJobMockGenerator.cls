/**********************************************************************************************
Name		:	ESRIServicesMockGenerator
Author     	:	Gaurav
Date       	: 	07/12/2017
Description	:	HTTP Callout Mock class to generate mock response for ESRI services
**********************************************************************************************/
@isTest
global class TalendJobMockGenerator implements HttpCalloutMock{

    global HTTPResponse respond(HTTPRequest req) {
    
      // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        
        if(req.getEndpoint().contains('http://52.24.8.231:8080/Esri_Populate_0.1/services/Esri_Populate?method=runJob')){
           system.assertEquals('GET', req.getMethod());
           res.setBody('{"Response":[{"success":true}]}');
        }else if(req.getEndpoint().contains('http://52.24.8.231:8080')){
           system.assertEquals('GET', req.getMethod());
           res.setBody('{"Response":[{"success":true}]}');
        }
         res.setStatusCode(200);        
         
         return res;
    
    }
}