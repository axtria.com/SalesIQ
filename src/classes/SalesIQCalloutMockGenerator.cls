global class SalesIQCalloutMockGenerator implements HttpCalloutMock
{
    global string s;
    global string objectName;
    global String ns = SalesIQGlobalConstants.Name_space;
    global HTTPResponse respond(HTTPRequest req) 
    {
        // Create a fake response.
        // Set response values, and 
        // return response.
        HttpResponse res = new HttpResponse();

        System.debug('req :: '+req.getEndpoint());
        
        if(objectName == 'Employee__c' || objectName == SalesIQGlobalConstants.Name_space + 'Employee__c')
        {
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"columns":[{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"Name","hidden":false,"label":"Employee Name","selectListItem":"Name","sortDirection":"ascending","sortIndex":0,"sortable":true,"type":"string"},{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath": "'+ns+'Employee_ID__c","hidden":false,"label":"Employee ID","selectListItem":"'+ns+'Employee_ID__c","sortDirection":null,"sortIndex":null,"sortable":true,"type":"string"},{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"'+ns+'Manager__r.Name","hidden":false,"label":"Manager","selectListItem":"'+ns+'Manager__r.Name","sortDirection":null,"sortIndex":null,"sortable":true,"type":"string"}],"id":"00B41000003hNC7","query":"SELECT Name, '+ns+'Employee_ID__c,'+ns+'Manager__r.Name FROM '+ns+'Employee__c WHERE '+ns+'Employee_ID__c != null ORDER BY Name ASC NULLS FIRST", "whereCondition" : {"conditions":[], "conjunction":"and"}}');
            res.setStatusCode(200);
            return res;

        }
        else if(objectName == 'Position__c' || objectName == SalesIQGlobalConstants.Name_space + 'Position__c')
        {
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"columns":[{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"Name","hidden":false,"label":"Position Name","selectListItem":"Name","sortDirection":"ascending","sortIndex":0,"sortable":true,"type":"string"},{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"'+ns+'Effective_End_Date__c","hidden":false,"label":"End Date","selectListItem":"'+ns+'Effective_End_Date__c","sortDirection":"descending","sortIndex":null,"sortable":true,"type":"Date"},{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"'+ns+'Client_Position_Code__c","hidden":false,"label":"Position ID","selectListItem":"'+ns+'Client_Position_Code__c","sortDirection":null,"sortIndex":null,"sortable":true,"type":"string"},{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"'+ns+'Parent_Position__r.Name","hidden":false,"label":"Parent Position","selectListItem":"'+ns+'Parent_Position__r.Name","sortDirection":null,"sortIndex":null,"sortable":true,"type":"Reference"}],"id":"00B6A0000013lVLUAY","query":"SELECT Name,'+ns+'Client_Position_Code__c ,'+ns+'Parent_Position__r.Name,'+ns+'Effective_End_Date__c FROM '+ns+'Position__c WHERE '+ns+'Client_Position_Code__c != null ORDER BY Name ASC NULLS FIRST", "whereCondition" : {"conditions":[], "conjunction":"and"}}');
            res.setStatusCode(200);
        
            return res;
        }
        else if(objectName == 'Scenario__c' || objectName == SalesIQGlobalConstants.Name_space + 'Scenario__c')
        {
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"columns":[{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"Name","hidden":false,"label":"Scenario Name","selectListItem":"Name","sortDirection":"ascending","sortIndex":0,"sortable":true,"type":"string"},{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"'+ns+'Scenario_Name_Formula__c","hidden":false,"label":"Scenario Formula Name","selectListItem":"'+ns+'Scenario_Name_Formula__c","sortDirection":null,"sortIndex":null,"sortable":true,"type":"Formula"},{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"'+ns+'Workspace__r.Name","hidden":false,"label":"Workspace__c","selectListItem":"'+ns+'Workspace__r.Name","sortDirection":null,"sortIndex":null,"sortable":true,"type":"string"}],"id":"00B41000003hNC7","query":"SELECT Name, '+ns+'Scenario_Name_Formula__c,'+ns+'Workspace__r.Name FROM '+ns+'Scenario__c ORDER BY Name ASC NULLS FIRST", "whereCondition" : {"conditions":[], "conjunction":"and"}}');
            res.setStatusCode(200);
        
            return res;
        }else 
        if(objectName == 'Country__c' || objectName == SalesIQGlobalConstants.Name_space + 'Country__c')
        {
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"columns":[{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"Name","hidden":false,"label":"Country Name","selectListItem":"Name","sortDirection":"ascending","sortIndex":0,"sortable":true,"type":"string"},{"ascendingLabel":"Low to High","descendingLabel":"High to Low","fieldNameOrPath":"Country_Code__c","hidden":false,"label":"Country Code","selectListItem":"toLabel(Country_Code__c)","sortDirection":null,"sortIndex":null,"sortable":true,"type":"picklist"},{"ascendingLabel":"Ascending","descendingLabel":"Descending","fieldNameOrPath":"Parent_Organization__r.Name","hidden":false,"label":"Parent Organization","selectListItem":"Parent_Organization__r.Name","sortDirection":null,"sortIndex":null,"sortable":true,"type":"reference"},{"ascendingLabel":"Low to High","descendingLabel":"High to Low","fieldNameOrPath":"Status__c","hidden":false,"label":"Status","selectListItem":"toLabel(Status__c)","sortDirection":null,"sortIndex":null,"sortable":true,"type":"picklist"},{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"ISO_Code__c","hidden":false,"label":"ISO Code","selectListItem":"ISO_Code__c","sortDirection":null,"sortIndex":null,"sortable":true,"type":"textarea"},{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"Country_Flag__c","hidden":false,"label":"Country Flag","selectListItem":"Country_Flag__c","sortDirection":null,"sortIndex":null,"sortable":true,"type":"string"},{"ascendingLabel":null,"descendingLabel":null,"fieldNameOrPath":"Id","hidden":true,"label":"Record ID","selectListItem":"Id","sortDirection":null,"sortIndex":null,"sortable":false,"type":"id"},{"ascendingLabel":null,"descendingLabel":null,"fieldNameOrPath":"CreatedDate","hidden":true,"label":"Created Date","selectListItem":"CreatedDate","sortDirection":null,"sortIndex":null,"sortable":false,"type":"datetime"},{"ascendingLabel":null,"descendingLabel":null,"fieldNameOrPath":"LastModifiedDate","hidden":true,"label":"Last Modified Date","selectListItem":"LastModifiedDate","sortDirection":null,"sortIndex":null,"sortable":false,"type":"datetime"},{"ascendingLabel":null,"descendingLabel":null,"fieldNameOrPath":"SystemModstamp","hidden":true,"label":"System Modstamp","selectListItem":"SystemModstamp","sortDirection":null,"sortIndex":null,"sortable":false,"type":"datetime"}],"id":"00B41000008Mnpj","orderBy":[{"fieldNameOrPath":"Name","nullsPosition":"first","sortDirection":"ascending"},{"fieldNameOrPath":"Id","nullsPosition":"first","sortDirection":"ascending"}],"query":"SELECT Name, toLabel(Country_Code__c), Parent_Organization__r.Name, toLabel(Status__c), ISO_Code__c, Country_Flag__c, Id, CreatedDate, LastModifiedDate, SystemModstamp FROM Country__c ORDER BY Name ASC NULLS FIRST, Id ASC NULLS FIRST","scope":null,"sobjectType":"Country__c","whereCondition":{"conditions":[],"conjunction":"and"}}');
            res.setStatusCode(200);
        
            return res;
        } 
        else 
        if(objectName == 'Workspace__c' || objectName == SalesIQGlobalConstants.Name_space + 'Workspace__c')
        {
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"columns":[{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"Name","hidden":false,"label":"Workspace Name","selectListItem":"Name","sortDirection":"ascending","sortIndex":0,"sortable":true,"type":"string"},{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"'+ns+'Workspace_Description__c","hidden":false,"label":"Workspace_Description__c","selectListItem":"'+ns+'Workspace_Description__c","sortDirection":null,"sortIndex":null,"sortable":true,"type":"String"},{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"'+ns+'Workspace_End_Date__c","hidden":false,"label":"End Date","selectListItem":"'+ns+'Workspace_End_Date__c","sortDirection":null,"sortIndex":null,"sortable":true,"type":"Date"}],"id":"00B41000003hNC7","query":"SELECT Name, '+ns+'Workspace_Description__c,'+ns+'Workspace_End_Date__c FROM '+ns+'Workspace__c ORDER BY Name ASC NULLS FIRST", "whereCondition" : {"conditions":[], "conjunction":"and"}}');
            res.setStatusCode(200);
        
            return res;
        }  
        else 
        if(objectName == 'Position_Account_Call_Plan__c' || objectName == SalesIQGlobalConstants.Name_space + 'Position_Account_Call_Plan__c')
        {

            if(req.getEndpoint().contains('/describe')){

                res.setHeader('Content-Type', 'application/json');
                res.setBody('{"columns":[{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"Name","hidden":false,"label":"Name","selectListItem":"Name","sortDirection":null,"sortIndex":null,"sortable":true,"type":"string"},{"ascendingLabel":"Ascending","descendingLabel":"Descending","fieldNameOrPath":"Team_Instance__r.Name","hidden":false,"label":"Team Instance","selectListItem":"Team_Instance__r.Name","sortDirection":null,"sortIndex":null,"sortable":true,"type":"reference"},{"ascendingLabel":"Ascending","descendingLabel":"Descending","fieldNameOrPath":"Account__r.Name","hidden":false,"label":"Account","selectListItem":"Account__r.Name","sortDirection":null,"sortIndex":null,"sortable":true,"type":"reference"},{"ascendingLabel":"New to Old","descendingLabel":"Old to New","fieldNameOrPath":"Effective_Start_Date__c","hidden":false,"label":"Effective Start Date","selectListItem":"Effective_Start_Date__c","sortDirection":null,"sortIndex":null,"sortable":true,"type":"date"},{"ascendingLabel":"New to Old","descendingLabel":"Old to New","fieldNameOrPath":"Effective_End_Date__c","hidden":false,"label":"Effective End Date","selectListItem":"Effective_End_Date__c","sortDirection":null,"sortIndex":null,"sortable":true,"type":"date"},{"ascendingLabel":"Ascending","descendingLabel":"Descending","fieldNameOrPath":"Position__r.Name","hidden":false,"label":"Position","selectListItem":"Position__r.Name","sortDirection":"ascending","sortIndex":0,"sortable":true,"type":"reference"},{"ascendingLabel":"Ascending","descendingLabel":"Descending","fieldNameOrPath":"Position_Team_Instance__r.Name","hidden":false,"label":"Position Team Instance","selectListItem":"Position_Team_Instance__r.Name","sortDirection":null,"sortIndex":null,"sortable":true,"type":"reference"},{"ascendingLabel":"Ascending","descendingLabel":"Descending","fieldNameOrPath":"Team_Instance_Account__r.Name","hidden":false,"label":"Team Instance Account","selectListItem":"Team_Instance_Account__r.Name","sortDirection":null,"sortIndex":null,"sortable":true,"type":"reference"},{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"Source__c","hidden":false,"label":"Source","selectListItem":"Source__c","sortDirection":null,"sortIndex":null,"sortable":true,"type":"string"},{"ascendingLabel":"Z-A","descendingLabel":"A-Z","fieldNameOrPath":"Id","hidden":false,"label":"Record ID","selectListItem":"Id","sortDirection":"ascending","sortIndex":1,"sortable":true,"type":"id"},{"ascendingLabel":null,"descendingLabel":null,"fieldNameOrPath":"CreatedDate","hidden":true,"label":"Created Date","selectListItem":"CreatedDate","sortDirection":null,"sortIndex":null,"sortable":false,"type":"datetime"},{"ascendingLabel":null,"descendingLabel":null,"fieldNameOrPath":"LastModifiedDate","hidden":true,"label":"Last Modified Date","selectListItem":"LastModifiedDate","sortDirection":null,"sortIndex":null,"sortable":false,"type":"datetime"},{"ascendingLabel":null,"descendingLabel":null,"fieldNameOrPath":"SystemModstamp","hidden":true,"label":"System Modstamp","selectListItem":"SystemModstamp","sortDirection":null,"sortIndex":null,"sortable":false,"type":"datetime"}],"id":"00B6A000003P3n0","orderBy":[{"fieldNameOrPath":"Position__r.Name","nullsPosition":"first","sortDirection":"ascending"},{"fieldNameOrPath":"Id","nullsPosition":"first","sortDirection":"ascending"}],"query":"SELECT Name, Team_Instance__r.Name, Account__r.Name, Effective_Start_Date__c, Effective_End_Date__c, Position__r.Name, Position_Team_Instance__r.Name, Team_Instance_Account__r.Name, Source__c, Id, CreatedDate, LastModifiedDate, SystemModstamp FROM Position_Account_Call_Plan__c ORDER BY Position__r.Name ASC NULLS FIRST, Id ASC NULLS FIRST","scope":null,"sobjectType":"Position_Account_Call_Plan__c","whereCondition":{"conditions":[],"conjunction":"and"}}');
                res.setStatusCode(200);
                
            }else{
                res.setHeader('Content-Type', 'application/json');
                res.setBody('{"done":true,"listviews":[{"describeUrl":"/services/data/v32.0/sobjects/Position_Account_Call_Plan__c/listviews/00B6A000003P3n0UAC/describe","developerName":"All","id":"00B6A000003P3n0UAC","label":"All","resultsUrl":"/services/data/v32.0/sobjects/Position_Account_Call_Plan__c/listviews/00B6A000003P3n0UAC/results","soqlCompatible":true,"url":"/services/data/v32.0/sobjects/Position_Account_Call_Plan__c/listviews/00B6A000003P3n0UAC"}],"nextRecordsUrl":null,"size":1,"sobjectType":"Position_Account_Call_Plan__c"}');
                res.setStatusCode(200);
            }
            
        
            return res;
        }        
        else
        {    	
            return null;
        }
	}
}