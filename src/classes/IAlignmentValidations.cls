/*An Interface that defines the validations that need to be checked.*/
global Interface IAlignmentValidations{
	
	 ValidationResponse checkValidationOnDestinationSelection(string teamInstanceId, set<string> selectedZips, set<string> selectedAccount ,string sourcePosCode,
	 string destPosCode,string operation);   
	 
	 ValidationResponse checkValidationOnConfirmMovement(string teamInstanceId,Id sourceId ,Id destinationPositionId ,set<string> selectedZips, set<string> selectedAccount ,string operation);         
	 
	 ValidationResponse checkValidationOnSharing(string teamInstanceId ,Id sourceId,String destinationName,set<string> selectedRecords, string operation); 
	 
	 ValidationResponse checkValidationOnSubmissionPending(string checkedzips,string uncheckedzips,string checkedAccounts,string uncheckedAccounts,string proposedPosition,string teamInstanceId);   
	 
	

}