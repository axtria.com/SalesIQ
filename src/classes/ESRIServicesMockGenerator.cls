/**********************************************************************************************
Name		:	ESRIServicesMockGenerator
Author     	:	Raghvendra Rathore
Date       	: 	03/08/2016
Description	:	HTTP Callout Mock class to generate mock response for ESRI services
**********************************************************************************************/
@isTest
global class ESRIServicesMockGenerator implements HttpCalloutMock{
	global HTTPResponse respond(HTTPRequest req) {
		
		MapServerURL__c ESRIService = MapServerURL__c.getOrgDefaults();
		system.debug('##### ESRIServicesMockGenerator ' + req);
	    system.debug('##### ESRIServicesMockGenerator ' + req.getEndpoint());
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        
        if(req.getEndpoint().startsWith(ESRIService.Endpoint__c+'/MapServer/1/query?f=json&returnGeometry=false&outFields=zip,objectid&where=zip')){        	
        	system.assertEquals('GET', req.getMethod());
        	res.setBody('{"displayFieldName":"zip","fieldAliases":{"zip":"zip","objectid":"objectid"},"fields":[{"name":"zip","type":"esriFieldTypeString","alias":"zip","length":10},{"name":"objectid","type":"esriFieldTypeOID","alias":"objectid"}],"features":[{"attributes":{"zip":"11788","objectid":32608}}]}');
        }else if(req.getEndpoint().startsWith(ESRIService.Endpoint__c+'/MapServer/1/query?f=json&returnGeometry=false&outFields=objectid,territory_code&where=territory_code=')){        	
        	system.assertEquals('GET', req.getMethod());
        	res.setBody('{"displayFieldName":"zip","fieldAliases":{"objectid":"objectid","territory_code":"TERRITORY_CODE"},"fields":[{"name":"objectid","type":"esriFieldTypeOID","alias":"objectid"},{"name":"territory_code","type":"esriFieldTypeString","alias":"TERRITORY_CODE","length":255}],"features":[{"attributes":{"objectid":6,"territory_code":"1NE30000"}}]}');
        }else if(req.getEndpoint().startsWith(ESRIService.Service_URL__c+'/FeatureServer/1/updateFeatures')){        
        	system.assertEquals('POST', req.getMethod());
        	res.setBody('{"updateResults":[{"objectId":32608,"success":true}]}');
        }else if(req.getEndpoint().contains('addFeatures')){  // Condition Added for Add ZIP Share
           res.setBody('{"AddedResults":[{"objectId":32609,"success":true}]}');
           res.setStatus('Ok');
        }else if(req.getEndpoint().equals(ESRIService.Service_URL__c+'/FeatureServer/1/query?')){ // Condition Added to fetch Object ID's from ESRI
          
           res.setBody('{"displayFieldName":"zip","fieldAliases":{"objectid":"objectid","territory_code":"TERRITORY_CODE"},"fields":[{"name":"objectid","type":"esriFieldTypeOID","alias":"objectid"},{"name":"territory_code","type":"esriFieldTypeString","alias":"TERRITORY_CODE","length":255}],"features":[{"attributes":{"objectid":6,"geography_name":11789,"level1_code":"1NE30011","level1_name":"Albany","team_instance_name":"HTN_Q1_2018"}}]}');
           res.setStatus('Ok');
        }else if(req.getEndpoint().contains('deleteFeatures')){ //Condition To delete records in ESRI in case of Remove sharing
           res.setBody('{"Response":[{"success":true}]}');
           res.setStatus('Ok');
        }
        else if(req.getEndpoint().equals(ESRIService.Service_URL__c+ESRIService.Account_Layer__c +'/query?')){
           system.debug('###### Inside  Account Layer ');

          res.setBody('{"objectIdFieldName":"objectid","objectIds":688180}');
          res.setStatus('Ok');
          
        }
        
        res.setStatusCode(200);
        
        return res;
	}
}