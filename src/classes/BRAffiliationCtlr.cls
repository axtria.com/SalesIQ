public with sharing class BRAffiliationCtlr {
    @AuraEnabled
    public static string getBusinessRuleId(String scenarioRuleInstanceId){
        list<Business_Rules__c> lstRules = [select id from Business_Rules__c where ScenarioRuleInstanceDetails__c =:scenarioRuleInstanceId];
        system.debug('lstRules--'+lstRules);
        if(lstRules.size()>0){
                return lstRules[0].Id;
        }else{
            return '';
        }
    }

    @AuraEnabled
    public static List<Data_Set_Rule_Map__c> getInputOutputDatasources(String scenarioRuleInstanceId) {

        List<Data_Set_Rule_Map__c> inputOutputDataSet = new Data_Set_Rule_Map__c[7];
        List<Data_Set_Rule_Map__c> inputOutputDataSourcesList = [SELECT dataset_id__c,dataset_id__r.Name,ds_type__c,Id,table_name__c,File_Type__c,Table_Display_Name__c FROM Data_Set_Rule_Map__c WHERE scenario_rule_instance_id__c =: scenarioRuleInstanceId];

        for(Data_Set_Rule_Map__c dsRuleMap : inputOutputDataSourcesList) {
            if(dsRuleMap.ds_type__c == 'I' && dsRuleMap.File_Type__c == CPGConstants.BASE_FILE_TYPE) {
                inputOutputDataSet[0] = dsRuleMap;
            } 
            else if(dsRuleMap.ds_type__c == 'I' && dsRuleMap.File_Type__c == CPGConstants.CUST_TERR_FILE_TYPE) {
                inputOutputDataSet[1] = dsRuleMap;
            }
            else if(dsRuleMap.ds_type__c == 'I' && dsRuleMap.File_Type__c == CPGConstants.ZIP_TERR_FILE_TYPE) {
                inputOutputDataSet[2] = dsRuleMap;
            }
            else if(dsRuleMap.ds_type__c == 'I' && dsRuleMap.File_Type__c == CPGConstants.AFFILIATION_FILE_TYPE) {
                inputOutputDataSet[3] = dsRuleMap;
            } 
            else if(dsRuleMap.ds_type__c == 'I' && dsRuleMap.File_Type__c == CPGConstants.BLOCK_FILE_TYPE) {
                inputOutputDataSet[4] = dsRuleMap;
            }
            else if(dsRuleMap.ds_type__c == 'I' && dsRuleMap.File_Type__c == CPGConstants.PRODUCT_FILE_TYPE) {
                inputOutputDataSet[5] = dsRuleMap;
            }
            else if(dsRuleMap.ds_type__c == 'O') {
                inputOutputDataSet[6] = dsRuleMap;
            }
        }
        return inputOutputDataSet;
    }

	@AuraEnabled
	public static list<BRAffiliationWrapper> setAffiliationRulesWrapper(String ruleType, String geographicPreference,String ruleLevel,String level, String selectedPositions,List<String> parsedJsonQueryList,List<String> whereClauseList,List<String> stagingClauseList,List<String> sourceQueryList) {

		List<BRAffiliationWrapper> affiliationRulesDataList = new List<BRAffiliationWrapper>();

		BRAffiliationWrapper detailData = new BRAffiliationWrapper();

		detailData.ruleType = ruleType;
		detailData.geographicPreference = geographicPreference;
		detailData.level = level; 
		detailData.selectedPositions = selectedPositions;
		detailData.parsedJsonQuery = parsedJsonQueryList;
		detailData.whereClauseQuery = whereClauseList;
		detailData.stagingClauseQuery = stagingClauseList;
		detailData.sourceQuery = sourceQueryList;
		detailData.ruleLevel = ruleLevel;

		affiliationRulesDataList.add(detailData);

		return affiliationRulesDataList;  
	}

	@AuraEnabled
	public static Boolean saveFinalValues (String rulename, String BRAffiliationWrapperList,String scenarioRuleInstanceId,String scenarioId,Boolean alreadyExistingRuleFlag, String selectedBusinessRuleId){

		System.debug('scenarioRuleInstanceId :: '+scenarioRuleInstanceId);
		System.debug('scenarioId :::: '+scenarioId);
		System.debug('rulename :::: '+rulename);

		System.debug('BRAffiliationWrapperList');
		System.debug(BRAffiliationWrapperList);
		System.debug('alreadyExistingRuleFlag :: ');
		System.debug(alreadyExistingRuleFlag);


		List<BRAffiliationWrapper> wrapper = (List<BRAffiliationWrapper>)JSON.deserialize(BRAffiliationWrapperList,List<BRAffiliationWrapper>.class);

		if(alreadyExistingRuleFlag == false){

			List<Scenario_Rule_Instance_Details__c> componenttypeList = [SELECT Component_Type_Master__c FROM Scenario_Rule_Instance_Details__c where Id =: scenarioRuleInstanceId limit 1];
			String componentTypeId = componenttypeList[0].Component_Type_Master__c;
			List<Business_Rule_Type_Master__c> businessruletypeval = [SELECT Id,Name FROM Business_Rule_Type_Master__c WHERE Component_Type__c =: componentTypeId];
			String businessruletypeid = businessruletypeval[0].Id;
			

			List<Business_Rules__c> existingBusinessRules = [ Select Business_Rule_Type_Master__c FROM Business_Rules__c where Scenario__c =: scenarioId and Business_Rule_Type_Master__c =: businessruletypeid ];

			Integer existingRulesCount = 0;

			if(existingBusinessRules != null && existingBusinessRules.size() > 0){
				existingRulesCount = existingBusinessRules.size();
			}

			System.debug('wrapper :::: '+wrapper);

			List<Business_Rules__c> finalbusinessrulelist = new List<Business_Rules__c>();

			Business_Rules__c obj = new Business_Rules__c();
			obj.Business_Rule_Type_Master__c = businessruletypeid;
			obj.Execution_Sequence__c = existingRulesCount + 1 ;
			obj.Is_Derived__c = false;
			obj.Name = rulename;
			obj.ScenarioRuleInstanceDetails__c = scenarioRuleInstanceId;
			obj.Scenario__c = scenarioId;
			obj.Rule_Type__c = wrapper[0].ruleType;
			obj.Geographic_Preference__c = wrapper[0].geographicPreference;
			obj.Level__c = wrapper[0].level;
			obj.Selected_Positions__c = wrapper[0].selectedPositions;
			obj.Rule_Level__c = wrapper[0].ruleLevel;
			
			finalbusinessrulelist.add(obj);

			if(finalbusinessrulelist.size() > 0){
				insert finalbusinessrulelist;
			}   
				
			System.debug('finalbusinessrulelist :: '+finalbusinessrulelist);

			String bruleId;
			for(Integer i=0;i<finalbusinessrulelist.size();i++){
				bruleId = finalbusinessrulelist[i].Id;
			}
			System.debug('bruleId ::: '+bruleId);
			
			List<BR_JoinRule__c> joinRuleList = new List<BR_JoinRule__c>();

			//if(wrapper[0].ruleType == CPGConstants.AFFILIATION_RULE_TYPE_BOTTOM_UP)
			//{

			for(BRAffiliationWrapper brwrapobject : wrapper)
			{
				for(integer i=0;i<brwrapobject.whereClauseQuery.size();i++)
				{
					/*if((Math.mod(Integer.valueOf(i),2) == 0))
					{
						sequence ++;
					}*/

					BR_JoinRule__c brjoinruleobj = new BR_JoinRule__c();
					brjoinruleobj.Scenario__c = scenarioId;
					brjoinruleobj.Business_Rules__c = bruleId;
					brjoinruleobj.where_expression__c = brwrapobject.stagingClauseQuery[i];
					brjoinruleobj.where_display_expression__c = brwrapobject.whereClauseQuery[i];
					brjoinruleobj.where_Json_Expression__c = brwrapobject.parsedJsonQuery[i];
					brjoinruleobj.where_expression_source__c = brwrapobject.sourceQuery[i];
					
					if(wrapper[0].ruleType == CPGConstants.AFFILIATION_RULE_TYPE_BOTTOM_UP) {
						// Bottom Up
						if((Math.mod(Integer.valueOf(i),2) == 0)){
							brjoinruleobj.Affiliation_Hierarchy__c = 'Child';
						}
						else{
							brjoinruleobj.Affiliation_Hierarchy__c = 'Parent';
						}
					} 
					else {
						// Top Down 
						if((Math.mod(Integer.valueOf(i),2) == 0)){
							brjoinruleobj.Affiliation_Hierarchy__c = 'Parent';
						}
						else{
							brjoinruleobj.Affiliation_Hierarchy__c = 'Child';
						}
					}
					
					brjoinruleobj.Execution_Sequence__c = obj.Execution_Sequence__c;
					joinRuleList.add(brjoinruleobj);
				}
			}
			//}

			/*else
			{
				for(BRAffiliationWrapper brwrapobject : wrapper)
				{
					for(integer i=0;i<brwrapobject.whereClauseQuery.size();i++)
					{

						BR_JoinRule__c brjoinruleobj = new BR_JoinRule__c();
						brjoinruleobj.Scenario__c = scenarioId;  
						brjoinruleobj.Business_Rules__c = bruleId;
						brjoinruleobj.where_expression__c = brwrapobject.stagingClauseQuery[i];
						brjoinruleobj.where_display_expression__c = brwrapobject.whereClauseQuery[i];
						brjoinruleobj.where_Json_Expression__c = brwrapobject.parsedJsonQuery[i];
						brjoinruleobj.where_expression_source__c = brwrapobject.sourceQuery[i];
						if((Math.mod(Integer.valueOf(i),2) == 0)){
							brjoinruleobj.Affiliation_Hierarchy__c = 'Parent';
						}
						else{
							brjoinruleobj.Affiliation_Hierarchy__c = 'Child';
						}
						//brjoinruleobj.Execution_Sequence__c = sequence;
						brjoinruleobj.Execution_Sequence__c = existingRulesCount + 1;
						joinRuleList.add(brjoinruleobj);
					}
				} 
			}*/

			System.debug('joinRuleList ::: '+joinRuleList);

			if(joinRuleList.size() > 0){
				insert joinRuleList;
				return true;
			}    
			else{
				return false; 
			}
		}

		else{ 
			/* Update Case for Affiliation Rules */

			List<Business_Rules__c> existingBusinessRuleId = [ Select Id,Rule_Type__c,Geographic_Preference__c,Level__c, Execution_Sequence__c FROM Business_Rules__c where Scenario__c =: scenarioId and Id =: selectedBusinessRuleId ];

			/*for(Integer j=0;j<existingBusinessRuleId.size();j++){

				existingBusinessRuleId[j].Rule_Type__c = wrapper[0].ruleType;
				existingBusinessRuleId[j].Geographic_Preference__c = wrapper[0].geographicPreference;
				existingBusinessRuleId[j].Level__c = wrapper[0].level;
			}*/

			if(existingBusinessRuleId != null && existingBusinessRuleId.size() > 0){
				if(wrapper[0].ruleLevel == 'Position')
					existingBusinessRuleId[0].Selected_Positions__c = wrapper[0].selectedPositions;
				else
					existingBusinessRuleId[0].Selected_Positions__c = '';

				if(rulename != 'undefined' || rulename != null || rulename != ''){
					existingBusinessRuleId[0].Name = rulename;
				}
					
				existingBusinessRuleId[0].Rule_Level__c = wrapper[0].ruleLevel;
				update existingBusinessRuleId;
			}

			String businessRuleId = existingBusinessRuleId[0].Id;

			List<BR_JoinRule__c> existingListToBeDeleted = [SELECT Affiliation_Hierarchy__c,Business_Rules__c,Execution_Sequence__c,Id,Scenario__c
			,where_display_expression__c,where_expression__c,where_Json_Expression__c FROM BR_JoinRule__c where Business_Rules__c =: businessRuleId];

			if(existingListToBeDeleted.size() > 0){
				delete existingListToBeDeleted;
			}

			List<BR_JoinRule__c> joinRuleList = new List<BR_JoinRule__c>();
			Integer sequence = 0;
			//if(wrapper[0].ruleType == CPGConstants.AFFILIATION_RULE_TYPE_BOTTOM_UP)
			//{
			for(BRAffiliationWrapper brwrapobject : wrapper)
			{
				for(integer i=0;i<brwrapobject.whereClauseQuery.size();i++)
				{
					BR_JoinRule__c brjoinruleobj = new BR_JoinRule__c();
					brjoinruleobj.Scenario__c = scenarioId;
					brjoinruleobj.Business_Rules__c = businessRuleId;
					brjoinruleobj.where_expression__c = brwrapobject.stagingClauseQuery[i];
					brjoinruleobj.where_display_expression__c = brwrapobject.whereClauseQuery[i];
					brjoinruleobj.where_Json_Expression__c = brwrapobject.parsedJsonQuery[i];
					brjoinruleobj.where_expression_source__c = brwrapobject.sourceQuery[i];
					
					if(wrapper[0].ruleType == CPGConstants.AFFILIATION_RULE_TYPE_BOTTOM_UP) {
						if((Math.mod(Integer.valueOf(i),2) == 0)){
							brjoinruleobj.Affiliation_Hierarchy__c = 'Child';
						}
						else{
							brjoinruleobj.Affiliation_Hierarchy__c = 'Parent';
						}
					}
					else 
					{
						if((Math.mod(Integer.valueOf(i),2) == 0)){
							brjoinruleobj.Affiliation_Hierarchy__c = 'Parent';
						}
						else{
							brjoinruleobj.Affiliation_Hierarchy__c = 'Child';
						}
					}

					brjoinruleobj.Execution_Sequence__c = existingBusinessRuleId[0].Execution_Sequence__c;
					joinRuleList.add(brjoinruleobj);
				}
			}
			//}
			/*else
			{
				for(BRAffiliationWrapper brwrapobject : wrapper)
				{
					for(integer i=0;i<brwrapobject.whereClauseQuery.size();i++)
					{
						if((Math.mod(Integer.valueOf(i),2) == 0))
						{
							sequence ++;
						}
						BR_JoinRule__c brjoinruleobj = new BR_JoinRule__c();

						brjoinruleobj.Scenario__c = scenarioId;  
						brjoinruleobj.Business_Rules__c = businessRuleId;
						brjoinruleobj.where_expression__c = brwrapobject.stagingClauseQuery[i];
						brjoinruleobj.where_display_expression__c = brwrapobject.whereClauseQuery[i];
						brjoinruleobj.where_Json_Expression__c = brwrapobject.parsedJsonQuery[i];
						brjoinruleobj.where_expression_source__c = brwrapobject.sourceQuery[i];
						if((Math.mod(Integer.valueOf(i),2) == 0)){
							brjoinruleobj.Affiliation_Hierarchy__c = 'Parent';
						}
						else{
							brjoinruleobj.Affiliation_Hierarchy__c = 'Child';
						}
						//brjoinruleobj.Execution_Sequence__c = sequence;
						joinRuleList.add(brjoinruleobj);
					}
				}
			}*/

			System.debug('joinRuleList ::: '+joinRuleList);

			if(joinRuleList.size() > 0){
				insert joinRuleList;
				return true;
			}    
			else{
				return false; 
			}
		}

	}

	@AuraEnabled
	public static void updateAffiliationRuleAttributes(String scenarioRuleInstanceId, Map<String,String> ruleAttributes)
	{

		system.debug(' ::::::scenarioRuleInstanceId ::::: ');
		system.debug(scenarioRuleInstanceId);

		system.debug(' ::::::ruleAttributes ::::: ');
		system.debug(ruleAttributes);
		
		List<Business_Rules__c> existingBusinessRulesList = [SELECT Id,Geographic_Preference__c,Rule_Type__c,Level__c FROM Business_Rules__c 
		WHERE ScenarioRuleInstanceDetails__c =: scenarioRuleInstanceId];

		for(Business_Rules__c brRule :existingBusinessRulesList){
			brRule.Geographic_Preference__c = ruleAttributes.get('geographicPreference');
			brRule.Rule_Type__c = ruleAttributes.get('affiliationRuleType');
			brRule.Level__c = ruleAttributes.get('affiliationLevel');
		}

		update existingBusinessRulesList;

	}

	@AuraEnabled
	public static Integer getHierarchyLevel(String scenarioId)
	{
		Scenario__c mysc = [select Team_Instance__r.Affiliation_Network__c, Team_Instance__r.Affiliation_Network__r.Hierarchy_Level__c from Scenario__c where id=: scenarioId];
		if(mysc.Team_Instance__r.Affiliation_Network__r.Hierarchy_Level__c != null)
		{
			return Integer.valueOf(mysc.Team_Instance__r.Affiliation_Network__r.Hierarchy_Level__c);
		}
		else
		{
			return null;
		}
	}

	@AuraEnabled
	public static List<BRAffiliationWrapper> callExistingBusinessRules (String scenarioId,String scenarioRuleInstanceId, String selectedBusinessRuleId){

		List<Business_Rules__c> existingBusinessRulesList = [SELECT Id,Geographic_Preference__c,Rule_Type__c,Level__c FROM Business_Rules__c 
		WHERE ScenarioRuleInstanceDetails__c =: scenarioRuleInstanceId AND Scenario__c =: scenarioId and Id = :selectedBusinessRuleId limit 1];

		if(existingBusinessRulesList.size() > 0){
			String businessRuleId = existingBusinessRulesList[0].Id;
			String geographicPreference = existingBusinessRulesList[0].Geographic_Preference__c;
			String ruleType = existingBusinessRulesList[0].Rule_Type__c;
			String level = existingBusinessRulesList[0].Level__c;


			String soql = 'SELECT Affiliation_Hierarchy__c,Business_Rules__c,Execution_Sequence__c,Id, where_display_expression__c,where_expression__c,where_Json_Expression__c,where_expression_source__c FROM BR_JoinRule__c where Business_Rules__c =: businessRuleId order by Affiliation_Hierarchy__c' ;

			// set the order by on the basis of rule type
            if(ruleType == SalesIQGlobalConstants.AFF_RULE_BOTTOM_UP) 
                soql += ' ASC' ;
            else   
                soql += ' DESC' ;

            List<BR_JoinRule__c> affiliationRulesBrUpdateList = Database.query(soql);

			System.debug('affiliationRulesBrUpdateList:::: '+affiliationRulesBrUpdateList);
			System.debug('affiliationRulesBrUpdateList size:::: '+affiliationRulesBrUpdateList.size());

			List<String> tempList1 = new List<String>();
			List<String> tempList2 = new List<String>(); 
			List<String> tempList3 = new List<String>(); 
			List<String> tempList4 = new List<String>(); 
			List<Decimal> tempList5 = new List<Decimal>();
			List<String> tempList6 = new List<String>(); 

			List<BRAffiliationWrapper> updatedBRAffiliationWrapperList = new List<BRAffiliationWrapper>();

			for(BR_JoinRule__c obj : affiliationRulesBrUpdateList)
			{
				tempList1.add(obj.where_Json_Expression__c);
				tempList2.add(obj.where_display_expression__c);
				tempList3.add(obj.where_expression__c);
				tempList4.add(obj.Affiliation_Hierarchy__c);
				tempList5.add(obj.Execution_Sequence__c);
				tempList6.add(obj.where_expression_source__c);
			}

			BRAffiliationWrapper affWrapperObj = new BRAffiliationWrapper();

			affWrapperObj.ruleType = ruleType;
			affWrapperObj.geographicPreference = geographicPreference;
			affWrapperObj.level = level;
			affWrapperObj.parsedJsonQuery = tempList1;
			affWrapperObj.whereClauseQuery = tempList2;
			affWrapperObj.stagingClauseQuery = tempList3;
			affWrapperObj.affiliationHierarchy = tempList4;
			affWrapperObj.executionSequence = tempList5;
			affWrapperObj.sourceQuery = tempList6;

			updatedBRAffiliationWrapperList.add(affWrapperObj);

			System.debug('updatedBRAffiliationWrapperList :: '+updatedBRAffiliationWrapperList);

			return updatedBRAffiliationWrapperList;  
		}
		else{
			return null;
		}
	}


	public class BRAffiliationWrapper {

		@AuraEnabled
		public String ruleType {get;set;}
		@AuraEnabled
		public String geographicPreference {get;set;}
		@AuraEnabled
		public String level{get;set;}
		@AuraEnabled
		public List<String> parsedJsonQuery {get; set;}      
		@AuraEnabled
		public List<String> whereClauseQuery {get; set;}
		@AuraEnabled
		public List<String> stagingClauseQuery {get; set;}
		@AuraEnabled
		public List<String> sourceQuery {get; set;}
		@AuraEnabled
		public List<String> affiliationHierarchy {get; set;}
		@AuraEnabled
		public List<Decimal> executionSequence {get;set;}  
		@AuraEnabled
		public String ruleLevel {get;set;}
		@AuraEnabled
		public String selectedPositions {get;set;}
	}
}