/******************************************************************************************************************************************************
 * Name         :   BRUnionCtrl.cls
 * Description  :   Controller for Union component. 
 * Author       :   Aakash Gupta
 * Created On   :   03/22/2018
******************************************************************************************************************************************************/

public without Sharing class BRUnionCtrl {

    @AuraEnabled
    public static String getUnionFieldDetails(String scenarioInstId) {

    	List<BR_JoinRule__c> dataObjectInstance = [SELECT Select_Display_Expression__c, Select_Execute_Expression__c FROM BR_JoinRule__c where Business_Rules__r.ScenarioRuleInstanceDetails__c =:scenarioInstId ];
        if(dataObjectInstance.isEmpty())
        	return '';
        else
        	return dataObjectInstance[0].Select_Display_Expression__c;
    }


   @AuraEnabled
    public static List<Data_Set_Rule_Map__c> getInputOutputDatasources(String scenarioRuleInstanceId) {

          // first element is first data set, 2nd is second dataset and third is output
      List<Data_Set_Rule_Map__c> inputOutputDataSet = new List<Data_Set_Rule_Map__c>{null,null,null};
      List<Data_Set_Rule_Map__c> inputOutputDataSourcesList = [SELECT dataset_id__c,dataset_id__r.Name,ds_type__c,Id,table_name__c,File_Type__c,Table_Display_Name__c FROM Data_Set_Rule_Map__c WHERE scenario_rule_instance_id__c =: scenarioRuleInstanceId ORDER BY Table_Display_Name__c DESC];
  System.debug('inputOutputDataSourcesList :: '+inputOutputDataSourcesList);
  System.debug('inputOutputDataSourcesList size:: '+inputOutputDataSourcesList.size());
      for(Data_Set_Rule_Map__c dsRuleMap : inputOutputDataSourcesList) {
      
        inputOutputDataSet.add(dsRuleMap);
      }
      System.debug('inputOutputDataSet :: '+inputOutputDataSet);
      System.debug('inputOutputDataSet size :: '+inputOutputDataSet.size());
      return inputOutputDataSet;
    }
}