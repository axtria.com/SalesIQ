public class CPGConstants {
    public static final string CPGCOMPUTATION='CPGComputation';
    public static final string DERIVED='Derived';
    public static final string SUCCESS='success';
    public static final string ERROR='error';
    public static final string READY='Ready';
    public static final string CYCLICREFERENCEERROR='There exists a cyclic reference';
    public static final string SAVEDMSG='Saved';
    public static final string ERRORMSG='Error';
    
    public static final string DERIVEDFIELD='Derived Field';
    
    public static final string CATEGORY='Category';
    public static final string COMPUTE='Compute';
    public static final string RANK='Rank';
    public static final string AGGREGATE='Aggregate';
    public static final string FILTER='Filter';
    public static final string talend_Instance='BRMS';
    public static final string CALLBALANCING='Call Balancing';

    public static final String DATATYPE_TEXT = 'Text';
    public static final String DATATYPE_NUMERIC = 'Numeric';
    public static final String DATATYPE_TEXTNUMERIC = 'TextNumeric';
    public static final String DATATYPE_MULTIVALUE = 'Multivalue';

    public static final String BASE_FILE_TYPE = 'Base';
    public static final String ASSIGNMENT_FILE_TYPE = 'Assignment';
    public static final String AFFILIATION_FILE_TYPE = 'Affiliation';
    public static final String CUST_TERR_FILE_TYPE = 'Cust Terr';
    public static final String ZIP_TERR_FILE_TYPE = 'Zip Terr';
    public static final String BLOCK_FILE_TYPE = 'Block';
    public static final String PRODUCT_FILE_TYPE = 'Product';
    public static final String AFFILIATION_RULE_TYPE_BOTTOM_UP = 'Bottom Up';
    public static final String AFFILIATION_RULE_TYPE_TOP_DOWN = 'Top Down';
    
    public static final string CPGPARSERHELPER='CPGParserHelper';

    // Scenario scheduling related constants
    public static final String QUEUE_LOGGER_RECORDTYPE = 'Queue Logger';
    public static final String QUEUE_LOGGER_RECORDTYPE_API = 'Queue_Logger';
    public static final String QUEUE_LOGGER_TYPE_MASTER = 'Master Data Syncing';
    public static final String AFFILIATION_NETWORK_DEPTH= 'Affiliation Network Depth';
    
    
    public class Response{
        @AuraEnabled
        public string status{get;set;}
        @AuraEnabled
        public string msg{get;set;}
        @AuraEnabled
        public List<sObject> lstData {get;set;}
        
    }
       
        

}