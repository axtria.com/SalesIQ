/*
Author : Ankit Manendra
Description : Test class for CPGCanvasCtrl
Date : 3 Novomber, 2017

*/

@isTest
private class TestCPGCanvasCtrl{
    static testMethod void cpgCanvasTest() {
    
        Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj;
        
        ComponentTypeMaster__c ComTypeMasObj = new ComponentTypeMaster__c();
        ComTypeMasObj.Name='Call Balancing';
        ComTypeMasObj.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj.DisplayIcon__c='add';
        ComTypeMasObj.DisplayIconClass__c='window information';
        insert ComTypeMasObj;

        Business_Rule_Template__c  busRuleTemObj = new Business_Rule_Template__c();
        busRuleTemObj.isActive__c=true;
        busRuleTemObj.isInternal__c=true;
        insert busRuleTemObj;
        
        BusinessRule_Template_Details__c busRuleTemDet = new BusinessRule_Template_Details__c();
        busRuleTemDet.is_Visible__c=true;
        busRuleTemDet.Business_Rule_Template_Id__c=busRuleTemObj.id;
        insert busRuleTemDet;
        
        BusinessRule_Template_Details__c busRuleTemDet1 = new BusinessRule_Template_Details__c();
        busRuleTemDet1.is_Visible__c=true;
        busRuleTemDet1.Business_Rule_Template_Id__c=busRuleTemObj.id;
        insert busRuleTemDet1;
        
        Config_Call_Balancing_Map__c callBalObj = new Config_Call_Balancing_Map__c();
        callBalObj.Business_Rule_Template_Details__c=busRuleTemDet.id;
        insert callBalObj;
        
        Config_Call_Balancing_Map__c callBalObj1 = new Config_Call_Balancing_Map__c();
        callBalObj1.Business_Rule_Template_Details__c=busRuleTemDet1.id;
        callBalObj1.Base_Config_Call_Balancing_Map_Id__c=callBalObj.id;
        insert callBalObj1;
        
        Organization_Master__c orgMaster = new Organization_Master__c();
        orgMaster.Name ='Russia';
        orgMaster.Org_Level__c = 'Global';
        orgMaster.Parent_Country_Level__c = true;
        insert orgMaster;
        
        Country__c countryobj = new Country__c();
        countryobj.Name = 'Spain';
        countryobj.Parent_Organization__c= orgMaster.Id;
        countryobj.Status__c = 'Active'; 
        insert countryobj;
        
        Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        insert scenarioObj;
        
        Config_Call_Balancing_Constraint_Detail__c configCallBalObj = new Config_Call_Balancing_Constraint_Detail__c();
        configCallBalObj.Config_Call_Balancing_Map_Id__c = callBalObj.id;
        insert configCallBalObj;
         
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        sceRuleInsDetObj.Component_Display_Order__c=4;
        sceRuleInsDetObj.Is_Visible__c=true;
        sceRuleInsDetObj.Component_Type_Master__c=ComTypeMasObj.id;
        sceRuleInsDetObj.BRT_Details_Id__c=busRuleTemDet.id;
        insert sceRuleInsDetObj;
        
        Data_Set__c dataSetObj = new Data_Set__c();
        dataSetObj.Name = 'Call Plan Summary';
        insert dataSetObj;
        
        Data_Set_Rule_Map__c dataSetRuleObj = new Data_Set_Rule_Map__c();
        dataSetRuleObj.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj.ds_type__c='I';
        dataSetRuleObj.dataset_id__c=dataSetObj.id;
        insert dataSetRuleObj;
        
        Data_Set_Rule_Map__c dataSetRuleObj1 = new Data_Set_Rule_Map__c();
        dataSetRuleObj1.scenario_rule_instance_id__c=null;
        dataSetRuleObj1.ds_type__c='I';
        dataSetRuleObj1.dataset_id__c=dataSetObj.id;
        dataSetRuleObj1.Business_Rule_Template_Details__c=busRuleTemDet.id;
        insert dataSetRuleObj1;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj.Name='Rank';
        insert busRuleTypeMasObj;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj1 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj1.Name='Category';
        insert busRuleTypeMasObj1;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj2 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj2.Name='Increase Reach';
        insert busRuleTypeMasObj2;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj3 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj3.Name='Decrease Frequency';
        insert busRuleTypeMasObj3;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj4 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj4.Name='Decrease Reach';
        insert busRuleTypeMasObj4;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj5 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj5.Name='Increase Frequency';
        insert busRuleTypeMasObj5;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj6 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj6.Name='Aggregate';
        insert busRuleTypeMasObj6;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj7 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj7.Name='Filter';
        insert busRuleTypeMasObj7;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj8 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj8.Name='Compute';
        insert busRuleTypeMasObj8;
        
        
        Business_Rules__c busRuleObjagg = new Business_Rules__c();
        busRuleObjagg.Name='Rank rule 278';
        busRuleObjagg.Business_Rule_Type_Master__c=busRuleTypeMasObj6.id;
        busRuleObjagg.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObjagg.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjagg.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        insert busRuleObjagg;
        
        Business_Rules__c busRuleObjaggregation = new Business_Rules__c();
        busRuleObjaggregation.Name='Rank rule 278';
        busRuleObjaggregation.Business_Rule_Type_Master__c=busRuleTypeMasObj6.id;
        busRuleObjaggregation.ScenarioRuleInstanceDetails__c=null;
        busRuleObjaggregation.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjaggregation.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        busRuleObjaggregation.Parent_Rule_Id__c=busRuleObjagg.id;
        busRuleObjaggregation.Scenario__c=scenarioObj.id;
        insert busRuleObjaggregation;
        
        
        Business_Rules__c busRuleObjdecFreq = new Business_Rules__c();
        busRuleObjdecFreq.Name='Rank rule 278';
        busRuleObjdecFreq.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObjdecFreq.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObjdecFreq.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjdecFreq.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        insert busRuleObjdecFreq;
        
        Business_Rules__c busRuleObjdecFrequency = new Business_Rules__c();
        busRuleObjdecFrequency.Name='Rank rule 278';
        busRuleObjdecFrequency.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObjdecFrequency.ScenarioRuleInstanceDetails__c=null;
        busRuleObjdecFrequency.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjdecFrequency.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        busRuleObjdecFrequency.Parent_Rule_Id__c=busRuleObjdecFreq.id;
        busRuleObjdecFrequency.Scenario__c=scenarioObj.id;
        insert busRuleObjdecFrequency;
        
        Rank_Rule__c rankObj = new Rank_Rule__c();
        rankObj.Business_Rules__c=busRuleObjdecFrequency.id;
        insert rankObj;
    
        Business_Rules__c busRuleObjCom = new Business_Rules__c();
        busRuleObjCom.Name='Rank rule 278';
        busRuleObjCom.Business_Rule_Type_Master__c=busRuleTypeMasObj8.id;
        busRuleObjCom.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObjCom.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        insert busRuleObjCom;
        
        Business_Rules__c busRuleObjComPute = new Business_Rules__c();
        busRuleObjComPute.Name='Rank rule 278';
        busRuleObjComPute.Business_Rule_Type_Master__c=busRuleTypeMasObj8.id;
        busRuleObjComPute.ScenarioRuleInstanceDetails__c=null;
        busRuleObjComPute.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjComPute.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        busRuleObjComPute.Parent_Rule_Id__c=busRuleObjCom.id;
        busRuleObjComPute.Scenario__c=scenarioObj.id;
        insert busRuleObjComPute;
        
        Business_Rules__c busRuleObjDecRea = new Business_Rules__c();
        busRuleObjDecRea.Name='Rank rule 278';
        busRuleObjDecRea.Business_Rule_Type_Master__c=busRuleTypeMasObj4.id;
        busRuleObjDecRea.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObjDecRea.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjDecRea.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        insert busRuleObjDecRea;
        
        Business_Rules__c busRuleObjDecReach = new Business_Rules__c();
        busRuleObjDecReach.Name='Rank rule 278';
        busRuleObjDecReach.Business_Rule_Type_Master__c=busRuleTypeMasObj4.id;
        busRuleObjDecReach.ScenarioRuleInstanceDetails__c=null;
        busRuleObjDecReach.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjDecReach.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        busRuleObjDecReach.Parent_Rule_Id__c=busRuleObjDecRea.id;
        busRuleObjDecReach.Scenario__c=scenarioObj.id;
        insert busRuleObjDecReach;
        
        Business_Rules__c busRuleObjFil = new Business_Rules__c();
        busRuleObjFil.Name='Rank rule 278';
        busRuleObjFil.Business_Rule_Type_Master__c=busRuleTypeMasObj7.id;
        busRuleObjFil.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObjFil.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjFil.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        insert busRuleObjFil;
        
        Business_Rules__c busRuleObjFilter = new Business_Rules__c();
        busRuleObjFilter.Name='Rank rule 278';
        busRuleObjFilter.Business_Rule_Type_Master__c=busRuleTypeMasObj7.id;
        busRuleObjFilter.ScenarioRuleInstanceDetails__c=null;
        busRuleObjFilter.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjFilter.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        busRuleObjFilter.Parent_Rule_Id__c=busRuleObjFil.id;
        busRuleObjFilter.Scenario__c=scenarioObj.id;
        insert busRuleObjFilter;
        
        
        Business_Rules__c busRuleObj2 = new Business_Rules__c();
        busRuleObj2.Name='Rank rule 278';
        busRuleObj2.Business_Rule_Type_Master__c=busRuleTypeMasObj1.id;
        busRuleObj2.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj2.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObj2.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        insert busRuleObj2;
        
        Business_Rules__c busRuleObj3 = new Business_Rules__c();
        busRuleObj3.Name='Rank rule 278';
        busRuleObj3.Business_Rule_Type_Master__c=busRuleTypeMasObj1.id;
        busRuleObj3.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj3.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObj3.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        insert busRuleObj3;
        
        Business_Rules__c busRuleObjCat = new Business_Rules__c();
        busRuleObjCat.Name='Rank rule 278';
        busRuleObjCat.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObjCat.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObjCat.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjCat.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        busRuleObjCat.Parent_Rule_Id__c=busRuleObj3.id;
        busRuleObjCat.Scenario__c=scenarioObj.id;
        insert busRuleObjCat;
        
        Business_Rules__c busRuleObj = new Business_Rules__c();
        busRuleObj.Name='Rank rule 278';
        busRuleObj.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObj.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObj.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        busRuleObj.Parent_Rule_Id__c=busRuleObj2.id;
        busRuleObj.Scenario__c=scenarioObj.id;
        insert busRuleObj;
        
        Business_Rules__c busRuleObj1 = new Business_Rules__c();
        busRuleObj1.Name='Rank rule 278';
        busRuleObj1.Business_Rule_Type_Master__c=busRuleTypeMasObj2.id;
        busRuleObj1.ScenarioRuleInstanceDetails__c=null;
        busRuleObj1.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObj1.Aggregate_Business_Rule_Template_Details__c='Test';
        busRuleObj1.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        insert busRuleObj1;
        
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj.Param_Name__c='derived field 278';
        busRuleFieldObj.Business_Rule__c=busRuleObj.id;
        busRuleFieldObj.Param_Data_Type__c='Numeric';
        busRuleFieldObj.Param_Type__c='Derived';
        busRuleFieldObj.IO_Flag__c='O';
        
        insert busRuleFieldObj;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj1 = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj1.Param_Name__c='derived field 278';
        busRuleFieldObj1.Business_Rule__c=busRuleObj2.id;
        busRuleFieldObj1.Param_Data_Type__c='Numeric';
        busRuleFieldObj1.Param_Type__c='Derived';
        busRuleFieldObj1.IO_Flag__c='O';
        
        insert busRuleFieldObj1;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj2 = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj2.Param_Name__c='derived field 278';
        busRuleFieldObj2.Business_Rule__c=busRuleObj1.id;
        busRuleFieldObj2.Param_Data_Type__c='Numeric';
        busRuleFieldObj2.Param_Type__c='Derived';
        busRuleFieldObj2.IO_Flag__c='O';
        
        insert busRuleFieldObj2;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj3 = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj3.Param_Name__c='derived field 278';
        busRuleFieldObj3.Business_Rule__c=busRuleObjComPute.id;
        busRuleFieldObj3.Param_Data_Type__c='Numeric';
        busRuleFieldObj3.Param_Type__c='Derived';
        busRuleFieldObj3.IO_Flag__c='O';
        
        insert busRuleFieldObj3;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj4 = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj4.Param_Name__c='derived field 278';
        busRuleFieldObj4.Business_Rule__c=busRuleObjDecReach.id;
        busRuleFieldObj4.Param_Data_Type__c='Numeric';
        busRuleFieldObj4.Param_Type__c='Derived';
        busRuleFieldObj4.IO_Flag__c='O';
        
        insert busRuleFieldObj4;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj5 = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj5.Param_Name__c='derived field 278';
        busRuleFieldObj5.Business_Rule__c=busRuleObjdecFrequency.id;
        busRuleFieldObj5.Param_Data_Type__c='Numeric';
        busRuleFieldObj5.Param_Type__c='Derived';
        busRuleFieldObj5.IO_Flag__c='O';
        
        insert busRuleFieldObj5;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj6 = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj6.Param_Name__c='derived field 278';
        busRuleFieldObj6.Business_Rule__c=busRuleObjaggregation.id;
        busRuleFieldObj6.Param_Data_Type__c='Numeric';
        busRuleFieldObj6.Param_Type__c='Derived';
        busRuleFieldObj6.IO_Flag__c='O';
        
        insert busRuleFieldObj6;
        
        Aggregate_Rule_Level__c aggLevel = new Aggregate_Rule_Level__c();
        aggLevel.Business_Rules__c=busRuleObjaggregation.id;
        insert aggLevel;
        
        Aggregate_Rule_Detail__c aggRule = new Aggregate_Rule_Detail__c();
        aggRule.Business_Rules__c=busRuleObjaggregation.id;
        insert aggRule;
        
        list<BusinessRule_Template_Details__c> busRuleTemporaryList = new list<BusinessRule_Template_Details__c>();
        busRuleTemporaryList.add(busRuleTemDet);
        
        list<Scenario_Rule_Instance_Details__c> sceRuleDetList = new list<Scenario_Rule_Instance_Details__c>();
        sceRuleDetList.add(sceRuleInsDetObj);
        
        Data_Set_Column_Detail__c dataSetColObj = new Data_Set_Column_Detail__c();
        dataSetColObj.dataset_id__c=dataSetObj.id;
        insert dataSetColObj;
        
        Category_Derived_Variable_Metadata__c catDerMetObj = new Category_Derived_Variable_Metadata__c();
        catDerMetObj.Business_Rule_Id__c=busRuleObj.id;
        insert catDerMetObj;
        
        Category_Derived_Variable_Metadata__c catDerMetObj1 = new Category_Derived_Variable_Metadata__c();
        catDerMetObj1.Business_Rule_Id__c=busRuleObj1.id;
        insert catDerMetObj1;
        
        Category_Derived_Variable_Metadata__c catDerMetObj2 = new Category_Derived_Variable_Metadata__c();
        catDerMetObj2.Business_Rule_Id__c=busRuleObjDecReach.id;
        insert catDerMetObj2;
        
        Category_Derived_Variable_Metadata__c catDerMetObj3 = new Category_Derived_Variable_Metadata__c();
        catDerMetObj3.Business_Rule_Id__c=busRuleObjdecFrequency.id;
        insert catDerMetObj3;
        
        Category_Rule_Entity_Detail__c catRuleEntDet = new Category_Rule_Entity_Detail__c();
        catRuleEntDet.Base_Entity_Id__c=null;
        catRuleEntDet.Business_Rule__c=busRuleObj1.id;
        insert catRuleEntDet;
        
        Category_Rule_Entity_Detail__c catRuleEntDet1 = new Category_Rule_Entity_Detail__c();
        catRuleEntDet1.Base_Entity_Id__c=null;
        catRuleEntDet1.Business_Rule__c=busRuleObjDecReach.id;
        insert catRuleEntDet1;
        
        Category_Rule_Entity_Detail__c catRuleEntDet2 = new Category_Rule_Entity_Detail__c();
        catRuleEntDet2.Base_Entity_Id__c=null;
        catRuleEntDet2.Business_Rule__c=busRuleObjdecFrequency.id;
        insert catRuleEntDet2;
        
        Filter_Rule_Entity_Detail__c filRuleEntDet = new Filter_Rule_Entity_Detail__c();
        filRuleEntDet.Business_Rules__c=busRuleObjFilter.id;
        filRuleEntDet.Base_Filter_Rule_Entity_Detail__c=null;
        insert filRuleEntDet;
        
        Computation_Rule_Detail__c comRuleDet = new Computation_Rule_Detail__c();
        comRuleDet.Business_Rules__c=busRuleObjComPute.id;
        insert comRuleDet;
        
        system.debug('=====busRuleObj2===='+busRuleObj2.id);
        system.debug('=====busRuleObj1===='+busRuleObj1.id);
        system.debug('=====busRuleObj===='+busRuleObj.id);
        system.debug('=====busRuleObjCat===='+busRuleObjCat.id);
        
        Test.startTest();
        
        List<BusinessRule_Template_Details__c> busRuleTempList = CPGCanvasCtrl.getBusinessRuleDetails(busRuleTemObj.id);
        List<Scenario_Rule_Instance_Details__c> sceRuleInsList = CPGCanvasCtrl.checkScenarioExistence(scenarioObj.id);
        List<Scenario_Rule_Instance_Details__c> sceRuleInsList1 = CPGCanvasCtrl.getScenarioViewInstance(scenarioObj.id);
        List<Scenario_Rule_Instance_Details__c> sceRuleInsList2 = CPGCanvasCtrl.getScenarioInstanceViewListDetails(scenarioObj.id,'4');
        List<Scenario_Rule_Instance_Details__c> sceRuleInsList3 = CPGCanvasCtrl.createScenarioRuleInstance(busRuleTemporaryList,scenarioObj.id);
        List<ComponentTypeMaster__c> compTypeMasList = CPGCanvasCtrl.getComponentDetails(sceRuleDetList,scenarioObj.id);
        List<String> sceList = CPGCanvasCtrl.getDataset(sceRuleDetList,scenarioObj.id);
        List<Business_Rules__c> busRuleList = CPGCanvasCtrl.getcomputeRules(sceRuleDetList);
        List<BusinessRule_Template_Details__c> businessRuleDetails = CPGCanvasCtrl.getBusinessRuleDetails('templateID');
        String countryIDs = CPGCanvasCtrl.getCountryAttributes(countryobj.Id);
       // String orgNameSpace = CPGCanvasCtrl.getOrgNamespaceServer();
        Boolean lockCheck = CPGCanvasCtrl.checkRunLocked(scenarioObj.Id);
        List<Scenario_Rule_Instance_Details__c> scenarioInsSUmmary = CPGCanvasCtrl.getScenarioViewInstanceSummarized(scenarioObj.Id); 
        List<Scenario_Rule_Instance_Details__c> scenarioInsViewListDetails = CPGCanvasCtrl.getScenarioInstanceViewListDetailsSummarized(scenarioObj.Id,'1');
        List<String> viewDetails = CPGCanvasCtrl.getScenarioViewDetails(scenarioObj.Id);
        //String viewAffiliationId = CPGCanvasCtrl.getViewAffiliationId(scenarioObj.Id);
        //String affiliationNetworkworkid = CPGCanvasCtrl.getAffiliationNetworkId(scenarioObj.Id);
        String scenarioStatus = CPGCanvasCtrl.getScenarioStatus(scenarioObj.Id);
        String scenarioName = CPGCanvasCtrl.getScenarioName(scenarioObj.Id);
        CPGCanvasCtrl.callRun(scenarioObj.Id);
        CPGCanvasCtrl.callDelta(scenarioObj.Id);
        CPGCanvasCtrl.runFullandPromote(scenarioObj.Id);
        CPGCanvasCtrl.runDeltaAndPromote(scenarioObj.Id);
        Map<String,String> changePending = CPGCanvasCtrl.isChangeRequestPending(scenarioObj.Id,true);
        //String getDetails = CPGCanvasCtrl.getDetailsId(scenarioObj.Id);
        String getRulesExe = CPGCanvasCtrl.getRuleExecutionStatus(scenarioObj.Id);
        CPGCanvasCtrl.updateScenarioDataObjectMap(scenarioObj.Id,true);
        Test.stopTest();
        
    
    }
    @Istest(SeeAllData = true)
    static void cpgCanvasTest1() {
        String orgNameSpace = CPGCanvasCtrl.getOrgNamespaceServer();
        
    }
 }
