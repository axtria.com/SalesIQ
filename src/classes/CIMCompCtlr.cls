public with sharing class CIMCompCtlr{

    @RemoteAction
    public static  List<Position_Account__C> getPosAccount(String sql ,List<String> accIds){
        try{
            system.debug('soql is'+accIds);
            Set<String> accSet = New Set<String>();
            accSet.addAll(accIds);
            String soql = sql+' and account__C IN : accSet';
            system.debug('soql is'+soql);
              return database.query(soql);       
        }catch(Exception e){
            return null;
        }

    }

    @RemoteAction  
    public static String getDestinationPositionFilterCondition(){
        String filterCondition = 'false';
        if(Alignment_Global_Settings__c.getValues('FilterDestinationPosition') != null)
                filterCondition = Alignment_Global_Settings__c.getValues('FilterDestinationPosition').Tree_Hierarchy_Sort_Field__c;

        return filterCondition;

    } 
}