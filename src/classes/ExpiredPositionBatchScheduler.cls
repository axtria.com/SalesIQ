/************************************************************************************************
Name			: ExpiredPositionBatchScheduler
Description    	: Scheduler class for ProcessExpiredPositionsBatch
Last Updated By	: 
************************************************************************************************/
global with sharing class ExpiredPositionBatchScheduler implements Schedulable {
	global void execute(SchedulableContext sc) {
    	/*ProcessExpiredPositionsBatch batchInstance = new ProcessExpiredPositionsBatch();
    	database.executebatch(batchInstance,200); */
    	if(TriggerContol__c.getValues('ProcessExpiredPositionsBatch') == null){
			ProcessExpiredPositionsBatch batchInstance = new ProcessExpiredPositionsBatch();
    		database.executebatch(batchInstance,200); 
		}

    	
    	UpdateEmployeeBatch empBatch = new UpdateEmployeeBatch();
    	database.executebatch(empBatch,200);


        ProcessCRAssignmentBatch updateHierarchy = new ProcessCRAssignmentBatch(new set<ID>{}, 'CR_Position__c');
        database.executebatch(updateHierarchy,200);


   }
}