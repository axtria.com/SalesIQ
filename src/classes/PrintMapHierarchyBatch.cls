/**********************************************************************************************
@author     :  Harkirat Singh
@date       : 
@description: This is the controller is used to print current Hierarchy  of the map
Revison(s)  : 
**********************************************************************************************/

global class PrintMapHierarchyBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {
    
    public string logid{get;set;}
    
    public PrintMapHierarchyBatch(string id1)
    {
        logid=id1;  
    }
     
    global Database.QueryLocator start(Database.BatchableContext BC)
    {    
       String cancelStatus='Cancelled';
       String query = 'SELECT CreatedDate,Id,Interface_Name__c,Job_Status__c,Name,Output__c,Position__r.Client_Position_Code__c, Position__r.Hierarchy_Level__c,Request_Type__c,Team_Instance__r.Name,Team_Instance__r.Team_Instance_Code__c,Team_Instance__r.Geography_Type_Name__r.Geography_Table__c,Team_Instance__r.Team__r.Country__r.Country_Code__c,Position__r.Position_Type__c,Web_Map_as_JSON__c FROM Activity_Log__c WHERE id=:logid AND Job_Status__c !=:cancelStatus';
       return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Activity_Log__c> logs)
    {    
        try
        {
            SalesIQUtility.JsonParse tokenObject = SalesIQUtility.getAccessToken();
            MapServerURL__c ESRIService = MapServerURL__c.getOrgDefaults();   
            
            string  url;
            String printUrl=ESRIService.Print_URL__c;
            string table= ESRIService.ZIPTERR_Table__c; //'alignmax.sde.Nextgen_Dev_Admin_28112016';
            list<ETL_Config__c> lstETLInfo = ScenarioUtility.getETLConfigByName(SalesIQGlobalConstants.PYTHON_JOB);
            
            //logs[0].Job_Status__c='In Progress';
        
            if(logs[0].Position__r.Hierarchy_Level__c =='1' || logs[0].Position__r.Hierarchy_Level__c =='2')
            {
                Http h = new Http();
                HttpRequest req = new HttpRequest();
                String address = printUrl+'/submitJob?';
                 //String body='tableName='+table+'&requestID='+ logs[0].id ;
               
               // system.debug('find----body'+body);
                String address1;
         String body='tableName='+table+'&requestID='+ logs[0].id+'&username='+lstETLInfo[0].SF_UserName__c+'&namespace='+SalesIQGlobalConstants.NAME_SPACE+'&password='+lstETLInfo[0].SF_Password__c+'&securitytoken='+lstETLInfo[0].S3_Security_Token__c+'&orgtype='+lstETLInfo[0].Org_Type__c+'&f=json';

          system.debug('body is'+body);              
               req.setEndPoint(address);   //+'?tableName='+table+'&requestID='+ logs[0].id+'&username='+lstETLInfo[0].SF_UserName__c+','+lstETLInfo[0].SF_Password__c+'&namespace='+SalesIQGlobalConstants.NAME_SPACE+'&securitytoken='+lstETLInfo[0].S3_Security_Token__c+'&orgtype='+lstETLInfo[0].Org_Type__c+'&f=json');
            //req.setEndPoint(address+'?tableName='+table+'&requestID='+ logs[0].id+'&username='+lstETLInfo[0].SF_UserName__c+'&namespace='+SalesIQGlobalConstants.NAME_SPACE+'&password=axtria@1234&securitytoken='+lstETLInfo[0].S3_Security_Token__c+'&orgtype='+lstETLInfo[0].Org_Type__c+'&f=json');
                req.setMethod('POST');
            req.setTimeout(120000);
            req.setHeader('X-Frame-Options', 'ALLOW-FROM');
            req.setHeader('Pragma', 'no-cache');
            req.setHeader('Cache-Control', 'no-cache');
            req.setHeader('X-XSS-Protection', '1');
            req.setHeader('x-content-type-options', 'nosniff');
            req.setHeader('SET-COOKIE', 'HttpOnly');
            req.setHeader('Content-Length', '256');
            req.setBody(body);
                 req.setMethod('POST');
                req.setTimeout(120000);
                
                HttpResponse res = h.send(req);
                system.debug('response' +res);
                String loc = res.getHeader('Location');
                String status;
                 
                // get location of the redirect
                system.debug('loc===='+loc);
                system.debug('status====' + res.getStatus());
                

                Integer start = System.Now().millisecond();
                system.debug('-----start-----'+start);  

                Long startingTime = System.now().getTime(); // Num milliseconds since Jan 1 1970
                    
                try
                {
                    if(res.getStatusCode() ==200)
                    {
                        system.debug('res===='+res);
                        system.debug('status====' + res.getStatus());
                        JSONParser parser = JSON.createParser(res.getBody());
                        System.JSONToken token,token2, token3;
    
                        parser.nextToken();    // Eat first START_OBJECT 
                        string  jobStatus;
                        string  jobId;
                        while((token=parser.nextToken()) != null)
                        {
                            system.debug('---Text of Token---'+parser.getText());
                            if(((token=parser.getCurrentToken()) == JSONToken.FIELD_NAME )&&(parser.getText()=='jobStatus')){
                                parser.nextToken();
                                jobStatus=Parser.getText();
                                system.debug('--value is--'+jobStatus);
                            
                            }
                            
                            if(((token=parser.getCurrentToken()) == JSONToken.FIELD_NAME )&&(parser.getText()=='jobId')){
                                parser.nextToken();
                                jobId=Parser.getText();
                                system.debug('--value is of job id--'+jobId);
                                          
                            }
                        }
                        
                        if(status=='esriJobFailed')
                        {
                            logs[0].Job_Status__c='Failed';
                            update logs;    
                        } 
                        
                        if(jobStatus=='esriJobSucceeded')
                        {
                            status='esriJobSucceeded';
                        }
                        else
                        {
                            address = printUrl+'/jobs/'+jobId;
                            logs[0].External_Job_ID__c=jobId;
                            update logs[0];
                            
                            String day = string.valueOf(system.now().day());
                            String month = string.valueOf(system.now().month());
                            String hour = string.valueOf(system.now().hour());
                            String minute = string.valueOf(system.now().minute());
                            String second = string.valueOf(system.now().second()+30);
                            //String second = string.valueOf(system.now().second());
                            String year = string.valueOf(system.now().year());
                            
                            Integer min =Integer.valueOf(minute);
                            Integer hourvar=Integer.valueOf(system.now().hour());
                            Integer sec=Integer.valueOf(second);
                            if(sec>59){
                                  sec=sec-60; 
                                  min=min+1;
                            }
                            second=string.valueOf(sec);
                            minute=string.valueOf(min);
                      
                            if(min>59)
                            {
                                Integer x=min-60;
                                minute=string.valueOf(x);
                                
                                Integer hour1=(hourvar+1);
                                hour=string.valueOf(hour1);
                                if(Integer.valueof(hour)>=24)
                                {
                                    minute=string.valueOf(0);
                                    hour=string.valueOf(0);
                                }
                            } 
                            
                            String strJobName = 'SalesIQDownloadJob-' + second + '_' + minute + '_' + hour + '_' + day + '_' + month + '_' + year;
                            String strSchedule = second+ ' ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                            if(!test.isRunningTest()){
                                //System.schedule(strJobName, strSchedule, new PrintMapHierarchyJobStatusScheduler(logs[0].id));
                            }
                        }
                    }
                    else
                    {
                        req.setEndpoint(loc);
                        res = h.send(req);
                    }
                }
                catch(System.CalloutException e)
                {
                    system.debug('exception===='+e);
                    logs[0].Job_Status__c='Failed';
                    update logs;    
                }
                
                if(status=='esriJobSucceeded')
                {
                    address=address1+'/results/downloadURL';
                    req.setMethod('POST');
                    req.setEndPoint(address);
                    res = h.send(req);
                    
                    if(res.getStatusCode() ==200)
                    {
                        
                        JSONParser parser = JSON.createParser(res.getBody());
                        System.JSONToken token,token2, token3;
    
                        parser.nextToken();    // Eat first START_OBJECT 
            
                        string  jobId;
                        while((token=parser.nextToken()) != null)
                        {
                            system.debug('---Text of Token---'+parser.getText());
                            if(((token=parser.getCurrentToken()) == JSONToken.FIELD_NAME )&&(parser.getText()=='value')){
                                parser.nextToken();
                                url=Parser.getText();
                                
                            }
                        }
                               
                     
                    }                    
                    logs[0].Output__c=url;
                    logs[0].Job_Status__c='Completed';
                    update logs;                
                }
            }
        }
        catch(System.LimitException e)
        {
            System.debug('TIME LIMIT EXCEEDED');
            logs[0].Job_Status__c='Failed';
            update logs;
        }
        catch(System.StringException e)
        {
            System.debug('TIME LIMIT EXCEEDED');
            logs[0].Job_Status__c='Failed';
            update logs;
        }
    
    }
    global void finish(Database.BatchableContext BC)
    {
        List<Activity_Log__c> s=[SELECT CreatedDate,Id,Interface_Name__c,Job_Status__c,Name,Output__c,Position__r.Client_Position_Code__c,Request_Type__c,Team_Instance__r.Name,Position__r.Position_Type__c FROM Activity_Log__c WHERE id=:logid];
    }
}