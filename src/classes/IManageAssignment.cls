global Interface IManageAssignment {
    ValidationResponse checkValidations(String recordDetailsJSON, String moduleName);
}