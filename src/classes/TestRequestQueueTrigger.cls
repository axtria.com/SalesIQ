@isTest
private class TestRequestQueueTrigger {

    private static testMethod void test() {
        
        Team__c baseTeam = new Team__c(Name='HTN',Type__c='Base',Effective_End_Date__c=Date.today().addMonths(50));
        insert baseTeam;
        
        Workspace__c wrk = new Workspace__c();
        wrk.Workspace_Start_Date__c = Date.Today();
        wrk.Workspace_End_Date__c  = Date.today().addMonths(50);
        wrk.Name = 'Workspace 1';
        wrk.Workspace_Description__c = 'New Workspace';
        insert wrk;
        
        Scenario__c scenario = new Scenario__c();
        scenario.CR_Tracking__c = false;
        scenario.Scenario_Name__c = 'HTN_Q3';
        scenario.Workspace__c = wrk.id;
        scenario.Team_Name__c = baseTeam.id;
        scenario.Request_Process_Stage__c = SalesIQGlobalConstants.READY;
        scenario.Scenario_Stage__c =    SalesIQGlobalConstants.COLLABORATION;     
        insert scenario;
        
        
        Team_Instance__c BaseTeamInstance = new Team_Instance__c();
        BaseTeamInstance.Name = 'HTN_Q1_2016';
        BaseTeamInstance.Alignment_Period__c = 'Current';
        BaseTeamInstance.Team__c = baseTeam.id;
        BaseTeamInstance.Alignment_Type__c = 'Hybrid';
        BaseTeamInstance.isActiveCycle__c = 'Y';         
        BaseTeamInstance.Team_Instance_Code__c = 'DI-00001'; 
        BaseTeamInstance.Team_Cycle_Name__c = 'Current';
        insert BaseTeamInstance;
        
        Position__c destTerr = new Position__c();
        destTerr.Name = 'Long Island West';
        destTerr.Client_Territory_Name__c = 'Long Island West';
        destTerr.Client_Position_Code__c = '1NE30012';
        destTerr.Client_Territory_Code__c='1NE30012';
        destTerr.Position_Type__c='Territory';
        destTerr.inactive__c = false;
        destTerr.RGB__c = '41,210,117';
        destTerr.Team_iD__c = baseTeam.id;
        destTerr.Hierarchy_Level__c = '1';
        destTerr.Related_Position_Type__c  ='Base';
        destTerr.Team_Instance__c  =BaseTeamInstance.id;
        destTerr.Effective_End_Date__c=Date.today().addMonths(50);
        insert destTerr;
        
        Change_Request__c objChangeRequest = new Change_Request__c();
        objChangeRequest.Destination_Position__c = destTerr.id;
        objChangeRequest.AllZips__c = '11788';
        objChangeRequest.Status__c = 'Pending';
        objChangeRequest.Team_Instance_ID__c = BaseTeamInstance.id;
        objChangeRequest.Change_Effective_Date__c = Date.today();
        
        insert objChangeRequest;
        
        Id devRecordTypeId = Schema.SObjectType.Activity_Log__c.getRecordTypeInfosByName().get('Scenario Execution').getRecordTypeId();
        Activity_Log__c logRequest=new Activity_Log__c(Selected_Positions__c=destTerr.id,Job_Status__c='Not Started',Request_Type__c='Position Level',Priority__c=1,Change_Request__c=objChangeRequest.id,Scenario__c=scenario.id,RecordTypeId = devRecordTypeId,Execution_Mode__c=SalesIQGlobalConstants.TEXT_RUN_FULL_PROMOTE);
        
        test.startTest();
          insert logRequest;
          logRequest.Job_Status__c='In Progress';
          update logRequest;
          logRequest.Job_Status__c='Success';
        test.stopTest();
      
      
    }

}