@IsTest 
public with sharing class TestGlobalSearchComponentCtrl{
	static testMethod void TestglobalFindforGeography(){
		
		Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11778');
    	insert geo;
    	
		list<Account> allAccount = new list<Account>();
        integer iCount;
    	for(iCount = 0 ; iCount <= 2 ; iCount++){
    		allAccount.Add(CallPlanTestDataFactory.CreateAccount('Zip Aligned', geo.Name,'Physician'));
    	}
    	insert allAccount;
    	
    	test.startTest();
	    	GlobalSearchComponentCtrl objglobal = new GlobalSearchComponentCtrl();
	    	
	    	List<id> pendingWorkIds = new List<id>{allAccount[0].id};
		   
		    Test.setFixedSearchResults(pendingWorkIds);
	    	
	    	list<string> result = GlobalSearchComponentCtrl.globalFind(geo.name);
	    	result = GlobalSearchComponentCtrl.globalFindwithSoql(geo.name);
	    	
	    	result = GlobalSearchComponentCtrl.globalFind(geo.name.SubString(0,1));
	    	result = GlobalSearchComponentCtrl.globalFindwithSoql(geo.name.SubString(0,1));
	    	system.assert(result != null);
	    test.stopTest();
	}
	
	static testMethod void TestglobalFindforlengthAccount(){
		Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11778');
    	insert geo;
    	
		list<Account> allAccount = new list<Account>();
        integer iCount;
    	for(iCount=0;iCount<=2;iCount++){
    		allAccount.Add(CallPlanTestDataFactory.CreateAccount('Zip Aligned', geo.Name,'Physician'));
    	}
    	insert allAccount; 
    	
    	test.startTest();
	    	GlobalSearchComponentCtrl objglobal = new GlobalSearchComponentCtrl();
	    	List<id> pendingWorkIds = new List<id>{geo.id};
		    
		    Test.setFixedSearchResults(pendingWorkIds);
		    
	    	list<string> result = GlobalSearchComponentCtrl.globalFind(allAccount[0].name);
	    	result = GlobalSearchComponentCtrl.globalFindwithSoql(allAccount[0].name);
    	
    		system.assert(result != null);
    	test.stopTest();
	}
}