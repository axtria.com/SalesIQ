public class SummaryWrapper{
    @AuraEnabled
    public string summaryLabel{get;set;}
    @AuraEnabled
    public integer summaryValue{get;set;}
    public SummaryWrapper(string summaryLabel, integer summaryValue){
        this.summaryLabel = summaryLabel;
        this.summaryValue = summaryValue;
    }
}