/************************************************************************************************
Name			: TestProcessExpiredPositionsBatch
Description 	: Test class for ProcessExpiredPositionsBatch. Code Coverage : 100%
Author			: Lagnika Sharma
Last Modified	: 04/08/2019
************************************************************************************************/
@IsTest
public class TestProcessExpiredPositionsBatch {

	static Team__c baseTeam;
	static Position__c territory,destTerr;
	static  Change_Request__c posChange;

	static testMethod void TestProcessExpiredPositions(){
		TotalApproval__c setting = new TotalApproval__c();
		setting.Name = 'ProcessDay';
		setting.No_Of_Approval__c = -1;
		insert setting;
		
		baseTeam = new Team__c(Name='HTN',Type__c='Base',Effective_End_Date__c=Date.today().addMonths(50));
    	insert baseTeam;
    	
    	Team_Instance__c BaseTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(baseTeam,'Account'); 
        BaseTeamInstance.Name = 'HTN_Q1_2018';
        BaseTeamInstance.Alignment_Period__c = 'Current';
        BaseTeamInstance.IC_EffstartDate__c = Date.today();
        BaseTeamInstance.IC_EffEndDate__c = Date.today().addYears(1);
        BaseTeamInstance.Team_Instance_Code__c = 'DI-00004';
        BaseTeamInstance.Team_Cycle_Name__c = 'Current';
        insert BaseTeamInstance;

		territory = new Position__c();
        territory.name = 'Long Island East';
        territory.Client_Territory_Name__c = 'Long Island East';
     	territory.Client_Position_Code__c = 'Terr0002';
      	territory.Client_Territory_Code__c='1NE30011';
        territory.Position_Type__c='Territory';
        territory.inactive__c = false;
        territory.RGB__c = '41,210,117';
        territory.Effective_End_Date__c=Date.today().addDays(-1);
        territory.Effective_Start_Date__c=Date.today().addMonths(50);
        territory.Related_Position_Type__c = 'Base';
        territory.Hierarchy_Level__c = '1';
        territory.Team_iD__c = baseTeam.id;
        territory.Team_Instance__c = BaseTeamInstance.id;
	    insert territory;
		
		Employee__c emp = new Employee__c();
		emp.FirstName__c = 'Test';
		emp.Last_Name__c = 'emp';
		emp.Employee_Type__c = 'Full';
		emp.Employee_ID__c = '123';
		insert emp;
		
		Position_Employee__c employeeAssign = new Position_Employee__c();
		employeeAssign.Position__c = territory.Id;
		employeeAssign.Employee__c = emp.Id;
		employeeAssign.Assignment_Type__c = 'Primary';
		employeeAssign.Effective_Start_Date__c = system.today().addMonths(-2);
		employeeAssign.Effective_End_Date__c = system.today().addMonths(2);
		insert employeeAssign;

		ProcessExpiredPositionsBatch b1 = new ProcessExpiredPositionsBatch();

		test.startTest();
		Database.executeBatch(b1, 200);
		test.stopTest();

		List<Position__c> posList = [Select inactive__c, Assignment_status__c, Employee__c from Position__c where id =: territory.id];
		for(Position__c pos : posList){
			System.assertEquals(pos.inactive__c,true, 'Success!!!');
			System.assertEquals(pos.Assignment_status__c,'Filled', 'Success!!!');
			System.assertEquals(pos.Employee__c,emp.Id, 'Success!!!');
		}
		//Test Scheduler ExpiredPositionBatchScheduler
		ExpiredPositionBatchScheduler sh1 = new ExpiredPositionBatchScheduler();
		String sch = '0 0 23 * * ?'; 
		system.schedule('Test Scheduler', sch, sh1);
	}
	
	static testMethod void TestInactivateUserPosForExpiredAssignment(){
		TotalApproval__c setting = new TotalApproval__c();
		setting.Name = 'ProcessDay';
		setting.No_Of_Approval__c = 0;
		insert setting;
		
		baseTeam = new Team__c(Name='HTN',Type__c='Base',Effective_End_Date__c=Date.today().addMonths(50));
    	insert baseTeam;
    	
    	Team_Instance__c BaseTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(baseTeam,'Account'); 
        BaseTeamInstance.Name = 'HTN_Q1_2018';
        BaseTeamInstance.Alignment_Period__c = 'Current';
        BaseTeamInstance.IC_EffstartDate__c = Date.today();
        BaseTeamInstance.IC_EffEndDate__c = Date.today().addYears(1);
        BaseTeamInstance.Team_Instance_Code__c = 'DI-00004';
        BaseTeamInstance.Team_Cycle_Name__c = 'Current';
        insert BaseTeamInstance;

		territory = new Position__c();
        territory.name = 'Long Island East';
        territory.Client_Territory_Name__c = 'Long Island East';
     	territory.Client_Position_Code__c = 'Terr0002';
      	territory.Client_Territory_Code__c='1NE30011';
        territory.Position_Type__c='Territory';
        territory.inactive__c = false;
        territory.RGB__c = '41,210,117';
        territory.Effective_End_Date__c=Date.today().addDays(-1);
        territory.Effective_Start_Date__c=Date.today().addMonths(50);
        territory.Related_Position_Type__c = 'Base';
        territory.Hierarchy_Level__c = '1';
        territory.Team_iD__c = baseTeam.id;
        territory.Team_Instance__c = BaseTeamInstance.id;
		insert territory;

		User u = new User(
		     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
		     LastName = 'last',
		     Email = 'puser000@amamama.com',
		     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
		     CompanyName = 'TEST',
		     Title = 'title',
		     Alias = 'alias',
		     TimeZoneSidKey = 'America/Los_Angeles',
		     EmailEncodingKey = 'UTF-8',
		     LanguageLocaleKey = 'en_US',
		     LocaleSidKey = 'en_US'
		);
		insert u;
		
		Employee__c emp = new Employee__c();
		emp.FirstName__c = 'Test';
		emp.Last_Name__c = 'emp';
		emp.Employee_Type__c = 'Full';
		emp.Employee_ID__c = '123';
		emp.USER__c = u.Id;
		insert emp;
		
		Position_Employee__c employeeAssign = new Position_Employee__c();
		employeeAssign.Position__c = territory.Id;
		employeeAssign.Employee__c = emp.Id;
		employeeAssign.Assignment_Type__c = 'Primary';
		employeeAssign.Effective_Start_Date__c = system.today().addMonths(-2);
		employeeAssign.Effective_End_Date__c = system.today().addDays(-2);
		insert employeeAssign;

		User_Access_Permission__c uap = new User_Access_Permission__c();
		uap.User__c = u.Id;
		uap.Map_Access_Position__c = territory.Id;
		uap.Position__c = territory.Id;
		uap.Team_Instance__c = BaseTeamInstance.Id;
		uap.Sharing_Type__c = 'Implicit';
		uap.Is_Active__c = true;
		insert uap;

		test.startTest();
		ProcessExpiredPositionsBatch b1 = new ProcessExpiredPositionsBatch();
		Database.executeBatch(b1, 200);
		test.stopTest();
		
		system.assertEquals([select Is_Active__c from User_Access_Permission__c where id =: uap.Id].Is_Active__c,false);

		//Test Scheduler ExpiredPositionBatchScheduler
		ExpiredPositionBatchScheduler sh1 = new ExpiredPositionBatchScheduler();

		String sch = '0 0 23 * * ?'; 
		system.schedule('Test Scheduler', sch, sh1);
	}

	static testMethod void TestProcessCRAssignmentBatch(){

		Team__c Team = CallPlanTestDataFactory.createTeam('Zip');
		insert Team;
		Team_Instance__c TeamInstance  = CallPlanTestDataFactory.CreateTeamInstance(Team,'Zip');
		TeamInstance.IC_EffstartDate__c = Date.newInstance(2018, 01, 01);
		TeamInstance.IC_EffEndDate__c = Date.newInstance(2018, 04, 01);
		insert TeamInstance;
		Position__c region = CallPlanTestDataFactory.createRegionPosition(Team);
	    region.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
	    insert region;
	    
	    Position__c district = CallPlanTestDataFactory.createPositionParent(region,Team);
	    district.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
	    insert district;
		    
	    Position__c position = CallPlanTestDataFactory.createPosition(district, Team);
	    position.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
	    insert position;
	    
	    Position__c unassignedTerritory = new Position__c();
    	unassignedTerritory.Name = 'Unassigned Territory';
    	unassignedTerritory.Client_Territory_Name__c = 'Unassigned (Terr00000)';
 		unassignedTerritory.Client_Position_Code__c = SalesIQGlobalConstants.UNASSIGNED_TERRITORY_CODE;
  		unassignedTerritory.Client_Territory_Code__c=SalesIQGlobalConstants.UNASSIGNED_TERRITORY_CODE;
        unassignedTerritory.Position_Type__c='Territory';
        unassignedTerritory.inactive__c = false;
        unassignedTerritory.RGB__c = '41,210,117';
        unassignedTerritory.Team_iD__c = Team.id;
        unassignedTerritory.Effective_End_Date__c=Date.today().addMonths(50);
        unassignedTerritory.Hierarchy_Level__c = '1';
        unassignedTerritory.Related_Position_Type__c = 'Base';
        unassignedTerritory.Team_Instance__c = TeamInstance.id;
        insert unassignedTerritory;

        Account acc = CallPlanTestDataFactory.CreateAccount('Direct Aligned','11788','Physician');    
		acc.type = 'Physician';
		insert acc;

		Position_Account__c posAcc = new Position_Account__c();
		posAcc.Position__c = position.Id;
		posAcc.Team_Instance__c = TeamInstance.ID;
		posAcc.Account__c = acc.ID;
		posAcc.Effective_End_Date__c = Date.newInstance(2019, 12, 31);
		posAcc.Effective_Start_Date__c = TeamInstance.IC_EffstartDate__c;
		insert posAcc;

		Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11778');
		insert geo;

		Position_Geography__c posGeo = new Position_Geography__c();
		posGeo.Position__c = position.Id;
		posGeo.Geography__c = geo.ID;
		posGeo.Team_Instance__c = TeamInstance.ID;
		posGeo.Effective_End_Date__c = Date.newInstance(2019, 12, 31);
		posGeo.Effective_Start_Date__c = TeamInstance.IC_EffstartDate__c;
	    insert posGeo;

	   	ProcessCRAssignmentBatch pa1 = new ProcessCRAssignmentBatch(new set<ID>{position.Id},'Position_Account__c');
		ProcessCRAssignmentBatch pa12 = new ProcessCRAssignmentBatch(new set<ID>{position.Id},'Position_Geography__c');
		Id batchinstanceid1 = Database.executeBatch(pa1,9000);
        Id batchinstanceid2 = Database.executeBatch(pa12,9000);

		
	}
}