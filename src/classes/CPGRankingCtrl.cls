public with sharing class CPGRankingCtrl {
   
    public CPGRankingCtrl() {
                                                     	          
    }

   

      

     /**
     * calling Types List custom metatdata types for the type lists dropdown 
     */
      @AuraEnabled
      public static  Types_List__mdt[] getTypesList(){ 
        return CPGUtility.getTypesList();
      }

     /**
     * Get Value of Derived Fields 
     */
      @AuraEnabled
      public static  List<BRFieldWrapper> GetSourceAndDerivedFields(string scenarioRuleInstanceDetailsId){ 
        return CPGUtility.GetSourceAndDerivedFields(scenarioRuleInstanceDetailsId);
      }

    @AuraEnabled
    public static  List<Data_Set_Rule_Map__c> getInputTabName(String scenarioRuleInstanceDetailsId){
      List<Data_Set_Rule_Map__c> lstTableNames = CPGUtility.getInputTableName(scenarioRuleInstanceDetailsId);
      system.debug('lstTableNames::'+lstTableNames);
      return lstTableNames;
    }
    @AuraEnabled
    public static CPGConstants.Response insertRule(string strJsonBusinessRules,string strJsonRankRuleDetail
      ,string strJsonRankPartitionDetails,string strJsonRankOrderDetails, string strJsonBusinessRuleFieldMapDetails,string scenarioInstanceId){
      CPGConstants.Response objResponse=new CPGConstants.Response();
              System.debug('strobjBusinessRule::'+strJsonBusinessRules);   
               System.debug('strobjRankRule::'+strJsonRankRuleDetail);   
               System.debug('strobjRankPartition::'+strJsonRankPartitionDetails); 
               System.debug('strobjRankOrder::'+strJsonRankOrderDetails); 
        Savepoint sp = Database.setSavepoint();                                                        
        try{
               String scenarioId= CPGUtility.GetScenarioId(scenarioInstanceId); 
               Business_Rules__c objBusinessRule = (Business_Rules__c) JSON.deserialize(strJsonBusinessRules, Business_Rules__c.class);  
               List<Rank_Rule__c> objRankRule = (List<Rank_Rule__c>) JSON.deserialize(strJsonRankRuleDetail, List<Rank_Rule__c>.class);                                                                              
               List<Rank_Partition__c> objRankPartition = (List<Rank_Partition__c>) JSON.deserialize(strJsonRankPartitionDetails, List<Rank_Partition__c>.class);                                                                              
               List<Business_Rule_FieldMap_Details__c> lstBusinessRuleFieldMapDetails=(List<Business_Rule_FieldMap_Details__c>) JSON.deserialize(strJsonBusinessRuleFieldMapDetails, List<Business_Rule_FieldMap_Details__c>.class);                                                                              
               List<Rank_Order__c> lstRankOrder = (List<Rank_Order__c>) JSON.deserialize(strJsonRankOrderDetails, List<Rank_Order__c>.class);
               //objBusinessRule.Execution_Sequence__c= CPGUtility.GetExecutionSequence();

               boolean isExists= CPGUtility.ValidateBusinessRuleNames(objBusinessRule.ScenarioRuleInstanceDetails__c,objBusinessRule.Name,false);               
               system.debug('Duplicate Rule Name'+isExists);
                if(isExists){
                    objResponse.status=CPGConstants.ERROR;
                    objResponse.msg='Business Rule Name already exists.';
                    return objResponse;
                }
            
             boolean isNameExists= CPGUtility.ValidateFieldNames(objBusinessRule.ScenarioRuleInstanceDetails__c,objRankRule[0].Field_Name__c );               
           system.debug('Duplicate Field Name'+isNameExists);
            if(isNameExists){
                objResponse.status=CPGConstants.ERROR;
                 objResponse.msg=' Field Name already exists.';
                return objResponse;
            }  
               objBusinessRule.Scenario__c = scenarioId;
               insert  objBusinessRule;
               for(Rank_Rule__c rankObj : objRankRule){
                  rankObj.Business_Rules__c=  objBusinessRule.Id;
                  insert rankObj;
                  List<Data_Set_Rule_Map__c> lstDataSetRuleMap=new List<Data_Set_Rule_Map__c>();
                  lstDataSetRuleMap=[select Id,dataset_id__c from Data_Set_Rule_Map__c 
                                 where scenario_rule_instance_id__c= :objBusinessRule.ScenarioRuleInstanceDetails__c 
                                    and ds_type__c ='I']; 
                  if(lstDataSetRuleMap!=null && lstDataSetRuleMap.size()>0){
                    string dataSetId=lstDataSetRuleMap[0].dataset_id__c;
                    Data_Set_Column_Detail__c dataSetColumnDetail=new Data_Set_Column_Detail__c();
                    dataSetColumnDetail.dataset_id__c=dataSetId;
                    dataSetColumnDetail.ds_col_name__c=rankObj.Field_Name__c;
                    dataSetColumnDetail.tb_col_nm__c=rankObj.Field_Name__c.replace(' ','_').toLowerCase();   
                    dataSetColumnDetail.datatype__c=rankObj.data_type__c;
                    dataSetColumnDetail.value_type__c=rankObj.value_type__c;
                    dataSetColumnDetail.Table_Data_Type__c = rankObj.data_type__c.toLowerCase();   
                    dataSetColumnDetail.variable_type__c=CPGConstants.DERIVED;
                    insert  dataSetColumnDetail; 
                  }  
              }
                for(Rank_Partition__c rankPartitionObj: objRankPartition){
                  rankPartitionObj.Rank_Rule__c = objRankRule[0].Id;
                  insert rankPartitionObj;
                }
                for(Business_Rule_FieldMap_Details__c busRuleFieldMapDetails: lstBusinessRuleFieldMapDetails){
                     busRuleFieldMapDetails.Business_Rule__c=  objBusinessRule.Id;
                     insert busRuleFieldMapDetails;
                }
                for(Rank_Order__c rankOrderObject : lstRankOrder){
                    rankOrderObject.Rank_Rule_id__c = objRankRule[0].Id;
                    insert rankOrderObject;
                }
                if(!CPGUtility.UpdateExecutionSequences(objBusinessRule.ScenarioRuleInstanceDetails__c)){
                    throw new DmlException(CPGConstants.CYCLICREFERENCEERROR); 
                }
                objResponse.status=CPGConstants.SUCCESS;
                objResponse.msg=CPGConstants.SAVEDMSG;
        }catch(Exception ex){
            System.debug('exception-->'+ex.getMessage());
            System.debug('getStackTraceString-->'+ex.getStackTraceString());
            Database.rollback(sp);
            //CPGUtility.InsertErrorLog(ex.getMessage(),ex.getStackTraceString());
            objResponse.status=CPGConstants.ERROR;
           objResponse.msg=ex.getMessage();
        }
        return objResponse;  
    }

    @AuraEnabled
    public static CPGConstants.Response UpdateRule(string strJsonBusinessRules,string strJsonRankRuleDetail
      ,string strJsonRankPartitionDetails,string strJsonRankOrderDetails,string strJsonBusinessRuleFieldMapDetails){
        system.debug('Inside update');
        CPGConstants.Response objResponse=new CPGConstants.Response();
        Savepoint sp = Database.setSavepoint(); 
        System.debug('strobjRankRule::'+strJsonRankRuleDetail);   
        System.debug('strobjRankPartition::'+strJsonRankPartitionDetails); 
        System.debug('strobjRankOrder::'+strJsonRankOrderDetails);  
        try{ 
               Business_Rules__c objBusinessRule = (Business_Rules__c) JSON.deserialize(strJsonBusinessRules, Business_Rules__c.class);  
               List<Rank_Rule__c> objRankRule = (List<Rank_Rule__c>) JSON.deserialize(strJsonRankRuleDetail, List<Rank_Rule__c>.class);                                                                              
               List<Rank_Partition__c> objRankPartition = (List<Rank_Partition__c>) JSON.deserialize(strJsonRankPartitionDetails, List<Rank_Partition__c>.class);                                                                              
               List<Business_Rule_FieldMap_Details__c> lstBusinessRuleFieldMapDetails=(List<Business_Rule_FieldMap_Details__c>) JSON.deserialize(strJsonBusinessRuleFieldMapDetails, List<Business_Rule_FieldMap_Details__c>.class);                                                                              
               List<Rank_Order__c> lstRankOrder = (List<Rank_Order__c>) JSON.deserialize(strJsonRankOrderDetails, List<Rank_Order__c>.class);
               
               String businessRuleTypeID = objBusinessRule.Id;
               List<Business_Rules__c> lstBusinessRules=new List<Business_Rules__c>();
               List<RankInfoWrapper> lstFieldName=new List<RankInfoWrapper>();
               lstBusinessRules = GetBusinessRuleName(businessRuleTypeID);
               lstFieldName = GetRankType(businessRuleTypeID);
               system.debug('businessRuleTypeID-->'+businessRuleTypeID);
               //system.debug(lstBusinessRules);   
               //system.debug(lstBusinessRules[0].Name);
               system.debug(lstFieldName[0].lstWrankRules[0].Field_Name__c);
                
                String existingruleName = lstBusinessRules[0].Name;
                String existingfieldName = lstFieldName[0].lstWrankRules[0].Field_Name__c;
                boolean isExists= CPGUtility.ValidateBusinessRuleNamesUpdate(objBusinessRule.ScenarioRuleInstanceDetails__c,objBusinessRule.Name,existingruleName,false);               
                system.debug('Duplicate Rule Name'+isExists);
                if(isExists){
                    objResponse.status=CPGConstants.ERROR;
                    objResponse.msg='Business Rule Name already exists.';
                    return objResponse;
                }  
            
               boolean isFieldExists= CPGUtility.ValidateFieldNamesUpdate(objBusinessRule.ScenarioRuleInstanceDetails__c,objRankRule[0].Field_Name__c ,existingfieldName );               
               system.debug('Duplicate Rule Name'+isFieldExists);
               if(isFieldExists){
                    objResponse.status=CPGConstants.ERROR;
                    objResponse.msg='Field Name already exists.';
                    return objResponse;
                } 

               delete[select id from Rank_Partition__c where Rank_Rule__c= :objRankPartition[0].Id] ; 
               delete[select id from Rank_Order__c where Rank_Rule_id__c =: objRankRule[0].Id] ; 
               delete[select id from Rank_Rule__c where Business_Rules__c= :objBusinessRule.Id] ;
               delete[select id from Business_Rule_FieldMap_Details__c where Business_Rule__c= :objBusinessRule.Id] ;  

               update  objBusinessRule;
               //objBusinessRule.Execution_Sequence__c= CPGUtility.GetExecutionSequence();
               for(Rank_Rule__c rankObj : objRankRule){
                  rankObj.Business_Rules__c=  objBusinessRule.Id;
                  insert rankObj;
                  List<Data_Set_Rule_Map__c> lstDataSetRuleMap=new List<Data_Set_Rule_Map__c>();
                  lstDataSetRuleMap=[select Id,dataset_id__c from Data_Set_Rule_Map__c 
                                 where scenario_rule_instance_id__c= :objBusinessRule.ScenarioRuleInstanceDetails__c]; 
                  if(lstDataSetRuleMap!=null && lstDataSetRuleMap.size()>0){
                    string dataSetId=lstDataSetRuleMap[0].dataset_id__c;
                    system.debug('dataSetId::'+dataSetId);
                    system.debug('rankObj.Field_Name__c::'+rankObj.Field_Name__c);
                    string columnName = existingfieldName;
                    system.debug('Field to be deleted'+columnName);
                    //delete[select Id from Data_Set_Column_Detail__c where dataset_id__c= :dataSetId and ds_col_name__c= :rankObj.Field_Name__c ];  
                  delete[select Id from Data_Set_Column_Detail__c where dataset_id__c= :dataSetId and ds_col_name__c= :columnName];  

                   /* Data_Set_Column_Detail__c dataSetColumnDetail=[select Id from Data_Set_Column_Detail__c 
                                                                       where dataset_id__c= :dataSetId limit 1];*/
                    Data_Set_Column_Detail__c dataSetColumnDetail=new  Data_Set_Column_Detail__c(); 
                    dataSetColumnDetail.dataset_id__c=dataSetId;
                    dataSetColumnDetail.ds_col_name__c=rankObj.Field_Name__c;
                    dataSetColumnDetail.tb_col_nm__c=rankObj.Field_Name__c.replace(' ','_').toLowerCase();   
                    dataSetColumnDetail.datatype__c=rankObj.data_type__c;
                    dataSetColumnDetail.value_type__c=rankObj.value_type__c;
                    dataSetColumnDetail.Table_Data_Type__c = rankObj.data_type__c.toLowerCase();    
                    dataSetColumnDetail.variable_type__c=CPGConstants.DERIVED;
                    insert dataSetColumnDetail; 
                  }  
              }
                for(Rank_Partition__c rankPartitionObj: objRankPartition){
                  rankPartitionObj.Rank_Rule__c = objRankRule[0].Id;
                  insert rankPartitionObj;
                }
                for(Business_Rule_FieldMap_Details__c businessRuleFieldMapDetails: lstBusinessRuleFieldMapDetails){
                     businessRuleFieldMapDetails.Business_Rule__c=objBusinessRule.Id;
                     insert businessRuleFieldMapDetails;
                }
                for(Rank_Order__c rankOrderObject : lstRankOrder){
                    rankOrderObject.Rank_Rule_id__c = objRankRule[0].Id;
                    insert rankOrderObject;
                }
                if(!CPGUtility.UpdateExecutionSequences(objBusinessRule.ScenarioRuleInstanceDetails__c)){
                    throw new DmlException(CPGConstants.CYCLICREFERENCEERROR); 
                }
                objResponse.status=CPGConstants.SUCCESS;
                objResponse.msg=CPGConstants.SAVEDMSG;
        }catch(Exception ex){
            System.debug('exception-->'+ex.getMessage());
            Database.rollback(sp);
            //CPGUtility.InsertErrorLog(ex.getMessage(),ex.getStackTraceString());
            objResponse.status=CPGConstants.ERROR;
            objResponse.msg=ex.getMessage();
        }
        return objResponse;
    }

    @AuraEnabled
    public static List<Business_Rule_Type_Master__c> getBusinessRuleTypeMaster(){
        
        List<Business_Rule_Type_Master__c> lstBusinessRuleTypeMaster=[select Id,Name from  Business_Rule_Type_Master__c where Component_Type__r.Name='Derived Field'];
        return lstBusinessRuleTypeMaster;
    }


    @AuraEnabled
    public static List<RankInfoWrapper> GetRankType(string businessRuleId){
        List<Business_Rules__c> lstBusinessRules=new List<Business_Rules__c>();
        lstBusinessRules=[select Id,Name,Business_Rule_Type_Master__c from Business_Rules__c where id= :businessRuleId];
        List<Rank_Rule__c> lstRankRules = [select Id,Name,Business_Rules__c,Field_Name__c,data_type__c,rank_methodology__c,value_type__c from Rank_Rule__c where Business_Rules__c IN : 
                                            lstBusinessRules];
        List<Rank_Partition__c> lstRankPartition = [select Id,Name,Partition_Field__c from Rank_Partition__c where Rank_Rule__c IN : 
                                                    lstRankRules];
        List<Business_Rule_FieldMap_Details__c> lstBusinessRuleFieldMap = [select Id,IO_flag__c,Name,Param_type__c from Business_Rule_FieldMap_Details__c where Business_Rule__c IN : 
                                                                            lstBusinessRules];
        List<Rank_Order__c> lstRankOrder = [Select Id,Order_Clause__c,Order_Field__c,Rank_Rule_id__c,Display_Seq_No__c,Excecution_Order__c 
                                            from Rank_Order__c where Rank_Rule_id__c IN: lstRankRules order by Display_Seq_No__c];
        List<RankInfoWrapper> lstRankInfoWrapper = new List<RankInfoWrapper>();
        lstRankInfoWrapper.add(new RankInfoWrapper(lstBusinessRules,lstRankRules,lstRankPartition,lstBusinessRuleFieldMap,lstRankOrder));
        return lstRankInfoWrapper;
    }

    @AuraEnabled
    public static List<Business_Rules__c> GetBusinessRuleName(string businessRuleTypeId){
        List<Business_Rules__c> lstBusinessRules=new List<Business_Rules__c>();
        lstBusinessRules=[select Id,Name,
                          (select Id,IO_Flag__c,Name,Param_Type__c from Business_Rule_FieldMap_Details__r),
                          (select Id,Display_expression__c,Derived_field_name__c,Data_Type__c,Value_Type__c from Computation_Rule_Details__r) 
                          from Business_Rules__c where id= :businessRuleTypeId];
        
        //string strBusinessRules= JSON.serialize(lstBusinessRules);
        System.debug('value=='+lstBusinessRules);
        return lstBusinessRules;
    }


    public class RankInfoWrapper{
        @AuraEnabled
        public List<Business_Rules__c> lstWbusinessRules{get;set;}
        @AuraEnabled
        public List<Rank_Rule__c> lstWrankRules{get;set;}
        @AuraEnabled
        public List<Rank_Partition__c> lstWrankPartitions{get;set;}
        @AuraEnabled
        public List<Business_Rule_FieldMap_Details__c> lstWbusinessRuleTypeMaster{get;set;}
        @AuraEnabled
        public List<Rank_Order__c> lstWrankOrder{get;set;}
        @AuraEnabled
        public String selectedRank {get; set;} 
        
        public RankInfoWrapper(List<Business_Rules__c> lstWbusinessRules, List<Rank_Rule__c> lstWrankRules,List<Rank_Partition__c> lstWrankPartitions, List<Business_Rule_FieldMap_Details__c> lstWbusinessRuleTypeMaster,List<Rank_Order__c> lstWrankOrder){
            this.lstWbusinessRules = lstWbusinessRules;
            this.lstWrankRules = lstWrankRules;
            this.lstWrankPartitions = lstWrankPartitions;
            this.lstWbusinessRuleTypeMaster = lstWbusinessRuleTypeMaster;
            this.lstWrankOrder = lstWrankOrder;
        }
    }
}