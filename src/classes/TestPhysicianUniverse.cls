@IsTest 
public with sharing class TestPhysicianUniverse{
     // DEPRECATED BASE CLASS
    /*
     static testMethod void TestRunQuery(){
        
        User objUser = CallPlanTestDataFactory.createUser();
        insert objUser;

        test.startTest();
        TestDataFactory.assignPermissionSet(objUser.Id, 'SalesIQ_DM');
        test.stopTest();

        System.runAs(objUser){
            
            TriggerContol__c offSecurityUtil = new TriggerContol__c(Name='SecurityUtil', IsStopTrigger__c = true);
            insert offSecurityUtil;

            Team__c Team = CallPlanTestDataFactory.createTeam('Zip');
            insert Team;
            
            Team_Instance__c TeamInstace  =CallPlanTestDataFactory.CreateTeamInstance(Team,'Zip');
            insert TeamInstace;
            
            //Create New
            Position__c sourcePosition = CallPlanTestDataFactory.createPosition(null, Team);
            sourcePosition.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
            insert sourcePosition;
            
            User_Access_Permission__c objUserAccessPermTest =  CallPlanTestDataFactory.createUserAccessPerm(sourcePosition, objUser,TeamInstace);
              
            insert objUserAccessPermTest;   
            
            Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11778');
            insert geo;
        
            list<Account> allAccount = new list<Account>();
            integer iCount;
            for(iCount=0;iCount<=2;iCount++){
                allAccount.Add(CallPlanTestDataFactory.CreateAccount('ZIP Aligned', geo.Name,'Physician'));
            }
            insert allAccount;
            
            Position_Team_Instance__c posTeamInstance =CallPlanTestDataFactory.CreatePosTeamInstance(sourcePosition,null,TeamInstace);
            insert posTeamInstance;
            
            Team_Instance_Account__c lstTeamInstanceAccount = CallPlanTestDataFactory.createTeamInstanceAccount(allAccount[0],TeamInstace);         
            insert lstTeamInstanceAccount;
            
            Position_Account_Call_Plan__c posAcc = CallPlanTestDataFactory.CreatePosAccCallPlan(allAccount[0],sourcePosition,TeamInstace,posTeamInstance,lstTeamInstanceAccount,'Revise');
            insert posAcc;
            
            apexpages.currentpage().getparameters().put('id' , allAccount[0].AccountNumber);
            apexpages.currentpage().getparameters().put('firstname' , allAccount[0].FirstName__c);
            apexpages.currentpage().getparameters().put('city' , allAccount[0].BillingCity);
            apexpages.currentpage().getparameters().put('state' , allAccount[0].BillingState);
            apexpages.currentpage().getparameters().put('specialty' , allAccount[0].Speciality__c);
            
            
            //test.startTest();
            PhysicianUniverse objPhy = new PhysicianUniverse();
            objPhy.toggleSort();
            objPhy.displayList[0].selected=true;
            objPhy.getphyAddDrop();
            
            objPhy.IncludeInCallplan();
            objPhy.clearSearch();
            //objPhy.redirectToReasonPage();
             objPhy.saveReason();
            objPhy.cancelReason();
            objPhy.redirectToReasonPage();
            system.assert(objPhy.displayList!=null);
            
            //test.stopTest();
         }
     }
     */
}