global class BRMasterDataSyncScheduler implements Schedulable,Database.AllowsCallouts{
    
    global void execute(SchedulableContext SC) {
        BRScenarioSchedulerHandler.syncMasterData();
    }
}