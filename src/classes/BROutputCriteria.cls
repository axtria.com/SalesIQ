public class BROutputCriteria {
    @AuraEnabled
    public String categName{get;set;}        
    @AuraEnabled
    public String dataType {get; set;}        
    @AuraEnabled
    public Boolean isDropdown {get; set;}        
    @AuraEnabled
    public String valueType {get; set;}        
    @AuraEnabled
    public String[] values {get; set;}        
    @AuraEnabled
    public String selValue {get; set;}  
    @AuraEnabled
    public Boolean isNew {get; set;}
    @AuraEnabled
	public Boolean isDisabled {get; set;}  
	@AuraEnabled
	public String childDependent {get; set;}
}