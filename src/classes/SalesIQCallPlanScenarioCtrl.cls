public class SalesIQCallPlanScenarioCtrl{

	@AuraEnabled
	public static CallPlanScenarioSettingWrapper initCallPlan(String scenarioId){

		String callPlanType =  SalesIQGlobalConstants.SCENARIO_TYPE_CALL_PLAN;
		Scenario__c scenarioRecord = [select Name, Team_Instance__c,Team_Instance__r.Name,Team_Instance__r.Team__c, Team_Instance__r.Team__r.Name,
										Scenario_Name__c,Scenario_Stage__c, CR_Tracking__c, Table_View_Config__c, Table_Column_Config__c, 
										Team_Instance_Config__c,Summary_Cards_Config__c, KPI_Card_Guardrail_Config__c,Approval_Config__c, Scenario_Source__c 
										from Scenario__c where Id =: scenarioId];

		CallPlanScenarioSettingWrapper callPlanWrapper = new CallPlanScenarioSettingWrapper();
		callPlanWrapper.scenarioRecord = scenarioRecord;
		callPlanWrapper.teamInstanceCheckbox = scenarioRecord.Team_Instance_Config__c;
		callPlanWrapper.tableViewCheckbox = scenarioRecord.Table_View_Config__c;
		callPlanWrapper.tableColumnCheckbox = scenarioRecord.Table_Column_Config__c;
		callPlanWrapper.approvalConfigCheckbox = scenarioRecord.Approval_Config__c;
		callPlanWrapper.kpiCheckbox = scenarioRecord.KPI_Card_Guardrail_Config__c;
		callPlanWrapper.summaryCardsCheckbox = scenarioRecord.Summary_Cards_Config__c;
		callPlanWrapper.crTrackingCheckbox = scenarioRecord.CR_Tracking__c;

		callPlanWrapper.brmsMetadataList = getBRMSColumnName(scenarioId);
		callPlanWrapper.pacpMetaDataList = getPACPColumnName();
		callPlanWrapper.alreadyMappedField = getAlreadyMappedField(scenarioId);

		return callPlanWrapper;
	}

	@AuraEnabled 
	public static String confirmScenarioRequest(Scenario__c newScenarioRecord, String scenarioStage,String mappedFieldsString)
	{
		if(scenarioStage == SalesIQGlobalConstants.DESIGN || scenarioStage == SalesIQGlobalConstants.COLLABORATION)
		{
			String updateInsertAction = updateInsertRecords(String.valueOf(newScenarioRecord.Id), mappedFieldsString,true);
		}	
		String callPlanType =  SalesIQGlobalConstants.SCENARIO_TYPE_CALL_PLAN;
		Scenario__c scenarioRecord = [select Name,Team_Instance__c, Scenario_Stage__c, CR_Tracking__c, Table_View_Config__c, Table_Column_Config__c, 
										Team_Instance_Config__c,Summary_Cards_Config__c, KPI_Card_Guardrail_Config__c,Approval_Config__c 
										from Scenario__c where id=: newScenarioRecord.Id] /*Team_Instance__c =: teamInstanceRecord.Id AND Scenario_Type__c =: callPlanType]*/;

		
		scenarioRecord.Scenario_Stage__c = scenarioStage;
		scenarioRecord.Team_Instance_Config__c = newScenarioRecord.Team_Instance_Config__c;
		scenarioRecord.Table_Column_Config__c = newScenarioRecord.Table_Column_Config__c;
		scenarioRecord.Summary_Cards_Config__c = newScenarioRecord.Summary_Cards_Config__c;
		scenarioRecord.Table_View_Config__c = newScenarioRecord.Table_View_Config__c;
		scenarioRecord.KPI_Card_Guardrail_Config__c = newScenarioRecord.KPI_Card_Guardrail_Config__c;
		scenarioRecord.Approval_Config__c = newScenarioRecord.Approval_Config__c;
		scenarioRecord.CR_Tracking__c = newScenarioRecord.CR_Tracking__c;

		system.debug('Scenario kpi bool ='+newScenarioRecord.KPI_Card_Guardrail_Config__C);
		if(newScenarioRecord.KPI_Card_Guardrail_Config__C){
			system.debug('Scenario kpi bool ='+newScenarioRecord.KPI_Card_Guardrail_Config__C);
			CIMPositionMatrixSummaryUpdateCtlr cim=new CIMPositionMatrixSummaryUpdateCtlr(scenarioRecord.Team_Instance__c,newScenarioRecord.Id);
		}
		System.debug('scenarioRecord - '+scenarioRecord);
		
		update scenarioRecord;
		return '';
		
	}

	@AuraEnabled
	public static String getListViewID(String objectName, String viewName){
		
    	ListView listviews = [SELECT Id, Name FROM ListView WHERE SobjectType =: objectName  AND Name=: viewName limit 1];
    	
    	return listviews.id;
	}

	 @AuraEnabled
    public static String getOrgNamespace() 
    {
        return SalesIQGlobalConstants.NAME_SPACE;        
    }

    public static list<String> getPACPColumnName(){

    	set<String> mandatoryFields = new set<String>{'Account__c','Position__c','isTarget__c','isTarget_Updated__c','isTarget_Approved__c','Call_Frequency_Approved__c','Call_Frequency_Updated__c','Call_Frequency__c','Name','OwnerId'};

    	set<String> depricatedFields = new set<String>{'Account_Alignment_type__c','Alignment_Change_Action__c','Call_Sequence_Approved__c','Call_Sequence_Updated__c','Call_Sequence_Original__c','Calls_Original__c','Calls_Approved__c','Calls_Updated__c','isAddedFromAlignment__c','isLocked__c','isModified__c','lastApprovedTarget__c','isAccountTarget__c','OverAll_Target__c','Rank__c','ReasonAdd__c','ReasonDrop__c','Recommended_Calls__c','Recommended_Calls_Approved__c','Recommended_Calls_Updated__c','Sample_Flag__c','isincludedCallPlan__c'};

    	Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get(SalesIQGlobalConstants.NAME_SPACE+'Position_Account_Call_Plan__c').getDescribe().fields.getMap();
		List<String> editableFields = new List<String>();
		editableFields.add('--None--');
		for(Schema.SObjectField fieldRef : fields.values()) {
		    Schema.DescribeFieldResult fieldResult = fieldRef.getDescribe();
		    if(!mandatoryFields.contains(fieldResult.getLocalName()) && !depricatedFields.contains(fieldResult.getLocalName())){
			    if(fieldResult.isUpdateable()) {
			        editableFields.add(fieldResult.getLocalName());
			    }
			}
    	}

    	return editableFields;
	}


    public static List<String> getBRMSColumnName(String scenarioId){

	    list<Scenario_Rule_Instance_Details__c> rules = [Select id from Scenario_Rule_Instance_Details__c where Scenario_Id__c =: scenarioId AND Component_Type_Master__r.Name =: SalesIQGlobalConstants.DATA_SOURCE];
	    list<String> rulesList = new list<String>();
	    for(Scenario_Rule_Instance_Details__c rule :rules){
	        rulesList.add(rule.Id);
	    }
	    
	    list<Data_Set_Rule_Map__c> dataSet = [select dataset_id__c from Data_Set_Rule_Map__c where scenario_rule_instance_id__c in: rulesList AND ds_type__c = 'O'];
	    list<String> data = new list<String>();
	    for(Data_Set_Rule_Map__c ruleSet : dataSet){
	        data.add(ruleSet.dataset_id__c);
	    }
	    
	    list<Data_Set_Column_Detail__c> colDetail = [select ds_col_name__c from Data_Set_Column_Detail__c where dataset_id__c in: data];
	    list<String> colName = new list<String>();
	    colName.add('--None--');
	    for(Data_Set_Column_Detail__c col : colDetail){
	        colName.add(col.ds_col_name__c);
	    }
    
    return colName;
    }

    @AuraEnabled 
    public static String checkSaveValidations(String mappedFieldsString,String scenarioStage,String scenarioId)
    {
    	list<MappingFieldsWrapper> fieldWrapper = (list<MappingFieldsWrapper>)JSON.deserialize(mappedFieldsString, list<MappingFieldsWrapper>.class);
    	
    	Set<String> requiredSet = new Set<String>(); 
		for(MappingFieldsWrapper obj : fieldWrapper)
    	{
    		if(obj.pacpField != '--None--')
    		{
    			if(!requiredSet.contains(obj.pacpField))
    				requiredSet.add(obj.pacpField);
    			else
    				return 'Duplicate';
    		}
    		
    	}
    	if(scenarioStage == SalesIQGlobalConstants.COLLABORATION)
    	{
    		for(MappingFieldsWrapper wrapper : fieldWrapper)
    		{
    			if(wrapper.isRequired && wrapper.systemField == '--None--')
    				return 'Required Fields Missing';
    		}
    	}
    	else
    	{
    		return updateInsertRecords(scenarioId,mappedFieldsString,false);
    	}

    	

    	return 'Success';
    	
    }


    public static Map<String,String> getAlreadyMappedField(String scenarioId)
    {
    	// list<MappingFieldsWrapper> dataMap = new list<MappingFieldsWrapper>();
    	Map<String,String> dataMap = new Map<String,String>();
    	Set<String> requiredFields = new Set<String>{'Account__c','Position__c','isTarget__c','isTarget_Updated__c','isTarget_Approved__c','Call_Frequency_Approved__c','Call_Frequency_Updated__c','Call_Frequency__c'};

    	List<Scenario_Data_Object_Map__c> scenarioDataMapList = SalesIQUtility.getScenarioDataObjectFromScenarioID(scenarioId);
    	if(scenarioDataMapList.size() > 0)
    	{
	    	for(Scenario_Data_Object_Map__c data : scenarioDataMapList)
	    	{
	    		if(!dataMap.containsKey(data.SalesIQ_Field__c))
	    		{
	    			dataMap.put(data.SalesIQ_Field__c, data.Call_Plan_Field__c);
	    		}
	    		

	    	}
		}

		// dataMap.put('Account__c', 'Account_Temp__c');
		// dataMap.put('Position__c', 'Position_Temp__c');
		return dataMap;
    }


    // @AuraEnabled
    private static String updateInsertRecords(String scenarioId,String mappedFieldsString, Boolean save){
        
        list<MappingFieldsWrapper> fieldWrapper = new list<MappingFieldsWrapper>();
        if(mappedFieldsString != '')
            fieldWrapper = (list<MappingFieldsWrapper>)JSON.deserialize(mappedFieldsString, list<MappingFieldsWrapper>.class);
    	
    	// Set<String> requiredSet = new Set<String>(); 
    	// for(MappingFieldsWrapper obj : fieldWrapper)
    	// {
    	// 	if(!requiredSet.contains(obj.pacpField))
    	// 		requiredSet.add(obj.pacpField);
    	// 	else
    	// 		return 'Duplicate';
    	// }

    	List<Scenario_Data_Object_Map__c> scenarioDataMapList = SalesIQUtility.getScenarioDataObjectFromScenarioID(scenarioId);

    	Set<String> requiredFields = new Set<String>{'Account__c','Position__c','isTarget__c','isTarget_Updated__c','isTarget_Approved__c','Call_Frequency_Approved__c','Call_Frequency_Updated__c','Call_Frequency__c'};
    	
    	map<String,map<String,String>> alreadyMappedFieldsMap = new map<String,map<String,String>>();
    	// map<String,String> alreadyMappedFieldsMap = new map<String,String>();
    	for(Scenario_Data_Object_Map__c scData : scenarioDataMapList)
    	{
    		System.debug('scData :: '+scData);
    		if(!alreadyMappedFieldsMap.containsKey(scData.Id))
    		{
    			alreadyMappedFieldsMap.put(String.valueOf(scData.Id), new map<String,String>{scData.SalesIQ_Field__c => scData.Call_Plan_Field__c});
    			// alreadyMappedFieldsMap.put(scData.SalesIQ_Field__c,scData.Call_Plan_Field__c );
    		}
    	}

    	/*alreadyMappedFieldsMap.put('1',new map<String,String>{'Account__c' => 'Account_Temp__c'});
    	alreadyMappedFieldsMap.put('2',new map<String,String>{'Position__c' => 'Position_Temp__c'});*/

    	System.debug('fieldWrapper : ' + fieldWrapper);
    	//map<String, String> updatedMapping = new Map<String,String>();
    	list<Scenario_Data_Object_Map__c> updateFieldsMapping = new list<Scenario_Data_Object_Map__c>();
    	list<Scenario_Data_Object_Map__c> insertFieldsMapping = new list<Scenario_Data_Object_Map__c>();
    	Scenario_Data_Object_Map__c dataObj ;

    	System.debug('alreadyMappedField::::' + scenarioDataMapList.size()+'::::' + scenarioDataMapList);

    	RecordType recordId = [select id from RecordType where Name = 'Mapping'];
    	Integer f = 0;
    	for(MappingFieldsWrapper wrapper : fieldWrapper)
    	{
    		if(wrapper.pacpField == '--None--' || wrapper.systemField == '--None--')
    		{
    			continue;
    		}

    		System.debug('wrapper :: '+wrapper);
    		System.debug('alreadyMappedField::::'+alreadyMappedFieldsMap.size() + ':::::' + alreadyMappedFieldsMap);
    		if(alreadyMappedFieldsMap.size() >0)
    		{
    			f = 0;
	    		for(String keyId : alreadyMappedFieldsMap.keySet())
	    		{
	    			
	    			map<String,String> mapping = alreadyMappedFieldsMap.get(keyId);
	    			System.debug('wrapper :: '+wrapper);

	    			if(mapping.containsKey(wrapper.pacpField))
	    			{
	    				if(mapping.get(wrapper.pacpField) != wrapper.systemField)
	    				{
	    					System.debug('mapping Update:: '+mapping);
		    				dataObj = new Scenario_Data_Object_Map__c();
		    				dataObj.Id = Id.valueOf(keyId);
		    				dataObj.SalesIQ_Field__c = wrapper.pacpField;
		    				dataObj.Call_Plan_Field__c = wrapper.systemField;
		    				// dataObj.RecordType = recordId;
		    				updateFieldsMapping.add(dataObj);
	    				}
	    				f = 1;
	    				alreadyMappedFieldsMap.remove(keyId);
	    				break;
	    			}
	    		}
	    		if(f == 0)
	    		{
    				// System.debug('mapping Insert:: '+mapping);
    				dataObj = new Scenario_Data_Object_Map__c(Scenario__c = scenarioId);
    				dataObj.SalesIQ_Field__c = wrapper.pacpField;
    				dataObj.Call_Plan_Field__c = wrapper.systemField;
    				dataObj.RecordType = recordId;
    				insertFieldsMapping.add(dataObj);

	    		}
    		}
    		else
    		{
    			dataObj = new Scenario_Data_Object_Map__c(Scenario__c = scenarioId);
				dataObj.SalesIQ_Field__c = wrapper.pacpField;
				dataObj.Call_Plan_Field__c = wrapper.systemField;
				dataObj.RecordType = recordId;
				insertFieldsMapping.add(dataObj);
    		}
    	}

    	List<Scenario_Data_Object_Map__c> deleteObj = new list<Scenario_Data_Object_Map__c>();
    	if(alreadyMappedFieldsMap.size() > 0)
    	{
    		// alreadyMappedFieldsMap.keySet();
    		for(String str : alreadyMappedFieldsMap.keySet())
    		{
    			Scenario_Data_Object_Map__c obj = new Scenario_Data_Object_Map__c();
    			obj.id = str;
    			deleteObj.add(obj);
    		}
    		
    	}
    	System.debug('updateFieldsMapping ::: '+updateFieldsMapping);
    	System.debug('insertFieldsMapping ::: '+insertFieldsMapping);

    	if(!save)
    	{
    		if(!(updateFieldsMapping.size() > 0) && !(insertFieldsMapping.size() > 0) && !(deleteObj.size() >0))
	    	{
	    		return 'Nothing to update';
	    	}

    	}
    	else
    	{
    		if(updateFieldsMapping.size() > 0)
	    	{
	    		update updateFieldsMapping;
	    	}
	    	if(insertFieldsMapping.size() > 0)
	    	{
	    		insert insertFieldsMapping;
	    	}
	    	if(deleteObj.size() >0){
	    		delete deleteObj;
	    	}
    	}
    	
	    

    	return 'Success';
    	
    }

    public class MappingFieldsWrapper{
    	public String pacpField;
    	public String systemField;
    	public Boolean isRequired;
    }


	public class CallPlanScenarioSettingWrapper{

		/*@AuraEnabled 
		public Team_Instance__c teamInstanceRecord {get;set;}*/
		@AuraEnabled 
		public Scenario__c scenarioRecord {get;set;}
		@AuraEnabled
		public Boolean teamInstanceCheckbox {get;set;}
		@AuraEnabled
		public Boolean tableViewCheckbox {get;set;}
		@AuraEnabled
		public Boolean kpiCheckbox {get;set;}
		@AuraEnabled
		public Boolean tableColumnCheckbox {get;set;}
		@AuraEnabled
		public Boolean approvalConfigCheckbox {get;set;}
		@AuraEnabled
		public Boolean summaryCardsCheckbox {get;set;}
		@AuraEnabled
		public Boolean crTrackingCheckbox{get;set;}

		@AuraEnabled
		public list<String> pacpMetaDataList {get;set;}
		@AuraEnabled
		public list<String> brmsMetadataList {get;set;}
		@AuraEnabled
		public Map<String,String> alreadyMappedField {get;set;}

		
		public CallPlanScenarioSettingWrapper(){
			scenarioRecord = new Scenario__c();
		}
		/*public CallPlanScenarioSettingWrapper(Team_Instance__c teamInstanceRecord, Scenario__c scenarioRecord){
			this.teamInstanceRecord = teamInstanceRecord;
			this.scenarioRecord = scenarioRecord;
		}*/
	}
}
