@isTest
public with sharing class TestPrintMapHierarchyBatch {
       static testMethod void myUnitTest() 
      {
        Team__c team=new Team__c(Name='HTN');
        insert team;
        Position__c position=new Position__c(Name='HSMLAND',Team_iD__c=team.id,Hierarchy_Level__c='2');
        insert position;
        Team_Instance__c teaminstance=new Team_Instance__c(Name='HTN_Q3',Team__c=team.id);
        insert teaminstance;
        Activity_Log__c s=new Activity_Log__c(name='HSM LOG',Job_Status__c='Pending',Position__c=position.id,Request_Type__c='Current Hierarchy',Team_Instance__c=teaminstance.id);
        insert s;

        ETL_Config__c objETL = new ETL_Config__c();
        objETL.Name = SalesIQGlobalConstants.PYTHON_JOB;
        objETL.SF_UserName__c = 'test@salesforce.com';
        objETL.SF_Password__c = 'test@salesforce.com'; 
        objETL.S3_Security_Token__c = 'XYZjj';
        objETL.org_type__C = 'login';

        insert objETL;
        PrintMapMockGenerator mock = new PrintMapMockGenerator();
        mock.s='true';
        //mock.test1();
        //Test.setMock(HttpCalloutMock.class,mock);
         Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
      
        Database.executeBatch( new PrintMapHierarchyBatch(s.id));
        Test.stopTest();
      }
        static testMethod void myUnitTest2() 
      {
        Team__c team=new Team__c(Name='HTN');
        insert team;
        Position__c position=new Position__c(Name='HSMLAND',Team_iD__c=team.id,Hierarchy_Level__c='1');
        insert position;
        Team_Instance__c teaminstance=new Team_Instance__c(Name='HTN_Q3',Team__c=team.id);
        insert teaminstance;
        Activity_Log__c s=new Activity_Log__c(name='HSM LOG',Job_Status__c='Pending',Position__c=position.id,Request_Type__c='Current Hierarchy',Team_Instance__c=teaminstance.id);
        insert s;
        ETL_Config__c objETL = new ETL_Config__c();
        objETL.Name = SalesIQGlobalConstants.PYTHON_JOB;
        objETL.SF_UserName__c = 'test@salesforce.com';
        objETL.SF_Password__c = 'test@salesforce.com'; 
        objETL.S3_Security_Token__c = 'XYZjj';
        objETL.org_type__C = 'login';

        //objETL.End_Point__c = 'https://gisdev.axtria.com/api/validateContiguityANDDoughnut?';
        insert objETL;
         PrintMapMockGenerator mock = new PrintMapMockGenerator();
        mock.s='true';
         Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
      
        Database.executeBatch( new PrintMapHierarchyBatch(s.id));
        Test.stopTest();
      }
        static testMethod void myUnitTest3() 
      {
        Team__c team=new Team__c(Name='HTN');
        insert team;
        Position__c position=new Position__c(Name='HSMLAND',Team_iD__c=team.id,Hierarchy_Level__c='2');
        insert position;
        Team_Instance__c teaminstance=new Team_Instance__c(Name='HTN_Q3',Team__c=team.id);
        insert teaminstance;
        Activity_Log__c s=new Activity_Log__c(name='HSM LOG',Job_Status__c='Pending',Position__c=position.id,Request_Type__c='Current Hierarchy',Team_Instance__c=teaminstance.id);
        insert s;
        ETL_Config__c objETL = new ETL_Config__c();
        objETL.Name = SalesIQGlobalConstants.PYTHON_JOB;
        objETL.SF_UserName__c = 'test@salesforce.com';
        objETL.SF_Password__c = 'test@salesforce.com'; 
        objETL.S3_Security_Token__c = 'XYZjj';
        objETL.org_type__C = 'login';

        //objETL.End_Point__c = 'https://gisdev.axtria.com/api/validateContiguityANDDoughnut?';
        insert objETL;
         PrintMapMockGenerator mock = new PrintMapMockGenerator();
        mock.s='true1';
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
      
        Database.executeBatch( new PrintMapHierarchyBatch(s.id));
        Test.stopTest();
      }
        static testMethod void myUnitTest4() 
      {
        Team__c team=new Team__c(Name='HTN');
        insert team;
        Position__c position=new Position__c(Name='HSMLAND',Team_iD__c=team.id,Hierarchy_Level__c='2');
        insert position;
        Team_Instance__c teaminstance=new Team_Instance__c(Name='HTN_Q3',Team__c=team.id);
        insert teaminstance;
        Activity_Log__c s=new Activity_Log__c(name='HSM LOG',Job_Status__c='Pending',Position__c=position.id,Request_Type__c='Current Hierarchy',Team_Instance__c=teaminstance.id);
        insert s;
        ETL_Config__c objETL = new ETL_Config__c();
        objETL.Name = SalesIQGlobalConstants.PYTHON_JOB;
        objETL.SF_UserName__c = 'test@salesforce.com';
        objETL.SF_Password__c = 'test@salesforce.com';
        objETL.S3_Security_Token__c = 'XYZjj';
        objETL.org_type__C = 'login';

        //objETL.End_Point__c = 'https://gisdev.axtria.com/api/validateContiguityANDDoughnut?';
        insert objETL;


        String strJobName = 'Job-';
        String strSchedule = '20 30 8 10 2 ?';

         PrintMapMockGenerator mock = new PrintMapMockGenerator();
        mock.s='false';
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        System.schedule(strJobName, strSchedule, new PrintMapHierarchyBatchScheduler(s.id));
        Database.executeBatch( new PrintMapHierarchyBatch(s.id));
        Test.stopTest();
      }
       static testMethod void myUnitTest5() 
      {
        Test.startTest();
        Activity_Log__c s=new Activity_Log__c (Name='Activity Log1',Request_Type__c='Current Hierarchy',Module__c='Map Print');
        insert s;
        
        Test.stopTest();
        List<Activity_Log__c> log = [SELECT Id FROM Activity_Log__c  WHERE id = :s.id];
        system.assertEquals(1,log.size());//Check if one record is created in Invoivce sObject
      }
     
    
}