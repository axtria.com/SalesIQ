@isTest
private class TestSalesIQManageAccountExclusionCtrl {
    static testMethod void testMethod1() {
        SalesIQManageAccountExclusionCtrl.getOrgNamespace();

        Organization_Master__c org=new Organization_Master__c();
        org.Name='Asia';
        org.Org_Level__c='Global';
        org.Parent_Country_Level__c=true;
        insert org;
        
        Country__c ctr=new Country__c();
        ctr.name='USA';
        ctr.Country_Code__c='US';
        ctr.Status__c='Active';
        ctr.Parent_Organization__c=org.id;
        insert ctr;

        Account acc = new Account();
        acc.Name = '11788';
        acc.FirstName__c = 'Abby';
        acc.BillingCity='NJ';
        acc.BillingState='NY';
        acc.AccountNumber='11788';
        acc.type = 'Physician';
        acc.AccountType__c = 'Physician';
        acc.BillingPostalCode = '11788';
        acc.Speciality__c = '11788Oncology';
        acc.Alignment_Type__c = 'Direct Aligned';
        acc.BillingLatitude = 40.5571389900;
        acc.BillingLongitude = -74.2123329900;
        acc.External_Country_Id__c=ctr.id;
        //acc.Marketing_Code__c  = 'AI';

        Account acc2 = new Account();
        acc2.Name = '11789';
        acc2.FirstName__c = 'Abby';
        acc2.BillingCity='NJ';
        acc2.BillingState='NY';
        acc2.AccountNumber='11788';
        acc2.type = 'Physician';
        acc2.AccountType__c = 'Physician';
        acc2.BillingPostalCode = '11788';
        acc2.Speciality__c = '11788Oncology';
        acc2.Alignment_Type__c = 'Direct Aligned';
        acc2.BillingLatitude = 40.5571389900;
        acc2.BillingLongitude = -74.2123329900;
        acc2.External_Country_Id__c=ctr.id;
        //acc2.Marketing_Code__c  = 'AB';

        Account acc3 = new Account();
        acc3.Name = '11788';
        acc3.FirstName__c = 'Abby';
        acc3.BillingCity='NJ';
        acc3.BillingState='NY';
        acc3.AccountNumber='11780';
        acc3.type = 'Physician';
        acc3.AccountType__c = 'Physician';
        acc3.BillingPostalCode = '11788';
        acc3.Speciality__c = '11788Oncology';
        acc3.Alignment_Type__c = 'Direct Aligned';
        acc3.BillingLatitude = 40.5571389900;
        acc3.BillingLongitude = -74.2123329900;
        acc3.External_Country_Id__c=ctr.id;
        //acc3.Marketing_Code__c  = 'AR';
        insert new list<Account>{acc, acc2, acc3};

        SalesIQManageAccountExclusionCtrl.fetchCountry(ctr.Id);

        list<Account_Exclusion__c> lstAllExclued = new list<Account_Exclusion__c>();
        Account_Exclusion__c objExcludedAccount = new Account_Exclusion__c();
        objExcludedAccount.Account__c = acc.id;
        objExcludedAccount.Is_Exclusion_Batch_Run__c = false;
        objExcludedAccount.Status__c = 'Active';
        lstAllExclued.add(objExcludedAccount);

        Account_Exclusion__c objExcludedAccount2 = new Account_Exclusion__c();
        objExcludedAccount2.Account__c = acc2.id;
        objExcludedAccount2.Is_Exclusion_Batch_Run__c = false;
        objExcludedAccount2.Status__c = 'Active';
        lstAllExclued.add(objExcludedAccount2);

        insert lstAllExclued; 

        SalesIQManageAccountExclusionCtrl.fetchAccountExclusion(objExcludedAccount.Id);

        SalesIQManageAccountExclusionCtrl.saveAccountExclusion(objExcludedAccount, 'update');


        Account_Exclusion__c objExcludedAccount3 = new Account_Exclusion__c();
        objExcludedAccount3.Account__c = acc3.id;
        objExcludedAccount3.Is_Exclusion_Batch_Run__c = false;
        objExcludedAccount3.Status__c = 'Active';

        SalesIQManageAccountExclusionCtrl.saveAccountExclusion(objExcludedAccount3, 'insert');
        SalesIQManageAccountExclusionCtrl.logException('Test Class error', 'Alignment Module');

    }
}