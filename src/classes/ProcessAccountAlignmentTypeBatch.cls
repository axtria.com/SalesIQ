//Batch to update Account Relationship flag with implicit/explicit
global class ProcessAccountAlignmentTypeBatch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {

	global String theQuery;
	global String affiliationNetwork;
	global Set<Id> globalRootParents;
	global Id teamInstanceId;
	global Boolean isSuccessful;
	global String scenarioId;
	global Boolean syncEsri = false;
	
	global processAccountAlignmentTypeBatch(Id teamInstance) {
		// DEPRECATED
	}

	global processAccountAlignmentTypeBatch(Id teamInstance, Id scenario) {
		// DEPRECATED
	}

	global ProcessAccountAlignmentTypeBatch(Id teamInstance, Id scenario,Boolean syncEsriVar){
		try{
			isSuccessful = true;
			scenarioId = scenario;
			teamInstanceId = teamInstance;
			globalRootParents = new Set<Id>();
			syncEsri = syncEsriVar;
			Team_Instance__c teamInsObj = SalesIQUtility.getTeamInstanceById(teamInstance);
			/*if(teamInsObj.Affiliation_Network__c == null && teamInsObj.Enable_Affiliation_Movement__c && !teamInsObj.Restrict_Account_Affiliation__c){
				updateScenarioStatus(scenarioId, true);
				return;
			}*/

	        affiliationNetwork = teamInsObj.Affiliation_Network__c;
	        theQuery = 'Select Id, Account__c, Account__r.Name, Account__r.AccountNumber, Parent_Account__c, Parent_Account__r.Name, Parent_Account__r.AccountNumber, Root_Account__c, Root_Account__r.Name, Root_Account__r.AccountNumber, Affiliation_Network__c From Account_Affiliation__c Where Affiliation_Network__c =: affiliationNetwork';
		}
		catch(Exception ex){
			isSuccessful = false;
			SalesIQLogger.logCatchedException(ex, false, 'Module : ProcessAccountAlignmentTypeBatch.cls');
		}
	}

	global Database.QueryLocator start(Database.BatchableContext BC){
		try{
			return Database.getQueryLocator(theQuery);
		}
		catch(Exception ex){
			isSuccessful = false;
			SalesIQLogger.logCatchedException(ex, false, 'Module : ProcessAccountAlignmentTypeBatch.cls');
			Database.QueryLocator dbquery;
			return dbquery;
		}
	}

	global void execute(Database.BatchableContext BC, List<Account_Affiliation__c> scope){
		try{
			system.debug('#### ProcessAccountAlignmentTypeBatch Started');
			Set<String> rootParentAccounts = new Set<String>();
			if(scope.size() == 0)
	            return;
	        else{
	            for(Account_Affiliation__c accAffObj : scope)
	                rootParentAccounts.add(accAffObj.Root_Account__c);
	        }

	        List<String> rootParentAccountsList = new List<String>();

	        for(String rootAccount : rootParentAccounts){
	        	if(!globalRootParents.contains(rootAccount))
	        		rootParentAccountsList.add(rootAccount);
	        }

	        if(rootParentAccountsList.size() == 0)
	        	return;

	        List<Account_Affiliation__c> listAccAffiliation = SalesiqUtility.getAffiliatedAccounts(new List<String>{affiliationNetwork}, rootParentAccountsList, new List<String>());

			Map<String,String> mapChildParent = new Map<String,String>();
			Set<Id> allAccountIds = new Set<Id>();

			for(Account_Affiliation__c accAffObj : listAccAffiliation){
				mapChildParent.put(accAffObj.Account__c, accAffObj.Parent_Account__c);
				allAccountIds.addAll(new Set<Id>{accAffObj.Account__c, accAffObj.Parent_Account__c});
			}

			List<Position_Account__c> listPosAcc = new List<Position_Account__c>();
			if(SecurityUtil.checkRead(Position_Account__c.SObjectType, new List<String>{'Position__c','Account__c','Team_Instance__c','Assignment_Status__c','Account_Relationship_Type__c'}, false) && SecurityUtil.checkRead(Account.SObjectType, new List<String>{'AccountNumber'}, false)){
	            listPosAcc = [Select Id, Position__c, Account__c, Account__r.Name, Account_Relationship_Type__c, Team_Instance__r.Affiliation_Network__c From Position_Account__c Where Team_Instance__c =: teamInstanceId AND Account__c in:allAccountIds AND Assignment_Status__c !=: SalesIqGlobalConstants.POSITION_GEOGRAPHY_INACTIVE];
	        }

	        Map<String, Position_Account__c> mapAccountPosAcc = new Map<String, Position_Account__c>();
	        for(Position_Account__c posAccObj : listPosAcc)
	        	mapAccountPosAcc.put(posAccObj.Account__c, posAccObj);

	        Map<Id, ChangeRequestTriggerHandler.HierarchyNode> mapAccountIdANDnode = new Map<Id, ChangeRequestTriggerHandler.HierarchyNode>();
	       
	        for(String childAccountId : mapChildParent.keySet())
	            mapAccountIdANDnode.put(childAccountId,new ChangeRequestTriggerHandler.HierarchyNode(childAccountId));
	            
	        for(String childAccountId : mapChildParent.keySet()){
	            if(mapChildParent.get(childAccountId) != '' && mapChildParent.get(childAccountId) != null){
	            	if(mapAccountIdANDnode.get(mapChildParent.get(childAccountId)) != null){
		                ChangeRequestTriggerHandler.HierarchyNode referenceNode = mapAccountIdANDnode.get(mapChildParent.get(childAccountId));
		                referenceNode.children.add(mapAccountIdANDnode.get(childAccountId));
	            	}
	            }
	        }

	        for(String rootAccountId : rootParentAccounts){
	            ChangeRequestTriggerHandler.HierarchyNode referenceNode = mapAccountIdANDnode.get(rootAccountId);
	            ChangeRequestTriggerHandler.traverseChildren(referenceNode.children, mapAccountIdANDnode, mapAccountPosAcc , mapChildParent);
	        }

	        if(mapAccountPosAcc.values().size() > 0)
	        	update mapAccountPosAcc.values();
	    }
	    catch(Exception ex){
	    	isSuccessful = false;
			SalesIQLogger.logCatchedException(ex, false, 'Module : ProcessAccountAlignmentTypeBatch.cls');
	    }    
	}
	
	global void finish(Database.BatchableContext BC){
        /*try{
	        updateScenarioStatus(scenarioId, isSuccessful);
	        if(syncEsri)
	        {
	        	esriSyncingwithTalend(scenarioId);
	        }
	        system.debug('#### ProcessAccountAlignmentTypeBatch finished');
    	}
    	catch(Exception ex){
    		SalesIQLogger.logCatchedException(ex, false, 'Module : ProcessAccountAlignmentTypeBatch.cls');
    		system.debug('#### ProcessAccountAlignmentTypeBatch finished');
    	}*/
    }	


    /*private static void updateScenarioStatus(Id scenarioId, Boolean isSuccessful){
    	Scenario__c scenarioObj = [Select Id From Scenario__c Where Id =: scenarioId];
        if(!isSuccessful)
        	scenarioObj.Request_Process_Stage__c = SalesIQGlobalConstants.ERROR_OCCURED;
        else
        	scenarioObj.Request_Process_Stage__c = SalesIQGlobalConstants.READY;
    }*/

}