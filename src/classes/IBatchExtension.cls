/*Interface to extend functionality of batches */

global Interface IBatchExtension{
	
	/*This method is called from ProcessAffiliationRulesBatch execute method and scope is being passed here.
	  External class can implement this interface and use this method to override existing batch behaviour.	  
	*/
    void executeAffiliationBatch(list<Position_Account__c> lstPositionAcc , Id teamInstId , list<string> newlyAlignedAccounts);
}