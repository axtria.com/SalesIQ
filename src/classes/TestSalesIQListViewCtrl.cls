@isTest
private class TestSalesIQListViewCtrl {
    static testMethod void testMethod1() 
    {
    	Employee__c emp = CallPlanTestDataFactory.createEmployee();
        insert emp;
        
        test.startTest();
        SalesIQListViewCtrl.getOrgNamespace();
        try{
            List<SalesIQListViewCtrl.SalesIQListViewWrapper> wrap = SalesIQListViewCtrl.getListViews('Employee__c','',SalesIQGlobalConstants.EMPLOYEE_UNIVERSE,'','');    
        }catch(Exception e){

        }
        
        SalesIQListViewCtrl.logException('hello','world');
        test.stopTest();
    }

    static testMethod void testMethodPosition() {

        

        Organization_Master__c orgMaster = new Organization_Master__c();
        orgMaster.Name = 'North America';
        orgMaster.Parent_Country_Level__c = true;
        orgMaster.Org_Level__c  = 'Region';
        insert orgMaster;

        Country__c country = new Country__c();
        country.Name = 'USA';
        country.Parent_Organization__c = orgMaster.id;
        country.status__c = 'Active';
        country.Country_Code__c = 'US';
        insert country;

        Team__c team = TestDataFactory.createTeam('Base');
        team.Country__c = country.Id;
        insert team;
        
        Team__c team2 = TestDataFactory.createTeam('Overlay');
        team2.Controlling_Team__c = team.Id;
        insert team2;

        Team_Instance__c team_instance1 = TestDataFactory.CreateTeamInstance(team.Id,'Zip','',Date.today()-100,Date.today()+100);
        insert team_instance1;

        Team_Instance__c team_instance = TestDataFactory.CreateTeamInstance(team2.Id,'Zip','',Date.today()-100,Date.today()+100);
        insert team_instance;

        Position__C pos = CallPlanTestDataFactory.createPosition(null,team);
        insert pos;
        Position__C pos1 = CallPlanTestDataFactory.createPosition(pos,team);
        insert pos1;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User usr = TestDataFactory.createUser(p.Id);
        insert usr;
        System.runAs(usr)
        { 
        
	        TriggerContol__c tc = new TriggerContol__c();
	        tc.Name = 'SecurityUtil';
	        tc.IsStopTrigger__c = true ;
	        insert tc;

	        TriggerContol__c tc1 = new TriggerContol__c();
	        tc1.Name = 'ScenarioTrigger';
	        tc1.IsStopTrigger__c = true ;
	        insert tc1;

	        PermissionSet ps = [SELECT Id, Name From PermissionSet WHERE Name = 'SalesIQ_HO'];
	        insert new PermissionSetAssignment(AssigneeId = usr.Id, PermissionSetId = ps.Id );
	        
            User_Access_Permission__c objUserAccessPerm = TestDataFactory.createUserAccessPerm(pos1.Id,usr.Id,team_instance1.Id);
	        insert objUserAccessPerm;
	            
            test.startTest();
            SalesIQCalloutMockGenerator generator= new SalesIQCalloutMockGenerator();
            generator.s = 'true';
            generator.objectName = 'Position__c';
            Test.setMock(HttpCalloutMock.class, generator);

            map<String,String> response = SalesIQListViewCtrl.checkCountryAccess(String.valueOf(country.Id));
            
            try{
                List<SalesIQListViewCtrl.SalesIQListViewWrapper> wrap = SalesIQListViewCtrl.getListViews('Position__c',team_instance1.Id,SalesIQGlobalConstants.POSITION_UNIVERSE,'',country.Id);
                List<SalesIQListViewCtrl.SalesIQListViewWrapper> wrap2 = SalesIQListViewCtrl.getListViews('Position__c','','',SalesIQGlobalConstants.POSITION_UNIVERSE,country.Id);
            }catch(Exception e) 
            {
        		String message = e.getTypeName() + ': ' + e.getMessage() + '\n\n' + e.getStackTraceString() ;
        	}
            test.stopTest();
        }
    }


    static testMethod void testMethodPositionAccount() {

        

        Organization_Master__c orgMaster = new Organization_Master__c();
        orgMaster.Name = 'North America';
        orgMaster.Parent_Country_Level__c = true;
        orgMaster.Org_Level__c  = 'Region';
        insert orgMaster;

        Country__c country = new Country__c();
        country.Name = 'USA';
        country.Parent_Organization__c = orgMaster.id;
        country.status__c = 'Active';
        country.Country_Code__c = 'US';
        insert country;

        Team__c team = TestDataFactory.createTeam('Base');
        team.Country__c = country.Id;
        insert team;
        Team__c team2 = TestDataFactory.createTeam('Overlay');
        team2.Controlling_Team__c = team.Id;
        insert team2;

        Team_Instance__c team_instance1 = TestDataFactory.CreateTeamInstance(team.Id,'Zip','',Date.today()-100,Date.today()+100);
        insert team_instance1;

        Team_Instance__c team_instance = TestDataFactory.CreateTeamInstance(team2.Id,'Zip','',Date.today()-100,Date.today()+100);
        insert team_instance;

        Position__C pos = CallPlanTestDataFactory.createPosition(null,team);
        pos.Is_Unassigned_Position__c = false;
        pos.Hierarchy_Level__c = '3';
        pos.Team_Instance__c = team_instance1.Id;
        insert pos;
        Position__C pos1 = CallPlanTestDataFactory.createPosition(pos,team);
        insert pos1;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User usr = TestDataFactory.createUser(p.Id);
        insert usr;
        //System.runAs(usr)
       // { 
        
            TriggerContol__c tc = new TriggerContol__c();
            tc.Name = 'SecurityUtil';
            tc.IsStopTrigger__c = true ;
            insert tc;

            TriggerContol__c tc1 = new TriggerContol__c();
            tc1.Name = 'ScenarioTrigger';
            tc1.IsStopTrigger__c = true ;
            insert tc1;
            
            //Commenting below line becasue getting Mixed DML Error
            //PermissionSet ps = [SELECT Id, Name From PermissionSet WHERE Name = 'SalesIQ_HO'];
            //insert new PermissionSetAssignment(AssigneeId = usr.Id, PermissionSetId = ps.Id );
            
            User_Access_Permission__c objUserAccessPerm = TestDataFactory.createUserAccessPerm(pos.Id,usr.Id,team_instance1.Id);
            insert objUserAccessPerm;

            Workspace__c workspace = new Workspace__c();
            workspace.Name = 'W1';
            workspace.Workspace_Description__c = 'this is testing workspace';
            workspace.Workspace_Start_Date__c = System.today().addMonths(-1);
    		workspace.Workspace_End_Date__c = System.today().addMonths(+1);
            insert workspace;

            Scenario__c scenario = new Scenario__c();
            scenario.Scenario_Type__c = 'Call Plan Generation';
            scenario.Team_Instance__c = team_instance1.Id;
            scenario.Scenario_Name__c = 'Scenario 111';
            scenario.Scenario_Stage__c = 'Collaboration';
            scenario.Scenario_Status__c = 'Active';
            scenario.Workspace__c = workspace.Id;
            scenario.Scenario_Source__c = SalesIQGlobalConstants.SCENARIO_SOURCE_CREATE_FROM_FILE;
            scenario.KPI_Card_Guardrail_Config__C = true;
            insert scenario;

            team_instance1.Scenario__c = scenario.ID;
            update team_instance1;

                
            test.startTest();
            SalesIQCalloutMockGenerator generator = new SalesIQCalloutMockGenerator();
            generator.s = 'true';
            generator.objectName = 'Position_Account_Call_Plan__c';
            Test.setMock(HttpCalloutMock.class, generator);
             
            List<SalesIQListViewCtrl.SalesIQListViewWrapper> wrap = SalesIQListViewCtrl.getListViews('Position_Account_Call_Plan__c',team_instance1.Id,pos.Id,'Call Plan',country.Id);
            
            try{
                List<SalesIQListViewCtrl.SalesIQListViewWrapper> wrap2 = SalesIQListViewCtrl.getListViews('Position_Account_Call_Plan__c','','','Call Plan',country.Id);
            }catch(Exception e) 
            {
                String message = e.getTypeName() + ': ' + e.getMessage() + '\n\n' + e.getStackTraceString() ;
            }
            test.stopTest();
        //}
    }
}