global class CreateBulkMovementRequestChange implements Database.Batchable<sObject>, Database.Stateful{
    
    
    public string theQuery;
    public string crId;
    public Change_Request__c changeRequest;
    public string destinationPosition;
    public string selectedCycle;
    public map<string,List<string>> sourceGeoMap = new map<string,List<string>>();
    public List<String> sourceBulkIds ;
    Map<id,id> mapPositionTeamInstancePosition;
    public List<String> posgeList;
    public List<String> zipList;
    public list<string> crGeoIds = new list<string>();
    public string assignmentStatus;
    public list<SalesIQ_Logger__c> lstLogger = new list<SalesIQ_Logger__c>();
    global CreateBulkMovementRequestChange(List<string> changeRequestId){
        
        List<Change_Request__c> changeRequestList =[Select  id,AllZips__C,Status__c,Request_Type_Change__c,Alignment_Type__c,Destination_Position__c,Source_Positions__c,Change_Effective_Date__c,Team_Instance_ID__c,Sources_Bulk__c, Execution_Status__c,Point_Zips__c,Rule_Execution_Status__c,Position_Rules_Mismatch__c FROM Change_Request__c WHERE Id in :changeRequestId limit 1];
        if(!changeRequestList.isEmpty()){
            try{
                changeRequest = changeRequestList[0];
                sourceBulkIds = new list<string>();
                destinationPosition = changeRequestList[0].Destination_Position__c;
                selectedCycle  =  changeRequestList[0].Team_Instance_ID__c;
                //zipList = new List<String>();
                zipList = changeRequestList[0].AllZips__c.split(',');
                assignmentStatus = SalesIQUtility.getAssignmentStatusbyTeamInstance(changeRequestList[0].Team_Instance_ID__c);

                string allZips='';

                if(!string.isBlank(changeRequestList[0].AllZips__c)){                
                    //allZips = req.AllZips__c;
                    allZips = changeRequestList[0].AllZips__c;

                }
                if(!string.isBlank(changeRequestList[0].Point_Zips__c)){
                    allZips = allZips+','+ changeRequestList[0].Point_Zips__c;               
                }
                if(allZips.length() > 0){                
                    zipList = allZips.split(',');
                }  

                sourceBulkIds = changeRequestList[0].Sources_Bulk__c.split(',');

                system.debug('sourceBulkIds ' +sourceBulkIds);
                system.debug('assignmentStatus ' +assignmentStatus);
                system.debug('zipList ' +zipList);
               
                theQuery = 'Select id,Geography__c,Geography__r.Name,Position__c FROM Position_Geography__c WHERE geography__r.name IN: zipList and Assignment_status__c =: assignmentStatus and Team_Instance__c =: selectedCycle and position__c in:sourceBulkIds';

                posgeList = new List<String>();
                crId = changeRequestList[0].id;
                set<string> allPos = new set<string>();
                
                allPos.add(changeRequestList[0].Destination_Position__c);            
                
                allPos.addAll(sourceBulkIds);
                mapPositionTeamInstancePosition  = new Map<id,id>();
                
                list<String> POSITION_TEAM_INSTANCE_READ_FIELD = new list<String>{'Team_Instance_ID__c','Position_ID__c'};
                if ((SecurityUtil.checkRead(Position_Team_Instance__c.SObjectType, POSITION_TEAM_INSTANCE_READ_FIELD, false))){             
                    for(Position_Team_Instance__c mappos: [Select id, Position_Id__c from Position_Team_Instance__c where Position_ID__c in : allPos and Team_Instance_ID__c =: selectedCycle]){
                        mapPositionTeamInstancePosition.put(mappos.position_id__c, mappos.id);
                    }
                }
                
            }catch(Exception e){
                system.debug('Error: '+e.getStackTraceString());
                SalesIQUtility.updateChangeRequestStatus(new set<id>{changeRequest.id}, true);
                lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('Error: '+e.getMessage()+' in contructor of CreateBulkMovementRequestChange with  id ' ,'', 'Bulk Movement',changeRequest.id ));
                insert lstLogger;
                
            }
        }
    }

    global Database.Querylocator start(Database.BatchableContext bc){
        system.debug('in start method of 1st batch');
        return Database.getQueryLocator(theQuery);
    }

    global void execute (Database.BatchableContext BC,list<Position_Geography__c> listPosGeos){
        system.debug('in execute method of 1st batch');
        try{
            if(lstLogger.size() > 0){
                lstLogger.clear();
            }
            
            
            list<CR_Geography__c> crGeographiesToInsert = new list<CR_Geography__c>();
            for(Position_Geography__c posGeo:listPosGeos){
                    CR_Geography__c geoToMove = new CR_Geography__c(Name = 'CR-' + SalesIQUtility.generateRandomNumber(),Change_Request__c = CRId, Geography__c = posGeo.Geography__c, Destination_Position__c = destinationPosition, Source_Position__c = posGeo.Position__c,Source_Position_Team_Instance__c=mapPositionTeamInstancePosition.get(posGeo.Position__c),Destination_Position_Team_Instance__c=mapPositionTeamInstancePosition.get(destinationPosition));
                    crGeographiesToInsert.add(geoToMove);
                    posgeList.add(posGeo.Id);
            }
            insert crGeographiesToInsert;
            lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('Inserted CR GeoGRaphy' ,'', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,changeRequest.id ));
            
            for(CR_Geography__c crGeo:crGeographiesToInsert){
                crGeoIds.add(crGeo.id);
            }

        }
        catch(Exception e){
            system.debug('Exception :: ' + e.getMessage());
            lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('Error: '+e.getMessage()+' in Execute method of CreateBulkMovementRequestChange with  id ','', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,changeRequest.id ));
            SalesIQUtility.updateChangeRequestStatus(new set<id>{changeRequest.id}, true);
        }
        finally{
            insert lstLogger;
            //Clearing logger because in case of 2 batch ,it will clear the log for secod time to evade error "cannot specify Id in an insert call"
            if(lstLogger.size() > 0){
                lstLogger.clear();
            }
            
        }            

    }

    global void finish(Database.BatchableContext BC)
    {
        if(lstLogger.size() > 0){
            lstLogger.clear();
        }
        //lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('Finished Batch execution of CreateBulkMovementRequestChange','', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,changeRequest.id  ));
        lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('In Finishing method of CreateBulkMovementRequestChange','', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,changeRequest.id  ));
        
        try{
            changeRequest.Execution_Status__c = SalesIQGlobalConstants.BULK_CR_IN_PROGRESS;
            update changeRequest;

            //lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('Updated CR Execution_Status__c status to '+SalesIQGlobalConstants.BULK_CR_IN_PROGRESS  ,'', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,changeRequest.id ));
                      
            System.debug('####HSMcrGeoIds');
            System.debug(crGeoIds);
            
                
            Database.executeBatch(new CreateBulkMovementCRAccount(mapPositionTeamInstancePosition,changeRequest,crGeoIds,posgeList,zipList), 1500);
           
        }catch(Exception e){
            SalesIQUtility.updateChangeRequestStatus(new set<id>{changeRequest.id}, true);
            system.debug('Exception :: ' + e.getMessage());
            lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData('Error: '+e.getMessage()+' in Finish method of CreateBulkMovementRequestChange' ,'', SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP,changeRequest.id  ));
            
        }
        finally{
            insert lstLogger;
            
        } 
    }

    
}