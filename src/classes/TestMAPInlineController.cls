@IsTest 
public with sharing class TestMAPInlineController {
    static testMethod void testZIPAlignment(){
        test.startTest();
        Profile hoProfile = [SELECT Id FROM Profile WHERE Name =: 'System Administrator' limit 1];
        
        User usr = new User(LastName = 'ho',
                            FirstName='test',
                            Alias = 'test',
                            Email = 'test.ho@test.com',
                            Username = 'test.ho@test.com',
                            ProfileId = hoProfile.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US');
        insert usr;
        
        list<Alignment_Global_Settings__c> allAlignmentGlobalSetting = new list<Alignment_Global_Settings__c>();
        
        Alignment_Global_Settings__c alignmentsettingThresholdValue  = new Alignment_Global_Settings__c();
        alignmentsettingThresholdValue.Name = 'MapAccountThresholdValue';
        alignmentsettingThresholdValue.Tree_Hierarchy_Sort_Field__c = '1000';
        //insert alignmentsettingThresholdValue;
        allAlignmentGlobalSetting.add(alignmentsettingThresholdValue);
        
        insert allAlignmentGlobalSetting;
        
        Organization_Master__c org = CallPlanTestDataFactory.createOrganizationMasterRecord();
        insert org;
        Country__c country = CallPlanTestDataFactory.createCountryMasterRecord(org.id);
        insert country;
        Geography_Type__c geotype = CallPlanTestDataFactory.createGeographyTypeRecord(country.id);
        insert geotype;
        
        
        Team__c geoBasedTeam = new Team__c(Name='HTN',Type__c='Base', Country__c=country.id);
        insert geoBasedTeam;
        
        Team_Instance__c geoBasedTeamInstance = new Team_Instance__c();
        geoBasedTeamInstance.Name = 'HTN_Q1_2016';
        geoBasedTeamInstance.Alignment_Period__c = 'HTN_Q1_2016';
        geoBasedTeamInstance.Team__c = geoBasedTeam.id;
        geoBasedTeamInstance.Alignment_Type__c = 'ZIP';
        geoBasedTeamInstance.isActiveCycle__c = 'Y';
        insert geoBasedTeamInstance;
        
        
        Position__c district = new Position__c();
        district.name = 'Newyork';
        district.Client_Territory_Name__c = 'Newyork';
        district.Client_Position_Code__c = '1NE30001';
        district.Client_Territory_Code__c='1NE30001';
        district.Position_Type__c='District';
        district.inactive__c = false;
        district.RGB__c = '41,210,117';
        district.Team_iD__c = geoBasedTeam.id;     
        district.Hierarchy_Level__c='2';
        insert district;
        
        
        Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, null, geoBasedTeamInstance);
        districtTeamInstance.X_Max__c=-72.6966429900;
        districtTeamInstance.X_Min__c=-73.9625820000;
        districtTeamInstance.Y_Max__c=40.9666490000;
        districtTeamInstance.Y_Min__c=40.5821279800;
        insert districtTeamInstance;
        
        
        Position__c territory = new Position__c();
        territory.name = 'Long Island East';
        territory.Client_Territory_Name__c = 'Long Island East';
        territory.Client_Position_Code__c = '1NE30011';
        territory.Client_Territory_Code__c='1NE30011';
        territory.Position_Type__c='Territory';
        territory.inactive__c = false;
        territory.RGB__c = '41,210,117';
        territory.Team_iD__c = geoBasedTeam.id;
        territory.Parent_Position__c = district.Id;
        insert territory;
        
        Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, district, geoBasedTeamInstance);
        territoryTeamInstance.X_Max__c=-72.6966429900;
        territoryTeamInstance.X_Min__c=-73.9625820000;
        territoryTeamInstance.Y_Max__c=40.9666490000;
        territoryTeamInstance.Y_Min__c=40.5821279800;
        insert territoryTeamInstance;
        
        
        Position__c destTerr = new Position__c();
        destTerr.Name = 'Long Island West';
        destTerr.Client_Territory_Name__c = 'Long Island West';
        destTerr.Client_Position_Code__c = '1NE30012';
        destTerr.Client_Territory_Code__c='1NE30012';
        destTerr.Position_Type__c='Territory';
        destTerr.inactive__c = false;
        destTerr.RGB__c = '41,210,117';
        destTerr.Team_iD__c = geoBasedTeam.id;
        destTerr.Parent_Position__c = district.Id;
        insert destTerr;
        
        Position_Team_Instance__c destTerrTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(destTerr, district, geoBasedTeamInstance);
        destTerrTeamInstance.X_Max__c=-72.6966429900;
        destTerrTeamInstance.X_Min__c=-73.9625820000;
        destTerrTeamInstance.Y_Max__c=40.9666490000;
        destTerrTeamInstance.Y_Min__c=40.5821279800;
        insert destTerrTeamInstance;
        
        User_Access_Permission__c access = new User_Access_Permission__c();
        access.Position__c = district.id;
        access.Map_Access_Position__c = district.id;
        access.name='access';
        access.Team_Instance__c = geoBasedTeamInstance.id;   
        access.User__c = UserInfo.getUserId();
        access.Is_Active__c = true;
        insert access;
        
        
        Geography__c geo1 = new Geography__c();
        geo1.Name = '11788';
        geo1.City__c='Test 11788';
        geo1.State__c = '11788';
        geo1.Centroid_Latitude__c = 0.0;
        geo1.Centroid_Longitude__c = 0.0;
        insert geo1; 
        
        /*Team_Instance_Geography__c teamInsGeo1 = new Team_Instance_Geography__c();
        teamInsGeo1.Geography__c = geo1.id;
        teamInsGeo1.Team_Instance__c = geoBasedTeamInstance.id;
        teamInsGeo1.Effective_Start_Date__c = Date.today().addMonths(-1);
        teamInsGeo1.Effective_End_Date__c = Date.today().addYears(1);
        teamInsGeo1.Metric1__c  =2.010000000000;
        teamInsGeo1.Metric2__c  =29.000000000000;
        teamInsGeo1.Metric3__c  =2.000000000000;
        teamInsGeo1.Metric4__c  =1.431479545000;
        teamInsGeo1.Metric5__c  =1.490000000000;
        teamInsGeo1.Metric6__c  =2.010000000000;
        teamInsGeo1.Metric7__c  =29.000000000000;
        teamInsGeo1.Metric8__c  =2.000000000000;
        teamInsGeo1.Metric9__c  =1.431479545000;
        teamInsGeo1.Metric10__c =1.490000000000;
        insert teamInsGeo1;*/
        
        Position_Geography__c posGeo1  = new Position_Geography__c();
        posGeo1.Effective_Start_Date__c  = Date.today().addMonths(-1);
        posGeo1.Effective_End_Date__c  = Date.today().addYears(1);
        posGeo1.Geography__c = geo1.id;
        posGeo1.Position__c = territory.id;
        posGeo1.Team_Instance__c = geoBasedTeamInstance.id ;
       // posGeo1.Team_Instance_Geography__c = teamInsGeo1.id;
        posGeo1.Position_Team_Instance__c = territoryTeamInstance.id;
        posGeo1.Change_Status__c = 'No Change';
        insert posGeo1;
        
        Geography__c geo2 = new Geography__c();
        geo2.Name = '11789';
        geo2.City__c='Test 11789';
        geo2.State__c = '11789';
        geo2.Centroid_Latitude__c = 0.0;
        geo2.Centroid_Longitude__c = 0.0;
        geo2.Zip_Type__c = 'Point';
        geo2.Parent_Zip_Code__c = geo1.Id;
        insert geo2; 
        
        /*Team_Instance_Geography__c teamInsGeo2 = new Team_Instance_Geography__c();
        teamInsGeo2.Geography__c = geo2.id;
        teamInsGeo2.Team_Instance__c = geoBasedTeamInstance.id;
        teamInsGeo2.Effective_Start_Date__c = Date.today().addMonths(-1);
        teamInsGeo2.Effective_End_Date__c = Date.today().addYears(1);
        teamInsGeo2.Metric1__c  =2.010000000000;
        teamInsGeo2.Metric2__c  =29.000000000000;
        teamInsGeo2.Metric3__c  =2.000000000000;
        teamInsGeo2.Metric4__c  =1.431479545000;
        teamInsGeo2.Metric5__c  =1.490000000000;
        teamInsGeo2.Metric6__c  =2.010000000000;
        teamInsGeo2.Metric7__c  =29.000000000000;
        teamInsGeo2.Metric8__c  =2.000000000000;
        teamInsGeo2.Metric9__c  =1.431479545000;
        teamInsGeo2.Metric10__c =1.490000000000;
        insert teamInsGeo2;*/
        
        Position_Geography__c posGeo2  = new Position_Geography__c();
        posGeo2.Effective_Start_Date__c  = Date.today().addMonths(-1);
        posGeo2.Effective_End_Date__c  = Date.today().addYears(1);
        posGeo2.Geography__c = geo2.id;
        posGeo2.Position__c = territory.id;
        posGeo2.Team_Instance__c = geoBasedTeamInstance.id ;
        //posGeo2.Team_Instance_Geography__c = teamInsGeo2.id;
        posGeo2.Position_Team_Instance__c = territoryTeamInstance.id;
        posGeo2.Change_Status__c = 'No Change';
        insert posGeo2;
        
        
        Account acc = new Account();
        acc.Name = '11788';
        acc.FirstName__c = 'Abby';
        acc.BillingCity='NJ';
        acc.BillingState='NY';
        acc.AccountNumber='11788';
        acc.type = 'Physician';
        acc.AccountType__c = 'Physician';
        acc.BillingPostalCode = geo2.name;
        acc.Speciality__c = '11788Oncology';
        acc.Alignment_Type__c = 'Direct Aligned';
        acc.BillingLatitude = 40.5571389900;
        acc.BillingLongitude = -74.2123329900;
        insert acc;
        
        /*Team_Instance_Account__c teamInstanceAcc = CallPlanTestDataFactory.createTeamInstanceAccount(acc,geoBasedTeamInstance);
        insert teamInstanceAcc;*/
        
        
        Change_Request__c objChangeRequest = new Change_Request__c();
        objChangeRequest.Approver1__c = usr.id;
        objChangeRequest.Destination_Position__c = destTerr.id;
        objChangeRequest.Source_Position__c = territory.id;
        objChangeRequest.AllZips__c = '11788';
        objChangeRequest.Status__c = 'Pending';
        objChangeRequest.OwnerID = usr.id;
        objChangeRequest.Request_Type_Change__c = SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP;
        objChangeRequest.Team_Instance_ID__c = geoBasedTeamInstance.id;
        insert objChangeRequest;
        
        
            
        
        CR_Account__c crAcc = new CR_Account__c();
        crAcc.Account__c = acc.id;
        crAcc.Destination_Position__c = destTerr.id;
        crAcc.Source_Position__c = territory.id;
        crAcc.Destination_Position_Team_Instance__c = destTerrTeamInstance.id;
        crAcc.Source_Position_Team_Instance__c = territoryTeamInstance.id;
        //crAcc.Team_Instance_Account__c = teamInstanceAcc.id;
        crAcc.Change_Request__c = objChangeRequest.Id;
        crAcc.IsImpactCallPlan__c = true;
        insert crAcc;
        
        test.stopTest();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objChangeRequest);
        MAPInlineCtrl onjMApinline = new MAPInlineCtrl(sc);
                
        string zips = geo2.Name+','+geo1.Name;
        AggregateResult result = MAPInlineCtrl.getExtents(zips+':'+'Zip');
        AggregateResult result1 = MAPInlineCtrl.getExtents(zips+':'+'Accounts');        
        
        
    }
    
    
    static testMethod void testAccountAlignment(){
        
        Profile hoProfile = [SELECT Id FROM Profile WHERE Name =: 'System Administrator' limit 1];
        
        User usr = new User(LastName = 'ho',
                            FirstName='test',
                            Alias = 'test',
                            Email = 'test.ho@test.com',
                            Username = 'test.ho@test.com',
                            ProfileId = hoProfile.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US');
        insert usr;
        
        test.startTest();
        
        Organization_Master__c org = CallPlanTestDataFactory.createOrganizationMasterRecord();
        insert org;
        Country__c country = CallPlanTestDataFactory.createCountryMasterRecord(org.id);
        insert country;
        Geography_Type__c geotype = CallPlanTestDataFactory.createGeographyTypeRecord(country.id);
        insert geotype;
        
        Team__c AccountBasedTeam = new Team__c(Name='HTN',Type__c='Account', country__c=country.id);
        insert AccountBasedTeam;
        
        Team_Instance__c AccountBasedTeamInstance = new Team_Instance__c();
        AccountBasedTeamInstance.Name = 'HTN_Q1_2016';
        AccountBasedTeamInstance.Alignment_Period__c = 'HTN_Q1_2016';
        AccountBasedTeamInstance.Team__c = AccountBasedTeam.id;
        AccountBasedTeamInstance.Alignment_Type__c = 'Account';
        AccountBasedTeamInstance.isActiveCycle__c = 'Y';
        insert AccountBasedTeamInstance;
        
         list<Alignment_Global_Settings__c> allAlignmentGlobalSetting = new list<Alignment_Global_Settings__c>();
        Alignment_Global_Settings__c alignmentsettingThresholdValue  = new Alignment_Global_Settings__c();
        alignmentsettingThresholdValue.Name = 'MapAccountThresholdValue';
        alignmentsettingThresholdValue.Tree_Hierarchy_Sort_Field__c = '1000';
        
        allAlignmentGlobalSetting.add(alignmentsettingThresholdValue);
        
        Alignment_Global_Settings__c alignmentsettingMapAccountClusterFlag  = new Alignment_Global_Settings__c();
        alignmentsettingMapAccountClusterFlag.Name = 'MapAccountClusterFlag';
        alignmentsettingMapAccountClusterFlag.Tree_Hierarchy_Sort_Field__c = 'true';
        //insert alignmentsettingMapAccountClusterFlag;
        allAlignmentGlobalSetting.add(alignmentsettingMapAccountClusterFlag);
        
        insert allAlignmentGlobalSetting;
        
        list<Team_Instance_Object_Attribute__c> allTeamAttrConfig = new list<Team_Instance_Object_Attribute__c>();
        Team_Instance_Object_Attribute__c teamInstAttrAccountRendring =  CallPlanTestDataFactory.createObjectAttributes(AccountBasedTeamInstance, SalesIQGlobalConstants.ACCOUNT_RENDERING_INTERFACE_NAME,'Metric1__c','Position_Account__c','Alignment Index',true,true,1);       
        
        allTeamAttrConfig.add(teamInstAttrAccountRendring);
        
        insert allTeamAttrConfig;
        
        
        Position__c district = new Position__c();
        district.name = 'Newyork';
        district.Client_Territory_Name__c = 'Newyork';
        district.Client_Position_Code__c = '1NE30001';
        district.Client_Territory_Code__c='1NE30001';
        district.Position_Type__c='District';
        district.inactive__c = false;
        district.RGB__c = '41,210,117';
        district.Team_iD__c = AccountBasedTeam.id;
        district.Hierarchy_Level__c = '2';
        insert district;
        
        
        Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, null, AccountBasedTeamInstance);
        districtTeamInstance.X_Max__c=-72.6966429900;
        districtTeamInstance.X_Min__c=-73.9625820000;
        districtTeamInstance.Y_Max__c=40.9666490000;
        districtTeamInstance.Y_Min__c=40.5821279800;
        insert districtTeamInstance;
        
        
        Position__c territory = new Position__c();
        territory.name = 'Long Island East';
        territory.Client_Territory_Name__c = 'Long Island East';
        territory.Client_Position_Code__c = '1NE30011';
        territory.Client_Territory_Code__c='1NE30011';
        territory.Position_Type__c='Territory';
        territory.inactive__c = false;
        territory.RGB__c = '41,210,117';
        territory.Team_iD__c = AccountBasedTeam.id;
        territory.Parent_Position__c = district.Id;
        insert territory;
        
        Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, district, AccountBasedTeamInstance);
        territoryTeamInstance.X_Max__c=-72.6966429900;
        territoryTeamInstance.X_Min__c=-73.9625820000;
        territoryTeamInstance.Y_Max__c=40.9666490000;
        territoryTeamInstance.Y_Min__c=40.5821279800;
        insert territoryTeamInstance;
        
        
        Position__c destTerr = new Position__c();
        destTerr.Name = 'Long Island West';
        destTerr.Client_Territory_Name__c = 'Long Island West';
        destTerr.Client_Position_Code__c = '1NE30012';
        destTerr.Client_Territory_Code__c='1NE30012';
        destTerr.Position_Type__c='Territory';
        destTerr.inactive__c = false;
        destTerr.RGB__c = '41,210,117';
        destTerr.Team_iD__c = AccountBasedTeam.id;
        destTerr.Parent_Position__c = district.Id;
        insert destTerr;
        
        Position_Team_Instance__c destTerrTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(destTerr, district, AccountBasedTeamInstance);
        destTerrTeamInstance.X_Max__c=-72.6966429900;
        destTerrTeamInstance.X_Min__c=-73.9625820000;
        destTerrTeamInstance.Y_Max__c=40.9666490000;
        destTerrTeamInstance.Y_Min__c=40.5821279800;
        insert destTerrTeamInstance;
        
        User_Access_Permission__c access = new User_Access_Permission__c();
        access.Position__c = district.id;
        access.Map_Access_Position__c = district.id;
        access.name='access';
        access.Team_Instance__c = AccountBasedTeamInstance.id;   
        access.User__c = UserInfo.getUserId();
        access.Is_Active__c = true;
        insert access;
        
        
        Geography__c geo1 = new Geography__c();
        geo1.Name = '11788';
        geo1.City__c='Test 11788';
        geo1.State__c = '11788';
        geo1.Centroid_Latitude__c = 0.0;
        geo1.Centroid_Longitude__c = 0.0;
        insert geo1; 
        
        /*Team_Instance_Geography__c teamInsGeo1 = new Team_Instance_Geography__c();
        teamInsGeo1.Geography__c = geo1.id;
        teamInsGeo1.Team_Instance__c = AccountBasedTeamInstance.id;
        teamInsGeo1.Effective_Start_Date__c = Date.today().addMonths(-1);
        teamInsGeo1.Effective_End_Date__c = Date.today().addYears(1);
        teamInsGeo1.Metric1__c  =2.010000000000;
        teamInsGeo1.Metric2__c  =29.000000000000;
        teamInsGeo1.Metric3__c  =2.000000000000;
        teamInsGeo1.Metric4__c  =1.431479545000;
        teamInsGeo1.Metric5__c  =1.490000000000;
        teamInsGeo1.Metric6__c  =2.010000000000;
        teamInsGeo1.Metric7__c  =29.000000000000;
        teamInsGeo1.Metric8__c  =2.000000000000;
        teamInsGeo1.Metric9__c  =1.431479545000;
        teamInsGeo1.Metric10__c =1.490000000000;
        insert teamInsGeo1;
        */
            
        
        Account acc = new Account();
        acc.Name = '11788';
        acc.FirstName__c = 'Abby';
        acc.BillingCity='NJ';
        acc.BillingState='NY';
        acc.AccountNumber='11788';
        acc.type = 'Physician';
        acc.AccountType__c = 'Physician';
        acc.BillingPostalCode = geo1.name;
        acc.Speciality__c = '11788Oncology';
        acc.Alignment_Type__c = 'Direct Aligned';
        acc.BillingLatitude = 40.5571389900;
        acc.BillingLongitude = -74.2123329900;
        insert acc;
        
        /*Team_Instance_Account__c teamInstanceAcc = CallPlanTestDataFactory.createTeamInstanceAccount(acc,AccountBasedTeamInstance);
        insert teamInstanceAcc;*/
        
        /*Position_Account__c posAcc2 = CallPlanTestDataFactory.createPositionAccount(acc, territory, AccountBasedTeamInstance, territoryTeamInstance);
        insert posAcc2;
        */
        
        Change_Request__c objChangeRequestAccount = new Change_Request__c();
        objChangeRequestAccount.Approver1__c = usr.id;
        objChangeRequestAccount.Destination_Position__c = destTerr.id;
        objChangeRequestAccount.Source_Position__c = territory.id;
        objChangeRequestAccount.Account_Moved_Id__c = acc.AccountNumber;
        objChangeRequestAccount.Account_SF_Id__c = acc.id;
        objChangeRequestAccount.Status__c = 'Pending';
        objChangeRequestAccount.OwnerID = usr.id;
        objChangeRequestAccount.Request_Type_Change__c = SalesIQGlobalConstants.MOVEMENT_TYPE_ACCOUNT;
        objChangeRequestAccount.Team_Instance_ID__c = AccountBasedTeamInstance.id;
        insert objChangeRequestAccount;
        
        
        CR_Account__c crAcc = new CR_Account__c();
        crAcc.Account__c = acc.id;
        crAcc.Destination_Position__c = destTerr.id;
        crAcc.Source_Position__c = territory.id;
        crAcc.Destination_Position_Team_Instance__c = destTerrTeamInstance.id;
        crAcc.Source_Position_Team_Instance__c = territoryTeamInstance.id;
        //crAcc.Team_Instance_Account__c = teamInstanceAcc.id;
        crAcc.Change_Request__c = objChangeRequestAccount.Id;
        crAcc.IsImpactCallPlan__c = true;
        insert crAcc;
         
        Team_Instance_Object_Attribute__c teamInstAttr =  new Team_Instance_Object_Attribute__c();
        teamInstAttr.Interface_Name__c = 'AccInfo';
        teamInstAttr.Attribute_API_Name__c = 'Position__r.Name';
        teamInstAttr.Object_Name__c = 'Position_Account__c';
        teamInstAttr.Data_Type__c = 'Text';
        teamInstAttr.Attribute_Display_Name__c = 'Position';
        teamInstAttr.isEnabled__c=true;
        teamInstAttr.Team_Instance__c = AccountBasedTeamInstance.Id;
        teamInstAttr.Display_Column_Order__c = 1;
        insert teamInstAttr;   
        
        test.stopTest();
        ApexPages.StandardController Accountsc = new ApexPages.StandardController(objChangeRequestAccount);
        MAPInlineCtrl onjMApinlineAcc = new MAPInlineCtrl(Accountsc);  
        
        onjMApinlineAcc.getLayerConfig();
        MAPInlineCtrl.getOrgNameSpace();
        MAPInlineCtrl.getAccountInfoDetails('HTN_Q3'); 
        MAPInlineCtrl.getTeamAndTeamInstances();
        //MAPInlineCtrl.getAccountRenderingAttributes('HTN_Q3');
        MAPInlineCtrl.getAccountInfoValues('11788',AccountBasedTeamInstance.id,'Current');
        MAPInlineCtrl.getExtentsforRequestGeography('','');
        MAPInlineCtrl.getExtents('');
                    
        map<string,List<MAPInlineCtrl.AccountDataWrapper>> mapAcc = MAPInlineCtrl.getAccountDataforChangeRequest(district.id, AccountBasedTeamInstance.id, 1,acc.BillingLongitude, acc.BillingLatitude,acc.BillingLatitude,acc.BillingLatitude);
        //system.assertEquals(1, mapAcc.size());
        map<String,list<Team_Instance_Object_Attribute_Detail__c>> mapdetail  = MAPInlineCtrl.getAccountRenderingAttributes(AccountBasedTeamInstance.id);
        
        map<string,List<Position_Account__c>> lstGetData  =  MAPInlineCtrl.getAccountData('','',0,0,0,0);
        map<string,List<Position_Account__c>> lstGetData1  =  MAPInlineCtrl.getAccountData('','',1,0,0,0,0);
        
        string local  = onjMApinlineAcc.userLocaleCountry;
        
        
        
        
    }
    
    
    
    
    
    
    
}