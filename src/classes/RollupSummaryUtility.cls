/*************************************************************************************************************************************
* Name			: RollupSummaryUtility
* Author 		: Raghvendra.Rathore
* Created On 	: 21-Feb-2017
* Description 	: Batch class to populate extents on Positions while data load in Position Geography and Position Account.
* Parameters	: String - PositionAccount/PositionGeography
				  String - Team instance id
**************************************************************************************************************************************/
global class RollupSummaryUtility implements Database.Batchable<sObject>{
	
    global final String Query;
    global final String teamInstanceId;
    global final String sobjectName;
    
    global RollupSummaryUtility(string sObjectType, string teamInstance){
        system.debug('#### RollUpExtentsOnPositionBatch called');
        teamInstanceId = teamInstance;
        sobjectName = sObjectType;
        if(sObjectName == 'Position_Geography__c'){
          Query = 'Select Id,Position_Id_External__c, Position_Team_Instance__c from '+sObjectName+' where Team_Instance__c =: teamInstanceId';
        }else{
          Query = 'Select Id,Position_Team_Instance__c from '+sObjectName+' where Team_Instance__c =: teamInstanceId';
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        system.debug('#### Start method executed');
        system.debug('#### Query : '+Query);
        system.debug('#### teamInstanceId : '+teamInstanceId);
        return Database.getQueryLocator(Query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        system.debug('#### execute method executed');
        system.debug('#### scope : '+scope.size());
        if(sobjectName == 'Position_Account__c'){
        	list<Position_Account__c> posAccList = new list<Position_Account__c>();
	        for(SObject obj : scope){
	            posAccList.add((Position_Account__c)obj);
	        }
	        system.debug('#### posAccList : '+posAccList);
	        PositionAccountTriggerHandler.rollupExtentsOnTerritoryFromTalend(posAccList,new map<id,Position_Account__c>());
        }else if(sobjectName == 'Position_Geography__c'){
        	list<Position_Geography__c> posGeoList = new list<Position_Geography__c>();
            set<Id> posGeoIds = new set<Id>();
	        for(SObject obj : scope){
	            posGeoList.add((Position_Geography__c)obj);
                posGeoIds.add(obj.Id);
	        }
	        system.debug('#### posGeoList : '+posGeoList);
            HandlerPosGeo.rollupExtentsOnTerritory(posGeoIds, new list<set<Id>>() );
        }else{}
    }
    
    global void finish(Database.BatchableContext BC){
        system.debug('#### finish method executed');
    }
}