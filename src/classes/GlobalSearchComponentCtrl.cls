/**********************************************************************************************
    Name        :   GlobalSearchComponentCtrl
    Description :   Controller for GlobalSearchComponent
                    Contains remote method to fetch Account/Geography based on input provided.
**********************************************************************************************/
global with sharing class GlobalSearchComponentCtrl{
    public GlobalSearchComponentCtrl() {}
        
    @RemoteAction 
    global static list<string> globalFind(string searchStr){
        return new list<string>();
    }
    
    @RemoteAction 
    global static list<string> globalFindwithSoql(string searchStr){
        return new list<string>();
    }
}