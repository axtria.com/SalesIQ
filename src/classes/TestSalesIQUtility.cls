
public class TestSalesIQUtility {

    /*static testMethod void testGetChildPositions() {
        Team__c baseTeam = TestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_BASE);
        insert baseTeam ;

        Position__c parentPosition = TestDataFactory.createPosition(baseTeam, 'Parent Position', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_DISTRICT, '2', null);
        insert parentPosition ;

        Position__c child1 = TestDataFactory.createPosition(baseTeam, 'Child 1', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_TERRITORY, '1', parentPosition.Id);
        Position__c child2 = TestDataFactory.createPosition(baseTeam, 'Child 2', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_TERRITORY, '1', parentPosition.Id);
        Position__c child3 = TestDataFactory.createPosition(baseTeam, 'Child 3', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_TERRITORY, '1', parentPosition.Id);
        insert new Position__c[]{child1, child2, child3};

        SalesIQUtility.getChildPositions(parentPosition.Id, Date.today());
    }

    static testMethod void testGetAssociatedPositionEmployees() {
        Team__c baseTeam = TestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_BASE);
        insert baseTeam ;

        Position__c position = TestDataFactory.createPosition(baseTeam, 'Position', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_TERRITORY, '1', null);
        insert position ;
        Employee__c employee = TestDataFactory.createEmployee('Billy', 'Hudson');
        insert employee ;

        Position_Employee__c pe = TestDataFactory.createPositionEmployee(position.Id, employee.Id, SalesIQGlobalConstants.POSITION_EMPLOYEE_ASSIGNMENT_PRIMARY, Date.today().addDays(-10), Date.today().addDays(10));
        insert pe ;

        List<Position_Employee__c> result = SalesIQUtility.getAssociatedPositionEmployees(position.Id, Date.today());
    }

    static testMethod void testGetAssociatedPositionAccounts() {
        Team__c baseTeam = TestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_BASE);
        insert baseTeam ;

        Workspace__c workspace = TestDataFactory.createWorkspace('Test', System.today().addMonths(-1), System.today().addMonths(1));
        insert workspace ;

        Scenario__c scenario = TestDataFactory.createScenario(workspace.Id,null,null,'Live','Ready','Active',System.today().addMonths(-1),System.today().addMonths(2));
        insert scenario ;

        Team_Instance__c baseTeamInstance = TestDataFactory.createTeamInstance(baseTeam.Id,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP,scenario.Id,Date.today()-100,Date.today()+100);
        insert baseTeamInstance;

        Position__c position = TestDataFactory.createPosition(baseTeam, 'Position', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_TERRITORY, '1', null);
        insert position ;

        Position_Team_Instance__c positionTeamInstance = TestDataFactory.createPositionTeamInstance(position.Id, null, baseTeamInstance.Id);
        insert positionTeamInstance;

        Position_Account__c positionAccount = new Position_Account__c(Position_Team_Instance__c=positionTeamInstance.Id, Team_Instance__c=baseTeamInstance.Id, Effective_Start_Date__c=System.today().addMonths(-2), Effective_End_Date__c=System.today().addMonths(2));
        insert positionAccount ;

        List<Position_Account__c> result = SalesIQUtility.getAssociatedPositionAccounts(position.Id, baseTeamInstance.Id, System.today());
    }

    static testMethod void testGetAssociatedPositionGeographies() {
        Team__c baseTeam = TestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_BASE);
        insert baseTeam ;

        Workspace__c workspace = TestDataFactory.createWorkspace('Test', System.today().addMonths(-1), System.today().addMonths(1));
        insert workspace ;

        Scenario__c scenario = TestDataFactory.createScenario(workspace.Id,null,null,'Live','Ready','Active',System.today().addMonths(-1),System.today().addMonths(2));
        insert scenario ;

        Team_Instance__c baseTeamInstance = TestDataFactory.createTeamInstance(baseTeam.Id,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP,scenario.Id,Date.today()-100,Date.today()+100);
        insert baseTeamInstance;

        Position__c position = TestDataFactory.createPosition(baseTeam, 'Position', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_TERRITORY, '1', null);
        insert position ;

        Position_Team_Instance__c positionTeamInstance = TestDataFactory.createPositionTeamInstance(position.Id, null, baseTeamInstance.Id);
        insert positionTeamInstance;

        Position_Geography__c positionGeography = new Position_Geography__c(Position_Team_Instance__c=positionTeamInstance.Id, Team_Instance__c=baseTeamInstance.Id, Effective_Start_Date__c=System.today().addMonths(-2), Effective_End_Date__c=System.today().addMonths(2));
        insert positionGeography ;

        List<Position_Geography__c> result = SalesIQUtility.getAssociatedPositionGeographies(position.Id, baseTeamInstance.Id, System.today());
    }

    static testMethod void testGetAssociatedOverlayByBase() {

    }

    static testMethod void testGetRelatedPositions() {

    }

    static testMethod void testGetBaseParentRelatedPositions() {

    }

    static testMethod void testIsPositionGeographyExist() {

    }

    static testMethod void testIsPositionAccountExist() {
    	
    }

    // Test all getRootpositionbyId versions with overlay team
    static testMethod void testGetPositionbyRootPositionId() {
        Alignment_Global_Settings__c alignmentGlobal =  new Alignment_Global_Settings__c(Name = 'global', Tree_Hierarchy_Sort_Field__c='Name');
        insert alignmentGlobal ;

        Team__c baseTeam = TestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_BASE);
        insert baseTeam ;

        Team__c overlayTeam = TestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_OVERLAY);
        overlayTeam.Name='HTN Overlay';
        overlayTeam.Controlling_Team__c=baseTeam.id;
        insert overlayTeam ;

        Team_Instance__c baseTeamInstance = TestDataFactory.createTeamInstance(baseTeam.Id,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP,'',Date.today()-100,Date.today()+100);
        insert baseTeamInstance;

        Team_Instance__c overlayTeamInstance = TestDataFactory.createTeamInstance(overlayTeam.Id,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP,'',Date.today()-100,Date.today()+100);

        Position__c districtPosition = TestDataFactory.createPosition(baseTeam, 'District', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_DISTRICT, '2', null);
        insert districtPosition;

        Position__c position = TestDataFactory.createPosition(overlayTeam, 'Territory', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_TERRITORY, '1', districtPosition.Id);
        insert position;

        SalesIQUtility.getPositionbyRootPositionId(districtPosition.Id, SalesIQGlobalConstants.TEAM_TYPE_OVERLAY);
    }

    static testMethod void testGetChangeRequestCurrentStatus() {
        Team__c baseTeam = TestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_BASE);
        insert baseTeam ;

        Team_Instance__c baseTeamInstance = TestDataFactory.createTeamInstance(baseTeam.Id,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP,'',Date.today()-100,Date.today()+100);
        insert baseTeamInstance;

        Position__c position = TestDataFactory.createPosition(baseTeam, 'Territory', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_TERRITORY, '1', null);
        insert position;

        Change_Request__c cr = TestDataFactory.createChangeRequest(UserInfo.getUserId(), position.Id, position.Id, baseTeamInstance.Id, SalesIQGlobalConstants.REQUEST_STATUS_PENDING);
        insert cr;

        SalesIQUtility.getChangeRequestCurrentStatus(cr.Id);
    }

    static testMethod void testCreateCIMpositionMetricFromBeginning() {
        //not at all covered createCIMpositionMetricFromBeginning

    }

    static testMethod void testGetAccountData() {
        // nothing is getting covered inside try

        // second variant of method is never called.
    }

    static testMethod void testCreateApprovals() {

    }

    static testMethod void testGetCRPositionById() {
        CR_Position__c data = new CR_Position__c() ;
        insert data;

        SalesIQUtility.getCRPositionById(data.Id);
    }

    static testMethod void testGetCreateCRempAssgn() {
        // test with isNew__c = false case
        //SalesIQUtility.createCRempAssgn();
    }

    static testMethod void testCreatePositionTeamInstance() {
        // test with isCreatePosMetric=true case
        // SalesIQUtility.createPositionTeamInstance();
    }

    static testMethod void testGenerateRandomNumber() {
        SalesIQUtility.generateRandomNumber();
    }

    static testMethod void testGetAlignmentAccessPermission() {
        SalesIQUtility.getAlignmentAccessPermission();
    }

    static testMethod void testGetOverlayTeamByBaseTeam() {
        Team__c baseTeam = TestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_BASE);
        insert baseTeam ;

        Team__c overlayTeam = TestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_OVERLAY);
        overlayTeam.Name='HTN Overlay';
        overlayTeam.Controlling_Team__c=baseTeam.id;
        insert overlayTeam ;

        SalesIQUtility.getOverlayTeamByBaseTeam(baseTeam.Id);
    }

    static testMethod void testGetOverlayInstanceTeamByBaseTeam() {
        Team__c baseTeam = TestDataFactory.createTeam(SalesIQGlobalConstants.TEAM_TYPE_BASE);
        insert baseTeam ;

        Team_Instance__c baseTeamInstance = TestDataFactory.createTeamInstance(baseTeam.Id,SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP,'',Date.today()-100,Date.today()+100);
        insert baseTeamInstance;

        SalesIQUtility.getOverlayInstanceTeamByBaseTeam(baseTeam.Id);
    }

    static testMethod void testGetTeamInstanceGeographyByGeographyAndTeamInstance() {
        SalesIQUtility.getTeamInstanceGeographyByGeographyAndTeamInstance(new Set<String>{}, '');
    }

    //SalesIQUtility.getPositionAccountByPositionId()
    //SalesIQUtility.getAggregatePositionAccountByAccountId()
    //SalesIQUtility.getCurrentTeamInstances()
    //SalesIQUtility.getTeamInstanceListHierarchy();
    // onBeforeInsertWorkspace();
    // updateTeamInstanceCode();
    // updateTeamCode();
    // getPositionAccountByAccountSFId();
    // returnMirrorCRObject();

    static testMethod void testGetMetricQuery() {
        SalesIQUtility.getMetricQuery();
        SalesIQUtility.getUserDateFormat();
        SalesIQUtility.getUserDateTimeFormat();
        
    }

*/

}