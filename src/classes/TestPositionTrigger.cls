@isTest
public with sharing class TestPositionTrigger {
	static testMethod void testUpdate(){
		TriggerContol__c triggerSetting = new TriggerContol__c(Name='PositionTrigger',IsStopTrigger__c=false);
		insert triggerSetting;
		
		MapServerURL__c ESRIService = MapServerURL__c.getOrgDefaults();
		ESRIService.Endpoint__c = 'https://uat.align-max.com/arcgis/rest/services/Nextgen/USA_ZIP5_2015_LT_Nextgen_Dev';
		ESRIService.Service_URL__c = '/MapServer';
		ESRIService.Territory_Layer__c = '/MapServer/1';
		ESRIService.ZIP_Layer__c = '/MapServer/0';
		upsert ESRIService;
		
		Team__c team = new Team__c(Name='HTN',Type__c='GeoBased');
		insert team;
		
		Team_Instance__c teamInstance = new Team_Instance__c();
		teamInstance.Name = 'HTN_Q1_2016';
		teamInstance.Alignment_Period__c = 'HTN_Q1_2016';
		teamInstance.Team__c = team.id;
		teamInstance.Alignment_Type__c = 'Zip';
		teamInstance.isActiveCycle__c = 'Y';
		insert teamInstance;
		
		Employee__c emp = CallPlanTestDataFactory.createEmployee();
		emp.USER__c = userinfo.getuserid();
        insert emp;
		
		Employee__c emp2 = new Employee__c();
        emp2.Name = 'Test Emp2';
        emp2.Employee_ID__c = '12346';
        emp2.Last_Name__c = 'Emp2';
        emp2.FirstName__c = 'Test1';
        emp2.Gender__c = 'M';
        emp2.Change_status__c = 'Pending';
        emp2.USER__c = userinfo.getuserid();
        insert emp2;
		
		Position__c position = new Position__c();
	    position.Name = 'North East';
	    position.Client_Territory_Name__c = 'North east';
	 	position.Client_Position_Code__c = '1NE30000';
	  	position.Client_Territory_Code__c='1NE30000';
	    position.Position_Type__c='Region';
	    position.Inactive__c = false;
	    position.RGB__c = '41,210,117';
	    position.Team_iD__c = team.id;
	    position.Employee__c = emp.id;
		insert position;
		
		// Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new ESRIServicesMockGenerator());
		test.startTest();
			position.Name = 'NE';
			update position;
			position.Employee__c = emp2.id;
			update position;
		test.stopTest();
	}
}