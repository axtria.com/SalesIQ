public class SalesIQDataTableWrapper {    

	@AuraEnabled
	public String queryString{get;set;}

    @AuraEnabled
	public String ExclusionType{get;set;}
    
	@AuraEnabled
    public List<String> columnsList{get;set;}

    @AuraEnabled
    public List<Map<String,String>> columnsArray{get;set;}

    @AuraEnabled
    public List<List<String>> dataArray{get;set;}

     @AuraEnabled
    public List<Sobject> dataGridList{get;set;}

    @AuraEnabled
    public List<List<SalesIQDataTableReferenceWrapper>> dataMap{get;set;}

    @AuraEnabled
    public List<String> columnLabels{get;set;}

    @AuraEnabled
    public Map<String,String> dataTypesList {get;set;}

    @AuraEnabled
    public String objectLabel{get;set;}

    @AuraEnabled
    public Integer pageNumber{get;set;}

    @AuraEnabled
    public Integer pageSize{get;set;}

    @AuraEnabled
    public String searchString {get;set;}

    @AuraEnabled
    public Integer totalRecords {get;set;}

    // @AuraEnabled
    // public Map<String,String> referenceColumns{get;set;}

    @AuraEnabled
    public List<Interface_Style_Config__c> listInterfaceStyConf {get;set;}

    public class SalesIQDataTableReferenceWrapper {
        @AuraEnabled
        public String data{get;set;}
        
        @AuraEnabled
        public String ExclusionType{get;set;}

        @AuraEnabled
        public String referenceId{get;set;}

        @AuraEnabled
        public String positionTeamInstanceId{get;set;}

        @AuraEnabled
        public String isLink{get;set;}

        @AuraEnabled
        public String columnName{get;set;}

        @AuraEnabled
        public String isCreate {get;set;}

        @AuraEnabled
        public String isEdit {get;set;}

         @AuraEnabled
        public String isEditEmployee {get;set;}

        @AuraEnabled
        public String isDelete {get;set;}

        @AuraEnabled
        public String isChangeHierarchy {get;set;}

        @AuraEnabled
        public String isUpdateAttributes {get;set;}

        @AuraEnabled
        public String isEmployeeAssignment {get;set;}

        @AuraEnabled
        public String isOpenScenario {get;set;}

        @AuraEnabled
        public String countryStatus {get;set;} 

        @AuraEnabled
        public String isOpenCallPlan {get;set;}

        @AuraEnabled
        public String isOpenCanvas {get;set;}

        @AuraEnabled
        public String style {get;set;}
        
        @AuraEnabled
        public String editOverlayAssociationLabel {get;set;}
        
        @AuraEnabled
        public Boolean showSpanOnCountryName {get;set;}
        
        @AuraEnabled
        public String isFailedScenario {get;set;}
        
        @AuraEnabled
        public String deleteScenario {get;set;}

        @AuraEnabled
        public String isSendEmployeeEmail {get;set;}
    }


    // @AuraEnabled
    // public SalesIQDataTableReferenceWrapper referenceWrapper{get;set;}

    // @AuraEnabled
    // public List<List<SalesIQDataTableReferenceWrapper>> referenceWrapperList{get;set;}

    @AuraEnabled
    public String sortColumn{get;set;}

    @AuraEnabled
    public List<String> filterColumns{get;set;}
    
    @AuraEnabled
    public String orgNamespace {get{
        orgNamespace = SalesIQGlobalConstants.NAME_SPACE;
        return orgNamespace;
    }set;}

    @AuraEnabled
    public String teamInstance{get;set;}

    @AuraEnabled
    public String isMasterTeamInstance{get;set;}

    @AuraEnabled
    public String teamType{get;set;}
    
    @AuraEnabled
    public String errorText{get;set;}
}