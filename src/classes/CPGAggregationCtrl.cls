public with sharing class CPGAggregationCtrl {

     @AuraEnabled
    public static List<BRFieldWrapper> GetSourceAndDerivedFields(string scenarioRuleInstanceDetailsId){
       List<BRFieldWrapper> lstWrapper;
        try{
            system.debug('GetSourceAndDerivedFields');
            system.debug(scenarioRuleInstanceDetailsId);
             lstWrapper=CPGUtility.GetSourceAndDerivedFields(scenarioRuleInstanceDetailsId);
          } 
        catch(Exception ex){
             CPGUtility.InsertErrorLog(ex.getMessage(),ex.getStackTraceString());
        }
       return lstWrapper;
   }
   
   /**
     * calling Table Name for aggregation screen display
     *  
     *  @author  Aakash Gupta
     *  @version 1.0
     *  @since   2017-09-05  
    */
    @AuraEnabled
    public static List<Data_Set_Rule_Map__c> getInputTableName(string scenarioRuleInstanceDetailsId){
     
    	List<Data_Set_Rule_Map__c> inputOutpurTable = [SELECT Id, ds_type__c, table_name__c, Table_Display_Name__c 
                                                       FROM Data_Set_Rule_Map__c
                                                       WHERE scenario_rule_instance_id__c = :scenarioRuleInstanceDetailsId ];
    	//String inputTableName = inputTable[0t.Input_Table__c;
    	system.debug('Output Value:' +inputOutpurTable);
    	return inputOutpurTable;
    }
    
    /**
     * calling Aggregate Functions custom metatdata types for the aggregation functions dropdown
     *  
     *  @author  Aakash Gupta
     *  @version 1.0
     *  @since   2017-09-29  
    */
    
      @AuraEnabled
      public static  CPG_Aggregate_Functions__mdt[] getAggregateFunctionsList(){ 
        List<CPG_Aggregate_Functions__mdt> aggFunctions = [Select Id,Field_Type__c,	Functions__c from CPG_Aggregate_Functions__mdt ];
        system.debug('typesList====>'+aggFunctions);
          return  aggFunctions;
      } 
    
     /**
     * calling RuleId for Aggregate DerivedField
     *  
     *  @author  Aakash Gupta
     *  @version 1.0
     *  @since   2017-10-11  
    */
    @AuraEnabled
    public static List<Business_Rule_Type_Master__c > getRuleIdDetails(){
    
        List<Business_Rule_Type_Master__c > RuleId = [Select Id FROM Business_Rule_Type_Master__c where Name='Aggregate'];
        return RuleId;
    }
    
     /**
     * calling BRFMDetails paramns name and details
     *  
     *  @author  Aakash Gupta
     *  @version 1.0
     *  @since   2017-10-11  
    */
    @AuraEnabled
    public static List<Business_Rule_FieldMap_Details__c > getBRFMDetailsParams( string scenarioRuleInstanceId){
    
        List<Business_Rule_FieldMap_Details__c > parameterss = [SELECT  Param_Name__c FROM Business_Rule_FieldMap_Details__c where Scenario_Rule_Instance_Id__c= :scenarioRuleInstanceId];
        System.debug('params=='+parameterss);
        return parameterss;
    }
    
   /**
     * calling FieldToAggregate dropdown values
     *  
     *  @author  Aakash Gupta
     *  @version 1.0
     *  @since   2017-09-29  
    */
    
    @AuraEnabled
    public static List<FieldWrapper> getFieldToAggregateValues(string scenarioRuleInstanceDetailsId){
       List<FieldWrapper> lstWrapper=new List<FieldWrapper>();
       try{
             List<Data_Set_Rule_Map__c> lstDataSetRuleMap=new List<Data_Set_Rule_Map__c>(); 
             List<Data_Set_Column_Detail__c> lstDataColumnDetail=new List<Data_Set_Column_Detail__c>();        
             lstDataSetRuleMap=[select dataset_id__c from Data_Set_Rule_Map__c 
                                where scenario_rule_instance_id__r.Id= :scenarioRuleInstanceDetailsId and ds_type__c='I' and dataset_id__c !=null];
                if(lstDataSetRuleMap!=null && lstDataSetRuleMap.size()>0){
                    for(integer ctr=0;ctr<lstDataSetRuleMap.size();ctr++){
                       lstDataColumnDetail= [select ds_col_name__c,variable_type__c,value_type__c,datatype__c,tb_col_nm__c from Data_Set_Column_Detail__c 
                                            where dataset_id__r.Id= :lstDataSetRuleMap[0].dataset_id__c and value_type__c ='Discrete'   ];
                       if(lstDataColumnDetail!=null && lstDataColumnDetail.size()>0){
                           for(Data_Set_Column_Detail__c columnDetail : lstDataColumnDetail){
                               FieldWrapper wrapper=new  FieldWrapper(columnDetail.ds_col_name__c,columnDetail.variable_type__c
                                                                      ,columnDetail.value_type__c,columnDetail.datatype__c
                                                                      ,columnDetail.tb_col_nm__c);
                               lstWrapper.add(wrapper);
                           }
                         
                       }
                    }
                }
          } 
        catch(Exception ex){
             CPGUtility.InsertErrorLog(ex.getMessage(),ex.getStackTraceString());
        }
        system.debug('AggregatedFields data'+lstWrapper);
        return lstWrapper;
        
    }
    
    public class FieldWrapper{
         @AuraEnabled
        public string fieldName{get;set;}
         @AuraEnabled
        public string variableType{get;set;}
        @AuraEnabled
        public string valueType{get;set;}
        @AuraEnabled
        public string dataType{get;set;}
        @AuraEnabled
        public string tblColName{get;set;}
        
         public FieldWrapper(string fieldName,string variableType,string valueType,string dataType,string tblColName){
            this.fieldName = fieldName;
            this.variableType = variableType;
            this.valueType = valueType; 
            this.dataType=dataType; 
            this.tblColName=tblColName; 
           }
    }
    
    /**
     * saving aggregation details
     *  
     *  @author  Aakash Gupta
     *  @version 1.0
     *  @since   2017-10-12  
    */
    
   /* @AuraEnabled 
    public static string saveAgg(string strJsonBusinessRules,string strJsonAggrRuleDetails,string strJsonRuleLevelDetails){
        system.debug('--controller');
        return 'Yes';
    }*/
    
   
    
    @AuraEnabled
    public static CPGConstants.Response saveAgg(string strJsonBusinessRules,string strJsonAggrRuleDetails
                                                ,string strJsonRuleLevelDetails,string strJsonBRFMDetails){
      system.debug('--controller');
        CPGConstants.Response objResponse=new CPGConstants.Response();
        Savepoint sp = Database.setSavepoint();   
        try{
             
             Business_Rules__c objBusinessRule=(Business_Rules__c) JSON.deserialize(strJsonBusinessRules, Business_Rules__c.class);  
             List<Aggregate_Rule_Detail__c> lstAggregateRuleDetails=(List<Aggregate_Rule_Detail__c>) JSON.deserialize(strJsonAggrRuleDetails, List<Aggregate_Rule_Detail__c>.class);
             List<Aggregate_Rule_Level__c> lstAggregateRuleLevel=(List<Aggregate_Rule_Level__c>) JSON.deserialize(strJsonRuleLevelDetails, List<Aggregate_Rule_Level__c>.class);
             List<Business_Rule_FieldMap_Details__c> lstBusinessRuleFieldMapDetails=(List<Business_Rule_FieldMap_Details__c>) JSON.deserialize(strJsonBRFMDetails, List<Business_Rule_FieldMap_Details__c>.class);
            
             System.debug('objBusinessRule'+objBusinessRule);     
             System.debug('lstAggregateRuleDetails'+lstAggregateRuleDetails);
             System.debug('lstAggregateRuleLevel'+lstAggregateRuleLevel);
            System.debug('lstBusinessRuleFieldMapDetails'+lstBusinessRuleFieldMapDetails);
             List<Business_Rule_Type_Master__c> lstRuleTypeMaster= [Select Id FROM Business_Rule_Type_Master__c where Name='Aggregate'];
              objBusinessRule.Business_Rule_Type_Master__c=lstRuleTypeMaster[0].Id;
              objBusinessRule.Status__c  = 'Ready';
                 insert  objBusinessRule;  
                 system.debug('BusinessRuleID'+objBusinessRule.Id);
              for(Business_Rule_FieldMap_Details__c busRuleFieldMapDetails: lstBusinessRuleFieldMapDetails){ 
                     busRuleFieldMapDetails.Business_Rule__c=  objBusinessRule.Id;
                     insert busRuleFieldMapDetails;
                } 
                List<Data_Set_Column_Detail__c> aggDetailDSColList = new List<Data_Set_Column_Detail__c>();
               for(Aggregate_Rule_Detail__c aggRuleDetails: lstAggregateRuleDetails)
               {
                     aggRuleDetails.Business_Rules__c=  objBusinessRule.Id;
                     insert aggRuleDetails;
                   
                      List<Data_Set_Rule_Map__c> lstDataSetRuleMap=new List<Data_Set_Rule_Map__c>();
                    lstDataSetRuleMap=[select Id,dataset_id__c from Data_Set_Rule_Map__c 
                                       where scenario_rule_instance_id__c= :objBusinessRule.ScenarioRuleInstanceDetails__c
                                       and ds_type__c='O']; 
                    if(lstDataSetRuleMap!=null && lstDataSetRuleMap.size()>0)
                    {
                      string dataSetId=lstDataSetRuleMap[0].dataset_id__c;
                      Data_Set_Column_Detail__c dataSetColumnDetail=new Data_Set_Column_Detail__c();
                      dataSetColumnDetail.dataset_id__c=dataSetId;
                      dataSetColumnDetail.ds_col_name__c=aggRuleDetails.Aggregate_Field__c;
                      dataSetColumnDetail.tb_col_nm__c=aggRuleDetails.Aggregate_Field__c.replace(' ','_').replace('__c','').replace('#','total').replace('.','_').toLowerCase();  
                      dataSetColumnDetail.datatype__c=aggRuleDetails.Data_Type__c;
                      dataSetColumnDetail.Table_Data_Type__c = aggRuleDetails.Data_Type__c.toLowerCase();
                      dataSetColumnDetail.value_type__c=aggRuleDetails.Value_Type__c;
                      dataSetColumnDetail.variable_type__c=CPGConstants.DERIVED;
                      system.debug(dataSetColumnDetail);  
                      aggDetailDSColList.add(dataSetColumnDetail); 
                  }
                }
                insert aggDetailDSColList; 
               string aggrLevel='';

               List<Data_Set_Column_Detail__c> aggLevelDSColList = new List<Data_Set_Column_Detail__c>();
               List<Data_Set_Rule_Map__c> lstDataSetRuleMap12 = new List<Data_Set_Rule_Map__c>();
                lstDataSetRuleMap12 = [select Id,dataset_id__c from Data_Set_Rule_Map__c where scenario_rule_instance_id__c = :objBusinessRule.ScenarioRuleInstanceDetails__c and ds_type__c = 'I'];
                Map<String,String> sourceColumnMap = getSourceColumnMap(lstDataSetRuleMap12[0].dataset_id__c);
                Map<String,Data_Set_Column_Detail__c> datatypeValue = new Map<String,Data_Set_Column_Detail__c>();
                datatypeValue = getDataTypeMap(lstDataSetRuleMap12[0].dataset_id__c);

              for(Aggregate_Rule_Level__c aggRuleLevel: lstAggregateRuleLevel){
                     aggRuleLevel.Business_Rules__c=  objBusinessRule.Id;
                  if(aggrLevel==''){
                      aggrLevel=aggRuleLevel.group_field__c;
                  }
                  else{
                     aggrLevel=aggrLevel+'-'+aggRuleLevel.group_field__c;
                  }  
                     insert aggRuleLevel;
                  /*String datatypeval;
                  string valueType;
                  List<Data_Set_Column_Detail__c> datatypeValue = new List<Data_Set_Column_Detail__c>();
                  datatypeValue = getDataType(aggRuleLevel.group_field__c);
                  if(datatypeValue!=null && datatypeValue.size()>0){
                          datatypeval=datatypeValue[0].datatype__c;
                          valueType = datatypeValue[0].value_type__c;
                      system.debug('datatypeValue====>'+datatypeval);}*/
                  
                  String datatypeval;
                  string valueType;
                   // List<Data_Set_Rule_Map__c> lstDataSetRuleMap12=new List<Data_Set_Rule_Map__c>();
                   //  lstDataSetRuleMap12=[select Id,dataset_id__c from Data_Set_Rule_Map__c 
                   //                     where scenario_rule_instance_id__c= :objBusinessRule.ScenarioRuleInstanceDetails__c
                   //                     and ds_type__c='I']; 
                  
                  if(datatypeValue!=null && datatypeValue.size()>0)
                  {
                      if(datatypeValue.get(aggRuleLevel.group_field__c) != null)
                      {
                        datatypeval=datatypeValue.get(aggRuleLevel.group_field__c).datatype__c;
                        valueType = datatypeValue.get(aggRuleLevel.group_field__c).value_type__c;
                      }  
                      system.debug('datatypeValue====>'+datatypeval);
                  }
                  
                  List<Data_Set_Rule_Map__c> lstDataSetRuleMap=new List<Data_Set_Rule_Map__c>();
                    lstDataSetRuleMap=[select Id,dataset_id__c from Data_Set_Rule_Map__c 
                                       where scenario_rule_instance_id__c= :objBusinessRule.ScenarioRuleInstanceDetails__c
                                       and ds_type__c='O']; 
                      if(lstDataSetRuleMap!=null && lstDataSetRuleMap.size()>0){
                        string dataSetId=lstDataSetRuleMap[0].dataset_id__c;
                        Data_Set_Column_Detail__c dataSetColumnDetail=new Data_Set_Column_Detail__c();
                        dataSetColumnDetail.dataset_id__c=dataSetId;
                        dataSetColumnDetail.ds_col_name__c=aggRuleLevel.group_field__c;
                        dataSetColumnDetail.source_column__c = sourceColumnMap.get(aggRuleLevel.tb_col_nm__c);
                        //dataSetColumnDetail.tb_col_nm__c=aggRuleLevel.group_field__c.replace(' ','_').replace('#','total').replace('.','_').toLowerCase();  
                        dataSetColumnDetail.tb_col_nm__c = aggRuleLevel.tb_col_nm__c;
                        dataSetColumnDetail.datatype__c=datatypeval;
                        dataSetColumnDetail.Table_Data_Type__c = datatypeval.toLowerCase();
                        dataSetColumnDetail.value_type__c= valueType;
                        //dataSetColumnDetail.variable_type__c=CPGConstants.DERIVED;
                        dataSetColumnDetail.variable_type__c= aggRuleLevel.variable_type__c;
                        system.debug(dataSetColumnDetail);  
                        aggLevelDSColList.add(dataSetColumnDetail); 
                    } 
                  
                }
                insert aggLevelDSColList;
              system.debug('aggrLevel-->'+aggrLevel);
            // objBusinessRule.Aggregate_Level__c=aggrLevel;
             List<Scenario_Rule_Instance_Details__c> lstSceInstance=[select Scenario_Id__c from Scenario_Rule_Instance_Details__c
                                                                     where Id= :objBusinessRule.ScenarioRuleInstanceDetails__c];
             if(lstSceInstance!=null && lstSceInstance.size()>0){
                 objBusinessRule.Scenario__c=lstSceInstance[0].Scenario_Id__c;
                 objBusinessRule.Status__c  = 'Ready';
             }
             update objBusinessRule;
             objResponse.status=CPGConstants.SUCCESS;
             objResponse.msg=CPGConstants.SAVEDMSG;
        }
        catch(Exception ex){
            System.debug('exception-->'+ex.getStackTraceString()+'~^'+ex.getMessage());
            Database.rollback(sp);
            CPGUtility.InsertErrorLog(ex.getMessage(),ex.getStackTraceString());
            objResponse.status=CPGConstants.ERROR;
            objResponse.msg=ex.getMessage();
        }
        return objResponse;                                    
    }
    
    
    @AuraEnabled
    public static List<AggregationInfoWrapper> GetAggregationType(string businessRuleId){
        
        system.debug('inside GetAggregationType apex method');
        List<Business_Rules__c> lstBusinessRules=new List<Business_Rules__c>();
        lstBusinessRules=[select Id,Name,Business_Rule_Type_Master__c from Business_Rules__c where id= :businessRuleId];
        List<Aggregate_Rule_Detail__c> lstAggregateDetails = [select Id,Seq_No__c, Field_Name__c,Aggregate_Field__c,Aggregate_Function__c,Data_Type__c,Value_Type__c from Aggregate_Rule_Detail__c where Business_Rules__c IN : 
                                            lstBusinessRules];
        
        List<Aggregate_Rule_Level__c> lstAggregateRuleLevel = [select Id,Display_Seq_No__c,group_field__c,Is_selected__c from Aggregate_Rule_Level__c where Business_Rules__c IN : 
                                            lstBusinessRules];
         
       /* List<Business_Rule_FieldMap_Details__c> lstBusinessRuleFieldMap = [select Id,IO_flag__c,Name,Param_type__c from Business_Rule_FieldMap_Details__c where Business_Rule__c IN : 
                                                                            lstBusinessRules];*/
 
        List<AggregationInfoWrapper> lstAggregationInfoWrapper = new List<AggregationInfoWrapper>();
        lstAggregationInfoWrapper.add(new AggregationInfoWrapper(lstBusinessRules,lstAggregateDetails,lstAggregateRuleLevel ));
        system.debug(lstAggregationInfoWrapper);
        return lstAggregationInfoWrapper;
    }

    public class AggregationInfoWrapper{
        @AuraEnabled
        public List<Business_Rules__c> lstWbusinessRules{get;set;}
        @AuraEnabled
        public List<Aggregate_Rule_Detail__c> lstWAggregateDetails{get;set;}
        @AuraEnabled
        public List<Aggregate_Rule_Level__c> lstWAggregateRuleLevel{get;set;}
       /* @AuraEnabled
        public List<Business_Rule_FieldMap_Details__c> lstWbusinessRuleTypeMaster{get;set;}*/
       
        
        public AggregationInfoWrapper(List<Business_Rules__c> lstWbusinessRules, List<Aggregate_Rule_Detail__c> lstWAggregateDetails,List<Aggregate_Rule_Level__c> lstWAggregateRuleLevel ){
            this.lstWbusinessRules = lstWbusinessRules;
            this.lstWAggregateDetails = lstWAggregateDetails;
            this.lstWAggregateRuleLevel = lstWAggregateRuleLevel;
             
        }
    }
    
     @AuraEnabled
    public static CPGConstants.Response updateAgg(string strJsonBusinessRules,string strJsonAggrRuleDetails
                                                            ,string strJsonRuleLevelDetails,string strJsonBRFMDetails){
      system.debug('--controllerUpdate');
        CPGConstants.Response objResponse=new CPGConstants.Response();
        Savepoint sp = Database.setSavepoint();                                                        
        try{
            system.debug(strJsonBusinessRules);
            system.debug(strJsonAggrRuleDetails);
            system.debug(strJsonRuleLevelDetails);
            system.debug(strJsonBRFMDetails);
            Business_Rules__c objBusinessRule=(Business_Rules__c) JSON.deserialize(strJsonBusinessRules, Business_Rules__c.class);  
            List<Aggregate_Rule_Detail__c> lstAggregateRuleDetails=(List<Aggregate_Rule_Detail__c>) JSON.deserialize(strJsonAggrRuleDetails, List<Aggregate_Rule_Detail__c>.class);
            List<Aggregate_Rule_Level__c> lstAggregateRuleLevel=(List<Aggregate_Rule_Level__c>) JSON.deserialize(strJsonRuleLevelDetails, List<Aggregate_Rule_Level__c>.class);
            List<Business_Rule_FieldMap_Details__c> lstBusinessRuleFieldMapDetails=(List<Business_Rule_FieldMap_Details__c>) JSON.deserialize(strJsonBRFMDetails, List<Business_Rule_FieldMap_Details__c>.class);
            
            String businessRuleTypeID = objBusinessRule.Id;
            List<AggregationInfoWrapper> lstAggregationInfoWrapper=new List<AggregationInfoWrapper>();
            lstAggregationInfoWrapper = GetAggregationType(businessRuleTypeID);
            //system.debug(lstAggregationInfoWrapper[0].lstWAggregateDetails[0].Aggregate_Field__c);
            
            delete[select id from Aggregate_Rule_Detail__c where Business_Rules__c= :objBusinessRule.Id] ; 
            delete[select id from Aggregate_Rule_Level__c where Business_Rules__c =:objBusinessRule.Id] ; 
            delete[select id from Business_Rule_FieldMap_Details__c where Business_Rule__c =:objBusinessRule.Id] ; 
            
            
            
              List<Business_Rule_Type_Master__c> lstRuleTypeMaster= [Select Id FROM Business_Rule_Type_Master__c where Name='Aggregate'];
              objBusinessRule.Business_Rule_Type_Master__c=lstRuleTypeMaster[0].Id;
              update  objBusinessRule;
            
              system.debug('BUSINESS RULE ID=='+objBusinessRule.Id);
              List<Data_Set_Rule_Map__c> lstDataSetRuleMap=new List<Data_Set_Rule_Map__c>();
                      lstDataSetRuleMap=[select Id,dataset_id__c from Data_Set_Rule_Map__c 
                                 where scenario_rule_instance_id__c= :objBusinessRule.ScenarioRuleInstanceDetails__c
                                  and  ds_type__c='O' ]; 
              string dataSetId=lstDataSetRuleMap[0].dataset_id__c;
            system.debug('dataSetId===>'+dataSetId);
             system.debug('lstAggregationInfoWrapper[0].lstWAggregateDetails===>'+lstAggregationInfoWrapper[0].lstWAggregateDetails);
            system.debug('lstAggregationInfoWrapper[0].lstWAggregateRuleLevel===>'+lstAggregationInfoWrapper[0].lstWAggregateRuleLevel); 
            List<Data_Set_Column_Detail__c> toBeDeletedList = new List<Data_Set_Column_Detail__c>();  
              Set<string> toBeDeletedIds=new Set<string>();
              for(Aggregate_Rule_Detail__c aggrDetail:lstAggregationInfoWrapper[0].lstWAggregateDetails){
                    List<Data_Set_Column_Detail__c> lst=[select Id from Data_Set_Column_Detail__c where dataset_id__c= :dataSetId 
                           and ds_col_name__c = :aggrDetail.Aggregate_Field__c];
                  system.debug('LIST TO BE DELETED'+lst);
                  if(lst!=null && lst.size()>0 ){
                      //toBeDeletedList.addAll(lst);  
                      toBeDeletedIds.add(lst[0].Id);
                          system.debug('List to be deleted'+toBeDeletedList);
                  }
                     
                }
             for(Aggregate_Rule_Level__c aggrLevel:lstAggregationInfoWrapper[0].lstWAggregateRuleLevel){
                    List<Data_Set_Column_Detail__c> lst1=[select Id from Data_Set_Column_Detail__c where dataset_id__c= :dataSetId 
                           and ds_col_name__c = :aggrLevel.group_field__c];
                  system.debug('LIST TO BE DELETED'+lst1);
                  if(lst1!=null && lst1.size()>0 ){
                      //toBeDeletedList.addAll(lst);  
                      toBeDeletedIds.add(lst1[0].Id);
                          system.debug('List to be deleted'+toBeDeletedList);
                  }
                     
                }
             
              system.debug('List to be deleted 2'+toBeDeletedList);
                delete [select Id from Data_Set_Column_Detail__c where id in :toBeDeletedIds];
            
            List<Data_Set_Column_Detail__c> addDetailDSColList = new List<Data_Set_Column_Detail__c>();
            
             for(Aggregate_Rule_Detail__c aggRuleDetails: lstAggregateRuleDetails)
             {
                    Data_Set_Column_Detail__c dataSetColumnDetail=new Data_Set_Column_Detail__c();
                     aggRuleDetails.Business_Rules__c=  objBusinessRule.Id;
                     insert aggRuleDetails;
                  
                   //Updating Data set column detail
                     
                                      
                      System.debug('Queried Ids inside update method'+lstDataSetRuleMap);                 
                      if(lstDataSetRuleMap!=null && lstDataSetRuleMap.size()>0){
                        
                         
                        
                       
                   /* Data_Set_Column_Detail__c dataSetColumnDetail=[select Id from Data_Set_Column_Detail__c 
                                                                       where dataset_id__c= :dataSetId limit 1];*/
                    //Data_Set_Column_Detail__c dataSetColumnDetail=new  Data_Set_Column_Detail__c();  
                    // 
                       // Data_Set_Column_Detail__c dataSetColumnDetail=new Data_Set_Column_Detail__c();
                        dataSetColumnDetail.dataset_id__c=dataSetId;
                        dataSetColumnDetail.ds_col_name__c=aggRuleDetails.Aggregate_Field__c;
                        dataSetColumnDetail.tb_col_nm__c=aggRuleDetails.Aggregate_Field__c.replace(' ','_').replace('__c','').replace('#','total').replace('.','_').toLowerCase();  
                        dataSetColumnDetail.datatype__c=aggRuleDetails.Data_Type__c;
                        dataSetColumnDetail.value_type__c=aggRuleDetails.Value_Type__c;
                        dataSetColumnDetail.Table_Data_Type__c = aggRuleDetails.Data_Type__c.toLowerCase();
                        dataSetColumnDetail.variable_type__c=CPGConstants.DERIVED;
                         addDetailDSColList.add(dataSetColumnDetail); 
                    }
                     
                }
               // insert  dataSetColumnDetail;
                insert addDetailDSColList;
              string aggrLevel='';
            
            // Data_Set_Column_Detail__c dataSetColumnDetail1=new Data_Set_Column_Detail__c();
              List<Data_Set_Rule_Map__c> lstDataSetRuleMap12 = new List<Data_Set_Rule_Map__c>();
                lstDataSetRuleMap12 = [select Id,dataset_id__c from Data_Set_Rule_Map__c where scenario_rule_instance_id__c = :objBusinessRule.ScenarioRuleInstanceDetails__c and ds_type__c = 'I'];
                Map<String,String> sourceColumnMap = getSourceColumnMap(lstDataSetRuleMap12[0].dataset_id__c);
                Map<String,Data_Set_Column_Detail__c> datatypeValue = new Map<String,Data_Set_Column_Detail__c>();
                datatypeValue = getDataTypeMap(lstDataSetRuleMap12[0].dataset_id__c);

                List<Data_Set_Column_Detail__c> aggLevelDSColList = new List<Data_Set_Column_Detail__c>();

                List<Data_Set_Rule_Map__c> lstDataSetRuleMap123=new List<Data_Set_Rule_Map__c>();
                    lstDataSetRuleMap123=[select Id,dataset_id__c from Data_Set_Rule_Map__c 
                                       where scenario_rule_instance_id__c= :objBusinessRule.ScenarioRuleInstanceDetails__c
                                       and ds_type__c='O'];  
            
            for(Aggregate_Rule_Level__c aggRuleLevel: lstAggregateRuleLevel)
            {
                Data_Set_Column_Detail__c dataSetColumnDetail1=new Data_Set_Column_Detail__c();
                aggRuleLevel.Business_Rules__c=  objBusinessRule.Id;
                if(aggrLevel == '')
                {
                    aggrLevel = aggRuleLevel.group_field__c;
                }
                else
                {
                    aggrLevel=aggrLevel+'-'+aggRuleLevel.group_field__c;
                }  
                //To be inserted after for loop
                insert aggRuleLevel;
                
                String datatypeval;
                String valueType;
                
                 
                
                //List<Data_Set_Column_Detail__c> datatypeValue = new List<Data_Set_Column_Detail__c>();
                //datatypeValue = getDataType(aggRuleLevel.group_field__c,lstDataSetRuleMap12[0].dataset_id__c);
                
                // if(datatypeValue != null && datatypeValue.size()>0)
                // {
	               //  datatypeval=datatypeValue[0].datatype__c;
	               //  valueType = datatypeValue[0].value_type__c;
                //     system.debug('datatypeValue====>'+datatypeval);
                // }
                
                if(datatypeValue!=null && datatypeValue.size()>0)
                {
                    if(datatypeValue.get(aggRuleLevel.group_field__c) != null)
                    {
                      datatypeval=datatypeValue.get(aggRuleLevel.group_field__c).datatype__c;
                      valueType = datatypeValue.get(aggRuleLevel.group_field__c).value_type__c;
                    }
                    
                    system.debug('datatypeValue====>'+datatypeval);
                }
                  
                 
                  
                  
                  
                if(lstDataSetRuleMap123 !=null && lstDataSetRuleMap123.size()>0)
                {
                    string dataSetId123=lstDataSetRuleMap[0].dataset_id__c;
                    // Data_Set_Column_Detail__c dataSetColumnDetail=new Data_Set_Column_Detail__c();
                    dataSetColumnDetail1.dataset_id__c=dataSetId123;
                    dataSetColumnDetail1.ds_col_name__c=aggRuleLevel.group_field__c;
                    //dataSetColumnDetail.tb_col_nm__c=aggRuleLevel.group_field__c.replace(' ','_').replace('#','total').replace('.','_').toLowerCase();  
                    dataSetColumnDetail1.tb_col_nm__c=aggRuleLevel.tb_col_nm__c;
                    dataSetColumnDetail1.source_column__c = sourceColumnMap.get(aggRuleLevel.tb_col_nm__c);
                    dataSetColumnDetail1.datatype__c=datatypeval;
                    dataSetColumnDetail1.Table_Data_Type__c = datatypeval.toLowerCase();
                    dataSetColumnDetail1.value_type__c= valueType;
                    dataSetColumnDetail1.variable_type__c=aggRuleLevel.variable_type__c;
                    system.debug(dataSetColumnDetail1);  
                     aggLevelDSColList.add(dataSetColumnDetail1); 
                } 
                  
            }
            
        	// insert  dataSetColumnDetail1;
          insert aggLevelDSColList;
         	system.debug('aggrLevel-->'+aggrLevel);
         	//objBusinessRule.Aggregate_Level__c=aggrLevel;
         
         	List<Scenario_Rule_Instance_Details__c> lstSceInstance=[select Scenario_Id__c from Scenario_Rule_Instance_Details__c
                                                                 where Id= :objBusinessRule.ScenarioRuleInstanceDetails__c];
         	system.debug('lstSceInstance[0].Scenario_Id__c-->'+lstSceInstance[0].Scenario_Id__c); 
        	if(lstSceInstance!=null && lstSceInstance.size()>0)
        	{
             	objBusinessRule.Scenario__c=lstSceInstance[0].Scenario_Id__c;
         	}
        	update objBusinessRule;
          	
          	for(Business_Rule_FieldMap_Details__c busRuleFieldMapDetails: lstBusinessRuleFieldMapDetails)
          	{
                busRuleFieldMapDetails.Business_Rule__c=  objBusinessRule.Id;
                insert busRuleFieldMapDetails;
            }
        	objResponse.status=CPGConstants.SUCCESS;
         	objResponse.msg=CPGConstants.SAVEDMSG;
        }
        catch(Exception ex){
            System.debug('exception-->'+ex.getStackTraceString()+'~^'+ex.getMessage());
            Database.rollback(sp);
            CPGUtility.InsertErrorLog(ex.getMessage(),ex.getStackTraceString());
            objResponse.status=CPGConstants.ERROR;
            objResponse.msg=ex.getMessage();
        }
        return objResponse;                                    
    }
    @AuraEnabled
    public static string getBusinessRuleId(String scenarioRuleInstanceId){
        system.debug('getBusinessRuleId-->');
        list<Business_Rules__c> lstRules = [select id from Business_Rules__c where ScenarioRuleInstanceDetails__c =:scenarioRuleInstanceId];
        system.debug('lstRules-->'+lstRules);
        return lstRules[0].Id;
    }
     @AuraEnabled
    public static List<Scenario_Rule_Instance_Details__c> getScenarioID(String ScenarioRuleInstanceId){
     
       List<Scenario_Rule_Instance_Details__c> sceID = [SELECT Id, Scenario_Id__c FROM Scenario_Rule_Instance_Details__c where id=:ScenarioRuleInstanceId];
       return sceID;
    }
    //  @AuraEnabled
    // public static List<Data_Set_Column_Detail__c> getDataType(String fieldName ,String datasetid){
    //     system.debug('parameter=='+fieldName);
    //     List<Data_Set_Column_Detail__c> dataTypeValue = [Select datatype__c,value_type__c from Data_Set_Column_Detail__c where ds_col_name__c =:fieldName and dataset_id__c=:datasetid];
    //     system.debug(dataTypeValue);
    //     return dataTypeValue;
    // }

    @AuraEnabled
    public static Map<string,Data_Set_Column_Detail__c> getDataTypeMap(String datasetid){
        // system.debug('parameter=='+fieldName);
        List<Data_Set_Column_Detail__c> dataTypeValue = [Select datatype__c,ds_col_name__c,value_type__c from Data_Set_Column_Detail__c where dataset_id__c=:datasetid];
        Map<string,Data_Set_Column_Detail__c> dataTypeMap = new Map<string,Data_Set_Column_Detail__c>();
        for(Data_Set_Column_Detail__c dscd : dataTypeValue)
        {
          dataTypeMap.put(dscd.ds_col_name__c,dscd);
        }
        system.debug(dataTypeValue);
        return dataTypeMap;
    }

    @AuraEnabled
    public static Map<String,String> getSourceColumnMap(String datasetid){
        List<Data_Set_Column_Detail__c> dataTypeValue = [Select source_column__c,tb_col_nm__c from Data_Set_Column_Detail__c where dataset_id__c=:datasetid];
        system.debug(dataTypeValue);
        Map<String,String> sourceColumnMap = new Map<String,String>();
        for(Data_Set_Column_Detail__c dscolD : dataTypeValue)
        {
          sourceColumnMap.put(dscolD.tb_col_nm__c,dscolD.source_column__c);
        }
        return sourceColumnMap;
    }
   
}