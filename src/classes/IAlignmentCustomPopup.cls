global Interface IAlignmentCustomPopup{
     map<string,string> opencustomModalOnChangeImpact(string selectZipData,string selectAccountData,string teamInstanceId);
     map<string,string> opencustomModalOnMapView(string selectZipData,string selectAccountData,string teamInstanceId);
     map<string,string> opencustomModalOnTableView(string selectZipData,string selectAccountData,string teamInstanceId);
     map<string,string> opencustomModalOnPositionUniverse(List<string> selectedRecord,string selectedTeamInstance, string moduleName);
     map<string,string> opencustomModalOnEmployeeUniverse(List<string> selectedRecord,string selectedTeamInstance, string moduleName);
     map<string,string> opencustomModalOnWorkspaceUniverse(List<string> selectedRecord,string selectedTeamInstance, string moduleName);
}