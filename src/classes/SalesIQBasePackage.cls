/************************************************************************************************
    Name        :   SalesIQBasePackage.cls
    Auther      :   Arun Kumar
    Date        :   27 Dec 2017
    Description :   Controller for implemnetation team who wants to extend their functionalty 
                    Develper team has to create seprate Interfacce for seprate seprate functionality
                    becase once the interfaces defind, you can't add new method,  becasuse whereevr that interface is usse
                    we need to implement newly created method.
************************************************************************************************/
global with sharing class SalesIQBasePackage{
    global interface ICustomMetricExtension{        
        string getCustomMetricdata(string source, string destination, string teamInstance, list<string> impactedZIPs, string countryId, string crStatus,Change_Request__c crRequest,string movementType,string pageName);       
    }    
     
}