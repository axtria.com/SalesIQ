public class CPGDeleteDerivedFieldCtr {
    public static List<Business_Rule_FieldMap_Details__c> BRFMapList = new List<Business_Rule_FieldMap_Details__c>(); 
      /**
     * deleting category field on basis of BusinessRule Id
     *  
     *  @author  Aakash Gupta
     *  @version 1.0
     *  @since   2017-09-20  
    */
    @AuraEnabled
    public static CPGConstants.Response DeleteCategoryBusinessRule(string businessRuleId,string scenarioRuleInstanceDetailsId,string columnName){
       CPGConstants.Response objResponse=new CPGConstants.Response();
        if(!CPGUtility.ContainsReference(businessRuleId, columnName, scenarioRuleInstanceDetailsId)){
             System.debug('inside category field delete apex method');
            list<Category_Derived_Variable_Metadata__c> lstMetaData=[select id from Category_Derived_Variable_Metadata__c 
                                                       where Business_Rule_Id__c =: businessRuleId];
            delete lstMetaData;
        
        
            list<Business_Rule_FieldMap_Details__c> lstFieldMap=[select id from Business_Rule_FieldMap_Details__c
                                                     where Business_Rule__c =: businessRuleId];
	        delete lstFieldMap;
            DeleteFromDataSetColumnDetails(scenarioRuleInstanceDetailsId,columnName);
	                
	        list<Category_Rule_Entity_Detail__c> lstRuleEntity = [select id,category_type__c from Category_Rule_Entity_Detail__c
	                                                              where Business_Rule__c =: businessRuleId];
	        Set<id> setBlock = new Set<id>();
	        Set<id> setGroup = new Set<id>();
	        for(Category_Rule_Entity_Detail__c blockEntity : lstRuleEntity){
	            if(blockEntity.category_type__c == 'Block')
	                setBlock.add(blockEntity.id);
	            else
	                setGroup.add(blockEntity.id);
	        }    
	        list<Category_Rule_Criteria_Detail__c> lstGrpEntity = [select id from Category_Rule_Criteria_Detail__c
	                                                               where Category_Rule_Entity_Detail__c in: setGroup];        
	        delete lstGrpEntity;
	        
	        
	        
	        list<Category_Rule_Output_Detail__c> lstOutPutDetail= [select id from Category_Rule_Output_Detail__c 
	                                                      where Category_Rule_Entity_Detail__c in: setBlock];
	        delete lstOutPutDetail;
	        
	        delete lstRuleEntity;
	        
	        Business_rules__c rule= [select id from Business_rules__c
	        where id=:businessRuleId limit 1];
	        delete rule;
            
            DeleteFromFilterOutputDataSet(scenarioRuleInstanceDetailsId,columnName);
            objResponse.status=CPGConstants.SUCCESS;
            objResponse.msg=CPGConstants.SAVEDMSG;
            objResponse.lstData = CPGUtility.BRFMList;
        }
        else{
                objResponse.status=CPGConstants.SUCCESS;
                objResponse.msg=CPGConstants.SAVEDMSG;
                objResponse.lstData = CPGUtility.BRFMList; 
        }
        return objResponse;   
                                                          
                                                                 
    }
    
      /**
     * deleting computation field on basis of BusinessRule Id and scenarioRuleInstanceID
     *  
     *  @author  Aakash Gupta
     *  @version 1.0
     *  @since   2017-09-22  
    */
    
     @AuraEnabled
    public static CPGConstants.Response DeleteComputationType(string businessRuleId,string scenarioRuleInstanceDetailsId,string columnName){
        CPGConstants.Response objResponse=new CPGConstants.Response();
        system.debug('businessRuleId-->'+businessRuleId);
        system.debug('scenarioRuleInstanceDetailsId-->'+scenarioRuleInstanceDetailsId);
        system.debug('columnName-->'+columnName);
        Savepoint sp = Database.setSavepoint();  
        try{
             if(!CPGUtility.ContainsReference(businessRuleId,columnName,scenarioRuleInstanceDetailsId)){
                 
           		DeleteFromDataSetColumnDetails(scenarioRuleInstanceDetailsId,columnName);
                delete[select Id from Business_Rule_FieldMap_Details__c where business_rule__c= :businessRuleId];
	            
                 delete[select Id from Computation_Rule_Detail__c where business_rules__c= :businessRuleId];
                 delete[select Id from Business_Rules__c where Id= :businessRuleId]; 
                 
                 DeleteFromFilterOutputDataSet(scenarioRuleInstanceDetailsId,columnName);
                 objResponse.status=CPGConstants.SUCCESS;
                 objResponse.msg=CPGConstants.SAVEDMSG;
                 objResponse.lstData = CPGUtility.BRFMList;
           		 	
           		 }else{
                  objResponse.status=CPGConstants.SUCCESS;
                  objResponse.msg=CPGConstants.SAVEDMSG;
                  objResponse.lstData =CPgUtility.BRFMList;
           	  }
             
           
             
            
        }
        catch(Exception ex){
            system.debug('ex.getMessage()--->'+ex.getMessage());
            Database.rollback(sp);
            CPGUtility.InsertErrorLog(ex.getMessage(),ex.getStackTraceString());
            objResponse.status=CPGConstants.ERROR;
            objResponse.msg=CPGConstants.ERRORMSG;
               
        }
        return  objResponse;
        
    }


      /**
     * Deleting Rank field on basis of BusinessRule Id and scenarioRuleInstanceID
     *  
     *  @author  Pooja Dhiman
     *  @version 1.0
     *  @since   2017-09-27  
    */
    
     @AuraEnabled
    public static CPGConstants.Response deleteRankFields(string businessRuleId,string scenarioRuleInstanceDetailsId,string columnName){
        CPGConstants.Response objResponse=new CPGConstants.Response();
        Savepoint sp = Database.setSavepoint();  
        try{
           if(!CPGUtility.ContainsReference(businessRuleId,columnName,scenarioRuleInstanceDetailsId)){
            delete[select Id from Business_Rule_FieldMap_Details__c where business_rule__c= :businessRuleId];
            DeleteFromDataSetColumnDetails(scenarioRuleInstanceDetailsId,columnName);   
            List<Data_Set_Rule_Map__c> lstDataSetRuleMap=new List<Data_Set_Rule_Map__c>();
            lstDataSetRuleMap=[select Id,dataset_id__c from Data_Set_Rule_Map__c 
                                  where scenario_rule_instance_id__c= :scenarioRuleInstanceDetailsId]; 
            if(lstDataSetRuleMap!=null && lstDataSetRuleMap.size()>0){
                 string dataSetId=lstDataSetRuleMap[0].dataset_id__c;  
                  List<Rank_Rule__c> lstRankRule = new List<Rank_Rule__c>();
                  lstRankRule = [select id,Name,Field_Name__c from Rank_Rule__c where  Business_Rules__c= :businessRuleId];
                  delete[select id from Rank_Partition__c where Rank_Rule__c= :lstRankRule[0].Id] ; 
                  delete[select id from Rank_Order__c where Rank_Rule_id__c =: lstRankRule[0].Id] ; 
                  if(lstRankRule!=null && lstRankRule.size()>0){
                     delete[select Id from Data_Set_Column_Detail__c  where dataset_id__c= :dataSetId and variable_type__c ='Derived'  and ds_col_name__c = :columnName]  ;
               }
                 
            }    
             delete[select Id from Rank_Rule__c where Business_Rules__c= :businessRuleId];
             delete[select Id from Business_Rules__c where Id= :businessRuleId];
            DeleteFromFilterOutputDataSet(scenarioRuleInstanceDetailsId,columnName);   
             objResponse.status=CPGConstants.SUCCESS;
             objResponse.msg=CPGConstants.SAVEDMSG;  
             objResponse.lstData=cpgUtility.BRFMList;  
           }    
            else{
                  objResponse.status=CPGConstants.SUCCESS;
                  objResponse.msg=CPGConstants.SAVEDMSG;
                  objResponse.lstData =CPgUtility.BRFMList;
            } 
        }
        catch(Exception ex){
            Database.rollback(sp);
            CPGUtility.InsertErrorLog(ex.getMessage(),ex.getStackTraceString());
            objResponse.status=CPGConstants.ERROR;
            objResponse.msg=CPGConstants.ERRORMSG;
               
        }
        return  objResponse;
        
    }
    
    /**
     * Deleting Call Balancing Rule on basis of BusinessRule Id and scenarioRuleInstanceID
     *  
     *  @author  Pooja Dhiman
     *  @version 1.0
     *  @since   2017-09-27  
    */
    
     @AuraEnabled
    public static CPGConstants.Response deleteCallBalanceRule(string businessRuleId,string scenarioRuleInstanceDetailsId,string columnName){
        CPGConstants.Response objResponse=new CPGConstants.Response();
        Savepoint sp = Database.setSavepoint();  
        try{
            system.debug('-->>>'+ContainsCallBalanceReference(businessRuleId,scenarioRuleInstanceDetailsId));
            
           if(!ContainsCallBalanceReference(businessRuleId,scenarioRuleInstanceDetailsId)){
            delete[select Id from Business_Rule_FieldMap_Details__c where business_rule__c= :businessRuleId];  
            
                 
            List<Config_Call_Balancing_Map__c> lstConfigCallBalancingMap = new List<Config_Call_Balancing_Map__c>();
            lstConfigCallBalancingMap = [select id from Config_Call_Balancing_Map__c where  Scenario_Rule_Instance_Details__c= :scenarioRuleInstanceDetailsId];
            delete[select Id from Business_Rules__c where Id= :businessRuleId];
             objResponse.status=CPGConstants.SUCCESS;
             objResponse.msg=CPGConstants.SAVEDMSG;  
             objResponse.lstData=new List<Business_Rule_FieldMap_Details__c>();  
           }    
            else{
                  objResponse.status=CPGConstants.SUCCESS;
                  objResponse.msg=CPGConstants.SAVEDMSG;
                  objResponse.lstData =BRFMapList;
            } 
        }
        catch(Exception ex){
            Database.rollback(sp);
            CPGUtility.InsertErrorLog(ex.getMessage(),ex.getStackTraceString());
            objResponse.status=CPGConstants.ERROR;
            objResponse.msg=CPGConstants.ERRORMSG;
               
        }
        return  objResponse;
        
    }
    private static Boolean ContainsCallBalanceReference(string businessRuleId,string scenarioRuleInstanceDetailsId){
        BRFMapList = [select Business_Rule__c,Business_Rule__r.Name,Param_Name__c,IO_Flag__c  from Business_Rule_FieldMap_Details__c 
                    where Business_Rule__c =: businessRuleId  and IO_Flag__c = 'I'];
        List<Config_Call_Balancing_Constraint_RuleMap__c> lstConfigCallBalObj =[SELECT Id FROM Config_Call_Balancing_Constraint_RuleMap__c where Config_Call_Balancing_Constraint_Detail__r.Config_Call_Balancing_Map_Id__r.Scenario_Rule_Instance_Details__c =:scenarioRuleInstanceDetailsId AND business_rules__c=:businessRuleId];
        if(lstConfigCallBalObj!=null && lstConfigCallBalObj.size()>0){
            return true;
        }else{
            return false;
        }
    }
    public static void DeleteFromDataSetColumnDetails(string scenarioRuleInstanceDetailsId,string columnName){
        List<Data_Set_Rule_Map__c> lstDataSetRuleMap=new List<Data_Set_Rule_Map__c>();
	           lstDataSetRuleMap=[select Id,dataset_id__c from Data_Set_Rule_Map__c 
	                                  where scenario_rule_instance_id__c= :scenarioRuleInstanceDetailsId and ds_type__c='I']; 
	           if(lstDataSetRuleMap!=null && lstDataSetRuleMap.size()>0){
	               string dataSetId=lstDataSetRuleMap[0].dataset_id__c;  
	               delete[select Id from Data_Set_Column_Detail__c  where dataset_id__c= :dataSetId and 
                                    variable_type__c ='Derived'  and ds_col_name__c = :columnName]  ;
                       
                 
                }   
        
    }
    
     /**
     * Getting BusinessRules from ID which are not allower for deletion
     *  
     *  @author  Aakash Gupta
     *  @version 1.0
     *  @since   2017-10-06  
    */
    
     @AuraEnabled
    public static List<FieldMapWrapper> deleteComputationErrorRules(string strJsonbusinessRuleIds){
    Set<String> ids = new Set<String>();
    for(string s : strJsonbusinessRuleIds.split(',')){
    	ids.add(s);
    }
    List<Business_Rule_FieldMap_Details__c> BRNames = new List<Business_Rule_FieldMap_Details__c>();
    BRNames =  [select Business_Rule__r.Name  from Business_Rule_FieldMap_Details__c 
	           	            where id IN: ids];
    List<FieldMapWrapper> lstFieldMapWrapper=new List<FieldMapWrapper>();    
        for(Business_Rule_FieldMap_Details__c brName:BRNames)   {
            lstFieldMapWrapper.add(new FieldMapWrapper(brName.Business_Rule__r.Name));
        } 
	system.debug(BRNames.size());       	            
    return lstFieldMapWrapper;
    }
    
    public class FieldMapWrapper{
        @AuraEnabled
        public string brName{get;set;}
        public FieldMapWrapper(string brName){
            this.brName=brName;
        }
    }
    
    
    private static void DeleteFromFilterOutputDataSet(string scenarioInstanceId,string columnName){
        system.debug('deleting--->');
        Scenario_Rule_Instance_Details__c sceInstDetails=[select Id,Exec_Seq__c from Scenario_Rule_Instance_Details__c
                                                         where Id= :scenarioInstanceId];
        
        
        String SCID =[select Scenario_Id__c from Scenario_Rule_Instance_Details__c where id= :scenarioInstanceId][0].Scenario_Id__c;
        
        List<Scenario_Rule_Instance_Details__c> lstSceInstDetails=[select Id,Exec_Seq__c,Component_Type_Master__r.Name from Scenario_Rule_Instance_Details__c
                                                                   where Scenario_Id__c= :SCID and Exec_Seq__c= :(sceInstDetails.Exec_Seq__c
                                                                   +1)];
        system.debug('lstSceInstDetails--->'+lstSceInstDetails);
        if(lstSceInstDetails!=null && lstSceInstDetails.size()>0 ){
            if(lstSceInstDetails[0].Component_Type_Master__r.Name==CPGConstants.FILTER){
                List<Data_Set_Rule_Map__c> lstDatasetRuleMap=[select Id,dataset_id__c from Data_Set_Rule_Map__c 
                                                              where scenario_rule_instance_id__c= :lstSceInstDetails[0].Id 
                                                              and ds_type__c='O'];
                if(lstDatasetRuleMap!=null && lstDatasetRuleMap.size()>0){
                     string dataSetId=lstDataSetRuleMap[0].dataset_id__c;  
	                 delete[select Id from Data_Set_Column_Detail__c  where dataset_id__c= :dataSetId and 
                                    variable_type__c ='Derived'  and ds_col_name__c = :columnName] ;
                    
                }
                
            }
            
        }
        
    }
}