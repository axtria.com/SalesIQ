/*
Author : Ankit Manendra
Description : Test class for CPGDerivedFieldsListCtrl
Date : 2 Novomber, 2017

*/

@isTest
private class TestCPGDerivedFieldsListCtrl{
    static testMethod void cpgDerTest() {
        
        Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj;
        
        Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        insert scenarioObj;
        List<Scenario_Rule_Instance_Details__c> lstScenarioDetails = new List<Scenario_Rule_Instance_Details__c>();
        

        

        Business_Rule_Type_Master__c busRuleTypeMasObj = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj.Name='Rank';
        insert busRuleTypeMasObj;
        
        
        
        Team__c team1 = new Team__c();
        team1.Name = 'OBU';
        insert team1;

        
        ComponentTypeMaster__c ComTypeMasObj = new ComponentTypeMaster__c();
        ComTypeMasObj.Name='Call Balancing';
        ComTypeMasObj.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj.DisplayIcon__c='add';
        ComTypeMasObj.DisplayIconClass__c='window information';
        insert ComTypeMasObj;

        Business_Rule_Template__c  busRuleTemObj = new Business_Rule_Template__c();
        busRuleTemObj.isActive__c=true;
        busRuleTemObj.isInternal__c=true;
        insert busRuleTemObj;
        
        BusinessRule_Template_Details__c busRuleTemDet = new BusinessRule_Template_Details__c();
        busRuleTemDet.is_Visible__c=true;
        busRuleTemDet.Business_Rule_Template_Id__c=busRuleTemObj.id;
        insert busRuleTemDet;
        
        BusinessRule_Template_Details__c busRuleTemDet1 = new BusinessRule_Template_Details__c();
        busRuleTemDet1.is_Visible__c=true;
        busRuleTemDet1.Business_Rule_Template_Id__c=busRuleTemObj.id;
        insert busRuleTemDet1;
        
        Config_Call_Balancing_Map__c callBalObj = new Config_Call_Balancing_Map__c();
        callBalObj.Business_Rule_Template_Details__c=busRuleTemDet.id;
        insert callBalObj;
        
        Config_Call_Balancing_Map__c callBalObj1 = new Config_Call_Balancing_Map__c();
        callBalObj1.Business_Rule_Template_Details__c=busRuleTemDet1.id;
        callBalObj1.Base_Config_Call_Balancing_Map_Id__c=callBalObj.id;
        insert callBalObj1;
                
        Config_Call_Balancing_Constraint_Detail__c configCallBalObj = new Config_Call_Balancing_Constraint_Detail__c();
        configCallBalObj.Config_Call_Balancing_Map_Id__c = callBalObj.id;
        insert configCallBalObj;
         
        
        
        Data_Set__c dataSetObj = new Data_Set__c();
        dataSetObj.Name = 'Call Plan Summary';
        insert dataSetObj;
        
        
        Business_Rule_Type_Master__c busRuleTypeMasObj1 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj1.Name='Category';
        insert busRuleTypeMasObj1;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj2 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj2.Name='Increase Reach';
        insert busRuleTypeMasObj2;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj3 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj3.Name='Decrease Frequency';
        insert busRuleTypeMasObj3;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj4 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj4.Name='Decrease Reach';
        insert busRuleTypeMasObj4;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj5 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj5.Name='Increase Frequency';
        insert busRuleTypeMasObj5;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj6 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj6.Name='Aggregate';
        insert busRuleTypeMasObj6;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj7 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj7.Name='Filter';
        insert busRuleTypeMasObj7;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj8 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj8.Name='Compute';
        insert busRuleTypeMasObj8;
        
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        sceRuleInsDetObj.Component_Display_Order__c=4;
        sceRuleInsDetObj.Is_Visible__c=true;
        sceRuleInsDetObj.Component_Type_Master__c=ComTypeMasObj.id;
        sceRuleInsDetObj.BRT_Details_Id__c=busRuleTemDet.id;
        insert sceRuleInsDetObj;
        lstScenarioDetails.add(sceRuleInsDetObj);
        Data_Set_Rule_Map__c dataSetRuleObj = new Data_Set_Rule_Map__c();
        dataSetRuleObj.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj.ds_type__c='I';
        insert dataSetRuleObj;
        
        Data_Set_Rule_Map__c dataSetRuleObj1 = new Data_Set_Rule_Map__c();
        dataSetRuleObj1.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj1.Name='anc';
        dataSetRuleObj1.ds_type__c='o';
        insert dataSetRuleObj1;

        Business_Rules__c busRuleObjagg = new Business_Rules__c();
        busRuleObjagg.Name='Rank rule 278';
        busRuleObjagg.Business_Rule_Type_Master__c=busRuleTypeMasObj6.id;
        busRuleObjagg.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObjagg.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjagg.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        insert busRuleObjagg;
        
        Business_Rules__c busRuleObjaggregation = new Business_Rules__c();
        busRuleObjaggregation.Name='Rank rule 278';
        busRuleObjaggregation.Business_Rule_Type_Master__c=busRuleTypeMasObj6.id;
        busRuleObjaggregation.ScenarioRuleInstanceDetails__c=null;
        busRuleObjaggregation.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjaggregation.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        busRuleObjaggregation.Parent_Rule_Id__c=busRuleObjagg.id;
        busRuleObjaggregation.Scenario__c=scenarioObj.id;
        insert busRuleObjaggregation;
        
        
        Business_Rules__c busRuleObjdecFreq = new Business_Rules__c();
        busRuleObjdecFreq.Name='Rank rule 278';
        busRuleObjdecFreq.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObjdecFreq.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObjdecFreq.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjdecFreq.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        insert busRuleObjdecFreq;
        
        Business_Rules__c busRuleObjdecFrequency = new Business_Rules__c();
        busRuleObjdecFrequency.Name='Rank rule 278';
        busRuleObjdecFrequency.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObjdecFrequency.ScenarioRuleInstanceDetails__c=null;
        busRuleObjdecFrequency.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjdecFrequency.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        busRuleObjdecFrequency.Parent_Rule_Id__c=busRuleObjdecFreq.id;
        busRuleObjdecFrequency.Scenario__c=scenarioObj.id;
        insert busRuleObjdecFrequency;
        
        Rank_Rule__c rankObj = new Rank_Rule__c();
        rankObj.Business_Rules__c=busRuleObjdecFrequency.id;
        insert rankObj;
    
        Business_Rules__c busRuleObjCom = new Business_Rules__c();
        busRuleObjCom.Name='Rank rule 278';
        busRuleObjCom.Business_Rule_Type_Master__c=busRuleTypeMasObj8.id;
        busRuleObjCom.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObjCom.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        insert busRuleObjCom;
        
        Business_Rules__c busRuleObjComPute = new Business_Rules__c();
        busRuleObjComPute.Name='Rank rule 278';
        busRuleObjComPute.Business_Rule_Type_Master__c=busRuleTypeMasObj8.id;
        busRuleObjComPute.ScenarioRuleInstanceDetails__c=null;
        busRuleObjComPute.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjComPute.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        busRuleObjComPute.Parent_Rule_Id__c=busRuleObjCom.id;
        busRuleObjComPute.Scenario__c=scenarioObj.id;
        insert busRuleObjComPute;
        
        Business_Rules__c busRuleObjDecRea = new Business_Rules__c();
        busRuleObjDecRea.Name='Rank rule 278';
        busRuleObjDecRea.Business_Rule_Type_Master__c=busRuleTypeMasObj4.id;
        busRuleObjDecRea.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObjDecRea.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjDecRea.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        insert busRuleObjDecRea;
        
        Business_Rules__c busRuleObjDecReach = new Business_Rules__c();
        busRuleObjDecReach.Name='Rank rule 278';
        busRuleObjDecReach.Business_Rule_Type_Master__c=busRuleTypeMasObj4.id;
        busRuleObjDecReach.ScenarioRuleInstanceDetails__c=null;
        busRuleObjDecReach.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjDecReach.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        busRuleObjDecReach.Parent_Rule_Id__c=busRuleObjDecRea.id;
        busRuleObjDecReach.Scenario__c=scenarioObj.id;
        insert busRuleObjDecReach;
        
        Business_Rules__c busRuleObjFil = new Business_Rules__c();
        busRuleObjFil.Name='Rank rule 278';
        busRuleObjFil.Business_Rule_Type_Master__c=busRuleTypeMasObj7.id;
        busRuleObjFil.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObjFil.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjFil.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        insert busRuleObjFil;
        
        Business_Rules__c busRuleObjFilter = new Business_Rules__c();
        busRuleObjFilter.Name='Rank rule 278';
        busRuleObjFilter.Business_Rule_Type_Master__c=busRuleTypeMasObj7.id;
        busRuleObjFilter.ScenarioRuleInstanceDetails__c=null;
        busRuleObjFilter.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjFilter.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        busRuleObjFilter.Parent_Rule_Id__c=busRuleObjFil.id;
        busRuleObjFilter.Scenario__c=scenarioObj.id;
        insert busRuleObjFilter;
        
        
        Business_Rules__c busRuleObj2 = new Business_Rules__c();
        busRuleObj2.Name='Rank rule 278';
        busRuleObj2.Business_Rule_Type_Master__c=busRuleTypeMasObj1.id;
        busRuleObj2.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj2.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObj2.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        insert busRuleObj2;
        
        Business_Rules__c busRuleObj3 = new Business_Rules__c();
        busRuleObj3.Name='Rank rule 278';
        busRuleObj3.Business_Rule_Type_Master__c=busRuleTypeMasObj1.id;
        busRuleObj3.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj3.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObj3.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        insert busRuleObj3;
        
        Business_Rules__c busRuleObjCat = new Business_Rules__c();
        busRuleObjCat.Name='Rank rule 278';
        busRuleObjCat.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObjCat.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObjCat.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjCat.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        busRuleObjCat.Parent_Rule_Id__c=busRuleObj3.id;
        busRuleObjCat.Scenario__c=scenarioObj.id;
        insert busRuleObjCat;
        
        Business_Rules__c busRuleObj = new Business_Rules__c();
        busRuleObj.Name='Rank rule 278';
        busRuleObj.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObj.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObj.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        busRuleObj.Parent_Rule_Id__c=busRuleObj2.id;
        busRuleObj.Scenario__c=scenarioObj.id;
        insert busRuleObj;
        
        Business_Rules__c busRuleObj1 = new Business_Rules__c();
        busRuleObj1.Name='Rank rule 278';
        busRuleObj1.Business_Rule_Type_Master__c=busRuleTypeMasObj2.id;
        busRuleObj1.ScenarioRuleInstanceDetails__c=null;
        busRuleObj1.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObj1.Aggregate_Business_Rule_Template_Details__c='Test';
        busRuleObj1.Aggregate_Scenario_Rule_Instance__c=sceRuleInsDetObj.id;
        insert busRuleObj1;
        
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj.Param_Name__c='derived field 278';
        busRuleFieldObj.Business_Rule__c=busRuleObj.id;
        busRuleFieldObj.Param_Data_Type__c='Numeric';
        busRuleFieldObj.Param_Type__c='Derived';
        busRuleFieldObj.IO_Flag__c='O';
        
        insert busRuleFieldObj;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj1 = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj1.Param_Name__c='derived field 278';
        busRuleFieldObj1.Business_Rule__c=busRuleObj2.id;
        busRuleFieldObj1.Param_Data_Type__c='Numeric';
        busRuleFieldObj1.Param_Type__c='Derived';
        busRuleFieldObj1.IO_Flag__c='O';
        
        insert busRuleFieldObj1;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj2 = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj2.Param_Name__c='derived field 278';
        busRuleFieldObj2.Business_Rule__c=busRuleObj1.id;
        busRuleFieldObj2.Param_Data_Type__c='Numeric';
        busRuleFieldObj2.Param_Type__c='Derived';
        busRuleFieldObj2.IO_Flag__c='O';
        
        insert busRuleFieldObj2;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj3 = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj3.Param_Name__c='derived field 278';
        busRuleFieldObj3.Business_Rule__c=busRuleObjComPute.id;
        busRuleFieldObj3.Param_Data_Type__c='Numeric';
        busRuleFieldObj3.Param_Type__c='Derived';
        busRuleFieldObj3.IO_Flag__c='O';
        
        insert busRuleFieldObj3;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj4 = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj4.Param_Name__c='derived field 278';
        busRuleFieldObj4.Business_Rule__c=busRuleObjDecReach.id;
        busRuleFieldObj4.Param_Data_Type__c='Numeric';
        busRuleFieldObj4.Param_Type__c='Derived';
        busRuleFieldObj4.IO_Flag__c='O';
        
        insert busRuleFieldObj4;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj5 = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj5.Param_Name__c='derived field 278';
        busRuleFieldObj5.Business_Rule__c=busRuleObjdecFrequency.id;
        busRuleFieldObj5.Param_Data_Type__c='Numeric';
        busRuleFieldObj5.Param_Type__c='Derived';
        busRuleFieldObj5.IO_Flag__c='O';
        
        insert busRuleFieldObj5;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj6 = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj6.Param_Name__c='derived field 278';
        busRuleFieldObj6.Business_Rule__c=busRuleObjaggregation.id;
        busRuleFieldObj6.Param_Data_Type__c='Numeric';
        busRuleFieldObj6.Param_Type__c='Derived';
        busRuleFieldObj6.IO_Flag__c='O';
        
        insert busRuleFieldObj6;
        
        Aggregate_Rule_Level__c aggLevel = new Aggregate_Rule_Level__c();
        aggLevel.Business_Rules__c=busRuleObjaggregation.id;
        insert aggLevel;
        
        Aggregate_Rule_Detail__c aggRule = new Aggregate_Rule_Detail__c();
        aggRule.Business_Rules__c=busRuleObjaggregation.id;
        insert aggRule;
        
        list<BusinessRule_Template_Details__c> busRuleTemporaryList = new list<BusinessRule_Template_Details__c>();
        busRuleTemporaryList.add(busRuleTemDet);
        
        list<Scenario_Rule_Instance_Details__c> sceRuleDetList = new list<Scenario_Rule_Instance_Details__c>();
        sceRuleDetList.add(sceRuleInsDetObj);
        
        Data_Set_Column_Detail__c dataSetColObj = new Data_Set_Column_Detail__c();
        dataSetColObj.dataset_id__c=dataSetObj.id;
        insert dataSetColObj;
        
        Category_Derived_Variable_Metadata__c catDerMetObj = new Category_Derived_Variable_Metadata__c();
        catDerMetObj.Business_Rule_Id__c=busRuleObj.id;
        insert catDerMetObj;
        
        Category_Derived_Variable_Metadata__c catDerMetObj1 = new Category_Derived_Variable_Metadata__c();
        catDerMetObj1.Business_Rule_Id__c=busRuleObj1.id;
        insert catDerMetObj1;
        
        Category_Derived_Variable_Metadata__c catDerMetObj2 = new Category_Derived_Variable_Metadata__c();
        catDerMetObj2.Business_Rule_Id__c=busRuleObjDecReach.id;
        insert catDerMetObj2;
        
        Category_Derived_Variable_Metadata__c catDerMetObj3 = new Category_Derived_Variable_Metadata__c();
        catDerMetObj3.Business_Rule_Id__c=busRuleObjdecFrequency.id;
        insert catDerMetObj3;
        
        Category_Rule_Entity_Detail__c catRuleEntDet = new Category_Rule_Entity_Detail__c();
        catRuleEntDet.Base_Entity_Id__c=null;
        catRuleEntDet.Business_Rule__c=busRuleObj1.id;
        insert catRuleEntDet;
        
        Category_Rule_Entity_Detail__c catRuleEntDet1 = new Category_Rule_Entity_Detail__c();
        catRuleEntDet1.Base_Entity_Id__c=null;
        catRuleEntDet1.Business_Rule__c=busRuleObjDecReach.id;
        insert catRuleEntDet1;
        
        Category_Rule_Entity_Detail__c catRuleEntDet2 = new Category_Rule_Entity_Detail__c();
        catRuleEntDet2.Base_Entity_Id__c=null;
        catRuleEntDet2.Business_Rule__c=busRuleObjdecFrequency.id;
        insert catRuleEntDet2;
        
        Filter_Rule_Entity_Detail__c filRuleEntDet = new Filter_Rule_Entity_Detail__c();
        filRuleEntDet.Business_Rules__c=busRuleObjFilter.id;
        filRuleEntDet.Base_Filter_Rule_Entity_Detail__c=null;
        insert filRuleEntDet;
        
        Computation_Rule_Detail__c comRuleDet = new Computation_Rule_Detail__c();
        comRuleDet.Business_Rules__c=busRuleObjComPute.id;
        insert comRuleDet;
        
        List<Data_Set__c> lstDataSets = new List<Data_Set__c>();
        Set<String> sids = new Set<String>();
        Data_Set__c dataSetObj1 = new Data_Set__c();
        dataSetObj1.Name ='ZIP-Terr';
        dataSetObj1.SalesIQ_Internal__c = false;
        dataSetObj1.is_internal__c = false;
        lstDataSets.add(dataSetObj1);
        insert lstDataSets;
        for(Data_Set__c d:lstDataSets){
            sids.add(d.Id);
        }
        

        CPGDerivedFieldsListCtrl cpgDerCtrl = new CPGDerivedFieldsListCtrl();
        list<Scenario_Rule_Instance_Details__c> sceRuleInsList = CPGDerivedFieldsListCtrl.getScenrioRuleInstanceId(sceRuleInsDetObj.id);
        list<Data_Set_Rule_Map__c> datasetRuleList = CPGDerivedFieldsListCtrl.getInputTableName(sceRuleInsDetObj.id);
        list<Scenario_Rule_Instance_Details__c> sceRuleIns2List = CPGDerivedFieldsListCtrl.getScenarioID(sceRuleInsDetObj.id);
        list<CPGDerivedFieldsListCtrl.Wrapper>  listCPGWrap = CPGDerivedFieldsListCtrl.getDerivedFieldListData(sceRuleInsDetObj.id);
        
        Map<String,String> parentIdVsRuleType = new Map<String,String>();
        parentIdVsRuleType.put('Category','Category');
        Map<String,String> businessRulesMap = new Map<String,String>();
        businessRulesMap.put('Category','Category');
         Test.startTest();
         //String s = CPGUtility.getColumns(team1.Id);
         list<string> lstString = CPGUtility.GetColumnsToExclude(sceRuleInsDetObj.id);
         List_Of_Operators__mdt[] mdt = CPGUtility.getOperatorTypeValues();
         Boolean b1 = CPGUtility.ValidateBusinessRules(sceRuleInsDetObj.Id,'Rule1',false);
         Boolean b2 = CPGUtility.ValidateBusinessRuleNames(sceRuleInsDetObj.Id,'Rule1',false);
         Boolean b3 = CPGUtility.ValidateBusinessRuleNamesUpdate(sceRuleInsDetObj.Id,'Rule1','Rule2',false);
         Boolean b4 = CPGUtility.ValidateFieldNames(sceRuleInsDetObj.Id,'Rule1');
         Boolean b5 = CPGUtility.ValidateFieldNamesUpdate(sceRuleInsDetObj.Id,'Rule1','Rule1');
         CPGUtility.updateDataSetRuleMapTableName(lstScenarioDetails,sids);
         //CPGUtility.createRuleInstance(parentIdVsRuleType,businessRulesMap,scenarioObj.Id);
         CPGUtility.getBusinessRuleDetails(busRuleTemObj.id,scenarioObj.id);
         /*CPGUtility.createScenarioRuleInstance(busRuleTemporaryList,scenarioObj.id);
         CPGUtility.getcomputeRules(sceRuleDetList);*/
         CPGUtility.replicateExistingAggregateRuleDetails(scenarioObj.id,scenarioObj.Id,new Set<Id>(),new Map<String,String>());
        CPGUtility.replicateExistingJoinBusinessRules(scenarioObj.id, scenarioObj.id,new Set<Id>(),new Map<String,String>(),new Map<String,String>());
        CPGUtility.replicateExistingJoinBusinessRuleSelectCondition(scenarioObj.id, scenarioObj.id,new Set<Id>(),new Map<String,String>(),new Map<String,String>(),new Map<String,String>());
        CPGUtility.replicateExistingAggregateRuleLevels(scenarioObj.id, scenarioObj.id,new Set<Id>(),new Map<String,String>());
        CPGUtility.replicateExistingBusinessRuleFieldMap(new Map<String,String>(),scenarioObj.id, scenarioObj.id,new Set<Id>(),new Map<String,String>());
        CPGUtility.replicateExistingJoinBusinessRuleCondition(scenarioObj.id, scenarioObj.id,new Set<Id>(),new Map<String,String>(),new Map<String,String>());
        CPGUtility.udpateScenarioInstanceDSRuleMapTableNames(new List<Data_Set_Rule_Map__c>(),scenarioObj.id);
        CPGUtility.replicateExistingScenarioBusinessRules(new Map<String,String>(),scenarioObj.id,scenarioObj.id,new Map<String,String>());
         Test.stopTest();
        
    }
}