/*
Author : Ankit Manendra
Description : Test class for CPGFilterCtrl
Date : 8 Novomber, 2017

*/

@isTest
private class TestCPGFilterCtrl{
    @isTest static void cpgFilterTest1() {
    
        Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj;
        
        Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        insert scenarioObj;
        
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj;

        sceRuleInsDetObj.Component_Display_Order__c = 1;
        sceRuleInsDetObj.Status__c = CPGConstants.SUCCESS;
        update sceRuleInsDetObj;

    
         Scenario_Rule_Instance_Details__c sceRuleInsDetObj1 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj1.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj1.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj1;
        
        sceRuleInsDetObj1.Component_Display_Order__c = 1;
        sceRuleInsDetObj1.Status__c = SalesIQGlobalConstants.ERROR_OCCURED;
        update sceRuleInsDetObj1;

        Data_Set_Rule_Map__c dataSetRuleObj = new Data_Set_Rule_Map__c();
        dataSetRuleObj.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj.ds_type__c='I';
        insert dataSetRuleObj;
        
        Data_Set_Rule_Map__c dataSetRuleObj1 = new Data_Set_Rule_Map__c();
        dataSetRuleObj1.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj1.ds_type__c='O';
        insert dataSetRuleObj1;
        
        system.debug('==dataSetRuleObj===='+dataSetRuleObj);
        
        ComponentTypeMaster__c ComTypeMasObj = new ComponentTypeMaster__c();
        ComTypeMasObj.Name='Call Balancing';
        ComTypeMasObj.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj.DisplayIcon__c='add';
        ComTypeMasObj.DisplayIconClass__c='window information';
        insert ComTypeMasObj;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj.Name='Rank';
        busRuleTypeMasObj.Component_Type__c=ComTypeMasObj.id;
        insert busRuleTypeMasObj;
        
        ComponentTypeMaster__c ComTypeMasObj1 = new ComponentTypeMaster__c();
        ComTypeMasObj1.Name='Aggregation';
        ComTypeMasObj1.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj1.DisplayIcon__c='add';
        ComTypeMasObj1.DisplayIconClass__c='window information';
        insert ComTypeMasObj1;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj1 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj1.Name='Rank';
        busRuleTypeMasObj1.Component_Type__c=ComTypeMasObj1.id;
        insert busRuleTypeMasObj1;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj2 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj2.Name='Filter';
        busRuleTypeMasObj2.Component_Type__c=ComTypeMasObj1.id;
        insert busRuleTypeMasObj2;
        
        Business_Rules__c busRuleObj = new Business_Rules__c();
        busRuleObj.Name='Rank rule 278';
        busRuleObj.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObj.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj.Aggregate_Level__c='Test';
        busRuleObj.customer_prioritization__c='Test';
        insert busRuleObj;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj.Param_Name__c='derived field 278';
        busRuleFieldObj.Business_Rule__c=busRuleObj.id;
        busRuleFieldObj.Param_Data_Type__c='Numeric';
        busRuleFieldObj.Param_Type__c='Derived';
        busRuleFieldObj.IO_Flag__c='O';
        
        insert busRuleFieldObj;
        
        Computation_Rule_Detail__c compRuleDetObj = new Computation_Rule_Detail__c();
        compRuleDetObj.Business_Rules__c=busRuleObj.id;
        compRuleDetObj.Data_Type__c='Numeric';
        compRuleDetObj.Derived_field_name__c='FIELD 123';
        
        insert compRuleDetObj;
        
        Data_Set__c dataSetObj = new Data_Set__c();
        dataSetObj.Name = 'Call Plan Summary';
        insert dataSetObj;
        
        Data_Set_Column_Detail__c dataSetCol = new Data_Set_Column_Detail__c();
        dataSetCol.dataset_id__c=dataSetObj.id;
        dataSetCol.variable_type__c='Derived';
        dataSetCol.ds_col_name__c='colTest';
        insert dataSetCol;
        
        list<Data_Set_Column_Detail__c> dataSetColDetList = new list<Data_Set_Column_Detail__c>();
        dataSetColDetList.add(dataSetCol);
        
        Filter_Rule_Entity_Detail__c filterRuleEntityblockObj = new Filter_Rule_Entity_Detail__c();
        filterRuleEntityblockObj.Business_Rules__c =busRuleObj.Id;
        filterRuleEntityblockObj.Json_Expression__c = '';
        filterRuleEntityblockObj.Filter_Display_Expression__c ='Master tier in (\'Tier1\') OR gattexlead_flag = 1 OR callactivity_flag = 1 OR targettype_flag = 1 OR REMS_FLG = 1';
        filterRuleEntityblockObj.Priority__c=1;
        insert filterRuleEntityblockObj;

        Test.startTest();
        
        List<Scenario_Rule_Instance_Details__c> sceRuleList = CPGFilterCtrl.getScenarioID(sceRuleInsDetObj.id);
        string getInputStr = CPGFilterCtrl.getInputTableName(sceRuleInsDetObj.id);
        string getInputStr1 = CPGFilterCtrl.getOutputTableName(sceRuleInsDetObj.id);
        String columndata= '[{"colDatatype":"Text","colValuetype":"Discrete","Id":"a0g1N000009z8V9QAI","Name":"Account_Alignment_Type__c","statusFlag":true,"tableColumnName":"account_alignment_type__c","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VAQAY","Name":"AccountNumber__c","statusFlag":true,"tableColumnName":"accountnumber__c","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Discrete","Id":"a0g1N000009z8VBQAY","Name":"Territory_ID__C","statusFlag":true,"tableColumnName":"territory_id__c","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VCQAY","Name":"POSITION__NAME","statusFlag":true,"tableColumnName":"position__name","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VDQAY","Name":"Name","statusFlag":true,"tableColumnName":"name","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VEQAY","Name":"BillingStreet","statusFlag":true,"tableColumnName":"billingstreet","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VFQAY","Name":"BillingCity","statusFlag":true,"tableColumnName":"billingcity","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VGQAY","Name":"BillingPostalCode","statusFlag":true,"tableColumnName":"billingpostalcode","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VHQAY","Name":"BillingCountry","statusFlag":true,"tableColumnName":"billingcountry","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VIQAY","Name":"BillingLatitude","statusFlag":true,"tableColumnName":"billinglatitude","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VJQAY","Name":"BillingLongitude","statusFlag":true,"tableColumnName":"billinglongitude","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VKQAY","Name":"FirstName__c","statusFlag":true,"tableColumnName":"firstname__c","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VLQAY","Name":"LastName__c","statusFlag":true,"tableColumnName":"lastname__c","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Numeric","colValuetype":"Continuous","Id":"a0g1N000009z8VMQAY","Name":"Metric1__c","statusFlag":true,"tableColumnName":"metric1__c","tableDataType":"numeric","variabletype":"Source"},{"colDatatype":"Numeric","colValuetype":"Continuous","Id":"a0g1N000009z8VNQAY","Name":"Metric2__c","statusFlag":true,"tableColumnName":"metric2__c","tableDataType":"numeric","variabletype":"Source"},{"colDatatype":"Numeric","colValuetype":"Continuous","Id":"a0g1N000009z8VOQAY","Name":"Metric3__c","statusFlag":true,"tableColumnName":"metric3__c","tableDataType":"numeric","variabletype":"Source"},{"colDatatype":"Numeric","colValuetype":"Continuous","Id":"a0g1N000009z8VPQAY","Name":"Metric4__c","statusFlag":true,"tableColumnName":"metric4__c","tableDataType":"numeric","variabletype":"Source"},{"colDatatype":"Numeric","colValuetype":"Continuous","Id":"a0g1N000009z8VQQAY","Name":"Metric5__c","statusFlag":true,"tableColumnName":"metric5__c","tableDataType":"numeric","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Discrete","Id":"a0g1N000009z8VRQAY","Name":"Speciality__c","statusFlag":true,"tableColumnName":"speciality__c","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VSQAY","Name":"External_Account_Number__c","statusFlag":true,"tableColumnName":"external_account_number__c","tableDataType":"text","variabletype":"Source"}]';
        string wrapperData= '[{"criteriaLogic":"(1 OR 2 OR 3) AND (4)","lstWrapGroup":[{"lstwrapCriteria":[{"criName":"-","criNameOptions":["-"],"fieldName":[{"dataType":"Numeric","dsColName":"gattexlead_flag","tblColName":"gattexlead_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"SHIRE_ID","tblColName":"shire_id","variableType":"Source"},{"dataType":"Text","dsColName":"Master tier","tblColName":"master_tier","variableType":"Source"},{"dataType":"Numeric","dsColName":"callactivity_flag","tblColName":"callactivity_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"REMS_FLG","tblColName":"rems_flg","variableType":"Source"},{"dataType":"Text","dsColName":"ZIP","tblColName":"zip","variableType":"Source"},{"dataType":"Text","dsColName":"SPECIALTY_CODE","tblColName":"specialty_code","variableType":"Source"},{"dataType":"Text","dsColName":"PROF_TYPE","tblColName":"prof_type","variableType":"Source"},{"dataType":"Text","dsColName":"PROF_SUBTYPE","tblColName":"prof_subtype","variableType":"Source"},{"dataType":"Text","dsColName":"PROF_STATUS","tblColName":"prof_status","variableType":"Source"},{"dataType":"Text","dsColName":"SF_EXCLUSION","tblColName":"sf_exclusion","variableType":"Source"},{"dataType":"Numeric","dsColName":"NO_SAMPLE","tblColName":"no_sample","variableType":"Source"},{"dataType":"Numeric","dsColName":"NO_ACCESS","tblColName":"no_access","variableType":"Source"},{"dataType":"Numeric","dsColName":"HR_EXCLUSION","tblColName":"hr_exclusion","variableType":"Source"},{"dataType":"Numeric","dsColName":"RUL","tblColName":"rul","variableType":"Source"},{"dataType":"Numeric","dsColName":"Gattex_XS_Final","tblColName":"gattex_xs_final","variableType":"Source"},{"dataType":"Numeric","dsColName":"Gattex_XT","tblColName":"gattex_xt","variableType":"Source"},{"dataType":"Text","dsColName":"territory_ID","tblColName":"territory_id","variableType":"Source"},{"dataType":"Text","dsColName":"GATTEX CALL PLAN ELIGIBLE FLAG","tblColName":"gattex_call_plan_eligible_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"targettype_flag","tblColName":"targettype_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"comp_name","tblColName":"comp_name","variableType":"Derived"},{"dataType":"Numeric","dsColName":"frcomp1","tblColName":"frcomp1","variableType":"Derived"},{"dataType":"Numeric","dsColName":"fff1","tblColName":"fff1","variableType":"Derived"},{"dataType":"Numeric","dsColName":"pp","tblColName":"pp","variableType":"Derived"},{"dataType":"Numeric","dsColName":"f1cat","tblColName":"f1cat","variableType":"Derived"},{"dataType":"Numeric","dsColName":"f1","tblColName":"f1","variableType":"Derived"}],"fieldVal":"TIER 1, TIER 2,TIER 3, PROSPECT","opVal":["equals","not equals","in","not in","contains","does not contains","begins with","ends with","is null","is not null"],"selFieldName":"0@0@0@Text@master_tier@Source","selOpVal":"0@0@0@in","seqNo":1},{"criName":"OR","criNameOptions":["AND","OR"],"fieldName":[{"dataType":"Numeric","dsColName":"gattexlead_flag","tblColName":"gattexlead_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"SHIRE_ID","tblColName":"shire_id","variableType":"Source"},{"dataType":"Text","dsColName":"Master tier","tblColName":"master_tier","variableType":"Source"},{"dataType":"Numeric","dsColName":"callactivity_flag","tblColName":"callactivity_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"REMS_FLG","tblColName":"rems_flg","variableType":"Source"},{"dataType":"Text","dsColName":"ZIP","tblColName":"zip","variableType":"Source"},{"dataType":"Text","dsColName":"SPECIALTY_CODE","tblColName":"specialty_code","variableType":"Source"},{"dataType":"Text","dsColName":"PROF_TYPE","tblColName":"prof_type","variableType":"Source"},{"dataType":"Text","dsColName":"PROF_SUBTYPE","tblColName":"prof_subtype","variableType":"Source"},{"dataType":"Text","dsColName":"PROF_STATUS","tblColName":"prof_status","variableType":"Source"},{"dataType":"Text","dsColName":"SF_EXCLUSION","tblColName":"sf_exclusion","variableType":"Source"},{"dataType":"Numeric","dsColName":"NO_SAMPLE","tblColName":"no_sample","variableType":"Source"},{"dataType":"Numeric","dsColName":"NO_ACCESS","tblColName":"no_access","variableType":"Source"},{"dataType":"Numeric","dsColName":"HR_EXCLUSION","tblColName":"hr_exclusion","variableType":"Source"},{"dataType":"Numeric","dsColName":"RUL","tblColName":"rul","variableType":"Source"},{"dataType":"Numeric","dsColName":"Gattex_XS_Final","tblColName":"gattex_xs_final","variableType":"Source"},{"dataType":"Numeric","dsColName":"Gattex_XT","tblColName":"gattex_xt","variableType":"Source"},{"dataType":"Text","dsColName":"territory_ID","tblColName":"territory_id","variableType":"Source"},{"dataType":"Text","dsColName":"GATTEX CALL PLAN ELIGIBLE FLAG","tblColName":"gattex_call_plan_eligible_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"targettype_flag","tblColName":"targettype_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"comp_name","tblColName":"comp_name","variableType":"Derived"},{"dataType":"Numeric","dsColName":"frcomp1","tblColName":"frcomp1","variableType":"Derived"},{"dataType":"Numeric","dsColName":"fff1","tblColName":"fff1","variableType":"Derived"},{"dataType":"Numeric","dsColName":"pp","tblColName":"pp","variableType":"Derived"},{"dataType":"Numeric","dsColName":"f1cat","tblColName":"f1cat","variableType":"Derived"},{"dataType":"Numeric","dsColName":"f1","tblColName":"f1","variableType":"Derived"}],"fieldVal":"1","opVal":["equals","not equals","in","not in","greater than","greater or equal","less than","less or equal","between","is null","is not null"],"selFieldName":"1@0@0@Numeric@gattexlead_flag@Source","selOpVal":"1@0@0@equals","seqNo":2},{"criName":"OR","criNameOptions":["AND","OR"],"fieldName":[{"dataType":"Numeric","dsColName":"gattexlead_flag","tblColName":"gattexlead_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"SHIRE_ID","tblColName":"shire_id","variableType":"Source"},{"dataType":"Text","dsColName":"Master tier","tblColName":"master_tier","variableType":"Source"},{"dataType":"Numeric","dsColName":"callactivity_flag","tblColName":"callactivity_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"REMS_FLG","tblColName":"rems_flg","variableType":"Source"},{"dataType":"Text","dsColName":"ZIP","tblColName":"zip","variableType":"Source"},{"dataType":"Text","dsColName":"SPECIALTY_CODE","tblColName":"specialty_code","variableType":"Source"},{"dataType":"Text","dsColName":"PROF_TYPE","tblColName":"prof_type","variableType":"Source"},{"dataType":"Text","dsColName":"PROF_SUBTYPE","tblColName":"prof_subtype","variableType":"Source"},{"dataType":"Text","dsColName":"PROF_STATUS","tblColName":"prof_status","variableType":"Source"},{"dataType":"Text","dsColName":"SF_EXCLUSION","tblColName":"sf_exclusion","variableType":"Source"},{"dataType":"Numeric","dsColName":"NO_SAMPLE","tblColName":"no_sample","variableType":"Source"},{"dataType":"Numeric","dsColName":"NO_ACCESS","tblColName":"no_access","variableType":"Source"},{"dataType":"Numeric","dsColName":"HR_EXCLUSION","tblColName":"hr_exclusion","variableType":"Source"},{"dataType":"Numeric","dsColName":"RUL","tblColName":"rul","variableType":"Source"},{"dataType":"Numeric","dsColName":"Gattex_XS_Final","tblColName":"gattex_xs_final","variableType":"Source"},{"dataType":"Numeric","dsColName":"Gattex_XT","tblColName":"gattex_xt","variableType":"Source"},{"dataType":"Text","dsColName":"territory_ID","tblColName":"territory_id","variableType":"Source"},{"dataType":"Text","dsColName":"GATTEX CALL PLAN ELIGIBLE FLAG","tblColName":"gattex_call_plan_eligible_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"targettype_flag","tblColName":"targettype_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"comp_name","tblColName":"comp_name","variableType":"Derived"},{"dataType":"Numeric","dsColName":"frcomp1","tblColName":"frcomp1","variableType":"Derived"},{"dataType":"Numeric","dsColName":"fff1","tblColName":"fff1","variableType":"Derived"},{"dataType":"Numeric","dsColName":"pp","tblColName":"pp","variableType":"Derived"},{"dataType":"Numeric","dsColName":"f1cat","tblColName":"f1cat","variableType":"Derived"},{"dataType":"Numeric","dsColName":"f1","tblColName":"f1","variableType":"Derived"}],"fieldVal":"1","opVal":["equals","not equals","in","not in","greater than","greater or equal","less than","less or equal","between","is null","is not null"],"selFieldName":"2@0@0@Numeric@f1@Derived","selOpVal":"2@0@0@equals","seqNo":3}]},{"lstwrapCriteria":[{"criName":"-","criNameOptions":["-"],"fieldName":[{"dataType":"Numeric","dsColName":"gattexlead_flag","tblColName":"gattexlead_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"SHIRE_ID","tblColName":"shire_id","variableType":"Source"},{"dataType":"Text","dsColName":"Master tier","tblColName":"master_tier","variableType":"Source"},{"dataType":"Numeric","dsColName":"callactivity_flag","tblColName":"callactivity_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"REMS_FLG","tblColName":"rems_flg","variableType":"Source"},{"dataType":"Text","dsColName":"ZIP","tblColName":"zip","variableType":"Source"},{"dataType":"Text","dsColName":"SPECIALTY_CODE","tblColName":"specialty_code","variableType":"Source"},{"dataType":"Text","dsColName":"PROF_TYPE","tblColName":"prof_type","variableType":"Source"},{"dataType":"Text","dsColName":"PROF_SUBTYPE","tblColName":"prof_subtype","variableType":"Source"},{"dataType":"Text","dsColName":"PROF_STATUS","tblColName":"prof_status","variableType":"Source"},{"dataType":"Text","dsColName":"SF_EXCLUSION","tblColName":"sf_exclusion","variableType":"Source"},{"dataType":"Numeric","dsColName":"NO_SAMPLE","tblColName":"no_sample","variableType":"Source"},{"dataType":"Numeric","dsColName":"NO_ACCESS","tblColName":"no_access","variableType":"Source"},{"dataType":"Numeric","dsColName":"HR_EXCLUSION","tblColName":"hr_exclusion","variableType":"Source"},{"dataType":"Numeric","dsColName":"RUL","tblColName":"rul","variableType":"Source"},{"dataType":"Numeric","dsColName":"Gattex_XS_Final","tblColName":"gattex_xs_final","variableType":"Source"},{"dataType":"Numeric","dsColName":"Gattex_XT","tblColName":"gattex_xt","variableType":"Source"},{"dataType":"Text","dsColName":"territory_ID","tblColName":"territory_id","variableType":"Source"},{"dataType":"Text","dsColName":"GATTEX CALL PLAN ELIGIBLE FLAG","tblColName":"gattex_call_plan_eligible_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"targettype_flag","tblColName":"targettype_flag","variableType":"Source"},{"dataType":"Numeric","dsColName":"comp_name","tblColName":"comp_name","variableType":"Derived"},{"dataType":"Numeric","dsColName":"frcomp1","tblColName":"frcomp1","variableType":"Derived"},{"dataType":"Numeric","dsColName":"fff1","tblColName":"fff1","variableType":"Derived"},{"dataType":"Numeric","dsColName":"pp","tblColName":"pp","variableType":"Derived"},{"dataType":"Numeric","dsColName":"f1cat","tblColName":"f1cat","variableType":"Derived"},{"dataType":"Numeric","dsColName":"f1","tblColName":"f1","variableType":"Derived"}],"fieldVal":"2","opVal":["equals","not equals","in","not in","greater than","greater or equal","less than","less or equal","between","is null","is not null"],"selFieldName":"0@1@0@Numeric@gattexlead_flag@Source","selOpVal":"0@1@0@not in","seqNo":4}],"selGroupOp":"AND"}],"priority":[1],"selPriority":1}]'; 
        string parsedJson = '{"bindClause":"AND","criterias":[{"anotherValue":"","dataType":"text","field":"name","innerConditionWrapper":{},"isGroup":false,"operator":"equals","value":"abs"}]}';
        String finalQuery = 'name = \'abs\'';
        String stagingQuery = 'name = \'abs\'';
        list<BRColumnWrapper> colWrapList = CPGFilterCtrl.CreateColumnWrapper(sceRuleInsDetObj.id);
        List<string> str5 = CPGFilterCtrl.getRuleName(sceRuleInsDetObj.id);
        list<BRColumnWrapper> colWrap5 = CPGFilterCtrl.getColumns(sceRuleInsDetObj.id);
        Filter_Rule_Criteria_Detail__c filRuleCri = new Filter_Rule_Criteria_Detail__c();
        filRuleCri.Sequence__c='1';
        insert filRuleCri;
        
        string busRUleStr = CPGFilterCtrl.getBusinessRuleId(sceRuleInsDetObj.id);
        Id saveBusId = CPGFilterCtrl.SaveBusinessRule('testBusName',sceRuleInsDetObj.id);
         
        CPGFilterCtrl.saveUpdate('Test Rule Name',parsedJson,finalQuery,sceRuleInsDetObj.Id,'componentId',columndata,stagingQuery);  
        Test.stopTest(); 
        }
   
}