/**
* Description   : Controller for Request Inline vf page. Contains controller methods for diplaying CIM data for a Request
* Author        : Arun.Kumar
* Since         : 30-Dec-2015
* Version       : 1.0
*/

public with sharing class RequestCtlr{   
    
    //public Decimal guardrailNormal {get;set;} 
    public CIM_Config__c cimGraudrail{get;set;}
    public CIM_Config__c cimGraudrailForOverlay{get;set;}

    //display header name for KPI name and its id
    public Map<string,List<CIM_Change_Request_Impact__c>> MapCIMDisplayNamewithType {get;set;} 
    
    // KIP data for each metric
    public Map<String, List<RequestUtilityClass.RequestCIMWrapper>> requestKPIDataBase {get;set;}
    public Map<String, List<RequestUtilityClass.RequestCIMWrapper>> requestKPIDataOverlay{get;set;}

    public String destinationName{get;set;}
    public String sourceName{get;set;}
    public String overlayDestinationName{get;set;}
    public String overlaySourceName {get;set;}
    public String sourceTeamInstancePosition {get;set;}
    public String destinationTeamInstancePosition {get;set;}
    public String overlaySourcePositionTeamInstance {get;set;}
    public String overlayDestinationPositionTeamInstance {get;set;}
    public Boolean renderOverlayTab {get; set;}   
    public Boolean overlayMoved {get; set;}   
    //Commenting this as it is not getting used now on Request Page
    //public String quarterName{get;set;}
    public Boolean allOk {get; set;}
    
    public String base{get;set;}
    public String overlay{get;set;}
    public string teamId;
    
    public String selectedOverlayTeamInstance {get;set;}
    public Id configIdForBase;
    public ID basePosTeamInstance ;
    public boolean hideBaseTab {get;set;}
    public boolean showselectedOverlay {get;set;}

    private string sourcePosition;
    private string destinationPosition;
    private string allZIPs;
    private string selectedCycle;
    public boolean showCustomMetric{get;set;}
    private string movementType;
    private boolean restrictZIPShare;
    private string crStatus;

    public boolean cimFound{get;set;}
    
    public RequestCtlr(ApexPages.StandardController controller){
        try {
            restrictZIPShare = false;
            cimFound = false;
            crStatus='';
        	
            sourcePosition='';
            destinationPosition='';
            allZIPs='';
            selectedCycle='';
        	hideBaseTab = false;
            showCustomMetric = false;
            getAllCIMforRequest(controller.getId());
            
            allOk = true ;
            //Fetch team specific Overlay/base naming convensions
            if(teamId != null && Team_Instance_Configuration__c.getValues(teamId.substring(0, 15)) != null){
                base = Team_Instance_Configuration__c.getValues(teamId.substring(0, 15)).Position_Type__c;
                overlay = Team_Instance_Configuration__c.getValues(teamId.substring(0, 15)).Configuration_Type__c;
            }else{
                overlay = SalesIQGlobalConstants.TEAM_TYPE_OVERLAY;
                base = SalesIQGlobalConstants.TEAM_TYPE_BASE;
            }
        } catch(Exception e) {
            SalesIQLogger.logMessage(e);
            allOk = false;
        }
    }

    /*
     * @description   It is responsible for displaying the header dynamically
     *                This method will get the CIM data which was included at the time of Request creation.
     * @param         appRequestId - string of current Change Request ID
     */ 
    public map<String,String> multipleOverlayTeamInstanceMap {get;set;}
    list<CIM_Change_Request_Impact__c> lstCIMRequest;
    private void getAllCIMforRequest(string appRequestId){
        //This will contain the CIM as a key and  impacted value of ZIp/Account
        lstCIMRequest = new list<CIM_Change_Request_Impact__c>();
        Map<string,string> mapCIMwithImpactedValue = new Map<string,string>();
        Map<id,set<Id>> CIMidMap = new Map<id,set<Id>>();
        Set<Id> positionTeamInstanceIdSet = new Set<Id>();
        renderOverlayTab = false ;
        overlayMoved = false ;
        multipleOverlayTeamInstanceMap = new map<String,String>();
        lstCIMRequest = RequestUtilityClass.getChangeRequestImpactData(appRequestId,'');  
                
        
        if(lstCIMRequest!=null && lstCIMRequest.size()>0){
            cimFound = true;
            List<CIM_Change_Request_Impact__c> MapCIMDisplayNameBase = new List<CIM_Change_Request_Impact__c>();
            List<CIM_Change_Request_Impact__c> MapCIMDisplayNameOverlay = new List<CIM_Change_Request_Impact__c>();
            MapCIMDisplayNamewithType = new Map<string,List<CIM_Change_Request_Impact__c>>();
            sourceTeamInstancePosition = lstCIMRequest[0].Change_Request__r.Source_Positions__c;
            destinationTeamInstancePosition = lstCIMRequest[0].Change_Request__r.Destination_Positions__c;
            sourceName = lstCIMRequest[0].Change_Request__r.Source_Position__r.Name;
            destinationName = lstCIMRequest[0].Change_Request__r.Destination_Position__r.Name;   

            sourcePosition = lstCIMRequest[0].Change_Request__r.Source_Position__c;
            destinationPosition = lstCIMRequest[0].Change_Request__r.Destination_Position__c;   
            allZIPs = lstCIMRequest[0].Change_Request__r.AllZips__c;
            if(lstCIMRequest[0].Change_Request__r.Point_Zips__c!=null){
                allZIPs  +=','+lstCIMRequest[0].Change_Request__r.Point_Zips__c;
            } 
            selectedCycle = lstCIMRequest[0].Change_Request__r.Team_Instance_id__c;
            restrictZIPShare = lstCIMRequest[0].Change_Request__r.Team_Instance_id__r.Restrict_ZIP_Share__c;
            //Custom metrric shouldn't be visible if ZIP sharing is enable
            //SPD-4587
            if(restrictZIPShare){
                showCustomMetric  = lstCIMRequest[0].Change_Request__r.Team_Instance_id__r.Show_Custom_Metric__c;
            }
            
            movementType = lstCIMRequest[0].Change_Request__r.Request_Type_Change__c;
            crStatus = lstCIMRequest[0].Change_Request__r.Status__c;

            
			
            Map<Id, Related_Position__c> overlayPositionsMapByBasePosition = RequestUtilityClass.getRelatesPositionsGroupByPositionTeamInstance(new Set<Id>{lstCIMRequest[0].Change_Request__r.Source_Position__c, lstCIMRequest[0].Change_Request__r.Destination_Position__c},'');
            if(!overlayPositionsMapByBasePosition.isEmpty())
                renderOverlayTab = true ;
            //Id configIdForBase, configIdForOverlay ;
            ID configIdForOverlay;
            Set<Id> baseCIM = new Set<Id>(), overlayCIM = new Set<Id>();
            for(CIM_Change_Request_Impact__c imp:lstCIMRequest) {
                teamId = imp.CIM_Config__r.Team_Instance__r.Team__c;
                imp.CIM_Config__r.Display_Name__c = AlignmentUtility.getLabelString(imp.CIM_Config__r.Display_Name__c);
                //teamCycleName = imp.CIM_Config__r.Team_Instance__r.Team_Cycle_Name__c;
                //System.debug('teamCycleName :'+teamCycleName);
                System.debug('config ::: '+imp);
                if(!multipleOverlayTeamInstanceMap.containsKey(imp.Team_Instance__c)){
                	if(imp.CIM_Config__r.Team_Instance__r.Team__r.Type__c == SalesIQGlobalConstants.TEAM_TYPE_OVERLAY){
                		System.debug(' team instance -- '+imp.Team_Instance__c);
                		multipleOverlayTeamInstanceMap.put(imp.Team_Instance__c, imp.Team_Instance__r.Name);
                	}
                }
                	
               	
                if(imp.CIM_Config__r.Team_Instance__r.Team__r.Type__c != SalesIQGlobalConstants.TEAM_TYPE_OVERLAY)
                {
                    
                    if(!baseCIM.contains(imp.CIM_Config__c) ) {
                        MapCIMDisplayNameBase.add(imp);
                        baseCIM.add(imp.CIM_Config__c) ;
                    }
                    configIdForBase = imp.CIM_Config__c ;
                }
                else
                {
                    configIdForOverlay = imp.CIM_Config__c ;
                    //MapCIMDisplayNameOverlay.put(imp.CIM_Config__c,imp.CIM_Config__r.display_name__c);
                    if(!overlayCIM.contains(imp.CIM_Config__c) ) {
                        MapCIMDisplayNameOverlay.add(imp);
                        overlayCIM.add(imp.CIM_Config__c) ;
                    }
					
                    // code added by aditi to populate source and destination overlay position name in variable, SPD - 247
                    if(overlayPositionsMapByBasePosition.get(imp.Change_Request__r.Source_Positions__c) != null && overlayPositionsMapByBasePosition.get(imp.Change_Request__r.Source_Positions__c).Related_Position_Team_Instance__c == imp.Position_Team_Instance__c) {

                        overlaySourceName = imp.Position__c ;
                        overlaySourcePositionTeamInstance = imp.Position_Team_Instance__c ;
                    } 

                    if(overlayPositionsMapByBasePosition.get(imp.Change_Request__r.Destination_Positions__c) != null && overlayPositionsMapByBasePosition.get(imp.Change_Request__r.Destination_Positions__c).Related_Position_Team_Instance__c == imp.Position_Team_Instance__c) {

                        overlayDestinationName = imp.Position__c ; 
                        overlayDestinationPositionTeamInstance = imp.Position_Team_Instance__c ;
                    }
                }

                positionTeamInstanceIdSet.add(imp.Position_Team_Instance__c);
				basePosTeamInstance = imp.Position_Team_Instance__c;
				
                if(CIMidMap.containsKey(imp.Team_Instance__c))
                    CIMidMap.get(imp.Team_Instance__c).add(imp.CIM_Config__c);
                else
                    CIMidMap.put(imp.Team_Instance__c,new set<Id>{imp.CIM_Config__c});

                mapCIMwithImpactedValue.put(imp.CIM_Config__c,imp.Impact__c);
            }

            if(overlaySourcePositionTeamInstance == null || overlayDestinationPositionTeamInstance == null) {
                // don't load the overlay data, things has been moved or no overlay ever existed
                overlayMoved = true ;
            }
            
            MapCIMDisplayNamewithType.put(SalesIQGlobalConstants.TEAM_TYPE_BASE,MapCIMDisplayNameBase);
            if(!MapCIMDisplayNameOverlay.isEmpty())
                MapCIMDisplayNamewithType.put(SalesIQGlobalConstants.TEAM_TYPE_OVERLAY,MapCIMDisplayNameOverlay);
            
            // get first CIM config ID to find the guradrails
            cimGraudrail = getCIMgraudrailInfo(configIdForBase);
            if(configIdForOverlay != null) {
                cimGraudrailForOverlay = getCIMgraudrailInfo(configIdForOverlay);
            }


            getCIMSummaryData(mapCIMwithImpactedValue, sourceTeamInstancePosition, destinationTeamInstancePosition, overlaySourcePositionTeamInstance, overlayDestinationPositionTeamInstance, lstCIMRequest[0].Change_Request__r.Status__c, CIMidMap, positionTeamInstanceIdSet);
            
        }
        else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, System.Label.NO_CIM_FOUND);
            ApexPages.addMessage(myMsg); 
        }
         system.debug('cimFound '+cimFound);

        if(lstCIMRequest[0].Change_Request__r.Team_Instance_id__r.CIM_Available__c){
            cimFound = true;
            
        }else{
            allOk = false;
            cimFound = false;
        }

    }
    
    /*
    * This method is used to display the Graudrail % on inline page     
     * @return  void
     */ 
    private CIM_Config__c getCIMgraudrailInfo(String configId){
        return RequestUtilityClass.getCIMConfigDatabyCIMID(configId); 
    }
    
    /*
     * @description This method will get the CIM data from RequestImpactSummary based on Position Team Instance
     * @param impactValueMap has key-value of CIM id and Impact value from CIM_Change_Request_Impact__c
     * @param string of SourcePosition Team Instance
     * @param status of currently requested Chnage Request
     * @return  void
     */ 
    private void getCIMSummaryData(Map<string,string> impactValueMap, string sourceTeamInstancePosition, string destinationTeamInstancePosition, string overlaySourcePositionTeamInstance, string overlayDestinationPositionTeamInstance, string requestStatus, Map<id,set<id>> cimMetricGroupByTeamInstanceMap, Set<Id> positionTeamInstanceIdSet)
    {

        list<string> POSITION_TEAM_INSTANCE_READ_FIELD = new list<string>{'Position_ID__c', 'Team_Instance_ID__c'};
        list<string> POSITION_READ_FIELD = new list<string>{'name', 'Related_Position_Type__c'};
        requestKPIDataBase = new Map<String, List<RequestUtilityClass.RequestCIMWrapper>>();
        requestKPIDataOverlay = new Map<String, List<RequestUtilityClass.RequestCIMWrapper>>();
        if(SecurityUtil.checkRead(Position__c.SObjectType, POSITION_READ_FIELD, false) && SecurityUtil.checkRead(Position_Team_Instance__c.SObjectType, POSITION_TEAM_INSTANCE_READ_FIELD, false)) {
            
            Map<Id, Position_Team_Instance__c> basePositionTeamInstanceMap = new Map<Id, Position_Team_Instance__c>([select id,Position_ID__r.name,Team_Instance_ID__c from Position_Team_Instance__c where id in:positionTeamInstanceIdSet and Position_ID__r.Related_Position_Type__c = 'Base' ]);
						
            Map<Id, Position_Team_Instance__c> overlayPositionTeamInstanceMap = new Map<Id, Position_Team_Instance__c>([select id,Position_ID__r.name,Team_Instance_ID__c from Position_Team_Instance__c where id in:positionTeamInstanceIdSet and Position_ID__r.Related_Position_Type__c = 'Overlay']);

			//Arun SPD-5212
			//resetting because it won't exetute else condition
			if(string.isBlank(overlaySourcePositionTeamInstance )){
			    overlayMoved = true;
			}
			
            if(selectedOverlayTeamInstance == null || selectedOverlayTeamInstance == ''){            	
            	requestKPIDataBase = RequestUtilityClass.getCIMKPIDatabyPositionTeamInstance(cimMetricGroupByTeamInstanceMap.get(basePositionTeamInstanceMap.get(sourceTeamInstancePosition).Team_Instance_ID__c), basePositionTeamInstanceMap.keySet(), impactValueMap,sourceTeamInstancePosition, requestStatus,destinationTeamInstancePosition);
            }            
            else{
            	hideBaseTab = true;            	
	            if(!overlayPositionTeamInstanceMap.isEmpty() && !overlayMoved) {
	                requestKPIDataOverlay = RequestUtilityClass.getCIMKPIDatabyPositionTeamInstance(cimMetricGroupByTeamInstanceMap.get(overlayPositionTeamInstanceMap.get(overlaySourcePositionTeamInstance).Team_Instance_ID__c), overlayPositionTeamInstanceMap.keySet(), impactValueMap,overlaySourcePositionTeamInstance, requestStatus,overlayDestinationPositionTeamInstance);	
	            }
            }            
            
        }
         
        if(requestKPIDataOverlay.size() > 0 ){
        	overlayMoved = false;
        }
    }
    
    public void getSelectedOverlayCIMRequest(){
            //Arun 
            //Resetting to blank because if some Over/Mirror team instance doexn't have data in the related position
            //then this hold the previously clicked Tesm Istance (from tab);
            overlaySourcePositionTeamInstance='';
    	
    		
    		showselectedOverlay = true;
    		//List<CIM_Change_Request_Impact__c> MapCIMDisplayNameBase = new List<CIM_Change_Request_Impact__c>();
            List<CIM_Change_Request_Impact__c> MapCIMDisplayNameOverlay = new List<CIM_Change_Request_Impact__c>();
            MapCIMDisplayNamewithType = new Map<string,List<CIM_Change_Request_Impact__c>>();
            Set<Id> positionTeamInstanceIdSet = new Set<Id>();
            Map<string,string> mapCIMwithImpactedValue = new Map<string,string>();
       		Map<id,set<Id>> CIMidMap = new Map<id,set<Id>>();
    		
    		Map<Id, Related_Position__c> overlayPositionsMapByBasePosition = RequestUtilityClass.getRelatesPositionsGroupByPositionTeamInstance(new Set<Id>{lstCIMRequest[0].Change_Request__r.Source_Position__c, lstCIMRequest[0].Change_Request__r.Destination_Position__c},selectedOverlayTeamInstance);
            if(!overlayPositionsMapByBasePosition.isEmpty())
                renderOverlayTab = true ;

			
            Id configIdForOverlay ;
            overlayMoved = false;
            Set<Id> baseCIM = new Set<Id>(), overlayCIM = new Set<Id>();
            for(CIM_Change_Request_Impact__c imp:lstCIMRequest) {
            	
            	if(imp.Team_Instance__c == selectedOverlayTeamInstance){
            		
            		
                	teamId = imp.CIM_Config__r.Team_Instance__r.Team__c;
                
                    configIdForOverlay = imp.CIM_Config__c ;
                    
                    if(!overlayCIM.contains(imp.CIM_Config__c) ) {
                        MapCIMDisplayNameOverlay.add(imp);
                        overlayCIM.add(imp.CIM_Config__c) ;
                    }
					
					
                    if(overlayPositionsMapByBasePosition.get(imp.Change_Request__r.Source_Positions__c) != null && overlayPositionsMapByBasePosition.get(imp.Change_Request__r.Source_Positions__c).Related_Position_Team_Instance__c == imp.Position_Team_Instance__c) {
						
                        overlaySourceName = imp.Position__c ;
                        overlaySourcePositionTeamInstance = imp.Position_Team_Instance__c ;
                    } 

                    if(overlayPositionsMapByBasePosition.get(imp.Change_Request__r.Destination_Positions__c) != null && overlayPositionsMapByBasePosition.get(imp.Change_Request__r.Destination_Positions__c).Related_Position_Team_Instance__c == imp.Position_Team_Instance__c) {
						
						overlayDestinationName = imp.Position__c ; 
                        overlayDestinationPositionTeamInstance = imp.Position_Team_Instance__c ;
                    }
              

	                positionTeamInstanceIdSet.add(imp.Position_Team_Instance__c);
	                positionTeamInstanceIdSet.add(basePosTeamInstance);
	
	                if(CIMidMap.containsKey(imp.Team_Instance__c)){
	                	System.debug('if ----'+imp.CIM_Config__c);
	                    CIMidMap.get(imp.Team_Instance__c).add(imp.CIM_Config__c);
	                }else{
	                	System.debug('else ----'+imp.CIM_Config__c);
	                    CIMidMap.put(imp.Team_Instance__c,new set<Id>{imp.CIM_Config__c});
	                }
                	mapCIMwithImpactedValue.put(imp.CIM_Config__c,imp.Impact__c);
            	}
            }

            if(overlaySourcePositionTeamInstance == null || overlayDestinationPositionTeamInstance == null) {
                // don't load the overlay data, things has been moved or no overlay ever existed
                
                overlayMoved = true ;
            }
            
            //MapCIMDisplayNamewithType.put(SalesIQGlobalConstants.TEAM_TYPE_BASE,MapCIMDisplayNameBase);
            if(!MapCIMDisplayNameOverlay.isEmpty())
                MapCIMDisplayNamewithType.put(SalesIQGlobalConstants.TEAM_TYPE_OVERLAY,MapCIMDisplayNameOverlay);
            
            // get first CIM config ID to find the guradrails
            
            cimGraudrail = getCIMgraudrailInfo(configIdForBase);
            if(configIdForOverlay != null) {
                cimGraudrailForOverlay = getCIMgraudrailInfo(configIdForOverlay);
            }
            getCIMSummaryData(mapCIMwithImpactedValue, sourceTeamInstancePosition, destinationTeamInstancePosition, overlaySourcePositionTeamInstance, overlayDestinationPositionTeamInstance, lstCIMRequest[0].Change_Request__r.Status__c, CIMidMap, positionTeamInstanceIdSet);
               		
    
    }

    public string customMetricValue{get;set;}
    /*@author : Arun Kumar
    @date : 27 Dec 2017  
    @description : this method call the external method which extis outside the managed package   
    @return : void
    */  
    public void enableCustomMetric(){
        //added if condition in order to ensure that below method will not call if some one enable the Show Custom Metric for Account Team
        if(!string.isBlank(allZIPs)){
            //list<string> movedZips = allZIPs.split(',');
            //customMetricValue  = SalesIQUtility.getCustomMetric(sourcePosition,destinationPosition,selectedCycle,movedZips, '','View',null,movementType, SalesIQLogger.CHANGE_REQUEST);      
            customMetricValue = AlignmentUtility.getCustomMetric(sourcePosition,destinationPosition,'Pending',allZIPs,selectedCycle, movementType,'Request');
            system.debug('customMetricValue ');
        }
       
    }

}