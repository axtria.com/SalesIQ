/**
 * Utility class for checking FLS/CRUD, throws Exception in case access level of user is restricted
 **/
public class SecurityUtil { 

    /**
     * If set to true all check methods will always return void, and never throw exceptions.
     * This should really only be set to true if an app-wide setting to disable in-apex
     * FLS and CRUD checks exists and is enabled.
     * Per security best practices setting BYPASS should be an a opt-in, and not the default behavior.
     **/
    public static boolean BYPASS_INTERNAL_FLS_AND_CRUD = TriggerContol__c.getValues('SecurityUtil') != null && TriggerContol__c.getValues('SecurityUtil').IsStopTrigger__c ;

    @testVisible 
    private Enum OperationType { C, R, U, D }

    public static Boolean checkObjectIsReadable(SObjectType objType)
    {
        if (!BYPASS_INTERNAL_FLS_AND_CRUD && !objType.getDescribe().isAccessible())
            throw new CrudException(OperationType.R, objType);

        return true ;
    }

    public static Boolean checkObjectIsInsertable(SObjectType objType)
    {
        if (!BYPASS_INTERNAL_FLS_AND_CRUD && !objType.getDescribe().isCreateable())
            throw new CrudException(OperationType.C, objType);

        return true ;
    }

    public static Boolean checkObjectIsUpdateable(SObjectType objType)
    {
        if (!BYPASS_INTERNAL_FLS_AND_CRUD && !objType.getDescribe().isUpdateable())
            throw new CrudException(OperationType.U, objType);

        return true ;
    }
    
    public static Boolean checkObjectIsDeletable(SObjectType objType)
    {
        
        if (!BYPASS_INTERNAL_FLS_AND_CRUD && !objType.getDescribe().isDeletable())
            throw new CrudException(OperationType.D, objType);

        return true ;
    }

    public static Boolean checkRead(SObjectType objType, List<String> fieldNames, Boolean notImplyNamespace)
    {
        checkObjectIsReadable(objType);
        for (String fieldName : fieldNames)
        {
            checkFieldIsReadable(objType, fieldName, notImplyNamespace);
        }

        return true ;
    }

    public static void checkFieldIsReadable(SObjectType objType, String fieldName, Boolean notImplyNamespace)
    {
        checkFieldIsReadable(getFieldDescribe(objType, fieldName, notImplyNamespace), objType);
    }

    public static void checkFieldIsReadable(DescribeFieldResult fieldDescribe, SObjectType objType)
    {
        if (!BYPASS_INTERNAL_FLS_AND_CRUD && !fieldDescribe.isAccessible())
            throw new FlsException(OperationType.R, objType, fieldDescribe.getSObjectField());
    }

    public static Boolean checkInsert(SObjectType objType, List<String> fieldNames, Boolean notImplyNamespace)
    {
        checkObjectIsInsertable(objType);
        for (String fieldName : fieldNames) {
            checkFieldIsInsertable(objType, fieldName, notImplyNamespace);
        }

        return true ;
    }

    public static void checkFieldIsInsertable(SObjectType objType, String fieldName, Boolean notImplyNamespace)
    {
        checkFieldIsInsertable(getFieldDescribe(objType, fieldName, notImplyNamespace), objType);
    }

    public static void checkFieldIsInsertable(DescribeFieldResult fieldDescribe, SObjectType objType)
    {
        if (!BYPASS_INTERNAL_FLS_AND_CRUD && !fieldDescribe.isCreateable())
            throw new FlsException(OperationType.C, objType, fieldDescribe.getSObjectField());
    }

    public static Boolean checkUpdate(SObjectType objType, List<String> fieldNames, Boolean notImplyNamespace)
    {
        checkObjectIsUpdateable(objType);
        for (String fieldName : fieldNames)
        {
            checkFieldIsUpdateable(objType, fieldName, notImplyNamespace);
        }

        // if control reached here, that means no exception
        return true ;
    }

    public static void checkFieldIsUpdateable(SObjectType objType, String fieldName, Boolean notImplyNamespace)
    {
        checkFieldIsUpdateable(getFieldDescribe(objType, fieldName, notImplyNamespace), objType);
    }

    public static void checkFieldIsUpdateable(DescribeFieldResult fieldDescribe, SObjectType objType)
    {
        if (!BYPASS_INTERNAL_FLS_AND_CRUD && !fieldDescribe.isUpdateable())
            throw new FlsException(OperationType.U, objType, fieldDescribe.getSObjectField());
    }


    public static DescribeFieldResult getFieldDescribe(SObjectType objType, String fieldName, Boolean notImplyNamespace)
    {
        DescribeFieldResult fieldDescribe = objCache.describeField(objType, fieldName, notImplyNamespace);
        if (fieldDescribe == null)
            throw new SecurityException(String.format(System.Label.Common_Error_Invalid_Field, new List<String>{fieldName, objType.getDescribe().getLabel()}));

        return fieldDescribe;
    }

    // Custom class instance
    public static SObjectDescribe objCache = new SObjectDescribe();




    public virtual class SecurityException extends Exception {
        protected OperationType m_operation;
        protected Schema.SObjectType m_objectType;
    }

    /**
     * CrudException represents a running user's lack of read/create/update/delete access at a profile (or permission set)
     * level. Sharing and field level security issues will never cause this.
     **/
    public class CrudException extends SecurityException{
        
        private CrudException(OperationType operation, Schema.SObjectType objectType){
            this.m_operation = operation;
            this.m_objectType = objectType;
            if(operation == OperationType.C)
                this.setMessage(System.Label.Security_Error_Object_Not_Insertable);
            else if(operation == OperationType.R)
                this.setMessage(System.Label.Security_Error_Object_Not_Readable);
            else if(operation == OperationType.U)
                this.setMessage(System.Label.Security_Error_Object_Not_Updateable);
            else if(operation == OperationType.D)
                this.setMessage(System.Label.Security_Error_Object_Not_Deletable);

            this.setMessage( String.format( this.getMessage(), new List<String>{objectType.getDescribe().getLabel()} ));
        }
    }
    /**
     * FlsException represents a running user's lack of field level security to a specific field at a profile (or permission set) level
     * Sharing and CRUD security issues will never cause this to be thrown.
     **/
    public class FlsException extends SecurityException{
        private Schema.SObjectField m_fieldToken;

        private FlsException(OperationType operation, Schema.SObjectType objectType, Schema.SObjectField fieldToken){
            this.m_operation = operation;
            this.m_objectType = objectType;
            this.m_fieldToken = fieldToken;
            if(operation == OperationType.C)
                this.setMessage(System.Label.Security_Error_Field_Not_Insertable);
            else if(operation == OperationType.R)
                this.setMessage(System.Label.Security_Error_Field_Not_Readable);
            else if(operation == OperationType.U)
                this.setMessage(System.Label.Security_Error_Field_Not_Updateable);

            this.setMessage(String.format(this.getMessage(), new List<String>{ objectType.getDescribe().getLabel(), fieldToken.getDescribe().getLabel()}));
        }
    }
}