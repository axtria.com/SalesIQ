/**
* Description 	: Controller for Request Inline vf page to show MAP. 
* Author 		: Arun.Kumar
* Since 		: 29-June-2016
* Version 		: 1.0
*/
global without sharing class MAPInlineCtrl{ 
	
	//Added an instance varaible for the standard controller
    private ApexPages.StandardController controller {get; set;}
    // the actual account
    private Change_Request__c chngReq;
    public string strZIPs {get;set;}// impacted zips while lasso
    public static String namespace {get{ return SalesIQGlobalConstants.NAME_SPACE;} set;}
    public string strAccounts {get;set;}
    public string AlignmentType {get;set;}
    public string AlignmentPeriod {get;set;}
    public string rootPositionCode {get;set;}
    public string TeamInstance {get;set;}
    public string TeamInstanceCode {get;set;}
    public string PositionTeamInstanceQuery {get;set;}
    public string selectedCycleName {get;set;}
    public map<string,string> hierarchyLevelMap {get;set;}
    public integer noOfLevel{get;set;}
    public string mapAccountClusterFlag{get;set;}
    public string initialExtents{get;set;}
    public string selectedMovementType {get;set;}
    public string posLevel {get;set;}
    public  string selectedCountry {get;set;}

    /*variables for globalisation story*/
    public string geographyLayer {get;set;}
    public string geographyEndPoint {get;set;}
    public string geographyTable {get;set;}
    public string cityLayer {get;set;}
    public Boolean accountShapeAvailable {get;set;}
    public Boolean geographyShapeAvailable {get;set;}
	public String userLocaleCountry{
        get{
         userLocaleCountry=SalesIqUtility.getUserLocaleCountry();
         return userLocaleCountry;
        }set;}

	list<string> MovedAccountIds ;
	 
	
	public MAPInlineCtrl(ApexPages.StandardController controller){
		try{
			strZIPs='';
			selectedMovementType = '';
			MovedAccountIds = new list<String>();
			this.controller = controller;
	        this.chngReq = (Change_Request__c)controller.getRecord();	
	        list<string> lstRequestID = new list<string>();
	        lstRequestID.add(chngReq.id);
	        set<string> UniqueZips = new set<string>();
	        hierarchyLevelMap = new map<string,string>();
	        
	        
	        //if(Alignment_Global_Settings__c.getValues('MapAccountClusterFlag') != null)
	         // mapAccountClusterFlag = Alignment_Global_Settings__c.getValues('MapAccountClusterFlag').Tree_Hierarchy_Sort_Field__c;
	          
	        mapAccountClusterFlag = 'false';
	          
	        list<Change_Request__c> lstRequest = RequestUtilityClass.getChangeRequestByRequestID(lstRequestID);
	        system.debug('******* listRequest ' + lstRequest);
	        if(lstRequest!=null && lstRequest.size() > 0){
	        	for(Change_Request__c cr:lstRequest){
	        	    selectedMovementType = cr.Request_Type_Change__c;
	        	    selectedMovementType  =selectedMovementType.split(' ')[0];
	        	    system.debug('selectedMovementType '+selectedMovementType);
	        		if(cr.Request_Type_Change__c==SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP){
	        			system.debug('****** Inside Iffff');
	        			strZIPs = cr.AllZips__c;
	        		}
	        		else{
	        			//Accountnumber = cr.Account_Moved_Id__c;
	        			system.debug('****** Inside elseeee');
	        			strAccounts = cr.Account_Moved_Id__c;
	        			System.debug(strAccounts);
	        			system.debug('*********** Moved Accounts ' + strAccounts);
			        	/*MovedAccountIds = strAccounts.split(',');  // list of moved accounts
			        	list<Team_Instance_Account__c> lstAcc = AlignmentUtility.getTeamInstanceAccountByAccountNumber(MovedAccountIds, TeamInstance);		        	
			        	if(lstAcc!=null && lstAcc.size() > 0){
			        		for(Team_Instance_Account__c TeamAcc: lstAcc){
			        			UniqueZips.add(TeamAcc.Account_ID__r.billingpostalcode);
			        		}                		
			        	}		        	
			        	if(UniqueZips!=null && UniqueZips.size()>0){
			        		for(string Zips :UniqueZips ){        			
			        			strZIPs +=strZIPs+','+Zips+',';
			        		}
			        	}*/
	        			
	        		}
	        		system.debug('********** Alignment Type ' + cr.Request_Type_Change__c);
	        		AlignmentType = cr.Team_Instance_ID__r.Alignment_Type__c;
	        		TeamInstance = cr.Team_Instance_ID__c;
	        		TeamInstanceCode = cr.Team_Instance_ID__r.Team_Instance_Code__c;
                    AlignmentPeriod=cr.Team_Instance_ID__r.Alignment_Period__c;
	        		selectedCycleName = cr.Team_Instance_ID__r.Name;
                    geographyLayer =cr.Team_Instance_ID__r.Geography_Type_Name__r.Geography_Layer__c;
                    geographyEndPoint=cr.Team_Instance_ID__r.Geography_Type_Name__r.Geography_End_Point__c;
                    geographyTable=cr.Team_Instance_ID__r.Geography_Type_Name__r.Geography_Table__c;
                    cityLayer=cr.Team_Instance_ID__r.Geography_Type_Name__r.City_Layer__c;
                    selectedCountry=cr.Team_Instance_ID__r.Team__r.Country__c;
                    accountShapeAvailable=cr.Team_Instance_ID__r.Geography_Type_Name__r.Account_Shape_Available__c;
                    geographyShapeAvailable=cr.Team_Instance_ID__r.Geography_Type_Name__r.Shape_Available__c; //Shape_Available__c
                    posLevel ='4';

    	        		
	        	}
	        	if(strZIPs!=''){
	        		strZIPs = strZIPs.removeEnd(',');
	        		strZIPs = strZIPs.removeStart(',');     
	        	}
	        	   	
	        	UserAccessPermission(UserInfo.getUserId(), TeamInstance);
	        	string strUnassigned = SalesIQGlobalConstants.UNASSIGNED_TERRITORY_CODE;
	        	PositionTeamInstanceQuery = 'Select '+SalesIQGlobalConstants.NAME_SPACE+'Position_ID__r.'+ SalesIQGlobalConstants.NAME_SPACE +'RGB__c, '+SalesIQGlobalConstants.NAME_SPACE+'Position_ID__r.'+SalesIQGlobalConstants.NAME_SPACE+'Client_Position_Code__c, '+SalesIQGlobalConstants.NAME_SPACE+'Position_ID__r.Name, '+SalesIQGlobalConstants.NAME_SPACE+'Position_ID__r.'+SalesIQGlobalConstants.NAME_SPACE+'Position_Type__c , ' + SalesIQGlobalConstants.NAME_SPACE+'Position_ID__r.'+SalesIQGlobalConstants.NAME_SPACE+'Hierarchy_Level__c , '+ SalesIQGlobalConstants.NAME_SPACE+'Position_ID__c, '+SalesIQGlobalConstants.NAME_SPACE+'Centroid_Longitude__c, '+SalesIQGlobalConstants.NAME_SPACE+'Centroid_Latitude__c, '+SalesIQGlobalConstants.NAME_SPACE+'X_Min__c, '+SalesIQGlobalConstants.NAME_SPACE+'Y_Min__c, '+SalesIQGlobalConstants.NAME_SPACE+'X_Max__c, '+SalesIQGlobalConstants.NAME_SPACE+'Y_Max__c From '+SalesIQGlobalConstants.NAME_SPACE+'Position_Team_Instance__c where ';
	        	PositionTeamInstanceQuery += SalesIQGlobalConstants.NAME_SPACE+'Team_Instance_ID__c = \'' + TeamInstance +'\'';
	        	//PositionTeamInstanceQuery +=' and ('+SalesIQGlobalConstants.NAME_SPACE+'Position_ID__c = \'' + rootPositionCode +'\' OR '+SalesIQGlobalConstants.NAME_SPACE+'Position_ID__r.'+SalesIQGlobalConstants.NAME_SPACE+'Parent_Position__c = \'' + rootPositionCode  + '\' OR '+SalesIQGlobalConstants.NAME_SPACE+'Position_ID__r.'+SalesIQGlobalConstants.NAME_SPACE+'Parent_Position__r.'+SalesIQGlobalConstants.NAME_SPACE+'Parent_Position__c = \'' + rootPositionCode + '\' OR '+SalesIQGlobalConstants.NAME_SPACE+'Position_ID__r.'+SalesIQGlobalConstants.NAME_SPACE+'Parent_Position__r.'+SalesIQGlobalConstants.NAME_SPACE+'Parent_Position__r.'+SalesIQGlobalConstants.NAME_SPACE+'Parent_Position__c = \'' + rootPositionCode + '\' or '+SalesIQGlobalConstants.NAME_SPACE+'Position_ID__r.Name = \''+ strUnassigned + '\')';
	        	PositionTeamInstanceQuery +=' and ('+SalesIQGlobalConstants.NAME_SPACE+'Position_ID__c = \'' + rootPositionCode +'\' OR '+SalesIQGlobalConstants.NAME_SPACE+'Position_ID__r.'+SalesIQGlobalConstants.NAME_SPACE+'Parent_Position__c = \'' + rootPositionCode  + '\' OR '+SalesIQGlobalConstants.NAME_SPACE+'Position_ID__r.'+SalesIQGlobalConstants.NAME_SPACE+'Parent_Position__r.'+SalesIQGlobalConstants.NAME_SPACE+'Parent_Position__c = \'' + rootPositionCode + '\' OR '+SalesIQGlobalConstants.NAME_SPACE+'Position_ID__r.'+SalesIQGlobalConstants.NAME_SPACE+'Parent_Position__r.'+SalesIQGlobalConstants.NAME_SPACE+'Parent_Position__r.'+SalesIQGlobalConstants.NAME_SPACE+'Parent_Position__c = \'' + rootPositionCode + '\' or '+SalesIQGlobalConstants.NAME_SPACE+'Position_ID__r.'+SalesIQGlobalConstants.NAME_SPACE+'Parent_Position__r.'+SalesIQGlobalConstants.NAME_SPACE+'Parent_Position__r.'+SalesIQGlobalConstants.NAME_SPACE+'Parent_Position__r.'+SalesIQGlobalConstants.NAME_SPACE+'Parent_Position__c = \'' + rootPositionCode + '\' or '+SalesIQGlobalConstants.NAME_SPACE+'Position_ID__r.'+SalesIQGlobalConstants.NAME_SPACE+'Client_Position_Code__c = \''+ strUnassigned + '\')';
	        	//system.debug('#### TeamInstance '+TeamInstance);
	            //hierarchyLevelMap = SalesIQUtility.getPositionTypeByHierarchy(TeamInstance);
	        	//system.debug('#### noOfLevel '+hierarchyLevelMap);
	        	
	        	
	        }    
        
        }catch(Exception e){
        
        	System.debug('message ::'+e.getLineNumber()+':  '+e.getmessage());
        	System.debug(e.getstackTraceString());
        	SalesIQLogger.logMessage(e);
        }  	
	}
	
	/*
	* This method will provide the rootposition id of loggedin User
    * @param  string of Logged in User
    * @param  string of Selected Team Instance
    * @return void
    */ 
	private void UserAccessPermission(string loggedInUserID, string TeamInstance){
		try{
			 list<User_Access_Permission__c> ObjAccess = SalesIQUtility.getUserAccessPermistion(loggedInUserID,TeamInstance );
			 system.debug('ObjAccess '+ObjAccess);
			 if(ObjAccess!=null && ObjAccess.size() > 0){
			 	for(User_Access_Permission__c objPer:ObjAccess){
			 		rootPositionCode = objPer.Map_Access_Position__c;
			 		noOfLevel = integer.valueof(objPer.Position__r.Hierarchy_Level__c);
			 	}
			 	
			 	
			 }
			 else{
			 	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, System.Label.No_UAP_Assigned));
			 }
			
		}catch(Exception e){
            system.debug('#### EXCEPTION in MAPInlineCtrl UserAccessPermission'+e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'ERROR:'+e.getMessage()));
        }
	}
    @RemoteAction   
    public static map<string,string> getAccountInfoValues(String accountNumber, String teamInstance,String alignmentPeriod){
        try {
            map<string,string> accInfoMap = new map<string,string>();
            map<string,string> accInfoAttributesMap = new map<string,string>();
            string objName;
            for(Team_Instance_Object_Attribute__c att : SalesIQUtility.getTeamInstanceAttr(new list<string>{SalesIQGlobalConstants.INFO_TYPE_ACCOUNT}, teamInstance,true)){
                objName = att.Object_Name__c;
                accInfoAttributesMap.put(att.Attribute_API_Name__c, att.Attribute_Display_Name__c+'#'+att.Section__c);
            }
            
            string soql = 'SELECT Id';
            list<string> POSITION_ACCOUNT_READ_FIELDS = new list<string>();
            
            for(string fld : accInfoAttributesMap.keyset()){
                soql += ', '+fld;
                POSITION_ACCOUNT_READ_FIELDS.add(fld);
            }
            soql += ',Account__r.BillingCity,Account__r.BillingStreet,Account__r.BillingState,Account__r.BillingPostalCode FROM '+objName+' WHERE Account__r.AccountNumber =: accountNumber';
            if(alignmentPeriod == SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE){
                soql += ' and Assignment_Status__c = \'Active\'';
            }else if(alignmentPeriod == SalesIQGlobalConstants.FUTURE_TEAM_CYCLE_TYPE){
                soql += ' and Assignment_Status__c = \'Future Active\'';
            }else{
                soql += ' and Assignment_Status__c = \'Inactive\'';
            }
                soql += 'and Team_Instance__c =:teamInstance and Position__r.inactive__c=false Limit 1';
            
            list<Position_Account__c> posAcc;
            if(SecurityUtil.checkRead(Position_Account__c.SObjectType, POSITION_ACCOUNT_READ_FIELDS, false) && 
                   SecurityUtil.checkRead(Position__c.SObjectType, new list<string>{'Name'}, false) 
                   ){
                 posAcc = database.query(soql);
            }
            if(posAcc.size() == 1){
             /*Added by Harkirat - for Account info Change Request Id*/
                 List<CR_Account__c> crAcc=[Select Change_Request__r.id,Change_Request__r.Name,Account__r.id From CR_Account__c where Account__r.AccountNumber=:accountNumber AND Change_Request__r.Team_Instance_id__c =:teamInstance AND  Change_Request__r.Status__c='Pending'];
                 List<Account_Affiliation__c> parentAcc;
                 if(SecurityUtil.checkRead(Account_Affiliation__c.SObjectType, new List<String>{'Account__c', 'Affiliation_Network__c', 'Parent_Account__c', 'Root_Account__c'}, false) && SecurityUtil.checkRead(Account.SObjectType, new List<String>{'Name','AccountNumber'}, false)){
                        parentAcc=  [select Account__c, Affiliation_Network__c, Parent_Account__c, Parent_Account__r.AccountNumber, Account__r.Name, Account__r.AccountNumber, Parent_Account__r.Name, Root_Account__c, Root_Account__r.AccountNumber from Account_Affiliation__c where Account__r.AccountNumber=:accountNumber Limit 1];

                 }
                for(string fld : accInfoAttributesMap.keyset()){
                    list<string> relFields = fld.split('\\.');
                    if(relFields.size() == 3){
                        if(posAcc[0].getSObject(relFields[0]).getSObject(relFields[1])!=null){
                         accInfoMap.put(accInfoAttributesMap.get(fld),relFields[relFields.size()-1]+ '#'+ posAcc[0].getSObject(relFields[0]).getSObject(relFields[1]).get(relFields[2]));
                        }else{
                         accInfoMap.put(accInfoAttributesMap.get(fld),relFields[relFields.size()-1]+ '#'+ ' ');
                        }
                    }else if(relFields.size() == 2){
                        accInfoMap.put(accInfoAttributesMap.get(fld),relFields[relFields.size()-1]+ '#'+ string.valueOf(posAcc[0].getSObject(relFields[0]).get(relFields[1])));
                    }else if(relFields.size() == 1){
                        accInfoMap.put(accInfoAttributesMap.get(fld),fld + '#'+ string.valueOf(posAcc[0].get(fld)));
                    }else{
                        //No fields to return
                    }
                }
                if(crAcc.size()>=1){
                    accInfoMap.put('Change Request','ChangeRequest#'+ string.valueOf(crAcc[0].Change_Request__r.Name));
                    accInfoMap.put('Change Request Id','CRid#'+ string.valueOf(crAcc[0].Change_Request__r.id));
                }else{
                    accInfoMap.put('Change Request','ChangeRequest#');
                    accInfoMap.put('Change Request Id','CRid#');//BillingPostalCode
                }
                if(parentAcc.size()>=1){
                    if(parentAcc[0].Parent_Account__c!=null)
                    accInfoMap.put('Parent Account','ParentAccount#'+ string.valueOf(parentAcc[0].Parent_Account__r.Name));
                    else
                    accInfoMap.put('Parent Account','ParentAccount#'); 
                }else{
                    accInfoMap.put('Parent Account','ParentAccount#');
                }
                accInfoMap.put('Address','Address#'+string.valueOf(posAcc[0].Account__r.BillingStreet)+', '+string.valueOf(posAcc[0].Account__r.BillingState)+', '+string.valueOf(posAcc[0].Account__r.BillingPostalCode));
                    
            }else{
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.ZIP_Not_Found));
                //notification-toast
                 accInfoMap.put( System.Label.Message_Theme_Error ,System.Label.ZIP_Not_Found);
            }
            
            // Commented by Arun
        //for(string str : zipInfoMap.keyset()){
              //  system.debug('####'+str+' : '+zipInfoMap.get(str));
            //}
            return accInfoMap;
        }catch(Exception e){
            SalesIQLogger.logCatchedException(e, true, SalesIQLogger.ALIGNMENT_MODULE);    
            return null ;       
        }
	}

    /*
    @DEPRECATED
    global static map<string,list<Position_Account__c>> getAccountData(string rootPositionId, string selectedCycle, decimal xmin, decimal ymin, decimal xmax, decimal ymax){
       return null;
    }
    */
	
	/*
	* For loading extent based account data
    * @return PageReference, it will redirect to same page after submitting the request
    */
	@RemoteAction	 
    global static map<string,List<MAPInlineCtrl.AccountDataWrapper>> getAccountDataforChangeRequest(string rootPositionId, string selectedCycle,Integer level,decimal xmin, decimal ymin, decimal xmax, decimal ymax){
        try{       
            map<string,List<MAPInlineCtrl.AccountDataWrapper>> mapNew =new map<string,List<MAPInlineCtrl.AccountDataWrapper>>();
            map<string,list<SalesIQUtility.AccountDataWrapper>> mapDummy = SalesIQUtility.getAccountData(new set<string>{rootPositionId},selectedCycle,level,xmin,ymin,xmax,ymax );
            
            for(String posCode : mapDummy.keySet()){
                list<MAPInlineCtrl.AccountDataWrapper> wrapperNewList = new list<MAPInlineCtrl.AccountDataWrapper>();
                
                list<SalesIQUtility.AccountDataWrapper> retrivedWrapper = mapDummy.get(posCode);

                for(SalesIQUtility.AccountDataWrapper objWrapper : retrivedWrapper){
                    wrapperNewList.add(new MAPInlineCtrl.AccountDataWrapper(objWrapper.posAcc, objWrapper.name, objWrapper.changeRequestId));
                }
                     
                mapNew.put(posCode, wrapperNewList);
            }

            return mapNew;
        }catch(Exception e){
            system.debug('#### EXCEPTION in MAPInlineCtrl getAccountData '+e.getMessage());
            return null;
        }
    }
    @RemoteAction    
    global static map<string,List<Position_Account__c>> getAccountData(string rootPositionId, string selectedCycle,decimal xmin, decimal ymin, decimal xmax, decimal ymax){
        /*Not in use
        I left this method blank, because it was declared as global.
        and you can't change the method signature and paramter for global method once method get included in
        packages
          */
        return null;

    }
      @RemoteAction    
    global static map<string,List<Position_Account__c>> getAccountData(string rootPositionId, string selectedCycle,Integer level,decimal xmin, decimal ymin, decimal xmax, decimal ymax){
        /*Not in use
        I left this method blank, because it was declared as global.
        and you can't change the method signature and paramter for global method once method get included in
        packages
          */
        return null;

    }
    @RemoteAction
    global static AggregateResult getExtentsforRequestGeography(string listGeo,string selectedCountry){ 

        List<String> splitStrings = listGeo.split(':') ;
        listGeo = splitStrings[0] ;

        String alignmentType ;
        if(splitStrings.size() > 1)
            alignmentType = splitStrings[1] ;

    	AggregateResult Max_Min_values  =null;     
        list<string> arr1 = Listgeo.split(',',0);
        list<string> GEOGRAPHY_READ_FIELD = new list<string>{'X_Max__c','X_Min__c','Y_Min__c','Y_Max__c','name'};
        list<String> ACCOUNT_READ_FIELD = new list<String>{'BillingLatitude','BillingLatitude'};
        if(alignmentType == SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP){
        if(SecurityUtil.checkRead(Geography__c.SObjectType, GEOGRAPHY_READ_FIELD, false)){
	        		Max_Min_values = [select Max(X_Max__c) max_x ,Min(X_Min__c) min_x ,Min(Y_Min__c) min_y,MAx(Y_Max__c) max_y from Geography__c where name in: arr1  and External_Country_Id__c =:selectedCountry];
	        }
        }else{
        	if(SecurityUtil.checkRead(Account.SObjectType, ACCOUNT_READ_FIELD, false)){
        		Max_Min_values = [select Max(BillingLatitude) max_y, Min(BillingLatitude) min_y,  Max(BillingLongitude) max_x ,Min(BillingLongitude) min_x  from Account where AccountNumber in: arr1 and External_Country_Id__c =:selectedCountry];
        	}
        }
        
        return  Max_Min_values;        
    }  
	
    @RemoteAction
    global static AggregateResult getExtents(string listGeo){ 

        List<String> splitStrings = listGeo.split(':') ;
        listGeo = splitStrings[0] ;

        String alignmentType ;
        if(splitStrings.size() > 1)
            alignmentType = splitStrings[1] ;

    	AggregateResult Max_Min_values  =null;     
        list<string> arr1 = Listgeo.split(',',0);
        list<string> GEOGRAPHY_READ_FIELD = new list<string>{'X_Max__c','X_Min__c','Y_Min__c','Y_Max__c','name'};
        list<String> ACCOUNT_READ_FIELD = new list<String>{'BillingLatitude','BillingLatitude'};
        if(alignmentType == SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP){
        if(SecurityUtil.checkRead(Geography__c.SObjectType, GEOGRAPHY_READ_FIELD, false)){
	        		Max_Min_values = [select Max(X_Max__c) max_x ,Min(X_Min__c) min_x ,Min(Y_Min__c) min_y,MAx(Y_Max__c) max_y from Geography__c where name in: arr1];
	        }
        }else{
        	if(SecurityUtil.checkRead(Account.SObjectType, ACCOUNT_READ_FIELD, false)){
        		Max_Min_values = [select Max(BillingLatitude) max_y, Min(BillingLatitude) min_y,  Max(BillingLongitude) max_x ,Min(BillingLongitude) min_x  from Account where AccountNumber in: arr1];
        	}
        }
        
        return  Max_Min_values;        
    }    
    
    public map<string,CR_Team_Instance_Config__c> layerCongigMap{get;set;}
    
    public void getLayerConfig(){
    	system.debug('TeamInstance in getLayerConfig' + TeamInstance);
        layerCongigMap = new map<String, CR_Team_Instance_Config__c>();  
        
        list<CR_Team_Instance_Config__c> lstLayerConfig = AlignmentUtility.getTeamInstanceConfiguration(TeamInstance,SalesIQGlobalConstants.LAYER_CONFIG);
        
        for(CR_Team_Instance_Config__c layerConfig : lstLayerConfig){
            layerCongigMap.put(layerConfig.Configuration_Name__c,layerConfig);
        
        }
       // system.debug('layerCongigMap :::'+layerCongigMap);
        //return layerCongigMap;
    }
    

    @RemoteAction
    global static map<string,String> getPendingAccountData(string TeamInstance){
        // DEPRECATED METHOD
        return null ;
    }
    
    @RemoteAction
	global static string getOrgNameSpace(){
    	return SalesIQGlobalConstants.getOrgNameSpace();
    }
    
     /*
     * This Method is used to return the columns to be shown on Account info popup
     * @Param: teamInstance(string)  
     * @return Map<string,string> having field api name and label
    */
    @RemoteAction   
    global static map<string,string> getAccountInfoDetails(String teamInstance){
        map<string,string> accInfoMap = new map<string,string>();
        list<string> interfaceName = new list<String> {SalesIQGlobalConstants.INFO_TYPE_ACCOUNT};
        for(Team_Instance_Object_Attribute__c teamAttr : SalesIQUtility.getTeamInstanceAttr(interfaceName,teamInstance,true)){
            accInfoMap.put(teamAttr.Attribute_API_Name__c, teamAttr.Attribute_Display_Name__c);
        }
        return accInfoMap;
    }
    
      @RemoteAction
    global static list<User_Access_Permission__c> getTeamAndTeamInstances(){
         try { 
            system.debug('------------layerSourceList------------');
            list<User_Access_Permission__c> layerSourceList = SalesIQUtility.getUserAccessPermistion(UserInfo.getUserId());
            return layerSourceList;
           } 
           catch(Exception e) {
            SalesIQLogger.logCatchedException(e, true, SalesIQLogger.ALIGNMENT_MODULE);
            return null ;            
           }
    }
    
    // get enabled metric from teaminstanceobjattrdetail - Lagnika
    @RemoteAction
    global static map<String,list<Team_Instance_Object_Attribute_Detail__c>> getAccountRenderingAttributes(String selectedCycle){
    	
    	map<String,list<Team_Instance_Object_Attribute_Detail__c>> mapAccountMetricDetails = new map<String,list<Team_Instance_Object_Attribute_Detail__c>>();
    	list<Team_Instance_Object_Attribute_Detail__c> objAttrDetailList = SalesIQUtility.getTeamInstanceAttrDetail(selectedCycle, SalesIQGlobalConstants.ACCOUNT_RENDERING_INTERFACE_NAME);
    	 
    	if(objAttrDetailList.size() > 0){
    		mapAccountMetricDetails.put(objAttrDetailList[0].Object_Attibute_Team_Instance__r.Attribute_API_Name__c,objAttrDetailList);
    	}
    	return mapAccountMetricDetails;
    
    }
    global class AccountDataWrapper {
        public Position_Account__c posAcc {get; set;}
        public String name {get; set;}
        public String changeRequestId{get;set;}
        public AccountDataWrapper(Position_Account__c p,String nam,String crId) {
            posAcc = p;
            name  = nam;
            changeRequestId=crId;
        }

    }
}