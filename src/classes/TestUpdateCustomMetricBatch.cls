@istest
public class TestUpdateCustomMetricBatch{
	static testMethod void testCustomMetric(){

		Organization_Master__c org = CallPlanTestDataFactory.createOrganizationMasterRecord();
        insert org;
        Country__c country = CallPlanTestDataFactory.createCountryMasterRecord(org.id);
        insert country;
        Geography_Type__c geotype = CallPlanTestDataFactory.createGeographyTypeRecord(country.id);
        insert geotype;

		Team__c team = CallPlanTestDataFactory.createTeam('ZIP');
        insert team;
        
        Team_Instance__c baseTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(team,'ZIP');
        baseTeamInstance.Alignment_Period__c = 'Future';
        baseTeamInstance.IC_EffstartDate__c = Date.today().addMonths(1);
        baseTeamInstance.IC_EffEndDate__c = Date.today().addYears(1);
        baseTeamInstance.Team_Instance_Code__c = 'DI-00004';
        insert baseTeamInstance;

        Team_Instance__c counterTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(team,'ZIP');
        counterTeamInstance.Alignment_Period__c = 'Future';
        counterTeamInstance.IC_EffstartDate__c = Date.today().addMonths(1);
        counterTeamInstance.IC_EffEndDate__c = Date.today().addYears(1);
        counterTeamInstance.Team_Instance_Code__c = 'DI-00005';
        insert counterTeamInstance;

        Team_Instance_Mapping__c objMapping= new Team_Instance_Mapping__c();
        objMapping.controlling_Team_instance__c  = baseTeamInstance.id;
        objMapping.Team_Instance1__c  = counterTeamInstance.id;
        insert objMapping;

        Position__c territory = new Position__c();
        territory.name = 'USA';
        territory.Client_Territory_Name__c = 'North east';
        territory.Client_Position_Code__c = '1NE30000';
        territory.Client_Territory_Code__c='1NE30000';
        territory.Position_Type__c='territory';
        territory.inactive__c = false;
        territory.RGB__c = '41,210,117';
        territory.Team_iD__c = team.id;
        territory.Hierarchy_Level__c='1';
        territory.Related_Position_Type__c  ='Base';
        territory.Effective_End_Date__c=Date.today().addMonths(50);
        territory.Team_Instance__c =  baseTeamInstance.id;
        territory.Is_Unassigned_Position__c  =false;
        insert territory;
        system.debug('##### territory ' + territory);

        Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, null, baseTeamInstance);
        territoryTeamInstance.X_Max__c=-72.6966429900;
        territoryTeamInstance.X_Min__c=-73.9625820000;
        territoryTeamInstance.Y_Max__c=40.9666490000;
        territoryTeamInstance.Y_Min__c=40.5821279800;
        insert territoryTeamInstance;

        Position__c territory2 = new Position__c();
        territory2.name = 'USA';
        territory2.Client_Territory_Name__c = 'North east';
        territory2.Client_Position_Code__c = '1NE30001';
        territory2.Client_Territory_Code__c='1NE30001';
        territory2.Position_Type__c='territory';
        territory2.inactive__c = false;
        territory2.RGB__c = '41,210,117';
        territory2.Team_iD__c = team.id;
        territory2.Hierarchy_Level__c='1';
        territory2.Related_Position_Type__c  ='Base';
        territory2.Effective_End_Date__c=Date.today().addMonths(50);
        territory2.Team_Instance__c =  baseTeamInstance.id;
        territory2.Is_Unassigned_Position__c  =false;
        insert territory2;


		Position_Team_Instance__c territoryTeamInstance2 = CallPlanTestDataFactory.CreatePosTeamInstance(territory2, null, baseTeamInstance);
        territoryTeamInstance2.X_Max__c=-72.6966429900;
        territoryTeamInstance2.X_Min__c=-73.9625820000;
        territoryTeamInstance2.Y_Max__c=40.9666490000;
        territoryTeamInstance2.Y_Min__c=40.5821279800;
        insert territoryTeamInstance2;

        Geography__c geo1 = new Geography__c();
        geo1.Name = '11788';
        geo1.City__c='Test 11788';
        geo1.State__c = '11788';
        geo1.Neighbor_Geography__c = '11789';
        geo1.Zip_Type__c = 'Standard';
        geo1.Centroid_Latitude__c = 0.0;
        geo1.Centroid_Longitude__c = 0.0;
        geo1.External_Country_Id__c=country.id;
        geo1.External_Geo_Type__c= geotype.id;
        geo1.Geography_Type__c= geotype.id;
        insert geo1; 

        Geography__c geo2 = new Geography__c();
        geo2.Name = '11789';
        geo2.City__c='Test 11789';
        geo2.State__c = '11789';
        geo2.Neighbor_Geography__c = '11788';
        geo2.Zip_Type__c = 'Standard';
        geo2.Centroid_Latitude__c = 0.0;
        geo2.Centroid_Longitude__c = 0.0;
        geo2.External_Country_Id__c=country.id;
        geo2.External_Geo_Type__c= geotype.id;
        geo2.Geography_Type__c= geotype.id;
        insert geo2; 

        Position_Geography__c posGeo1  = new Position_Geography__c();
        posGeo1.Effective_Start_Date__c  = Date.today().addMonths(1);
        posGeo1.Effective_End_Date__c  = Date.today().addYears(1);
        posGeo1.Geography__c = geo1.id;     
        posGeo1.Position__c = territory.id;
        posGeo1.Team_Instance__c = baseTeamInstance.id ;
        //posGeo1.Team_Instance_Geography__c = teamInsGeo1.id;
        posGeo1.Position_Team_Instance__c = territoryTeamInstance.id;
        posGeo1.Change_Status__c = 'No Change';
        posGeo1.Metric1__c  =2.010000000000;
        posGeo1.Metric2__c  =29.000000000000;
        posGeo1.Metric3__c  =2.000000000000;
        posGeo1.Metric4__c  =1.431479545000;
        posGeo1.Metric5__c  =1.490000000000;
        posGeo1.Metric6__c  =2.010000000000;
        posGeo1.Metric7__c  =29.000000000000;
        posGeo1.Metric8__c  =2.000000000000;
        posGeo1.Metric9__c  =1.431479545000;
        posGeo1.Metric10__c =1.490000000000;
        insert posGeo1;

        system.debug('posGeo1 '+posGeo1);

        Position_Geography__c posGeo2  = new Position_Geography__c();
        posGeo2.Effective_Start_Date__c  = Date.today().addMonths(1);
        posGeo2.Effective_End_Date__c  = Date.today().addYears(1);
        posGeo2.Geography__c = geo2.id;     
        posGeo2.Position__c = territory2.id;
        posGeo2.Team_Instance__c = baseTeamInstance.id ;
        //posGeo1.Team_Instance_Geography__c = teamInsGeo1.id;
        posGeo2.Position_Team_Instance__c = territoryTeamInstance2.id;
        posGeo2.Change_Status__c = 'No Change';
        posGeo2.Metric1__c  =2.010000000000;
        posGeo2.Metric2__c  =29.000000000000;
        posGeo2.Metric3__c  =2.000000000000;
        posGeo2.Metric4__c  =1.431479545000;
        posGeo2.Metric5__c  =1.490000000000;
        posGeo2.Metric6__c  =2.010000000000;
        posGeo2.Metric7__c  =29.000000000000;
        posGeo2.Metric8__c  =2.000000000000;
        posGeo2.Metric9__c  =1.431479545000;
        posGeo2.Metric10__c =1.490000000000;
        insert posGeo2;

        Change_Request_Type__c objRequest = CallPlanTestDataFactory.CreateZipRequestType();
        insert objRequest;

        list<CIM_Config__c> cimList = CallPlanTestDataFactory.createCIMCOnfig(objRequest, baseTeamInstance,'Team_Instance_Geography__c');
        cimList[0].is_Custom_Metric__c = true;
        
        insert cimList;


        test.startTest();
        UpdateCustomMetricBatch obj = new UpdateCustomMetricBatch(baseTeamInstance.id,objRequest.CR_Type_Name__c);
        Database.executeBatch(obj,2);

        test.stopTest();








	}


}