/***********************************************************************************************************************
 * Name         :   LookupComponentCtlr.cls
 * Description  :   Controller for SalesIQLookup lightning component to handle custom lookup search
 * Author       :   Raghvendra Rathore
 * Created On   :   09/22/2017
******************************************************************************************************************************************************/
public class LookupComponentCtlr {

    /**
     * Returns JSON of list of ResultWrapper to Lex Components
     * @objectName - Name of SObject
     * @fld_API_Text - API name of field to display to user while searching
     * @fld_API_Val - API name of field to be returned by Lookup COmponent
     * @lim   - Total number of record to be returned
     * @fld_API_Search - API name of field to be searched
     * @searchText - text to be searched
     * @filter - Additional filter
     * */
    @AuraEnabled 
    public static String searchDB(String objectName, String fld_API_Text, String fld_API_Val, 
                                  Integer lim,String fld_API_Search,String searchText, String filter,String action,string cellField){
        

        System.debug('objectName :'+objectName);
        System.debug('fld_API_Text :'+fld_API_Text);
        System.debug('fld_API_Val :'+fld_API_Val);
        System.debug('fld_API_Search :'+fld_API_Search);
        System.debug('filter :'+filter);
        System.debug('searchText :'+searchText);

        try{
            searchText='\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';
            String lookupfields = '';
            System.debug(fld_API_Search.indexOf('.'));
            
            if(fld_API_Search.indexOf('.') != -1){
                system.debug('-- inside if condtion for lookup fields ---');
                fld_API_Search = fld_API_Search.split('\\.')[1];
                lookupfields = fld_API_Text.split('\\.')[0].replace('__r','__c')+','+fld_API_Text.split('\\.')[1];
            }else{
                lookupfields = 'Name';
                fld_API_Text = 'Name';
                fld_API_Search = 'Name';
            }

            System.debug('lookupfields :'+lookupfields);

            String query = 'SELECT '+lookupfields+' ,'+fld_API_Val+
                           ' FROM '+objectName+
                           ' WHERE '+fld_API_Search+' LIKE '+searchText;
           if(filter != ''){
                
                Boolean executePositionTriggerValidation = false;
                if(Alignment_Global_Settings__c.getValues('ExceutePositionTriggerValidation') != null){
                    if(Alignment_Global_Settings__c.getValues('ExceutePositionTriggerValidation').Tree_Hierarchy_Sort_Field__c == 'True'){
                        executePositionTriggerValidation = true;
                    }
                }
                
                Boolean executePositionChannelValidation = false;
                if(Alignment_Global_Settings__c.getValues('ExceutePositionChannelValidation') != null){
                    String channelInfo = Alignment_Global_Settings__c.getValues('ExceutePositionChannelValidation').Tree_Hierarchy_Sort_Field__c;
                        executePositionChannelValidation = Boolean.valueOf(channelInfo.split(';')[0]);
                        lim = Integer.valueOf(channelInfo.split(';')[1]);
                }

                if(!executePositionTriggerValidation &&  executePositionChannelValidation && action == 'Create Position'){
                    System.debug('query :: '+query);
                    query = query;
                }


                else if(executePositionTriggerValidation && action == 'Create Position' && !executePositionChannelValidation){
                    query += ' AND Team_Instance__c  = \''+filter+'\'';
                }
                else if(cellField == 'Account' && ( action == '' || action == null)){
                    query += ' AND country__c=\''+ filter+'\'';
                }else if(cellField == 'Product' && ( action == '' || action == null)){
                        query += ' AND country__c=\''+ filter+'\'';

                }else{

                    string selectedHLevel = filter.split(';')[0];
                    string teamInstanceId = filter.split(';')[1];
                    string teamType = filter.split(';')[2];

                    system.debug('#### teamInstanceId : '+teamInstanceId);
                    system.debug('#### selectedHLevel : '+selectedHLevel);

                    //Fetch the one level up positionn type from CR Team Instance configuration.
                    list<CR_Team_Instance_Config__c> crTeamInstanceConfigList = [SELECT Configuration_Value__c, Configuration_Name__c, Hierarchy_Level_Number__c FROM CR_Team_Instance_Config__c 
                                                                                 WHERE Configuration_Type__c = 'Hierarchy Configuration' 
                                                                                 AND Hierarchy_Level_Number__c >: Integer.valueOf(selectedHLevel) 
                                                                                 AND Team_Instance__c =: teamInstanceId ORDER BY Hierarchy_Level_Number__c asc limit 1];

                    decimal hLevelNumber = 0;
                    if(crTeamInstanceConfigList.size() == 1){
                        hLevelNumber = crTeamInstanceConfigList[0].Hierarchy_Level_Number__c;
                    }

                    if(teamType == SalesIQGlobalConstants.TEAM_TYPE_OVERLAY){
                        Team_Instance__c teamInstance = SalesIQUtility.getTeamInstanceById(teamInstanceId);
                        system.debug('#### Converging_Level : '+teamInstance.Converging_Level__c);
                        system.debug('#### Hierarchy_Level : '+hLevelNumber);
                        
                        if(teamInstance.Converging_Level__c != 0 && teamInstance.Converging_Level__c <= hLevelNumber){
                            query += ' AND Team_Instance__c = \''+teamInstance.Base_team_Instance__c+'\'';
                        }else{
                            query += ' AND Team_Instance__c = \''+teamInstance.Id+'\'';
                        }
                    }else{
                        query += ' AND Team_Instance__c = \''+teamInstanceId+'\'';
                    }
                    string hLevel = '\''+string.valueOf(hLevelNumber)+'\'';

                    List<Team_Instance_Configuration__c> lstTeamInsCong = [Select id From Team_Instance_Configuration__c Where Configuration_Type__c =: SalesIQGlobalConstants.JAGGED_HIERARCHY and Team_Instance__c =:teamInstanceId];

                    if(lstTeamInsCong.size() > 0)
                        query += ' AND Hierarchy_Level__c >= '+hLevel;
                    else
                        query += ' AND Hierarchy_Level__c = '+hLevel;
                     // code updated to show only active positions in auto search
                    query += ' AND inactive__c = false';
		        }
			
            }
    
            query += ' LIMIT '+lim;
            system.debug('#### query : '+query);
            List<sObject> sobjList = Database.query(query);
            List<ObjectDetailWrapper.LookupDataWrapper> lstRet = new List<ObjectDetailWrapper.LookupDataWrapper>();
            
            for(SObject s : sobjList){
                ObjectDetailWrapper.LookupDataWrapper obj = new ObjectDetailWrapper.LookupDataWrapper();
                obj.objName = objectName;
                if(fld_API_Text.indexOf('.') != -1){

                    fld_API_Text = fld_API_Text.split('\\.')[1];
                    system.debug('-- inside if getting data from position ---'+fld_API_Text);
                }
                obj.text = String.valueOf(s.get(fld_API_Text)) ;
                obj.val = String.valueOf(s.get(fld_API_Val))  ;
                lstRet.add(obj);
            } 
            return JSON.serialize(lstRet) ;
        }catch(Exception e){
            SalesIQLogger.logCatchedException(e,'Positions');
            return e.getMessage();
        }
    }
}
