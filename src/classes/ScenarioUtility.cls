/************************************************************************************************
    Name        :   ScenarioUtility.cls
    Description :   Utility class for scenario related workflow. 
    Author       :   Lagnika Sharma
    Created On   :   06/15/2017
                    
************************************************************************************************/


public with sharing class ScenarioUtility{

 /*@author : Lagnika Sharma
    @date : 6 July 2017  
    @description : This method will fetch Scenario__c records based on  passed parameters (used in trigger)
    @param1: scenarioStages : list of scenarioStage
    @param2 :scenarioStageStatuses : list of scenarioStageStatus
    @param3 : scenarioIds : set  of scenarioIds
    @return : Scenario__c list
    */

    public static List<Scenario__c> getScenarioByRequestName(list<String> scenarioStages, List<String> scenarioStageStatuses, String scenarioType, Set<Id> scenarioIds){
        system.debug('#### scenarioStages : '+scenarioStages);
        system.debug('#### scenarioStageStatuses : '+scenarioStageStatuses);
        system.debug('#### scenarioType : '+scenarioType);
        List<Scenario__c> lstScenario;
        if(SecurityUtil.checkRead(Scenario__c.SObjectType, new List<string>{'File_Path__c','Scenario_Source__c','Request_Process_Stage__c','Team_Instance__c','Source_Team_Instance__c','Effective_End_Date__c','Effective_Start_Date__c'}, false)) {
            if(scenarioIds!=null && scenarioIds.size() > 0){
                lstScenario = [select Skip_Data_Validation__c, File_Path__c, Scenario_Source__c, Source_Team_Instance__c,Team_Instance__c,Effective_End_Date__c,Effective_Start_Date__c, Business_Rule_Template__c from Scenario__c where Request_Process_Stage__c in : scenarioStageStatuses and Scenario_Stage__c in : scenarioStages and Scenario_Type__c =: scenarioType and Scenario_Stage__c !=: SalesIQGlobalConstants.SCENARIO_STATUS_INACTIVE and Id not in : scenarioIds order by CreatedDate limit 1];

            }else{
                lstScenario = [select Skip_Data_Validation__c, File_Path__c, Scenario_Source__c, Source_Team_Instance__c,Team_Instance__c,Effective_End_Date__c,Effective_Start_Date__c, Business_Rule_Template__c from Scenario__c where Request_Process_Stage__c in : scenarioStageStatuses and Scenario_Stage__c in : scenarioStages and Scenario_Type__c =: scenarioType order by CreatedDate limit 1];
            }
        }
        return lstScenario;
    }
    

    /*@author : Lagnika Sharma
    @date : 6 July 2017  
    @description : This method will fetch ETL_Config__c records based on  etl Config Names
    @param1: etlConfigName : etl config name
    @return : ETL_Config__c list
    */
    
    public static List<ETL_Config__c> getETLConfigByName(string etlConfigName){
        List<ETL_Config__c> etlConfigList;
        if(SecurityUtil.checkRead(ETL_Config__c.SObjectType, new List<string>{'Name','End_Point__c','SF_UserName__c','SF_Password__c', 'ESRI_UserName__c', 'ESRI_Password__c','S3_Key_Name__c','S3_Access_Key__c','S3_Security_Token__c'}, false)) {
            etlConfigList = [select Name, Org_Type__c, End_Point__c, SF_UserName__c, SF_Password__c, ESRI_UserName__c, ESRI_Password__c, S3_Key_Name__c, S3_Access_Key__c, S3_Security_Token__c from ETL_Config__c where name =:etlConfigName limit 1];
        }
        return etlConfigList;
    }

    /*@author : Lagnika Sharma
    @date : 6 July 2017  
    @description : This method will fetch Scenario__c records based on  passed ids
    @param1: scenarioIds : set of ScenarioIds
    @return : Scenario__c list
    */
    
    public static List<Scenario__c> getScenarioByScenarioId(Set<Id> scenarioIds){
        List<Scenario__c> scenarioList;
        if(SecurityUtil.checkRead(Scenario__c.SObjectType, new List<string>{'Request_Process_Stage__c'}, false)) {
            scenarioList = [select Request_Process_Stage__c from Scenario__c where id in:scenarioIds];
        }
        return scenarioList;
    }

    /*
    @author : Aditi Jain
    @description : Method to create Activity_Log__c record for Module Integration
    @param1: talendJobName : Name of Activity_Log__c
    @return : Activity_Log__c SObject
    */
    public static Activity_Log__c createIntegrationActivityLog(String talendJobName){
        Activity_Log__c activityLog = new Activity_Log__c(Name = talendJobName, Module__c = 'Integration');
        if(SecurityUtil.checkInsert(Activity_Log__c.SObjectType, new List<string>{'Name','Module__c'}, false)) {
            insert activityLog;
        }
        return activityLog;
    }
    
     /*@author : Lagnika Sharma
    @date : 6 July 2017  
    @description : This method will fetch User_Access_Permission__c records 
    @param1: teamInstances : scenarioTeam In stance set
    @param2: scenarioNames : name of scenarios 
    @param3: sharingType : Explicit/Implicit
    @param4: userRoles : user role for whom User_Access_Permission__c records need to be fetched (this is used for implicit sharing)
    @return : User_Access_Permission__c list
    */


    public static List<User_Access_Permission__c> getUserAccessPermissionRecordFromScenario(Set<String> teamInstances, Set<String> scenarioNames, String sharingType, set<String> userRoles){
        
        List<String> userAccessReadFields = new List<String>{'Is_Active__c','Map_Access_Position__c','Team_Instance__c','Position__c', 'Access_Level__c', 'User__c'};
        List<User_Access_Permission__c> userAcessList = new list<User_Access_Permission__c>();
        
        list<String> whereClauses = new list<String>();
        
        if(SecurityUtil.checkRead(User_Access_Permission__c.SObjectType, userAccessReadFields, false) && SecurityUtil.checkRead(Position__c.SObjectType, new List<String>{'Name'}, false) && 
                SecurityUtil.checkRead(User.SObjectType, new List<String>{'Name'}, false) && SecurityUtil.checkRead(UserRole.SObjectType, new List<String>{'Name'}, false))
        {
            if(teamInstances != null && teamInstances.size() > 0){
                whereClauses.add('Team_Instance__c in: teamInstances');
            }
            if(scenarioNames != null && scenarioNames.size() > 0){
                whereClauses.add('name in: scenarioNames');
            }
            if(sharingType != '' && sharingType != null ) {
                whereClauses.add('Sharing_Type__c =: sharingType');
                //userAcessList = [select Is_Active__c,Map_Access_Position__c, Map_Access_Position__r.Name,Position__c, Team_Instance__c,Access_Level__c,User__c,User__r.Name, User__r.UserRole.Name from User_Access_Permission__c where Team_Instance__c in: teamInstances and Sharing_Type__c =: sharingType];
            }
            if(userRoles != null && userRoles.size() > 0){
                whereClauses.add('User__r.Profile.Name in: userRoles');
            }
            
            String soql = 'select User__r.ProfileID,User__r.Profile.Name,Is_Active__c,Map_Access_Position__c, Map_Access_Position__r.Name,Position__c, Team_Instance__c,Access_Level__c,User__c,User__r.Name, User__r.UserRole.Name,Position__r.Name from User_Access_Permission__c ';
            if(whereClauses.size() > 0){
                soql += ' where ' + String.join(whereClauses, ' and ');
            }
            
            soql += ' LIMIT 50000';
            userAcessList = Database.query(soql);
        }
        return userAcessList;
    }

        
     /*@author : Lagnika Sharma
    @date : 6 July 2017  
    @description : This method will fetch Positions for seleected scenario team instance
    @param1: scenarioteam isnatnce set
    @param2: type of posiiton ex: nation/ district
    @return : Position list
    */
    public static List<Position__c> getPositionFromScenarioTeamInstance(Set<String> teamInsatnces, String posType){
        List<String> positionReadFields = new List<String>{'Name','Team_Instance__c','Position_Type__c'};
        List<Position__c> positionList;
        
        if(SecurityUtil.checkRead(Position__c.SObjectType, positionReadFields, false)){
            if(posType != null)
                positionList = [select Name,Team_Instance__c from Position__c where Team_Instance__c in: teamInsatnces and Position_Type__c =: posType];
            else
                positionList = [select Name,Team_Instance__c from Position__c where Team_Instance__c in: teamInsatnces];                
        }
        return positionList;
        
    }
    
    /*@author : Lagnika Sharma
    @date : 6 July 2017  
    @description : This method will fetch Positions for seleected scenario team instance hierarchy level != 1
    @param1: scenarioteam isnatnce set
    @param2: type of posiiton ex: nation/ district
    @return : Position list
    */
    public static List<Position__c> getPositionAboveTerritoryFromScenarioTeamInstance(Set<String> teamInsatnces){
        List<String> positionReadFields = new List<String>{'Name','Team_Instance__c'};
        List<Position__c> positionList;
        
        if(SecurityUtil.checkRead(Position__c.SObjectType, positionReadFields, false)){
             positionList = [select id, Name,Team_Instance__c from Position__c where Team_Instance__c in: teamInsatnces and Hierarchy_Level__c != '1'];
                        
        }
        return positionList;
        
    }
    
     /*@author : Lagnika Sharma
    @date : 6 July 2017  
    @description : This method will get team instance record of scenario (used for updating team inmstance to read only in scenario trigger) 
    @param1: set scenarioteaminstance
    @return : team isnatnce recorsd.
    */
    
    public static List<Team_Instance__c> getTeamInstancesFromScenario(Set<String> teamInstanceIds){
        List<Team_Instance__c> teamInstanceList;
        List<String> teamInstanceReadFields = new List<String>{'AccountMovement__c','ReadOnlyCreatePosition__c','ReadOnlyDeletePosition__c','ReadOnlyEditPosition__c','Restrict_Hierarchy_Change__c','IsAllowedManageAssignment__c','Restrict_Update_Position_Attribute__c','ZIPMovement__c'};
        
        if(SecurityUtil.checkRead(Team_Instance__c.SObjectType, teamInstanceReadFields, false)){
            teamInstanceList = [Select AccountMovement__c,ReadOnlyCreatePosition__c,ReadOnlyDeletePosition__c,ReadOnlyEditPosition__c,Restrict_Hierarchy_Change__c,IsAllowedManageAssignment__c,Restrict_Update_Position_Attribute__c,ZIPMovement__c from Team_Instance__c where id in: teamInstanceIds];
        }
        return teamInstanceList;
    }
    
     /*@author : Lagnika Sharma
    @date : 6 July 2017  
    @description : This method will fetch employee data 
    @param1: where clause conditions to be applied
    @return : list of employee
    */
    public static List<Employee__c> getEmployeesData(List<String> whereClauses){
        
        List<String> employeeReadFields = new List<String>{'Name', 'Change_status__c', 'Original_Hire_Date__c', 'HR_Termination_Date__c', 'Employee_id__c', 'Manager__c', 'User__c'};
        List<Employee__c> empList;

        if(SecurityUtil.checkRead(Employee__c.SObjectType, employeeReadFields, false) && SecurityUtil.checkRead(User.SObjectType, new List<String>{'Name'}, false)){
            
            String soql = 'select Name, Change_status__c, Original_Hire_Date__c, HR_Termination_Date__c, Employee_id__c, Manager__c, Manager__r.Name from Employee__c';
            if(whereClauses != null && !whereClauses.isEmpty())
                soql += ' where ' + String.join(whereClauses, ' and ');

            empList = (List<Employee__c>)Database.query(soql);
        }
        return empList;
    }

     /*@author : Lagnika Sharma
    @date : 6 July 2017  
    @description : This method will fetch user roles of existiong users in the system
    @param1: scenario teaminstance
    @return :  list of user roles
    */
    public static map<String,Boolean> getUserRolesFromUserPositionFilter(Set<String> teamInstances) {
        
        Set<Id> userRoles = new Set<Id>();
        map<String,Boolean> profileUserAccessMap = new map<String,Boolean>();
      
        for(User_Access_Permission__c permission : [Select User__r.ProfileID,User__r.Profile.Name, Is_Active__c from User_Access_Permission__c where Team_Instance__c in :teamInstances and Sharing_Type__c = 'Implicit' and User__r.ProfileID != null]){
            //userRoles.add(permission.User__r.ProfileID);
            if(permission.User__r.Profile.Name != 'System Administrator')
                profileUserAccessMap.put(permission.User__r.Profile.Name, permission.Is_Active__c);
        }
        
        //return [select id,Name from Profile where id in :userRoles];

        return profileUserAccessMap;
    }
    
     /*@author : Lagnika Sharma
    @date : 6 July 2017  
    @description : This method will fetch pos_emp records for selected scenario
    @param1: scenario team instance set
    @return : list of pos_emp
    */
    
    public static list<Position_Employee__c> getPosEmpForExplicitSharing(set<String> teamInstanceSet){
    
        list<Position_Employee__c> posEmpList = new list<Position_Employee__c>();
        
        posEmpList = [select id,Employee__c,Position__c,Position__r.Team_Instance__c,Effective_End_Date__c,Effective_Start_Date__c from Position_Employee__c where Position__r.Team_Instance__c in: teamInstanceSet];
        
        return posEmpList;
    
    }
    
     /*@author : Lagnika Sharma
    @date : 6 July 2017  
    @description : This method will fetch list of employee
    @return : employee list
    */
    
    public static List<Employee__c> getUserEmployeesData(){
        
        List<String> employeeReadFields = new List<String>{'Name', 'User__c'};
        List<Employee__c> empList;

        if(SecurityUtil.checkRead(Employee__c.SObjectType, employeeReadFields, false) && SecurityUtil.checkRead(User.SObjectType, new List<String>{'Name'}, false)){
            
            String soql = 'select Name, User__c,User__r.UserRole.Name,User__r.Name from Employee__c where User__c != null';

            empList = (List<Employee__c>)Database.query(soql);
        }
        return empList;
    }
    
     /*@author : Lagnika Sharma
    @date : 6 July 2017  
    @description : This method will fetch list of user to delete 
    @return : user pos  list
    */
    
    public static List<User_Access_Permission__c> getUserPosDataToDelete(set<String> userposIds){
        
        List<String> userAccessReadFields = new List<String>{'Is_Active__c','Map_Access_Position__c','Team_Instance__c','Position__c', 'Access_Level__c', 'User__c'};
        List<User_Access_Permission__c> userList = new list<User_Access_Permission__c>();

        if(SecurityUtil.checkRead(User_Access_Permission__c.SObjectType, userAccessReadFields, false)){
            
            String soql = 'select id,Is_Active__c,Map_Access_Position__c,Team_Instance__c,Position__c, Access_Level__c, User__c from User_Access_Permission__c where id in: userPosIds';

            userList = (List<User_Access_Permission__c>)Database.query(soql);
        }
        return userList;
    }


     /*@author : Lagnika Sharma
    @date : 21 July 2017  
    @description : This method will fetch list of user to delete 
    @return : userset for HO and Admin profile
    */
    
    
     public static set<String> getUsersOfHOLevelProfile(){
        
        list<User> userList = new list<User>();
        set<String> userSet = new set<String>();
        String profileHO = '';
        if(TotalApproval__c.getValues('HO') != null ){
            if(TotalApproval__c.getValues('HO').Subscriber_Profile__c != null)
                profileHO = TotalApproval__c.getValues('HO').Subscriber_Profile__c;
        }
        String adminPRofile = '';
        if(TotalApproval__c.getValues('System Administrator') != null){
            if(TotalApproval__c.getValues('System Administrator').Subscriber_Profile__c!= null)
                adminPRofile = TotalApproval__c.getValues('System Administrator').Subscriber_Profile__c;
        }

        //SPD-3916
        //Scenario security access should support multiple HO profiles
        list<string> multipleProfile = profileHO.split(';');    
        userList = [select id, Name from User where IsActive= true AND (User.Profile.Name in: multipleProfile OR User.Profile.Name =: adminPRofile)];
        for(User us : userList){
            userSet.add(us.id);
        }

        return userSet;
    }
}