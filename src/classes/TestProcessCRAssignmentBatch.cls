/************************************************************************************************
Name			: TestProcessCRAssignmentBatch
Description 	: Test class for ProcessCRAssignmentBatch. Code Coverage : 100%
************************************************************************************************/

@IsTest
public class TestProcessCRAssignmentBatch {

	static Team__c baseTeam;
	static Position__c territory,destTerr;
	static  Change_Request__c posChange;
	static User hoUser1 ;

	static testMethod void TestProcessCRAssignmentBatch()
	{
		try{
			Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
	        User objUser = new User(Alias = 'Test', Email='testadmin@testorg.com', 
	                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	                            LocaleSidKey='en_US', ProfileId = p.Id, 
	                            TimeZoneSidKey='America/Los_Angeles', UserName='testadmin@testorg.com');
	        insert objUser;
		 	
	 	 	Team__c Team = CallPlanTestDataFactory.createTeam('Zip');
		    insert Team;
		    
		    Team_Instance__c TeamInstance  = CallPlanTestDataFactory.CreateTeamInstance(Team,'Zip');
		    insert TeamInstance;
		    
		    Position__c region = CallPlanTestDataFactory.createRegionPosition(Team);
		    region.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
		    insert region;
		    
		    Position__c district = CallPlanTestDataFactory.createPositionParent(region,Team);
		    district.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
		    insert district;
			    
		    Position__c position = CallPlanTestDataFactory.createPosition(district, Team);
		    position.Related_Position_Type__c = SalesIQGlobalConstants.TEAM_TYPE_BASE ;
		    insert position;
		    
		    Position__c unassignedTerritory = new Position__c();
	    	unassignedTerritory.Name = 'Unassigned Territory';
	    	unassignedTerritory.Client_Territory_Name__c = 'Unassigned (Terr00000)';
	 		unassignedTerritory.Client_Position_Code__c = SalesIQGlobalConstants.UNASSIGNED_TERRITORY_CODE;
	  		unassignedTerritory.Client_Territory_Code__c=SalesIQGlobalConstants.UNASSIGNED_TERRITORY_CODE;
	        unassignedTerritory.Position_Type__c='Territory';
	        unassignedTerritory.inactive__c = false;
	        unassignedTerritory.RGB__c = '41,210,117';
	        unassignedTerritory.Team_iD__c = Team.id;
	        unassignedTerritory.Effective_End_Date__c=Date.today().addMonths(50);
	        unassignedTerritory.Hierarchy_Level__c = '1';
	        unassignedTerritory.Related_Position_Type__c = 'Base';
	        unassignedTerritory.Team_Instance__c = TeamInstance.id;
	        insert unassignedTerritory;

		    Change_Request__c ChangeReq =  CallPlanTestDataFactory.createChangeRequest(objUser, position, TeamInstance,'Pending' );
		    ChangeReq.Destination_Position__c = unassignedTerritory.Id;
		    insert ChangeReq;
		    
		  	User_Access_Permission__c userPermission = CallPlanTestDataFactory.createUserAccessPerm(district, objUser,TeamInstance);
		    insert userPermission;
		    
		    CR_Position__c crPosition = CallPlanTestDataFactory.createCRPosition(ChangeReq);
		    crPosition.Position__c = position.Id ;
		    crPosition.Effective_End_Date__c = date.today() + 1;
		    insert crPosition;
		    
		    Employee__c emp = CallPlanTestDataFactory.createEmployee();
		    insert emp;
		    
		    Position_Employee__c empPosition = CallPlanTestDataFactory.createPosEmploye(position,emp);
		    empPosition.Effective_End_Date__c = date.today() + 50;
		    insert empPosition;
		    System.debug('### posEmpInsert ::' + empPosition.Effective_End_Date__c);


		    Position_Team_Instance__c posUnAssignedInstance = CallPlanTestDataFactory.CreatePosTeamInstance(unassignedTerritory,district,TeamInstance); 
		    insert posUnAssignedInstance;

		    Position_Team_Instance__c posTerrTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(position,district,TeamInstance ); 
		    insert posTerrTeamInstance;
		    
		    Position_Team_Instance__c posDistrictTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district,region,TeamInstance ); 
		    insert posDistrictTeamInstance;
		    
		    Position_Team_Instance__c posRegionTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(region,null,TeamInstance ); 
		    insert posRegionTeamInstance;

		    Account acc = CallPlanTestDataFactory.CreateAccount('Direct Aligned','11788','Physician');    
			acc.type = 'Physician';
			insert acc;

			/*Team_Instance_Account__c teamInstanceAcc = CallPlanTestDataFactory.createTeamInstanceAccount(acc,TeamInstance);
			insert teamInstanceAcc;	*/

			Position_Account__c posAcc = CallPlanTestDataFactory.createPositionAccount(acc, position, TeamInstance, posTerrTeamInstance);
			posAcc.Account__c = acc.Id;
			insert posAcc;
	            
	        System.debug('#### accinsert' + posAcc.Account__c + '::' + posAcc.Position__c);

			Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11778');
			insert geo;

			/*Team_Instance_Geography__c geographyTeamInstance = CallPlanTestDataFactory.CreateTeamInstanceGeoGraphy(geo,TeamInstance);
	    	insert geographyTeamInstance;*/

	    	Position_Geography__c posGeo = CallPlanTestDataFactory.createPosGeography(geo,position,posTerrTeamInstance,TeamInstance);
	    	insert posGeo;

	    	System.debug('### geoInsert::' + posGeo.Position__c);

	    	CR_Account__c CR_Account= CallPlanTestDataFactory.createCRaccount(ChangeReq, acc, position, position, TeamInstance, posTerrTeamInstance, posTerrTeamInstance);
			insert CR_Account;

			CR_Employee_Assignment__c crEmpAssigment = CallPlanTestDataFactory.cr_EmployeeAssignment(ChangeReq, emp, position);
			insert crEmpAssigment;

			CR_Geography__c crgeo = new CR_Geography__c();		
			crgeo.Change_Request__c = ChangeReq.Id;
			crgeo.Geography__c = geo.Id;
			crgeo.Source_Position__c = position.Id;
			crgeo.Destination_Position__c = position.Id;
			crgeo.Source_Position_Team_Instance__c = posTerrTeamInstance.Id;
			crgeo.Destination_Position_Team_Instance__c = posTerrTeamInstance.Id;
			insert crgeo;

			List<CR_Account__c> accList = [Select id,Name,Account__c,Change_Request__c from CR_Account__c where Change_Request__c =: ChangeReq.id];
			List<CR_Geography__c> geoList = [Select id,Name,Geography__c,Change_Request__c from CR_Geography__c where Change_Request__c =: ChangeReq.id];
			List<CR_Employee_Assignment__c> empList = [Select id,Name,Employee_ID__c,Change_Request_ID__c from CR_Employee_Assignment__c where Change_Request_ID__c =: ChangeReq.id];

			System.debug('###teamInstance---' + TeamInstance.id);
            set<id> posSet = new set<Id>();
            posSet.add(position.Id);
			ProcessCRAssignmentBatch b1 = new ProcessCRAssignmentBatch(ChangeReq,'CR_Geography');
	    	ProcessCRAssignmentBatch b2 = new ProcessCRAssignmentBatch(ChangeReq,'CR_Account');
	    	ProcessCRAssignmentBatch b3 = new ProcessCRAssignmentBatch(ChangeReq,'CR_Employee');
            ProcessCRAssignmentBatch b4 = new ProcessCRAssignmentBatch(posSet,'Position_Account__c');
            ProcessCRAssignmentBatch b5 = new ProcessCRAssignmentBatch(posSet,'Position_Geography__c');
            
	    	test.startTest();
		    	Id batchinstanceid1 = Database.executeBatch(b1,500);
		        System.debug('#### begin account');
		    	Id batchinstanceid2 = Database.executeBatch(b2,500);
		        System.debug('#### begin employee');
		    	Id batchinstanceid3 = Database.executeBatch(b3,500);
		    	System.debug('#### begin Position Account');
		    	Id batchinstanceid4 = Database.executeBatch(b4,500);
		    	System.debug('#### begin Position Geography');
		    	Id batchinstanceid5 = Database.executeBatch(b5,500);
		    	
	    	test.stopTest();
		}
		catch(Exception ex){
			System.debug(ex);
		}
	}
}