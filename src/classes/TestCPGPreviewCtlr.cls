/*
Author : Ankit Manendra
Description : Test class for CPGFilterCtrl
Date : 8 Novomber, 2017

*/

@isTest
private class TestCPGPreviewCtlr{
    @isTest static void cpgPrevTest() {
    
        Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj;
        
        Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        insert scenarioObj;
        
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.Status__c = 'Success';
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj;
        
         Scenario_Rule_Instance_Details__c sceRuleInsDetObj1 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj1.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj1.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj1;
        
        Data_Set__c dataSetObj = new Data_Set__c();
        dataSetObj.Name = 'Call Plan Summary';
        insert dataSetObj;
        
        Data_Set_Rule_Map__c dataSetRuleObj = new Data_Set_Rule_Map__c();
        dataSetRuleObj.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj.ds_type__c='I';
        dataSetRuleObj.dataset_id__c = dataSetObj.id;
        insert dataSetRuleObj;
        
        Data_Set_Rule_Map__c dataSetRuleObj1 = new Data_Set_Rule_Map__c();
        dataSetRuleObj1.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj1.ds_type__c='O';
        dataSetRuleObj1.dataset_id__c = dataSetObj.id;
        insert dataSetRuleObj1;
        
        system.debug('==dataSetRuleObj===='+dataSetRuleObj);
        
        ComponentTypeMaster__c ComTypeMasObj = new ComponentTypeMaster__c();
        ComTypeMasObj.Name='Call Balancing';
        ComTypeMasObj.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj.DisplayIcon__c='add';
        ComTypeMasObj.DisplayIconClass__c='window information';
        insert ComTypeMasObj;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj.Name='Rank';
        busRuleTypeMasObj.Component_Type__c=ComTypeMasObj.id;
        insert busRuleTypeMasObj;
        
        ComponentTypeMaster__c ComTypeMasObj1 = new ComponentTypeMaster__c();
        ComTypeMasObj1.Name='Aggregation';
        ComTypeMasObj1.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj1.DisplayIcon__c='add';
        ComTypeMasObj1.DisplayIconClass__c='window information';
        insert ComTypeMasObj1;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj1 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj1.Name='Rank';
        busRuleTypeMasObj1.Component_Type__c=ComTypeMasObj1.id;
        insert busRuleTypeMasObj1;
        
        Business_Rules__c busRuleObj = new Business_Rules__c();
        busRuleObj.Name='Rank rule 278';
        busRuleObj.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObj.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj.Aggregate_Level__c='Test';
        busRuleObj.customer_prioritization__c='Test';
        insert busRuleObj;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj.Param_Name__c='derived field 278';
        busRuleFieldObj.Business_Rule__c=busRuleObj.id;
        busRuleFieldObj.Param_Data_Type__c='Numeric';
        busRuleFieldObj.Param_Type__c='Derived';
        busRuleFieldObj.IO_Flag__c='O';
        
        insert busRuleFieldObj;
        
        Computation_Rule_Detail__c compRuleDetObj = new Computation_Rule_Detail__c();
        compRuleDetObj.Business_Rules__c=busRuleObj.id;
        compRuleDetObj.Data_Type__c='Numeric';
        compRuleDetObj.Derived_field_name__c='FIELD 123';
        
        insert compRuleDetObj;
        
        Data_Set_Column_Detail__c dataSetCol = new Data_Set_Column_Detail__c();
        dataSetCol.dataset_id__c=dataSetObj.id;
        dataSetCol.variable_type__c='Derived';
        dataSetCol.ds_col_name__c='colTest';
        insert dataSetCol;
        
        list<Data_Set_Column_Detail__c> dataSetColDetList = new list<Data_Set_Column_Detail__c>();
        dataSetColDetList.add(dataSetCol);
        
        Category_Rule_Entity_Detail__c catRuleEntDet = new Category_Rule_Entity_Detail__c();
        catRuleEntDet.Entity_Type__c='Block';
        catRuleEntDet.Business_Rule__c=busRuleObj.id;
        catRuleEntDet.Priority__c='1';
        insert catRuleEntDet;
        
        Category_Derived_Variable_Metadata__c catDerMetObj = new Category_Derived_Variable_Metadata__c();
        catDerMetObj.Field_Name__c='TestCatDer';
        catDerMetObj.Business_Rule_Id__c=busRuleObj.id;
        insert catDerMetObj;
        
        Category_Rule_Entity_Detail__c catRuleEntDet1 = new Category_Rule_Entity_Detail__c();
        catRuleEntDet1.Entity_Type__c='Group';
        catRuleEntDet1.Business_Rule__c=busRuleObj.id;
        catRuleEntDet1.Priority__c='1';
        catRuleEntDet1.Parent_Entity__c=catRuleEntDet.id;
        insert catRuleEntDet1;
        
        Category_Rule_Output_Detail__c catRuleOutDet = new Category_Rule_Output_Detail__c();
        catRuleOutDet.Category_Rule_Entity_Detail__c=catRuleEntDet.id;
        catRuleOutDet.Derived__c='TestCatDer';
        insert catRuleOutDet;
        
        Category_Rule_Criteria_Detail__c catRuleCriDet = new Category_Rule_Criteria_Detail__c();
        catRuleCriDet.FieldDataType__c='Numeric';
        catRuleCriDet.Sequence__c='1';
        insert catRuleCriDet;
        CPGDataObjectHandler dataObj1 = new CPGDataObjectHandler();
        
        ETL_Config__c etlObj = new ETL_Config__c();
        etlObj.Name= 'BRMS';
        etlObj.SF_UserName__c = 'SFUser';
        etlObj.SF_Password__c = 'abx';
        etlObj.BR_PG_Host__c = 'abc';
        etlObj.BR_PG_UserName__c = 'sfuswrname';
        etlObj.SFTP_Password__c = 'sfuswrname';
        etlObj.SFTP_Username__c = 'sfuswrname';
        etlObj.SFTP_Folder__c = 'sfuswrname';
        etlObj.BR_PG_Schema__c = 'sfuswrname';
        insert etlObj;
        List<ETL_Config__c> etlConfig = CPGDataObjectHandler.getETLConfigByName('BRMS');
        Data_Object__c dataObj = new Data_Object__c();
        dataObj.dataset_id__c=dataSetObj.id;
        insert dataObj;
        
        Test.startTest();
        // Set mock callout class 
        MockHttpResponseGenerator mocker = new MockHttpResponseGenerator();
        mocker.respType = 1;
        Test.setMock(HttpCalloutMock.class, mocker); 
        List<Data_Object__c> dataObjList = CPGPreviewCtlr.getDataSetInformation('testId');
        //string str = CPGPreviewCtlr.getInstanceName();
        List<BRColumnInfoWrapper> colInfoList = CPGPreviewCtlr.getColumnInformation(dataSetObj.id);
        BRPreviewWrapper prevWrap = CPGPreviewCtlr.getDataForUnderLyingData();
        List<Scenario_Rule_Instance_Details__c> sceRuleDetList = CPGPreviewCtlr.getScenarioID(sceRuleInsDetObj.id);
        string str1 = CPGPreviewCtlr.getTableName(sceRuleInsDetObj.id);
        List<BRColumnInfoWrapper> colInfoWrap = CPGPreviewCtlr.GetSourceAndDerivedFields(sceRuleInsDetObj.id);
        String comptype =CPGPreviewCtlr.getComponentTypeMasterName(sceRuleInsDetObj.id);
        String sceSta =CPGPreviewCtlr.getScenarioStatus(sceRuleInsDetObj.id);
        List<String> strList = CPGPreviewCtlr.getInstanceNameAndURL();
        CPGPreviewCtlr.getCalloutResponse('www.testMethod.in','abc',sceRuleInsDetObj.id); 
        CPGPreviewCtlr.getAggregateFunctionsList();   
        //CPGPreviewCtlr.getbucketdetails();
        CPGPreviewCtlr.webSeriveResponseMethod('www.testMethod.in');
        CPGPreviewCtlr.webSeriveResponseMethodChart('www.testMethod.in','Select');
        CPGPreviewCtlr.webSeriveResponseMethodQueryBuilder('www.testMethod.in','Select');
        CPGPreviewCtlr.reportsFilterParser('Select');

        List<String> getLocalData = CPGPreviewCtlr.getLocaleDateFormats();
        List<Map<String, List<String>>> convertedData = CPGPreviewCtlr.convertedData('{"last_modified_cust_addr":["07/06/2018 10:23:43"]}','{"billinglatitude":[""],"billinglongitude":[""]}');
        //List<String> foramteddates = CPGPreviewCtlr.getformatedDates('06/06/2018 13:28:54","06/06/2018 13:28:54","06/06/2018 13:28:54"]}');
        List<String> lstfmtddates = new List<String>();
        lstfmtddates.add('06/06/2018 13:28:54');
        lstfmtddates.add('06/07/2018 13:28:54');
        lstfmtddates.add('06/06/2018 13:28:54');
        lstfmtddates.add('06/06/2018 13:28:54');
        List<String> lstdates = CPGPreviewCtlr.getformatedDates(lstfmtddates);
        //Map<String, List<String>> formattedNoMap = CPGPreviewCtlr.getFormatedNumbersMap('1234546.45');
        List<String> lstformattednumbers = new List<String>();
        lstformattednumbers.add('1');
        List<String> lstnums = CPGPreviewCtlr.getformatedNumbers(lstformattednumbers);
        String expDetails = CPGPreviewCtlr.getexportAlldetails('www.testMethod.in','table');
        String strFiletype = CPGPreviewCtlr.getFileType(sceRuleInsDetObj.id);
       
        Object obj = CPGPreviewCtlr.getCalloutResponseforControlChart('www.testMethod.in','Select','','','');
       // Map< String, Object > mapObj1 = CPGPreviewCtlr.getCalloutResponseforBarChart('www.testMethod.in','abc','select');
       // Map< String, Object > mapObj2 = CPGPreviewCtlr.getCalloutResponseforPieChart('www.testMethod.in','','');
        
        // Set mock callout class 
        MockHttpResponseGenerator mocker1 = new MockHttpResponseGenerator();
        mocker1.respType = 4;
        Test.setMock(HttpCalloutMock.class, mocker1);  
        
        Map<String,String> serviceResponse = new Map<String,String>();
        serviceResponse.put('serviceUrl','testURL');
        Map< String, Object > calloutBarChart = CPGPreviewCtlr.getCalloutResponseforBarChart(serviceResponse,'where');
        
        MockHttpResponseGenerator mocker2 = new MockHttpResponseGenerator();
        mocker2.respType = 5;
        Test.setMock(HttpCalloutMock.class, mocker2);
        Map< String, Object > calloutPieChart = CPGPreviewCtlr.getCalloutResponseforPieChart(serviceResponse,'where');
        
       
        
        Test.stopTest(); 
        
        }        
     }