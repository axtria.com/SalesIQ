@IsTest 
public with sharing class TestSalesIQAddDropTargetCtrl{

    static testMethod void testAddDrop(){

    	Team__c team = TestDataFactory.createTeam('Base');
        //team.Country__c = country.id;
		insert team;

		Team_Instance__c team_instance = TestDataFactory.CreateTeamInstance(team.Id,'Zip','',Date.today()-100,Date.today()+100);
		insert team_instance;

    	CR_Team_Instance_Config__c config = new CR_Team_Instance_Config__c();
    	config.Configuration_Name__c = SalesIQGlobalConstants.ADD_TARGET_MODULE;
    	config.Configuration_Type__c = SalesIQGlobalConstants.DATA_TABLE_CONFIG;
    	config.Configuration_Value__c = 'ab:c;x:yz';
    	insert config;

    	Position__C pos1 = TestDataFactory.createPosition(team, 'PositionRegion', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_REGION, '3', null);
        pos1.Team_Instance__c = team_instance.Id;
        insert pos1;

        Position__C pos2 = TestDataFactory.createPosition(team, 'PositionDistrict', Date.today().addDays(-30), Date.today().addDays(30), SalesIQGlobalConstants.POSITION_TYPE_DISTRICT, '2', pos1.Id);
        pos2.Team_Instance__c = team_instance.Id;
        insert pos2;

        insert TestDataFactory.createPositionTeamInstance(pos1.Id, null, team_instance.Id);
        insert TestDataFactory.createPositionTeamInstance(pos2.Id, null, team_instance.Id);

        Account acc = new Account();
        acc.Name = 'Chelsea Parson';
        acc.FirstName__c = 'Chelsea';
        acc.LastName__c = 'Parson';
        acc.Speciality__c = 'Cardiology';
        insert acc;

        Account acc1 = new Account();
        acc1.Name = 'Watson Parson';
        acc1.FirstName__c = 'Chelsea';
        acc1.LastName__c = 'Parson';
        insert acc1;

    	Position_Account_Call_Plan__c pacp1 = new Position_Account_Call_Plan__c();
    	pacp1.isTarget_Updated__c = false;
    	pacp1.Position__c = pos1.Id;
    	pacp1.Account__c = acc.Id;
    	pacp1.Team_Instance__c = team_instance.Id;
    	pacp1.Picklist1_Segment_Updated__c = 'Class 1';
    	insert pacp1;

    	Position_Account_Call_Plan__c pacp2 = new Position_Account_Call_Plan__c();
    	pacp2.isTarget_Updated__c = false;
    	pacp2.Position__c = pos2.Id;
    	pacp2.Account__c = acc1.Id;
    	pacp2.Team_Instance__c = team_instance.Id;
    	insert pacp2;

    	Position_Account_Call_Plan__c pacp3 = new Position_Account_Call_Plan__c();
    	pacp3.isTarget_Updated__c = false;
    	pacp3.Position__c = pos1.Id;
    	pacp3.Account__c = acc1.Id;
    	pacp3.Team_Instance__c = team_instance.Id;
    	insert pacp3;

    	list<String> results = new list<string>();
    	results.add(pacp1.Id);

    	SalesIQAddDropTargetCtrl.getModuleOptions(team_instance.ID,'');
       
       	SalesIQAddDropTargetCtrl.updateTargets(SalesIQGlobalConstants.ELIGIBLE_TARGETS,'""',new list<String>{pacp2.Id},pos1.Id,team_instance.Id);
       	SalesIQAddDropTargetCtrl.updateTargets(SalesIQGlobalConstants.ELIGIBLE_ACROSS_TEAM_TARGETS,'""',new list<String>{pacp2.Id},pos1.Id,team_instance.Id);
		SalesIQAddDropTargetCtrl.updateTargets(SalesIQGlobalConstants.ELIGIBLE_ACROSS_TEAM_TARGETS,'""',new list<String>{pacp3.Id},pos1.Id,team_instance.Id);
       
    	//SalesIQAddDropTargetCtrl.updateTargets('','""',results,'',team_instance.Id);
    	Map<String,String> recordValueMAP = new Map<String,String>();
    	recordValueMAP.put('Change_Action_Type__c', SalesIQGlobalConstants.TARGET_DROPPED);
    	recordValueMAP.put('Picklist1_Segment_Updated__c', 'Class 1');
    	//recordValueMAP.put('Account__r.Speciality__c', 'Cardiology');
    	recordValueMAP.put('isTarget_Updated__c', 'true');
        SalesIQAddDropTargetCtrl.setFieldAPIvalue((Sobject) pacp1,recordValueMAP,SalesIQGlobalConstants.NAME_SPACE+'Position_Account_Call_Plan__c',new set<String>{'Change_Action_Type__c','isTarget_Updated__c','Picklist1_Segment_Updated__c'});    
        
    }
}