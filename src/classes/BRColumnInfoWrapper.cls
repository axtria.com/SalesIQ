public class BRColumnInfoWrapper{
        @AuraEnabled
        public string fieldName{get;set;}
        @AuraEnabled
        public string dataType{get;set;}
        @AuraEnabled
        public string description{get;set;}
        @AuraEnabled
        public string variableType{get;set;}
        @AuraEnabled
        public string valueType{get;set;}
        @AuraEnabled
        public string columnName{get;set;}
        public BRColumnInfoWrapper(string fieldName, string dataType, string description, string valueType, string variableType,string columnName){
            this.fieldName = fieldName;
            this.dataType = dataType;
            this.description = description;
            this.valueType = valueType;
            this.variableType = variableType;
            this.columnName =columnName;
        }
    }