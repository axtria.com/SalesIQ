public class BROperatorsWrapper {
    @AuraEnabled
    public String dataType;
    @AuraEnabled
    public List<OperatorWrapper> lstOfOperators;
    
    public BROperatorsWrapper(string dataType, List<BROperatorsWrapper.OperatorWrapper> operators){
        this.dataType = dataType;
        this.lstOfOperators = operators;
    }

    public class OperatorWrapper {
    	@AuraEnabled
    	public String operatorsName ;
    	@AuraEnabled
    	public String operatorTranslated ;
    	@AuraEnabled
    	public String operatorStaging ;
        @AuraEnabled
        public String operatorSource;


    	public OperatorWrapper(String operatorsName, String operatorTranslated, String operatorStaging,String operatorSource) {
    		this.operatorsName = operatorsName ;
    		this.operatorTranslated = operatorTranslated ;
    		this.operatorStaging = operatorStaging ;
            this.operatorSource = operatorSource;
    	}
    }
}