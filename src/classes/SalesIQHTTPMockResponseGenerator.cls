/**********************************************************************************************
Name		:	SalesIQHTTPMockResponseGenerator
Author     	:	Ankur
Date       	: 	
Description	:	HTTP Callout Mock class to generate mock response for ESRI services
**********************************************************************************************/
@isTest
global class SalesIQHTTPMockResponseGenerator implements HttpCalloutMock{
	global HTTPResponse respond(HTTPRequest req) {
		
		MapServerURL__c ESRIService = MapServerURL__c.getOrgDefaults();
		system.debug('##### ESRIServicesMockGenerator ' + req);
	    system.debug('##### ESRIServicesMockGenerator ' + req.getEndpoint());
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('105');
        res.setStatus('Success');
        res.setStatusCode(200);
        return res;
	}
}