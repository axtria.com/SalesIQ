/* @author : Ankur Dohare
    @date : 21 July 2017  
    @description : Utility Class for Product Management Module
*/
public with sharing class ProductManagementUtility {

     /*@author : Ankur Dohare
    @date : 21 July 2017 
    @description : Method to add position product records to team instance product records.
    @param1: list of team instance product records
    @return : void
    */
    
    public static void addPositionProducts(List<Team_Instance_Product__c> productList){
        set<ID> teamInstanceIds = new set<ID>();
        map<ID,Team_Instance_Product__c> mapOfTeamInstanceIdToProductId = new map<Id,Team_Instance_Product__c>();

        for(Team_Instance_Product__c tip :productList){
            teamInstanceIds.add(tip.Team_Instance__c);
            mapOfTeamInstanceIdToProductId.put(tip.Team_Instance__c,tip);
        }

        list<Position__c> lstPos = ProductManagementUtility.getPositionList(teamInstanceIds);
        List<Position_Product__c> posProductList = new List<Position_Product__c>();
        for(Position__c posObj :lstPos){
            Position_Product__c obj = new Position_Product__c();
            obj.Position__c = posObj.id;
            obj.Product_Master__c = (Id) mapOfTeamInstanceIdToProductId.get(posObj.Team_Instance__c).get('Product_Master__c');
            obj.Team_Instance__c = posObj.Team_Instance__c;
            //Required Fields
            obj.Product_Weight__c = 0;
            if(mapOfTeamInstanceIdToProductId.get(posObj.Team_Instance__c).get('Effective_Start_Date__c')!= null
                && mapOfTeamInstanceIdToProductId.get(posObj.Team_Instance__c).get('Effective_End_Date__c')!= null
            ){
                obj.Effective_Start_Date__c = (Date) mapOfTeamInstanceIdToProductId.get(posObj.Team_Instance__c).get('Effective_Start_Date__c');
                obj.Effective_End_Date__c = (Date) mapOfTeamInstanceIdToProductId.get(posObj.Team_Instance__c).get('Effective_End_Date__c');
            }else{
                obj.Effective_Start_Date__c = Date.today();
                obj.Effective_End_Date__c = Date.today();
            }
            posProductList.add(obj);
        }
        if( posProductList != null && posProductList.size()>0){
            INSERT posProductList;
        }
    }

    /*@author : Ankur Dohare
    @date : 21 July 2017 
    @description : Method to remove position product records assigned to team instance product records.
    @param1: list of team instance product records
    @return : void
    */
    public static void removePositionProducts(List<Team_Instance_Product__c> productList){
        set<ID> teamInstanceIds = new set<ID>();
        set<Id> productIds = new set<ID>();
        List<Position_Product__c> delPosProductList = new List<Position_Product__c>();
        for(Team_Instance_Product__c tip :productList){
            teamInstanceIds.add(tip.Team_Instance__c);
            productIds.add(tip.Product_Master__c);
        }
        delPosProductList = ProductManagementUtility.getListPositionProduct(teamInstanceIds,productIds);
        if( delPosProductList != null && delPosProductList.size()>0){
            DELETE delPosProductList;
        }
    }

    /*@author : Ankur Dohare
    @date : 21 July 2017 
    @description : Get Position list from a set of team instance ids
    @param1: set of team instance ids
    @return : list<Position__c>
    */

    public static list<Position__c> getPositionList(Set<Id> teamInstanceIds){
        return [    SELECT id , Team_Instance__c
                    FROM Position__c 
                    WHERE Team_Instance__c IN :teamInstanceIds 
                    AND inactive__c = false
                    AND Hierarchy_Level__c = '1' 
                    limit 10000
                ];     
    }

    /*@author : Ankur Dohare
    @date : 21 July 2017 
    @description : Get Position Product list 
    @param1: Set<TeamInstanceIds> and Set<ProductIds>
    @return : List<Position_Product__c>
    */

    public static List<Position_Product__c> getListPositionProduct(Set<Id> teamInstanceIds, Set<Id> productIds){
        return [    SELECT id, Team_Instance__r.IC_EffEndDate__c 
                    FROM Position_Product__c 
                    WHERE Team_Instance__c IN :teamInstanceIds
                    AND Product_Master__c IN :productIds 
                    limit 10000
                ];
    }

    /*@author : Ankur Dohare
    @date : Sept 28 2017 
    @description : delete team instance product records
    @param1: List<Product__c>
    @return : void
    */
    public static void removeTeamInstanceProducts(List<Product__c> productList){
        set<ID> teamInstanceIds = new set<ID>();
        set<Id> productIds = new set<ID>();
        List<Team_Instance_Product__c> delPosProductList = new List<Team_Instance_Product__c>();
        for(Product__c pr :productList){
            productIds.add(pr.id);
        }
        delPosProductList = ProductManagementUtility.getTeamInstaceProductList(productIds);
        if( delPosProductList != null && delPosProductList.size()>0){
            DELETE delPosProductList;
        }
    }

    /*@author : Ankur Dohare
    @date : Sept 28 2017 
    @description : Get team instance product list 
    @param1: Set<TeamInstanceIds> and Set<ProductIds>
    @return : List<Position_Product__c>
    */

    public static List<Team_Instance_Product__c> getTeamInstaceProductList(Set<Id> productIds){
        return [    SELECT id 
                    FROM Team_Instance_Product__c 
                    WHERE Product_Master__c IN :productIds
                    limit 10000
                ];
    }



}