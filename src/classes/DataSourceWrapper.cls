public class DataSourceWrapper {
    @AuraEnabled
    public string dataSetName{get;set;}
    @AuraEnabled
    public string dataSetId{get;set;}
    @AuraEnabled
    public string filePath{get;set;}
    @AuraEnabled
    public string tableDisplayName{get;set;}

    public DataSourceWrapper(String dataSetName,String dataSetId,String filePath, String tableDisplayName){
    	this.dataSetName = dataSetName;
    	this.dataSetId = dataSetId;
    	this.filePath = filePath;
        this.tableDisplayName = tableDisplayName;
    }
}