@isTest
public with sharing class TestBRQueryBuilderCtrl {
    @isTest 
    static void testMethodQueryBuilder1() {
    	Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj;
        
        Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        insert scenarioObj;

        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj;


        Data_Set_Rule_Map__c dataSetRuleObj = new Data_Set_Rule_Map__c();
        dataSetRuleObj.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj.ds_type__c='I';
        insert dataSetRuleObj;

        Data_Set__c dataSetObj = new Data_Set__c();
        dataSetObj.Name = 'Call Plan Summary';
        insert dataSetObj;

        Data_Set_Column_Detail__c dataSetCol = new Data_Set_Column_Detail__c();
        dataSetCol.dataset_id__c=dataSetObj.id;
        dataSetCol.variable_type__c='Derived';
        dataSetCol.ds_col_name__c='colTest';
        insert dataSetCol;

        
    	String queryJson = '';
    	BRFilterConditionWrapper brObj = BRQueryBuilderCtrl.initializeBlock(queryJson);
    	String filterWrap = '{"bindClause":"AND","criterias":[{"anotherValue":"","dataType":"text","field":"name","innerConditionWrapper":{},"isGroup":false,"operator":"equals","value":"abs"}]}';
    	BRFilterConditionWrapper addInnerGroup = BRQueryBuilderCtrl.addInnerGroup(filterWrap);
    	BRFilterConditionWrapper addInnerCriteria = BRQueryBuilderCtrl.addInnerCriteria(filterWrap);
    	BRFilterConditionWrapper deleteInnerCriteria = BRQueryBuilderCtrl.deleteInnerCriteria(filterWrap,0);
    	BRFilterConditionWrapper deleteInnerGroup = BRQueryBuilderCtrl.deleteInnerGroup(filterWrap);
        List<BROperatorsWrapper> lstOfOp =  BRQueryBuilderCtrl.getOperatorValues();
        list<BRColumnWrapper> lstOfColmns = BRQueryBuilderCtrl.createColumnWrapperData(sceRuleInsDetObj.Id,'');
        String filterWrapper = '{"bindClause":"AND","criterias":[{"anotherValue":"","dataType":"text","field":"name","innerConditionWrapper":{},"isGroup":false,"operator":"equals","value":"abs"},{"innerConditionWrapper":{"bindClause":"AND","criterias":[{"anotherValue":"","dataType":"text","field":"billingstreet","innerConditionWrapper":{},"isGroup":false,"operator":"equals","value":"A"},{"innerConditionWrapper":{"bindClause":"AND","criterias":[{"anotherValue":"","dataType":"text","field":"name","innerConditionWrapper":{},"isGroup":false,"operator":"not_equals","value":"abx"}]},"isGroup":true}]},"isGroup":true}]}';
        String ColumnNames = '[{"colDatatype":"Text","colValuetype":"Discrete","Id":"a0g1N000009z8V9QAI","Name":"Account_Alignment_Type__c","tableColumnName":"account_alignment_type__c","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VAQAY","Name":"AccountNumber__c","tableColumnName":"accountnumber__c","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Discrete","Id":"a0g1N000009z8VBQAY","Name":"Territory_ID__C","tableColumnName":"territory_id__c","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VCQAY","Name":"POSITION__NAME","tableColumnName":"position__name","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VDQAY","Name":"Name","tableColumnName":"name","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VEQAY","Name":"BillingStreet","tableColumnName":"billingstreet","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VFQAY","Name":"BillingCity","tableColumnName":"billingcity","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VGQAY","Name":"BillingPostalCode","tableColumnName":"billingpostalcode","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VHQAY","Name":"BillingCountry","tableColumnName":"billingcountry","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VIQAY","Name":"BillingLatitude","tableColumnName":"billinglatitude","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VJQAY","Name":"BillingLongitude","tableColumnName":"billinglongitude","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VKQAY","Name":"FirstName__c","tableColumnName":"firstname__c","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VLQAY","Name":"LastName__c","tableColumnName":"lastname__c","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Numeric","colValuetype":"Continuous","Id":"a0g1N000009z8VMQAY","Name":"Metric1__c","tableColumnName":"metric1__c","tableDataType":"numeric","variabletype":"Source"},{"colDatatype":"Numeric","colValuetype":"Continuous","Id":"a0g1N000009z8VNQAY","Name":"Metric2__c","tableColumnName":"metric2__c","tableDataType":"numeric","variabletype":"Source"},{"colDatatype":"Numeric","colValuetype":"Continuous","Id":"a0g1N000009z8VOQAY","Name":"Metric3__c","tableColumnName":"metric3__c","tableDataType":"numeric","variabletype":"Source"},{"colDatatype":"Numeric","colValuetype":"Continuous","Id":"a0g1N000009z8VPQAY","Name":"Metric4__c","tableColumnName":"metric4__c","tableDataType":"numeric","variabletype":"Source"},{"colDatatype":"Numeric","colValuetype":"Continuous","Id":"a0g1N000009z8VQQAY","Name":"Metric5__c","tableColumnName":"metric5__c","tableDataType":"numeric","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Discrete","Id":"a0g1N000009z8VRQAY","Name":"Speciality__c","tableColumnName":"speciality__c","tableDataType":"text","variabletype":"Source"},{"colDatatype":"Text","colValuetype":"Continuous","Id":"a0g1N000009z8VSQAY","Name":"External_Account_Number__c","tableColumnName":"external_account_number__c","tableDataType":"text","variabletype":"Source"}]';
        String lstOdOperators = '[{"dataType":"text","lstOfOperators":[{"operatorsName":"equals","operatorStaging":"=","operatorTranslated":"equals"},{"operatorsName":"not_equals","operatorStaging":"!=","operatorTranslated":"not equals"},{"operatorsName":"in","operatorStaging":"in","operatorTranslated":"in"},{"operatorsName":"not_in","operatorStaging":"not in","operatorTranslated":"not in"},{"operatorsName":"is_null","operatorStaging":"is null","operatorTranslated":"is null"},{"operatorsName":"is_not_null","operatorStaging":"is not null","operatorTranslated":"is not null"},{"operatorsName":"contains","operatorStaging":"like","operatorTranslated":"contains"},{"operatorsName":"does_not_contains","operatorStaging":"not like","operatorTranslated":"does not contains"},{"operatorsName":"begins_with","operatorStaging":"like","operatorTranslated":"begins with"},{"operatorsName":"ends_with","operatorStaging":"like","operatorTranslated":"ends with"}]},{"dataType":"numeric","lstOfOperators":[{"operatorsName":"equals","operatorStaging":"=","operatorTranslated":"equals"},{"operatorsName":"not_equals","operatorStaging":"!=","operatorTranslated":"not equals"},{"operatorsName":"in","operatorStaging":"in","operatorTranslated":"in"},{"operatorsName":"not_in","operatorStaging":"not in","operatorTranslated":"not in"},{"operatorsName":"is_null","operatorStaging":"is null","operatorTranslated":"is null"},{"operatorsName":"is_not_null","operatorStaging":"is not null","operatorTranslated":"is not null"},{"operatorsName":"less_than","operatorStaging":"<","operatorTranslated":"less than"},{"operatorsName":"less_or_equal","operatorStaging":"<=","operatorTranslated":"less or equal"},{"operatorsName":"greater_than","operatorStaging":">","operatorTranslated":"greater than"},{"operatorsName":"greater_or_equal","operatorStaging":">=","operatorTranslated":"greater or equal"},{"operatorsName":"between","operatorStaging":"between","operatorTranslated":"between"}]}]';
        String[] query = BRQueryBuilderCtrl.generateQuery(filterWrapper,ColumnNames,lstOdOperators);
        String[] instanceNameandUrl = BRQueryBuilderCtrl.getInstanceNameAndURL(sceRuleInsDetObj.Id);
	
    	
    }

    @isTest 
    static void testMethodQueryBuilder2() {
    String queryJson = '{"bindClause":"AND","criterias":[{"anotherValue":"","dataType":"text","field":"name","innerConditionWrapper":{},"isGroup":false,"operator":"equals","value":"abs"}]}';
	BRFilterConditionWrapper brObj = BRQueryBuilderCtrl.initializeBlock(queryJson);
	
	}
}