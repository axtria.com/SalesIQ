global class CIMPopulateCallPlanBatch implements Database.Batchable<sObject>, Database.Stateful {
    global string theQuery;
    global string teamInstanceID;
    global List<string> queryList=new List<String>();
    global string theQuerySecond;
    global string finalQuery;
    global string scenarioId;
    global list<Scenario__c> sc=new list<Scenario__c>();
    
    
    global AggregateResult[] updateList ;
    global list<CIM_Config__c> listCIMconfig;
    public list<CIM_Position_Metric_Summary__c> updateListPostionMetric =new list<CIM_Position_Metric_Summary__c> ();
    global map<string,id> cimID;
    global CIMPopulateCallPlanBatch(string teamInstance,List<String> queryList1,string theQueryPart2, map<String,String> cimIdx,string scenarioIdx){
        teamInstanceID=teamInstance;
        queryList=queryList1;
        
        theQuerySecond=theQueryPart2;
        cimId=cimIdx;
        scenarioId=scenarioIdx;
        System.debug('theQueryFirst ='+queryList);
        
        
        
        theQuery='select id  from Position__C where Team_Instance__c =:teamInstanceID';
           
    }
    global Database.Querylocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(theQuery);
    }
    global void execute (Database.BatchableContext BC,list<Position__c> listPosition){
        
      
        list<string> posId=new List<id>();
        for(Position__C ps :listPosition)
        {
           
            posId.add(ps.id);
        }
        System.debug(queryList);
        for(String s:queryList)
        {
            
            finalQuery=s+' Team_Instance__c =:teamInstanceID and Position__c IN : posId'+theQuerySecond;
            System.debug('finalQuery ='+finalQuery);
            updateList = Database.query(finalQuery);
                for(AggregateResult  objCIMConfig : updateList){
                  string a = string.valueOF(objCIMConfig);
                    string ar=a.remove('AggregateResult:').remove('{').remove('}');
                    string[] ars = ar.split(',');
                    string[] finals=ars[0].split('=');
                    //aggregationObjectName objCimFromSobject = (aggregationObjectName)objCIMConfig;
                    //System.debug('##### hiii'+setFields);
                    
                     String originalCalls = String.valueOF(objCIMConfig.get(finals[0]));
                    
                            if(originalCalls == null)
                                originalCalls = '0';
                            CIM_Position_Metric_Summary__c objCIMPositionMetric = new CIM_Position_Metric_Summary__c(CIM_Config__c = cimId.get(finals[0]), Original__c = originalCalls, Team_Instance__c = teamInstanceID, Proposed__c = originalCalls, Approved__c = originalCalls, Position_Team_Instance__c = String.valueOF(objCIMConfig.get('aggregationAtrributeAPIName')));
                            updateListPostionMetric.add(objCIMPositionMetric);
                    /*
                    for(String key:cimId.keySet()){ 
                            System.debug(cimId.get(key));
                            System.debug('### objCIMConfig :'+objCIMConfig);
                            
                              System.debug(objCIMConfig.get(key));
                            String originalCalls = String.valueOF(objCIMConfig.get(key));
                    
                            if(originalCalls == null)
                                originalCalls = '0';
                            CIM_Position_Metric_Summary__c objCIMPositionMetric = new CIM_Position_Metric_Summary__c(CIM_Config__c = cimId.get(key), Original__c = originalCalls, Team_Instance__c = teamInstanceID, Proposed__c = originalCalls, Approved__c = originalCalls, Position_Team_Instance__c = String.valueOF(objCIMConfig.get('aggregationAtrributeAPIName')));
                            updateListPostionMetric.add(objCIMPositionMetric);
                          
                    }
                    */
                }
        }

    }
              global void finish(Database.BatchableContext BC){
             //for(CIM_Position_Metric_Summary__c cim:updateListPostionMetric)
            //System.debug('Cim_Config ='+cim.CIM_Config__c+' Position_Team_Instance__c=='+cim.Position_Team_Instance__c);
                  try{
                  insert updateListPostionMetric;
                  sc=[select id,Request_Process_Stage__c from Scenario__c where id=:scenarioId];
                  for(Scenario__c scf:sc)
                  {
                      scf.Request_Process_Stage__c='Ready';
                  }
                  update sc;
                  }catch(Exception e){
                      sc=[select id,Request_Process_Stage__c from Scenario__c where id=:scenarioId];
                  for(Scenario__c scf:sc)
                  {
                      scf.Request_Process_Stage__c='Error Occured';
                  }
                  update sc;
                  }
              }
}