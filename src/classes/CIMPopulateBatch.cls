global class CIMPopulateBatch implements Database.Batchable<sObject>, Database.Stateful,Database.AllowsCallouts {
    global string theQuery;
    global string teamInstanceID;
    global string theQueryFirst;
    global string theQuerySecond;
    global string finalQuery;
    global Integer xyz=0;
    global Integer mainLoop=0;
    global string scenarioId;
    global list<Scenario__c> sc=new list<Scenario__c>();
    global Boolean syncEsri = false;
    global list<String> posTeamInst=new list<String>();
    
    global AggregateResult[] updateList ;
    global list<CIM_Config__c> listCIMconfig;
    public list<CIM_Position_Metric_Summary__c> updateListPostionMetric =new list<CIM_Position_Metric_Summary__c> ();
    global map<string,id> cimID;
    global CIMPopulateBatch(string teamInstance,string theQueryPart1,string theQueryPart2, map<String,String> cimIdx,string scenarioIdx){
        teamInstanceID=teamInstance;
        
        theQueryFirst=theQueryPart1;
        theQuerySecond=theQueryPart2;
        cimId=cimIdx;
        scenarioId=scenarioIdx;
        theQuery='select id  from Position__C where Team_Instance__c =:teamInstanceID';
    }
    global CIMPopulateBatch(string teamInstance,string theQueryPart1,string theQueryPart2, map<String,String> cimIdx,string scenarioIdx,Boolean syncEsriVar){
        teamInstanceID=teamInstance;
        
        theQueryFirst=theQueryPart1;
        theQuerySecond=theQueryPart2;
        cimId=cimIdx;
        scenarioId=scenarioIdx;
        syncEsri = syncEsriVar;
        System.debug('theQueryFirst ='+theQueryFirst);
        System.debug('theQuerySecond ='+theQueryFirst);
        
        theQuery='select id  from Position__C where Team_Instance__c =:teamInstanceID';
    }

    global Database.Querylocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(theQuery);
    }

    global void execute (Database.BatchableContext BC,list<Position__c> listPosition){
        list<string> posId=new List<id>();
        mainLoop=mainLoop+1;
        
        for(Position__C ps :listPosition)
        {
            posId.add(ps.id);
        }

        finalQuery=theQueryFirst+' where Team_Instance__c =:teamInstanceID and Position__c IN : posId'+theQuerySecond;
        System.debug('finalQuery ='+finalQuery);
        System.debug('mainLoop ='+mainLoop);
        
        updateList = Database.query(finalQuery);
        for(AggregateResult  objCIMConfig : updateList){
            //aggregationObjectName objCimFromSobject = (aggregationObjectName)objCIMConfig;
            for(String key:cimId.keySet()){ 
                xyz=xyz+1;
                System.debug(cimId.get(key));
                String originalCalls = String.valueOF(objCIMConfig.get(key));
                    
                if(originalCalls == null)
                    originalCalls = '0';
                posTeamInst.add(String.valueOF(objCIMConfig.get('aggregationAtrributeAPIName')));
                
                CIM_Position_Metric_Summary__c objCIMPositionMetric = new CIM_Position_Metric_Summary__c(CIM_Config__c = cimId.get(key), Original__c = originalCalls, Team_Instance__c = teamInstanceID, Proposed__c = originalCalls, Approved__c = originalCalls, Position_Team_Instance__c = String.valueOF(objCIMConfig.get('aggregationAtrributeAPIName')));
                updateListPostionMetric.add(objCIMPositionMetric);
                System.debug('xyz ='+xyz);
            }
            System.debug('updateListPostionMetric Batch ='+updateListPostionMetric);
        }
    }

    global void finish(Database.BatchableContext BC)
    {
        //for(CIM_Position_Metric_Summary__c cim:updateListPostionMetric)
        //System.debug('Cim_Config ='+cim.CIM_Config__c+' Position_Team_Instance__c=='+cim.Position_Team_Instance__c);
        try{
		
			 List<Position_Team_Instance__c> remposTeamInst=[select id from Position_Team_Instance__c where id NOT IN : posTeamInst and Position_ID__r.Hierarchy_Level__c='1' and Team_Instance_ID__c =:teamInstanceID];
            //system.debug('###########hi'+remposTeamInst.size());
            for(Position_Team_Instance__c pos:remposTeamInst){
                for(String key:cimId.keySet()){
                    String originalCalls='0';
                    CIM_Position_Metric_Summary__c objCIMPositionMetric = new CIM_Position_Metric_Summary__c(CIM_Config__c = cimId.get(key), Original__c = originalCalls, Team_Instance__c = teamInstanceID, Proposed__c = originalCalls, Approved__c = originalCalls, Position_Team_Instance__c = pos.ID);
                    updateListPostionMetric.add(objCIMPositionMetric);
                }

            }
            insert updateListPostionMetric;
            //Invoke job to update Accountrelationship type for account affiliation

            if(syncEsri) {
                Database.executeBatch(new esriSyncingWithTalendBatch(scenarioId));
            }
            // Commneted by Aditi, after discussion with Raghav, this feature is deprecated now
            /*else {
                Team_Instance__c teamInsObj = SalesIQUtility.getTeamInstanceById(teamInstanceID);
                if(!(teamInsObj.Affiliation_Network__c == null && teamInsObj.Enable_Affiliation_Movement__c && !teamInsObj.Restrict_Account_Affiliation__c)){
                    Database.executeBatch(new processAccountAlignmentTypeBatch(teamInstanceID, scenarioId,syncEsri));
                }
            }*/
        }catch(Exception e)
        {
            system.debug('Exception :: ' + e.getMessage());
            SalesIQLogger.logCatchedException(e, 'Module : CIMPopulateBatch.cls');
            sc=[select id,Request_Process_Stage__c from Scenario__c where id=:scenarioId];
            for(Scenario__c scf:sc)
            {
                scf.Request_Process_Stage__c='Error Occured';
            }
            update sc;
        }
    }

    // public static void esriSyncingwithTalend(String scenarioId)
    // {
    //     String s_username,s_password;
    //     list<ETL_Config__c> lstETLInfo = [SELECT SF_Password__c,SF_UserName__c, S3_Security_Token__c FROM ETL_Config__c where name =: CPGConstants.talend_Instance limit 1];

    //     if(lstETLInfo != null && lstETLInfo.size() > 0)
    //     {
    //         s_username = lstETLInfo[0].SF_UserName__c;
    //         s_password = lstETLInfo[0].SF_Password__c+lstETLInfo[0].S3_Security_Token__c;
    //     }

    //     string esriSyncService = CPGUtility.getBRMSConfigValues('BREsriSyncAfterPush');
    //     String url = esriSyncService + '?method=runJob&arg0=--context_param salesiq_userPassword_userId='+s_username+'&arg1=--context_param salesiq_userPassword_password='+s_password+'&arg2=--context_param salesiq_scenarioid='+scenarioId; 

    //     system.debug(' ESRi sync service url>>>'+url);

    //     Http h = new Http();

    //     // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
    //     HttpRequest req = new HttpRequest();
    //     url = url.replaceAll( '\\s+', '%20');
    //     req.setEndpoint(url);
    //     req.setMethod('GET');
    //     req.setHeader('Content-Type', 'application/json');
    //     req.setHeader('Accept','application/json');
    //     req.setTimeout(120000);
    //     // Send the request, and return a response
    //     HttpResponse res = h.send(req);
    //     System.debug('esriSyncingwithTalend response:--> ' + res.getBody());
    //     String result = res.getBody();
    // }
}