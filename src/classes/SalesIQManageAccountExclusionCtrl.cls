public with sharing class SalesIQManageAccountExclusionCtrl {
    
    @AuraEnabled
    public static String getOrgNamespace() 
    {
        return SalesIQGlobalConstants.NAME_SPACE;        
    }

    @AuraEnabled
    public static Country__c fetchCountry(String recordId){
        List<Country__c> listCountry = SalesIQUtility.getCountryByCountryId(new List<String>{recordId});
        if(!listCountry.isEmpty()){
            return listCountry[0];
        }
        else
            return new Country__c();
    }
    
    @AuraEnabled
    public static Account_Exclusion__c fetchAccountExclusion(String recordId){
        Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> accountExclFieldsMap = globalDescribe.get(SalesIQGlobalConstants.NAME_SPACE+'Account_Exclusion__c').getDescribe().fields.getMap();

        Set<String> fieldName = accountExclFieldsMap.keySet();
        List<String> fieldList = new List<String>();
        fieldList.addAll(fieldName);
        String soql = 'SELECT ' + SalesIQGlobalConstants.NAME_SPACE + 'Account__r.Name, ' + String.join(fieldList, ',') + ' FROM ' + SalesIQGlobalConstants.NAME_SPACE + 'Account_Exclusion__c Where Id =: recordId';
        System.debug('##soql ' + soql);
        List<Account_Exclusion__c> listAccountExclusion = Database.query(soql);
        if(listAccountExclusion.size() > 0)
            return listAccountExclusion[0];
        else
            return new Account_Exclusion__c();
    }

    @AuraEnabled
    public static String saveAccountExclusion(Account_Exclusion__c accountExclusionObj, String operationType){

    	System.debug('accountExclusionObj ::: '+accountExclusionObj);
    	try
        {
            String returnMessage = '';
            System.debug('accountExclusionObj ' + accountExclusionObj.get('Id'));
            if(accountExclusionObj.get('Id') != null){
                returnMessage = string.format(System.Label.Account_Exclusion_Updated, new list<string>{(String)accountExclusionObj.get( SalesIQGlobalConstants.NAME_SPACE + 'Account_Name__c')});
                upsert accountExclusionObj;
            }
            else{
                upsert accountExclusionObj;
                String name = [Select Id, Account_Name__c From Account_Exclusion__c Where Id =: accountExclusionObj.Id].Account_Name__c;
                returnMessage = string.format(System.Label.Account_Exclusion_Created, new list<string>{name});
            }
            
            return returnMessage; 
    	}
        catch(Exception e)
        {
    		String message = e.getTypeName() + ': ' + e.getMessage() + '\n\n' + e.getStackTraceString() ;
            AuraHandledException ex = new AuraHandledException(message);
            ex.setMessage(message);
            throw ex;
    	}
    }

    @AuraEnabled
    public static void logException(String message,String module) 
    {
        if(message.length() > 2000)
           message = message.substring(0,2000); 
        SalesIQLogger.logErrorMessage(message, module);
    }
}