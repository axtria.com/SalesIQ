@isTest
public with sharing class TestAccountTrigger {
	static testMethod void testUpdate(){
		
		Account acc = new Account();
	    acc.Name = 'North East';
	    acc.BillingPostalCode='12232';
		insert acc;
		
		// Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new ESRIServicesMockGenerator());
		test.startTest();
			acc.BillingPostalCode = '13232';
			update acc;
	        test.stopTest();
	}
}