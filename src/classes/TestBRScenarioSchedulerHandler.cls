@isTest
public class TestBRScenarioSchedulerHandler {

    public static testMethod void test1() {
        Id recordId = Schema.SObjectType.SalesIQ_Logger__c.getRecordTypeInfosByName().get(CPGConstants.QUEUE_LOGGER_RECORDTYPE).getRecordTypeId();
        SalesIQ_Logger__c masterSynclog = new SalesIQ_Logger__c(Module__c=SalesIQLogger.BRMS_MODULE, Status__c=SalesIQGlobalConstants.IN_PROGRESS, Type__c= CPGConstants.QUEUE_LOGGER_TYPE_MASTER);
        masterSynclog.RecordtypeId= recordId;
        insert masterSynclog;
            
        Data_Set__c dataset =new Data_Set__c(Name='DataSet', Is_Master__c = true, is_internal__c = false, SalesIQ_Internal__c = true, Data_Set_Object_Name__c = 'Account');
        insert dataset;
        
        Data_Object__c dataObj = new Data_Object__c(dataset_id__c = dataset.Id);
        insert dataObj;

        ETL_Config__c etl = new ETL_Config__c(Name = 'BRMS');
        insert etl;
        
        Organization_Master__c org = CallPlanTestDataFactory.createOrganizationMasterRecord();
        insert org;
        Country__c country = CallPlanTestDataFactory.createCountryMasterRecord(org.id);
        insert country;
        
        Team__c team = new Team__c(Name='HTN',Type__c='Base',Effective_End_Date__c=Date.today().addMonths(50),Country__c=country.id);
        insert team;
        
        Workspace__c wrk = new Workspace__c();
        wrk.Workspace_Start_Date__c = Date.Today();
        wrk.Workspace_End_Date__c  = Date.today().addMonths(50);
        wrk.Name = 'Workspace 1';
        wrk.Workspace_Description__c = 'New Workspace';
        wrk.Country__c=country.id;
        insert wrk;
        
        Scenario__c scenario = new Scenario__c();
        scenario.CR_Tracking__c = false;
        scenario.Scenario_Name__c = 'HTN_Q3';
        scenario.Team_Name__c = team.id;
        scenario.Workspace__c = wrk.id;
        scenario.Request_Process_Stage__c = SalesIQGlobalConstants.READY;
        scenario.Scenario_Stage__c =    SalesIQGlobalConstants.COLLABORATION;
        
        Id recordId2 = Schema.SObjectType.Scenario__c.getRecordTypeInfosByName().get('Alignment Scenario').getRecordTypeId();
        scenario.RecordtypeId=recordId2;
        insert scenario;
        
        
        Scenario_Execution_Setting__c  scenarioExecSetting1 =new Scenario_Execution_Setting__c(Name='scenarioExecSetting1',Record_Id__c=team.id,Type__c=SalesIQGlobalConstants.SCENARIO_RECORDTYPE_ALIGNMENT, Scenario_Where_Clause__c='Id != null' , Additional_Where_Clause__c='Name != null');
        insert scenarioExecSetting1;
        
        Scenario_Execution_Setting__c  scenarioExecSetting2 =new Scenario_Execution_Setting__c(Name='scenarioExecSetting2',Record_Id__c=country.id,Type__c=SalesIQGlobalConstants.SCENARIO_RECORDTYPE_BLOCK, Scenario_Where_Clause__c='Id != null' , Additional_Where_Clause__c='Name != null');
        insert scenarioExecSetting2;

        Test.startTest();
        //BRScenarioSchedulerHandler.syncMasterData();
        //BRScenarioSchedulerHandler.queueScenariosForDeltaRun();
        //BRScenarioSchedulerHandler.queueExclusionScenarioForDeltaRun();
        String strJobName = 'Job-';
        String strSchedule = '20 30 8 10 2 ?';
        System.schedule(strJobName, strSchedule, new BRMasterDataSyncScheduler());
        strJobName='Job2-';
        System.schedule(strJobName, strSchedule, new BRRunExclusionScenarioScheduler());
        strJobName='Job3-';
        System.schedule(strJobName, strSchedule, new BRRunScenarioScheduler());
        
        BRScenarioSchedulerHandler.callpythonService(masterSynclog.id,dataset.id);

        Test.stopTest();
        scenario.Last_Promote_Success_Date__c = Datetime.now();
        update scenario;

        strJobName='Job4-';
        System.schedule(strJobName, strSchedule, new BRRunScenarioScheduler());

        String ns=SalesIQGlobalConstants.getOrgNameSpace();
        // Insert data set and data object
        Data_Set__c affDataset = new Data_Set__c(Data_Set_Object_Name__c = ns+'Account_Affiliation__c');
        insert affDataset;

        Data_Set_Column_Detail__c ds1 = new Data_Set_Column_Detail__c(dataset_id__c = affDataset.Id, Source_Column__c = ns+'Affiliation_Network__c', tb_col_nm__c = '');
        Data_Set_Column_Detail__c ds2 = new Data_Set_Column_Detail__c(dataset_id__c = affDataset.Id, Source_Column__c = ns+'Parent_Account__c', tb_col_nm__c = '');
        Data_Set_Column_Detail__c ds3 = new Data_Set_Column_Detail__c(dataset_id__c = affDataset.Id, Source_Column__c = ns+'Account__c', tb_col_nm__c = '');

        Data_Object__c dataObject = new Data_Object__c(dataset_id__c = affDataset.Id, status__c = SalesIQGlobalConstants.IN_PROGRESS);
        insert dataObject;

        dataObject.status__c = SalesIQGlobalConstants.SUCCESS;
        update dataObject;

        dataObject = [select final_Table__c from Data_Object__c where Id =: dataObject.Id];

        Affiliation_Network__c netwrk1 = new Affiliation_Network__c(Name='Test Netwrk 1');
        Affiliation_Network__c netwrk2 = new Affiliation_Network__c(Name='Test Netwrk 2');
        insert new Affiliation_Network__c[]{netwrk1, netwrk2};

        MockHttpResponseGenerator mockResponse = new MockHttpResponseGenerator();
        mockResponse.respType = 3;

        // mock JSON with 2 valid network response
        mockResponse.jsonMockResp = '[{"Id":"' + netwrk1.Id + '", "Level":5},{"Id":"' + netwrk2.Id + '", "Level":3}]' ;
        Test.setMock(HttpCalloutMock.class, mockResponse);

        SalesIQ_Logger__c accountNetworkDepthlog = [select Id from SalesIQ_Logger__c where Module__c=: SalesIQLogger.BRMS_MODULE and Type__c=: CPGConstants.AFFILIATION_NETWORK_DEPTH];

        String networkfield = 'affil_network';
        String parentfield = 'id_parent_account';
        String accountfield = 'id_account';
        BRScenarioSchedulerHandler.pythonAffliation(dataObject.final_Table__c,networkfield,parentfield,accountfield,accountNetworkDepthlog.id);
	}

    public static testMethod void test2() {

        ETL_Config__c etl = new ETL_Config__c(Name = 'BRMS');
        insert etl;
        
        MockHttpResponseGenerator mockResponse = new MockHttpResponseGenerator();
        mockResponse.respType = 3;
        Test.setMock(HttpCalloutMock.class, mockResponse);

        SalesIQ_Logger__c accountNetworkDepthlog = new SalesIQ_Logger__c(Module__c= SalesIQLogger.BRMS_MODULE, Type__c= CPGConstants.AFFILIATION_NETWORK_DEPTH);
        insert accountNetworkDepthlog;

        String networkfield = 'affil_network';
        String parentfield = 'id_parent_account';
        String accountfield = 'id_account';
        BRScenarioSchedulerHandler.pythonAffliation('t_fin_do_009',networkfield,parentfield,accountfield,accountNetworkDepthlog.id);
    }
}