@isTest
private class TestBRAssignmentCtrl{
	static testMethod void brAssignmentTest() {

		Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        insert workSpaceObj;
      
        ETL_Config__c etlConfig = new ETL_Config__c();
        etlConfig.Name = 'BRMS';
        etlConfig.BR_PG_Password__c = 'pass';
        etlConfig.BR_PG_Port__c = '1234';
        insert etlConfig;
        
		Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        scenarioObj.isUniversal__c = true;
        insert scenarioObj;

         Business_Rule_Template__c  busRuleTemObj = new Business_Rule_Template__c();
        busRuleTemObj.isActive__c=true;
        busRuleTemObj.isInternal__c=true;
        insert busRuleTemObj;

        BusinessRule_Template_Details__c busRuleTemDet = new BusinessRule_Template_Details__c();
        busRuleTemDet.is_Visible__c=true;
        busRuleTemDet.Business_Rule_Template_Id__c=busRuleTemObj.id;
        insert busRuleTemDet;

        ComponentTypeMaster__c ComTypeMasObj = new ComponentTypeMaster__c();
        ComTypeMasObj.Name='Call Balancing';
        ComTypeMasObj.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj.DisplayIcon__c='add';
        ComTypeMasObj.DisplayIconClass__c='window information';
        insert ComTypeMasObj;

        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        sceRuleInsDetObj.Component_Display_Order__c=4;
        sceRuleInsDetObj.Is_Visible__c=true;
        sceRuleInsDetObj.Component_Type_Master__c=ComTypeMasObj.id;
        sceRuleInsDetObj.BRT_Details_Id__c=busRuleTemDet.id;
        insert sceRuleInsDetObj;

         Scenario_Rule_Instance_Details__c sceRuleInsDetObj1 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj1.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj1.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        sceRuleInsDetObj1.Component_Display_Order__c=4;
        sceRuleInsDetObj1.Is_Visible__c=true;
        sceRuleInsDetObj1.Component_Type_Master__c=ComTypeMasObj.id;
        sceRuleInsDetObj1.BRT_Details_Id__c=busRuleTemDet.id;
        insert sceRuleInsDetObj1;

        Data_Set__c dataSetObj = new Data_Set__c();
        dataSetObj.Name = 'Call Plan Summary';
        insert dataSetObj;
        System.debug('dataSetObj Id:: '+dataSetObj.Id);

        Data_Set__c dataSetObj1 = new Data_Set__c();
        dataSetObj1.Name = 'Final Call Plan';
        insert dataSetObj1;
        System.debug('dataSetObj1 Id:: '+dataSetObj1.Id);

         Data_Set_Rule_Map__c dataSetRuleObj = new Data_Set_Rule_Map__c();
        dataSetRuleObj.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj.ds_type__c='I';
        dataSetRuleObj.dataset_id__c=dataSetObj.id;
        insert dataSetRuleObj;

        Data_Set_Rule_Map__c dataSetRuleObj2 = new Data_Set_Rule_Map__c();
        dataSetRuleObj2.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj2.ds_type__c='O';
        dataSetRuleObj2.dataset_id__c=dataSetObj.id;
        insert dataSetRuleObj2;

        Data_Set_Rule_Map__c dataSetRuleObj3 = new Data_Set_Rule_Map__c();
        dataSetRuleObj3.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj3.ds_type__c='I';
        dataSetRuleObj3.dataset_id__c=dataSetObj.id;
        insert dataSetRuleObj3;

        Data_Set_Column_Detail__c dataSetColObj = new Data_Set_Column_Detail__c();
        dataSetColObj.dataset_id__c=dataSetObj.id;
        dataSetColObj.ds_col_name__c='AccountNumber';
        dataSetColObj.tb_col_nm__c = 'account_number';
        dataSetColObj.dataset_id__c = dataSetObj.id;
        dataSetColObj.datatype__c = 'Text';
        dataSetColObj.JoinStatus_Flag__c = true;
        dataSetColObj.Table_Data_Type__c = 'text';
        dataSetColObj.variable_type__c = 'Source';
        dataSetColObj.value_type__c = 'Discrete';
        dataSetColObj.Name = 'Customer Universe';

        insert dataSetColObj;

        Data_Set_Column_Detail__c dataSetColObj1 = new Data_Set_Column_Detail__c();
        dataSetColObj1.ds_col_name__c='AcctNum';
        dataSetColObj1.tb_col_nm__c = 'account_num';
        dataSetColObj1.dataset_id__c = dataSetObj1.id;
        dataSetColObj1.datatype__c = 'Text';
        dataSetColObj1.JoinStatus_Flag__c = true;
        dataSetColObj1.Table_Data_Type__c = 'text';
        dataSetColObj1.variable_type__c = 'Source';
        dataSetColObj1.value_type__c = 'Discrete';
        dataSetColObj1.Name = 'Customer Universe';
        dataSetColObj1.Key_Column__c = true;
        insert dataSetColObj1;

        Business_Rule_Type_Master__c busRuleTypeMasObj8 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj8.Name='Assignment Rules';
        busRuleTypeMasObj8.Component_Type__c = ComTypeMasObj.Id;
        insert busRuleTypeMasObj8;

        Business_Rules__c busRuleObjCom = new Business_Rules__c();
        busRuleObjCom.Name='Join rule 66';
        busRuleObjCom.Description__c = 'Test';
        busRuleObjCom.Key_Column_Value__c = 'a1N1N000004I8YuUAK';
        busRuleObjCom.Key_Column_Name__c = 'Remicade';
        busRuleObjCom.Key_Display_Column_Value__c = 'Remicade';
        busRuleObjCom.Business_Rule_Type_Master__c=busRuleTypeMasObj8.id;
        busRuleObjCom.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObjCom.BusinessRuleTemplateDetailsId__c=busRuleTemDet.id;
        busRuleObjCom.Scenario__c = scenarioObj.id;
        insert busRuleObjCom;

        BR_Join_Rule_Condition__c joinrulecondobj = new BR_Join_Rule_Condition__c();
        joinrulecondobj.Business_Rules__c = busRuleObjCom.Id;
        joinrulecondobj.Data_Set_1_Field__c = dataSetColObj.Id;
        joinrulecondobj.Data_Set_1__c = dataSetObj.Id;
        joinrulecondobj.Data_Set_2_Field__c = dataSetColObj1.Id;
        joinrulecondobj.Data_Set_2__c = dataSetObj1.Id;
        joinrulecondobj.JoinType__c = 'INNER';
        joinrulecondobj.Scenario__c = scenarioObj.Id;
        insert joinrulecondobj;

        BRJoinRuleSelectCondition__c selectcondobj = new BRJoinRuleSelectCondition__c();
        selectcondobj.Business_Rules__c = busRuleObjCom.Id;
        selectcondobj.Data_Set_Field__c = dataSetColObj.Id;
        selectcondobj.Data_Set__c = dataSetObj.Id;
        selectcondobj.Scenario__c = scenarioObj.Id;
        insert selectcondobj;

        BR_JoinRule__c joinruleobj = new BR_JoinRule__c();
        joinruleobj.Business_Rules__c = busRuleObjCom.Id;
        joinruleobj.Scenario__c = scenarioObj.Id;
        joinruleobj.insert_exec_expr__c = 'cust_team,cust_number,custname,street,city,state,zip,country,latitude,longitude,first_name,last_name,segment,tier,trx,decile,subcategory,middle_name,speciality,cust_number_ext,team_id,team_name,product_id,product_code,product_name';
        joinruleobj.Join_Rule_Execute_Expression__c = '(SELECT cust_team,cust_number,custname,street,city,state,zip,country,latitude,longitude,first_name,last_name,segment,tier,trx,decile,subcategory,middle_name,speciality,cust_number_ext,a1N1N000004I8Z9UAK as product_id FROM t_cust_universe_a1y1n000004xor4uag where ( first_name not like "a" )) as t_cust_universe_a1y1n000004xor4uag INNER JOIN t_product_team_a1y1n000004xor4uag ON t_cust_universe_a1y1n000004xor4uag.product_id = t_product_team_a1y1n000004xor4uag.product_id';
        joinruleobj.Scenario__c = scenarioObj.Id;
        joinruleobj.Select_Execute_Expression__c = 'SELECT t_cust_universe_a1y1n000004xor4uag.cust_team,t_cust_universe_a1y1n000004xor4uag.cust_number,t_cust_universe_a1y1n000004xor4uag.custname,t_cust_universe_a1y1n000004xor4uag.street,t_cust_universe_a1y1n000004xor4uag.city,t_cust_universe_a1y1n000004xor4uag.state,t_cust_universe_a1y1n000004xor4uag.zip,t_cust_universe_a1y1n000004xor4uag.country,t_cust_universe_a1y1n000004xor4uag.latitude,t_cust_universe_a1y1n000004xor4uag.longitude,t_cust_universe_a1y1n000004xor4uag.first_name,t_cust_universe_a1y1n000004xor4uag.last_name,t_cust_universe_a1y1n000004xor4uag.segment,t_cust_universe_a1y1n000004xor4uag.tier,t_cust_universe_a1y1n000004xor4uag.trx,t_cust_universe_a1y1n000004xor4uag.decile,t_cust_universe_a1y1n000004xor4uag.subcategory,t_cust_universe_a1y1n000004xor4uag.middle_name,t_cust_universe_a1y1n000004xor4uag.speciality,t_cust_universe_a1y1n000004xor4uag.cust_number_ext,t_product_team_a1y1n000004xor4uag.team_id,t_product_team_a1y1n000004xor4uag.team_name,t_product_team_a1y1n000004xor4uag.product_id,t_product_team_a1y1n000004xor4uag.product_code,t_product_team_a1y1n000004xor4uag.product_name';
        joinruleobj.where_display_expression__c = '( FirstName__c does not contains a )';
        joinruleobj.where_expression__c = '{"bindClause":"AND","criterias":[{"anotherValue":"","dataType":"text","field":"first_name","innerConditionWrapper":{},"isGroup":false,"operator":"does_not_contains","value":"a"}]}';
        insert joinruleobj;

        BRAssignmentCtrl.BRMAssignmentFieldsWrapper fieldobj = new BRAssignmentCtrl.BRMAssignmentFieldsWrapper();
        fieldobj.columnId = '';
        fieldobj.tableColumnName = dataSetColObj.tb_col_nm__c;
        fieldobj.colDatatype = dataSetColObj.datatype__c;
        fieldobj.colValuetype = dataSetColObj.value_type__c;
        fieldobj.statusFlag = dataSetColObj.JoinStatus_Flag__c;
        fieldobj.datasourceName = dataSetColObj.Name;
        fieldobj.datasourceColumnName = dataSetColObj.ds_col_name__c;
        fieldobj.datasetId = dataSetObj.id;
        fieldobj.tableDataType = dataSetColObj.Table_Data_Type__c;
        fieldobj.variableType = dataSetColObj.variable_type__c;
        fieldobj.Id = dataSetColObj.Id;
        List<BRAssignmentCtrl.BRMAssignmentFieldsWrapper> assignmentFieldsWrapperList = new List<BRAssignmentCtrl.BRMAssignmentFieldsWrapper>();
        assignmentFieldsWrapperList.add(fieldobj);

        BRAssignmentCtrl.BRMAssignmentFieldsWrapper fieldobj1 = new BRAssignmentCtrl.BRMAssignmentFieldsWrapper();
        fieldobj1.columnId = '';
        fieldobj1.tableColumnName = dataSetColObj1.tb_col_nm__c;
        fieldobj1.colDatatype = dataSetColObj1.datatype__c;
        fieldobj1.colValuetype = dataSetColObj1.value_type__c;
        fieldobj1.statusFlag = dataSetColObj1.JoinStatus_Flag__c;
        fieldobj1.datasourceName = dataSetColObj1.Name;
        fieldobj1.datasourceColumnName = dataSetColObj1.ds_col_name__c;
        fieldobj1.datasetId = dataSetObj1.id;
        fieldobj1.tableDataType = dataSetColObj1.Table_Data_Type__c;
        fieldobj1.variableType = dataSetColObj1.variable_type__c;
        fieldobj1.Id = dataSetColObj1.Id;
        assignmentFieldsWrapperList.add(fieldobj1);

        BRAssignmentCtrl.BRMRuleInfoWrapper rulewrapobj = new BRAssignmentCtrl.BRMRuleInfoWrapper();
        rulewrapobj.rulename = busRuleObjCom.Name;
        rulewrapobj.ruledescription = busRuleObjCom.Description__c;
      //  rulewrapobj.keycolumnvalues = Map<'',''>;
        rulewrapobj.ruleValue = busRuleObjCom.Key_Column_Value__c;
        rulewrapobj.keycolumnname = busRuleObjCom.Key_Column_Name__c;
        rulewrapobj.keydisplaycolumnvalue = busRuleObjCom.Key_Display_Column_Value__c;
        rulewrapobj.businessRuleId = busRuleObjCom.id;
       // rulewrapobj.stagingQuery = joinruleobj.where_display_expression__c;
        rulewrapobj.parsedQuery = joinruleobj.where_display_expression__c;
        rulewrapobj.parsedJSONQuery = joinruleobj.where_expression__c;
        
        List<BRAssignmentCtrl.BRMRuleInfoWrapper> ruleInfoWrapperList = new List<BRAssignmentCtrl.BRMRuleInfoWrapper>();
        ruleInfoWrapperList.add(rulewrapobj);

        BRAssignmentCtrl.BRDataWrapper datawrapobj = new BRAssignmentCtrl.BRDataWrapper();

        datawrapobj.previousRuleName = busRuleObjCom.Name;
        datawrapobj.previousRuleDescription = busRuleObjCom.Description__c;
        datawrapobj.previousWhereClause = joinruleobj.where_display_expression__c;

        BRAssignmentCtrl.BRInstanceNameWrapper instanceWrapobj = new BRAssignmentCtrl.BRInstanceNameWrapper();

        instanceWrapobj.brassignmenturl = 'http://52.24.8.231/BRMS/getData';
        instanceWrapobj.brdbname = 'AxtriaSalesIQTM_QA_4.5';
        instanceWrapobj.keycolumn = 'product_id';
        instanceWrapobj.keydisplaycolumn = 'product_name';
        instanceWrapobj.tablename = 't_fin_do33';

         List<BRAssignmentCtrl.BRDataWrapper> dataWrapperList = new List<BRAssignmentCtrl.BRDataWrapper>();
        dataWrapperList.add(datawrapobj);

        String keycolumn = 'product_id';
        String keydisplaycolumn = 'product_name';
        String stagingQuery = '';
       
	

        Test.startTest();
      
        Map<String,String> serviceResponse = new Map<String,String>();
        serviceResponse.put('finalURL','finalURL');
        MockHttpResponseGenerator mocker = new MockHttpResponseGenerator();
        mocker.respType = 6;
        Test.setMock(HttpCalloutMock.class, mocker);
        
        BRAssignmentCtrl.BRMRuleInfoWrapper obj = BRAssignmentCtrl.assignedKeyColumn(scenarioObj.id,sceRuleInsDetObj.Id,false,serviceResponse,keycolumn,keydisplaycolumn, 'where_claus');
          
         
         BRAssignmentCtrl.AssignmentRulesComponentWrapper getInputOutputDatasourcesList = BRAssignmentCtrl.getInputOutputDatasources(scenarioObj.id,sceRuleInsDetObj.Id);
         List<BRAssignmentCtrl.BRMAssignmentFieldsWrapper> fieldswrapperList = BRAssignmentCtrl.getColumns(dataSetObj.Id,dataSetObj1.Id);
         List<BRAssignmentCtrl.BRDataWrapper> datawrapList = BRAssignmentCtrl.getPreviousData(busRuleObjCom.id);
         List<BRAssignmentCtrl.BRMRuleInfoWrapper> ruleWrapList = BRAssignmentCtrl.setRuleValues(scenarioObj.id,sceRuleInsDetObj.Id,busRuleObjCom.Name,busRuleObjCom.Description__c,busRuleObjCom.Key_Column_Value__c,busRuleObjCom.Key_Column_Name__c,busRuleObjCom.id);
        Boolean ruleDeleteVar = BRAssignmentCtrl.ruleDelete(busRuleObjCom.id);
        List<BRAssignmentCtrl.BRInstanceNameWrapper> nameWrapList = BRAssignmentCtrl.getInstanceNameAndURL(sceRuleInsDetObj.Id,dataSetObj1.Id,scenarioObj.id);
        String ruleInfoWrapperList1='{"businessRuleId":"'+busRuleObjCom.id+'","keycolumnname":"","keycolumnvalues":{"a1N1N000004I8Z9UAK":"Xildra"},"keydisplaycolumnalue":"","parsedJSONQuery":"","ruledescription":"","rulename":"Rule 6","ruleValue":"a1N1N000004I8Z9UAK","stagingQuery":"","parsedQuery":""}';
        String fieldsWrapperListSerialized = JSON.serialize(fieldswrapperList);
         List<String> lstfmtddates = new List<String>();
        lstfmtddates.add('2018-07-06 13:28:54');
        lstfmtddates.add('2018-07-06 13:28:54');
        lstfmtddates.add('2018-07-03 13:28:54');
        lstfmtddates.add('2018-07-05 13:28:54');
        List<String> lstdates = BRAssignmentCtrl.getformatedDates(lstfmtddates);
        BRAssignmentCtrl.saveAssignmentRule(ruleInfoWrapperList1,scenarioObj.id,sceRuleInsDetObj.Id,joinruleobj.where_display_expression__c,stagingQuery,joinruleobj.where_expression__c,dataSetObj.Id,dataSetObj1.Id,fieldsWrapperListSerialized,keydisplaycolumn,keycolumn);
        BRAssignmentCtrl.saveUpdatedAssignmentRules(ruleInfoWrapperList1,scenarioObj.id,sceRuleInsDetObj.Id,joinruleobj.where_display_expression__c,stagingQuery,joinruleobj.where_expression__c,dataSetObj.Id,dataSetObj1.Id,fieldsWrapperListSerialized,keydisplaycolumn,keycolumn);
        List<BRAssignmentCtrl.BRMRuleInfoWrapper> ruleWrapList1 = BRAssignmentCtrl.getBusinessRules(scenarioObj.id,sceRuleInsDetObj.Id,true);
        
          
        
        Test.stopTest();

    }
}