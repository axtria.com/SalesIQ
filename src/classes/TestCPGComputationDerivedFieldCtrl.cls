/*
Author : Ankit Manendra
Description : Test class for CPGComputationDerivedFieldCtrl
Date : 2 Novomber, 2017

*/

@isTest
private class TestCPGComputationDerivedFieldCtrl{
    static testMethod void cpgComDerTest() {

    Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj;
        
        Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        insert scenarioObj;
        
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj;
        
         Scenario_Rule_Instance_Details__c sceRuleInsDetObj1 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj1.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj1.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj1;
        
        Data_Set_Rule_Map__c dataSetRuleObj = new Data_Set_Rule_Map__c();
        dataSetRuleObj.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj.ds_type__c='I';
        insert dataSetRuleObj;
        
        system.debug('==dataSetRuleObj===='+dataSetRuleObj);
        
        Business_Rule_Type_Master__c busRuleTypeMasObj = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj.Name='Rank';
        insert busRuleTypeMasObj;
        
        Business_Rules__c busRuleObj = new Business_Rules__c();
        busRuleObj.Name='Rank rule 278';
        busRuleObj.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObj.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        insert busRuleObj;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj.Param_Name__c='derived field 278';
        busRuleFieldObj.Business_Rule__c=busRuleObj.id;
        busRuleFieldObj.Param_Data_Type__c='Numeric';
        busRuleFieldObj.Param_Type__c='Derived';
        busRuleFieldObj.IO_Flag__c='O';
        
        insert busRuleFieldObj;
        
        Computation_Rule_Detail__c compRuleDetObj = new Computation_Rule_Detail__c();
        compRuleDetObj.Business_Rules__c=busRuleObj.id;
        compRuleDetObj.Data_Type__c='Numeric';
        compRuleDetObj.Derived_field_name__c='FIELD 123';
        
        insert compRuleDetObj;
        
        string strJsonBusinessRules = '{"Business_Rules__c":{"'+SObjectDescribe.namespacePrefix+'Business_Rule_Type_Master__c":"'+busRuleTypeMasObj.id+'","Name":"NEW RULE","'+SObjectDescribe.namespacePrefix+'Is_Derived__c":true,"'+SObjectDescribe.namespacePrefix+'Is_Mandatory__c":true,"'+SObjectDescribe.namespacePrefix+'ScenarioRuleInstanceDetails__c":' + '"'+ sceRuleInsDetObj.id + '"}}';  //'"a1Mf4000000ou48EAA"}}';
        system.debug('===strJsonBusinessRules===='+strJsonBusinessRules);

        //string strJsonComputationRuleDetail='[{"Computation_Rule_Detail__c":[{"Name":"a1Tf4000000dIlH","Display_expression__c":"REMS_FLG +7 ","Execute_expression__c":"rems_flg~+~7","Derived_field_name__c":"FIELD 123","Value_Type__c":"Continuous","Data_Type__c":"Numeric"}]}]';
        
        string strJsonComputationRuleDetail='[{"Name":"a1Tf4000000dIlH","'+SObjectDescribe.namespacePrefix+'Display_expression__c":"REMS_FLG +7 ","'+SObjectDescribe.namespacePrefix+'Execute_expression__c":"rems_flg~+~7","'+SObjectDescribe.namespacePrefix+'Derived_field_name__c":"FIELD 123","'+SObjectDescribe.namespacePrefix+'Value_Type__c":"Continuous","'+SObjectDescribe.namespacePrefix+'Data_Type__c":"Numeric"}]';
        string strJsonBusinessRuleFieldMapDetails='[{"'+SObjectDescribe.namespacePrefix+'IO_flag__c":"O","'+SObjectDescribe.namespacePrefix+'Param_Name__c":"FIELD 123","'+SObjectDescribe.namespacePrefix+'Param_Data_Type__c":"Numeric","'+SObjectDescribe.namespacePrefix+'Param_type__c":"Derived"}]';
        
        List<BRFieldWrapper> fieldWrapList =  CPGComputationDerivedFieldCtrl.GetSourceAndDerivedFields(sceRuleInsDetObj.id);
        
        //CPGConstants.Response res2 = CPGComputationDerivedFieldCtrl.DeleteComputationType(busRuleObj.id,sceRuleInsDetObj.id);
        List_Of_Operators__mdt[] listOfOper = CPGComputationDerivedFieldCtrl.getOperators('Add');
        Rule_Types__mdt[] ruleType = CPGComputationDerivedFieldCtrl.getRuleTypeDetails();
        List_Of_Operators__mdt[] listOfMdt = CPGComputationDerivedFieldCtrl.getOperatorTypeValues();
        Types_List__mdt[] typemdt = CPGComputationDerivedFieldCtrl.getTypesList();
        List<Business_Rule_Type_Master__c> listBusRuleType = CPGComputationDerivedFieldCtrl.getBusinessRuleTypeMaster('test');
        List<Data_Set_Rule_Map__c> listDataSet = CPGComputationDerivedFieldCtrl.getInputTableName(sceRuleInsDetObj.id);
        
        CPGConstants.Response res = CPGComputationDerivedFieldCtrl.SaveComputationType(strJsonBusinessRules,strJsonComputationRuleDetail,strJsonBusinessRuleFieldMapDetails);
        
        string strJsonBusinessRulesUpdate = '{"Business_Rules__c":{"'+SObjectDescribe.namespacePrefix+'Business_Rule_Type_Master__c":"'+busRuleTypeMasObj.id+'","Name":"UPDATE RULE","'+SObjectDescribe.namespacePrefix + 'Is_Derived__c":true,"'+SObjectDescribe.namespacePrefix+'Is_Mandatory__c":true,"' + SObjectDescribe.namespacePrefix + 'ScenarioRuleInstanceDetails__c":' + '"'+ sceRuleInsDetObj.id +'"'+ ',"Id":'+'"'+ busRuleObj.id +'"'+  '}}';
        
        CPGConstants.Response res1 = CPGComputationDerivedFieldCtrl.UpdateComputationType(strJsonBusinessRulesUpdate,strJsonComputationRuleDetail,strJsonBusinessRuleFieldMapDetails);
        //List<Data_Set_Rule_Map__c> dataSetRuleList = CPGComputationDerivedFieldCtrl.getInputTableName(sceRuleInsDetObj.id);
        

        // for coverage

        CPGComputationDerivedFieldCtrl.DeleteComputationType('','');
        CPGComputationDerivedFieldCtrl.getOrgNamespace();

        CPGComputationDerivedFieldCtrl.GetSourceAndDerivedFields('');

        // create duplicate name Businee rules
        Business_Rules__c busRuleObjDuplicate = new Business_Rules__c();
        busRuleObjDuplicate.Name='UPDATE RULE';
        busRuleObjDuplicate.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObjDuplicate.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        insert busRuleObjDuplicate;

        CPGComputationDerivedFieldCtrl.UpdateComputationType(strJsonBusinessRulesUpdate,strJsonComputationRuleDetail,strJsonBusinessRuleFieldMapDetails);
        //UpdateComputationType
    }
}