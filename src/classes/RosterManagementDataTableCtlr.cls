/**********************************************************************************************
@author     :  Aditi Singh
@date       : 
@description: This is the controller for main workbench screen of roster
Revison(s)  : 
**********************************************************************************************/
global with sharing class RosterManagementDataTableCtlr {
    public static String namespace {get{ return SalesIQGlobalConstants.NAME_SPACE;} set;}
    public String selectedCycle {get;set;}
    public Map <String,Position__c> allPositionMap;
    private static Map <Id, List<Position__c>> parentChildPositionMap;
    public String selectedPosition{get;set;}
    public static String teamVal{get;set;}
    public String teamInstanceVal{get;set;}
    public String team{get;set;}
    public String posToDelete{get;set;}
    public String posToDeleteEmp{get;set;}
    public String posToDeleteParent{get;set;}
    public String posToDeleteTeam{get;set;}
    public position__c positionToDelete{get;set;}
    public boolean openDeletePosPopup{get;set;}
    public String type{get;set;}
    public boolean workbenchpopup{get;set;}
    public list<SelectOption> listOfEvents{get;set;}
    public String selectedEvent{get;set;}
    public boolean createPosPopup{get;set;}
    public static String newPosTeam{get;set;}
    public String newPosClientPositionCode{get;set;}
    public String newPosClientTerritoryName{get;set;}
    public String terrSelectVal{get;set;}
    public Change_Request__c executionDate{get;set;}
    public String territoryId{get;set;}
    public String rootPositionId{get;set;}
    list<CR_Position__c>positionEvents;
    Position__c deletedPosition;
    public String selectedEventName{get;set;}
    public String selectedEventId{get;set;}
    public boolean addZipsToPosPopup{get;set;}
    public String selectedPosRecord{get;set;}
    public String posToAddInWorkbench{get;set;}
    public String posType{get;set;}
    list<Position_Employee__c> checkEmp;
    public String newPosParentPos{get;set;}
    public Position__c newPosInstance{get;set;}
    public boolean editPosPopup{get;set;}
    public boolean UpdateAttributeBoolean{get;set;}
    public boolean deletePosPopup{get;set;}
    public position__c editedPosition{get;set;}
    public list<SelectOption> lstTeam{get;set;}
    public boolean isDeleteComplete{get;set;}
    public boolean isCreateComplete{get;set;}
    string posToDeleteParentID{get;set;}
    public boolean RosterAccess{get;set;}
    public date StartDate{get;set;}
    public date EndDate{get;set;}
    public string operationType{get;set;}
    public boolean createPositionAccess{get;set;}
    public boolean editPositionAccess{get;set;}
    public boolean updateAttributesAccess{get;set;}
    public boolean deletePositionAccess{get;set;}
    public boolean hierarchyChangeAccess{get;set;}
    public boolean accessManageAssignment{get;set;}
    
    public boolean terrPos{get;set;}
    public boolean showCRMsg {get; set;}
    public string crname {get; set;}
    public boolean isOverlay {get;set;}
    public string message {get;set;}
    
    public string teamType {get;set;}
    public string baseTeamId{get;set;}
    public string baseTeamInstanceId{get;set;}
    public string levelAccessJsonMap{get;set;}

    public Position__c selectedPos;
    public String selectedCycleInstanceName {get;set;}
    
    public Cr_Team_Instance_Config__c crConfigTeamIns {get;set;}
    public Cr_Team_Instance_Config__c crConfigTeamInsCreate {get;set;}
    
    public String orderByClause{get;set;}
    public Boolean isPageLoadOK {get; set;} 
	public String isPrimary {get;set;}

    public String severity{get;set;}
    public Boolean showMessage{get;set;}
    public String notificationTimer{get;set;}
    public boolean crTrackingOn{get;set;}
	
    public Integer currentUserTimeOffset {get {return (UserInfo.getTimezone().getOffset(Datetime.now()))/1000 ;} private set;}

    public RosterManagementDataTableCtlr(){
        try {
            isPageLoadOK = true;
            allPositionMap = new map<String, Position__c>();
        	RosterAccess = SalesIQUtility.getRosterAccessPermission();
        	positionToDelete = new position__c();
            deletedPosition = new Position__c();
            executionDate = new Change_Request__c();
            positionEvents = new list<CR_Position__c>();
            newPosInstance = new Position__c();
            isDeleteComplete = true;
            isCreateComplete = true;
            terrPos = false;
            newPosInstance.Effective_Start_Date__c = date.newInstance(2016, 4, 3);
            newPosInstance.Effective_End_Date__c =  Date.newInstance(2099, 12, 31);
            StartDate = date.newInstance(2016, 4, 3);
            EndDate = Date.newInstance(2099, 12, 31);
            posType = '';
            lstTeam = new list<SelectOption>();
            lstTeam =SalesIQUtility.getTeamList();
            isOverlay = true;
            showMessage = false;
            
            if(Alignment_Global_Settings__c.getValues('notificationTimeOut') != null){
                notificationTimer = Alignment_Global_Settings__c.getValues('notificationTimeOut').Tree_Hierarchy_Sort_Field__c;
            }else{
                notificationTimer = '10000';
            }


            if(Alignment_Global_Settings__c.getValues('global') != null)
                orderByClause = Alignment_Global_Settings__c.getValues('global').Tree_Hierarchy_Sort_Field__c ;

            if(orderByClause == null || orderByClause == '')
                orderByClause = 'Name' ;
            
            if(ApexPages.currentPage().getParameters().get('teamID') != null){
                teamVal = ApexPages.currentPage().getParameters().get('teamID');
                team = [select id, name from team__c where id=:teamVal].name;

                newPosTeam = teamVal;
                RosterInit(newPosTeam);
            }
            else{
                if(lstTeam!=null && lstTeam.size() > 0){
                	
                	teamVal = String.valueOf(lstTeam[0].getValue());
                	team  = String.valueOf(lstTeam[0].getLabel());
                    
                    newPosTeam = teamVal;
        	        RosterInit(newPosTeam); 
                }
            }    
            checkActionInit(); 
    	}catch(exception ex){
    		SalesIQLogger.logMessage(ex);
    		isPageLoadOK = false;
    	}
    }
    
    public void actionInit() {
        // check the security util for getters methods and action fucntion called on page load
        try {
            // Check from TreeUtility.init method (getJsonString method)
            list<string> USER_ACCESS_PERMISSION_READ_FIELD = new list<string>{'id','Position__c', 'Team_Instance__c', 'User__c', 'Is_Active__c'};
            list<String> TEAM_INSTANCE_READ_FIELD = new list<String>{'Team__c','Name','Alignment_Type__c','ReadOnlyCreatePosition__c','ReadOnlyEditPosition__c','ReadOnlyDeletePosition__c','IsAllowedManageAssignment__c'};
            list<String> POSITION_READ_FIELD = new list<String>{'id','Name','Client_Territory_Name__c','Parent_Position__c', 'createdDate', 'Effective_start_Date__c', 'Effective_end_Date__c', 'Employee__c','Client_Position_Code__c','Position_Type__c','Hierarchy_Level__c','RGB__c','Team_iD__c','inactive__c','X_Max__c', 'X_Min__c', 'Y_Max__c', 'Y_Min__c', 'Related_Position_Type__c', 'Change_Status_del__c', 'Client_Territory_Code__c'};
            
            SecurityUtil.checkRead(User_Access_Permission__c.SObjectType, USER_ACCESS_PERMISSION_READ_FIELD, false);
            SecurityUtil.checkRead(Position__c.SObjectType, POSITION_READ_FIELD, false) ;
            SecurityUtil.checkRead(Team_Instance__c.SObjectType, TEAM_INSTANCE_READ_FIELD, false) ;

            // Check from TreeUtility.getJsonString method (getJsonString method)
            list<String> POSITION_TEAM_INS_READ_FIELD = new list<String>{'Position_ID__c','Parent_Position_ID__c','X_Max__c','X_Min__c','Y_Max__c','Y_Min__c','Effective_End_Date__c','Effective_Start_Date__c'};
            SecurityUtil.checkRead(Position_Team_Instance__c.SObjectType, POSITION_TEAM_INS_READ_FIELD, false);
            SecurityUtil.checkRead(Position__c.SObjectType, POSITION_READ_FIELD, false);

            // Check from SalesIqUtility.getPositionbyRootPositionIdRoster (refreshListViewBasedOnTeamInstance method)
            SecurityUtil.checkRead(Position__c.SObjectType, POSITION_READ_FIELD, false); 
            SecurityUtil.checkObjectIsReadable(Team__c.SObjectType);
        }catch(exception ex){
            SalesIQLogger.logMessage(ex);
            isPageLoadOK = false;
        }
    }

    public void checkActionInit(){
        List<String> TEAM_INSTANCE_OBJECT_ATTRIBUTE_READ_FIELDS = new List<String>{'isAutoSuggested__c', 'Attribute_API_Name__c','Attribute_Display_Name__c','Data_Type__c','Display_Column_Order__c','Object_Name__c','isRequired__c','Team_Instance__c','Interface_Name__c', 'isEditable__c'};
        
        Set<string> listOperationToCheck = new Set<string>();
        Team_Instance__c teamInstanceRestrict = SalesIQUtility.getTeamInstanceById(selectedCycle);
        
        ReadOnlyApplication__c readOnlyApplication = ReadOnlyApplication__c.getInstance(UserInfo.getProfileId());
        if(teamInstanceRestrict.ReadOnlyCreatePosition__c == false && readOnlyApplication.RosterPositionAccess__c == false){
            listOperationToCheck.add(SalesIQGlobalConstants.ROSTER_CREATE_POSITION_POPUP);
        }

        if(teamInstanceRestrict.ReadOnlyDeletePosition__c == false && readOnlyApplication.RosterDeletePosition__c == false){
            listOperationToCheck.add(SalesIQGlobalConstants.ROSTER_DELETE_POSITION_POPUP);
        }

        if(teamInstanceRestrict.ReadOnlyEditPosition__c == false && readOnlyApplication.RosterEditPosition__c == false){
            listOperationToCheck.add(SalesIQGlobalConstants.ROSTER_EDIT_POSITION_POPUP);
        }

        if(teamInstanceRestrict.Restrict_Hierarchy_Change__c == false && readOnlyApplication.RosterHierarchyChange__c == false){
            listOperationToCheck.add(SalesIQGlobalConstants.ROSTER_HIERARCHY_CHANGE_POPUP);
        }


        list<Team_instance_object_attribute__c> fetchAllRequiredColumns = new list<Team_instance_object_attribute__c>();
        if(SecurityUtil.checkRead(Team_Instance_Object_Attribute__c.SObjectType, TEAM_INSTANCE_OBJECT_ATTRIBUTE_READ_FIELDS, false))
        {
            fetchAllRequiredColumns = [Select isAutoSuggested__c, Attribute_API_Name__c, Attribute_Display_Name__c, Data_Type__c, Display_Column_Order__c, Object_Name__c, isEditable__c, isRequired__c from Team_Instance_Object_Attribute__c where isEnabled__c = true and Team_Instance__c =: selectedCycle and Interface_Name__c in: listOperationToCheck order by Display_Column_Order__c limit 10];   
        }

        list<string> readFields = new list<string>();
        list<string> editFields = new list<string>();
        
        for(Team_Instance_Object_Attribute__c teamInsObjAtt : fetchAllRequiredColumns){
            if(teamInsObjAtt.isEditable__c){
                editFields.add(teamInsObjAtt.Attribute_API_Name__c);
            }
            else{
                readFields.add(teamInsObjAtt.Attribute_API_Name__c);
            }
        }

        SecurityUtil.checkRead(Position__c.SObjectType, readFields, false) ;
        SecurityUtil.checkUpdate(Position__c.SObjectType, editFields, false) ;

        if(listOperationToCheck.contains(SalesIQGlobalConstants.ROSTER_CREATE_POSITION_POPUP)) {
            SecurityUtil.checkInsert(Position__c.SObjectType, editFields, false) ;
        }
    }

    public void updateshowCRMesgFlag(){
        if(!crTrackingOn){
            message = System.Label.Changes_Successful;
            setMessageParams( System.Label.Message_Theme_Success ,message);
        }
        else{
            message = string.format(System.Label.Change_Request_Submitted,new list<string>{crname});
            setMessageParams( System.Label.Message_Theme_Success ,message);    
        }
    	
    	posRgbMap = getAllPositionsRGBs();
    }
        
    static final List<String> USER_ACCESS_PERMISSION_READ_FIELD = new List<String>{'Id', 'Position__c',  'Team_Instance__c','Is_Active__c'} ;
    static final List<String> TEAM_INSTANCE_READ_FIELD = new List<String>{'Team__c', 'Name',  'Alignment_Type__c'};
    static final List<String> TEAM_READ_FIELD =  new List<String>{'Name', 'Effective_End_Date__c',  'Effective_Start_Date__c','Type__c'};
    static final List<String> POSITION_READ_FIELD = new List<String>{'team_id__c','isRuleBased__c', 'isGeoBased__c',  'Team_iD__c','RGB__c','Position_Type__c','Parent_Position__c','Name','Id','Employee__c','Effective_Start_Date__c','Effective_End_Date__c','name','Client_Territory_Code__c','Client_Position_Code__c','Change_Status_del__c','X_Max__c','X_Min__c','Y_Max__c','Y_Min__c','inactive__c','CreatedById','Createddate'} ;
    static final List<String> EMPLOYEE_READ_FIELD = new List<String>{'Name', 'Employee_ID__c'} ;
    static final List<String> POSITION_INSERT_FIELD = new List<String>{'Position_Type__c','Parent_Position__c','Client_Position_Code__c','name','Team_iD__c','name','Effective_Start_Date__c','Effective_End_Date__c','RGB__c'};
    static final list<String> CHANGE_REQUEST_TYPE_READ = new list<String>{'id','name','CR_Type_Name__c'};
    static final list<String> CR_POSITION_INSERT_FIELD = new List<String>{'name','Position__c','Position_Name__c','Change_Request__c','Team__c','Effective_End_Date__c','Effective_Start_Date__c','Employee__c'};
    static final List<String> CR_EMPLOYEE_ASSIGNMENT_INSERT_FIELD = new List<String>{'name','Employee_ID__c','Position_ID__c','Change_Request_ID__c'};
    static final list<string> CR_EMPLOYEE_ASSIGNMENT_READ_FIELD = new list<string>{'id','Position_ID__c', 'is_Active__c','CreatedById','Createddate'};
    static final list<string> POSITION_EMPLOYE_READ_FIELD = new list<string>{'id','position__c','employee__c','assignment_type__c','Effective_Start_Date__c','Effective_End_Date__c'};
    static final list<string> POSITION_EMPLOYEE_UPDATE_FIELD = new list<string>{'Effective_End_Date__c'};
    static final list<string> CHANGE_REQUEST_READ_FIELD = new list<string>{'id','name','status__c','Execution_Date__c','RecordTypeID__c'};
    static final list<String> CR_POSITION_READ_FIELD = new List<String>{'id','Effective_End_Date__c','Effective_Start_Date__c','CreatedDate','CreatedById','Position__c','Team__c'};
    static final list<string> USER_READ_FIELD = new list<string>{'id','name'};
    
    public map<string,string> posRgbMap{get;set;} 
    
    private void RosterInit(string SelectedTeam){
    	
        selectedPosition = null;    	
    	TreeUtility.wrapperCallingClassVariables objClassData = TreeUtility.init(SelectedTeam);  
		
        if(objClassData != null){
			system.debug('************* selectedCycle ' + objClassData.selectedCycle);
			system.debug('********** CreatePositionAccess ' + objClassData.readOnlyCreatePosition);
			team = objClassData.selectedTeamName;
	    	teamVal = objClassData.selectedteamID;
	    	teamType = objClassData.teamType;
	    	selectedCycle = objClassData.selectedCycle;
	    	rootPositionId = objClassData.rootPositionId;
	    	selectedPosition = rootPositionId;
	    	createPositionAccess = objClassData.readOnlyCreatePosition;
	    	editPositionAccess = objClassData.readOnlyeditPosition;
            updateAttributesAccess = objClassData.readOnlyUpdateAttributes;
	    	deletePositionAccess = objClassData.readOnlyDeletePosition;
            hierarchyChangeAccess = objClassData.readOnlyHierarchyChange;
	    	accessManageAssignment = objClassData.readOnlyManageAssignment;
	    	
	    	if(teamType == SalesIQGlobalConstants.TEAM_TYPE_OVERLAY){
	    		baseTeamId = objClassData.controllingTeamId;
	    	}else{
	    		baseTeamId = '';
	    	}
	    	
            selectedCycleInstanceName  = objClassData.selectedCycleName; 
            territoryId = objClassData.rootPositionCode;
            posRgbMap = new map<string,string>();
            posRgbMap = TreeUtility.getAllPositionsRGBs(SelectedTeam);

            map<string,CR_Team_Instance_Config__c> levelAccessMap = new map<string,CR_Team_Instance_Config__c>();
        
            List<String> CONFIG_READ_FIELDS = new List<String>{'Configuration_Name__c', 'Create__c', 'Delete__c', 'Edit__c', 'Update_Position_Attributes__c', 'Hierarchy_Change__c', 'Configuration_Type__c', 'Team_Instance__c'};
            if(SecurityUtil.checkRead(CR_Team_Instance_Config__c.SObjectType, CONFIG_READ_FIELDS,false) && SecurityUtil.checkRead(Team_Instance__c.SObjectType, new List<String>{'Alignment_Period__c', 'Team__c'},false)) {

                for(CR_Team_Instance_Config__c config : [Select Configuration_Name__c, Create__c, Delete__c, Edit__c,Hierarchy_Change__c, Update_Position_Attributes__c from CR_Team_Instance_Config__c where Team_Instance__r.Team__c =: SelectedTeam AND Team_Instance__r.Alignment_Period__c =: SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE and Configuration_Type__c =: SalesIQGlobalConstants.HIERARCHY_CONFIG]){
    		      levelAccessMap.put(config.Configuration_Name__c,config);
                }

                list<CR_Team_Instance_Config__c> listCRteamConfig = SalesIQUtility.getNonFieldPositionTypes();
                for(CR_Team_Instance_Config__c objCRteamIns : listCRteamConfig){
                    levelAccessMap.put(objCRteamIns.Configuration_Name__c,objCRteamIns);
                }
            }

            system.debug('--levelAccessMap ' + levelAccessMap);
    		levelAccessJsonMap = JSON.serialize(levelAccessMap);
            
            system.debug('#### SelectedTeam : '+SelectedTeam);
            
            if(teamType == SalesIQGlobalConstants.TEAM_TYPE_OVERLAY){
            	if(SecurityUtil.checkRead(Team_Instance__c.SObjectType, new List<String>{'Alignment_Period__c', 'Team__c'}, false)) {
                    list<team_instance__c> listBaseCurrentTeamInstance = [select id from team_instance__c where Alignment_Period__c =: SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE AND team__c =:baseTeamId];
            	   baseTeamInstanceId = listBaseCurrentTeamInstance[0].id;
                }
            }else{
            	baseTeamInstanceId = '';
            }

            system.debug('#### teamType : '+teamType);
            system.debug('#### baseTeamId : '+baseTeamId);
            refreshListViewBasedOnTeamInstance();
    	        
            /*
            list<Team__c> currTeam = [Select Type__c from Team__c where id =: SelectedTeam and Controlling_Team__r.team_instance_ID__c.Alignment_Period__c = 'Current' limit 1];
            if(currTeam.size() > 0){
                teamType = currTeam[0].Type__c;
                baseTeamId = currTeam[0].Controlling_Team__c;
                baseTeamInstanceId = currTeam[0].Controlling_Team__r.team_instance_ID__c;
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.No_UAP_Assigned));
            }*/
            

            //Praveen - The code does nothing, hence commented
            /*    
            if( ApexPages.currentPage().getParameters().ContainsKey('showCRMsg') == true){
                if(ApexPages.currentPage().getParameters().get('showCRMsg') == 'true')
                    showCRMsg = true;
                //notification toast message
                showMessage = true;

                if(ApexPages.currentPage().getParameters().ContainsKey('crTrackingOn')){
                    setMessageParams(System.Label.Message_Theme_Success, System.Label.Changes_Successful);
                    message = System.Label.Changes_Successful;
                }
                else{
                    crname = ApexPages.currentPage().getParameters().get('crname');
                    setMessageParams(System.Label.Message_Theme_Success,string.format(System.Label.Change_Request_Submitted,new list<string>{crname}));
                    message = string.format(System.Label.Change_Request_Submitted,new list<string>{crname});
                }
            }
            
            if(ApexPages.currentPage().getParameters().containsKey('selectedTeam') && ApexPages.currentPage().getParameters().containsKey('selectedCycle')) {
                // when both the parameters exist in the url, set them as selectedTeam and selectedCycle
                newPosTeam = ApexPages.currentPage().getParameters().get('selectedTeam') ;
                selectedCycle = ApexPages.currentPage().getParameters().get('selectedCycle') ;
            }
            */
        }else{
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.No_UAP_Assigned ));
            //notification toast message
            setMessageParams( System.Label.Message_Theme_Error, System.Label.No_UAP_Assigned );
            isPageLoadOK = false ; 
        }
    }
	
	public void onchangeTeamVal(){
		try{
	     	createPosPopup = false;
	        editPosPopup = false;
	        deletePosPopup = false;
	     	
	        RosterInit(newPosTeam);
	        refreshListViewBasedOnTeamInstance();
            checkActionInit();
		}
		catch(exception ex){
            		SalesIQLogger.logMessage(ex);
			SalesIQLogger.logCatchedException(ex, false, SalesIQLogger.POSITION_MODULE);
            		isPageLoadOK = false;
		}
    }
     
    public void  closeCRMsg(){
        showCRMsg = false;
    }
     	 
     
    /**********************************************************************************************
    @author     : Aditi Singh
    @date       : 
    @description:The below two methods are for tree display on VF page.
    Revison(s)  :
    **********************************************************************************************/
   
   	public static string getJsonString(){
   		try{
    		if(newPosTeam != null){
	    		TreeUtility.init(newPosTeam);
	        	String jsonStringValue = TreeUtility.getJsonString();  	
	    		return jsonStringValue;	
	    	}
    	}
   		catch(exception ex){
   			SalesIQLogger.logMessage(ex);
   		}
   		return '';
    }

    public void displayErrorOnClick(){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,System.label.Position_Not_Found));
        //notification toast message
        setMessageParams( System.Label.Message_Theme_Error, System.Label.Position_Not_Found );
    }
    
    // done in Tree utility
    public static map<string,string> getAllPositionsRGBs(){
    	map<string,string> posRgbMap = new map<string,string>();
    	posRgbMap = TreeUtility.getAllPositionsRGBs(newPosTeam);
    	return posRgbMap;
    }
    
	// VALIDATION ADDED ON CREATE , EDIT , DELETE POSITION FLOW FOR SECURITYUTIL EXCEPTION HANDLING - SPD 1553 (LAGNIKA)
    public void openNewPositionPopup(){
    	
    	list<team_instance_object_Attribute__c> teamattrList = SalesIQUtility.getTeamInstanceAttr(new list<String>{SalesIQGlobalConstants.ROSTER_CREATE_POSITION_POPUP}, selectedcycle, true);
    	list<String> fieldNameList = new list<String>();// LIST OF FIELDS ACCESSED ON POSITION__C FOR CREATE POSITION FLOW , QUERY ON TEAM_INSTANCE_OBJECT_ATTRIBUTE
    	
    	for(team_instance_object_attribute__c teamAttr : teamAttrList){
    		fieldNameList.add(teamAttr.Attribute_API_Name__c);
    	}
    	
    	try{
			// CHECK FOR INSERT FIELD PERMISSION - FIRSTLY IT WILL CHECK READ ON OBJECT AND READ ON FIELD PERMISSION....
			SecurityUtil.checkInsert(Position__c.SObjectType, fieldNameList, true);
			createPosPopup = true;
			operationType = 'Create';
			
		}catch(exception ex){
            SalesIQLogger.logMessage(ex);
            SalesIQLogger.logCatchedException(ex, false, SalesIQLogger.POSITION_MODULE);
	        createPosPopup = false;
		}
    }
    
    public void closeNewPositionPopup(){
    	createPosPopup = false;
    	operationType = 'Create';
    }
    
    
    public void deleteTerritory(){
    	
    	list<team_instance_object_Attribute__c> teamattrList = SalesIQUtility.getTeamInstanceAttr(new list<String>{SalesIQGlobalConstants.ROSTER_DELETE_POSITION_POPUP}, selectedcycle, true);
    	list<String> fieldNameList = new list<String>();// LIST OF FIELDS ACCESSED ON POSITION__C FOR CREATE POSITION FLOW , QUERY ON TEAM_INSTANCE_OBJECT_ATTRIBUTE
    	
    	for(team_instance_object_attribute__c teamAttr : teamAttrList){
    		fieldNameList.add(teamAttr.Attribute_API_Name__c);
    	}
    	
    	try{
    		// CHECK FOR UPDATE FIELD PERMISSION - FIRSTLY IT WILL CHECK READ ON OBJECT AND READ ON FIELD PERMISSION....
    		SecurityUtil.checkUpdate(Position__c.SObjectType, fieldNameList, true);
    		deletePosPopup = true;
	    	operationType = 'Delete';
	        openDeletePosPopup = true;
	        
    	}catch(exception ex){
            SalesIQLogger.logMessage(ex);
            SalesIQLogger.logCatchedException(ex, false, SalesIQLogger.POSITION_MODULE);
        	deletePosPopup = false;
        	openDeletePosPopup = false;
		}
    }
    
    public void closeDeletePopup(){
        deletePosPopup = false;
        operationType = 'Delete';
    }
    
    public boolean hierarchyChangePosPopup {get;set;}
    public void openHierarchyChange(){

        list<team_instance_object_Attribute__c> teamattrList = SalesIQUtility.getTeamInstanceAttr(new list<String>{SalesIQGlobalConstants.ROSTER_HIERARCHY_CHANGE_POPUP}, selectedcycle, true);
        list<String> fieldNameList = new list<String>();// LIST OF FIELDS ACCESSED ON POSITION__C FOR CREATE POSITION FLOW , QUERY ON TEAM_INSTANCE_OBJECT_ATTRIBUTE
        
        for(team_instance_object_attribute__c teamAttr : teamAttrList){
            fieldNameList.add(teamAttr.Attribute_API_Name__c);
        }
        
        try{
            // CHECK FOR UPDATE FIELD PERMISSION - FIRSTLY IT WILL CHECK READ ON OBJECT AND READ ON FIELD PERMISSION....
            SecurityUtil.checkUpdate(Position__c.SObjectType, fieldNameList, true);
            hierarchyChangePosPopup = true;
             operationType = 'Hierarchy Change';
            
        }catch(exception ex){
            SalesIQLogger.logMessage(ex);
            SalesIQLogger.logCatchedException(ex, false, SalesIQLogger.POSITION_MODULE);
            hierarchyChangePosPopup = false;
        }
    }

    public void closeHierarchyChange(){
        hierarchyChangePosPopup = false;
        operationType = 'Hierarchy Change';
    }
    
  	/*
	* Method is to get the details of selected position to be edited.	
    * @return void
    */
  	public void editPosition(){
  		
  		list<team_instance_object_Attribute__c> teamattrList = SalesIQUtility.getTeamInstanceAttr(new list<String>{SalesIQGlobalConstants.ROSTER_EDIT_POSITION_POPUP}, selectedcycle, true);
    	list<String> fieldNameList = new list<String>();// LIST OF FIELDS ACCESSED ON POSITION__C FOR CREATE POSITION FLOW , QUERY ON TEAM_INSTANCE_OBJECT_ATTRIBUTE
    	
    	for(team_instance_object_attribute__c teamAttr : teamAttrList){
    		fieldNameList.add(teamAttr.Attribute_API_Name__c);
    	}
    	
    	try{
    		// CHECK FOR UPDATE FIELD PERMISSION - FIRSTLY IT WILL CHECK READ ON OBJECT AND READ ON FIELD PERMISSION....
    		SecurityUtil.checkUpdate(Position__c.SObjectType, fieldNameList, true);
    		editPosPopup = true;  	
  			operationType = 'Edit';
	        
    	}catch(exception ex){
            SalesIQLogger.logMessage(ex);
            SalesIQLogger.logCatchedException(ex, false, SalesIQLogger.POSITION_MODULE);
        	editPosPopup = false;
		}
  	}
  
  	public void closeEditPopup(){
  		editPosPopup = false;  	
  		operationType = 'Edit';
  	}
  	
  	/*
	* Method is to update the edited Position.	
    * @return void
    */
  	public void confirmEdit(){	   	   	
		list<string> POSITION_UPDATE_FIELD = new list<string>{'name','position_type__c','Client_Position_Code__c','Effective_Start_Date__c','Effective_End_Date__c','parent_position__c'};
		if(SecurityUtil.checkUpdate(Position__c.SObjectType, POSITION_UPDATE_FIELD, false)){
			update editedPosition;
			editPosPopup = false;
		}   
    }


    public void openUpdateAttributePopUp(){
        UpdateAttributeBoolean = true;
        operationType = 'Update Position Attributes';
    }

    public void closeUpdateAttributePopUp(){
        UpdateAttributeBoolean = false;
        operationType = 'Update Position Attributes';
    }
    
    @RemoteAction
    global static List<Business_Unit__c> getHighlightCriteria(){
		try{
	        if(SecurityUtil.checkRead(Business_Unit__c.SObjectType, new List<String>{'Code__c','Criteriaforstatus__c','Name','Operator__c','Interface_Name__c','Description__c','Color__c'}, false)){
	            List<Business_Unit__c> business= [select id,Code__c,Criteriaforstatus__c,Name,Operator__c,Interface_Name__c,Description__c,Color__c FROM Business_Unit__c where Interface_Name__c='Position'];
	            return business;
	        }
		}catch(exception ex){
			SalesIQLogger.logCatchedException(ex, true, SalesIQLogger.POSITION_MODULE);
		}
        return null ;
    }
    
    public void listmethod(){}

    // method added by Lagnika for displaying position hierarchy in breadcrum
    public list<string> selectedTerritoryHierarchy {get;set;}
    public string selectedPositionNode {get;set;}
    public string posLevel {get;set;}
    
    public void refreshListViewBasedOnTeamInstance(){
        try{
	        //refresh data for breadcrumb
	        selectedTerritoryHierarchy = new list<string>();

	        // code added by Lagnika refreshbreadcrum Hierarchy
	        //list<Position__c> allAccessiblePositions = SalesIQUtility.getPositionbyRootPositionId(selectedPosition, teamType);
	        list<Position__c> allAccessiblePositions =  SalesIqUtility.getPositionbyRootPositionIdRoster(selectedPosition, teamType, selectedCycle, baseTeamInstanceId);
         
	        for(Position__c pos : allAccessiblePositions){
	            allPositionMap.put(pos.Id,pos);
	        }
        
	        selectedPos = allPositionMap.get(selectedPosition);   
	        posLevel = selectedPos.Hierarchy_Level__c;
       
	        //selectedTerritoryHierarchy.add(team);
	        //selectedTerritoryHierarchy.add(selectedCycleInstanceName);
        
	        Position__c level2Pos;
	        Position__c level3Pos;
	        Position__c level4Pos;
	        Position__c level5Pos;
        
	        if(allPositionMap.containsKey(selectedPos.Parent_Position__c)){
	            level2Pos = allPositionMap.get(selectedPos.Parent_Position__c);
	        }
    
	        if(level2Pos != null && allPositionMap.containsKey(level2Pos.Parent_Position__c)){
	            level3Pos = allPositionMap.get(level2Pos.Parent_Position__c);    
	        }
   
	        if(level3Pos != null && allPositionMap.containsKey(level3Pos.Parent_Position__c)){
	            level4Pos = allPositionMap.get(level3Pos.Parent_Position__c);    
	        }
   
	        if(level4Pos != null && allPositionMap.containsKey(level4Pos.Parent_Position__c)){
	            level5Pos = allPositionMap.get(level4Pos.Parent_Position__c);    
	        }
        
	        if(level5Pos != null)
	            selectedTerritoryHierarchy.add(level5Pos.Name);
	        if(level4Pos != null)
	            selectedTerritoryHierarchy.add(level4Pos.Name);
	        if(level3Pos != null)
	            selectedTerritoryHierarchy.add(level3Pos.Name);
	        if(level2Pos != null)
	            selectedTerritoryHierarchy.add(level2Pos.Name);
	        selectedTerritoryHierarchy.add(selectedPos.Client_Territory_Name__c);
        }catch(exception ex){
        	SalesIQLogger.logMessage(ex);
        }
    }

    public class wrapperClass{
        public Position_Employee__c posEmp{get;set;}
        public Position__c pos{get;set;}
        public String status{get;set;}
        public string positionStatus{get;set;} // whether the position is in position state or not 
        public boolean isselected{get;set;}
        public String posId{get;set;}
        public String startDate{get;set;}
        public String endDate{get;set;}
        public String empName{get;set;}
        public String createdBy{get;set;}
        public String createdDate{get;set;}
        public string hierarchylevel{get;set;}
        
        public wrapperClass(Position__c pos,string positionStatus, Position_Employee__c posEmp,String status,boolean isselected,String posId,Date startDate,Date endDate,String empName,String createdBy,Date createdDate, string hierarchyLevel){
        
            this.pos = pos;
            this.positionStatus = positionStatus;
            this.posEmp = posEmp;
            this.isselected = isselected;
            this.status = status;
            this.posId = posId;

            if(startDate != null)
                this.startDate = startDate.format();

            if(endDate != null)
                this.endDate = endDate.format();

            this.empName = empName;
            this.createdBy = createdBy;

            if(createdDate != null)
                this.createdDate = createdDate.format();
            this.hierarchylevel = hierarchyLevel;
        }
    }

    public void hideToast(){
        showMessage = false;
    }

    public void setMessageParams(String svrty, String messageText){
        showMessage = true;
        severity = String.valueOf(svrty);
        message = messageText;
    }
}