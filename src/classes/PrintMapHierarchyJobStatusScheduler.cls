global class PrintMapHierarchyJobStatusScheduler implements Schedulable{
        public string id{get;set;}
        public PrintMapHierarchyJobStatusScheduler(String id1)
        {
            id=id1;
        } 
    global void execute(SchedulableContext SC) {
     
       
        Database.executeBatch( new PrintMapHierarchyStatusBatch(id));
    } 
}