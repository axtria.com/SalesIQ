@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    global Integer respType;
    global String jsonMockResp = '';

    // Implement this interface method    
    global HTTPResponse respond(HTTPRequest req) {
        //Get JSON from Static Resource
        if(respType == 1){
            StaticResource resource = [SELECT Body FROM StaticResource WHERE Name='Preview_MOCK_JSON']; 
            Blob myBlob = resource.body;
            jsonMockResp = myBlob.toString();
        }
        else if(respType == 2){
            StaticResource resource = [SELECT Body FROM StaticResource WHERE Name='Run_Log_MOCK_JSON']; 
            Blob myBlob = resource.body;
            jsonMockResp = myBlob.toString();
        } 
        else if (respType == 3) {
            // generate mock for affiliation depth
        }
         else if(respType == 4){
            // Response bar chart report
            jsonMockResp = '["{"CPG_Report_preview_summary_chart ":[{"status":"Yes","segment":null,"sum":null},{"status":"INAC","segment":null,"sum":null},{"status":"No","segment":null,"sum":null},{"status":"ACTI","segment":null,"sum":null}]}"]';
        }
        else if(respType == 5){
            // Response bar chart report
            jsonMockResp = '["{"CPG_Report_preview_summary_chart ":[{"status":"Yes","segment":null,"sum":null},{"status":"INAC","segment":null,"sum":null},{"status":"No","segment":null,"sum":null},{"status":"ACTI","segment":null,"sum":null}]}"]';
        }
         else if(respType == 6){
            // Response derived assignment 
            jsonMockResp = '[{"product_name_team_prod": "Citramon", "id_team_prod": "a1l2F000000ZrclQAC"}]';
         }
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(jsonMockResp);
        res.setStatusCode(200);
        return res;
    }
}