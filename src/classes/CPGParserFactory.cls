public class CPGParserFactory {
    Business_Rules__c businessRules;
    CPGParserHelper parserHelper;
    CPGExpressionWrapper exprwrapper;
    string className;
    string scenarioInstanceDetailsId;
    public CPGParserFactory(Business_Rules__c businessRules){
       this.businessRules=businessRules;
      // this.parserHelper=parserHelper;
       className=CPGUtility.getType(CPGConstants.CPGPARSERHELPER) ;
       Type parserHelperType=Type.forName(className);
        if(parserHelperType!=null){
            parserHelper=(CPGParserHelper)parserHelperType.newInstance();
        }
       exprwrapper=new CPGExpressionWrapper(); 
    }
    
    public void ParseExpression(string scenarioInstanceDetailsId,list<Data_Set_Column_Detail__c> lstDataSetColumnDetails){
        System.debug('businessRuleTypeName--'+businessRules.Business_Rule_Type_Master__r.Name);
        System.debug('scenarioInstanceDetailsId--->'+scenarioInstanceDetailsId);
        system.debug('lstDataSetColumnDetails source-->'+lstDataSetColumnDetails);
        this.scenarioInstanceDetailsId=scenarioInstanceDetailsId;
        if(businessRules.Business_Rule_Type_Master__r.Name==CPGConstants.CATEGORY ||
           businessRules.Business_Rule_Type_Master__r.Component_Type__r.Name==CPGConstants.CALLBALANCING){
            ParseCategory();
        }
        else{           
            
            if(businessRules.Business_Rule_Type_Master__r.Name==CPGConstants.COMPUTE){
                System.debug('businessRuleTypeName-->'+businessRules.Business_Rule_Type_Master__r.Name);
                ParseComputation(lstDataSetColumnDetails);        
            }
            
            else if(businessRules.Business_Rule_Type_Master__r.Name==CPGConstants.RANK){
                ParseRank(lstDataSetColumnDetails);                 
            }
            else if(businessRules.Business_Rule_Type_Master__r.Name==CPGConstants.AGGREGATE){
                ParseAggregate(lstDataSetColumnDetails);                 
            }
            else if(businessRules.Business_Rule_Type_Master__r.Name==CPGConstants.FILTER){
                ParseFilter(lstDataSetColumnDetails);
            }
        }
        InsertBusinessRulesExpression();
    }
    
    private void ParseFilter(List<Data_Set_Column_Detail__c> lstDataSetColumnDetails){
        List<Filter_Rule_Entity_Detail__c> lstFilterRuleEntityDetail=[select Id,Logic_Operator__c,Category_Type__c
                                                                      ,Entity_Type__c,Parent_Entity__c,Priority__c,Filter_Display_Expression__c,Filter_Exec_expression__c
                                                                      from Filter_Rule_Entity_Detail__c
                                                                      where Business_Rules__c= :businessRules.Id];
         parserHelper.GetExpressionForFilter(exprwrapper, lstDataSetColumnDetails, lstFilterRuleEntityDetail);
    }
    private  void ParseComputation(List<Data_Set_Column_Detail__c> lstDataSetColumnDetails){
        List<Computation_Rule_Detail__c> lstComputationRuleDetail=[select Id,Display_expression__c,Execute_Expression__c,Derived_field_name__c 
                                                                   from Computation_Rule_Detail__c 
                                                                   where Business_Rules__c= :businessRules.Id];
        string tableColumnName='';
        if(lstComputationRuleDetail!=null && lstComputationRuleDetail.size()>0){
            for(Computation_Rule_Detail__c compRuleDetail:lstComputationRuleDetail){
                for(Data_Set_Column_Detail__c datasetColumnDetails:lstDataSetColumnDetails){
                    if(datasetColumnDetails.ds_col_name__c==compRuleDetail.Derived_field_name__c ){
                        tableColumnName=datasetColumnDetails.tb_col_nm__c;
                    }
                }
                 parserHelper.GetExpressionForComputation(exprwrapper,compRuleDetail.Execute_Expression__c,tableColumnName);
            }
           
        }
        
        
    }
    
    private void ParseCategory(){        
        parserHelper.GetExpressionForCategory(exprwrapper,businessRules.id);
    }
    
    private  void ParseRank(List<Data_Set_Column_Detail__c> lstDataSetColumnDetails){
        string partition='';
        List<Rank_Rule__c> lstRankRule=[select Id,Field_Name__c,Excecute_Expression__c from  Rank_Rule__c 
                                        where Business_Rules__c= :businessRules.Id];
        string tableColumnName='';
        if(lstRankRule!=null && lstRankRule.size()>0){
            for(Data_Set_Column_Detail__c datasetColumnDetails:lstDataSetColumnDetails){
                    if(datasetColumnDetails.ds_col_name__c==lstRankRule[0].Field_Name__c ){
                        tableColumnName=datasetColumnDetails.tb_col_nm__c;
                    }
             }
        }
        
        List<Rank_Partition__c> lstrankPartition=[select Id,Partition_Field__c,Name from  Rank_Partition__c 
                                               where Rank_Rule__r.Business_Rules__c= :businessRules.Id];
      //  System.debug('lstrankPartition[0].Partition_Field__c-->'+lstrankPartition[0].Partition_Field__c);
        if(lstrankPartition!=null && lstrankPartition.size()>0){
          partition=lstrankPartition[0].Partition_Field__c;  
        }
        List<Rank_Order__c> lstRankOrder=[select Order_Clause__c,Order_Field__c from Rank_Order__c 
                                          where Rank_Rule_id__r.Business_Rules__c= :businessRules.Id];
        parserHelper.GetExpressionForRank(exprwrapper,partition,lstRankOrder,tableColumnName,lstDataSetColumnDetails);
      
    }
    
    private void ParseAggregate(List<Data_Set_Column_Detail__c> lstDataSetColumnDetails){
        
        List<Aggregate_Rule_Level__c> lstAggregateRuleLevel=[select Id,Display_Seq_No__c,group_field__c 
                                                             from  Aggregate_Rule_Level__c 
                                                    where Business_Rules__c= :businessRules.Id
                                                              and Is_selected__c=true order by Display_Seq_No__c];
       /* Map<Id,string> mapAggregateRuleColumn=new Map<Id,string>();
        if(lstAggregateRuleLevel!=null && lstAggregateRuleLevel.size()>0){
            for(Aggregate_Rule_Level__c aggrLevel:lstAggregateRuleLevel) {
               
                for(Data_Set_Column_Detail__c datasetColumnDetails:lstDataSetColumnDetails){
                    if(datasetColumnDetails.ds_col_name__c.toLowerCase()==aggrLevel.group_field__c.toLowerCase()){
                        mapAggregateRuleColumn.put(aggrLevel.Id,datasetColumnDetails.tb_col_nm__c);
                    }
               }
            }
            
        }*/
        
        Data_Set_Rule_Map__c ruleMap=[select Id,dataset_id__c,scenario_rule_instance_id__c from Data_Set_Rule_Map__c 
                                           where scenario_rule_instance_id__c = :scenarioInstanceDetailsId
                                           and ds_type__c='I' limit 1];
        List<Data_Set_Column_Detail__c> lstDataSetColumnDetailsInput=[select Id,ds_col_name__c,tb_col_nm__c,dataset_id__c,datatype__c 
                                                  from Data_Set_Column_Detail__c 
                                                  where dataset_id__c= :ruleMap.dataset_id__c]; 
        
        List<Aggregate_Rule_Detail__c> lstAggrRuleDetails=[select Id,Aggregate_Field__c,Aggregate_Function__c,Field_Name__c
                                                           from  Aggregate_Rule_Detail__c 
                                                           where Business_Rules__c= :businessRules.Id
                                                           order by Seq_No__c];
       parserHelper.GetExpressionForAggregate(exprwrapper, lstDataSetColumnDetailsInput,lstAggregateRuleLevel,lstAggrRuleDetails);        
    }
    
    private void InsertBusinessRulesExpression(){
        System.debug('InsertBusinessRulesExpression-->');
       // delete[select Id from Business_Rules_Expression__c where Id= :businessRules.Id];
        List<Business_Rules_Expression__c> lstExpression=new  List<Business_Rules_Expression__c>();
                
        if(exprwrapper.expressionCategory!=null && exprwrapper.expressionCategory.size()>0){
            for(string key : exprwrapper.expressionCategory.keyset()){
                  Business_Rules_Expression__c expression=new Business_Rules_Expression__c();
                  expression.Business_Rules__c=businessRules.Id;
                  expression.Select_Expr__c=exprwrapper.expressionCategory.get(key);
                  expression.column_alias__c=key;
                  lstExpression.add(expression); 
            }
        }
        else{
            Business_Rules_Expression__c expression=new Business_Rules_Expression__c();
            expression.Business_Rules__c=businessRules.Id;
            expression.Select_Expr__c=exprwrapper.selectExpression;
        	expression.Where_Expr__c=exprwrapper.whereExpression;
        	expression.Group_By_Expr__c=exprwrapper.groupByExpression;
            if(exprwrapper.columnalias==null){
                expression.column_alias__c=exprwrapper.columnalias;
            }else{
                expression.column_alias__c=exprwrapper.columnalias;
            }

            lstExpression.add(expression);
        }

        insert lstExpression;
    }
    
   
    
    
    

}