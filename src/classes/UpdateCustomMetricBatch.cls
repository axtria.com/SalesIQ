/************************************************************************************************
    Name        :   UpdateCustomMetricBatch.cls
    Author      :   Arun Kumar
    Date        :   1 Jan 2018
    Description :   This method will calculate the CIM position metric summary based on Team Instance and CR type
                    
************************************************************************************************/
global class UpdateCustomMetricBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {

    global final String Query;
    global final String teamInstanceId;
    global final String assignmentStatus;
    global final String crMovementType;

    global UpdateCustomMetricBatch(string teamInstance, string movementType){
        teamInstanceId = teamInstance;
        assignmentStatus = getAssignmentStatusbyTeamInstance(teamInstance);
        crMovementType = movementType;
    
        Query = 'select id,name from position__c where team_instance__c =:teamInstanceId and inactive__c=false and Hierarchy_Level__c=\'1\' and Client_Position_Code__c!=\'00000\' and Is_Unassigned_Position__c=false ';
     
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){       
    
        return Database.getQueryLocator(Query);    
    }
    
    global void execute(Database.BatchableContext bc, List<Position__c> scope){
        
        set<string> positionIds = new set<string>();
        set<id> setpositionIds = new set<id>();//create new set of type id coz input parameter for two diff methods are different

        for(Position__c pos:scope){
            positionIds.add(pos.id);
            setpositionIds.add(pos.id);
        }
        
        list<position_geography__c> lstPosGeo= getPositionGeography(positionIds,teamInstanceId,assignmentStatus );        

        map<Id,Position_Team_Instance__c> mapPositionTeamInstance = SalesIQUtility.getPositionTeamInstanceMap(setpositionIds);
                
        map<id, list<string>> mapPositionGeography = new map<id, list<string>>();
        for(position_geography__c posgeo:lstPosGeo){
            if(mapPositionGeography.containskey(posgeo.position__c)){
                list<string> temp = mapPositionGeography.get(posgeo.position__c);
                temp.add(posgeo.geography__r.name);
                mapPositionGeography.put(posgeo.position__c, temp);       
                
            }else{
                mapPositionGeography.put(posgeo.position__c, new list<string>{posgeo.geography__r.name});      
            }
            
        }

        set<id> counterpartTeamInstance = getcounterTeamInstance(teamInstanceId);       

        
        list<CIM_config__c> lstCustomMetric = getCustomMetric(new set<id>{teamInstanceId}, crMovementType);
        string customMetric = '';
        for(CIM_config__c cim:lstCustomMetric){
            customMetric  =cim.id;
        }
        
        list<CIM_Position_Metric_Summary__c> positionSummary = new list<CIM_Position_Metric_Summary__c>();
        for(string selectedPos:positionIds ){
             system.debug('selectedPos '+selectedPos);
            
            CIM_Position_Metric_Summary__c objSummary = new CIM_Position_Metric_Summary__c();
            list<string> geoName = mapPositionGeography.get(selectedPos);
            string positionTeamInstance = '';
            if(mapPositionTeamInstance.get(selectedPos)!=null){
                positionTeamInstance = mapPositionTeamInstance.get(selectedPos).id;
            }
            
            
            //checked positionTeamInstance becasue if any position doesn't have Position Team Instance record, in that case CIM will not be calculated
            //for that position            
            if(geoName!=null && geoName.size()>0 && positionTeamInstance!=''){
                integer netPositionCount=0; 
                AggregateResult aggregateCountDestination = [select COUNT_distinct(Position__c) territorycount from Position_Geography__c where Assignment_status__c=:assignmentStatus and Team_Instance__c in: counterpartTeamInstance and Geography__r.name in :geoName ];
                system.debug('aggregateCountDestination '+aggregateCountDestination);
                
                string countTerritory = string.valueOf(aggregateCountDestination.get('territorycount'));
                if(!string.isBlank(countTerritory)){
                    if(integer.valueOf(countTerritory)>0){   
                        //adding 1 because in the counter part territory we have to add "1" for base Team instance(where movement is occuring)                 
                        //netPositionCount = integer.valueOf(countTerritory)+1;
                        netPositionCount = integer.valueOf(countTerritory);
                    }
                }
                //if countTerritory variable has no value that sumarry record will be created with 0 value
                
                objSummary.Original__c = string.valueOf(netPositionCount);
                objSummary.Proposed__c = string.valueOf(netPositionCount);
                objSummary.Approved__c = string.valueOf(netPositionCount);
                objSummary.Team_Instance__c = mapPositionTeamInstance.get(selectedPos).Team_Instance_id__c;
                objSummary.Position_Team_Instance__c = positionTeamInstance;
                objSummary.CIM_Config__c = customMetric;
                
                positionSummary.add(objSummary);

               

            }            
        }
        if(positionSummary.size()>0){
            system.debug('positionSummary '+positionSummary); 
            system.debug('positionSummary '+positionSummary.size()); 
            insert positionSummary;   
        }
    }
    
    private set<id> getcounterTeamInstance(string controllingTeamInstance){
        list<Team_Instance_Mapping__c> lstMapping = [select controlling_Team_instance__c, Team_Instance1__c, team_instance2__c from Team_Instance_Mapping__c where controlling_Team_instance__c=:controllingTeamInstance];
        set<id> counterpartTeamInstance = new set<id>();
        if(lstMapping!=null && lstMapping.size()>0){            
            for(Team_Instance_Mapping__c mapping:lstMapping){
                if(!string.isBlank(mapping.Team_Instance1__c)){
                    counterpartTeamInstance.add(mapping.Team_Instance1__c);
                }
                if(!string.isBlank(mapping.Team_Instance2__c)){
                    counterpartTeamInstance.add(mapping.Team_Instance2__c);
                }
            }
        }
        system.debug('counterpartTeamInstance '+counterpartTeamInstance);
        
        return counterpartTeamInstance ;
    
    }

    public static list<CIM_config__c> getCustomMetric(set<id> teamInstance, string movementType){
        system.debug('teamInstance '+teamInstance);
        system.debug('movementType '+movementType);
        list<CIM_config__c> lstCIMConfig = [select id,name from CIM_config__c where Is_Custom_Metric__c=true and Team_Instance__c in:teamInstance and Enable__c=true and Change_Request_Type__r.CR_Type_Name__c like :movementType];
        return lstCIMConfig;

    }
    
    public static list<position_geography__c> getPositionGeography(set<String> positionIds, String teamInstanceId, string assignmentStatus){
     return [select id, Geography__c, Geography__r.name, Position__c from position_geography__c where position__c in:positionIds and team_instance__c=:teamInstanceId and Assignment_status__c=:assignmentStatus];
       
    }

    /*@author : Arun Kumar
    @date : 25 Dec 2017  
    @description : This method will return the assignment status bases on Team Instance Alignment Period
    @param1: TeamInstance SF Id
    @return : String
    */    
    public static String getAssignmentStatusbyTeamInstance(id teamInstance){
        String alignmentPeriod='';
        String assignmentStatus='';

        Team_Instance__c objTeamInstance= getTeamInstanceById(teamInstance);
        if(!String.isBlank(objTeamInstance.id)){
             alignmentPeriod = objTeamInstance.Alignment_Period__c;
        }
        else{
             alignmentPeriod = 'Current';
        }

        if(alignmentPeriod == 'Current'){
            assignmentStatus = 'Active';
        }else{
            assignmentStatus = 'Future Active';
        }

        return assignmentStatus;
    }

     public static Team_Instance__c getTeamInstanceById(String teamInstanceId){
        Team_Instance__c teamInstance = new Team_Instance__c();
        
        list<Team_Instance__c> lstTeamInstance = [select id, Alignment_Type__c, Alignment_Period__c from team_instance__c where id =: teamInstanceId];
        for(Team_Instance__c TI: lstTeamInstance){
            teamInstance = TI;
        }
        
        return teamInstance;
    }
    
     
    global void finish(Database.BatchableContext bc){
        system.debug('#### Finish method');
    }


}