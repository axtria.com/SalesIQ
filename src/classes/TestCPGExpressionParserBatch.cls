/*
Author : Ankit Manendra
Description : Test class for CPGExpressionParserBatch
Date : 8 Novomber, 2017

*/

@isTest
private class TestCPGExpressionParserBatch{
    @isTest
    static void cpgExpParBatchTest() {
    
    Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj;
        
        Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        insert scenarioObj;
        
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj;
        
         Scenario_Rule_Instance_Details__c sceRuleInsDetObj1 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj1.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj1.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj1;
        
        Data_Set__c dataSetObj = new Data_Set__c();
        dataSetObj.Name = 'Call Plan Summary';
        insert dataSetObj;
        
        Data_Set_Rule_Map__c dataSetRuleObj = new Data_Set_Rule_Map__c();
        dataSetRuleObj.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj.ds_type__c='O';
        dataSetRuleObj.dataset_id__c=dataSetObj.id;
        insert dataSetRuleObj;
        
        Data_Set_Rule_Map__c dataSetRuleObj1 = new Data_Set_Rule_Map__c();
        dataSetRuleObj1.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj1.ds_type__c='I';
        dataSetRuleObj1.dataset_id__c=dataSetObj.id;
        insert dataSetRuleObj1;
        
        system.debug('==dataSetRuleObj===='+dataSetRuleObj);
        
        ComponentTypeMaster__c ComTypeMasObj = new ComponentTypeMaster__c();
        ComTypeMasObj.Name='Compute';
        ComTypeMasObj.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj.DisplayIcon__c='add';
        ComTypeMasObj.DisplayIconClass__c='window information';
        insert ComTypeMasObj;
        
        ComponentTypeMaster__c ComTypeMasObj1 = new ComponentTypeMaster__c();
        ComTypeMasObj1.Name='Call Balancing';
        ComTypeMasObj1.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj1.DisplayIcon__c='add';
        ComTypeMasObj1.DisplayIconClass__c='window information';
        insert ComTypeMasObj1;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj.Name='Rank';
        busRuleTypeMasObj.Component_Type__c=ComTypeMasObj.id;
        insert busRuleTypeMasObj;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj4 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj4.Name='Compute';
        busRuleTypeMasObj4.Component_Type__c=ComTypeMasObj.id;
        insert busRuleTypeMasObj4;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj5 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj5.Name='Category';
        busRuleTypeMasObj5.Component_Type__c=ComTypeMasObj.id;
        insert busRuleTypeMasObj5;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj2 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj2.Name='Aggregate';
        busRuleTypeMasObj2.Component_Type__c=ComTypeMasObj.id;
        insert busRuleTypeMasObj2;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj3 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj3.Name='Filter';
        busRuleTypeMasObj3.Component_Type__c=ComTypeMasObj.id;
        insert busRuleTypeMasObj3;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj6 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj6.Name='Decrease Reach';
        busRuleTypeMasObj6.Component_Type__c=ComTypeMasObj.id;
        insert busRuleTypeMasObj6;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj7= new Business_Rule_Type_Master__c();
        busRuleTypeMasObj7.Name='Decrease';
        busRuleTypeMasObj7.Component_Type__c=ComTypeMasObj.id;
        insert busRuleTypeMasObj7;

        ComponentTypeMaster__c ComTypeMasObj2 = new ComponentTypeMaster__c();
        ComTypeMasObj2.Name='Aggregation';
        ComTypeMasObj2.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj2.DisplayIcon__c='add';
        ComTypeMasObj2.DisplayIconClass__c='window information';
        insert ComTypeMasObj2;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj1 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj1.Name='Rank';
        busRuleTypeMasObj1.Component_Type__c=ComTypeMasObj1.id;
        insert busRuleTypeMasObj1;
        
        Business_Rules__c busRuleObj = new Business_Rules__c();
        busRuleObj.Name='Rank rule 278';
        busRuleObj.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObj.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj.Aggregate_Level__c='Test';
        busRuleObj.customer_prioritization__c='Test';
        insert busRuleObj;
        
        Business_Rules__c busRuleObj1 = new Business_Rules__c();
        busRuleObj1.Name='Rank rule 278';
        busRuleObj1.Business_Rule_Type_Master__c=busRuleTypeMasObj4.id;
        busRuleObj1.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj1.Aggregate_Level__c='Test';
        busRuleObj1.customer_prioritization__c='Test';
        insert busRuleObj1;
        
        Business_Rules__c busRuleObj2 = new Business_Rules__c();
        busRuleObj2.Name='Rank rule 278';
        busRuleObj2.Business_Rule_Type_Master__c=busRuleTypeMasObj2.id;
        busRuleObj2.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj2.Aggregate_Level__c='Test';
        busRuleObj2.customer_prioritization__c='Test';
        insert busRuleObj2;
        
         Business_Rules__c busRuleObj3 = new Business_Rules__c();
        busRuleObj3.Name='Rank rule 278';
        busRuleObj3.Business_Rule_Type_Master__c=busRuleTypeMasObj3.id;
        busRuleObj3.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj3.Aggregate_Level__c='Test';
        busRuleObj3.customer_prioritization__c='Test';
        insert busRuleObj3;
        
        Business_Rules__c busRuleObj5 = new Business_Rules__c();
        busRuleObj5.Name='Rank rule 278';
        busRuleObj5.Business_Rule_Type_Master__c=busRuleTypeMasObj5.id;
        busRuleObj5.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj5.Aggregate_Level__c='Test';
        busRuleObj5.customer_prioritization__c='Test';
        insert busRuleObj5;
        
        Business_Rules__c busRuleObj6 = new Business_Rules__c();
        busRuleObj6.Name='Rank rule 278';
        busRuleObj6.Business_Rule_Type_Master__c=busRuleTypeMasObj6.id;
        busRuleObj6.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj6.Aggregate_Level__c='Test';
        busRuleObj6.customer_prioritization__c='Test';
        insert busRuleObj6;
        

        Business_Rules__c busRuleObj7 = new Business_Rules__c();
        busRuleObj7.Name='Rank rule 278';
        busRuleObj7.Business_Rule_Type_Master__c=busRuleTypeMasObj7.id;
        busRuleObj7.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj7.Aggregate_Level__c='Test';
        busRuleObj7.customer_prioritization__c='Test';
        insert busRuleObj7;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj.Param_Name__c='derived field 278';
        busRuleFieldObj.Business_Rule__c=busRuleObj.id;
        busRuleFieldObj.Param_Data_Type__c='Numeric';
        busRuleFieldObj.Param_Type__c='Derived';
        busRuleFieldObj.IO_Flag__c='O';
        
        insert busRuleFieldObj;
        Business_Rule_FieldMap_Details__c busRuleFieldObj1 = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj1.Param_Name__c='derived field 278';
        busRuleFieldObj1.Business_Rule__c=busRuleObj7.id;
        busRuleFieldObj1.Param_Data_Type__c='Numeric';
        busRuleFieldObj1.Param_Type__c='Derived';
        busRuleFieldObj1.IO_Flag__c='O';
        
        insert busRuleFieldObj1;
        
        Computation_Rule_Detail__c compRuleDetObj = new Computation_Rule_Detail__c();
        compRuleDetObj.Business_Rules__c=busRuleObj1.id;
        compRuleDetObj.Data_Type__c='Numeric';
        compRuleDetObj.Derived_field_name__c='FIELD 123';
        compRuleDetObj.Execute_Expression__c='Or~(~1~<~2~,~3~)';
        
        insert compRuleDetObj;
        
        Computation_Rule_Detail__c compRuleDetObj1 = new Computation_Rule_Detail__c();
        compRuleDetObj1.Business_Rules__c=busRuleObj1.id;
        compRuleDetObj1.Data_Type__c='Numeric';
        compRuleDetObj1.Derived_field_name__c='FIELD 123';
        compRuleDetObj1.Execute_Expression__c='And~(~1~<~2~>~3~,~4~,~2~>~5~,~Or~(~1~,~0~)~)';
        
        insert compRuleDetObj1;
        
        Computation_Rule_Detail__c compRuleDetObj2 = new Computation_Rule_Detail__c();
        compRuleDetObj2.Business_Rules__c=busRuleObj1.id;
        compRuleDetObj2.Data_Type__c='Numeric';
        compRuleDetObj2.Derived_field_name__c='FIELD 123';
        compRuleDetObj2.Execute_Expression__c='Average~(~1~,~2~,~3~)';
        
        insert compRuleDetObj2;
        
        Computation_Rule_Detail__c compRuleDetObj3 = new Computation_Rule_Detail__c();
        compRuleDetObj3.Business_Rules__c=busRuleObj1.id;
        compRuleDetObj3.Data_Type__c='Numeric';
        compRuleDetObj3.Derived_field_name__c='FIELD 123';
        compRuleDetObj3.Execute_Expression__c='Between~(~1~,~2~,~3~)';
        
        insert compRuleDetObj3;
        
        Data_Set_Column_Detail__c dataSetCol = new Data_Set_Column_Detail__c();
        dataSetCol.dataset_id__c=dataSetObj.id;
        dataSetCol.variable_type__c='Derived';
        dataSetCol.ds_col_name__c='colTest';
        insert dataSetCol;
        
        list<Data_Set_Column_Detail__c> dataSetColDetList = new list<Data_Set_Column_Detail__c>();
        dataSetColDetList.add(dataSetCol);
        
        Category_Rule_Entity_Detail__c catRuleEntDet = new Category_Rule_Entity_Detail__c();
        catRuleEntDet.Entity_Type__c='Block';
        catRuleEntDet.Business_Rule__c=busRuleObj.id;
        catRuleEntDet.Priority__c='1';
        insert catRuleEntDet;
        
        Category_Derived_Variable_Metadata__c catDerMetObj = new Category_Derived_Variable_Metadata__c();
        catDerMetObj.Field_Name__c='TestCatDer';
        catDerMetObj.Business_Rule_Id__c=busRuleObj.id;
        insert catDerMetObj;
        
        Category_Derived_Variable_Metadata__c catDerMetObj5 = new Category_Derived_Variable_Metadata__c();
        catDerMetObj5.Field_Name__c='TestCatDer';
        catDerMetObj5.Business_Rule_Id__c=busRuleObj5.id;
        insert catDerMetObj5;
        
        Category_Rule_Entity_Detail__c catRuleEntDet1 = new Category_Rule_Entity_Detail__c();
        catRuleEntDet1.Entity_Type__c='Group';
        catRuleEntDet1.Business_Rule__c=busRuleObj.id;
        catRuleEntDet1.Priority__c='1';
        catRuleEntDet1.Parent_Entity__c=catRuleEntDet.id;
        insert catRuleEntDet1;
        
        Category_Rule_Entity_Detail__c catRuleEntDet5 = new Category_Rule_Entity_Detail__c();
        catRuleEntDet5.Entity_Type__c='Group';
        catRuleEntDet5.Business_Rule__c=busRuleObj5.id;
        catRuleEntDet5.Priority__c='1';
        catRuleEntDet5.Category_Type__c='If';
        insert catRuleEntDet5;
        
        Category_Rule_Entity_Detail__c catRuleEntDet52 = new Category_Rule_Entity_Detail__c();
        catRuleEntDet52.Entity_Type__c='Block';
        catRuleEntDet52.Business_Rule__c=busRuleObj5.id;
        catRuleEntDet52.Priority__c='1';
        catRuleEntDet52.Category_Type__c='If';
        insert catRuleEntDet52;
        
         Category_Rule_Entity_Detail__c catRuleEntDet5parent = new Category_Rule_Entity_Detail__c();
        catRuleEntDet5parent.Entity_Type__c='Group';
        catRuleEntDet5parent.Business_Rule__c=busRuleObj5.id;
        catRuleEntDet5parent.Priority__c='1';
        catRuleEntDet5parent.Category_Type__c='If';
        catRuleEntDet5parent.Parent_Entity__c=catRuleEntDet52.id;
        insert catRuleEntDet5parent;
        
        Category_Rule_Output_Detail__c catRuleOutDet = new Category_Rule_Output_Detail__c();
        catRuleOutDet.Category_Rule_Entity_Detail__c=catRuleEntDet.id;
        catRuleOutDet.Derived__c='TestCatDer';
        insert catRuleOutDet;
        
        Category_Rule_Output_Detail__c catRuleOutDet5 = new Category_Rule_Output_Detail__c();
        catRuleOutDet5.Category_Rule_Entity_Detail__c=catRuleEntDet5.id;
        catRuleOutDet5.Derived__c='TestCatDer';
        insert catRuleOutDet5;
        
        Category_Rule_Criteria_Detail__c catRuleCriDet = new Category_Rule_Criteria_Detail__c();
        catRuleCriDet.FieldDataType__c='Numeric';
        catRuleCriDet.Sequence__c='1';
        insert catRuleCriDet;
        
        Category_Rule_Criteria_Detail__c catRuleCriDet5 = new Category_Rule_Criteria_Detail__c();
        catRuleCriDet5.FieldDataType__c='Numeric';
        catRuleCriDet5.Sequence__c='1';
        catRuleCriDet5.Category_Rule_Entity_Detail__c=catRuleEntDet5.id;
        insert catRuleCriDet5;
        
        Category_Rule_Criteria_Detail__c catRuleCriDet52 = new Category_Rule_Criteria_Detail__c();
        catRuleCriDet52.FieldDataType__c='Numeric';
        catRuleCriDet52.Sequence__c='1';
        catRuleCriDet52.Category_Rule_Entity_Detail__c=catRuleEntDet52.id;
        insert catRuleCriDet52;
        
        Category_Rule_Criteria_Detail__c catRuleCriDet52parent = new Category_Rule_Criteria_Detail__c();
        catRuleCriDet52parent.FieldDataType__c='Numeric';
        catRuleCriDet52parent.Sequence__c='1';
        catRuleCriDet52parent.Category_Rule_Entity_Detail__c=catRuleEntDet5parent.id;
        catRuleCriDet52parent.Criteria_operator__c='Test';
        catRuleCriDet52parent.Field__c='#Test';
        catRuleCriDet52parent.Param_value__c='TestParam';
        catRuleCriDet52parent.Logic_Operator__c='And';
        insert catRuleCriDet52parent;
        
        Category_Rule_Output_Detail__c catRuleOutDet52Parent = new Category_Rule_Output_Detail__c();
        catRuleOutDet52Parent.Category_Rule_Entity_Detail__c=catRuleEntDet5parent.id;
        catRuleOutDet52Parent.Derived__c='TestCatDer';
        insert catRuleOutDet52Parent;
        
        Category_Rule_Output_Detail__c catRuleOutDet52 = new Category_Rule_Output_Detail__c();
        catRuleOutDet52.Category_Rule_Entity_Detail__c=catRuleEntDet52.id;
        catRuleOutDet52.Derived__c='TestCatDer';
        insert catRuleOutDet52;
        
        Rank_Rule__c rankObj = new Rank_Rule__c();
        rankObj.Business_Rules__c=busRuleObj.id;
        insert rankObj;
        
        Rank_Rule__c rankObj1 = new Rank_Rule__c();
        rankObj1.Business_Rules__c=busRuleObj1.id;
        insert rankObj1;
        
        Rank_Partition__c rankPar = new Rank_Partition__c();
        rankPar.Rank_Rule__c=rankObj.id;
        rankPar.Partition_Field__c='colTest';
        insert rankPar;
        
        Rank_Order__c rankOrd = new Rank_Order__c();
        rankOrd.Rank_Rule_id__c=rankObj.id;
        rankOrd.Order_Field__c='colTest';
        insert rankOrd;
        
        Aggregate_Rule_Detail__c aggRuleDet = new Aggregate_Rule_Detail__c();
        aggRuleDet.Business_Rules__c=busRuleObj2.id;
        aggRuleDet.Field_Name__c='colTest';
        aggRuleDet.Aggregate_Field__c='Master tier';
        insert aggRuleDet;
        
        Filter_Rule_Entity_Detail__c filRuleDet = new Filter_Rule_Entity_Detail__c();
        filRuleDet.entity_type__c='Group';
        filRuleDet.Business_Rules__c=busRuleObj3.id;
        insert filRuleDet;
        
        Filter_Rule_Criteria_Detail__c filRuleCri = new Filter_Rule_Criteria_Detail__c();
        filRuleCri.Filter_Rule_Entity_Detail__c=filRuleDet.id;
        filRuleCri.FieldDataType__c='Text';
        filRuleCri.Criteria_operator__c='in';
        filRuleCri.Param_value__c='TestParam';
        insert filRuleCri;
        Filter_Rule_Criteria_Detail__c filRuleCri1 = new Filter_Rule_Criteria_Detail__c();
        filRuleCri1.Filter_Rule_Entity_Detail__c=filRuleDet.id;
        filRuleCri1.FieldDataType__c='Text';
        filRuleCri1.Criteria_operator__c='contains';
        filRuleCri1.Param_value__c='TestParam';
        insert filRuleCri1;
        Filter_Rule_Criteria_Detail__c filRuleCri2 = new Filter_Rule_Criteria_Detail__c();
        filRuleCri2.Filter_Rule_Entity_Detail__c=filRuleDet.id;
        filRuleCri2.FieldDataType__c='Numeric';
        filRuleCri2.Criteria_operator__c='in';
        filRuleCri2.Param_value__c='TestParam';
        insert filRuleCri2;
        Filter_Rule_Criteria_Detail__c filRuleCri3 = new Filter_Rule_Criteria_Detail__c();
        filRuleCri3.Filter_Rule_Entity_Detail__c=filRuleDet.id;
        filRuleCri3.FieldDataType__c='Text';
        filRuleCri3.Criteria_operator__c='begins with';
        filRuleCri3.Param_value__c='TestParam';
        insert filRuleCri3;
        Filter_Rule_Criteria_Detail__c filRuleCri4 = new Filter_Rule_Criteria_Detail__c();
        filRuleCri4.Filter_Rule_Entity_Detail__c=filRuleDet.id;
        filRuleCri4.FieldDataType__c='Text';
        filRuleCri4.Criteria_operator__c='ends with';
        filRuleCri4.Param_value__c='TestParam';
        insert filRuleCri4;
        
        
        
        Test.startTest();
        CPGCategoryHelperParser obj = new CPGCategoryHelperParser();
        
        CPGCategoryHelperParser.CreateCategoryParser(busRuleObj7.Id);
        
		//System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());

        MockHttpResponseGenerator mocker = new MockHttpResponseGenerator();
        mocker.respType = 1;
        Test.setMock(HttpCalloutMock.class, mocker); 

        Database.ExecuteBatch(new CPGExpressionParserBatch(scenarioObj.id));
            
        Test.stopTest();
        
        
        }
    @isTest
    static void cpgExpParserFiltertest() {
    
        Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj;
        
        Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        insert scenarioObj;
        
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj;
        
         Scenario_Rule_Instance_Details__c sceRuleInsDetObj1 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj1.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj1.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj1;
        
        Data_Set__c dataSetObj = new Data_Set__c();
        dataSetObj.Name = 'Call Plan Summary';
        insert dataSetObj;
        
        Data_Set_Rule_Map__c dataSetRuleObj = new Data_Set_Rule_Map__c();
        dataSetRuleObj.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj.ds_type__c='O';
        dataSetRuleObj.dataset_id__c=dataSetObj.id;
        insert dataSetRuleObj;
        
        Data_Set_Rule_Map__c dataSetRuleObj1 = new Data_Set_Rule_Map__c();
        dataSetRuleObj1.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj1.ds_type__c='I';
        dataSetRuleObj1.dataset_id__c=dataSetObj.id;
        insert dataSetRuleObj1;
        
        system.debug('==dataSetRuleObj===='+dataSetRuleObj);
        
        ComponentTypeMaster__c ComTypeMasObj = new ComponentTypeMaster__c();
        ComTypeMasObj.Name='Compute';
        ComTypeMasObj.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj.DisplayIcon__c='add';
        ComTypeMasObj.DisplayIconClass__c='window information';
        insert ComTypeMasObj;
        
        ComponentTypeMaster__c ComTypeMasObj1 = new ComponentTypeMaster__c();
        ComTypeMasObj1.Name='Call Balancing';
        ComTypeMasObj1.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj1.DisplayIcon__c='add';
        ComTypeMasObj1.DisplayIconClass__c='window information';
        insert ComTypeMasObj1;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj.Name='Rank';
        busRuleTypeMasObj.Component_Type__c=ComTypeMasObj.id;
        insert busRuleTypeMasObj;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj1 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj1.Name='Rank';
        busRuleTypeMasObj1.Component_Type__c=ComTypeMasObj1.id;
        insert busRuleTypeMasObj1;
        
        Business_Rules__c busRuleObj = new Business_Rules__c();
        busRuleObj.Name='Rank rule 278';
        busRuleObj.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObj.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        busRuleObj.Aggregate_Level__c='Test';
        busRuleObj.customer_prioritization__c='Test';
        insert busRuleObj;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj.Param_Name__c='derived field 278';
        busRuleFieldObj.Business_Rule__c=busRuleObj.id;
        busRuleFieldObj.Param_Data_Type__c='Numeric';
        busRuleFieldObj.Param_Type__c='Derived';
        busRuleFieldObj.IO_Flag__c='O';
        
        insert busRuleFieldObj;
  
        
        Data_Set_Column_Detail__c dataSetCol = new Data_Set_Column_Detail__c();
        dataSetCol.dataset_id__c=dataSetObj.id;
        dataSetCol.variable_type__c='Derived';
        dataSetCol.ds_col_name__c='colTest';
        insert dataSetCol;
        
        list<Data_Set_Column_Detail__c> dataSetColDetList = new list<Data_Set_Column_Detail__c>();
        dataSetColDetList.add(dataSetCol);
        
        
        
        Filter_Rule_Entity_Detail__c filRuleDet = new Filter_Rule_Entity_Detail__c();
        filRuleDet.entity_type__c='Group';
        filRuleDet.Business_Rules__c=busRuleObj.id;
        insert filRuleDet;
        
        Filter_Rule_Criteria_Detail__c filRuleCri = new Filter_Rule_Criteria_Detail__c();
        filRuleCri.Filter_Rule_Entity_Detail__c=filRuleDet.id;
        filRuleCri.FieldDataType__c='Text';
        filRuleCri.Criteria_operator__c='equals';
        filRuleCri.Param_value__c='TestParam';
        insert filRuleCri;
        Test.startTest();
        
        MockHttpResponseGenerator mocker = new MockHttpResponseGenerator();
        mocker.respType = 1;
        Test.setMock(HttpCalloutMock.class, mocker); 
        
		//System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        Database.ExecuteBatch(new CPGExpressionParserBatch(scenarioObj.id));
            
        Test.stopTest();
        
        
        }
     }