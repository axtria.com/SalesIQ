/**
* Description   : Batch class to update isShared and SharedWith on Position Account 
* Author        : Gaurav 
* Constructor 1 Parameters : Team Instance ID (String)
* Constructor 2 Parameters : Team Instance ID (String) and List<string> Account Ids
* Date        : 3-May-2018
*/

global class UpdatePositionAccountSharedBatch implements Database.Batchable<sObject> ,Database.Stateful{
        
    // This variable will remain stateful throughout batch execution to maintain Account Ids with their list of Position Accounts.
    global map<ID,list<Position_Account__c>>  mapOfAccountIdAndPositionAccount = new map<ID,list<Position_Account__c>>();
    global String query;
    string teamInstanceId;
    list<string> accIDs;
    private map<ID,Position_Account__c> toBeUpdatedPosAccMap;
    set<string> sharedPositions ;
    set<Id> accountIds;
    boolean isBulkMovement;
    set<id> changeReqId;

     
    public UpdatePositionAccountSharedBatch (string teamInstance){
        teamInstanceId = teamInstance;
        accIDs = new list<string>();
        sharedPositions = new set<string>();
        accountIds = new set<Id>();
        toBeUpdatedPosAccMap = new map<ID,Position_Account__c>();
        query = ''; 
        string namespace = SalesIQGlobalConstants.getOrgNameSpace();
        list<AggregateResult> Results = [select Account__c, count(ID) from Position_Account__c where Team_Instance__c =: teamInstanceId group by Account__c having count(ID)>1 limit 2000];
        system.debug('######### Aggregate Result ' + Results);
        for(AggregateResult sharedAccounts : Results){
            system.debug('########## account id ' + sharedAccounts.get(namespace+'Account__c'));
            if(sharedAccounts.get(namespace+'Account__c') != null){
                accIDs.add(string.valueOf(sharedAccounts.get(namespace+'Account__c')));
            }
        }
        isBulkMovement  =false;
        system.debug('########## accIDs ' + accIDs);
        query = 'select id,SharedWith__c,IsShared__c,Position__c,Account__c,Position__r.Name from Position_Account__c where Team_Instance__c =: teamInstanceId and Account__c in: accIDs and Assignment_Status__c != \''+ SalesIQGlobalConstants.POSITION_GEOGRAPHY_INACTIVE+'\'';
    }
     
    public UpdatePositionAccountSharedBatch (string teamInstance,list<String> accounts){
        teamInstanceId = teamInstance;
        accIDs = new list<string>();
        toBeUpdatedPosAccMap = new map<ID,Position_Account__c>();
        accountIds = new set<Id>();
        sharedPositions = new set<string>();
        query = '';   
        isBulkMovement  =false;      
       
        List<Team_Instance__c>  teamInstanceList= [Select Alignment_Period__c FROM Team_Instance__c WHERE id=:teamInstance];
        String assignmentStatus ='Active';
        if(teamInstanceList[0].Alignment_Period__c=='Current')
            assignmentStatus = 'Active';
        else
            assignmentStatus = 'Future Active';
        if(accounts != null && accounts.size() >0){
            accIDs.addAll(accounts);
            query = 'select id,SharedWith__c,IsShared__c,Position__c,Account__c,Position__r.Name from Position_Account__c where Team_Instance__c =: teamInstanceId and Account__c in: accIDs and Assignment_Status__c = \''+ assignmentStatus+'\'';
        }
    }

    /*
    This method gets called in case of Bulk CR, where we have accounts in thousands. Based on CR id, it will fetch accounts from CR account and den 
    from position accounts
    @Param 3, is used when shared bacth gets called via bulk movment, so that I can update the CR status to completed in finish method
    */
    public UpdatePositionAccountSharedBatch (string teamInstance,set<id> crID, boolean bulkMovement){
        isBulkMovement = bulkMovement;
        teamInstanceId = teamInstance;
        accIDs = new list<string>();  
        changeReqId = new set<id>();
        changeReqId.addAll(crID);
        //changeReqId = crID
        toBeUpdatedPosAccMap = new map<ID,Position_Account__c>();
        accountIds = new set<Id>();
        sharedPositions = new set<string>();
       
        query = '';  
        set<id> accounts = new set<id>();       
       
        List<Team_Instance__c>  teamInstanceList= [Select Alignment_Period__c FROM Team_Instance__c WHERE id=:teamInstance];
        String assignmentStatus ='Active';
        if(teamInstanceList[0].Alignment_Period__c=='Current')
            assignmentStatus = 'Active';
        else
            assignmentStatus = 'Future Active';

        list<CR_Account__C> lstCRAccount = [select id,account__c from CR_Account__C where Change_Request__c in:crID limit 50000];
        for(CR_Account__C crAccount: lstCRAccount){
            accIDs.add(crAccount.account__c);
        }

        if(accIDs != null && accIDs.size() >0){
            //accIDs.addAll(accounts);
           
            query = 'select id,SharedWith__c,IsShared__c,Position__c,Account__c,Position__r.Name from Position_Account__c where IsShared__c=true and Team_Instance__c =: teamInstanceId and Account__c in: accIDs and Assignment_Status__c = \''+ assignmentStatus+'\'';
        }
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        system.debug('query ' + query);
        return Database.getQueryLocator(query);
    }
     
    public void Execute (Database.BatchableContext bc , list<Position_Account__c> scope){
        system.debug('######## scope ' + scope);
        /*
            Modified By : Raghvendra Rathoreset<Id> accountIds = new set<Id>();
            Modified On : 9th May 2018
            Description : Modified to fix issue SIQ-1777. Clear inactive position assignments from SharedWith__c
        */
        //set<string> sharedPositions = new set<string>();
        //
        for(Position_Account__c posAcc : scope){
            if(!string.isBlank(posAcc.SharedWith__c)){
                sharedPositions.addAll(posAcc.SharedWith__c.split(';'));
            }
            sharedPositions.add(posAcc.Position__r.Name);
            accountIds.add(posAcc.Account__c);
        }
        system.debug('#### sharedPositions : '+sharedPositions);
        system.debug('#### accountIds: '+accountIds);

        //Arun-Query on position object because Position Account have ,millions of accounts and we need mode index field to filter data
        map<id,position__c> mapPosition = new map<id,position__c>([select id from position__c where name in : sharedPositions and Team_Instance__c =: teamInstanceId]) ;
        if(mapPosition.size() > 0){  
        
            //RR : Find active shared assignments for selected accounts
            map<string,list<Position_Account__c>> sharedAssignmentMap = new map<string,list<Position_Account__c>>();
            for(Position_Account__c pa : [SELECT Id, Position__r.Name, Account__c FROM Position_Account__c WHERE Proposed_Position__c IN : mapPosition.keySet() AND 
                                          Account__c IN : accountIds AND Assignment_Status__c !=: SalesIQGlobalConstants.POSITION_GEOGRAPHY_INACTIVE AND 
                                          Team_Instance__c =: teamInstanceId]){
                system.debug('pa '+pa);
                system.debug('pa.Position__r.Name '+pa.Position__r.Name);
                list<Position_Account__c> temp = sharedAssignmentMap.get(pa.Position__r.Name);
                system.debug('temp '+temp);
                if(temp == null){
                    temp = new list<Position_Account__c>();
                }
                temp.add(pa);
                sharedAssignmentMap.put(pa.Position__r.Name,temp);
                system.debug('#### sharedAssignmentMap : '+sharedAssignmentMap);
            }
            system.debug('#### sharedAssignmentMap : '+sharedAssignmentMap);

            /*In this loop we are updating current record as well as records that we have maintained inside the stateful map.*/
            for(Position_Account__c posAcc : scope){
                system.debug('#### posAcc : '+posAcc);
                system.debug('#### mapOfAccountIdAndPositionAccount : '+mapOfAccountIdAndPositionAccount);
                if(mapOfAccountIdAndPositionAccount.containsKey(posAcc.Account__c)){
                    list<Position_Account__c> lsPosAcc = mapOfAccountIdAndPositionAccount.get(posAcc.Account__c);               
                    for(Position_Account__c pAcc : lsPosAcc){
                        system.debug('#### pAcc : '+pAcc);
                        //RR : Remove position from pAcc.SharedWith__c if no active assignment found
                    	string sharedWithStr = '';
                        if(!string.isBlank(pAcc.SharedWith__c)){
                            for(string pos : pAcc.SharedWith__c.split(';')){
                            	system.debug('#### shared pos 1 '+ pos);
                                if(sharedAssignmentMap.containsKey(pos)){
                                    for(Position_Account__c pa : sharedAssignmentMap.get(pos)){
                                        system.debug('#### shared pos : '+pos);
                                        if(pa.Account__c == pAcc.Account__c){
                                            sharedWithStr += pos+';';
                                        }
                                    }
                                }
                            }
                            sharedWithStr = sharedWithStr.removeEnd(';');
                            pAcc.SharedWith__c = sharedWithStr;
                            system.debug('#### pAcc.SharedWith__c : '+pAcc.SharedWith__c);
                            system.debug('#### sharedWithStr : '+sharedWithStr);
                        }
                        
                        //Update pAcc.SharedWith__c
                        if(!string.isBlank(pAcc.SharedWith__c) && !pAcc.SharedWith__c.contains(posAcc.Position__r.Name)){                  
                            pAcc.SharedWith__c += ';'+posAcc.Position__r.Name;
                        }else{
                            pAcc.SharedWith__c = posAcc.Position__r.Name;
                        }
                        pAcc.IsShared__c = true;
                        toBeUpdatedPosAccMap.put(pAcc.ID,pAcc);
                        
                        //RR : Remove position from posAcc.SharedWith__c if no active assignment found
                        sharedWithStr = '';
                        if(!string.isBlank(posAcc.SharedWith__c)){
                            for(string pos : posAcc.SharedWith__c.split(';')){
                            	system.debug('#### shared pos : '+pos);
                                if(sharedAssignmentMap.containsKey(pos)){
                                    for(Position_Account__c pa : sharedAssignmentMap.get(pos)){
                                        system.debug('#### shared assignment : '+ pa);
                                        if(pa.Account__c == posAcc.Account__c){
                                            sharedWithStr += pos+';';
                                        }
                                    }
                                }
                            }
                            sharedWithStr = sharedWithStr.removeEnd(';');
                            posAcc.SharedWith__c = sharedWithStr;
                            system.debug('#### posAcc.SharedWith__c : '+posAcc.SharedWith__c);
                            system.debug('#### sharedWithStr : '+sharedWithStr);
                        }

                        if(!string.isBlank(posAcc.SharedWith__c) && !posAcc.SharedWith__c.contains(pAcc.Position__r.Name)){
                            posAcc.SharedWith__c += ';'+pAcc.Position__r.Name;
                        }else{
                            posAcc.SharedWith__c = pAcc.Position__r.Name;
                        }
                        posAcc.IsShared__c = true;
                        toBeUpdatedPosAccMap.put(posAcc.ID,posAcc);
                    }
                    system.debug('############ toBeUpdatedPosAccMap ' + toBeUpdatedPosAccMap);
                    mapOfAccountIdAndPositionAccount.get(posAcc.Account__c).add(posAcc);
                }else{
                    mapOfAccountIdAndPositionAccount.put(posAcc.Account__c,new list<Position_Account__c>{posAcc});
                }
                system.debug('############ mapOfAccountIdAndPositionAccount ' + mapOfAccountIdAndPositionAccount);
            }
            
            /*Raghav - Remove the sharing flag from the assignments not shared anymore*/
            for(Id accId : mapOfAccountIdAndPositionAccount.keyset()){
                if(mapOfAccountIdAndPositionAccount.get(accId).size() == 1){
                    list<Position_Account__c> posAcc = mapOfAccountIdAndPositionAccount.get(accId);
                    if(posAcc[0].IsShared__c || posAcc[0].SharedWith__c != ''){
                        posAcc[0].IsShared__c = false;
                        posAcc[0].SharedWith__c = '';
                        toBeUpdatedPosAccMap.put(posAcc[0].Id,posAcc[0]);
                    }
                }
            }
            
            update toBeUpdatedPosAccMap.values();
        }
    }
     
    public void Finish(Database.BatchableContext bc){
        system.debug('######### Batch Execution finished UpdatePositionAccountSharedBatch');
        system.debug('#########isBulkMovement '+isBulkMovement);
        //Below condition will execute only when lighting mode is on, because in that cae we need to update CR status to complete
        if(isBulkMovement){
            //SalesIQUtility.getChangeRequestbyId(changeReqId);
        }
    }
}