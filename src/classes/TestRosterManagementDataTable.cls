@IsTest 
public with sharing class TestRosterManagementDataTable {
   
    static testMethod void TestRosterManagementData(){
    
        Test.startTest();

        TotalApproval__c totalApproval = new TotalApproval__c (No_Of_Approval__c = 3 , Name='Approvals');
        insert totalApproval;
         
        ReadOnlyApplication__c setting  = new ReadOnlyApplication__c();
        setting.Roster__c = true;
        setting.Secondary_Assignment_Roster__c = true;
        insert setting;
        
        Team__c geoBasedTeam = new Team__c(Name='HTN',Type__c='Base', Effective_Start_Date__c = Date.newInstance(2017,01,01), Effective_End_Date__c = Date.newInstance(2018,12,31));
        insert geoBasedTeam;
         
        Team_Instance__c geoBasedTeamInstance = new Team_Instance__c();
        geoBasedTeamInstance.Name = 'HTN_Q1_2016';
        geoBasedTeamInstance.Alignment_Period__c = 'Current';
        geoBasedTeamInstance.Team__c = geoBasedTeam.id;
        geoBasedTeamInstance.Alignment_Type__c = 'Zip';
        geoBasedTeamInstance.isActiveCycle__c = 'Y';
        geoBasedTeamInstance.IC_EffstartDate__c = Date.newInstance(2017,01,01);
        geoBasedTeamInstance.IC_EffendDate__c = Date.newInstance(2018,12,31);
        insert geoBasedTeamInstance; 
        
        Geography__c geo1 = new Geography__c();
        geo1.Name = '11788';
        geo1.City__c='Test 11788';
        geo1.State__c = '11788';
        geo1.Centroid_Latitude__c = 0.0;
        geo1.Centroid_Longitude__c = 0.0;
        
        insert geo1; 
            
        Account acc = new Account();
        acc.Name = '11788';
        acc.FirstName__c = 'Abby';
        acc.BillingCity='NJ';
        acc.BillingState='NY';
        acc.AccountNumber='11788';
        acc.type = 'Physician';
        acc.AccountType__c = 'Physician';
        acc.BillingPostalCode = '11788';
        acc.Speciality__c = '11788Oncology';
        acc.Alignment_Type__c = 'Direct Aligned';
        acc.BillingLatitude = 40.5571389900;
        acc.BillingLongitude = -74.2123329900;
        insert acc;
            
        /*Team_Instance_Account__c teamInstanceAcc = CallPlanTestDataFactory.createTeamInstanceAccount(acc,geoBasedTeamInstance);
        insert teamInstanceAcc;*/
        
        /*Team_Instance_Geography__c teamInsGeo1 = new Team_Instance_Geography__c();
        teamInsGeo1.Geography__c = geo1.id;
        teamInsGeo1.Team_Instance__c = geoBasedTeamInstance.id;
        teamInsGeo1.Effective_Start_Date__c = Date.today().addMonths(-1);
        teamInsGeo1.Effective_End_Date__c = Date.today().addYears(1);
        teamInsGeo1.Metric1__c  =2.010000000000;
        teamInsGeo1.Metric2__c  =29.000000000000;
        teamInsGeo1.Metric3__c  =2.000000000000;
        teamInsGeo1.Metric4__c  =1.431479545000;
        teamInsGeo1.Metric5__c  =1.490000000000;
        teamInsGeo1.Metric6__c  =2.010000000000;
        teamInsGeo1.Metric7__c  =29.000000000000;
        teamInsGeo1.Metric8__c  =2.000000000000;
        teamInsGeo1.Metric9__c  =1.431479545000;
        teamInsGeo1.Metric10__c =1.490000000000;
        insert teamInsGeo1;*/
        
        Position__c RegionPos = CallPlanTestDataFactory.createRegionPosition(geoBasedTeam);
        RegionPos.Hierarchy_Level__c = '3';
        RegionPos.Client_Territory_Name__c='1NE30001';
        insert RegionPos;
        
        Position__c newPosParentPos = CallPlanTestDataFactory.createRegionPosition(geoBasedTeam);
        newPosParentPos.Hierarchy_Level__c = '3';
        newPosParentPos.Client_Territory_Name__c='1NE30001';
        insert newPosParentPos;
        
        Position__c positionObj2 = CallPlanTestDataFactory.createPositionParent(RegionPos, geoBasedTeam);
        positionObj2.Hierarchy_Level__c = '2';
        positionObj2.Client_Territory_Name__c='1NE30001';
        
        insert positionObj2;
        
        Position__c positionObj = CallPlanTestDataFactory.createPosition(positionObj2,geoBasedTeam);
        positionObj.Hierarchy_Level__c = '2';
        positionObj.Client_Territory_Name__c='1NE30001';
        positionObj.Related_Position_Type__c = 'Base';
        positionObj.Team_Instance__c = geoBasedTeamInstance.id;
        insert positionObj;
            
        Position__c DestinationpositionObj = CallPlanTestDataFactory.createDestinationPosition(positionObj2,geoBasedTeam);
        DestinationpositionObj.Hierarchy_Level__c = '1';
        DestinationpositionObj.Client_Territory_Name__c='1NE30001';
        insert DestinationpositionObj;
        
        set<Id> posIds = new set<Id>();
        posIds.add(RegionPos.id);
        posIds.add(newPosParentPos.id);
        posIds.add(positionObj2.id);
        posIds.add(positionObj.id);
        posIds.add(DestinationpositionObj.id);
    
        Position_Team_Instance__c posTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(positionObj,positionObj,geoBasedTeamInstance);
        insert posTeamInstance; 
        
        Position_Team_Instance__c posTeamInstanceDestination =CallPlanTestDataFactory.CreatePosTeamInstance(DestinationpositionObj,positionObj2,geoBasedTeamInstance);
        insert posTeamInstanceDestination;
        
         Position_Team_Instance__c posDistrictTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(positionObj2,RegionPos,geoBasedTeamInstance ); 
         insert posDistrictTeamInstance;
        
         Position_Team_Instance__c posRegionTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(RegionPos,null,geoBasedTeamInstance ); 
         insert posRegionTeamInstance;
        
        Position_Account__c PosAccount = CallPlanTestDataFactory.createPositionAccount(acc, positionObj, geoBasedTeamInstance,posTeamInstance);
        insert PosAccount;
        
        Position_Geography__c Posgeo = CallPlanTestDataFactory.createPosGeography(geo1,positionObj,posTeamInstance,geoBasedTeamInstance );
        insert Posgeo;      
            
        Change_Request_Type__c objZipRequest = CallPlanTestDataFactory.CreateZipRequestType();
        insert objZipRequest;
        
        Change_Request_Type__c objAccRequest = CallPlanTestDataFactory.CreateAccountRequestType();
        insert objAccRequest;
        
        Change_Request_Type__c objCallPlanRequest = CallPlanTestDataFactory.CreateCallPlanRequestType();
        insert objCallPlanRequest;
         
        Change_Request_Approver_Detail__c objCRADsecond = CallPlanTestDataFactory.createCRADsecond(positionObj, new User());
        insert objCRADsecond;
         
        list<Change_Request__c> changeRequestObj = CallPlanTestDataFactory.createChangeRequestDestSourc(new User(), positionObj, DestinationpositionObj, objZipRequest, objCRADsecond, posTeamInstance, posTeamInstanceDestination, geoBasedTeamInstance);
        //insert changeRequestObj;
            
        CR_Position__c objCRposition = CallPlanTestDataFactory.createCRPosition(changeRequestObj[0]);
        //insert objCRposition;
        
        Employee__c emp = CallPlanTestDataFactory.createEmployee();
        insert emp;
        
        CR_Employee_Assignment__c crEmpAssigment = CallPlanTestDataFactory.cr_EmployeeAssignment(changeRequestObj[1], emp, positionObj);
        //insert crEmpAssigment;
        
        Position_Employee__c posEmp = CallPlanTestDataFactory.createPosEmploye(positionObj, emp);
        insert posEmp;
        
        User_Access_Permission__c access = new User_Access_Permission__c();
        access.Position__c = RegionPos.id;
        access.Map_Access_Position__c = RegionPos.id;
        access.name='access';
        access.Team_Instance__c = geoBasedTeamInstance.id;   
        access.User__c = UserInfo.getUserId();
        access.Is_Active__c = true;
        insert access;
        
        Employee__c emp2 = new Employee__c();
        emp2.Name = 'Test Emp2';
        emp2.Employee_ID__c = '12346';
        emp2.Last_Name__c = 'Emp2';
        emp2.FirstName__c = 'Test1';
        emp2.Gender__c = 'M';
        emp2.Change_status__c = 'Pending';
        emp2.USER__c = userinfo.getuserid();
        insert emp2;
        
        Position_Employee__c posEmp2 = new Position_Employee__c();            
        posEmp2.Position__c = positionObj.id;
        posEmp2.Employee__c = emp2.id;        
        posEmp2.Status__c ='Approved';
        posEmp2.effective_start_date__c = date.today()-3;
        posEmp2.Effective_End_Date__c = date.today()+3;
        posEmp2.Assignment_Type__c = SalesIQGlobalConstants.POSITION_EMPLOYEE_ASSIGNMENT_SECONDARY;
        insert posEmp2;
            
        Change_Request_Type__c crTypeRoster = CallPlanTestDataFactory.CreateRequestType('Roster');
        insert crTypeRoster;
        
        CR_Team_Instance_Config__c crTeamInstConf = new CR_Team_Instance_Config__c();
        //crTeamInstConf.name = 'TerrConfigCr';
        crTeamInstConf.Configuration_Value__c = 'Territory';
        crTeamInstConf.Configuration_Type__c = 'Hierarchy Configuration';
        crTeamInstConf.Configuration_Name__c = '1';
        crTeamInstConf.team_instance__c = geoBasedTeamInstance.id;
        //insert crTeamInstConf;
            
        CR_Team_Instance_Config__c crTeamInstConfDistrict = new CR_Team_Instance_Config__c();
        //crTeamInstConfDistrict.name = 'DistConfigCr';
        crTeamInstConfDistrict.Configuration_Value__c = 'District';
        crTeamInstConfDistrict.Configuration_Type__c = 'Hierarchy Configuration';
        crTeamInstConfDistrict.Configuration_Name__c = '2';
        crTeamInstConfDistrict.team_instance__c = geoBasedTeamInstance.id;
        //insert crTeamInstConfDistrict;
    
        insert new CR_Team_Instance_Config__c[]{crTeamInstConf, crTeamInstConfDistrict} ;
            
        Test.stopTest();
        
        RosterManagementDataTableCtlr classObjRosMan = new RosterManagementDataTableCtlr();  
        
        
        RosterManagementDataTableCtlr.wrapperClass wrapper = new RosterManagementDataTableCtlr.wrapperClass(RegionPos, '', posEmp, '',true,'',Date.newInstance(2016,01,01),Date.newInstance(2017,01,01),'','',Date.today(), RegionPos.hierarchy_level__c);
       
        classObjRosMan.actionInit();
        classObjRosMan.crTrackingOn = true;
        classObjRosMan.updateshowCRMesgFlag();
        classObjRosMan.openHierarchyChange();
        classObjRosMan.closeHierarchyChange();        
        classObjRosMan.selectedEventId = objCRposition.id;
        classObjRosMan.selectedPosition = positionObj2.id;
        classObjRosMan.type = positionObj2.hierarchy_level__c;
        classObjRosMan.newPosParentPos = newPosParentPos.name;
        //classObjRosMan.fullPartSelected = 'Full Time';
        classObjRosMan.newPosClientTerritoryName = 'Sample 1'; 
        classObjRosMan.newPosClientPositionCode = 'Terr001';
        classObjRosMan.newPosInstance.Effective_End_Date__c = Date.newInstance(2018,12,31);
        classObjRosMan.newPosInstance.Effective_Start_Date__c = Date.newInstance(2017,01,01);
        RosterManagementDataTableCtlr.newPosTeam = geoBasedTeam.id;
             
        classObjRosMan.OpenNewPositionPopup();
         
        classObjRosMan.newPosInstance = null;
        
        //classObjRosMan.createNewPosition();
        classObjRosMan.newPosInstance = new Position__c();
        
        positionObj2.Parent_Position__c = RegionPos.id;
        update positionObj2;
        //classObjRosMan.createNewPosition();
        //classObjRosMan.fullPartSelected = 'Full Time';
        //classObjRosMan.createNewPosition();
        classObjRosMan.newPosClientTerritoryName = 'test pos';
        classObjRosMan.newPosClientPositionCode = null;
        //classObjRosMan.createNewPosition();
        classObjRosMan.newPosClientPositionCode = '12345';
        //classObjRosMan.createNewPosition();
        classObjRosMan.newPosInstance.Effective_End_Date__c = Date.newInstance(system.today().year(), system.today().month(), system.today().day());
        classObjRosMan.newPosInstance.Effective_Start_Date__c = Date.newInstance(system.today().year(), system.today().month(), system.today().day());
        //classObjRosMan.createNewPosition();
        classObjRosMan.newPosParentPos = null;
        //classObjRosMan.createNewPosition();
        classObjRosMan.newPosParentPos = newPosParentPos.name;
        //classObjRosMan.createNewPosition();
    
    
        classObjRosMan.editedPosition = positionObj;
        positionObj.Effective_End_Date__c = Date.newInstance(2018,12,31);
        
        classObjRosMan.closeEditPopup();
        
        classObjRosMan.selectedEventId = crEmpAssigment.id;
        classObjRosMan.selectedPosRecord = positionObj.id;
        classObjRosMan.deleteTerritory();
        classObjRosMan.selectedPosRecord = positionObj2.id;
        classObjRosMan.deleteTerritory();
        
        
    
        classObjRosMan.selectedEvent = 'Manage Assignment';
        
        classObjRosMan.selectedEvent = 'Expire Position';
        
        classObjRosMan.selectedEventId = objCRposition.id;
        //classObjRosMan.expirePosition();
        //classObjRosMan.closeWorkbenchPopup();
        
        classObjRosMan.selectedEventId = crEmpAssigment.id; 
            
        classObjRosMan.onchangeTeamVal();
        classObjRosMan.displayErrorOnClick();
             
        classObjRosMan.selectedPosRecord = positionObj2.id;
        classObjRosMan.editPosition();
        
        classObjRosMan.closeCRMsg();
        classObjRosMan.closeNewPositionPopup();
        classObjRosMan.closeDeletePopup();
        //classObjRosMan.refreshListViewBasedOnTeamInstance();
        
        //list<SelectOption> ls = RosterManagementDataTableCtlr.getTeamList();
        map<string,string> posRGB = RosterManagementDataTableCtlr.getAllPositionsRGBs();
        string testJSON = RosterManagementDataTableCtlr.getJsonString();
        
        set<id> ChangeReqIdset = new set<Id>();
        
        RosterUtility.getPositionEmployeByPositionAndEmployeeId(positionObj.id,emp.id);
        RosterUtility.getCRpositionByStatus('','');
        /*RosterUtility.getCRpositionById(objCRposition.id);
        RosterUtility.getCRpositionByStatus('','');
        RosterUtility.getCRPositionByChangeRequestId('');
        RosterUtility.getEmployeeAssignmentById('');
        RosterUtility.getEmployeeAssignmentByChangeReqId(ChangeReqIdset);
        RosterUtility.getChangeRequestforUpdate(changeRequestObj[0].id,positionObj.ID,newPosParentPos.ID,'Pending',Date.newInstance(2018,09,09));
        RosterUtility.getEmployeeAssignmentRosterById(crEmpAssigment.id);*/
        
    }
    
    static testMethod void TestGetHighlightCriteria(){
        Business_Unit__c bu = new Business_Unit__c();
        bu.Code__c = 'AxtriaSalesIQTM__Assignment_status__c';
        bu.Name = 'Assignment Status';
        bu.Operator__c = '=';
        bu.Interface_Name__c = 'Position';
        bu.Description__c = 'Vacant';
        bu.Color__c = '#FF0000';
        insert bu;
        test.startTest();
        RosterManagementDataTableCtlr.getHighlightCriteria();
        test.stopTest();
    }
}