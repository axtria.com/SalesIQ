/******************************************************************************************************************************************************
 * Name         :   RequestQueueTriggerHandler.cls
 * Description  :   This class for will use as Handler class for RequestQueueTrigger
 * Author       :   Ankur Dohare
 * Created On   :   Nov 2018
******************************************************************************************************************************************************/

public class RequestQueueTriggerHandler {
	public static Set<Id> queueIdsToAvoidRecursion = new Set<Id>();
    private static boolean firstRun = true;

    public static boolean isFirstRun(){
        if(firstRun){
            firstRun = false;
            return true;
        }else{
            return firstRun;
        }
    }

	public static void updateRequestQueue(){
        if(!isFirstRun())
            return;

        String objectAPIName = SalesIQGlobalConstants.NAME_SPACE+'Activity_Log__c';
        Id recordTypeId = Schema.getGlobalDescribe().get(objectAPIName).getDescribe().getRecordTypeInfosByName().get('Scenario Execution').getRecordTypeId();

        List<Activity_Log__c> queuedRequests = [ SELECT id,Scenario__c,Scenario__r.Rule_Execution_Status__c, Execution_Mode__c, CreatedById, CreatedBy.Name,Job_Status__c FROM Activity_Log__c WHERE Job_Status__c = :SalesIQGlobalConstants.REQUEST_QUEUE_STATUS_NOT_STARTED and RecordtypeId = :recordTypeId and Id NOT IN : trigger.new AND Id NOT IN :queueIdsToAvoidRecursion ORDER BY Priority__c, CreatedDate  ];

        Set<Id> scenarioSet = new Set<Id>();
        for(Activity_Log__c queueObj : (List<Activity_Log__c>)Trigger.new) {
        	//queueIdsToAvoidRecursion.add(queueObj.Id);
            if(queueObj.RecordTypeId == recordTypeId && queueObj.Job_Status__c == SalesIQGlobalConstants.REQUEST_QUEUE_STATUS_NOT_STARTED) {
                queuedRequests.add(queueObj);
                scenarioSet.add(queueObj.Scenario__c);
            }
        }

        Map<Id, Scenario__c> updateScenarioMap = new Map<Id, Scenario__c>([ select Rule_Execution_Status__c from Scenario__c where id in :scenarioSet]);

        Map<Id, Activity_Log__c> scenarioRequestMap = new Map<Id, Activity_Log__c>();
        Set<Id> createdByUserId = new Set<Id>();
        for(Activity_Log__c queueObj : queuedRequests){
            if(!scenarioRequestMap.containsKey(queueObj.Scenario__c)) {

                Activity_Log__c dupObj = queueObj.clone(true,true,true,false);

                String scenarioStatus;
                if(updateScenarioMap.containsKey(dupObj.Scenario__c)) {
                    scenarioStatus = updateScenarioMap.get(dupObj.Scenario__c).Rule_Execution_Status__c ;
                } else {
                    scenarioStatus = dupObj.Scenario__r.Rule_Execution_Status__c ;
                }

                if(scenarioStatus == null || scenarioStatus == SalesIQGlobalConstants.READY_FOR_RUN || scenarioStatus == SalesIQGlobalConstants.SUCCESS || scenarioStatus == SalesIQGlobalConstants.ERROR  ) {
                    dupObj.Job_Status__c = SalesIQGlobalConstants.QUEUED;
                    scenarioRequestMap.put(dupObj.Scenario__c, dupObj);
                    createdByUserId.add(dupObj.CreatedById);
                }
            }
        }

        system.debug(':::: scenarioRequestMap ::::');
        system.debug(scenarioRequestMap);
        update scenarioRequestMap.values();

        Map<Id, User> userMap = new Map<Id, User>([select Id, Name from User where Id in: createdByUserId]);
       
        List<Scenario__c> tobeUpdatedScenarios = new List<Scenario__c>();
        for(Id scenarioId :scenarioRequestMap.keySet()){
            Scenario__c newScenario = new Scenario__c(Id = scenarioId);
            newScenario.Rule_Execution_Status__c =  SalesIQGlobalConstants.QUEUED;
            newScenario.Request_Queue__c = scenarioRequestMap.get(scenarioId).Id ;
            newScenario.Promote_Mode__c = scenarioRequestMap.get(scenarioId).Execution_Mode__c;
            newScenario.Run_Response_Details__c = null;
            newScenario.Last_Executed_By__c = userMap.get(scenarioRequestMap.get(scenarioId).CreatedById).Name ;
            tobeUpdatedScenarios.add(newScenario);
        }
        update tobeUpdatedScenarios;

        system.debug(':::: tobeUpdatedScenarios ::::');
        system.debug(tobeUpdatedScenarios);
    }

    public static void markFailScenariosInQueue(Activity_Log__c requestQueue){
        String objectAPIName = SalesIQGlobalConstants.NAME_SPACE+'Activity_Log__c';
        Id recordTypeId = Schema.getGlobalDescribe().get(objectAPIName).getDescribe().getRecordTypeInfosByName().get('Scenario Execution').getRecordTypeId();

        Set<String> excludedStatuses = new Set<String>{SalesIQGlobalConstants.SUCCESS, SalesIQGlobalConstants.ERROR,SalesIQGlobalConstants.IN_PROGRESS};
        List<Activity_Log__c> queuedRequests = [ select id from Activity_Log__c where Scenario__c = :requestQueue.Scenario__c and Id <> :requestQueue.Id  and RecordtypeId = :recordTypeId and Job_Status__c NOT IN :excludedStatuses ];
        
        for(Activity_Log__c queueObj :queuedRequests ){
            queueObj.Job_Status__c = SalesIQGlobalConstants.ERROR;
        }

        update queuedRequests;
    }

    public static void updateJobStatus(Map<Id,String> queueIdToStatusMap){
        List<Activity_Log__c> queuedRequests = new List<Activity_Log__c>();
        for(Id queueId : queueIdToStatusMap.keySet()){
            Activity_Log__c queueObj = new Activity_Log__c(Id = queueId);
            queueObj.Job_Status__c = queueIdToStatusMap.get(queueId);
            queuedRequests.add(queueObj);
        }

        update queuedRequests;
    }

    public static void deleteQueueRecordsOnScenarioDelete(){
        String objectAPIName = SalesIQGlobalConstants.NAME_SPACE+'Activity_Log__c';
        Id recordTypeId = Schema.getGlobalDescribe().get(objectAPIName).getDescribe().getRecordTypeInfosByName().get('Scenario Execution').getRecordTypeId();

        List<Activity_Log__c> queuedRequests = [ SELECT id from Activity_Log__c where RecordtypeId = :recordTypeId AND Scenario__c = null limit 10000 ];

        if(queuedRequests.size() > 0 )
            delete queuedRequests;
    }
}