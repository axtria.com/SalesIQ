/*
Author : Ankit Manendra
Description : Test class for CPGDeleteDerivedFieldCtr
Date : 2 Novomber, 2017

*/

@isTest
private class TestCPGDeleteDerivedFieldCtr{
    static testMethod void cpgDeleteDerTest() {
        
        Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj;
        
        Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        insert scenarioObj;
        
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        sceRuleInsDetObj.Exec_Seq__c=1;
        insert sceRuleInsDetObj;
        
        Data_Set_Rule_Map__c dataSetRuleObj = new Data_Set_Rule_Map__c();
        dataSetRuleObj.scenario_rule_instance_id__c=sceRuleInsDetObj.id;
        dataSetRuleObj.ds_type__c='I';
        insert dataSetRuleObj;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj.Name='Rank';
        insert busRuleTypeMasObj;
        
        Business_Rules__c busRuleObj = new Business_Rules__c();
        busRuleObj.Name='Rank rule 278';
        busRuleObj.Business_Rule_Type_Master__c=busRuleTypeMasObj.id;
        busRuleObj.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj.id;
        insert busRuleObj;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj.Param_Name__c='derived field 278';
        busRuleFieldObj.Business_Rule__c=busRuleObj.id;
        busRuleFieldObj.Param_Data_Type__c='Numeric';
        busRuleFieldObj.Param_Type__c='Derived';
        busRuleFieldObj.IO_Flag__c='O';
         
        insert busRuleFieldObj;
        
        Config_Call_Balancing_Map__c configCall = new Config_Call_Balancing_Map__c();
        configCall.Scenario_Rule_Instance_Details__c=sceRuleInsDetObj.id;
        insert configCall;
        
        Test.startTest();
        
        CPGDeleteDerivedFieldCtr cpgDelDer = new CPGDeleteDerivedFieldCtr();
        
        List<CPGDeleteDerivedFieldCtr.FieldMapWrapper> busRuleList = CPGDeleteDerivedFieldCtr.deleteComputationErrorRules(busRuleFieldObj.id);
        
        CPGConstants.Response res = CPGDeleteDerivedFieldCtr.DeleteCategoryBusinessRule(busRuleObj.id,sceRuleInsDetObj.id,'derived field 278');
        
        Workspace__c workSpaceObj1 = new Workspace__c();
        workSpaceObj1.Name = 'Workspace CPG 1';
        workSpaceObj1.Workspace_Description__c='Workspace CPG 1';
        workSpaceObj1.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj1;
        
        Scenario__c scenarioObj1 = new Scenario__c ();
        scenarioObj1.Workspace__c= workSpaceObj1.id;
        insert scenarioObj1;
        
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj1 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj1.Scenario_Id__c= scenarioObj1.id;
        sceRuleInsDetObj1.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        sceRuleInsDetObj1.Exec_Seq__c=1;
        insert sceRuleInsDetObj1;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj1 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj1.Name='Rank';
        insert busRuleTypeMasObj1;
        
        Business_Rules__c busRuleObj1 = new Business_Rules__c();
        busRuleObj1.Name='Rank rule 278';
        busRuleObj1.Business_Rule_Type_Master__c=busRuleTypeMasObj1.id;
        busRuleObj1.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj1.id;
        insert busRuleObj1;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj1 = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj1.Param_Name__c='derived field 278';
        busRuleFieldObj1.Business_Rule__c=busRuleObj1.id;
        busRuleFieldObj1.Param_Data_Type__c='Numeric';
        busRuleFieldObj1.Param_Type__c='Derived';
        busRuleFieldObj1.IO_Flag__c='O';
         
        insert busRuleFieldObj1;
        
        CPGConstants.Response res1 = CPGDeleteDerivedFieldCtr.DeleteComputationType(busRuleObj1.id,sceRuleInsDetObj1.id,'derived field 278');
        
        CPGConstants.Response res3 = CPGDeleteDerivedFieldCtr.deleteRankFields(busRuleObj1.id,sceRuleInsDetObj1.id,'derived field 278');
        
        Workspace__c workSpaceObj2 = new Workspace__c();
        workSpaceObj2.Name = 'Workspace 2';
        workSpaceObj2.Workspace_Description__c='Workspace 2';
        workSpaceObj2.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj2;
        
        Scenario__c scenarioObj2 = new Scenario__c ();
        scenarioObj2.Workspace__c= workSpaceObj2.id;
        insert scenarioObj2;
        
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj2 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj2.Scenario_Id__c= scenarioObj2.id;
        sceRuleInsDetObj2.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        sceRuleInsDetObj2.Exec_Seq__c=1;
        insert sceRuleInsDetObj2;
        
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj3 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj3.Scenario_Id__c= scenarioObj2.id;
        sceRuleInsDetObj3.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        sceRuleInsDetObj3.Exec_Seq__c=2;
        insert sceRuleInsDetObj3;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj2 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj2.Name='Rank';
        insert busRuleTypeMasObj2;
        
        Business_Rules__c busRuleObj2 = new Business_Rules__c();
        busRuleObj2.Name='Rank rule 278';
        busRuleObj2.Business_Rule_Type_Master__c=busRuleTypeMasObj2.id;
        busRuleObj2.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj2.id;
        insert busRuleObj2;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj2 = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj2.Param_Name__c='derived field 278';
        busRuleFieldObj2.Business_Rule__c=busRuleObj2.id;
        busRuleFieldObj2.Param_Data_Type__c='Numeric';
        busRuleFieldObj2.Param_Type__c='Derived';
        busRuleFieldObj2.IO_Flag__c='O';
         
        insert busRuleFieldObj2;
        
        CPGConstants.Response res2 = CPGDeleteDerivedFieldCtr.deleteRankFields(busRuleObj2.id,sceRuleInsDetObj2.id,'derived field 278');
        
         Workspace__c workSpaceObj3 = new Workspace__c();
        workSpaceObj3.Name = 'Workspace 3';
        workSpaceObj3.Workspace_Description__c='Workspace 3';
        workSpaceObj3.Workspace_Start_Date__c=Date.parse('1/1/2014');
        
        insert workSpaceObj3;
        
        Scenario__c scenarioObj3 = new Scenario__c ();
        scenarioObj3.Workspace__c= workSpaceObj3.id;
        insert scenarioObj3;
        
        Scenario_Rule_Instance_Details__c sceRuleInsDetObj4 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj4.Scenario_Id__c= scenarioObj3.id;
        sceRuleInsDetObj4.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        insert sceRuleInsDetObj4;
        
        Business_Rule_Type_Master__c busRuleTypeMasObj3 = new Business_Rule_Type_Master__c();
        busRuleTypeMasObj3.Name='Rank';
        insert busRuleTypeMasObj3;
        
        Business_Rules__c busRuleObj3 = new Business_Rules__c();
        busRuleObj3.Name='Rank rule 278';
        busRuleObj3.Business_Rule_Type_Master__c=busRuleTypeMasObj3.id;
        busRuleObj3.ScenarioRuleInstanceDetails__c=sceRuleInsDetObj3.id;
        insert busRuleObj3;
        
        Business_Rule_FieldMap_Details__c busRuleFieldObj3 = new Business_Rule_FieldMap_Details__c();
        busRuleFieldObj3.Param_Name__c='derived field 278';
        busRuleFieldObj3.Business_Rule__c=busRuleObj3.id;
        busRuleFieldObj3.Param_Data_Type__c='Numeric';
        busRuleFieldObj3.Param_Type__c='Derived';
        busRuleFieldObj3.IO_Flag__c='O';
         
        insert busRuleFieldObj3;
        
        CPGConstants.Response res4 = CPGDeleteDerivedFieldCtr.deleteCallBalanceRule(busRuleObj3.id,sceRuleInsDetObj3.id,'derived field 278');
        
        
        Test.stopTest();
        
    }
    
}