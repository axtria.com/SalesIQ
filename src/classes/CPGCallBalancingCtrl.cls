public class CPGCallBalancingCtrl {
    public static string scenarioId='';
    @AuraEnabled
    public static List<Scenario_Rule_Instance_Details__c> getScenarioID(String ScenarioRuleInstanceId){
        
        List<Scenario_Rule_Instance_Details__c> sceID = [SELECT Id, Scenario_Id__c FROM Scenario_Rule_Instance_Details__c where id=:ScenarioRuleInstanceId];
        return sceID;
    }
    @AuraEnabled
    public static boolean ValidateRuleData(String scenarioInstanceId){      
        return FillRuleDetails(scenarioInstanceId); 
    }
     @AuraEnabled
    public static boolean ValidateGuardRails(String ScenarioRuleInstanceId){              
        List<BRCallBalancingParamsWrapper> lst= getConstraintValues(ScenarioRuleInstanceId);
        if(lst.size()>0)
            return true;
        else
            return false;
    }
    public static boolean FillRuleDetails(String scenarioInstanceId){
        String scenarioId= CPGUtility.GetScenarioId(scenarioInstanceId);
        system.debug('ishita scenarioInstanceId: '+scenarioInstanceId);
        set<Id> rankBusinessId = new set<Id>();
        map<Id,String> mapBusinessMaster = new map<Id,String>();
        list<business_rule_type_master__c> lstRuleType = [select id, name,Component_Type__r.name
                                                          from business_rule_type_master__c
                                                          where Component_Type__r.name
                                                          in ('Call Balancing','Aggregation','Derived Field')];
        
        //list<business_rule_type_master__c> lstRulesCall = new list<business_rule_type_master__c>();
        for(business_rule_type_master__c ruleMaster : lstRuleType){
            if(ruleMaster.Component_Type__r.name =='Aggregation'){                
                mapBusinessMaster.put(ruleMaster.id, ruleMaster.Component_Type__r.name);  
            }
            else if(ruleMaster.Component_Type__r.name =='Derived Field' && ruleMaster.name=='Rank'){
                mapBusinessMaster.put(ruleMaster.id, ruleMaster.name); 
            }
        }
        list<Business_rules__c> lstRules = [select id, aggregate_level__c,Business_Rule_Type_Master__c,
                                            ScenarioRuleInstanceDetails__c
                                            from Business_rules__c
                                            where Business_Rule_Type_Master__c in: mapBusinessMaster.keySet() 
                                            and Scenario__c=: scenarioId];
        list<string> lstLevels = new list<string>();
        for(Business_rules__c rule : lstRules){
            String componentName = mapBusinessMaster.get(rule.Business_Rule_Type_Master__c);
            system.debug('ishita: '+componentName);
            if(componentName =='Aggregation'){
                lstLevels.add(rule.aggregate_level__c);
            }
            else if (componentName =='Rank'){
                rankBusinessId.add(rule.id);
            }
        }
       
        if(rankBusinessId.size()>0 && lstLevels.size()>0)
            return true;
        else
            return false;
        
    }
    
    @AuraEnabled
    public static List<BRCallBalancingWrapper>  getcallBalancingRuleName(string scenarioRuleInstanceId){
        List<BRCallBalancingWrapper> lstWrapper=new List<BRCallBalancingWrapper>();
        ComponentTypeMaster__c objCallPlanBalancing = [SELECT Component_Definition__c,Component_Type_Master_Ext_Id__c,
                                                       Id,Name
                                                       FROM ComponentTypeMaster__c 
                                                       WHERE Component_Definition__c = 'CPGCallPlanBalancingRules' LIMIT 1];
        List<Business_Rule_Type_Master__c> lstBusinessRuleTypeMaster = [SELECT Business_Rule_Type_Master_Ext_Id__c
                                                                        ,Component_Type__c,Id,Name 
                                                                        FROM Business_Rule_Type_Master__c
                                                                        WHERE Component_Type__c =: objCallPlanBalancing.Id];
        set<Id> sbusinessRuleTypeMasterIds = new set<Id>();
        for(Business_Rule_Type_Master__c obj:lstBusinessRuleTypeMaster){
            sbusinessRuleTypeMasterIds.add(obj.Id);
        }
       // system.debug('ScenarioRuleInstanceId'+scenarioRuleInstanceId);
       // system.debug('sbusinessRuleTypeMasterIds'+sbusinessRuleTypeMasterIds);
        List<Scenario_Rule_Instance_Details__c> lstScenarioRuleInstanceDetails = [Select Id,Component_Type_Master__c 
                                                                                  from Scenario_Rule_Instance_Details__c 
                                                                                  where Id =:scenarioRuleInstanceId]; 
        
        List<Business_Rules__c> lstBusinessRules = [SELECT Business_Rule_Type_Master__r.Name,Aggregate_Level__c,
                                                    Id,Name,Parent_Rule_Id__c,Prioritization_Order__c,
                                                    ScenarioRuleInstanceDetails__c,Scenario__c 
                                                    FROM Business_Rules__c 
                                                    WHERE ScenarioRuleInstanceDetails__c =:scenarioRuleInstanceId 
                                                    AND Business_Rule_Type_Master__c IN:sbusinessRuleTypeMasterIds];
        
        // system.debug('lstBusinessRules'+lstBusinessRules);
        for(Business_Rules__c obj:lstBusinessRules){
            lstWrapper.add(new BRCallBalancingWrapper(obj.Id,obj.Name,obj.Business_Rule_Type_Master__r.Name,obj.Aggregate_Level__c));
        }
      //  system.debug(lstWrapper);
        return  lstWrapper;                                                          
        
    }
    
    @AuraEnabled
    public static List<BRCallBalancingParamsWrapper>  getConstraintValues(string ScenarioRuleInstanceId){
        //Get scenario id from callplanning scearioruleintanceid
        system.debug('ScenarioRuleInstanceId-->'+ScenarioRuleInstanceId);
        String scenarioId= CPGUtility.GetScenarioId(ScenarioRuleInstanceId);
        system.debug('scenarioId-->'+scenarioId);
        
        //Get scenario instanceid of aggregation component
        List<Scenario_Rule_Instance_Details__c> lstscenarioRuleInstance = [SELECT ComponentTypeLabel__c,Exec_Seq__c,                                                                       
                                                                           Id,Scenario_Id__c 
                                                                           FROM Scenario_Rule_Instance_Details__c 
                                                                           where Scenario_Id__c=:scenarioId 
                                                                           AND Component_Type_Master__r.Component_Definition__c ='CPGAggregation'];
        
        //Get BR id of aggreagtion rules
        List<Business_Rules__c> lstAggregationBusinessRules= [SELECT Id,Name,ScenarioRuleInstanceDetails__c,
                                                              Aggregate_Level__c FROM Business_Rules__c 
                                                              WHERE ScenarioRuleInstanceDetails__c  in :lstscenarioRuleInstance];
        //Map of aggregate sceRuleInstanceId & aggregateLevel
        map<id, string> mapAggregationLevel = new map<id, string>();
        
        //Map of aggregate aggregateLevel & sceRuleInstanceId 
        map<string, id> mapAggLvlSceRuleInstanceId = new map<string, id>();
        for(Business_Rules__c rule : lstAggregationBusinessRules){
            mapAggregationLevel.put(rule.ScenarioRuleInstanceDetails__c, rule.Aggregate_Level__c);
            mapAggLvlSceRuleInstanceId.put(rule.Aggregate_Level__c,rule.ScenarioRuleInstanceDetails__c);
        }
        //Map of aggregate sceRuleInstanceId & guardrail execution seq
        map<id,integer> mapExecSeqs=new map<id,integer>();
        set<Integer> aggScenraioIId= new set<Integer>();
        for(Scenario_Rule_Instance_Details__c sceRuleInstance:lstscenarioRuleInstance){
            mapExecSeqs.put(sceRuleInstance.id,integer.valueOf(sceRuleInstance.Exec_Seq__c)+2);
            aggScenraioIId.add(integer.valueOf(sceRuleInstance.Exec_Seq__c)+2);
        }
        system.debug('aggScenraioIId-->'+aggScenraioIId);
        //map guardrail execution seq & aggregateLevel
        map<Integer,string> mapAggLevelScenarioId = new map<Integer,string>();
        for(Id aggScenraioId : mapAggregationLevel.keySet()){
            mapAggLevelScenarioId.put(mapExecSeqs.get(aggScenraioId),mapAggregationLevel.get(aggScenraioId));
        }
        
        //Get scenrioInstance id of guard rail
        List<Scenario_Rule_Instance_Details__c> lstConstraintSceRuleInstance = [SELECT ComponentTypeLabel__c,Exec_Seq__c,Id 
                                                                                FROM Scenario_Rule_Instance_Details__c 
                                                                                where Scenario_Id__c= :scenarioId 
                                                                                AND Exec_Seq__c in: aggScenraioIId];
        // map of guardrail scenarioInstid and executionSeq
        map<Integer,Id> sceRuleInstanceIds=new map<Integer,Id>();
        set<Id> setGuardRailSId = new set<id>();
        for(Scenario_Rule_Instance_Details__c sceRuleInstance:lstConstraintSceRuleInstance){
            sceRuleInstanceIds.put(Integer.valueOf(sceRuleInstance.Exec_Seq__c),sceRuleInstance.Id);
            setGuardRailSId.add(sceRuleInstance.Id);
        }
        //map of guardrail scenarioInstid & aggLevel
        map<ID,String> mapAggGuradRail = new map<ID,String>();
        for(Integer exeSeq : mapAggLevelScenarioId.keySet()){
            mapAggGuradRail.put(sceRuleInstanceIds.get(exeSeq),mapAggLevelScenarioId.get(exeSeq));
        }
        
        //Get BR of all guardRails
        List<Business_Rules__c> lstBusinessRules= [SELECT Business_Rule_Type_Master__r.Name,Id,Name,
                                                   Parent_Rule_Id__c,Prioritization_Order__c,
                                                   ScenarioRuleInstanceDetails__c, Scenario__c  
                                                   FROM Business_Rules__c 
                                                   WHERE ScenarioRuleInstanceDetails__c in :setGuardRailSId];
        System.debug('lstBusinessRules-->'+lstBusinessRules);
        
        
        set<Id> sbusinessRuleIds = new set<Id>();
        for(Business_Rules__c obj:lstBusinessRules){
            sbusinessRuleIds.add(obj.Id);            
        }
        System.debug('sbusinessRuleIds-->'+sbusinessRuleIds);
        //Get derived data of GuardRails'BRID
        List<Category_Derived_Variable_Metadata__c> lstCategoryderivedVarMetadata =[SELECT Business_Rule_Id__c,Business_Rule_Id__r.ScenarioRuleInstanceDetails__c,
                                                                                    Category_Derived_Variable_Metadata_ExtId__c,
                                                                                    DataType__c,Field_Name__c,Id,
                                                                                    Permitted_Values__c,Value_Type__c 
                                                                                    FROM Category_Derived_Variable_Metadata__c 
                                                                                    where Business_Rule_Id__c IN :sbusinessRuleIds 
                                                                                    AND Value_Type__c ='Discrete'];
        //map aggregateLvl & GuardRails BusinessRules
        map<String, list<Category_Derived_Variable_Metadata__c>> mapAggLvlGuardRail = 
            new map<String, list<Category_Derived_Variable_Metadata__c>>();
        for(Category_Derived_Variable_Metadata__c metaData:lstCategoryderivedVarMetadata){
            Id sInstId=metaData.Business_Rule_Id__r.ScenarioRuleInstanceDetails__c;
            String aggLevel= mapAggGuradRail.get(sInstId);
            
            if(mapAggLvlGuardRail.containsKey(aggLevel)){
                List<Category_Derived_Variable_Metadata__c> lstMetaData= mapAggLvlGuardRail.get(aggLevel);
                lstMetaData.add(metaData);
                mapAggLvlGuardRail.put(aggLevel, lstMetaData);   
            }
            else
                mapAggLvlGuardRail.put(aggLevel, new List<Category_Derived_Variable_Metadata__c> {metaData});
            
        }
        system.debug('mapAggLvlGuardRail: '+mapAggLvlGuardRail);
        List<BRCallBalancingParamsWrapper> lstCallBalancingParamsWrapper=new List<BRCallBalancingParamsWrapper>();
        for(string aggLvl : mapAggLvlGuardRail.keySet()){
            lstCallBalancingParamsWrapper.add(new BRCallBalancingParamsWrapper(aggLvl,mapAggLvlSceRuleInstanceId.get(aggLvl),mapAggLvlGuardRail.get(aggLvl)));
            // lstCallBalancingParamsWrapper.add(new BRCallBalancingParamsWrapper(aggLvl,mapAggLvlGuardRail.get(aggLvl)));
        }
        system.debug('lstCallBalancingParamsWrapper--->'+lstCallBalancingParamsWrapper);
        return lstCallBalancingParamsWrapper;
    }
    
    @AuraEnabled
    public static CPGConstants.Response saveCallBalanceConfigRulesServer(string arrConfigCallBalancingMap,string arrCallBalancingConstraintDetails,string arrConfigCallBalancingConstraintRuleMap,string scenarioInstanceDetailsId){
        CPGConstants.Response objResponse=new CPGConstants.Response();
        Savepoint sp = Database.setSavepoint(); 
        try{
            system.debug('arrConfigCallBalancingMap-->'+arrConfigCallBalancingMap); 
            system.debug('arrCallBalancingConstraintDetails-->'+arrCallBalancingConstraintDetails);
            system.debug('arrConfigCallBalancingConstraintRuleMap-->'+arrConfigCallBalancingConstraintRuleMap);
            List<Config_Call_Balancing_Map__c> lstConfigCallBalancingMap=  (List<Config_Call_Balancing_Map__c>) JSON.deserialize(arrConfigCallBalancingMap, List<Config_Call_Balancing_Map__c>.class);  
            system.debug('lstConfigCallBalancingMap-->'+lstConfigCallBalancingMap);
            List<CallBalancingConstraintDetailDeserializeType> lstCallBalancingConstraintDetails=  (List<CallBalancingConstraintDetailDeserializeType>) JSON.deserialize(arrCallBalancingConstraintDetails, List<CallBalancingConstraintDetailDeserializeType>.class);   
            system.debug('lstCallBalancingConstraintDetails-->'+lstCallBalancingConstraintDetails);
            List<List<CallBalancingConstraintRuleMapDeserializeType>> lstCallBalancingConstraintRuleMaps=  (List<List<CallBalancingConstraintRuleMapDeserializeType>>) JSON.deserialize(arrConfigCallBalancingConstraintRuleMap, List<List<CallBalancingConstraintRuleMapDeserializeType>>.class);   
            system.debug('lstCallBalancingConstraintRuleMaps-->'+lstCallBalancingConstraintRuleMaps);
            system.debug('scenarioInstanceDetailsId-->'+scenarioInstanceDetailsId);
            scenarioId= CPGUtility.GetScenarioId(scenarioInstanceDetailsId);
            system.debug('scenarioId-->'+scenarioId);
            SaveConfigCallBalancingMap(lstConfigCallBalancingMap,lstCallBalancingConstraintDetails
                                       ,lstCallBalancingConstraintRuleMaps);
            objResponse.status=CPGConstants.SUCCESS;
            objResponse.msg=CPGConstants.SAVEDMSG;
        }
        catch(Exception ex){
            System.debug('exception-->'+ex.getMessage());
            Database.rollback(sp);
            //CPGUtility.InsertErrorLog(ex.getMessage(),ex.getStackTraceString());
            objResponse.status=CPGConstants.ERROR;
            objResponse.msg=ex.getMessage();
            system.debug('stackTrace-->'+ex.getStackTraceString());
        }
        
        // SaveConfigCallBalancingConstraintDetails(arrCallBalancingConstraintDetails);
        //SaveConfigCallBalancingConstraintRuleMap(arrConfigCallBalancingConstraintRuleMap);  
        return objResponse;                                                    
    }
    
    private static void SaveConfigCallBalancingMap(List<Config_Call_Balancing_Map__c> lstConfigCallBalancingMap,List<CallBalancingConstraintDetailDeserializeType> lstCallBalancingConstraintDetails,List<List<CallBalancingConstraintRuleMapDeserializeType>> lstCallBalancingConstraintRuleMaps){
        List<Id> lstCallBalanceMapIds=new List<Id>();
        List<Config_Call_Balancing_Map__c> lstConfigCallBalancingMapsToInsert=new List<Config_Call_Balancing_Map__c>(); 
        List<Config_Call_Balancing_Map__c> lstConfigCallBalancingMapsToUpdate=new List<Config_Call_Balancing_Map__c>(); 
        for(Config_Call_Balancing_Map__c configCallBalancingMap:lstConfigCallBalancingMap){
            system.debug('configCallBalancingMap'+configCallBalancingMap);
            system.debug('configCallBalancingMap.Id'+configCallBalancingMap.Id);
            String configId = string.valueOf(configCallBalancingMap.Id);
            configCallBalancingMap.Scenario__c=scenarioId;
            if(configId=='' || configId==null){
                //insert configCallBalancingMap;
                
                lstConfigCallBalancingMapsToInsert.add(configCallBalancingMap);
            }
            else{
                lstConfigCallBalancingMapsToUpdate.add(configCallBalancingMap);
            }
            system.debug('lstConfigCallBalancingMapsToInsert-->'+lstConfigCallBalancingMapsToInsert);
            system.debug('lstConfigCallBalancingMapsToUpdate-->'+lstConfigCallBalancingMapsToUpdate);
            
            
            
        }
        insert lstConfigCallBalancingMapsToInsert;
        update lstConfigCallBalancingMapsToUpdate;
        for(Config_Call_Balancing_Map__c configCallBalancingMap:lstConfigCallBalancingMapsToInsert){
            lstCallBalanceMapIds.add(configCallBalancingMap.id);
        }
        for(Config_Call_Balancing_Map__c configCallBalancingMap:lstConfigCallBalancingMapsToUpdate){
            lstCallBalanceMapIds.add(configCallBalancingMap.id);
        }
        
        SaveConfigCallBalancingConstraintDetails(lstCallBalanceMapIds,lstCallBalancingConstraintDetails
                                                 ,lstCallBalancingConstraintRuleMaps);
        
    }
    
    private static void  SaveConfigCallBalancingConstraintDetails(List<Id> lstCallBalanceMapIds,List<CallBalancingConstraintDetailDeserializeType> lstCallBalancingConstraintDetails,List<List<CallBalancingConstraintRuleMapDeserializeType>> lstCallBalancingConstraintRuleMaps){
        
        List<Id> lstDeletedConstraintDetailIds= new List<Id>();
        List<Config_Call_Balancing_Constraint_Detail__c> lstConstraintDetails=[select Id from Config_Call_Balancing_Constraint_Detail__c where Config_Call_Balancing_Map_Id__c in :lstCallBalanceMapIds];
        for(Config_Call_Balancing_Constraint_Detail__c constraintDetails:lstConstraintDetails){
            lstDeletedConstraintDetailIds.add(constraintDetails.Id);
        }
        delete lstConstraintDetails;
        List<Config_Call_Balancing_Constraint_Detail__c> lstInsertConstraintDetails
            =new List<Config_Call_Balancing_Constraint_Detail__c>();
        
        for(integer ctr=0;ctr<lstCallBalanceMapIds.size();ctr++){
            List<Config_Call_Balancing_Constraint_Detail__c> lstConfigCallBalancingConstraintDetail
                =lstCallBalancingConstraintDetails[ctr].CallBalancingConstraintDetails;
            for(Config_Call_Balancing_Constraint_Detail__c ConfigCallBalancingConstraintDetail:lstConfigCallBalancingConstraintDetail){
                ConfigCallBalancingConstraintDetail.Config_Call_Balancing_Map_Id__c=lstCallBalanceMapIds[ctr];
                ConfigCallBalancingConstraintDetail.Scenario__c=scenarioId;
                lstInsertConstraintDetails.add(ConfigCallBalancingConstraintDetail);
            }    
        }
        system.debug('lstInsertConstraintDetails-->'+lstInsertConstraintDetails);                                                              
        insert lstInsertConstraintDetails;
        List<Id> lstInsertedConstraintDetailIds= new List<Id>();
        for(Config_Call_Balancing_Constraint_Detail__c constraintDetail:lstInsertConstraintDetails){
            lstInsertedConstraintDetailIds.add(constraintDetail.Id);
        }
        SaveConfigCallBalancingConstraintRuleMapDetails(lstCallBalanceMapIds,lstInsertedConstraintDetailIds,lstDeletedConstraintDetailIds,lstCallBalancingConstraintRuleMaps);
        
    }
    
    private static void SaveConfigCallBalancingConstraintRuleMapDetails(List<Id> lstCallBalanceMapIds,List<Id> lstInsertedConstraintDetailIds,List<Id> lstDeletedConstraintDetailIds,List<List<CallBalancingConstraintRuleMapDeserializeType>> lstCallBalancingConstraintRuleMaps ){
        delete[select Id from Config_Call_Balancing_Constraint_RuleMap__c where Config_Call_Balancing_Constraint_Detail__r.Id in :lstDeletedConstraintDetailIds];
        List<Config_Call_Balancing_Constraint_RuleMap__c> insertedConfigCallBalancingConstraintRuleMap=
            new List<Config_Call_Balancing_Constraint_RuleMap__c>();
        for(integer ctrCallBalanceMapIds=0;ctrCallBalanceMapIds<lstCallBalanceMapIds.size();ctrCallBalanceMapIds++){
            List<CallBalancingConstraintRuleMapDeserializeType> CallBalancingConstraintRuleMapDeserializeType=
                lstCallBalancingConstraintRuleMaps[ctrCallBalanceMapIds];
            if(CallBalancingConstraintRuleMapDeserializeType.size()>0 ){
                for(integer ctrInsertedConstraintDetailIds=0;ctrInsertedConstraintDetailIds<lstInsertedConstraintDetailIds.size();ctrInsertedConstraintDetailIds++){
                    if(CallBalancingConstraintRuleMapDeserializeType.size()>ctrInsertedConstraintDetailIds){
                            List<Config_Call_Balancing_Constraint_RuleMap__c> lstConfigCallBalancingConstraintRuleMap=
                           CallBalancingConstraintRuleMapDeserializeType[ctrInsertedConstraintDetailIds].CallBalancingConstraintRuleMaps;
                        for(Config_Call_Balancing_Constraint_RuleMap__c ConfigCallBalancingConstraintRuleMap:lstConfigCallBalancingConstraintRuleMap){
                          ConfigCallBalancingConstraintRuleMap.Config_Call_Balancing_Constraint_Detail__c=lstInsertedConstraintDetailIds[ctrInsertedConstraintDetailIds];
                          ConfigCallBalancingConstraintRuleMap.Scenario__c=scenarioId;
                            insertedConfigCallBalancingConstraintRuleMap.add(ConfigCallBalancingConstraintRuleMap);
                       }
                    }
                    
             }  
           }
            
        }
        system.debug('insertedConfigCallBalancingConstraintRuleMap-->'+insertedConfigCallBalancingConstraintRuleMap);
        insert insertedConfigCallBalancingConstraintRuleMap;
    }
    
    public class CallBalancingConstraintDetailDeserializeType{
        
        public List<Config_Call_Balancing_Constraint_Detail__c> CallBalancingConstraintDetails{get;set;}
    }
    public class CallBalancingConstraintRuleMapDeserializeType{
       // public integer configCallBalancingConstraintDetailIndex{get;set;}
        
        public List<Config_Call_Balancing_Constraint_RuleMap__c> CallBalancingConstraintRuleMaps{get;set;}
    }

    @AuraEnabled
    public static ConstraintValuesWrapper fillConstraintWrapper(String ScenarioRuleInstanceId){
        string wrapper='{';
        system.debug('ScenarioRuleInstanceId-->'+ScenarioRuleInstanceId);
        List<ConstraintValuesWrapper> lstConstraintValueWrapper = new  List<ConstraintValuesWrapper>();
        List<Config_Call_Balancing_Map__c> lstConfigCallBalancingMap = [SELECT Aggregate_Level__c,Constraint_1__c
                                                                        ,Constraint_2__c,Id,Scenario_Rule_Instance_Details__c 
                                                                        FROM Config_Call_Balancing_Map__c 
                                                                        WHERE Scenario_Rule_Instance_Details__c 
                                                                        =: ScenarioRuleInstanceId];
        wrapper=wrapper+'\"lstConfigCallBalancingMap\":'+JSON.serialize(lstConfigCallBalancingMap)+',';
       
        set<Id> sIds = new set<Id>();
        for(Config_Call_Balancing_Map__c obj:lstConfigCallBalancingMap){
            sIds.add(obj.Id);
        }
        system.debug('sIds-->'+sIds);
        List<Config_Call_Balancing_Constraint_Detail__c> lstConfigCallBalConstraintdetail = [SELECT Id
                                                                                             ,Config_Call_Balancing_Map_Id__c
                                                                                             ,Constraint_1_Goal__c
                                                                                             ,Constraint_2_Goal__c
                                                                                             ,Constraint_1_Value__c
                                                                                             ,Constraint_2_Value__c 
                                                                    FROM Config_Call_Balancing_Constraint_Detail__c 
                                                                    WHERE Config_Call_Balancing_Map_Id__c IN :sIds];
        wrapper=wrapper+'\"lstConfigCallBalConstraintdetail\":'+JSON.serialize(lstConfigCallBalConstraintdetail)+',';
         set<Id> sConstraintDetailsIds = new set<Id>();
        for(Config_Call_Balancing_Constraint_Detail__c obj:lstConfigCallBalConstraintdetail){
            sConstraintDetailsIds.add(obj.Id);
        }
        List<Config_Call_Balancing_Constraint_RuleMap__c> lstConstraintRuleMap=[select Id,name,Exec_Seq__c,Config_Call_Balancing_Constraint_Detail__c
                                                                                ,Business_Rules__c,Business_Rules__r.Name
                                                                              from Config_Call_Balancing_Constraint_RuleMap__c
                                                                              where Config_Call_Balancing_Constraint_Detail__c 
                                                                                in  :sConstraintDetailsIds];
        
        ConstraintValuesWrapper constraintwrapper=new ConstraintValuesWrapper(lstConfigCallBalancingMap
                                                                    ,lstConfigCallBalConstraintdetail
                                                                    ,lstConstraintRuleMap);
        //wrapper=wrapper+'\"lstConstraintRuleMap\":'+JSON.serialize(lstConstraintRuleMap)+'}';
        
        
       /* for(Config_Call_Balancing_Map__c obj:lstConfigCallBalancingMap){
            lstConstraintValueWrapper.add(new ConstraintValuesWrapper(obj.Constraint_1__c,obj.Constraint_2__c
                                                                      ,lstConfigCallBalConstraintdetail,obj.Aggregate_Level__c));
        }*/
        
        return constraintwrapper;
    }
    
   
        /*public BRCallBalancingParamsWrapper(string aggrLevel,List<Category_Derived_Variable_Metadata__c> lstCatDerivedVariableMetadata){
this.aggrLevel=aggrLevel;
this.lstCatDerivedVariableMetadata=lstCatDerivedVariableMetadata;
}*/
        

    
    
    /* public  class ConstraintValuesWrapper {
@AuraEnabled
public string constraint1{get;set;}
@AuraEnabled
public string constraint2{get;set;}
@AuraEnabled
public string level{get;set;}
@AuraEnabled
public List<Config_Call_Balancing_Constraint_Detail__c> constraintValues{get;set;}

public ConstraintValuesWrapper(string constraint1,string constraint2,List<Config_Call_Balancing_Constraint_Detail__c> constraintValues,string level){
this.constraint1=constraint1;
this.constraint2=constraint2;
this.constraintValues = constraintValues;
this.level =level;
}
}*/
    public class ConstraintValuesWrapper{
        @AuraEnabled
        public List<Config_Call_Balancing_Map__c> lstConfigCallBalancingMap{get;set;} 
         @AuraEnabled
        public List<Config_Call_Balancing_Constraint_Detail__c> lstConfigCallBalancingConstraintDetails{get;set;} 
         @AuraEnabled
        public List<Config_Call_Balancing_Constraint_RuleMap__c> lstConfigCallBalancingConstraintRuleMap{get;set;} 
        
        public ConstraintValuesWrapper(List<Config_Call_Balancing_Map__c> lstConfigCallBalancingMap,
                                       List<Config_Call_Balancing_Constraint_Detail__c> lstConfigCallBalancingConstraintDetails,
                                       List<Config_Call_Balancing_Constraint_RuleMap__c> lstConfigCallBalancingConstraintRuleMap){
          this.lstConfigCallBalancingMap=lstConfigCallBalancingMap; 
          this.lstConfigCallBalancingConstraintDetails=lstConfigCallBalancingConstraintDetails;
          this.lstConfigCallBalancingConstraintRuleMap=lstConfigCallBalancingConstraintRuleMap;                                           
        }
    }
}