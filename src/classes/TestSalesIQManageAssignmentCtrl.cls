@isTest
private class TestSalesIQManageAssignmentCtrl 
{
    Static Employee__c emp;
    Static Position__c pos;
    Static String namespacePrefix = SalesIQGlobalConstants.NAME_SPACE ;

    static void testData() 
    {
        Team__c team = CallPlanTestDataFactory.createTeam('Base');
        insert team;

        emp = CallPlanTestDataFactory.createEmployee();
        insert emp;

        Employee__c emp2 = CallPlanTestDataFactory.createEmployee();
        insert emp2;

        Team_Instance__c team_instance = CallPlanTestDataFactory.CreateTeamInstance(team,'Zip');
        team_instance.IC_EffstartDate__c = date.today() - 5;
        team_instance.IC_EffEndDate__c = date.today() + 5;
        insert team_instance;

        pos = CallPlanTestDataFactory.createPosition(null,team);
        pos.Team_Instance__c = team_instance.Id;
        insert pos;

        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User usr = TestDataFactory.createUser(p.Id);
        insert usr;

        Change_Request__c cr1 = CallPlanTestDataFactory.createChangeRequest(usr, pos, team_instance,SalesIQGlobalConstants.REQUEST_STATUS_PENDING);
        CR_Employee_Assignment__c cr_EmployeeAssignment = CallPlanTestDataFactory.cr_EmployeeAssignment(cr1,emp,pos);

        Team_Instance_Object_Attribute__c tioa = new Team_Instance_Object_Attribute__c();
        tioa.Attribute_API_Name__c = namespacePrefix+'Manager__r.Name';
        tioa.Attribute_Display_Name__c = 'Manager';
        tioa.Data_Type__c = 'REFERENCE';
        tioa.Display_Column_Order__c = 2;
        tioa.Interface_Name__c = SalesIQGlobalConstants.MANAGE_ASSIGNMENT_EMPLOYEE_DETAILS;
        tioa.isEditable__c = false;
        tioa.isEnabled__c = true;
        tioa.Object_Name__c = namespacePrefix+'Employee__c';
        tioa.Team_Instance__c  = team_instance.Id;
        insert(tioa);

        Team_Instance_Object_Attribute__c tioa1 = new Team_Instance_Object_Attribute__c();
        tioa1.Attribute_API_Name__c = namespacePrefix+'Employee_Type__c';
        tioa1.Attribute_Display_Name__c = 'Employee Type';
        tioa1.Data_Type__c = 'Text';
        tioa1.Display_Column_Order__c = 1;
        tioa1.Interface_Name__c = SalesIQGlobalConstants.MANAGE_ASSIGNMENT_EMPLOYEE_DETAILS;
        tioa1.isEditable__c = false;
        tioa1.isEnabled__c = true;
        tioa1.Object_Name__c = namespacePrefix+'Employee__c';
        tioa1.Team_Instance__c  = team_instance.Id;
        insert(tioa1);

        Team_Instance_Object_Attribute__c tioa2 = new Team_Instance_Object_Attribute__c();
        tioa2.Attribute_API_Name__c = namespacePrefix+'Position__r.'+namespacePrefix+'Client_Position_code__c';
        tioa2.Attribute_Display_Name__c = ' Territory ID';
        tioa2.Data_Type__c = 'Text';
        tioa2.Display_Column_Order__c = 1;
        tioa2.Interface_Name__c = SalesIQGlobalConstants.MANAGE_ASSIGNMENT_EDIT_EMPLOYEE_POPUP;
        tioa2.isEditable__c = false;
        tioa2.isEnabled__c = true;
        tioa2.Object_Name__c = namespacePrefix+'Position_Employee__c';
        tioa2.Team_Instance__c  = team_instance.Id;
        insert(tioa2);

        Team_Instance_Object_Attribute__c tioa3 = new Team_Instance_Object_Attribute__c();
        tioa3.Attribute_API_Name__c = namespacePrefix+'Assignment_Type__c';
        tioa3.Attribute_Display_Name__c = ' Assign. Type';
        tioa3.Data_Type__c = 'Picklist';
        tioa3.Display_Column_Order__c = 2;
        tioa3.Interface_Name__c = SalesIQGlobalConstants.MANAGE_ASSIGNMENT_EDIT_EMPLOYEE_POPUP;
        tioa3.isEditable__c = true;
        tioa3.isEnabled__c = true;
        tioa3.Object_Name__c = namespacePrefix+'Position_Employee__c';
        tioa3.Team_Instance__c = team_instance.Id;
        insert(tioa3);

        Team_Instance_Object_Attribute__c tioa4 = new Team_Instance_Object_Attribute__c();
        tioa4.Attribute_API_Name__c = namespacePrefix+'Effective_End_Date__c';
        tioa4.Attribute_Display_Name__c = 'Assign. End Date';
        tioa4.Data_Type__c = 'Date';
        tioa4.Display_Column_Order__c = 3;
        tioa4.Interface_Name__c = SalesIQGlobalConstants.MANAGE_ASSIGNMENT_EDIT_EMPLOYEE_POPUP;
        tioa4.isEditable__c = true;
        tioa4.isEnabled__c = true;
        tioa4.Object_Name__c = namespacePrefix+'Position_Employee__c';
        tioa4.isRequired__c = true;
        tioa4.Team_Instance__c  = team_instance.Id;
        insert(tioa4);

        Team_Instance_Object_Attribute__c tioa5 = new Team_Instance_Object_Attribute__c();
        tioa5.Attribute_API_Name__c = namespacePrefix+'Effective_Start_Date__c';
        tioa5.Attribute_Display_Name__c = 'Assign. Start Date';
        tioa5.Data_Type__c = 'Date';
        tioa5.Display_Column_Order__c = 4;
        tioa5.Interface_Name__c = SalesIQGlobalConstants.MANAGE_ASSIGNMENT_EDIT_EMPLOYEE_POPUP;
        tioa5.isEditable__c = true;
        tioa5.isEnabled__c = true;
        tioa5.isRequired__c = true;
        tioa5.Object_Name__c = namespacePrefix+'Position_Employee__c';
        tioa5.Team_Instance__c  = team_instance.Id;
        insert(tioa5);

        TotalApproval__c totalApp = new TotalApproval__c(Name='Approvals',No_Of_Approval__c=3);
        insert totalApp; 

        Position_Employee__c posEmp = CallPlanTestDataFactory.createPosEmploye(pos,emp2);

        Position_Employee__c posEmp2 = CallPlanTestDataFactory.createPosEmploye(pos,emp);
        insert new List<Position_Employee__c>{posEmp,posEmp2};

         TriggerContol__c tc = new TriggerContol__c();
        tc.Name = 'SecurityUtil';
        tc.IsStopTrigger__c = true ;
        insert tc;
        
        SalesIQManageAssignmentCtrl.isBusinessRuleRunning(team_instance.Id);



    }
    
    static testMethod void testSaveAssignments()    
    {
        testData();
        test.startTest();
        //SalesIQDetailCardWrapper wrap = SalesIQManageAssignmentCtrl.getRecordData(emp.Id,'Employee__c');
        List<ObjectDetailWrapper> wrap2 = SalesIQManageAssignmentCtrl.getAssignments(emp.Id, SalesIQGlobalConstants.EMPLOYEE_UNIVERSE);
        List<ObjectDetailWrapper> wrap3 = SalesIQManageAssignmentCtrl.addAssignments(new List<String>{pos.Id},emp.Id, SalesIQGlobalConstants.EMPLOYEE_UNIVERSE, 'Primary');
        System.debug('####wrap2' + wrap2);
        for(Integer i = 0;i<wrap3[0].recordDetails.size();i++)
            if(wrap3[0].recordDetails[i].fieldApiName == namespacePrefix+'Effective_End_Date__c')
                wrap3[0].recordDetails[i].fieldValue = String.valueOf(date.today() + 4);
        System.debug('---->>>' + wrap3);
        System.debug(wrap2[0].recordDetails);
        SalesIQManageAssignmentCtrl.getLocaleDateFormats();
        SalesIQManageAssignmentCtrl.getOrgNamespace();
        SalesIQManageAssignmentCtrl.getExternalComponentDetail();
        SalesIQManageAssignmentCtrl.logException('No Error','POSITION UNIVERSE');
        
        
        map<String,list<String>> profileNameMap = new map<String,list<String>>();
        profileNameMap.put('HO',SalesIQGlobalConstants.HO_PROFILE);
        profileNameMap.put('HR',SalesIQGlobalConstants.HR_PROFILE);
        profileNameMap.put('DM',SalesIQGlobalConstants.DM_PROFILE);
        profileNameMap.put('RM',SalesIQGlobalConstants.RM_PROFILE);
        profileNameMap.put('Rep',SalesIQGlobalConstants.REP_PROFILE);
        
        List<String> list1 = SalesIQManageAssignmentCtrl.checkValidations(JSON.serialize(wrap3), SalesIQGlobalConstants.EMPLOYEE_UNIVERSE);
        List<String> list2 = SalesIQManageAssignmentCtrl.saveAssignments(JSON.serialize(wrap3), SalesIQGlobalConstants.EMPLOYEE_UNIVERSE,JSON.serialize(profileNameMap));
        System.debug('###list1' + list1);
        System.debug('###list2' + list2);
        test.stopTest();


    }

    static testMethod void highEndDateValidation()    
    {
        testData();
        test.startTest();
        //SalesIQDetailCardWrapper wrap = SalesIQManageAssignmentCtrl.getRecordData(emp.Id,'Employee__c');
        List<ObjectDetailWrapper> wrap2 = SalesIQManageAssignmentCtrl.getAssignments(emp.Id, SalesIQGlobalConstants.EMPLOYEE_UNIVERSE);
        List<ObjectDetailWrapper> wrap3 = SalesIQManageAssignmentCtrl.addAssignments(new List<String>{pos.Id},emp.Id, SalesIQGlobalConstants.EMPLOYEE_UNIVERSE, 'Primary');
       
        for(Integer i = 0;i<wrap3[0].recordDetails.size();i++)
        {
            if(wrap3[0].recordDetails[i].fieldApiName == namespacePrefix+'Effective_End_Date__c')
                wrap3[0].recordDetails[i].fieldValue = String.valueOf(date.today() + 500);
            if(wrap3[0].recordDetails[i].fieldApiName == namespacePrefix+'Effective_Start_Date__c')
                wrap3[0].recordDetails[i].fieldValue = String.valueOf(date.today() - 500);
        }
        System.debug('###highEndDateValidationwrap' + wrap3);
        List<String> list1 = SalesIQManageAssignmentCtrl.checkValidations(JSON.serialize(wrap3), SalesIQGlobalConstants.EMPLOYEE_UNIVERSE);
        System.debug('###listenddate' + list1);
        test.stopTest();
    }

    static testMethod void noChangeValidation()    
    {
        testData();
        test.startTest();
        //SalesIQDetailCardWrapper wrap = SalesIQManageAssignmentCtrl.getRecordData(emp.Id,'Employee__c');
        List<ObjectDetailWrapper> wrap2 = SalesIQManageAssignmentCtrl.getAssignments(emp.Id, SalesIQGlobalConstants.EMPLOYEE_UNIVERSE);
        List<ObjectDetailWrapper> wrap3 = SalesIQManageAssignmentCtrl.addAssignments(new List<String>{pos.Id},emp.Id, SalesIQGlobalConstants.EMPLOYEE_UNIVERSE, 'Primary');       
        List<String> list1 = SalesIQManageAssignmentCtrl.checkValidations(JSON.serialize(wrap3), SalesIQGlobalConstants.EMPLOYEE_UNIVERSE);
        System.debug('###noChangeValidation' + list1);
        test.stopTest();
    }

    // validation fail
    static testMethod void testStartEnddateValidation()    
    {
        testData();
        test.startTest();
        //SalesIQDetailCardWrapper wrap = SalesIQManageAssignmentCtrl.getRecordData(emp.Id,'Employee__c');
        List<ObjectDetailWrapper> wrap2 = SalesIQManageAssignmentCtrl.getAssignments(emp.Id, SalesIQGlobalConstants.EMPLOYEE_UNIVERSE);
        List<ObjectDetailWrapper> wrap3 = SalesIQManageAssignmentCtrl.addAssignments(new List<String>{pos.Id},emp.Id, SalesIQGlobalConstants.EMPLOYEE_UNIVERSE, 'Primary');   
        for(Integer i = 0;i<wrap3[0].recordDetails.size();i++)
        {
            if(wrap3[0].recordDetails[i].fieldApiName == namespacePrefix+'Effective_End_Date__c')
                wrap3[0].recordDetails[i].fieldValue = String.valueOf(date.today() - 3);
            if(wrap3[0].recordDetails[i].fieldApiName == namespacePrefix+'Effective_Start_Date__c')
                wrap3[0].recordDetails[i].fieldValue = String.valueOf(date.today() + 3);
        }    
        List<String> list1 = SalesIQManageAssignmentCtrl.checkValidations(JSON.serialize(wrap3), SalesIQGlobalConstants.EMPLOYEE_UNIVERSE);
        System.debug('###startenddate' + list1);
        test.stopTest();
    }

    // validation fail
    static testMethod void testRequiredMissingValidation()    
    {
        testData();
        test.startTest();
        //SalesIQDetailCardWrapper wrap = SalesIQManageAssignmentCtrl.getRecordData(emp.Id,'Employee__c');
        List<ObjectDetailWrapper> wrap2 = SalesIQManageAssignmentCtrl.getAssignments(emp.Id, SalesIQGlobalConstants.EMPLOYEE_UNIVERSE);
        List<ObjectDetailWrapper> wrap3 = SalesIQManageAssignmentCtrl.addAssignments(new List<String>{pos.Id},emp.Id, SalesIQGlobalConstants.EMPLOYEE_UNIVERSE, 'Primary');   
        for(Integer i = 0;i<wrap3[0].recordDetails.size();i++)
        {
            if(wrap3[0].recordDetails[i].fieldApiName == namespacePrefix+'Effective_End_Date__c')
                wrap3[0].recordDetails[i].fieldValue = '';
            if(wrap3[0].recordDetails[i].fieldApiName == namespacePrefix+'Effective_Start_Date__c')
                wrap3[0].recordDetails[i].fieldValue = '';
        }
        List<String> list1 = SalesIQManageAssignmentCtrl.checkValidations(JSON.serialize(wrap3), SalesIQGlobalConstants.EMPLOYEE_UNIVERSE);
        System.debug('###requiredMissing' + list1);
        test.stopTest();
    }

    // validation fail
    static testMethod void testEmployeeJoiningDateValidation()    
    {
        testData();

        emp.Original_Hire_Date__c = System.today();
        emp.HR_Termination_Date__c = System.today();
        update emp;

        test.startTest();
        //SalesIQDetailCardWrapper wrap = SalesIQManageAssignmentCtrl.getRecordData(emp.Id,'Employee__c');
        List<ObjectDetailWrapper> wrap2 = SalesIQManageAssignmentCtrl.getAssignments(emp.Id, SalesIQGlobalConstants.EMPLOYEE_UNIVERSE);
        List<ObjectDetailWrapper> wrap3 = SalesIQManageAssignmentCtrl.addAssignments(new List<String>{pos.Id},emp.Id, SalesIQGlobalConstants.EMPLOYEE_UNIVERSE, 'Primary');   
        for(Integer i = 0;i<wrap3[0].recordDetails.size();i++)
        {
            if(wrap3[0].recordDetails[i].fieldApiName == namespacePrefix+'Effective_End_Date__c')
                wrap3[0].recordDetails[i].fieldValue = String.valueOf(date.today() + 30);
            if(wrap3[0].recordDetails[i].fieldApiName == namespacePrefix+'Effective_Start_Date__c')
                wrap3[0].recordDetails[i].fieldValue = String.valueOf(date.today() - 30);
            if(wrap3[0].recordDetails[i].fieldApiName == namespacePrefix+'Assignment_Type__c')
                wrap3[0].recordDetails[i].fieldValue = null;
        }
        List<String> list1 = SalesIQManageAssignmentCtrl.checkValidations(JSON.serialize(wrap3), SalesIQGlobalConstants.EMPLOYEE_UNIVERSE);
        System.debug('###employeeHireDate' + list1);
        test.stopTest();
    }


    static testMethod void getPendingCR()    
    {
        testData();
        test.startTest();
        //SalesIQDetailCardWrapper wrap = SalesIQManageAssignmentCtrl.getRecordData(emp.Id,'Employee__c');
        List<String> pendingCR =  SalesIQManageAssignmentCtrl.getPendingChangeRequests(emp.Id, SalesIQGlobalConstants.EMPLOYEE_UNIVERSE);
        List<String> pendingCR2 =  SalesIQManageAssignmentCtrl.getPendingChangeRequests(pos.Id, SalesIQGlobalConstants.EMPLOYEE_UNIVERSE);
        test.stopTest();
    }


}