@isTest
private class TestSecurityUtil {

	static User setupTestUser(String profileName){
		//username global uniqueness is still enforced in tests 
		//make sure we get something unique to avoid issues with parallel tests
		String uniqueness = DateTime.now()+':'+Math.random()+'axtriasalesiq';
		Profile p = [SELECT id, Name FROM Profile WHERE Name = :profileName];
		User result = new User(
			username=UserInfo.getUserId()+'.'+uniqueness.HashCode()+'@'+UserInfo.getOrganizationId()+'.sfdcOrg',
			alias = 'TSU',
			email='salesiq@example.com',
			emailencodingkey='UTF-8',
			lastname='Testing',
			languagelocalekey='en_US',
			localesidkey='en_US',
			profileid = p.Id,
			timezonesidkey='America/Los_Angeles'
		);
		insert result;
		return result;
	}

	@isTest
	static void testFieldAccessOnReadOnlyProfile() {
		User testUser = setupTestUser('Read Only');
		System.runAs(testUser){
			{
				SecurityUtil.SecurityException ex;
				try{
					SecurityUtil.checkFieldIsInsertable(Account.SObjectType, 'naMe', true);
				} catch(SecurityUtil.SecurityException e) {
					ex = e;
				}

				// If there is no Insert permission on object Account, all fields will loose FLS
				System.assertEquals(ex.getMessage(), String.format(Label.Security_Error_Field_Not_Insertable, new List<String>{'Account', 'Account Name'})) ;
				System.assert(ex instanceof SecurityUtil.FlsException);
			}

			{
				SecurityUtil.SecurityException ex;
				try{
					SecurityUtil.checkFieldIsReadable(Account.SObjectType, 'TyPE', true);
				}catch(SecurityUtil.SecurityException e){
					ex = e;
				}

				System.assertEquals(null, ex) ;
			}

			{
				SecurityUtil.SecurityException ex;
				try{
					SecurityUtil.checkFieldIsReadable(Account.SObjectType, SObjectDescribe.namespacePrefix + 'speciaLity__c', true);
				}catch(SecurityUtil.SecurityException e){
					ex = e;
				}
				
				System.assertEquals(ex.getMessage(), String.format(Label.Security_Error_Field_Not_Readable, new List<String>{'Account', Account.SObjectType.fields.Speciality__c.getDescribe().getLabel()})) ;
				System.assert(ex instanceof SecurityUtil.FlsException);
			}
			{
				SecurityUtil.SecurityException ex;
				try{
					SecurityUtil.checkFieldIsUpdateable(Account.SObjectType, 'Fax', true);
				}catch(SecurityUtil.SecurityException e){
					ex = e;
				}

				System.assertEquals(ex.getMessage(), String.format(Label.Security_Error_Field_Not_Updateable, new List<String>{'Account', 'Account Fax'})) ;
				System.assert(ex instanceof SecurityUtil.FlsException);
			}

			{
				// Invalid field testing
				SecurityUtil.SecurityException ex;
				try{
					SecurityUtil.checkFieldIsReadable(Account.SObjectType, 'Dummy_unknowField', true);
				}catch(SecurityUtil.SecurityException e){
					ex = e;
				}

				System.assertEquals(ex.getMessage(), String.format(Label.Common_Error_Invalid_Field, new List<String>{'Dummy_unknowField', 'Account'})) ;
			}

			SecurityUtil.BYPASS_INTERNAL_FLS_AND_CRUD = true;
			{ 
				//no exceptions, as crud fls is bypassed
				SecurityUtil.checkFieldIsInsertable(Account.SObjectType, 'naMe', true);
				SecurityUtil.checkFieldIsReadable(Account.SObjectType, SObjectDescribe.namespacePrefix+'speciaLIty__c', true);
				SecurityUtil.checkFieldIsUpdateable(Account.SObjectType, 'Fax', true);
			}

		}
	}

	@isTest
	static void testObjectAccessOnReadOnlyProfile() {
		User testUser = setupTestUser('Read Only');
		System.runAs(testUser){
			{
				SecurityUtil.SecurityException ex;
				try{
					SecurityUtil.checkObjectIsUpdateable(Account.SObjectType);
				}catch(SecurityUtil.SecurityException e){
					ex = e;
				}
				System.assertNotEquals(null, ex, 'Read only profile should not be able to insert Account');
				System.assert(ex instanceof SecurityUtil.CrudException);
			}
			{
				SecurityUtil.SecurityException ex;
				try{
					SecurityUtil.checkObjectIsReadable(Account.SObjectType);
				}catch(SecurityUtil.SecurityException e){
					ex = e;
				}
				System.assertEquals(null, ex, 'Read only profile should be able to read Account');
			}
			{
				SecurityUtil.SecurityException ex;
				try{
					SecurityUtil.checkObjectIsUpdateable(Account.SObjectType);
				}catch(SecurityUtil.SecurityException e){
					ex = e;
				}
				System.assertNotEquals(null, ex, 'Read only profile should not be able to update Account');
				System.assert(ex instanceof SecurityUtil.CrudException);
			}
			{
				SecurityUtil.SecurityException ex;
				try{
					SecurityUtil.checkObjectIsDeletable(Account.SObjectType);
				}catch(SecurityUtil.SecurityException e){
					ex = e;
				}
				System.assertNotEquals(null, ex, 'Read only profile should not be able to delete Account');
				System.assert(ex instanceof SecurityUtil.CrudException);
			}
			{
				SecurityUtil.SecurityException ex;
				try{
					SecurityUtil.checkObjectIsReadable(Position__c.SObjectType);
				}catch(SecurityUtil.SecurityException e){
					ex = e;
				}
				System.assertNotEquals(null, ex, 'Read only profile should not be able to read Position');
				System.assert(ex instanceof SecurityUtil.CrudException);
			}

			SecurityUtil.BYPASS_INTERNAL_FLS_AND_CRUD = true;
			{
				SecurityUtil.checkObjectIsInsertable(Account.SObjectType);
				SecurityUtil.checkObjectIsReadable(Account.SObjectType);
				SecurityUtil.checkObjectIsReadable(Position__c.SObjectType);
				SecurityUtil.checkObjectIsUpdateable(Account.SObjectType);
				SecurityUtil.checkObjectIsDeletable(Account.SObjectType);
			}
		}
	}
	
	

	@isTest
	static void testReadOnlyObjectAndFieldAccessOnReadOnlyProfile() {
		User testUser = setupTestUser('Read Only');
		System.runAs(testUser){
			{
				SecurityUtil.SecurityException ex;
				try{
					SecurityUtil.checkInsert(Account.SObjectType, new List<String>{'Name','ParentId','ownerId'}, true);
				}catch(SecurityUtil.SecurityException e){
					ex = e;
				}
				System.assertNotEquals(null, ex, 'Read only profile should not be able to insert Account');
				System.assert(ex instanceof SecurityUtil.CrudException);
			}
			{
				SecurityUtil.SecurityException ex;
				try{
					SecurityUtil.checkRead(Account.SObjectType, new List<String>{'Name','ParentId','ownerId'}, true);
				}catch(SecurityUtil.SecurityException e){
					ex = e;
				}
				System.assertEquals(null, ex, 'Read only profile should be able to read Account');
			}
			{
				SecurityUtil.SecurityException ex;
				try{
					SecurityUtil.checkUpdate(Account.SObjectType, new List<String>{'Name','ParentId','ownerId'}, true);
				}catch(SecurityUtil.SecurityException e){
					ex = e;
				}
				System.assertNotEquals(null, ex, 'Read only profile should not be able to update Account');
				System.assert(ex instanceof SecurityUtil.CrudException);
			}

			SecurityUtil.BYPASS_INTERNAL_FLS_AND_CRUD = true;
			{
				SecurityUtil.checkInsert(Account.SObjectType, new List<String>{'Name','ParentId','ownerId'}, true);
				SecurityUtil.checkRead(Account.SObjectType, new List<String>{'Name','ParentId','ownerId'}, true);
				SecurityUtil.checkUpdate(Account.SObjectType, new List<String>{'Name','ParentId','ownerId'}, true);
			}
		}
	}
}