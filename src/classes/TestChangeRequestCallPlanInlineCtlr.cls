@IsTest
public with sharing class TestChangeRequestCallPlanInlineCtlr {
    
    // DEPRECATED THE BASE CLASS 
    /*
    static testMethod void testCimConfig() {
        
        TriggerContol__c  triggerctrl =  new TriggerContol__c(IsStopTrigger__c=false , Name='ChangeRequestTrigger');
        insert triggerctrl;
        
        Account acc = CallPlanTestDataFactory.CreateAccount('Direct Aligned','11788','Physician');    
        
        insert acc; 
  
        Account acc2 = CallPlanTestDataFactory.CreateAccount('Direct Aligned','11789','Physician');
        
        insert acc2;
        
        Geography__c geo = CallPlanTestDataFactory.createGeography('Standard',null,'11788');
        insert geo;
        
        
        
        
        
        Profile hoProfile = [SELECT Id FROM Profile WHERE Name =: 'System Administrator' limit 1];
        
        User hoUser1 = new User(LastName = 'ho',
                            FirstName='test',
                            Alias = 'test',
                            Email = 'test.ho@test.com',
                            Username = 'test.ho@test.com',
                            ProfileId = hoProfile.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US');
        insert hoUser1;     
                
                
        Team__c AccountBasedTeam = new Team__c(Name='HTN',Type__c='Account');
        insert AccountBasedTeam;
        
        Team_Instance__c AccountBasedTeamInstance = new Team_Instance__c();
        AccountBasedTeamInstance.Name = 'HTN_Q1_2016';
        AccountBasedTeamInstance.Alignment_Period__c = 'HTN_Q1_2016';
        AccountBasedTeamInstance.Team__c = AccountBasedTeam.id;
        AccountBasedTeamInstance.Alignment_Type__c = 'Account';
        AccountBasedTeamInstance.isActiveCycle__c = 'Y';
        insert AccountBasedTeamInstance;    
        
                
        Position__c district = CallPlanTestDataFactory.createPositionParent(null,AccountBasedTeam);
        insert district;
        
        Position_Team_Instance__c districtTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(district, null, AccountBasedTeamInstance);
        districtTeamInstance.X_Max__c=-72.6966429900;
        districtTeamInstance.X_Min__c=-73.9625820000;
        districtTeamInstance.Y_Max__c=40.9666490000;
        districtTeamInstance.Y_Min__c=40.5821279800;
        insert districtTeamInstance;
        
        Position__c territory =  CallPlanTestDataFactory.createPosition(district,AccountBasedTeam);
        territory.Related_Position_Type__c = 'Base';
        insert territory;
        
        Position_Team_Instance__c territoryTeamInstance = CallPlanTestDataFactory.CreatePosTeamInstance(territory, district, AccountBasedTeamInstance);
        territoryTeamInstance.X_Max__c=-72.6966429900;
        territoryTeamInstance.X_Min__c=-73.9625820000;
        territoryTeamInstance.Y_Max__c=40.9666490000;
        territoryTeamInstance.Y_Min__c=40.5821279800;
        insert territoryTeamInstance;
        
        Team_Instance_Account__c teamInsAccount = CallPlanTestDataFactory.createTeamInstanceAccount(acc,AccountBasedTeamInstance);
        insert teamInsAccount;
        
        Position_Account__c SourcepositionAccount = CallPlanTestDataFactory.createPositionAccountPending(acc, territory, AccountBasedTeamInstance, territoryTeamInstance, teamInsAccount);
        insert SourcepositionAccount;
    
    
        Change_Request_Type__c callPlanRequestType = CallPlanTestDataFactory.CreateRequestType(SalesIQGlobalConstants.MOVEMENT_TYPE_CALL_PLAN);     
        insert callPlanRequestType;
        
        list<CIM_Config__c> cimConfigList = CallPlanTestDataFactory.createCIMCOnfig(callPlanRequestType,AccountBasedTeamInstance,'TeamInstanceZeography');
        cimConfigList[0].Name='No of Targets';  
        cimConfigList[1].Name='Total Calls';        
        insert cimConfigList;

        
        list<CIM_Position_Metric_Summary__c> impactSummaryList = CallPlanTestDataFactory.createCIMMetricSummary(cimConfigList,territoryTeamInstance,AccountBasedTeamInstance);
        insert impactSummaryList;
        
        Change_Request__c objChangeRequest = new Change_Request__c();
        objChangeRequest.Approver1__c = hoUser1.id;
        objChangeRequest.Destination_Position__c = territory.id;      
        objChangeRequest.Status__c = 'Pending';
        objChangeRequest.Account_Moved_Id__c=acc.AccountNumber;
        objChangeRequest.Request_Type_Change__c = SalesIQGlobalConstants.MOVEMENT_TYPE_CALL_PLAN;
        objChangeRequest.Team_Instance_ID__c = AccountBasedTeamInstance.id;
        insert objChangeRequest;
        
        list<CR_Call_Plan__c> crCallPlanList = CallPlanTestDataFactory.createCRCallPlan(objChangeRequest,acc,'Pending');
        insert crCallPlanList;
        
        Position_Account_Call_Plan__c positionAccountCallPlan = CallPlanTestDataFactory.CreatePosAccCallPlan(acc,territory,AccountBasedTeamInstance,territoryTeamInstance,teamInsAccount,'No Change');
        insert positionAccountCallPlan;
        
        Position_Account_Call_Plan__c positionAccountCallPlanNaxavar = CallPlanTestDataFactory.CreatePosAccCallPlan(acc,territory,AccountBasedTeamInstance,territoryTeamInstance,teamInsAccount,'No Change');
        positionAccountCallPlanNaxavar.Picklist2_Segment__c='Nexavar';
        insert positionAccountCallPlanNaxavar;
       
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(objChangeRequest);
        ChangeRequestCallPlanInlineCtlr obj = new ChangeRequestCallPlanInlineCtlr(sc);
        test.stopTest();
    
    }
    */
    
}