/************************************************************************************************
    Name        :   AlignmentListViewCtrl.cls
    Description :   Controller for Alignment vf page. 
                    Contains controller methods for Alignment page and Tree & Map components
    author      :   Lagnika Sharma
************************************************************************************************/
public with sharing class AlignmentListViewCtrl{

	/*public string cimConfigMetric {get;set;}
	public String selectedTeamInstance {get;set;}
	public String teamInstanceObjAttrLabelMapJSON{
		get{
			if(selectedTeamInstance != null){
				
				list<Team_Instance_Object_Attribute__c> configList = new list<Team_Instance_Object_Attribute__c>();
				map<String,String> displayNameLabelMap = new map<String,String>();
				set<String> interfaceNameSet = new set<String>{'Zip Movement','Account Movement','Position List View','Impact Account Tab','Impact ZIP Tab'};
				configList = [select Attribute_API_Name__c,Attribute_Display_Name__c from Team_Instance_Object_Attribute__c where Interface_Name__c in: interfaceNameSet and Team_Instance__c =: selectedTeamInstance];
				for(Team_Instance_Object_Attribute__c config : configList){

					displayNameLabelMap.put(config.Attribute_Display_Name__c, AlignmentUtility.getLabelString(config.Attribute_Display_Name__c));
				}
				
				return JSON.serialize(displayNameLabelMap);
			}
			return ''; 
		}
		set;
	}*/ 
	
	/*private String getTeamInstanceObjAttrLabels(String selectedTeamInstance){
		System.debug('2222222222222 : '+selectedTeamInstance);
		list<Team_Instance_Object_Attribute__c> configList = new list<Team_Instance_Object_Attribute__c>();
		map<String,String> displayNameLabelMap = new map<String,String>();
		set<String> interfaceNameSet = new set<String>{'Zip Movement','Account Movement','Position List View'};
		configList = [select Attribute_API_Name__c,Attribute_Display_Name__c from Team_Instance_Object_Attribute__c where Interface_Name__c in: interfaceNameSet and Team_Instance__c =: selectedTeamInstance];
		for(Team_Instance_Object_Attribute__c config : configList){

			displayNameLabelMap.put(config.Attribute_Display_Name__c, AlignmentUtility.getLabelString(config.Attribute_Display_Name__c));
		}
		if(displayNameLabelMap.size() == 0)
		return JSON.serialize(displayNameLabelMap); 

	}*/

	
}