public with sharing class CPGChartsCtrl {
    	
    @AuraEnabled
    public static  string getOrgNamespaceServer(){
       ApexClass classObj=[select NamespacePrefix,Name from ApexClass where Name ='CPGChartsCtrl' limit 1];
       return classObj.NamespacePrefix; 
        
    } 
    
    @AuraEnabled
	public static List<String> getTeamInstanceDetails ()
	{
		set<String> teamset = new set<String>();
		List<Scenario__c> scenarioChartList = [SELECT Team_Name__r.Name FROM Scenario__c];
		
		if(scenarioChartList.size() > 0)
		{
			for(Scenario__c obj:scenarioChartList){
			teamset.add(obj.Team_Name__r.Name);
			}
		}
		
		 System.debug('teamset :::: '+teamset);

		List <String> teamParsedSet = new List<String>();

		for(String s:teamset) {
			teamParsedSet.add(s);
		}

		return teamParsedSet;
	}

	@AuraEnabled
	public static List<Scenario__c> getScenarioInstances(String teamoptions){

		System.debug('teamoptions :::: '+teamoptions);
		Set<Id> steamIds = new Set<Id>();
		List <Scenario__c> teamid = [SELECT Id,Scenario_Name__c,Team_Name__c,Request_Process_Stage__c,Scenario_Stage__c FROM Scenario__c where Team_Name__r.Name =: teamoptions and Request_Process_Stage__c = 'Success'];
		for(Scenario__c Obj:teamid){
			steamIds.add(Obj.Team_Name__c);
		}

		List<Scenario__c> displayscenarioList = [SELECT Id,LastModifiedDate,Scenario_Name__c,Request_Process_Stage__c,Scenario_Stage__c FROM Scenario__c where Team_Name__c IN: steamIds and Request_Process_Stage__c = 'Success'];
		System.debug('displayscenarioList :::::: '+displayscenarioList);
		return displayscenarioList;
	}

	@AuraEnabled
	public static List<Scenario__c> getPreviousScenarioValues(String selectedCurrentScenarioValue,String teamoptions){

		System.debug('selectedCurrentScenarioValue :::: '+selectedCurrentScenarioValue);

		System.debug('teamoptions :::: '+teamoptions);
		Set<Id> pteamIds = new Set<Id>();
		List <Scenario__c> teamid = [SELECT Id,Scenario_Name__c,Team_Name__c,Request_Process_Stage__c,Scenario_Stage__c FROM Scenario__c where Team_Name__r.Name =: teamoptions and Request_Process_Stage__c = 'Success'];
		for(Scenario__c Obj:teamid){
			pteamIds.add(Obj.Team_Name__c);
		}

		
		List <Scenario__c> previousScenarioIdList = [SELECT Id,LastModifiedDate,Scenario_Name__c,Scenario_Stage__c,Request_Process_Stage__c FROM Scenario__c where Id != : selectedCurrentScenarioValue and Team_Name__c IN: pteamIds and Request_Process_Stage__c = 'Success'];
		
		return previousScenarioIdList;
	}

	@AuraEnabled
    public static Map < String,Object > generateVisualizationReport(String url) {
 
        // Instantiate a new http object
        Http h = new Http();
 
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setTimeout(120000);
      //  req.setHeader('Content-Type', 'application/json');
       // req.setHeader('Accept','application/json');
        // Send the request, and return a response
        if(!Test.isRunningTest()){
	        HttpResponse res = h.send(req);
	        System.debug('response:--> ' + res.getBody());
	 
	        // Deserialize the JSON string into collections of primitive data types.
	        Map<String,Object> finalResult = new Map<String,Object>();
	        Map < String,Object > resultsMap = (Map < String, Object > ) JSON.deserializeUntyped(res.getBody());
	        system.debug('resultsMap-->' + resultsMap);
	        return resultsMap;
        }
        else{
        	return null;
        }
    }
    
    @AuraEnabled
    public static ChartsWrapper getInstanceNameAndUrl() {
        string cpgInstanceName =  CPGUtility.getBRMSConfigValues('BRDatabaseName');
        string url=CPGUtility.getType('CPGDiagnosticsUrl');
        //string columns=CPGUtility.getColumns(team);
        return new ChartsWrapper(cpgInstanceName,url);
    }
    
    @AuraEnabled
    public static string getColumns(string team) {
        string columns = CPGUtility.getColumns(team);
        return columns;
    }
    
    public class ChartsWrapper{
        @AuraEnabled
        public  string instancename{get;set;}
        
        @AuraEnabled
        public  string url{get;set;}
        
        
        public ChartsWrapper(string instancename,string url){
            this.instancename=instancename;
            this.url=url;
           
        }
    }
       
}