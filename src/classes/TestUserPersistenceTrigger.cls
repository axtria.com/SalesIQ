@isTest
private class TestUserPersistenceTrigger {
  
    private static testMethod void test() {
        Organization_Master__c org = CallPlanTestDataFactory.createOrganizationMasterRecord();
        insert org;
        Country__c country = CallPlanTestDataFactory.createCountryMasterRecord(org.id);
        insert country;
         Country__c country1 = CallPlanTestDataFactory.createCountryMasterRecord(org.id);
        insert country1;

        User_Persistence__c userPersistance = new User_Persistence__c();
        userPersistance.Workspace_Country__c = country.id;
        userPersistance.Default_Country__c = country.id;
        userPersistance.User__c = UserInfo.getUserId();
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {

            Group newGroup = new Group();
            newGroup.Name = 'sds';
            insert newGroup;
            
            GroupMember gr =new GroupMember();
            gr.GroupId=newGroup.id;
            gr.UserOrGroupId=UserInfo.getUserId();
            insert gr;
            
            Country__Share jobShr  = new Country__Share();
            jobShr.ParentId = country.id;
            jobShr.UserOrGroupId = newGroup.id;
            jobShr.AccessLevel = 'Read';
            jobShr.RowCause = Schema.Country__Share.RowCause.Manual;
            Database.SaveResult sr = Database.insert(jobShr,false);
            test.startTest();
                insert userPersistance;
               
                
                Country__Share jobShr1  = new Country__Share();
                jobShr1.ParentId = country1.id;
                jobShr1.UserOrGroupId = newGroup.id;
                jobShr1.AccessLevel = 'Read';
                jobShr1.RowCause = Schema.Country__Share.RowCause.Manual;
                Database.insert(jobShr1,false);
                
                userPersistance.Default_Country__c=country1.id;
                update userPersistance;
                
                
                
                
            test.stopTest();
        }
    }

}