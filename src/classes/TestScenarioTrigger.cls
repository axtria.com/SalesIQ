/******************************************************************************************************************************************************
 * Name         :   TestScenarioTrigger.cls
 * Description  :   Test class for ScenarioTrigger.
 * Author       :   Lagnika Sharma
 * Created On   :   21 June 2017
******************************************************************************************************************************************************/

@istest
public with sharing class TestScenarioTrigger {

    static testMethod void testScenarioTrigger(){
        try{
        
            list<Profile> hoProfile = [SELECT Id FROM Profile WHERE Name =: SalesIQGlobalConstants.HR_PROFILE limit 1];
            if(hoProfile.size() == 0){
                hoProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1];
            }
            
            User usr = new User(LastName = 'ho',
                                FirstName='test',
                                Alias = 'test',
                                Email = 'test.ho@test.com',
                                Username = 'test.ho@test.com',
                                ProfileId = hoProfile[0].id,
                                TimeZoneSidKey = 'GMT',
                                LanguageLocaleKey = 'en_US',
                                EmailEncodingKey = 'UTF-8',
                                LocaleSidKey = 'en_US');
            insert usr;
            
            ETL_Config__c etlConfig =  CallPlanTestDataFactory.createETLConfig();
            etlConfig.Name = SalesIQGlobalConstants.TALEND_JOB;
            insert etlconfig;
            
            list<ETL_Config__c> etlConfigList = new list<ETL_Config__c>();
            etlConfigList.add(etlConfig);
            
            Workspace__c workspace = CallPlanTestDataFactory.createWorkspace();
            insert workspace;
            
            Team__c team = CallPlanTestDataFactory.createTeam('ZIP');
            insert team;
            
            Team_Instance__c teamInstance = CallPlanTestDataFactory.CreateTeamInstance(team,'ZIP');
            insert teamInstance;
            
           /* Team_Instance__c sourceTeamInstance = CallPlanTestDataFactory.CreateTeamInstance(team,'ZIP');
            sourceTeamInstance.Name = 'HTN_Q3';
            insert sourceTeamInstance;*/
            
            Activity_Log__c log = CallPlanTestDataFactory.createActivityLog();
            insert log;
            
            Position__c territory = new Position__c();
            territory.name = 'USA';
            territory.Client_Territory_Name__c = 'North east';
            territory.Client_Position_Code__c = '1NE30000';
            territory.Client_Territory_Code__c='1NE30000';
            territory.Position_Type__c='territory';
            territory.inactive__c = false;
            territory.RGB__c = '41,210,117';
            territory.Team_iD__c = team.id;
            territory.Hierarchy_Level__c='1';
            territory.Related_Position_Type__c  ='Base';
            territory.Effective_End_Date__c=Date.today().addMonths(50);
            territory.Team_Instance__c =  teamInstance.id;
            insert territory;
            system.debug('##### territory ' + territory);
            
            Employee__c emp = CallPlanTestDataFactory.createEmployee(usr);
            insert emp;
            
            Position_Employee__c posEmp = CallPlanTestDataFactory.createPosEmploye(territory,emp);
            posEmp.Assignment_Type__c = 'Primary';
            insert posEmp;
            
            
            Position__c territory1 = new Position__c();
            territory1.name = 'USA';
            territory1.Client_territory_Name__c = 'North east';
            territory1.Client_Position_Code__c = '1NE30000';
            territory1.Client_territory_Code__c='1NE30000';
            territory1.Position_Type__c='territory';
            territory1.inactive__c = false;
            territory1.RGB__c = '41,210,117';
            territory1.Team_iD__c = team.id;
            territory1.Hierarchy_Level__c='1';
            territory1.Related_Position_Type__c  ='Base';
            territory1.Effective_End_Date__c=Date.today().addMonths(50);
            territory1.Team_Instance__c =  teamInstance.id;
            insert territory1;
            
            Employee__c emp1 = CallPlanTestDataFactory.createEmployee(usr);
            insert emp1;
            
            Position_Employee__c posEmp1 = CallPlanTestDataFactory.createPosEmploye(territory1,emp1);
            posEmp1.Assignment_Type__c = 'Primary';
            insert posEmp1;
            
            
            Scenario__c scenario = CallPlanTestDataFactory.createScenario(workspace, teamInstance);
            scenario.Scenario_Name__c = 'Scenario1';
            scenario.Scenario_Type__c = 'Alignment Scenario';
            scenario.Enable_Affiliation_Based_Alignment__c = false;
            insert scenario;

            scenario.Enable_Affiliation_Based_Alignment__c = true;
            
            List<Scenario_Rule_Instance_Details__c> sridList = new List<Scenario_Rule_Instance_Details__c>();
            Scenario_Rule_Instance_Details__c srid = new Scenario_Rule_Instance_Details__c();
            srid.ComponentTypeLabel__c = 'Affiliation';
            srid.Is_Visible_Summarize__c = true;
            srid.Is_Visible__c = true;
            srid.Scenario_Id__c = scenario.id;
            sridList.add(srid);
            
            Scenario_Rule_Instance_Details__c srid1 = new Scenario_Rule_Instance_Details__c();
            srid1.ComponentTypeLabel__c = 'Affiliation Network';
            srid1.Is_Visible_Summarize__c = true;
            srid1.Is_Visible__c = true;
            srid1.Scenario_Id__c = scenario.id;
            sridList.add(srid1);
            insert sridList;
            update scenario;
            
            scenario.Enable_Affiliation_Based_Alignment__c = false;
            update scenario;
            
            Scenario__c scenarioWithoutTeamInt = CallPlanTestDataFactory.createScenario(workspace, teamInstance);
            scenarioWithoutTeamInt.Scenario_Name__c ='testScenaio';
            scenarioWithoutTeamInt.Team_Instance__c = null;
            scenarioWithoutTeamInt.Scenario_Type__c = 'Alignment Scenario';
            insert scenarioWithoutTeamInt;

            Scenario__c copyScenario = CallPlanTestDataFactory.createScenario(workspace, teamInstance);
            copyScenario.Scenario_Name__c ='testScenaio1';
            copyScenario.Team_Instance__c = null;
            copyScenario.Scenario_Type__c = 'Alignment Scenario';
            insert copyScenario;
            
            Scenario__c scenario2 = CallPlanTestDataFactory.createScenario(workspace, teamInstance);
            scenario2.Scenario_Name__c = 'Scenario2';
            scenario2.Scenario_Type__c = 'Alignment Scenario';
            insert scenario2;
            
            Scenario__c scenario3 = CallPlanTestDataFactory.createScenario(workspace, teamInstance);        
            scenario3.Request_Process_Stage__c = SalesIQGlobalConstants.READY;  
            scenario3.Employee_Assignment__c = false;
            scenario3.Source_Team_Instance__c = teamInstance.id;
            scenario3.Effective_Start_Date__c = system.today() + 10;   
            scenario3.Effective_End_Date__c = system.today() + 20;
            scenario3.Scenario_Name__c = 'scenario3';
            scenario3.Scenario_Type__c = 'Alignment Scenario';
            insert scenario3;  
            
            scenario.Shared__c = true;
            scenario.Scenario_Status__c = SalesIQGlobalConstants.POSITION_GEOGRAPHY_ACTIVE;
            scenario.Scenario_Stage__c = SalesIQGlobalConstants.COLLABORATION;
            scenario.Employee_Assignment__c = true;
            scenario.Request_Process_Stage__c = SalesIQGlobalConstants.INQUIRY_STATUS_INPROGRESS;   

            update scenario;
            
            scenario.Scenario_Stage__c = SalesIQGlobalConstants.DESIGN;
            scenario.Request_Process_Stage__c = SalesIQGlobalConstants.READY;
            scenario.Employee_Assignment__c = false;
            
            update scenario;
            
            scenario.Employee_Assignment__c = false;
            scenario.Scenario_Stage__c = SalesIQGlobalConstants.PUBLISHING;
            
            scenario.Request_Process_Stage__c = SalesIQGlobalConstants.READY;
            update scenario;
            
            scenario.Employee_Assignment__c = true;
            scenario.Scenario_Stage__c = SalesIQGlobalConstants.PUBLISHING;
            scenario.Request_Process_Stage__c = SalesIQGlobalConstants.INQUIRY_STATUS_INPROGRESS;
            
            update scenario;

            System.debug('scenarioWithoutTeamInt --'+scenarioWithoutTeamInt);

            scenarioWithoutTeamInt.Team_Instance__c = teamInstance.id;
            scenarioWithoutTeamInt.Source_Team_Instance__c = teamInstance.id;
            scenarioWithoutTeamInt.Scenario_Stage__c = SalesIQGlobalConstants.DESIGN;
            scenarioWithoutTeamInt.Request_Process_Stage__c = SalesIQGlobalConstants.IN_QUEUE;
            scenarioWithoutTeamInt.Scenario_Source__c = 'File';
            update scenarioWithoutTeamInt;

            
            copyScenario.Team_Instance__c = teamInstance.id;
            copyScenario.Source_Team_Instance__c = teamInstance.id;
            copyScenario.Scenario_Stage__c = SalesIQGlobalConstants.DESIGN;
            copyScenario.Request_Process_Stage__c = SalesIQGlobalConstants.IN_QUEUE;
            copyScenario.Scenario_Source__c = 'Existing Scenario';
            update copyScenario;
            
            scenario2.Scenario_Stage__c = SalesIQGlobalConstants.DESIGN;
            scenario2.Scenario_Status__c = SalesIQGlobalConstants.POSITION_GEOGRAPHY_ACTIVE;
            scenario2.Request_Process_Stage__c = SalesIQGlobalConstants.READY;
            
            update scenario2;

            scenario2.Request_Process_Stage__c = SalesIQGlobalConstants.DELETE_IN_QUEUE;
            update scenario2;

            scenario2.Request_Process_Stage__c = SalesIQGlobalConstants.DELETE_IN_PROGRESS;
            update scenario2;

            scenario2.Request_Process_Stage__c = SalesIQGlobalConstants.DELETE_ERROR_OCCURED;
            update scenario2;

            scenario3.Scenario_Stage__c = SalesIQGlobalConstants.COLLABORATION;
            scenario3.Employee_Assignment__c = true;
            scenario3.Scenario_Status__c = SalesIQGlobalConstants.POSITION_GEOGRAPHY_ACTIVE;
            scenario3.Request_Process_Stage__c = SalesIQGlobalConstants.INQUIRY_STATUS_INPROGRESS;
            update scenario3;
             
            
            Test.setMock(HttpCalloutMock.class, new TalendJobMockGenerator());
        }
        catch(Exception ex){
            system.debug(ex);
        }
    } 

    static testMethod void testAccountDataObject(){
        
        ScenarioTriggerHandler.getAccountDataObjectId();
    }

    static testMethod void testScenarioTriggerCallPlan(){
        try{
        
            list<Profile> hoProfile = [SELECT Id FROM Profile WHERE Name =: SalesIQGlobalConstants.HR_PROFILE limit 1];
            if(hoProfile.size() == 0){
                hoProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1];
            }
            
            User usr = new User(LastName = 'ho',
                                FirstName='test',
                                Alias = 'test',
                                Email = 'test.ho@test.com',
                                Username = 'test.ho@test.com',
                                ProfileId = hoProfile[0].id,
                                TimeZoneSidKey = 'GMT',
                                LanguageLocaleKey = 'en_US',
                                EmailEncodingKey = 'UTF-8',
                                LocaleSidKey = 'en_US');
            insert usr;
            
            ETL_Config__c etlConfig =  CallPlanTestDataFactory.createETLConfig();
            etlConfig.Name = SalesIQGlobalConstants.TALEND_JOB;
            insert etlconfig;
            
            list<ETL_Config__c> etlConfigList = new list<ETL_Config__c>();
            etlConfigList.add(etlConfig);
            
            Workspace__c workspace = CallPlanTestDataFactory.createWorkspace();
            insert workspace;
            
            Team__c team = CallPlanTestDataFactory.createTeam('ZIP');
            insert team;
            
            Team_Instance__c teamInstance = CallPlanTestDataFactory.CreateTeamInstance(team,'ZIP');
            insert teamInstance;
            
            Activity_Log__c log = CallPlanTestDataFactory.createActivityLog();
            insert log;
            
            Position__c territory = new Position__c();
            territory.name = 'USA';
            territory.Client_Territory_Name__c = 'North east';
            territory.Client_Position_Code__c = '1NE30000';
            territory.Client_Territory_Code__c='1NE30000';
            territory.Position_Type__c='territory';
            territory.inactive__c = false;
            territory.RGB__c = '41,210,117';
            territory.Team_iD__c = team.id;
            territory.Hierarchy_Level__c='1';
            territory.Related_Position_Type__c  ='Base';
            territory.Effective_End_Date__c=Date.today().addMonths(50);
            territory.Team_Instance__c =  teamInstance.id;
            insert territory;
            system.debug('##### territory ' + territory);
            
            Employee__c emp = CallPlanTestDataFactory.createEmployee(usr);
            insert emp;
            
            Position_Employee__c posEmp = CallPlanTestDataFactory.createPosEmploye(territory,emp);
            posEmp.Assignment_Type__c = 'Primary';
            insert posEmp;
            
            
            Position__c territory1 = new Position__c();
            territory1.name = 'USA';
            territory1.Client_territory_Name__c = 'North east';
            territory1.Client_Position_Code__c = '1NE30000';
            territory1.Client_territory_Code__c='1NE30000';
            territory1.Position_Type__c='territory';
            territory1.inactive__c = false;
            territory1.RGB__c = '41,210,117';
            territory1.Team_iD__c = team.id;
            territory1.Hierarchy_Level__c='1';
            territory1.Related_Position_Type__c  ='Base';
            territory1.Effective_End_Date__c=Date.today().addMonths(50);
            territory1.Team_Instance__c =  teamInstance.id;
            insert territory1;
            
            Employee__c emp1 = CallPlanTestDataFactory.createEmployee(usr);
            insert emp1;
            
            Position_Employee__c posEmp1 = CallPlanTestDataFactory.createPosEmploye(territory1,emp1);
            posEmp1.Assignment_Type__c = 'Primary';
            insert posEmp1;
            
            
            Scenario__c scenario = CallPlanTestDataFactory.createScenario(workspace, teamInstance);
            scenario.Scenario_Name__c = 'Scenario1';
            scenario.Scenario_Type__c = 'Call Plan Generation';
            insert scenario;

            
            Scenario__c scenarioWithoutTeamInt = CallPlanTestDataFactory.createScenario(workspace, teamInstance);
            scenarioWithoutTeamInt.Scenario_Name__c ='testScenaio';
            scenarioWithoutTeamInt.Team_Instance__c = null;
            scenarioWithoutTeamInt.Scenario_Type__c = 'Call Plan Generation';
            insert scenarioWithoutTeamInt;

            Scenario__c copyScenario = CallPlanTestDataFactory.createScenario(workspace, teamInstance);
            copyScenario.Scenario_Name__c ='testScenaio1';
            copyScenario.Team_Instance__c = null;
            copyScenario.Scenario_Type__c = 'Call Plan Generation';
            insert copyScenario;
            
            Scenario__c scenario2 = CallPlanTestDataFactory.createScenario(workspace, teamInstance);
            scenario2.Scenario_Name__c = 'Scenario2';
            scenario2.Scenario_Type__c = 'Call Plan Generation';
            insert scenario2;
            
            Scenario__c scenario3 = CallPlanTestDataFactory.createScenario(workspace, teamInstance);        
            scenario3.Request_Process_Stage__c = SalesIQGlobalConstants.READY;  
            scenario3.Employee_Assignment__c = false;
            scenario3.Source_Team_Instance__c = teamInstance.id;
            scenario3.Effective_Start_Date__c = system.today() + 10;   
            scenario3.Effective_End_Date__c = system.today() + 20;
            scenario3.Scenario_Name__c = 'scenario3';
            scenario3.Scenario_Type__c = 'Call Plan Generation';
            insert scenario3;  
            
            scenario.Shared__c = true;
            scenario.Scenario_Status__c = SalesIQGlobalConstants.POSITION_GEOGRAPHY_ACTIVE;
            scenario.Scenario_Stage__c = SalesIQGlobalConstants.COLLABORATION;
            scenario.Employee_Assignment__c = true;
            scenario.Request_Process_Stage__c = SalesIQGlobalConstants.INQUIRY_STATUS_INPROGRESS;   

            update scenario;
            
            scenario.Scenario_Stage__c = SalesIQGlobalConstants.DESIGN;
            scenario.Request_Process_Stage__c = SalesIQGlobalConstants.READY;
            scenario.Employee_Assignment__c = false;
            
            update scenario;
            
            scenario.Employee_Assignment__c = false;
            scenario.Scenario_Stage__c = SalesIQGlobalConstants.PUBLISHING;
            scenario.Request_Process_Stage__c = SalesIQGlobalConstants.INQUIRY_STATUS_INPROGRESS;
            update scenario;
            
            scenario.Employee_Assignment__c = true;
            scenario.Scenario_Stage__c = SalesIQGlobalConstants.PUBLISHING;
            scenario.Request_Process_Stage__c = SalesIQGlobalConstants.READY;
            update scenario;

            System.debug('scenarioWithoutTeamInt --'+scenarioWithoutTeamInt);

            scenarioWithoutTeamInt.Team_Instance__c = teamInstance.id;
            scenarioWithoutTeamInt.Source_Team_Instance__c = teamInstance.id;
            scenarioWithoutTeamInt.Scenario_Stage__c = SalesIQGlobalConstants.DESIGN;
            scenarioWithoutTeamInt.Request_Process_Stage__c = SalesIQGlobalConstants.IN_QUEUE;
            scenarioWithoutTeamInt.Scenario_Source__c = 'File';
            update scenarioWithoutTeamInt;

            
            copyScenario.Team_Instance__c = teamInstance.id;
            copyScenario.Source_Team_Instance__c = teamInstance.id;
            copyScenario.Scenario_Stage__c = SalesIQGlobalConstants.DESIGN;
            copyScenario.Request_Process_Stage__c = SalesIQGlobalConstants.IN_QUEUE;
            copyScenario.Scenario_Source__c = 'Existing Scenario';
            update copyScenario;
            
            scenario2.Scenario_Stage__c = SalesIQGlobalConstants.DESIGN;
            scenario2.Scenario_Status__c = SalesIQGlobalConstants.POSITION_GEOGRAPHY_ACTIVE;
            scenario2.Request_Process_Stage__c = SalesIQGlobalConstants.READY;
            
            update scenario2;

            scenario2.Request_Process_Stage__c = SalesIQGlobalConstants.DELETE_IN_QUEUE;
            update scenario2;

            scenario2.Request_Process_Stage__c = SalesIQGlobalConstants.DELETE_IN_PROGRESS;
            update scenario2;

            scenario2.Request_Process_Stage__c = SalesIQGlobalConstants.DELETE_ERROR_OCCURED;
            update scenario2;

            scenario3.Scenario_Stage__c = SalesIQGlobalConstants.COLLABORATION;
            scenario3.Employee_Assignment__c = true;
            scenario3.Scenario_Status__c = SalesIQGlobalConstants.POSITION_GEOGRAPHY_ACTIVE;
            scenario3.Request_Process_Stage__c = SalesIQGlobalConstants.INQUIRY_STATUS_INPROGRESS;
            update scenario3;
             
            
            Test.setMock(HttpCalloutMock.class, new TalendJobMockGenerator());
        }
        catch(Exception ex){
            system.debug(ex);
        }
    } 

    static testMethod void testScenarioTriggerDelete(){
       try{
            
            Workspace__c workspace = CallPlanTestDataFactory.createWorkspace();
            insert workspace;

            Activity_Log__c log = CallPlanTestDataFactory.createActivityLog();
            insert log;

            Scenario__c scenario = new Scenario__c();
            scenario.Scenario_Name__c = 'test Scenario';
            scenario.Workspace__c = workspace.Id;
            scenario.Scenario_Type__c = 'Alignment Scenario';
            insert scenario;

            scenario.Scenario_Stage__c = SalesIQGlobalConstants.COLLABORATION;
            scenario.Scenario_Status__c = SalesIQGlobalConstants.POSITION_GEOGRAPHY_ACTIVE;
            scenario.Request_Process_Stage__c = SalesIQGlobalConstants.READY;
            update scenario;

            delete scenario;

            scenario.Request_Process_Stage__c = SalesIQGlobalConstants.DELETE_IN_QUEUE;
            update scenario;

            scenario.Request_Process_Stage__c = SalesIQGlobalConstants.DELETE_IN_PROGRESS;
            update scenario;



            
        }
        catch(Exception ex){
            system.debug(ex);
        }
       
    }

    static testMethod void testUpdateScenario(){
        Workspace__c workspace = CallPlanTestDataFactory.createWorkspace();
        insert workspace;

        Activity_Log__c log = CallPlanTestDataFactory.createActivityLog();
        insert log;

        Team__c team = CallPlanTestDataFactory.createTeam('ZIP');
        insert team;
        
        Team_Instance__c teamInstance = CallPlanTestDataFactory.CreateTeamInstance(team,'ZIP');
        insert teamInstance;

        Scenario__c scenario = new Scenario__c();
        scenario.Scenario_Name__c = 'test Scenario';
        scenario.Workspace__c = workspace.Id;
        scenario.Scenario_Type__c = 'Alignment Scenario';
        scenario.Team_Instance__c = teamInstance.Id;
        scenario.Team_Name__c = team.Id;
        insert scenario;

        Scenario__c scenario1 = new Scenario__c();
        scenario1.Scenario_Name__c = 'test Scenario';
        scenario1.Workspace__c = workspace.Id;
        scenario1.Scenario_Type__c = 'Alignment Scenario';
        scenario1.Team_Instance__c = teamInstance.Id;
        scenario1.Team_Name__c = team.Id;
        insert scenario1;

        list<Scenario__c> scenarioList = new list<Scenario__c>();
        ScenarioTriggerHandler.updateLatestEmployeeAssignment(scenarioList);
        ScenarioTriggerHandler.UpdateScenarioToInactive(scenarioList);

        ScenarioTriggerHandler.InitiateETLProcessV2(scenario1,'copy');


    }
    
    static testMethod void testCopyUserPosition(){
    // for copyPositionEmployeeBatch when scenario_source =file.
       try{
          list<Profile> hoProfile = [SELECT Id FROM Profile WHERE Name =: SalesIQGlobalConstants.HR_PROFILE limit 1];
        if(hoProfile.size() == 0){
            hoProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' limit 1];
        }
        
        User usr = new User(LastName = 'ho',
                            FirstName='test',
                            Alias = 'test',
                            Email = 'test.ho@test.com',
                            Username = 'test.ho@test.com',
                            ProfileId = hoProfile[0].id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US');
        insert usr;

        Team__c team = CallPlanTestDataFactory.createTeam('ZIP');
        insert team;
        
        Team_Instance__c teamInstance = CallPlanTestDataFactory.CreateTeamInstance(team,'ZIP');
        insert teamInstance;

        Position__c territory = new Position__c();
        territory.name = 'USA';
        territory.Client_Territory_Name__c = 'North east';
        territory.Client_Position_Code__c = '1NE30000';
        territory.Client_Territory_Code__c='1NE30000';
        territory.Position_Type__c='territory';
        territory.inactive__c = false;
        territory.RGB__c = '41,210,117';
        territory.Team_iD__c = team.id;
        territory.Hierarchy_Level__c='1';
        territory.Related_Position_Type__c  ='Base';
        territory.Effective_End_Date__c=Date.today().addMonths(50);
        territory.Team_Instance__c =  teamInstance.id;
        insert territory;
        system.debug('##### territory ' + territory);
            
        Employee__c  emp = CallPlanTestDataFactory.createEmployee(usr);
        insert emp;
        
        Workspace__c workspace = CallPlanTestDataFactory.createWorkspace();
        insert workspace;
        
        

        Position_Employee__c posEmp = CallPlanTestDataFactory.createPosEmploye(territory,emp);
        posEmp.Assignment_Type__c = 'Primary';
        insert posEmp;

        Position__c territory1 = new Position__c();
        territory1.name = 'USA';
        territory1.Client_territory_Name__c = 'North east';
        territory1.Client_Position_Code__c = '1NE30000';
        territory1.Client_territory_Code__c='1NE30000';
        territory1.Position_Type__c='territory';
        territory1.inactive__c = false;
        territory1.RGB__c = '41,210,117';
        territory1.Team_iD__c = team.id;
        territory1.Hierarchy_Level__c='1';
        territory1.Related_Position_Type__c  ='Base';
        territory1.Effective_End_Date__c=Date.today().addMonths(50);
        territory1.Team_Instance__c =  teamInstance.id;
        insert territory1;
        
        Employee__c emp1 = CallPlanTestDataFactory.createEmployee(usr);
        insert emp1;
        
        Position_Employee__c posEmp1 = CallPlanTestDataFactory.createPosEmploye(territory1,emp1);
        posEmp1.Assignment_Type__c = 'Primary';
        insert posEmp1;


        Scenario__c createUserPosScenario = CallPlanTestDataFactory.createScenario(workspace, teamInstance);        
        createUserPosScenario.Request_Process_Stage__c = SalesIQGlobalConstants.READY;  
        createUserPosScenario.Employee_Assignment__c = false;
        createUserPosScenario.Source_Team_Instance__c = teamInstance.id;
        createUserPosScenario.Effective_Start_Date__c = system.today() + 10;   
        createUserPosScenario.Effective_End_Date__c = system.today() + 20;
        createUserPosScenario.Scenario_Name__c = 'scenario3';
        createUserPosScenario.Scenario_Source__c = 'File';
        createUserPosScenario.Scenario_Type__c  = 'Alignment Scenario';
        insert createUserPosScenario;  

        createUserPosScenario.Scenario_Source__c = 'File';
        createUserPosScenario.Scenario_Stage__c = SalesIQGlobalConstants.COLLABORATION;
        createUserPosScenario.Employee_Assignment__c = true;
        createUserPosScenario.Scenario_Status__c = SalesIQGlobalConstants.POSITION_GEOGRAPHY_ACTIVE;
        createUserPosScenario.Request_Process_Stage__c = SalesIQGlobalConstants.INQUIRY_STATUS_INPROGRESS;
        update createUserPosScenario;
        }
        catch(Exception ex){
            system.debug(ex);
        }
    
      }

      static testMethod void testInitScenario() {
        Workspace__c workspace = new Workspace__c(Name = 'test workspace',
                              Workspace_Start_Date__c = system.today().addMonths(-5),
                              Workspace_End_Date__c = system.today().addMonths(5),
                              Workspace_Description__c = 'Test Workspace');
        insert workspace;

        Organization_Master__c org = new Organization_Master__c();
        org.Name = 'North America';
        org.Org_Level__c = 'Region';
        org.Parent_Country_Level__c = true;
        insert org;

        Country__c country = new Country__c();
        country.name ='USA';
        country.Country_Code__c = 'US';
        country.Status__c = 'Active';
        country.Parent_Organization__c = org.id;
        insert country;

        Business_Rule_Template__c template = new Business_Rule_Template__c();
        template.Name = 'Alignment Template';
        template.isUniversal__c = true;
        insert template;

        // ScenarioWrapper scenarioObj;
        test.startTest();
        ScenarioTriggerHandler.insertCustomerBlockWorkspace(country.id);

        // try{
        //     // ScenarioWrapper scenarioObjError = ScenarioCtlr.initScenario('');
        // }catch(Exception e){}
        
        test.stopTest();
        
        // system.assert(scenarioObj != null);
    }
}