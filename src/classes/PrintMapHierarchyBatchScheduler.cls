global class PrintMapHierarchyBatchScheduler implements Schedulable{
        public string id{get;set;}
        public PrintMapHierarchyBatchScheduler(String id1)
        {
            id=id1;
        } 
    global void execute(SchedulableContext SC) {
          Database.executeBatch( new PrintMapHierarchyBatch(id));
    } 
}