public with sharing class SalesIQAddDropTargetCtrl 
{
    @AuraEnabled
    public static String getOrgNamespace() 
    {
        return SalesIQGlobalConstants.NAME_SPACE;        
    }

    @AuraEnabled
    public static boolean isScenarioLock(String scenarioId){
        return [SELECT Is_Locked__c FROM Scenario__c where id =: scenarioId ].Is_Locked__c;
    }

    @AuraEnabled
    Public Static Map<String,String> getModuleOptions(String selectedTeamInstance, String moduleName)
    {
        List<CR_Team_Instance_Config__c> listCRteamInsConfig = [Select Team_Instance__c, Bulk_Edit_Allowed__c, Fixed_Column_From_Left__c, Fixed_Column_From_Right__c, Page_Size__c, Pagination_on__c, Configuration_Value__c from CR_Team_Instance_Config__c where (Team_Instance__c =: selectedTeamInstance or Team_Instance__c = null) and Configuration_Name__c =: SalesIQGlobalConstants.ADD_TARGET_MODULE and Configuration_Type__c =: SalesIQGlobalConstants.DATA_TABLE_CONFIG];

        Map<String, CR_Team_Instance_Config__c> mapCRTeamInsIdANDobj = new Map<String, CR_Team_Instance_Config__c>();
        for(CR_Team_Instance_Config__c crTeamInsObj : listCRteamInsConfig)
        {
            if(crTeamInsObj.Team_Instance__c == selectedTeamInstance)
            {
                mapCRTeamInsIdANDobj.put(crTeamInsObj.Team_Instance__c, crTeamInsObj);
                break;
            }
            else
                mapCRTeamInsIdANDobj.put('global', crTeamInsObj);
        }

        CR_Team_Instance_Config__c crTeamInsObj;
        if(mapCRTeamInsIdANDobj.containsKey(selectedTeamInstance))
            crTeamInsObj= mapCRTeamInsIdANDobj.get(selectedTeamInstance);
        else if(mapCRTeamInsIdANDobj.containsKey('global'))
            crTeamInsObj= mapCRTeamInsIdANDobj.get('global');


        Map<String,String> mapOptions = new Map<String,String>();
        if(crTeamInsObj != null)
        {
            List<String> configValuesList = crTeamInsObj.Configuration_Value__c.split(';');
            for(String configValue:configValuesList)
            {
                mapOptions.put(configValue.split(':')[0],configValue.split(':')[1]);
            }    
        }    

        return mapOptions;
    }
    
    @AuraEnabled
    Public Static String updateTargets(String subInterface, String updatedRecs, List<String> selectedRecords, String selectedPositionId, String selectedTeamInstanceId){

        try{
                Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
                Map<String, Schema.SObjectField> pacpFieldsMap =  globalDescribe.get(SalesIQGlobalConstants.NAME_SPACE+'Position_Account_Call_Plan__c').getDescribe().fields.getMap();
                
                Map<String,Map<String,String>> changedMade = new Map<String, Map<String,String>>();
                if(updatedRecs != '""'){
                    System.debug('--updatedRecs ' + updatedRecs);
                    changedMade = (Map<String,Map<String,String>>)JSON.deserialize(updatedRecs, Map<String,Map<String,String>>.Class);
                }

                if(subInterface == SalesIQGlobalConstants.ELIGIBLE_TARGETS)
                {
                    List<Position_Account_Call_Plan__c> listPACP = new List<Position_Account_Call_Plan__c>();
                    for(String pacpId : selectedRecords){
                        Position_Account_Call_Plan__c pacpObj = new Position_Account_Call_Plan__c();
                        pacpObj.Id = pacpId;
                        pacpObj.isTarget_Updated__c = true;
                        pacpObj.Change_Status__c = SalesIQGlobalConstants.REQUEST_STATUS_SUBMISSION_PENDING;
                        

                        System.debug('--changedMade ' + changedMade);
                        if(changedMade.containsKey(pacpId)){
                            Map<String,String> recordMAP = changedMade.get(pacpId);
                            recordMAP.remove('rowIndex');
                            pacpObj = (Position_Account_Call_Plan__c) SalesIQAddDropTargetCtrl.setFieldAPIvalue((Sobject) pacpObj, recordMAP, SalesIQGlobalConstants.NAME_SPACE+'Position_Account_Call_Plan__c', recordMAP.keyset());
                        }
                    pacpObj.Change_Action_Type__c = SalesIQGlobalConstants.TARGET_ADDED;
                        listPACP.add(pacpObj);
                    }

                    if(listPACP.size() > 0)
                    {
                        update listPACP;
                        String operationType = 'Save';
                        SalesIQUtility.updatePosMetricSummary(selectedTeamInstanceId, selectedPositionId, 'Call_Plan_Change', SalesIQGlobalConstants.REQUEST_STATUS_PENDING, operationType);
                    }
                }
                else if(subInterface == SalesIQGlobalConstants.ELIGIBLE_ACROSS_TEAM_TARGETS)
                {
                    List<String> allFields = new List<String>();
                    allFields.addAll(pacpFieldsMap.keySet());

                    String pacpAllFields = String.join(allFields, ',');
                    List<Position_Account_Call_Plan__c> listPACP = Database.query( 'Select ' + pacpAllFields + ' FROM Position_Account_Call_Plan__c WHERE Id in: selectedRecords');         

                    List<Position_Account_Call_Plan__c> updatedPACPlist = new List<Position_Account_Call_Plan__c>();

                    for(Position_Account_Call_Plan__c pacpObj : listPACP){

                        Position_Account_Call_Plan__c newPACPobj = new Position_Account_Call_Plan__c();

                        if(pacpObj.Position__c == selectedPositionId){
                            pacpObj.isTarget_Updated__c = true;
                            pacpObj.Change_Action_Type__c = SalesIQGlobalConstants.TARGET_ADDED;
                            pacpObj.Change_Status__c = SalesIQGlobalConstants.REQUEST_STATUS_SUBMISSION_PENDING;
                            
                            if(changedMade.containsKey(pacpObj.Id)){
                                Map<String,String> recordMAP = changedMade.get(pacpObj.Id);
                                recordMAP.remove('rowIndex');
                                pacpObj = (Position_Account_Call_Plan__c) SalesIQAddDropTargetCtrl.setFieldAPIvalue((Sobject) pacpObj, recordMAP, SalesIQGlobalConstants.NAME_SPACE+'Position_Account_Call_Plan__c', recordMAP.keyset());
                            }

                            updatedPACPlist.add(pacpObj);
                        }
                        else{
                            newPACPobj = pacpObj.clone(true,true);
                            newPACPobj.isTarget_Updated__c = true;
                            
                            if(changedMade.containsKey(pacpObj.Id)){
                                Map<String,String> recordMAP = changedMade.get(pacpObj.Id);
                                recordMAP.remove('rowIndex');
                                newPACPobj = (Position_Account_Call_Plan__c) SalesIQAddDropTargetCtrl.setFieldAPIvalue((Sobject) newPACPobj, recordMAP, SalesIQGlobalConstants.NAME_SPACE+'Position_Account_Call_Plan__c', recordMAP.keyset());
                            }

                            newPACPobj.Position__c = selectedPositionId;
                            newPACPobj.Position_Team_Instance__c = SalesIQUtility.getPosTeamInsByPosIDsANDteamins(new Set<Id>{selectedPositionId}, selectedTeamInstanceId, '').get(selectedPositionId).Id;
                            newPACPobj.Change_Action_Type__c = SalesIQGlobalConstants.TARGET_ADDED;
                            newPACPobj.Change_Status__c = SalesIQGlobalConstants.REQUEST_STATUS_SUBMISSION_PENDING;

                            updatedPACPlist.add(newPACPobj);
                        }
                    }

                    if(updatedPACPlist.size() > 0)
                    {
                        upsert updatedPACPlist;
                        String operationType = 'Save';

                        SalesIQUtility.updatePosMetricSummary(selectedTeamInstanceId, selectedPositionId, 'Call_Plan_Change', SalesIQGlobalConstants.REQUEST_STATUS_PENDING, operationType);
                    }
                }

                return System.Label.Target_Added;
        }
        catch(Exception e) {
            String message = e.getTypeName() + ': ' + e.getMessage() + '\n\n' + e.getStackTraceString() ;
            AuraHandledException ex = new AuraHandledException(message);
            ex.setMessage(message);
            throw ex;
            //return message;
        }
    	
    }

    public Static SObject setFieldAPIvalue(Sobject sObj, Map<String,String> recordValueMAP, String objectName, set<String> fieldAPIList){
    	Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMAP = globalDescribe.get(objectName).getDescribe().fields.getMap();


        for(String fieldAPI : fieldAPIList){
			if(fieldMAP.get(SalesIQGlobalConstants.NAME_SPACE+fieldAPI).getDescribe().getType() == Schema.DisplayType.STRING)
                sObj.put(fieldAPI,String.valueOf(recordValueMAP.get(fieldAPI))); 
            else if(fieldMAP.get(SalesIQGlobalConstants.NAME_SPACE+fieldAPI).getDescribe().getType() == Schema.DisplayType.INTEGER)
                sObj.put(fieldAPI,Integer.valueOf(recordValueMAP.get(fieldAPI)));
            else if(fieldMAP.get(SalesIQGlobalConstants.NAME_SPACE+fieldAPI).getDescribe().getType() == Schema.DisplayType.DOUBLE)
            	sObj.put(fieldAPI,Decimal.valueOf(recordValueMAP.get(fieldAPI)));
            else if(fieldMAP.get(SalesIQGlobalConstants.NAME_SPACE+fieldAPI).getDescribe().getType() == Schema.DisplayType.BOOLEAN)
                sObj.put(fieldAPI,Boolean.valueOf(recordValueMAP.get(fieldAPI)));  
            else if(fieldMAP.get(SalesIQGlobalConstants.NAME_SPACE+fieldAPI).getDescribe().getType() == Schema.DisplayType.PICKLIST)
                sObj.put(fieldAPI,String.valueOf(recordValueMAP.get(fieldAPI)));
		}

		return sObj;
    }

    @AuraEnabled
    public static Integer submissionPendingRecords(String selectedTeamInstanceId, String selectedPositionId){
        return SalesIQUtility.submissionPendingRecordsPACP(selectedTeamInstanceId, selectedPositionId).size();
    }

    @AuraEnabled
    public static void logException(String message,String module) 
    {
        if(message.length() > 2000)
           message = message.substring(0,2000); 
        SalesIQLogger.logErrorMessage(message, module);
    }
}