public with sharing class BRMJoinCtlr {
    public BRMJoinCtlr() {

    }

    @AuraEnabled
    public static List<Data_Set_Rule_Map__c> getInputOutputDatasources(String scenarioRuleInstanceId) {

          // first element is first data set, 2nd is second dataset and third is output
      List<Data_Set_Rule_Map__c> inputOutputDataSet = new List<Data_Set_Rule_Map__c>{null,null,null};
      List<Data_Set_Rule_Map__c> inputOutputDataSourcesList = [SELECT dataset_id__c,dataset_id__r.Name,ds_type__c,Id,table_name__c,File_Type__c,Table_Display_Name__c FROM Data_Set_Rule_Map__c WHERE scenario_rule_instance_id__c =: scenarioRuleInstanceId ORDER BY Table_Display_Name__c DESC];
  System.debug('inputOutputDataSourcesList :: '+inputOutputDataSourcesList);
  System.debug('inputOutputDataSourcesList size:: '+inputOutputDataSourcesList.size());
      for(Data_Set_Rule_Map__c dsRuleMap : inputOutputDataSourcesList) {
      
        inputOutputDataSet.add(dsRuleMap);
      }
      System.debug('inputOutputDataSet :: '+inputOutputDataSet);
      System.debug('inputOutputDataSet size :: '+inputOutputDataSet.size());
      return inputOutputDataSet;
    }

    @AuraEnabled
    public static List<Data_Set_Column_Detail__c> callDatasourceFields(String selectedId){

      List<Data_Set_Column_Detail__c> fieldsList = [SELECT dataset_id__c,datatype__c,Id,tb_col_nm__c,value_type__c,variable_type__c,ds_col_name__c FROM Data_Set_Column_Detail__c 
      WHERE dataset_id__c =: selectedId Order by ds_col_name__c];

      return fieldsList;
    }

    @AuraEnabled
    public static List<Data_Set_Column_Detail__c> generateexecuteexpression(List<String> picklistValuesList,List<String> datalistArray){

      List<Data_Set_Column_Detail__c> tablenamedetails = [SELECT ds_col_name__c,tb_col_nm__c FROM Data_Set_Column_Detail__c where ds_col_name__c IN: picklistValuesList and dataset_Id__c IN: datalistArray];

      return tablenamedetails;
    }

    @AuraEnabled
    public static List<JoinExpressionWrapper> getJoinExpression(String ruleName,String defaultexpression,list<String> selectedPicklistValues,String firstdatasource,String seconddatasource,String executeexpression){

     list<JoinExpressionWrapper> expressionvalues = CreateJoinExpressionWrapper(ruleName,defaultexpression,selectedPicklistValues,firstdatasource,seconddatasource,executeexpression);
     return expressionvalues;
   }

   public static list<JoinExpressionWrapper> CreateJoinExpressionWrapper(String ruleName,String defaultexpression,List<String> selectedPicklistValues,String firstdatasource,String seconddatasource,String executeexpression)
   {


    List<JoinExpressionWrapper> expressionData = new List<JoinExpressionWrapper>();

    JoinExpressionWrapper dataobj = new JoinExpressionWrapper(ruleName,defaultexpression,selectedPicklistValues,firstdatasource,seconddatasource,executeexpression);

    expressionData.add(dataobj);

    return expressionData;  

  }

  @AuraEnabled
  public static list<JoinFieldsWrapper> getColumns(String scenarioRuleInstanceId,String DS1selectedId,String DS2selectedId)
  {
    list<JoinFieldsWrapper> columns= CreateColumnWrapper(scenarioRuleInstanceId,DS1selectedId,DS2selectedId);
    return columns;        
  }

  public static list<JoinFieldsWrapper> CreateColumnWrapper(String scenarioRuleInstanceId,String DS1selectedId,String DS2selectedId)
  {
    
    List<Data_Set_Column_Detail__c> firstDSfieldsList = [SELECT dataset_id__r.Name,dataset_Id__c,column_id__c,datatype__c,Id, Is_Visible__c,tb_col_nm__c,value_type__c,is_mandatory__c,ds_col_name__c,Table_Data_Type__c,variable_type__c,JoinStatus_Flag__c FROM Data_Set_Column_Detail__c 
    WHERE dataset_id__c =: DS1selectedId ];

    List<Data_Set_Column_Detail__c> secondDSfieldsList = [SELECT dataset_id__r.Name,dataset_Id__c,column_id__c,datatype__c,Id, Is_Visible__c,tb_col_nm__c,value_type__c,is_mandatory__c,ds_col_name__c,Table_Data_Type__c,variable_type__c,JoinStatus_Flag__c FROM Data_Set_Column_Detail__c 
    WHERE dataset_id__c =: DS2selectedId];

    List<Data_Set_Column_Detail__c> cummulativefieldsList = new List<Data_Set_Column_Detail__c>();


    cummulativefieldsList.addAll(firstDSfieldsList);
    cummulativefieldsList.addAll(secondDSfieldsList);

    List<JoinFieldsWrapper> lstColumnData = new List<JoinFieldsWrapper>();

    for(Data_Set_Column_Detail__c n : cummulativefieldsList)
    {
      JoinFieldsWrapper columns=new JoinFieldsWrapper();
      columns.Id = n.Id;
      columns.datasetId = n.dataset_id__c;
      columns.columnId = n.column_id__c;
      columns.tableColumnName=n.tb_col_nm__c;
      columns.datasourceName=n.dataset_id__r.Name;
      columns.colDatatype=n.datatype__c;
      columns.colValuetype=n.value_type__c;
      columns.datasourceColumnName=n.ds_col_name__c;
      columns.tableDataType=n.Table_Data_Type__c;
      columns.variableType=n.variable_type__c;
      columns.statusFlag = n.JoinStatus_Flag__c;
      columns.isVisible = n.Is_Visible__c;
      lstColumnData.add(columns);
    }

    System.debug('lstColumnData :: '+lstColumnData);

    return lstColumnData;  

  }

  @AuraEnabled
  public static List <BR_JoinRule__c> createJoinBusinessRule(String ruleName,String scenarioRuleInstanceId,String scenarioId,String joinFieldsWrapper,String JoinExpressionWrapperList,String jointypeval){

   /* This creates a business rule for join component when the rule does not exist in Business_Rules__c */

   List <Business_Rules__c> joinBusinessRuleList = new List<Business_Rules__c>();
   String BusinessRuleTemplateDetailsId;
   String componentType;

   List<Scenario_Rule_Instance_Details__c> scenRuleInstDetList = [SELECT BRT_Details_Id__c,BusinessRuleTemplateId__c,ComponentTypeLabel__c,
   Component_Type_Master__c,Id,Scenario_Id__c,Status__c FROM Scenario_Rule_Instance_Details__c WHERE 
   Scenario_Id__c =: scenarioId and Id=: scenarioRuleInstanceId limit 1];

   componentType = scenRuleInstDetList[0].Component_Type_Master__c;
   System.debug('BusinessRuleTemplateDetailsId :: '+componentType);

   List<Business_Rule_Type_Master__c> brtmasterList = [SELECT Id FROM Business_Rule_Type_Master__c 
   WHERE Component_Type__c =: componentType limit 1];

   String businessRuleType = brtmasterList[0].Id;

   Business_Rules__c obj = new Business_Rules__c();

   obj.Business_Rule_Type_Master__c = businessRuleType;
   obj.Execution_Sequence__c = 1;
   obj.Is_Derived__c = true;
   obj.Is_Mandatory__c = true;
   obj.Name = ruleName;
   obj.ScenarioRuleInstanceDetails__c = scenarioRuleInstanceId;
   obj.Scenario__c = scenarioId;
   obj.Status__c = '';

   joinBusinessRuleList.add(obj);

   System.debug('joinBusinessRuleList ::: '+joinBusinessRuleList);

   if(joinBusinessRuleList.size() > 0){
      insert joinBusinessRuleList;
   }

   String businessRulesId;

   for(Integer i=0;i<joinBusinessRuleList.size();i++){

      businessRulesId = joinBusinessRuleList[0].Id;
   }
    

  return saveJoinValues(joinFieldsWrapper,JoinExpressionWrapperList,businessRulesId,scenarioId,jointypeval,scenarioRuleInstanceId);

  }

  @AuraEnabled
  public static List <BR_JoinRule__c> saveJoinValues(String joinFieldsWrapper,String JoinExpressionWrapperList,String businessRulesId,String scenarioId,String jointypeval,String scenarioRuleInstanceId){
    system.debug('--saveJoinValues called');

    System.debug(JoinExpressionWrapperList);
    list<JoinFieldsWrapper> FieldsWrapper = (list<JoinFieldsWrapper>)JSON.deserialize(joinFieldsWrapper,list<JoinFieldsWrapper>.class);
    list<JoinExpressionWrapper> ExpressionWrapper = (list<JoinExpressionWrapper>)JSON.deserialize(JoinExpressionWrapperList,list<JoinExpressionWrapper>.class);
    System.debug('ExpressionWrapper deserialized :::: '+ExpressionWrapper);
    List <BR_Join_Rule_Condition__c> joinRuleconditionList = new List<BR_Join_Rule_Condition__c>();
    String firstdatasource;
    String seconddatasource;

  //  businessRulesId = joinBusinessRuleList[0].Id;

    List<String> picklistValuesList = new List<String>();
    List<String> firstsourceList = new List<String>();
    List<String> secondsourceList = new List<String>();
    JoinExpressionWrapper obj = ExpressionWrapper[0];
    System.debug('obj:: '+obj);
    System.debug('obj.selectedPicklistValues ' + obj.selectedPicklistValues[0]);
    Map<String,String> selectedPicklistValuesMap = (Map<String,String>)JSON.deserialize(obj.selectedPicklistValues[0],Map<String,String>.class);
    Map<String,List<String>> datasourcevscolumnIdMap = new Map<String,List<String>>();
    for(String picklistvalues : selectedPicklistValuesMap.keySet())
    {
      if(Math.mod(Integer.valueOf(picklistvalues),2) == 1){
        firstsourceList.add(selectedPicklistValuesMap.get(picklistvalues));
      }
      else{
        secondsourceList.add(selectedPicklistValuesMap.get(picklistvalues));
      }
    }
    datasourcevscolumnIdMap.put(obj.firstdatasource,firstsourceList);
    datasourcevscolumnIdMap.put(obj.seconddatasource,secondsourceList);

    System.debug('datasourcevscolumnIdMap :: '+datasourcevscolumnIdMap);

    Map<String,Id> columnIdMap = new Map<String,Id>();
    System.debug('picklistValuesList::');
    System.debug(picklistValuesList);

    String firstId = obj.firstdatasource;
    String secondId = obj.seconddatasource;
    List<Data_Set_Column_Detail__c>  dummyObj = [Select id, ds_col_name__c,dataset_id__c from Data_Set_Column_Detail__c where (dataset_id__c =:firstId and ds_col_name__c IN: datasourcevscolumnIdMap.get(firstId)) or (dataset_id__c =:secondId and ds_col_name__c IN: datasourcevscolumnIdMap.get(secondId))];

    List<Data_Set_Column_Detail__c> dsColList = new List<Data_Set_Column_Detail__c>();
    Map<String, Data_Set_Column_Detail__c> mapDummy = new Map<String, Data_Set_Column_Detail__c>();
    for(Data_Set_Column_Detail__c obj1 : dummyObj){
      mapDummy.put(obj1.ds_col_name__c+obj1.dataset_id__c, obj1);
    }

    system.debug('mapDummy:: ' + mapDummy);

    for(String strList : datasourcevscolumnIdMap.keySet())
    {
      for(String str : datasourcevscolumnIdMap.get(strList))
      {
        system.debug('-- ' + str + ' -- ' + strList);
        dsColList.add(mapDummy.get(str+strList));
      }

    }
    system.debug('--dsColList ' + dsColList);

    Map<String,Integer> columncount=new Map<String,Integer>();
    for(String key:datasourcevscolumnIdMap.keyset()){
      for(String var:datasourcevscolumnIdMap.get(key)){
       if(columncount.get(key+var)!=null)
        columncount.put(key+var,columncount.get(key+var)+1);
      else
        columncount.put(key+var,1);
    }

  }
  System.debug('columncount  : '+columncount);

  Map<String, List<List<String>>> mapFinalData = new Map<String, List<List<String>>>();
  for(Data_Set_Column_Detail__c objDs : dsColList){
    if(mapFinalData.containsKey(objDs.dataset_id__c)){
      List<List<String>> tempList = mapFinalData.get(objDs.dataset_id__c);
      for(Integer i=1;i<=columncount.get(objDs.dataset_id__c+objDs.ds_col_name__c);i++){
          tempList.add(new List<String>{objDs.Id, objDs.ds_col_name__c});
      }

      mapFinalData.put(objDs.dataset_id__c, tempList);
    }
    else{
      List<List<String>> finalList = new List<List<String>>();

      for(Integer i=1;i<=columncount.get(objDs.dataset_id__c+objDs.ds_col_name__c);i++){

        finalList.add(new List<String>{objDs.Id, objDs.ds_col_name__c});
      }


      mapFinalData.put(objDs.dataset_id__c, finalList);
    }
  }

  system.debug('mapFinalData ' + mapFinalData);
  system.debug('scenarioRuleInstanceId save--->' + scenarioRuleInstanceId);

  List<BR_Join_Rule_Condition__c> brJoinRuleList = new List<BR_Join_Rule_Condition__c>();
  Integer totalFields = mapFinalData.get(firstId).size();

  List<List<String>> outerListd1=mapFinalData.get(firstId);
  List<List<String>> outerListd2=mapFinalData.get(secondId);
  Integer index1=0;
  for(Integer i=0;i<outerListd1.size();i++)
  {
    List<String> list1=outerListd1[i];
    List<String> list2=outerListd2[i];
    Integer z=0;
    for(Integer j=0;j<list1.size()/2;j++)
    {

      BR_Join_Rule_Condition__c brJoinObj = new BR_Join_Rule_Condition__c();
      brJoinObj.Data_Set_1__c =  firstId;
      brJoinObj.Data_Set_2__c = secondId;
      brJoinObj.Data_Set_1_Field__c = list1[z];
      brJoinObj.Data_Set_2_Field__c = list2[z];
      brJoinObj.Business_Rules__c = businessRulesId;
      brJoinObj.Scenario__c = scenarioId;
      brJoinObj.JoinType__c = jointypeval;
      brJoinObj.Operator__c  = '=';
      brJoinObj.Sequence__c = i;

      z=z+2;

      brJoinRuleList.add(brJoinObj);
    }
  }


  System.debug('brJoinRuleList ::::: '+brJoinRuleList);

  insert brJoinRuleList;

  // First table list inserted

  List<BRJoinRuleSelectCondition__c> joinRuleSelectionList = new List<BRJoinRuleSelectCondition__c>();
  Map<String,String> datasetvsTableNameMap = new Map<String,String>();

  List<Data_Set_Rule_Map__c> datasetTableNameList = [SELECT dataset_id__c,table_name__c FROM Data_Set_Rule_Map__c WHERE scenario_rule_instance_id__c =: scenarioRuleInstanceId];

  for(Data_Set_Rule_Map__c nobj : datasetTableNameList){

    datasetvsTableNameMap.put(nobj.dataset_id__c,nobj.table_name__c);
  }

  System.debug('datasetvsTableNameMap ::: '+datasetvsTableNameMap);

  Integer index = 1;
  for(JoinFieldsWrapper fieldwrapobj : FieldsWrapper)
  {
    BRJoinRuleSelectCondition__c brJoinSelectObj = new BRJoinRuleSelectCondition__c();
    index++;
    brJoinSelectObj.Business_Rules__c = businessRulesId;
    brJoinSelectObj.Data_Set_Field__c = fieldwrapobj.Id;
    brJoinSelectObj.Data_Set__c = fieldwrapobj.datasetId;
    brJoinSelectObj.Scenario__c = scenarioId;
    brJoinSelectObj.StatusFlag__c = fieldwrapobj.statusFlag;
    brJoinSelectObj.exec_tbl_name__c = datasetvsTableNameMap.get(fieldwrapobj.datasetId);
    brJoinSelectObj.Sequence__c = index;

    joinRuleSelectionList.add(brJoinSelectObj);

  }
  System.debug('joinRuleSelectionList ::::: '+joinRuleSelectionList);

  insert joinRuleSelectionList;     

  System.debug('Business Rule Id received before insert into Join Rule :: '+businessRulesId);
  String displayexpression = 'SELECT ';
  String executeexpression = 'SELECT ';
  String insertexecexpression = '';

  List<BRJoinRuleSelectCondition__c> selectedColumnsList = [SELECT Base_Business_Rule__c,Business_Rules__c,Data_Set_Field__r.ds_col_name__c,
  Data_Set__c,exec_tbl_name__c,Id,Name,Scenario__c,Sequence__c,StatusFlag__c,Data_Set_Field__r.tb_col_nm__c
  FROM BRJoinRuleSelectCondition__c  where Scenario__c =: scenarioId and StatusFlag__c = true and Business_Rules__c =: businessRulesId];

  for(Integer z=0;z<selectedColumnsList.size();z++){

   displayexpression = displayexpression + selectedColumnsList[z].Data_Set_Field__r.ds_col_name__c + ',';
   executeexpression = executeexpression + datasetvsTableNameMap.get(selectedColumnsList[z].Data_Set__c) +'.'+selectedColumnsList[z].Data_Set_Field__r.tb_col_nm__c + ',';
   insertexecexpression = insertexecexpression + selectedColumnsList[z].Data_Set_Field__r.tb_col_nm__c + ',';
  }

  System.debug('selectedColumnsList size::: '+selectedColumnsList.size());

  displayexpression = displayexpression.removeEnd(',');
  executeexpression = executeexpression.removeEnd(',');
  insertexecexpression = insertexecexpression.removeEnd(',');


  List<BR_JoinRule__c> joinRuleList = new List<BR_JoinRule__c>();

  for(JoinExpressionWrapper joinwrapobj : ExpressionWrapper)
  {

    BR_JoinRule__c brjoinruleobj = new BR_JoinRule__c();        
    brjoinruleobj.Business_Rules__c = businessRulesId;
    brjoinruleobj.Join_Rule_Display_Expression__c = joinwrapobj.defaultexpression;
    brjoinruleobj.Join_Rule_Execute_Expression__c = joinwrapobj.executeexpression;
    brjoinruleobj.Scenario__c = scenarioId;
    brjoinruleobj.Select_Display_Expression__c = displayexpression;
    brjoinruleobj.Select_Execute_Expression__c = executeexpression;
    brjoinruleobj.insert_exec_expr__c = insertexecexpression;

    joinRuleList.add(brjoinruleobj);
  }

  System.debug('joinRuleList ::: '+joinRuleList);

  insert joinRuleList; 

  return joinRuleList;
  }



  @AuraEnabled
  public static List<String> saveUpdatedValues(String joinFieldsWrapper,String JoinExpressionWrapperList,String businessRuleId,String scenarioId,String jointypeval,String scenarioRuleInstanceId,String ruleName, String whereClaus)
  {
    System.debug('inside saveUpdatedValues');

    List<Business_Rules__c> ruleconfiguredList = [SELECT Name FROM Business_Rules__c where Id =: businessRuleId limit 1];

    if(ruleconfiguredList.size() > 0){

      ruleconfiguredList[0].Name = ruleName;
      update ruleconfiguredList;
    }

    System.debug(JoinExpressionWrapperList);
    list<JoinFieldsWrapper> FieldsWrapper = (list<JoinFieldsWrapper>)JSON.deserialize(joinFieldsWrapper,list<JoinFieldsWrapper>.class);
    list<JoinExpressionWrapper> ExpressionWrapper = (list<JoinExpressionWrapper>)JSON.deserialize(JoinExpressionWrapperList,list<JoinExpressionWrapper>.class);
    List <BR_Join_Rule_Condition__c> joinRuleconditionList = new List<BR_Join_Rule_Condition__c>();

    List<String> picklistValuesList = new List<String>();
    List<String> firstsourceList = new List<String>();
    List<String> secondsourceList = new List<String>();
    JoinExpressionWrapper obj = ExpressionWrapper[0];
    Map<String,String> selectedPicklistValuesMap = (Map<String,String>)JSON.deserialize(obj.selectedPicklistValues[0],Map<String,String>.class);
    Map<String,List<String>> datasourcevscolumnIdMap = new Map<String,List<String>>();
    for(String picklistvalues : selectedPicklistValuesMap.keySet())
    {
      if(Math.mod(Integer.valueOf(picklistvalues),2) == 1)
      {
        firstsourceList.add(selectedPicklistValuesMap.get(picklistvalues));
      }
      else
      {
        secondsourceList.add(selectedPicklistValuesMap.get(picklistvalues));
      }
    }
    datasourcevscolumnIdMap.put(obj.firstdatasource,firstsourceList);
    datasourcevscolumnIdMap.put(obj.seconddatasource,secondsourceList);

    System.debug('datasourcevscolumnIdMap :: '+datasourcevscolumnIdMap);

    Map<String,Id> columnIdMap = new Map<String,Id>();

    String firstId = obj.firstdatasource;
    String secondId = obj.seconddatasource;
    system.debug('firstId ' + firstId);
    system.debug('secondId ' + secondId);

    List<Data_Set_Column_Detail__c>  dummyObj = [Select id, ds_col_name__c,dataset_id__c from Data_Set_Column_Detail__c where (dataset_id__c =:firstId and ds_col_name__c IN: datasourcevscolumnIdMap.get(firstId)) or (dataset_id__c =:secondId and ds_col_name__c IN: datasourcevscolumnIdMap.get(secondId))];

    List<Data_Set_Column_Detail__c> dsColList = new List<Data_Set_Column_Detail__c>();
    Map<String, Data_Set_Column_Detail__c> mapDummy = new Map<String, Data_Set_Column_Detail__c>();
    for(Data_Set_Column_Detail__c obj1 : dummyObj){
      mapDummy.put(obj1.ds_col_name__c+obj1.dataset_id__c, obj1);
    }

    for(String strList : datasourcevscolumnIdMap.keySet())
    {
      for(String str : datasourcevscolumnIdMap.get(strList))
      {
        dsColList.add(mapDummy.get(str+strList));
      }

    }
    system.debug('--dsColList ' + dsColList);

    Map<String,Integer> columncount=new Map<String,Integer>();
    for(String key:datasourcevscolumnIdMap.keyset())
    {
      for(String var:datasourcevscolumnIdMap.get(key))
      {
       if(columncount.get(key+var)!=null)
        columncount.put(key+var,columncount.get(key+var)+1);
      else
        columncount.put(key+var,1);
    }

  }

  System.debug('columncount  : '+columncount);

  Map<String, List<List<String>>> mapFinalData = new Map<String, List<List<String>>>();
  for(Data_Set_Column_Detail__c objDs : dsColList)
  {
    if(mapFinalData.containsKey(objDs.dataset_id__c))
    {
      List<List<String>> tempList = mapFinalData.get(objDs.dataset_id__c);

      for(Integer i=1;i<=columncount.get(objDs.dataset_id__c+objDs.ds_col_name__c);i++)
      {
                   // List<String> tempDsList = new List<String>();
                    //tempDsList.addAll();
        tempList.add(new List<String>{objDs.Id, objDs.ds_col_name__c});
      }

      mapFinalData.put(objDs.dataset_id__c, tempList);
    }

    else
    {
      List<List<String>> finalList = new List<List<String>>();

      for(Integer i=1;i<=columncount.get(objDs.dataset_id__c+objDs.ds_col_name__c);i++)
      {
        finalList.add(new List<String>{objDs.Id, objDs.ds_col_name__c});
      }

      mapFinalData.put(objDs.dataset_id__c, finalList);
    }
  }

  system.debug('mapFinalData ' + mapFinalData);
  /* Delete the existing records of the existing business Rule Id and insert new updated records */

  List<BR_Join_Rule_Condition__c> existingjoinRuleList = [SELECT Base_Business_Rule__c,Business_Rules__c,Data_Set_1_Field__c,Data_Set_1__c,
  Data_Set_2_Field__c,Data_Set_2__c,JoinType__c,Name,Operator__c,Scenario__c,Sequence__c
  FROM BR_Join_Rule_Condition__c WHERE Business_Rules__c =: businessRuleId];

  if(existingjoinRuleList.size() > 0){
    delete existingjoinRuleList;
  }

  System.debug('existingjoinRuleList:: '+existingjoinRuleList);
  System.debug('existingjoinRuleList size:: '+existingjoinRuleList.size());

  List<BR_Join_Rule_Condition__c> brJoinRuleList = new List<BR_Join_Rule_Condition__c>();
  Integer totalFields = mapFinalData.get(firstId).size();

  List<List<String>> outerListd1=mapFinalData.get(firstId);
  List<List<String>> outerListd2=mapFinalData.get(secondId);
  Integer index1=0;
  for(Integer i=0;i<outerListd1.size();i++)
  {
    List<String> list1=outerListd1[i];
    List<String> list2=outerListd2[i];
    Integer z=0;
    for(Integer j=0;j<list1.size()/2;j++)
    {

      BR_Join_Rule_Condition__c brJoinObj = new BR_Join_Rule_Condition__c();
      brJoinObj.Data_Set_1__c =  firstId;
      brJoinObj.Data_Set_2__c = secondId;
      brJoinObj.Data_Set_1_Field__c = list1[z];
      brJoinObj.Data_Set_2_Field__c = list2[z];
      brJoinObj.Business_Rules__c = businessRuleId;
      brJoinObj.Scenario__c = scenarioId;
      brJoinObj.JoinType__c = jointypeval;
      brJoinObj.Operator__c  = '=';
      brJoinObj.Sequence__c = i;

      z=z+2;

      brJoinRuleList.add(brJoinObj);
    }
  }


  System.debug('brJoinRuleList ::::: '+brJoinRuleList);

  insert brJoinRuleList;

  List<BRJoinRuleSelectCondition__c> exisjoinruleselectionlist = [SELECT Business_Rules__c,Data_Set_Field__c,Data_Set__c,
  exec_tbl_name__c,Id,Name,Scenario__c,Sequence__c FROM BRJoinRuleSelectCondition__c WHERE Business_Rules__c =: businessRuleId];

  if(exisjoinruleselectionlist.size() > 0)
  {
    delete exisjoinruleselectionlist;
  }

  System.debug('exisjoinruleselectionlist delted from Join Select Object :: '+exisjoinruleselectionlist);
  System.debug('exisjoinruleselectionlist size:: '+exisjoinruleselectionlist.size());


  List<BRJoinRuleSelectCondition__c> joinRuleSelectionList = new List<BRJoinRuleSelectCondition__c>();
  Map<String,String> datasetvsTableNameMap = new Map<String,String>();

  List<Data_Set_Rule_Map__c> datasetTableNameList = [SELECT dataset_id__c,table_name__c FROM Data_Set_Rule_Map__c WHERE scenario_rule_instance_id__c =: scenarioRuleInstanceId];

  for(Data_Set_Rule_Map__c nobj : datasetTableNameList)
  {
    datasetvsTableNameMap.put(nobj.dataset_id__c,nobj.table_name__c);
  }

  System.debug('datasetvsTableNameMap ::: '+datasetvsTableNameMap);

  Integer index = 1;

  for(JoinFieldsWrapper fieldwrapobj : FieldsWrapper)
  {
    BRJoinRuleSelectCondition__c brJoinSelectObj = new BRJoinRuleSelectCondition__c();
    index++;
    brJoinSelectObj.Business_Rules__c = businessRuleId;
    brJoinSelectObj.Data_Set_Field__c = fieldwrapobj.Id;
    brJoinSelectObj.Data_Set__c = fieldwrapobj.datasetId;
    brJoinSelectObj.Scenario__c = scenarioId;
    brJoinSelectObj.StatusFlag__c = fieldwrapobj.statusFlag;
    brJoinSelectObj.exec_tbl_name__c = datasetvsTableNameMap.get(fieldwrapobj.datasetId);
    brJoinSelectObj.Sequence__c = index;

    joinRuleSelectionList.add(brJoinSelectObj);

  }
  System.debug('joinRuleSelectionList ::::: '+joinRuleSelectionList);

  insert joinRuleSelectionList;

  List<BR_JoinRule__c> exisruleList = [SELECT Business_Rules__c,Id,insert_exec_expr__c,Join_Rule_Display_Expression__c,Join_Rule_Execute_Expression__c,Name,Scenario__c,Select_Display_Expression__c,Select_Execute_Expression__c FROM BR_JoinRule__c WHERE Business_Rules__c =: businessRuleId] ;

  if(exisruleList.size() > 0){
    delete exisruleList;
  }

  String displayexpression = 'SELECT ';
  String executeexpression = 'SELECT ';
  String insertexecexpression = '';

  List<BRJoinRuleSelectCondition__c> selectedColumnsList = [SELECT Base_Business_Rule__c,Business_Rules__c,Data_Set_Field__r.ds_col_name__c,
  Data_Set__c,exec_tbl_name__c,Id,Name,Scenario__c,Sequence__c,StatusFlag__c,Data_Set_Field__r.tb_col_nm__c
  FROM BRJoinRuleSelectCondition__c  where Scenario__c =: scenarioId and StatusFlag__c = true and Business_Rules__c =: businessRuleId];

  for(Integer z=0;z<selectedColumnsList.size();z++){

   displayexpression = displayexpression + selectedColumnsList[z].Data_Set_Field__r.ds_col_name__c + ',';
   executeexpression = executeexpression + datasetvsTableNameMap.get(selectedColumnsList[z].Data_Set__c) +'.'+selectedColumnsList[z].Data_Set_Field__r.tb_col_nm__c + ',';
   insertexecexpression = insertexecexpression + selectedColumnsList[z].Data_Set_Field__r.tb_col_nm__c + ',';
  }

  displayexpression = displayexpression.removeEnd(',');
  executeexpression = executeexpression.removeEnd(',');
  insertexecexpression = insertexecexpression.removeEnd(',');


  List<BR_JoinRule__c> joinRuleList = new List<BR_JoinRule__c>();

  for(JoinExpressionWrapper joinwrapobj : ExpressionWrapper)
  {

    BR_JoinRule__c brjoinruleobj = new BR_JoinRule__c();        
    brjoinruleobj.Business_Rules__c = businessRuleId;
    brjoinruleobj.Join_Rule_Display_Expression__c = joinwrapobj.defaultexpression;
    brjoinruleobj.Join_Rule_Execute_Expression__c = joinwrapobj.executeexpression;
    brjoinruleobj.Scenario__c = scenarioId;
    brjoinruleobj.Select_Display_Expression__c = displayexpression;
    brjoinruleobj.Select_Execute_Expression__c = executeexpression;
    brjoinruleobj.insert_exec_expr__c = insertexecexpression;

    System.debug('Join Execute Expression Value ::: '+joinwrapobj.executeexpression);
    if(whereClaus!=null && whereClaus!='' && whereClaus.indexOf(':') != -1){
          String[] str = whereClaus.split(':');
          system.debug(str);
         brjoinruleobj.where_expression__c = str[1];
        brjoinruleobj.where_display_expression__c  = str[0];
        } 
    joinRuleList.add(brjoinruleobj);
  }

  System.debug('joinRuleList ::: '+joinRuleList);

  insert joinRuleList;
  return null;

  }

  @AuraEnabled
  public static string getBusinessRuleId(String scenarioRuleInstanceId,String scenarioId){
    System.debug('scenarioRuleInstanceId ::: '+scenarioRuleInstanceId);
    System.debug('scenarioId ::: '+scenarioId);
    list<Business_Rules__c> lstRules = [select id from Business_Rules__c where ScenarioRuleInstanceDetails__c =:scenarioRuleInstanceId
    and Scenario__c =: scenarioId];
    System.debug('lstRules ::: '+lstRules);
    System.debug('lstRules size ::: '+lstRules.size());
    if(lstRules.size() > 0){
      return lstRules[0].Id;
    }
    else{
      return null;
    }

  }


  @AuraEnabled 
  public static JoinExpressionWrapper getinitializedValues(String businessRuleId,String firstdefaultdatasource,String seconddefaultdatasource)
  {

    List<Business_Rules__c> ruleconfiguredList = [SELECT Name FROM Business_Rules__c where Id =: businessRuleId limit 1];


    String ruleName = ruleconfiguredList[0].Name;

    System.debug('ruleconfiguredList::::: '+ruleconfiguredList);

    List<BR_JoinRule__c> joinRuleList = [SELECT Business_Rules__r.Name,Id,Join_Rule_Display_Expression__c,Name,Scenario__c,Join_Rule_Execute_Expression__c FROM BR_JoinRule__c where Business_Rules__c =: businessRuleId];

    String default_expression = joinRuleList[0].Join_Rule_Display_Expression__c;
    String execute_expression = joinRuleList[0].Join_Rule_Execute_Expression__c;
    System.debug('joinRuleList::::: '+joinRuleList);

    List<String> selectedPicklistValues = new list<String>();

    List<String> fieldsIdList = new List<String>();

    List<String> finalvaluesList = new List<String>();
    String namespace = SalesIQGlobalConstants.NAME_SPACE;

    List<BR_Join_Rule_Condition__c> picklistvaluesList = [SELECT Business_Rules__c,Data_Set_1_Field__r.ds_col_name__c,Data_Set_2_Field__r.ds_col_name__c FROM BR_Join_Rule_Condition__c WHERE Business_Rules__c =: businessRuleId order by Sequence__c];

    for(BR_Join_Rule_Condition__c pickobj : picklistvaluesList)
    {

      fieldsIdList.add(String.valueOf(pickobj.getSObject(namespace+'Data_Set_1_Field__r').get(namespace+'ds_col_name__c')));
      fieldsIdList.add(String.valueOf(pickobj.getSObject(namespace+'Data_Set_2_Field__r').get(namespace+'ds_col_name__c')));
    }

    System.debug('fieldsIdList ::: '+fieldsIdList);

    List<String> expressionwrapList = new List<String>();

    JoinExpressionWrapper joinexpobj = new JoinExpressionWrapper(ruleName,default_expression,fieldsIdList,firstdefaultdatasource,seconddefaultdatasource,execute_expression);


    return joinexpobj;
  }

  @AuraEnabled 
  public static List<JoinFieldsWrapper> getupdatedfields(String scenarioRuleInstanceId)
  {

   List<JoinFieldsWrapper> wrapperList = new List<JoinFieldsWrapper>();

   list<Data_Set_Rule_Map__c> outputdatasetcolumnsList = [select id,dataset_id__c,table_display_name__c from Data_Set_Rule_Map__c
   where ds_type__c='I' and scenario_rule_instance_id__r.Id =: scenarioRuleInstanceId];

   List<Id> inputdatasetIdList = new List<Id>();

   for(Data_Set_Rule_Map__c nobj :outputdatasetcolumnsList){

     inputdatasetIdList.add(nobj.dataset_id__c);
   }

   List<Data_Set_Column_Detail__c> columnsList = [SELECT column_id__c,Is_Visible__c,dataset_id__c,datatype__c,
   ds_col_name__c,format__c,Id,Key_Column__c,Name,Table_Data_Type__c,tb_col_nm__c,value_type__c,
   variable_type__c,dataset_id__r.Name,JoinStatus_Flag__c FROM Data_Set_Column_Detail__c where dataset_id__c IN: inputdatasetIdList and
   variable_type__c != 'Derived'];

   for(Data_Set_Column_Detail__c jrobj : columnsList)
   {

    JoinFieldsWrapper expressionobj = new JoinFieldsWrapper();

    expressionobj.datasourceName = jrobj.dataset_id__r.Name;
    expressionobj.datasetId = jrobj.dataset_id__c;
    expressionobj.datasourceColumnName = jrobj.ds_col_name__c;
    expressionobj.tableColumnName = jrobj.tb_col_nm__c;
    expressionobj.statusFlag = jrobj.JoinStatus_Flag__c;
    expressionobj.Id = jrobj.Id;
    expressionobj.columnId = jrobj.column_id__c;
    expressionobj.colDatatype = jrobj.datatype__c;
    expressionobj.colValuetype = jrobj.value_type__c;
    expressionobj.tableDataType = jrobj.Table_Data_Type__c;
    expressionobj.variableType = jrobj.variable_type__c;
    expressionobj.isVisible = jrobj.Is_Visible__c;

    wrapperList.add(expressionobj);
  }
    System.debug('wrapperList::::: '+wrapperList);
    return  wrapperList;
  }

  @AuraEnabled
     public static String getQueryClauseDetails(String businessRuleId) {
        List<BR_JoinRule__c> dataObjectInstance = [SELECT Id, where_expression__c, where_display_expression__c, Scenario__c FROM BR_JoinRule__c where Business_Rules__c =: businessRuleId];
        if(dataObjectInstance.isEmpty())
          return '';
        else
          return dataObjectInstance[0].where_display_expression__c +':'+dataObjectInstance[0].where_expression__c;
    }
}
