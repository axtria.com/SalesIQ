public with sharing class BRReportsFilterCtrl {    
    static  List<string> lstExpression=new List<string>();
    static list<Data_Set_Column_Detail__c> lstColumns = new list<Data_Set_Column_Detail__c>();
    static integer expressionSequence=1;
    public static string scenarioRuleInstanceId{get;set;}
    
    @AuraEnabled
    public static String[] getDataOptions(string colDataType){     
        system.debug('reached'+colDataType);
        String[] strOperator= new String[]{};
        if(colDataType=='Text'){
            strOperator = new String[] {'None','equals','not equals','in','not in','contains','does not contains','begins with','ends with','is null','is not null'};
        }
        else if(colDataType=='Numeric'){
            strOperator = new String[] {'None','equals','not equals','in','not in','greater than','greater or equal','less than','less or equal','between','is null','is not null'};
        }
        else
            strOperator = new String[] {'No data Available'};
        return strOperator;
    }
    @AuraEnabled
    public static List<Scenario_Rule_Instance_Details__c> getScenarioID(String ScenarioRuleInstanceId){
     
       List<Scenario_Rule_Instance_Details__c> sceID = [SELECT Id, Scenario_Id__c FROM Scenario_Rule_Instance_Details__c where id=:ScenarioRuleInstanceId];
       return sceID;
    }
     @AuraEnabled
    public static String[] getOperators(string oprtype){
        String[] strOperator= new String[]{};
        if(oprtype=='Text'){
            strOperator = new String[] {'None','equals','not equals','in','not in','contains','does not contains','begins with','ends with','is null','is not null'};
        }
        else if(oprtype=='Numeric'){
            strOperator = new String[] {'None','equals','not equals','in','not in','greater than','greater or equal','less than','less or equal','between','is null','is not null'};
        }
        return strOperator;
    }
    
   
       
    @AuraEnabled
    public static list<Data_Set_Column_Detail__c> getColumnsData(String scenarioInstanceId){
        list<Data_Set_Rule_Map__c> lstRuleMap = [select id,dataset_id__c,table_display_name__c from Data_Set_Rule_Map__c
                                                 where ds_type__c='I' and scenario_rule_instance_id__r.Id =: scenarioInstanceId];
        //list<Data_Set_Column_Detail__c> lstcolumns;
        //system.debug(lstRuleMap[0].table_display_name__c);
        if(lstRuleMap.size()>0)
        {
            lstcolumns=[select Id,ds_col_name__c,column_id__c,datatype__c,value_type__c,variable_type__c,
                        tb_col_nm__c,Table_Data_type__c from Data_Set_Column_Detail__c where dataset_id__r.Id=:lstRuleMap[0].dataset_id__c]; //a1af4000000hFWiAAM
            
        }
        system.debug(lstcolumns);        
        return lstcolumns;
    }
    @AuraEnabled
    public static String getInputTableName(String scenarioInstanceId){
        
        list<Data_Set_Rule_Map__c> lstRuleMap = [select id,dataset_id__c,table_display_name__c from Data_Set_Rule_Map__c
                                                  where ds_type__c='i' and scenario_rule_instance_id__c=: scenarioInstanceId];
      
        system.debug(lstRuleMap);
        return lstRuleMap[0].table_display_name__c;
    }

    @AuraEnabled
    public static String getOutputTableName(String scenarioInstanceId){
        system.debug(scenarioInstanceId);
        list<Data_Set_Rule_Map__c> lstRuleMap = [select id,dataset_id__c,table_display_name__c from Data_Set_Rule_Map__c
                                                  where ds_type__c='o' and scenario_rule_instance_id__c=: scenarioInstanceId];
      
        system.debug(lstRuleMap);
        return lstRuleMap[0].table_display_name__c;
    }
    
    @AuraEnabled
    public static BROutputCriteria AddDynamicData(String lstCriteria,String strCategName,String strDataType,
                                                String strValueType,String strValues)
    {
        BROutputCriteria cri = new BROutputCriteria();
        cri.categName = strCategName;
        cri.dataType = strDataType;
        cri.valueType = strValueType;
        cri.selValue='';
        if(strValueType=='Discrete'){
            list<String> arr= new list<String>();
            arr.add('---');
            cri.isDropdown=true;  
            for(String str : strValues.split(','))
            arr.add(str);            
            cri.values=arr;//.add('--');
        }
        else
            cri.isDropdown=false;
        return cri;
    }
    
    @AuraEnabled
    public static List<Business_Rule_Type_Master__c> getBusinessRuleTypeMaster(string componentTypeMasterId){
        
        List<Business_Rule_Type_Master__c> lstBusinessRuleTypeMaster=[select Id,Name from  Business_Rule_Type_Master__c
                                                                      where Component_Type__r.Id= :componentTypeMasterId];
        return lstBusinessRuleTypeMaster;
    }
    ///kirti
    public static list<BRColumnWrapper> CreateColumnWrapper(String scenarioId)
    {
        list<Data_Set_Rule_Map__c> lstRuleMapOutput = [select id,dataset_id__c,table_display_name__c from Data_Set_Rule_Map__c
                                                 where ds_type__c='O' and scenario_rule_instance_id__r.Id =: scenarioId];
        list<Data_Set_Column_Detail__c> lstcolumnsOutput;
        if(lstRuleMapOutput.size()>0)
        {
            lstcolumnsOutput=[select ds_col_name__c,column_id__c,datatype__c,value_type__c,variable_type__c,
                        tb_col_nm__c,Table_Data_type__c from Data_Set_Column_Detail__c where dataset_id__r.Id=:lstRuleMapOutput[0].dataset_id__c]; //a1af4000000hFWiAAM
            
        }

        list<BRColumnWrapper> lstColumnData = new list<BRColumnWrapper>();
        list<Data_Set_Column_Detail__c> lstColumns=getColumnsData(scenarioId);
        set<String> setColumnNames = new set<String>();
        for(Data_Set_Column_Detail__c obj:lstcolumnsOutput){
            setColumnNames.add(obj.ds_col_name__c);
        }
        for(Data_Set_Column_Detail__c n : lstColumns)
          {
              BRColumnWrapper columns=new BRColumnWrapper();
              columns.Id=n.Id;
              columns.Name=n.ds_col_name__c;
              columns.variabletype=n.variable_type__c;
              columns.colDatatype=n.datatype__c;
              columns.colValuetype=n.value_type__c;
              columns.tableDataType = n.Table_Data_type__c;
              columns.tableColumnName = n.tb_col_nm__c;
              if(setColumnNames.contains(n.ds_col_name__c)){
                columns.statusFlag=true;
              }else{
                columns.statusFlag=false;
              }
              lstColumnData.add(columns);
           }
        
         return lstColumnData;  
        
    }
   
    static BRWrapCriteria AddaCriteria(boolean isFrst, String scenarioId){
        system.debug(scenarioId);
        
        list<Data_Set_Column_Detail__c> lstColumns = new list<Data_Set_Column_Detail__c>();
        lstColumns.add(new Data_Set_Column_Detail__c(ds_col_name__c = 'None', datatype__c = 'Text', tb_col_nm__c = 'None', variable_type__c = 'String'));
        System.debug( '--initial lstColumns' + lstColumns);

        lstColumns.addAll(getColumnsData(scenarioId)); 
        
        map<string, string> mapCols = new map<string, string>();
        for(Data_Set_Column_Detail__c col : lstColumns){
            mapCols.put(col.tb_col_nm__c,col.variable_type__c);
        }
        
        System.debug('--mapCols ' + mapCols);

        BRWrapCriteria wrapCriteria = new BRWrapCriteria();
        wrapCriteria.seqNo = 1;
        
        if(isFrst){
            wrapCriteria.criNameOptions=new string[] {'-'};
                wrapCriteria.criName='-';
        }
        else{
            wrapCriteria.criNameOptions=new string[] {'And','OR'};
                wrapCriteria.criName='AND';
        }       
       
        if(lstColumns.size()>0){             
            List<BRWrapBlock.DataSetColumnDetails> dtSetColDetails=new List<BRWrapBlock.DataSetColumnDetails>();
            for(Data_Set_Column_Detail__c cols:lstColumns){
                dtSetColDetails.add(new BRWrapBlock.DataSetColumnDetails(cols.ds_col_name__c,cols.datatype__c,cols.tb_col_nm__c,cols.variable_type__c));
            } 
            wrapCriteria.fieldName = dtSetColDetails;      

            List<String> operators = getOperators(lstColumns[0].datatype__c); //data type of first column 
            wrapCriteria.opVal = operators;

            wrapCriteria.selOpVal = '0@0@0@'+operators[0];
            System.debug('--lstColumns ' + lstcolumns);
            wrapCriteria.selFieldName = '0@0@0@'+lstColumns[0].datatype__c+'@'+lstColumns[0].tb_col_nm__c+'@'+mapCols.get(lstColumns[0].tb_col_nm__c);
        }   
        string tableName;
        Map<String,String> lstOfValues = getColumnValues(lstColumns,tableName);

        wrapCriteria.fieldVal='';
        wrapCriteria.fieldType ='';        
        return WrapCriteria;
    }
    private static Map<String,String> getColumnValues(List<Data_Set_Column_Detail__c> lstOfColumns,String tableName){
        return null;
    }
    @AuraEnabled
    public static string SaveCheck(String ruleName, String wrapperData,String scenarioId,String componentId,String columndata)
    {
        //list<BROutputCriteria> lstCategoryDetails = (list<BROutputCriteria>)JSON.deserialize(categoryData,list<BROutputCriteria>.class);    
        list<BRWrapBlock> lstWrapperBlock = (list<BRWrapBlock>)JSON.deserialize(wrapperData,list<BRWrapBlock>.class);   
        list<BRColumnWrapper> lstColumnWrapper = (list<BRColumnWrapper>)JSON.deserialize(columndata,list<BRColumnWrapper>.class);
        list<Business_Rules__c> lstRules = [select id from Business_Rules__c where ScenarioRuleInstanceDetails__c =:scenarioId];
        Id busiId;
        system.debug('lstWrapperBlock-->'+lstWrapperBlock);
        system.debug('Id-->'+scenarioId);
        system.debug('lstRules-->'+lstRules);
        if(lstRules!=Null && lstRules.size()>0){
            UpdateBusinessRule(lstRules[0].Id, ruleName);
            deleteBusinessFilters(lstRules[0].Id);
            busiId=lstRules[0].Id;
        }
        else{
            busiId= SaveBusinessRule(ruleName,scenarioId);      
        }
        map<Integer,Filter_Rule_Entity_Detail__c> mapBlockEntity= SaveCategoryBlockRuleEntity(lstWrapperBlock,busiId);        
        map<Integer,id> mapGroupEntity= SaveCategoryGroupRuleEntity(lstWrapperBlock,mapBlockEntity,busiId);
        SaveFilterRuleCriteria(lstWrapperBlock,mapGroupEntity,busiId,scenarioId);
        SaveColumnsToInclude(lstColumnWrapper,scenarioId);  
       // SaveBusinessRuleFieldMap(busiId,lstRules,scenarioId,lstWrapperBlock);              
        system.debug(busiId);
        return 'Success';        
    }

    public static void SaveBusinessRuleFieldMap(Id busiId,list<Filter_Rule_Criteria_Detail__c> lstFilterRuleCriDetails, String scenarioId,
                                         list<BRWrapBlock> lstWrapperBlock)
    {        
        //Below list is to add derived fields as Output to Business_Rule_FieldMap_Details__c, 
        //if they are being created as a derived field in category module
        list<Business_Rule_FieldMap_Details__c> lstFieldMap = new list<Business_Rule_FieldMap_Details__c>();
        for(Filter_Rule_Criteria_Detail__c cri : lstFilterRuleCriDetails){
            Business_Rule_FieldMap_Details__c fieldMap = new Business_Rule_FieldMap_Details__c();
            fieldMap.Business_Rule__c=busiId;
            fieldMap.IO_Flag__c='O';
            fieldMap.Param_Name__c=cri.Filter_Field_Name__c;
            fieldMap.Param_Data_Type__c= cri.FieldDataType__c;
            fieldMap.Param_Type__c= 'Derived';
            lstFieldMap.add(fieldMap);          
        }
        system.debug('lstWrapperBlock>>'+lstWrapperBlock);
        //Below list is to add derived fields as Input to Business_Rule_FieldMap_Details__c, 
        //if they are being used as input data in category
        list<Business_Rule_FieldMap_Details__c> lstDerivedCols = new list<Business_Rule_FieldMap_Details__c>();
        if(lstWrapperBlock!=null){
            for(BRWrapBlock block : lstWrapperBlock){
                for(BRWrapBlock.WrapGroup grp : block.lstwrapGroup){
                    for(BRWrapCriteria cri : grp.lstWrapCriteria){
                        if(cri.selFieldName.split('@')[5] == 'Derived'){
                            Business_Rule_FieldMap_Details__c fieldMap = new Business_Rule_FieldMap_Details__c();
                            fieldMap.Business_Rule__c=busiId;
                            fieldMap.IO_Flag__c='I';
                            string paramName =cri.selFieldName.split('@')[4].replace('_',' ');
                            fieldMap.Param_Name__c=paramName;
                            fieldMap.Param_Data_Type__c= cri.selFieldName.split('@')[3];
                            fieldMap.Param_Type__c= 'Derived';
                            lstDerivedCols.add(fieldMap);
                        }
                    }
                }
            }
        }
        if(lstDerivedCols.size()>0)
            insert lstDerivedCols;
            insert lstFieldMap;
                
    }

    @AuraEnabled
    public static string getBusinessRuleId(String scenarioId){
        list<Business_Rules__c> lstRules = [select id from Business_Rules__c where ScenarioRuleInstanceDetails__c =:scenarioId];
        return lstRules[0].Id;
    }
    static void UpdateBusinessRule(String ruleId, String ruleName){
        Business_Rules__c rule = [select id from Business_Rules__c where id=:ruleId];  
        rule.name=ruleName;        
        update rule;        
    }
     public static Id SaveBusinessRule(String ruleName,String scenarioId){
        Business_Rule_Type_Master__c ruleMaster = [select id from Business_Rule_Type_Master__c 
                                                   where name ='Filter'
                                                   limit 1];
        Scenario_Rule_Instance_Details__c scenarioRule = [select id from Scenario_Rule_Instance_Details__c   
                                                   where id =: scenarioId
                                                   limit 1];
        Business_Rules__c rule = new Business_Rules__c();
        rule.name=ruleName;
        rule.Business_Rule_Type_Master__c=ruleMaster.id;
        rule.ScenarioRuleInstanceDetails__c=scenarioRule.id;
        rule.Is_Derived__c=true;
        rule.Is_Mandatory__c=true;
        insert rule;
        
        system.debug('rule'+rule.id);
        return rule.id;
    }   
    
    static void deleteBusinessFilters(String ruleId){
        
        list<Business_Rule_FieldMap_Details__c> lstFieldMap=[select id from Business_Rule_FieldMap_Details__c
                                                     where Business_Rule__c =: ruleId];
        delete lstFieldMap;
    
        list<Filter_Rule_Entity_Detail__c> lstRuleEntity = [SELECT Base_Filter_Rule_Entity_Detail__c,Business_Rules__c,Category_Type__c,Filter_Display_Expression__c,Entity_Type__c,Filter_Rule_Entity_Detail_Ext_Id__c,Id FROM Filter_Rule_Entity_Detail__c WHERE Business_Rules__c =: ruleId];
        list<Filter_Rule_Criteria_Detail__c> lstGrpEntity = [SELECT Criteria_operator__c,FieldDataType__c,Filter_Field_Name__c,Filter_Rule_Criteria_Detail_Ext_Id__c,Filter_Rule_Entity_Detail__c,Id,Logic_operator__c,Param_value__c,Sequence__c FROM Filter_Rule_Criteria_Detail__c  where Filter_Rule_Entity_Detail__c in: lstRuleEntity];        
        delete lstGrpEntity;
              
        delete lstRuleEntity;   
    }

    static map<Integer,Filter_Rule_Entity_Detail__c> SaveCategoryBlockRuleEntity(list<BRWrapBlock> lstWrapperBlock,Id busiRuleId){        
        list<Filter_Rule_Entity_Detail__c> lstEntities = new list<Filter_Rule_Entity_Detail__c>();
        Integer priority=0;
        for(BRWrapBlock wrapData : lstWrapperBlock){
            Filter_Rule_Entity_Detail__c ruleEntity = new Filter_Rule_Entity_Detail__c();            
            if(wrapData.selPriority==1) //1 is a if
                ruleEntity.Category_Type__c='If';   
            ruleEntity.Business_Rules__c = busiRuleId;
            ruleEntity.Entity_Type__c='Block';
            ruleEntity.Priority__c=Decimal.valueOf(wrapData.selPriority);
            ruleEntity.Filter_Display_Expression__c=wrapData.criteriaLogic;
            //ruleEntity.Filter_Exec_expression__c=wrapData.executionLogic;
            priority=wrapData.selPriority;
            lstEntities.add(ruleEntity);
        }
        
        insert lstEntities;
        //Map of blocks id
        map<Integer,Filter_Rule_Entity_Detail__c> mapBlockEntity = new map<Integer,Filter_Rule_Entity_Detail__c>();        
        for(Filter_Rule_Entity_Detail__c categDet : [select id,Priority__c,Category_Type__c from Filter_Rule_Entity_Detail__c 
                                                       where Entity_Type__c='Block' and
                                                        Business_Rules__c=:busiRuleId 
                                                      order by Priority__c asc])
        {
            mapBlockEntity.put(Integer.valueOf(categDet.Priority__c), categDet);            
        }
        //Create child groups against each block
        return mapBlockEntity;
    }    
    static map<Integer,id> SaveCategoryGroupRuleEntity(list<BRWrapBlock> lstWrapperBlock,map<Integer,Filter_Rule_Entity_Detail__c> mapBlockEntity,Id busiRuleId){        
        list<Filter_Rule_Entity_Detail__c> lstEntities = new list<Filter_Rule_Entity_Detail__c>();                
        Integer blockSeq=1;
        system.debug(mapBlockEntity);
        for(BRWrapBlock block : lstWrapperBlock){
            if(block.lstWrapGroup!=null){
                for(Integer i=0 ; i < block.lstWrapGroup.size(); i++){
                   // if(mapBlockEntity.get(blockSeq).Category_Type__c!='Else'){
                        BRWrapBlock.WrapGroup grp = block.lstWrapGroup[i];
                        Filter_Rule_Entity_Detail__c ruleEntityGrp = new Filter_Rule_Entity_Detail__c();             
                        ruleEntityGrp.Entity_Type__c='Group';
                        ruleEntityGrp.Parent_Entity__c=mapBlockEntity.get(blockSeq).id;                
                        ruleEntityGrp.Priority__c= Decimal.valueOf(i+1);
                        ruleEntityGrp.Business_Rules__c = busiRuleId;
                        if(i != 0)
                            ruleEntityGrp.Logic_Operator__c=block.lstWrapGroup[i].selGroupOp;                
                        lstEntities.add(ruleEntityGrp);
                   // }
                }
            }
            blockSeq++;
        }
        insert lstEntities;
        
        //Get map of groups id
        map<Integer,id> mapGroupEntity = new map<Integer,id>();
        Integer j=1;
        for(Filter_Rule_Entity_Detail__c categDet : [select id from Filter_Rule_Entity_Detail__c 
                                                       where Entity_Type__c='Group' and Business_Rules__c =: busiRuleId])
        {
            mapGroupEntity.put(j, categDet.id);
            j++;
        }
        return mapGroupEntity;        
    }    
    
    static void SaveFilterRuleCriteria(list<BRWrapBlock> lstWrapperBlock,map<Integer,id> mapGroupEntity,Id busiId,String scenarioId){ 
        system.debug('SaveCategoryRuleCriteria');               
        map<String, String> mapColumns = new map<String, String>();
        //list<Data_Set_Column_Detail__c> lstColumns = getColumnsData(scenarioRuleInstanceId);
        if(lstColumns!=null){
            for(Data_Set_Column_Detail__c col : lstColumns){
                mapColumns.put(col.ds_col_name__c, col.datatype__c);
            }
        }
             
        list<Filter_Rule_Criteria_Detail__c> lstRuleDetail = new list<Filter_Rule_Criteria_Detail__c>();
       
        for(BRWrapBlock block : lstWrapperBlock){ 
            Integer k=1;
            if(block.lstWrapGroup!=null){
                for(BRWrapBlock.WrapGroup grp : block.lstWrapGroup){                                                          
                    for(BRWrapCriteria cri : grp.lstwrapCriteria){
                        Filter_Rule_Criteria_Detail__c ruleDetail = new Filter_Rule_Criteria_Detail__c();
                        ruleDetail.Filter_Rule_Entity_Detail__c = mapGroupEntity.get(k);
                        system.debug(ruleDetail.Filter_Rule_Entity_Detail__c);
                        ruleDetail.Criteria_operator__c=cri.selOpVal.split('@')[3]; 
                        system.debug(cri.selOpVal.split('@')[3]);
                        system.debug('ruleDetail.Criteria_operator__c::::'+ruleDetail.Criteria_operator__c);//equal - not equal
                        if(cri.selFieldName.contains('@')){
                            String colName=cri.selFieldName.split('@')[4];
                            ruleDetail.Filter_Field_Name__c=colName;                    
                            ruleDetail.FieldDataType__c =cri.selFieldName.split('@')[3];
                        }
                        else{
                            ruleDetail.Filter_Field_Name__c=cri.selFieldName;                    
                            ruleDetail.FieldDataType__c = mapColumns.get(cri.selFieldName);
                        }
                        ruleDetail.Logic_Operator__c=cri.criName;
                        ruleDetail.Sequence__c=String.valueOf(cri.seqNo);
                        ruleDetail.Param_value__c=cri.fieldVal;
                        lstRuleDetail.add(ruleDetail);
                    }
                   k++; 
                }
            }
        }
        insert lstRuleDetail;
        SaveBusinessRuleFieldMap(busiId,lstRuleDetail,scenarioId,lstWrapperBlock); 
    }
    
    static void SaveColumnsToInclude(list<BRColumnWrapper> lstColumnWrapper,string scenarioInsId)
    {   
        list<Data_Set_Rule_Map__c> lstRuleMap = [select id,dataset_id__c,table_display_name__c from Data_Set_Rule_Map__c
                                                 where ds_type__c='O' and scenario_rule_instance_id__r.Id =: scenarioInsId];

        list<Data_Set_Column_Detail__c> lstColumnDetail = new  list<Data_Set_Column_Detail__c>();
        list<Data_Set_Column_Detail__c> lstColumnDetailToInsert = new  list<Data_Set_Column_Detail__c>();
        Map<string,ID> mapColumnDetails=new Map<string,ID>();
        list<String> lstColumnDetailToDelete = new  list<String>();
        set<String> setColumnName = new set<String>();
        List<Data_Set_Column_Detail__c> lstColumnNames =[select Id,ds_col_name__c,column_id__c,datatype__c,value_type__c,variable_type__c,
                        tb_col_nm__c from Data_Set_Column_Detail__c where dataset_id__r.Id=:lstRuleMap[0].dataset_id__c];
        for(Data_Set_Column_Detail__c obj:lstColumnNames){
            setColumnName.add(obj.ds_col_name__c);
            mapColumnDetails.put(obj.ds_col_name__c,obj.Id);
        }
        system.debug(mapColumnDetails);
        for(BRColumnWrapper col : lstColumnWrapper)
        {

            if(col.statusFlag==true && !setColumnName.contains(col.Name)){
                Data_Set_Column_Detail__c  columnDetail=new Data_Set_Column_Detail__c ();
                columnDetail.ds_col_name__c= col.Name; 
                columnDetail.datatype__c=col.colDatatype;
                columnDetail.value_type__c=col.colValuetype;
                columnDetail.variable_type__c=col.variabletype;
                columnDetail.Table_Data_type__c = col.tableDataType;
                columnDetail.tb_col_nm__c = col.tableColumnName;
                columnDetail.dataset_id__c=lstRuleMap[0].dataset_id__c;
                lstColumnDetailToInsert.add(columnDetail);
            }else if(col.statusFlag==false){
                if(setColumnName.contains(col.Name)){
                    lstColumnDetailToDelete.add(mapColumnDetails.get(col.Name));
                }

            }
            
        }
        system.debug(lstColumnDetailToDelete);
        List<Data_Set_Column_Detail__c> lstColumnNamesToBeDeleted =[select Id from Data_Set_Column_Detail__c where Id IN:lstColumnDetailToDelete];
        delete lstColumnNamesToBeDeleted;
        insert lstColumnDetailToInsert;
        system.debug('Columns Inserted:'+lstColumnDetailToInsert);
    }
    
    public static BRWrapBlock.WrapGroup AddGroup(boolean isFrst, String scenarioId)
    {
        BRWrapBlock.WrapGroup grp = new BRWrapBlock.WrapGroup();
        //grp.groupName='Group'+GrpSeq;         
        list<BRWrapCriteria> lstCrtitera= new list<BRWrapCriteria>();
        BRWrapCriteria wrapCriteria = AddaCriteria(isFrst,scenarioId);
        //ColumnsDetail columns= Columns();
        lstCrtitera.add(wrapCriteria);
        grp.lstwrapCriteria=lstCrtitera;   
        grp.selGroupOp='AND';
        return grp; 
    }
    
    public static BRWrapBlock AddBlock(boolean isFrst, String scenarioId){        
        BRWrapBlock block =new BRWrapBlock();
        //block.priority=BlockSeq;        
        
        BRWrapBlock.WrapGroup grp=AddGroup(isFrst,scenarioId); 
       
        List<BRWrapBlock.WrapGroup> lstWrapGrp = new List<BRWrapBlock.WrapGroup>();        
        lstWrapGrp.add(grp);    
        
        block.lstWrapGroup=lstWrapGrp;    
        block.criteriaLogic='(1)';
        
        return block;
    }
    @AuraEnabled
    public static String getRuleName(String scenarioId){
        Business_Rules__c rule = [select id,name from Business_Rules__c where ScenarioRuleInstanceDetails__c=: scenarioId limit 1];
        return rule.Name;
    }
    //Kirti
    @AuraEnabled
    public static list<BRColumnWrapper> getColumns(string scenarioId)
    {
        list<BRColumnWrapper> columns= CreateColumnWrapper(scenarioId);
        return columns;        
    }
    
    
    @AuraEnabled
    public static list<BRWrapBlock> initialiseBlock(String scenarioId){    
    	System.debug('--scenarioId ' + scenarioId);
        String executeExpression='';
        scenarioRuleInstanceId = scenarioId;
        
        list<BRWrapBlock> lstWrapBlock = new list<BRWrapBlock>();
        list<Business_Rules__c> lstRules = [select id from Business_Rules__c where ScenarioRuleInstanceDetails__c =:scenarioRuleInstanceId]; 
        System.debug('--lstRules ' + lstRules);

        if(lstRules != null && lstRules.size()>0)   {     
            system.debug(lstRules[0].id);
            lstWrapBlock=  InitialiseRule(lstRules[0].id,scenarioId);    
        }
        else{
            BRWrapBlock wrapBlock = AddBlock(true,scenarioId);                
            wrapBlock.criteriaSeq = 1;
            Integer intP = 1;
            list<integer> lstInte = new list<integer>();
            lstInte.add(intP);                
            wrapBlock.priority=lstInte;

            wrapBlock.selPriority=1;
            lstWrapBlock.add(wrapBlock);
        }
        system.debug('wrapBlock-->'+lstWrapBlock);
        return lstWrapBlock;
    }
  public static list<BRWrapBlock> InitialiseRule(String businessId,String scenarioId){
        map<id, list<id>> mapGrp = new map<id, list<id>>();
        //mapGrp.put('','-');
        map<id, Filter_Rule_Entity_Detail__c> mapBlock = new map<id, Filter_Rule_Entity_Detail__c>();
        list<id> lstBlockId = new List<id>();
        list<id> lstGrpId = new List<id>();
        
        list<Filter_Rule_Entity_Detail__c> lstRuleEntity= [select id,Parent_Entity__c,Category_Type__c,Logic_Operator__c,
                                                             Entity_Type__c,Filter_Display_Expression__c,Filter_Exec_expression__c,
                                                             Priority__c from Filter_Rule_Entity_Detail__c 
                                                             where Business_Rules__c =: businessId order by Priority__c];  
        for(Filter_Rule_Entity_Detail__c entity : lstRuleEntity)
        {
            mapBlock.put(entity.id, entity);
            if(entity.Entity_Type__c =='Group'){
                lstGrpId.add(entity.id);                
                if(mapGrp.containsKey(entity.Parent_Entity__c)){
                    List<id> grpId= mapGrp.get(entity.Parent_Entity__c);
                    grpId.add(entity.Id);
                    mapGrp.put(entity.Parent_Entity__c, grpId);                     
                }
                else{
                    mapGrp.put(entity.Parent_Entity__c, new List<Id> {entity.id});                    
                }
            }
            else if(entity.Entity_Type__c =='Block'){
                lstBlockId.add(entity.id);
            }
        }
        map<id,list<Filter_Rule_Criteria_Detail__c>> mapRuleCri = new map<id,list<Filter_Rule_Criteria_Detail__c>>();      
        //Id blankID = 'id';
        //mapRuleCri.put(blankID,new List<Filter_Rule_Criteria_Detail__c>());
        for(Filter_Rule_Criteria_Detail__c criDetail : [SELECT Criteria_operator__c,Filter_Field_Name__c,Filter_Rule_Criteria_Detail_Ext_Id__c,
                                                        Filter_Rule_Entity_Detail__c,Id,Sequence__c,FieldDataType__c,Logic_operator__c,Param_value__c  FROM Filter_Rule_Criteria_Detail__c
                                                        where Filter_Rule_Entity_Detail__c in: lstGrpId order by Sequence__c ])
        {
            if(mapRuleCri.containsKey(criDetail.Filter_Rule_Entity_Detail__c)){
                    List<Filter_Rule_Criteria_Detail__c> lstCri= mapRuleCri.get(criDetail.Filter_Rule_Entity_Detail__c);
                    lstCri.add(criDetail);
                    mapRuleCri.put(criDetail.Filter_Rule_Entity_Detail__c, lstCri);   
                }
                else
                    mapRuleCri.put(criDetail.Filter_Rule_Entity_Detail__c, new List<Filter_Rule_Criteria_Detail__c> {criDetail});
        }
    
        list<BRWrapBlock> lstWrapBlock = new list<BRWrapBlock>();   
        Integer seq=1;
        String categType='';
        Id elseBlockId;
        
        list<integer> lstInte= new list<integer>();
        for(Integer i=0; i<lstBlockId.size(); i++){
            lstInte.add(i+1);
        }
        Integer per=1;
        Integer blockSeq=0;
        for(Id blockId : lstBlockId){            
            if(mapBlock.get(blockId).Category_Type__c=='Else'){
                categType='Else';
                elseBlockId=blockId;
            }
            else{
            BRWrapBlock block = CreateBlock(blockId,mapGrp,mapRuleCri,mapBlock,categType,blockSeq,scenarioId);                                                   
            block.priority=lstInte;            
            block.selPriority=per;
            lstWrapBlock.add(block); 
                per++;
                blockSeq++;
            }
        }        
           
        return lstWrapBlock;
    }
    

   public static BRWrapBlock CreateBlock(Id blockId, map<id, list<id>> mapGrp,
                                 map<id,list<Filter_Rule_Criteria_Detail__c>> mapRuleCri,                                 
                                 map<id, Filter_Rule_Entity_Detail__c> mapBlock,String categType, Integer blockSeq,String scenarioId) 
       {
        BRWrapBlock block = new BRWrapBlock();
        list<BRWrapBlock.WrapGroup> lstGroup = new list<BRWrapBlock.WrapGroup>();                 
        Filter_Rule_Entity_Detail__c GrpDet = mapBlock.get(blockId);
        block.criteriaLogic= grpDet.Filter_Display_Expression__c;
        block.executionLogic= grpDet.Filter_Exec_expression__c;  
        Integer grpSeq=0;   
        if(mapGrp.containsKey(blockId)){   
            for(Id grpId : mapGrp.get(blockId)){        
                Filter_Rule_Entity_Detail__c EntityDet= mapBlock.get(grpId);            
                BRWrapBlock.WrapGroup grp= CreateGroup(grpId,mapRuleCri,EntityDet,blockSeq, grpSeq,scenarioId);         
                lstGroup.add(grp);
                grpSeq++;
            }
        }
      
        block.lstWrapGroup = lstGroup;
        return block;
    }
    
    public static BRWrapBlock.WrapGroup CreateGroup(Id grpId,map<id,list<Filter_Rule_Criteria_Detail__c>> mapRuleCri,
                                 Filter_Rule_Entity_Detail__c categ,Integer blockSeq, Integer grpSeq,String scenarioId)
    {
        system.debug('categ for debug-->'+categ);
        BRWrapBlock.WrapGroup grp = new BRWrapBlock.WrapGroup();
        List<BRWrapCriteria> lstCriteria = new List<BRWrapCriteria>();
        system.debug(grpId);
        system.debug(mapRuleCri);
        Integer criSeq=0;
        for(Filter_Rule_Criteria_Detail__c criDetail : mapRuleCri.get(grpId)){
            BRWrapCriteria cri = CreateCriteria(criDetail,blockSeq, grpSeq, criSeq,scenarioId);
            lstCriteria.add(cri);
            criSeq++;
        }
        grp.lstwrapCriteria = lstCriteria;        
        
        if(categ != null && categ.Logic_Operator__c != '')
            grp.selGroupOp = categ.Logic_Operator__c;        
        return grp;
    }
    
    public static BRWrapCriteria CreateCriteria(Filter_Rule_Criteria_Detail__c criDetail,Integer blockSeq,Integer grpSeq,Integer criSeq,String scenarioId){  
        list<Data_Set_Column_Detail__c> lstColumns=getColumnsData(scenarioId);
      system.debug(scenarioId);
      map<string, string> mapCols = new map<string, string>();
      mapCols.put('','');
        for(Data_Set_Column_Detail__c col : lstColumns){
            mapCols.put(col.tb_col_nm__c,col.variable_type__c);
        }
    BRWrapCriteria cri = new BRWrapCriteria();           
        //cri.fieldName=lstColumns;
       List<BRWrapBlock.DataSetColumnDetails> dtSetColDetails=new List<BRWrapBlock.DataSetColumnDetails>();
           dtSetColDetails.add(new BRWrapBlock.DataSetColumnDetails('','','',''));
        for(Data_Set_Column_Detail__c cols:lstColumns){
            dtSetColDetails.add(new BRWrapBlock.DataSetColumnDetails(cols.ds_col_name__c,cols.datatype__c,cols.tb_col_nm__c,cols.variable_type__c));
        }   
        cri.fieldName= dtSetColDetails;  
        cri.selOpVal=criSeq+'@'+grpSeq+'@'+blockSeq+'@'+criDetail.Criteria_operator__c; //equal - not equal
        cri.opVal=getOperators(criDetail.FieldDataType__c);   
        system.debug('mapCols-->>'+mapCols.get(criDetail.Filter_Field_Name__c));     
       cri.selFieldName=criSeq+'@'+grpSeq+'@'+blockSeq+'@'+criDetail.FieldDataType__c+'@'+criDetail.Filter_Field_Name__c+'@'+mapCols.get(criDetail.Filter_Field_Name__c); 

       // cri.valueType = mapCols.get(criDetail.value_type__c);               
        cri.criName=criDetail.Logic_Operator__c;
        cri.seqNo=Integer.valueOf(criDetail.Sequence__c);
        cri.fieldVal=   criDetail.Param_value__c;
        string strValues = criDetail.Param_value__c;
        list<String> arr= new list<String>();
        List<BRWrapBlock.selectoption> optns=new List<BRWrapBlock.selectoption>();
        List<selectoption> tempList = new List<selectoption>();
        for(String str : strValues.split(',')){
            optns.add(new BRWrapBlock.SelectOption(str,str));
            arr.add(str);
        }
        cri.optionType =  optns;
        cri.selColValue = criDetail.Param_value__c;
        if(criDetail.Logic_Operator__c == null || criDetail.Logic_Operator__c == '' || criDetail.Logic_Operator__c == '-'){
            criDetail.Logic_Operator__c='-';
            cri.criNameOptions=new string[] {'-'};  
        }
        else
            cri.criNameOptions=new string[] {'AND','OR'};  
        return cri;
    }
 
    @AuraEnabled
    public static BRWrapBlock.WrapGroup AddAGroup(String scenarioId) {                                  
        BRWrapBlock.WrapGroup grp=AddGroup(true,scenarioId);        
        return grp;
    }

    @AuraEnabled
    public static BRWrapCriteria AddCriteria(String scenarioId){ 
        BRWrapCriteria wrapCriteria = AddaCriteria(false,scenarioId);        
        return wrapCriteria;
        
    }
}