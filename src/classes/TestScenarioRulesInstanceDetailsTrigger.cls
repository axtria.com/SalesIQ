@isTest
private class TestScenarioRulesInstanceDetailsTrigger {
    static testMethod void testMethod1() 
    {
    	Workspace__c workSpaceObj = new Workspace__c();
        workSpaceObj.Name = 'Workspace 1';
        workSpaceObj.Workspace_Description__c='Workspace 1';
        workSpaceObj.Workspace_Start_Date__c=Date.parse('1/1/2014');
        insert workSpaceObj;

		Scenario__c scenarioObj = new Scenario__c ();
        scenarioObj.Workspace__c= workSpaceObj.id;
        insert scenarioObj;

         Business_Rule_Template__c  busRuleTemObj = new Business_Rule_Template__c();
        busRuleTemObj.isActive__c=true;
        busRuleTemObj.isInternal__c=true;
        insert busRuleTemObj;

        BusinessRule_Template_Details__c busRuleTemDet = new BusinessRule_Template_Details__c();
        busRuleTemDet.is_Visible__c=true;
        busRuleTemDet.Business_Rule_Template_Id__c=busRuleTemObj.id;
        insert busRuleTemDet;

        ComponentTypeMaster__c ComTypeMasObj = new ComponentTypeMaster__c();
        ComTypeMasObj.Name='Call Balancing';
        ComTypeMasObj.Component_Definition__c='CPGDerivedFieldsList';
        ComTypeMasObj.DisplayIcon__c='add';
        ComTypeMasObj.DisplayIconClass__c='window information';
        insert ComTypeMasObj;

        Scenario_Rule_Instance_Details__c sceRuleInsDetObj = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        sceRuleInsDetObj.Component_Display_Order__c=4;
        sceRuleInsDetObj.Is_Visible__c=true;
        sceRuleInsDetObj.Component_Type_Master__c=ComTypeMasObj.id;
        sceRuleInsDetObj.BRT_Details_Id__c=busRuleTemDet.id;
        insert sceRuleInsDetObj;

         Scenario_Rule_Instance_Details__c sceRuleInsDetObj1 = new Scenario_Rule_Instance_Details__c ();
        sceRuleInsDetObj1.Scenario_Id__c= scenarioObj.id;
        sceRuleInsDetObj1.componentTypeLabel__c='Flags for Inclusion / Exclusion and Other Fields';
        sceRuleInsDetObj1.Component_Display_Order__c=4;
        sceRuleInsDetObj1.Is_Visible__c=true;
        sceRuleInsDetObj1.Component_Type_Master__c=ComTypeMasObj.id;
        sceRuleInsDetObj1.BRT_Details_Id__c=busRuleTemDet.id;
        insert sceRuleInsDetObj1;

        Test.startTest();
        sceRuleInsDetObj.status__c = CPGConstants.SUCCESS;
        update sceRuleInsDetObj;
        sceRuleInsDetObj.status__c = CPGConstants.SUCCESS;
        sceRuleInsDetObj1.status__c = CPGConstants.SUCCESS;
        update new List<Scenario_Rule_Instance_Details__c>{sceRuleInsDetObj,sceRuleInsDetObj1};
        sceRuleInsDetObj1.status__c = CPGConstants.READY;
        update sceRuleInsDetObj1;
        Test.stopTest();
        
    }
}