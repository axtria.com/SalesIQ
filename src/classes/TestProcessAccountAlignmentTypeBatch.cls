/******************************************************************************************************************************************************
 * Name         :   TestProcessAccountAlignmentTypeBatch.cls
 * Description  :   Test class for processAccountAlignmentTypeBatch
 * Author       :   Raghvendra Rathore
 * Created On   :   02/13/2018
******************************************************************************************************************************************************/
@isTest
private class TestProcessAccountAlignmentTypeBatch {
	//Setup method to create test data to be used in all test methods
	@testSetup
	private static void setupTestData() {
		Team__c baseTeam = new Team__c(Name = 'HTN', Type__c = 'Base', Effective_Start_Date__c = Date.today().addMonths(-3), Effective_End_Date__c = Date.today().addMonths(3));
        insert baseTeam;

        Affiliation_Network__c affNet = new Affiliation_Network__c(Name = 'test aff');
        insert affNet;

        Workspace__c wrk = new Workspace__c();
        wrk.Workspace_Start_Date__c = Date.Today();
        wrk.Workspace_End_Date__c  = Date.today().addMonths(50);
        wrk.Name = 'Workspace 1';
        wrk.Workspace_Description__c = 'New Workspace';
        insert wrk;
        
        
        Scenario__c scenario = new Scenario__c();
        scenario.CR_Tracking__c = false;
        scenario.Scenario_Name__c = 'HTN_Q3';
        scenario.Workspace__c = wrk.id;
        scenario.Team_Name__c = baseTeam.id;
        scenario.Request_Process_Stage__c = SalesIQGlobalConstants.READY;
        insert scenario;
        
        Team_Instance__c BaseTeamInstance = new Team_Instance__c();
        BaseTeamInstance.Name = 'HTN_Q1';
        BaseTeamInstance.Alignment_Period__c = 'Current';
        BaseTeamInstance.Team__c = baseTeam.id;
        BaseTeamInstance.Alignment_Type__c = 'ZIP';
        BaseTeamInstance.isActiveCycle__c = 'Y';  
        BaseTeamInstance.Team_Instance_Code__c = 'DI-00001'; 
        BaseTeamInstance.IC_EffstartDate__c = Date.today().addMonths(-3);
        BaseTeamInstance.IC_EffEndDate__c = Date.today().addYears(3);
        BaseTeamInstance.Restrict_Account_Affiliation__c = false;
        BaseTeamInstance.Enable_Affiliation_Movement__c = true;
        BaseTeamInstance.Affiliation_Network__c = affNet.Id;
        BaseTeamInstance.Scenario__c = scenario.Id;
        insert BaseTeamInstance;

        List<Account> testAccts = new List<Account>();
        for(Integer i = 0 ; i < 5 ; i++) {
            testAccts.add(new Account(Name = 'TestAcct'+i, AccountNumber = 'A0000'+i));
        }
        insert testAccts;

        Position__c nation = new Position__c();
        nation.name = 'USA';
        nation.Client_Territory_Name__c = 'North east';
        nation.Client_Position_Code__c = '1NE30000';
        nation.Client_Territory_Code__c='1NE30000';
        nation.Position_Type__c='Nation';
        nation.inactive__c = false;
        nation.RGB__c = '41,210,117';
        nation.Team_iD__c = baseTeam.id;
        nation.Hierarchy_Level__c = '2';
        nation.Related_Position_Type__c ='Base';
        nation.Effective_Start_Date__c = Date.today().addMonths(-3);
        nation.Effective_End_Date__c = Date.today().addMonths(3);
        nation.Team_Instance__c = BaseTeamInstance.id;
        insert nation;

        Position__c territory = new Position__c();
        territory.name = 'Long Island East';
        territory.Client_Territory_Name__c = 'Long Island East';
        territory.Client_Position_Code__c = '1NE30011';
        territory.Client_Territory_Code__c = '1NE30011';
        territory.Position_Type__c = 'Territory';
        territory.inactive__c = false;
        territory.RGB__c = '41,210,117';
        territory.Team_iD__c = baseTeam.id;
        territory.Parent_Position__c = nation.Id; 
        territory.Effective_Start_Date__c = Date.today().addMonths(-3);       
        territory.Effective_End_Date__c = Date.today().addMonths(3);
        territory.Related_Position_Type__c = 'Base';
        territory.Hierarchy_Level__c = '1'; 
        territory.Team_Instance__c =  BaseTeamInstance.id;
        insert territory;

        list<Position_Account__c> posAccList = new list<Position_Account__c>();
        for(Account acc : testAccts){
        	Position_Account__c obj = new Position_Account__c();
	        obj.Position__c = territory.id;
	        obj.Team_Instance__c = BaseTeamInstance.id;
	        obj.Account__c = acc.id;
	        obj.Change_Status__c = 'No Change';
	        obj.Metric1__c = 2.010000000000;
	        obj.Metric2__c = 29.000000000000;
	        obj.Metric3__c = 2.000000000000;
	        obj.Metric4__c = 1.431479545000;
	        obj.Metric5__c = 1.490000000000;
	        obj.Metric6__c = 2.010000000000;
	        obj.Metric7__c = 29.000000000000;
	        obj.Metric8__c = 2.000000000000;
	        obj.Metric9__c = 1.431479545000;
	        obj.Metric10__c =1.490000000000;
	        obj.Effective_Start_Date__c = Date.today().addMonths(-3);
	        obj.Effective_End_Date__c = Date.today().addYears(3);
	        obj.Account_Relationship_Type__c = 'Implicit';
	        posAccList.add(obj);
        }
        insert posAccList;

        list<Account_Affiliation__c> accAffList = new list<Account_Affiliation__c>();
        accAffList.add(new Account_Affiliation__c(Affiliation_Network__c = affNet.Id, Account__c = testAccts[0].Id, Root_Account__c = testAccts[0].Id));
        accAffList.add(new Account_Affiliation__c(Affiliation_Network__c = affNet.Id, Account__c = testAccts[1].Id, Parent_Account__c = testAccts[0].Id, Root_Account__c = testAccts[0].Id));
		accAffList.add(new Account_Affiliation__c(Affiliation_Network__c = affNet.Id, Account__c = testAccts[2].Id, Parent_Account__c = testAccts[1].Id, Root_Account__c = testAccts[0].Id));
        accAffList.add(new Account_Affiliation__c(Affiliation_Network__c = affNet.Id, Account__c = testAccts[3].Id, Parent_Account__c = testAccts[2].Id, Root_Account__c = testAccts[0].Id));
        accAffList.add(new Account_Affiliation__c(Affiliation_Network__c = affNet.Id, Account__c = testAccts[4].Id, Parent_Account__c = testAccts[3].Id, Root_Account__c = testAccts[0].Id));
        insert accAffList;
	}
	
	//testmethod to test the contructor - positive
	static testMethod void testConstructor() {
	    Scenario__c scenario = [select id from Scenario__c limit 1];
    	Team_Instance__c teamInstance = [select Id from Team_Instance__c limit 1];
    	
    	test.startTest();
    	    processAccountAlignmentTypeBatch batch = new processAccountAlignmentTypeBatch(teamInstance.Id, scenario.Id, false);
    	test.stopTest();
    	system.assert(batch.theQuery != '');
    	
    	//Cover costructor catch block with no rows for assignment to SObject error by passing incorrect teaminstance id
    	batch = new processAccountAlignmentTypeBatch(scenario.Id, scenario.Id, false);
	}
	
	//testmethod to test the contructor - negative
	static testMethod void testConstructorFalse() {
	    Scenario__c scenario = [select id from Scenario__c limit 1];
    	Team_Instance__c teamInstance = [select Id, Affiliation_Network__c from Team_Instance__c limit 1];
    	
    	//Cover catch block
    	processAccountAlignmentTypeBatch batch = new processAccountAlignmentTypeBatch(teamInstance.Id, teamInstance.Id, false);
    	
    	teamInstance.Affiliation_Network__c = null;
    	update teamInstance;
    	
    	test.startTest();
    	    batch = new processAccountAlignmentTypeBatch(teamInstance.Id, scenario.Id, false);
    	test.stopTest();
	}
	
	//testmethod to test the execute method - positive
	static testMethod void testExecute() {
	    Scenario__c scenario = [select id from Scenario__c limit 1];
    	Team_Instance__c teamInstance = [select Id from Team_Instance__c limit 1];
    	
    	test.startTest();
    	    database.executeBatch(new processAccountAlignmentTypeBatch(teamInstance.Id, scenario.Id, true));
    	test.stopTest();
    	
    	system.assertEquals(SalesIQGlobalConstants.READY, [select Request_Process_Stage__c from Scenario__c where id =: scenario.Id].Request_Process_Stage__c);
	}
	
	//testmethod to test the execute method - negative
	static testMethod void testExecuteFalse() {
    	Team_Instance__c teamInstance = [select Id from Team_Instance__c limit 1];
    	
    	test.startTest();
    	    //Cover costructor catch block with no rows for assignment to SObject error by passing incorrect scenario id
    	    database.executeBatch(new processAccountAlignmentTypeBatch(teamInstance.Id, teamInstance.Id, false));
    	test.stopTest();
	}
}