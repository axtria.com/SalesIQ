/**********************************************************************************************
@author     :  Harkirat Singh
@date       : 
@description: This is the controller for used to check status of Pending print jobs.
Revison(s)  : 
**********************************************************************************************/
global class PrintMapHierarchyStatusBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {
    
    global string statusfinal{get;set;}
    global string logid{get;set;}
    
    public PrintMapHierarchyStatusBatch(string id1)
    {
        logid=id1;
        statusfinal='In_Progress';
    }
     
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String cancelStatus='Cancelled';    
        String query = 'SELECT CreatedDate,Id,External_Job_ID__c,Interface_Name__c,Job_Status__c,Name,Output__c,Position__r.Client_Position_Code__c, Position__r.Hierarchy_Level__c,Request_Type__c,Team_Instance__r.Name,Team_Instance__r.Team_Instance_Code__c,Team_Instance__r.Geography_Type_Name__r.Geography_Table__c,Team_Instance__r.Team__r.Country__r.Country_Code__c,Position__r.Position_Type__c,Web_Map_as_JSON__c FROM Activity_Log__c WHERE id=:logid AND Job_Status__c !=:cancelStatus';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Activity_Log__c> logs)
    {  
        
    
    }  
    global void finish(Database.BatchableContext BC)
    {
        
     
    }
}