@isTest
public class CallPlanTestDataFactory {
    
    public static ETL_Config__c createETLConfig(){
        ETL_Config__c config = new ETL_Config__c();
        config.Name = SalesIQGlobalConstants.TALEND_JOB;
        config.End_Point__c = 'http://52.24.8.231:8080/salesiq_nishant5/services/salesiq?method=runJob';
        config.SF_UserName__c = 'admin@salesiq.com.qa';
        config.SF_Password__c = 'axtria@123';
        config.ESRI_UserName__c = 'test';
        config.ESRI_Password__c = 'test';
        return config;
    }

    public static Workspace__c createWorkspace(){
        Workspace__c Workspace = new Workspace__c();
        Workspace.Name = 'test 1';
        Workspace.Workspace_Description__c = 'test';
        Workspace.Workspace_Start_Date__c = system.today().addMonths(-1);
        Workspace.Workspace_End_Date__c = system.today().addMonths(1);
        return Workspace;
    }

    public static Activity_Log__c createActivityLog(){
        Activity_Log__c activityLog = new Activity_Log__c(Name = 'testTalenJob');
        return activityLog;
    }

    public static Scenario__c createScenario(Workspace__c Workspace, Team_Instance__c teamIntance){
        Scenario__c scenario = new Scenario__c();
        scenario.Scenario_Name__c = 'test 1';
        scenario.Workspace__c = Workspace.Id;
        scenario.Team_Instance__c = teamIntance.Id;
        return scenario;
    }

    public static void createReadOnlySettings(){
        ReadOnlyApplication__c setting  = new ReadOnlyApplication__c();
        setting.Alignment__c = false;
        setting.CallPlan__c = false;
        setting.Roster__c = false;
        insert setting;
    }
    
    public static User_Access_Permission__c createUserAccessPerm(Position__c pos, User userObj, Team_Instance__c teamIntance ){
        User_Access_Permission__c objUserAccessPerm = new User_Access_Permission__c();
        objUserAccessPerm.Position__c = pos.id;
        objUserAccessPerm.name='access';
        objUserAccessPerm.Team_Instance__c =teamIntance.id;   
        if(userObj != null){
            objUserAccessPerm.User__c = userObj.id;
        }
        objUserAccessPerm.Is_Active__c = true;
        objUserAccessPerm.Map_Access_Position__c = pos.id;
        
        return objUserAccessPerm;
    }
    
    public static Related_Position__c createRelatedPos(Position_Team_Instance__c baseteaminstance,Position_Team_Instance__c overlayteaminstance,Position__c overlaypos, Position__c basePos){
     Related_Position__c relpos=new Related_Position__c();
  
     relpos.isActive__c=true;
     relpos.Base_Position_Team_Instance__c=baseteaminstance.id;
     relpos.Related_Position_Team_Instance__c=overlayteaminstance.id;
     relpos.Related_Position__c=overlaypos.id;
     relpos.Base_Position__c = basePos.id ;
     relPos.Effective_Start_Date__c = system.today() - 10;
     relPos.Effective_End_Date__c = system.today() + 10; 
     return relpos;
    }
      
    
    public static CR_Employee_Assignment__c cr_EmployeeAssignment(Change_Request__c reqId, Employee__c emp, Position__c pos ){
       
        CR_Employee_Assignment__c empAssigment = new CR_Employee_Assignment__c();        
        empAssigment.Position_ID__c = pos.id;
        empAssigment.Change_Request_ID__c = reqId.id;   
        empAssigment.Employee_ID__c = emp.id;
        empAssigment.Is_Active__c = true;
        
        return empAssigment;
        
      }
    
    // Arun Kumar added new parameter 'team Instace' in order to pass call plan test cases
    //Make the changes if any one is using   
    public static Change_Request__c createChangeRequest(User usr, Position__c pos, Team_Instance__c TeamInstance, string status ){
        Change_Request__c objChangeRequest = new Change_Request__c();
        objChangeRequest.Approver1__c = usr.id;
        objChangeRequest.Destination_Position__c = pos.id;
        objChangeRequest.Source_Position__c = pos.id;
   
         objChangeRequest.Change_String_Value__c = '11788';
        objChangeRequest.Status__c = status;
         objChangeRequest.OwnerID  =usr.id;
         objChangeRequest.Team_Instance_ID__c = TeamInstance.id;
         objChangeRequest.scenario_name__c='Scenario';
         objChangeRequest.Execution_Date__c = date.today();
        
        return objChangeRequest;
    }
    
    
     public static Change_Request__c createChangeRequestChild(Change_Request__c Parent, User usr, Position__c pos, Position__c Dstination, Team_Instance__c TeamInstance, string status ){
        Change_Request__c objChangeRequest = new Change_Request__c();
        objChangeRequest.Approver1__c = usr.id;
        objChangeRequest.Destination_Position__c = Dstination.id;
         objChangeRequest.Source_Position__c = pos.id;
   
         objChangeRequest.Change_String_Value__c = '11788,';
        objChangeRequest.Status__c = status;
         objChangeRequest.OwnerID  =usr.id;
         objChangeRequest.Team_Instance_ID__c = TeamInstance.id;
         objChangeRequest.scenario_name__c='Scenario';
         objChangeRequest.Parent_Change_Request_ID__c = Parent.id;
        return objChangeRequest;
    }
    
    
    public static list<Change_Request__c> createChangeRequestDestSourc(User usr, Position__c source, Position__c dest, Change_Request_Type__c recType, Change_Request_Approver_Detail__c objCRAD, Position_Team_Instance__c posteamInsSource, Position_Team_Instance__c posteamInsDestination, Team_Instance__c TeamInstance){
        list<Change_Request__c> listChangeRequest = new list<Change_Request__c>();
        Integer iCount;
        for(iCount=1;iCount<4;iCount++){
            Change_Request__c objChangeRequest = new Change_Request__c();
            objChangeRequest.Approver1__c = usr.id;
            objChangeRequest.Destination_Position__c = dest.id;
            objChangeRequest.Source_Position__c = source.id;
            objChangeRequest.RecordTypeID__c = recType.id;
         
            objChangeRequest.Change_String_Value__c='11788';
            objChangeRequest.status__c = 'Pending';
            objChangeRequest.Request_Type_Change__c = recType.CR_Type_Name__c;
            objChangeRequest.CurrentApprover__c = objCRAD.id;
            objChangeRequest.Source_Positions__c = posteamInsSource.id;
            objChangeRequest.Destination_Positions__c = posteamInsDestination.id;
            objChangeRequest.Team_Instance_ID__c = TeamInstance.id;
            
            listChangeRequest.add(objChangeRequest);
        }
        system.debug('======listChangeRequest---------'+listChangeRequest);
        
        return listChangeRequest;
    }
    
    public static Change_Request_Approver_Detail__c createCRAD(Change_Request__c crObj, Position__c pos, User usr){
        Change_Request_Approver_Detail__c obj = new Change_Request_Approver_Detail__c();
        obj.Approver_Position__c = pos.id;
        obj.Change_Request__c = crObj.id;
        obj.Approver_User__c = usr.id;
        obj.Status__c = crObj.status__c;
        obj.Comments__c = 'CRAD created';       
        return obj;
    }
   
    public static Change_Request_Approver_Detail__c createCRADsecond( Position__c pos, User usr){
        Change_Request_Approver_Detail__c obj = new Change_Request_Approver_Detail__c();
        obj.Approver_Position__c = pos.id;
        obj.Approver_User__c = usr.id;
        obj.Comments__c = 'CRAD created';       
        return obj;
    }
    
    /*public static list<CR_Call_Plan__c> createCRCallPlan(Change_Request__c crObj, Account acc, string status){
        list<CR_Call_Plan__c> listCrCallPlan = new list<CR_Call_Plan__c>();
        Integer iCount;
        for(iCount=1;iCount<2;iCount++){
            CR_Call_Plan__c objChangeRequest = new CR_Call_Plan__c();
            objChangeRequest.Change_Request_ID__c = crObj.id;
            objChangeRequest.Account__c = acc.id;
            objChangeRequest.After_Value_Metric1__c = 2;
            objChangeRequest.After_Value_Metric2__c = 2;
            objChangeRequest.Before_Value_Metric1__c = 2;
            objChangeRequest.Before_Value_Metric2__c = 2;
            objChangeRequest.Comments__c = 'Test';
            objChangeRequest.isExcludedCallPlan__c = false;
            objChangeRequest.isIncludedCallPlan__c = true;
            objChangeRequest.Metric2__c = 'Methonol-Disprin';
            objChangeRequest.ReasonAddDrop__c = 'Rep retired';
            objChangeRequest.Rep_Goal_After__c = '23';
            objChangeRequest.Rep_Goal_Before__c = '22';
            objChangeRequest.Rep_Goal_Updated__c = '23';
            objChangeRequest.Rep_Reason_After__c ='Rep Died';
            objChangeRequest.Rep_Reason_Before__c ='Rep Died';
            objChangeRequest.Sales_Direction_After__c = 'Methonol-Disprin';
            objChangeRequest.Sales_Direction_Before__c = 'Disprin-Methonol';
            objChangeRequest.Status__c = status;
            listCrCallPlan.add(objChangeRequest);
        }
        
        return listCrCallPlan;
    }*/
    
     public static CR_Position__c createCRPosition(Change_Request__c chReq){
        CR_Position__c objCRPosition = new CR_Position__c();
        objCRPosition.Change_Request__c = chReq.id;
        
        return objCRPosition;
        
      
      }
    
    //TYpe = Zip Aligned or Direct Aligned
    //Added new parameter AccountType in order to handle Acount type- Physician or Hospital
    public static Account CreateAccount(string Type, string PostalCode,string AccountType){
        Account obj = new Account();
        obj.name = '11788';
        obj.FirstName__c = 'Abby';
        obj.BillingCity='NJ';
        obj.BillingState='NY';
        
        obj.AccountNumber='11788';
        //obj.type ='Physician';
        obj.type =AccountType;
        obj.AccountType__c =AccountType;
        obj.BillingPostalCode=PostalCode;
        obj.Speciality__c='11788Oncology';
        obj.Alignment_Type__c = Type;
        obj.BillingLatitude  =40.5571389900;
        obj.BillingLongitude = -74.2123329900;
        
        
        createaccount2(Type, PostalCode);
        /*decimal xmin, decimal ymin, decimal xmax, decimal ymax
        '-74.2123329900,40.5571389900,-71.8561499800,41.2072030200';
        */
        return obj;
    }
    
    private static void createaccount2(string Type, string PostalCode){
        Account obj = new Account();
        obj.name = '11788';
        obj.FirstName__c = '11788';
        obj.BillingCity='NJ';
        obj.BillingState='NY';
        obj.AccountNumber='11788';
        obj.type ='Physician';
        obj.BillingPostalCode=PostalCode;
        obj.Speciality__c='Oncology11788';
        obj.Alignment_Type__c = Type;
        obj.BillingLatitude  =41.2072030200;
        obj.BillingLongitude = -71.8561499800;
        insert obj;
    }
    
    // Type are:- Zip,Account,Hybrid
    /*public static Team_Instance_Account__c createTeamInstanceAccount(Account Acc,Team_Instance__c TeamInstance){
        Team_Instance_Account__c obj = new Team_Instance_Account__c();
        obj.Account_ID__c = Acc.id;
        obj.Team_Instance__c = TeamInstance.id;
        obj.Effective_End_Date__c  = Date.newInstance(2018,1,1);
        obj.Effective_Start_Date__c  = Date.newInstance(2015,1,1);
        obj.Metric1__c = 2.010000000000;
        obj.Metric2__c  =29.000000000000;
        obj.Metric3__c =2.000000000000;
        obj.Metric4__c  =1.431479545000;
        obj.Metric5__c  =1.490000000000;
        obj.Metric6__c = 2.010000000000;
        obj.Metric7__c  =29.000000000000;
        obj.Metric8__c =2.000000000000;
        obj.Metric9__c  =1.431479545000;
        obj.Metric10__c  =1.490000000000;
        return obj; 
        
    }*/
    
    
    public static Position_Team_Instance__c CreatePosTeamInstance(Position__c pos, Position__c ParentPos,Team_Instance__c TeamInstance){
        Position_Team_Instance__c obj = new Position_Team_Instance__c();
        obj.Position_ID__c = pos.id;
        if(ParentPos!=null)
            obj.Parent_Position_ID__c = ParentPos.id;
        obj.Effective_End_Date__c  = Date.newInstance(2018,1,1);
        obj.Effective_Start_Date__c  = Date.newInstance(2015,1,1);
        obj.Team_Instance_ID__c = TeamInstance.id;
        obj.X_Max__c=-72.6966429900;
        obj.X_Min__c=-73.9625820000;
        obj.Y_Max__c=40.9666490000;
        obj.Y_Min__c=40.5821279800;     
        
        return obj;
    }
    
    public static Position_Account__c createPositionAccount(Account Acc, Position__c Position, Team_Instance__c TeamInstance, Position_Team_Instance__c PosTeamInstance){
        Position_Account__c obj = new Position_Account__c();
        obj.Position__c = Position.id;
        obj.Team_Instance__c = TeamInstance.id;
        obj.Account__c = Acc.id;
        obj.Position_Team_Instance__c = PosTeamInstance.id;
        //obj.Team_Instance_Account__c = TeamInstanceAcc.id;
        obj.Change_Status__c = 'Approved';
        obj.Metric1__c  =2.010000000000;
        obj.Metric2__c  =29.000000000000;
        obj.Metric3__c  =2.000000000000;
        obj.Metric4__c  =1.431479545000;
        obj.Metric5__c  =1.490000000000;
        obj.Metric6__c  =2.010000000000;
        obj.Metric7__c  =29.000000000000;
        obj.Metric8__c  =2.000000000000;
        obj.Metric9__c  =1.431479545000;
        obj.Metric10__c =1.490000000000;
        obj.Effective_Start_Date__c  = Date.today().addMonths(-1);
        obj.Effective_End_Date__c  = Date.today().addYears(1);
        obj.Proposed_Position__c = Position.id;
        return obj;
        
    }
    
     public static Position_Account__c createPositionAccountPending(Account Acc, Position__c Position, Team_Instance__c TeamInstance, Position_Team_Instance__c PosTeamInstance){
        Position_Account__c obj = new Position_Account__c();
        obj.Position__c = Position.id;
        obj.Account__c = Acc.id;
        obj.Team_Instance__c = TeamInstance.id;
        obj.Effective_Start_Date__c  = Date.today().addMonths(-1);
        obj.Effective_End_Date__c  = Date.today().addYears(1);
        obj.Position_Team_Instance__c = PosTeamInstance.id;
        //obj.Team_Instance_Account__c = TeamInstanceAcc.id;
        obj.Change_Status__c = 'Pending';
        obj.Metric1__c  =2.010000000000;
        obj.Metric2__c  =29.000000000000;
        obj.Metric3__c  =2.000000000000;
        obj.Metric4__c  =1.431479545000;
        obj.Metric5__c  =1.490000000000;
        obj.Metric6__c  =2.010000000000;
        obj.Metric7__c  =29.000000000000;
        obj.Metric8__c  =2.000000000000;
        obj.Metric9__c  =1.431479545000;
        obj.Metric10__c =1.490000000000;
        return obj;
        
    }
    
    public static Geography__c createGeography(string ziptype,Geography__c geo,string name){
        Geography__c obj = new Geography__c();
        obj.name = name;
        obj.City__c='Test 11788';
        obj.State__c = '11788';
        obj.Centroid_Latitude__c = 0.0;
        obj.Centroid_Longitude__c = 0.0;
        obj.Zip_Type__c=ziptype;
        if(geo!=null)
        obj.Parent_Zip_Code__c=geo.id;
        return obj;     
    }
    
    /*public static Team_Instance_Geography__c CreateTeamInstanceGeoGraphy(Geography__c geo, Team_Instance__c teamInstance){
        Team_Instance_Geography__c obj = new Team_Instance_Geography__c();
        obj.Geography__c  =geo.id;
        obj.Team_Instance__c = teamInstance.id;
        obj.Effective_Start_Date__c = Date.today().addMonths(-1);
        obj.Effective_End_Date__c = Date.today().addYears(1);
        obj.Metric1__c = 2.010000000000;
        obj.Metric2__c  =29.000000000000;
        obj.Metric3__c =2.000000000000;
        obj.Metric4__c  =1.431479545000;
        obj.Metric5__c  =1.490000000000;
        obj.Metric6__c = 2.010000000000;
        obj.Metric7__c  =29.000000000000;
        obj.Metric8__c =2.000000000000;
        obj.Metric9__c  =1.431479545000;
        obj.Metric10__c  =1.490000000000;
        return obj;
    }*/
    
    public static list<CR_Employee_Assignment__c>  CreateEmployeeAssignment(Change_Request__c objCr, Employee__c objEmp, Position__c pos){
        Integer iCount;
        list<CR_Employee_Assignment__c>  objList = new list<CR_Employee_Assignment__c>();
        for(iCount = 0; iCount<=1 ; iCount++){
            CR_Employee_Assignment__c obj = new CR_Employee_Assignment__c();            
            obj.Change_Request_ID__c = objCr.id;
            obj.Employee_ID__c = objEmp.id;
            obj.Position_ID__c = pos.id;
            obj.After_Assignment_Type__c = ' ' ;
            obj.Before_Assignment_Type__c = ' ' ;           
            obj.Before_Effective_End_Date__c = Date.today();
            obj.Before_Effective_Start_Date__c = Date.today();
            obj.After_Effective_End_Date__c = Date.today();
            obj.After_Effective_Start_Date__c = Date.today();
            obj.is_Active__c = true;
            objList.add(obj);
        }
        
        return objList;
    }
    
    
    public static Position_Geography__c createPosGeography(Geography__c geo, Position__c pos,Position_Team_Instance__c PosteamInstance, Team_Instance__c TeamInstance ){
        Position_Geography__c obj  = new Position_Geography__c();
        obj.Effective_Start_Date__c  = Date.today().addMonths(-1);
        obj.Effective_End_Date__c  = Date.today().addYears(1);
        obj.Geography__c = geo.id;
        obj.Position__c = pos.id;
        obj.Team_Instance__c =TeamInstance.id ;
        //obj.Team_Instance_Geography__c = TeamInstanceGeo.id;
        obj.Position_Team_Instance__c  =PosteamInstance.id;
        obj.Change_Status__c = 'Approved';
        obj.Metric1__c  =2.010000000000;
        obj.Metric2__c  =29.000000000000;
        obj.Metric3__c  =2.000000000000;
        obj.Metric4__c  =1.431479545000;
        obj.Metric5__c  =1.490000000000;
        obj.Metric6__c  =2.010000000000;
        obj.Metric7__c  =29.000000000000;
        obj.Metric8__c  =2.000000000000;
        obj.Metric9__c  =1.431479545000;
        obj.Metric10__c =1.490000000000;
        return obj;     
        
    }
    
    
    public static Position__c createRegionPosition(Team__c Team){
        
        Position__c pos= new Position__c();
        pos.name = 'Test Region';
        pos.Client_Territory_Name__c = 'Test Region';
       
        pos.Client_Position_Code__c = '1NE30001';
        pos.Client_Territory_Code__c='1NE30001';
        pos.Position_Type__c='Region';
        pos.inactive__c = false;
        pos.RGB__c = '41,210,117';
        pos.Team_iD__c = Team.id;
        pos.Effective_Start_Date__c  =date.today()-5;
        pos.Effective_End_Date__c  =date.today()+5;
        pos.Related_Position_Type__c  ='Base';
        
        return pos;
    }
    
    
    public static Position__c createPositionParent(Position__c region, Team__c Team){
        
        Position__c pos= new Position__c();
        pos.name = 'New York, NY';
        if(region!=null)
            pos.Parent_Position__c = region.id;
        pos.Client_Territory_Name__c='New York, NY';
        pos.Client_Position_Code__c = 'P10';
        pos.Client_Territory_Code__c='1NE30000';
        pos.Position_Type__c='District';
        pos.RGB__c = '41,210,117';
        pos.inactive__c = false;
        pos.Team_iD__c = Team.id;
         pos.Effective_Start_Date__c  =date.today()-5;
        pos.Effective_End_Date__c  =date.today()+5;
        pos.Related_Position_Type__c  ='Base';
        
        return pos;
    }
    
    public static Position__c createPosition(Position__c p1,Team__c Team){
        
        Position__c pos= new Position__c();
        pos.name = 'Long Island East, NY';
        pos.Client_Territory_Name__c = 'Long Island East, NY';
        if(p1!=null){
            pos.Parent_Position__c = p1.ID;
        }
        pos.Client_Position_Code__c = '1NE30002';
        pos.Client_Territory_Code__c='1NE30002';
        pos.Position_Type__c='Territory';
       pos.Related_Position_Type__c=Team.Type__c;
        pos.inactive__c = false;
        pos.Effective_Start_Date__c  =date.today()-5;
        pos.Effective_End_Date__c  =date.today()+5;
        pos.RGB__c = '41,210,117';
        pos.Team_iD__c = Team.id;
        pos.X_Max__c=-72.6966429900;
        pos.X_Min__c=-73.9625820000;
        pos.Y_Max__c=40.9666490000;
        pos.Y_Min__c=40.5821279800;
       
        
        return pos;
    }
    
    
    
    
    
     public static Position__c createDestinationPosition(Position__c p1,Team__c Team){
        
        Position__c pos= new Position__c();
        pos.name = 'Test Destination Terr';
        pos.Client_Territory_Name__c = 'Test Destination Terr';
        if(p1!=null){
            pos.Parent_Position__c = p1.ID;
         }
        pos.Client_Position_Code__c = '1NE30002';
        pos.Client_Territory_Code__c='1NE30002';
        pos.Position_Type__c='Territory';
        pos.inactive__c = false;
        pos.Related_Position_Type__c=Team.Type__c;
        pos.RGB__c = '41,210,117';
        pos.Team_iD__c = Team.id;
        pos.X_Max__c=-71.8561499800;
        pos.X_Min__c=-73.6513570200;
        pos.Y_Max__c=41.2072030200;
        pos.Y_Min__c=40.5729730200;
        
        return pos;
    }
    
    public static Employee__c createEmployee(){
        Employee__c obj = new Employee__c();
        obj.Name = 'Test Empl';
        obj.Employee_ID__c = '12345';
        obj.Last_Name__c = 'Empl';
        obj.FirstName__c = 'Test';
        obj.Gender__c = 'M';
        obj.Change_status__c = 'Pending';
        return obj;
    }
    
    public static Employee__c createEmployee(User userobj){
        Employee__c obj = new Employee__c();
        obj.Name = 'Test Empl';
        obj.User__c = userobj.id;
        obj.Employee_ID__c = '12345';
        obj.Last_Name__c = 'Empl';
        obj.FirstName__c = 'Test';
        obj.Gender__c = 'M';
        obj.Change_status__c = 'Pending';
        return obj;
    }
    
    public static Position_Employee__c createPosEmploye(Position__c p1,Employee__c emp){
        Position_Employee__c obj = new Position_Employee__c();       
        obj.Assignment_Type__c = 'Primary';
        obj.Position__c = p1.id;
        obj.Employee__c = emp.id;        
        obj.Status__c ='Approved';
        obj.effective_start_date__c = date.today()-3;
        obj.Effective_End_Date__c = date.today()+3;
        
        return obj;
    }
    
    public static Team__c createTeam(String Type){
        Team__c objTeam = new Team__c();
        objTeam.Name='HTN';
        objTeam.Type__c =Type; 
        objTeam.Effective_End_Date__c=Date.today().addMonths(50);
        
        return objTeam;
    }    
   
    
    //Type will defins wheather it is Zipp Terr or Account Terr Team Instance
    public Static Team_Instance__c CreateTeamInstance(Team__c Team,string Type){
        Team_Instance__c objTeamInstance = new Team_Instance__c();
        objTeamInstance.name = 'HTN_Q1_2016';
        objTeamInstance.Alignment_Period__c = 'Current';
        objTeamInstance.Team__c = Team.id;
        objTeamInstance.Alignment_Type__c = Type;
        objTeamInstance.isActiveCycle__c = 'Y';
        objTeamInstance.Team_Instance_Code__c = 'DI-00001'; 
        return objTeamInstance;
    }
    
    
    public static Team_Instance_Object_Attribute__c createObjectAttributes(Team_Instance__c TeamInstance, string ObjectName){
        Team_Instance_Object_Attribute__c obj = new Team_Instance_Object_Attribute__c();
        obj.Attribute_API_Name__c = 'Segment1__c';
        obj.Attribute_Display_Name__c='Test';
        obj.isEnabled__c = true;
        //obj.Object_Name__c='Team_Instance_Geography__c';
        obj.Object_Name__c=ObjectName;
        //obj.Display_Name__c='Test display';
        obj.Team_Instance__c=  TeamInstance.id;
        obj.isStatic__c  =true;
        obj.Data_Type__c='';
        return obj;
    }
    
    public static Team_Instance_Object_Attribute_Detail__c createAttributesDetails(Team_Instance_Object_Attribute__c objAttributes, Team_Instance__c TeamInstance){
        Team_Instance_Object_Attribute_Detail__c obj = new Team_Instance_Object_Attribute_Detail__c();
        obj.isActive__c  =true;
        obj.Team_Instance__c = TeamInstance.id;
        obj.Object_Attibute_Team_Instance__c = objAttributes.id;
        obj.Object_Value_Name__c = 'Metrci1__c';
        obj.Object_Value_Seq__c='1';
        return obj;
    }
    
    public static Team_Instance_Object_Attribute__c createObjectAttributes(Team_Instance__c TeamInstance, string InterfaceName, string AttributeApIName, string ObjectName, string DisplayName, boolean IsEnable, boolean IsEditable, integer DisplayOrder ){
        Team_Instance_Object_Attribute__c teamInstAttrData =  new Team_Instance_Object_Attribute__c();
        teamInstAttrData.Interface_Name__c = InterfaceName;
        teamInstAttrData.Attribute_API_Name__c = AttributeApIName;
        teamInstAttrData.Object_Name__c = ObjectName;
        teamInstAttrData.Data_Type__c = 'Text';
        teamInstAttrData.Attribute_Display_Name__c = DisplayName;
        teamInstAttrData.isEnabled__c=IsEnable;
        teamInstAttrData.isEditable__c=IsEditable;        
        teamInstAttrData.Team_Instance__c = TeamInstance.Id;
        teamInstAttrData.Display_Column_Order__c = DisplayOrder;
        return teamInstAttrData;
    }
    
    public static Change_Request_Type__c CreateRequestType(string reqType){
        Change_Request_Type__c obj = new Change_Request_Type__c();
        obj.Change_Request_Code__c = 'Request  Code';
        obj.CR_Type_Name__c = reqType;
        return obj;
    }
    
    public static Change_Request_Type__c CreateZipRequestType(){
        Change_Request_Type__c obj = new Change_Request_Type__c();
        obj.Change_Request_Code__c='Request  Code';
        obj.CR_Type_Name__c='ZIP Movement';
        return obj;
    }
    
     public static Change_Request_Type__c CreateAccountRequestType(){
        Change_Request_Type__c obj = new Change_Request_Type__c();
        obj.Change_Request_Code__c='Request  Code';
        obj.CR_Type_Name__c='Account Movement';
        return obj;
    }
    
     public static Change_Request_Type__c CreateCallPlanRequestType(){
        Change_Request_Type__c obj = new Change_Request_Type__c();
        obj.Change_Request_Code__c='Request  Code';
        obj.CR_Type_Name__c='Call_Plan_Change';
        return obj;
    }
    
    public static Change_Request_Type__c CreateScenarioRequestType(){
        Change_Request_Type__c obj = new Change_Request_Type__c();
        obj.Change_Request_Code__c='Request  Code';
        obj.CR_Type_Name__c='Scenario';
        return obj;
    }    
    
    public static list<CIM_Config__c> createCIMCOnfig(Change_Request_Type__c Type,Team_Instance__c TeamInstance, string ObjectName){
        list<CIM_Config__c> lstCIM = new list<CIM_Config__c>();
        Integer iCount;
        for(iCount=1;iCount<=10;iCount++){
            CIM_Config__c obj = new CIM_Config__c();
            obj.Name = 'Test CIM'+iCount;
            obj.Aggregation_Type__c = 'Sum';
            obj.Attribute_API_Name__c = SalesIQGlobalConstants.NAME_SPACE+'Metric'+iCount+'__c';
            obj.Attribute_Display_Name__c = 'Label.AlignmentIndex';
            obj.Display_Name__c = 'Label.AlignmentIndex';
            obj.Change_Request_Type__c = Type.id;
            obj.MetricCalculationType__c = 'Percentage';
            obj.isOptimum__c = false;
            obj.Enable__c = true;
            obj.Team_Instance__c = TeamInstance.id;
            obj.Object_Name__c = ObjectName;
            obj.Threshold_Min__c = '-40';
            obj.Threshold_Max__c = '40';
            obj.Threshold_Warning_Min__c = '-20';
            obj.Threshold_Warning_Max__c = '20';
            obj.Hierarchy_Level__c = '1';
            obj.Acceptable_Max__c = '5';
            obj.Acceptable_Min__c = '-5';
            obj.Default_on__c = true;
            lstCIM.add(obj);
        }
        
        system.debug('lstCIM is '+lstCIM);
        return lstCIM;
    }
    
    public static list<CIM_Change_Request_Impact__c> createCIMchangeRequestImpact(Change_Request__c crObj, CIM_Config__c objCIMconfig, Position_Team_Instance__c posTeamInstance){
        list<CIM_Change_Request_Impact__c> lstCIMchangeRequestImpact = new list<CIM_Change_Request_Impact__c>();
        Integer iCount;
        for(iCount=1;iCount<2;iCount++){
            CIM_Change_Request_Impact__c obj = new CIM_Change_Request_Impact__c();
            obj.CIM_Config__c = objCIMconfig.id; 
            obj.Position_Team_Instance__c = crObj.Source_Positions__c;
            obj.Change_Request__c = crObj.id;
            obj.Impact__c = '-1.00';
            lstCIMchangeRequestImpact.add(obj);
        }
        system.debug('lstCIMchangeRequestImpact is '+lstCIMchangeRequestImpact);
        return lstCIMchangeRequestImpact;
    }

    public static list<CIM_Change_Request_Impact__c> createDestinationCIMchangeRequestImpact(Change_Request__c crObj, CIM_Config__c objCIMconfig, Position_Team_Instance__c posTeamInstance){
        list<CIM_Change_Request_Impact__c> lstCIMchangeRequestImpact = new list<CIM_Change_Request_Impact__c>();
        Integer iCount;
        for(iCount=1;iCount<2;iCount++){
            CIM_Change_Request_Impact__c obj = new CIM_Change_Request_Impact__c();
            obj.CIM_Config__c = objCIMconfig.id; 
            obj.Position_Team_Instance__c = crObj.Source_Positions__c;
            obj.Change_Request__c = crObj.id;
            obj.Impact__c = '+1.00';
            lstCIMchangeRequestImpact.add(obj);
        }        
        return lstCIMchangeRequestImpact;
    }
    
    public static list<CIM_Position_Metric_Summary__c> createCIMMetricSummary(list<CIM_Config__c> CIMConfig,Position_Team_Instance__c PosTeamInstance, Team_Instance__c TeamInstance){
        list<CIM_Position_Metric_Summary__c> lstCIMSummary = new list<CIM_Position_Metric_Summary__c>();
        Integer iCount;
        for(CIM_Config__c COnfig:CIMConfig){
            CIM_Position_Metric_Summary__c obj = new CIM_Position_Metric_Summary__c();
            obj.Optimum__c = 999;
            obj.Approved__c = '100';
            obj.Original__c = '100';
            obj.Proposed__c = '100';
            obj.CIM_Config__c = COnfig.id;
            obj.Team_Instance__c = TeamInstance.id;
            obj.Position_Team_Instance__c = PosTeamInstance.id;
            lstCIMSummary.add(obj);
        }
        return lstCIMSummary;
    }
    
    
    public static Position_Account_Call_Plan__c CreatePosAccCallPlan(Account Acc, Position__c Pos,Team_Instance__c TeamInstance, Position_Team_Instance__c PosTeamInstance, string Status){
        Position_Account_Call_Plan__c obj = new Position_Account_Call_Plan__c();
        obj.Effective_End_Date__c  = Date.today();
        obj.Effective_Start_Date__c  = Date.today();
        //obj.Account_Alignment_type__c='Zip Aligned';
        obj.Account__c = Acc.id;
        obj.Position__c = Pos.id;
        obj.Team_Instance__c = TeamInstance.id;
        obj.Position_Team_Instance__c =  PosTeamInstance.id;
        //obj.Team_Instance_Account__c = TeamInstanceAcc.id;
        //obj.isIncludedCallPlan__c   =false;
        //obj.Calls_Original__c  =6;
        //obj.isAccountTarget__c = true;
        obj.Segment1__c = '3';
        //obj.ReasonAdd__c='Existing Physician Relationship';
        //obj.ReasonDrop__c = 'test drop';
        //obj.Change_Status__c='Revise';
        obj.Change_Status__c=Status;
        CreatePosAccCallPlanlist(Acc,Pos,TeamInstance,PosTeamInstance);
        return obj;
    }    
    
    
    
     public static void CreatePosAccCallPlanlist(Account Acc, Position__c Pos,Team_Instance__c TeamInstance, Position_Team_Instance__c PosTeamInstance){
        
        list<string> reason = new list<string>();
        reason.add('Favorable Formulary/Managed Care Coverage');
        reason.add('Group Practice Physician');
        reason.add('Non-targeted Clinic');
        reason.add('Original Call Plan Physician');
        list<Position_Account_Call_Plan__c> lst = new list<Position_Account_Call_Plan__c>();
        for(string readoncode:reason){
            Position_Account_Call_Plan__c obj = new Position_Account_Call_Plan__c();
            obj.Effective_End_Date__c  = Date.today();
            obj.Effective_Start_Date__c  = Date.today();
            //obj.Account_Alignment_type__c='Zip Aligned';
            obj.Account__c = Acc.id;
            obj.Position__c = Pos.id;
            obj.Team_Instance__c = TeamInstance.id;
            obj.Position_Team_Instance__c =  PosTeamInstance.id;
            //obj.Team_Instance_Account__c = TeamInstanceAcc.id;
            //obj.isIncludedCallPlan__c   =true;
            //obj.Calls_Original__c  =6;
            //obj.isAccountTarget__c = true;
            obj.Segment1__c = '4';
            //obj.ReasonAdd__c=readoncode;
            //obj.ReasonDrop__c = 'test drop';
            obj.Change_Status__c='Revise';
            lst.add(obj);
        }
                
        insert lst;
    }  
    
     public static Contact createcontact(Account Acc){
        Contact objcon = new Contact();
        objcon.FirstName ='FirstName';
        objcon.LastName  ='last';
        objcon.Trx__c='2';
        objcon.Calls__c=4;
        objcon.Sales__c = 300;
        objcon.Type__c='Nurse';
        objcon.AccountId = Acc.id;
        return objcon;
    }
       
    public static CR_Account__c createCRaccount( Change_Request__c crObj, Account Acc, Position__c desPos, Position__c sourcePos, Team_Instance__c TeamInstance, Position_Team_Instance__c sourcePosTeamIns, Position_Team_Instance__c desPosTeamIns){
        CR_Account__c obj = new CR_Account__c();
        obj.Account__c = Acc.id;
        obj.Destination_Position__c = desPos.id;
        obj.Source_Position__c = sourcePos.id;
        obj.Destination_Position_Team_Instance__c = desPosTeamIns.id;
        obj.Source_Position_Team_Instance__c = sourcePosTeamIns.id;
        //obj.Team_Instance_Account__c = TeamInstanceAcc.id;
        obj.Change_Request__c = crObj.Id;
        obj.IsImpactCallPlan__c = true;
        
        return obj;
    }
    
    public static Employee_Status__c CreateEmployeeStatus(Employee__c emp, string Status, date startdate, date Enddate){
        Employee_Status__c obj = new Employee_Status__c();
        obj.Employee__c = emp.id;
        obj.Effective_Start_Date__c = startdate;
        obj.Effective_End_Date__c = Enddate;
        obj.Employee_Status__c = Status;
        return obj;
    }
    
    public static User createUser(){
            // This code runs as the system user
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            User tUser = new User(Alias = 'Rep', Email='repuser@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='repuserQuset@testorg.com');
            return tUser;
    }   

    // create user with role and profile id
    public static User createUserWithRoleAndProfile(){
            // This code runs as the system user
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            UserRole ur = new UserRole(Name = 'HO');
            insert ur;
            User tUser = new User(Alias = 'ho', Email='houser@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = p.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='houserQuset@testorg.com',
                                UserRoleId = ur.Id);
            return tUser;
    }   

    @future
    public static void assignPermissionSet(Id userId, String permissionSetName) {
        PermissionSet ps = [SELECT Id, Name From PermissionSet WHERE Name = :permissionSetName];
        insert new PermissionSetAssignment(AssigneeId = userId, PermissionSetId = ps.Id );
    } 

    public static CR_Team_Instance_Config__c LayerConfig(string level, string ConfigType, string ConfigValue,boolean isEnable,Team_Instance__c TI){
        CR_Team_Instance_Config__c objConfig = new CR_Team_Instance_Config__c();
        objConfig.Configuration_Name__c=level;
        objConfig.Configuration_Type__c =ConfigType;
        objConfig.Configuration_Value__c=ConfigValue;
        objConfig.Config_Value__c = isEnable;
        if(TI!=null){
            objConfig.Team_Instance__c  =TI.id;
        }        
        return objConfig;
    }

    

     public static Team_Instance_Object_Attribute_Detail__c createAttributesDetails(Team_Instance_Object_Attribute__c objAttributes, Team_Instance__c TeamInstance, string valueName,string valueSeq){
        Team_Instance_Object_Attribute_Detail__c obj = new Team_Instance_Object_Attribute_Detail__c();
        obj.isActive__c  =true;
        obj.Team_Instance__c = TeamInstance.id;
        obj.Object_Attibute_Team_Instance__c = objAttributes.id;
        obj.Object_Value_Name__c = valueName;
        obj.Object_Value_Seq__c=valueSeq;
        return obj;
    }

    public static Organization_Master__c createOrganizationMasterRecord(){
        Organization_Master__c org = new Organization_Master__c();
        org.Name = 'North America';
        org.Org_Level__c = 'Region';
        org.Parent_Country_Level__c = true;
        return org;
    }

    public static Country__c createCountryMasterRecord(String orgid){
        Country__c country = new Country__c();
        country.name ='USA';
        country.Country_Code__c = 'US';
        country.Status__c = 'Active';
        country.Parent_Organization__c = orgid;
        return country;
    }

    public static Geography_Type__c createGeographyTypeRecord(String countryid){
        Geography_Type__c geotype = new Geography_Type__c();
        geotype.Name = 'ZIP';
        geotype.Country__c = countryid;
        geotype.Shape_Available__c = true;
        return geotype;
    }        
        
    
}