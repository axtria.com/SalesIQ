<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Call Plan App</description>
    <label>CallMAx</label>
    <tab>Call_Plan</tab>
    <tab>CIM_Change_Request_Impact__c</tab>
    <tab>CIM_Config__c</tab>
    <tab>CIM_Position_Metric_Summary__c</tab>
    <tab>Change_Request__c</tab>
    <tab>Position_Account_Call_Plan__c</tab>
    <tab>Team_Instance_Object_Attribute_Detail__c</tab>
    <tab>Team_Instance_Object_Attribute__c</tab>
    <tab>Employee__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Document</tab>
</CustomApplication>
