({
    openModal: function(component,event,helper) {
        console.log('params ', event.getParam("recordId") + ' - ' + event.getParam("operationType"));
        
        if(event.getParam("recordId"))
            component.set("v.recordId", event.getParam("recordId"));
        else
            component.set("v.recordId", "");
        
        if(event.getParam("operationType"))
            component.set("v.operationType", event.getParam("operationType"));
        else
            component.set("v.operationType", "");

        component.set("v.isOpen" , 'true');
        var accExclusionComp = component.find("accExclusionComp");
        accExclusionComp.openModalPopup();
    },

    closeModal:function(component,event,helper){    
        var modalCloseEvt = cmp.getEvent("ModalCloseEvent");
		modalCloseEvt.fire();
    },

    redirectToDataImport : function (component) {
        var url = '/dataImporter/dataimporter.app?';  
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        "url": url 
        });
        urlEvent.fire();       
    },
    
    updateCountryAttributes : function(component, event, helper){
        var countryId = event.getParam('countryId');
        console.log('country Id inside siqaccexjs :::: '+countryId);
        component.set('v.countryId',countryId);
        var flag = component.find('flagComponent');
        flag.updateCountryAttributes(countryId);
    }
})