({
    Init : function(component, event, helper) {
        component.showSpinner();

        // var treeHierarchyComp = component.find("treeHierarchy");
        // var cr_id = component.get("v.countryId");
        // console.log('cr_id');
        // console.log(cr_id);
        // treeHierarchyComp.refreshTree(component.get("v.countryId"));


        component.setRuleLevel();
        //console.log(component.get("v.selectedRule"));
        component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0 !important;}   .forceStyle .viewport.oneHeader.desktop {z-index:0 !important;} .forceStyle.desktop .viewport{overflow:hidden}");
            var wrapperObj = component.get("v.BRMRuleInfoWrapper");
        var updateruleflag = component.get('v.updateruleflag');
        var dataSetMap = component.get("v.dataSetMap");

        //UPDATE TABLE NAMES

        var ns =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0]+ '__';

        try{
            component.set("v.assignmentTableName", dataSetMap['Assignment'][ns+'Table_Display_Name__c']);
            component.set("v.baseTableName", dataSetMap['Base'][ns+'Table_Display_Name__c']);
            component.set("v.outputTableName", dataSetMap['Output'][ns+'Table_Display_Name__c']);
        }catch(e){
            console.log(e);
        }
        if(component.get('v.updateruleflag') == false)
        {
            component.getAssignmentKeyColumn();
            var treeHierarchyComp = component.find("treeHierarchy");
            $A.util.addClass(treeHierarchyComp, 'slds-hide');
            var disabledButton = component.find("disabledButton"); 
            $A.util.removeClass(disabledButton, 'slds-hide');
            var selectionmsg1 = component.find("auraIDPositions"); 
            $A.util.addClass(selectionmsg1, 'slds-hide');
        }
        else{

            if(wrapperObj.ruleLevel == "Position"){
                var treeHierarchyComp = component.find("treeHierarchy");
                $A.util.removeClass(treeHierarchyComp, 'slds-hide');
                var disabledButton = component.find("disabledButton"); 
                $A.util.addClass(disabledButton, 'slds-hide');
                var selectionmsg1 = component.find("auraIDPositions"); 
                $A.util.removeClass(selectionmsg1, 'slds-hide');
                 
            }else{
                var treeHierarchyComp = component.find("treeHierarchy");
                $A.util.addClass(treeHierarchyComp, 'slds-hide');
                var disabledButton = component.find("disabledButton"); 
                $A.util.removeClass(disabledButton, 'slds-hide');
                var selectionmsg1 = component.find("auraIDPositions"); 
                $A.util.addClass(selectionmsg1, 'slds-hide');
            }

            component.getAssignmentKeyColumn();
        

            /* Method to get the previously saved values corresponding to existing business rule on edit */
            var businessRuleId = wrapperObj.businessRuleId

            var  action = component.get('c.getPreviousData');
            action.setParams({ 
              "businessRuleId" : businessRuleId
            });

            action.setCallback(this, function(data){
                var state = data.getState();
                if(state === 'SUCCESS')
                {   
                    var previousValuesList = data.getReturnValue();
                    console.log('previousValuesList', previousValuesList);

                    if(previousValuesList.length > 0){
                        for(var i=0;i<previousValuesList.length;i++)
                        {
                            var BRDataWrapper = previousValuesList[0];
                            console.log('BRDataWrapper :: ', BRDataWrapper);
                        }   

                        component.set('v.BRDataWrapper',BRDataWrapper);
                    }

                    //var childComponent = component.find("queryBuilderAssignment");
                    //childComponent.init();
                }
                else{
                    console.log('error');
                    component.showErrorToast();
                }
            });
            $A.enqueueAction(action);
        }
    },

    showSpinner : function(component, event, helper) {
        component.set("v.Spinner", true);
    },

    hideSpinner : function(component, event, helper) {
        component.set("v.Spinner", false);
    },

    closeDeriveButton : function(component, event, helper) {
        $j = jQuery.noConflict();
        $j('.deleteClassPopOver').removeClass('slds-hide');
    },

    okayCancel : function(component, event, helper) {
        component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
        var record_Id = component.get('v.record_Id');
        var workSpaceId = component.get('v.workSpaceId');

        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:BRAssignmentRule",
            componentAttributes: {
                record_Id : record_Id,
                workSpaceId : workSpaceId,
                scenarioRuleInstanceId : component.get('v.scenarioRuleInstanceId'),
                summarizedViewFlag : component.get("v.summarizedViewFlag"),
                namespace : component.get('v.namespace') 
            }
        });

        try{
            var treeHierarchyComp = component.find("treeHierarchy");
            treeHierarchyComp.destroy();
        }catch(e){
            console.log(e);
        }
        evt.fire();
        component.destroy();
    },

    cancelCancel:function(component, event, helper) {
        $j = jQuery.noConflict();
        $j('.deleteClassPopOver').addClass('slds-hide');
    },

    restrictInputsRule :function(component, event, helper){
        var ruleName=component.find("ruleName");
        var text=ruleName.get('v.value'); 
        var newtext = text.replace(/(?!^-)[^0-9a-zA-Z_\s]/g, ''); 
        //component.set(ruleName, newtext);        
        ruleName.set('v.value', newtext); 
        console.log('newtext value :: '+newtext);   
    },

    hidepopover : function(component,event,helper){
        document.getElementById('changepopover').classList.add('slds-hide');
    },

    loadTreeHierarchy : function(component,event,helper){
        var treeHierarchyComp = component.find("treeHierarchy");
        var cr_id = component.get("v.countryId");
        console.log('cr_id');
        console.log(cr_id);
        treeHierarchyComp.refreshTree(component.get("v.countryId"));
    },

    getAssignmentKeyColumn : function(component,event,helper){
        console.log('inside getAssignmentKeyColumn');        
        var dataMap =  component.get("v.dataSetMap");
        debugger;
        if( $A.util.isUndefined(dataMap) || dataMap == null){
            return;
        }

        var seconddatasoureselectedId = dataMap['Assignment']['Id'];

        var ns =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0]+ '__';
                                
        var isSalesIQInternal = dataMap['Product'][ns+'dataset_id__r'][ns+'SalesIQ_Internal__c'];

        if(isSalesIQInternal) { 
            var getProductAction =component.get('c.getProductOptions');
            getProductAction.setParams({ 
                "internalDataSetId" : dataMap['Product'][ns+'dataset_id__r']['Id'],
                "scenarioId" : component.get('v.record_Id'),
                "updateruleflag" : component.get('v.updateruleflag')
            });

            getProductAction.setCallback(this, function(data)
            {
                debugger;
                var state = data.getState();
                if(state === 'SUCCESS'){
                    var optionsarray=[];
                    var productIds = [];

                    var tempObj = data.getReturnValue();
                    console.log('tempObj ::::::: ');
                    console.log(tempObj);

                    var tempMap = tempObj.keycolumnvalues;

                    if(component.get('v.updateruleflag') == false)
                    {
                        component.set("v.BRMRuleInfoWrapper",data.getReturnValue());
                    } else {
                        component.set("v.BRMRuleInfoWrapper.keycolumnvalues",tempMap);
                    }

                    component.set('v.keycolumn',tempObj.keycolumnname);
                    component.set('v.keydisplaycolumn',tempObj.keyDisplayColumnName);

                    optionsarray.push({'label':'','value':''});
                    var productIds = new Array();

                    Object.keys(tempMap).forEach(function(key)
                    {
                        optionsarray.push({'label':tempMap[key],'value':key});
                        productIds.push(key);
                    });

                    component.set('v.teamProductIds',productIds);
                    //GET TREE RECORDS
                    component.loadTreeHierarchy();
                    component.set('v.items',optionsarray);

                    //if(component.get('v.updateruleflag') == false) {
                        var childComponent = component.find("queryBuilderAssignment");
                        childComponent.init();
                    //}
                }else{
                    console.log('error occured');
                    component.showErrorToast();
                    console.log(data.getState());
                }
            });
            console.log('calling now getProductAction');
            $A.enqueueAction(getProductAction);

        }
        else if(!isSalesIQInternal && seconddatasoureselectedId!= undefined && seconddatasoureselectedId!=null)
        {

            /* Method to call webservice to get Assignment File components */
            console.log('call getInstanceNameAndURL ');
            var action=component.get('c.getInstanceNameAndURL');
            action.setParams({ 
                "scenarioRuleInstanceId" : component.get('v.scenarioRuleInstanceId'),
                "seconddatasoureselectedId" : seconddatasoureselectedId,
                "scenarioId" : component.get('v.record_Id')
            });
            action.setCallback(this, function(data)
            {
                var state = data.getState();
                if(state === 'SUCCESS')
                {
                    var columndata = data.getReturnValue();
                    console.log('columndata', columndata);
                      console.log('call inside  getInstanceNameAndURL');
                    for(var i=0;i<columndata.length;i++)
                    {
                        //SSL
                        var serviceresponseMap = {};
                        var brassignmenturl = columndata[0].brassignmenturl;
                        var keycolumn = columndata[0].keycolumn;
                        var keydisplaycolumn = columndata[0].keydisplaycolumn;
                        var tablename = columndata[0].tablename;
                        var brdbname = columndata[0].brdbname;
                        var brdbname1 = brdbname.replace('/','');
                        var whereClause = columndata[0].whereclause;

                        serviceresponseMap['finalURL'] = brassignmenturl;
                        serviceresponseMap['columns'] = keycolumn+','+keydisplaycolumn;
                        serviceresponseMap['tableName'] = tablename;
                        serviceresponseMap['dbname'] = brdbname1;

                        component.set('v.keycolumn',keycolumn);
                        component.set('v.keydisplaycolumn',keydisplaycolumn);

                        console.log('brassignmenturl :: '+brassignmenturl);
                        console.log('keycolumn :: '+keycolumn);
                        console.log('keydisplaycolumn :: '+keydisplaycolumn);
                        console.log('tablename :: '+tablename);
                        console.log('whereClause :: '+whereClause);
                        console.log('serviceresponseMap ::: ',serviceresponseMap);

                        console.log('call assignedKeyColumn ');
                        var action2=component.get('c.assignedKeyColumn');
                        action2.setParams({ 
                            "serviceresponseMap" : serviceresponseMap,
                            "scenarioId" : component.get('v.record_Id'),
                            "scenarioRuleInstanceId" : component.get('v.scenarioRuleInstanceId'),
                            "updateruleflag" : component.get('v.updateruleflag'),
                            "keycolumn" : keycolumn,
                            "keydisplaycolumn" : keydisplaycolumn,
                            "whereClause" : whereClause
                        });
                   
                        action2.setCallback(this, function(data)
                        {
                            var state = data.getState();
                            if(state === 'SUCCESS'){
                                  console.log('call inside getAssignnedCOlum ');
                                var tempObj = data.getReturnValue();
                                var tempMap = tempObj.keycolumnvalues;
                                if(component.get('v.updateruleflag') == false)
                                {
                                    component.set("v.BRMRuleInfoWrapper",tempObj);
                                } else {
                                    component.set("v.BRMRuleInfoWrapper.keycolumnvalues",tempMap);
                                }

                                var optionsarray=[];
                                var productIds = [];
                                var noneVal;
                                var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
                                console.log('namespace val' +namespace);
                                if(namespace != ''){
                                    noneVal = $A.get("$Label.AxtriaSalesIQTM.None");
                                }else{
                                    noneVal = $A.get("$Label.c.None");
                                }

                                optionsarray.push({'label':noneVal,'value':''});
                                var productIds = new Array();
                          
                                
                                Object.keys(tempMap).forEach(function(key)
                                {
                                    optionsarray.push({'label':tempMap[key],'value':key});
                                    productIds.push(key);
                                });
                        
                                component.set('v.teamProductIds',productIds);

                                console.log('productIds');
                                console.log(productIds);
                                //GET TREE RECORDS
                                component.loadTreeHierarchy();
                                component.set('v.items',optionsarray);
             
                                //if(component.get('v.updateruleflag') == false) {
                                    var childComponent = component.find("queryBuilderAssignment");
                                    childComponent.init();
                                //}
                            }

                            else{
                                component.showErrorToast();
                            }
                        });

                        $A.enqueueAction(action2);
                    }
                }else{
                    component.showErrorToast();
                }
            });

            $A.enqueueAction(action);

        }


        
    },

    updateProductTree : function(component,event,helper){
        var treeHierarchyComp = component.find("treeHierarchy");
        treeHierarchyComp.updateNodesOnProductSelection();
        helper.closeConfirmationModal(component);
    },


    onAssignmentChange : function(component,event,helper)
    {
        console.log('assignment change');
        console.log(component.get("v.BRMRuleInfoWrapper"));
        //component.set('v.BRMRuleInfoWrapper',component.get("v.BRMRuleInfoWrapper"));
        if(component.get("v.BRMRuleInfoWrapper.ruleLevel") == "Position"){
            //GET CURRENT PRODUCT POSITION MAPPING
            var selectedProductId = component.get('v.BRMRuleInfoWrapper.ruleValue');
            console.log('::: selectedProductId ::: '+selectedProductId);
            // if( selectedProductId != undefined && selectedProductId != null && selectedProductId != '' ){
                var selNodes = component.get('v.BRMRuleInfoWrapper.selectedPositions');
                var setSelectedNodes = new Set();

                if( selNodes != undefined && selNodes != null && selNodes.length > 0 ){
                    setSelectedNodes = new Set(selNodes.split(','));
                } 
                
                if(setSelectedNodes.size > 0 ){
                  //show confirmation modal
                  helper.openConfirmationModal(component);
                }else{
                    var treeHierarchyComp = component.find("treeHierarchy");
                    treeHierarchyComp.updateNodesOnProductSelection();
                }
            // }else{
            //     var treeHierarchyComp = component.find("treeHierarchy");
            //     treeHierarchyComp.updateNodesOnProductSelection();
            // }
        }
    },

    handleMouseEnter1 : function(component, event, helper) {
        var popover = component.find("popover1");
        $A.util.removeClass(popover,'slds-hide');
    },
    
    //make a mouse leave handler here
    handleMouseLeave1 : function(component, event, helper) {
        var popover = component.find("popover1");
        $A.util.addClass(popover,'slds-hide');
    },

    doPageValidation:function(component, event, helper){
        component.showSpinner();
        var buttonclickedId = event.target.id;
        console.log('buttonclickedId:: '+buttonclickedId);
        component.set('v.buttonclickedId',buttonclickedId);
        
        if(component.get('v.updateruleflag') == true){
            var childComponent = component.find("queryBuilderAssignment");
            childComponent.getQuery();
        }
        else if(component.get('v.updateruleflag') == false && buttonclickedId == 'savebutton'){
           var childComponent = component.find("queryBuilderAssignment");
           childComponent.getQuery();
        }
        else{
           component.closeDeriveButton();
        }
    },

    saveRuleInfoValues : function(component,event,helper){

        component.set('v.isDisabled',true);
        var ruleLevel;
        var positionString;
        // if(component.get("v.selectedRule") == ""){
        //     ruleLevel = 'Team';
        // }
        // else{
        //     ruleLevel = component.get("v.selectedRule");
        // }
        
        // console.log("Rule Value saved: "+ruleLevel);

        // if(ruleLevel == 'Position'){
        //  if(component.get("v.treeVals") == undefined || component.get("v.treeVals")==""){
        //     console.log("Please select Positons first");
        //   }
        //   else{
        //      positionString = component.get("v.treeVals");
        //   }
        // }else{

        //   console.log("This features works only when Position is selected");
        // }
        // console.log("Position value saved:  "+positionString);
      
        console.log('stagingQuery query :::: '+component.get('v.stagingQuery'));
        var assignmentFieldsWrapperList = component.get('v.assignmentFieldsWrapperList');

        console.log('assignmentFieldsWrapperList in saveRuleInfoValues :::: ');
        console.log(assignmentFieldsWrapperList);
        console.log('BRMRuleInfoWrapper value ::: ',component.get("v.BRMRuleInfoWrapper"));

        var ruleWrapper = component.get("v.BRMRuleInfoWrapper");

        var ns = ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0]+'__';

        if(component.get('v.updateruleflag') == true && component.get("v.isConditionValid") == true){
            var updateaction = component.get("c.saveUpdatedAssignmentRules");

            updateaction.setParams({ 
                "BRMRuleInfoWrapperList" : JSON.stringify(component.get("v.BRMRuleInfoWrapper")),
                "scenarioId" : component.get('v.record_Id'),
                "scenarioRuleInstanceId" : component.get('v.scenarioRuleInstanceId'),
                "parsedQuery" : ruleWrapper.parsedQuery,
                "stagingQuery" : ruleWrapper.stagingQuery,
                "parsedJSONQuery" : ruleWrapper.parsedJSONQuery,
                "firstdatasoureselectedId" : component.get("v.dataSetMap")['Base'][ns+'dataset_id__c'],
                "seconddatasoureselectedId" : component.get("v.dataSetMap")['Assignment'][ns+'dataset_id__c'],
                "assignmentFieldsWrapperList" : JSON.stringify(component.get('v.assignmentFieldsWrapperList')),
                "keydisplaycolumn" : component.get('v.keydisplaycolumn'),
                "keycolumn" : component.get('v.keycolumn')
            });
            
            updateaction.setCallback(this, function(data){
                var state = data.getState();
                if(state === 'SUCCESS'){
                    component.hideSpinner();
                    component.showSuccessToast();
                    component.closeDeriveButton(); 
                }
                else{
                    console.log("state "+state);
                }               
            });
            $A.enqueueAction(updateaction);
        }
        else if(component.get('v.updateruleflag') == false && component.get('v.isConditionValid') == true){

            var assignmentFieldsWrapperList = component.get('v.assignmentFieldsWrapperList');
            var action = component.get("c.saveAssignmentRule");

            var ns = ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0]+'__';

            action.setParams({ 
                "BRMRuleInfoWrapperList" : JSON.stringify(component.get("v.BRMRuleInfoWrapper")),
                "scenarioId" : component.get('v.record_Id'),
                "scenarioRuleInstanceId" : component.get('v.scenarioRuleInstanceId'),
                "parsedQuery" : ruleWrapper.parsedQuery,
                "stagingQuery" : component.get('v.stagingQuery'),
                "parsedJSONQuery" : component.get('v.parsedJSONQuery'),
                "firstdatasoureselectedId" : component.get("v.dataSetMap")['Base'][ns+'dataset_id__c'],
                "seconddatasoureselectedId" : component.get("v.dataSetMap")['Assignment'][ns+'dataset_id__c'],
                "assignmentFieldsWrapperList" : JSON.stringify(component.get('v.assignmentFieldsWrapperList')),
                "keydisplaycolumn" : component.get('v.keydisplaycolumn'),
                "keycolumn" : component.get('v.keycolumn')
            });
            
            action.setCallback(this, function(data){
                var state = data.getState();
                if(state === 'SUCCESS'){
                    console.log('success');
                    component.hideSpinner();
                    component.showSuccessToast();
                    component.closeDeriveButton(); 
                }
                else{
                    console.log("state "+state);
                }               
            });
            $A.enqueueAction(action);
        }
    },

    showSuccessToast : function(component, event, helper) {
        var ruleName=component.find("ruleName");
        var text=ruleName.get('v.value');
        console.log(text);

        var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
        console.log('namespace val' +namespace);
        if(namespace != ''){
            
            updateHint = $A.get("$Label.AxtriaSalesIQTM.Updated_successfully");
            createHint = $A.get("$Label.AxtriaSalesIQTM.Created_successfully");

        }else{
            updateHint = $A.get("$Label.c.Updated_successfully");
            createHint = $A.get("$Label.c.Created_successfully");
        }

        if(component.get("v.isEdit") == true){
            var msgVal = text +" "+ updateHint ;
        }else{
            var msgVal = text +" "+ createHint;
        }

        var toastEvent = $A.get("e.force:showToast");
        var ns = ( component.getType().split(':')[0] == 'c' ) ? 'c' : component.getType().split(':')[0];
        var successlabel = $A.get('$Label.'+ns+'.changes_successfully_saved');
        toastEvent.setParams({
            message: msgVal ,
            messageTemplate: msgVal,
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
        component.okayCancel();
    },

    showErrorToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        var ns = ( component.getType().split(':')[0] == 'c' ) ? 'c' : component.getType().split(':')[0];
        var errorlabel = $A.get('$Label.'+ns+'.webservice_not_available');
        toastEvent.setParams({
            message: errorlabel,
            messageTemplate: errorlabel,
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
    },

    showErrorToastCancel : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        //var ns = ( component.getType().split(':')[0] == 'c' ) ? 'c' : component.getType().split(':')[0];
        //var errorlabel = $A.get('$Label.'+ns+'.webservice_not_available');
        var msg = "Confirm Cancel"
        toastEvent.setParams({
            message: msg,
            messageTemplate: msg,
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
    },

    updateFilterWrapper : function (component, event, helper) {

        component.set('v.popoverMessage', []);
        var parsedJsonQuery = event.getParam("parsedJson");
        var whereClause = event.getParam("whereClause");
        var isValid = event.getParam("isValid");
        var errMsg = event.getParam("errorMsg");
        var stagingClause = event.getParam("stagingWhereClause");
        console.log(parsedJsonQuery);
        console.log(isValid);
        
        var validateEvent = $A.get("e.c:modalCloseEvent");

		validateEvent.setParams({
            'doValidation': true
        }).fire();
      
        if(component.get('v.buttonclickedId') == 'closebutton' || component.get('v.buttonclickedId') == 'cancelbutton')
        {
            var BRMRuleInfoWrapper = component.get("v.BRMRuleInfoWrapper");
            var BRDataWrapper = component.get('v.BRDataWrapper');
            //COMPONENT ERROR FIX : UNFDEFINED CHECKS
            //if(BRMRuleInfoWrapper.rulename != BRDataWrapper.previousRuleName)
            //else if(BRMRuleInfoWrapper.ruledescription != BRDataWrapper.previousRuleDescription)
            //else if(whereClause != BRDataWrapper.previousWhereClause){
            if( BRMRuleInfoWrapper == null && BRDataWrapper == null ){
                return ;
            }
            var rulename = BRMRuleInfoWrapper['rulename'];
            var previousRuleName = BRMRuleInfoWrapper['previousRuleName'];
            var ruledescription  = BRMRuleInfoWrapper['ruledescription'];
            var previousRuleDescription = BRMRuleInfoWrapper['previousRuleDescription'];
            var previousWhereClause = BRMRuleInfoWrapper['previousWhereClause'];

            if( ( !$A.util.isUndefined(rulename) && !$A.util.isUndefined(previousRuleName) ) && (rulename != previousRuleName)){
                component.set('v.isChanged',true);
            }
            else if( (!$A.util.isUndefined(ruledescription) && !$A.util.isUndefined(previousRuleDescription)) && (ruledescription != previousRuleDescription) ){
                component.set('v.isChanged',true);
            }
            else if( (!$A.util.isUndefined(whereClause) && !$A.util.isUndefined(previousWhereClause) ) &&(whereClause != previousWhereClause) ){
                component.set('v.isChanged',true);
            }
            else{
                component.set('v.isChanged',false);
                component.closeDeriveButton();
            }

            if(component.get('v.isChanged') == true){
                component.hideSpinner();
                var changepopover = document.getElementById('changepopover')
                if(changepopover.className.indexOf('slds-hide') != -1)
                {
                    changepopover.classList.remove('slds-hide');
                }
                else
                {
                    changepopover.classList.add('slds-hide');  
                } 
            }
        }
        else
        {
            component.set("v.isValid", isValid);

            var ruleWrapper = component.get("v.BRMRuleInfoWrapper");
            ruleWrapper.parsedQuery = whereClause;
            ruleWrapper.parsedJSONQuery = parsedJsonQuery;
            ruleWrapper.stagingQuery = stagingClause;
            component.set("v.BRMRuleInfoWrapper", ruleWrapper);

            // push error only in case if isValid failed
            console.log('is valid flag from query builder '+component.get("v.isValid"));
            //console.log('parsed query::: '+component.get('v.stagingQuery'));

            helper.doValidation(component, event, helper);
            var errorList = component.get('v.popoverMessage');
            errorList.push(errMsg);
            component.set('v.popoverMessage', errorList);
            var errorIcn = component.find("errorIcon1");
            component.hideSpinner();
            if(errorList == undefined || errorList == ""){
                $A.util.addClass(errorIcn,'slds-hide');
            }
            else{
                $A.util.removeClass(errorIcn,'slds-hide');
                //component.hideSpinner();
            }

            if(component.get("v.isValid") == "true" && component.get('v.isConditionValid') == true) {
                component.saveRuleInfoValues();
            }
        }
    },

    accordionclick : function(component,event,helper){

        var sectionone = component.find('firstsection');
        $A.util.toggleClass(sectionone,'slds-hide');

        var chevdown = component.find('chevdown');
        var isExpandable = $A.util.hasClass(chevdown, "slds-hide");      
        if(isExpandable){            
            $A.util.removeClass(chevdown, 'slds-hide');
        }
        else{
            $A.util.addClass(chevdown, 'slds-hide');
        }
        var chevleft = component.find('chevleft');
        var isExpandable1 = $A.util.hasClass(chevleft, "slds-hide");      
        if(isExpandable1){            
            $A.util.removeClass(chevleft, 'slds-hide');
        }
        else{
            $A.util.addClass(chevleft, 'slds-hide');
        }
    },

    secondaccordionclick : function(component,event,helper){

        var sectiontwo = component.find('secondsection');
        $A.util.toggleClass(sectiontwo,'slds-hide');

        var chevdown1 = component.find('chevdown1');
        var isExpandable = $A.util.hasClass(chevdown1, "slds-hide");      
        if(isExpandable){            
            $A.util.removeClass(chevdown1, 'slds-hide');
        }
        else{
            $A.util.addClass(chevdown1, 'slds-hide');
        }
        var chevleft1 = component.find('chevleft1');
        var isExpandable1 = $A.util.hasClass(chevleft1, "slds-hide");      
        if(isExpandable1){            
            $A.util.removeClass(chevleft1, 'slds-hide');
        }
        else{
            $A.util.addClass(chevleft1, 'slds-hide');
        }
    },
    
    handleRendering : function(component, event) {

        var message = event.getParam("message");
        console.log("Value Received----" +message);
        if(message == 'Loaded'){
            component.hideSpinner();
        }
    },

    closePicklistDropdown : function(component, event, helper) {
        var ev = event.target;
        var cls = ev.classList[0];
        var classSet = new Set(['icn','slds-button','uiInput','icon-nodes','slds-tree__item','slds-m-right_x-small','slds-input','slds-icon',undefined,'slds-is-expanded','slds-truncate','slds-tree']);
        if(!classSet.has(cls)){
            $A.get("e.c:modalCloseEvent").fire();  
        } 
    },

    setRuleLevel  : function(component, event, helper) {

        var action = component.get("c.getRuleLevel");
        action.setParams({ 
          "scenarioId" : component.get('v.record_Id')
        });
        action.setCallback(this, function(data){
            var state = data.getState();
            if(state === 'SUCCESS'){

                var tempObj = data.getReturnValue();
                console.log("Rule Level Values");
                console.log(tempObj);
                var optionsarray=[];
                // optionsarray.push({'label':noneVal,'value':''});
                for(var i=0;i<tempObj.length;i++){
                    optionsarray.push({'label':tempObj[i],'value':tempObj[i]});
                }
                component.set("v.rulLevel",optionsarray);
                console.log(optionsarray);
                //console.log(component.get("v.selectedRule"));
            }
            else{
                console.log("state "+state);
                component.showErrorToast();
            }               
        });
        $A.enqueueAction(action);
    },

    onRuleLevelChange : function(component, event, helper) {

        console.log(component.get("v.BRMRuleInfoWrapper.ruleLevel"));
        if(component.get("v.BRMRuleInfoWrapper.ruleLevel") == "Position"){
            var treeHierarchyComp = component.find("treeHierarchy");
            $A.util.removeClass(treeHierarchyComp, 'slds-hide');

            var disabledButton = component.find("disabledButton"); 
            $A.util.addClass(disabledButton, 'slds-hide');
            var selectionmsg1 = component.find("auraIDPositions"); 
            $A.util.removeClass(selectionmsg1, 'slds-hide');

            //GET CURRENT PRODUCT POSITION MAPPING
            var selectedProductId = component.get('v.BRMRuleInfoWrapper.ruleValue');
            console.log('::: selectedProductId ::: '+selectedProductId);

            if( selectedProductId != undefined && selectedProductId != null && selectedProductId != '' ){
                //UPDATE TREE ON CHANGE FROM TEAM -> PROD

                var selNodes = component.get('v.BRMRuleInfoWrapper.selectedPositions');
                var setSelectedNodes = new Set();

                if( selNodes != undefined && selNodes != null && selNodes.length > 0 ){
                    setSelectedNodes = new Set(selNodes.split(','));
                } 

                if(setSelectedNodes.size > 0 ){
                    //show confirmation modal
                    helper.openConfirmationModal(component);
                }else{
                    //UPDATE TREE ON CHANGE FROM TEAM -> PROD
                    treeHierarchyComp.updateNodesOnProductSelection();
                }
            }

        } else {
            var treeHierarchyComp = component.find("treeHierarchy");
            $A.util.addClass(treeHierarchyComp, 'slds-hide');
            var disabledButton = component.find("disabledButton"); 
            $A.util.removeClass(disabledButton, 'slds-hide');

            var selectionmsg1 = component.find("auraIDPositions"); 
            $A.util.addClass(selectionmsg1, 'slds-hide');
        }

    },

    closeConfirmationModal : function(component){
        var confirmPromoteModal = component.find('confirmationModal');
        $A.util.addClass(confirmPromoteModal, 'slds-hide');
    }
})