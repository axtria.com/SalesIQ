({
	doValidation : function(component, event, helper){
    	var errMessage='';
        var errorList = [];

    	var errMessage=component.get("v.errMessage");
        var isValid = component.get("v.isValid");       
        var ruleName = component.get('v.BRMRuleInfoWrapper.rulename');
        var assignmentvalues = component.get('v.BRMRuleInfoWrapper.ruleValue');
        
        var rulenames = component.get('v.rulenames');
        
        

        var noneVal;
        var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
        console.log('namespace val' +namespace);
        if(namespace != ''){
            
            noneVal = $A.get("$Label.AxtriaSalesIQTM.None");
            

         }else{
            noneVal = $A.get("$Label.c.None");
         }


        if( ( component.get('v.selectedNodesVals') == 0 ) && ( component.get("v.BRMRuleInfoWrapper.ruleLevel") == "Position") ){
            errMessage = $A.get("$Label.c.No_Position_Selected_Label");
            errorList.push(errMessage);
            var treecmp = component.find('treeHierarchy');
            $A.util.addClass(treecmp , 'erroroccured');
        }else{
            var treecmp = component.find('treeHierarchy');
            $A.util.removeClass(treecmp , 'erroroccured');
        }

        if(!ruleName || ruleName==undefined || ruleName.trim() == ''){
            errMessage = $A.get("$Label.c.Please_enter_rule_name");
            errorList.push(errMessage);
            var sname = component.find('ruleName');
            $A.util.addClass(sname , 'erroroccured');
        }else{
            var setNames = new Set(rulenames);
            if( component.get("v.isEdit") && setNames.has(ruleName.toLowerCase()) ){
                setNames.delete(ruleName.toLowerCase());
            }
            //Check dupelicate rule names
            if(setNames.has(ruleName.toLowerCase())){
                errMessage = 'Rule with name '+ruleName +' already exists.';
                errorList.push(errMessage);
                var sname = component.find('ruleName');
                $A.util.addClass(sname , 'erroroccured');
            }
        }
       
       if(component.get('v.isProductEnabled')){
        if(!assignmentvalues || assignmentvalues == undefined || assignmentvalues == '' || assignmentvalues == noneVal){
            errMessage = $A.get("$Label.c.Please_Select_Assign_Value");
            errorList.push(errMessage);
            var expname = component.find("assnselect");
            $A.util.addClass(expname , 'erroroccured');
        }
       }
       

        if(errorList.length > 0){
        	if(ruleName){
            	var sname = component.find('ruleName');
            	$A.util.removeClass(sname , 'erroroccured');
          	}
          	if(assignmentvalues){
            	var sname = component.find('assnselect');
            	$A.util.removeClass(sname , 'erroroccured');
          	}
           component.set('v.isConditionValid',false);
           component.set('v.popoverMessage',errorList);
           var errorIcn = component.find("errorIcon1");
           $A.util.removeClass(errorIcn,'slds-hide');
        }

        else{
        	component.set('v.isConditionValid',true);
        	var errorIcn = component.find("errorIcon1");
          	$A.util.addClass(errorIcn,'slds-hide');
           	var sname = component.find('ruleName');
            $A.util.removeClass(sname , 'erroroccured');
            var expname = component.find("assnselect");
          	$A.util.removeClass(expname , 'erroroccured');
        }
    },

    openConfirmationModal : function(component){
        var confirmPromoteModal = component.find('confirmationModal');
        $A.util.removeClass(confirmPromoteModal, 'slds-hide');
    },

    closeConfirmationModal : function(component){
        var confirmPromoteModal = component.find('confirmationModal');
        $A.util.addClass(confirmPromoteModal, 'slds-hide');
    }
})