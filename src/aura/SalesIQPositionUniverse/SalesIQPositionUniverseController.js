({
    closePopups :  function(component, event, helper) {
        console.log(' --- closePopups --- ');
      console.log(event);

      var setSelectedNodes = new Set();
      if(event.path != undefined){
          if(event.path[0] != undefined && event.path[0].size > 0){
            if(event.path[0].classList != undefined && event.path[0].classList != null){
            setSelectedNodes = new Set(event.path[0].classList);
                if( event.path[0].id != 'treeSearchBox'){
                    if(!(setSelectedNodes.has('hierarchyButton') )){
                      var DataTable = component.find('DataTable');
                      DataTable.closeDropDown();
                  }
                }
            }
        }
      }
    // this code is commnmeted due to component error coming on roster module on change of team instance
     /* if((event.path != undefined || event.path != null) && event.path[0].parentElement != null)
      {
        if(event.path[0].parentElement.className != 'brdCrmbPopOut' && event.path[0].className.indexOf('hierarchyButton')== -1 && event.path[0].className.indexOf('icon-nodes') == -1 && event.path[0].id != "treeSearchBox")
        {
          var DataTable = component.find('DataTable');
          DataTable.closeDropDown();
          document.querySelectorAll('[id*="_treeContainer"]')[0].className = 'slds-tree_container slds-hide' ;
        }
      } 
      else if((event.target != undefined || event.target != null) && event.target.parentElement != null)
      {
        if(event.target.nodeName == 'svg' || event.target.nodeName == 'use')
        {
          if(event.target.parentElement.nodeName == 'svg' || event.target.parentElement.nodeName == 'use')
          {
            if(event.target.parentElement.className.baseVal != 'brdCrmbPopOut' && event.target.className.baseVal.indexOf('hierarchyButton')== -1 && event.target.id != "treeSearchBox" && event.target.className.baseVal.indexOf('icon-nodes') == -1)
            {
              var DataTable = component.find('DataTable');
              DataTable.closeDropDown();
              document.querySelectorAll('[id*="_treeContainer"]')[0].className = 'slds-tree_container slds-hide' ;
            }
          }
          else if(event.target.parentElement.className != 'brdCrmbPopOut' && event.target.className.baseVal.indexOf('hierarchyButton')== -1 && event.target.id != "treeSearchBox" && event.target.className.baseVal.indexOf('icon-nodes') == -1)
          {
            var DataTable = component.find('DataTable');
            DataTable.closeDropDown();
            document.querySelectorAll('[id*="_treeContainer"]')[0].className = 'slds-tree_container slds-hide' ;
          }
        }
        else if(event.target.parentElement.className != 'brdCrmbPopOut' && event.target.className.indexOf('hierarchyButton')== -1 && event.target.id != "treeSearchBox" && event.target.className.indexOf('icon-nodes') == -1)
        {
          var DataTable = component.find('DataTable');
          DataTable.closeDropDown();
          document.querySelectorAll('[id*="_treeContainer"]')[0].className = 'slds-tree_container slds-hide' ;
        }
      }*/
    },    
    refreshDataTable : function(component,event,helper) {

      console.log('-- refreshDataTable position universe -- ');
      console.log('event  : ',event.getParam("filterQueryWhereClause"));
      console.log('view - '+component.get("v.selectedViewName"));
      var datatable = component.find("DataTable");
      datatable.refreshData(event.getParam("filterQueryWhereClause"));
      
      if(!event.getParam("restrictListView")){
         var listViewComponent = component.find("listViewComp");
         listViewComponent.refreshListViews();
      }
    },
    viewChange : function(component,event,helper)
    {
        var datatable = component.find('DataTable');
        var viewId = event.getParam("viewId");
        var viewName = event.getParam("viewName");
        var queryString = event.getParam("queryString");


        console.log('view in viewchange- '+component.get("v.selectedViewName"));

        console.log('viewId -- '+viewId);
        console.log('viewName -- '+viewName);
        console.log('queryString in posuniverse--- '+queryString);
        console.log('invoke in posuniverse--- '+event.getParam("invokeFromListView"));

        // var $j = jQuery.noConflict();
        // $j('#cardName').text(viewName);
        component.set("v.selectedViewName",viewName);
        console.log('position  universe view change' + viewId);

        datatable.changeView(viewId,queryString,event.getParam("invokeFromListView"));
    },

    onSelectPosition : function(component,event,helper){

    	var recordId = event.getParam('recordId');
    	component.set("v.recordId",recordId);

    	var actionState = event.getParam('actionState');
        component.set("v.globalActionState", actionState);

        var teamInstanceId = event.getParam('teamInstanceId');
        console.log('teamInstanceId : '+teamInstanceId);
        component.set("v.selectedTeamInstance", teamInstanceId);
        
        var teamType = event.getParam('teamType');
        console.log('teamType : '+teamType);
        component.set("v.selectedTeamType", teamType);
    },

    positionActionOpen: function(component, event, helper) {
        console.log('positionActionOpen called');
        
        var actionTypeValue = event.getParam("actionType");

        if(actionTypeValue != 'Dynamic Style'){
            var modalTitleValue = event.getParam("modalTitle");
            var selectedPositionValue = event.getParam("selectedPosition");
            var selectedTeamInsValue = event.getParam("teamInstance");
            var selectedTeamType = event.getParam("teamType");
            
            console.log('showCreatePositionPopup : '+component.get('v.showCreatePositionPopup'));
            console.log('showFilterPositionPopup  - '+component.get("v.showFilterPositionPopup"));
            console.log('actionTypeValue : '+actionTypeValue);
            console.log('modalTitleValue : '+modalTitleValue);
            console.log('--- 2 '+component.get("v.queryString"));
            
            component.set("v.selectedAction", actionTypeValue);
            component.set("v.selectedTitle", modalTitleValue);
              
              console.log('-- recordid -- '+component.get("v.recordId"));
              console.log('-- selectedPositionValue -- '+selectedPositionValue);
              if(actionTypeValue != 'Create Position'){
                 component.set('v.recordId',selectedPositionValue);
              }
              component.set('v.selectedPositionTeamInstance',selectedTeamInsValue);
              component.set('v.selectedTeamType',selectedTeamType);

              if(actionTypeValue == 'Filter Position'){
                component.set('v.showFilterPositionPopup', true);
                component.set("v.queryString",event.getParam("queryString"));
              }
              else{
                  component.set('v.showCreatePositionPopup', true);
              }
        }
       
        
    },

    positionActionClose: function(component) {
        component.set('v.showCreatePositionPopup', false);
    },
    leftslider : function(component,event,helper)
    {
      var modalListView =  component.find('listViewComp');
      modalListView.slider('left');
    },
    rightslider : function(component,event,helper)
    {
      
      var modalListView =  component.find('listViewComp');
      modalListView.slider('right');
      
    },
    hideListViewButtons: function(component,event,helper)
      {
        var anchorButton = component.find('anchorButton');
        $A.util.addClass(anchorButton,'slds-hide');
      },

    updateCountryAttributes: function(component, event, helper) 
    {
        console.log('Caught update attribute - Call Plan Controller');
        var datatable = component.find('DataTable');
        var countryId = event.getParam('countryId');
        datatable.updateCountryAttributes(countryId);

        var flag = component.find('flagComponent');
        flag.updateCountryAttributes(countryId);
    },
    resetFilterConditions:  function(component, event, helper) 
    {      
        component.set("v.clearFilters",true);
        component.set("v.clearFilters",false);
        console.log('filters cleared');
    }
    
})