({
    toggle: function(component, event, helper) {
        component.set("v.expanded", !component.get("v.expanded"));
    },

    selectTreeNode : function(component,event,helper)
    {
        var node = component.get("v.node");
        var item = component.find('treeNodeInnerDiv');
        var $j = jQuery.noConflict();
        
        // For call plan module, every non-territory row shouldn't be selected
        if(component.get("v.moduleName") == 'Call Plan') {
            if(node.PositionLevel == '1') {
                if(!$A.util.hasClass(item,'slds-is-selected'))
                {
                    $j('.slds-tree .slds-is-selected').removeClass('slds-is-selected');
                }
                $j(event.target.parentElement).addClass('slds-is-selected');
            }
        } else {
            if(!$A.util.hasClass(item,'slds-is-selected'))
            {
                $j('.slds-tree .slds-is-selected').removeClass('slds-is-selected');
            }
            $j(event.target.parentElement).addClass('slds-is-selected');
        }

        if(component.get("v.moduleName") != 'Call Plan' || (component.get("v.moduleName") == 'Call Plan' && node.PositionLevel == '1')) {
            var evt = $A.get("e.c:selectTreeNodeEvent");
            evt.setParams({
                selectedTreeNode : node.Id
            });           
            evt.fire();
        }
    },

    searchKeyChange: function(component, event) 
    {
            // var x = component.find("treeNodeOuterDiv");
            // if(x != undefined )
            //     x.set("v.body", []);
            // component.set("v.node", {});
            // component.set("v.isSearch",true);
       
	},

    breadcrumbChange : function(component,event,helper)
    {
        var x = component.find("treeNodeOuterDiv");
        //x.set("v.body", []);
        if(x != undefined){
            x.set("v.body", []);
        }
        component.set("v.node", {});
    },

    showToolTip : function(component,event,helper)
    {
    	
        var idx = parseInt(event.target.id.split('_')[1]);
        if(!idx)
            return false;
       /* if(event.target.id.indexOf('popover_'+idx) != -1 || event.target.id.indexOf('dialog-body-id_'+idx) != -1 || event.target.id.indexOf('crList_'+idx) != -1 || event.target.id.indexOf('dialogText_'+idx) != -1)
            return false;*/
        var eventPos = event.target.offsetTop;
        var ulscroll = document.getElementById('ulDiv').scrollTop;
        //document.getElementById('popover_'+idx).classList.remove('slds-hide');
        // document.getElementById('popover_'+idx).classList.add('tootlTipShow');
        document.getElementById('popover_'+idx).style.top = (eventPos - ulscroll - 27) + 'px';
        // document.getElementById('popover_'+idx).style.top = (event.pageY- 90)/2;
    },

    updateSelectedNode: function(component,event,helper){

        var selectNodeEvent = $A.get("e.c:SelectChange");
        selectNodeEvent.setParams({
            "selectedValuesString": event.getSource().get("v.name"),
            "isSelectedNode" : event.getSource().get("v.value")
        });
        selectNodeEvent.fire();       
    },

})