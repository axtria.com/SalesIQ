({
	setBusinessRulesData : function(component,event,helper,tableData) {
		 	var updatedwrapperlist = tableData;
            console.log('updatedwrapperlist');
            console.log(updatedwrapperlist);

            var ns =  ( component.getType().split(':')[0] == '' ) ? '' : component.getType().split(':')[0];
            console.log('namespace val' +ns);

            if(updatedwrapperlist != null && updatedwrapperlist.length > 1 ){

                updatedwrapperlist.shift();
                component.set('v.BRMRuleInfoWrapperList',updatedwrapperlist);
                component.set('v.isProductEnabled',updatedwrapperlist[0]['isProductEnabled']);


                var $j = jQuery.noConflict();
                if($j(document.getElementById('table-1')) != null || $j(document.getElementById('table-1')) != undefined)
                {
                    $j(document.getElementById('table-1')).remove();
                    $j(document.getElementById('table1_container')).append("<table id='table-1' aura:id='rulesdatatable' class='slds-table slds-table_bordered slds-table_cell-buffer first_table' style='width:100%;'> </table>");
                }
                
                component.set('v.disableProductLevelRules', true);
                component.createRulesDataTable();
            }
            else{
                component.set('v.updatedwrapperlist','');
                component.set('v.disableProductLevelRules', false);
                component.set('v.isProductEnabled',updatedwrapperlist[0]['isProductEnabled']);
        }
	},

    closeConfirmationModal : function(component){
        component.set('v.showConfirmationModal',false);
    },

    updataTableHeight : function(component,event,helper,className){
        var tableCmp = component.find('rules_datatable');
        $A.util.addClass(tableCmp, className);
    },
    
    getBusinessRuleNames : function(component,event,helper){
        
        try{
            var brlist = component.get('v.BRMRuleInfoWrapperList');
            var rulesnames = brlist.map( function(d) { 
                return d.rulename.toLowerCase();
            });
            debugger;
            return rulesnames;
            
        }catch(e){
           console.log(e); 
        }
        
        
        
    },

    checkBusinessRulesList : function(component){
        try{
            var brlist = component.get('v.BRMRuleInfoWrapperList');
            console.log(brlist);
            if(brlist == undefined || brlist.length == 0 ){
                return false;
            }
            else if( brlist != undefined && brlist.length > 0){
                return true;
            }
            else{
                return true;
            }
        }catch(e){
            console.log(e);
        }   
    }
})