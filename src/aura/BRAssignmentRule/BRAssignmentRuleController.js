({
    doInit : function(component, event, helper) 
    {

        var scenarioId = component.get('v.record_Id');
        var workspaceId = component.get('v.workSpaceId');
        var scenarioRuleInstanceId = component.get('v.scenarioRuleInstanceId');
        if(component.get('v.scenarioId') == undefined || component.get('v.scenarioId') == null){
            component.set('v.scenarioId',component.get('v.record_Id'));
        }

         //SET ORG NAMESPACE ON INIT
        let ns = ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0]+'__';
        

        var action = component.get("c.getInputOutputDatasources");
        action.setParams({
            scenarioId : component.get('v.scenarioId'),
            scenarioRuleInstanceId : scenarioRuleInstanceId
        });
      
        action.setCallback(this, function(response) {
            component.showSpin();
            var state = response.getState();
            if(state === 'SUCCESS'){

                let result = response.getReturnValue();
                let dataSetAttributes,dataSetColumnAttributes;

                if( result['dataSetAttributes'] != undefined ){
                    dataSetAttributes = result['dataSetAttributes'] ;
                }

                if( result['dataSetColumnAttributes'] != undefined ){
                    //POPULATE DATA SET COLUMNS 
                    dataSetColumnAttributes = result['dataSetAttributes'] ;
                    if(dataSetColumnAttributes.length > 0){
                        component.set('v.assignmentFieldsWrapperList',dataSetColumnAttributes);
                        component.createInfoTable();
                    }
                    else{
                        component.set('v.assignmentFieldsWrapperList','');
                    }
                }


                let inputDataSetKey = ( component.get('v.brModuleName') == 'BRAffilationRules') ? 'Affiliation' : 'Assignment';

                var tableclassName = ( inputDataSetKey == 'Assignment' ) ? 'siq_table_height_top_table' : 'siq_table_height_bottom_table';

                //SET DATATABLE HEIGHT
                helper.updataTableHeight(component,event,helper,tableclassName);

                if(result['listBusinessrules']!= undefined ){
                    //POPULATE BUSINESS RULES 
                    helper.setBusinessRulesData(component,event,helper,result['listBusinessrules']);
                    //SET AFFILIATION ATTRIBUTES IF EXISITING BR RULES
                        if(result['levelOptions']!= undefined  && result['levelOptions']!= null ){
                            if(inputDataSetKey == 'Affiliation'){
                                component.set('v.levelOpts', result['levelOptions']);
                            }
                        }
                    
                        if(inputDataSetKey == 'Affiliation'){

                            if(result['listBusinessrules'][0]['ruleType'] != undefined || result['listBusinessrules'][0]['ruleType'] != null || result['listBusinessrules'][0]['ruleType'] != '' ){
                                component.set("v.affiliationRuleType",result['listBusinessrules'][0]['ruleType']);
                                component.set("v.geographicPreference", result['listBusinessrules'][0]['geographicPreference'] );
                                component.set("v.affiliationLevel",result['listBusinessrules'][0]['level']);


                                try{

                                    var affiliationAttributes = {};
                                    affiliationAttributes['ruleType'] = result['listBusinessrules'][0]['ruleType'];
                                    affiliationAttributes['geographicPreference'] = result['listBusinessrules'][0]['geographicPreference'];
                                    affiliationAttributes['level'] = result['listBusinessrules'][0]['level'];
                                    component.set('v.affiliationAttributes',affiliationAttributes);

                                }catch(e){

                                }
                               


                            }
                            component.set('v.isChanged',false);
                        }
                }

                
               
 

                if( dataSetAttributes!= undefined && Object.keys(dataSetAttributes).length > 0 && dataSetAttributes!= undefined && dataSetAttributes[inputDataSetKey] != undefined && dataSetAttributes['Base'] != undefined && dataSetAttributes['Output'] != undefined ){


                    component.set("v.dataSetMap", dataSetAttributes);
                    let inputDataSetKey = ( component.get('v.brModuleName') == 'BRAffilationRules') ? 'Affiliation' : 'Assignment';


                    if(inputDataSetKey != 'Affiliation'){
                        //INIT COLUMNS TABLE
                        component.fieldsTabSelected();
                    }
                        

                    //SET COUNTRY ID AND TEAM INSTANCE
                    //scenario_rule_instance_id__r.Scenario_Id__r.Team_Instance__c
                    component.set('v.countryId', dataSetAttributes[inputDataSetKey][ns+'scenario_rule_instance_id__r'][ns+'Scenario_Id__r'][ns+'Country__c']);

                    component.set('v.teamInstance', dataSetAttributes[inputDataSetKey][ns+'scenario_rule_instance_id__r'][ns+'Scenario_Id__r'][ns+'Team_Instance__c']);

                    console.log("Team instance value"+ component.get("v.teamInstance"));
                        
                    //SET DATA SOURCES TABLES NAMES
                    component.set("v.assignmentTableName", dataSetAttributes[inputDataSetKey][ns+'Table_Display_Name__c']);

                    component.set("v.baseTableName", dataSetAttributes['Base'][ns+'Table_Display_Name__c']);

                    console.log(dataSetAttributes['Output'][ns+'Table_Display_Name__c']);
                    
                
                    component.set("v.outputTableName", dataSetAttributes['Output'][ns+'Table_Display_Name__c']);

                    //console.log('dataSetMap');
                    //console.log(dataSetMap);

                    component.set('v.hierarchyOptions', result['affilationLevels']);
                    component.set('v.ruleLevelOptions', result['ruleLevelOptions']);
                 
                  
                     //LOAD TREE HIERARCHY INIT
                    var treeHierarchyComp = component.find("treeHierarchy");
                    if(treeHierarchyComp != undefined )
                    treeHierarchyComp.refreshTree(dataSetAttributes[inputDataSetKey][ns+'scenario_rule_instance_id__r'][ns+'Scenario_Id__r'][ns+'Country__c']);

                    var addRuleButton = component.find('addRuleButton');
                    $A.util.removeClass(addRuleButton, "slds-hide");
                    component.hideSpin();
                }

                else{
                    component.showErrorToast();
                    component.hideSpin();
                }
            }
            else{
              console.log("Failed with state:  "+ state);
               component.showErrorToast();
               component.hideSpin();
            }
        });
            
        $A.enqueueAction(action);
    },

    showChanges :  function(component,event,helper){
        //console.log("ruleAttributes has changed");
        //console.log("old value: " + event.getParam("oldValue"));
        //console.log("current value: " + event.getParam("value"));

        //var is_changed = component.get('v.isChanged');
        //if(is_changed == true){
        $A.enqueueAction(component.get('c.updateRuleAttributes'));
        //}
       
       

      
    },

    loadTree :  function(component){
        //UPDATE TREE BASED ON SELECTED POSITION
        var treeHierarchyComp = component.find("treeHierarchy");
        console.log('treeHierarchyComp load tree called');
        console.log(treeHierarchyComp);
        if(treeHierarchyComp != undefined ){
            treeHierarchyComp.updateTree();
            treeHierarchyComp.openTree();
        }
    },

    showErrorToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        var dsnotavailable = $A.get("$Label.c.Datasources_Not_Available");
        toastEvent.setParams({
            message:dsnotavailable,
            messageTemplate: dsnotavailable,
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
    },

    /* This method gets the rows from BusinessRules for datatable */
    getAssignmentBusinessRules : function(component,event,helper){
      
        var scenarioId = component.get('v.scenarioId');
        var scenarioRuleInstanceId = component.get('v.scenarioRuleInstanceId');
      
        var action2=component.get('c.getBusinessRules');
        var isAffilaitionRules = ( component.get('v.brModuleName') == 'BRAffilationRules') ? true : false;
        action2.setParams({ 
            "scenarioId" : scenarioId,
            "scenarioRuleInstanceId" : scenarioRuleInstanceId,
            "isAffiliationRule" : isAffilaitionRules
        });
        action2.setCallback(this, function(data){
            var updatedwrapperlist = data.getReturnValue();
            var ns =  ( component.getType().split(':')[0] == '' ) ? '' : component.getType().split(':')[0];
            try{
                if(updatedwrapperlist != null && updatedwrapperlist.length > 1 ){
                    updatedwrapperlist.shift();
                    component.set('v.BRMRuleInfoWrapperList',updatedwrapperlist);
                    component.set('v.isProductEnabled',updatedwrapperlist[0]['isProductEnabled']);
                    var $j = jQuery.noConflict();
                    if($j(document.getElementById('table-1')) != null || $j(document.getElementById('table-1')) != undefined){
                        $j(document.getElementById('table-1')).remove();
                        $j(document.getElementById('table1_container')).append("<table id='table-1' aura:id='rulesdatatable' class='slds-table slds-table_bordered slds-table_cell-buffer first_table' style='width:100%;'> </table>");
                    }
                    component.set('v.disableProductLevelRules', true);
                    component.createRulesDataTable();
                }
                else{
                    component.set('v.updatedwrapperlist','');
                    component.set('v.disableProductLevelRules', false);
                    component.set('v.isProductEnabled',updatedwrapperlist[0]['isProductEnabled']);
                }
            }catch(ex){
                console.log(ex);
            }
            
        })
        
        $A.enqueueAction(action2);
    },

    createRulesDataTable : function(component, event, helper)
    {
        component.showSpin();

        var editHint;
        var deleteHint;
        var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
        console.log('namespace val' +namespace);
        
        if(namespace != ''){
            editHint = $A.get("$Label.AxtriaSalesIQTM.Edit_Menu_Label");
            deleteHint = $A.get("$Label.AxtriaSalesIQTM.Delete_Menu_Label");
        } else {
            editHint = $A.get("$Label.c.Edit_Menu_Label");
            deleteHint = $A.get("$Label.c.Delete_Menu_Label");
        }

    	var html='<div class="slds-button-group editDeleteGroup" role="group">';
        html=html+'<button class="slds-button slds-button_icon slds-button_icon-border-filled edit" aria-pressed="false" title='+editHint+'>';
        html=html+'<span class="slds-icon_container slds-icon-utility-edit" data-aura-rendered-by="138:0"><span data-aura-rendered-by="141:0" class="lightningPrimitiveIcon" data-aura-class="lightningPrimitiveIcon">';
        html=html+'<div class="icon-edit"></div>';
        html=html+'</span></span><span class="slds-assistive-text">Edit</span>';
        html=html+'</button>';
        html=html+'<button class="slds-button slds-button_icon slds-button_icon-border-filled delete" aria-pressed="false" title='+deleteHint+'>';
        html=html+'<span class="slds-icon_container slds-icon-utility-delete" data-aura-rendered-by="145:0"><span data-aura-rendered-by="148:0" class="lightningPrimitiveIcon" data-aura-class="lightningPrimitiveIcon">';
        html=html+'<div class="icon-delete"></div></span><!--render facet: 149:0--></span><span class="slds-assistive-text">Delete</span>'; 
        html=html+'</button>';
        html=html+'</div>';

        var editBtn = '<div class="slds-button-group editDeleteGroup" role="group">';
        editBtn += '<button class="slds-button slds-button_icon slds-button_icon-border-show hierarchy" aria-pressed="false" title='+editHint+'>';
        editBtn += '<span class="icon-nodes icon-svg slds-icon-text-default hierarchyButton" data-aura-rendered-by="138:0"><span data-aura-rendered-by="141:0" class="lightningPrimitiveIcon" data-aura-class="lightningPrimitiveIcon">';
        editBtn += '<div class="icon"></div>';
        editBtn +='</span></span><span class="slds-assistive-text">Edit</span>';
        editBtn +='</button>';
        editBtn +='</div>';


        console.log('Inside createRulesDataTable');
        var sessionTable;
        var $j = jQuery.noConflict();
        var BRMRuleInfoWrapperList = component.get("v.BRMRuleInfoWrapperList");
        console.log('BRMRuleInfoWrapperList');
        console.log(BRMRuleInfoWrapperList);
        var result = BRMRuleInfoWrapperList;
        var moduleName = component.get("v.brModuleName");
        let isColumnVisible = (component.get("v.brModuleName") == 'BRAffilationRules') ? false : true;



      	if(result != undefined && result.length > 0 &&  result[0]['tableHeaders'] != undefined){
            //var coulmnJsonResult = [];
            var timestampData = []; 
                try{
                    sessionTable = $j('.first_table').DataTable(
                    {                            
                        "searching": false,
                        destroy : true,
                        "dom": '<"div-tbl">t<"bottom-info"> ',
                        "data": result,
                        paging:false,
                        'orderable': true,
                        "language":
                        {
                            "emptyTable": "No Data Available"
                        },
                               
                        columns:result[0]['tableHeaders'],
                        columnDefs :
                        [
                            {
                                targets : [0],
                                'searchable': false,
                                "createdCell":function (td, cellData, rowData, row, col)
                                {   
                                    $j(td).html(html); 
                                    $j(td).width('50px');
                                }
                            },
                            {
                                targets : [1],
                                "visible" : true,
                                'searchable': false,
                                "createdCell":function (td, cellData, rowData, row, col) {   
                                                    var parsehtml;
                                                    var tdx = rowData.rulename;   
                                                    $j(td).html(tdx);
                                                }
                            },
                            {
                                targets : [2],
                                "visible" : isColumnVisible,
                                "createdCell":function (td, cellData, rowData, row, col)
                                {
                                    //var tdx = rowData.ruleValue;
                                    var tdx = rowData.reasonCode;
                                    $j(td).html(tdx);
                                }
                            },
                            {
                                targets : [3],
                                "visible" : isColumnVisible,
                                "createdCell":function (td, cellData, rowData, row, col)
                                {
                                    var tdx = rowData.keydisplaycolumnvalue;
                                                                           
                                    $j(td).html(tdx);
                                }
                            },
                            {
                                targets : [4],
                                "createdCell":function (td, cellData, rowData, row, col)
                                {
                                    //var tdx = rowData.ruleValue;
                                    var tdx = rowData.ruleLevel;
                                    $j(td).html(tdx);
                                }
                            },
                            {
                                targets : [5],
                                "createdCell":function (td, cellData, rowData, row, col)
                                {
                                    //var tdx = rowData.ruleValue;
                                    var x = rowData.selectedPositions.split(",");
                                    if(rowData.selectedPositions == ""){
                                      x.length = 0;
                                    } 
                                    console.log('x'+x.length);
                                    if(rowData.ruleLevel!="Team"){
                                      var tdx = x.length +' Positions';
                                      $j(td).html(editBtn + "&nbsp" +  tdx); 
                                    }else{
                                      var tdx = ' - ';
                                      $j(td).html(tdx);
                                    }
                                }
                            },
                            {
                                targets : [6],
                                "visible" : isColumnVisible,
                                "createdCell":function (td, cellData, rowData, row, col)
                                {
                                  
                                    flagval = false;
                                    var tdx = rowData.parsedQuery;
                                    if(tdx != undefined && tdx.length>30){
                                        flagval = true;           
                                    }else{
                                      flagval = false;         
                                    }

                                    console.log(flagval);

                                    if(flagval){ 
                                        var html='<div aura:id="expandButton123" class="slds-truncate" style="width:200px">';
                                        //html=html+tdx+'</div><button class="buttonCLass" id="expandButton'+row+'">See More</button>';
                                        html=html+tdx+'</div><a href="javascript:void(0);" id="expandButton'+row+'" class="slds-text-link">See More</a>';
                                    }else{
                                        var html='<div aura:id="expandButton123" class="slds-truncate" style="width:200px">';
                                        html=html+tdx+'</div>';
                                    }
                                    console.log(row);
                                    console.log(rowData);
                                    console.log(cellData);
                                    console.log(col);
                                    $j(td).addClass(" slds-cell-wrap")                           
                                    $j(td).html(html);
                                    $j(td).attr('title',tdx);
                                }
                            }
                        ],
                    });
                }catch(e){}


                    $j("[class*='filled edit']").on("click", function(){
                        console.log('--edit called');
                        var sessionTable = $j("[class*='first_table']").DataTable();
                        var idx = sessionTable.cell( $j(this).parents('td') ).index().row;
                        var row = sessionTable.rows(idx).data();
                        component.set('v.currentrowvalue',row);
                        component.editRuleNavigation();

                    });

                    //CLICKED TREE HIERARCHY BUTTON WHEN POSITIONS
                    $j("[class*='show hierarchy']").on("click", function(event){
                               
                        console.log('clicked open tree');
                        var sessionTable = $j("[class*='first_table']").DataTable();
                        var idx = sessionTable.cell( $j(this).parents('td') ).index().row;
                        var row = sessionTable.rows(idx).data();

                        //UPDATE POSITION 
                        var positionOffset = $j(this).offset();
                        var top = positionOffset.top - 242;
                        var left = positionOffset.left - 85;

                        
                                           
                      

                        //UPDATE SELECTED POSITIONS
                        component.set('v.selectedPositions', row[0].selectedPositions);


                        component.loadTree();
                        component.loadTree();

                        console.log(row[0].selectedPositions);
                        
                              
                    });

                    $j("[id*='expandButton']").on("click", function(){
                        console.log('--expand called');
                        var sessionTable = $j("[class*='first_table']").DataTable();
                        var idx = sessionTable.cell( $j(this).parents('td') ).index().row;
                        var row = sessionTable.rows(idx).data();
                        if($j(this).html() == "See More") {
                            $j(this).prev().removeClass('slds-truncate');
                            $j(this).html("See Less");
                        } else {
                            $j(this).prev().addClass('slds-truncate');
                            $j(this).html("See More");
                        }

                    });

                    $j("[class*='filled delete']").on("click", function(){
                        //component.showSpin();
                        console.log('--delete called');
                        //COMPONENT ERROR CODE UPDATED
                        //var idx = sessionTable.cell( $j(this).parents('td') ).index().row;
                        var sessionTable = $j("[class*='first_table']").DataTable();
                        var idx = sessionTable.cell( $j(this).parents('td') ).index().row;
                        var row = sessionTable.rows(idx).data();
                        component.set('v.currentrowvalue',row);
                        //var deletepopover = document.getElementById('deletepopover');
                        //deletepopover.classList.remove('slds-hide');
                        $j("[class*='deleteClassPopOver']").removeClass('slds-hide');
                        var top = $j(this).offset().top;
                        console.log('--top ' + top);
                        //deletepopover.style.top = top - 188 + 'px';
                        $j("[class*='deleteClassPopOver']").css('top',top - 188 + 'px');
                    });
                //}
                component.hideSpin();
            // });
            // $A.enqueueAction(action);
        }
    },

    expandDetails : function(component, event, helper){

        //component.get('v.scenarioId');
        var idx = event.target.id;
        console.log(idx);
        $j = jQuery.noConflict();
        
        if($j(this).html == "See More"){
            $j(this).prev().removeClass('slds-truncate');
            $j(this).html("See Less");
        } else {
            $j(this).prev().addClass('slds-truncate');
            $j(this).html("See More");
        }
    },

    showSpinner : function (component, event, helper) 
    {
        component.set("v.Spinner", true);
        console.log('showSpinner');
    },

    hideSpinner : function (component, event, helper) 
    {
        component.set("v.Spinner", false);
        console.log('hideSpinner');    
    },

    completeLoading: function (component, event, helper) 
    {
        component.set("v.Spinner", false);
        console.log('complete loading');    
    },

    editRuleNavigation : function(component,event,helper){
      
        console.log('inside editRuleNavigation');
        var currentrowvalue = component.get('v.currentrowvalue');
        var scenarioId = component.get('v.scenarioId');
        var scenarioRuleInstanceId = component.get('v.scenarioRuleInstanceId');

        component.set('v.BRMRuleInfoWrapper',currentrowvalue[0]);
        component.set('v.updateruleflag',true);
        component.set('v.isEdit',true);
        component.addRule();      
    },

    deleteExistingRule : function(component,event,helper){
        component.hideSpin();
        var currentrowvalue = component.get('v.currentrowvalue');
        var scenarioId = component.get('v.scenarioId');
        var scenarioRuleInstanceId = component.get('v.scenarioRuleInstanceId');
        console.log('--currentrowvalue ', currentrowvalue);
        var businessRuleId = currentrowvalue[0].businessRuleId;
          
        component.set('v.businessRuleId',businessRuleId);
        console.log('businessRuleId :: '+businessRuleId);
        var  deleteaction = component.get('c.ruleDelete');
        deleteaction.setParams({ 
          "businessRuleId" : businessRuleId
        });

        deleteaction.setCallback(this, function(data){
            var state = data.getState();
            if(state === 'SUCCESS')
            {
                var deleteflag = data.getReturnValue();
                if(deleteflag == true)
                {
                    component.getAssignmentBusinessRules();
                    var BRMRuleInfoWrapperList = component.get('v.BRMRuleInfoWrapperList');
                    if(BRMRuleInfoWrapperList.length == 1){
                        // document.getElementById('table-1').destroy();
                        // table.destroy();
                        var resetRulesTable  = new Array();
                        component.set('v.BRMRuleInfoWrapperList',resetRulesTable);
                        $j = jQuery.noConflict();
                        $j('#table-1').remove();
                    }
                    $j = jQuery.noConflict();
                    $j('.deleteClassPopOver').addClass('slds-hide');
                }
            }                        
        });
        $A.enqueueAction(deleteaction);
    },

    hidepopover : function(component,event,helper){
        //document.getElementById('deletepopover').classList.add('slds-hide');
        $j = jQuery.noConflict();
        $j('.deleteClassPopOver').addClass('slds-hide');
    },

    addRule : function(component,event,helper)
    {
        component.set('v.isDisabled',false);

        let countryId = component.get('v.countryId');
        console.log('countryId :'+countryId);

        var attr = {};
             attr['affiliationRuleType'] = component.get('v.affiliationRuleType');
             attr['geographicPreference'] = component.get('v.geographicPreference');
             attr['affiliationLevel'] = component.get('v.affiliationLevel');

        var brRule = component.get('v.BRMRuleInfoWrapper');
     

        var br_id = brRule != undefined && brRule['businessRuleId']  != undefined ? brRule['businessRuleId'] : undefined; 
        var br_name = brRule != undefined && brRule['rulename']  != undefined ? brRule['rulename'] : undefined;
        var sel_positions =   brRule != undefined && brRule['selectedPositions']  != undefined ? brRule['selectedPositions'] : ''; 
        var rule_level =  brRule != undefined && brRule['ruleLevel']  != undefined ? brRule['ruleLevel'] : '' ;

        var  ra = component.get('v.dataSetMap');
        var pos = component.get('v.selectedPositions');
        var rulenames = helper.getBusinessRuleNames(component,event,helper);


        if(component.get("v.brModuleName") == 'BRAffilationRules'){

            component.set('v.isChanged',true);

            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:BRAffilationRules",
                componentAttributes: {
                    ruleAttributes : attr,
                    scenarioId: component.get('v.scenarioId'),
                    scenarioRuleInstanceId: component.get('v.scenarioRuleInstanceId'),
                    workSpaceId : component.get('v.workSpaceId'),
                    namespace:component.get("v.namespace"),
                    brModuleName : "BRAffiliationRules",
                    alreadyExistingRuleFlag : component.get('v.isEdit'),
                    selectedTeamInstance : component.get("v.teamInstance"),
                    selectedBusinessRuleId : br_id,
                    countryId : component.get('v.countryId'),
                    record_Id : component.get('v.record_Id'),
                    rulename : br_name,
                    ruleLevel : rule_level,
                    selectedPositions : sel_positions,
                    rulenames : rulenames,
                    ruleLevelOptions : component.get('v.ruleLevelOptions')
                },
                isredirect : true
            });
            evt.fire(); 

        }else{
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:BRDeriveAssignmentRule",
                componentAttributes: {
                    record_Id : component.get('v.scenarioId'),
                    workSpaceId : component.get('v.workSpaceId'),
                    scenarioRuleInstanceId : component.get('v.scenarioRuleInstanceId'),
                    updateruleflag : component.get("v.updateruleflag"),
                    BRMRuleInfoWrapper : component.get("v.BRMRuleInfoWrapper"),
                    brModuleName : "BRAssignmentRules",
                    rulenames : rulenames,
                    dataSetMap : component.get('v.dataSetMap'),
                    assignmentFieldsWrapperList : component.get("v.assignmentFieldsWrapperList"),
                    summarizedViewFlag : component.get("v.summarizedViewFlag"),
                    namespace : component.get('v.namespace'),
                    isEdit : component.get("v.isEdit"),
                    selectedTeamInstance : component.get("v.teamInstance"),
                    countryId : component.get("v.countryId"),
                    isProductEnabled : component.get("v.isProductEnabled")
                },
                isredirect : true
            });

            evt.fire();

        }
        
        // var treeHierarchyComp = component.find("treeHierarchy");
        // treeHierarchyComp.destroy();
        // component.destroy();
    },

    closeAssnRuleButton : function(component, event, helper) {
      
        component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
        var scenarioId = component.get('v.scenarioId');
        if(scenarioId == '' || scenarioId == undefined || scenarioId == null){
            scenarioId = component.get('v.record_Id');
        }
        
        var workSpaceId = component.get('v.workSpaceId');
        var ischanged = component.get('v.isChanged');


        if( (component.get('v.brModuleName') == 'BRAffilationRules' ) && (ischanged == false)){

            
                var affiliationAttributes = component.get('v.affiliationAttributes');
                var attr = {};
                attr['affiliationRuleType'] =  affiliationAttributes['ruleType'] ;
                attr['geographicPreference'] =  affiliationAttributes['geographicPreference'] ;
                attr['affiliationLevel'] =  affiliationAttributes['level'] ;


                var srid = component.get('v.scenarioRuleInstanceId');
                //if(component.get('v.isChanged')){
                var action=component.get('c.updateAffiliationRuleAttributes');
                action.setParams({ 
                    scenarioRuleInstanceId : component.get('v.scenarioRuleInstanceId'),
                    ruleAttributes: attr
                });

                action.setCallback(this, function(response){
                   console.log(response);
                });

                $A.enqueueAction(action);
            
        }


        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:CPGbusinessRuleCanvas",
            componentAttributes: {
                record_Id : scenarioId,
                viewcanvas : "Y",
                workSpaceId : workSpaceId,
                summarizedViewFlag : component.get("v.summarizedViewFlag")
            }
        });


         try{
              var treeHierarchyComp = component.find("treeHierarchy");
              treeHierarchyComp.destroy();
          }catch(e){
              console.log(e);
          }


        evt.fire();
        component.destroy();
    },

    fieldsTabSelected : function(component,event,helper){

        let inputDataSetKey = ( component.get('v.brModuleName') == 'BRAffilationRules') ? 'Affiliation' : 'Assignment';
        
        component.showSpin();

        var addRuleButton = component.find('addRuleButton');
        $A.util.addClass(addRuleButton, "slds-hide");

        let ns = ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0]+'__';

        var action2=component.get('c.getColumns');
        action2.setParams({ 
            "basedatasetId" : component.get("v.dataSetMap")['Base'][ ns+'dataset_id__c'],
            "assignmentdatasetId" : component.get("v.dataSetMap")['Assignment'][ns+'dataset_id__c']
        });

        debugger;
        action2.setCallback(this, function(data){
            var lstColumnData= data.getReturnValue();
            console.log('lstColumnData');
            if(lstColumnData.length > 0){
            component.set('v.assignmentFieldsWrapperList',lstColumnData);
                component.createInfoTable();
            }
            else{
                component.set('v.assignmentFieldsWrapperList','');
            }
            component.hideSpin();
        })
        
        $A.enqueueAction(action2);
    },


    createInfoTable : function(component, event, helper)
    {
        component.showSpin();
        console.log('Inside createInfoTable');
        var sessionTable;
        var $j = jQuery.noConflict();
        var assignmentFieldsWrapperList = component.get("v.assignmentFieldsWrapperList");
        console.log('assignmentFieldsWrapperList');
        console.log(assignmentFieldsWrapperList);
        
        var result = assignmentFieldsWrapperList;
        var coulmnJsonResult = [];

        var parsedResult = JSON.parse(JSON.stringify( result, ["statusFlag","datasourceName","datasourceColumnName","variableType","colDatatype","colValuetype"] , 6)); 
        var element = parsedResult[0];
          
        for(var key in element){
            if(key=='statusFlag'){
                coulmnJsonResult.push({"data":key,"title":'       ',defaultContent:''});
            }
            else if(key=='datasourceName'){
                coulmnJsonResult.push({"data":key,"title":'DATASOURCE',defaultContent:''});
            }
            else if(key=='datasourceColumnName'){
                coulmnJsonResult.push({"data":key,"title":'FIELDS',defaultContent:''});
            }
            else if(key=='variableType'){
                coulmnJsonResult.push({"data":key,"title":'TYPE',defaultContent:''});
            }
            else if(key=='colDatatype'){
                coulmnJsonResult.push({"data":key,"title":'DATA TYPE',defaultContent:''});
            }
            else if(key=='colValuetype'){
                coulmnJsonResult.push({"data":key,"title":'VALUE TYPE',defaultContent:''});
            }
                
        }

        try{
            sessionTable = $j('.fields_table').DataTable(
            {
                "searching": false,
                destroy : true,
                "dom": '<"div-tbl">t<"bottom-info"> ',  
                columns:coulmnJsonResult,
                "data": result,
                paging:false,
                'orderable': false,
                'scrollY': '200px',
                
                "language":
                {
                    "emptyTable": "No Data Available"
                },
                
                columns:coulmnJsonResult,
                columnDefs :
                [
                            
                    {
                        targets : [0],
                        "visible" : true,
                        'searchable': false,
                        'orderable': false,
                        "createdCell":function (td, cellData, rowData, row, col)
                        {   
                            var parsehtml;
                            if(rowData.statusFlag == true){
                                parsehtml="<input type='checkbox' name='options' id='checkbox_"+row+"' disabled='true' checked='checked' value='" + rowData.statusFlag + "' />";
                            }
                            else if(rowData.statusFlag == false){
                                parsehtml="<input type='checkbox' name='options' id='checkbox_"+row+"' disabled='true' value='" + rowData.statusFlag + "'/>";
                            }
                            
                            $j(td).html(parsehtml);
                        }
                    },
                    {
                        targets : [1],
                        "createdCell":function (td, cellData, rowData, row, col)
                        {
                            var tdx = rowData.datasourceName;
                            $j(td).html(tdx);
                        }
                    },

                    {
                        targets : [2],
                        "createdCell":function (td, cellData, rowData, row, col)
                        {   
                            $j(td).html(rowData.datasourceColumnName);
                        }
                    },
                    {
                        targets : [3],
                        "createdCell":function (td, cellData, rowData, row, col)
                        {   
                            $j(td).html(rowData.variableType);
                        }
                    },
                    {
                        targets : [4],
                        "createdCell":function (td, cellData, rowData, row, col)
                        {   
                            $j(td).html(rowData.colDatatype);
                        }
                    },
                    {
                        targets : [5],
                        "createdCell":function (td, cellData, rowData, row, col)
                        {   
                            $j(td).html(rowData.colValuetype);
                        }
                    }
                ],
            });
            component.hideSpin();
        }catch(e){}
    },

    closeTreeDropDown : function(component,event){
        var ev = event.target;
        var cls = ev.classList[0];
        var classSet = new Set(['icn','slds-button','uiInput','icon-nodes','slds-tree__item','slds-m-right_x-small','slds-input','slds-icon',undefined,'slds-is-expanded','slds-truncate','slds-tree']);
        if(!classSet.has(cls)){
            $A.get("e.c:modalCloseEvent").fire();  
        } 
    },

    toggleProductEnable :  function(component,event){
        var action=component.get('c.setProductEnable');
        action.setParams({ 
            "scenarioId" : component.get('v.scenarioId'),
            "isProductEnabled" : component.get('v.isProductEnabled')
        });
        action.setCallback(this, function(response){
           console.log(response);
        });
        $A.enqueueAction(action);
    },

    onRuleTypeChange : function(component,event,helper){    
            //let showLevelTypeInfo =  (component.get('v.affiliation_rule_type') == "Bottom Up") ? true : false;
            //component.set('v.affiliation_rule_type', showLevelTypeInfo);
    },

    updateRuleAttributes : function(component,event,helper){
        var attr = {};
        attr['affiliationRuleType'] = component.get('v.affiliationRuleType');
        attr['geographicPreference'] = component.get('v.geographicPreference');
        attr['affiliationLevel'] = component.get('v.affiliationLevel');

        var srid = component.get('v.scenarioRuleInstanceId');
        //if(component.get('v.isChanged')){
        var action=component.get('c.updateAffiliationRuleAttributes');
        action.setParams({ 
            scenarioRuleInstanceId : component.get('v.scenarioRuleInstanceId'),
            ruleAttributes: attr
        });

        action.setCallback(this, function(response){
           //console.log(response);
        });

        $A.enqueueAction(action);
        //}
        //helper.closeConfirmationModal(component);
        //close component -> back to canvas
        //$A.enqueueAction(component.get('c.closeAssnRuleButton'));
       
    },

    openConfirmationModal : function(component,event,helper){
        var isRuleExisting = helper.checkBusinessRulesList(component);
        component.set('v.isBRRuleExisting',isRuleExisting);
        component.set('v.isChanged', true);
        if(!isRuleExisting){
            var confirmationMessageAf = 'You cannot save until an affiliation criteria is created. Please create a criteria before saving.';
            component.set('v.confirmationMessage',confirmationMessageAf);
            component.set('v.showConfirmationModal',true);
        }
        else{
            //save affiliation rules
            //close modal
            $A.enqueueAction(component.get('c.updateRuleAttributes'));
            //close component -> back to canvas
            $A.enqueueAction(component.get('c.closeAssnRuleButton'));
            // if(component.get('v.isChanged')){
            //     var confirmationModalMessage = 'Changes will be applied accross all affiliation rules. Are you sure to keep these changes?';
            //     component.set('v.confirmationMessage',confirmationModalMessage);
            //     component.set('v.showConfirmationModal',true);
            // }else{
            //$A.enqueueAction(component.get('c.updateRuleAttributes'));
            //}  
        }
    },

    closeConfirmationModal : function(component){
        component.set('v.showConfirmationModal',false);
    }
})