({
    doInit : function(component, event, helper) {   
       
        component.set("v.Spinner", true); 
        var params = event.getParam('arguments');
        if (params) {
            var param1 = params.param1;
            var param2 = params.param2;
            var param3 = params.param3;
            var param4 = params.param4;
            var param5 = params.param5;
            var param6 = params.param6;
            var param7 = params.param7;
            var param8 = params.param8;
            
            component.set("v.currentScenarioId",param1);
            component.set("v.previousScenarioId",param2) ;
            component.set("v.ScenarioName",param3);
            component.set("v.PrevScenarioName",param4) ;
            component.set("v.currentInstance",param5) ;
            component.set("v.teamName",param6) ;
            component.set("v.url",param7) ;
            component.set("v.columns",param8) ;
            
        }
        if(!param1  || param1=='undefined' ){
            component.set("v.Spinner", false); 
        }
        else{
            
            var currentScenarioId = component.get("v.currentScenarioId");
            var previousScenarioId = component.get("v.previousScenarioId");
            var tab_id = "segment_sum";
            if(component.get("v.currentInstance")){  
                var currentins =   component.get("v.currentInstance").replace('/','');
            }
            var action = component.get("c.generateVisualizationReport");
            var url=component.get("v.url");
            var res = url+'?scenario_1='+currentScenarioId+'&scenario_2='+previousScenarioId+'&tab_id='+tab_id+'&team='+param6+'&context='+currentins;
            console.log('res in this-->'+res);
            action.setParams({
                "url": res
            });
            
            action.setCallback(this,function(response){
                var state = response.getState();
                if(state === 'SUCCESS' && response.getReturnValue()!== null)
                {
                    var divtable = component.find("divtable-1");
                    $A.util.removeClass(divtable,'slds-hide');
                    var str = "Segment wise Plan Breakup";
                    component.set("v.heading",str);
                    var noData = component.find("noData");
                    $A.util.addClass(noData,'slds-hide');
                    var div1 = component.find("div1");
                    $A.util.removeClass(div1,'slds-hide');
                    var div2 = component.find("div2");
                    $A.util.removeClass(div2,'slds-hide');
                    
                    var resultsMap = response.getReturnValue();
                    component.set("v.resultsMap",resultsMap);
                    
                    //try{
                        var labels = [];
                        var data = [];                
                        var sessionTable;
                        var $j = jQuery.noConflict();
                        var opts =  (resultsMap.CPG_Report_segment_wise);                
                        var result = opts;
                        var arrHtml=[]; 
                        arrHtml.push("<thead><tr>");
                        for(var key in result[0]){
                           arrHtml.push("<th  rowspan='1' colspan='1' style='width: 0px;'>"+key.toUpperCase()+"</th>") ;
                        }
                        arrHtml.push("</tr></thead><tbody>"); 
                        for(var ctr=0;ctr<result.length;ctr++){
                            arrHtml.push("<tr class='slds-hint-parent odd' role='row'>");
                            for(var key in result[ctr]){
                                 arrHtml.push("<td class='slds-text-align_left slds-truncate number'>"+common.myToLocaleInteger(result[ctr][key])+"</td>");
                                
                            }
                            arrHtml.push("</tr>");
                           
                            
                        }
                        arrHtml.push("</tbody>");
                        console.log(arrHtml.join(""));
                        $j('#table-1').empty().html(arrHtml.join(""));
                        
                        /*var dynamicColumns = [];
                        var i = 0;
                        $j.each(result[0], function (key, value) {
                            var obj = { sTitle: key,defaultContent:'' };
                            dynamicColumns[i] = obj;                            
                            i++;
                        });*/
                        //fetch all records from JSON result and make row data set.
                       /* var rowDataSet = [];
                        var i = 0;
                        for(var ctr=0;ctr<result.length;ctr++){
                            var rowData = [];
                            for(var key in result[ctr]){
                                rowData.push(result[ctr][key]);
                            }
                            rowDataSet.push(rowData);
                        }
                        
                        sessionTable = $j('#table-1').DataTable(
                            {
                                "processing": false,
                                "dom": '<"div-search"><"div-tbl">t<"bottom-info"> ',
                                "data": rowDataSet,
                                paging:false,                 
                                destroy : true,
                                "ordering": false,                   
                                "language":
                                {
                                    "emptyTable": "Loading Data"
                                },
                                
                                buttons: [{
                                    extend : 'csv',
                                    text : '',
                                    title : 'Call Plan',  
                                }],
                                columns: dynamicColumns,
                                "columnDefs": [{
                                   
                                    "createdCell": function (td, cellData, rowData, row, col)
                                    {   
                                        var selectHtml = '0%';
                                        if(cellData != '%')
                                            selectHtml = cellData;                
                                        $j(td).html(selectHtml);
                                    }   
                                }
                                              ],
                                "createdRow": function ( row, data, index ){
                                    $j(row).addClass('slds-hint-parent');
                                    $j('td', row).addClass('slds-text-align_left slds-truncate');
                                    $j('thead > tr> th').addClass('slds-text-title_caps');
                                    $j('thead > tr').addClass('slds-text-title_caps ');
                                    
                                },
                                
                            });  */
                        
                        
                      /*  $j("#table-1 .menu").on("click", function(){
                            
                            if(!($j(this).hasClass('slds-is-open'))){
                                $j("#table-1 .menu").removeClass('slds-is-open');
                                var idx = sessionTable.cell( $j(this).parent() ).index().row;
                                var val = ($j(this)).val();
                                var row = sessionTable.rows(idx).data();
                                $j(this).addClass('slds-is-open');
                                $j(this).children()[1].style["position"] = "inherit";
                                
                            } else {
                                $j(this).removeClass('slds-is-open');
                            }
                        });
                        component.set("v.sessionTable",sessionTable);*/
                        component.set("v.Spinner", false);
                        component.bindTable();
                        
                   /* }
                    catch(err){
                        component.set("v.Spinner", false);
                        var noData = component.find("noData");
                        $A.util.removeClass(noData,'slds-hide');
                        var div1 = component.find("div1");
                        $A.util.addClass(div1,'slds-hide');
                        var div2 = component.find("div2");
                        $A.util.addClass(div2,'slds-hide');
                        console.log('--err ' + err);
                    }*/
                    
                }
                else
                {
                    component.set("v.Spinner", false);
                    var noData = component.find("noData");
                    $A.util.removeClass(noData,'slds-hide');
                    var div1 = component.find("div1");
                    $A.util.addClass(div1,'slds-hide');
                    var div2 = component.find("div2");
                    $A.util.addClass(div2,'slds-hide');             
                }
                
            });
            $A.enqueueAction(action);
        }        
    },
    
    bindTable : function(component, event, helper){  
          
        var j$=jQuery.noConflict();
        j$('#table-3 tbody tr').remove();
        var x = component.get("v.resultsMap");
        
        var data=x["CPG_Report_valuation_comparison "];
        if(data){
            var str = "Valuation Comparison";
            component.set("v.headingCrossTable",str);
            var divCrossTable = component.find("divCrossTable");
            $A.util.removeClass(divCrossTable,'slds-hide');
        }        
        //var columns=component.get("v.columns");
        var columns=[];
        var arrColumns=x["KEYS"];
        var arrHtml=[];
        var arrthHtml=[];
        
        //var arrKeys=[];
        
        var teamName=component.get("v.teamName") ;
        for(var ctr=0;ctr<arrColumns.length;ctr++){
            columns.push(arrColumns[ctr]["column_name"]);
        }
        var keyCollection={teamName:columns};
        for(var ctr=0;ctr<data.length;ctr++){
            arrHtml.push("<tr class='number'>");
            arrthHtml.push("<tr>");
            if(ctr==0){
                arrthHtml.push("<th colspan='2' style='border:none;background:none;'></th>");
            }    
            var ctr2=0
            for(var keysCtr=0;keysCtr<keyCollection.teamName.length;keysCtr++){
                if(ctr==0 &&  keysCtr>0){
                    arrthHtml.push("<th></th>");
                }
                /*  if(keysCtr>0){
                    arrKeys.push(keyCollection.teamName[keysCtr]);
                }*/
                if(data[ctr][keyCollection.teamName[keysCtr]] && isNaN(data[ctr][keyCollection.teamName[keysCtr]])){
                    arrHtml.push("<td>"+data[ctr][keyCollection.teamName[keysCtr]].toUpperCase()+"</td>");
                }
                else{
                    arrHtml.push("<td>"+common.myToLocaleInteger(data[ctr][keyCollection.teamName[keysCtr]])+"</td>"); 
                }
                ++ctr2;
                
            }
            arrHtml.push("</tr>");
            arrthHtml.push("</tr>");
        }
        component.set("v.keys",columns); 
        j$("#table-3 thead").empty().html(arrthHtml.join(""));
        j$("#table-3 tbody").empty().html(arrHtml.join(""));
        component.formatTable();   
    },
    
    formatTable:function(component,event,helper){        
        var scenName = component.get("v.ScenarioName");
        var prevScenName = component.get("v.PrevScenarioName");
        var j$=jQuery.noConflict();
        var arrKeys=component.get("v.keys");
        j$("#table-3 > thead > tr:eq(0) > th:gt(0)").each(function(index){
            if(isNaN(arrKeys[index+1])){
                j$(this).html(arrKeys[index+1].toUpperCase());
            }
            else{
                j$(this).html(arrKeys[index+1]);
            }
            
        });
        var rowspanSize=j$("#table-3 > tbody > tr").length;
        j$("#table-3 > tbody > tr:eq(0) > td:eq(0)").before("<td rowspan='"+rowspanSize+"' style='vertical-align:middle;'>"+prevScenName+"</td>");
        var colspanCount=j$("#table-3 > thead > tr:eq(0) > th:gt(0)").length;
        j$("#table-3 > thead > tr:eq(0)").before("<tr><th colspan='2' style='border:0;background:none !important;'></th><th style='text-align:center;' colspan='"+colspanCount+"'>"+scenName+"</th></tr>");
        j$("#table-3 > tbody > tr:last-child > td:gt(0)").css({"background-color":"#abadb0","font-weight":"bold","color": "#fff"});
    },
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    destoryCmp : function (component, event, helper) {
        var sessionTable=component.get("v.sessionTable");
        sessionTable.destroy();
        component.destroy();
    }
    
})