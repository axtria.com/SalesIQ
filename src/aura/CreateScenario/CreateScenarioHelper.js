({
	getLocaleDateFormats: function(component) {
        var action = component.get("c.getLocaleDateFormats");
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue() != null)
            {
                var formatList = response.getReturnValue();
                component.set("v.dateFormat", formatList[0]); 
            }
        });
        $A.enqueueAction(action); 
    },

    initScenario : function(component,event,helper){
        var action = component.get("c.initScenario");
        action.setParams({ workSpaceId : component.get("v.recordId")});
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var result = response.getReturnValue();
                if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result))
                {   
                    console.log('===== initScenario =====');
                    console.log(result);
                    component.set("v.newScenario",response.getReturnValue());
                }
                component.getScenarioType();
                //component.callBusinessRuleTemplateOptions();
            } 
            else if(state == "ERROR"){
                helper.logException(component, response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    setOrgNamespace : function(component,event,helper){
        //GET NAMESPACE
        var action = component.get("c.getOrgNamespace");
        action.setCallback(this,function(response){
            var state = response.getState();
            console.log('Namespace : ',response.getReturnValue());
            if(state == 'SUCCESS' && response.getReturnValue() != null)
            {
                component.set("v.namespace",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },

    setRequiredLabels : function(component,event,helper){
        //SET REQUIRED LABELS
        var namespace = component.get("v.namespace");
        var option_none, validation_team_name_missing, validation_scenario_name_missing,
        validation_scenario_type_missing,validation_create_type_missing, validation_team_instance_missing,
        validation_file_name_missing, validation_brms_template_missing, validation_alignment_type_mission, validation_brms_data_source_missing,validation_enable_display_af_network,
        validation_convergence_level_missing,errorMessageMap = {};

        if(namespace!=''){
            option_none =  $A.get("$Label."+namespace.slice(0,-2)+".PICKLIST_NONE");
            validation_team_name_missing =  $A.get("$Label."+namespace.slice(0,-2)+".VALIDATION_TEAM_NAME_MISSING");
            validation_scenario_name_missing= $A.get("$Label."+namespace.slice(0,-2)+".VALIDATION_SCENARIO_NAME_MISSING");
            validation_geo_layer_missing =  $A.get("$Label."+namespace.slice(0,-2)+".VALIDATION_GEO_LAYER_MISSING");     
            validation_scenario_type_missing = $A.get("$Label."+namespace.slice(0,-2)+".VALIDATION_SCENARIO_TYPE_MISSING");
            validation_create_type_missing = $A.get("$Label."+namespace.slice(0,-2)+".VALIDATION_CREATE_TYPE_MISSING");
            validation_team_instance_missing= $A.get("$Label."+namespace.slice(0,-2)+".VALIDATION_TEAM_INSTANCE_MISSING");
            validation_file_name_missing = $A.get("$Label."+namespace.slice(0,-2)+".VALIDATION_FILE_NAME_MISSING");
            //validation_brms_template_missing= $A.get("$Label.AxtriaSalesIQTM.VALIDATION_BRMS_TEMPLATE_MISSING");
            validation_brms_data_source_missing = $A.get("$Label."+namespace.slice(0,-2)+".VALIDATION_BRMS_DATA_SOURCE_MISSING");
            validation_enable_display_af_network = $A.get("$Label."+namespace.slice(0,-2)+".VALIDATION_ENABLE_DISPLAY_AFFILIATION_NETWORK");
            validation_alignment_type_mission = $A.get("$Label."+namespace.slice(0,-2)+".VALIDATION_ALIGNMENT_TYPE_MISSING");
            validation_convergence_level_missing=$A.get("$Label."+namespace.slice(0,-2)+".VALIDATION_CONVERGENCE_LEVEL_MISSING");
        }else{
            option_none = $A.get("$Label.c.PICKLIST_NONE");
            validation_team_name_missing = $A.get("$Label.c.VALIDATION_TEAM_NAME_MISSING");
            validation_scenario_name_missing= $A.get("$Label.c.VALIDATION_SCENARIO_NAME_MISSING");
            validation_geo_layer_missing =  $A.get("$Label.c.VALIDATION_GEO_LAYER_MISSING");
            validation_scenario_type_missing= $A.get("$Label.c.VALIDATION_SCENARIO_TYPE_MISSING");
            validation_create_type_missing = $A.get("$Label.c.VALIDATION_CREATE_TYPE_MISSING");
            validation_team_instance_missing= $A.get("$Label.c.VALIDATION_TEAM_INSTANCE_MISSING");
            validation_file_name_missing = $A.get("$Label.c.VALIDATION_FILE_NAME_MISSING");
            validation_brms_data_source_missing = $A.get("$Label.c.VALIDATION_BRMS_DATA_SOURCE_MISSING");
            validation_enable_display_af_network = $A.get("$Label.c.VALIDATION_ENABLE_DISPLAY_AFFILIATION_NETWORK");
            validation_alignment_type_mission = $A.get("$Label.c.VALIDATION_ALIGNMENT_TYPE_MISSING");
            validation_convergence_level_missing=$A.get("$Label.c.VALIDATION_CONVERGENCE_LEVEL_MISSING");
        }

        component.set('v.noneValue',option_none);
        
        errorMessageMap['initialTeamSelect'] = [validation_team_name_missing];
        errorMessageMap['inputScenarioName'] = [validation_scenario_name_missing];
        errorMessageMap['InputSelectGeoLayer'] = [validation_geo_layer_missing];
        errorMessageMap['scenarioTypeOpts'] = [validation_scenario_type_missing];
        errorMessageMap['createTypeSelect'] = [validation_create_type_missing];
        errorMessageMap['InputSelectTeamIns'] = [validation_team_instance_missing];
        errorMessageMap['filePath'] = [validation_file_name_missing];
        errorMessageMap['selDataSource'] = [validation_brms_data_source_missing];
        errorMessageMap['InputSelectAffiliationNetwork'] = [validation_enable_display_af_network];
        errorMessageMap['InputSelectAlignType'] = [validation_alignment_type_mission];
        errorMessageMap['inputselectConvergeLevel'] = [validation_convergence_level_missing];
        component.set('v.validation_error_messages',errorMessageMap);

    },

    getGeoLayerOptions : function(component,event,helper){
        var geoLayerAction = component.get("c.getGeoLayerOptions");
        var option_none  = component.get("v.noneValue");

        geoLayerAction.setParams({countryId : component.get("v.countryId")});
        geoLayerAction.setCallback(this,function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var opts = [];
                console.log(response.getReturnValue());
                opts.push({"class": "optionClass", label: option_none, value: "None"});
                for(var obj in response.getReturnValue())
                {
                    opts.push({"class": "optionClass", label: response.getReturnValue()[obj], value: obj});
                }

                /* set default value as first, SIQ-2422 */
                if(opts.length == 2) {
                    //only one option available
                    component.set("v.newScenario.geoLayer", opts[1].value);
                }
                
                component.set("v.geoLayerOptions",opts);
            } 
            else if(state == "ERROR"){
                helper.logException(component, response.getError());
            }
        });
        $A.enqueueAction(geoLayerAction);
    },

    getTeamOptions : function(cmp,event,helper){
        var teamOptionAction = cmp.get("c.getTeamOptions");
        var option_none  = cmp.get("v.noneValue");
        teamOptionAction.setParams({countryId : cmp.get("v.countryId")});
        var isScenarioCreatedFromExisting =  cmp.get("v.isScenarioCreatedFromExisting");
        var isAlignment =  cmp.get("v.isAlignment");
        teamOptionAction.setCallback(this,function(response){
            var state = response.getState();
            if(state == "SUCCESS")
            {
                var opts = [];
                opts.push({"class": "optionClass", label: option_none, value: "None"});
                for(var obj in response.getReturnValue())
                {
                    opts.push({"class": "optionClass", label: response.getReturnValue()[obj], value: obj});
                }

                var initialTeamSelect = cmp.find("initialTeamSelect");
                initialTeamSelect.set("v.options", opts);

                /* set default value as first, SIQ-2422 */
                if(opts.length == 2) {
                    //only one option available
                    cmp.set("v.newScenario.teamName", opts[1].value);
                }

                if(isScenarioCreatedFromExisting && isAlignment){
                    var teamInsSelect = cmp.find("InputSelectTeamIns");
                    var tiOpts = [];
                    tiOpts.push({"class": "optionClass", label: option_none, value: "None"});
                    teamInsSelect.set("v.options", tiOpts);
                }
               
            } 
            else if(state == "ERROR")
            {
                helper.logException(cmp, response.getError());
            }
        });
        $A.enqueueAction(teamOptionAction);
    },

    getBaseTeamInstance: function(component,event,helper){
        component.set("v.baseteamInstance", []);
        var option_none = component.get("v.noneValue"); 
        var action = component.get("c.getTeamInstanceByBaseTeam");
        action.setParams({teamId : component.get("v.newScenario.teamName")});
        component.set("v.isValidOverlayMirror", false);
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var opts = [];
                var optsConverge = [];
                opts.push({"class": "optionClass", label: option_none, value: 'None'});
                optsConverge.push({"class": "optionClass", label: option_none, value: 'None'});   
                //var teamInstances = response.getReturnValue();
                //console.log('teamsAttributes '+teamsAttributes);
                
                for(var obj in response.getReturnValue()){
                    opts.push({"class": "optionClass", label: response.getReturnValue()[obj], value: obj});
                }
                
                
                    
                    component.set("v.isValidOverlayMirror", true);
                    component.set("v.baseteamInstance", opts);

                    component.set("v.convergeLevel", optsConverge);
                                            

            }
            else if(state == "ERROR")
            {
                helper.logException(component, response.getError());
            }            

        });
        $A.enqueueAction(action);

    },

    getCookieValue : function(component,event,helper){
        var regex = new RegExp('[; ]'+'apex__CountryID'+'=([^\\s;]*)');
        var cookieValue = (' '+document.cookie).match(regex);
        return (cookieValue!=null || cookieValue!= undefined)?unescape(cookieValue[1]):'';
    },

    updateCountryAttributes : function(component,event,helper){
        var flag = component.find('flagComponent');
        flag.updateCountryAttributes(component.get("v.countryId"));
    },

    logException : function(component, errors) {
        console.log('::: errors :::');
        console.log(errors);
        if (!$A.util.isUndefined(errors[0]) && !$A.util.isUndefined(errors[0]['message'])) 
        {
            //ERROR FOR APEX EXCEPTION RETURN VALUES
            var action = component.get("c.logException");
            var errorMessage = errors[0]['message'];

            action.setParams({
                message : errorMessage,
                module : 'Create Scenario'
            });

            action.setCallback(this,function(response)
            {
                var state = response.getState();
                if(state === 'SUCCESS')
                {
                    if(!$A.util.isUndefined(errorMessage)){
                        console.log(errorMessage);
                        component.set("v.errorList",[errorMessage]);
                        component.set('v.hasValidationFailed',true);
                    }
                }
            });
            $A.enqueueAction(action);
        }
        else 
        {
            //ERROR FOR STRING RETURN VALUES
           if(!$A.util.isUndefined(errors)){
                console.log(errors);
                component.set("v.errorList",[errors]);
                component.set('v.hasValidationFailed',true);
            }
        }
	},

    disableButton : function (component, event, helper) 
    {
        var saveBtn = component.find('btn_save');
        saveBtn.set("v.disabled",true); 
    },

    enableButton : function (component, event, helper) 
    {
        var saveBtn = component.find('btn_save');
        saveBtn.set("v.disabled",false);        
    },

    disableOption : function (aurid,component){
         component.find(aurid).set("v.disabled", true); 
    },

    enableOption : function (aurid,component){
         component.find(aurid).set("v.disabled", false); 
    },

    clearInputValue : function (aurid,component){
         component.find(aurid).set("v.value", ''); 
    },

    setInputValue : function (aurid, inputValue, component){
         component.find(aurid).set("v.value", inputValue); 
    },

    disableInputOptions : function(component,helper){
        helper.disableOption('createTypeSelect',component);
        helper.disableOption('inputScenarioName',component);  
        helper.disableOption('inputScenarioDescription',component); 
        helper.setInputValue('inputScenarioName','Exclusion List',component);
    },

    enableInputOptions : function(component,helper){
        helper.enableOption('createTypeSelect',component);
        helper.enableOption('inputScenarioName',component);
        helper.enableOption('inputScenarioDescription',component);
        helper.clearInputValue('inputScenarioName',component);
    },

    handleBlankFields : function(inputs, component ,event ,helper){
        
    },

    handleErrors : function(component,event,helper){

        // var inputs = ['scenarioTypeOpts','createTypeSelect','InputSelectAlignType','InputSelectTeamIns','InputSelectGeoLayer','initialTeamSelect', 'filePath', 'brmsTemplateOption','selDataSource','inputScenarioName'];

        // var errorList = [];
        // var validationFailed = false;
        //     try{
        //         for(var key in inputs){

        //             console.log(inputs[key]);
        //             var inputCmp = component.find(inputs[key]);
        //              if(!$A.util.isUndefined(inputCmp)){
        //                 var value = inputCmp.get("v.value");
        //                 if($A.util.isEmpty(value) || value == 'None' || value.trim().length == 0 ){

        //                     $A.util.addClass(inputCmp, 'inputError');

        //                     var errorMessage = component.get('v.validation_error_messages')[inputs[key]][0];
        //                     errorList.push(errorMessage);
                            
        //                     inputCmp.set("v.errors", [{message: errorMessage}]);               
                            
        //                     validationFailed = true;
                            

        //                     continue;
                           
        //                 }
        //                 else{
        //                     inputCmp.set("v.errors", null);   
        //                     $A.util.removeClass(inputCmp, 'inputError');

        //                     continue;
        //                 }  
        //             } 
        //         }
        //     }catch(error){
        //             console.log('error '+error);
        //     }
            


        //     component.set("v.errorList",errorList);
        //     component.set('v.hasValidationFailed',validationFailed);



        //     if(validationFailed){
        //         console.log(component.get('v.errorList'));
        //         component.set('v.showToastPopOver',true);
        //     }else{

        //         component.set('v.showToastPopOver',false);
        //     }     

        //     return validationFailed;


    },

    clearErrors : function(component,event,helper){
        try{
            //REMOVED brmsTemplateOption
            var inputs = ['inputScenarioName','InputSelectAlignType','InputSelectAffiliationNetwork','scenarioTypeOpts','createTypeSelect','InputSelectTeamIns','InputSelectGeoLayer','initialTeamSelect', 'filePath','selDataSource','inputselectConvergeLevel'];
            for(var index in inputs){
                var inputCmp = component.find(inputs[index]);
                if(!$A.util.isUndefined(inputCmp)){
                        inputCmp.set("v.errors", []);   
                        $A.util.removeClass(inputCmp, 'inputError');
                        component.set('v.hasValidationFailed',false);
                        component.set("v.errorList",[]);   
                } 
            }
        }catch(Exception){
            console.log('Exception'+Exception);
        }
        

    },

    showToastPopOver : function(component,event,helper){
        var warningIcon = component.find('validation_failed_icon');
        $A.util.removeClass(warningIcon,'slds-hide');
        var toast = component.find("validation_failed_popover");    
        $A.util.removeClass(toast,'slds-hide');
            
    }



    
})