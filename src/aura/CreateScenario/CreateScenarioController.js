({
    doinit: function(component,event,helper){
        //SET ORG NAMESPACE ON INIT
        component.set('v.namespace',(( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0]+'__'));
        helper.getLocaleDateFormats(component);
        //UPATE COUNTRY FLAG
        helper.updateCountryAttributes(component,event,helper);
        //SET ORG NAMESPACE
        helper.setOrgNamespace(component,event,helper);
        //INIT LABELS
        helper.setRequiredLabels(component,event,helper);
        //INIT SCENARIO        
        helper.initScenario(component,event,helper);
        //GET TEAM OPTIONS
        helper.getTeamOptions(component,event,helper);
        //GET GEOLAYER OPTIONS
        helper.getGeoLayerOptions(component,event,helper);
        //GET AFFLICATION NETWORK OPTIONS
        component.getAffiliationNetworkOptions();
    },

    scenarioTypeOptions : function(component){
        console.log('Inside scenarioTypeOptions');
        var option_none =  component.get('v.noneValue');
        var namespace = component.get("v.namespace");

        console.log(' namespace :::::::::  '+namespace);

        var action = component.get("c.getScenarioTypeOptions");
        action.setCallback(this, function(response){
            var result = response.getReturnValue();
            var opts = [];
            var mapFile = {};
            var creationTypeOptionsMap = {};

            opts.push({"class": "optionClass", label: option_none, value: "None"});
            Object.keys(result).forEach(function(key) {
                var val= result[key][namespace+'Attribute_Display_Name__c'];
                if(val.indexOf('Label.')>=0){
                    //var label = "$Label.c." + val.split('.')[1];
                    var label;
                    if(namespace != ''){
                        label = "$Label."+namespace.slice(0,-2)+"." + val.split('.')[1];
                    }else{
                        label = "$Label.c." + val.split('.')[1];
                    }
                    component.set('v.labelAttr',$A.getReference(label));
                    if(component.get('v.labelAttr') == ""){
                        $A.get('e.force:refreshView').fire();
                    }
                    val = component.get('v.labelAttr');
                }
                opts.push({label: key, value: key});
                mapFile[key] = key;

                //SET CREATING TYPE OPTIONS
                var crt_options  = result[key][namespace+'Scenario_Type_Options__c'].split(';')
                var opts_cr = [];
                var selectedScenarioTypeName = component.get('v.selectedScenarioType');
                console.log( '====selectedScenarioTypeName'+selectedScenarioTypeName );

                opts_cr.push({"class": "optionClass", label: option_none, value: "None"});

                for(var opt in crt_options){
                    var labelArr_ = [];
                    if(crt_options[opt].indexOf(',') >= 0 ){
                        labelArr_ = crt_options[opt].split(',');
                        var val_ = labelArr_[0];
                        if(val_.indexOf('Label.') >=0 ){
                            var label_;
                            //var label = "$Label.c." + val.split('.')[1];
                            if(namespace != ''){
                                label_ = "$Label."+namespace.slice(0,-2)+"." + val_.split('.')[1];
                            }else{
                                label_ = "$Label.c." + val_.split('.')[1];
                            }
                            component.set('v.labelAttr',$A.getReference(label_));
                            val_ = component.get('v.labelAttr');
                            if(component.get('v.labelAttr') == ""){
                                $A.get('e.force:refreshView').fire();
                            }
                        }
                        opts_cr.push({label: val_, value: labelArr_[1]});
                    }else{
                        opts_cr.push({label: crt_options[opt], value: crt_options[opt]});
                    }
                }
                creationTypeOptionsMap[key] = opts_cr;
            });

            

            //scenarioOptionsMap
            component.set('v.scenarioOptionsMap',creationTypeOptionsMap);
            component.set('v.scenarioTypeOptions', opts);
            component.set('v.selectedMap',mapFile);

            /* set default value as first, SIQ-2422 */
            if(opts.length == 2) {
                component.set('v.selectedScenarioType', opts[1].value);
                opts[1].selected = true;
            }
            
            component.getCreationType();

            
            //TBD IN LOCALISATION
            /*var selectedScenarioType = component.find("scenarioTypeOpts");
            var selectedScenarioTypeName = selectedScenarioType.get("v.value");
            selectedScenarioTypeName = component.get('v.selectedMap')[selectedScenarioTypeName];*/
            var selectedScenarioTypeName = component.get('v.selectedScenarioType');

            var businessRuletempaction = component.get("c.getBusinessRuleTemplateOptions");
            businessRuletempaction.setParams({ScenarioTypeName : selectedScenarioTypeName});

            businessRuletempaction.setCallback(this, function(response) {
                var mapFile = response.getReturnValue();
                var opts = [];
                var index = 1;
                opts.push({"class": "optionClass", label: option_none, value: "None"});
                Object.keys(mapFile).forEach(function(key) {
                    index++;
                    opts.push({label: mapFile[key], value: key});
                });        
                component.set('v.businessRuleTemplateoptions', opts);
                //COMMENTED component.teamInstanceOptions();
            });
            $A.enqueueAction(businessRuletempaction);
        });

        $A.enqueueAction(action);
    },

    creationTypeOptions : function(component,event,helper){
        //var selectedScenarioType = component.find("scenarioTypeOpts");
        //var selectedScenarioTypeName = selectedScenarioType.get("v.value");
        //selectedScenarioTypeName = component.get('v.selectedMap')[selectedScenarioTypeName];
        var selectedScenarioTypeName = component.get('v.selectedScenarioType');

        var selectedOptionsByScenarioName =  component.get('v.scenarioOptionsMap')[selectedScenarioTypeName];
        var option_none = component.get('v.noneValue');
       
        var defaultCreationTypeOpts =[];
        defaultCreationTypeOpts.push({"class": "optionClass", label: option_none, value: "None"});

        if( selectedOptionsByScenarioName!= undefined && selectedScenarioTypeName!= undefined){
            component.set('v.creationTypeoptions', selectedOptionsByScenarioName);

            /* set default value as first, SIQ-2422 */
            if(selectedOptionsByScenarioName.length == 2) {
                component.set('v.createType', selectedOptionsByScenarioName[1].value);
                selectedOptionsByScenarioName[1].selected = true;
            }

        }else{
            component.set('v.creationTypeoptions',defaultCreationTypeOpts);
        }

        var selectedCreationType = component.find("createTypeSelect");
        var selectedCreationTypeName = selectedCreationType.get("v.value");

        if ( selectedScenarioTypeName == 'Alignment Scenario' ){
            component.set('v.isAlignment',true);
            component.set('v.isCallPlan',false);
            component.set('v.isProductBlock',false);
            helper.enableInputOptions(component,helper);
        }
        else if ( selectedScenarioTypeName == 'Call Plan Generation' ){
            component.set('v.isAlignment',false);
            component.set('v.isCallPlan',true);
            component.set('v.isProductBlock',false);
            helper.enableInputOptions(component,helper);
        }
        else if ( selectedScenarioTypeName == 'Block List' ){
            component.set('v.isAlignment',false);
            component.set('v.isCallPlan',false);
            component.set('v.isProductBlock',true);
            helper.disableInputOptions(component,helper);
        }

        console.log('selectedCreationTypeName : '+selectedCreationTypeName);
        if(selectedScenarioTypeName=='Call Plan Generation' && selectedCreationTypeName=='Create scenario from rule'){
            //component.set("v.hasBusinessRuleTemplate", true);
        }else{
            component.creationTypeChange();
            //COMMENTED
            //component.alignmentTypeOptions();
            //component.set("v.hasBusinessRuleTemplate", false);
        }
    },

    checkForScenarioType :function(component,event,helper){
        console.log('checkForScenarioType');
        component.set('v.createType','None');
        component.set("v.isAlignmentScenario", false);
        component.set('v.selectedfileTemplate','None');
        component.set('v.selectedValue','None');
        var selectedScenarioType=component.get('v.selectedScenarioType');
        if(selectedScenarioType == 'Alignment Scenario'){
            component.set("v.isAlignmentScenario", true);
        }
        component.getCreationType();
        
        
        // var selectedCreationType = component.find("createTypeSelect");
        // var selectedCreationTypeName = selectedCreationType.get("v.value");
        // var selectedScenarioType = component.find("scenarioTypeOpts");
        // var selectedScenarioTypeName = selectedScenarioType.get("v.value");
        // selectedScenarioTypeName = component.get('v.selectedMap')[selectedScenarioTypeName];
        // console.log(selectedScenarioTypeName);
        // if(selectedScenarioTypeName == 'Call Plan Generation' && selectedCreationTypeName == 'Create scenario from rule'){
        //     //component.set("v.hasBusinessRuleTemplate", true);
        // }else{
        //    //component.set("v.hasBusinessRuleTemplate", false);
        // }
        // console.log('hasBusinessRuleTemplate : '+component.get("v.hasBusinessRuleTemplate"));
    },

    callBusinessRuleTemplateOptions : function(component){
        var option_none = component.get("v.noneValue");
        console.log('businessRuleTemplateOptions');
        var selectedScenarioType = component.find("scenarioTypeOpts");
        var selectedScenarioTypeName = selectedScenarioType.get("v.value");
        // selectedScenarioTypeName=component.get('v.selectedMap')[selectedScenarioTypeName];
        var action1 = component.get("c.getBusinessRuleTemplateOptions");
        action1.setParams({ScenarioTypeName : selectedScenarioTypeName});
        action1.setCallback(this, function(response) {
            var mapFile = response.getReturnValue();
            var opts = [];
            //var index = 1;
            opts.push({"class": "optionClass", label: option_none, value: "None"});
            Object.keys(mapFile).forEach(function(key) {
               // if(index == 1)
                 //    component.set('v.selectedValue4', key);
                //index++;
                opts.push({label: mapFile[key], value: key});
            });        
            component.set('v.businessRuleTemplateoptions', opts);
            console.log(component.get('v.selectedfileTemplate'));
            component.getFileMetaData();
        });
        $A.enqueueAction(action1);
    },

    fileMetaDataOptions : function(component){
        var option_none = component.get("v.noneValue");
        //DISABLE SAVE BUTTON 
        component.set("v.disable_save",true);
        console.log('disable save btn');
        var selectedBuisnessRuleTemplateId = component.get("v.selectedfileTemplate");
        console.log(selectedBuisnessRuleTemplateId);
        var action1 = component.get("c.getFileMetaDataOptions");
        action1.setParams({selectedBuisnessRuleTemplateId : selectedBuisnessRuleTemplateId});
        action1.setCallback(this, function(response) {
        
            var mapFile = response.getReturnValue();
        
            var opts = [];
            var index = 1;
            opts.push({"class": "optionClass", label: option_none, value: "None"});
            Object.keys(mapFile).forEach(function(key) {
                opts.push({label: mapFile[key], value: key});
            });        
            component.set('v.options', opts);
        });
        $A.enqueueAction(action1);

        //GET SCENARIO LEVEL EXTERNAL DATA SOURCE FILES
        if(component.get("v.selectedfileTemplate")!='None'){
            var action = component.get("c.getCustomerMetricsOptions");
            action.setParams({selectedBuisnessRuleTemplateId : selectedBuisnessRuleTemplateId});
            action.setCallback(this, function(response) {
                var dataSources = response.getReturnValue();
                if(dataSources.length > 0){
                   component.set('v.showRuleTemplateInfo',true); 
                }else{
                   component.set('v.showRuleTemplateInfo',false); 
                }
                component.set('v.dataSources', dataSources);
                component.set("v.disable_save",false);
                console.log('enable save btn');
            });
            $A.enqueueAction(action);
        }else{
            component.set('v.dataSources', []);
            component.set('v.showRuleTemplateInfo',false); 
            component.set("v.disable_save",false);
            console.log('enable save btn');
            return;
        }
    },

    getDocumentTemplateFunction : function(component,event,helper)
    {
        console.log('getDocumentTemplateFunction called ---');
        var $j = jQuery.noConflict();
        var action = component.get("c.getDocumentTemplate");
        var selectedScenarioType = component.find("scenarioTypeOpts");
        var selectedScenarioTypeName = selectedScenarioType.get("v.value");
        selectedScenarioTypeName = component.get('v.selectedMap')[selectedScenarioTypeName];
        console.log('selectedScenarioTypeName : '+selectedScenarioTypeName);
        action.setParams({
            scenarioType : selectedScenarioTypeName
        });
    
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state == "SUCCESS" && response.getReturnValue!=null)
            {
                console.log(response.getReturnValue());
                var retResponse = response.getReturnValue()
                var fileSize = parseFloat(retResponse.BodyLength)/1000;
                retResponse.BodyLength = fileSize + ' KB';
                component.set("v.template",retResponse);
                //document.getElementById("fileTemplateRow").className = '';
            
            } else if(state == "ERROR" || response.getReturnValue == null){
                //document.getElementById("fileTemplateRow").className = 'slds-hide';
            }
        });
        $A.enqueueAction(action);
    },

    affiliationNetworkOptions : function(component,event,helper){
        var action = component.get("c.getAffiliationNetworkOptions");
        var namespace = component.get("v.namespace");
        var option_none = component.get("v.noneValue"); 

        action.setParams({
            countryId : component.get("v.countryId")
        });

        var opts = [];
        opts.push({"class": "optionClass", label: option_none, value: 'None'});
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if(state == "SUCCESS")
            {
                console.log(response.getReturnValue());
                var result = response.getReturnValue();

                // Reverting SIMPS-404 code, causing other issues
                /*Object.keys(result).forEach(function(key) {
                    if( result[key]['Name'] != undefined  && result[key]['Id']!= undefined){
                        console.log('label :',result[key]['Name']);
                        console.log('value :',result[key]['Id']);
                        opts.push({"class": "optionClass", label: result[key]['Name'], value: result[key]['Id']});
                    }
                  
                });*/

                for(var obj in result){
                    opts.push({"class": "optionClass", label: result[obj], value: obj});
                }
                component.set("v.affiliationNetworkOptions",opts);
            } 
            else if(state == "ERROR")
            {
                component.set("v.affiliationNetworkOptions",opts);
                helper.logException(component, response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    teamInstanceOptions : function(component,event,helper){
        //helper.updateScenarioOptions(component,event,helper);
        var $j = jQuery.noConflict();
        console.log('teamInstanceOptions called : ',component.get("v.newScenario.teamName"));
        //Get team instance options
        var selectedTeam = component.get("v.newScenario.teamName");
        component.alignmentTypeOptions();

        var opts = [];
        var namespace = component.get("v.namespace");

        var option_none = component.get("v.noneValue");  
        var selectedScenarioType = component.find("scenarioTypeOpts");
        var selectedScenarioTypeName = selectedScenarioType.get("v.value");
        selectedScenarioTypeName = component.get('v.selectedMap')[selectedScenarioTypeName];
        
        $j('#summaryDiv').addClass('slds-hide');
        
        if(selectedTeam != 'None')
        {
            var action = component.get("c.getTeamInstanceOptions");
            action.setParams({
                teamId : component.get("v.newScenario.teamName"),
                scenarioType : selectedScenarioTypeName
            });
            action.setCallback(this, function(response) 
            {
                var state = response.getState();
                if(state == "SUCCESS")
                {
                    console.log(response.getReturnValue());
                    component.set("v.newScenario.teamInstance",'None');
                    
                    opts.push({"class": "optionClass", label: option_none, value: 'None'});
                    for(var obj in response.getReturnValue())
                    {
                        opts.push({"class": "optionClass", label: response.getReturnValue()[obj], value: obj});
                    }

                    //if(teamInsSelect != undefined ){
                    component.set("v.scenarioTeamInstanceOptions", opts);
                    //}
                    
                } 
                else if(state == "ERROR")
                {
                    helper.logException(component, response.getError());
                }
            });
            $A.enqueueAction(action);
        }
        else
        {
            component.set("v.newScenario.teamInstance",'None');
            opts.push({"class": "optionClass", label: option_none, value: 'None'});
            component.set("v.scenarioTeamInstanceOptions", opts);
        }
    },
    
    showSummary : function(component,event,helper)
    {
        console.log('showSummary called');
        var $j = jQuery.noConflict();
        var selectedTeamIns = component.get("v.newScenario.teamInstance");
        var namespace = component.get("v.namespace");
        var option_none = component.get("v.noneValue");
        var selectedScenarioTypeName = component.get('v.selectedScenarioType');
        var select = component.find('createTypeSelect');
        var createTypeSelect = select.get("v.value");
        if(selectedScenarioTypeName == 'Alignment Scenario' && createTypeSelect=='Copy scenario from existing'){
            var action = component.get("c.getShared");
            action.setParams({
                teamInstanceId : component.get("v.newScenario.teamInstance")
            });
            action.setCallback(this, function(response) 
            {
                var state = response.getState();
                if(state == "SUCCESS")
                {
                    var result = response.getReturnValue();
                    console.log('--result--');
                    console.log(result);
                    component.set('v.checkedShared',result);
                }
                else if(state == "ERROR")
                {
                    helper.logException(component, response.getError());
                }
            });
            $A.enqueueAction(action);
        }

        if(selectedTeamIns != 'None')
        {
            if(component.get("v.createType").indexOf('Copy scenario from existing') != -1){
            //     var fetchTeamInstance = component.get("c.getTeamInstanceById");
            //     fetchTeamInstance.setParams({teamInstanceId : component.get("v.newScenario.teamInstance")});
            //     fetchTeamInstance.setCallback(this, function(response) 
            //     {
            //         var state = response.getState();
            //         if(state == "SUCCESS")
            //         {
            //             var affiliationNetwork = response.getReturnValue()[namespace+'Affiliation_Network__c'];
            //             if(affiliationNetwork != null && affiliationNetwork != ''){
            //                 //var affiliationNetworkSelect = component.find("InputSelectAffiliationNetwork");
            //                 //affiliationNetworkSelect.set("v.options", []);
            //                 //
            //                 //opts.push({"class": "optionClass", label: response.getReturnValue().Affiliation_Network__r.Name, value: affiliationNetwork, selected: "true" });
            //                 //affiliationNetworkSelect.set("v.options", opts);
            //                 var opts = [];
            //                 opts.push({"class": "optionClass", label: response.getReturnValue()[namespace+'Affiliation_Network__r.Name'], value: affiliationNetwork, selected: "true" });
            //                 component.set("v.affiliationNetworkOptions",opts);
            //             }
            //             else{

            //                 //var affiliationNetworkSelect = component.find("InputSelectAffiliationNetwork");
            //                 var opts = [];
            //                 opts.push({"class": "optionClass", label: option_none, value: 'None'});
            //                 //affiliationNetworkSelect.set("v.options", opts);
            //                 component.set("v.affiliationNetworkOptions",opts);
            //             }
            //         }
            //         else if(state == "ERROR")
            //         {
            //             helper.logException(component, response.getError());
            //         }
            //     });
            //     $A.enqueueAction(fetchTeamInstance);
            }
            else{
                console.log('inside aff network opt show summary');
                component.getAffiliationNetworkOptions();
            }

            var action = component.get("c.getSummary");
            action.setParams({
                teamInstanceId : component.get("v.newScenario.teamInstance"),
                workspaceId : component.get("v.recordId")
            });
            action.setCallback(this, function(response) 
            {
                var state = response.getState();
                if(state == "SUCCESS")
                {
                    var result = response.getReturnValue();
                    if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result))
                    {
                        component.set("v.summaries",result);
                    }
                    
                    if(component.get("v.createType").indexOf('Copy scenario from existing') != -1)
                    {
                      $j('#summaryDiv').removeClass('slds-hide');
                    }

                    component.showExistingScenarioDataSources();
                }
                else if(state == "ERROR")
                {
                    helper.logException(component, response.getError());
                }
            });
            $A.enqueueAction(action);
        }
        else
        {
            $j('#summaryDiv').addClass('slds-hide');
        }
    },

    showExistingScenarioDataSources : function(component,event,helper) {
        // GET EXISTING BUSINESS RULE TEMPLATE ID
        var existingBRTemplateId; var opts = [];
        var businessRuletempaction = component.get("c.getBusinessRuleTemplateOptionsFromTeamInstance");
        businessRuletempaction.setParams({teamInstanceId : component.get("v.newScenario.teamInstance")});
        businessRuletempaction.setCallback(this, function(response) {
            if(response.getReturnValue() != undefined ){
                var exsitingBRTemplate = response.getReturnValue();
                opts.push({label: exsitingBRTemplate.Name, value: exsitingBRTemplate.Id});

                console.log(' exsitingBRTemplate ::::'+ exsitingBRTemplate.Id);
                //  ADD UI ROW IF BUSINESS RULES ARE CONFIGURED
                var isRuleapplicableRow;
                var summaries = component.get("v.summaries");
                if( exsitingBRTemplate.Id != undefined ){
                    summaries.push({ summaryLabel: "Business Rules Configured", summaryValue: 'Yes'});
                        // GET DATA SOURCES IF TEMPLATE EXISTS
                        if( exsitingBRTemplate.Id != undefined ){
                            var action = component.get("c.getCustomerMetricsOptions");
                            action.setParams({ selectedBuisnessRuleTemplateId : exsitingBRTemplate.Id });
                            action.setCallback(this, function(response) {
                                var dataSources = response.getReturnValue();

                                console.log(dataSources.length );
                                if(dataSources.length > 0){
                                    component.set('v.showRuleTemplateInfo',true); 
                                    component.set('v.isAlignmentRulesTemplate',true); 
                                }else{
                                   component.set('v.showRuleTemplateInfo',false); 
                                }
                                component.set('v.dataSources', dataSources);
                            });
                            $A.enqueueAction(action);
                        }
                }else{
                    component.set('v.showRuleTemplateInfo',false); 
                    component.set('v.isAlignmentRulesTemplate',false); 
                    summaries.push({ summaryLabel: "Business Rules Configured", summaryValue: 'None'});
                }
                component.set("v.summaries",summaries);

            }
            component.set('v.businessRuleTemplateoptions', opts);

        });
        $A.enqueueAction(businessRuletempaction);
        
        //GET ATTIBUTES OF EXISTING TEAM INSTANCE
        var teamInstanceAttributesAction = component.get("c.getExistingTeamInstanceAttributes");
        teamInstanceAttributesAction.setParams({teamInstanceId : component.get("v.newScenario.teamInstance")});
        teamInstanceAttributesAction.setCallback(this,function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                var ns = component.get('v.namespace');
                var teamInstanceAttributes = response.getReturnValue();
                console.log( teamInstanceAttributes );
                //SET GEO LAYER OPTIONS
                var geoLayerOptions = [];

                if( teamInstanceAttributes[ns+'Geography_Type_Name__c']  != undefined ){
                    geoLayerOptions.push({"class": "optionClass", label: teamInstanceAttributes[ns+'Geography_Type_Name__r']['Name'], value: teamInstanceAttributes[ns+'Geography_Type_Name__c'] });
                    component.set("v.geoLayerOptions",geoLayerOptions);
                }

                //SET AFF NETWORK OPTIONS
                var afnOptions = [];
                if( teamInstanceAttributes[ns+'Affiliation_Network__c']  != undefined ){
                    afnOptions.push({"class": "optionClass", label: teamInstanceAttributes[ns+'Affiliation_Network__r']['Name'], value: teamInstanceAttributes[ns+'Affiliation_Network__c'] });
                    component.set("v.affiliationNetworkOptions",afnOptions);
                ///
                    component.set("v.newScenario.geoLayer",teamInstanceAttributes[ns+'Geography_Type_Name__c']);
                    component.set("v.newScenario.affiliationNetwork",teamInstanceAttributes[ns+'Affiliation_Network__c']);
                }  
            } 
            else if(state == "ERROR"){
                helper.logException(component, response.getError());
            }

        });
        $A.enqueueAction(teamInstanceAttributesAction);
    },

    checkScenarioService: function(component,event,helper){
        console.log('in checkScenarioService');
        var action = component.get("c.validateScenarioService");
        action.setParams({jobName : 'PythonValidation'});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                console.log('return code is '+response.getReturnValue());
                var toastEvent = $A.get("e.force:showToast");
                var message='';
                var errorList = [];
                var title='error';
                var namespace = component.get("v.namespace");
                if(response.getReturnValue()=='106'){
                    if(namespace!=''){
                        message=$A.get("$Label."+namespace.slice(0,-2)+".NoRemoteSetting");
                    }
                    else{
                        message=$A.get("$Label.c.NoRemoteSetting");
                    }
                    errorList.push(message);
                    component.set("v.errorList",errorList);
                    component.set('v.hasValidationFailed',true);
                    helper.enableButton(component,event,helper);
                    return;
                    //message = 'No Remote Site setting found';

                }else if(response.getReturnValue()=='107'){
                    if(namespace!=''){
                        message=$A.get("$Label."+namespace.slice(0,-2)+".NoETLconfig");
                    }
                    else{
                        message= $A.get("$Label.c.NoETLconfig");
                    }
                    errorList.push(message);
                    component.set("v.errorList",errorList);
                    component.set('v.hasValidationFailed',true);
                    helper.enableButton(component,event,helper);
                    return;
                    //message = 'No ETL Config found';

                }
                else if(response.getReturnValue()=='404'){
                    if(namespace!=''){
                        message=$A.get("$Label."+namespace.slice(0,-2)+".NoScenarioService");
                    }
                    else{
                        message=$A.get("$Label.c.NoScenarioService");
                    }
                    errorList.push(message);
                    component.set("v.errorList",errorList);
                    component.set('v.hasValidationFailed',true);
                    helper.enableButton(component,event,helper);
                    return;
                    //message = 'Scenario Service not available';

                }else if(response.getReturnValue()=='200'){
                    message='';
                    title='';

                    //component.newScenario(component,event,helper);
                    $A.enqueueAction(component.get("c.newScenario"));

                }
                toastEvent.setParams({
                    "title": title,
                    "type":title,
                    "message": message
                });
                toastEvent.fire();

            }
            else if(state == "ERROR")
            {
                helper.logException(component, response.getError());
            }

        });
        $A.enqueueAction(action);

    },
    
    newScenario : function(component,event,helper) 
    {
        helper.disableButton(component,event,helper);
        var $j = jQuery.noConflict();
        //Check validation for required fields
        var validationFailed = false;
        var errorMsg = '';
        var reqFieldMiss;
        var namespace = component.get("v.namespace");

        //REMOVED  brmsTemplateOption
        var inputs = ['scenarioTypeOpts','InputSelectAlignType','createTypeSelect','InputSelectTeamIns','initialTeamSelect', 'filePath','selDataSource','inputScenarioName'];
        
        if(component.get('v.checkedAffilation')){
            inputs.push('InputSelectAffiliationNetwork');
        }
        if(component.get('v.selectBaseGeography')){
            inputs.push('InputSelectGeoLayer');
        }
        var options_selected = {};
        var errorList = [];
        var validationFailed = false;
            try{
                for(var key in inputs){
                    console.log(inputs[key]);
                    var inputCmp = component.find(inputs[key]);
                     if(!$A.util.isUndefined(inputCmp)){
                        var value ;
                        if( inputCmp[0] != undefined ){
                            //IN CASE INPUT IS RERENDERED IT GOES INTO ARRAY
                            inputCmp = inputCmp[0];
                            value = inputCmp.get("v.value");
                            var optionVals = inputCmp.get("v.options");

                            options_selected[value] = value;

                            if( optionVals != undefined && optionVals.length > 0 ){
                                optionVals.forEach( function(element){
                                    if(element.selected != undefined && element.selected == true)
                                        options_selected[value] = element.label;
                                });
                            }
                            
                        }else{
                            value = inputCmp.get("v.value");
                            var optionVals = inputCmp.get("v.options");

                            options_selected[value] = value;

                            if( optionVals != undefined && optionVals.length > 0 ){
                                optionVals.forEach( function(element){
                                    if(element.selected != undefined && element.selected == true)
                                        options_selected[value] = element.label;
                                });
                            }
                        }
                        
                        if($A.util.isEmpty(value) || value == 'None' || value.trim().length == 0 ){

                            $A.util.addClass(inputCmp, 'inputError');
                            var errorMessage = component.get('v.validation_error_messages')[inputs[key]][0];

                            errorList.push(errorMessage);
                            inputCmp.set("v.errors", [{message: errorMessage}]);               
                            validationFailed = true;
                            continue;
                        }
                        else{
                            inputCmp.set("v.errors", []);   
                            $A.util.removeClass(inputCmp, 'inputError');
                            continue;
                        }  
                    } 
                }
                var inputCmpSelectedTeamInstance =component.find('inputSelectTeamInstance');
                if(!$A.util.isUndefined(inputCmpSelectedTeamInstance)){
                    var valueSelectedTeamInstance = component.find('inputSelectTeamInstance').get("v.value");
                    if(!($A.util.isEmpty(valueSelectedTeamInstance) || valueSelectedTeamInstance == 'None' || valueSelectedTeamInstance.trim().length == 0 )){
                        var valueSelectedConvergeLevel = component.find('inputselectConvergeLevel').get("v.value");
                        var inputCmpSelectedConvergeLevel =component.find('inputselectConvergeLevel');
                        console.log(' valueSelectedConvergeLevel'+valueSelectedConvergeLevel);
                        if($A.util.isEmpty(valueSelectedConvergeLevel) || valueSelectedConvergeLevel == 'None' || valueSelectedConvergeLevel.trim().length == 0){
                            
                            $A.util.addClass(inputCmpSelectedConvergeLevel, 'inputError');
                            console.log(component.get('v.validation_error_messages'));
                            var errorMessage = component.get('v.validation_error_messages')['inputselectConvergeLevel'][0];
                            errorList.push(errorMessage);
                            inputCmpSelectedConvergeLevel.set("v.errors", [{message: errorMessage}]);               
                            validationFailed = true;
                        }
                        else{
                            inputCmpSelectedConvergeLevel.set("v.errors", []);   
                            $A.util.removeClass(inputCmpSelectedConvergeLevel, 'inputError');
                        }
                    }
                }

            }catch(error){
                    console.log('error '+error);
            }

            component.set("v.errorList",errorList);
            component.set('v.hasValidationFailed',validationFailed);

        if(validationFailed){
             helper.enableButton(component,event,helper);
             return;
        }
        
        console.log('Duplicate name validation failed 1 : '+validationFailed);
        // if(validationFailed)
        // {
        //     if(namespace!='')
        //     {
        //         helper.logException(component, $A.get("$Label.AxtriaSalesIQTM.Required_Fields_Missing"));
        //     }
        //     else
        //     {
        //         helper.logException(component, $A.get("$Label.c.Required_Fields_Missing"));
        //     }
        // }
        // else
        // {
            var scenarioValidationAction = component.get('c.duplicateScenarioValidation');
            
            var teamIns = component.find("InputSelectTeamIns");
            //var geoType = component.find("InputSelectGeoLayer");
            var scenarioName = component.find("inputScenarioName");

            var selectedScenarioType = component.find("scenarioTypeOpts");
            var selectedScenarioTypeName = selectedScenarioType.get("v.value");
            selectedScenarioTypeName = component.get('v.selectedMap')[selectedScenarioTypeName];
            console.log('save  -- '+selectedScenarioTypeName);
            var createType = component.get("v.createType");
            console.log(createType);
            var namespace = component.get("v.namespace");
            //var createScenarioExist,createScenarioRule,createScenarioFile;
            var isCallPlan = component.get('v.isCallPlan');
            var isAlignment = component.get('v.isAlignment');
            var isScenarioCreatedFromFile = component.get('v.isScenarioCreatedFromFile');
            var isScenarioCreatedFromRule = component.get('v.isScenarioCreatedFromRule');
            var isScenarioCreatedFromExisting = component.get('v.isScenarioCreatedFromExisting');
/*  

            if(namespace!=''){

                createScenarioExist=component.get("v.createType").indexOf($A.get("$Label.AxtriaSalesIQTM.Create_Scenario_From_Existing"));
                createScenarioRule=$A.get("$Label.AxtriaSalesIQTM.Create_Scenario_From_Rule");
                createScenarioFile=$A.get("$Label.AxtriaSalesIQTM.Create_Scenario_From_File");
            }
            else
            {

                createScenarioRule=$A.get("$Label.c.Create_Scenario_From_Rule");
                createScenarioExist=component.get("v.createType").indexOf($A.get("$Label.c.Create_Scenario_From_Existing")); 
                createScenarioFile=$A.get("$Label.c.Create_Scenario_From_File");
            }*/

            var finalAttributes_ = {};

            try{

                //SET CONFITMATION NUBBING VALUES
                var brOptions = component.get("v.businessRuleTemplateoptions");
                var afOptions = component.get("v.affiliationNetworkOptions");
                var nwId = component.get("v.newScenario.affiliationNetwork");
                var brtId = component.get("v.selectedfileTemplate");

                var brLabel  = '';
                var afLabel = '';

                brOptions.forEach( function(element){

                    if(element.value != undefined && element.value == brtId)
                        brLabel= element.label;
                });

                afOptions.forEach( function(element){
                    if(element.value != undefined && element.value == nwId)
                        afLabel = element.label;
                });


                finalAttributes_['teamName'] = options_selected[component.get("v.newScenario.teamName")];
                finalAttributes_['alignmentType'] = component.get("v.alignmentType");
                finalAttributes_['creationType'] = ( component.get("v.createType") == 'Upload scenario from file' ) ? 'From File' : 'From Existing';
                finalAttributes_['isAffilaitonEnabled'] = component.get("v.checkedAffilation") ? 'True': 'False';
                finalAttributes_['affiliationNetwork'] = afLabel;
                finalAttributes_['templateName'] = brLabel;
                component.set('v.finalAttributes',finalAttributes_);


            }
            catch(e){
                console.log(e);
            }
           

            
            if(isScenarioCreatedFromExisting)
            {
                //CALL PLAN SCENARIO FROM EXISTING
                scenarioValidationAction.setParams({
                    /*copyTeam : component.get("v.newScenario.teamName"),
                    workspaceId : component.get("v.recordId"),
                    employeeAssignment : component.get("v.checkedEmployeeAssignment"),*/
                    TeamInstanceId :  component.get("v.newScenario.teamInstance"),
                    sName : scenarioName.get("v.value"),
                    countryId : component.get("v.countryId")
                });

            }else if( isScenarioCreatedFromRule || (isScenarioCreatedFromFile && isCallPlan)){
                //CALL PLAN SCENARIO
                scenarioValidationAction.setParams({
                    TeamInstanceId : component.get("v.newScenario.teamInstance"),
                    sName : scenarioName.get("v.value"), 
                    scenarioType : selectedScenarioTypeName,
                    createType : createType,
                    countryId : component.get("v.countryId")
                });

            }else{
                //ALIGNMENT SCENARIO FROM FILE
                scenarioValidationAction.setParams({
                    teamName : component.get("v.newScenario.teamName"),
                    workspaceId : component.get("v.recordId"),
                    employeeAssignment : component.get("v.checkedEmployeeAssignment"),
                    sName : scenarioName.get("v.value"),
                    countryId : component.get("v.countryId")
                });
            }
            
            scenarioValidationAction.setCallback(this, function(response) 
            {
                var state = response.getState();
                console.log('state : ',state);
                if(state == "SUCCESS")
                {
                    console.log('Response : ',response.getReturnValue());


                    if(response.getReturnValue()!='0')
                    {
                        console.log(';'+response.getReturnValue());
                        validationFailed = true;
                        helper.enableButton(component,event,helper);
                        console.log(validationFailed);
                        helper.logException(component, response.getReturnValue());
                    }else {
                        helper.disableButton(component,event,helper);
                        if(isCallPlan || isScenarioCreatedFromExisting){
                            component.createScenario();
                        }else{
                            component.set('v.showConfirmation',true);
                            //SHOW CONFIRMATION POPOVER BEFORE CREATING A SCENARIO
                            //SIMPS - 525 , SIQ 3160
                            //component.createScenario();
                        }
                    }
                }
                else if(state == "ERROR")
                {
                    helper.logException(component, response.getError());
                }
            });
            $A.enqueueAction(scenarioValidationAction);
        //}
        
        console.log('Duplicate name validation failed 2 : '+validationFailed);
    },

    copyExistingScenario : function(component){
        var action = component.get("c.createExistingScenarioInstance");
        action.setParams({
        selectedScenarioId :  component.get('v.selectedscenarioid'),
        newScenarioId : component.get('v.newScenarioId')
        });
        
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
              console.log('response ::: '+response.getReturnValue());
              if(response.getReturnValue() != null || response.getReturnValue() != undefined || response.getReturnValue() != '')
              {
                var selectedscenarioid = response.getReturnValue();
                console.log('selectedscenarioid ::: '+selectedscenarioid);
                component.set('v.selectedscenarioid',selectedscenarioid);
              }
              else{
                alert('No Scenario Exists for selected Team Instance');
              }
               
            }

            else{
                alert('No Scenario Exists for selected Team Instance');
            }
        });
        $A.enqueueAction(action);
    },

    confirmCreate : function(component,event,helper){

        console.log('confirm create called--');

        component.set('v.showConfirmation',false);
        var namespace = component.get("v.namespace");
        var selectedScenarioType = component.find("scenarioTypeOpts");
        var geoType = component.find("InputSelectGeoLayer");
        var affiliationNetwork = component.find("InputSelectAffiliationNetwork");
        var selectedScenarioTypeid = selectedScenarioType.get("v.value");
        //var scenarioExistingVar,scenarioExistingFile,scenarioRuleVar;
        component.set('v.copyexistingscenarioflag',true);
        
        var selectedScenarioTypeName = component.get('v.selectedMap')[selectedScenarioTypeid];
        var scenarioTypeOptionsMap = component.get('v.scenarioOptionsMap');
        var selectedCreationType = component.find("createTypeSelect");
        var selectedCreationTypeName = selectedCreationType.get("v.value");

        if(component.get('v.isScenarioCreatedFromExisting'))
        {
            //Create Scenario record
            console.log('Create_Scenario_From_Existing');

            var action = component.get("c.createScenario");

            var baseTeamInstance = component.find('inputSelectTeamInstance');
            if(baseTeamInstance != undefined){
            var baseTeamInstanceId = baseTeamInstance.get("v.value");
        }

            var clevel = component.find('inputselectConvergeLevel');
            if(clevel != undefined){
            var convegLevel = clevel.get("v.value");
            }

            action.setParams({ 
                workspaceId : component.get("v.recordId"),
                name : component.get("v.newScenario.scenarioName"),
                teamId : component.get("v.newScenario.teamName"),
                teamInstanceId : component.get("v.newScenario.teamInstance"),
                description : component.get("v.newScenario.scenarioDescription"),
                scenarioType : selectedScenarioTypeid,
                scenarioTypeName : selectedScenarioTypeName,
                affiliationNetwork : component.get("{!v.newScenario.affiliationNetwork}"),
                copyexistingscenarioflag : component.get('v.copyexistingscenarioflag'),
                dataSourceWrapper : JSON.stringify(component.get("v.dataSources")),
                baseTeamInstance:baseTeamInstanceId,
                /*employeeAssignment : component.get("v.checkedEmployeeAssignment"),*/
                shared : component.get("v.checkedShared"),
                convergingLevel:convegLevel
            });
            
            action.setCallback(this, function(response) 
            {
                var state = response.getState();
                if (state === "SUCCESS") 
                {
                    //HIDE ERROR POPUP ON SUCCESS
                    /*var warningIcon = component.find('validation_failed_icon');
                    $A.util.addClass(warningIcon,'slds-hide');
                    var toast = component.find("validation_failed_popover");    
                    $A.util.addClass(toast,'slds-hide');
                    component.set("v.errorList",[]);*/
                   
                    
                    // Display the success message in a toast
                    var resultsToast = $A.get("e.force:showToast");
                    
                    var newScId;
                    var newScName
                    for(var obj in response.getReturnValue()){
                        newScId = obj;
                        component.set('v.newScenarioId',newScId);
                        newScName = response.getReturnValue()[obj];
                    }
                    
                    var tempLabel; 
                    if(namespace!=''){
                        tempLabel= $A.get("$Label."+namespace.slice(0,-2)+".Scenario_Create_Success");
                    }
                    else{
                        tempLabel=$A.get("$Label.c.Scenario_Create_Success");
                    }

                    resultsToast.setParams
                    ({
                        mode: 'dismissible',
                        type: 'Success',
                        "message":tempLabel,
                        "messageTemplate": tempLabel,
                        "messageTemplateData": 
                        [{
                            url: '/'+newScId,
                            label: newScName,
                        }]
                    });
                    resultsToast.fire();
                    helper.enableButton(component,event,helper);



                    //Refresh workspace
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams
                    ({
                        "recordId": component.get("v.recordId")
                    });
                    navEvt.fire();
                    
                    // Close the action panel
                    //helper.hideSpinner(component,event,helper);
                    component.closeAction();
                    
                }
                else if (state === "ERROR") 
                {
                    helper.logException(component, response.getError());
                }
            });
            $A.enqueueAction(action);
        }

        else if(component.get('v.isScenarioCreatedFromFile'))
        {
            //Create Scenario record
            console.log('Create_Scenario_From_File');

            var creationType = component.find("createTypeSelect");
            var creationTypeName;
            if(creationType != ''){
                creationTypeName = creationType.get("v.value");
            }else{
                creationTypeName = ''; 
            }

            var baseTeamInstance = component.find('inputSelectTeamInstance');
            var baseTeamInstanceId = baseTeamInstance.get("v.value");

            var clevel = component.find('inputselectConvergeLevel');
            var convegLevel = clevel.get("v.value");

            
            var action = component.get("c.createScenarioFromFile");
            action.setParams({ 
                workspaceId : component.get("v.recordId"),
                name : component.get("v.newScenario.scenarioName"),
                teamId : component.get("v.newScenario.teamName"),
                filePath : component.get("v.filePath"),
                description : component.get("v.newScenario.scenarioDescription"),
                scenarioType : selectedScenarioTypeid,
                scenarioTypeName : selectedScenarioTypeName,
                teamInstanceId : component.get("v.newScenario.teamInstance"),
                geoType : component.get("{!v.newScenario.geoLayer}"),
                affiliationNetwork : component.get("{!v.newScenario.affiliationNetwork}"),
                templateid : component.get("v.selectedfileTemplate"),
                dataSourceWrapper : JSON.stringify(component.get("v.dataSources")), 
                affilationChecked : component.get("v.checkedAffilation"),
                skipValidation : component.get("v.checkedSkipValidation"),
                employeeAssignment : component.get("v.checkedEmployeeAssignment"),
                shared : component.get("v.checkedShared"),
                baseTeamInstance:baseTeamInstanceId,
                convergingLevel:convegLevel
            });
                
            action.setCallback(this, function(response) 
            {
                var state = response.getState();
                if (state === "SUCCESS") 
                {
                    //HIDE POPOP WHEN SUCCESS
                    /*var warningIcon = component.find('validation_failed_icon');
                    $A.util.addClass(warningIcon,'slds-hide');
                    var toast = component.find("validation_failed_popover");    
                    $A.util.addClass(toast,'slds-hide');*/
                    
                    // Display the success message in a toast
                    var resultsToast = $A.get("e.force:showToast");
                    var newScId;
                    var newScName
                    for(var obj in response.getReturnValue()){
                        newScId = obj;
                        newScName = response.getReturnValue()[obj];
                    }

                    var tempLabel;
                    if(namespace!=''){
                        tempLabel=$A.get("$Label."+namespace.slice(0,-2)+".Scenario_Create_Success");
                    }
                    else{
                        tempLabel=$A.get("$Label.c.Scenario_Create_Success");
                    }
                    
                    resultsToast.setParams({
                        mode: 'dismissible',
                        type: 'Success',
                        "message":tempLabel,
                        "messageTemplate": tempLabel,
                        "messageTemplateData": [{
                            url: '/'+newScId,
                            label: newScName,
                        }]
                    });
                    resultsToast.fire();

                    //Refresh workspace
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                      "recordId": component.get("v.recordId")
                    });
                    navEvt.fire();

                    // Close the action panel
                    component.closeAction();
                    
                }
                else if (state === "ERROR") 
                {
                    helper.logException(component, response.getError());
                }
            });
            $A.enqueueAction(action);
        }
            
        else if(component.get('v.isScenarioCreatedFromRule'))
        {
            console.log('Create new scenario from rule');
            var fname = component.find("selDataSource");
            var fileId = fname.get("v.value");
            
            console.log('scenarioTypeName : '+selectedScenarioType);
       
            var creationType = component.find("createTypeSelect");
            var creationTypeName;
            if(creationType != ''){
                creationTypeName = creationType.get("v.value");
            }else{
                creationTypeName = ''; 
            }
            console.log('creationTypeName : '+creationTypeName);
            var buisnessRuleTempName = component.find("brmsTemplateOption").get("v.value");

            if(buisnessRuleTempName==''){
                buisnessRuleTempName = '';
            }
            console.log(buisnessRuleTempName);
            var teamIns = component.find("InputSelectTeamIns");
            //Create Scenario record
            var action = component.get("c.createScenarioFromRule");
            action.setParams({ workspaceId : component.get("v.recordId"),
                               name : component.get("v.newScenario.scenarioName"),
                               teamId : component.get("v.newScenario.teamName"),
                               teamInstanceId : component.get("v.newScenario.teamInstance"),
                               fileMetaDataId : fileId,
                               scenarioType : selectedScenarioTypeid,
                               scenarioTypeName:selectedScenarioTypeName,
                               creationType : creationTypeName,
                               businessRuleTemplate: buisnessRuleTempName});
            
            action.setCallback(this, function(response) {
                console.log('After Call_Back');
                console.log('Success message : ',response.getReturnValue());
                var responseMap = response.getReturnValue();
                
                var newScId;
                var newScName;
                var brtVal;
                
                newScId = responseMap['0'];
                newScName = responseMap['1'];
                brtVal = responseMap['2'];
                
                var state = response.getState();

                if (state === "SUCCESS") {
                    //BRM-349 : ScenarioType CHANGED selectedScenarioType
                    var evt = $A.get("e.force:navigateToComponent");
                    evt.setParams({
                        componentDef : "c:CPGbusinessRuleCanvas",
                        componentAttributes: {
                            scenarioId : newScId,
                            businessRuleTemplateId : brtVal,
                            ScenarioType : selectedScenarioTypeid,
                            workSpaceId : component.get("v.recordId")
                        }
                    });
                    evt.fire();
                    
                }else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    closeAction : function (component){
        component.destroy();
        console.log('closing create scenario');
        var event = $A.get("e.c:modalCloseEvent");
        event.setParams( { 
        });
        event.fire();
    },

    closeNubbin :  function (component,event,helper){
        component.set('v.showConfirmation',false);
        component.set('v.finalAttributes',{});
        helper.enableButton(component,event,helper);
    },
    createTypeChange : function(component,event,helper)
    {
            console.log('createTypeChange called');
            var $j = jQuery.noConflict();
            var select = component.find('createTypeSelect');
            var createTypeSelect = select.get("v.value");
            var createType = select.get("v.value");
            component.set('v.checkedShared', false);
            component.set('v.checkedEmployeeAssignment', false);
            var selectedScenarioType = component.find("scenarioTypeOpts");
            //var selectedScenarioTypeName = selectedScenarioType.get("v.value");
            var selectedScenarioTypeName = component.get('v.selectedScenarioType');
            //selectedScenarioTypeName = component.get('v.selectedMap')[selectedScenarioTypeName];
            var namespace = component.get("v.namespace");
            var option_none = component.get("v.noneValue");
            var isAlignment = false, isCallPlan = false, isProductBlock = false, isFromFile = false, isFromExisting = false, isFromRule = false;

            component.set("v.isFromFile", false);
            if(createTypeSelect == 'Upload scenario from file'){
                component.set("v.isFromFile", true);
            }
            if(createTypeSelect == 'None'){
                component.set('v.showScenarioDetails',false);
                component.set('v.isAlignment',false);
                component.set('v.isCallPlan',false);
                component.set('v.isProductBlock',false);
                component.set('v.isAlignmentRulesTemplate',false);
                component.set('v.isCallPlanRulesTemplate',false);
                component.set('v.isScenarioCreatedFromFile',false);
                component.set('v.isScenarioCreatedFromRule',false);
                component.set('v.isScenarioCreatedFromExisting',false);
                return;
            }else{
                component.set('v.showScenarioDetails',true);
            }

            if(createTypeSelect == option_none && isFromExisting){
                // isAlignment = false;
                isAlignment = true;
                isCallPlan = false;
                //component.set('v.isAlignmentRulesTemplate',false);
                component.set('v.isAlignmentRulesTemplate',true);
                component.set('v.isCallPlanRulesTemplate',false);
            }




            console.log(selectedScenarioTypeName);
            console.log('createTypeSelect'+createTypeSelect);
            console.log('option_none'+option_none);

            if ( selectedScenarioTypeName == 'Alignment Scenario' 
                    && createTypeSelect != option_none){
                isAlignment = true;
                isCallPlan = false;
                isProductBlock = false;

                component.set('v.isAlignment',true);
                component.set('v.isCallPlan',false);
                component.set('v.isProductBlock',false);

                //GET GEOLAYER OPTIONS
                //helper.getGeoLayerOptions(component,event,helper);
                //GET AFFLICATION NETWORK OPTIONS
                //component.getAffiliationNetworkOptions();

            }


            if (selectedScenarioTypeName == 'Call Plan Generation' && createTypeSelect != option_none){
                isAlignment = false;
                isCallPlan = true;
                isProductBlock = false;

                component.set('v.isAlignment',false);
                component.set('v.isCallPlan',true);
                component.set('v.isProductBlock',false);
            }
            
            if ( selectedScenarioTypeName == 'Block List' && createTypeSelect != option_none){
                isAlignment = false;
                isCallPlan = false;
                isProductBlock = true;

                component.set('v.isAlignment',false);
                component.set('v.isCallPlan',false);
                component.set('v.isProductBlock',true);
            }

            if(createTypeSelect=='Copy scenario from existing'){
                component.set('v.isScenarioCreatedFromFile',false);
                component.set('v.isScenarioCreatedFromRule',false);
                component.set('v.isScenarioCreatedFromExisting',true);
                component.set('v.selectBaseGeography',false);
                component.set('v.disableShared', true);
                component.set('v.disableEmpAss', true);
            }else if(createTypeSelect=='Upload scenario from file'){
                component.set('v.isScenarioCreatedFromFile',true);
                component.set('v.isScenarioCreatedFromRule',false);
                component.set('v.isScenarioCreatedFromExisting',false);
                component.set('v.selectBaseGeography',true);
                component.set('v.disableShared', false);
                component.set('v.disableEmpAss', false);
            }else if(createTypeSelect == 'Create scenario from rule'){
                component.set('v.isScenarioCreatedFromFile',false);
                component.set('v.isScenarioCreatedFromRule',true);
                component.set('v.isScenarioCreatedFromExisting',false);
                component.set('v.selectBaseGeography',true);
            }

            isFromFile = component.get('v.isScenarioCreatedFromFile');
            isFromExisting = component.get('v.isScenarioCreatedFromExisting');
            isFromRule = component.get('v.isScenarioCreatedFromRule');

            /*if(isAlignment){
                //GET GEOLAYER OPTIONS
                helper.getGeoLayerOptions(component,event,helper);
                //GET AFFLICATION NETWORK OPTIONS
                component.getAffiliationNetworkOptions();
            }*/

            if((isAlignment && isFromExisting)|| (isCallPlan && isFromFile) || (isCallPlan && isFromRule)){
                //get team instance options
                component.teamInstanceOptions();
            }

            

            if( (isFromFile && isCallPlan && createTypeSelect != option_none)){//isFromExisting
                component.set('v.isAlignmentRulesTemplate',false);
                component.set('v.isCallPlanRulesTemplate',false);
            }
            else if(isAlignment && isFromFile && createTypeSelect != option_none){
                component.set('v.isAlignmentRulesTemplate',true);
                component.set('v.isCallPlanRulesTemplate',false);
                component.alignmentTypeOptions();
            }else if(isCallPlan && isFromRule && createTypeSelect != option_none){
                component.set('v.isAlignmentRulesTemplate',false);
                component.set('v.isCallPlanRulesTemplate',true);
            }

            //HIDE PREVIOUS ERROR POPUPS
            //HIDE VALIDATION ERRORS INOPUTS
           /* var errorElemList = $j(".uiInputDefaultError");
            for(var i=0 ; i < errorElemList.length ; i++)
            {
                if($j(".uiInputDefaultError").eq(i).hasClass("slds-hide") == false)
                {
                    $j(".uiInputDefaultError").eq(i).addClass("slds-hide");
                }
            }
            $j(".requiredInput").each(function( index, value ) 
            {
                if($j(this).children('input').hasClass("errorInput"))
                {
                    $j(this).children('input').removeClass("errorInput");
                }
                else if($j(this).children('select').hasClass("errorInput"))
                {
                    $j(this).children('select').removeClass("errorInput");
                }
            }); */   
            //HIDE TOAST POPUP
           /* var toast = component.find("validation_failed_popover");    
            $A.util.addClass(toast,'slds-hide');
            var warningIcon = component.find('validation_failed_icon');
            $A.util.addClass(warningIcon,'slds-hide');*/


            if(isFromExisting){
                console.log('For Existing');
                //$j('#copyExistingDiv').removeClass('slds-hide');
                //$j('#filePath').addClass('slds-hide');
                //$j('#fileCopyDiv').addClass('slds-hide');
                /*$j('#geoDivAlignment').removeClass('slds-hide');
                $j('#geoDivAlignment').addClass('requiredInput');*/
                //$j('#divAlignmentype').addClass('slds-hide');
                //$j('#divAffliationbasedAlignment').addClass('slds-hide');
                if(isCallPlan){

                   // $j('#affiliationDivAlignment').addClass('slds-hide');
                }
                else if(isAlignment){
                    // ---- SHOW AF NW BUT DISBALE OPTION AND FILL NONE -----
                    // $j('#affiliationDivAlignment').removeClass('slds-hide');
                    //var affiliationNetworkSelect = component.find("InputSelectAffiliationNetwork");
                    //affiliationNetworkSelect.set("v.disabled", true);
                    //var opts = [];
                    //opts.push({"class": "optionClass", label: option_none, value: 'None'});
                    //affiliationNetworkSelect.set("v.options", opts);
                }
                
                //$j('#fileCopyDivAlignment').addClass('slds-hide');
                $j('#summaryDiv').addClass('slds-hide');
               // component.find("SelectTeamFile").set("v.value","None");
               // component.set("v.hasBusinessRuleTemplate", false);
            }
            
            else if(isFromFile){
                component.getDocumentTemplateFunction();
                $j('#summaryDiv').addClass('slds-hide');
                if(isCallPlan){
                    //$j('#copyExistingDiv').removeClass('slds-hide');
                    // $j('#fileCopyDivAlignment').addClass('slds-hide');
                    /*$j('#geoDivAlignment').addClass('slds-hide');
                    $j('#geoDivAlignment').removeClass('requiredInput');*/
                    // $j('#affiliationDivAlignment').addClass('slds-hide');
                    // $j('#fileCopyDiv').removeClass('slds-hide');
                    // $j('#filePath').removeClass('slds-hide');
                }
                else{

                    // $j('#fileCopyDiv').removeClass('slds-hide');
                    // $j('#filePath').removeClass('slds-hide');   
                    //$j('#copyExistingDiv').addClass('slds-hide');
                    // $j('#fileCopyDivAlignment').addClass('slds-hide');
                    /*$j('#geoDivAlignment').removeClass('slds-hide');
                    $j('#geoDivAlignment').addClass('requiredInput');*/
                    // $j('#affiliationDivAlignment').removeClass('slds-hide'); 
                    // $j('#divAffliationbasedAlignment').removeClass('slds-hide');
                    // $j('#divAlignmentype').removeClass('slds-hide');  
                    
                    //var affiliationNetworkSelect = component.find("InputSelectAffiliationNetwork");
                    //if( affiliationNetworkSelect != undefined )
                    //affiliationNetworkSelect.set("v.disabled", false);
                    //COMMENTED component.getAffiliationNetworkOptions();
                }

                //var opts = [];
                //opts.push({"class": "optionClass", label: option_none, value: 'None'});
                //component.find("InputSelectTeamIns").set("v.options", opts);
                //component.find("InputSelectTeamIns").set("v.value","None");
                //component.set("v.hasBusinessRuleTemplate", false);
            }

            else if( isCallPlan && isFromRule){
                //$j('#filePath').addClass('slds-hide');
                //$j('#copyExistingDiv').removeClass('slds-hide');
                //$j('#fileCopyDiv').addClass('slds-hide');
                //$j('#affiliationDivAlignment').addClass('slds-hide');
                //$j('#fileCopyDivAlignment').addClass('slds-hide');
                //$j('#geoDivAlignment').addClass('slds-hide');
                //$j('#geoDivAlignment').removeClass('requiredInput');
                $j('#summaryDiv').addClass('slds-hide');
                //$j('#divAlignmentype').addClass('slds-hide');
                 //$j('#fileTemplateRow').addClass('slds-hide');
                //$j('#divAffliationbasedAlignment').addClass('slds-hide');
                //component.set("v.hasBusinessRuleTemplate", true);
                ///COMMENTED component.teamInstanceOptions();
            }  
            else if(isAlignment && isFromRule)
            {
                //$j('#filePath').addClass('slds-hide');
                //$j('#copyExistingDiv').removeClass('slds-hide');
                //$j('#fileCopyDiv').addClass('slds-hide'); 
                //$j('#affiliationDivAlignment').addClass('slds-hide');
                // $j('#fileCopyDivAlignment').addClass('slds-hide');
                //$j('#geoDivAlignment').addClass('slds-hide');
                //$j('#geoDivAlignment').removeClass('requiredInput');
                $j('#summaryDiv').addClass('slds-hide');
                // $j('#divAlignmentype').addClass('slds-hide');
                // $j('#divAffliationbasedAlignment').addClass('slds-hide');
                //component.set("v.hasBusinessRuleTemplate", true);
            }
            else if(isCallPlan && isFromFile){
                //component.set("v.hasBusinessRuleTemplate", false);
            }

            
            else{
                // $j('#filePath').addClass('slds-hide');
                // $j('#geoDivAlignment').addClass('slds-hide');
                //$j('#copyExistingDiv').addClass('slds-hide');
                // $j('#fileCopyDiv').addClass('slds-hide');
                // $j('#affiliationDivAlignment').addClass('slds-hide');
                // $j('#fileCopyDivAlignment').addClass('slds-hide');
                // $j('#summaryDiv').addClass('slds-hide');
                // $j('#divAffliationbasedAlignment').addClass('slds-hide');
                // $j('#divAlignmentype').addClass('slds-hide');
                //component.set("v.hasBusinessRuleTemplate", false);
                //component.find("InputSelectTeam").set("v.value","None");
                //var opts = [];
                //opts.push({"class": "optionClass", label: option_none, value: 'None'});
                //component.find("InputSelectTeamIns").set("v.options", opts);
                //component.find("InputSelectTeamIns").set("v.value","None");
               // component.find("SelectTeamFile").set("v.value","None"); 

                //component.set('v.isScenarioCreatedFromFile',false);
                //component.set('v.isScenarioCreatedFromRule',false);
                //component.set('v.isScenarioCreatedFromExisting',false);
            }

            /* commenting code to set "None" to alignment type and geo layer, SIQ-2422 */
            // component.set('v.alignmentType','None');
            // component.set('v.newScenario.geoLayer','None');

            //COMMENTED SIQ 1022
            // var affiliationCheckbox = component.find('affiliationCheckbox');
            // var affiliationSelComponent = component.find("InputSelectAffiliationNetwork");
            // if(!$A.util.isUndefined(affiliationSelComponent) && !$A.util.isUndefined(affiliationCheckbox)){
            //     if(!$A.util.isUndefined(affiliationSelComponent[0])){
            //         //IN CASE INPUT IS RERENDERED IT GOES INTO ARRAY
            //         affiliationSelComponent = affiliationSelComponent[0];
            //     }
            //     affiliationCheckbox.set('v.value',false);
            //     affiliationSelComponent.set("v.value",'None');
            //     affiliationSelComponent.set("v.disabled",true);
            //     affiliationCheckbox.set('v.disabled',true);
            // }

    },


    openToastMessage : function(component,event,helper){
            var toast = component.find("validation_failed_popover");
            $A.util.toggleClass(toast,'slds-hide');
    },

    closeToast : function(component,event,helper)
    {
            var toast = component.find("validation_failed_popover");    
            $A.util.addClass(toast,'slds-hide');
            component.set("v.errorList",[]);
            helper.enableButton(component,event,helper);
            helper.clearErrors(component,event,helper);
            component.set('v.hasValidationFailed',false);
    },

    alignmentTypeOptions : function(component,event,helper){
        var opts = [];
        //SIMPS-1392
        //var action = component.get("c.getAlignmentType");
        var action = component.get("c.getTeamDetails");
        var option_none = component.get("v.noneValue"); 
        action.setParams({teamId : component.get("v.newScenario.teamName")});
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if(state == "SUCCESS")
            {
                var opts = [];
                var teamType='';
                var ns = component.get('v.namespace');
                console.log(response.getReturnValue());
                /*opts.push({"class": "optionClass", label: option_none, value: 'None'});
                for(var obj in response.getReturnValue())
                {
                    opts.push({"class": "optionClass", label: response.getReturnValue()[obj], value: obj});
                }*/
                var teamsAttributes = response.getReturnValue();
                if(teamsAttributes!=undefined){
                    opts.push({"class": "optionClass", label: teamsAttributes[ns+'Alignment_Type__c'], value: teamsAttributes[ns+'Alignment_Type__c'] });
                }
                var alignmentTypeSelect = component.find("InputSelectAlignType");
                if(alignmentTypeSelect != undefined){
                    alignmentTypeSelect.set("v.options", opts);  

                    /* set default value as first, SIQ-2422 */
                    if(opts.length == 2) {
                        //only one option available
                        component.set("v.alignmentType", opts[1].value);
                    }
                }

                var teamType = component.find("inputTeamType");
                if(teamType != undefined){
                    component.set("v.teamType", teamsAttributes[ns+'Type__c']);

                }
            } 
            else if(state == "ERROR")
            {
                helper.logException(cmp, response.getError());
            }
        });
        $A.enqueueAction(action);

        
        helper.getBaseTeamInstance(component,event,helper);
        
    },

    getconvergeOption : function(component,event,helper){
        var action = component.get("c.getConvergesLevel");
        var option_none = component.get("v.noneValue"); 
        var scenarioname = component.find('inputSelectTeamInstance');
        console.log('getconvergeOption');
        var createTypeSelect = scenarioname.get("v.value");
        console.log(createTypeSelect);
        if(createTypeSelect!='None'){
            action.setParams({scenarioId : createTypeSelect});
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state == "SUCCESS"){
                    var opts = [];                
                    var ns = component.get('v.namespace');
                    opts.push({"class": "optionClass", label: option_none, value: 'None'}); 
                    opts.push({"class": "optionClass", label: '0', value: '0'});              
                    
                    for(var obj in response.getReturnValue()){
                        opts.push({"class": "optionClass", label: response.getReturnValue()[obj][ns+'Configuration_Name__c'], value: response.getReturnValue()[obj][ns+'Configuration_Name__c']});
                    }

                    component.set("v.convergeLevel", opts);

                }
                else if(state == "ERROR")
                {
                    helper.logException(component, response.getError());
                } 

            });
            $A.enqueueAction(action);
        }
        else{
            var opts = [];                
            var ns = component.get('v.namespace');
            opts.push({"class": "optionClass", label: option_none, value: 'None'});
            component.set("v.convergeLevel", opts);
        }
        

    },


    setDefaultOption : function(component,event,helper){
        try{
            var option_none = component.get("v.noneValue"); 
            var affiliationSelComponent = component.find("InputSelectAffiliationNetwork");
            
            if(!component.get("v.checkedAffilation") && !$A.util.isUndefined(affiliationSelComponent)){
                if(!$A.util.isUndefined(affiliationSelComponent[0])){
                    //IN CASE INPUT IS RERENDERED IT GOES INTO ARRAY
                    affiliationSelComponent = affiliationSelComponent[0];
                }
                affiliationSelComponent.set("v.value",option_none);
                affiliationSelComponent.set("v.errors", []); 
                helper.clearErrors();
            }

            if(!$A.util.isUndefined(affiliationSelComponent)){
                if(!$A.util.isUndefined(affiliationSelComponent[0])){
                    //IN CASE INPUT IS RERENDERED IT GOES INTO ARRAY
                    affiliationSelComponent = affiliationSelComponent[0];
                }
                affiliationSelComponent.set("v.errors", []);
                helper.clearErrors(component,event,helper); 
            }
        }catch(e){
            console.log(e);
        }
       
    },

    toggleAffNetworkAttributes :  function(component,event,helper){
        var affiliationCheckbox = component.find('affiliationCheckbox');
        var affiliationSelComponent = component.find("InputSelectAffiliationNetwork");
        var alignmentType = component.get('v.alignmentType');
        var option_none = component.get("v.noneValue");
        if(!$A.util.isUndefined(affiliationSelComponent)){
            if(!$A.util.isUndefined(affiliationSelComponent[0])){
                //IN CASE INPUT IS RERENDERED IT GOES INTO ARRAY
                affiliationSelComponent = affiliationSelComponent[0];
            }
        }
        var zipVal;
        var accntVal;
        var hybridVal;
        var noneVal;
        var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
        try{
            if(namespace != ''){
                                            
                zipVal = $A.get("$Label.AxtriaSalesIQTM.PICKLIST_ZIP_ALIGNMENT_TYPE");
                accntVal = $A.get("$Label.AxtriaSalesIQTM.PICKLIST_ACCOUNT_ALIGNMENT_TYPE");
                hybridVal = $A.get("$Label.AxtriaSalesIQTM.PICKLIST_HYBRID_ALIGNMENT_TYPE");
                noneVal = $A.get("$Label.AxtriaSalesIQTM.PICKLIST_NONE");

             }else{
                zipVal = $A.get("$Label.c.PICKLIST_ZIP_ALIGNMENT_TYPE");
                accntVal = $A.get("$Label.c.PICKLIST_ACCOUNT_ALIGNMENT_TYPE");
                hybridVal = $A.get("$Label.c.PICKLIST_HYBRID_ALIGNMENT_TYPE");
                noneVal = $A.get("$Label.c.PICKLIST_NONE");
             }

            if((alignmentType.toLowerCase() == zipVal.toLowerCase() )) {
                affiliationCheckbox.set('v.value',false);
                affiliationCheckbox.set('v.disabled',true);
                affiliationSelComponent.set("v.value",option_none);
            }else if(( alignmentType.toLowerCase() == accntVal.toLowerCase() || alignmentType.toLowerCase()  == hybridVal.toLowerCase())){
                affiliationCheckbox.set('v.disabled',false);
                affiliationCheckbox.set('v.value',false);
            }
            else if(( alignmentType.toLowerCase() == noneVal.toLowerCase() )){
                affiliationCheckbox.set('v.disabled',true);
                affiliationCheckbox.set('v.value',false);
                affiliationSelComponent.set("v.value",option_none);
            } 
            affiliationSelComponent.set("v.errors", []); 
            helper.clearErrors(component,event,helper);
        }catch(e){
            console.log(e);
        }
    },

    toggleNetworkCheckbox : function(component,event,helper){
        var affiliationSelComponent = component.find("InputSelectAffiliationNetwork");
        var affiliationCheckbox = component.find('affiliationCheckbox');
        try{
            if(!$A.util.isUndefined(affiliationSelComponent)){
                if(!$A.util.isUndefined(affiliationSelComponent[0])){
                    //IN CASE INPUT IS RERENDERED IT GOES INTO ARRAY
                    affiliationSelComponent = affiliationSelComponent[0];
                }
                var valAf = affiliationSelComponent.get("v.value");
                if(valAf == 'None'){
                    affiliationCheckbox.set('v.disabled',false);
                    affiliationCheckbox.set('v.value',false);
                }  

                //RESET ERRROS ON CHANGE
                affiliationSelComponent.set("v.errors", []);
                helper.clearErrors(component,event,helper); 
            }
        }catch(e){
            console.log(e);
        }
    }
})