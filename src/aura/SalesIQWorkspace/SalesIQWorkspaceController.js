({
	doInit : function(component,event,helper)
	{
		var datatable = component.find('DataTable');
		datatable.getNamespace();
	},

    createRecord : function (component, event, helper) 
    {
    	var namespace = component.get("v.namespace");
	    var createRecordEvent = $A.get("e.force:createRecord");
	    createRecordEvent.setParams
	    ({
	        "entityApiName": namespace+"Workspace__c"
	    });
	    createRecordEvent.fire();
	},

	updateCountryAttributes : function(component, event, helper){
        var countryId = event.getParam('countryId');

        var flag = component.find('flagComponent');
        flag.updateCountryAttributes(countryId);
    }
})