({
    viewChange : function(component,event,helper)
    {
        var datatable = component.find('DataTable');
        var viewId = event.getParam("viewId");
        var viewName = event.getParam("viewName");
        // var $j = jQuery.noConflict();
        component.set("v.selectedViewName",viewName);
        datatable.changeView(viewId);
    },
    leftslider : function(component,event,helper)
    {
      var modalListView =  component.find('listViewComp');
      modalListView.slider('left');
    },
    rightslider : function(component,event,helper)
    {
      
      var modalListView =  component.find('listViewComp');
      modalListView.slider('right');
      
    },
    hideListViewButtons: function(component,event,helper)
    {
        var anchorButton = component.find('anchorButton');
        $A.util.addClass(anchorButton,'slds-hide');
    },
    updateCountryAttributes: function(component, event, helper) 
    {
        console.log('Caught update attribute - Call Plan Controller');
        var datatable = component.find('DataTable');
        var countryId = event.getParam('countryId');
        datatable.updateCountryAttributes(countryId);

        var flag = component.find('flagComponent');
        flag.updateCountryAttributes(countryId);
    },

    positionActionOpen : function(component, event, helper){

        console.log('--- calling positionActionOpen in employeeuniverse----');
        
        var actionTypeValue = event.getParam("actionType");
        
        if(actionTypeValue != 'Dynamic Style'){
            var modalTitleValue = event.getParam("modalTitle");
            var selectedPositionValue = event.getParam("selectedPosition");
            var selectedTeamInsValue = event.getParam("teamInstance");
            var selectedTeamType = event.getParam("teamType");

            component.set("v.selectedAction", actionTypeValue);
            component.set("v.selectedTitle", modalTitleValue);
            component.set("v.recordId",selectedPositionValue);
            
            console.log('selectedPosition  - '+selectedPositionValue);
            console.log('actionTypeValue : '+actionTypeValue);
            console.log('modalTitleValue : '+modalTitleValue);
            console.log('--- 2 '+component.get("v.queryString"));
            
            component.set("v.showEditEmployeePopup", true);
        }


    }

})