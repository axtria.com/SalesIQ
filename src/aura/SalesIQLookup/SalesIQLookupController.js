({
	itemSelected : function(component, event, helper) {
        console.log('itemSelected called');
		helper.itemSelected(component, event, helper);
	}, 
    serverCall :  function(component, event, helper) {
        console.log('serverCall called');
        document.getElementById("combobox-unique-id").autocomplete  = "off";
		helper.serverCall(component, event, helper);
	},
    clearSelection : function(component, event, helper){
        console.log('clearSelection called');
        helper.clearSelection(component, event, helper);
    } 
})