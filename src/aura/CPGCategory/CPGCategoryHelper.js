({    
    getRuleTypeValues : function(component) {        
        var action = component.get("c.getBusinessRuleTypeMaster");        
        action.setCallback(this, function(response){ 
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS")
            {
                var opts = [];
                var arrRuleTypes=response.getReturnValue();
                for(var ctr=0;ctr<arrRuleTypes.length;ctr++){                                                           
                    if(arrRuleTypes[ctr].Name=='Category')
                        opts.push({label:arrRuleTypes[ctr].Id, value: arrRuleTypes[ctr].Name, selected:arrRuleTypes[ctr].Name});
                    else                    
                        opts.push({label:arrRuleTypes[ctr].Id, value: arrRuleTypes[ctr].Name});                                    
                }
                component.set("v.ruleOptions",opts);                  
            }
            else{
                console.log("Failed with state:  "+ state);
            }
        })
        $A.enqueueAction(action);
    },
    getColumnDetailValue:function(component){
        var action = component.get("c.getColumnsData");
        action.setParams({ "scenarioInstanceId" : component.get("v.scenarioRuleInstanceId")});
        action.setCallback(this, function(response){ 
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS")
            {
                var opts = [];
                var arrRuleTypes=response.getReturnValue();
                for(var ctr=0;ctr<arrRuleTypes.length;ctr++){                    
                    opts.push({label:arrRuleTypes[ctr].Id, value: arrRuleTypes[ctr].Name});                    
                }
                component.set("v.ruleOptions",opts);   
            }
            else{
                console.log("Failed with state:  "+ state);
            }
        })
        $A.enqueueAction(action);
    },
    showPopupHelper: function(component, componentId, className){   
        component.set("v.popoverMessage2","");                
        var errorIcn = component.find("errorIcon2");
        $A.util.addClass(errorIcn,'slds-hide');
        
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className + 'slds-hide');
        $A.util.addClass(modal, className + 'slds-show');        
        //hide value section
        var valueSec= component.find("ValueSection"); 
        $A.util.removeClass(valueSec , 'display');
        $A.util.addClass(valueSec , 'hide');
        //set valuetype as discreete
        component.find("valueTypes").set("v.value","Continuous");
        component.find("dataTypes").set("v.value","Numeric");
        var sname = component.find("values");
        $A.util.removeClass(sname , 'errorInput');
        var sname2 = component.find("catgName");
        $A.util.removeClass(sname2 , 'errorInput');
        
    },
    hidePopupHelper: function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'slds-hide');
        $A.util.removeClass(modal, className+'slds-show');
        
    },
    showQueryPopupHelper: function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className + 'hide');
        $A.util.addClass(modal, className + 'open');                
    },
    hideQueryPopupHelper: function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
        
    },
    getOperatortype : function(component, event, helper) {
        console.log("inside operationtype select method");
        var action = component.get("c.getOperatorTypeValues");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS")
            {
                var opts = [];
                var arrOperatorTypes=[];
                var arrExpressions=response.getReturnValue(); 
                component.set("v.operators",response.getReturnValue());
                
                console.log("CHECK");
                console.log(arrExpressions);
                for(var arrCtr=0;arrCtr<arrExpressions.length;arrCtr++){
                    if(arrOperatorTypes.indexOf(arrExpressions[arrCtr].Operator_Type__c)==-1){
                        arrOperatorTypes.push(arrExpressions[arrCtr].Operator_Type__c);
                    }  
                }
                opts.push({label: 'Select an Option', value: 'Select an Option'});
                for(var ctr=0;ctr<arrOperatorTypes.length;ctr++){ 
                    
                    opts.push({label:arrOperatorTypes[ctr],value: arrOperatorTypes[ctr]});
                    component.set("v.typeoptions", opts);                 }
            }
            else{
                console.log("Failed with state:  "+ state);
            }
        })
        $A.enqueueAction(action);
    },
    getFields : function(component, event, helper) {
        console.log("inside getFields controller");  
        var sampledata = [{"fieldName":"gattexlead_flag","fieldType":"source","dataType":"Numeric"}
                          ,{"fieldName":"callactivity_flag","fieldType":"Derived","dataType":"Text"}];
        component.set("v.columndata",sampledata);
        
    },
    RebindGroup: function(wrapperBlock, idB){       
        //var lstBlock = component.get('v.wrapperBlock');                 
        var block = wrapperBlock[idB]; // each block
        console.log('rebind block');
        console.log(block);
        var lstGroup= block.lstWrapGroup; // lstGrp under each block
        var criSeq=1;
        for(var k=0; k < lstGroup.length; k++){
            var grp = lstGroup[k];
            var lstCriteria = grp.lstwrapCriteria; //list of criteria
            for(var j=0; j < lstCriteria.length; j++){
                var cri = lstCriteria[j];
                cri.seqNo=criSeq;
                criSeq++;
            }
        }        
        // component.set('v.wrapperBlock['+idB+']',block);        
    },
    ToggleCollapseHandler: function(component, event) {          
        var idx = event.currentTarget.getAttribute("data-index");
        //console.log(idx);
        var container = document.getElementById("Dummy_"+idx);         
        
        var isExpandable = $A.util.hasClass(container, "slds-is-open");      
        if(isExpandable){            
            $A.util.removeClass(container, 'slds-is-open');
        }
        else{            
            $A.util.addClass(container, 'slds-is-open'); 
        }
    },
    
    DisplayQuery:function(component){
        
        var oTextarea = component.find("oTextarea");       
        var lstBlock = component.get('v.wrapperBlock');         
        var executionLogic='';        
        
        for(var m=0; m< lstBlock.length; m++){
            var block = lstBlock[m];
            if(m==0)
                executionLogic+="If";
            else
                executionLogic+="\nElse If";
            var lstCategory= block.lstOutputCriteria;
            var lstGroup= block.lstWrapGroup; // lstGrp under each block
            for(var k=0; k < lstGroup.length; k++){
                var grp = lstGroup[k];
                var lstCriteria = grp.lstwrapCriteria; //list of criteria
                executionLogic+= '(';
                for(var j=0; j < lstCriteria.length; j++){
                    var cri = lstCriteria[j];  
                    if(cri.fieldVal == null)
                        cri.fieldVal='';
                    console.log(cri.criName);
                    if(cri.criName != '-'){                        
                        executionLogic+=' '+cri.criName+' '+cri.selFieldName.split('@')[4] +' '+ cri.selOpVal.split('@')[3] +' '+cri.fieldVal;
                    }
                    else{                        
                        executionLogic+=cri.selFieldName.split('@')[4] +' '+ cri.selOpVal.split('@')[3] +' '+cri.fieldVal;
                    }                    
                }                                    
                executionLogic+=') ';    
                if(k!=lstGroup.length-1){                    
                    executionLogic+=lstGroup[k+1].selGroupOp+' ';
                }
            }   
            if(lstCategory != null){
                executionLogic+= "\nThen (";
                for(var p=0;p<lstCategory.length;p++){
                    if(p!=0)
                        executionLogic+=" and ";
                    var categ=lstCategory[p];
                    if(categ.selValue== null || categ.selValue=='---')
                        categ.selValue='';
                    executionLogic+= categ.categName+" = "+categ.selValue;                
                }
                executionLogic+=") ";
            }
        }
        
        if(lstCategory != null){            
            var mesLogic='';
            var lstElseCategory= lstBlock[0].lstElseCriteria;
            for(var p=0;p<lstElseCategory.length;p++){                
                var categ=lstElseCategory[p];
                 if(categ.selValue== null )
                        categ.selValue='';
                if(categ.selValue !='' && categ.selValue !='---'){
                    if(p!=0 && mesLogic!= '')
                    mesLogic+=" and ";
                    mesLogic+= categ.categName+" = "+categ.selValue;                
                }
            }
            if(mesLogic!='')
                executionLogic+= "\nElse ("+mesLogic+") ";                
        }
        
        oTextarea.set("v.value", executionLogic);
        
    },
    AddCategoryToElseBlock:function(component){
        var values='';
        var lstCriteria = component.get("v.OutputCategory");        
        var wrapperBlock = component.get('v.wrapperBlock');
        var len= wrapperBlock.length;
        var criLen = lstCriteria.length;
        var nwC = parseInt(criLen);
        var action1 = component.get("c.AddDynamicData");
        if(component.find("valueTypes").get("v.value")=='Discrete')
            values=component.find("values").get("v.value");   
         var isNewField = 'true';
        if(component.get('v.isNewCateg') != null)
            isNewField=component.get('v.isNewCateg');
        
        action1.setParams({   
            "lstCriteria" :JSON.stringify(component.get("v.OutputCategory")),
            "scenarioId": component.get("v.scenarioRuleInstanceId"),            
            'strCategName':component.find('catgName').get('v.value'),
            'strDataType':component.find("dataTypes").get("v.value"),
            'strValueType':component.find("valueTypes").get("v.value"),
            'strValues':values,
            'isNewCateg':isNewField           
        });
        
        action1.setCallback(this, function(response){                                  
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS")
            {               
                if(response.getReturnValue() != null){
                    var updatedCategory = response.getReturnValue(); //new category 
                    var responseData = JSON.parse(JSON.stringify(updatedCategory));
                    component.set('v.wrapperBlock[0].lstElseCriteria['+nwC+']',responseData);  
                }
                else
                {
                    var sname = component.find("catgName");
                    $A.util.addClass(sname , 'errorInput');                    
                    component.set("v.popoverMessage2","Derived Field Name already exists");                
                    var errorIcn = component.find("errorIcon2");
                    $A.util.removeClass(errorIcn,'slds-hide'); 
                }
            }
            else{
                console.log("Failed with state:  "+ state);
            }
        })
        $A.enqueueAction(action1);
    },
    AddCategoryToThanBlock:function(component){
        var lstCriteria = component.get("v.OutputCategory");        
        var wrapperBlock = component.get('v.wrapperBlock');
        var len= wrapperBlock.length;
        var criLen = lstCriteria.length;
        var nwC = parseInt(criLen);
        var values='';
        var isNewField = 'true';
        if(component.get('v.isNewCateg') != null)
            isNewField=component.get('v.isNewCateg');
        var action = component.get("c.AddDynamicData");
        if(component.find("valueTypes").get("v.value")=='Discrete')
            values=component.find("values").get("v.value");                        
        action.setParams({  
            "lstCriteria" :JSON.stringify(component.get("v.OutputCategory")),
            "scenarioId": component.get("v.scenarioRuleInstanceId"),
            'strCategName':component.find('catgName').get('v.value'),
            'strDataType':component.find("dataTypes").get("v.value"),
            'strValueType':component.find("valueTypes").get("v.value"),
            'strValues':values,
            'isNewCateg':isNewField
        });
        action.setCallback(this, function(response){                                  
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS")
            {   
                if(response.getReturnValue() != null){
                    var updatedCategory = response.getReturnValue(); //new category                    
                    var arrCategory = JSON.parse(JSON.stringify(lstCriteria));
                    arrCategory.push(updatedCategory);                
                    component.set('v.OutputCategory',arrCategory);
                    for(var i=0; i< len ; i++){                                        
                        component.set('v.wrapperBlock['+i+'].lstOutputCriteria['+nwC+']',updatedCategory);
                    }                                                        
                    component.find('catgName').set('v.value', '');                                
                    component.find('values').set('v.value', ''); 
                }
                else
                {
                    var sname = component.find("catgName");
                    $A.util.addClass(sname , 'errorInput');
                    
                    component.set('v.isValid','false');
                    component.set('v.popoverMessage','Derived Field Name already exists');
                    var errorIcn = component.find("errorIcon");
                    $A.util.removeClass(errorIcn,'slds-hide');
                }
            }
            else{
                console.log("Failed with state:  "+ state);
            }
        })
        $A.enqueueAction(action);
    },
    addCriteria : function(component,event,helper) {              
        var idx = event.currentTarget.getAttribute("data-recId");
        var idG= parseInt(idx.split('_')[0]);
        var idB= parseInt(idx.split('_')[1]);        
        var wrapperBlock = component.get('v.wrapperBlock');           
        var wrapperGroup =wrapperBlock[idB].lstWrapGroup;  
        var wrapperOutputCriteria = wrapperBlock[idB].lstOutputCriteria;  
        var wrapperCriteria =(wrapperGroup)[idG].lstwrapCriteria;        
        wrapperBlock.criteriaSeq=parseInt(wrapperCriteria.length+1);
        console.log(parseInt(wrapperCriteria.length+1));       
        var action =  component.get("c.AddCriteria");
        action.setParams({
            "scenarioId": component.get("v.scenarioRuleInstanceId")
        });
        action.setCallback(this, function(data){            
            var state = data.getState();            
            if (state === "SUCCESS") {
                
                var response= data.getReturnValue();                  
                wrapperCriteria.push(response);
				component.set('v.wrapperBlock['+idB+'].lstWrapGroup['+idG+'].lstwrapCriteria',wrapperCriteria);                
				//component.set('v.wrapperBlock',wrapperBlock);          
                //component.set('v.wrapperBlock',wrapperBlock);          
               	helper.changeSequenceOrder(component);
            }           
            else if (state === "ERROR") {                
                console.log(response.getError());
            }                              
        });
        $A.enqueueAction(action); 
    },
    addGroup:function(component,event,helper){          
        var idx = event.currentTarget.getAttribute("data-recId");
        component.set('v.grpSeq',idx);
        console.log(idx);
        var idG= parseInt(idx.split('_')[0]);
        var idB= parseInt(idx.split('_')[1]);
        var nwG = parseInt(idG+1);
        console.log('grpBind_'+nwG+'_'+idB);        
        var wrapperBlock= component.get('v.wrapperBlock');
        //wrapperBlock[idB].lstWrapGroup[idG].selGroupOp='AND';
        var action =  component.get("c.AddAGroup"); 
        action.setParams({
            "scenarioId": component.get("v.scenarioRuleInstanceId")
        });
        action.setCallback(this, function(data){   
            
            var state = data.getState();
            console.log(state);
            if (state === "SUCCESS") {              
                var response= data.getReturnValue();  //brand new grp               
                console.log(response);                
                component.set("v.newGrp",response);   
                helper.resequenceGroup(component,event,helper);
            }           
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log(errors);
            }                              
        });
        $A.enqueueAction(action); 
        var modal = document.getElementById('grpOp_'+idG+'_'+idB);
        $A.util.removeClass(modal, 'hide');
        $A.util.addClass(modal, 'display');         
    },
    deleteGroup:function(component,event,helper){        
        var idx = event.currentTarget.getAttribute("data-recId");
        var idG= parseInt(idx.split('_')[0]);
        var idB= parseInt(idx.split('_')[1]);
        var lstwrapperBlock= component.get('v.wrapperBlock');
        var lstGrp = lstwrapperBlock[idB].lstWrapGroup;   
        if(lstGrp.length==1){
            var sname = component.find("ruleName");
            $A.util.addClass(sname , 'errorInput');            
            component.set('v.isValid','false');
            component.set('v.popoverMessage','You cannot delete the only group of the block');
            var errorIcn = component.find("errorIcon");
            $A.util.removeClass(errorIcn,'slds-hide');
        }
        else{
            lstGrp.splice(idG, 1);
            var modal = document.getElementById('grpOp_'+idG-1+'_'+idB); 
            $A.util.addClass(modal, 'hide');
            $A.util.removeClass(modal, 'display'); 
            component.set("v.wrapperBlock[idB].lstWrapGroup", lstGrp);                
            helper.changeSequenceOrder(component);
        }
    },
    resequenceCriteria:function(component,event,helper){ 
        console.log('resequenceGroup');
        var idx=event.currentTarget.getAttribute("data-index");  
        var wrapperBlock= component.get('v.wrapperBlock');
        var idB= parseInt(idx.split('_')[0]);
        var idG= parseInt(idx.split('_')[1]);
        var idC = parseInt(idx.split('_')[2]);
        var arrayGrps = [];
        var lstCriteria = wrapperBlock[idB].lstWrapGroup[idG].lstwrapCriteria
        
        //push all groups before inserting a new group
        for(var i=0; i<idC; i++){
            arrayGrps.push(lstCriteria[i]);
        }
        
        for(var i=idC+1; i<lstCriteria.length; i++){ //2 ---
            arrayGrps.push(lstCriteria[i]);             
        }       
        component.set('v.wrapperBlock['+idB+'].lstWrapGroup['+idG+'].lstwrapCriteria',arrayGrps);               
    },
    resequenceGroup:function(component,event,helper){ 
        console.log('resequenceGroup');
        var idx =component.get('v.grpSeq');// event.currentTarget.getAttribute("data-recId");
        var wrapperBlock= component.get('v.wrapperBlock');
        var idG= parseInt(idx.split('_')[0]); //1
        var idB= parseInt(idx.split('_')[1]);
        var arrayGrps = [];
        var block = wrapperBlock[idB];
        var lstGrp = block.lstWrapGroup; //5
        //push all groups before inserting a new group
        for(var i=0; i<=idG; i++){
            arrayGrps.push(lstGrp[i]);
        }
        //add newly added group
        var response = component.get("v.newGrp");
        arrayGrps.push(response); //2
        for(var i=idG+1; i<lstGrp.length; i++){ //2 ---
            arrayGrps.push(lstGrp[i]);             
        }       
        component.set('v.wrapperBlock['+idB+'].lstWrapGroup',arrayGrps);       
        helper.changeSequenceOrder(component);
    },
    changeSequenceOrder:function(component){
        
      var wrapperBlock= component.get('v.wrapperBlock');
        for(var m=0; m<wrapperBlock.length; m++){
            var block = wrapperBlock[m]; // each block  
            var criteriaLogic='';
            var executionLogic='';
            var lstGroup= block.lstWrapGroup; // lstGrp under each block
            var criSeq=1;
            for(var k=0; k < lstGroup.length; k++){
                var grp = lstGroup[k];
                var lstCriteria = grp.lstwrapCriteria; //list of criteria
                criteriaLogic+= '(';
                if(k==0)
                    executionLogic+='if(';                    
                else if(k == lstGroup.length-1)
                    executionLogic+= 'else';
                    else
                        executionLogic+= 'else if('; 
                for(var j=0; j < lstCriteria.length; j++){
                    var cri = lstCriteria[j];
                    cri.seqNo=criSeq;
                    console.log(cri.criName);
                    if(cri.criName != '-'){
                        criteriaLogic+=' '+cri.criName+' '+cri.seqNo;
                        executionLogic+=' '+cri.criName+' '+cri.selFieldName.split('@')[4] +' '+ cri.selOpVal.split('@')[3] +' '+cri.fieldVal;
                    }
                    else{
                        criteriaLogic+=cri.seqNo;
                        executionLogic+=cri.selFieldName.split('@')[4] +' '+ cri.selOpVal.split('@')[3] +' '+cri.fieldVal;
                    }
                    criSeq++;
                }                    
                criteriaLogic+=') ';    
                executionLogic+=') ';    
                if(k!=lstGroup.length-1){
                    criteriaLogic+= lstGroup[k+1].selGroupOp+' ';
                    executionLogic+=lstGroup[k+1].selGroupOp+' ';
                }
            }            
            block.criteriaLogic=criteriaLogic;
            block.executionLogic=executionLogic;           
        }
         component.set('v.wrapperBlock',wrapperBlock);                 
    },
    handleAndPress : function(component) {
        console.log("button AND pressed");
    },
    handleORPress : function(component) {
        console.log("button OR pressed");
    },
    resequenceBlock:function(component){
        var idB =component.get('v.blockSeq');// current block
        var lstBlocks= component.get('v.wrapperBlock');
        var arrayBlocks = [];
        
        var block = lstBlocks[idB];
        //var lstGrp = block.lstWrapGroup; //5
        //push all blocks before inserting a new block
        for(var i=0; i<=idB; i++){
            //lstBlocks[i].selPriority=parseInt(i)+2;
            arrayBlocks.push(lstBlocks[i]);
        }
        //add newly added block
        var response = component.get("v.newBlock");
        arrayBlocks.push(response); //2
        for(var i=parseInt(idB)+1; i<lstBlocks.length; i++){ //2 ---
            lstBlocks[i].selPriority=parseInt(i)+2;
            arrayBlocks.push(lstBlocks[i]);                         
        }       
        component.set('v.wrapperBlock',arrayBlocks);              
    },
    addBlock: function(component,event,helper){    
        
        var idB = event.currentTarget.getAttribute("data-index");
        var wrapperBlock = component.get('v.wrapperBlock');
        var priority = wrapperBlock[0].priority[wrapperBlock[0].priority.length-1];                        
        var nextPriority = parseInt(priority)+1;
        wrapperBlock[0].priority.push(nextPriority);
        
        var len= wrapperBlock.length;
        var newLen = parseInt(len+1);
        var outputCategory = component.get('v.OutputCategory');   
        
        var categoryData='';
        if(outputCategory.length>0)
            categoryData=JSON.stringify(outputCategory);
        
        var action =  component.get("c.AddNewBlock");   
        action.setParams({
            'outputCategory' : categoryData,
            "scenarioId": component.get("v.scenarioRuleInstanceId")
        });
        
        action.setCallback(this, function(data){            
            var state = data.getState();
            console.log(state);
            if (state === "SUCCESS") {
                var response= data.getReturnValue();  //brand new block                 
                response.selPriority=parseInt(idB)+2;     
                response.priority = wrapperBlock[0].priority;
                component.set('v.newBlock',response);
                component.set('v.blockSeq',idB);
                helper.resequenceBlock(component);
            }           
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log(errors);      
            }                              
        });
        $A.enqueueAction(action); 
        
    },
    deleteBlock:function(component,event){
        var idB = event.currentTarget.getAttribute("data-index");       
        var lstwrapperBlock= component.get('v.wrapperBlock');  
        if(lstwrapperBlock.length>1){
            if(idB==0)
                var lstElseCri= lstwrapperBlock[0].lstElseCriteria;                    
            lstwrapperBlock.splice(idB, 1);                      
            var arrayBlocks = [];    
            var priority = lstwrapperBlock[0].priority[lstwrapperBlock[0].priority.length-1]; 
            var index=lstwrapperBlock[0].priority.length-1;
             lstwrapperBlock[0].priority.splice(index,1);
            //push all blocks before inserting a new block
            for(var i=0; i<lstwrapperBlock.length; i++){               
                lstwrapperBlock[i].selPriority=parseInt(i)+1;
                arrayBlocks.push(lstwrapperBlock[i]);
                if(idB==0 && i==0)
                    lstwrapperBlock[i].lstElseCriteria=lstElseCri;
            }
            component.set("v.wrapperBlock", arrayBlocks);    
        }
        else{            
            var sname = component.find("ruleName");
            $A.util.addClass(sname , 'errorInput');            
            component.set('v.isValid','false');
            component.set('v.popoverMessage','Atleast one block is required to create a rule');
            var errorIcn = component.find("errorIcon");
            $A.util.removeClass(errorIcn,'slds-hide');
        }
    },
    validateCategory:function(component,event, helper){
        var categoryName= component.get('v.CategoryToEdit');
        var action =  component.get("c.ValidateCategory");   
        action.setParams({                        
            'categName':categoryName,
            "scenarioId": component.get("v.scenarioRuleInstanceId"),
        });
        
        action.setCallback(this, function(data){            
            var state = data.getState();
            if (state === "SUCCESS") {
                var response= data.getReturnValue();  //brand new block   
                if(response){                    
                    helper.editCategoryField(component, event,helper);
                }
                else
                {
                    var sname = component.find("categPlaceholder");
                    $A.util.addClass(sname , 'errorInput');                    
                    component.set('v.isValid','false');
                    component.set('v.popoverMessage','Cannot edit Derived Field, as it is being used in other scenario.');
                    var errorIcn = component.find("errorIcon");
                    $A.util.removeClass(errorIcn,'slds-hide');
                    
                    
                }
            }
        });
        $A.enqueueAction(action);
    },
    editCategoryField: function(component, event, helper){
        var categoryName= component.get('v.CategoryToEdit');
        var action =  component.get("c.getCategoryFieldData");   
        action.setParams({                        
            'categName':categoryName,
            'lstCriteria':JSON.stringify(component.get('v.OutputCategory'))  
        });
        
        action.setCallback(this, function(data){            
            var state = data.getState();
            if (state === "SUCCESS") {
                var outPutCriteria= data.getReturnValue();  //brand new block   
                if(outPutCriteria.valueType == 'Discrete')                    
                    component.find("values").set("v.value",outPutCriteria.values);
                component.find("catgName").set("v.value",outPutCriteria.categName);
              //  component.find("dataTypes").set("v.value",outPutCriteria.dataType);
                component.find("valueTypes").set("v.value",outPutCriteria.valueType);
               component.find("dataTypes").set("v.value","Text");
                helper.showPopupHelper(component, 'modaldialog', 'demo-only ');
                component.set('v.isNewCateg', false);
                
               /* 
                var action1 =  component.get("c.UpdateOuputCategoryList");   
                action1.setParams({
                    'lstCriteria':JSON.stringify(component.get('v.OutputCategory')),            
                    'categName':categoryName,
                    'updatedCriteria':outPutCriteria
                });
                
                action1.setCallback(this, function(data){            
                    var state = data.getState();
                    console.log(state);
                    if (state === "SUCCESS") {
                        var response= data.getReturnValue();  //brand new block                                 
                        component.set('v.OutputCategory',response);
                    }           
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        console.log(errors);      
                    }                              
                });
                $A.enqueueAction(action1);  
                */
            }
        });
        $A.enqueueAction(action);
    },
  
})