({    
	initPage : function(component, event, helper){  
        component.set("v.isBtnClick","");
        component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0 !important;}   .forceStyle .viewport.oneHeader.desktop {z-index:0 !important;} .forceStyle.desktop .viewport{overflow:hidden}");
        //Fill Rules and data types
        console.log('==log ruleid=='+component.get('v.businessRuleId'));
        component.getRuleTypes(); 
        component.getTableDetails();
        component.getTypeList();
        if(component.get("v.businessRuleId") != ''){
            component.getExistingRule();
        }        
        component.initializePage();
	},
    changeSequenceOrder:function(component, event, helper) {
        helper.changeSequenceOrder(component, event);
    },
    initializePage:function(component, event, helper) {
        var action1 = component.get('c.initialiseBlock');
        action1.setParams({ 
            "scenarioId" : component.get("v.scenarioRuleInstanceId"),
            "ruleId" : component.get("v.businessRuleId")
        });                
        action1.setCallback(this, function(data){            
            var response= data.getReturnValue();  
            if(response != null){
                var responseData = JSON.parse(JSON.stringify(response));
                var prio=[];
                for(var i=0; i< responseData.length; i++){
                    prio.push(i+1);
                }
                for(var i=0; i< responseData.length; i++){
                    responseData[i].priority= prio;
                    responseData[i].selPriority=i+1;
                }                        
                component.set('v.wrapperBlock',responseData);    
            }
        })        
        $A.enqueueAction(action1);
    },
    deleteABlock: function(component, event,helper){
        helper.deleteBlock(component, event);        
    },
    getExistingRule:function(component, event, helper) {
        var actionRuleName = component.get("c.getRuleName");
            actionRuleName.setParams({ 
                "scenarioId" : component.get("v.scenarioRuleInstanceId"),
                "ruleId" : component.get("v.businessRuleId")
            });
            actionRuleName.setCallback(this, function(data){
                var response= data.getReturnValue();                  
                component.find('ruleName').set('v.value',response);
            })
            $A.enqueueAction(actionRuleName);
            var actionGetCateg = component.get("c.DisplayCategories");
            actionGetCateg.setParams({                 
                "ruleId" : component.get("v.businessRuleId")
            });
            actionGetCateg.setCallback(this, function(data){
                var response= data.getReturnValue();                  
                component.set('v.OutputCategory',response);
            })
            $A.enqueueAction(actionGetCateg);
    },
    getTypeList:function(component, event, helper) {
        var action = component.get("c.getTypesList");
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(component.isValid() && state==="SUCCESS")
            {             
                var dataTypeList=[];
                var valueTypeList=[];
                var itemsSample = response.getReturnValue();                                     
                var ii=0;
                var jj=0;
                for(var ctr=0;ctr<itemsSample.length;ctr++){                   
                    if(itemsSample[ctr].Type__c=="Data Type")
                    {                        
                        dataTypeList.push({label:itemsSample[ctr].Type__c, value: itemsSample[ctr].Value__c});
                        component.set("v.datatypeoptions",dataTypeList);
                        if(ii == 0)
                            component.find("dataTypes").set('v.value',itemsSample[ctr].Value__c);
                        ii++;
                    }                    
                    if(itemsSample[ctr].Type__c=="Value Type")
                    {
                        valueTypeList.push({label:itemsSample[ctr].Type__c, value: itemsSample[ctr].Value__c});
                        component.set("v.valueoptions",valueTypeList);
                        if(jj == 0)
                            component.find("valueTypes").set('v.value',itemsSample[ctr].Value__c);
                        jj++;
                    }
                }
            }
            else{
                console.log("Failed with state:  "+ state);
            }
        })
        $A.enqueueAction(action);        
    },
    
    doneRendering: function(component, event, helper) {    
       // var seq=0;
       // if(seq==0){
      var j$=jQuery.noConflict();
     
            var response= component.get('v.wrapperBlock'); 
        console.log(response);
            for(var k=0; k <response.length; k++){
                var lstWrapGrp = response[k].lstWrapGroup;
                if(lstWrapGrp != null){
                    for(var i=0; i < lstWrapGrp.length; i++){
                        var criList = lstWrapGrp[i].lstwrapCriteria;
                        var btnOR = document.getElementById('btnOR_'+i+'_'+k);
                        var btnAnd = document.getElementById('btnAnd_'+i+'_'+k);
                        var modal = document.getElementById('grpOp_'+i+'_'+k);                
                        if(i!=lstWrapGrp.length-1){                    
                            $A.util.removeClass(modal, 'hide');
                            $A.util.addClass(modal, 'display'); 
                            
                            if(component.get("v.isBtnClick") == ''){
                                if(lstWrapGrp[i+1].selGroupOp == 'OR'){
                                    $A.util.addClass(btnOR,'slds-is-selected');
                                    $A.util.removeClass(btnAnd,'slds-is-selected');
                                }
                                else{
                                    $A.util.addClass(btnAnd,'slds-is-selected');
                                    $A.util.removeClass(btnOR,'slds-is-selected');
                                }
                            }
                        }
                        else if(lstWrapGrp.length==1){                    
                            var modal = document.getElementById('grpOp_'+i+'_'+k);
                            $A.util.removeClass(modal, 'display');
                            $A.util.addClass(modal, 'hide');         
                        }
                        /*for(var a=0;a< criList.length; a++){ 
                        var idType= a+'@'+i+'@'+k;    
                        var textObject = component.find(idType);
                        
                        var cri = criList[a];                   
                        if(cri.selOpVal.split('@')[3] =='is null' || cri.selOpVal.split('@')[3] == 'is not null'){
                            textObject.disabled = true; 
                            textObject.placeholder='';
                        }
                        else{
                            textObject.disabled = false; 
                            textObject.placeholder='Enter Value';
                        }
                    }*/
                }
            }
        }
           // seq++;
        //}
    },
    deleteGroup:function(component,event,helper){
        helper.deleteGroup(component,event,helper);
    },
    getRuleTypeValues:function(component, event, helper) {
		helper.getRuleTypeValues(component);       
    },
    ShowPopup:  function(component, event, helper){
        var sname = component.find("categPlaceholder");
        $A.util.removeClass(sname , 'errorInput');
        component.set('v.isNewCateg', true);
        helper.showPopupHelper(component, 'modaldialog', 'demo-only ');
    },   
    handleInputText:  function(component, event, helper){        
        var sname = component.find("ruleName");
        $A.util.removeClass(sname , 'errorInput');
    },
    handleCategName:  function(component, event, helper){        
        var sname = component.find("catgName");
        $A.util.removeClass(sname , 'errorInput');
    },
    onValuesPress:  function(component, event, helper){        
        var sname = component.find("values");
        $A.util.removeClass(sname , 'errorInput');
    },
    onValueTypeChange :function(component, event, helper){
        var selected = component.find("valueTypes").get("v.value");
        console.log(selected);
    },
    ShowQuery: function(component, event, helper){
        helper.showQueryPopupHelper(component, 'modalViewQuery', 'slds-');       
        helper.DisplayQuery(component);
    },
     HideQueryPopup:  function(component, event, helper){
        helper.hideQueryPopupHelper(component, 'modalViewQuery', 'slds-');
    },
    onOperatorChange : function(component, event, helper){     
        helper.changeSequenceOrder(component);
    },
    SaveUpdateCategory : function(component, event, helper){        
        component.doPageValidation();        
        if(component.get('v.isValid')=='true'){
            //var msgdiv = component.find('divmsg'); 
            var categoryData= component.get('v.OutputCategory');        
            var wrapperData= component.get('v.wrapperBlock');            
            console.log(JSON.stringify(wrapperData));
            var ruleName = component.find('ruleName').get('v.value');
            var scenarioId = component.get('v.scenarioRuleInstanceId');
            var ruleId  = component.get("v.businessRuleId");
            var action = component.get('c.SaveCheck');        
            action.setParams({ 
                'categoryData':JSON.stringify(categoryData), 
                'wrapperData':JSON.stringify(wrapperData),
                'ruleName':ruleName,
                'scenarioId':scenarioId,
                'ruleId' : ruleId
            });
            action.setCallback(this, function(data){
                var state= data.getState();
                console.log(state);
                if(component.isValid() && state==="SUCCESS"){
                    var response= data.getReturnValue();
                    console.log(response);
                    if(response== 'true'){
                        component.closeButton(); 
                       /* if(ruleId!='')
                            alert('Business Rule updated successfully!');
                        else
                            alert('Business Rule created successfully!');
                        console.log(response);                   
                        var mess1= 'You are about to leave this page';
                        var x = confirm(mess1);   
                        if (x == true)  
                        {  
                            component.closeButton(); 
                        } */                    
                    }
                    else{
                        //alert(response);
                        var sname = component.find("ruleName");
                        $A.util.addClass(sname , 'errorInput');
                        
                        component.set('v.isValid','false');
                        component.set('v.popoverMessage',response);
                        var errorIcn = component.find("errorIcon");
                        $A.util.removeClass(errorIcn,'slds-hide');
                    }
                }
                else{
                    var sname = component.find("ruleName");
                    $A.util.addClass(sname , 'errorInput');                    
                    component.set('v.isValid','false');
                    component.set('v.popoverMessage','Some error occured!');
                    var errorIcn = component.find("errorIcon");
                    $A.util.removeClass(errorIcn,'slds-hide');
                }
            })
            $A.enqueueAction(action);
        }        
    },
    doPageValidation:function(component, event, helper){
        var sname = component.find("ruleName");
        $A.util.removeClass(sname , 'errorInput');
        var sname = component.find("categPlaceholder");
        $A.util.removeClass(sname , 'errorInput');
        
        var errMessage='';
        var lstWrapperBlock= component.get('v.wrapperBlock');
        var categoryData= component.get('v.OutputCategory');    
        //Check for rule name to bt not blank
        var ruleName = component.find('ruleName').get('v.value');
        if(!ruleName){
            errMessage+='rule name';
            var sname = component.find("ruleName");
            $A.util.addClass(sname , 'errorInput');
        }
        //Check if output category is not empty
        if(categoryData.length==0){
            if(errMessage!='')
                errMessage+=', ';
            errMessage+='atleast one category';
            //
			var sname = component.find("categPlaceholder");
            $A.util.addClass(sname , 'errorInput');
        }
        else{
            //check all text boxes and then boxes are filled with data
            var arrPriority=[];
            for(var i=0; i<lstWrapperBlock.length; i++){
                var block = lstWrapperBlock[i];                                
                if(arrPriority.indexOf(parseInt(block.selPriority)) == -1){
                    for(var j=0; j <block.lstOutputCriteria.length; j++){
                        var categoryOutput= block.lstOutputCriteria[j];
                        if(categoryOutput.selValue== null || categoryOutput.selValue == '' || categoryOutput.selValue == '--'){
                            if(errMessage!='')
                                errMessage+=', ';
                            errMessage+='value to then clause \n';
                            break;
                        }                    
                    }
                    for(var k=0; k <block.lstWrapGroup.length; k++){
                        var grp = block.lstWrapGroup[k];                 
                        for(var m=0; m < grp.lstwrapCriteria.length; m++){
                            var cri = grp.lstwrapCriteria[m];
                            if(cri.fieldVal == '' && (cri.selOpVal.split('@')[3] !='is null' && cri.selOpVal.split('@')[3] != 'is not null')){
                                if(errMessage!='')
                                    errMessage+=', ';
                                errMessage+='value to if clause';
                                break;
                            }
                            //check datatype validation
                            else{                            
                                var selectedOperator= cri.selFieldName.split('@')[3];
                                if(selectedOperator=='Numeric' && (cri.selOpVal.split('@')[3] !='is null' && cri.selOpVal.split('@')[3] != 'is not null')){
                                    var numberPattern = /^(-?\d*\.?\d*)(,-?\d*\.?\d*)*$/;
                                    var pattern=numberPattern.test(cri.fieldVal);
                                    if(!pattern){
                                        if(errMessage!=''){
                                            errMessage+=', ';
                                        }
                                        errMessage+='datatype mismatch in if clause';
                                        break;                                    
                                    }
                                }
                            }
                        }
                    }
                    
                }
                else{
                    if(errMessage!='')
                            errMessage+=', ';
                        errMessage+='different priorities \n';
                        break;
                }
                arrPriority.push((parseInt(block.selPriority)));
                //check for than block                
                /*var lstElseCategory= lstWrapperBlock[0].lstElseCriteria;
                for(var a =0; a<lstElseCategory.length; a++){
                    var categ=lstElseCategory[a];
                    if(categ.selValue == ''){
                        if(errMessage!='')
                            errMessage+=', ';
                        errMessage+='value to else clause \n';
                        break;
                    }                    
                }*/
            }
            
        }
        if(errMessage !=''){
            component.set('v.isValid','false');
            //alert(errMessage);
            /*var msgDiv= component.find('divmsg');
            $A.util.removeClass(msgDiv,'slds-hide');*/
            component.set('v.popoverMessage','Please enter '+errMessage);
            var errorIcn = component.find("errorIcon");
            $A.util.removeClass(errorIcn,'slds-hide');

        }
        else
        component.set('v.isValid','true');
    },
    SaveDynamicFields : function(component, event,helper) {  
        var errMessage='';
        var Name=component.find('catgName').get('v.value');        
        if(!Name){
            errMessage='Name';
            var sname = component.find("catgName");
            $A.util.addClass(sname , 'errorInput');
        }
        if(component.find("valueTypes").get("v.value")=='Discrete' && !component.find("values").get("v.value"))
        {                            
            if(errMessage!='')
                errMessage+=',';
            errMessage+='Values';    
            var sname = component.find("values");
            $A.util.addClass(sname , 'errorInput');
        }
        else if(component.find("valueTypes").get("v.value")=='Discrete' && component.find("values").get("v.value"))
        {
            var val1=component.find("values").get("v.value");                
            var isValuesCorrect= false;
            var format = /[ !@#^&*()+\-=\[\]{};:\\|.<>\/?]/;
            isValuesCorrect= format.test(val1); 
            if(!isValuesCorrect && component.find("dataTypes").get("v.value")=='Numeric'){
                var m= false;
                if(component.find("dataTypes").get("v.value")=='Numeric'){
                    var numberPattern = /^(-?\d*\.?\d*)(,-?\d*\.?\d*)*$/;
                    m=numberPattern.test(val1);                        
                }
                if(!m){              
                    if(errMessage!='')
                        errMessage+=',';
                    errMessage+="numeric values";                                        
                    var sname = component.find("values");
                    $A.util.addClass(sname , 'errorInput');
                }
            }
            else if(isValuesCorrect){  
                if(errMessage!='')
                    errMessage+=',';
                errMessage+="values without special characters";
                var sname = component.find("values");
                $A.util.addClass(sname , 'errorInput');
            }
        }
        if(errMessage!=''){
            component.set("v.popoverMessage2","Please enter "+errMessage);                
            var errorIcn = component.find("errorIcon2");
            $A.util.removeClass(errorIcn,'slds-hide');            
        }
        else{
            if(((component.find("valueTypes").get("v.value")=='Discrete' && component.find("dataTypes").get("v.value")=='Numeric') && (m))
               ||(component.find("valueTypes").get("v.value")=='Continuous')
               || (component.find("valueTypes").get("v.value")=='Discrete' && component.find("dataTypes").get("v.value")=='Text') && !(m))
            { 
                helper.AddCategoryToThanBlock(component);
                helper.AddCategoryToElseBlock(component);
            }
        }            
    },
    
    HidePopUp:  function(component, event, helper){
        component.find('catgName').set('v.value', '');                                
        component.find('values').set('v.value', '');          
        helper.hidePopupHelper(component, 'modaldialog', 'demo-only ');
    },
    onSelectChange : function(component, event, helper) {
        var selected = component.find("valueTypes").get("v.value");
        console.log(selected);
        var valueSec= component.find("ValueSection"); 
        var sname = component.find("values");
        $A.util.removeClass(sname , 'errorInput');
		component.find('values').set('v.value', '');          
        if(selected=='Discrete'){                                    
            $A.util.removeClass(valueSec , 'hide');
            $A.util.addClass(valueSec , 'display');
        }
        else
        {            
            $A.util.removeClass(valueSec , 'display');
            $A.util.addClass(valueSec , 'hide');
        }       
    },
    
    onChangeOutputCategory: function(component, event, helper){
        helper.hidePopupHelper(component, 'modaldialog', 'demo-only ');                 
    },
    UpdateWrapper: function (component, event, helper){
        console.log('event finaly triggered');
        var response= event.getParam("updateStr"); 
        component.set('v.wrapperBlock',response);
    },
  addABlock: function(component, event, helper){
        helper.addBlock(component,event,helper);
    },
    onOutputFeildsChange : function (component, event, helper) {
       component.set('v.outputData',event.getParam("value"))      
    },
    onChange: function(component, event, helper){
        var wrapperBlock = component.get('v.wrapperBlock');
        var event = component.getEvent('UpdateMainBlockEvent');                
        event.setParams({ 
            "updateStr" : JSON.stringify(wrapperBlock)
        });
        event.fire();   
    },
         
    addAGroup : function(component, event, helper){
        helper.addGroup(component,event,helper);        
    },                          
    RefreshControl: function(component, event, helper){
        var message = event.getParam("outputCategory");
        console.log(message);
    },    
    onSelectchange: function(component, event, helper){                     
        console.log(event.getSource().get('v.value'));
        var changedVal= event.getSource().get('v.value');          
    },
    HideShow: function(component, event, helper) { 
        helper.ToggleCollapseHandler(component, event);
    },
      addACriteria : function(component, event, helper){          
        helper.addCriteria(component,event,helper);
    },
    btnANDClick : function(component, event, helper){
        var idx = event.currentTarget.getAttribute("data-index");
        var idG= parseInt(idx.split('_')[0]);
        var idB= parseInt(idx.split('_')[1]);
        var btnOR = document.getElementById('btnOR_'+idx);
        var btnAnd = document.getElementById('btnAnd_'+idx);
        $A.util.removeClass(btnOR,'slds-is-selected');
        $A.util.addClass(btnAnd,'slds-is-selected');
        
        console.log(idx);
        var wrapperBlock= component.get('v.wrapperBlock');
        var block = wrapperBlock[idB]; // each block                
        var criteriaLogic='';
        var lstGroup= block.lstWrapGroup; // lstGrp under each block                
        for(var k=0; k < lstGroup.length; k++){
            var grp = lstGroup[k];
            var lstCriteria = grp.lstwrapCriteria; //list of criteria
            if(idG==k)
                lstGroup[k+1].selGroupOp ='AND';
            criteriaLogic+= '(';
            for(var j=0; j < lstCriteria.length; j++){
                var cri = lstCriteria[j];
                if(cri.criName != '-')
                    criteriaLogic+=' '+cri.criName+' '+cri.seqNo;
                else
                    criteriaLogic+=cri.seqNo;
            }
            criteriaLogic+=') ';
            console.log(lstGroup.length);
            if(k!=lstGroup.length-1)
                criteriaLogic+= lstGroup[k+1].selGroupOp+' ';
        }
        //code to update sequence ends 
        block.criteriaLogic=criteriaLogic;               
        component.set('v.wrapperBlock',wrapperBlock);                
        component.set("v.isBtnClick","yes");
        
    },
    btnORClick : function(component, event, helper){
        console.log('hwre');
        component.set("v.isBtnClick","yes");
        var idx = event.currentTarget.getAttribute("data-index");
        var idG= parseInt(idx.split('_')[0]);
        var idB= parseInt(idx.split('_')[1]);
        var btnOR = document.getElementById('btnOR_'+idx);
        var btnAnd = document.getElementById('btnAnd_'+idx);
        
        $A.util.removeClass(btnAnd,'slds-is-selected');
        $A.util.addClass(btnOR,'slds-is-selected');
        
        console.log(idx);
        var wrapperBlock= component.get('v.wrapperBlock');
        var block = wrapperBlock[idB]; // each block                
        var criteriaLogic='';
        var lstGroup= block.lstWrapGroup; // lstGrp under each block                
        for(var k=0; k < lstGroup.length; k++){
            var grp = lstGroup[k];
            var lstCriteria = grp.lstwrapCriteria; //list of criteria
            if(idG==k)
                lstGroup[k+1].selGroupOp ='OR';
            criteriaLogic+= '(';
            for(var j=0; j < lstCriteria.length; j++){
                var cri = lstCriteria[j];
                if(cri.criName != '-')
                    criteriaLogic+=' '+cri.criName+' '+cri.seqNo;
                else
                    criteriaLogic+=cri.seqNo;
            }
            criteriaLogic+=') ';
            console.log(lstGroup.length);
            if(k!=lstGroup.length-1)
                criteriaLogic+= lstGroup[k+1].selGroupOp+' ';
        }
        //code to update sequence ends 
        block.criteriaLogic=criteriaLogic;               
        component.set('v.wrapperBlock',wrapperBlock);                
    },
    
    
    
     onRuleChange : function(component, event, helper) {
    
    console.log("inside onRuleChange method");
    var ruleVal = component.get("v.selectedRule");
    var ruleText;  
   var ruleOptions= component.get("v.ruleOptions");
   console.log(ruleOptions[0]);
   for(var ctr=0;ctr<ruleOptions.length;ctr++){
     if(ruleOptions[ctr].label==ruleVal){
        ruleText=ruleOptions[ctr].value
        break;
     }
   }
    console.log("Rule val is = "+ruleVal);
    if(ruleText=='Category'){
          console.log("its category");
           var evt = $A.get("e.force:navigateToComponent");
		     evt.setParams({
		        componentDef : "c:CPGCategory",
		        componentAttributes: {
		            scenarioRuleInstanceId : component.get("v.scenarioRuleInstanceId"),
                    workSpaceId: component.get("v.workSpaceId")
		        }
		    });
		    evt.fire();
    
    }
    if(ruleText=='Rank'){
          console.log("its rank");
           var evt = $A.get("e.force:navigateToComponent");
		     evt.setParams({
		        componentDef : "c:CPGRankingCreateDerivedField",
		        componentAttributes: {
		              mode: "Create",
		              scenarioRuleInstanceId : component.get("v.scenarioRuleInstanceId"),
                      workSpaceId: component.get("v.workSpaceId")
		        }
		    });
		    evt.fire();
    
    }
    
     if(ruleText=='Compute'){
          console.log("its compute");
           var evt = $A.get("e.force:navigateToComponent");
		     evt.setParams({
		        componentDef : "c:CPGComputationDerivedField",
		        componentAttributes: {
		              mode: "Create",
		             scenarioRuleInstanceId : component.get("v.scenarioRuleInstanceId"),
                     workSpaceId: component.get("v.workSpaceId")
		        }
		    });
		    evt.fire();
    
    }
    
    },
    closeButton : function(component, event, helper) {
        alert('You are about to leave this page');
        console.log("inside closeButton method"+component.get('v.scenarioRuleInstanceId'));
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:CPGDerivedFieldsList",
            
            componentAttributes: {
                "scenarioRuleInstanceId":component.get('v.scenarioRuleInstanceId'),
                workSpaceId: component.get("v.workSpaceId")
            }
        });
        evt.fire();
    } ,
    getTableName : function(component, event, helper) {
        var oTableName = component.find("oTableName");       
              var action = component.get("c.getInputTableName");
            action.setParams({"scenarioInstanceId":component.get("v.scenarioRuleInstanceId")}); 
             action.setCallback(this, function(response){
                 var state = response.getState();
                 console.log(state);  
                 if(component.isValid() && state==="SUCCESS")
                 {                    
                     var data = response.getReturnValue();                       
                     oTableName.set("v.value", data);
                 }else{
                     console.log("Failed with state:  "+ state);
                 }
         });
          $A.enqueueAction(action);     
    },
    onCriOperatorChange:function(component,event) {  
        
        var $j = jQuery.noConflict();
        var opType = event.getSource().get("v.value").split('@')[3];  
        var idType= event.getSource().get("v.value").split('@')[0]+'@'
        +event.getSource().get("v.value").split('@')[1]+'@'+event.getSource().get("v.value").split('@')[2];    
        var objDropdown=$j(event.currentTarget);
        //var textObject = document.getElementsByClassName("Input");
       // $(textObject.find())
       // var textObject = component.find(idType);
        if(opType =='is null' || opType== 'is not null'){
            //$j("#InputTextBox").find('Input').prop("disabled",true);
           // textObject.disabled = true; 
            //textObject.placeholder='';
        }
        else{
            textObject.disabled = false; 
            textObject.placeholder='Enter Value';
        }
    },
    onColumnChange :function(component,event) {      
        
        var strDataType = event.getSource().get("v.value");
        var opStr = strDataType.split('@')[0]+'@'+strDataType.split('@')[1]+'_'+strDataType.split('@')[2];
        var idC=strDataType.split('@')[0];
        var idG= strDataType.split('@')[1];
        var idB = strDataType.split('@')[2];
        var lstBlock = component.get('v.wrapperBlock');
        var criteria =lstBlock[idB].lstWrapGroup[idG].lstwrapCriteria[idC];
        var colDataType = strDataType.split('@')[3];
        var actionRuleName = component.get("c.getDataOptions");
            actionRuleName.setParams({ 
                "colDataType" : colDataType                
            });
            actionRuleName.setCallback(this, function(data){
                var response= data.getReturnValue();                  
                console.log(response);               
                criteria.opVal = response;                
                component.set('v.wrapperBlock[idB].lstWrapGroup[idG].lstwrapCriteria[idC].opVal',response); 
            })
            $A.enqueueAction(actionRuleName);
    },
    getOperatorTypes:function(component, event){
        
    },
    closeButton : function(component, event, helper) {
        component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");    
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:CPGDerivedFieldsList",
            
            componentAttributes: {
                "scenarioRuleInstanceId":component.get("v.scenarioRuleInstanceId"),
                workSpaceId: component.get("v.workSpaceId")
            }
        });
        evt.fire();
    },
    deleteCriteria:function(component, event, helper){
               
        var idx=event.currentTarget.getAttribute("data-index");        
        if(idx!= null){
            var idB= parseInt(idx.split('_')[0]);
            var idG= parseInt(idx.split('_')[1]);
            var idC = parseInt(idx.split('_')[2]);
            //if(idG ==0 && idC==0){
            if(idC==0){               
                var sname = component.find("ruleName");
                $A.util.addClass(sname , 'errorInput');
                
                component.set('v.isValid','false');
                component.set('v.popoverMessage','You cannot delete first criteria of the Group');
                var errorIcn = component.find("errorIcon");
                $A.util.removeClass(errorIcn,'slds-hide');
            }
            else{
				var lstwrapperBlock= JSON.parse(JSON.stringify(component.get('v.wrapperBlock')));                
                if(lstwrapperBlock[idB].lstWrapGroup[idG].lstwrapCriteria.length==1){
                    //delete group
                    var lstGrp = lstwrapperBlock[idB].lstWrapGroup;//[idG];                    
                    lstGrp.splice(idG, 1);
                    component.set("v.wrapperBlock[idB].lstWrapGroup", lstGrp); 
                }
                else{
                    //delete criteria                                   
                    //var lstCriteria = lstwrapperBlock[idB].lstWrapGroup[idG].lstwrapCriteria;
                    //lstCriteria.splice(idC, 1);                    
                    //component.set('v.wrapperBlock', lstwrapperBlock); 
                    //component.set('v.wrapperBlock['+idB+'].lstWrapGroup['+idG+']', lstGrp); 
                     helper.resequenceCriteria(component,event,helper);
                }
                helper.changeSequenceOrder(component);
            }           
        }
    },
    
    handleMouseEnter : function(component, event, helper) {
        var popover = component.find("popover");
        $A.util.removeClass(popover,'slds-hide');
    },
    //make a mouse leave handler here
handleMouseLeave : function(component, event, helper) {
        var popover = component.find("popover");
        $A.util.addClass(popover,'slds-hide');
    },
    handleMouseEnter2 : function(component, event, helper) {
        var popover = component.find("popover2");
        $A.util.removeClass(popover,'slds-hide');
    },
    //make a mouse leave handler here
handleMouseLeave2 : function(component, event, helper) {
        var popover = component.find("popover2");
        $A.util.addClass(popover,'slds-hide');
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    restrictInputsRule :function(component, event, helper){
        var ruleName=component.find("ruleName");
        var text=ruleName.get('v.value'); 
        var newtext = text.replace(/(?!^-)[^0-9a-zA-Z_\s]/g, ''); 
        //component.set(ruleName, newtext);        
        ruleName.set('v.value', newtext);    
    },
    restrictFeildName:function(component, event, helper){
        var ruleName=component.find("catgName");
        var text=ruleName.get('v.value'); 
        if(text != null){
           var newtext = text.replace(/(?!^-)[^0-9a-zA-Z_\s]/g, ''); 
            ruleName.set('v.value', newtext);    
        }
    },
    editCategory: function(component, event, helper){  
        var categoryName = event.currentTarget.getAttribute("data-index");
        component.set('v.CategoryToEdit',categoryName);
        helper.validateCategory(component, event,helper);
    },
    deleteCategory: function(component, event, helper){ 
        var categoryName = event.currentTarget.getAttribute("data-index");
        var action =  component.get("c.deleteFieldCategory");   
        action.setParams({            
            'lstWrapperBlock':JSON.stringify(component.get('v.wrapperBlock')),
            'categName':categoryName,
            "scenarioId": component.get("v.scenarioRuleInstanceId"),
        });
        
        action.setCallback(this, function(data){            
            var state = data.getState();
            console.log(state);
            if (state === "SUCCESS") {
                var response= data.getReturnValue();  //brand new block   
                if(response ==null){                    
                    var sname = component.find("categPlaceholder");
                    $A.util.addClass(sname , 'errorInput');
                    
                    component.set('v.isValid','false');
                    component.set('v.popoverMessage','Cannot delete Derived Field, as it is being used in other scenario.');
                    var errorIcn = component.find("errorIcon");
                    $A.util.removeClass(errorIcn,'slds-hide');
                }
                else{
                    var action1 =  component.get("c.deleteListFieldCategory");   
                    action1.setParams({
                        'lstCriteria':JSON.stringify(component.get('v.OutputCategory')),            
                        'categName':categoryName
                    });
                    
                    action1.setCallback(this, function(data){            
                        var state = data.getState();
                        console.log(state);
                        if (state === "SUCCESS") {
                            var response= data.getReturnValue();  //brand new block                                 
                            component.set('v.OutputCategory',response);
                        }           
                        else if (state === "ERROR") {
                            var errors = response.getError();
                            console.log(errors);      
                        }                              
                    });
                    $A.enqueueAction(action1);   
                    component.set('v.wrapperBlock',response);
                }
            }           
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log(errors);      
            }                              
        });
        $A.enqueueAction(action);
    },
})