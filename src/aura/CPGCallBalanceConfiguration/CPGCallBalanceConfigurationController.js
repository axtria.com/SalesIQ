({
	initPage : function(component, event, helper){  
    component.showSpinner();
		var $j = jQuery.noConflict();
        component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0 !important;}   .forceStyle .viewport.oneHeader.desktop {z-index:0 !important;} .forceStyle.desktop .viewport{overflow:hidden}");        
        $j(".constraint1").on("change",function(){
            
            helper.constraint1_onChange(component, event,$j(this).val(),$j(this));
        });
        $j(".constraint2").on("change",function(){
            helper.constraint2_onChange(component,event,$j(this).val(),$j(this));
        })
		component.populateBalanceRulePreferences();
        component.populateConstraintValues();
       
	},
    populateBalanceRulePreferences:function(component, event, helper){
            
             var $j = jQuery.noConflict();
             var action = component.get("c.getcallBalancingRuleName");
              
             action.setParams({ 
                    "scenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
                    action.setCallback(this, function(data){
                        
                        var options=[]
                        var response= data.getReturnValue(); 
                        var html=[];
                        for(var ctr=0;ctr<response.length;ctr++){
                          html.push("<option value='"+response[ctr].businessRuleId+"'>"+response[ctr].ruleName+"</option>");
                            
                        }
                        component.set("v.balancingRuleHtml",html.join(""));
                        /* $j(".chosen-select").html(html.join(""));
                         $j(".chosen-select").chosen({max_selected_options: 100});
                         $j( ".chosen-choices" ).sortable();*/
                      });
                $A.enqueueAction(action); 
        
    },
    populateConstraintValues:function(component, event, helper){
       
       var $j = jQuery.noConflict();       
      var action = component.get("c.getConstraintValues");
      var arrconstraint1PermittedValues=[];
      var arrconstraint2PermittedValues=[];  
      var arrconstraint1PermittedGoals=[];
      var arrconstraint2PermittedGoals=[];    
      action.setParams({ 
      "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
      action.setCallback(this, function(data){
        
          var response= data.getReturnValue(); 
          console.log(response);
          component.set("v.callBalancingParamValues",response)
          var arrConstraintsHtml=[];
          var jsonPermittedValues={};
          if(response.length>0){
              var inputLevel = $j(".level").val(response[0].aggrLevel);
              $j(".level").attr("id",response[0].aggrScenarioRuleInstance);
              for(var ctr=0;ctr<response[0].lstCatDerivedVariableMetadata.length;ctr++){
                  arrConstraintsHtml.push("<option value='"+response[0].lstCatDerivedVariableMetadata[ctr][common.getKey(component,"Field_Name__c")]+"'>"
                                          +response[0].lstCatDerivedVariableMetadata[ctr][common.getKey(component,"Field_Name__c")]+"</option>"); 
                  jsonPermittedValues[response[0].lstCatDerivedVariableMetadata[ctr][common.getKey(component,"Field_Name__c")]]=response[0].lstCatDerivedVariableMetadata[ctr][common.getKey(component,"Permitted_Values__c")].split(",");
              }
           }
          component.set("v.jsonPermittedValues",jsonPermittedValues);
          component.set("v.constraintHtml",arrConstraintsHtml.join(""));
           $j(".constraint1").html(arrConstraintsHtml.join(""));
           $j(".constraint2").html(arrConstraintsHtml.join(""));
           $j(".constraint2").find("option:eq(0)").before("<option value='-'>-</option>");
           $j(".constraint2").val('-').attr("disabled","disabled");
           $j(".constraint1").find("option:eq(0)").before("<option value='-'>-</option>");
           $j(".constraint1").val('-');
           component.getExistingConstraints(); 
         
          
        });
        $A.enqueueAction(action); 
        
    },
    getExistingConstraints:function(component,event,helper){
         
         console.log('ScenarioRuleInstanceId-->'+component.get("v.scenarioRuleInstanceId"));
         var action = component.get("c.fillConstraintWrapper");
         action.setParams({ "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
         action.setCallback(this, function(data){
              
              var response= data.getReturnValue(); 
              console.log("response");
              console.log(response);
              component.set("v.constraints",response);
              helper.populateExistingCallBalancingConstraints(component,event,helper);
              component.hideSpinner();
          });
         $A.enqueueAction(action); 
    },
    destoryCmp : function (component, event, helper) {
        
        component.destroy();
    },
    
    saveCallBalanceConfigRules:function(component,event,helper){
        
       
       var $j = jQuery.noConflict();    
       var arrConfigCallBalancingMap=[];
       var  arrCallBalancingConstraintDetails=[]; 
       var arrConfigCallBalancingConstraintRuleMap=[]; 
       var isValid=true; 
        
        
        $j(".constraint1").each(function(index){
            if($j(this).val()=="-")   {
                isValid=false;
                return false;
            }                  
        });
        if(!isValid){
            //alert("Please select constraints");
            component.set("v.popoverMessage","Please select constraints");
            console.log("popover message assigned" +component.get("v.popoverMessage"));
            
            
            var errorIcn = component.find("errorIcon");
            $A.util.removeClass(errorIcn,'slds-hide'); 
            return false;
        }
        $A.util.addClass(errorIcn,'slds-hide');  
        
        $j(".configCallBalanceContainer").each(function(index){
            
            
            var jsonConfigCallBalancingMap={};
            jsonConfigCallBalancingMap["Id"]=$j(this).attr("id");
            var idx=index+1
            jsonConfigCallBalancingMap["Name"]="Balance Configuration # "+idx;
            jsonConfigCallBalancingMap[common.getKey(component,"Run_order__c")]=$j(this).find(".runOrder").val();
            jsonConfigCallBalancingMap[common.getKey(component,"Constraint_1__c")]=$j(this).find(".constraint1").val();
            jsonConfigCallBalancingMap[common.getKey(component,"Constraint_2__c")]=$j(this).find(".constraint2").val();
            jsonConfigCallBalancingMap[common.getKey(component,"aggregate_level__c")]=$j(this).find(".level").val();
            jsonConfigCallBalancingMap[common.getKey(component,"Scenario_Rule_Instance_Details__c")]=component.get("v.scenarioRuleInstanceId");
            jsonConfigCallBalancingMap[common.getKey(component,"Aggregate_Scenario_Rule_Instance__c")]=$j(this).find(".level").attr("id");
            arrCallBalancingConstraintDetails.push({"CallBalancingConstraintDetails":[]});
            arrConfigCallBalancingConstraintRuleMap.push([]);
            
            $j(this).find(".option").each(function(index2){
                
               arrConfigCallBalancingConstraintRuleMap[index].push({ "CallBalancingConstraintRuleMaps":[]}); 
                var jsonCallBalancingConstraintDetails={};
                jsonCallBalancingConstraintDetails[common.getKey(component,"Constraint_1_Value__c")]=$j(this).find(".input1").val();
                jsonCallBalancingConstraintDetails[common.getKey(component,"Constraint_2_Value__c")]=$j(this).find(".input2").val();
                jsonCallBalancingConstraintDetails[common.getKey(component,"Constraint_1_Goal__c")]=$j(this).find(".selectGoals1").val();
                jsonCallBalancingConstraintDetails[common.getKey(component,"Constraint_2_Goal__c")]=$j(this).find(".selectGoals2").val();
                arrCallBalancingConstraintDetails[index]["CallBalancingConstraintDetails"].push(jsonCallBalancingConstraintDetails);
                var arrBalancingRules=[]; //=$j(this).find(".chosen-select").val();
                //var arrIds=$j(this).find(".chosen-select").sortable("toArray"); 
                var object=$j(this);
                $j(this).find("li.search-choice").each(function(index){
                   arrBalancingRules.push(object.find(".chosen-select option:contains('"+$j(this).find("span").html()+"')").val()) ;
                });
                if(arrBalancingRules && arrBalancingRules.length>0){
                    for(var ctrBalancingRules=0;ctrBalancingRules<arrBalancingRules.length;ctrBalancingRules++){
                        var jsonConfigCallBalancingConstraintRuleMap={};
                        jsonConfigCallBalancingConstraintRuleMap[common.getKey(component,"Business_Rules__c")]=arrBalancingRules[ctrBalancingRules];
                        jsonConfigCallBalancingConstraintRuleMap[common.getKey(component,"Exec_Seq__c")]=ctrBalancingRules+1;
                        arrConfigCallBalancingConstraintRuleMap[index][index2]["CallBalancingConstraintRuleMaps"].push(jsonConfigCallBalancingConstraintRuleMap);
                   	}
                	
                }
                 
                
                
                
            });
            
            arrConfigCallBalancingMap.push(jsonConfigCallBalancingMap);
        }) ;
       
        console.log('arrConfigCallBalancingMap');
        console.log(JSON.stringify(arrConfigCallBalancingMap));
        console.log('arrConfigCallBalancingConstraintRuleMap');
        console.log(JSON.stringify(arrConfigCallBalancingConstraintRuleMap));
        console.log('arrCallBalancingConstraintDetails');
        console.log(JSON.stringify(arrCallBalancingConstraintDetails));
         var action = component.get("c.saveCallBalanceConfigRulesServer");
         action.setParams({ "arrConfigCallBalancingMap" : JSON.stringify(arrConfigCallBalancingMap),
                           "arrCallBalancingConstraintDetails":JSON.stringify(arrCallBalancingConstraintDetails),
                           "arrConfigCallBalancingConstraintRuleMap":JSON.stringify(arrConfigCallBalancingConstraintRuleMap),
                           "scenarioInstanceDetailsId":component.get("v.scenarioRuleInstanceId")});
         action.setCallback(this, function(response){
               var state = response.getState(); 
             // var response= data.getReturnValue(); 
             if(component.isValid() && state==="SUCCESS"){ 
                component.closeButton();
             }   
          });
         $A.enqueueAction(action); 
        
        

},
    
  closeButton : function(component, event, helper) {
    component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
         var action = component.get("c.getScenarioID");
        action.setParams({ "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});

        action.setCallback(this, function(response){ 
        var state = response.getState();
        if(component.isValid() && state==="SUCCESS"){
                var arrListValues=response.getReturnValue();

                var evt = $A.get("e.force:navigateToComponent");
                   evt.setParams({
                      componentDef : "c:CPGbusinessRuleCanvas",
                      componentAttributes: {
                    record_Id : arrListValues[0][common.getKey(component,"Scenario_Id__c")],
                    viewcanvas : "Y",
                    workSpaceId : component.get("v.workSpaceId"),
                    namespace : component.get("v.namespace")
                      }
                  });
                  evt.fire();
                
        }else{
                    console.log("Failed with state:  "+ state);
            }
        })
        $A.enqueueAction(action);
    },
        // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },

     handleMouseEnter : function(component, event, helper) {
        var popover = component.find("popover");
        $A.util.removeClass(popover,'slds-hide');
    },
    //make a mouse leave handler here
    handleMouseLeave : function(component, event, helper) {
        var popover = component.find("popover");
        $A.util.addClass(popover,'slds-hide');
    },
    

    
})