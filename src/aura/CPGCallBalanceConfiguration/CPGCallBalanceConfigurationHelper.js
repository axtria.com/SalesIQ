({
	constraint1_onChange : function(component,event,value,dropdownobject) {
       
	   var $j=jQuery.noConflict();
       var constraintHtml=component.get("v.constraintHtml");
       var jsonPermittedValues=component.get("v.jsonPermittedValues"); 
       var divObject= dropdownobject.closest("div.configCallBalanceContainer");
       var constraint2Select=divObject.find(".constraint2");
         $j(".chosen-select").chosen('destroy');
        constraint2Select.html(divObject.find(".constraint1:eq(0)").html());
        if(value!='-'){
            divObject.find(".level2").addClass('slds-hide');
            divObject.find(".level").removeClass('slds-hide');
            var arrConstraint1PermittedValues=jsonPermittedValues[value]; 
            //constraint2Select.removeAttr("disabled");
            /*if(constraint2Select.find("option:eq(0)").val()=="-"){
                constraint2Select.find("option:eq(0)").remove();
            }*/
            constraint2Select.find("option:contains('"+value+"')").remove();
            
            
             var object=divObject.find(".option:eq(0)");
             component.set("v.object",object);
             divObject.find(".option").remove();
             
           for(var ctr=0;ctr<arrConstraint1PermittedValues.length;ctr++){
                var clonedPlaceHolder= object.clone(true).removeClass('slds-hide');
                divObject.append(clonedPlaceHolder);  
                divObject.find(".option .input1:eq("+ctr+")").val(arrConstraint1PermittedValues[ctr]);
            }
            var divOption=divObject.find(".option");
            divOption.find(".input2").hide();
            divOption.find(".select2,.selectGoals2").parent().hide();
           // divOption.find(".selectGoals2").parent().hide();
            var html=component.get("v.balancingRuleHtml");
            var html2=component.get("v.constraint1GoalsHtml");
            //var html3=component.get("v.constraint2GoalsHtml");
            divOption.find(".chosen-select").empty().html(html);
            divOption.find(".chosen-select").chosen({max_selected_options: 100});
            divOption.find(".chosen-choices" ).sortable();
            divOption.find(".select1").each(function(index){
               $j(this).html(html2); 
            });
            //this.constraint2_onChange(component,event,value,constraint2Select);
             var arrGoals1Html=[];
            var arrGoals2Html=[];
            for(var ctr=0;ctr<arrConstraint1PermittedValues.length;ctr++){
                arrGoals1Html.push("<option value='"+arrConstraint1PermittedValues[ctr]+"'>"+arrConstraint1PermittedValues[ctr]+"</option>");
                
            }
            /*for(var ctr=0;ctr<arrConstraint2PermittedValues.length;ctr++){
                arrGoals2Html.push("<option value='"+arrConstraint2PermittedValues[ctr]+"'>"+arrConstraint2PermittedValues[ctr]+"</option>");
                
                
            }*/
            divObject.find(".selectGoals1").html(arrGoals1Html.join(""));
           // divObject.find(".selectGoals2").html(arrGoals2Html.join(""));
            component.set('v.goals1Html',arrGoals1Html.join(""));
            //component.set('v.goals2Html',arrGoals2Html.join(""));
            
        } 
        else{
             $j(".level").addClass('slds-hide');
            $j(".level2").removeClass('slds-hide');
            var object=component.get("v.object");
            divObject.find(".option").remove();
            divObject.append(object);
            divObject.find(".option").addClass('slds-hide');
            
            divObject.find(".constraint2").find("option:eq(0)").before("<option value='-'>-</option>");
            divObject.find(".constraint2").val("-");
            divObject.find(".constraint2").attr("disabled","disabled");
        }
        
       
	},
    
    constraint2_onChange:function(component,event,value,dropdownobject){
        
        var $j=jQuery.noConflict();
        var divObject= dropdownobject.closest("div.configCallBalanceContainer");
        $j(".chosen-select").chosen('destroy');
        if(divObject.find(".constraint2").val()!='-'){
            divObject.find(".option .input2").show();
            divObject.find(".option .select2").parent().show();
            divObject.find(".option .selectGoals2").parent().show();
            var jsonPermittedValues=component.get("v.jsonPermittedValues"); 
            var arrConstraint1PermittedValues=jsonPermittedValues[divObject.find(".constraint1").val()];
            var arrConstraint2PermittedValues=jsonPermittedValues[divObject.find(".constraint2").val()];
            var object=divObject.find(".option:eq(0)");
            divObject.find(".option").remove();
            for(var ctr=0;ctr<arrConstraint1PermittedValues.length;ctr++){
                 for(var ctr2=0;ctr2<arrConstraint2PermittedValues.length;ctr2++){
                      var clonedPlaceHolder= object.clone(false).removeClass('slds-hide');
                      divObject.append(clonedPlaceHolder);
                      divObject.find(".option:last .input1").val(arrConstraint1PermittedValues[ctr]);
                      divObject.find(".option:last .input2").val(arrConstraint2PermittedValues[ctr2]);
                     // $j(".input1:eq("+ctr+")").val(arrConstraint1PermittedValues[ctr]);
                 }
                
            }
            
            var arrGoals1Html=[];
            var arrGoals2Html=[];
            for(var ctr=0;ctr<arrConstraint1PermittedValues.length;ctr++){
                arrGoals1Html.push("<option value='"+arrConstraint1PermittedValues[ctr]+"'>"+arrConstraint1PermittedValues[ctr]+"</option>");
                
            }
            for(var ctr=0;ctr<arrConstraint2PermittedValues.length;ctr++){
                arrGoals2Html.push("<option value='"+arrConstraint2PermittedValues[ctr]+"'>"+arrConstraint2PermittedValues[ctr]+"</option>");
                
                
            }
            divObject.find(".selectGoals1").html(arrGoals1Html.join(""));
            divObject.find(".selectGoals2").html(arrGoals2Html.join(""));
            component.set('v.goals1Html',arrGoals1Html.join(""));
            component.set('v.goals2Html',arrGoals2Html.join(""));
            
           // $j(".input1").each(function(index){
              // var arrIndex= 
           // });
            
             var html=component.get("v.balancingRuleHtml");
            divObject.find(".option .chosen-select").empty().html(html);
            divObject.find(".option .chosen-select").chosen({max_selected_options: 100});
            divObject.find(".option .chosen-choices" ).sortable();
            var html2=component.get("v.constraint1GoalsHtml");
            var html3=component.get("v.constraint2GoalsHtml");
           
             divObject.find(".option .select2").each(function(index){
               $j(this).html(html3); 
            });    
        }
        else{
            this.constraint1_onChange(component,event,divObject.find(".constraint1").val(),divObject.find(".constraint1"));
        }
        
    },
    
    bindBalancingRuleDropdown:function(){
          var html=component.get("v.balancingRuleHtml");
        $j(".chosen-select").html(html);
        $j(".chosen-select").chosen({max_selected_options: 100});
        $j(".chosen-choices").sortable();
    },
    populateExistingCallBalancingConstraints:function(component,event, helper){
        
        var $j=jQuery.noConflict();
        var constraints=component.get("v.constraints");
        if(constraints.lstConfigCallBalancingMap && constraints.lstConfigCallBalancingMap.length>0){
            var constraintHtml=$j("#divParent .configCallBalanceContainer:eq(0)");
            $j("#divParent .configCallBalanceContainer:eq(0)").remove();
            var divParent=$j("#divParent");
            for(var ctrCallBalancingMap=0;ctrCallBalancingMap<constraints.lstConfigCallBalancingMap.length
                ;ctrCallBalancingMap++){
                divParent.append(constraintHtml.clone(true));
                var container=divParent.find(".configCallBalanceContainer:eq("+ctrCallBalancingMap+")");
                var constraint1=container.find(".constraint1");
                var constraint2=container.find(".constraint2");
                 constraint1.on("change",function(){
                  
                    helper.constraint1_onChange(component,event,$j(this).val(),$j(this));
                });
                constraint2.on("change",function(){
                  
                    helper.constraint2_onChange(component,event,$j(this).val(),$j(this));
                });
                container.attr("id",constraints.lstConfigCallBalancingMap[ctrCallBalancingMap].Id);
                constraint1.val(constraints.lstConfigCallBalancingMap[ctrCallBalancingMap][common.getKey(component,"Constraint_1__c")]);
                //constraint2.val(constraints.lstConfigCallBalancingMap[ctrCallBalancingMap].Constraint_2__c).removeAttr('disabled');
                $j(".constraint2").find("option:contains('"+$j(".constraint1").val()+"')").remove();
                //$j(".constraint2").find("option:contains('-')").remove();
                container.find(".level").val(constraints.lstConfigCallBalancingMap[ctrCallBalancingMap][common.getKey(component,"Aggregate_Level__c")]).show();
                container.find(".level2").hide();
               this.populateCallBalancingConstraintDetails(component,event
                                                            ,constraints.lstConfigCallBalancingMap[ctrCallBalancingMap].Id
                                                            ,ctrCallBalancingMap,container);
               var object= container.find(".option:eq(0)");
               component.set("v.object",object); 
                
                
               if(!container.find(".constraint2").val() || container.find(".constraint2").val()=='-'){
                  container.find(".input2").hide();
                  container.find(".selectGoals2").parent().hide(); 
               } 
            }
            
        }
          
         //var html=component.get("v.balancingRuleHtml");
       
        /*$j(".chosen-select").empty().html(html);
        $j(".chosen-select").chosen({max_selected_options: 100});
        $j( ".chosen-choices" ).sortable();*/
        
    },
    populateCallBalancingConstraintDetails:function(component,event,callBalancingMapId,index,container){
        
       var $j=jQuery.noConflict();  
       var constraints=component.get("v.constraints"); 
        console.log("callBalancingMapId::"+callBalancingMapId); 
        console.log(constraints.lstConfigCallBalancingConstraintDetails);
       var arrConstraintDetails = $j.grep(constraints.lstConfigCallBalancingConstraintDetails, function( item ) {return item[common.getKey(component,"Config_Call_Balancing_Map_Id__c")] == callBalancingMapId;});
        console.log("arrConstraintDetails");
        console.log(arrConstraintDetails);
        var optionObject=container.find(".option:eq(0)");
        var clonedObject=optionObject.clone(true);
        optionObject.remove();
        //$j("#divParent .configCallBalanceContainer:eq("+index+") .option:eq(0)").remove();
        var arrgoals1=[];
        var arrgoals2=[];
        var goals1Html=[];
        var goals2Html=[];
        for(var ctr=0;ctr<arrConstraintDetails.length;ctr++){
           // if($j("#divParent .configCallBalanceContainer:eq("+index+") .option:eq("+ctr+")").length==0){
                optionObject.clone(false).appendTo(container);
                
            //}
            container.find(".option:last").removeClass("slds-hide");
            container.find(".input1:last").val(arrConstraintDetails[ctr][common.getKey(component,"Constraint_1_Value__c")]);
            if(arrgoals1.indexOf(arrConstraintDetails[ctr][common.getKey(component,"Constraint_1_Value__c")])==-1){
               arrgoals1.push(arrConstraintDetails[ctr][common.getKey(component,"Constraint_1_Value__c")]) ;
                goals1Html.push("<option value='"+arrConstraintDetails[ctr][common.getKey(component,"Constraint_1_Value__c")]+"'>"
                            +arrConstraintDetails[ctr][common.getKey(component,"Constraint_1_Value__c")]+"</option>"); 
            }
            container.find(".input2:last").val(arrConstraintDetails[ctr][common.getKey(component,"Constraint_2_Value__c")]);
           if(arrgoals2.indexOf(arrConstraintDetails[ctr][common.getKey(component,"Constraint_2_Value__c")])==-1){
               arrgoals2.push(arrConstraintDetails[ctr][common.getKey(component,"Constraint_2_Value__c")]) ;
               goals2Html.push("<option value='"+arrConstraintDetails[ctr][common.getKey(component,"Constraint_2_Value__c")]+"'>"
                            +arrConstraintDetails[ctr][common.getKey(component,"Constraint_2_Value__c")]+"</option>");
            }
            this.populateCallBalancingConstraintRuleMap(component,event
                                                        ,arrConstraintDetails[ctr].Id,ctr,index
                                                       ,container);
            //$j("#divParent .container:eq("+index+") .selectGoals1:last").val(arrConstraintDetails[ctr][common.getKey(component,"Constraint_1_Goal__c")]);
            //$j("#divParent .container:eq("+index+") .selectGoals2:last").val(arrConstraintDetails[ctr][common.getKey(component,"Constraint_2_Goal__c")]);
        }
         $j(".selectGoals1,.selectGoals2").html(goals1Html.join(""));
         container.find(".option").each(function(index2){
            $j(".selectGoals1:eq("+index2+")").val(arrConstraintDetails[index2][common.getKey(component,"Constraint_1_Goal__c")]);
            $j(".selectGoals2:eq("+index2+")").val(arrConstraintDetails[index2][common.getKey(component,"Constraint_2_Goal__c")]);
        });
        
    },
    populateCallBalancingConstraintRuleMap:function(component,event,constraintDetailId,ctr,index,container){
        
        var $j=jQuery.noConflict();
         var html=component.get("v.balancingRuleHtml");
       container.find(".chosen-select:eq("+ctr+")").empty().html(html);
      
          
       var constraints=component.get("v.constraints"); 
        console.log("constraintDetailId::"+constraintDetailId); 
        console.log(constraints.lstConfigCallBalancingConstraintRuleMap);
       var arrConstraintRuleMap = $j.grep(constraints.lstConfigCallBalancingConstraintRuleMap, function( item ) {return item[common.getKey(component,"Config_Call_Balancing_Constraint_Detail__c")] == constraintDetailId;});
       arrConstraintRuleMap=arrConstraintRuleMap.sort(function(a,b){return parseInt(a[common.getKey(component,"Exec_Seq__c")])-parseInt(b[common.getKey(component,"Exec_Seq__c")])});
        console.log("arrConstraintRuleMap");
        console.log(arrConstraintRuleMap);
        
        var arrBusinessRules=[];
        for(var ctrarrConstraintRuleMap=0;ctrarrConstraintRuleMap<arrConstraintRuleMap.length;ctrarrConstraintRuleMap++){
            arrBusinessRules.push(arrConstraintRuleMap[ctrarrConstraintRuleMap][common.getKey(component,"Business_Rules__c")]);
            //liArray.find(":eq("+ctrarrConstraintRuleMap+") span").html(arrConstraintRuleMap[ctrarrConstraintRuleMap].Business_Rules__r.Name);
        }
        
        var chosenSelect=container.find(".chosen-select:eq("+ctr+")");
        chosenSelect.val(arrBusinessRules);
        
        
         chosenSelect.chosen({max_selected_options: 100});
         container.find(".chosen-choices:eq("+ctr+")" ).sortable();
        var liArray=chosenSelect.parent().find("li.search-choice"); 
        liArray.each(function(index){
            $j(this).find("span").html(arrConstraintRuleMap[index][common.getKey(component,"Business_Rules__r")]["Name"]);
        });
       
    }
   
})