({   
    onTextChange : function(component, event, helper){   
        var $j = jQuery.noConflict();
        if(component.get("v.ComponentName") =='CallBalancing'){
            var blockId=  event.currentTarget.getAttribute("data-index");
            var categName = event.currentTarget.getAttribute("data-name"); 
            var depend = event.currentTarget.getAttribute("data-depen"); 
            var txtVal='';
            if($j('#'+blockId+'inpText'+categName).val() == 0)
                txtVal=0;
            else if($j('#'+blockId+'inpText'+categName).val() > 0)
                txtVal=1;            
            $j('#'+blockId+'inpText'+depend).val(txtVal);
            
            var lstBlock = component.get('v.wrapperBlock');
            var block = lstBlock[blockId];   
            
            var categ = block.lstOutputCriteria;
            for(var k=0; k <categ.length; k++){
                categ[k].selValue = $j('#'+blockId+'inpText'+categ[k].categName).val() ;
            }            
        }
    },
	initPage : function(component, event, helper){   
       component.showSpinner(); 
        component.set("v.isBtnClick","");
        component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0 !important;}   .forceStyle .viewport.oneHeader.desktop {z-index:0 !important;} .forceStyle.desktop .viewport{overflow:hidden}");
        //Fill Rules and data types        
        if(component.get("v.ComponentName") !='CallBalancing'){
            component.getRuleTypes(); 
            component.getTypeList();
        }
        else{
            if(component.get("v.businessRuleId") == '')
                component.getRuleTypeDetails();            
        }        
        if(component.get("v.businessRuleId") != ''){
            component.getExistingRule();
        }        
        component.getTableName();  
            
	},
    onRuleTypeChange:function(component, event, helper){
        component.initializePage(); 
    },
    initializePage:function(component, event, helper) {
        var ruleType='';
        if(component.get("v.ComponentName") =='CallBalancing'){
            ruleType = component.find('ruleType').get('v.value');
        }
        var action1 = component.get('c.initialiseBlock');
        action1.setParams({ 
            "scenarioId" : component.get("v.scenarioRuleInstanceId"),            
            "ruleId" : component.get("v.businessRuleId"),
            "ruleType": ruleType
        });                
        action1.setCallback(this, function(data){  

            var response= data.getReturnValue();  
            if(response != null){
                var responseData = JSON.parse(JSON.stringify(response));
                var prio=[];
                for(var i=0; i< responseData.length; i++){
                    prio.push(i+1);
                }
                for(var i=0; i< responseData.length; i++){
                    responseData[i].priority= prio;
                    responseData[i].selPriority=i+1;
                }                        
                component.set('v.wrapperBlock',responseData);    
            }
            if(component.get("v.ComponentName") =='CallBalancing'){
                helper.AddOutputField(component);  
            }
            component.hideSpinner();
        })        
        $A.enqueueAction(action1);

    },
    getTableName : function(component, event, helper) {        
        var oTableName = component.find("oTableName");            
        var iTableName = component.find("iTableName");     
        var action = component.get("c.getInputTableName");
        action.setParams({"scenarioInstanceId":component.get("v.scenarioRuleInstanceId")}); 
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(component.isValid() && state==="SUCCESS")
            {                    
                var arrTableName = response.getReturnValue();  
                for(var i=0 ; i < arrTableName.length; i++){
                    if(arrTableName[i][common.getKey(component,"ds_type__c")]=='O'){
                        oTableName.set("v.value", arrTableName[i][common.getKey(component,"Table_Display_Name__c")]);                        
                        component.set('v.DataSetId',arrTableName[i][common.getKey(component,"dataset_id__c")]);
                    }
                    if(component.get("v.ComponentName") =='CallBalancing'){
                        if(arrTableName[i][common.getKey(component,"ds_type__c")]=='I')
                            iTableName.set("v.value", arrTableName[i][common.getKey(component,"Table_Display_Name__c")]);
                    }
                }
               // component.hideSpinner();
               component.initializePage(); 
            }
            else{
                console.log("Failed with state:  "+ state);
            }
        });
        $A.enqueueAction(action);     
    },
    getRuleTypeDetails : function(component, event,helper){  
        var action = component.get("c.GetRuleDetails");    
        action.setParams({ "scenarioInstanceId" : component.get("v.scenarioRuleInstanceId")});
        action.setCallback(this, function(response){ 
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS")
            {
                var arrRuleTypes=response.getReturnValue();                  
                component.set("v.RuleDetails",arrRuleTypes);  				              
            }
            else{
                console.log("Failed with state:  "+ state);
            }
        })
        $A.enqueueAction(action);
    },
    changeSequenceOrder:function(component, event, helper) {
        helper.changeSequenceOrder(component, event);
    },    
    deleteABlock: function(component, event,helper){
        helper.deleteBlock(component, event);        
    },
    getExistingRule:function(component, event, helper) {
        var actionRuleName = component.get("c.getRuleName");
            actionRuleName.setParams({ 
                "scenarioId" : component.get("v.scenarioRuleInstanceId"),
                "ruleId" : component.get("v.businessRuleId")
            });
            actionRuleName.setCallback(this, function(data){
                var response= data.getReturnValue();                 
                component.set("v.RuleDetails",response);
                if(component.get("v.ComponentName") =='CallBalancing'){
                    component.find('ruleNameBalancing').set('v.value',response.ruleName);
                    component.set("v.disabled", true); 
                }
                else
                    component.find('ruleName').set('v.value',response.ruleName);
            })
            $A.enqueueAction(actionRuleName);
            var actionGetCateg = component.get("c.DisplayCategories");
            actionGetCateg.setParams({                 
                "ruleId" : component.get("v.businessRuleId")
            });
            actionGetCateg.setCallback(this, function(data){
                var response= data.getReturnValue();                  
                component.set('v.OutputCategory',response);
            })
            $A.enqueueAction(actionGetCateg);
    },
    getTypeList:function(component, event, helper) {
        var action = component.get("c.getTypesList");
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(component.isValid() && state==="SUCCESS")
            {             
                var dataTypeList=[];
                var valueTypeList=[];
                var itemsSample = response.getReturnValue();                                     
                var ii=0;
                var jj=0;
                for(var ctr=0;ctr<itemsSample.length;ctr++){                   
                    if(itemsSample[ctr][common.getKey(component,"Type__c")]=="Data Type")
                    {                        
                        dataTypeList.push({label:itemsSample[ctr][common.getKey(component,"Type__c")], value: itemsSample[ctr][common.getKey(component,"Value__c")]});
                        component.set("v.datatypeoptions",dataTypeList);
                        if(ii == 0)
                            component.find("dataTypes").set('v.value',itemsSample[ctr][common.getKey(component,"Value__c")]);
                        ii++;
                    }                    
                    if(itemsSample[ctr][common.getKey(component,"Type__c")]=="Value Type")
                    {
                        valueTypeList.push({label:itemsSample[ctr][common.getKey(component,"Type__c")], value: itemsSample[ctr][common.getKey(component,"Value__c")]});
                        component.set("v.valueoptions",valueTypeList);
                        if(jj == 0)
                            component.find("valueTypes").set('v.value',itemsSample[ctr][common.getKey(component,"Value__c")]);
                        jj++;
                    }
                }
            }
            else{
                console.log("Failed with state:  "+ state);
            }
            component.hideSpinner();
        })
        $A.enqueueAction(action);        
    },
    
    doneRendering: function(component, event, helper) {    
        var response= component.get('v.wrapperBlock'); 
        for(var k=0; k <response.length; k++){
            var lstWrapGrp = response[k].lstWrapGroup;
            if(lstWrapGrp != null){
                for(var i=0; i < lstWrapGrp.length; i++){
                    var criList = lstWrapGrp[i].lstwrapCriteria;
                    var btnOR = document.getElementById('btnOR_'+i+'_'+k);
                    var btnAnd = document.getElementById('btnAnd_'+i+'_'+k);
                    var modal = document.getElementById('grpOp_'+i+'_'+k);                
                    if(i!=lstWrapGrp.length-1){                    
                        $A.util.removeClass(modal, 'hide');
                        $A.util.addClass(modal, 'display'); 
                        
                        if(component.get("v.isBtnClick") == ''){
                            if(lstWrapGrp[i+1].selGroupOp == 'OR'){
                                $A.util.addClass(btnOR,'slds-is-selected');
                                $A.util.removeClass(btnAnd,'slds-is-selected');
                            }
                            else{
                                $A.util.addClass(btnAnd,'slds-is-selected');
                                $A.util.removeClass(btnOR,'slds-is-selected');
                            }
                        }
                    }
                    else if(lstWrapGrp.length==1){                    
                        var modal = document.getElementById('grpOp_'+i+'_'+k);
                        $A.util.removeClass(modal, 'display');
                        $A.util.addClass(modal, 'hide');         
                    }
                    for(var a=0;a< criList.length; a++){ 
                        var idType= 'div'+a+i+k;    
                        var textObject = document.getElementById(idType);
                        
                        var cri = criList[a];                   
                        if(cri.selOpVal.split('@')[3] =='is null' || cri.selOpVal.split('@')[3] == 'is not null'){
                            // textObject.disabled = true; 
                            //textObject.placeholder='';
                           textObject.style.visibility = "hidden";
                        }
                        else{
                            textObject.style.visibility = "visible";
                            //textObject.disabled = false; 
                            // textObject.placeholder='Enter Value';
                        }
                    }
                    }
                }
           }
        // seq++;
        //}
       
    },
    deleteGroup:function(component,event,helper){
        helper.deleteGroup(component,event,helper);
    },
    getRuleTypeValues:function(component, event, helper) {
		helper.getRuleTypeValues(component);       
    },
    ShowPopup:  function(component, event, helper){
        var sname = component.find("categPlaceholder");
        $A.util.removeClass(sname , 'errorInput');
        component.set('v.isNewCateg', true);
        helper.showPopupHelper(component, 'modaldialog', 'demo-only ');
    },   
    handleInputText:  function(component, event, helper){        
        var sname = component.find("ruleName");
        $A.util.removeClass(sname , 'errorInput');
    },
    handleCategName:  function(component, event, helper){        
        var sname = component.find("catgName");
        $A.util.removeClass(sname , 'errorInput');
    },
    onValuesPress:  function(component, event, helper){        
        var sname = component.find("values");
        $A.util.removeClass(sname , 'errorInput');
    },
    onValueTypeChange :function(component, event, helper){
        var selected = component.find("valueTypes").get("v.value");        
    },
    ShowQuery: function(component, event, helper){
        helper.showQueryPopupHelper(component, 'modalViewQuery', 'slds-');  
        var modal = component.find('modalQueryForm');
        $A.util.addClass(modal, 'none_opacity');
        helper.DisplayQuery(component);
    },
     HideQueryPopup:  function(component, event, helper){
         helper.hideQueryPopupHelper(component, 'modalViewQuery', 'slds-');
         var modal = component.find('modalQueryForm');
         $A.util.removeClass(modal, 'none_opacity');         
    },
    onOperatorChange : function(component, event, helper){     
        helper.changeSequenceOrder(component);
    },
    SaveUpdateCategory : function(component, event, helper){   
        helper.doPageValidation(component, event, helper);        
        if(component.get('v.isValid')=='true'){            
            var categoryData= component.get('v.OutputCategory');        
            var wrapperData= component.get('v.wrapperBlock');            
            var ruleName;
            var ruleType='';
            var selLevel='';
            var custPrior='';
            if(component.get("v.ComponentName") !='CallBalancing'){
                ruleName = component.find('ruleName').get('v.value');
            }
            else{
                ruleType = component.find('ruleType').get('v.value');
                selLevel = component.find('selectLevel').get('v.value');                
                custPrior = component.find('selectCustPrior').get('v.value');
                ruleName = component.find('ruleNameBalancing').get('v.value');
            }
            var scenarioId = component.get('v.scenarioRuleInstanceId');
            var ruleId  = component.get("v.businessRuleId");
            var action = component.get('c.SaveCheck');   
            //component.showSpinner();
            action.setParams({ 
                'categoryData':JSON.stringify(categoryData), 
                'wrapperData':JSON.stringify(wrapperData),
                'ruleName':ruleName,
                'scenarioId':scenarioId,
                'ruleId' : ruleId,
                'DataSetId': component.get("v.DataSetId"),
                'ruleType':ruleType,
                'selLevel':selLevel,                
                'custPrior':custPrior,
                'deletedCategoryFields':JSON.stringify(component.get("v.OldOutputCategory"))
            });
            action.setCallback(this, function(data){
                var state= data.getState();
                if(component.isValid() && state==="SUCCESS"){
                    var response= data.getReturnValue();
                    if(response== 'true'){
                        component.closeButton(); 
                    }
                    else{
                        var sname = component.find("ruleName");
                        $A.util.addClass(sname , 'errorInput');
                        
                        component.set('v.isValid','false');
                        component.set('v.popoverMessage',response);
                        var errorIcn = component.find("errorIcon");
                        $A.util.removeClass(errorIcn,'slds-hide');
                    }
                }
                else{
                    var sname = component.find("ruleName");
                    $A.util.addClass(sname , 'errorInput');                    
                    component.set('v.isValid','false');
                    component.set('v.popoverMessage','Some error occured!');
                    var errorIcn = component.find("errorIcon");
                    $A.util.removeClass(errorIcn,'slds-hide');
                }
            })
            $A.enqueueAction(action);
        }  
    },    
    SaveDynamicFields : function(component, event,helper) {  
        var errMessage='';
        var Name=component.find('catgName').get('v.value');        
        if(Name == undefined || !Name.trim()){
            errMessage='Name';
            var sname = component.find("catgName");
            $A.util.addClass(sname , 'errorInput');
        }
        if(component.find("valueTypes").get("v.value")=='Discrete' && !component.find("values").get("v.value"))
        {                            
            if(errMessage!='')
                errMessage+=',';
            errMessage+='Values';    
            var sname = component.find("values");
            $A.util.addClass(sname , 'errorInput');
        }
        else if(component.find("valueTypes").get("v.value")=='Discrete' && component.find("values").get("v.value"))
        {
            var val1=component.find("values").get("v.value");                
            var isValuesCorrect= false;
                    if(component.find("dataTypes").get("v.value")=='Numeric'){
            var m= false;
            var numberPattern = /^[0-9]+(,[0-9]+)*$/;
            m=numberPattern.test(val1);                        
            
            if(!m){              
                if(errMessage!='')
                    errMessage+=',';
                errMessage+="numeric values";                                        
                var sname = component.find("values");
                $A.util.addClass(sname , 'errorInput');
            }
        }
        else{
            if(component.find("dataTypes").get("v.value")=='Text'){
            var m= false;
               // var specialCharPattern = /[0-9a-zA-Z]+(,[0-9a-zA-Z]+)*/g;
                //m=specialCharPattern.test(val1);                    
               // var textPattern = /([^,]+)/g; 
               var textPattern = /^[0-9a-zA-Z]+(,[0-9a-zA-Z]+)*$/;
                m=textPattern.test(val1);                       
                
            if(!m){              
                if(errMessage!='')
                    errMessage+=',';
                errMessage+="values without special characters";                                        
                var sname = component.find("values");
                $A.util.addClass(sname , 'errorInput');
            }
            }
        }
        }
        if(errMessage!=''){
            component.set("v.popoverMessage2","Please enter "+errMessage);                
            var errorIcn = component.find("errorIcon2");
            $A.util.removeClass(errorIcn,'slds-hide');            
        }
        else{
            if(((component.find("valueTypes").get("v.value")=='Discrete' && component.find("dataTypes").get("v.value")=='Numeric') )
           ||(component.find("valueTypes").get("v.value")=='Continuous')
           || (component.find("valueTypes").get("v.value")=='Discrete' && component.find("dataTypes").get("v.value")=='Text') )
            { 
                helper.AddCategoryToThanBlock(component);
                helper.AddCategoryToElseBlock(component);
                
            }
        }            
    },
    
    HidePopUp:  function(component, event, helper){
        component.find('catgName').set('v.value', '');                                
        component.find('values').set('v.value', '');          
        helper.hidePopupHelper(component, 'modaldialog', 'demo-only ');
    },
    onSelectChange : function(component, event, helper) {
        var selected = component.find("valueTypes").get("v.value");
        var valueSec= component.find("ValueSection"); 
        var sname = component.find("values");
        $A.util.removeClass(sname , 'errorInput');
		component.find('values').set('v.value', '');          
        if(selected=='Discrete'){                                    
            $A.util.removeClass(valueSec , 'hide');
            $A.util.addClass(valueSec , 'display');
        }
        else
        {            
            $A.util.removeClass(valueSec , 'display');
            $A.util.addClass(valueSec , 'hide');
        }       
    },
    
    onChangeOutputCategory: function(component, event, helper){
        helper.hidePopupHelper(component, 'modaldialog', 'demo-only ');                 
    },
    UpdateWrapper: function (component, event, helper){
        var response= event.getParam("updateStr"); 
        component.set('v.wrapperBlock',response);
    },
  addABlock: function(component, event, helper){
        helper.addBlock(component,event,helper);
    },
    onOutputFeildsChange : function (component, event, helper) {
       component.set('v.outputData',event.getParam("value"))      
    },
    onChange: function(component, event, helper){
        var wrapperBlock = component.get('v.wrapperBlock');
        var event = component.getEvent('UpdateMainBlockEvent');                
        event.setParams({ 
            "updateStr" : JSON.stringify(wrapperBlock)
        });
        event.fire();   
    },
         
    addAGroup : function(component, event, helper){
        helper.addGroup(component,event,helper);        
    },                          
    RefreshControl: function(component, event, helper){
        var message = event.getParam("outputCategory");
    },    
    onSelectchange: function(component, event, helper){ 
        var changedVal= event.getSource().get('v.value');          
    },
    HideShow: function(component, event, helper) { 
        helper.ToggleCollapseHandler(component, event);
    },
      addACriteria : function(component, event, helper){          
        helper.addCriteria(component,event,helper);
    },
    btnANDClick : function(component, event, helper){
        var idx = event.currentTarget.getAttribute("data-index");
        var idG= parseInt(idx.split('_')[0]);
        var idB= parseInt(idx.split('_')[1]);
        var btnOR = document.getElementById('btnOR_'+idx);
        var btnAnd = document.getElementById('btnAnd_'+idx);
        $A.util.removeClass(btnOR,'slds-is-selected');
        $A.util.addClass(btnAnd,'slds-is-selected');
        
        var wrapperBlock= component.get('v.wrapperBlock');
        var block = wrapperBlock[idB]; // each block                
        var criteriaLogic='';
        var lstGroup= block.lstWrapGroup; // lstGrp under each block                
        for(var k=0; k < lstGroup.length; k++){
            var grp = lstGroup[k];
            var lstCriteria = grp.lstwrapCriteria; //list of criteria
            if(idG==k)
                lstGroup[k+1].selGroupOp ='AND';
            criteriaLogic+= '(';
            for(var j=0; j < lstCriteria.length; j++){
                var cri = lstCriteria[j];
                if(cri.criName != '-')
                    criteriaLogic+=' '+cri.criName+' '+cri.seqNo;
                else
                    criteriaLogic+=cri.seqNo;
            }
            criteriaLogic+=') ';
            
            if(k!=lstGroup.length-1)
                criteriaLogic+= lstGroup[k+1].selGroupOp+' ';
        }
        //code to update sequence ends 
        block.criteriaLogic=criteriaLogic;               
        component.set('v.wrapperBlock',wrapperBlock);                
        component.set("v.isBtnClick","yes");
        
    },
    btnORClick : function(component, event, helper){        
        component.set("v.isBtnClick","yes");
        var idx = event.currentTarget.getAttribute("data-index");
        var idG= parseInt(idx.split('_')[0]);
        var idB= parseInt(idx.split('_')[1]);
        var btnOR = document.getElementById('btnOR_'+idx);
        var btnAnd = document.getElementById('btnAnd_'+idx);
        
        $A.util.removeClass(btnAnd,'slds-is-selected');
        $A.util.addClass(btnOR,'slds-is-selected');
        
        var wrapperBlock= component.get('v.wrapperBlock');
        var block = wrapperBlock[idB]; // each block                
        var criteriaLogic='';
        var lstGroup= block.lstWrapGroup; // lstGrp under each block                
        for(var k=0; k < lstGroup.length; k++){
            var grp = lstGroup[k];
            var lstCriteria = grp.lstwrapCriteria; //list of criteria
            if(idG==k)
                lstGroup[k+1].selGroupOp ='OR';
            criteriaLogic+= '(';
            for(var j=0; j < lstCriteria.length; j++){
                var cri = lstCriteria[j];
                if(cri.criName != '-')
                    criteriaLogic+=' '+cri.criName+' '+cri.seqNo;
                else
                    criteriaLogic+=cri.seqNo;
            }
            criteriaLogic+=') ';
            
            if(k!=lstGroup.length-1)
                criteriaLogic+= lstGroup[k+1].selGroupOp+' ';
        }
        //code to update sequence ends 
        block.criteriaLogic=criteriaLogic;               
        component.set('v.wrapperBlock',wrapperBlock);                
    },    
    onRuleChange : function(component, event, helper) {
        var ruleVal = event.currentTarget.value;//component.get("v.selectedRule");
        var ruleText;  
        var ruleOptions= component.get("v.ruleOptions");
        
        for(var ctr=0;ctr<ruleOptions.length;ctr++){
            if(ruleOptions[ctr].label==ruleVal){
                ruleText=ruleOptions[ctr].value
                break;
            }
        }
        
        if(ruleText=='Category'){
            
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:CPGCallPlanning",
                componentAttributes: {
                    scenarioRuleInstanceId : component.get("v.scenarioRuleInstanceId"),
                    workSpaceId: component.get("v.workSpaceId"),
                    namespace:component.get("v.namespace"),
                    summarizedViewFlag : component.get("v.summarizedViewFlag")
                }
            });
            evt.fire();
            
        }
        if(ruleText=='Rank'){            
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:CPGRankingCreateDerivedField",
                componentAttributes: {
                    mode: "Create",
                    scenarioRuleInstanceId : component.get("v.scenarioRuleInstanceId"),
                    workSpaceId: component.get("v.workSpaceId"),
                    namespace:component.get("v.namespace"),
                    summarizedViewFlag : component.get("v.summarizedViewFlag")
                }
            });
            evt.fire();            
        }
        
        if(ruleText=='Compute'){            
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:CPGComputationDerivedField",
                componentAttributes: {
                    mode: "Create",
                    scenarioRuleInstanceId : component.get("v.scenarioRuleInstanceId"),
                    workSpaceId: component.get("v.workSpaceId"),
                    namespace:component.get("v.namespace"),
                    summarizedViewFlag : component.get("v.summarizedViewFlag")
                }
            });
            evt.fire();
        }
        
    },
    closeButton : function(component, event, helper) {
        component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");    
        var evt = $A.get("e.force:navigateToComponent");
        if(component.get("v.ComponentName") !='CallBalancing'){
            evt.setParams({
                componentDef : "c:CPGDerivedFieldsList",            
                componentAttributes: {
                    "scenarioRuleInstanceId":component.get('v.scenarioRuleInstanceId'),
                    workSpaceId: component.get("v.workSpaceId"),
                    namespace:component.get("v.namespace"),
                    summarizedViewFlag : component.get("v.summarizedViewFlag")
                }
            });
        }
        else{
            evt.setParams({
                componentDef : "c:CPGCallPlanBalancingRules",            
                componentAttributes: {
                    "scenarioRuleInstanceId":component.get('v.scenarioRuleInstanceId'),
                    workSpaceId: component.get("v.workSpaceId"),
                    namespace:component.get("v.namespace"),
                    summarizedViewFlag : component.get("v.summarizedViewFlag")
                }
            });
        }
        evt.fire();
    } ,
    
    onCriOperatorChange:function(component,event) {       
        var $j = jQuery.noConflict();
        var opType = event.getSource().get("v.value").split('@')[3];  
        var idType="div"+ event.getSource().get("v.value").split('@')[0]+
            +event.getSource().get("v.value").split('@')[1]+event.getSource().get("v.value").split('@')[2]; 
        var idDisType="divdis"+ event.getSource().get("v.value").split('@')[0]+
            +event.getSource().get("v.value").split('@')[1]+event.getSource().get("v.value").split('@')[2]; 
        var textObject = document.getElementById(idType);
        var textObjectDis = document.getElementById(idDisType);
        if(opType =='is null' || opType== 'is not null'){
            
            $A.util.removeClass(textObjectDis, 'hide');
            $A.util.addClass(textObjectDis, 'display');
            
            $A.util.addClass(textObject, 'hide');
            $A.util.removeClass(textObject, 'display');
        }
        else{
            $A.util.addClass(textObjectDis, 'hide');
            $A.util.removeClass(textObjectDis, 'display');
            
            $A.util.removeClass(textObject, 'hide');
            $A.util.addClass(textObject, 'display');
        }
    },
    onColumnChange :function(component,event) {              
        var strDataType = event.getSource().get("v.value");
        var opStr = strDataType.split('@')[0]+'@'+strDataType.split('@')[1]+'_'+strDataType.split('@')[2];
        var idC=strDataType.split('@')[0];
        var idG= strDataType.split('@')[1];
        var idB = strDataType.split('@')[2];
        var lstBlock = component.get('v.wrapperBlock');
        var criteria =lstBlock[idB].lstWrapGroup[idG].lstwrapCriteria[idC];
        var colDataType = strDataType.split('@')[3];
        var actionRuleName = component.get("c.getDataOptions");
            actionRuleName.setParams({ 
                "colDataType" : colDataType                
            });
            actionRuleName.setCallback(this, function(data){
                var response= data.getReturnValue();                  
                          
                criteria.opVal = response;                
                component.set('v.wrapperBlock[idB].lstWrapGroup[idG].lstwrapCriteria[idC].opVal',response); 
            })
            $A.enqueueAction(actionRuleName);
    },
    getOperatorTypes:function(component, event){
        
    },
   /* closeButton : function(component, event, helper) {
        component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");    
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:CPGDerivedFieldsList",
            
            componentAttributes: {
                "scenarioRuleInstanceId":component.get("v.scenarioRuleInstanceId"),
                workSpaceId: component.get("v.workSpaceId")
            }
        });
        evt.fire();
    },*/
    deleteCriteria:function(component, event, helper){             
        var idx=event.currentTarget.getAttribute("id");        
        if(idx!= null){
            var idB= parseInt(idx.split('_')[0]);
            var idG= parseInt(idx.split('_')[1]);
            var idC = parseInt(idx.split('_')[2]);
            //if(idG ==0 && idC==0){
            if(idC==0){               
                var sname = component.find("ruleName");
                $A.util.addClass(sname , 'errorInput');
                
                component.set('v.isValid','false');
                component.set('v.popoverMessage','You cannot delete first criteria of the Group');
                var errorIcn = component.find("errorIcon");
                $A.util.removeClass(errorIcn,'slds-hide');
            }
            else{
				var lstwrapperBlock= JSON.parse(JSON.stringify(component.get('v.wrapperBlock')));                
                if(lstwrapperBlock[idB].lstWrapGroup[idG].lstwrapCriteria.length==1){
                    //delete group
                    var lstGrp = lstwrapperBlock[idB].lstWrapGroup;//[idG];                    
                    lstGrp.splice(idG, 1);
                    component.set("v.wrapperBlock[idB].lstWrapGroup", lstGrp); 
                }
                else{
                    //delete criteria                                   
                    //var lstCriteria = lstwrapperBlock[idB].lstWrapGroup[idG].lstwrapCriteria;
                    //lstCriteria.splice(idC, 1);                    
                    //component.set('v.wrapperBlock', lstwrapperBlock); 
                    //component.set('v.wrapperBlock['+idB+'].lstWrapGroup['+idG+']', lstGrp); 
                     helper.resequenceCriteria(component,event,helper);
                }
                helper.changeSequenceOrder(component);
            }           
        }
    },
    
    handleMouseEnter : function(component, event, helper) {
        var popover = component.find("popover");
        $A.util.removeClass(popover,'slds-hide');
    },
    //make a mouse leave handler here
handleMouseLeave : function(component, event, helper) {
        var popover = component.find("popover");
        $A.util.addClass(popover,'slds-hide');
    },
    handleMouseEnter2 : function(component, event, helper) {
        var popover = component.find("popover2");
        $A.util.removeClass(popover,'slds-hide');
    },
    //make a mouse leave handler here
handleMouseLeave2 : function(component, event, helper) {
        var popover = component.find("popover2");
        $A.util.addClass(popover,'slds-hide');
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    restrictInputsRule :function(component, event, helper){
        var ruleName=component.find("ruleName");
        var text=ruleName.get('v.value'); 
        var newtext = text.replace(/(?!^-)[^0-9a-zA-Z_\s]/g, ''); 
        //component.set(ruleName, newtext);        
        ruleName.set('v.value', newtext);    
    },
     restrictBalancingInputsRule :function(component, event, helper){
        var ruleName=component.find("ruleNameBalancing");
        var text=ruleName.get('v.value'); 
        var newtext = text.replace(/(?!^-)[^0-9a-zA-Z_\s]/g, '');             
        ruleName.set('v.value', newtext);    
    },
    restrictFeildName:function(component, event, helper){
        var ruleName=component.find("catgName");
        var text=ruleName.get('v.value'); 
        if(text != null){
           var newtext = text.replace(/(?!^-)[^0-9a-zA-Z_\s]/g, ''); 
            ruleName.set('v.value', newtext);    
        }
    },
    editCategory: function(component, event, helper){  
        var categoryName = event.currentTarget.getAttribute("data-index");
        component.set('v.CategoryToEdit',categoryName);
        helper.validateCategory(component, event,helper);
    },
    deleteCategory: function(component, event, helper){ 
        var categoryName = event.currentTarget.getAttribute("dataName");
        var idx = event.currentTarget.getAttribute("data-index");
        console.log('idx ', idx);
        var listCategory = JSON.parse(JSON.stringify(component.get("v.OutputCategory")));
        console.log('--listCategory ', listCategory);
        if(listCategory.length > 0){
            
            var arrOldOutputCategory=component.get("v.OldOutputCategory");
            arrOldOutputCategory.push(listCategory[idx]);
            console.log("arrOldOutputCategory");
            console.log(arrOldOutputCategory);
            component.set("v.OldOutputCategory",arrOldOutputCategory);

            listCategory.splice(idx,1);

            var listWrapBlock = JSON.parse(JSON.stringify(component.get("v.wrapperBlock")));
            for(var index=0; index < listWrapBlock.length; index++)
                listWrapBlock[index].lstOutputCriteria = listCategory;
            
            listWrapBlock[0].lstElseCriteria = listCategory;
            component.set("v.wrapperBlock", listWrapBlock);
            component.set("v.OutputCategory", listCategory);
        }
        else
            return;

        /*var action =  component.get("c.deleteFieldCategory");   
        action.setParams({            
            'lstWrapperBlock':JSON.stringify(component.get('v.wrapperBlock')),
            'categName':categoryName,
            "scenarioId": component.get("v.scenarioRuleInstanceId"),
        });
        
        action.setCallback(this, function(data){            
            var state = data.getState();            
            if (state === "SUCCESS") {
                var response= data.getReturnValue();  //brand new block   
                if(response ==null){                    
                    var sname = document.getElementById('main'+categoryName);
                    $A.util.addClass(sname , 'errorInput');
                    
                    component.set('v.isValid','false');
                    component.set('v.popoverMessage','Cannot delete \''+categoryName+'\', as it is being used in other scenario.');
                    var errorIcn = component.find("errorIcon");
                    $A.util.removeClass(errorIcn,'slds-hide');
                }
            }           
            else if (state === "ERROR") {
                var errors = response.getError();
            }                              
        });
        $A.enqueueAction(action);*/
    },
    addNestedGroup: function(component, event, helper){
        helper.addNestedGrp(component,event,helper);
    },
    destoryCmp : function (component, event, helper) {
        component.destroy();
    },
})