({
    initPage : function(component, event, helper){ 
        console.log('--called again');
        console.log('==initPage called==');

        console.log(component.get("v.scenarioRuleInstanceId"));
        component.set("v.isBtnClick","");
        component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0 !important;}   .forceStyle .viewport.oneHeader.desktop {z-index:0 !important;} .forceStyle.desktop .viewport{overflow:hidden}");
            
        var actionBusinessRuleId = component.get("c.getBusinessRuleId");
        actionBusinessRuleId.setParams({
            "scenarioId" : component.get("v.scenarioRuleInstanceId")
        });
        
        actionBusinessRuleId.setCallback(this, function(data){
          
            var response= data.getReturnValue();  
            console.log(response);
            component.set('v.businessRuleId',response);
                    
            /*if(component.get("v.businessRuleId") != ''){
                component.getExistingRule();
            }*/

            var action1 = component.get('c.initialiseBlock');
            action1.setParams({ 
                "scenarioId" : component.get("v.scenarioRuleInstanceId")            
            });       
            
            action1.setCallback(this, function(data){
                var response= data.getReturnValue();  
                var responseData = JSON.parse(JSON.stringify(response));
                console.log('responseData');
                console.log(response);
                        
                component.set('v.wrapperBlock',response);  
                var action2 = component.get('c.getColumns');
                action2.setParams({ 
                    "scenarioId" : component.get("v.scenarioRuleInstanceId"),
                    
                });
                action2.setCallback(this, function(data){
                    var response= data.getReturnValue(); 
                    component.set('v.ColumnWrapper',response);
                    component.getTableName();
                })
                
                $A.enqueueAction(action2);                   
            })
            $A.enqueueAction(action1);

        })
        $A.enqueueAction(actionBusinessRuleId);
    }, 

    blankWrapBlock : function(component, event, helper){
         component.set('v.wrapperBlock',"[]");   
    }, 

    setBlock : function(component, event, helper){
        console.log('--setblock called');
        var params = event.getParam('arguments');
        if (params) {
            var param1 = params.param1;
            console.log("Received WRAPPER");
            var newFile = JSON.parse(JSON.stringify(param1));
            console.log('+++++++++ OuterList');
            var outerList = JSON.parse(JSON.stringify(param1))["0"].lstWrapGroup;
            
            console.log(outerList);
            for(var loop = 0 ; loop < outerList.length; loop++)
            {
                var innerLoop = outerList[loop].lstwrapCriteria;
                console.log(innerLoop);

                for(var insideLoop = 0; insideLoop < innerLoop.length; insideLoop ++)
                {

                    var operatorIs = innerLoop[insideLoop].selOpVal.split("@")[3];

                    if(operatorIs == 'in' || operatorIs == 'not in')
                    {
                        var reqString = innerLoop[insideLoop].fieldVal;
                        var newString = '';

                        for(var i = 0 ;i< reqString.length; i++)
                        {
                            if(reqString[i] != '\'' && reqString[i] != '\"')
                            {
                                newString += reqString[i];
                                console.log(newString);
                            }
                        }
                        newFile["0"].lstWrapGroup[loop].lstwrapCriteria[insideLoop].fieldVal = newString;
                    }
                    console.log(newFile);

                    console.log('Hey Operator is ');
                    console.log(operatorIs);
                }

            }
            param1 = JSON.stringify(newFile);
            component.set("v.wrapperBlock", newFile);
            

        }
    },

    doneRendering: function(component, event, helper) {    
        var response= component.get('v.wrapperBlock'); 
        console.log(response);
            for(var k=0; k <response.length; k++){
                var lstWrapGrp = response[k].lstWrapGroup;
                if(lstWrapGrp != null){
                    for(var i=0; i < lstWrapGrp.length; i++){
                        var criList = lstWrapGrp[i].lstwrapCriteria;
                        var btnOR = document.getElementById('btnOR_'+i+'_'+k);
                        var btnAnd = document.getElementById('btnAnd_'+i+'_'+k);
                        var modal = document.getElementById('grpOp_'+i+'_'+k);                
                        if(i!=lstWrapGrp.length-1){                    
                            $A.util.removeClass(modal, 'hide');
                            $A.util.addClass(modal, 'display'); 
                            if(component.get("v.isBtnClick") == ''){
                                if(lstWrapGrp[i+1].selGroupOp == 'OR'){
                                    $A.util.addClass(btnOR,'slds-is-selected');
                                    $A.util.removeClass(btnAnd,'slds-is-selected');
                                }
                                else{
                                    $A.util.addClass(btnAnd,'slds-is-selected');
                                    $A.util.removeClass(btnOR,'slds-is-selected');
                                }
                            }
                            
                        }
                        else if(lstWrapGrp.length==1){                    
                            var modal = document.getElementById('grpOp_'+i+'_'+k);
                            $A.util.removeClass(modal, 'display');
                            $A.util.addClass(modal, 'hide');         
                        }
                       
                }
            }
        }
           
    },
    getTableName : function(component, event, helper){ 
            var actionInpTableName = component.get("c.getInputTableName");
            actionInpTableName.setParams({ 
                "scenarioInstanceId" : component.get("v.scenarioRuleInstanceId")
            });
            actionInpTableName.setCallback(this, function(response){
                 var state = response.getState();
                 if(component.isValid() && state==="SUCCESS")
                 {
                    var itemsSample = response.getReturnValue();  
                    component.set('v.inputTableName',itemsSample);
                    
                }else{
                    console.log("Failed with state:  "+ state);
                }
            });
            $A.enqueueAction(actionInpTableName);

            var actionOutTableName = component.get("c.getOutputTableName");
            actionOutTableName.setParams({ 
                "scenarioInstanceId" : component.get("v.scenarioRuleInstanceId")
            });
            actionOutTableName.setCallback(this, function(data){
                var response= data.getReturnValue();  
                component.set('v.outputTableName',response);
                //component.hideSpinner();
            });
            $A.enqueueAction(actionOutTableName);

    },
    CallFunction : function(component, event, helper){   
    // alert('CallFunction');       
        var fName=event.getParam('dataindex').split('@')[1];        
        if(fName=='AddCriteria'){
            helper.addCriteria(component,event);
        }
        else if (fName=='DeleteCriteria'){
            helper.deleteCriteria(component,event);
        }
        else if (fName=='AddGroup'){
            helper.addGroup(component,event,helper);
        }
        else if (fName=='DeleteGroup'){
            helper.deleteGroup(component,event);
        }else if (fName=='AndClicked'){
            helper.btnANDClick(component,event);
        }else if (fName=='OrClicked'){
            helper.btnORClick(component,event);
        }else if (fName=='OnColumnChange'){
           helper.onColumnChange(component,event);
        }else if (fName=='OnOperatorChange'){
            console.log('Inside callFunction');
            helper.onOperatorChange(component,event);
        }
    },
    
    addABlock: function(component, event, helper){
        helper.addBlock(component);
    },

    ShowQuery: function(component, event, helper){
        var filterRuleId = component.find("filterRuleId");
        $A.util.addClass(filterRuleId,'none_opacity');
        helper.showPopupHelper(component, 'modalViewQuery', 'slds-');
        helper.DisplayQuery(component);
    },
    
     resultQuery: function(component, event, helper){
        var oTextarea = component.find("oTextarea");       
        var lstBlock = component.get('v.wrapperBlock');         
        var executionLogic = '';        
        var noneClause = false;
        for(var m=0; m< lstBlock.length; m++){
            var block = lstBlock[m];
            if(m==0){}
                //executionLogic+="If";
            else
                //executionLogic+="Else If";

            var lstCategory= block.lstOutputCriteria;
            var lstGroup= block.lstWrapGroup; // lstGrp under each block
            
            console.log('block.lstWrapGroup-->');
            console.log(block.lstWrapGroup);

            var notValidQuery = false;
             executionLogic+= '(';
            for(var k=0; k < lstGroup.length; k++){
                var grp = lstGroup[k];
                var lstCriteria = grp.lstwrapCriteria;
                executionLogic+= '(';

                for(var j=0; j < lstCriteria.length; j++){
                    var cri = lstCriteria[j];                
                    
                    console.log(cri.criName);
                    console.log(cri.selFieldName.split("@")[3]);
                    console.log(cri.Param_value__c);
                    console.log(cri.Filter_Field_Name__c);
                    console.log(cri.Criteria_operator__c);
                    console.log(cri.Logic_Operator__c);
                    console.log(block.lstWrapGroup["0"].lstwrapCriteria.length);

                    //check for valid query
                    if(
                        ((cri.selOpVal.split("@")[3].trim() == 'None' || cri.selFieldName.split("@")[4].trim() == 'None') && (block.lstWrapGroup["0"].lstwrapCriteria.length>1)) || 
                        (!((cri.selOpVal.split("@")[3].trim() != 'None' && cri.selFieldName.split("@")[4].trim() != 'None') || (cri.selOpVal.split("@")[3].trim() == 'None' && cri.selFieldName.split("@")[4].trim() == 'None')) && (block.lstWrapGroup["0"].lstwrapCriteria.length =='1'))
                        ) {
                        notValidQuery = true;
                        console.log(block.lstWrapGroup["0"].lstwrapCriteria.length);
                        
                        console.log('--inside notvalidquery');
                        var errorMessage = 'Conditions not set properly. Kindly create a valid query or everything will be lost.';
                        /*var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            mode: 'dismissible',
                            message: errorMessage,
                            type : 'error'
                            
                        });
                        toastEvent.fire();*/
                        
                        var oTextarea1 = component.find("oTextarea");  
                        console.log(oTextarea1); 
                        
                        oTextarea1.set("v.value", errorMessage);

                        helper.showPopupHelper(component, 'modalViewQuery', 'slds-');
                    }
                    else{
                         /*if(((cri.selOpVal.split("@")[3].trim() == 'None' || cri.selFieldName.split("@")[4].trim() == 'None') && cri.fieldVal=='') && (block.lstWrapGroup["1"].lstwrapCriteria.length>=0)) {
                                  var errorMessage = 'Conditions not set properly. Kindly create a valid query or everything will be lost.'; 
                                   var oTextarea1 = component.find("oTextarea");  
                                   console.log(oTextarea1); 
                                   oTextarea1.set("v.value", errorMessage);
                                   helper.showPopupHelper(component, 'modalViewQuery', 'slds-');
                                   return;
                            }*/
                       
                        if(((cri.selOpVal.split("@")[3].trim() == 'None' || cri.selFieldName.split("@")[4].trim() == 'None') && cri.fieldVal=='') && (block.lstWrapGroup.length>1)) {
                             var errorMessage = 'Conditions not set properly. Kindly create a valid query or everything will be lost.';
                               var oTextarea1 = component.find("oTextarea");  
                               console.log(oTextarea1); 
                               oTextarea1.set("v.value", errorMessage);
                               helper.showPopupHelper(component, 'modalViewQuery', 'slds-');
                               return;
                        }
                        else if(((cri.selOpVal.split("@")[3].trim() == 'None' || cri.selFieldName.split("@")[4].trim() == 'None') && cri.fieldVal=='') && (block.lstWrapGroup.length=='1')) {
                             noneClause = true;
                        }
                        else if((cri.selOpVal.split("@")[3].trim() == 'None' || cri.selFieldName.split("@")[4].trim() == 'None') && cri.fieldVal!=''){

                               var errorMessage = 'Conditions not set properly. Kindly create a valid query or everything will be lost.';
                               var oTextarea1 = component.find("oTextarea");  
                               console.log(oTextarea1); 
                               oTextarea1.set("v.value", errorMessage);
                               helper.showPopupHelper(component, 'modalViewQuery', 'slds-');
                               return;
                        }
                        else if(cri.selFieldName.split("@")[4].trim() != 'None' && ( cri.selOpVal.split("@")[3].trim() != 'is null' && cri.selOpVal.split("@")[3].trim() != 'is not null') && cri.fieldVal.trim()=='' ){

                               var errorMessage = 'Conditions not set properly. Kindly create a valid query or everything will be lost.';
                               var oTextarea1 = component.find("oTextarea");  
                               console.log(oTextarea1); 
                               oTextarea1.set("v.value", errorMessage);
                               helper.showPopupHelper(component, 'modalViewQuery', 'slds-');
                               return;
                        }
                        else{
                        var operatorStr = cri.selOpVal.split("@")[3].trim();
                        console.log('--operator ' + cri.selOpVal.split("@")[3].trim());
                        if(cri.selOpVal.split("@")[3].trim() == 'equals')
                            operatorStr = '=';
                        
                        if(cri.selOpVal.split("@")[3].trim() == 'not equals')
                            operatorStr = '!=';

                        if(cri.selOpVal.split("@")[3].trim() == 'contains')
                            operatorStr = 'Like';    
                        
    					if(cri.selOpVal.split("@")[3].trim() == 'does not contains')
                            operatorStr = 'Not Like'; 
    					
    					if(cri.selOpVal.split("@")[3].trim() == 'greater than')
                            operatorStr = '>';
    					
    					if(cri.selOpVal.split("@")[3].trim() == 'greater or equal')
                            operatorStr = '>=';
    					
    					if(cri.selOpVal.split("@")[3].trim() == 'less than')
                            operatorStr = '<';
    					
    					if(cri.selOpVal.split("@")[3].trim() == 'less or equal')
                            operatorStr = '<=';
    					
    					if(cri.selOpVal.split("@")[3].trim() == 'begins with')
                            operatorStr = 'Like';
    					
    					if(cri.selOpVal.split("@")[3].trim() == 'ends with')
                            operatorStr = 'Like';
                        
                        console.log('--operatorStr ' + operatorStr);
                        
                        if(cri.criName != '-'){
                            /*if(cri.selFieldName.split("@")[3]== "Text"){
                                if(cri.selOpVal.split("@")[3].trim() == 'contains')
                                    executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"''%"+cri.fieldVal+"%''";
                                
                                else
                                    executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"''"+cri.fieldVal+"''";
                            }
                            else{
                                executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+cri.fieldVal;    
                            }*/
                             if(cri.selFieldName.split("@")[3]== "Text"){
    							if(cri.selOpVal.split("@")[3].trim() == 'does not contains'){
                                    executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"''%25"+cri.fieldVal.trim()+"%25''";
                                }
                                else if(cri.selOpVal.split("@")[3].trim() == 'contains'){
                                    executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"''%25"+cri.fieldVal.trim()+"%25''";
                                }
    							else if(cri.selOpVal.split("@")[3].trim() == 'begins with'){
                                    executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"''"+cri.fieldVal.trim()+"%25''";
                                }
    							else if(cri.selOpVal.split("@")[3].trim() == 'ends with'){
                                    executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"''%25"+cri.fieldVal.trim()+"''";
                                }
                                else if(cri.selOpVal.split("@")[3].trim() == 'not in'){
                                 var listOfNotIn = cri.fieldVal.trim().split(',');
                                 var strNotInValue = '\'\'';
                                 var strNotIn = '';
                                 for(var i=0; i < listOfNotIn.length; i++){
                                    strNotIn = strNotIn +strNotInValue + listOfNotIn[i] + '\'\''+','; 
                                 }
                                 strNotIn = strNotIn.slice(0, -1);
                                 cri.fieldVal = strNotIn;
                                 console.log("NOT IN---"+cri.fieldVal);
                                 executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"("+cri.fieldVal.trim()+")"; 
                               }  
                                else if(cri.selOpVal.split("@")[3].trim() == 'in'){
                                 var listOfIn = cri.fieldVal.trim().split(',');
                                 var strInValue = '\'\'';
                                 var strIn = '';
                                 for(var i=0; i < listOfIn.length; i++){
                                    strIn = strIn +strInValue + listOfIn[i] + '\'\''+','; 
                                 }
                                 strIn = strIn.slice(0, -1);
                                 cri.fieldVal = strIn;
                                 console.log("IN---"+cri.fieldVal);
                                 executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"("+cri.fieldVal.trim()+")"; 
                               } 
    						   else if(cri.selOpVal.split("@")[3].trim() == 'is null'){
    						     executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr;
    						   }
    						   else if(cri.selOpVal.split("@")[3].trim() == 'is not null'){
    						     executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr;
    						   }
                               else{
                                    executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"''"+cri.fieldVal.trim()+"''";
                                   }
                            }
                            else{
                                if(cri.selOpVal.split("@")[3].trim() == 'not in'){
                                 var listOfNotIn = cri.fieldVal.trim().split(',');
                                 //var strNotInValue = '\'\'';
                                 var strNotIn = '';
                                 for(var i=0; i < listOfNotIn.length; i++){
                                     strNotIn = strNotIn + listOfNotIn[i] +','; 
                                 }
                                 strNotIn = strNotIn.slice(0, -1);
                                 cri.fieldVal = strNotIn;
                                 console.log("NOT IN---"+cri.fieldVal);
                                 executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"("+cri.fieldVal.trim()+")"; 
                               } 
                               else if(cri.selOpVal.split("@")[3].trim() == 'in'){
                                var listOfIn = cri.fieldVal.trim().split(',');
                                var strInValue = '\'\'';
                                var strIn = '';
                                for(var i=0; i < listOfIn.length; i++){
                                    strIn = strIn +strInValue + listOfIn[i] + '\'\''+','; 
                                }
                                 strIn = strIn.slice(0, -1);
                                 cri.fieldVal = strIn;
                                 console.log("IN---"+cri.fieldVal);
                                 executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"("+cri.fieldVal.trim()+")"; 
                              }
    						   else if(cri.selOpVal.split("@")[3].trim() == 'is null'){
    						     executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr;
    						   }
    						   else if(cri.selOpVal.split("@")[3].trim() == 'is not null'){
    						     executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr;
    						   }
    						  else{
                                    executionLogic+=" "+cri.criName+" "+cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+cri.fieldVal.trim(); 
                                } 
                            }
                            
                        }
                        else{
                            if(cri.selFieldName.split("@")[3]== "Text"){
    							if(cri.selOpVal.split("@")[3].trim() == 'does not contains'){
                                    executionLogic+=cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"''%25"+cri.fieldVal.trim()+"%25''";
                                }
                                else if(cri.selOpVal.split("@")[3].trim() == 'contains'){
                                    executionLogic+=cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"''%25"+cri.fieldVal.trim()+"%25''";
                                }
    							else if(cri.selOpVal.split("@")[3].trim() == 'begins with'){
                                    executionLogic+=cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"''"+cri.fieldVal.trim()+"%25''";
                                }
    							else if(cri.selOpVal.split("@")[3].trim() == 'ends with'){
                                    executionLogic+=cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"''%25"+cri.fieldVal.trim()+"''";
                                }
                                else if(cri.selOpVal.split("@")[3].trim() == 'not in'){
                                 var listOfNotIn = cri.fieldVal.trim().split(',');
                                 var strNotInValue = '\'\'';
                                 var strNotIn = '';
                                 for(var i=0; i < listOfNotIn.length; i++){
                                     strNotIn = strNotIn +strNotInValue + listOfNotIn[i] + '\'\''+','; 
                                 }
                                 strNotIn = strNotIn.slice(0, -1);
                                 cri.fieldVal = strNotIn;
                                 console.log("NOT IN---"+cri.fieldVal);
                                 executionLogic+=cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"("+cri.fieldVal.trim()+")"; 
                               }  
                                else if(cri.selOpVal.split("@")[3].trim() == 'in'){
                                 var listOfIn = cri.fieldVal.trim().split(',');
                                 var strInValue = '\'\'';
                                 var strIn = '';
                                 for(var i=0; i < listOfIn.length; i++){
                                     strIn = strIn +strInValue + listOfIn[i] + '\'\''+','; 
                                 }
                                 strIn = strIn.slice(0, -1);
                                cri.fieldVal = strIn;
                                console.log("IN---"+cri.fieldVal);
                                executionLogic+=cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"("+cri.fieldVal.trim()+")"; 
                              } 
    						   else if(cri.selOpVal.split("@")[3].trim() == 'is null'){
    						     executionLogic+=cri.selFieldName.split("@")[4] +" "+ operatorStr;
    						   }
    						   else if(cri.selOpVal.split("@")[3].trim() == 'is not null'){
    						     executionLogic+=cri.selFieldName.split("@")[4] +" "+ operatorStr;
    						   }
                                 else{
                                    executionLogic+=cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"''"+cri.fieldVal.trim()+"''";
                                    }
                            }
                            else{
                                if(cri.selOpVal.split("@")[3].trim() == 'not in'){
                                 var listOfNotIn = cri.fieldVal.trim().split(',');
                                 //var strNotInValue = '\'\'';
                                 var strNotIn = '';
                                 for(var i=0; i < listOfNotIn.length; i++){
                                     strNotIn = strNotIn + listOfNotIn[i] +','; 
                                 }
                                 strNotIn = strNotIn.slice(0, -1);
                                 cri.fieldVal = strNotIn;
                                 console.log("NOT IN---"+cri.fieldVal);
                                 executionLogic+=cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"("+cri.fieldVal.trim()+")"; 
                                } 
                                else if(cri.selOpVal.split("@")[3].trim() == 'in'){
                                 var listOfIn = cri.fieldVal.trim().split(',');
                                 var strInValue = '\'\'';
                                 var strIn = '';
                                 for(var i=0; i < listOfIn.length; i++){
                                     strIn = strIn +strInValue + listOfIn[i] + '\'\''+','; 
                                 }
                                 strIn = strIn.slice(0, -1);
                                 cri.fieldVal = strIn;
                                 console.log("IN---"+cri.fieldVal);
                                 executionLogic+=cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+"("+cri.fieldVal.trim()+")"; 
                                }
    							else if(cri.selOpVal.split("@")[3].trim() == 'is null'){
    						     executionLogic+=cri.selFieldName.split("@")[4] +" "+ operatorStr;
    						    }
    							else if(cri.selOpVal.split("@")[3].trim() == 'is not null'){
    						     executionLogic+=cri.selFieldName.split("@")[4] +" "+ operatorStr;
    						    }
    						    else{
                                    executionLogic+=cri.selFieldName.split("@")[4] +" "+ operatorStr +" "+cri.fieldVal.trim(); 
                                } 
                            }
                            
                                   
                        }           
                    }  

                   } 
                    
                    
                }
                executionLogic+= ')';
                if(k!=lstGroup.length-1){                    
                        executionLogic+=lstGroup[k+1].selGroupOp+' ';
                    }

            }
            executionLogic+= ')';
            if(noneClause){
                executionLogic='';
            }
        } 

        component.set("v.notValidQuery", notValidQuery); 

        if(!notValidQuery){
            console.log("resultantQuery ===="+executionLogic); 
            var cmpEvent = $A.get('e.c:BRResultFilterQueryEvent');
            cmpEvent.setParams({
                "outputFilter" :  executionLogic,
                "persistWrapBlock" : component.get('v.wrapperBlock')
            });
            cmpEvent.fire();

            component.set('v.wrapperBlock', "[]");
        }
    },
    
    HideQueryPopup:  function(component, event, helper){
        var filterRuleId = component.find("filterRuleId");
        $A.util.removeClass(filterRuleId,'none_opacity');
        helper.hidePopupHelper(component, 'modalViewQuery', 'slds-');

    },
    ShowPopup:  function(component, event, helper){
        helper.showPopupHelper(component, 'modaldialog', 'slds-');
    },
    closeButton : function(component, event, helper) {
        console.log('Inside close');
        component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
        var action = component.get("c.getScenarioID");
        action.setParams({ "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});

        action.setCallback(this, function(response){ 
        var state = response.getState();
        if(component.isValid() && state==="SUCCESS"){
                var arrListValues=response.getReturnValue();

                var evt = $A.get("e.force:navigateToComponent");
                   evt.setParams({
                      componentDef : "c:CPGbusinessRuleCanvas",
                      componentAttributes: {
                    record_Id : arrListValues[0][common.getKey(component,"Scenario_Id__c")],
                    viewcanvas : "Y",
                    workSpaceId : component.get("v.workSpaceId")
                      }
                  });
                  evt.fire();
                
                  }
                  else{
                    console.log("Failed with state:  "+ state);
               }
        })
        $A.enqueueAction(action);
    },
    NextTab : function(component, event, helper) {
        console.log('next');
        var rowFilterTabLink = component.find("RowFilterAuraId");
        var fieldsToIncludeTabLink = component.find("fieldsToIncludeAuraId");
        var rowFilterRuleDataTab = component.find("RowFilterId");
        var fieldsToIncludeDataTab = component.find("FieldsToIncludeId");
        
        $A.util.removeClass(rowFilterTabLink, 'slds-is-active');
        $A.util.addClass(fieldsToIncludeTabLink, 'slds-is-active');  
        $A.util.removeClass(rowFilterRuleDataTab, 'slds-show');
        $A.util.addClass(rowFilterRuleDataTab, 'slds-hide');
        $A.util.removeClass(fieldsToIncludeDataTab, 'slds-hide_frame'); 
        $A.util.addClass(fieldsToIncludeDataTab, 'slds-show');
        var isDataTablecalled=component.get("v.isDataTablecalled");
        if(!isDataTablecalled){
           component.createDataTablecall();
           isDataTablecalled=true; 
           component.set("v.isDataTablecalled",isDataTablecalled);
        }
        
        
        var RowFilterRulesFooter = component.find("RowFilterRulesFooter");
        $A.util.addClass(RowFilterRulesFooter, 'slds-hide');
        var FieldsToIncludeFooter = component.find("FieldsToIncludeFooter");
        $A.util.removeClass(FieldsToIncludeFooter, 'slds-hide');
       
    },
    updateColumnWrapper:function(component,event,helper){
          var $j = jQuery.noConflict();
           var columnWrapper=JSON.parse(JSON.stringify(component.get('v.ColumnWrapper')));
          
           $j("#table-1 > tbody > tr").each(function(index){
                 for(var ctr=0;ctr<columnWrapper.length;ctr++){

                    if(columnWrapper[ctr].Name==$j(this).find("td:eq(1)").text() && 
                        $j(this).find("input:checkbox").prop("checked")==true){
                            columnWrapper[ctr].statusFlag=true;
                            break;
                    }
                    else if(columnWrapper[ctr].Name==$j(this).find("td:eq(1)").text() && 
                        $j(this).find("input:checkbox").prop("checked")==false){
                            columnWrapper[ctr].statusFlag=false;
                            break;
                    }
                    

               }
             }
           )

            
           component.set('v.ColumnWrapper',columnWrapper);
    },

    SaveUpdate : function(component, event, helper){
       
        component.doPageValidation();
            if(component.get('v.isValid')=='true'){
            var wrapperData= component.get('v.wrapperBlock');
           
            component.updateColumnWrapper();
            var columndata=component.get('v.ColumnWrapper');
            console.log(columndata);
            var insertedColumns=[];
            console.log(columndata);
            var ruleName = component.find('ruleName').get('v.value');
            var scenarioId = component.get('v.scenarioRuleInstanceId');
            var ruleId  = component.get("v.businessRuleId");
            var componentId = component.get('v.componentTypeMasterId');
           // component.showSpinner();    
            var action = component.get('c.SaveCheck');        
            action.setParams({ 
                'ruleName' : ruleName,            
                'wrapperData':JSON.stringify(wrapperData),
                'scenarioId':scenarioId,
                'componentId' : componentId,          
                'columndata':JSON.stringify(columndata) 
            });
            action.setCallback(this, function(data){
                var state= data.getState();
                console.log(state);
                if(component.isValid() && state==="SUCCESS"){
                    var response= data.getReturnValue();
                    console.log(response);   
                    //component.hideSpinner();
                    component.closeButton();            
                }
            })
            
            $A.enqueueAction(action);
        }
    }, 

    restrictInputsRule :function(component, event, helper){
      console.log('restrictInputsRule');
      var text=component.find("ruleName");
      var ruleName = text.get('v.value');
      var newtext = ruleName.replace(/(?!^-)[^0-9a-zA-Z_\s]/g, ''); 
      text.set('v.value', newtext);
   
    },
    getExistingRule:function(component, event, helper) {
        var actionRuleName = component.get("c.getRuleName");
            actionRuleName.setParams({ 
                "scenarioId" : component.get("v.scenarioRuleInstanceId"),
            });
            actionRuleName.setCallback(this, function(data){
                var response= data.getReturnValue(); 
                console.log(response);                 
            })
            $A.enqueueAction(actionRuleName);
    },
    createDataTable : function(component, event, helper)
    {
        
        var opts = [];
        var resultColumnWrapper = component.get("v.ColumnWrapper");
        for(var i=0;i<resultColumnWrapper.length;i++){
            opts.push({Id:resultColumnWrapper[i].Id,statusFlag:resultColumnWrapper[i].statusFlag,Name:resultColumnWrapper[i].Name,variabletype:resultColumnWrapper[i].variabletype,colDatatype:resultColumnWrapper[i].colDatatype,colValuetype:resultColumnWrapper[i].colValuetype});
        }    
        if(opts.length>0){
            var sessionTable;
            var $j = jQuery.noConflict();
            var arrHtml=[];
            var result = opts;
            var coulmnJsonResult = [];

            var element = result[0];
            for(var key in element){
                if(key=='statusFlag'){
                    coulmnJsonResult.push({"data":key,"title":'',defaultContent:''});
                }else if(key=='Name'){
                    coulmnJsonResult.push({"data":key,"title":'Name',defaultContent:''});
                }else if(key=='variabletype'){
                    coulmnJsonResult.push({"data":key,"title":'Type',defaultContent:''});
                }else if(key=='colDatatype'){
                    coulmnJsonResult.push({"data":key,"title":'Data Type',defaultContent:''});
                }else if(key=='colValuetype'){
                    coulmnJsonResult.push({"data":key,"title":'Value Type',defaultContent:''});
                }
                

                
            }
            console.log(result);
            sessionTable = $j('#table-1').DataTable(
            {
                
                "search": false,
                 destroy : true,
                 stateSave: true,
                "processing": false,
                "dom": '<"div-tbl">t<"bottom-info"> ', // f search, p :- pagination , l:- page length 
                "data": result,
                 paging:false,
                 "ordering": true,
                 
                scrollX : true,
                 
                scrollY:     "325px",
               
                scrollCollapse: true,

                "language":
                {
                    "emptyTable": "Loading Data"
                },
               
                columns:coulmnJsonResult,
                columnDefs :
                [
                            { "orderable": false, "targets": [0,2,3,4]  },
                            {
                                targets : 0,
                                'searchable': false,
                                'orderable': false,
                                "createdCell":function (td, cellData, rowData, row, col)
                                {   
                                     //console.log(rowData);
                                      var parsehtml;
                                      if(rowData.statusFlag == true){
                                       parsehtml="<div class='slds-form-element'><div class='slds-form-element__control'><span class='slds-checkbox'><input type='checkbox' name='select_all' id='"+rowData.Id+"' checked value='" + rowData.Name + "' /><label class='slds-checkbox__label' for='"+rowData.Id+"'><span class='slds-checkbox_faux'></span></label></span></div></div>"; 
                                       //html="<input type='checkbox' name='options' id='' checked value='" + rowData.statusFlag + "' />"; 
                                      }else{
                                       	parsehtml="<div class='slds-form-element'><div class='slds-form-element__control'><span class='slds-checkbox'><input type='checkbox' name='select_all' id='"+rowData.Id+"'  value='" + rowData.Name + "' /><label class='slds-checkbox__label' for='"+rowData.Id+"'><span class='slds-checkbox_faux'></span></label></span></div></div>";
                                        //html="<input type='checkbox' name='options' id=''  value='" + rowData.statusFlag + "' />";
                                      }
                                      
                                    $j(td).html(parsehtml);
                                }
                            }
                          ],
                
                "createdRow": function ( row, data, index ) 
                {
                      $j(row).addClass('slds-hint-parent');
                      $j('td', row).addClass('slds-text-align_left slds-truncate');
                      $j('thead > tr> th').addClass('slds-is-sortable slds-is-resizable slds-text-title_caps slds-line-height_reset sorting');
                      $j('thead > tr').addClass('slds-text-title_caps ');
                      
                },

                
            });
            component.set("v.sessionTable",sessionTable);
            
                }else{
                 var tableVisibility = component.find("DataTableId");
                 $A.util.addClass(tableVisibility,'slds-hide');
                component.set("v.textMsg","No Data Found");
            }
    },
    
    RowFilterTab : function(component, event, helper) {
        
 
        var tab2 = component.find('RowFilterAuraId');
        var TabTwoData = component.find('RowFilterId');
        var tab3 = component.find('fieldsToIncludeAuraId');
        var TabThreeData = component.find('FieldsToIncludeId');

        $A.util.addClass(tab2, 'slds-is-active');
        $A.util.removeClass(TabTwoData, 'slds-hide');
        $A.util.addClass(TabTwoData, 'slds-show');
        // Hide and deactivate others tab
 
        $A.util.removeClass(tab3, 'slds-is-active');
        $A.util.removeClass(TabThreeData, 'slds-show');
        $A.util.addClass(TabThreeData, 'slds-hide_frame');
        
        var RowFilterRulesFooter = component.find("RowFilterRulesFooter");
        $A.util.removeClass(RowFilterRulesFooter, 'slds-hide');
        var FieldsToIncludeFooter = component.find("FieldsToIncludeFooter");
        $A.util.addClass(FieldsToIncludeFooter, 'slds-hide');

        /*var table=component.get("v.sessionTable");
        if(table){
             table.destroy();
        }*/
        
                
    },

    doPageValidation:function(component, event, helper){
        console.log('doPageValidation');
        var errMessage='';
        var lstWrapperBlock= component.get('v.wrapperBlock');  
        //Check for rule name to bt not blank
        var ruleName = component.find('ruleName').get('v.value');
        if(!ruleName){
            errMessage+='rule name';
            var sname = component.find("ruleName");
            $A.util.addClass(sname , 'errorInput');
        }else if(lstWrapperBlock.length>0){
                        var arrPriority=[];
            for(var i=0; i<lstWrapperBlock.length; i++){
                var block = lstWrapperBlock[i];                                
                if(arrPriority.indexOf(parseInt(block.selPriority)) == -1){
                    
                    for(var k=0; k <block.lstWrapGroup.length; k++){
                        var grp = block.lstWrapGroup[k];                 
                        for(var m=0; m < grp.lstwrapCriteria.length; m++){
                            var cri = grp.lstwrapCriteria[m];
                            if(cri.fieldVal == '' && (cri.selOpVal.split('@')[3] !='is null' && cri.selOpVal.split('@')[3] != 'is not null')){
                                if(errMessage!='')
                                    errMessage+=', ';
                                errMessage+='a value';
                                break;
                            }
                            //check datatype validation
                            else{                            
                                var selectedOperator= cri.selFieldName.split('@')[3];
                                if(selectedOperator=='Numeric' && (cri.selOpVal.split('@')[3] !='is null' && cri.selOpVal.split('@')[3] != 'is not null')){
                                    var numberPattern = /^(-?\d*\.?\d*)(,-?\d*\.?\d*)*$/;
                                    var pattern=numberPattern.test(cri.fieldVal);
                                    if(!pattern){
                                        if(errMessage!=''){
                                            errMessage+=', ';
                                        }
                                        errMessage+='datatype mismatch in if clause';
                                        break;                                    
                                    }
                                }
                            }
                        }
                    }
                    
                }
                else{
                    if(errMessage!='')
                            errMessage+=', ';
                        errMessage+='different priorities \n';
                        break;
                }
                arrPriority.push((parseInt(block.selPriority)));
            }

        }
        if(errMessage !=''){
            component.set('v.isValid','false');
            component.set('v.popoverMessage','Please enter '+errMessage);
            var errorIcn = component.find("errorIcon");
            $A.util.removeClass(errorIcn,'slds-hide');

        }
        else
        component.set('v.isValid','true');
    },
    handleMouseEnter : function(component, event, helper) {
        var popover = component.find("popover");
        $A.util.removeClass(popover,'slds-hide');
    },
       //make a mouse leave handler here
    handleMouseLeave : function(component, event, helper) {
        var popover = component.find("popover");
        $A.util.addClass(popover,'slds-hide');
    },
    handleMouseEnter1 : function(component, event, helper) {
        var popover = component.find("popover1");
        $A.util.removeClass(popover,'slds-hide');
    },
       //make a mouse leave handler here
    handleMouseLeave1 : function(component, event, helper) {
        var popover = component.find("popover1");
        $A.util.addClass(popover,'slds-hide');
    },
           // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
    destoryCmp : function (component, event, helper) {
        component.destroy();
    },
    
})