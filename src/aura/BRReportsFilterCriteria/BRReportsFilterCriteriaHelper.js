({ 
    deleteCriteria:function(component, event){
        var idx=event.getParam('dataindex').split('@')[0];
        //block, ,grp, cri
            if(idx!= null){
            var idB= parseInt(idx.split('_')[0]);
            var idG= parseInt(idx.split('_')[1]);
            var idC = parseInt(idx.split('_')[2]);
            if(idC==0){
                component.set('v.isValid','false');
                component.set('v.popoverMessage1','You cannot delete first criteria of the block');
                console.log(component.get('v.popoverMessage1'));
                var errorIcn = component.find("errorIcon1");
                $A.util.removeClass(errorIcn,'slds-hide');
            }
            else{
                var lstwrapperBlock= JSON.parse(JSON.stringify(component.get('v.wrapperBlock')));                
                if(lstwrapperBlock[idB].lstWrapGroup[idG].lstwrapCriteria.length==1){
                    //delete group
                    var lstGrp = lstwrapperBlock[idB].lstWrapGroup;//[idG];                    
                    lstGrp.splice(idG, 1);
                    component.set("v.wrapperBlock[idB].lstWrapGroup", lstGrp); 
                }
                else{
                    var wrapperBlock= component.get('v.wrapperBlock');
                    var arrayGrps = [];
                    var lstCriteria = wrapperBlock[idB].lstWrapGroup[idG].lstwrapCriteria
                    
                    //push all groups before inserting a new group
                    for(var i=0; i<idC; i++){
                        arrayGrps.push(lstCriteria[i]);
                    }
                    
                    for(var i=idC+1; i<lstCriteria.length; i++){ //2 ---
                        arrayGrps.push(lstCriteria[i]);             
                    }       
                    component.set('v.wrapperBlock['+idB+'].lstWrapGroup['+idG+'].lstwrapCriteria',arrayGrps); 
                }
                this.changeSequenceOrder(component,event);
            }  
        }
             
    },
    resequenceCriteria:function(component,event,helper){ 
        var idx=event.currentTarget.getAttribute("data-index");  
        var wrapperBlock= component.get('v.wrapperBlock');
        var idB= parseInt(idx.split('_')[0]);
        var idG= parseInt(idx.split('_')[1]);
        var idC = parseInt(idx.split('_')[2]);
        var arrayGrps = [];
        var lstCriteria = wrapperBlock[idB].lstWrapGroup[idG].lstwrapCriteria
        
        //push all groups before inserting a new group
        for(var i=0; i<idC; i++){
            arrayGrps.push(lstCriteria[i]);
        }
        
        for(var i=idC+1; i<lstCriteria.length; i++){ //2 ---
            arrayGrps.push(lstCriteria[i]);             
        }       
        component.set('v.wrapperBlock['+idB+'].lstWrapGroup['+idG+'].lstwrapCriteria',arrayGrps);               
    },
    addCriteria : function(component,event) {          
        var idx=event.getParam('dataindex').split('@')[0];
        var idG= parseInt(idx.split('_')[0]);
        var idB= parseInt(idx.split('_')[1]);        
        var wrapperBlock = component.get('v.wrapperBlock');           
        var wrapperGroup =wrapperBlock[idB].lstWrapGroup;        
        var wrapperCriteria =(wrapperGroup)[idG].lstwrapCriteria;        
        wrapperBlock.criteriaSeq=parseInt(wrapperCriteria.length+1);
        console.log(parseInt(wrapperCriteria.length+1));
        // console.log(JSON.stringify(wrapperCriteria));
        var action =  component.get("c.AddCriteria");
        action.setParams({ 
                        "scenarioId" : component.get("v.scenarioRuleInstanceId")            
                    });             
        action.setCallback(this, function(data){            
            var state = data.getState();
            //console.log(state);
            if (state === "SUCCESS") {
                var response= data.getReturnValue();                  
                wrapperCriteria.push(response);                
                var block = wrapperBlock[idB]; // each block  
                var criteriaLogic='';
                var executionLogic='';
                var lstGroup= block.lstWrapGroup; // lstGrp under each block
                var criSeq=1;
                for(var k=0; k < lstGroup.length; k++){
                    var grp = lstGroup[k];
                    var lstCriteria = grp.lstwrapCriteria; //list of criteria
                    criteriaLogic+= '(';
                    if(k==0)
                        executionLogic+='if(';                    
                    else if(k == lstGroup.length-1)
                        executionLogic+= 'else';
                        else
                            executionLogic+= 'else if('; 
                    for(var j=0; j < lstCriteria.length; j++){
                        var cri = lstCriteria[j];
                        cri.seqNo=criSeq;
                        console.log(cri.criName);
                        if(cri.criName != '-'){
                            criteriaLogic+=' '+cri.criName+' '+cri.seqNo;
                            executionLogic+=' '+cri.criName+' '+cri.selFieldName +' '+ cri.selOpVal +' '+cri.fieldVal;
                        }
                        else{
                            criteriaLogic+=cri.seqNo;
                            executionLogic+=cri.selFieldName +' '+ cri.selOpVal +' '+cri.fieldVal;
                        }
                        criSeq++;
                    }                    
                    criteriaLogic+=') ';    
                    executionLogic+=') ';    
                    if(k!=lstGroup.length-1){
                        criteriaLogic+= lstGroup[k+1].selGroupOp+' ';
                        executionLogic+=lstGroup[k+1].selGroupOp+' ';
                    }
                }
                
                block.criteriaLogic=criteriaLogic;
                block.executionLogic=executionLogic;
                component.set('v.wrapperBlock',wrapperBlock);
            }           
            else if (state === "ERROR") {                
                console.log(response.getError());
            }                              
        });
        $A.enqueueAction(action); 
    },
    resequenceGroup:function(component,event,helper){ 
        console.log('resequenceGroup');
        var idx=component.get('v.grpSeq');
        var wrapperBlock= component.get('v.wrapperBlock');
        var idG= parseInt(idx.split('_')[0]); //1
        var idB= parseInt(idx.split('_')[1]);
        var arrayGrps = [];
        var block = wrapperBlock[idB];
        var lstGrp = block.lstWrapGroup; //5
        //push all groups before inserting a new group
        for(var i=0; i<=idG; i++){
            arrayGrps.push(lstGrp[i]);
        }
        //add newly added group
        var response = component.get("v.newGrp");
        arrayGrps.push(response); //2
        for(var i=idG+1; i<lstGrp.length; i++){ //2 ---
            arrayGrps.push(lstGrp[i]);             
        }       
        component.set('v.wrapperBlock['+idB+'].lstWrapGroup',arrayGrps);
        /*var block = wrapperBlock[idB]; // each block                
        var criteriaLogic='';
        var lstGroup= block.lstWrapGroup; // lstGrp under each block
        var criSeq=1;
        for(var k=0; k < lstGroup.length; k++){                    
            var modal = document.getElementById('grpOp_'+k+'_'+idB);
            $A.util.removeClass(modal, 'hide');
            $A.util.addClass(modal, 'display');      
            
            var grp = lstGroup[k];
            var lstCriteria = grp.lstwrapCriteria; //list of criteria
            criteriaLogic+= '(';
            for(var j=0; j < lstCriteria.length; j++){
                var cri = lstCriteria[j];
                cri.seqNo=criSeq;
                criSeq++;
                if(cri.criName != '-')
                    criteriaLogic+=' '+cri.criName+' '+cri.seqNo;
                else
                    criteriaLogic+=cri.seqNo;
            }
            criteriaLogic+=') ';                    
            if(k!=lstGroup.length-1)
                criteriaLogic+= lstGroup[k+1].selGroupOp+' ';
        }
        //code to update sequence ends 
        block.criteriaLogic=criteriaLogic;               
        component.set('v.wrapperBlock',wrapperBlock); */
        this.changeSequenceOrder(component, event);
    },

    addGroup:function(component,event,helper){  
        var idx=event.getParam('dataindex').split('@')[0];
        component.set('v.grpSeq',idx);
        console.log(idx);
        var idG= parseInt(idx.split('_')[0]);
        console.log('Group Index'+idG);
        var idB= parseInt(idx.split('_')[1]);
        var nwG = parseInt(idG+1);
        console.log('grpBind_'+nwG+'_'+idB);        
        var wrapperBlock= component.get('v.wrapperBlock');
       // wrapperBlock[idB].lstWrapGroup[idG].selGroupOp='And';
        var action =  component.get("c.AddAGroup");
        action.setParams({ 
                        "scenarioId" : component.get("v.scenarioRuleInstanceId")            
                    });             
        action.setCallback(this, function(data){            
            var state = data.getState();
            console.log(state);
            if (state === "SUCCESS") {              
                var response= data.getReturnValue();  //brand new grp
                component.set("v.newGrp",response);               
                console.log(response);                
                helper.resequenceGroup(component,event,helper);
                //component.set('v.wrapperBlock['+idB+'].lstWrapGroup['+nwG+']',response);
                //code to update sequence
                               
               
            }           
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log(errors);
            }                              
        });
        $A.enqueueAction(action); 
       var modal = document.getElementById('grpOp_'+idG+'_'+idB);	    
		$A.util.removeClass(modal, 'hide');			
		$A.util.addClass(modal, 'display');  
    },
    deleteGroup:function(component,event){
        
        var idx=event.getParam('dataindex').split('@')[0]; //''grp_block
        var idG= parseInt(idx.split('_')[0]);
        var idB= parseInt(idx.split('_')[1]);
        var lstwrapperBlock= component.get('v.wrapperBlock');
        var lstGrp = lstwrapperBlock[idB].lstWrapGroup;//[idG];
        if(lstGrp.length==1){
            component.set('v.isValid','false');
            component.set('v.popoverMessage1','You cannot delete the only group of the block');
            console.log(component.get('v.popoverMessage1'));
            var errorIcn = component.find("errorIcon1");
            $A.util.removeClass(errorIcn,'slds-hide');
        }else{
            lstGrp.splice(idG, 1);
            var updatedidG = idG-1;
            var idxbtn = 'grpOp_'+updatedidG+'_'+idB;
            var modal = document.getElementById(idxbtn); 
            $A.util.addClass(modal, 'hide');
            $A.util.removeClass(modal, 'display');
            component.set("v.wrapperBlock[idB].lstWrapGroup", lstGrp);
            this.changeSequenceOrder(component, event);
        }
    },
    getColumnDetailValue:function(component){
        var action = component.get("c.getColumnsData");
        action.setParams({ "scenarioInstanceId" : component.get("v.scenarioRuleInstanceId")});
        action.setCallback(this, function(response){ 
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS")
            {
                var opts = [];
                var arrRuleTypes=response.getReturnValue();
                for(var ctr=0;ctr<arrRuleTypes.length;ctr++){                    
                    opts.push({label:arrRuleTypes[ctr].Id, value: arrRuleTypes[ctr].Name});                    
                }
                component.set("v.ruleOptions",opts);   
            }
            else{
                console.log("Failed with state:  "+ state);
            }
        })
        $A.enqueueAction(action);
    },
    showPopupHelper: function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className + 'hide');
        $A.util.addClass(modal, className + 'open');                
    },
    hidePopupHelper: function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
        
    },
    
    
    getOperatortype : function(component, event, helper) {
        console.log("inside operationtype select method");
        var action = component.get("c.getOperatorTypeValues");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS")
            {
                var opts = [];
                var arrOperatorTypes=[];
                var arrExpressions=response.getReturnValue(); 
                component.set("v.operators",response.getReturnValue());
                // alert(component.get("v.operators"));
                console.log("CHECK");
                console.log(arrExpressions);
                for(var arrCtr=0;arrCtr<arrExpressions.length;arrCtr++){
                    if(arrOperatorTypes.indexOf(arrExpressions[arrCtr][common.getKey(component,"Operator_Type__c")])==-1){
                        arrOperatorTypes.push(arrExpressions[arrCtr][common.getKey(component,"Operator_Type__c")]);
                    }  
                }
                opts.push({label: 'Select an Option', value: 'Select an Option'});
                for(var ctr=0;ctr<arrOperatorTypes.length;ctr++){ 
                    
                    opts.push({label:arrOperatorTypes[ctr],value: arrOperatorTypes[ctr]});
                    component.set("v.typeoptions", opts);                 }
            }
            else{
                console.log("Failed with state:  "+ state);
            }
        })
        $A.enqueueAction(action);
    },
    getFields : function(component, event, helper) {
        console.log("inside getFields controller");  
        var sampledata = [{"fieldName":"gattexlead_flag","fieldType":"source","dataType":"Numeric"}
                          ,{"fieldName":"callactivity_flag","fieldType":"Derived","dataType":"Text"}];
        component.set("v.columndata",sampledata);
        
    },
    RebindGroup: function(wrapperBlock, idB){       
        //var lstBlock = component.get('v.wrapperBlock');                 
        var block = wrapperBlock[idB]; // each block
        console.log('rebind block');
        console.log(block);
        var lstGroup= block.lstWrapGroup; // lstGrp under each block
        var criSeq=1;
        for(var k=0; k < lstGroup.length; k++){
            var grp = lstGroup[k];
            var lstCriteria = grp.lstwrapCriteria; //list of criteria
            for(var j=0; j < lstCriteria.length; j++){
                var cri = lstCriteria[j];
                cri.seqNo=criSeq;
                criSeq++;
            }
        }        
        // component.set('v.wrapperBlock['+idB+']',block);        
    },
    
    onSelectColmnchange :function(component,event) {   
        var selectedValue= component.find("selcOps").get("v.value");
        console.log(selectedValue);
    },
    DisplayQuery:function(component){
        
        var oTextarea = component.find("oTextarea");       
        var lstBlock = component.get('v.wrapperBlock');         
        var executionLogic='';        
        
        for(var m=0; m< lstBlock.length; m++){
            var block = lstBlock[m];
          //  if(m==0)
          //      executionLogic+="If";
          //  else
          //      executionLogic+="Else If";
            var lstCategory= block.lstOutputCriteria;
            var lstGroup= block.lstWrapGroup; // lstGrp under each block
            console.log('block.lstWrapGroup-->');
            console.log(block.lstWrapGroup);
            executionLogic+= '(';
            for(var k=0; k < lstGroup.length; k++){
                var grp = lstGroup[k];
                var lstCriteria = grp.lstwrapCriteria; //list of criteria
                executionLogic+= '(';
                for(var j=0; j < lstCriteria.length; j++){
                    var cri = lstCriteria[j];                
                    console.log(cri.criName);
                    if(cri.criName != '-'){                        
                        executionLogic+=' '+cri.criName+' '+cri.selFieldName.split('@')[4] +' '+ cri.selOpVal.split('@')[3] +' '+cri.fieldVal;
                    }
                    else{                        
                        executionLogic+=cri.selFieldName.split('@')[4] +' '+ cri.selOpVal.split('@')[3] +' '+cri.fieldVal;
                    }                    
                }                                    
                executionLogic+=') ';  
                
                 if(k!=lstGroup.length-1){                    
                    executionLogic+=lstGroup[k+1].selGroupOp+' ';
                }
            }

            executionLogic+=') ';  
            
        }
        oTextarea.set("v.value", executionLogic);
      
        
    },
    
    addBlock: function(component){        
        var wrapperBlock = component.get('v.wrapperBlock');
        var priority = wrapperBlock[0].priority[wrapperBlock[0].priority.length-1];                        
        var nextPriority = parseInt(priority)+1;
        wrapperBlock[0].priority.push(nextPriority);
        
        var len= wrapperBlock.length;
        var newLen = parseInt(len+1);
        var outputCategory = component.get('v.OutputCategory');   
        
        var categoryData='';
        if(outputCategory.length>0)
            categoryData=JSON.stringify(outputCategory);
        
        var action =  component.get("c.AddNewBlock");   
        action.setParams({
            'outputCategory' : categoryData
        });
        action.setCallback(this, function(data){            
            var state = data.getState();
            console.log(state);
            if (state === "SUCCESS") {
                var response= data.getReturnValue();  //brand new block 
                
                response.selPriority=nextPriority;     
                response.priority = wrapperBlock[0].priority;
                component.set('v.wrapperBlock['+len+']',response);
            }           
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log(errors);      
            }                              
        });
        $A.enqueueAction(action); 
        
    },
    // function for create new object Row in Criteria List 
    createCriteriaData: function(component, event, helper) {
        console.log('Inside Helper');
        var RowItemList = component.get("v.wrapperBlock");
        RowItemList.push({
            'sobjectType': 'Contact',
            'FirstName': '',
            'LastName': '',
            'Phone': ''
        });
        // set the updated list to attribute (contactList) again    
        component.set("v.wrapperBlock", RowItemList);
    },
    btnANDClick : function(component, event, helper){
        var idx=event.getParam('dataindex').split('@')[0]; //''grp_block
        var idG= parseInt(idx.split('_')[0]);
        var idB= parseInt(idx.split('_')[1]);
        var btnOR = document.getElementById('btnOR_'+idx);
        var btnAnd = document.getElementById('btnAnd_'+idx);
        $A.util.removeClass(btnOR,'slds-is-selected');
        $A.util.addClass(btnAnd,'slds-is-selected');
        var wrapperBlock= component.get('v.wrapperBlock');
        var block = wrapperBlock[idB]; // each block                
        var criteriaLogic='';
        var lstGroup= block.lstWrapGroup; // lstGrp under each block                
        for(var k=0; k < lstGroup.length; k++){
            var grp = lstGroup[k];
            var lstCriteria = grp.lstwrapCriteria; //list of criteria
            if(idG==k)
                lstGroup[k+1].selGroupOp ='AND';
            criteriaLogic+= '(';
            for(var j=0; j < lstCriteria.length; j++){
                var cri = lstCriteria[j];
                if(cri.criName != '-')
                    criteriaLogic+=' '+cri.criName+' '+cri.seqNo;
                else
                    criteriaLogic+=cri.seqNo;
            }
            criteriaLogic+=') ';
            if(k!=lstGroup.length-1)
                criteriaLogic+= lstGroup[k+1].selGroupOp+' ';
        }
        //code to update sequence ends 
        block.criteriaLogic=criteriaLogic;               
        component.set('v.wrapperBlock',wrapperBlock); 
        component.set("v.isBtnClick","yes");
        
    },
    btnORClick : function(component, event, helper){
        component.set("v.isBtnClick","yes");
        var idx=event.getParam('dataindex').split('@')[0]; //''grp_block
        var idG= parseInt(idx.split('_')[0]);
        var idB= parseInt(idx.split('_')[1]);
        var btnOR = document.getElementById('btnOR_'+idx);
        var btnAnd = document.getElementById('btnAnd_'+idx);
        $A.util.removeClass(btnAnd,'slds-is-selected');
        $A.util.addClass(btnOR,'slds-is-selected');
        var wrapperBlock= component.get('v.wrapperBlock');
        var block = wrapperBlock[idB]; // each block                
        var criteriaLogic='';
        var lstGroup= block.lstWrapGroup; // lstGrp under each block                
        for(var k=0; k < lstGroup.length; k++){
            var grp = lstGroup[k];
            var lstCriteria = grp.lstwrapCriteria; //list of criteria
            if(idG==k)
                lstGroup[k+1].selGroupOp ='OR';
            criteriaLogic+= '(';
            for(var j=0; j < lstCriteria.length; j++){
                var cri = lstCriteria[j];
                if(cri.criName != '-')
                    criteriaLogic+=' '+cri.criName+' '+cri.seqNo;
                else
                    criteriaLogic+=cri.seqNo;
            }
            criteriaLogic+=') ';
            if(k!=lstGroup.length-1)
                criteriaLogic+= lstGroup[k+1].selGroupOp+' ';
        }
        //code to update sequence ends 
        block.criteriaLogic=criteriaLogic;               
        component.set('v.wrapperBlock',wrapperBlock); 
        console.log('v.wrapperBlock');
        console.log( component.get('v.wrapperBlock'));
    },
    onColumnChange :function(component,event) {  
        console.log('onColumnChange');
        //var idx=event.getParam('dataindex').split('@')[2]; //''grp_block
        var idC= event.getParam('dataindex').split('@')[2];
        var idG= event.getParam('dataindex').split('@')[3];
        var idB= event.getParam('dataindex').split('@')[4];
       var colDataType = event.getParam('dataindex').split('@')[5];
       var colName = event.getParam('dataindex').split('@')[6];

       var lstBlock = component.get('v.wrapperBlock');
        var criteria =lstBlock[idB].lstWrapGroup[idG].lstwrapCriteria[idC];
        
      
        var actionRuleName = component.get("c.getDataOptions");
            actionRuleName.setParams({ 
                "colDataType" : colDataType                
            });
            actionRuleName.setCallback(this, function(data){
                var response= data.getReturnValue();  
                console.log(response);               
                criteria.opVal = response;                
                component.set('v.wrapperBlock',lstBlock); 
            })
            $A.enqueueAction(actionRuleName);
    },
    changeSequenceOrder:function(component,event){
        
        var wrapperBlock= component.get('v.wrapperBlock');
        for(var m=0; m<wrapperBlock.length; m++){
            var block = wrapperBlock[m]; // each block  
            var criteriaLogic='';
            var executionLogic='';
            var lstGroup= block.lstWrapGroup; // lstGrp under each block
            var criSeq=1;
            for(var k=0; k < lstGroup.length; k++){
                var grp = lstGroup[k];
                var lstCriteria = grp.lstwrapCriteria; //list of criteria
                criteriaLogic+= '(';
                executionLogic+='(';
                for(var j=0; j < lstCriteria.length; j++){
                    var cri = lstCriteria[j];
                    cri.seqNo=criSeq;
                    console.log(cri.criName);
                    if(cri.criName != '-'){
                        criteriaLogic+=' '+cri.criName+' '+cri.seqNo;
                        executionLogic+=' '+cri.criName+' '+cri.selFieldName.split('@')[4] +' '+ cri.selOpVal.split('@')[3] +' '+cri.fieldVal;
                    }
                    else{
                        criteriaLogic+=cri.seqNo;
                        executionLogic+=cri.selFieldName.split('@')[4] +' '+ cri.selOpVal.split('@')[3] +' '+cri.fieldVal;
                    }
                    criSeq++;
                }                    
                criteriaLogic+=') ';    
                executionLogic+=') ';    
                if(k!=lstGroup.length-1){
                    criteriaLogic+= lstGroup[k+1].selGroupOp+' ';
                    executionLogic+=lstGroup[k+1].selGroupOp+' ';
                }
            }            
            block.criteriaLogic=criteriaLogic;
            block.executionLogic=executionLogic;           
        }
         component.set('v.wrapperBlock',wrapperBlock);                 
    },
    onOperatorChange : function(component, event, helper){  
        console.log('onOperatorChange');   
        var lstBlock= component.get('v.wrapperBlock');        
        for(var i=0; i< lstBlock.length; i++){
            
            var block= lstBlock[i];
            var criteriaLogic='';
            var executionLogic='';
            var criSeq=1;
            var lstGroup= block.lstWrapGroup; // lstGrp under each block            
            for(var k=0; k < lstGroup.length; k++){
                var grp = lstGroup[k];
                var lstCriteria = grp.lstwrapCriteria; //list of criteria
                criteriaLogic+= '(';
                executionLogic+= '(';
                for(var j=0; j < lstCriteria.length; j++){
                    var cri = lstCriteria[j];  
                    cri.seqNo=criSeq;                  
                    console.log(cri.criName);
                    if(cri.criName != '-'){
                      criteriaLogic+=' '+cri.criName+' '+cri.seqNo;
                      executionLogic+=' '+cri.criName+' '+cri.selFieldName.split('@')[4] +' '+ cri.selOpVal.split('@')[3] +' '+cri.fieldVal;
                      
                    }else{
                        criteriaLogic+=cri.seqNo;  
                        executionLogic+=cri.selFieldName.split('@')[4] +' '+ cri.selOpVal.split('@')[3] +' '+cri.fieldVal;
                    }
                    criSeq++;                      
                }
                criteriaLogic+=') ';
                executionLogic+=') ';
                console.log(lstGroup.length);
                if(k!=lstGroup.length-1){
                    criteriaLogic+= lstGroup[k+1].selGroupOp+' ';
                    executionLogic+=lstGroup[k+1].selGroupOp+' ';
                }
            }
            block.criteriaLogic=criteriaLogic;  
            block.executionLogic=executionLogic;          
        }
        component.set('v.wrapperBlock',lstBlock);
    },
})