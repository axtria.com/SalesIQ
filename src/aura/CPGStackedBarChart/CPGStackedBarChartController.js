({
    doInit1 : function(component, event, helper) {
        
        component.set("v.Spinner", true); 
        var params = event.getParam('arguments');
        if (params) {
            var param1 = params.param1;
            var param2 = params.param2;
            var param3 = params.param3;
            var param4 = params.param4;
            var param5 = params.param5;
            component.set("v.currentScenarioId",param1);
            component.set("v.previousScenarioId",param2) ;
            component.set("v.currentInstance",param3) ;
            component.set("v.teamName",param4) ;
            component.set("v.url",param5) ;
            console.log("currentScenarioId is =="+param1); 
            console.log("previousScenarioId is =="+param2); 
        }
        if(!param1  || param1=='undefined' ){
            component.set("v.Spinner", false); 
        }else{
            component.histogramtargets(); }
    },
    
    histogramtargets : function(component, event, helper) {
        console.log('Script Loaded');
        
        var currentScenarioId = component.get("v.currentScenarioId");
        var previousScenarioId = component.get("v.previousScenarioId");
        if(component.get("v.currentInstance")){
         var currentins =   component.get("v.currentInstance").replace('/','');
        }
        var tab_id = "workload_sum";
        console.log('inside stacked column chart doinit method');
        
        console.log('currentScenarioId inside Stacked Bar Chart ::: '+currentScenarioId);
        console.log('previousScenarioId inside Stacked Bar Chart ::: '+previousScenarioId);
        var teamName = component.get("v.teamName");
        var action = component.get("c.generateVisualizationReport");
        var url=component.get("v.url");
        
        //var res = 'http://35.164.63.126:5000/CPG_report_tab_webservice/CPGData/home/?scenario_1='+currentScenarioId+'&scenario_2='+previousScenarioId+'&tab_id='+tab_id+'&team='+teamName+'&context='+currentins; 
        var res = url+'?scenario_1='+currentScenarioId+'&scenario_2='+previousScenarioId+'&tab_id='+tab_id+'&team='+teamName+'&context='+currentins; 
        console.log('strUrl->'+res);
        
        action.setParams({
            "url": res
        });
        
        action.setCallback(this,function(response)
                           {
                               var state = response.getState();
                               if(state === 'SUCCESS' && response.getReturnValue() != null)
                               {   
                                   
                                   var str = "Workload Histogram: Call Plan HCPs";
                                   component.set("v.workloadHistTargets",str);
                                   var resultsMap = response.getReturnValue();
                                   console.log("webservice response");
                                   console.log(resultsMap);
                                   component.set("v.resultsMap",resultsMap);
                                   var noData = component.find("noData");
      						       $A.util.addClass(noData,'slds-hide');
                                   var div1 = component.find("div1");
                                   $A.util.removeClass(div1,'slds-hide');
                                   try{
                                       var labels = [];
                                       var data = [];
                                       
                                       var resultsMap = component.get("v.resultsMap");
                                       console.log('resultsMap');
                                       console.log(resultsMap["CPG_Report_summary_of_targets "]);
                                       
                                       
                                       for(var i=0;i<resultsMap["CPG_Report_summary_of_targets "].length;i++){
                                           var ctr = 0;
                                           for(var key in resultsMap["CPG_Report_summary_of_targets "][i]){
                                               if(ctr==0){
                                                   labels.push(resultsMap["CPG_Report_summary_of_targets "][i][key]);
                                               }
                                               if(ctr==1){
                                                   data.push(resultsMap["CPG_Report_summary_of_targets "][i][key]);
                                               }
                                               ++ctr;
                                           }
                                           
                                          // labels.push(resultsMap["CPG_Report_summary_of_targets "][i].range_txt);
                                         //  data.push(resultsMap["CPG_Report_summary_of_targets "][i].count_summary_target);
                                       }
                                       
                                       console.log('labels:::::: '+labels);
                                       console.log('data:::::: '+data);

                                       component.set("v.Spinner", false);
                                       Highcharts.chart('container', {
    chart: {
        type: 'column',
        ignoreHiddenSeries: false
    },
    title: {
        text: 'Workload Histogram: Call Plan HCPs'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: 
             labels
        ,
        crosshair: true,
        lineColor: '#000',
        lineWidth: 1,
                            //gridLineColor: '#CCC',
                           // gridLineWidth: 1,
    },
    yAxis: {
        min: 0,
        lineColor: '#000',
        lineWidth: 1,
        title: {
            text: '# of Territories',
            style: {                    
                fontWeight:'bold',                    
            }
        },
        labels: {
            style: {                    
                fontSize:'12px',                    
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        //pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
         pointFormat: '<tr>' +
        '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
                                           credits:{
                                               enabled:false
                                           } ,
    series: [{
        name: '# of Call Plan HCPs ',
        data: data

    },]
});
             component.terrHighestTargets();

                                   }
                                   catch(err){
                                       component.set("v.Spinner", false);
                                       var noData = component.find("noData");
                                       $A.util.removeClass(noData,'slds-hide');
                                       var div1 = component.find("div1");
                                       $A.util.addClass(div1,'slds-hide');
                                       console.log('--err ' + err);
                                   }
                                   
                               } else{
                                   component.set("v.Spinner", false);
                                   var noData = component.find("noData");
                                   $A.util.removeClass(noData,'slds-hide');
                                   var div1 = component.find("div1");
                                   $A.util.addClass(div1,'slds-hide');
                               }
                               
                           });
        
        $A.enqueueAction(action);
        
    },
    
    
    terrHighestTargets : function(component, event, helper) {
        
        var result_top_5_targets = component.get("v.resultsMap");
        console.log("webservice response");
        if(result_top_5_targets){
            
            var str = "Territories with the Highest Number of Call Plan HCPs";
            component.set("v.dataTableHT",str);
            var TargetsHL = component.find("TargetsHL");
            $A.util.removeClass(TargetsHL,'slds-hide');
        }
        console.log(result_top_5_targets.CPG_Report_top_5_targets); 
        
        try{
            
            var labels = [];
            var data = [];
            
            var sessionTable;
            var $j = jQuery.noConflict();
            var opts =  result_top_5_targets.CPG_Report_top_5_targets;
            
            var result = opts;
            sessionTable = $j('#table-11').DataTable(
                {
                    
                    
                    // destroy : true,
                    "processing": false,
                    "dom": '<"div-search"><"div-tbl">t<"bottom-info"> ', // f search, p :- pagination , l:- page length 
                    "data": result,
                    paging:false,
                    //"order" : [[3,"asc"]],
                    // scrollX : true,
                    // scrollY:     "300px",
                    destroy : true,
                    "ordering": false,
                    // scrollCollapse: true,
                    
                    "language":
                    {
                        "emptyTable": "Loading Data"
                    },
                    
                    buttons: [{
                        //extend: isIE ? 'csvHtml5' : 'excelHtml5',
                        extend : 'csv',
                        text : '',
                        title : 'Call Plan',  
                    }],
                    columns:[
                        {'title' : 'Territory', 'data' : 'territory_id' },
                        {'title' : 'Targets', 'data' : 'targets' }
                        
                    ],
                    "columnDefs": [
                        { "orderable": false, "targets": [0,1]  }
                        
                        
                    ],
                    "createdRow": function ( row, data, index ) 
                    {
                        $j(row).addClass('slds-hint-parent');
                        $j('td', row).addClass('slds-text-align_left slds-truncate input');
                        $j('thead > tr> th').addClass('slds-text-title_caps slds-line-height_reset');
                        $j('thead > tr').addClass('slds-text-title_caps ');
                        
                    }
                });  
            
          
            $j("#table-11 .menu").on("click", function(){
                
                if(!($j(this).hasClass('slds-is-open'))){
                    $j("#table-11 .menu").removeClass('slds-is-open');
                    var idx = sessionTable.cell( $j(this).parent() ).index().row;
                    var val = ($j(this)).val();
                    var row = sessionTable.rows(idx).data();
                    $j(this).addClass('slds-is-open');
                    $j(this).children()[1].style["position"] = "inherit";
                    
                    
                } else {
                    $j(this).removeClass('slds-is-open');
                }
            });
            
            component.terrLowestTargets();
            
        }
        catch(err){
            console.log('--err ' + err);
        } 
    },
    
    terrLowestTargets : function(component, event, helper) {
        
        var result_bottom_5_targets = component.get("v.resultsMap");
        console.log("webservice response");
        if(result_bottom_5_targets){
            var str = "Territories with the Lowest Number of Call Plan HCPs";
            component.set("v.dataTableLT",str);
        }
        console.log(result_bottom_5_targets.CPG_Report_bottom_5_targets);
        
        try{ 
            var labels = [];
            var data = []; 
            var sessionTable;
            var $j = jQuery.noConflict();
            var opts =  result_bottom_5_targets.CPG_Report_bottom_5_targets;
            
            var result = opts;
            sessionTable = $j('#table-12').DataTable(
                {
                    
                    
                    // destroy : true,
                    "processing": false,
                    "dom": '<"div-search"><"div-tbl">t<"bottom-info"> ', // f search, p :- pagination , l:- page length 
                    "data": result,
                    paging:false,
                    //"order" : [[3,"asc"]],
                    // scrollX : true,
                    // scrollY:     "300px",
                    destroy : true,
                    "ordering": false,
                    // scrollCollapse: true,
                    
                    "language":
                    {
                        "emptyTable": "Loading Data"
                    },
                    
                    buttons: [{
                        //extend: isIE ? 'csvHtml5' : 'excelHtml5',
                        extend : 'csv',
                        text : '',
                        title : 'Call Plan',  
                    }],
                    columns:[
                        {'title' : 'Territory', 'data' : 'territory_id' },
                        {'title' : 'Targets', 'data' : 'targets' }
                        
                    ],
                    "columnDefs": [
                        { "orderable": false, "targets": [0,1]  }
                        
                        
                    ],
                    "createdRow": function ( row, data, index ) 
                    {
                        $j(row).addClass('slds-hint-parent');
                        $j('td', row).addClass('slds-text-align_left slds-truncate input');
                        $j('thead > tr> th').addClass(' slds-text-title_caps slds-line-height_reset');
                        $j('thead > tr').addClass('slds-text-title_caps ');
                        
                    }
                });  
            
            $j(document).ready( function() {
                sessionTable.columns.adjust().draw();
            });
            sessionTable.columns.adjust().draw();
            $j("#table-12 .menu").on("click", function(){
                
                if(!($j(this).hasClass('slds-is-open'))){
                    $j("#table-12 .menu").removeClass('slds-is-open');
                    var idx = sessionTable.cell( $j(this).parent() ).index().row;
                    var val = ($j(this)).val();
                    var row = sessionTable.rows(idx).data();
                    $j(this).addClass('slds-is-open');
                    $j(this).children()[1].style["position"] = "inherit";
                    
                } else {
                    $j(this).removeClass('slds-is-open');
                }
            });
            component.histogramCalls();
            
        }
        catch(err){
            console.log('--err ' + err);
        }  
    },
    
    histogramCalls   : function(component, event, helper) {
        
        var result_summary_calls = component.get("v.resultsMap");
        console.log("webservice response");
        if(result_summary_calls){
            var str = "Workload Histogram: Calls";
            component.set("v.workloadHistCalls",str);
        }
        console.log(result_summary_calls.CPG_Report_summary_of_calls);
        
        try{ 
            var labels = [];
            var data = [];
            
            var resultsMap = component.get("v.resultsMap");
            console.log('resultsMap');
            console.log(resultsMap);
            
            for(var i=0;i<result_summary_calls.CPG_Report_summary_of_calls.length;i++){
                var ctr = 0;
                for(var key in result_summary_calls.CPG_Report_summary_of_calls[i]){
                    if(ctr==0){
                        labels.push(result_summary_calls.CPG_Report_summary_of_calls[i][key]);
                    }
                    if(ctr==1){
                        data.push(result_summary_calls.CPG_Report_summary_of_calls[i][key]);
                    }
                    ++ctr;
                }
                //labels.push(result_summary_calls.CPG_Report_summary_of_calls[i].range_txt);
                //data.push(result_summary_calls.CPG_Report_summary_of_calls[i].count_summary_calls);
            }
            
            console.log('labels:::::: '+labels);
            console.log('data:::::: '+data);
           
            
                  Highcharts.chart('container2', {
    chart: {
        type: 'column',
        ignoreHiddenSeries: false
    },
    title: {
        text: 'Workload Histogram: Calls '
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: 
             labels
        ,
        crosshair: true,
        lineColor: '#000',
        lineWidth: 1,
                            //gridLineColor: '#CCC',
                            //gridLineWidth: 1,
    },
    yAxis: {
        min: 0,
        lineColor: '#000',
        lineWidth: 1,
        title: {
             text: '# of Territories',
            style: {                    
                fontWeight:'bold',                    
            }            
        },
        labels: {
            style: {                    
                fontSize:'12px',                    
            }
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
       // pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        pointFormat: '<tr>' +
        '<td style="padding:0"><b>{point.y} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
                      credits:{
                          enabled:false
                      } ,
    series: [{
         name: '# of Calls ',
        data: data

    },]
});
            
            
            component.terrHighestCalls();
        }
        catch(err){
            console.log('--err ' + err);
        } 
        
    },
    
    
    terrHighestCalls : function(component, event, helper) {
        
        var result_top_5_calls = component.get("v.resultsMap");
        console.log("webservice response");
        if(result_top_5_calls){
            // component.set("v.Spinner", false);
            var str = "Territories with the Highest Number of Calls";
            component.set("v.dataTableHC",str);
            var callsHL = component.find("callsHL");
            $A.util.removeClass(callsHL,'slds-hide');
        }
        console.log(result_top_5_calls.CPG_Report_top_5_calls);
        
        try{
            
            var labels = [];
            var data = [];
            
            var resultsMap = component.get("v.resultsMap");
            console.log('resultsMap');
            console.log(resultsMap); 
            
            var sessionTable;
            var $j = jQuery.noConflict();
            var opts =  result_top_5_calls.CPG_Report_top_5_calls;
            
            var result = opts;
            sessionTable = $j('#table-13').DataTable(
                {
                    
                    
                    // destroy : true,
                    "processing": false,
                    "dom": '<"div-search"><"div-tbl">t<"bottom-info"> ', // f search, p :- pagination , l:- page length 
                    "data": result,
                    paging:false,
                    //"order" : [[3,"asc"]],
                    // scrollX : true,
                    // scrollY:     "300px",
                    destroy : true,
                    "ordering": false,
                    // scrollCollapse: true,
                    
                    "language":
                    {
                        "emptyTable": "Loading Data"
                    },
                    
                    buttons: [{
                        //extend: isIE ? 'csvHtml5' : 'excelHtml5',
                        extend : 'csv',
                        text : '',
                        title : 'Call Plan',  
                    }],
                    columns:[
                        {'title' : 'Territory', 'data' : 'territory_id' },
                        {'title' : 'Calls', 'data' : 'total_calls' },
                        
                    ],
                        "columnDefs": [
                        { "orderable": false, "targets": [0,1]  }
                
                
                ],
                "createdRow": function ( row, data, index ) 
            {
                $j(row).addClass('slds-hint-parent');
                $j('td', row).addClass('slds-text-align_left slds-truncate input');
                $j('thead > tr> th').addClass(' slds-text-title_caps slds-line-height_reset');
                $j('thead > tr').addClass('slds-text-title_caps ');
                
            }
        });  
        
        $j(document).ready( function() {
            sessionTable.columns.adjust().draw();
        });
        sessionTable.columns.adjust().draw();
        $j("#table-13 .menu").on("click", function(){
            
            if(!($j(this).hasClass('slds-is-open'))){
                $j("#table-13 .menu").removeClass('slds-is-open');
                var idx = sessionTable.cell( $j(this).parent() ).index().row;
                var val = ($j(this)).val();
                var row = sessionTable.rows(idx).data();
                
                $j(this).addClass('slds-is-open');
                $j(this).children()[1].style["position"] = "inherit";
               
            } else {
                $j(this).removeClass('slds-is-open');
            }
        });
        component.terrLowestCalls();
        
    }
    catch(err){
    component.set("v.Spinner", false);
    console.log('--err ' + err);
}
 
 },
 
 
 terrLowestCalls : function(component, event, helper) {
    
    var result_bottom_5_calls = component.get("v.resultsMap");
    console.log("webservice response");
    if(result_bottom_5_calls){
        // component.set("v.Spinner", false);
        var str = "Territories with the Lowest Number of Calls";
        component.set("v.dataTableLC",str);
    }
    console.log(result_bottom_5_calls.CPG_Report_bottom_5_calls);
    try{ 
        var labels = [];
        var data = [];
        
        var resultsMap = component.get("v.resultsMap");
        console.log('resultsMap');
        console.log(resultsMap); 
        
        var sessionTable;
        var $j = jQuery.noConflict();
        var opts =  result_bottom_5_calls.CPG_Report_bottom_5_calls;
        
        var result = opts;
        sessionTable = $j('#table-14').DataTable(
            {
                
                
                // destroy : true,
                "processing": false,
                "dom": '<"div-search"><"div-tbl">t<"bottom-info"> ', // f search, p :- pagination , l:- page length 
                "data": result,
                paging:false,
                //"order" : [[3,"asc"]],
                // scrollX : true,
                // scrollY:     "300px",
                destroy : true,
                "ordering": false,
                // scrollCollapse: true,
                
                "language":
                {
                    "emptyTable": "Loading Data"
                },
                
                buttons: [{
                    //extend: isIE ? 'csvHtml5' : 'excelHtml5',
                    extend : 'csv',
                    text : '',
                    title : 'Call Plan',  
                }],
                columns:[
                    {'title' : 'Territory', 'data' : 'territory_id' },
                    {'title' : 'Calls', 'data' : 'total_calls' }
                    
                ],
                "columnDefs": [
                    { "orderable": false, "targets": [0,1]  }
                    
                    
                ],
                "createdRow": function ( row, data, index ) 
                {
                    $j(row).addClass('slds-hint-parent');
                    $j('td', row).addClass('slds-text-align_left slds-truncate input');
                    $j('thead > tr> th').addClass(' slds-text-title_caps slds-line-height_reset');
                    $j('thead > tr').addClass('slds-text-title_caps ');
                    
                }
            });  
        
        $j(document).ready( function() {
            sessionTable.columns.adjust().draw();
        });
        sessionTable.columns.adjust().draw();
        $j("#table-14 .menu").on("click", function(){
            
            if(!($j(this).hasClass('slds-is-open'))){
                $j("#table-14 .menu").removeClass('slds-is-open');
                var idx = sessionTable.cell( $j(this).parent() ).index().row;
                var val = ($j(this)).val();
                var row = sessionTable.rows(idx).data();
                
                $j(this).addClass('slds-is-open');
                $j(this).children()[1].style["position"] = "inherit";
                
            } else {
                $j(this).removeClass('slds-is-open');
            }
        });
        
        $j(".input").each(function(){
             $j(this).text(common.myToLocaleInteger($j(this).text()));
        });
        
        
    }
    catch(err){
        console.log('--err ' + err);
    } 
},
    
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
        
        // this function automatic call by aura:doneWaiting event 
        hideSpinner : function(component,event,helper){
            // make Spinner attribute to false for hide loading spinner    
            component.set("v.Spinner", false);
        },
            
            
            
})