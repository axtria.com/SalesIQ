({
    /*
     * This finction defined column header
     * and calls getPositions helper method for column data
     * editable:'true' will make the column editable
     * */
    doInit : function(component, event, helper) {


        var dateAction = component.get("c.getDateFormat");
        dateAction.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue() != null)
            {
                var dateFormat = response.getReturnValue();
                component.set("v.dateFormat", dateFormat[0]); 
                
            }
        });
        $A.enqueueAction(dateAction);
    	
    	//var rowActions = helper.getRowActions.bind(this,component),
            var actions = [
                { label: 'Edit Employee', name: 'isEditEmployee' },
                { label : 'Manage Assignment', name: 'isEmployeeAssignment'}
            ];
            
            
        
        console.log('--- dataTableWrapper --');

    	var labels = component.get("v.dataTableWrapper.columnLabels");
    	var fields = component.get('v.dataTableWrapper.columnsList');

        component.set("v.queryString", component.get("v.dataTableWrapper.queryString"));

        console.log('queryString :',component.get("v.queryString"));

        //var dataTypesList = JSON.stringify(component.get("v.colTypeList"));

        var colTypeList = JSON.parse(JSON.stringify(component.get("v.colTypeList")));
        
        console.log('colTypeList :',colTypeList);

        var columnsArray = [];

        for(var i =0;i<labels.length;i++)
        {
            var fieldType = 'text';
            for(var key in colTypeList){
                
                if(fields[i] == 'Name'){
                    fieldType = 'url';
                }
                else if(key == fields[i]){
                    if(colTypeList[key] == 'DATE' || colTypeList[key] == 'DATETIME'){
                        fieldType = 'date';
                        
                    }else if(colTypeList[key] == 'REFERENCE'){
                        fieldType = 'url';
                    }

                }
            }
            
            if(fields[i] == 'Name'){
                //columnsArray.push({label: labels[i] , fieldName: 'Id',editable: false, type: fieldType,  sortable : true,typeAttributes: { label: { fieldName: fields[i]}, target: '_blank', href: '', title : fields[i]}, cellAttributes: {style:'' }});
                columnsArray.push({label: labels[i] , fieldName: 'Id',editable: false, type: fieldType,  sortable : true,typeAttributes: {label: { fieldName: 'Name' },  target: '_blank'}, cellAttributes: {style:'' }});
            }
            else if(fieldType == 'url'){
                var tempfield = fields[i];
                var tempfieldValue = fields[i];
                console.log('tempfield :'+tempfield);
                if(tempfield.indexOf('__r') != -1){
                    
                    tempfield = tempfield.split('.')[0];
                    //tempfield = tempfield.replace('__r','__c');
                    tempfield += '.Id';
                    console.log('tempfield 1:'+tempfield);
                    

                    /*tempfieldValue = tempfieldValue.split('.')[0];
                    tempfieldValue += '.Name';
                    console.log('tempfield 2 :'+tempfieldValue);*/
                }
                columnsArray.push({label: labels[i] , fieldName: tempfield ,editable: false, type: fieldType,  sortable : true,typeAttributes: { label: { fieldName: fields[i]}, target: '_blank', href: '', title : fields[i]}, cellAttributes: {style:'' }});
            }
            else if(fieldType == 'date'){
                columnsArray.push({'label' : labels[i] , 'fieldName' : fields[i] , editable: false, type: 'date-local' , sortable : true,typeAttributes:{month:'2-digit',day:'2-digit',year:''}, cellAttributes: {style:'' }});
            }
            else{
                columnsArray.push({'label' : labels[i] , 'fieldName' : fields[i] , editable: false, type: fieldType , sortable : true, cellAttributes: {style:'' }});
            }
        }

        columnsArray.push({ type: 'action', typeAttributes: { rowActions: actions }});

        console.log('columnsArray :',columnsArray);
    
    	component.set("v.columns",columnsArray);
    	

        var dataSet = component.get("v.dataTableWrapper.dataGridList");
        console.log('dataSet :',dataSet);
        var objectName = component.get("v.objectName");
        var interfaceStyle = component.get("v.legendStyleList");
        for(var row=0;row <dataSet.length; row++){
            dataSet[row].Id = '/lightning/r/'+objectName+'/'+dataSet[row].Id +'/view';
            for(var col = 0;col < columnsArray.length ; col++){
                var field = columnsArray[col].fieldName;

                var colorStyle = dataSet[row]['Field_Status__c'];
                
               
                
                if(field != undefined){
                    var fieldValue = field.split('.')[0].replace('__r','__c');
                    if(columnsArray[col].type == 'url'  && field.indexOf('__r') != -1 && dataSet[row][fieldValue] != undefined)
                    {
                        console.log('-- inside if url---');
                        var url = window.location.origin;
                        dataSet[row][field] = url+'/'+dataSet[row][fieldValue];
                        var fieldTitle = columnsArray[col].typeAttributes.title;
                        dataSet[row][fieldTitle] = dataSet[row][fieldTitle.split('.')[0]][fieldTitle.split('.')[1]];
                        //columnsArray[col].typeAttributes.title = dataSet[row][fieldTitle.split('.')[0]][fieldTitle.split('.')[1]];
                        
                    }
                    if(columnsArray[col].type == 'date' && dataSet[row][field] != undefined){
                        console.log('-- inside if date---');
                        console.log(dataSet[row][field]);
                        var dateFormat = component.get("v.dateFormat");
                        
                        var dt = new Date(dataSet[row][field]);
                                                
                        var offsetInMinutes = dt.getTimezoneOffset() ;
                        dt.setSeconds(dt.getSeconds() + (dt.getTimezoneOffset()*60));
                        var mystring = '';
                        if(component.get("v.dateFormat").indexOf('.') !== -1){
                            mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate()).replace(/y+|Y+/,dt.getYear());
                        }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                           mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate()).replace(/y+|Y+/,dt.getYear());;
                        }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                            mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate()).replace(/y+|Y+/,dt.getYear());;
                        }

                        console.log('mystring :'+mystring);

                        dataSet[row][field] = mystring;
                    }
                }
                else{
                    console.log('--- hide manage assignment ---');
                    if(columnsArray[col].type == 'action'){
                        console.log('data' , columnsArray[col]);
                        var dataMap = component.get("v.dataTableWrapper.dataMap");
                        var menuActions = columnsArray[col].typeAttributes.rowActions;
                        console.log('menuActions :',menuActions);
                        for(var menu=0; menu < menuActions.length ;menu ++){
                            var actionName = menuActions[menu].name;
                            console.log('col :'+col);

                            if(dataMap[row][col][actionName] == 'HIDE'){
                                //columnsArray[col].typeAttributes.rowActions[menu] = [];
                                columnsArray[col].typeAttributes.rowActions.splice(menu,1);
                            }
                            else if(dataMap[row][col][actionName] == 'DISABLE'){

                            }
                        }

                    }
                }
                
            }
        }

        console.log('dataSet after changes:',dataSet);
        console.log('columnsArray after changes :',columnsArray);

        component.set("v.pageSize",dataSet.length);
        component.set("v.dataSize", dataSet.length);
        component.set("v.totalRecords", component.get("v.dataTableWrapper.totalRecords"))

        component.set("v.startRecordCount",1);

             

        console.log('dataMap :',component.get("v.dataTableWrapper.dataMap"));

        //const data = [{"Name": 'a', "Employee_ID__c": 'Cloudhub', "Job_Title__c": 0.2, "Employee_Type__c": 25000,"Field_Status__c": 'jrogers@cloudhub.com',"Email__c": '2352235235', "Joining_Date__c": 'utility:down'}];
        
        
        component.set("v.data", dataSet);
    	
        //helper.getPosition(component, helper);
    },
    
    menuRowAction : function (component,event,helper) {

        console.log('--menuRowAction-- ');
        console.log(event);
        var action = event.getParam('action').name;
        var row = event.getParam('row');
        var empID = row.Id;

        empID = empID.split('/')[4];

        var startD = new Date(); 
        startD.setMonth(startD.getMonth()-6);
        startD.setDate(1);
        var endD = new Date(); 
        endD.setMonth(endD.getMonth()+6);
        endD.setDate(0);

        switch (action) {
            case 'isEditEmployee':
                var customEdit = component.get("c.getEditEmployeeConfigSetting");
                customEdit.setCallback(this,function(response){
                    if(response.getState() == 'SUCCESS' && response.getReturnValue() != null){
                        console.log('-----  '+response.getReturnValue());
                        if(response.getReturnValue() != 'true'){
                            var navEvt = $A.get("e.force:navigateToSObject");
                            navEvt.setParams({
                                "recordId": empID
                            });
                            navEvt.fire();
                        }else{
                            console.log('-- calling custom edit ---');
                            var posActionEvent = component.getEvent("setPositionAction");
                            var editEmployeeHeader = 'Edit Employee';
                            
                            posActionEvent.setParams({
                                "actionType": editEmployeeHeader,
                                "modalTitle": editEmployeeHeader,
                                "selectedPosition": empID
                                
                            });               

                            posActionEvent.fire();
                        }
                    }
                });
                $A.enqueueAction(customEdit);
                /*var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": empID
                });
                navEvt.fire();*/
                
            break;
            case 'isEmployeeAssignment':
                var action = component.get("c.getSelectedTeamInstance");
                action.setParams({
                    "employeeId" : empID
                });
                action.setCallback(this,function(response)
                {
                    var state = response.getState();
                    if(state === 'SUCCESS')
                    {   
                        if(response.getReturnValue != null || response.getReturnValue !=''){
                            var selectedTeamInstance = response.getReturnValue();
                            component.set("v.selectedTeamInstance",selectedTeamInstance);
                        }
                        var evt = $A.get("e.force:navigateToComponent");
                        evt.setParams({
                            componentDef : "c:SalesIQManageAssignment",
                            componentAttributes: {
                                recordId : empID,
                                assigneeName : row.Name,
                                startDate : startD,
                                endDate : endD,
                                moduleName : 'Employee Universe',
                                selectedTeamInstance : component.get("v.selectedTeamInstance"),
                                selectedListView : component.get("v.listViewId"),
                                namespace : component.get("v.namespace"),
                                profileMap : component.get("v.profileMap")
                            },
                            isredirect : true
                        });
                        evt.fire();
                    }
                    else if (state === "ERROR" || response.getReturnValue() == null) 
                    {
                        console.log('ERROR: getSelectedTeamInstance callback failed');
                    }    
                });
                $A.enqueueAction(action);
                /*var evt = $A.get("e.force:navigateToComponent");
                evt.setParams({
                    componentDef : "c:SalesIQManageAssignment",
                    componentAttributes: {
                        recordId : empID,
                        assigneeName : row.Name,
                        startDate : startD,
                        endDate : endD,
                        moduleName : 'Employee Universe',
                        selectedTeamInstance : component.get("v.selectedTeamInstance"),
                        selectedListView : component.get("v.listViewId"),
                        namespace : component.get("v.namespace"),
                        profileMap : component.get("v.profileMap")
                        
                    },
                    isredirect : true
                });
                evt.fire();*/
            break;



        }

        
       
    },
    
    handleHeaderAction : function (component,row,helper) {
        var actionName = event.getParam('action').name;
        var colDef = event.getParam('columnDefinition');
        var columns = cmp.get('v.columns');
        var activeFilter = cmp.get('v.activeFilter');

        if (actionName !== activeFilter) {
            var idx = columns.indexOf(colDef);
            var actions = columns[idx].actions;

            actions.forEach(function (action) {
                action.checked = action.name === actionName;
            });

            cmp.set('v.activeFilter', actionName);
            cmp.set('v.columns', columns);
        }
    },

    updateColumnSorting : function(cmp, event, helper){
        console.log('-- calling updateColumnSorting ---');
        console.log('event :',event);
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
       
        console.log('columnsArray[i] :',cmp.get("v.columns"));
        var colArray = cmp.get("v.columns");
        for (var i = 0; i < colArray.length; i++) {
            console.log('i :',colArray[i].label);
            if(colArray[i].fieldName == fieldName){
                console.log('-- col name match ---');
                cmp.set("v.empSortColumn", colArray[i].label);
                //cmp.set("v.sortBy", colArray[i].label);
                //fieldName = colArray[i].label;

            }
        }
        
        cmp.set("v.sortBy", fieldName);
        cmp.set("v.sortDirection", sortDirection);
        /*cmp.set("v.sortedBy", fieldName);
        cmp.set("v.sortedDirection", sortDirection);*/

        console.log('fieldName :'+fieldName);
        console.log('sortDirection :'+sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);

    },

    handleNext : function(component, event, helper) { 
        component.set("v.isLoading", true);
        var pageNumber = component.get("v.pageNumber");
        component.set("v.pageNumber", pageNumber+1);
        var startCount = 0;
        console.log('pagenumber :'+component.get("v.pageNumber"));
        if(component.get("v.pageNumber") != 1){
            var dataSet = component.get("v.dataTableWrapper.dataGridList");
            startCount = ((component.get("v.pageNumber")-1)*dataSet.length);
            console.log('startcount :'+startCount);
            component.set("v.startRecordCount", startCount+1);
        }else{
            startCount = 1;
            component.set("v.startRecordCount", startCount);
        }
        helper.getData(component, helper);
        component.set("v.isLoading", false);

    },
     
    handlePrev : function(component, event, helper) {        
        var pageNumber = component.get("v.pageNumber");
        component.set("v.pageNumber", pageNumber-1);
        var startCount = 0;
        console.log('pagenumber :'+component.get("v.pageNumber"));
        if(component.get("v.pageNumber") != 1){
            var dataSet = component.get("v.dataTableWrapper.dataGridList");
            startCount = ((component.get("v.pageNumber")-1)*dataSet.length);
            console.log('startcount :'+startCount);
            component.set("v.startRecordCount", startCount+1);
        }else{
            startCount = 1;
            component.set("v.startRecordCount", startCount);
        }
        helper.getData(component, helper);
    },

    loadMoreData : function(component,event,helper){

        console.log('-- loadMoreData ---');
        event.getSource().set("v.isLoading", true);
        component.set('v.loadMoreStatus', 'Loading');

        var pageNumber = component.get("v.pageNumber");
        component.set("v.pageNumber", pageNumber+1);
        var startCount = 1;
        console.log('pagenumber :'+component.get("v.pageNumber"));
        if(component.get("v.pageNumber") != 1){
            var dataSet = component.get("v.dataTableWrapper.dataGridList");
            startCount = ((component.get("v.pageNumber")-1)*dataSet.length);
            console.log('startcount :'+startCount);
            component.set("v.startRecordCount", startCount+1);
        }
        helper.getData(component, helper);

    }

    
});