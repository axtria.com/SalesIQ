({
   
   getData : function(component, event){

        var action = component.get("c.getPaginationData");
        var pageSize = component.get("v.pageSize").toString();
        var pageNumber = component.get("v.pageNumber").toString();

        var currentData = component.get("v.data");
         
        action.setParams({
            "pageNumber" : pageNumber,
            "pageSize" : pageSize,
            "objectName" : component.get("v.objectName"),
            "queryString" : component.get("v.queryString")
        });

        action.setCallback(this, function (response){
            if(response.getState() == 'SUCCESS'){
                var resultData = response.getReturnValue();
                if(resultData.length < component.get("v.pageSize")){
                    component.set("v.isLastPage", true);
                } else{
                    component.set("v.isLastPage", false);
                }
                var objectName = component.get("v.objectName");
                var columnsArray = component.get("v.columns");
                
                var dataSet = resultData;
                for(var row=0;row <dataSet.length; row++){
                    dataSet[row].Id = '/lightning/r/'+objectName+'/'+dataSet[row].Id +'/view';
                    for(var col = 0;col < columnsArray.length ; col++){
                        var field = columnsArray[col].fieldName;
                        
                        if(field != undefined){
                            var fieldValue = field.split('.')[0].replace('__r','__c');
                            if(columnsArray[col].type == 'url'  && field.indexOf('__r') != -1 && dataSet[row][fieldValue] != undefined)
                            {
                                console.log('-- inside if url---');
                                var url = window.location.origin;
                                dataSet[row][field] = url+'/'+dataSet[row][fieldValue];
                                var fieldTitle = columnsArray[col].typeAttributes.title;
                                dataSet[row][fieldTitle] = dataSet[row][fieldTitle.split('.')[0]][fieldTitle.split('.')[1]];
                                //columnsArray[col].typeAttributes.title = dataSet[row][fieldTitle.split('.')[0]][fieldTitle.split('.')[1]];
                                
                            }
                            if(columnsArray[col].type == 'date' && dataSet[row][field] != undefined){
                                console.log('-- inside if date---');
                                console.log(dataSet[row][field]);
                                var dateFormat = component.get("v.dateFormat");
                                
                                var dt = new Date(dataSet[row][field]);
                                                        
                                var offsetInMinutes = dt.getTimezoneOffset() ;
                                dt.setSeconds(dt.getSeconds() + (dt.getTimezoneOffset()*60));
                                var mystring = '';
                                if(component.get("v.dateFormat").indexOf('.') !== -1){
                                    mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate()).replace(/y+|Y+/,dt.getYear());
                                }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                                   mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate()).replace(/y+|Y+/,dt.getYear());;
                                }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                                    mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate()).replace(/y+|Y+/,dt.getYear());;
                                }

                                console.log('mystring :'+mystring);

                                dataSet[row][field] = mystring;
                            }
                        }
                        else{
                            console.log('--- hide manage assignment ---');
                            if(columnsArray[col].type == 'action'){
                                console.log('data' , columnsArray[col]);
                                var dataMap = component.get("v.dataTableWrapper.dataMap");
                                var menuActions = columnsArray[col].typeAttributes.rowActions;
                                console.log('menuActions :',menuActions);
                                for(var menu=0; menu < menuActions.length ;menu ++){
                                    var actionName = menuActions[menu].name;
                                    console.log('col :'+col);

                                    if(dataMap[row][col][actionName] == 'HIDE'){
                                        //columnsArray[col].typeAttributes.rowActions[menu] = [];
                                        columnsArray[col].typeAttributes.rowActions.splice(menu,1);
                                    }
                                    else if(dataMap[row][col][actionName] == 'DISABLE'){

                                    }
                                }

                            }
                        }
                        
                    }
                }
                component.set("v.dataSize", resultData.length);
                component.set("v.data", resultData);
                
                
                
            }


            console.log(event);
            console.log(component);

            component.set("v.loadTable", 'false');
            window.setTimeout(
                $A.getCallback(function() {
                    console.log('Calling'); 
                    var posActionEvent = component.getEvent("setPositionAction");
                    var editEmployeeHeader = 'Dynamic Style';
                    
                    posActionEvent.setParams({
                        "actionType": editEmployeeHeader,
                    });               

                    posActionEvent.fire();
                }), 200
            );
            
        });
        $A.enqueueAction(action);
   },
    /*
     * Show toast with provided params
     * */
    showToast : function(params){
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams(params);
            toastEvent.fire();
        } else{
            alert(params.message);
        }
    },

    /*
     * reload data table
     * */
    reloadDataTable : function(){
    var refreshEvent = $A.get("e.force:refreshView");
        if(refreshEvent){
            refreshEvent.fire();
        }
    },

    sortData: function (cmp, fieldName, sortDirection) {

        console.log('-- sortData ----');

        var data = cmp.get("v.data");
        var reverse = sortDirection !== 'asc';
        //sorts the rows based on the column header that's clicked
        data.sort(this.sortBy(fieldName, reverse));
        console.log('data :',data);
        cmp.set("v.data", data);
               
        
    },
    sortBy: function (fieldName, reverse, primer) {
        console.log('primer :'+primer);

        if(fieldName == 'Id'){
            fieldName = 'Name';
        }
        
        var key = primer ?
            function(x) {
                console.log('x:'+primer(x[fieldName]));
                return primer(x[fieldName])
            } :
        function(x) {return x[fieldName]};
        //checks if the two rows should switch places
        reverse = !reverse ? 1 : -1;
        console.log('reverse:'+reverse);
        return function (a, b) {
            var n1 = key(a);
            var n2 = key(b);
            console.log('n1 :'+n1 +', n2 :'+n2);
            if(n1 == undefined){
                n1 = '';
            }
            if(n2 == undefined){
                n2 = '';
            }
            if(n1 != undefined && n2 != undefined){
                if((n1 === false || n1 === true) &&(n2 === false || n2 === true)){
                    return a = n1.toString(), b = n2.toString(), reverse * ((a > b) - (b > a));
                }else{
                    if(n1.match(/^-{0,1}\d+$/) && n2.match(/^-{0,1}\d+$/) && ((n1 !== false || n1 !== true) && (n2 !== false || n2 !== true))){
                        return a = parseInt(key(a)), b = parseInt(key(b)), reverse * ((a > b) - (b > a));
                    }
                    else{
                        return a = n1, b = n2, reverse * ((n1 > n2) - (n2 > n1));
                    }
                }
            }
            
        }

        
    }
})