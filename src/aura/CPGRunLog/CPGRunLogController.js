({
    doInit : function(component, event, helper) {
      component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0 !important;}   .forceStyle .viewport.oneHeader.desktop {z-index:0 !important;} .forceStyle.desktop .viewport{overflow:hidden}");
      component.showSpinner();
      component.getDBdetail();
      var actionInstanceName = component.get("c.getInstanceNameAndURL");
      actionInstanceName.setCallback(this, function(response){ 
           var instanceName = response.getState();
           if(instanceName==="SUCCESS")
            {
                console.log('Inside RunLog');
                var instanceNameANdURL = response.getReturnValue();
                component.set("v.instanceName",instanceNameANdURL[0]);
                component.set("v.instanceURL",instanceNameANdURL[1]);
                //var url = 'http://35.164.63.126:5000/CGP_webservice/CGPRunLog/home/?scenario=a06f4000001a9JxAAI&context=brms_dev';
                var scenarioId = component.get('v.scenarioId');
                //var instance = (component.get("v.instanceName")).replace('/','');
                var instance = component.get('v.dbName');
                var instanceURL = (component.get('v.instanceURL'));
                console.log(instanceURL);
                //var url = instanceURL+'home/?scenario='+scenarioId+'&context='+instance;
                var url = instanceURL;
                console.log(url);
                var actionRunLog = component.get("c.getCalloutResponseContents");
                actionRunLog.setParams({ 
                    "url": url,
                    "scenarioId": scenarioId
 
              });
                
                 
                actionRunLog.setCallback(this, function(response){ 
                var state = response.getState();
                     if(state==="SUCCESS" && response.getReturnValue() != null)
                       {
                          component.hideSpinner();
                          var data = response.getReturnValue().CPGRunLog;
                          var errorMsg =  response.getReturnValue().Error; 
                          console.log(data);
                          var jsonRecieved = [];
                          var fieldsList = [];
                          var numberToBeFormatted = [];
                          if(data!=null){
                            for(var i=0;i<data.length;i++){
                              console.log(data[i].Status);
                              if(data[i].Fields!= undefined){ 
                              fieldsList.push(data[i].Fields);
                              }else{
                                fieldsList.push(0);
                              } 
                              if(data[i].Status=="Error Occured"){
                                console.log(data[i].Status);
                                data[i].Status = 'Failed'; 
                              }
                              console.log(data[i].final_status);
                             /* if(data[i].final_status=="Error Occured"){
                                 
                                 data[i].final_status = 'Failed';
                              }*/
                            }
                          }
                          
                          /* date format changes */
                          console.log(data);
                          console.log(fieldsList);
                          for(var y=0;y<fieldsList.length-1; y++){
                            for(var z=0;z<fieldsList[y].length; z++)
                            {                              
                              console.log(fieldsList[y][z].Status);
                              if(fieldsList[y][z].Status!="" && fieldsList[y][z].Status!='Failed'){
                              numberToBeFormatted.push(fieldsList[y][z].Status);
                            }
                             }
                           }


                            var actionFormat = component.get("c.getformatedNumbers");
                            actionFormat.setParams({
                                "numberVal": numberToBeFormatted,
                            });
                            actionFormat.setCallback(this,function(response)
                            {

                                 
                                var state = response.getState();
                                if(state === 'SUCCESS' && response.getReturnValue != null)
                                {
                                  var result1 = response.getReturnValue();
                                  console.log(result1);
                                  var counter=0;

                                  for(var y=0;y<fieldsList.length-1; y++){
                                    for(var z=0;z<fieldsList[y].length; z++)
                                      
                                    {                              
                                      //console.log(fieldsList[y][z].Status);
                                      console.log("result1 status"+data[y].Fields[z].Status);
                                      if(data[y].Fields[z].Status!="" && data[y].Fields[z].Status!="Failed"){
                                       
                                      data[y].Fields[z].Status = result1[counter];
                                      counter++;
                                    }
                                       
                                     }
                                   }
                                 
                                  console.log(data);
                                    console.log(errorMsg);
                                    if(data!=null){
                                      for(var i=0;i<data.length-1;i++){
                                        jsonRecieved.push(data[i]);
                                      }
                                    }
                                    var status = data[data.length-1].final_status;
                                    if(status === 'Success'){
                                      component.set('v.finalStatus',status);
                                    }else
                                    if(status === 'Error Occured'){
                                      component.set('v.finalStatus','Failed');
                                    }
                                    
                                    component.set('v.options',jsonRecieved);
                                    console.log(jsonRecieved);
                                   // console.log(jsonRecieved.callPlanDate);
                                    component.set("v.popoverMessage","Function Name-"+errorMsg.proc_name);
                                    component.set("v.popoverMessage2","Error- "+errorMsg.error_name);
                                    
                                     console.log(component.get("v.popoverMessage"));

                                }else{
                                   console.log("Failed with state:  "+ state);
                                }
                            
                              });
                             $A.enqueueAction(actionFormat);
                       }
                          else{
                            component.set("v.textMsg","PLEASE RUN THE SCENARIO");
                            console.log("Failed with state:  "+ state);
                            component.hideSpinner();
                            //component.showErrorToast();
                            //component.closeButton();
                       }
                 });
                $A.enqueueAction(actionRunLog);
            }
                else{
                  console.log("Failed with state:  "+ state);
            }
       });
       $A.enqueueAction(actionInstanceName);
     },

     showErrorToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        //var dsnotavailable = 'Execution Logs Not Available';
        var dsnotavailable;
        var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
        console.log('namespace val' +namespace);
        if(namespace != ''){
            
            dsnotavailable = $A.get("$Label.AxtriaSalesIQTM.Please_Run");

         }else{
            dsnotavailable = $A.get("$Label.c.Please_Run");
         }
        //var dsnotavailable = 'PLEASE RUN THE SCENARIO';
        toastEvent.setParams({
          message:dsnotavailable,
          messageTemplate: dsnotavailable,
          duration:'1000',
          key: 'info_alt',
          type: 'error',
          mode: 'pester'
        });
        toastEvent.fire();
      },
    carouselFirstTab:function(component, event, helper){
      var idx = event.currentTarget.getAttribute("data-id");
      console.log(event.currentTarget);
      var dataIndexing = document.getElementById(idx);
      $A.util.toggleClass(dataIndexing, 'slds-is-open');
      //var popover = component.find("popover");
        //$A.util.addClass(popover,'slds-hide');
    },

    closeButton : function(component, event, helper) {
        component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
        var evt = $A.get("e.force:navigateToComponent");
                   evt.setParams({
                      componentDef : "c:CPGbusinessRuleCanvas",
                      componentAttributes: {
                    record_Id : component.get("v.scenarioId"),
                    scenarioId : component.get("v.scenarioId"),
                    viewcanvas : "Y",
                    workSpaceId : component.get("v.workSpaceId")
                      }
                  });
                  evt.fire();
    },
    
    handleMouseEnter : function(component, event, helper) {
      var element = event.currentTarget.childNodes[0];
      $A.util.removeClass(element,'slds-hide');
    },
    handleMouseLeave : function(component, event, helper) {
      var element = event.currentTarget.childNodes[0];
      $A.util.addClass(element,'slds-hide');
    },
      showSpinner : function (component, event, helper) 
    {
        var spinner_container = component.find('spinner_container');
        $A.util.removeClass(spinner_container, "slds-hide"); 
    },

    hideSpinner : function (component, event, helper) 
    {
        var spinner_container =component.find('spinner_container');

        $A.util.addClass(spinner_container, "slds-hide");        
    },

    getDBdetail : function (component, event, helper){
        console.log("inside get DB details method");
        var action = component.get("c.getDBDetails");
                action.setCallback(this, function(response){ 
                    var state = response.getState();
                    if(component.isValid() && state==="SUCCESS"){
                        
                        var dbName = response.getReturnValue();
                        console.log('dbName');
                        console.log(dbName); 
                        component.set('v.dbName',dbName);
                    }
                    else{
                        console.log("Failed with state:  "+ state);
                    }
                })
                $A.enqueueAction(action);
    },
})