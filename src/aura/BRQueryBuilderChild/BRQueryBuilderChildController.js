({  
    doInit: function(component,event,helper)
    {
        console.log('child init');
        console.log(component.get("v.conditionWrapper"));
        var evt = component.getEvent("QBLoaded");  
            evt.setParams({ 
            "message" : "Loaded"});        
        evt.fire();
    },

    hideCriteriaDropdown :  function(component, event, helper){
        var globalId = component.getGlobalId();
        var container = document.getElementById(globalId + 'buttonContainer');
        var isExpandable = $A.util.hasClass(container, "slds-is-open");      
        if(isExpandable){            
            $A.util.removeClass(container, 'slds-is-open');
        }
    },

    markBlankInputs :  function(component, event, helper) {
        try{
            function markInvalidSelect(inputComponentAuraId){
                var allValidSelect = component.find(inputComponentAuraId).reduce(function (validSoFar, inputCmp) {

                    debugger;
                    if(inputCmp.get("v.value") == ""){
                        $A.util.addClass(inputCmp, 'slds-has-error'); 
                        return false;
                    }else{
                        $A.util.removeClass(inputCmp, 'slds-has-error'); 
                        return false;
                    }

                }, true);
            }        
            markInvalidSelect('inputFieldSelect');
        }catch(e){
            console.log(e);
        }

        try{
            function markInvalidText(inputComponentAuraId){
                var allValidText = component.find(inputComponentAuraId).reduce(function (validSoFar, inputCmp) {

                    debugger;
                    if(inputCmp.get("v.value") == ""){
                        $A.util.addClass(inputCmp, 'slds-has-error'); 
                        return false;
                    }else{
                        $A.util.removeClass(inputCmp, 'slds-has-error'); 
                        return false;
                    }

                }, true);
            }        
            markInvalidText('inputFieldText');
        }catch(e){
            console.log(e);
        }

        try{
            function markColumnValue(){
                    var columnCmp = component.find('inputFieldColumn');  
                   if(columnCmp.get("v.value") == ""){
                        $A.util.addClass(columnCmp, 'slds-has-error'); 
                        return false;
                    }else{
                        $A.util.removeClass(columnCmp, 'slds-has-error'); 
                        return false;
                    }
            }
            markColumnValue();
        }catch(e){
            console.log(e);
        }
        
    }, 


    clearError :  function(component, event, helper) {
        try{
            var allValid = component.find('inputFieldText').reduce(function (validSoFar, inputCmp) {
            //inputCmp.focus();
            console.log(inputCmp.get("v.value"));
            if(inputCmp.get("v.value") == ""){
                    var errMessage = $A.get("$Label.c.Field_Value_Blank");
                    inputCmp.set("v.validity",true);
                //inputCmp.setCustomValidity(errMessage);
                inputCmp.reportValidity();
            }else{
                 inputCmp.set("v.validity",true);
                //inputCmp.setCustomValidity(errMessage);
                inputCmp.reportValidity();
            }


            }, true);
        }catch(e){
            console.log(e);
        }
        
    }, 
    showCriteriaDropdown :  function(component, event, helper) {
        console.log('-- showCriteriaDropdown ---')
        var globalId = component.getGlobalId();
        var container = document.getElementById(globalId + 'buttonContainer');
        var isExpandable = $A.util.hasClass(container, "slds-is-open");    
        console.log('globalId -'+globalId);
        console.log('container -'+container);
        console.log('isExpandable -'+isExpandable);  
        if(!isExpandable){            
            $A.util.addClass(container, 'slds-is-open'); 
            event.stopPropagation();
        }
        if(component.get("v.moduleName") == 'Filter Position'){
            if(isExpandable){  
                $A.util.removeClass(container,'slds-is-open'); 
            }

        }

    },    

    hideShow : function(component, event) {
        var globalId = component.getGlobalId();
        var container = document.getElementById(globalId + 'buttonContainer');
        
        var isExpandable = $A.util.hasClass(container, "slds-is-open");      
        if(isExpandable){            
            $A.util.removeClass(container, 'slds-is-open');
        }
        else{            
            $A.util.addClass(container, 'slds-is-open'); 
        }
    },

    addAGroup : function(component, event, helper){
        var action = component.get('c.addInnerGroup');
        console.log('scenarioRuleInstanceId---->'+component.get("v.scenarioRuleInstanceId"));
        action.setParams({
            "filterWrap" : JSON.stringify(component.get("v.conditionWrapper"))
        });
        action.setCallback(this, function(data){
            var response= data.getReturnValue();
            console.log('--response ', JSON.parse(JSON.stringify(response)));
            component.set('v.conditionWrapper',response);
        });
        $A.enqueueAction(action);       
    },

    addCriteria : function(component, event, helper){
        var action = component.get('c.addInnerCriteria');
        action.setParams({
            "filterWrap" : JSON.stringify(component.get("v.conditionWrapper"))
        });

        action.setCallback(this, function(data){
            var response = data.getReturnValue();
            console.log('--response ', JSON.parse(JSON.stringify(response)));
            component.set('v.conditionWrapper', response); 
        });
        $A.enqueueAction(action);
    },

    deleteCriteria : function(component, event, helper){
        var indx = event.currentTarget.getAttribute("data-recId");
        var action = component.get('c.deleteInnerCriteria');
        action.setParams({
            "filterWrapper" :JSON.stringify(component.get("v.conditionWrapper")),
            "idx" : indx
        });
        action.setCallback(this, function(data){
            var response= data.getReturnValue();
            component.set('v.conditionWrapper',response);
            console.log(response);
        });
        $A.enqueueAction(action);   
    },

    deleteGroup : function(component, event, helper){
        component.set('v.conditionWrapper', null);
        
        var deleteGroupCriteria = component.getEvent('deleteGroupCriteria');
        deleteGroupCriteria.fire();

        component.destroy();
    },

    handleDeleteGroup: function(component, event, helper){
        if(component.get("v.conditionWrapper") != null) {
            var action = component.get('c.deleteInnerGroup');
            action.setParams({
                "filterWrapper" :JSON.stringify(component.get("v.conditionWrapper"))
            });
            action.setCallback(this, function(response){
                var state = response.getState();
                if(component.isValid() && state==="SUCCESS")
                { 
                    component.set('v.conditionWrapper',response.getReturnValue());
                    console.log(response);
                } else {
                    console.log("Failed :  "+ response);
                }
            });
            $A.enqueueAction(action); 
            event.stopPropagation();
        }
    },

    clickedAND : function(component, event, helper){

        var filterWrapper = JSON.parse(JSON.stringify(component.get("v.conditionWrapper")));
        var btnOR = document.getElementById(component.getGlobalId() + "_orButton");
        var btnAnd = document.getElementById(component.getGlobalId() + "_andButton");
        $A.util.removeClass(btnOR,'slds-is-selected');
        $A.util.addClass(btnAnd,'slds-is-selected');

        filterWrapper.bindClause = 'AND';
        component.set("v.conditionWrapper", filterWrapper);
    },
    clickedOR : function(component, event, helper){
        var filterWrapper = JSON.parse(JSON.stringify(component.get("v.conditionWrapper")));
        var btnOR = document.getElementById(component.getGlobalId() + "_orButton");
        var btnAnd = document.getElementById(component.getGlobalId() + "_andButton");
        $A.util.removeClass(btnAnd,'slds-is-selected');
        $A.util.addClass(btnOR,'slds-is-selected');

        filterWrapper.bindClause = 'OR';
        component.set("v.conditionWrapper", filterWrapper);
    },

    onColumnChange : function(component, event, helper){

        console.log('init filter wrapper');
        console.log('-- ',component.get("v.listOfOperators"));
        var colDataType;
        var valueType;
        var filterWrapper = JSON.parse(JSON.stringify(component.get("v.conditionWrapper")));
        console.log(event.getSource());
        var selectedColumn = event.getSource().get("v.value");
        console.log(selectedColumn+'selectedColumn');
        var arrColumns = component.get("v.columnNames");
        var columnClass = event.getSource().get("v.class");
        columnClass = columnClass.replace('column_','');

        console.log('filterWrapper');
        console.log(filterWrapper);

        for(var i=0;i<arrColumns.length;i++){
            if(arrColumns[i].tableColumnName==selectedColumn){
                colDataType = (arrColumns[i].colDatatype).toLowerCase();
                valueType = arrColumns[i].colValuetype;
                break;
            }
        }
        
        filterWrapper.criterias[columnClass].operator = '' ;
        filterWrapper.criterias[columnClass].dataType = colDataType ;
        filterWrapper.criterias[columnClass].colValueType = valueType;
        component.set("v.conditionWrapper", filterWrapper);

        console.log('condition wrapper');
        console.log(filterWrapper);
        var evt = component.getEvent("refreshPicklist");          
        evt.fire();
        // var optionsIndex = filterWrapper.criterias.length - 1;

        // var scrId = component.get("v.scenarioRuleInstanceId");
        // var action = component.get('c.getInstanceNameAndURL');


        // console.log('scenarioid == ' + scrId)

        // action.setParams({
        //     "scenarioRuleInstanceId" : scrId
        // });

        // action.setCallback(this, function(response){
        //     var state = response.getState();
        //     if(component.isValid() && state==="SUCCESS")
        //     { 
        //         var result = response.getReturnValue();
        //         var finalURL = result[0]+'?columns='+selectedColumn+'&'+'tableName='+result[1]+'&'+'dbname='+result[2].replace('/','')+'&'+'where_clause='+'';
                

        //         console.log('finalURL :' + finalURL);
        //         //var actionColumnValues = component.get('c.getColumnValue');
        //         var actionColumnValues = component.get('c.getColumnValues');

        //         actionColumnValues.setParams({
        //             "url" :finalURL
        //         });

        //         actionColumnValues.setCallback(this, function(response){
        //             var state = response.getState();
        //             if(component.isValid() && state==="SUCCESS")
        //             { 
        //                 var result = response.getReturnValue();
        //                 console.log('result'+ result);
        //                 console.log(result);

        //                 var jsonOptions= JSON.parse(result);
        //                 //console.log(jsonValues);
        //                 var options_ = [];
        //                 options_ = Object.values(jsonOptions)[0].sort();

        //                 var optionArray2_ = component.get('v.optionValues');
        //                 console.log('optionArray2_');
                       
        //                 optionArray2_[optionsIndex] = options_;
        //                 component.set('v.optionValues',optionArray2_);
        //             } else {
        //                 console.log("failed In getting values");
        //             }
        //         });
        //         $A.enqueueAction(actionColumnValues);
        //     } else {
        //         console.log("failed");
        //     }
        // });
        // $A.enqueueAction(action);
    },

    onOperatorChange : function(component, event, helper)
    {
        // component.initChild();
        console.log(' ---- onOperatorChange ----');
        console.log(' optionvalues - ',component.get("v.optionValues"));
        var filterWrapper = JSON.parse(JSON.stringify(component.get("v.conditionWrapper")));
        console.log('filterWrapper - ',filterWrapper);
        var operatorClass = event.getSource().get("v.class");
        console.log('operatorClass - '+operatorClass);
        operatorClass = operatorClass.replace('operator_','');
        filterWrapper.criterias[operatorClass].value = '' ;
        filterWrapper.criterias[operatorClass].anotherValue = '' ;
        console.log('filterWrapper 2: ',filterWrapper);
        component.set( "v.conditionWrapper", filterWrapper );
    },

})