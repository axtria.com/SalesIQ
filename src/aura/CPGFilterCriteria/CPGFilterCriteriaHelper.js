({ 
    doValidation:function(component, event, helper){
        
        var errMessage=component.get("v.errMessage");
        var isValid = component.get("v.isValid");
        if(!isValid || isValid=='false'){
            component.set('v.popoverMessage1',errMessage);
            var errorIcn = component.find("errorIcon1");
            $A.util.removeClass(errorIcn,'slds-hide');
        }

        // ------ Validation code block to execute on NEXT Tab click -----
        var ruleName = component.find('ruleName').get('v.value');
        if(ruleName == undefined || ruleName.trim()==''){
            component.set("v.isValid",false);
            component.set('v.popoverMessage1','Please enter rule name');
            var errorIcn = component.find("errorIcon1");
            $A.util.removeClass(errorIcn,'slds-hide');
        }
        
        if(component.get("v.isValid")=='true'){
            console.log('next');
            var rowFilterTabLink = component.find("RowFilterAuraId");
            var fieldsToIncludeTabLink = component.find("fieldsToIncludeAuraId");
            var rowFilterRuleDataTab = component.find("RowFilterId");
            var fieldsToIncludeDataTab = component.find("FieldsToIncludeId");
            
            $A.util.removeClass(rowFilterTabLink, 'slds-is-active');
            $A.util.addClass(fieldsToIncludeTabLink, 'slds-is-active');  
            $A.util.removeClass(rowFilterRuleDataTab, 'slds-show');
            $A.util.addClass(rowFilterRuleDataTab, 'slds-hide');
            $A.util.removeClass(fieldsToIncludeDataTab, 'slds-hide_frame'); 
            $A.util.addClass(fieldsToIncludeDataTab, 'slds-show');
            var isDataTablecalled=component.get("v.isDataTablecalled");
            if(!isDataTablecalled){
               component.createDataTablecall();
               isDataTablecalled=true; 
               component.set("v.isDataTablecalled",isDataTablecalled);
            }
            
            var RowFilterRulesFooter = component.find("RowFilterRulesFooter");
            $A.util.addClass(RowFilterRulesFooter, 'slds-hide');
            var FieldsToIncludeFooter = component.find("FieldsToIncludeFooter");
            $A.util.removeClass(FieldsToIncludeFooter, 'slds-hide');
        }else{
            return;
        }
    }
})