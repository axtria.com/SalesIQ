({
    initPage : function(component, event, helper){                 
        //Fill Rules and data types
        // component.showSpinner();
        console.log('==log ruleid==');
        console.log(component.get("v.scenarioRuleInstanceId"));

        component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0 !important;}   .forceStyle .viewport.oneHeader.desktop {z-index:0 !important;} .forceStyle.desktop .viewport{overflow:hidden}");
        var actionBusinessRuleId = component.get("c.getBusinessRuleId");
        actionBusinessRuleId.setParams({ "scenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
        
        actionBusinessRuleId.setCallback(this, function(data)
        {
            var response= data.getReturnValue();  
            console.log(response);
            component.set('v.businessRuleId',response);
            if(component.get("v.businessRuleId") != ''){
                component.getExistingRule();
            }else{
                var childComponent = component.find("queryBuilder");
                childComponent.init();
            }
                            
            var action2=component.get('c.getColumns');
            action2.setParams({ 
                "scenarioId" : component.get("v.scenarioRuleInstanceId"),
            });
            action2.setCallback(this, function(data){
                var response= data.getReturnValue(); 
                component.set('v.ColumnWrapper',response);
                component.getTableName();
            })
                        
           $A.enqueueAction(action2); 
        });
        
        $A.enqueueAction(actionBusinessRuleId);
    },  

    getTableName : function(component, event, helper){ 
        var actionInpTableName = component.get("c.getInputTableName");
        actionInpTableName.setParams({ 
            "scenarioInstanceId" : component.get("v.scenarioRuleInstanceId")
        });
        actionInpTableName.setCallback(this, function(response){
             var state = response.getState();
             if(component.isValid() && state==="SUCCESS")
             {
                var itemsSample = response.getReturnValue();  
                component.set('v.inputTableName',itemsSample);
                
            }else{
                console.log("Failed with state:  "+ state);
            }
        });
        $A.enqueueAction(actionInpTableName);

        var actionOutTableName = component.get("c.getOutputTableName");
        actionOutTableName.setParams({ 
            "scenarioInstanceId" : component.get("v.scenarioRuleInstanceId")
        });
        actionOutTableName.setCallback(this, function(data){
            var response= data.getReturnValue();  
            component.set('v.outputTableName',response);
            //component.hideSpinner();
        });
        $A.enqueueAction(actionOutTableName);
    },

    closeButton : function(component, event, helper) {
        console.log('Inside close');
        component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
        var action = component.get("c.getScenarioID");
        action.setParams({ "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});

        action.setCallback(this, function(response){ 
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS"){
                var arrListValues=response.getReturnValue();

                var evt = $A.get("e.force:navigateToComponent");
                evt.setParams({
                    componentDef : "c:CPGbusinessRuleCanvas",
                    componentAttributes: {
                        record_Id : arrListValues[0][common.getKey(component,"Scenario_Id__c")],
                        viewcanvas : "Y",
                        workSpaceId : component.get("v.workSpaceId"),
                        summarizedViewFlag : component.get("v.summarizedViewFlag")
                    }
                });
                evt.fire();
                    
            }
            else{
                console.log("Failed with state:  "+ state);
            }
        })
        $A.enqueueAction(action);
    },

    NextTab : function(component, event, helper) {
        var childComponent = component.find("queryBuilder");
        childComponent.getQuery();
    },

    updateColumnWrapper:function(component,event,helper){
        var $j = jQuery.noConflict();
        var columnWrapper=JSON.parse(JSON.stringify(component.get('v.ColumnWrapper')));
          
        $j("#table-1 > tbody > tr").each(function(index){
            for(var ctr=0;ctr<columnWrapper.length;ctr++){

                if(columnWrapper[ctr].Name==$j(this).find("td:eq(1)").text() && 
                    $j(this).find("input:checkbox").prop("checked")==true){
                    columnWrapper[ctr].statusFlag=true;
                    break;
                }
                else if(columnWrapper[ctr].Name==$j(this).find("td:eq(1)").text() && $j(this).find("input:checkbox").prop("checked")==false){
                        columnWrapper[ctr].statusFlag=false;
                        break;
                }
            }
        });

        component.set('v.ColumnWrapper',columnWrapper);
    },

    saveAndUpdate : function(component, event, helper){
        console.log('Inside saveAndUpdate');
        console.log(component.get("v.parsedJSONQuery"));
        console.log(component.get("v.parsedQuery"));
        var ruleName = component.find('ruleName').get('v.value');
        var scenarioRuleInstanceId = component.get('v.scenarioRuleInstanceId');
        var ruleId  = component.get("v.businessRuleId");
        var componentId = component.get('v.componentTypeMasterId');

        component.updateColumnWrapper();
        var columndata=component.get('v.ColumnWrapper');
        console.log(columndata);

        var action = component.get('c.saveUpdate');        
        action.setParams({ 
            'ruleName' : ruleName,            
            'parsedJson': component.get("v.parsedJSONQuery"),
            'finalQuery': component.get("v.parsedQuery"),
            'scenarioRuleInstanceId': scenarioRuleInstanceId,
            'componentId' : componentId,          
            'columndata': JSON.stringify(columndata),
            'stagingQuery' : component.get("v.stagingQuery")
        });
        action.setCallback(this, function(data){
            var state= data.getState();
            console.log(state);
            if(component.isValid() && state==="SUCCESS"){
                var response= data.getReturnValue();
                console.log(response);   
                component.closeButton();            
            }
        });
        
        $A.enqueueAction(action);
    },

    restrictInputsRule :function(component, event, helper){
      console.log('restrictInputsRule');
      var text=component.find("ruleName");
      var ruleName = text.get('v.value');
      var newtext = ruleName.replace(/(?!^-)[^0-9a-zA-Z_\s]/g, ''); 
      text.set('v.value', newtext);
   
    },

    getExistingRule:function(component, event, helper) {
        var actionRuleName = component.get("c.getRuleName");
        actionRuleName.setParams({ 
            "scenarioId" : component.get("v.scenarioRuleInstanceId"),
        });
        actionRuleName.setCallback(this, function(data){
            var response= data.getReturnValue(); 
            console.log(response);   
            if(response != null) {              
                component.find('ruleName').set('v.value',response[0]);
                component.set("v.parsedJSONQuery",response[1]);
                component.set("v.parsedQuery",response[2]);
            }
            var childComponent = component.find("queryBuilder");
            childComponent.init();

        })
        $A.enqueueAction(actionRuleName);
    },

    createDataTable : function(component, event, helper)
    {
        
        var opts = [];
        var resultColumnWrapper = component.get("v.ColumnWrapper");
        for(var i=0;i<resultColumnWrapper.length;i++){
            opts.push({Id:resultColumnWrapper[i].Id,statusFlag:resultColumnWrapper[i].statusFlag,Name:resultColumnWrapper[i].Name,variabletype:resultColumnWrapper[i].variabletype,colDatatype:resultColumnWrapper[i].colDatatype,colValuetype:resultColumnWrapper[i].colValuetype});
        }    
        if(opts.length>0){
            var sessionTable;
            var $j = jQuery.noConflict();
            var arrHtml=[];
            var result = opts;
            var coulmnJsonResult = [];

            var element = result[0];
            for(var key in element){
                if(key=='statusFlag'){
                    coulmnJsonResult.push({"data":key,"title":'',defaultContent:''});
                }else if(key=='Name'){
                    coulmnJsonResult.push({"data":key,"title":'Name',defaultContent:''});
                }else if(key=='variabletype'){
                    coulmnJsonResult.push({"data":key,"title":'Type',defaultContent:''});
                }else if(key=='colDatatype'){
                    coulmnJsonResult.push({"data":key,"title":'Data Type',defaultContent:''});
                }else if(key=='colValuetype'){
                    coulmnJsonResult.push({"data":key,"title":'Value Type',defaultContent:''});
                }
                

                
            }
            console.log(result);
            sessionTable = $j('.fieldsIncludeTab').DataTable(
            {
                
                "search": false,
                 destroy : true,
                 stateSave: true,
                "processing": false,
                "dom": '<"div-tbl">t<"bottom-info"> ', // f search, p :- pagination , l:- page length 
                "data": result,
                 paging:false,
                 "ordering": true,
                 
                scrollX : true,
                 
                scrollY:     "325px",
               
                scrollCollapse: true,

                "language":
                {
                    "emptyTable": "Loading Data"
                },
               
                columns:coulmnJsonResult,
                columnDefs :
                [
                            { "orderable": false, "targets": [0,2,3,4]  },
                            {
                                targets : 0,
                                'searchable': false,
                                'orderable': false,
                                "createdCell":function (td, cellData, rowData, row, col)
                                {   
                                     //console.log(rowData);
                                      var parsehtml;
                                   //   if(rowData.statusFlag == true){
                                       parsehtml="<div class='slds-form-element'><div class='slds-form-element__control'><span class='slds-checkbox'><input type='checkbox' name='select_all' id='"+rowData.Id+"' checked value='" + rowData.Name + "' disabled='true'/><label class='slds-checkbox__label' for='"+rowData.Id+"'><span class='slds-checkbox_faux'></span></label></span></div></div>"; 
                                       //html="<input type='checkbox' name='options' id='' checked value='" + rowData.statusFlag + "' />"; 
                                     // }else{
                                     //  	parsehtml="<div class='slds-form-element'><div class='slds-form-element__control'><span class='slds-checkbox'><input type='checkbox' name='select_all' id='"+rowData.Id+"'  value='" + rowData.Name + "' disabled='true'/><label class='slds-checkbox__label' for='"+rowData.Id+"'><span class='slds-checkbox_faux'></span></label></span></div></div>";
                                        //html="<input type='checkbox' name='options' id=''  value='" + rowData.statusFlag + "' />";
                                     // }
                                      
                                    $j(td).html(parsehtml);
                                }
                            }
                          ],
                
                "createdRow": function ( row, data, index ) 
                {
                      $j(row).addClass('slds-hint-parent');
                      $j('td', row).addClass('slds-text-align_left slds-truncate');
                      $j('thead > tr> th').addClass('slds-is-sortable slds-is-resizable slds-text-title_caps slds-line-height_reset sorting');
                      $j('thead > tr').addClass('slds-text-title_caps ');
                      
                },

                
            });
            component.set("v.sessionTable",sessionTable);
        
        } else {
            var tableVisibility = component.find("DataTableId");
            $A.util.addClass(tableVisibility,'slds-hide');
            component.set("v.textMsg","No Data Found");
        }
    },
    
    RowFilterTab : function(component, event, helper) {
 
        var tab2 = component.find('RowFilterAuraId');
        var TabTwoData = component.find('RowFilterId');
        var tab3 = component.find('fieldsToIncludeAuraId');
        var TabThreeData = component.find('FieldsToIncludeId');

        $A.util.addClass(tab2, 'slds-is-active');
        $A.util.removeClass(TabTwoData, 'slds-hide');
        $A.util.addClass(TabTwoData, 'slds-show');
        // Hide and deactivate others tab
 
        $A.util.removeClass(tab3, 'slds-is-active');
        $A.util.removeClass(TabThreeData, 'slds-show');
        $A.util.addClass(TabThreeData, 'slds-hide_frame');
        
        var RowFilterRulesFooter = component.find("RowFilterRulesFooter");
        $A.util.removeClass(RowFilterRulesFooter, 'slds-hide');
        var FieldsToIncludeFooter = component.find("FieldsToIncludeFooter");
        $A.util.addClass(FieldsToIncludeFooter, 'slds-hide');
    },

    
    handleMouseEnter : function(component, event, helper) {
        var popover = component.find("popover");
        $A.util.removeClass(popover,'slds-hide');
    },
       //make a mouse leave handler here
    handleMouseLeave : function(component, event, helper) {
        var popover = component.find("popover");
        $A.util.addClass(popover,'slds-hide');
    },
    handleMouseEnter1 : function(component, event, helper) {
        var popover = component.find("popover1");
        $A.util.removeClass(popover,'slds-hide');
    },
       //make a mouse leave handler here
    handleMouseLeave1 : function(component, event, helper) {
        var popover = component.find("popover1");
        $A.util.addClass(popover,'slds-hide');
    },
           // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
    destoryCmp : function (component, event, helper) {
        component.destroy();
    },
    
    updateFilterWrapper : function(component, event, helper) {
        var updatedWrapper = event.getParam("parsedJson");
        var whereClause = event.getParam("whereClause");
        var isValid = event.getParam("isValid");
        var errMsg = event.getParam("errorMsg");
        var stagingClause = event.getParam("stagingWhereClause");
        console.log(updatedWrapper);
        console.log(isValid);

        // set the handler attributes based on event data
        component.set("v.parsedJSONQuery", updatedWrapper);
        component.set("v.isValid", isValid);
        component.set("v.errMessage", errMsg);
        component.set("v.parsedQuery", whereClause);
        component.set("v.stagingQuery", stagingClause);

        helper.doValidation(component, event, helper);
    }
})