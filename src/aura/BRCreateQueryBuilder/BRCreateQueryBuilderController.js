({
afterloadscript:function(component,event,helper){
		component.getBuilderData();
		
},

handleAddGroup:function(component,event,helper){
	console.log("Add group");
	
		
},
getBuilderData : function(component,event,helper)
{

	var existingblock = component.get("v.wrapperBlock");
	console.log(existingblock);
	
	var $j = jQuery.noConflict();
	console.log('in get builder');

	var idB = component.get('v.index');
     var filterJson = [];
     var columnDetails = component.get('v.columnDetailsresponse');
     for(var i=0;i<columnDetails.length;i++){
     	if(columnDetails[i].datatype__c=='Numeric'){
			filterJson.push({id:columnDetails[i].tb_col_nm__c,label:columnDetails[i].tb_col_nm__c,type:'double',operators: ['equal', 'not_equal','is_null','between','is_not_null','less','less_or_equal','greater','greater_or_equal'],values:{}});
    	 }else{
    	 	filterJson.push({id:columnDetails[i].tb_col_nm__c,label:columnDetails[i].tb_col_nm__c,type:'string',operators: ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null','contains','not_contains','begins_with','ends_with'],value:{}});
    	 }

     }
     console.log('**JSON**');
     console.log(filterJson);
     //Check If existing rule

     var rules_basic;
     var checkExistingRule = component.get('v.checkRuleExists');
     console.log(checkExistingRule);
     var isAddbuttonClicked = component.get('v.addButtonClicked');
     if(checkExistingRule==false)
     {
     	console.log('in if-->>');
     	console.log(filterJson);
     	console.log(rules_basic);
     		$j('#group_'+idB).queryBuilder('destroy');
	     	$j('#group_'+idB).queryBuilder({
	     	filters: filterJson,
		    rules: rules_basic
	 	});
     }else
     {
     	
     	console.log('in else-->>');
     	console.log(filterJson);
     	console.log(rules_basic);
     	if(isAddbuttonClicked=== undefined || isAddbuttonClicked===false){
     		rules_basic = JSON.parse(existingblock.criteriaLogic);
     		$j('#group_'+idB).queryBuilder('destroy');
	     	$j('#group_'+idB).queryBuilder({
	     	filters: filterJson,
		    rules: rules_basic
		 	}); 
     	}else{
     		$j('#group_'+idB).queryBuilder('destroy');
	     	$j('#group_'+idB).queryBuilder({
	     	filters: filterJson
	     	});
     	}
     	
     }
	 
	 
},

handleDisplayButton: function(component,event,helper){
	var $j = jQuery.noConflict();
	var idB = component.get('v.index');
	var isButtonDisplay = event.getParam("isdisplayButton");
	var sqlexpression;
	var blockJson;
	console.log('--button pressed--->'+isButtonDisplay);
	component.set('v.isDisplayPressed',isButtonDisplay);
	var isbuttonPressed = component.get('v.isDisplayPressed');
    if(isbuttonPressed){
     	var result = $j('#group_'+idB).queryBuilder('getSQL');
     	if (result.sql.length) {
	    sqlexpression =  result.sql;
	    component.set('v.sqlExpression',sqlexpression);
	    blockJson = $j('#group_'+idB).queryBuilder('getRules');
	  }
    }
     var wrapBlock= component.get('v.wrapperBlock'); 
     wrapBlock.criteriaLogic = JSON.stringify(blockJson, null, 2);
     wrapBlock.executionLogic = sqlexpression;
     
     
},


})