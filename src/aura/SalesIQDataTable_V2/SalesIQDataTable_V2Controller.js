({
    closeDropDown : function(component,event,helper)
    {
        if(component.get("v.showPositionHierarchy") == 'true') 
        { 
            var positionhierarchy = component.find('positionhierarchy');
            positionhierarchy.closeTeamDropDown();
        }  
        if(component.get("v.showTreeHierarchy") == 'true') 
        {
            var treeHierarchy = component.find('treeHierarchy');
            treeHierarchy.closeTree();
        }
    },

    initialize : function(component,event,helper)
    {

         //get org profile map 
        var profileAction = component.get("c.getProfileNameMap");
        profileAction.setCallback(this,function(response){
            console.log(response.getReturnValue());
            var profileMap = response.getReturnValue();
            
            Object.keys(profileMap).forEach(function(key) {
                console.log(key, profileMap[key]);
                var profileKey = profileMap[key];
                for (var i = 0; i < profileMap[key].length; i++) {
                    var temp = profileMap[key][i].split(' ').join('_');
                    var profileName = "$Label.c." + temp;
                    //console.log('profileName : '+profileName);
                    component.set("v.tempProfileName", $A.getReference(profileName));
                    profileKey[i] = component.get("v.tempProfileName");
                }
            });

            console.log('---- profile map ----');
            console.log(profileMap);
            component.set("v.profileMap", profileMap);
            
        });
        $A.enqueueAction(profileAction);

        component.getNamespace();

        /*var dependencyConditions = component.get("c.getDependencyControlData");
        dependencyConditions.setParams({
           teamInstanceId : component.get("v.selectedTeamInstance")
        });

        dependencyConditions.setCallback(this,function(response)
        {
            if(response.getState() === 'SUCCESS' && response.getReturnValue() != null)
            {
                component.set("v.dependencyControlConditions",response.getReturnValue()); 
                //component.checkDependencyConditionsOnLoad(colStructureHeaderDetailJson);
                component.set("v.executeSortingEvent",'true');
            }
            //component.hideSpin();
        });
        $A.enqueueAction(dependencyConditions);*/

        if(component.get("v.moduleName") != 'Call Plan')
        {
            component.initializeDataTable();
        }
    },

    getNamespace : function(component,event,helper)
    {
        var action = component.get("c.getOrgNamespace");

        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var ns = response.getReturnValue();
                component.set("v.namespace",ns);
            }    
        });

        action.setBackground();
        $A.enqueueAction(action);   
    },

    nodeSelectedClick: function(component,event,helper)
    {
        var selectedPosition = event.getParam("selectedTreeNode");
        console.log('--nodeSelectedClick ');
        console.log(selectedPosition);
        
        component.set("v.selectedPosition",selectedPosition);
        component.createDataTable();

        var evt = component.getEvent("refreshKpiEvent");
        evt.setParams({
            teamInstanceId : component.get("v.selectedTeamInstance"),
            positionId : component.get("v.selectedPosition"),
            crType : 'Call_Plan_Change'
        });           
        evt.fire();

        if(component.get("v.showPositionHierarchy") == "true") {    
            var positionhierarchy = component.find('positionhierarchy');
            positionhierarchy.openBreadcrumb();
            positionhierarchy.checkboxSelect(selectedPosition);
        }
    },

    showSpinner : function (component, event, helper) 
    {
        var spinner_container = component.find('spinner_container');
        $A.util.removeClass(spinner_container, "slds-hide"); 
    },

    hideSpinner : function (component, event, helper) 
    {
        var spinner_container =component.find('spinner_container');
        $A.util.addClass(spinner_container, "slds-hide");        
    },

    teamInstanceChange : function(component, event, helper){
        if(event.getParam("teamInstanceId") != null)
            component.set("v.selectedTeamInstance", event.getParam("teamInstanceId"));

        if(event.getParam("countryId") != null)
            component.set("v.countryId", event.getParam("countryId"));

        var $j = jQuery.noConflict();
        var globalId = component.getGlobalId();
        if($j(document.getElementById(globalId+'_datatableParentDiv')).height() > 0)
        {
            console.log('team instance change--->');
            $j(document.getElementById(globalId+'_dataTable')).DataTable().destroy();
            $j(document.getElementById(globalId+'_dataTable')).remove();
            $j(document.getElementById(globalId+'_tableContainer')).append("<table id='"+globalId+"_dataTable' class='slds-table slds-table_bordered slds-table_resizable-cols slds-table_fixed-layout' style='width:100%;'> </table>");
            
            // set the selected position to blank on change of team instance
            component.set("v.selectedPosition", '');
            component.initializeDataTable(component, event, helper);
        }
        
    },

    initializeDataTable: function(component, event, helper) {
        component.showSpin();

        console.log('called init : datatable_v2 :'+ component.get("v.countryId"));
        var ObjectDetailWrapperList = component.get("v.ObjectDetailWrapperList");
        console.log(ObjectDetailWrapperList);
        var globalId = component.getGlobalId();
        if(component.get("v.moduleName") == 'Position Employee Grid' && ObjectDetailWrapperList.length == 0)
        {
            var card = component.find('no_record_card');
            var dataTableComponent = component.find('jquery-dataTable');
            
            document.getElementById(globalId+'_tableContainer').style.display = 'none';
            $A.util.removeClass(card, "slds-hide");
            component.set("v.showSearchBar", false);
            component.hideSpin();
            return;
        }
        /*var country = component.get("v.countryId");
        if(country == '' || country == null || country == undefined){
            console.log("country id is not present : datatable_v2");
            component.showSpin();
            return;
        }*/

        /*var regex = new RegExp('[; ]'+'apex__CountryID'+'=([^\\s;]*)');
        var cookieValue = (' '+document.cookie).match(regex);
        if(cookieValue != null) {
            // in case cookie found is null, we expect coming it down from the parent component
            var cVal = (cookieValue!=null || cookieValue!= undefined)?unescape(cookieValue[1]):'';
            component.set("v.countryId",cVal);
        }*/
    
        var getColumnsHeaderJSON = component.get("c.initialiseDataTable");
        getColumnsHeaderJSON.setParams({
            "moduleName": component.get("v.moduleName"),
            "selectedTeamInstance" : component.get("v.selectedTeamInstance"),
            "selectedPositionId" : component.get("v.selectedPosition"),
            "subInterface" : component.get("v.subInterface"),
            "recordId" : component.get("v.recordId"),
            "countryId" : component.get("v.countryId")
        });

        getColumnsHeaderJSON.setCallback(this,function(response)
        {
            if(response.getState() === 'SUCCESS' && response.getReturnValue() != null)
            {
                var columnsHeaderResponse = JSON.parse(response.getReturnValue());

                console.log('--columnsHeaderResponse');
                console.log(columnsHeaderResponse);

                component.set("v.columnStructure", columnsHeaderResponse);
                component.set("v.selectedTeamInstance", columnsHeaderResponse.selectedTeamInstanceId);
                component.set("v.selectedTeamInstanceStartDate", columnsHeaderResponse.selectedTeamInstanceStartDate);
                component.set("v.selectedTeamInstanceEndDate", columnsHeaderResponse.selectedTeamInstanceEndDate);
                component.set("v.selectedPosition", columnsHeaderResponse.selectedPositionId);
                component.set("v.scenarioID", columnsHeaderResponse.scenarioID);
                component.set("v.workspaceID", columnsHeaderResponse.workspaceID);
                component.set("v.scenarioSource", columnsHeaderResponse.scenarioSource);
                component.set("v.scenarioName", columnsHeaderResponse.teamInstanceDisplayName);
                component.set("v.scenarioStage", columnsHeaderResponse.scenarioStage);

                component.createDataTable();
            }
            else if (response.getState() === "ERROR" || response.getReturnValue() == null) 
            {
                component.hideSpin();                
                var errors = response.getError();
                var errorMessage = errors[0].message;

                var uapLabel;
                if(component.get("v.namespace") != '')
                    uapLabel = $A.get("$Label.AxtriaSalesIQTM.No_UAP_Assigned");
                else
                    uapLabel = $A.get("$Label.c.No_UAP_Assigned");

                if(errorMessage.indexOf(uapLabel) != -1)
                {
                    var hideDiv = component.find('hideDiv');
                    $A.util.removeClass(hideDiv,'slds-hide');
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: uapLabel,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                    return;
                }

                helper.logException(component, response.getError());
            }
        });
        $A.enqueueAction(getColumnsHeaderJSON);
    },  

    searchKeyChange: function(component, event, helper){
        var $j = jQuery.noConflict();
        var globalId = component.getGlobalId();
        var dtTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();
        var inputVal = document.getElementById(globalId+'searchInputBox').value;

        if(inputVal != undefined && inputVal.length > 0){
            $A.util.removeClass(component.find('clearIcon'), "slds-hide");
            dtTable.search(inputVal).draw();
        }
        else{
            $A.util.addClass(component.find('clearIcon'), "slds-hide");
            dtTable.search('').draw() ;
        }
    },
      
    displayLegend : function(component, event, helper) {
        document.getElementById('legend').style.display="block";
    },

    closeLegend : function(component, event, helper) {
        document.getElementById('legend').style.display="none";
    },

    clearSearch :function(component,event,helper)
    {
        var globalId = component.getGlobalId(); 
        document.getElementById(globalId+'searchInputBox').value = "";
        component.searchKeyChange();
    },

    reloadDataTable : function(component, event, helper)
    {
        component.createDataTable();
        // component.showSpin();
        // var globalId = component.getGlobalId();
        // var $j = jQuery.noConflict();
        // // component.set("v.updatedRecs",{});
        // if(document.getElementById(globalId+'_tableContainer').style.display == 'none')
        // {
        //     var card = component.find('no_record_card');
        //     $A.util.addClass(card, "slds-hide");
        //     document.getElementById(globalId+'_tableContainer').style.display = 'block';
        // }
        // var allRecs = component.get("c.runQuery");
        // var colStructure = component.get("v.columnStructure");
        
        // allRecs.setParams(
        // {
        //     "stringifiedRequest": '{}' ,
        //     "moduleName" : component.get("v.moduleName"),
        //     "recordId" : component.get("v.recordId"),
        //     "selectedPositionId" : component.get("v.selectedPosition"),
        //     "selectedEmployeeId" : component.get("v.selectedEmployeeId"),
        //     "selectedTeamInstanceId"  : component.get("v.selectedTeamInstance"),
        //     "columnHeadersAPIname" : colStructure.allFieldsAPI,
        //     "whereClausePACP" : component.get("v.whereClausePACP"),
        //     "subInterface" : component.get("v.subInterface"),
        //     "ObjectDetailWrapperList" : JSON.stringify(component.get("v.ObjectDetailWrapperList"))
        // });
            
        // allRecs.setCallback(this, function(response)
        // {
        //    var state = response.getState();

        //     if(state === 'SUCCESS' && response.getReturnValue() != null)
        //     {
        //         var newData = response.getReturnValue().data;
        //         console.log('--new data');
        //         console.log(response.getReturnValue());
        //         console.log(newData);
                
                
        //         var newTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();
        //         console.log('---> reload -->');
        //         newTable.clear().rows.add(newData).draw();
        //         component.set("v.selectedCheckboxList",[]);
        //         component.checkDependencyConditionsOnLoad(component.get("v.colStructureHeaderDetailJson"));
        //         component.hideSpin();
        //     }
        //     else if (state === "ERROR" || response.getReturnValue() == null) 
        //     {
        //         component.hideSpin();
        //         helper.logException(component, response.getError());
        //     }
        // });   
        // $A.enqueueAction(allRecs);  
    },

    createDataTable : function(component, event, helper)
    {
        var action = component.get("c.getOrgNamespace");

        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var ns = response.getReturnValue();
                component.set("v.namespace",ns);
            }    
        
            component.showSpin();
            console.log('--ObjectDetailWrapperList ');
            console.log(JSON.parse(JSON.stringify(component.get("v.ObjectDetailWrapperList"))));
            var namespace = component.get("v.namespace");
            var oldPage = 0;
            var colStructure = component.get("v.columnStructure");
            var sortOrderDefault = JSON.parse(colStructure.sortOrderDefault);
            var colStructureHeaderJson = JSON.parse(colStructure.columnHeaderJson);
            //component.set("v.tableWidth", 'width :' + colStructureHeaderJson.length * 10 + '%;');
            var colStructureHeaderDetailJson = JSON.parse(colStructure.columnHeaderDetailJson);
            component.set("v.colStructureHeaderDetailJson", colStructureHeaderDetailJson);
            var columnHeaderAPIname = colStructure.allFieldsAPI;
            component.set("v.updatedRecs",'');
            component.set("v.selectedCheckboxList",[]);
            var titleLabel = '';
            if(component.get("v.namespace") != '')
                titleLabel = $A.get("$Label.AxtriaSalesIQTM.Call_Plan");
            else
                titleLabel = $A.get("$Label.c.Call_Plan");

            console.log(colStructureHeaderJson);
            var $j = jQuery.noConflict();
            var globalId = component.getGlobalId();

            var widthIndex = 10;
            if(component.get("v.moduleName") == 'Position Employee Grid')
                widthIndex = 15;

            if(colStructureHeaderJson.length * widthIndex > 100 && navigator.userAgent.indexOf("Mac") == -1){
                if(document.getElementById(globalId+'_dataTable') != null)
                    document.getElementById(globalId+'_dataTable').style.width = colStructureHeaderJson.length * widthIndex + '%';
            }
            else if(colStructureHeaderJson.length * (widthIndex + 3) > 100 && navigator.userAgent.indexOf("Mac") != -1){
                if(document.getElementById(globalId+'_dataTable') != null)
                    document.getElementById(globalId+'_dataTable').style.width = colStructureHeaderJson.length * (widthIndex + 3) + '%';
            }

            var tableHeight;
            if(component.get("v.moduleName") == 'Call Plan' && component.get("v.subInterface") == '' && navigator.userAgent.indexOf("Mac") == -1 && !navigator.userAgent.match(/Trident.*rv\:11\./))
            {
                if((document.getElementsByClassName("cSalesIQCallPlan")[1] && document.getElementsByClassName("cSalesIQDataTable_V2")[3] && document.getElementsByClassName("cSalesIQDataTable_V2")[2]) || (document.getElementsByClassName("AxtriaSalesIQTMSalesIQCallPlan")[1] && document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[3] && document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[2]))
                {
                    if(component.get("v.namespace") == ''){
                        tableHeight = document.getElementsByClassName("cSalesIQCallPlan")[1].offsetHeight - (document.getElementsByClassName("cSalesIQDataTable_V2")[3].offsetHeight + document.getElementsByClassName("cSalesIQDataTable_V2")[2].offsetHeight) - 80;
                    }
                    else{
                        tableHeight = document.getElementsByClassName("AxtriaSalesIQTMSalesIQCallPlan")[1].offsetHeight - (document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[3].offsetHeight + document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[2].offsetHeight) - 80;
                    }
                } 
                else
                {
                    tableHeight =  ($j(document).height() - 400);
                } 

                if(colStructure.isWrapText)
                {
                    tableHeight = tableHeight - 15;
                } 
                else
                {
                    tableHeight = tableHeight + 10;
                }
                tableHeight = tableHeight + 'px';
                //tableHeight =  ($j(document).height() - 400) + 'px';
                
            }   
            else if(component.get("v.moduleName") == 'Call Plan' && component.get("v.subInterface") == '' && navigator.userAgent.indexOf("Mac") == -1 && navigator.userAgent.match(/Trident.*rv\:11\./))
            {
                if((document.getElementsByClassName("cSalesIQCallPlan")[1] && document.getElementsByClassName("cSalesIQDataTable_V2")[3] && document.getElementsByClassName("cSalesIQDataTable_V2")[4]) || (document.getElementsByClassName("AxtriaSalesIQTMSalesIQCallPlan")[1] && document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[3] && document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[4]))
                {
                    if(component.get("v.namespace") == ''){
                        tableHeight = document.getElementsByClassName("cSalesIQCallPlan")[1].offsetHeight - (document.getElementsByClassName("cSalesIQDataTable_V2")[3].offsetHeight + document.getElementsByClassName("cSalesIQDataTable_V2")[4].offsetHeight) - 90;
                    }
                    else{
                        tableHeight = document.getElementsByClassName("AxtriaSalesIQTMSalesIQCallPlan")[1].offsetHeight - (document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[3].offsetHeight + document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[4].offsetHeight) - 90;
                    }
                } 
                else
                {
                    tableHeight =  ($j(document).height() - 410);
                }

                if(colStructure.isWrapText)
                {
                    tableHeight = tableHeight - 15;
                }
                else
                {
                    tableHeight = tableHeight + 10;
                }
                tableHeight = tableHeight + 'px';
                //tableHeight =  ($j(document).height() - 410) + 'px';
                // if(component.get("v.namespace") == '')
                //     tableHeight = document.getElementsByClassName("cSalesIQCallPlan")[1].offsetHeight - (document.getElementsByClassName("cSalesIQDataTable_V2")[3].offsetHeight + document.getElementsByClassName("cSalesIQDataTable_V2")[4].offsetHeight) - 85 + 'px';
                // else
                //     tableHeight = document.getElementsByClassName("AxtriaSalesIQTMSalesIQCallPlan")[1].offsetHeight - (document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[3].offsetHeight + document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[4].offsetHeight) - 85 + 'px';
            }
            else if(component.get("v.moduleName") == 'Call Plan' && component.get("v.subInterface") == '' && navigator.userAgent.indexOf("Mac") != -1)
            {   
               /* if((document.getElementsByClassName("cSalesIQCallPlan")[1] && document.getElementsByClassName("cSalesIQDataTable_V2")[3] && document.getElementsByClassName("cSalesIQDataTable_V2")[4]) || (document.getElementsByClassName("AxtriaSalesIQTMSalesIQCallPlan")[1] && document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[3] && document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[4]))
                {
                    if(component.get("v.namespace") == '')
                        tableHeight = document.getElementsByClassName("cSalesIQCallPlan")[1].offsetHeight - (document.getElementsByClassName("cSalesIQDataTable_V2")[3].offsetHeight + document.getElementsByClassName("cSalesIQDataTable_V2")[4].offsetHeight) - 100 + 'px';
                    else
                        tableHeight = document.getElementsByClassName("AxtriaSalesIQTMSalesIQCallPlan")[1].offsetHeight - (document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[3].offsetHeight + document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[4].offsetHeight) - 100 + 'px';
                } 
                else
                {*/
                    tableHeight =  ($j(document).height() - 430);
                    if(colStructure.isWrapText)
                    {
                        tableHeight = tableHeight - 15;
                    }
                    else
                    {
                        tableHeight = tableHeight + 10;
                    }
                    tableHeight = tableHeight + 'px';
                /*}*/
                // tableHeight =  ($j(document).height() - 430) + 'px';
            }
            else if(component.get("v.moduleName") == 'Call Plan' && component.get("v.subInterface") != '')
            {
                // tableHeight = ($j(document).height() - 460) + 'px';
                // tableHeight = '320px';
                tableHeight = document.getElementById('addDropModalContent').offsetHeight - 130;
                if(colStructure.isWrapText)
                {
                    tableHeight = tableHeight - 20;
                }
                tableHeight = tableHeight + 'px';
            }
            else if(component.get("v.moduleName") == 'Position Employee Grid')
            {
                if(navigator.userAgent.indexOf("Mac") == -1 && !navigator.userAgent.match(/Trident.*rv\:11\./))
                {
                    if(component.get("v.namespace") == '')
                    {
                        tableHeight = document.getElementById('modal_content').offsetHeight - document.getElementsByClassName("cSalesIQDataTable_V2")[2].offsetHeight - 90;
                    }
                    else
                    {
                        tableHeight = document.getElementById('modal_content').offsetHeight - document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[2].offsetHeight - 90;
                    }

                }
                else if(navigator.userAgent.indexOf("Mac") == -1 && navigator.userAgent.match(/Trident.*rv\:11\./))
                {
                    if(component.get("v.namespace") == '')
                    {
                        tableHeight = document.getElementById('modal_content').offsetHeight - document.getElementsByClassName("cSalesIQDataTable_V2")[3].offsetHeight - 90;
                    }
                    else
                    {
                        tableHeight = document.getElementById('modal_content').offsetHeight - document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[3].offsetHeight - 90;
                    }

                }
                else if(navigator.userAgent.indexOf("Mac") != -1)
                {
                    if(component.get("v.namespace") == '')
                    {
                        tableHeight = document.getElementById('modal_content').offsetHeight - document.getElementsByClassName("cSalesIQDataTable_V2")[3].offsetHeight - 110;
                    }
                    else
                    {
                        tableHeight = document.getElementById('modal_content').offsetHeight - document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[2].offsetHeight - 110;
                    }

                }
                else
                {
                    if(component.get("v.namespace") == '')
                    {
                        tableHeight = document.getElementById('modal_content').offsetHeight - document.getElementsByClassName("cSalesIQDataTable_V2")[2].offsetHeight - 90;
                    }
                    else
                    {
                        tableHeight = document.getElementById('modal_content').offsetHeight - document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[2].offsetHeight - 90;
                    }
                }
                // if(colStructure.isWrapText)
                // {
                //     tableHeight = tableHeight - 5;
                // }
                tableHeight = tableHeight + 'px';
                
                // tableHeight = 'true';
            }
            else
            {
                tableHeight = true;
            }

            var sessionTable = $j(document.getElementById(globalId+'_dataTable')).DataTable(
            {
                "info": true,
                "searching": true, 
                "destroy" : true,
                "processing": false,
                "order": sortOrderDefault,
                //"dom": '<"div-pg"pi><"div-search"f><"div-tbl">t<"bottom-info"> ', // f search, p :- pagination , l:- page length 
                "dom" : 'Z<<t>ip>',
                "colResize": {
                    "tableWidthFixed": false
                },
                "paging":colStructure.paginationOn,
                "pageLength": colStructure.paginationSize,
                // "serverSide": colStructure.serverSideLoading,
                "serverSide": false,
                "autoWidth": false,
                "scrollX" : true,
                "search": {
                    "regex": true,
                    "smart": false,
                }, 

                "initComplete": function( settings, json ) {
                    console.log('calling initComplete--------------');
                    // giving headers to datatable columns
                    var thElements = document.getElementsByTagName("th");
                    for(var i = 0; i < thElements.length ; i++){
                        if(thElements[i].innerHTML.indexOf('selectAllcheckbox') == -1)
                            thElements[i].title = thElements[i].innerHTML;
                    }
                    // var width = 230;
                    // if(document.getElementsByClassName('cph')[0])
                    //     width = document.getElementsByClassName('cph')[0].offsetWidth;
                    // if(document.getElementById('flagContainer'))
                    //     document.getElementById('flagContainer').style.paddingLeft = width + 'px';

                    var dependencyConditions = component.get("c.getDependencyControlData");
                    dependencyConditions.setParams({
                       teamInstanceId : component.get("v.selectedTeamInstance")
                    });

                    dependencyConditions.setCallback(this,function(response)
                    {
                        if(response.getState() === 'SUCCESS' && response.getReturnValue() != null)
                        {
                            component.set("v.dependencyControlConditions",response.getReturnValue()); 
                        }
                        
                    });
                    $A.enqueueAction(dependencyConditions);
                    console.log('dependency -- ',component.get("v.dependencyControlConditions"));
                    if(component.get("v.dependencyControlConditions").keys.length > 0){
                        component.checkDependencyConditionsOnLoad(colStructureHeaderDetailJson);
                    }
                    component.set("v.executeSortingEvent",'true');
                    component.hideSpin();
                    /*var dependencyConditions = component.get("c.getDependencyControlData");
                    dependencyConditions.setParams({
                       teamInstanceId : component.get("v.selectedTeamInstance")
                    });

                    dependencyConditions.setCallback(this,function(response)
                    {
                        if(response.getState() === 'SUCCESS' && response.getReturnValue() != null)
                        {
                            component.set("v.dependencyControlConditions",response.getReturnValue()); 
                            component.checkDependencyConditionsOnLoad(colStructureHeaderDetailJson);
                            component.set("v.executeSortingEvent",'true');
                        }
                        //component.hideSpin();
                    });
                    $A.enqueueAction(dependencyConditions);*/
                    if(component.get("v.moduleName") == 'Call Plan')
                    {
                        $j('.dataTables_scrollBody').on('scroll',function() 
                        {
                            component.closeErrorPopover();
                        }); 
                    }
                },

                "ajax": function (data, callback, settings) 
                {
                    var allRecs = component.get("c.runQuery");

                    var stringifyData = JSON.stringify(data);

                    allRecs.setParams(
                    {
                        "stringifiedRequest": stringifyData ,
                        "moduleName" : component.get("v.moduleName"),
                        "recordId" : component.get("v.recordId"),
                        "selectedPositionId" : component.get("v.selectedPosition"),
                        "selectedEmployeeId" : component.get("v.selectedEmployeeId"),
                        "selectedTeamInstanceId"  : component.get("v.selectedTeamInstance"),
                        "columnHeadersAPIname" : columnHeaderAPIname,
                        "whereClausePACP" : component.get("v.whereClausePACP"),
                        "subInterface" : component.get("v.subInterface"),
                        "ObjectDetailWrapperList" : JSON.stringify(component.get("v.ObjectDetailWrapperList"))
                    });
                        
                    allRecs.setCallback(this, function(response)
                    {
                       var state = response.getState();

                        if(state === 'SUCCESS' && response.getReturnValue() != null)
                        {
                            var resp = response.getReturnValue();
                            console.log(resp);
                            component.set("v.isCRraised", resp.isCRraised);
                            component.set("v.showCallPlanSetting", resp.isCallPlanSettingsEnabled);

                            if(resp.isReadOnly)
                                component.set("v.isCRraised", true);

                            if(component.get("v.isCRraised") == true && component.get("v.showSaveSubmitButtons") == true){    
                                document.getElementById("saveButton").disabled = true;
                                document.getElementById("submitButton").disabled = true;
                                document.getElementById("undoButton").disabled = true;
                                document.getElementById("addTarget").disabled = true;
                                $A.util.removeClass(component.find('addTargetIcon'),'enabledButton');
                            }
                            else if(component.get("v.showSaveSubmitButtons") == true){
                                document.getElementById("saveButton").disabled = false;
                                document.getElementById("submitButton").disabled = false;
                                document.getElementById("undoButton").disabled = false;
                                document.getElementById("addTarget").disabled = false;
                                $A.util.addClass(component.find('addTargetIcon'),'enabledButton');
                            }

                            if(component.get("v.moduleName") == 'Call Plan' && component.get("v.showPositionHierarchy") == "true") {    
                                var positionhierarchy = component.find('positionhierarchy');
                                positionhierarchy.openBreadcrumb();
                                positionhierarchy.checkboxSelect(component.get("v.selectedPosition"));
                            }

                            var card = component.find('no_record_card');
                            var dataTableComponent = component.find('jquery-dataTable');
                            
                            if(resp.recordsTotal == 0){
                                document.getElementById(globalId+'_tableContainer').style.display = 'none';
                                $A.util.removeClass(card, "slds-hide");
                                component.set("v.showSearchBar", false);
                                component.hideSpin();
                                return;
                            }
                            else{
                                document.getElementById(globalId+'_tableContainer').style.display = 'block';
                                $A.util.addClass(card, "slds-hide");
                                component.set("v.showSearchBar", true);
                            }
                                
                            setTimeout( function () {
                                callback( {
                                    draw:data.draw,
                                    data: resp.data,
                                    recordsTotal: resp.recordsTotal,
                                    recordsFiltered: resp.recordsTotal
                                } );
                                // Commented this line inorder to fix SPD-5098
                                try{
                                    sessionTable.fixedColumns().update();
                                }
                                catch(err){
                                    console.log('Error ', err);
                                }
                            }, 200 );

                            
                        }
                        else if (state === "ERROR" || response.getReturnValue() == null) 
                        {
                            helper.logException(component, response.getError());
                        }
                    });

                    $A.enqueueAction(allRecs); 
                    window.setTimeout(
                        $A.getCallback(function() {
                            console.log('Calling'); 
                        }), 200
                    );
                },
                scrollY: tableHeight,
                deferRender: true,
                "bSort": true,
                "aaSorting": [],
                fixedHeader: false,
                fixedColumns:   {
                    leftColumns: colStructure.fixedColumnFromLeft,
                    rightColumns: colStructure.fixedColumnFromRight,
                    paging: true
                    
                },
                 
                "language":
                {
                    "emptyTable": "Loading..."
                },
                buttons: [{
                    extend : 'excelHtml5',
                    text : '',
                    title : titleLabel, 
                    exportOptions: 
                    {
                        columns: function ( idx, data, node ) 
                        {
                            var isVisible = sessionTable.column( idx ).visible();
                            return !isVisible || ($j(sessionTable.column(idx).header()).text() == '' ||  $j(sessionTable.column(idx).header()).text() == 'Drop') ? false : true; 
                        },
                        format: {
                            body: function ( data, row, column, node ) 
                            {
                                if(colStructureHeaderDetailJson[column + 1].fieldDataType == 'Date')
                                {
                                    let fetchedDate = new Date(data);
                                    let convertedDate = (fetchedDate.getMonth()+1) + "/" + fetchedDate.getDate() + "/" +  fetchedDate.getFullYear();
                                    return convertedDate;
                                }
                                else if(colStructureHeaderDetailJson[column + 1].fieldDataType == 'Formula'){
                                    return $j(data).text(); 
                                }
                                else
                                    return data == '' ? ' ' : data;
                            }
                        }
                    }  
                }],

                columns:colStructureHeaderJson,
                columnDefs : 
                [
                    {
                        targets:  '_all',
                        "createdCell": function (td, cellData, rowData, row, col)
                        {   
                            $j(td).addClass('data_'+col);

                            // $j(td).css('height','30px !important');
                            if(colStructureHeaderDetailJson[col].fieldApiName == namespace+'Change_Status__c'){
                                if(cellData == 'Submission Pending'){
                                    var imgHtml = '<img alt="' + cellData + '" title="' + cellData + '" src="' + $A.get('$Resource.icons1')+ '/final/submission-pending.png' + '" />';                               
                                }
                                 else if(cellData == 'Approved'){
                                    var imgHtml = '<img alt="' + cellData + '" title="' + cellData + '" src="' + $A.get('$Resource.icons1') + '/final/Approved.png' +'" />';                          
                                }
                                else if(cellData == 'Approval Pending'){
                                    var imgHtml = '<img alt="' + cellData + '" title="' + cellData + '" src="' + $A.get('$Resource.icons1') + '/final/pending.png' + '" />';     
                                }
                                else{
                                   var imgHtml = '';
                                }
                                $j(td).removeClass('slds-text-align_left ').addClass('slds-text-align_center');
                                $j(td).html(imgHtml);
                            }
                            else if(colStructureHeaderDetailJson[col].fieldApiName == namespace+'Change_Action_Type__c')
                            {
                                if(cellData == 'Added')
                                {
                                    var imgHtml = '<img alt="' + cellData + '" title="' + cellData + '" src="' + $A.get('$Resource.icons1')+ '/final/added.png' + '" />'
                                    if(rowData[namespace+'Change_Status__c'] == 'Submission Pending' && component.get('v.subInterface') == '')
                                    {
                                        imgHtml = imgHtml + '<button class="slds-button undoRowButton slds-button--icon-border" style="margin-left:10px;" title="Undo" id="undoRowButton_'+row+'"><span class="icon-undo icon-svg slds-icon-text-default undoRowButton"></span></button>';
                                    }
                                    
                                }
                                else if(cellData == 'Dropped')
                                {
                                    var imgHtml = '<img alt="' + cellData + '" title="' + cellData + '" src="' + $A.get('$Resource.icons1') + '/final/removed.png' +'" />';
                                    if(rowData[namespace+'Change_Status__c'] == 'Submission Pending' && component.get('v.subInterface') == '')
                                    {
                                        imgHtml = imgHtml + '<button class="slds-button undoRowButton slds-button--icon-border" style="margin-left:10px;" title="Undo" id="undoRowButton_'+row+'"><span class="icon-undo icon-svg slds-icon-text-default undoRowButton"></span></button>';
                                    }                          
                                }
                                else if(cellData == 'Edited')
                                {
                                    var imgHtml = '<img alt="' + cellData + '" title="' + cellData + '" src="' + $A.get('$Resource.icons1') + '/final/Edited.png' +'" />';
                                    if(rowData[namespace+'Change_Status__c'] == 'Submission Pending' && component.get('v.subInterface') == '')
                                    {
                                        imgHtml = imgHtml + '<button class="slds-button undoRowButton slds-button--icon-border" style="margin-left:10px;" title="Undo" id="undoRowButton_'+row+'"><span class="icon-undo icon-svg slds-icon-text-default undoRowButton"></span></button>';
                                    }                          
                                }                                
                                else
                                {
                                    var imgHtml = '';
                                    if(rowData[namespace+'Change_Status__c'] == 'Submission Pending' && component.get('v.subInterface') == '')
                                    {
                                        imgHtml = imgHtml + '<button class="slds-button undoRowButton slds-button--icon-border" title="Undo" id="undoRowButton_'+row+'"><span class="icon-undo icon-svg slds-icon-text-default undoRowButton"></span></button>';
                                    }
                                }
                                $j(td).removeClass('slds-text-align_left ').addClass('slds-text-align_center');
                                $j(td).html(imgHtml);
                            }
                            else if(colStructureHeaderDetailJson[col].fieldDataType=='Picklist')
                            {
                                var selectHtml = "";
                                if(component.get("v.isCRraised") == true  || (rowData[namespace+'Change_Action_Type__c'] == 'Dropped' && component.get('v.subInterface') == ''))
                                    selectHtml = "<select disabled name=\"picklistInput\" class=\"slds-select\">" ;
                                else
                                    selectHtml = "<select name=\"picklistInput\" class=\"slds-select\">" ;

                                var data = colStructureHeaderDetailJson[col].dynamicValuesString;

                                selectHtml = selectHtml + "<option value='None' >" + 'None' + "</option>";

                                if(data != undefined && data != 'undefined'  && data != null && data != '') 
                                {
                                    
                                    var finaldata=data.split(',');
                                    for(var key=0;key<finaldata.length;key++)
                                    {

                                        if(colStructureHeaderDetailJson[col].isEditable == true)
                                        {
                                            if(finaldata[key]==rowData[colStructureHeaderDetailJson[col].fieldApiName])
                                            {
                                                selectHtml = selectHtml+ "<option selected = \"selected\" value='"+finaldata[key]+"'>"+finaldata[key]+" </option>"; 
                                            }
                                            else
                                            {
                                                selectHtml = selectHtml+ "<option value='"+finaldata[key]+"'>"+finaldata[key]+"</option>";
                                            }
                                        }
                                        else
                                        {
                                            selectHtml="<output title='" + rowData[colStructureHeaderDetailJson[col].fieldApiName] + "' >" + rowData[colStructureHeaderDetailJson[col].fieldApiName]+ "</output>";
                                            break;
                                        }

                                    }
                                }
                                else{
                                    selectHtml="<output title='' ></output>";
                                }

                                if(colStructureHeaderDetailJson[col].isEditable)
                                    selectHtml = selectHtml + '</select>';
                                $j(td).html(selectHtml);
                            }
                            else if(colStructureHeaderDetailJson[col].fieldDataType == 'Date'){
                                if(rowData[colStructureHeaderJson[col].data] != undefined && rowData[colStructureHeaderJson[col].data] != null){
                                    var mesc = Date.parse(rowData[colStructureHeaderJson[col].data]);
                                    var fetchedDate = new Date(mesc);
                                    var dateFormat = component.get("v.dateFormat");
                                    // offset is added to get exact timezone info 
                                    fetchedDate.setSeconds(fetchedDate.getSeconds() + (fetchedDate.getTimezoneOffset()*60));
                                    var convertedDate = dateFormat.replace(/y+|Y+/,fetchedDate.getFullYear()).replace(/M+|m+/,(fetchedDate.getMonth()+1)).replace(/d+|D+/,fetchedDate.getDate());
                                    
                                    console.log('convertedDate line 888:'+convertedDate);
                                    var selectHtml="<output title='" + convertedDate + "' >" + convertedDate + "</output>";
                                    $j(td).html(selectHtml);
                                }
                                else{
                                    var convertedDate = '';
                                    var selectHtml="<output title='" + convertedDate + "' >" + convertedDate + "</output>";
                                    $j(td).html(selectHtml);
                                }
                            }
                            else if(colStructureHeaderDetailJson[col].fieldDataType=='Text')
                            {
                                var selectHtml = "";
                                if(colStructureHeaderJson[col].data.indexOf('.') == -1) {
                                    
                                    if(rowData[colStructureHeaderJson[col].data] == undefined || rowData[colStructureHeaderJson[col].data] == 'undefined')
                                        rowData[colStructureHeaderJson[col].data] = '';
                                    
                                    if(colStructureHeaderDetailJson[col].isEditable)
                                        selectHtml="<input class=\"textInput slds-input\" type=\"text\" value='"+rowData[colStructureHeaderJson[col].data]+"'"+"\>";
                                    else
                                        var selectHtml="<output title='" + rowData[colStructureHeaderJson[col].data] + "' >" + rowData[colStructureHeaderJson[col].data]+ "</output>";
                                }
                                else if(colStructureHeaderJson[col].data.split('.').length == 2){
                                    console.log('-- AK-- colStructureHeaderJson[col].data:',colStructureHeaderJson[col].data);
                                    console.log('==rowdata==', rowData);
                                    var columnSplit = colStructureHeaderJson[col].data.split('.');
                                    console.log('--AK-- columnSplit ', columnSplit);

                                        var ObjectDetailWrapperList = component.get("v.ObjectDetailWrapperList");
                                        console.log('ObjectDetailWrapperList :',ObjectDetailWrapperList);
                                        var column = columnSplit[0].replace('__r','__c');
                                        var recordDetails = ObjectDetailWrapperList[row].recordDetails;
                                        var index;
                                        
                                        for (var j = 0; j < recordDetails.length; j++) {
                                            if(recordDetails[j].fieldDataType == 'Reference' && recordDetails[j].fieldApiName == column){
                                                index = j;
                                                break;
                                            }
                                        }
                                        
                                       
                                        console.log('1- ',rowData);
                                        console.log('2- ',columnSplit[0]);
                                        if(index != undefined && recordDetails[index] != undefined && rowData[columnSplit[0]] == undefined){
                                            if(rowData[columnSplit[0].replace('__r','__c')] != undefined && recordDetails[index].fieldDataType == 'Reference' && component.get("v.moduleName") == 'Position Employee Grid'){
                                                var value = recordDetails[index].lookupDataValue;
                                                console.log('value :',value);
                                                if(colStructureHeaderDetailJson[col].isEditable){
                                                    console.log('--rowData[column]:',rowData[column]);
                                                    selectHtml="<input class=\"textInput slds-input\" type=\"text\" value='"+value+"'"+"\>";
                                                }
                                                else{
                                                    console.log('--rowData[column]:',rowData[column]);
                                                    selectHtml="<output title='" + value + "' >" + value + "</output>";
                                                }

                                            }else{
                                                selectHtml="<output title='" + '' + "' >" + '' + "</output>";
                                            }
                                        }else{
                                            if(rowData[columnSplit[0]] != undefined ){
                                                console.log('--AK1-- rowData[columnSplit[0]]:',rowData[columnSplit[0]]);
                                                console.log('--date for lookup-');
                                                
                                                
                                                if(rowData[columnSplit[0]][columnSplit[1]] == undefined || rowData[columnSplit[0]][columnSplit[1]] == 'undefined'){
                                                    console.log('--AK11-- rowData[columnSplit[0]]:',rowData[columnSplit[0]][columnSplit[1]]);
                                                    rowData[columnSplit[0]][columnSplit[1]] = '';
                                                }

                                                if(colStructureHeaderDetailJson[col].isEditable){
                                                    console.log('--AK12-- rowData[columnSplit[0]][columnSplit[1]]:',rowData[columnSplit[0]][columnSplit[1]]);
                                                    selectHtml="<input class=\"textInput slds-input\" type=\"text\" value='"+rowData[columnSplit[0]][columnSplit[1]]+"'"+"\>";
                                                }
                                                else{
                                                    console.log('--AK13-- rowData[columnSplit[0]][columnSplit[1]]:',rowData[columnSplit[0]][columnSplit[1]]);
                                                    selectHtml="<output title='" + rowData[columnSplit[0]][columnSplit[1]] + "' >" + rowData[columnSplit[0]][columnSplit[1]] + "</output>";
                                                }
                                            
                                                
                                            }
                                            
                                            else{
                                                console.log('--AK2--');
                                                selectHtml="<output title='" + '' + "' >" + '' + "</output>";
                                            }
                                        }
                                   
                                }
                                else{
                                    console.log('--inside third');
                                    var columnSplit = colStructureHeaderJson[col].data.split('.');
                                    //console.log('--columnSplit ', columnSplit);
                                    var cellValue = '';
                                    if(!(rowData[columnSplit[0]][columnSplit[1]] == undefined || rowData[columnSplit[0]][columnSplit[1]] == 'undefined' || rowData[columnSplit[0]][columnSplit[1]][columnSplit[2]] == undefined || rowData[columnSplit[0]][columnSplit[1]][columnSplit[2]] == 'undefined')){
                                        cellValue = rowData[columnSplit[0]][columnSplit[1]][columnSplit[2]];
                                        console.log('inisde if');
                                    }

                                    if(colStructureHeaderDetailJson[col].isEditable)
                                        selectHtml="<input class=\"textInput slds-input\" type=\"text\" value='"+cellValue+"'"+"\>";
                                    else
                                        selectHtml="<output title='" + cellValue + "' >" + cellValue + "</output>";
                                }

                                if(component.get("v.isCRraised") == true  || (rowData[namespace+'Change_Action_Type__c'] == 'Dropped' && component.get('v.subInterface') == '')){
                                    selectHtml = selectHtml.replace("<input", "<input disabled");
                                }
                                $j(td).html(selectHtml);
                                $j(td).css('text-align','left');
                            }
                            else if(colStructureHeaderDetailJson[col].fieldDataType=='Number')
                            {
                                var selectHtml = "";
                                if(colStructureHeaderJson[col].data.indexOf('.') == -1) {
                                    
                                    if(rowData[colStructureHeaderJson[col].data] == undefined || rowData[colStructureHeaderJson[col].data] == 'undefined')
                                        rowData[colStructureHeaderJson[col].data] = '';
                                    
                                    if(colStructureHeaderDetailJson[col].isEditable)
                                        selectHtml="<input class=\"textInput slds-input\" type=\"text\" value='"+rowData[colStructureHeaderJson[col].data]+"'"+"\>";
                                    else
                                        var selectHtml="<output title='" + rowData[colStructureHeaderJson[col].data] + "' >" + rowData[colStructureHeaderJson[col].data]+ "</output>";
                                }
                                else if(colStructureHeaderJson[col].data.split('.').length == 2){
                                    var columnSplit = colStructureHeaderJson[col].data.split('.');
                                    //console.log('--columnSplit ', columnSplit);

                                    if(rowData[columnSplit[0]][columnSplit[1]] == undefined || rowData[columnSplit[0]][columnSplit[1]] == 'undefined')
                                        rowData[columnSplit[0]][columnSplit[1]] = '';

                                    if(colStructureHeaderDetailJson[col].isEditable)
                                        selectHtml="<input class=\"textInput slds-input\" type=\"text\" value='"+rowData[columnSplit[0]][columnSplit[1]]+"'"+"\>";
                                    else
                                        selectHtml="<output title='" + rowData[columnSplit[0]][columnSplit[1]] + "' >" + rowData[columnSplit[0]][columnSplit[1]] + "</output>";
                                }
                                else{
                                    console.log('--inside third');
                                    var columnSplit = colStructureHeaderJson[col].data.split('.');
                                    //console.log('--columnSplit ', columnSplit);
                                    var cellValue = '';
                                    if(!(rowData[columnSplit[0]][columnSplit[1]] == undefined || rowData[columnSplit[0]][columnSplit[1]] == 'undefined' || rowData[columnSplit[0]][columnSplit[1]][columnSplit[2]] == undefined || rowData[columnSplit[0]][columnSplit[1]][columnSplit[2]] == 'undefined')){
                                        cellValue = rowData[columnSplit[0]][columnSplit[1]][columnSplit[2]];
                                        console.log('inisde if');
                                    }

                                    if(colStructureHeaderDetailJson[col].isEditable)
                                        selectHtml="<input class=\"textInput slds-input\" type=\"text\" value='"+cellValue+"'"+"\>";
                                    else
                                        selectHtml="<output title='" + cellValue + "' >" + cellValue + "</output>";
                                }

                                $j(td).html(selectHtml);
                                $j(td).css('text-align','center');
                            }
                            else if(colStructureHeaderDetailJson[col].fieldDataType=='Boolean')
                            {
                                var selectHtml = "";
                                if(colStructureHeaderJson[col].data.indexOf('.') == -1) 
                                {
                                    
                                    if(rowData[colStructureHeaderJson[col].data] == undefined || rowData[colStructureHeaderJson[col].data] == 'undefined')
                                        rowData[colStructureHeaderJson[col].data] = '';
                                    
                                    if(colStructureHeaderDetailJson[col].isEditable){
                                        if(rowData[colStructureHeaderJson[col].data] == true)
                                            selectHtml="<input class=\"checkboxInput slds-input\" type=\"checkbox\" checked value='"+rowData[colStructureHeaderJson[col].data]+"'"+"\>";
                                        else
                                            selectHtml="<input class=\"checkboxInput slds-input\" type=\"checkbox\" value='"+rowData[colStructureHeaderJson[col].data]+"'"+"\>";
                                    }
                                    else
                                        if(rowData[colStructureHeaderJson[col].data] == true)
                                            selectHtml = "<input type='checkbox' checked value="+rowData[colStructureHeaderJson[col].data]+" disabled =\"disabled\" />";
                                        else
                                            selectHtml = "<input type='checkbox' value="+rowData[colStructureHeaderJson[col].data]+" disabled =\"disabled\" />";
                                }
                                else
                                {
                                    var columnSplit = colStructureHeaderJson[col].data.split('.');
                                    
                                    if(rowData[columnSplit[0]][columnSplit[1]] == undefined || rowData[columnSplit[0]][columnSplit[1]] == 'undefined')
                                        rowData[columnSplit[0]][columnSplit[1]] = '';

                                    if(colStructureHeaderDetailJson[col].isEditable)
                                        selectHtml="<input class=\"checkboxInput slds-input\" type=\"checkbox\" value='"+rowData[columnSplit[0]][columnSplit[1]]+"'"+"\>";
                                    else
                                        selectHtml = "<input type='checkbox'  value="+rowData[columnSplit[0]][columnSplit[1]]+" disabled =\"disabled\" />";
                                }

                                if(component.get("v.isCRraised") == true || (rowData[namespace+'Change_Action_Type__c'] == 'Dropped' && component.get('v.subInterface') == '')){
                                    selectHtml = selectHtml.replace("<input", "<input disabled");
                                }
                                $j(td).html(selectHtml);
                            }
                            else if(colStructureHeaderDetailJson[col].fieldDataType=='Checkbox')
                            {
                                if((rowData[namespace+'Change_Action_Type__c'] == 'Dropped' && component.get('v.subInterface') == ''))
                                {
                                    var selectHtml = "<input disabled type='checkbox' checked='checked' class='slds-checkbox firstCheckbox' name='choosePos' />";
                                }
                                else if((rowData[namespace+'Change_Status__c'] == 'Submission Pending' && component.get('v.subInterface') == ''))
                                {
                                    var selectHtml = "<input disabled type='checkbox' class='slds-checkbox firstCheckbox addCheckbox' name='choosePos' />";   
                                }
                                else if(component.get("v.subInterface") == '')
                                {
                                    var selectHtml = "<input type='checkbox' class='slds-checkbox firstCheckbox' name='choosePos' />";
                                }
                                else
                                {
                                    var selectHtml = "<input type='checkbox' class='slds-checkbox firstCheckbox addCheckbox' name='choosePos' />";   
                                }

                                if(component.get("v.isCRraised") == true)
                                {
                                    selectHtml = selectHtml.replace("<input", "<input disabled");
                                }

                                $j(td).html(selectHtml);
                            }
                        }
                    }
                ],

                "createdRow": function ( row, data, index ) 
                {   
                    $j(row).addClass('slds-hint-parent');
                    var rowHeight = colStructure.rowHeight;
                    // alert(rowHeight);
                    $j('td', row).css('font-size',Math.round(rowHeight/2)+'px');
                    $j('td', row).children('img').css('height',Math.round(rowHeight/2)+'px');
                    $j('td', row).children('select').css('height',(rowHeight-5)+'px');
                    $j('td', row).children('input').css('line-height',(rowHeight-5)+'px').css('min-height',(rowHeight-5)+'px');
                    $j('td',row).children('button[class*=undoRowButton]').css('height',(rowHeight-5)+'px');
                    // $j(td).css('padding-top','0px !important');
                    // $j(row).css('height','20px');
                    // $j('td', row).css('height','20px');
                    $j('td', row).addClass('slds-text-align_left slds-truncate');
                    
                    $j('td', row).css('padding','2px 18px');
                    // $j('td', row).eq(0).css('padding','5px 18px');
                    
                    $j('thead > tr> th').addClass('slds-is-sortable slds-is-resizable slds-text-title_caps slds-line-height_reset slds-truncate');
                    
                    if(colStructure.isWrapText || navigator.userAgent.indexOf("Mac") != -1)
                        $j('thead > tr> th').css('white-space','normal');

                    $j('thead > tr').addClass('slds-text-title_caps ');
                },
                "rowCallback": function( row, data, index ) 
                {
                   if(component.get("v.subInterface") != '')
                   {
                        var selectedCheckboxList = component.get("v.selectedCheckboxList");
                        if(selectedCheckboxList.indexOf(data.Id) != -1)
                        {
                            $j('input[class*=firstCheckbox]',row).prop('checked', true);
                        }
                        else
                        {
                            $j('input[class*=firstCheckbox]',row).prop('checked', false);
                        }
                   }
                }
            });

            $j(document.getElementById(globalId+'_dataTable')).on('click','input[class*=firstCheckbox]', function() 
            {   
                var selectedCheckboxList = component.get("v.selectedCheckboxList");
                var sessionTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();

                if(sessionTable.cell( $j(this).parent() ).index() == undefined)
                    return;

                var idxRow = sessionTable.cell( $j(this).parent() ).index().row;
                var idxColumn = $j(this).parent().attr('class').split(' ')[0].split('_')[1];
                console.log('select--->>>>' + idxRow + '::' + idxColumn);
                console.log(sessionTable.rows(idxRow).data()[0]['Id']);
                if($j(this).prop("checked"))
                {
                    if(selectedCheckboxList.indexOf(sessionTable.rows(idxRow).data()[0]['Id']) == -1)
                    {
                        sessionTable.cell(idxRow,idxColumn).data(true);
                        var selectHtml = "<input type='checkbox' checked='checked' class='slds-checkbox firstCheckbox' name='choosePos' />";
                        $j(sessionTable.cell(idxRow,idxColumn).node()).html(selectHtml);

                        /*if(component.get("v.subInterface") == '')
                        {
                            var attributeAPIname = namespace+'Change_Action_Type__c';
                            var updatedRecs = component.get("v.updatedRecs");
                            for(var i = 0;i<colStructureHeaderDetailJson.length;i++)
                            {
                                if(colStructureHeaderDetailJson[i].fieldApiName == namespace+'Change_Action_Type__c')
                                {
                                    var val = 'Dropped';
                                    sessionTable.cell(idxRow,i).data(val);
                                    var imgHtml = '<img alt="' + val + '" title="' + val + '" src="' + $A.get('$Resource.icons1') + '/final/removed.png' +'" style="width:20px;height:auto;"/>';
                                    $j(sessionTable.cell(idxRow,i).node()).removeClass('slds-text-align_left ').addClass('slds-text-align_center');
                                    $j(sessionTable.cell(idxRow,i).node()).html(imgHtml);
                                    var row = sessionTable.rows(idxRow).data();
                                    var data = row.data().toArray()[idxRow];
                                    console.log(data);
                                    console.log(updatedRecs);
                                    if(updatedRecs == '')
                                    {
                                        updatedRecs = {};
                                    }
                                    if(updatedRecs[data.Id] == undefined)
                                    {
                                        updatedRecs[data.Id] = {};
                                    }
                                    if(updatedRecs[data.Id][attributeAPIname] == undefined)
                                    {
                                        // console.log(updatedRecs[data.Id][attributeAPIname]);
                                        var newMap = updatedRecs[data.Id];
                                        newMap[attributeAPIname] = val;
                                        newMap['rowIndex'] = idxRow;
                                        updatedRecs[data.Id] = newMap;
                                        console.log('in if--->'+newMap);
                                    }
                                    else
                                    {
                                        console.log('in else--->'+updatedRecs)
                                        updatedRecs[data.Id][attributeAPIname] = val;
                                    }
                                }
                            }
                            // console.log(colStructureHeaderDetailJson[i].fieldApiName);
                            component.set("v.updatedRecs",updatedRecs);
                            component.checkDependencyConditions(attributeAPIname,colStructureHeaderDetailJson,updatedRecs,idxRow,sessionTable);
                        }*/

                        var attributeAPIname = namespace+'Change_Action_Type__c';
                            var updatedRecs = component.get("v.updatedRecs");
                            for(var i = 0;i<colStructureHeaderDetailJson.length;i++)
                            {
                                if(colStructureHeaderDetailJson[i].fieldApiName == namespace+'Change_Action_Type__c')
                                {
                                    var val = '';
                                    var rowHeight = colStructure.rowHeight;
                                    var imgHtml = '';
                                    if(component.get("v.subInterface") == ''){
                                        val = 'Dropped';
                                        imgHtml = '<img alt="' + val + '" title="' + val + '" src="' + $A.get('$Resource.icons1') + '/final/removed.png' +'" style="height:'+Math.round(rowHeight/2)+'px;"/>';
                                        // imgHtml = imgHtml  + '<button class="slds-button undoRowButton slds-button--icon-border" style="margin-left:10px;" title="Undo" id="undoRowButton_'+idxRow+'"><span class="icon-undo icon-svg slds-icon-text-default undoRowButton"></span></button>';
                                    }else{
                                        val = 'Added';
                                        imgHtml = '<img alt="' + val + '" title="' + val + '" src="' + $A.get('$Resource.icons1') + '/final/added.png' +'" style="height:'+Math.round(rowHeight/2)+'px;"/>';
                                    }
                                    
                                    
                                    sessionTable.cell(idxRow,i).data(val);
                                    $j(sessionTable.cell(idxRow,i).node()).removeClass('slds-text-align_left ').addClass('slds-text-align_center');
                                    $j(sessionTable.cell(idxRow,i).node()).html(imgHtml);
                                    var row = sessionTable.rows(idxRow).data();
                                    var data = row.data().toArray()[idxRow];
                                    console.log(data);
                                    console.log(updatedRecs);
                                    if(updatedRecs == '')
                                    {
                                        updatedRecs = {};
                                    }
                                    if(updatedRecs[data.Id] == undefined)
                                    {
                                        updatedRecs[data.Id] = {};
                                    }
                                    if(updatedRecs[data.Id][attributeAPIname] == undefined)
                                    {
                                        // console.log(updatedRecs[data.Id][attributeAPIname]);
                                        var newMap = updatedRecs[data.Id];
                                        newMap[attributeAPIname] = val;
                                        newMap['rowIndex'] = idxRow;
                                        updatedRecs[data.Id] = newMap;
                                        console.log('in if--->'+newMap);
                                    }
                                    else
                                    {
                                        console.log('in else--->'+updatedRecs)
                                        updatedRecs[data.Id][attributeAPIname] = val;
                                    }
                                }
                            }
                            // console.log(colStructureHeaderDetailJson[i].fieldApiName);
                            component.set("v.updatedRecs",updatedRecs);
                            component.checkDependencyConditions(attributeAPIname,colStructureHeaderDetailJson,updatedRecs,idxRow,sessionTable);
                        selectedCheckboxList.push(sessionTable.rows(idxRow).data()[0]['Id']);
                    }
                    component.set("v.selectedCheckboxList",selectedCheckboxList);
                    console.log('---> checked-->'+selectedCheckboxList.length);
                    console.log(selectedCheckboxList);
                }
                else
                {
                    if(selectedCheckboxList.indexOf(sessionTable.rows(idxRow).data()[0]['Id']) != -1)
                    {
                        sessionTable.cell(idxRow,idxColumn).data(false);
                        var selectHtml = "<input type='checkbox' class='slds-checkbox firstCheckbox' name='choosePos' />";
                        $j(sessionTable.cell(idxRow,idxColumn).node()).html(selectHtml);
                        //if(component.get("v.subInterface") == '')
                        //{
                            var attributeAPIname = namespace+'Change_Action_Type__c';
                            var updatedRecs = component.get("v.updatedRecs");
                            for(var i = 0;i<colStructureHeaderDetailJson.length;i++)
                            {
                                if(colStructureHeaderDetailJson[i].fieldApiName == namespace+'Change_Action_Type__c')
                                {
                                    // alert(i);
                                    var val = '';
                                    sessionTable.cell(idxRow,i).data(val);
                                    // var imgHtml = '<img alt="' + val + '" title="' + val + '" src="' + $A.get('$Resource.icons1') + '/final/Edited.png' +'" style="width:20px;height:auto;"/>';
                                    console.log('---> td-->');
                                    // console.log();
                                    // console.log($j(sessionTable.cell(idxRow,i).node()));
                                    // $j(sessionTable.cell(idxRow,i).node()).removeClass('slds-text-align_left ').addClass('slds-text-align_center');
                                    // $j(sessionTable.cell(idxRow,i).node()).html(imgHtml);
                                    var row = sessionTable.rows(idxRow).data();
                                    var data = row.data().toArray()[idxRow];
                                    console.log(data);
                                    console.log(updatedRecs);
                                    if(updatedRecs == '')
                                    {
                                        updatedRecs = {};
                                    }
                                    if(updatedRecs[data.Id] == undefined)
                                    {
                                        updatedRecs[data.Id] = {};
                                    }
                                    if(updatedRecs[data.Id][attributeAPIname] == undefined)
                                    {
                                        // console.log(updatedRecs[data.Id][attributeAPIname]);
                                        var newMap = updatedRecs[data.Id];
                                        newMap[attributeAPIname] = val;
                                        newMap['rowIndex'] = idxRow;
                                        updatedRecs[data.Id] = newMap;
                                        console.log('in if--->'+newMap);
                                    }
                                    else
                                    {
                                        console.log('in else--->'+updatedRecs)
                                        updatedRecs[data.Id][attributeAPIname] = val;
                                    }
                                }
                            }
                            // console.log(colStructureHeaderDetailJson[i].fieldApiName);
                            component.set("v.updatedRecs",updatedRecs);
                            component.checkDependencyConditions(attributeAPIname,colStructureHeaderDetailJson,updatedRecs,idxRow,sessionTable);
                        //}
                        selectedCheckboxList.splice(selectedCheckboxList.indexOf(sessionTable.rows(idxRow).data()[0]['Id']),1);
                        component.set("v.selectedCheckboxList",selectedCheckboxList);
                        var data = row.data().toArray()[idxRow];
                        component.set("v.selectedPACPId",data.Id);
                        component.set("v.undoRowNumber",idxRow);
                        updatedRecs[data.Id] = {};

                        var id = data.Id;
                        delete updatedRecs[id];

                        component.set("v.updatedRecs",updatedRecs);
                        console.log('updatedRecs : ',updatedRecs);
                        component.set("v.isUncheckEvent", "true");
                        component.undoRowChanges();

                        console.log('---> unchecked-->' + selectedCheckboxList + ':::' + selectedCheckboxList.length);
                    }
                }
                // Commented this line inorder to fix SPD-5098
                try{
                    sessionTable.fixedColumns().update();
                }
                catch(err){
                    console.log('Error ', err);
                }
                
            });

            $j('input[class*=selectAllcheckbox]').on('change', function() 
            {
                console.log('selectAll');
                var selectedCheckboxList = [];
                var sessionTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();
                $j('input[class*=addCheckbox]').prop('checked', this.checked);
                if($j(this).prop("checked"))
                {
                    var rows = sessionTable.rows().data();
                    for(var i=0;i<rows.length;i++)
                    {
                        // var selectedRecord = {};
                        // selectedRecord[sessionTable.rows(idxRow).data()[0]['Id']] = sessionTable.rows(idxRow).data()[0];
                        selectedCheckboxList.push(sessionTable.rows(i).data()[0]['Id']);
                    }
                   
                    component.set("v.selectedCheckboxList",selectedCheckboxList);
                    console.log(selectedCheckboxList);
                    console.log('---> checked-->' + selectedCheckboxList  + ':::' + selectedCheckboxList.length);
                }
                else
                {
                    selectedCheckboxList = [];
                    component.set("v.selectedCheckboxList",selectedCheckboxList);
                    console.log('---> unchecked-->' + selectedCheckboxList);
                }

                try{
                    sessionTable.fixedColumns().update();
                }
                catch(err){
                    console.log('Error ', err);
                }
            });    

            $j(document.getElementById(globalId+'_dataTable')).off('click', 'button[class*=undoRowButton]').on('click','button[class*=undoRowButton]', function() 
           {    
                var idxRow = parseInt($j(this).attr("id").split('_')[1]);
                var updatedRecs = component.get("v.updatedRecs");
                console.log(updatedRecs);
                var sessionTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();
                var row = sessionTable.rows(idxRow).data();
                var rowdata = row.data().toArray()[idxRow];
                component.set("v.undoRowNumber",idxRow);
                component.set("v.selectedPACPId",rowdata.Id);
                console.log(rowdata);

                var namespace = component.get("v.namespace");
                var modalMessage;
                if(namespace != '')
                    modalMessage = $A.get("$Label.AxtriaSalesIQTM.CALL_PLAN_UNDO_ROW_MESSAGE");
                else
                    modalMessage = $A.get("$Label.c.CALL_PLAN_UNDO_ROW_MESSAGE");

                $A.createComponent
                (
                    "c:SalesIQMessageDialog",
                    {
                        "body": modalMessage,
                        "namespace" : component.get("v.namespace"),
                        "onconfirm" : component.getReference("c.undoRowChanges"),
                        "aura:id" : "messageDialog"
                    },
                    function(cmp)
                    {                
                        if (component.isValid()) 
                        {
                            var targetCmp = component.find("getPopUp");
                            var body = targetCmp.get("v.body");
                            body.push(cmp);
                            targetCmp.set("v.body", body);
                        }
                    }
                );

           });

            $j(document.getElementById(globalId+'_dataTable')).off('click','button[class*=errorPopupButton]').on('click','button[class*=errorPopupButton]', function() 
           {
                var row_validation_failed_popover = document.getElementById('row_validation_failed_popover')
                if(row_validation_failed_popover.className.indexOf('slds-hide') != -1)
                {
                    row_validation_failed_popover.classList.remove('slds-hide');
                    var top = $j(this).offset().top ;
                    var left = $j(this).offset().left - 15;
                    if(component.get("v.subInterface") == '')
                        left = $j(this).offset().left - 15;
                    else
                        left = $j(this).offset().left - document.getElementById('addDropModalContainer').offsetLeft - 15;
                    row_validation_failed_popover.style.left = left + 'px';
                    row_validation_failed_popover.style.top = top - row_validation_failed_popover.clientHeight - 11 + 'px';
                }
                else
                {
                    row_validation_failed_popover.classList.add('slds-hide');  
                }



                // var validation_failed_popover = component.find('validation_failed_popover');
                // $A.util.toggleClass('slds-hide');
                // var idxRow = parseInt($j(this).attr("id").split('_')[1]);
                // var updatedRecs = component.get("v.updatedRecs");
                // console.log(updatedRecs);
                // var sessionTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();
                // var row = sessionTable.rows(idxRow).data();
                // var rowdata = row.data().toArray()[idxRow];
                // component.set("v.selectedPACPId",rowdata.Id);
                // console.log(rowdata);

                // var namespace = component.get("v.namespace");
                // var modalMessage;
                // if(namespace != '')
                //     modalMessage = $A.get("$Label.AxtriaSalesIQTM.CALL_PLAN_UNDO_ROW_MESSAGE");
                // else
                //     modalMessage = $A.get("$Label.c.CALL_PLAN_UNDO_ROW_MESSAGE");

                // $A.createComponent
                // (
                //     "c:SalesIQMessageDialog",
                //     {
                //         "body": modalMessage,
                //         "namespace" : component.get("v.namespace"),
                //         "onconfirm" : component.getReference("c.undoRowChanges"),
                //     },
                //     function(cmp)
                //     {                
                //         if (component.isValid()) 
                //         {
                //             var targetCmp = component.find("getPopUp");
                //             var body = targetCmp.get("v.body");
                //             body.push(cmp);
                //             targetCmp.set("v.body", body);
                //         }
                //     }
                // );

           });

           $j(document.getElementById(globalId+'_dataTable')).on('change','select[name=picklistInput], input[class*=textInput], input[class*=checkboxInput]', function() 
           {
                var sessionTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();
                var rowHeight = colStructure.rowHeight;
                
                if(sessionTable.cell( $j(this).parent() ).index() == undefined)
                    return ; 
                    
                var idxRow = sessionTable.cell( $j(this).parent() ).index().row;
                // var idxColumn = sessionTable.cell( $j(this).parent() ).index().column;
                
                var idxColumn = $j(this).parent().attr('class').split(' ')[0].split('_')[1];
                var val ;
                if($j(this).attr("type") == 'checkbox') 
                {
                    val = $j(this).prop("checked");
                    sessionTable.cell(idxRow,idxColumn).data(val);
                    if(val)
                    {
                        selectHtml="<input class=\"checkboxInput slds-input\" type=\"checkbox\" checked value='"+true+"'"+"\>";
                    }
                    else
                    {
                        selectHtml="<input class=\"checkboxInput slds-input\" type=\"checkbox\" value='"+false+"'"+"\>";
                    }
                     $j(sessionTable.cell(idxRow,idxColumn).node()).html(selectHtml);
                } 
                else 
                {

                    val = $j(this).val();
                    sessionTable.cell(idxRow,idxColumn).data(val);
                    var selectHtml = '';
                    if($j(this).attr('type') == 'text')
                    {
                        selectHtml="<input class=\"textInput slds-input\" type=\"text\" value='"+val+"'"+"\\>";
                        $j(sessionTable.cell(idxRow,idxColumn).node()).html(selectHtml);   
                    }
                    else
                    if(($j(this).attr('name') == 'picklistInput'))
                    {
                        selectHtml = "<select name=\"picklistInput\" class=\"slds-select\">";
                        
                        for(var i = 0;i<$j(this)[0].options.length;i++)
                        {
                            if($j(this)[0].options[i].value == val)
                            {
                                if(val == '')
                                {
                                    selectHtml = selectHtml + "<option selected = \"selected\" value='None'>" + 'None' + "</option>"; 
                                }
                                else
                                    selectHtml = selectHtml+ "<option selected = \"selected\" value='"+val+"'>"+val+" </option>"; 
                                $j(this)[0].options[i].selected = true;
                            }
                            else
                            {
                                if($j(this)[0].options[i].value == '')
                                {
                                    selectHtml = selectHtml + "<option value='None' >" + 'None' + "</option>"; 
                                }
                                else
                                    selectHtml = selectHtml+ "<option value='"+$j(this)[0].options[i].value+"'>"+$j(this)[0].options[i].value+" </option>"; 
                                $j(this)[0].options[i].selected = false;
                            }
                        }
                        selectHtml = selectHtml + '</select>';
                        console.log('selectHtml--->' + selectHtml);
                        $j(sessionTable.cell(idxRow,idxColumn).node()).html(selectHtml);

                    }
                    console.log('---val--->' + val);
                    console.log($j(this));
                    $j(sessionTable.cell(idxRow,idxColumn).node()).css('font-size',Math.round(rowHeight/2)+'px');
                    // $j(sessionTable.cell(idxRow,idxColumn).node()).children('img').css('height',Math.round(rowHeight/2)+'px');
                    $j(sessionTable.cell(idxRow,idxColumn).node()).children('select').css('height',(rowHeight-5)+'px');
                    $j(sessionTable.cell(idxRow,idxColumn).node()).children('input').css('line-height',(rowHeight-5)+'px').css('min-height',(rowHeight-5)+'px');
                    // $j(sessionTable.cell(idxRow,idxColumn).node()).children('button[class*=undoRowButton]').css('height',(rowHeight-5)+'px');
                    
                }
                var row = sessionTable.rows(idxRow).data();
                var data = row.data().toArray()[idxRow];
                var attributeAPIname = colStructureHeaderDetailJson[idxColumn].fieldApiName;
                var colIndexOfChangeAction = sessionTable.column( namespace+'Change_Action_Type__c:name' ).index();

                var updatedRecs = component.get("v.updatedRecs");

                if(updatedRecs == '')
                {
                    updatedRecs = {};
                }
                if(updatedRecs[data.Id] == undefined)
                {
                    updatedRecs[data.Id] = {};
                }
                console.log('data2----->' + sessionTable.cell(idxRow,colIndexOfChangeAction).data());
                if(colIndexOfChangeAction != null && (sessionTable.cell(idxRow,colIndexOfChangeAction).data() == '' || sessionTable.cell(idxRow,colIndexOfChangeAction).data() == 'No Change'))
                {
                    console.log('data----->' + sessionTable.cell(idxRow,colIndexOfChangeAction).data());
                    var actionVal = 'Edited';
                    sessionTable.cell(idxRow,colIndexOfChangeAction).data(actionVal);
                    var imgHtml = '<img alt="' + actionVal + '" title="' + actionVal + '" src="' + $A.get('$Resource.icons1') + '/final/Edited.png' +'" style="height:'+Math.round(rowHeight/2)+'px;"/>';
                    $j(sessionTable.cell(idxRow,colIndexOfChangeAction).node()).removeClass('slds-text-align_left ').addClass('slds-text-align_center');
                    $j(sessionTable.cell(idxRow,colIndexOfChangeAction).node()).html(imgHtml);
                    if(updatedRecs[data.Id][namespace+'Change_Action_Type__c'] == undefined)
                    {
                        var newMap = updatedRecs[data.Id];
                        newMap[namespace+'Change_Action_Type__c'] = actionVal;
                        newMap['rowIndex'] = idxRow;
                        updatedRecs[data.Id] = newMap;
                        console.log(updatedRecs[data.Id][namespace+'Change_Action_Type__c']);
                    }
                    else{
                        updatedRecs[data.Id][namespace+'Change_Action_Type__c'] = actionVal;
                    }
                    try{
                        sessionTable.fixedColumns().update();
                    }
                    catch(err){
                        console.log('Error ', err);
                    }
                }

                if(updatedRecs[data.Id][attributeAPIname] == undefined){
                    console.log(updatedRecs[data.Id][attributeAPIname]);
                    var newMap = updatedRecs[data.Id];
                    newMap[attributeAPIname] = val;
                    newMap['rowIndex'] = idxRow;
                    updatedRecs[data.Id] = newMap;
                    console.log(newMap);
                }
                else{
                    updatedRecs[data.Id][attributeAPIname] = val;
                }
                console.log('---> updatedRecs');
                console.log(updatedRecs);
                console.log(JSON.stringify(updatedRecs));
                component.set("v.updatedRecs",updatedRecs);

                console.log('calling dependencies --------');
                component.checkDependencyConditions(attributeAPIname,colStructureHeaderDetailJson,updatedRecs,idxRow,sessionTable);
                // Commented this line inorder to fix SPD-5098
                try{
                    sessionTable.fixedColumns().update();
                }
                catch(err){
                    console.log('Error ', err);
                }
                
            });


           //Page Change event of data table
            $j(document.getElementById(globalId+'_dataTable')).on('page.dt', function (){
                console.log('Page Change Called');
                var updatedRecs = component.get("v.updatedRecs");
                var globalId = component.getGlobalId();
                var sessionTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();
                var info = sessionTable.page.info();
                var newpage = info.page;
                if(oldPage != newpage){
                    if(updatedRecs != null && updatedRecs != "" && Object.keys(updatedRecs).length >0){
                        console.log('---- Pending changes ----');
                        var errorMsg;
                        if(component.get("v.namespace") != '')
                            errorMsg = $A.get("$Label.AxtriaSalesIQTM.Unsaved_Records_Call_Plan");
                        else
                            errorMsg = $A.get("$Label.c.Unsaved_Records_Call_Plan");
                        
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            mode: 'dismissible',
                            type: 'error',
                            message: errorMsg,
                            messageTemplate: errorMsg,
                            messageTemplateData: []
                        });
                        toastEvent.fire();
                        sessionTable.page(oldPage).draw(false);
                    }else{
                        oldPage = newpage;
                        console.log('no changes');
                        sessionTable.page(newpage).draw(false);
                        component.checkDependencyConditionsOnLoad(component.get("v.colStructureHeaderDetailJson"));
                        
                    }
                }else{
                   
                }
            });

            // this event captures sorting event on datatable
            $j(document.getElementById(globalId+'_dataTable')).on('order.dt', function (){
                // condition added for decreasing onload time 
                if(component.get("v.executeSortingEvent") == 'true'){
                    console.log('-------Sorting columns --------------');
                    if(component.get("v.dependencyControlConditions").keys.length > 0)
                        component.checkDependencyConditionsOnLoad(component.get("v.colStructureHeaderDetailJson"));
                }
                
                
            });
        });
        action.setBackground();
        $A.enqueueAction(action); 
    },

    saveData : function(component, event, helper)
    {   
        var namespace = component.get("v.namespace");
        var checkLock = component.get("c.isScenarioLock");
        checkLock.setParams({"scenarioId" : component.get("v.scenarioID")});
        checkLock.setCallback(this,function(response){
            if(response.getReturnValue() == true || response.getReturnValue() == 'true'){
                var message;
                if(namespace != '') 
                    message = $A.get("$Label.AxtriaSalesIQTM.Business_Rule_Running_Warning");
                else
                    message = $A.get("$Label.c.Business_Rule_Running_Warning");

                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    mode: 'dismissible',
                    type: 'Warning',
                    message: message,
                    messageTemplate: message,
                    messageTemplateData: []
                });
                toastEvent.fire();
                console.log('toastEvent', toastEvent);
                return;
            }
            var $j = jQuery.noConflict();
            var updatedRecs = component.get("v.updatedRecs");
            var globalId = component.getGlobalId();
            var colStructure = component.get("v.columnStructure");
            var rowHeight = colStructure.rowHeight;

            var message;
            if(namespace != '') 
                message = $A.get("$Label.AxtriaSalesIQTM.CALL_PLAN_NO_CHANGES_MESSAGE");
            else
                message = $A.get("$Label.c.CALL_PLAN_NO_CHANGES_MESSAGE");

            if(updatedRecs == '')
            {  
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    mode: 'dismissible',
                    type: 'Warning',
                    message: message,
                    messageTemplate: message,
                    messageTemplateData: []
                });
                toastEvent.fire();

                return;
            }else{
                component.validateMandatoryRules();
                var mandatoryFieldsMessage = component.get("v.mandatoryFieldsErrorMessage");
                console.log('error message is :'+mandatoryFieldsMessage);
                if(mandatoryFieldsMessage != ''){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        type: 'error',
                        message: mandatoryFieldsMessage,
                        messageTemplate: mandatoryFieldsMessage,
                        messageTemplateData: []
                    });
                    toastEvent.fire();

                    return;
                }else{
                    console.log('--updatedRecs ', updatedRecs);
                    var pendingRecords = component.get("c.submissionPendingRecords");
                    pendingRecords.setParams({
                        "selectedTeamInstanceId" : component.get("v.selectedTeamInstance"),
                        "selectedPositionId" : component.get("v.selectedPosition")
                    });

                    pendingRecords.setCallback(this, function(response)
                    {
                        var state = response.getState();
                        if (component.isValid() && state === "SUCCESS") 
                        {
                            if((Object.keys(updatedRecs).length + response.getReturnValue()) > component.get("v.columnStructure").maximumRecordsSaved ){
                                var toastMessage = $A.get("$Label.c.Permissible_Save_Limit_Call_Plan")
                                if(component.get("v.namespace") != '')
                                    toastMessage = $A.get("$Label.AxtriaSalesIQTM.Permissible_Save_Limit_Call_Plan")

                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    mode: 'dismissible',
                                    type: 'error',
                                    message: toastMessage,
                                    messageTemplate: toastMessage,
                                    messageTemplateData: ['' + component.get("v.columnStructure").maximumRecordsSaved + '']
                                });
                                toastEvent.fire();
                                return;
                            }

                            var saveFunction = component.get("c.saveCallPlan");
                            saveFunction.setParams({
                                "updatedRecs": JSON.stringify(updatedRecs),
                                "selectedTeamInstanceId" : component.get("v.selectedTeamInstance"),
                                "selectedPositionId" : component.get("v.selectedPosition"),
                                "operationType" : ''
                            });
                            saveFunction.setCallback(this, function(response)
                            {
                                var state = response.getState();
                                if (component.isValid() && state === "SUCCESS") 
                                {
                                    var retResponse = response.getReturnValue();
                                    var sessionTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();
                                    var colIndexOfStatus = sessionTable.column( namespace+'Change_Status__c:name' ).index();
                                    var colIndexOfChangeAction = sessionTable.column( namespace+'Change_Action_Type__c:name' ).index();
                                    var updatedRecsMap = updatedRecs; 
                                    var droppedRows = [];

                                    // Case when there is no change in the rows by changing all the values again back to approved state
                                    if(retResponse != null && retResponse['changedRows'] != null && retResponse['changedRows'].length == 0)
                                    {
                                        if(retResponse['updateStatus'] != null && retResponse['updateStatus'].length != 0)
                                        {
                                            if(updatedRecsMap != '')
                                            {
                                                Object.keys(updatedRecsMap).forEach(function(key) 
                                                {
                                                    if(retResponse['updateStatus'].indexOf(updatedRecsMap[key]['rowIndex'].toString()) != -1)
                                                    {
                                                        sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfStatus).data('No Change');
                                                        sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfStatus).node().innerHTML = '';
                                                        sessionTable.row(updatedRecsMap[key]['rowIndex']).nodes().to$().css("background-color", "#ffffff");

                                                        if(sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).data() == 'Edited')
                                                        {
                                                            sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).data('');
                                                            sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).node().innerHTML = '';
                                                        }
                                                    } 
                                                });
                                            }
                                        } else {
                                            // if no row has changed from approved state, i.e earlier they changed the values and now again sent it back to approved stage, 
                                            // so, clean the action status for all those rows too
                                            if(updatedRecsMap != '')
                                            {
                                                Object.keys(updatedRecsMap).forEach(function(key) 
                                                {
                                                    if(sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).data() == 'Edited')
                                                    {
                                                        sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).data('');
                                                        sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).node().innerHTML = '';   
                                                    }

                                                    // if list of those rows where action status should change back to Added, update change action type for them too
                                                    if(retResponse['addedTargetUpdateStatus'].indexOf(updatedRecsMap[key]['rowIndex'].toString()) != -1)
                                                    {
                                                        sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).data('Added');
                                                        var imgHtml = '<img alt="Added" title="Added" src="' + $A.get('$Resource.icons1')+ '/final/added.png' + '" style="height:'+Math.round(rowHeight/2)+'px;"/>';
                                                        if(sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfStatus).data() == 'Submission Pending' && component.get('v.subInterface') == '' &&  sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).node().innerHTML.indexOf('undoRowButton') == -1)
                                                        {
                                                            imgHtml = imgHtml + '<button class="slds-button undoRowButton slds-button--icon-border" style="margin-left:10px;height:'+(rowHeight-5)+'px;" title="Undo" id="undoRowButton_'+updatedRecsMap[key]['rowIndex']+'"><span class="icon-undo icon-svg slds-icon-text-default undoRowButton"></span></button>';
                                                        }
                                                        sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).node().innerHTML = imgHtml;
                                                        sessionTable.cell(updatedRecsMap[key]['rowIndex'],0).node().innerHTML = "<input disabled type='checkbox' class='slds-checkbox firstCheckbox' name='choosePos' />";
                                                    } 
                                                });
                                            }
                                        }

                                        // Commented this line inorder to fix SPD-5098
                                        try{
                                            sessionTable.fixedColumns().update();
                                        }
                                        catch(err){
                                            console.log('Error ', err);
                                        }
                                        
                                        var toastEvent = $A.get("e.force:showToast");
                                        toastEvent.setParams({
                                            mode: 'dismissible',
                                            type: 'Warning',
                                            message: message,
                                            messageTemplate: message,
                                            messageTemplateData: []
                                        });
                                        toastEvent.fire();  

                                        var evt = component.getEvent("refreshKpiEvent");
                                        evt.setParams({
                                            teamInstanceId : component.get("v.selectedTeamInstance"),
                                            positionId : component.get("v.selectedPosition"),
                                            crType : 'Call_Plan_Change'
                                        });           
                                        evt.fire();
                                        return;
                                    }
                                    
                                    if(updatedRecsMap != '')
                                    {
                                        Object.keys(updatedRecsMap).forEach(function(key) 
                                        {
                                            if(retResponse['changedRows'].indexOf(updatedRecsMap[key]['rowIndex'].toString()) != -1)
                                            {
                                                sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfStatus).data('Submission Pending');
                                                sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfStatus).node().innerHTML = '<img  src="' + $A.get('$Resource.icons1')+ '/final/submission-pending.png' + '" title="Submission Pending" style="height:'+Math.round(rowHeight/2)+'px;"/>';
                                                if(sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).node().innerHTML.indexOf('undoRowButton') == -1)
                                                {
                                                    // if(sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).node().innerHTML == '')
                                                    // {
                                                    //     sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).data('Edited');
                                                    //     sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).node().innerHTML = '<img alt="Edited" title="Edited" src="' + $A.get('$Resource.icons1')+ '/final/Edited.png' + '" />';
                                                        
                                                    // }
                                                    if(sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).node().innerHTML.length > 0)
                                                    {
                                                        sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).node().innerHTML = sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).node().innerHTML + '<button class="slds-button undoRowButton slds-button--icon-border" style="margin-left:10px;height:'+(rowHeight-5)+'px;" title="Undo" id="undoRowButton_'+updatedRecsMap[key]['rowIndex']+'"><span class="icon-undo icon-svg slds-icon-text-default undoRowButton"></span></button>';
                                                    }
                                                    else
                                                    {
                                                        sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).node().innerHTML = '<button class="slds-button undoRowButton slds-button--icon-border" style="height:'+(rowHeight-5)+'px;" title="Undo" id="undoRowButton_'+updatedRecsMap[key]['rowIndex']+'"><span class="icon-undo icon-svg slds-icon-text-default undoRowButton"></span></button>';
                                                    }
                                                    
                                                }
                                                sessionTable.cell(updatedRecsMap[key]['rowIndex'],0).node().innerHTML = "<input disabled type='checkbox' class='slds-checkbox firstCheckbox' name='choosePos' />";
                                            }

                                            if(retResponse['updateStatus'] != null && retResponse['updateStatus'].length != 0)
                                            {
                                                if(retResponse['updateStatus'].indexOf(updatedRecsMap[key]['rowIndex'].toString()) != -1)
                                                {
                                                    sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfStatus).data('No Change');
                                                    sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfStatus).node().innerHTML = '';
                                                    sessionTable.row(updatedRecsMap[key]['rowIndex']).nodes().to$().css("background-color", "#ffffff");
                                                }                                
                                            }
                                            if(updatedRecsMap[key][namespace+'Change_Action_Type__c'] == 'Dropped')
                                            {
                                                droppedRows.push(updatedRecsMap[key]['rowIndex']);
                                            } 

                                            // if list of those rows where action status should change back to Added, update change action type for them too
                                            if(retResponse['addedTargetUpdateStatus'].indexOf(updatedRecsMap[key]['rowIndex'].toString()) != -1)
                                            {
                                                sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).data('Added');
                                                sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).node().innerHTML = '<img alt="Added" title="Added" src="' + $A.get('$Resource.icons1')+ '/final/added.png' + '" style="height:'+Math.round(rowHeight/2)+'px;"/>';
                                                if(sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).node().innerHTML.indexOf('undoRowButton') == -1 && sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfStatus).data() == 'Submission Pending')
                                                {
                                                    sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).node().innerHTML = sessionTable.cell(updatedRecsMap[key]['rowIndex'],colIndexOfChangeAction).node().innerHTML + '<button class="slds-button undoRowButton slds-button--icon-border" style="margin-left:10px;height:'+(rowHeight-5)+'px;" title="Undo" id="undoRowButton_'+updatedRecsMap[key]['rowIndex']+'"><span class="icon-undo icon-svg slds-icon-text-default undoRowButton"></span></button>';
                                                }
                                            } 
                                        });

                                        if(droppedRows.length > 0)
                                        {
                                            droppedRows = droppedRows.sort();
                                            for(var i=0;i<droppedRows.length;i++)
                                            {
                                                sessionTable.row(droppedRows[i]-i).remove().draw();
                                            }    
                                        }
                                    }



                                    // Commented this line inorder to fix SPD-5098
                                    try{
                                        sessionTable.fixedColumns().update();
                                    }
                                    catch(err){
                                        console.log('Error ', err);
                                    }
                                    
                                    component.set("v.updatedRecs", '');

                                    var evt = component.getEvent("refreshKpiEvent");
                                    evt.setParams({
                                        teamInstanceId : component.get("v.selectedTeamInstance"),
                                        positionId : component.get("v.selectedPosition"),
                                        crType : 'Call_Plan_Change'
                                    });           
                                    evt.fire();

                                    //var saveMessage = 'All changes are saved but nothing is submitted yet. To submit changes, please click the submit button.';
                                    
                                    var saveMessage;
                                    if(namespace != '') 
                                        saveMessage = $A.get("$Label.AxtriaSalesIQTM.CALL_PLAN_SAVE_MESSAGE");
                                    else
                                        saveMessage = $A.get("$Label.c.CALL_PLAN_SAVE_MESSAGE");

                                    var toastEvent = $A.get("e.force:showToast");
                                    toastEvent.setParams({
                                        mode: 'dismissible',
                                        type: 'Success',
                                        message: saveMessage,
                                        messageTemplate: saveMessage,
                                        messageTemplateData: []
                                    });
                                    toastEvent.fire();
                                }
                                else if (state === "ERROR" || response.getReturnValue() == null) 
                                {
                                    helper.logException(component, response.getError());
                                }
                            });
                            $A.enqueueAction(saveFunction);
                        }
                        else if (state === "ERROR" || response.getReturnValue() == null) 
                        {
                            helper.logException(component, response.getError());
                        }
                    });
                    $A.enqueueAction(pendingRecords);
                }
            }
        });
        $A.enqueueAction(checkLock);
    },

    confirmSubmitData : function(component,event,helper)
    {   
        var namespace = component.get("v.namespace");
        var checkLock = component.get("c.isScenarioLock");
        checkLock.setParams({"scenarioId" : component.get("v.scenarioID")});
        checkLock.setCallback(this,function(response){
            if(response.getReturnValue() == true || response.getReturnValue() == 'true'){
                var message;
                if(namespace != '' && namespace != undefined) 
                    message = $A.get("$Label.AxtriaSalesIQTM.Business_Rule_Running_Warning");
                else
                    message = $A.get("$Label.c.Business_Rule_Running_Warning");

                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    mode: 'dismissible',
                    type: 'Warning',
                    message: message,
                    messageTemplate: message,
                    messageTemplateData: []
                });
                toastEvent.fire();
                console.log('toastEvent', toastEvent);
                return;
            }

            var $j = jQuery.noConflict();
            // var updatedRecs = JSON.stringify(component.get("v.updatedRecs"));
            var globalId = component.getGlobalId();
            var confrimSubmitCallPlan = component.get("c.confirmSubmitCallPlan");
            confrimSubmitCallPlan.setParams({
                "selectedPositionId": component.get("v.selectedPosition"),
                "selectedTeamInstanceId": component.get("v.selectedTeamInstance"),
                 "profileString" : JSON.stringify(component.get("v.profileMap"))
            });

            var sessionTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();
            confrimSubmitCallPlan.setCallback(this, function(response)
            {
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") 
                {
                    component.createDataTable();
                    var treeHierarchy = component.find('treeHierarchy');
                    treeHierarchy.refreshTree(component.get("v.countryId"));
                    component.set("v.updatedRecs", '');

                    var evt = component.getEvent("refreshKpiEvent");
                    evt.setParams({
                        teamInstanceId : component.get("v.selectedTeamInstance"),
                        positionId : component.get("v.selectedPosition"),
                        crType : 'Call_Plan_Change'
                    });           
                    evt.fire();

                    var messageTemplateData = [];
                    if(response.getReturnValue()[0] == 'Success' || response.getReturnValue()[2] != null)
                    {

                        var crObj = {'label':response.getReturnValue()[2],'url':response.getReturnValue()[3]};
                        messageTemplateData.push(crObj);
                    }
                    else
                    {
                        messageTemplateData.push('');
                    }

                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        type: response.getReturnValue()[0],
                        message: response.getReturnValue()[1],
                        messageTemplate: response.getReturnValue()[1],
                        messageTemplateData: messageTemplateData
                    });
                    toastEvent.fire();
                    console.log('completed');

                    component.set("v.updatedRecs", '');
                }
                else if (state === "ERROR" || response.getReturnValue() == null) 
                {
                    helper.logException(component, response.getError());
                }
            });
            $A.enqueueAction(confrimSubmitCallPlan);
        });
        $A.enqueueAction(checkLock);
    },   

    submitData : function(component, event, helper) {
        var namespace = component.get("v.namespace");
        var checkLock = component.get("c.isScenarioLock");
        checkLock.setParams({"scenarioId" : component.get("v.scenarioID")});
        checkLock.setCallback(this,function(response){
            if(response.getReturnValue() == true || response.getReturnValue() == 'true'){
                var message;
                if(namespace != '' && namespace != undefined) 
                    message = $A.get("$Label.AxtriaSalesIQTM.Business_Rule_Running_Warning");
                else
                    message = $A.get("$Label.c.Business_Rule_Running_Warning");

                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    mode: 'dismissible',
                    type: 'Warning',
                    message: message,
                    messageTemplate: message,
                    messageTemplateData: []
                });
                toastEvent.fire();
                console.log('toastEvent', toastEvent);
                return;
            }
            var $j = jQuery.noConflict();
            var updatedRecs = JSON.stringify(component.get("v.updatedRecs"));
            var globalId = component.getGlobalId();
            var namespace = component.get("v.namespace");
            var no_changes;
            if(namespace != '') 
                no_changes = $A.get("$Label.AxtriaSalesIQTM.CALL_PLAN_NO_CHANGES_MESSAGE");
            else
                no_changes = $A.get("$Label.c.CALL_PLAN_NO_CHANGES_MESSAGE"); 

            component.validateMandatoryRules();
            var mandatoryFieldsMessage = component.get("v.mandatoryFieldsErrorMessage");
            if(mandatoryFieldsMessage != '')
            {
                var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        type: 'error',
                        message: mandatoryFieldsMessage,
                        messageTemplate: mandatoryFieldsMessage,
                        messageTemplateData: []
                    });
                    toastEvent.fire();

                    return;
            }
            else
            {
                var submitCallPlan = component.get("c.submitCallPlan");
                submitCallPlan.setParams({
                    "updatedRecs" : JSON.stringify(component.get("v.updatedRecs")),
                    "selectedPositionId": component.get("v.selectedPosition"),
                    "selectedTeamInstanceId": component.get("v.selectedTeamInstance")
                });

                var sessionTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();
                submitCallPlan.setCallback(this, function(response)
                {
                    var state = response.getState();
                    if (component.isValid() && state === "SUCCESS") 
                    {
                        console.log(response.getReturnValue());
                        if(response.getReturnValue()[0] == 'Error')
                        {

                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                mode: 'dismissible',
                                type: response.getReturnValue()[0],
                                message: response.getReturnValue()[1],
                                messageTemplate: response.getReturnValue()[1],
                            });
                            toastEvent.fire();
                        }
                        else if(response.getReturnValue()[0] === no_changes)
                        {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                mode: 'dismissible',
                                type: 'warning',
                                message: response.getReturnValue()[0],
                                messageTemplate: response.getReturnValue()[0],
                            });
                            toastEvent.fire();
                        }
                        else
                        {   
                            var modalMessage, modelHeaderLabel;
                            if(component.get("v.namespace") != '') {
                                modalMessage = $A.get("$Label.AxtriaSalesIQTM.CALL_PLAN_SUBMIT_CONFIRM_LABEL");  ;
                                modelHeaderLabel = $A.get("$Label.AxtriaSalesIQTM.Call_Plan_Submit_Confirmation_Header_Label"); 
                            } else {
                                modalMessage = $A.get("$Label.c.CALL_PLAN_SUBMIT_CONFIRM_LABEL");  ;
                                modelHeaderLabel = $A.get("$Label.c.Call_Plan_Submit_Confirmation_Header_Label");
                            }
                            $A.createComponent
                            (
                                "c:SalesIQMessageDialog",
                                {
                                    "body": modalMessage,
                                    "recordId" : '',
                                    "onconfirm" : component.getReference("c.confirmSubmitData"),
                                    "moduleName": component.get("v.moduleName"),
                                    "headerLabel" : modelHeaderLabel,
                                    "showHeader" : 'true',
                                    "confirmLabel" : 'Confirm',
                                    "namespace" : component.get("v.namespace"),
                                    "paramsList" : response.getReturnValue(),
                                },
                                function(msgBox)
                                {                
                                    if (component.isValid()) 
                                    {
                                        var targetCmp = component.find('getPopUp');
                                        var body = targetCmp.get("v.body");
                                        body.push(msgBox);
                                        targetCmp.set("v.body", body);
                                    }
                                }
                            );
                        }
                        
                    }
                    else if (state === "ERROR" || response.getReturnValue() == null) 
                    {
                        helper.logException(component, response.getError());
                    }
                });
                $A.enqueueAction(submitCallPlan);
            }
        });
        $A.enqueueAction(checkLock);
    },

    downloadExcel : function(component, event, helper)
    {
        /*var $j = jQuery.noConflict();
        var globalId = component.getGlobalId();
        var sessionTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();
        sessionTable.buttons('.buttons-excel').trigger(); */

        var titleLabel = '';
        if(component.get("v.namespace") != '')
            titleLabel = $A.get("$Label.c.Call_Plan");
        else
            titleLabel = $A.get("$Label.c.Call_Plan");
      

        helper.downloadCSV(component,event,helper,titleLabel);
    },


    undoChanges : function(component, event, helper){
        var $j = jQuery.noConflict();

        var undoCallPlan = component.get("c.undoCallPlan");
        undoCallPlan.setParams({
            "selectedPositionId": component.get("v.selectedPosition"),
            "selectedTeamInstanceId": component.get("v.selectedTeamInstance")
        });

        var globalId = component.getGlobalId();

        //var sessionTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();
        undoCallPlan.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") 
            {
                component.createDataTable();
                var evt = component.getEvent("refreshKpiEvent");
                evt.setParams({
                    teamInstanceId : component.get("v.selectedTeamInstance"),
                    positionId : component.get("v.selectedPosition"),
                    crType : 'Call_Plan_Change'
                });           
                evt.fire();
            }
            else if (state === "ERROR" || response.getReturnValue() == null) 
            {
                helper.logException(component, response.getError());
            }
        });
        $A.enqueueAction(undoCallPlan);

        component.set("v.updatedRecs", '');
        if(document.getElementById(globalId+'searchInputBox'))
        {
            document.getElementById(globalId+'searchInputBox').value = "";
            $A.util.addClass(component.find('clearIcon'), "slds-hide");
        }
        
    },

    openUndoConfirmationPopUp : function(component, event, helper) 
    {
        var namespace = component.get("v.namespace");
        var modalMessage;
        if(namespace != '')
            modalMessage = $A.get("$Label.AxtriaSalesIQTM.CALL_PLAN_UNDO_MESSAGE");
        else
            modalMessage = $A.get("$Label.c.CALL_PLAN_UNDO_MESSAGE");

        //var modalMessage = 'Are you sure you want to undo the changes? All unsubmitted and saved changes will be lost.';
        //component.set("v.modalMessage", modalMessage);
        $A.createComponent
        (
            "c:SalesIQMessageDialog",
            {
                "body": modalMessage,
                "namespace" : component.get("v.namespace"),
                "onconfirm" : component.getReference("c.undoChanges"),
                "aura:id" : "undoRowMessageDialog"
            },
            function(cmp)
            {                
                if (component.isValid()) 
                {
                    var targetCmp = component.find("getPopUp");
                    var body = targetCmp.get("v.body");
                    body.push(cmp);
                    targetCmp.set("v.body", body);
                }
            }
        );
    },

    undoRowChanges : function(component,event,helper)
    {
        console.log("in undo row");
        var $j = jQuery.noConflict();
        var namespace = component.get("v.namespace");
        var globalId = component.getGlobalId();
        var colStructure = component.get("v.columnStructure");
        var rowHeight = colStructure.rowHeight;
        var undoRowAction = component.get("c.undoRow");
        undoRowAction.setParams({
            "selectedPositionId": component.get("v.selectedPosition"),
            "selectedTeamInstanceId": component.get("v.selectedTeamInstance"),
            "recordId" : component.get("v.selectedPACPId"),
            "columnHeadersAPIname" : colStructure.allFieldsAPI,
        });
        undoRowAction.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") 
            {
                if(component.get("v.isUncheckEvent") == 'false'){
                    var evt = component.getEvent("refreshKpiEvent");
                    evt.setParams({
                        teamInstanceId : component.get("v.selectedTeamInstance"),
                        positionId : component.get("v.selectedPosition"),
                        crType : 'Call_Plan_Change'
                    });           
                    evt.fire();
                }
                // component.createDataTable();
                var retResponse = response.getReturnValue();
                var sessionTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();
                
                // var colStructureHeaderJson = JSON.parse(colStructure.columnHeaderJson);
                var colStructureHeaderDetailJson = JSON.parse(colStructure.columnHeaderDetailJson);
                var colStructureHeaderJson = JSON.parse(colStructure.columnHeaderJson);
                var columnHeaderAPIname = colStructure.allFieldsAPI;
                var idxRow = component.get("v.undoRowNumber");
                var colIndexOfChangeAction = sessionTable.column( namespace+'Change_Action_Type__c:name' ).index();
                // var row = sessionTable.rows(idxRow).data();
                // var data = row.data().toArray()[idxRow];
                var rowData = retResponse;
                
                if((sessionTable.cell(idxRow,colIndexOfChangeAction).data() == 'Dropped' && retResponse[namespace+'Change_Action_Type__c'] != 'Dropped') || (sessionTable.cell(idxRow,colIndexOfChangeAction).data() == 'Added' && retResponse[namespace+'Change_Action_Type__c'] != 'Added'))
                {
                    sessionTable.row(idxRow).remove().draw();
                    $j('button[class*=undoRowButton]').each(function(index)
                    {
                        if(parseInt($j(this).attr("id").split('_')[1]) > idxRow)
                        {
                            $j(this).attr("id",'undoRowButton_'+ (parseInt($j(this).attr("id").split('_')[1]) - 1));
                        }
                    });
                    if(sessionTable.data().count() == 0)
                    {
                        var card = component.find('no_record_card');
                        document.getElementById(globalId+'_tableContainer').style.display = 'none';
                        $A.util.removeClass(card, "slds-hide");
                        component.set("v.showSearchBar", false);
                        return;
                    }
                    else if(document.getElementById(globalId+'_tableContainer').style.display == 'none')
                    {
                        var card = component.find('no_record_card');
                        document.getElementById(globalId+'_tableContainer').style.display = 'block';
                        $A.util.addClass(card, "slds-hide");
                        component.set("v.showSearchBar", true);
                    }
                }
                else
                {
                    for(var col=0;col< colStructureHeaderDetailJson.length; col++)
                    {
                        var cellData;
                        // if(retResponse[colStructureHeaderDetailJson[col].fieldApiName])
                        // {
                        sessionTable.cell(idxRow,col).data(retResponse[colStructureHeaderDetailJson[col].fieldApiName]); 
                        // }
                        cellData = retResponse[colStructureHeaderDetailJson[col].fieldApiName];
                        var td = sessionTable.cell(idxRow,col).node();


                        if(colStructureHeaderDetailJson[col].fieldApiName == namespace+'Change_Status__c')
                        {
                                if(cellData == 'Submission Pending')
                                {
                                    var imgHtml = '<img alt="' + cellData + '" title="' + cellData + '" src="' + $A.get('$Resource.icons1')+ '/final/submission-pending.png' + '" />';                               
                                }
                                 else if(cellData == 'Approved'){
                                    var imgHtml = '<img alt="' + cellData + '" title="' + cellData + '" src="' + $A.get('$Resource.icons1') + '/final/Approved.png' +'" />';                          
                                }
                                else if(cellData == 'Approval Pending'){
                                    var imgHtml = '<img alt="' + cellData + '" title="' + cellData + '" src="' + $A.get('$Resource.icons1') + '/final/pending.png' + '" />';     
                                }
                                else{
                                   var imgHtml = '';
                                }
                                $j(td).removeClass('slds-text-align_left ').addClass('slds-text-align_center');
                                $j(td).html(imgHtml);
                        }
                        else if(colStructureHeaderDetailJson[col].fieldApiName == namespace+'Change_Action_Type__c')
                        {
                            if(cellData == 'Added')
                            {
                                var imgHtml = '<img alt="' + cellData + '" title="' + cellData + '" src="' + $A.get('$Resource.icons1')+ '/final/added.png' + '" />'
                                if(rowData[namespace+'Change_Status__c'] == 'Submission Pending' && component.get('v.subInterface') == '')
                                {
                                    imgHtml = imgHtml + '<button class="slds-button undoRowButton slds-button--icon-border" style="margin-left:10px;" title="Undo" id="undoRowButton_'+row+'"><span class="icon-undo icon-svg slds-icon-text-default undoRowButton"></span></button>';
                                }
                                
                            }
                            else if(cellData == 'Dropped')
                            {
                                var imgHtml = '<img alt="' + cellData + '" title="' + cellData + '" src="' + $A.get('$Resource.icons1') + '/final/removed.png' +'" />';
                                if(rowData[namespace+'Change_Status__c'] == 'Submission Pending' && component.get('v.subInterface') == '')
                                {
                                    imgHtml = imgHtml + '<button class="slds-button undoRowButton slds-button--icon-border" style="margin-left:10px;" title="Undo" id="undoRowButton_'+row+'"><span class="icon-undo icon-svg slds-icon-text-default undoRowButton"></span></button>';
                                }                          
                            }
                            else if(cellData == 'Edited')
                            {
                                var imgHtml = '<img alt="' + cellData + '" title="' + cellData + '" src="' + $A.get('$Resource.icons1') + '/final/Edited.png' +'" />';
                                if(rowData[namespace+'Change_Status__c'] == 'Submission Pending' && component.get('v.subInterface') == '')
                                {
                                    imgHtml = imgHtml + '<button class="slds-button undoRowButton slds-button--icon-border" style="margin-left:10px;" title="Undo" id="undoRowButton_'+row+'"><span class="icon-undo icon-svg slds-icon-text-default undoRowButton"></span></button>';
                                }                          
                            }
                            
                            else
                            {
                                var imgHtml = '';
                                if(rowData[namespace+'Change_Status__c'] == 'Submission Pending' && component.get('v.subInterface') == '')
                                {
                                    imgHtml = imgHtml + '<button class="slds-button undoRowButton slds-button--icon-border" title="Undo" id="undoRowButton_'+row+'"><span class="icon-undo icon-svg slds-icon-text-default undoRowButton"></span></button>';
                                }
                            }
                            $j(td).removeClass('slds-text-align_left ').addClass('slds-text-align_center');
                            $j(td).html(imgHtml);
                        }
                        else if(colStructureHeaderDetailJson[col].fieldDataType=='Picklist')
                        {
                            var selectHtml = "";
                            if(component.get("v.isCRraised") == true  || (rowData[namespace+'Change_Action_Type__c'] == 'Dropped' && component.get('v.subInterface') == ''))
                                selectHtml = "<select disabled name=\"picklistInput\" class=\"slds-select\">" ;
                            else
                                selectHtml = "<select name=\"picklistInput\" class=\"slds-select\">" ;

                            var data = colStructureHeaderDetailJson[col].dynamicValuesString;

                            selectHtml = selectHtml + "<option value='None' >" + 'None' + "</option>";

                            if(data != undefined && data != 'undefined'  && data != null && data != '') 
                            {
                                
                                var finaldata=data.split(',');
                                for(var key=0;key<finaldata.length;key++)
                                {

                                    if(colStructureHeaderDetailJson[col].isEditable == true)
                                    {
                                        if(finaldata[key]==rowData[colStructureHeaderDetailJson[col].fieldApiName])
                                        {
                                            selectHtml = selectHtml+ "<option selected = \"selected\" value='"+finaldata[key]+"'>"+finaldata[key]+" </option>"; 
                                        }
                                        else
                                        {
                                            selectHtml = selectHtml+ "<option value='"+finaldata[key]+"'>"+finaldata[key]+"</option>";
                                        }
                                    }
                                    else
                                    {
                                        selectHtml="<output title='" + rowData[colStructureHeaderDetailJson[col].fieldApiName] + "' >" + rowData[colStructureHeaderDetailJson[col].fieldApiName]+ "</output>";
                                        break;
                                    }

                                }
                            }
                            else{
                                selectHtml="<output title='' ></output>";
                            }

                            if(colStructureHeaderDetailJson[col].isEditable)
                                selectHtml = selectHtml + '</select>';
                            $j(td).html(selectHtml);
                        }
                        else if(colStructureHeaderDetailJson[col].fieldDataType == 'Date'){
                            if(rowData[colStructureHeaderJson[col].data] != undefined && rowData[colStructureHeaderJson[col].data] != null){
                                var fetchedDate = new Date(rowData[colStructureHeaderJson[col].data]);
                                var convertedDate = (fetchedDate.getMonth()+1) + "/" + fetchedDate.getDate() + "/" +  fetchedDate.getFullYear() ;
                                var selectHtml="<output title='" + convertedDate + "' >" + convertedDate + "</output>";
                                $j(td).html(selectHtml);
                            }
                            else{
                                var convertedDate = '';
                                var selectHtml="<output title='" + convertedDate + "' >" + convertedDate + "</output>";
                                $j(td).html(selectHtml);
                            }
                        }
                        else if(colStructureHeaderDetailJson[col].fieldDataType=='Text')
                        {
                            var selectHtml = "";
                            if(colStructureHeaderJson[col].data.indexOf('.') == -1) {
                                
                                if(rowData[colStructureHeaderJson[col].data] == undefined || rowData[colStructureHeaderJson[col].data] == 'undefined')
                                    rowData[colStructureHeaderJson[col].data] = '';
                                
                                if(colStructureHeaderDetailJson[col].isEditable)
                                    selectHtml="<input class=\"textInput slds-input\" type=\"text\" value='"+rowData[colStructureHeaderJson[col].data]+"'"+"\>";
                                else
                                    var selectHtml="<output title='" + rowData[colStructureHeaderJson[col].data] + "' >" + rowData[colStructureHeaderJson[col].data]+ "</output>";
                            }
                            else if(colStructureHeaderJson[col].data.split('.').length == 2){
                                var columnSplit = colStructureHeaderJson[col].data.split('.');
                                //console.log('--columnSplit ', columnSplit);

                                if(rowData[columnSplit[0]][columnSplit[1]] == undefined || rowData[columnSplit[0]][columnSplit[1]] == 'undefined')
                                    rowData[columnSplit[0]][columnSplit[1]] = '';

                                if(colStructureHeaderDetailJson[col].isEditable)
                                    selectHtml="<input class=\"textInput slds-input\" type=\"text\" value='"+rowData[columnSplit[0]][columnSplit[1]]+"'"+"\>";
                                else
                                    selectHtml="<output title='" + rowData[columnSplit[0]][columnSplit[1]] + "' >" + rowData[columnSplit[0]][columnSplit[1]] + "</output>";
                            }
                            else{
                                console.log('--inside third');
                                var columnSplit = colStructureHeaderJson[col].data.split('.');
                                //console.log('--columnSplit ', columnSplit);
                                var cellValue = '';
                                if(!(rowData[columnSplit[0]][columnSplit[1]] == undefined || rowData[columnSplit[0]][columnSplit[1]] == 'undefined' || rowData[columnSplit[0]][columnSplit[1]][columnSplit[2]] == undefined || rowData[columnSplit[0]][columnSplit[1]][columnSplit[2]] == 'undefined')){
                                    cellValue = rowData[columnSplit[0]][columnSplit[1]][columnSplit[2]];
                                    console.log('inisde if');
                                }

                                if(colStructureHeaderDetailJson[col].isEditable)
                                    selectHtml="<input class=\"textInput slds-input\" type=\"text\" value='"+cellValue+"'"+"\>";
                                else
                                    selectHtml="<output title='" + cellValue + "' >" + cellValue + "</output>";
                            }

                            if(component.get("v.isCRraised") == true  || (rowData[namespace+'Change_Action_Type__c'] == 'Dropped' && component.get('v.subInterface') == '')){
                                selectHtml = selectHtml.replace("<input", "<input disabled");
                            }
                            $j(td).html(selectHtml);
                            $j(td).css('text-align','left');
                        }
                        else if(colStructureHeaderDetailJson[col].fieldDataType=='Number')
                        {
                            var selectHtml = "";
                            if(colStructureHeaderJson[col].data.indexOf('.') == -1) {
                                
                                if(rowData[colStructureHeaderJson[col].data] == undefined || rowData[colStructureHeaderJson[col].data] == 'undefined')
                                    rowData[colStructureHeaderJson[col].data] = '';
                                
                                if(colStructureHeaderDetailJson[col].isEditable)
                                    selectHtml="<input class=\"textInput slds-input\" type=\"text\" value='"+rowData[colStructureHeaderJson[col].data]+"'"+"\>";
                                else
                                    var selectHtml="<output title='" + rowData[colStructureHeaderJson[col].data] + "' >" + rowData[colStructureHeaderJson[col].data]+ "</output>";
                            }
                            else if(colStructureHeaderJson[col].data.split('.').length == 2){
                                var columnSplit = colStructureHeaderJson[col].data.split('.');
                                //console.log('--columnSplit ', columnSplit);

                                if(rowData[columnSplit[0]][columnSplit[1]] == undefined || rowData[columnSplit[0]][columnSplit[1]] == 'undefined')
                                    rowData[columnSplit[0]][columnSplit[1]] = '';

                                if(colStructureHeaderDetailJson[col].isEditable)
                                    selectHtml="<input class=\"textInput slds-input\" type=\"text\" value='"+rowData[columnSplit[0]][columnSplit[1]]+"'"+"\>";
                                else
                                    selectHtml="<output title='" + rowData[columnSplit[0]][columnSplit[1]] + "' >" + rowData[columnSplit[0]][columnSplit[1]] + "</output>";
                            }
                            else{
                                console.log('--inside third');
                                var columnSplit = colStructureHeaderJson[col].data.split('.');
                                //console.log('--columnSplit ', columnSplit);
                                var cellValue = '';
                                if(!(rowData[columnSplit[0]][columnSplit[1]] == undefined || rowData[columnSplit[0]][columnSplit[1]] == 'undefined' || rowData[columnSplit[0]][columnSplit[1]][columnSplit[2]] == undefined || rowData[columnSplit[0]][columnSplit[1]][columnSplit[2]] == 'undefined')){
                                    cellValue = rowData[columnSplit[0]][columnSplit[1]][columnSplit[2]];
                                    console.log('inisde if');
                                }

                                if(colStructureHeaderDetailJson[col].isEditable)
                                    selectHtml="<input class=\"textInput slds-input\" type=\"text\" value='"+cellValue+"'"+"\>";
                                else
                                    selectHtml="<output title='" + cellValue + "' >" + cellValue + "</output>";
                            }

                            $j(td).html(selectHtml);
                            $j(td).css('text-align','center');
                        }
                        else if(colStructureHeaderDetailJson[col].fieldDataType=='Boolean')
                        {
                            var selectHtml = "";
                            if(colStructureHeaderJson[col].data.indexOf('.') == -1) 
                            {
                                
                                if(rowData[colStructureHeaderJson[col].data] == undefined || rowData[colStructureHeaderJson[col].data] == 'undefined')
                                    rowData[colStructureHeaderJson[col].data] = '';
                                
                                if(colStructureHeaderDetailJson[col].isEditable){
                                    if(rowData[colStructureHeaderJson[col].data] == true)
                                        selectHtml="<input class=\"checkboxInput slds-input\" type=\"checkbox\" checked value='"+rowData[colStructureHeaderJson[col].data]+"'"+"\>";
                                    else
                                        selectHtml="<input class=\"checkboxInput slds-input\" type=\"checkbox\" value='"+rowData[colStructureHeaderJson[col].data]+"'"+"\>";
                                }
                                else
                                    if(rowData[colStructureHeaderJson[col].data] == true)
                                        selectHtml = "<input type='checkbox' checked value="+rowData[colStructureHeaderJson[col].data]+" disabled =\"disabled\" />";
                                    else
                                        selectHtml = "<input type='checkbox' value="+rowData[colStructureHeaderJson[col].data]+" disabled =\"disabled\" />";
                            }
                            else
                            {
                                var columnSplit = colStructureHeaderJson[col].data.split('.');
                                
                                if(rowData[columnSplit[0]][columnSplit[1]] == undefined || rowData[columnSplit[0]][columnSplit[1]] == 'undefined')
                                    rowData[columnSplit[0]][columnSplit[1]] = '';

                                if(colStructureHeaderDetailJson[col].isEditable)
                                    selectHtml="<input class=\"checkboxInput slds-input\" type=\"checkbox\" value='"+rowData[columnSplit[0]][columnSplit[1]]+"'"+"\>";
                                else
                                    selectHtml = "<input type='checkbox'  value="+rowData[columnSplit[0]][columnSplit[1]]+" disabled =\"disabled\" />";
                            }

                            if(component.get("v.isCRraised") == true || (rowData[namespace+'Change_Action_Type__c'] == 'Dropped' && component.get('v.subInterface') == '')){
                                selectHtml = selectHtml.replace("<input", "<input disabled");
                            }
                            $j(td).html(selectHtml);
                        }
                        else if(colStructureHeaderDetailJson[col].fieldDataType=='Checkbox')
                        {
                            if((rowData[namespace+'Change_Action_Type__c'] == 'Dropped' && component.get('v.subInterface') == ''))
                            {
                                var selectHtml = "<input disabled type='checkbox' checked='checked' class='slds-checkbox firstCheckbox' name='choosePos' />";
                            }
                            else if((rowData[namespace+'Change_Status__c'] == 'Submission Pending' && component.get('v.subInterface') == ''))
                            {
                                var selectHtml = "<input disabled type='checkbox' class='slds-checkbox firstCheckbox addCheckbox' name='choosePos' />";   
                            }
                            else if(component.get("v.subInterface") == '')
                            {
                                var selectHtml = "<input type='checkbox' class='slds-checkbox firstCheckbox' name='choosePos' />";
                            }
                            else
                            {
                                var selectHtml = "<input type='checkbox' class='slds-checkbox firstCheckbox addCheckbox' name='choosePos' />";   
                            }

                            if(component.get("v.isCRraised") == true)
                            {
                                selectHtml = selectHtml.replace("<input", "<input disabled");
                            }

                            $j(td).html(selectHtml);
                        }

                        $j(td).css('font-size',Math.round(rowHeight/2)+'px');
                        $j(td).children('img').css('height',Math.round(rowHeight/2)+'px');
                        $j(td).children('select').css('height',(rowHeight-5)+'px');
                        $j(td).children('input').css('line-height',(rowHeight-5)+'px').css('min-height',(rowHeight-5)+'px');
                        $j(td).children('button[class*=undoRowButton]').css('height',(rowHeight-5)+'px');
                    }
                }

                try{
                    sessionTable.fixedColumns().update();
                }
                catch(err){
                    console.log('Error ', err);
                }

                
            }
            else if (state === "ERROR" || response.getReturnValue() == null) 
            {
                helper.logException(component, response.getError());
            }
        });
        $A.enqueueAction(undoRowAction);

    },

    openAddDropTarget : function(component, event, helper)
    {
        component.set("v.isOpenAccountUniverse", true);
        var AddDropTargetCmp = component.find('AddDropTargetCmp');
        AddDropTargetCmp.showPopup(component.get("v.countryId"));
    },

    viewChange: function(component, event, helper) 
    {
        var params = event.getParam('arguments');
        console.log(params);
        if (params) {
            var tableView = params.selectedView;
            var whereClausePACP = params.whereClausePACP;
        }
        component.set("v.listViewId",tableView);
        component.set("v.whereClausePACP", whereClausePACP);
        console.log(tableView);
        console.log('whereClausePACP : '+whereClausePACP);
        component.initializeDataTable(component, event, helper);
    },

    checkDependencyConditions : function(component,event){
        try{
        var $j = jQuery.noConflict();

        var globalId = component.getGlobalId();
        /*if(document.getElementById(globalId+'searchInputBox'))
            document.getElementById(globalId+'searchInputBox').disabled = true;*/
        
        var dependencyConditions = JSON.parse(JSON.stringify(component.get("v.dependencyControlConditions")))[0];
        var namespace = component.get("v.namespace");
        var updatedRecs = event.getParam('arguments').updatedRecs;
        var attributeAPIname = event.getParam('arguments').attributeAPIname;
        var colStructure = component.get("v.columnStructure");
        var rowHeight = colStructure.rowHeight;
        var colStructureHeaderDetailJson = event.getParam('arguments').colStructureHeaderDetailJson;
        var idxRow = event.getParam('arguments').idxRow;
        //var sessionTable = $j('#dataTable').DataTable();
        var sessionTable = event.getParam('arguments').sessionTable;
        var row = sessionTable.rows(idxRow).data();
        var data = row.data().toArray()[idxRow];
        Object.keys(dependencyConditions).forEach( function(key) {
            var cellData1 = '';
            var cellData2 = '';
            var dc = '';
            var controllingField1 = key.split(';')[0];
            var controllingField2 = key.split(';')[1];
            if(attributeAPIname == controllingField2){
                controllingField1 = controllingField2;
            }
            for(var index=0; index < dependencyConditions[key].length; index++)
            {
                if(controllingField1 == attributeAPIname  && (cellData1 == '' || cellData2 == ''))
                {
                    if(!dependencyConditions[key][index][namespace+'Mandatory__c'])
                    {
                        dc = dependencyConditions[key][index];

                        var entryValue1 = '';
                        var field1 = '';
                        var operator1 = '';
                        var value1 = '';
                        if(dc[namespace+'Entry_Criteria_1__c'] != undefined){
                            entryValue1 = dc[namespace+'Entry_Criteria_1__c'];
                            field1 = entryValue1.split(' ')[0];
                            operator1 = entryValue1.split(' ')[1];
                            if(entryValue1.split(' ')[3] != undefined)
                                value1 =  entryValue1.split(' ')[2] + ' '+entryValue1.split(' ')[3];
                            else
                                value1 =  entryValue1.split(' ')[2];
                        }

                        if(value1 == "' '"){
                            value1 = '';
                        }

                        var entryValue2 = '';
                        var field2 = '';
                        var operator2 = '';
                        var value2 = ''; 

                        if(dc[namespace+'Entry_Criteria_2__c'] != undefined){
                            entryValue2 = dc[namespace+'Entry_Criteria_2__c'];
                            field2 = entryValue2.split(' ')[0];
                            operator2 = entryValue2.split(' ')[1];
                            if(entryValue2.split(' ')[3] != undefined)
                                value2 =  entryValue2.split(' ')[2] + ' '+entryValue2.split(' ')[3];
                            else
                                value2 =  entryValue2.split(' ')[2];
                        }
                        
                        if(value2 == "' '"){
                            value2 = '';
                        }


                        var updatedValue1 = '';
                        var updatedValue2 = '';
                        var isValue = false;
                        if(value1.indexOf('__c') == -1){
                            isValue = true;
                            data = row.data().toArray()[idxRow];
                            
                            if(updatedRecs != undefined && updatedRecs != ''){
                                if(updatedRecs[data.Id][field1] == undefined)
                                    updatedValue1 = data[field1];
                                else{
                                    if(updatedRecs[data.Id][field1] == ''){
                                        updatedValue1 = '';
                                    }else{
                                        updatedValue1 = updatedRecs[data.Id][field1];
                                    }
                                }
                            }else{
                                updatedValue1 = data[field1];
                            }
                        }else{
                            //value1 will be treated as field name
                            updatedValue2 = data[value1];
                        }
                  
                        var updatedValue3 = '';
                        var updatedValue4 = '';
                        var isValue2 = false;
                        if(value2.indexOf('__c') == -1){
                            isValue2 = true;
                            data = row.data().toArray()[idxRow];
                            if(updatedRecs != undefined && updatedRecs != ''){
                                if(updatedRecs[data.Id][field2] == undefined){
                                    updatedValue4 = '';
                                }else{
                                    updatedValue4 = updatedRecs[data.Id][field2];
                                }
                            }else{
                                updatedValue4 = data[field2];
                            }
                        }else{
                            //value2 will be treated as field name
                            updatedValue4 = data[value2];
                        }

                        var updatedValuefield2 = '';
                        if(field2.indexOf('__c') !== -1){
                            if(updatedRecs != undefined && updatedRecs != ''){
                                if(updatedRecs[data.Id][field2] == undefined )
                                     updatedValuefield2 = data[field2];
                                else
                                    updatedValuefield2 = updatedRecs[data.Id][field2];
                            }else{
                                updatedValuefield2 = data[field2];
                            }
                            
                        }
                        //case 1: 1 entry_Criteria
                        if(entryValue1 != '' && entryValue2 == ''){
                            console.log('************* case 1 *************');
                            if(operator1 == '='){
                                if(isValue){
                                    if(value1 == updatedValue1 || (value1 == updatedValue1+""))
                                    {
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                        
                                    }
                                }else{
                                    if(updatedValue1 == updatedValue2 || (updatedValue1 == updatedValue2+"")){
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                    }
                                }
                            }else if(operator1 == '>'){
                                if(isValue){
                                   if(value1 > updatedValue1 || value1 > updatedValue1+"" )
                                    {
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                        
                                    }
                                }else{
                                    if(updatedValue1 > updatedValue2 || updatedValue1 > updatedValue2+"")
                                    {
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                    }
                                }      
                            }else if(operator1 == '<'){
                                if(isValue){
                                    if(value1 < updatedValue1 || value1 < updatedValue1+"" )
                                    {
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                        
                                    }
                                }else{
                                    if(updatedValue1 < updatedValue2 || updatedValue1 < updatedValue2+"")
                                    {
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c']
                                        }
                                    }
                                }
                            }
                        }
                        //case:2 - 2 entryCriteria
                        if(entryValue1 != '' && entryValue2 != '')
                        {
                            console.log('************* case 2*************');
                            if(operator1 == '='){
                                if(operator2 == '='){
                                    if((value1 == updatedValue1 || value1 == updatedValue2) && (value2 == updatedValuefield2 || updatedValue4 == updatedValuefield2))
                                    {
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c']
                                        }
                                    }
                                }else if(operator2 == '>'){
                                    if((value1 == updatedValue1 || value1 == updatedValue2) && (value2 > updatedValuefield2 || updatedValuefield2 > updatedValue4)){
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                    }

                                }else if(operator2 == '<'){
                                    if((value1 == updatedValue1 || value1 == updatedValue2) && (updatedValuefield2 < value2 || updatedValuefield2 < updatedValue4)){
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c']
                                        }
                                    }

                                }else if(operator2 == '!='){
                                    if((value1 == updatedValue1 || value1 == updatedValue2) && (value2 != updatedValuefield2 || updatedValue4 != updatedValuefield2)){
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                    }

                                }
                            }
                            if(operator1 == '>'){
                                if(operator2 == '='){
                                    if((value1 > updatedValue1 || value1 > updatedValue2) && (value2 == updatedValuefield2 || updatedValue4 == updatedValuefield2))
                                    {
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                    }

                                }else if(operator2 == '>'){
                                    if((value1 > updatedValue1 || value1 > updatedValue2) && (value2 > updatedValuefield2 || updatedValuefield2 > updatedValue4)){
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                    }

                                }else if(operator2 == '<'){
                                    if((value1 > updatedValue1 || value1 > updatedValue2) && (value2 < updatedValuefield2 || updatedValuefield2 < updatedValue4)){
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                    }

                                }else if(operator2 == '!='){
                                    if((value1 > updatedValue1 || value1 > updatedValue2) && (value2 != updatedValuefield2 || updatedValuefield2 != updatedValue4)){
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                    }
                                    
                                }

                            }
                            if(operator1 == '<'){
                                if(operator2 == '='){
                                    if((value1 < updatedValue1 || value1 < updatedValue2) && (value2 == updatedValuefield2 || updatedValuefield2 == updatedValue4)){
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                    }

                                }else if(operator2 == '>'){
                                    if((value1 < updatedValue1 || value1 < updatedValue2) && (value2 > updatedValuefield2 || updatedValuefield2 > updatedValue4)){
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                    }

                                }else if(operator2 == '<'){
                                    if((value1 < updatedValue1 || value1 < updatedValue2) && (value2 < updatedValuefield2 || updatedValuefield2 < updatedValue4)){
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                    }

                                }else if(operator2 == '!='){
                                    if((value1 < updatedValue1 || value1 < updatedValue2) && (value2 != updatedValuefield2 || updatedValuefield2 != updatedValue4))
                                    {
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                    }
                                    
                                }

                            }
                            if(operator1 == '!='){
                                if(operator2 == '='){
                                    if((value1 != updatedValue1 || value1 != updatedValue2) && (value2 == updatedValuefield2 || updatedValuefield2 == updatedValue4))
                                    {
                                        
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                        
                                    }

                                }else if(operator2 == '>'){
                                    if((value1 != updatedValue1 || value1 != updatedValue2) && (value2 > updatedValuefield2 || updatedValuefield2 > updatedValue4))
                                    {
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c']
                                        }
                                    }

                                }else if(operator2 == '<'){
                                    if((value1 != updatedValue1 || value1 != updatedValue2) && (value2 < updatedValuefield2 || updatedValuefield2 < updatedValue4))
                                    {
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                    }

                                }else if(operator2 == '!='){
                                    if((value1 != updatedValue1 || value1 != updatedValue2) && (value2 != updatedValuefield2 || updatedValuefield2 != updatedValue4))
                                    {
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                    }
                                }
                            }
                        }

                        console.log(' ************ final data ****** ');
                        console.log('cellData1 : '+cellData1);
                        console.log('cellData2 : '+cellData2);
                        if((cellData1 != '' || cellData2 != '') && dc != ''){
                            var selectHtml1 = "";   
                            var selectHtml2 = "";
                            for(var i=0;i< colStructureHeaderDetailJson.length; i++)
                            {
                                console.log('colStructureHeaderDetailJson[i] : ');
                                console.log(colStructureHeaderDetailJson[i].fieldApiName);
                                console.log(dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_1__c']);
                                if(colStructureHeaderDetailJson[i].fieldApiName == dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_1__c'] )
                                { 
                                    
                                    if(colStructureHeaderDetailJson[i].fieldDataType == 'Picklist')
                                    {
                                    
                                        if(component.get("v.isCRraised") == true || !colStructureHeaderDetailJson[i].isEditable){
                                            selectHtml1 = "<select disabled name=\"picklistInput\" class=\"slds-select\">" ;
                                        }   
                                        else
                                            selectHtml1 = "<select name=\"picklistInput\" class=\"slds-select\">" ;

                                        var data = cellData1;
                                        if(data != undefined && data != 'undefined') {
                                        
                                            var finaldata = data.split(',');
                                            for(var key=0;key<finaldata.length;key++)
                                            {
                                                if(key == 0){

                                                    if(finaldata[key] == "' '"){
                                                        finaldata[key] = '';
                                                    }

                                                    data = row.data().toArray()[idxRow];
                                                    selectHtml1 = selectHtml1 + "<option selected = \"selected\" value='"+finaldata[key]+"'>"+finaldata[key]+" </option>";  
                                                    updatedRecs[data.Id][colStructureHeaderDetailJson[i].fieldApiName] = finaldata[key];
                                                    sessionTable.cell(idxRow,i).data(finaldata[0]);
                                                    attributeAPIname = colStructureHeaderDetailJson[i].fieldApiName;
                                                }
                                                else
                                                {
                                                    selectHtml1 = selectHtml1 + "<option value='"+finaldata[key]+"'>"+finaldata[key]+" </option>"; 
                                                }
                                            }
                                        }
                                        selectHtml1 = selectHtml1 + '</select>';
                                        
                                    }
                                    else if(colStructureHeaderDetailJson[i].fieldDataType == 'Text' || colStructureHeaderDetailJson[i].fieldDataType == 'Formula'){
                                        if(colStructureHeaderDetailJson[i].isEditable){
                                            data = row.data().toArray()[idxRow];

                                            if(cellData1 == "' '"){
                                                cellData1 = '';
                                            }

                                            selectHtml1= selectHtml1 + "<input class=\"textInput slds-input\" type=\"text\" value='"+cellData1+"'"+"\>";
                                            updatedRecs[data.Id][colStructureHeaderDetailJson[i].fieldApiName] = cellData1;
                                            sessionTable.cell(idxRow,i).data(cellData1);
                                            
                                        }
                                        else{
                                            if(cellData1 == '--None--' || cellData1 == 'None'){
                                                selectHtml1="<output title='' ></output>";
                                            }else{
                                                selectHtml1="<output title='" + cellData1 + "' >" + cellData1+ "</output>";
                                            }
                                            
                                        }

                                        attributeAPIname = colStructureHeaderDetailJson[i].fieldApiName;
                                            
                                    }
                                    else if(colStructureHeaderDetailJson[i].fieldDataType == 'Boolean'){
                                        
                                        if(colStructureHeaderDetailJson[i].isEditable){
                                            if(cellData1 == 'true')
                                                selectHtml1 ="<input class=\"checkboxInput slds-input\" type=\"checkbox\" checked value='"+cellData1+"'"+"\>";
                                            else
                                                selectHtml1 ="<input class=\"checkboxInput slds-input\" type=\"checkbox\" value='"+cellData1+"'"+"\>";
                                        }
                                        else{
                                            if(cellData1 == 'true')
                                                selectHtml1 = "<input class=\"checkboxInput slds-input\" type='checkbox' checked value="+cellData1+" disabled =\"disabled\" />";
                                            else
                                                selectHtml1 = "<input class=\"checkboxInput slds-input\" type='checkbox' value="+cellData1+" disabled =\"disabled\" />";
                                        }

                                        data = row.data().toArray()[idxRow];
                                        sessionTable.cell(idxRow,i).data(celldata1);
                                        updatedRecs[data.Id][colStructureHeaderDetailJson[i].fieldApiName] = cellData1;
                                        attributeAPIname = colStructureHeaderDetailJson[i].fieldApiName;
                                        
                                    }
                                }

                                if(dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_2__c'] != undefined){
                                    if(colStructureHeaderDetailJson[i].fieldApiName == dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_2__c'] ){
                                        
                                        if(colStructureHeaderDetailJson[i].fieldDataType == 'Picklist')
                                        {
                                            if(component.get("v.isCRraised") == true || !colStructureHeaderDetailJson[i].isEditable){
                                                selectHtml2 = "<select disabled name=\"picklistInput\" class=\"slds-select\">" ;
                                            }   
                                            else
                                                selectHtml2 = "<select name=\"picklistInput\" class=\"slds-select\">" ;

                                        var data = cellData2;
                                        if(data != undefined && data != 'undefined') {
                                        
                                            var finaldata = data.split(',');
                                            for(var key=0;key < finaldata.length;key++)
                                            {
                                                if(key == 0){
                                                    data = row.data().toArray()[idxRow];
                                                    if(finaldata[key] == "' '"){
                                                        finaldata[key] = '';
                                                    }
                                                    
                                                    selectHtml2 = selectHtml2 + "<option selected = \"selected\" value='"+finaldata[key]+"'>"+finaldata[key]+" </option>";  
                                                    updatedRecs[data.Id][colStructureHeaderDetailJson[i].fieldApiName] = finaldata[key] ;
                                                    sessionTable.cell(idxRow,i).data(finaldata[key]);
                                                    attributeAPIname = colStructureHeaderDetailJson[i].fieldApiName;

                                                }else{
                                                    selectHtml2 = selectHtml2 + "<option value='"+finaldata[key]+"'>"+finaldata[key]+" </option>"; 
                                                    
                                                }
                                            }
                                        }
                                        selectHtml2 = selectHtml2 + '</select>';

                                    }
                                    else if(colStructureHeaderDetailJson[i].fieldDataType == 'Text'){

                                        if(colStructureHeaderDetailJson[i].isEditable){
                                            data = row.data().toArray()[idxRow];

                                            if(cellData2 == "' '"){
                                                cellData2 = '';
                                            }
                                            
                                            selectHtml2 = selectHtml2 + "<input class=\"textInput slds-input\" type=\"text\" value='"+cellData2+"'"+"\>";
                                            updatedRecs[data.Id][colStructureHeaderDetailJson[i].fieldApiName] = cellData2; 
                                            sessionTable.cell(idxRow,i).data(cellData2);
                                            
                                        }
                                        else
                                            selectHtml2="<output title='" + cellData2 + "' >" + cellData2 + "</output>";

                                        if(cellData1 == ''){
                                            attributeAPIname = colStructureHeaderDetailJson[i].fieldApiName;   
                                            component.set("v.attributeAPIname", attributeAPIname);
                                        }
                                    }
                                    else if(colStructureHeaderDetailJson[i].fieldDataType == 'Boolean'){
                                        
                                        if(colStructureHeaderDetailJson[i].isEditable){
                                            if(cellData2 == 'true')
                                                selectHtml2="<input class=\"checkboxInput slds-input\" type=\"checkbox\" checked value='"+cellData2+"'"+"\>";
                                            else
                                                selectHtml2="<input class=\"checkboxInput slds-input\" type=\"checkbox\" value='"+cellData2+"'"+"\>";
                                        }
                                        else{
                                            if(cellData2 == 'true')
                                                selectHtml2 = "<input type='checkbox'  value="+cellData2+"  checked disabled =\"disabled\" />";
                                            else
                                                selectHtml2 = "<input type='checkbox' value="+cellData2+" disabled =\"disabled\" />";
                                        }

                                        data = row.data().toArray()[idxRow];
                                        sessionTable.cell(idxRow,i).data(cellData2);
                                        updatedRecs[data.Id][colStructureHeaderDetailJson[i].fieldApiName] = cellData2; 
                                        if(cellData1 == ''){
                                            attributeAPIname = colStructureHeaderDetailJson[i].fieldApiName;
                                        }
                                    }
                                    }
                                }

                                if(selectHtml1 != '' && colStructureHeaderDetailJson[i].fieldApiName == dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_1__c']){
                                   jQuery(sessionTable.cell(idxRow,i).node()).html(selectHtml1);
                                   jQuery(sessionTable.cell(idxRow,i).node()).children('select').css('height',(rowHeight-5)+'px');
                                   jQuery(sessionTable.cell(idxRow,i).node()).children('input').css('line-height',(rowHeight-5)+'px').css('min-height',(rowHeight-5)+'px');
                                   
                                }
                                if(selectHtml2 != '' && colStructureHeaderDetailJson[i].fieldApiName == dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_2__c']){
                                    jQuery(sessionTable.cell(idxRow,i).node()).html(selectHtml2);
                                    jQuery(sessionTable.cell(idxRow,i).node()).children('select').css('height',(rowHeight-5)+'px');
                                    jQuery(sessionTable.cell(idxRow,i).node()).children('input').css('line-height',(rowHeight-5)+'px').css('min-height',(rowHeight-5)+'px');
                                }
                            }
                            
                            if(event.getParam('arguments').attributeAPIname != attributeAPIname)
                                    component.checkDependencyConditions(attributeAPIname,colStructureHeaderDetailJson,updatedRecs,idxRow,sessionTable);
                        }
                    }
                }
            }
        });

        console.log('MEthod Ends *********** ');

        var globalId = component.getGlobalId();
        if(document.getElementById(globalId+'searchInputBox'))
            document.getElementById(globalId+'searchInputBox').disabled = false;

        console.log(updatedRecs);
        /*if(event.getParam('arguments').attributeAPIname != attributeAPIname)
            component.checkDependencyConditions(attributeAPIname,colStructureHeaderDetailJson,updatedRecs,idxRow,sessionTable);*/
    
        }
        catch(err){
            console.log("Error ", err);
        }
    },

    updateCountryAttributes : function(component, event, helper){

        var params = event.getParam('arguments');
        var countryId = '';
        if(params)
            countryId = params.countryId;

        component.set("v.countryId",countryId);
        console.log('updateCountryAttributes datatable ---> ' + component.get("v.countryId"));

        var positionhierarchy = component.find('positionhierarchy');
        if(positionhierarchy)
        {
            positionhierarchy.initialize(component.get("v.countryId"));
        }
        var treeHierarchy = component.find('treeHierarchy');
        if(treeHierarchy)
        {
            treeHierarchy.refreshTree(component.get("v.countryId"));
        }
    },

    validateMandatoryRules : function(component, event){
        console.log('*************** validateMandatoryRules Called **************');
        component.set("v.mandatoryFieldsErrorMessage" , '');
        var $j = jQuery.noConflict();
        var dependencyConditions = JSON.parse(JSON.stringify(component.get("v.dependencyControlConditions")))[0];
        var madatoryFieldsArray = [];
        var globalId = component.getGlobalId();
        var sessionTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();
        var namespace=component.get("v.namespace");
        var colIndexOfChangeStatus = sessionTable.column( namespace+'Change_Status__c:name' ).index();
        var updatedRowsIndexArray = [];
        var mandatoryFieldsMessage = '';
        var nodes = sessionTable.rows({ page: 'current' }).nodes();
        var colStructure = component.get("v.columnStructure");
        var rowHeight = colStructure.rowHeight;
        var records = component.get("v.updatedRecs");
        console.log('-- updated recs----');
        console.log(records);
        var rowsArray = [];
        var rowColumnArray= [];
        var namespace = component.get("v.namespace");
        console.log('nodes ----------');
        console.log(nodes);
        
        // clean code highlight if there is any highlited row 
        for(var i=0; i < nodes.length ;i++){
            var columns = sessionTable.settings().init().columns;
            for(var col=0; col<columns.length; col++)
            {
                if(nodes[i].cells[col] != undefined)
                {
                    var temp = nodes[i].cells[col].className;
                    temp = temp.replace(' highlight','');
                    nodes[i].cells[col].className = temp;
                    if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') != -1)
                    {
                        let imgHtml = '';
                        nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                    }
                }
            }
        }
        
        
        for(var i=0; i < nodes.length ;i++){
        
            Object.keys(dependencyConditions).forEach( function(key) {
                for(var index=0; index < dependencyConditions[key].length; index++)
                {
        
                    if(dependencyConditions[key][index][namespace+'Mandatory__c'])
                    {
                        var data = sessionTable.rows(nodes[i]).data();
                        var dc = dependencyConditions[key][index];
                        console.log(dc);
                        var dependentFieldValue = '';
                        var dependentFieldValue2 = '';
                        Object.keys(records).forEach( function(key) {
                            var mapValue = records[key];
                            var controllingfieldName = dc[namespace+'Dependency_Control__r'][namespace+'Controlling_Field_1__c'];
                            var entryCriteria1 = '';
                            var entryCriteria2 = '';
                            if(dc[namespace+'Entry_Criteria_1__c'] != undefined)
                                entryCriteria1 = dc[namespace+'Entry_Criteria_1__c'];
                                entryCriteria2 = '';
                            if(dc[namespace+'Entry_Criteria_2__c'] != undefined){
                                entryCriteria2 = dc[namespace+'Entry_Criteria_2__c'];
                            }

                            var operator1 = ''
                            var operator2 = ''
                            var value1 = '';
                            var field1 = '';
                            var value2 = '';
                            var field2 = '';
                            var oneCondition = false;
                            var twoCondition = false;
                            if(entryCriteria1 != '' && entryCriteria2 == ''){
                                oneCondition = true;
                                twoCondition = false;
                                var entryCriteriaField = '';
                                if(mapValue[controllingfieldName] == undefined || mapValue[controllingfieldName] == '' ){
                                    entryCriteriaField = entryCriteria1.split(' ')[0];
                                    if(entryCriteriaField.indexOf('__r') != -1){
                                        data = sessionTable.rows(nodes[i]).data()[0];
                                        var parentfield = entryCriteriaField.split('.')[0];
                                        var parentData = data[parentfield];
                                        field1 = parentData[entryCriteriaField.split('.')[1]];
                                    }else{
                                        data = sessionTable.rows(nodes[i]).data()[0];
                                        field1 = data[controllingfieldName];
                                    }
                                }else{
                                    data = sessionTable.rows(nodes[i]).data()[0];
                                    entryCriteriaField = entryCriteria1.split(' ')[0];
                                    field1 = mapValue[controllingfieldName];

                                }
                                operator1 = entryCriteria1.split(' ')[1];
                                if(entryCriteria1.split(' ')[2].indexOf('__c') == -1){
                                    value1 = entryCriteria1.split(' ')[2];
                                }else{
                                    value1 = data[value1];
                                    if(mapValue[controllingfieldName] == undefined || mapValue[controllingfieldName] == '' )
                                        value1 = data[value1];
                                    else{
                                        var entryCriteriaValue = entryCriteria1.split(' ')[2];
                                        value1 = mapValue.entryCriteriaValue;
                                        
                                    }
                                }
                            }

                            if(entryCriteria1 != '' && entryCriteria2 !=''){
                                twoCondition = true;
                                oneCondition = false;
                                var entryCriteriaField1 = '';
                                var entryCriteriaField2 = '';
                                if(mapValue[controllingfieldName] == undefined || mapValue[controllingfieldName] == '' ){
                                    entryCriteriaField1 = entryCriteria1.split(' ')[0];
                                    if(entryCriteriaField1.indexOf('__r') != -1){
                                        data = sessionTable.rows(nodes[i]).data()[0];
                                        var parentfield = entryCriteriaField1.split('.')[0];
                                        var parentData = data[parentfield];
                                        field = parentData[entryCriteriaField1.split('.')[1]];
                                    }else{
                                        data = sessionTable.rows(nodes[i]).data()[0];
                                        field1 = data[controllingfieldName];
                                    }
                                }else{
                                    data = sessionTable.rows(nodes[i]).data()[0];
                                    entryCriteriaField = entryCriteria1.split(' ')[0];
                                    field1 = mapValue[controllingfieldName];
                                }

                                operator1 = entryCriteria1.split(' ')[1];
                                if(entryCriteria1.split(' ')[2].indexOf('__c') == -1){
                                    value1 = entryCriteria1.split(' ')[2];
                                    if(entryCriteria1.split(' ')[3] != ''){
                                        value1 = value1+ ' '+entryCriteria1.split(' ')[3];
                                    }
                                }else{
                                    value1 = data[value1];
                                    if(mapValue[controllingfieldName] == undefined || mapValue[controllingfieldName] == '' )
                                        value1 = data[value1];
                                    else{
                                        var entryCriteriaValue = entryCriteria1.split(' ')[2];
                                        value1 = mapValue[entryCriteriaValue];
                                        
                                    }
                                }

                                entryCriteriaField2 = entryCriteria2.split(' ')[0];
                                if(mapValue[entryCriteriaField2] == undefined || mapValue[entryCriteriaField2] == '' ){
                                    if(entryCriteriaField2.indexOf('__r') != -1){
                                        data = sessionTable.rows(nodes[i]).data()[0];
                                        var parentfield = entryCriteriaField2.split('.')[0];
                                        var parentData = data[parentfield];
                                        field2 = parentData[entryCriteriaField2.split('.')[1]];
                                    }else{
                                        data = sessionTable.rows(nodes[i]).data()[0];
                                        field2 = data[entryCriteriaField2];
                                    }
                                }else{
                                    data = sessionTable.rows(nodes[i]).data()[0];
                                    entryCriteriaField2 = entryCriteriaField2.split(' ')[0];
                                    field2 = mapValue[entryCriteriaField2];
                                }

                                operator2 = entryCriteria2.split(' ')[1];
                                if(entryCriteria2.split(' ')[2].indexOf('__c') == -1){
                                    value2 = entryCriteria2.split(' ')[2];
                                }else{
                                    value2 = data[value2];
                                    if(mapValue[field2] == undefined || mapValue[field2] == '' )
                                        value2 = data[value2];
                                    else{
                                        var entryCriteriaValue2 = entryCriteria2.split(' ')[2];
                                        value2 = mapValue[entryCriteriaValue2];
                                        
                                    }
                                }
                            }
                            var dependentFieldName = dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_1__c'];
                            var dependentFieldName2 = dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_2__c'];
                            var columnIndex;
                            var columnIndex2;
                            var columns = sessionTable.settings().init().columns;
                            for(var col=0; col<columns.length; col++){
                                if(columns[col].name == dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_1__c']){
                                    columnIndex = col;
                                }
                                if(columns[col].name == dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_2__c']){
                                    columnIndex2 = col;
                                }
                            }
                            
                            var rowIndex;
                            console.log('rows : '+i);
                            console.log(nodes[i]);
                            console.log(sessionTable.rows(nodes[i]).data()[0].Id);
                            if(key == sessionTable.rows(nodes[i]).data()[0].Id)
                            {
                                    
                                    console.log('Field Message : '+mandatoryFieldsMessage);
                                    var mappp = mapValue[dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_1__c']];
                                    var depend = data[dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_1__c']];
                                    var mappp2 = mapValue[dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_2__c']];
                                    var depend2 = data[dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_2__c']];
                                    console.log('---values --------');
                                    console.log(mappp);
                                    console.log(depend);
                                    if(oneCondition)
                                    {
                                        if( mappp == 'None' || mappp =='' || (mappp == undefined && (depend=='None' || depend=='' || depend=='null') ))
                                        {
                                            dependentFieldValue = 'blank';
                                            console.log('operator1 ::: '+operator1);
                                            console.log('field1 ::::'+field1);
                                            console.log('value1 ::::: '+value1);
                                            if(operator1 == '='){
                                                if(field1 == value1 ){
                                                    console.log('Inside Highlight Column');
                                                    madatoryFieldsArray.push(dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_1__c']);
                                                    mandatoryFieldsMessage = dc[namespace+'Error__c'];
                                                    console.log('------------------columns  -----------------');
                                                    /*if(nodes[i].cells[columnIndex].children[0].className.indexOf('highlight') == -1)
                                                        nodes[i].cells[columnIndex].children[0].className += ' highlight';*/

                                                        if(nodes[i].className.indexOf('highlight') == -1)
                                                        {
                                                            nodes[i].cells[columnIndex].className += ' highlight';
                                                            if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') == -1)
                                                            {
                                                                let imgHtml = '<button class="slds-button errorPopupButton slds-button--icon" title="Error" id="errorPopupButton_'+i+'"><span class="icon-warning icon-svg slds-icon-text-default errorPopupButton"></span></button>';
                                                                nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                            }                                                            
                                                        }
                                                        else{
                                                            if(nodes[i].cells[columnIndex].children[0] != undefined){
                                                                var temp = nodes[i].cells[columnIndex].className;
                                                                temp = temp.replace(' highlight','');
                                                                nodes[i].cells[columnIndex].className = temp;
                                                                if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') != -1)
                                                                {
                                                                    let imgHtml = '';
                                                                    nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                                }
                                                            }
                                                        }
                                                    }
                                                
                                                }
                                            if(operator1 == '>'){
                                                if(field1 > value1)
                                                {
                                                    madatoryFieldsArray.push(dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_1__c']);
                                                    mandatoryFieldsMessage = dc[namespace+'Error__c'];
                                                    nodes[i].cells[columnIndex].className += ' highlight'; 
                                                    if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') == -1)
                                                    {
                                                        let imgHtml = '<button class="slds-button errorPopupButton slds-button--icon" title="Error" id="errorPopupButton_'+i+'"><span class="icon-warning icon-svg slds-icon-text-default errorPopupButton"></span></button>';
                                                        nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                    }
                                                }
                                                else{
                                                    if(nodes[i].cells[columnIndex].children[0] != undefined){
                                                        var temp = nodes[i].cells[columnIndex].className;
                                                        temp = temp.replace(' highlight','');
                                                        nodes[i].cells[columnIndex].className = temp;
                                                        if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') != -1)
                                                        {
                                                            let imgHtml = '';
                                                            nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                        }
                                                    }
                                                }
                                                    
                                            }
                                            if(operator1 == '<' )
                                            {
                                                if(field1 < value1)
                                                {
                                                    madatoryFieldsArray.push(dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_1__c']);
                                                    mandatoryFieldsMessage = dc[namespace+'Error__c'];
                                                    nodes[i].cells[columnIndex].className += ' highlight';
                                                    if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') == -1)
                                                    {
                                                        let imgHtml = '<button class="slds-button errorPopupButton slds-button--icon" title="Error" id="errorPopupButton_'+i+'"><span class="icon-warning icon-svg slds-icon-text-default errorPopupButton"></span></button>';
                                                        nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                    }
                                                }
                                                else{
                                                    if(nodes[i].cells[columnIndex].children[0] != undefined)
                                                    {
                                                        var temp = nodes[i].cells[columnIndex].className;
                                                        temp = temp.replace(' highlight','');
                                                        nodes[i].cells[columnIndex].className = temp;
                                                        if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') != -1)
                                                        {
                                                            let imgHtml = '';
                                                            nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                        }
                                                    }
                                                }
                                                    
                                            }

                                        }

                                        else{
                                            if(nodes[i].cells[columnIndex].children[0] != undefined){
                                                var temp = nodes[i].cells[columnIndex].className;
                                                temp = temp.replace(' highlight','');
                                                nodes[i].cells[columnIndex].className = temp;
                                                if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') != -1)
                                                {
                                                    let imgHtml = '';
                                                    nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                }
                                            }
                                            
                                        }

                                        if ( mappp2 == 'None' || mappp2 =='' || (mappp2 == undefined && (depend2 =='None' || depend2 =='' || depend2 =='null') )){
                                            dependentFieldValue2 = 'blank';
                                            if(operator1 == '='){
                                                if(field1 == value1 ){
                                                    console.log('Inside Highlight Column');
                                                    madatoryFieldsArray.push(dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_2__c']);
                                                    mandatoryFieldsMessage = dc[namespace+'Error__c'];
                                                    console.log('------------------columns  -----------------');
                                                    /*if(nodes[i].cells[columnIndex].children[0].className.indexOf('highlight') == -1)
                                                        nodes[i].cells[columnIndex].children[0].className += ' highlight';*/

                                                        if(nodes[i].className.indexOf('highlight') == -1)
                                                        {
                                                            nodes[i].cells[columnIndex2].className += ' highlight';
                                                            if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') == -1)
                                                            {
                                                                let imgHtml = '<button class="slds-button errorPopupButton slds-button--icon" title="Error" id="errorPopupButton_'+i+'"><span class="icon-warning icon-svg slds-icon-text-default errorPopupButton"></span></button>';
                                                                nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                            }                                                            
                                                        }
                                                        else{
                                                            if(nodes[i].cells[columnIndex2] != undefined){
                                                                var temp = nodes[i].cells[columnIndex2].className;
                                                                temp = temp.replace(' highlight','');
                                                                nodes[i].cells[columnIndex2].className = temp;
                                                                if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') != -1)
                                                                {
                                                                    let imgHtml = '';
                                                                    nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                                }
                                                            }
                                                        }
                                                    }
                                                
                                                }
                                            if(operator1 == '>'){
                                                if(field1 > value1)
                                                {
                                                    madatoryFieldsArray.push(dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_2__c']);
                                                    mandatoryFieldsMessage = dc[namespace+'Error__c'];
                                                    nodes[i].cells[columnIndex2].className += ' highlight'; 
                                                    if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') == -1)
                                                    {
                                                        let imgHtml = '<button class="slds-button errorPopupButton slds-button--icon" title="Error" id="errorPopupButton_'+i+'"><span class="icon-warning icon-svg slds-icon-text-default errorPopupButton"></span></button>';
                                                        nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                    }
                                                }
                                                else{
                                                    if(nodes[i].cells[columnIndex2] != undefined){
                                                        var temp = nodes[i].cells[columnIndex2].className;
                                                        temp = temp.replace(' highlight','');
                                                        nodes[i].cells[columnIndex2].className = temp;
                                                        if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') != -1)
                                                        {
                                                            let imgHtml = '';
                                                            nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                        }
                                                    }
                                                }
                                                    
                                            }
                                            if(operator1 == '<' )
                                            {
                                                if(field1 < value1)
                                                {
                                                    madatoryFieldsArray.push(dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_2__c']);
                                                    mandatoryFieldsMessage = dc[namespace+'Error__c'];
                                                    nodes[i].cells[columnIndex2].className += ' highlight';
                                                    if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') == -1)
                                                    {
                                                        let imgHtml = '<button class="slds-button errorPopupButton slds-button--icon" title="Error" id="errorPopupButton_'+i+'"><span class="icon-warning icon-svg slds-icon-text-default errorPopupButton"></span></button>';
                                                        nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                    }
                                                }
                                                else{
                                                    if(nodes[i].cells[columnIndex2] != undefined)
                                                    {
                                                        var temp = nodes[i].cells[columnIndex2].className;
                                                        temp = temp.replace(' highlight','');
                                                        nodes[i].cells[columnIndex2].className = temp;
                                                        if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') != -1)
                                                        {
                                                            let imgHtml = '';
                                                            nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                        }
                                                    }
                                                }
                                                    
                                            }
                                        }else{
                                            if(nodes[i].cells[columnIndex2] != undefined){
                                                var temp = nodes[i].cells[columnIndex2].className;
                                                temp = temp.replace(' highlight','');
                                                nodes[i].cells[columnIndex2].className = temp;
                                                if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') != -1)
                                                {
                                                    let imgHtml = '';
                                                    nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                }
                                            }
                                        }
                                    }

                                    if(twoCondition){
                                        if(operator1 == '='){
                                            if(operator2 == '='){
                                                if(field1 != value1 && field2 == value2){
                                                    mandatoryFieldsMessage = dc[namespace+'Error__c'];
                                                    if(nodes[i].cells[columnIndex].className.indexOf('highlight') == -1)
                                                        nodes[i].cells[columnIndex].className += ' highlight';
                                                    if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') == -1)
                                                    {
                                                        let imgHtml = '<button class="slds-button errorPopupButton slds-button--icon" title="Error" id="errorPopupButton_'+i+'"><span class="icon-warning icon-svg slds-icon-text-default errorPopupButton"></span></button>';
                                                        nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                    }
                                                }
                                                else{
                                                    if(nodes[i].cells[columnIndex] != undefined){
                                                    
                                                        var temp = nodes[i].cells[columnIndex].className;
                                                        temp = temp.replace(' highlight','');
                                                        nodes[i].cells[columnIndex].className = temp;
                                                        if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') != -1)
                                                        {
                                                            let imgHtml = '';
                                                            nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                        }
                                                    }
                                                }
                                            }else if(operator2 == '>'){
                                                if(field1 != value1 && field2 > value2){
                                                    mandatoryFieldsMessage = dc[namespace+'Error__c'];
                                                    if(nodes[i].cells[columnIndex].className.indexOf('highlight') == -1)
                                                        nodes[i].cells[columnIndex].className += ' highlight';
                                                    if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') == -1)
                                                    {
                                                        let imgHtml = '<button class="slds-button errorPopupButton slds-button--icon" title="Error" id="errorPopupButton_'+i+'"><span class="icon-warning icon-svg slds-icon-text-default errorPopupButton"></span></button>';
                                                        nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                    }
                                                }
                                                else{
                                                    if(nodes[i].cells[columnIndex] != undefined){
                                                    
                                                        var temp = nodes[i].cells[columnIndex].className;
                                                        temp = temp.replace(' highlight','');
                                                        nodes[i].cells[columnIndex].className = temp;
                                                        if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') != -1)
                                                        {
                                                            let imgHtml = '';
                                                            nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                        }
                                                    }
                                                }
                                            }else if(operator2 == '<'){
                                                if(field1 != value1 && field2 < value2){
                                                    mandatoryFieldsMessage = dc[namespace+'Error__c'];
                                                    if(nodes[i].cells[columnIndex].className.indexOf('highlight') == -1)
                                                        nodes[i].cells[columnIndex].className += ' highlight';
                                                    if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') == -1)
                                                    {
                                                        let imgHtml = '<button class="slds-button errorPopupButton slds-button--icon" title="Error" id="errorPopupButton_'+i+'"><span class="icon-warning icon-svg slds-icon-text-default errorPopupButton"></span></button>';
                                                        nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                    }
                                                }
                                                else{
                                                    if(nodes[i].cells[columnIndex] != undefined){
                                                    
                                                        var temp = nodes[i].cells[columnIndex].className;
                                                        temp = temp.replace(' highlight','');
                                                        nodes[i].cells[columnIndex].className = temp;
                                                        if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') != -1)
                                                        {
                                                            let imgHtml = '';
                                                            nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                        }
                                                    }
                                                }
                                            }else if(operator2 == '!='){
                                                if(field1 == value1 && field2 != value2){
                                                    mandatoryFieldsMessage = dc[namespace+'Error__c'];
                                                    if(nodes[i].cells[columnIndex].className.indexOf('highlight') == -1)
                                                        nodes[i].cells[columnIndex].className += ' highlight';
                                                    if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') == -1)
                                                    {
                                                        let imgHtml = '<button class="slds-button errorPopupButton slds-button--icon" title="Error" id="errorPopupButton_'+i+'"><span class="icon-warning icon-svg slds-icon-text-default errorPopupButton"></span></button>';
                                                        nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                    }
                                                }
                                                else{
                                                    if(nodes[i].cells[columnIndex] != undefined){
                                                    
                                                        var temp = nodes[i].cells[columnIndex].className;
                                                        temp = temp.replace(' highlight','');
                                                        nodes[i].cells[columnIndex].className = temp;
                                                        if(nodes[i].cells[colIndexOfChangeStatus].innerHTML.indexOf('errorPopupButton') != -1)
                                                        {
                                                            let imgHtml = '';
                                                            nodes[i].cells[colIndexOfChangeStatus].innerHTML = imgHtml;
                                                        }
                                                    }
                                                }
                                             }
                                        }
                                    }       
                            }
                                          
                        });

                    }
                }
            });
        }
        
        console.log('final error message is : '+mandatoryFieldsMessage);
        try{
            sessionTable.fixedColumns().update();
        }
        catch(err){
            console.log('Error ', err);
        }
        
        component.set("v.mandatoryFieldsErrorMessage" , mandatoryFieldsMessage);

    },

    checkDependencyConditionsOnLoad : function(component,event){
        try{
        console.log('****************** checkDependencyConditionsOnLoad ************************');
        //component.showSpin();

        var $j = jQuery.noConflict();
        var namespace = component.get("v.namespace");
        var dependencyConditions = JSON.parse(JSON.stringify(component.get("v.dependencyControlConditions")))[0];
        var colStructure = component.get("v.columnStructure");
        var rowHeight = colStructure.rowHeight;
        var colStructureHeaderDetailJson = event.getParam('arguments').colStructureHeaderDetailJson;
        var globalId = component.getGlobalId();
        /*if(document.getElementById(globalId+'searchInputBox'))
            document.getElementById(globalId+'searchInputBox').disabled = true;*/
        var sessionTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();
        var nodes = sessionTable.rows({ page: 'current' }).nodes();
        
        if(dependencyConditions.length < 1)
            return;

        for(var r=0; r < nodes.length ;r++){
            Object.keys(dependencyConditions).forEach( function(key) {
                var dc = '';
                var controllingField1 = key.split(';')[0];
                var controllingField2  = '';
                if(key.indexOf(';') !== -1)
                    controllingField2 = key.split(';')[1];
           
                for(var index=0; index < dependencyConditions[key].length; index++)
                {
                    var cellData1 = '';
                    var cellData2 = '';
                    /*if(cellData1 == '' || cellData2 == '')
                    {*/
                    if(!dependencyConditions[key][index][namespace+'Mandatory__c'])
                    {
                        console.log('--- inside dependency condition----');
                        var data = sessionTable.rows(r).data()[0];
                        console.log(data);
                        dc = dependencyConditions[key][index];
                        var entryValue1 = '';
                        var field1 = '';
                        var operator1 = '';
                        var value1 = '';
                        if(dc[namespace+'Entry_Criteria_1__c'] != undefined){
                            entryValue1 = dc[namespace+'Entry_Criteria_1__c'];
                            field1 = entryValue1.split(' ')[0];
                            operator1 = entryValue1.split(' ')[1];
                            if(entryValue1.split(' ')[3] != undefined)
                                value1 =  entryValue1.split(' ')[2] + ' '+entryValue1.split(' ')[3];
                            else
                                value1 =  entryValue1.split(' ')[2];
                        }

                        if(value1 == "' '"){
                            value1 = '';
                        }

                        var entryValue2 = '';
                        var field2 = '';
                        var operator2 = '';
                        var value2 = ''; 

                        if(dc[namespace+'Entry_Criteria_2__c'] != undefined){
                            entryValue2 = dc[namespace+'Entry_Criteria_2__c'];
                            field2 = entryValue2.split(' ')[0];
                            operator2 = entryValue2.split(' ')[1];
                            if(entryValue2.split(' ')[3] != undefined)
                                value2 =  entryValue2.split(' ')[2] + ' '+entryValue2.split(' ')[3];
                            else
                                value2 =  entryValue2.split(' ')[2];
                        }

                        if(value2 == "' '"){
                            value2 = '';
                        }

                        // if entrycriteria fields depends on field api name
                        //ex: Picklist1__c < Picklist2__c 
                        data = sessionTable.rows(nodes[r]).data()[0];
                        var isValue1 = true;
                        var isValue2 = true;
                        var apiValue1 = '';
                        var apiValue2 = '';
                        if(value1.indexOf('__c') !== -1){
                            if(data[value1] == '')
                                apiValue1 = '';
                            else{
                                apiValue1 = data[value1];
                            }
                            isValue1 = false;
                        }
                        
                        if(value2.indexOf('__c') !== -1){
                            if(data[value2] == ''){
                                apiValue2 = '';
                            }else{
                                apiValue2 = data[value2];
                            }
                            isValue2 = false;
                        }
                        
                        if(value1 == "' '"){
                            value1 = '';
                        }

                        //case 1: 1 entry_Criteria
                        if(entryValue1 != '' && entryValue2 == ''){
                            data = sessionTable.rows(nodes[r]).data()[0];
                             if(field1 == namespace+'Change_Action_Type__c' && (data[field1] == undefined || data[field1] == 'undefined'))
                                        data[field1] = '';
                            if(operator1 == '='){
                                console.log('field1 : '+field1);
                                console.log('value1 : '+value1);
                                console.log('data[field1] : '+data[field1]);
                                if(isValue1){
                                   
                                    if(value1 == data[field1] || (value1 == data[field1]+"") )//|| apifieldValue1 == apiValue1)
                                    {
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        console.log('cellData1 : :'+cellData1);
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                            
                                    }
                                }else{
                                    if(data[field1] == apiValue1 || (data[field1] == apiValue1+"") ){
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                    }
                                }
                            }else if(operator1 == '>'){
                                if(isValue1){
                                    if(value1 > data[field1] || (value1 > data[field1]+""))
                                    {
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                        
                                    } 
                                }else{
                                    if(data[field1] > apiValue1 || (date[field1] > apiValue1+""))
                                    {
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                        
                                    } 
                                }
                                    
                            }else if(operator1 == '<'){
                                if(isValue1){
                                    if(value1 < data[field1] || (value1 < data[field1]+""))
                                    {
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                        
                                    }
                                }else{
                                    if(data[field1] < apiValue1 || (data[field1] < apiValue1+""))
                                    {
                                        cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                        if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                            cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                        }
                                        
                                    }
                                }
                                
                            }
                        }
                        //case:2 - 2 entryCriteria
                        if(entryValue1 != '' && entryValue2 != '')
                        {
                            console.log('************* case 2*************');
                            data = sessionTable.rows(nodes[r]).data()[0];
                            console.log('value1 : '+value1 +', data[field1] : '+data[field1] +', apiValue1 : '+apiValue1);
                            console.log('value2 : '+data[value2] +',value2 :'+ value2+', data[field2] : '+data[field2] +', apiValue2 : '+apiValue2);
                            
                            if(operator1 == '='){
                                if(operator2 == '='){
                                    if(isValue2){
                                        if(value1 == data[field1] && value2 == data[field2]){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }

                                    }else{
                                        if(data[field1] == value1 && data[value2] == apiValue2){
                                            console.log('match found -----');
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }
                                    }
                                }else if(operator2 == '>'){
                                    if(isValue2){
                                        if(value1 == data[field1] && value2 > data[field2]){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }

                                    }else{
                                        if(data[field1] == value1 &&  data[field2] > apiValue2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }
                                    }

                                }else if(operator2 == '<'){
                                    if(isValue2){
                                        if(value1 == data[field1] && value2 < data[field2]){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }

                                    }else{
                                        if(data[field1] == value1 &&  data[field2] < apiValue2 ){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }
                                    }

                                }else if(operator2 == '!=')
                                {
                                    if(isValue2){
                                        if(value1 == data[field1] && value2 != data[field2]){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }

                                    }else{
                                        if(data[field1] == value1 &&  data[field2] != apiValue2 ){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }
                                    }

                                }
                            }
                            if(operator1 == '>'){
                                if(operator2 == '='){
                                    if(isValue2){
                                        if(data[field1] > value1 && data[field2] == value2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }

                                    }else{
                                        if(data[field1] > value1 && data[field2] == apiValue2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }
                                    }
                                
                                }else if(operator2 == '>'){
                                    if(isValue2){
                                        if(data[field1] > value1 && data[field2] > value2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }

                                    }else{
                                        if(data[field1] > value1 && data[field2] > apiValue2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }
                                    }
                                    
                                }else if(operator2 == '<'){
                                    if(isValue2){
                                        if(data[field1] > value1 && data[field2] < value2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }

                                    }else{
                                        if(data[field1] > value1 && data[field2] < apiValue2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }
                                    }

                                }else if(operator2 == '!='){
                                    if(isValue2){
                                        if(data[field1] > value1 && data[field2]  != value2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }

                                    }else{
                                        if(data[field1] > value1 && data[field2] != apiValue2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }
                                    }
                                    
                                }

                            }
                            if(operator1 == '<')
                            {
                                if(operator2 == '='){
                                    if(isValue2){
                                        if(data[field1] < value1 && data[field2]  == value2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }

                                    }else{
                                        if(data[field1] < value1 && data[field2] == apiValue2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }
                                    }

                                }else if(operator2 == '>'){
                                    if(isValue2){
                                        if(data[field1] < value1 && data[field2]  > value2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }

                                    }else{
                                        if(data[field1] < value1 && data[field2] > apiValue2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }
                                    }

                                }else if(operator2 == '<'){
                                    if(isValue2){
                                        if(data[field1] < value1 && data[field2]  < value2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }

                                    }else{
                                        if(data[field1] < value1 && data[field2] < apiValue2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }
                                    }

                                }else if(operator2 == '!='){
                                    if(isValue2){
                                        if(data[field1] < value1 && data[field2]  != value2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }

                                    }else{
                                        if(data[field1] < value1 && data[field2] != apiValue2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }
                                    }
                                    
                                }

                            }
                            if(operator1 == '!='){
                                if(operator2 == '='){
                                    if(isValue2){
                                        if(data[field1] != value1 && data[field2]  == value2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }

                                    }else{
                                        if(data[field1] != value1 && data[field2] == apiValue2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }
                                    }

                                }else if(operator2 == '>'){
                                    if(isValue2){
                                        if(data[field1] != value1 && data[field2]  > value2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }

                                    }else{
                                        if(data[field1] != value1 && data[field2] > apiValue2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }
                                    }

                                }else if(operator2 == '<'){
                                    if(isValue2){
                                        if(data[field1] != value1 && data[field2]  < value2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }

                                    }else{
                                        if(data[field1] != value1 && data[field2] < apiValue2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }
                                    }

                                }else if(operator2 == '!='){
                                    if(isValue2){
                                        if(data[field1] != value1 && data[field2]  != value2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }

                                    }else{
                                        if(data[field1] != value1 && data[field2] != apiValue2){
                                            cellData1 = dc[namespace+'Dependent_Field_1_Values__c'];
                                            if(dc[namespace+'Dependent_Field_2_Values__c'] != undefined){
                                                cellData2 = dc[namespace+'Dependent_Field_2_Values__c'];
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        console.log(' ************ final data ****** ');
                        console.log('cellData1 : '+cellData1);
                        console.log('cellData2 : '+cellData2);
                       
                        if((cellData1 != '' || cellData2 != '') && dc != ''){
                            var selectHtml1 = "";   
                            var selectHtml2 = "";
                            var colIndexOfChangeAction = sessionTable.column( namespace+'Change_Action_Type__c:name' ).index();
                            for(var i=0;i< colStructureHeaderDetailJson.length; i++)
                            {
                                
                                if(colStructureHeaderDetailJson[i].fieldApiName == dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_1__c'] && cellData1 != '')
                                { 
                                    if(colStructureHeaderDetailJson[i].fieldDataType == 'Picklist')
                                    {
                                        if(component.get("v.isCRraised") == true || !colStructureHeaderDetailJson[i].isEditable || sessionTable.rows(r).data()[0][namespace+'Change_Action_Type__c'] == 'Dropped'){
                                            selectHtml1 = "<select disabled name=\"picklistInput\" class=\"slds-select\">" ;
                                        }   
                                        else
                                            selectHtml1 = "<select name=\"picklistInput\" class=\"slds-select\">" ;

                                        var tempData1 = cellData1;
                                        if(tempData1 != undefined && tempData1 != 'undefined') {
                                        
                                            var finaldata1 = tempData1.split(',');
                                            var dataValue = '';
                                            for(var j=0; j < finaldata1.length;j++)
                                            {
                                                
                                                dataValue = data[colStructureHeaderDetailJson[i].fieldApiName];
                                                if(finaldata1[j] == "' '"){
                                                    finaldata1[j] = '';
                                                }
                                               
                                                if(data[colStructureHeaderDetailJson[i].fieldApiName] != undefined){
                                                    if(dataValue == finaldata1[j]){
                                                        selectHtml1 = selectHtml1 + "<option selected=\"selected\" value=\'"+data[colStructureHeaderDetailJson[i].fieldApiName]+"\'>"+data[colStructureHeaderDetailJson[i].fieldApiName]+" </option>"; 
                                                    }else{
                                                        selectHtml1 = selectHtml1 + "<option value=\'"+finaldata1[j]+"\'>"+finaldata1[j]+" </option>"; 
                                                    }
                                                }else{
                                                    console.log('===== no else ---'+j);
                                                    if(j == 0 && dataValue == ''){
                                                        selectHtml1 = selectHtml1 + "<option selected=\"selected\" value='"+finaldata1[j]+"'>"+finaldata1[j]+" </option>"; 
                                                    }else{
                                                        selectHtml1 = selectHtml1 + "<option value='"+finaldata1[j]+"'>"+finaldata1[j]+" </option>"; 
                                                    }
                                                }
                                                    //}
                                                    /*if(j == 0 ){
                                                        console.log()
                                                        if(finaldata1[j] == "' '"){
                                                            finaldata1[j] = '';
                                                        }

                                                        selectHtml1 = selectHtml1 + "<option selected = \"selected\" value='"+finaldata1[j]+"'>"+finaldata1[j]+" </option>"; 

                                                    }else{
                                                        selectHtml1 = selectHtml1 + "<option value='"+finaldata1[j]+"'>"+finaldata1[j]+" </option>"; 
                                                        
                                                    }*/
                                                //}
                                                
                                            }
                                        }
                                        selectHtml1 = selectHtml1 + '</select>';
                                       // sessionTable.cell(r,i).data(selectHtml1);
                                    }
                                    else if(colStructureHeaderDetailJson[i].fieldDataType == 'Text'){
                                       // selectHtml1 = cellData1;
                                       if(colStructureHeaderDetailJson[i].isEditable && !(sessionTable.rows(r).data()[0][namespace+'Change_Action_Type__c'] == 'Dropped')){
                                            if(cellData1 == "' '"){
                                                cellData1 = '';
                                            }

                                            selectHtml1= selectHtml1 + "<input class=\"textInput slds-input\" type=\"text\" value='"+cellData1+"'"+"\>";
                                            
                                        }
                                        else{
                                            if(cellData1 == '--None--'){
                                                selectHtml1= selectHtml1 + "<input class=\"textInput slds-input\" value='None' ></output>";
                                            }else{
                                                /*selectHtml1= selectHtml1 + "<output class=\"textInput slds-input\" title='" + cellData1 + "' >" + cellData1+ "</output>";*/
                                                selectHtml1 = selectHtml1 + "<input disabled class=\"textInput slds-input\" type=\"text\" value='"+cellData1+"'"+"\>";
                                            }
                                            
                                        }

                                        if(component.get("v.isCRraised") == true){
                                            selectHtml1 = selectHtml1.replace('<input ', '<input disabled ');
                                        }
                                    }

                                    else if(colStructureHeaderDetailJson[i].fieldDataType == 'Boolean'){
                                        
                                        if(colStructureHeaderDetailJson[i].isEditable && !(sessionTable.rows(r).data()[0][namespace+'Change_Action_Type__c'] == 'Dropped')){
                                            if(cellData1 == 'true')
                                                selectHtml1 ="<input class=\"checkboxInput slds-input\" type=\"checkbox\" checked value='"+cellData1+"'"+"\>";
                                            else
                                                selectHtml1 ="<input class=\"checkboxInput slds-input\" type=\"checkbox\" value='"+cellData1+"'"+"\>";
                                        }
                                        else{
                                            if(cellData1 == 'true')
                                                selectHtml1 = "<input class=\"checkboxInput slds-input\" type='checkbox' checked value="+cellData1+" disabled =\"disabled\" />";
                                            else
                                                selectHtml1 = "<input class=\"checkboxInput slds-input\" type='checkbox' value="+cellData1+" disabled =\"disabled\" />";
                                        } 

                                        if(component.get("v.isCRraised") == true){
                                            selectHtml1 = selectHtml1.replace('<input ', '<input disabled ');
                                        }
                                    }
                                }

                                if(dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_2__c'] != undefined && cellData2 != '')
                                {
                                    if(colStructureHeaderDetailJson[i].fieldApiName == dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_2__c'] && cellData2 != ''){
                                        
                                        if(colStructureHeaderDetailJson[i].fieldDataType == 'Picklist')
                                        {
                                            if(component.get("v.isCRraised") == true || !colStructureHeaderDetailJson[i].isEditable || sessionTable.rows(r).data()[0][namespace+'Change_Action_Type__c'] == 'Dropped'){
                                                selectHtml2 = "<select disabled name=\"picklistInput\" class=\"slds-select\">" ;
                                            }   
                                            else
                                                selectHtml2 = "<select name=\"picklistInput\" class=\"slds-select\">" ;

                                            var tempData2 = cellData2;
                                            if(tempData2 != undefined && tempData2 != 'undefined') {
                                            
                                                var finaldata2 = tempData2.split(',');
                                                for(var k=0;k < finaldata2.length;k++)
                                                {
                                                    var dataValue2 = data[colStructureHeaderDetailJson[i].fieldApiName];
                                                    if(finaldata2[k] == "' '"){
                                                        finaldata2[k] = '';
                                                    }
                                                   
                                                    if(data[colStructureHeaderDetailJson[i].fieldApiName] != undefined){
                                                        if(dataValue2 == finaldata2[k]){
                                                           selectHtml2 = selectHtml2 + "<option selected=\"selected\" value=\'"+finaldata2[k]+"\'>"+finaldata2[k]+" </option>"; 
                                                        }else{
                                                            selectHtml2 = selectHtml2 + "<option value=\'"+finaldata2[k]+"\'>"+finaldata2[k]+" </option>"; 
                                                        }
                                                    }else{
                                                        console.log('===== no else ---'+j);
                                                        if(k == 0 && dataValue2 == ''){
                                                            selectHtml2 = selectHtml2 + "<option selected=\"selected\" value='"+finaldata2[k]+"'>"+finaldata2[k]+" </option>"; 
                                                        }else{
                                                            selectHtml2 = selectHtml2 + "<option value='"+finaldata2[k]+"'>"+finaldata2[k]+" </option>"; 
                                                        }
                                                    }
                                                    /*if(k == 0){
                                                        if(finaldata2[k] == "' '"){
                                                            finaldata2[k] = '';
                                                        }
                                                
                                                        selectHtml2 = selectHtml2+ "<option selected = \"selected\" value='"+finaldata2[k]+"'>"+finaldata2[k]+" </option>"; 
                                                    }else{
                                                       
                                                        selectHtml2 = selectHtml2 + "<option value='"+finaldata2[k]+"'>"+finaldata2[k]+" </option>"; 
                                                    }*/
                                                }
                                            }
                                            selectHtml2 = selectHtml2 + '</select>';

                                            
                                        }
                                    else if(colStructureHeaderDetailJson[i].fieldDataType == 'Text')
                                    {
                                        //selectHtml2 = cellData2;
                                        if(colStructureHeaderDetailJson[i].isEditable && !(sessionTable.rows(r).data()[0][namespace+'Change_Action_Type__c'] == 'Dropped')){
                                            if(cellData2 == "' '"){
                                                cellData2 = '';
                                            }
                                            selectHtml2 = selectHtml2 + "<input class=\"textInput slds-input\" type=\"text\" value='"+cellData2+"'"+"\>";
                                         
                                        }
                                        else{
                                            selectHtml2= selectHtml2 + "<input disabled class=\"textInput slds-input\" title='" + cellData2 + "' >" + cellData2 + "</output>";
                                        }

                                        if(component.get("v.isCRraised") == true){
                                            selectHtml2 = selectHtml2.replace('<input ', '<input disabled ');
                                        }
                                         
                                    }
                                    else if(colStructureHeaderDetailJson[i].fieldDataType == 'Boolean'){
                                    
                                        if(colStructureHeaderDetailJson[i].isEditable && !(sessionTable.rows(r).data()[0][namespace+'Change_Action_Type__c'] == 'Dropped')){
                                            if(cellData2 == 'true')
                                                selectHtml2="<input class=\"checkboxInput slds-input\" type=\"checkbox\" checked value='"+cellData2+"'"+"\>";
                                            else
                                                selectHtml2="<input class=\"checkboxInput slds-input\" type=\"checkbox\" value='"+cellData2+"'"+"\>";
                                        }
                                        else{
                                            if(cellData2 == 'true')
                                                selectHtml2 = "<input class=\"checkboxInput slds-input\" type='checkbox' checked value="+cellData2+" disabled =\"disabled\" />";
                                            else
                                                selectHtml2 = "<input class=\"checkboxInput slds-input\" type='checkbox' value="+cellData2+" disabled =\"disabled\" />";
                                        }

                                       if(component.get("v.isCRraised") == true){
                                            selectHtml2 = selectHtml2.replace('<input ', '<input disabled ');
                                        } 
                                    }
                                    }
                                }
                                if(selectHtml1 != '' && colStructureHeaderDetailJson[i].fieldApiName == dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_1__c']){
                                    console.log('write cell selectHtml1 :::'+r + '  ,,,, ' +selectHtml1);
                                    //sessionTable.cell(nodes[r],i).data(selectHtml1);
									// code commneted for SPD 5225
									
                                    /*jQuery(sessionTable.cell(r,i).node()).html(selectHtml1);
                                    jQuery(sessionTable.cell(r,i).node()).children('select').css('height',(rowHeight-5)+'px');
                                    jQuery(sessionTable.cell(r,i).node()).children('input').css('line-height',(rowHeight-5)+'px').css('min-height',(rowHeight-5)+'px');*/

                                    jQuery(sessionTable.cell(nodes[r],i).node()).html(selectHtml1);
                                    jQuery(sessionTable.cell(nodes[r],i).node()).children('select').css('height',(rowHeight-5)+'px');
                                    jQuery(sessionTable.cell(nodes[r],i).node()).children('input').css('line-height',(rowHeight-5)+'px').css('min-height',(rowHeight-5)+'px');
                                    


                                                            
                                }
                                if(selectHtml2 != '' && colStructureHeaderDetailJson[i].fieldApiName == dc[namespace+'Dependency_Control__r'][namespace+'Dependent_Field_2__c']){
                                    console.log('write cell selectHtml2 :::' +r + '  ,,,, ' +selectHtml2);
                                    //sessionTable.cell(nodes[r],i).data(selectHtml2);
									// code commneted for SPD 5225
                                    /*jQuery(sessionTable.cell(r,i).node()).html(selectHtml2);
                                    jQuery(sessionTable.cell(r,i).node()).children('select').css('height',(rowHeight-5)+'px');
                                    jQuery(sessionTable.cell(r,i).node()).children('input').css('line-height',(rowHeight-5)+'px').css('min-height',(rowHeight-5)+'px');*/
                                    jQuery(sessionTable.cell(nodes[r],i).node()).html(selectHtml2);
                                    jQuery(sessionTable.cell(nodes[r],i).node()).children('select').css('height',(rowHeight-5)+'px');
                                    jQuery(sessionTable.cell(nodes[r],i).node()).children('input').css('line-height',(rowHeight-5)+'px').css('min-height',(rowHeight-5)+'px');
                                    

                                    
                                }
                            }
                        }
                    }
                    //}
                }
            });
        }

        console.log('MEthod Ends *********** ');
        component.hideSpin();   
       // console.log(updatedRecs);
        /*if(event.getParam('arguments').attributeAPIname != attributeAPIname)
            component.checkDependencyConditions(attributeAPIname,colStructureHeaderDetailJson,updatedRecs,idxRow,sessionTable);*/
    
        /*if(document.getElementById(globalId+'searchInputBox'))
            document.getElementById(globalId+'searchInputBox').disabled = false;*/
        }
        catch(err){
            console.log("Error ", err);
        }
    
    },

    closeErrorPopover : function(component,event,helper)
    {
        if(document.getElementById('row_validation_failed_popover') )
            document.getElementById('row_validation_failed_popover').classList.add('slds-hide');
        else
        {
            var row_validation_failed_popover = component.find('row_validation_failed_popover');
            $A.util.addClass(row_validation_failed_popover,'slds-hide');
        }
    }
})
