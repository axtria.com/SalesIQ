({
    logException : function(component, errors) {
        var errorLabel = '';
        if(component.get("v.namespace") != '') 
        {
            errorLabel = $A.get("$Label.AxtriaSalesIQTM.Unexpected_error_notification");
        }
        else
        {
            errorLabel = $A.get("$Label.c.Unexpected_error_notification");
        }

        if (errors[0] && errors[0].message) 
        {
            var errorMessage = errors[0].message;
            var action = component.get("c.logException");
        
            action.setParams({
                message : errorMessage,
                module : component.get("v.moduleName")
            });

            action.setCallback(this,function(response)
            {
                var state = response.getState();
                if(state === 'SUCCESS')
                {   
                    try{
                        if(errorMessage.indexOf('System.DmlException') != -1){
                            var errorlst = errorMessage.split(': [')[0].split(',');
                            errorLabel = errorlst[errorlst.length - 1];
                        }
                    }
                    catch(err){
                    }

                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: errorLabel,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }
        else 
        {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                mode: 'dismissible',
                message: errorLabel,
                type : 'error'
            });
            toastEvent.fire();
        }
    },
    downloadCSV : function(component,event,helper,fileName){
        //METHOD TO DOWNLOAD CSV DATA FROM JQUERY DATATABLE : GIVEN ALL HEADERS TITLES..
        var $j = jQuery.noConflict();
        var globalId = component.getGlobalId();

        var sessionTable = $j(document.getElementById(globalId+'_dataTable')).DataTable();
        
        //GET ALL COLUMNS HEADERS 
        var colStructure = component.get("v.columnStructure");
        var colStructureHeaderDetailJson = JSON.parse(colStructure.columnHeaderDetailJson);
        console.log('colStructureHeaderDetailJson - ',colStructureHeaderDetailJson);
        delete colStructureHeaderDetailJson[0];

        console.log('colStructureHeaderDetailJson' ,colStructureHeaderDetailJson);
        //FROM HEADER JSON GET ALL HEADER TITLES
        var allHeaders = colStructureHeaderDetailJson.map( d => d.fieldDisplayName);
        console.log('allHeaders - ',allHeaders);
        //GET ALL RENDERED AND FORMATTED DATA FROM TABLE

        var renderedData = sessionTable.cells().data();
         renderedData = renderedData.map( function(d){
                try{
                    if ( d.indexOf(',') > -1) {
                            return "\"" + d + "\"";
                    }else{
                            return  d;
                    }
                }catch(e){
                    console.log(e);
                }
            });
        console.log('renderedData - ',renderedData);

        //ADD FIRST ROW IN CSV
        var finalCsv = allHeaders.toString() +  '\r\n';;

        while (renderedData.length > 0){
            //SPLIT AND ADD TO CSV : SPILT CELL DATA IN BY SIZE OF COLUMNS
            
            finalCsv += renderedData.splice(0, allHeaders.length).toString() + '\r\n';
            //console.log(renderedData);
        }

       
        var uri = 'data:text/csv;charset=utf-8,' + escape(finalCsv);

        var link = document.createElement("a");    
        link.href = uri;
    
        link.style = "visibility:hidden";
        link.download = fileName + ".csv";
    
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    },

})