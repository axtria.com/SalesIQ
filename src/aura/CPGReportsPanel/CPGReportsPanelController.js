({
    
    getOrgNamespace: function(component,event,helper){
        
         var action = component.get("c.getOrgNamespaceServer");
         action.setCallback(this,function(response){
              var state = response.getState();
              if(state === 'SUCCESS' && response.getReturnValue() != null){
                  var namespace=response.getReturnValue();
                  component.set("v.namespace",namespace);
              }
              if(state === 'SUCCESS' && response.getReturnValue() == null){
                  var namespace=response.getReturnValue();
                  component.set("v.namespace",'');
              }
             
              component.loadOptions();
             
             
         });
        $A.enqueueAction(action);
        
    },
    loadOptions: function (component, event, helper) {
    	console.log('inside load options');
        component.set("v.options", [{'label':'Select Team','value':'Select Team'}]);
        component.set("v.previousScenariooptions",[{'label':'Select Scenario','value':'Select Scenario'}]);
        component.set("v.scenariooptions",[{'label':'Select Scenario','value':'Select Scenario'}]);
        component.set("v.previousScenarioStage","");
        component.set("v.previousScenariolastRundate","");
        component.set("v.scenarioStage",""); 
        component.set("v.finalresult",""); 
        var action = component.get("c.getTeamInstanceDetails");
        action.setCallback(this,function(response)
        {
        var state = response.getState();
        if(state === 'SUCCESS' && response.getReturnValue != null)
        {
        	var teamParsedSet = response.getReturnValue();
        	component.set("v.teamParsedSet", teamParsedSet);
        	console.log('teamParsedSet :::: ');
        	console.log(teamParsedSet);
        	//component.set("v.options", opts);
        	var jsonOpts=[];
            jsonOpts.push({'label':'Select Team' ,'value':'Select Team'});
        	for(var i=0;i<teamParsedSet.length;i++){
        		jsonOpts.push({'label':teamParsedSet[i],'value':teamParsedSet[i]})
        	}
        	console.log('jsonOpts');
        	console.log(jsonOpts);
            component.set("v.options", jsonOpts);
            component.set("v.teamParsedSet", teamParsedSet);
             component.getInstanceVal();
           // component.selectScenarioOptions();
        }
         
    });

    $A.enqueueAction(action);
},

    onTeamSelection : function(component,event,helper){

        var selectedTeamValue = component.find('mySelect').get('v.value');
        console.log('selectedTeamValue ::: '+selectedTeamValue);
        component.set("v.selectedTeamValue",selectedTeamValue);
        component.set("v.previousScenariooptions",[{'label':'Select Scenario','value':'Select Scenario'}]);
        component.set("v.scenarioSelectedValue",'Select Scenario');
        component.set("v.previousScenarioValue",'Select Scenario');
        component.set("v.previousScenarioStage","");
        component.set("v.previousScenariolastRundate","");
        component.set("v.scenarioStage",""); 
        component.set("v.finalresult",""); 
        
        component.selectScenarioOptions();
    },


    selectScenarioOptions: function (component, event, helper) {
        var j$=jQuery.noConflict();
       // alert(j$("select[name='previousScenarioSelect']").length);
    	
    	console.log('inside selectScenarioOptions');

        var selectedTeamValue = component.get("v.selectedTeamValue");
       

        var action = component.get("c.getScenarioInstances");
        action.setParams
        ({
            teamoptions : selectedTeamValue,
        });
        action.setCallback(this,function(response)
        {
        var state = response.getState();
        if(state === 'SUCCESS' && response.getReturnValue != null)
        {
        	console.log('inside success selectScenarioOptions');
        	var displayscenarioList = response.getReturnValue();
        	component.set("v.displayscenarioList", displayscenarioList);
        	console.log('displayscenarioList :::: ');
        	console.log(displayscenarioList);
        	//component.set("v.options", opts);
        	var ScenariojsonOpts=[];
            ScenariojsonOpts.push({'label':'Select Scenario','value':'Select Scenario'});
        	for(var i=0;i<displayscenarioList.length;i++){
        		ScenariojsonOpts.push({'label':displayscenarioList[i][common.getKey(component,"Scenario_Name__c")],'value':displayscenarioList[i].Id})
        	}
        	console.log('ScenariojsonOpts');
        	console.log(ScenariojsonOpts);
            component.set("v.scenariooptions", ScenariojsonOpts);


        }
         
    });

    $A.enqueueAction(action);
},

    oncurrentScenarioSelection : function(component,event,helper)
    {
       

       // var $j=jQuery.noConflict();
        var selectedCurrentScenarioValue = component.find('scen_select').get('v.value');
        console.log('selectedCurrentScenarioValue ::: '+selectedCurrentScenarioValue);
        component.set("v.selectedCurrentScenarioValue",selectedCurrentScenarioValue);
        component.setValues();
        component.getPreviousScenario();
       //alert($j(".prevSelect").html());

    },
    
    setValues:function(component,event,helper)
    {
        var scenarioList=component.get("v.displayscenarioList");
        var selectedScenarioValue=component.get("v.selectedCurrentScenarioValue");
        for(var ctr=0;ctr<scenarioList.length;ctr++)
        {
            if(selectedScenarioValue==scenarioList[ctr].Id)
            {
                if(selectedScenarioValue==='Select Scenario'){
                     component.set("v.previousScenariooptions",[{'label':'Select Scenario','value':'Select Scenario'}]);
                }
                var scenarioStage=scenarioList[ctr][common.getKey(component,"Scenario_Stage__c")];
                console.log("scenarioStage"+scenarioStage);
                var lastRundate=scenarioList[ctr].LastModifiedDate;
                console.log("lastRundate",lastRundate);
                var sublastRunDate = lastRundate.substring(0,10);
                component.set("v.scenarioFlag","Y");
                component.set('v.sublastRunDate',sublastRunDate);      /*Call method to get the formatted date*/
                var formatteddate = component.convertDate();
                
                //console.log(formatteddate);
                component.set("v.lastRundate",lastRundate);
                var formattedtime = component.convertTimeStamp();  /*Call method to get the formatted time*/
               // console.log(formattedtime);
                var finalresult = formattedtime + ' ' + formatteddate;
                console.log('finalresult:::::: '+finalresult);
                component.set("v.finalresult",finalresult);
                console.log('scenarioStage :::: '+scenarioStage);
                console.log('lastRundate :::: '+lastRundate);
                //lastRundate.substring(0,10);
                component.set("v.scenarioStage",scenarioStage);
                component.set("v.finalresult",finalresult);
                component.set("v.previousScenarioStage","");
                component.set("v.previousScenariolastRundate","");
                break;

            }else{
               component.set("v.scenarioStage",""); 
               component.set("v.finalresult",""); 
                 component.set("v.previousScenarioStage","");
                component.set("v.previousScenariolastRundate","");
               
            }

        }

    },

    convertDate : function(component,event,helper){

    if(component.get("v.scenarioFlag") == "Y"){
    var input = component.get("v.sublastRunDate"); 
    }
    else{
    var input = component.get("v.previouslastRunDatesubstr");    
    }  
    var date = new Date(''+input+'T00:00:00Z');
    var y=date.getFullYear();
        var m=date.getMonth()+1;
        var d=date.getDate();
        var mmm="";
        switch(m)
        {
         case 1:
          mmm="Jan";
            break;
            case 2:
          mmm="Feb";
            break;
            case 3:
          mmm="Mar";
            break;
            case 4:
          mmm="Apr";
            break;
            case 5:
          mmm="May";
            break;
            case 6:
          mmm="June";
            break;
            case 7:
          mmm="July";
            break;
            case 8:
          mmm="Aug";
            break;
            case 9:
          mmm="Sept";
            break;
            case 10:
          mmm="Oct";
            break;
            case 11:
          mmm="Nov";
            break;
            case 12:
          mmm="Dec";
            break;
        }
        
        return(d+' '+mmm+' '+y)

    },

    convertTimeStamp : function(component,event,helper)
    {
    if(component.get("v.scenarioFlag") == "Y"){ 
    var timestamp = component.get("v.lastRundate");
    }
    else{
    var timestamp = component.get("v.prevlastRundate");
    } 
    var dateTime = new Date(timestamp);
    dateTime.setHours(dateTime.getHours() - 5);
    dateTime.setMinutes(dateTime.getMinutes() - 30);
    console.log('dateTime value :::: '+ dateTime.getHours());
    var mid,hours;
    if(dateTime.getHours()>12){
        mid = 'pm';
        hours = dateTime.getHours()%12;
    }else{
        mid = 'am';
        hours = dateTime.getHours();
    }    
    var result = hours + ":" + dateTime.getMinutes() + " " + mid + " ";
    return(result);
    }, 

    getPreviousScenario : function(component,event,helper){

        console.log('inside getPreviousScenario');

        var selectedCurrentScenarioValue = component.get("v.selectedCurrentScenarioValue");
        var selectedTeamValue = component.get("v.selectedTeamValue");
        component.set("v.selectedTeamValue",selectedTeamValue);
        console.log('selectedCurrentScenarioValue ::: '+selectedCurrentScenarioValue);
        console.log('selectedTeamValue ::: '+selectedTeamValue);
        var scenariooptions=component.get("v.scenariooptions");	
        var previousScenariooptions = jQuery.grep(scenariooptions, function( n, i ) {
            return ( n.value !== selectedCurrentScenarioValue );
        });
        component.set("v.previousScenariooptions",previousScenariooptions);
        if(selectedCurrentScenarioValue==='Select Scenario'){
            component.set("v.previousScenariooptions",[{'label':'Select Scenario','value':'Select Scenario'}]);
        }


    },

    onpreviousScenarioSelection : function(component,event,helper){

        var selectedPreviousScenarioValue = component.find('previousScenarioSelect').get('v.value');
        console.log('selectedPreviousScenarioValue ::: '+selectedPreviousScenarioValue);
        component.set("v.selectedPreviousScenarioValue",selectedPreviousScenarioValue);
        component.setPreviousScenValues();
    },

      setPreviousScenValues:function(component,event,helper)
    {
        var previousScenarioIdList=component.get("v.displayscenarioList");
        var selectedPreviousScenarioValue=component.get("v.selectedPreviousScenarioValue");
        for(var ctr=0;ctr<previousScenarioIdList.length;ctr++)
        {
            if(selectedPreviousScenarioValue==previousScenarioIdList[ctr].Id)
            {
                var previousScenarioStage=previousScenarioIdList[ctr][common.getKey(component,"Scenario_Stage__c")];
                console.log("previousScenarioStage:::: "+previousScenarioStage);
                var previousScenariolastRundate=previousScenarioIdList[ctr].LastModifiedDate;
                var previouslastRunDatesubstr = previousScenariolastRundate.substring(0,10);
                component.set("v.scenarioFlag","N");
                component.set('v.previouslastRunDatesubstr',previouslastRunDatesubstr);    
                var formatteddate = component.convertDate();   /*Call method to get the formatted date*/
    
                component.set("v.prevlastRundate",previousScenariolastRundate);
                var formattedtime = component.convertTimeStamp();  /*Call method to get the formatted time*/
                var prevfinalresult = formattedtime + ' ' + formatteddate;
                console.log('prevfinalresult:::::: '+prevfinalresult);
                component.set("v.prevfinalresult",prevfinalresult);
                component.set("v.previousScenarioStage",previousScenarioStage);
                component.set("v.previousScenariolastRundate",prevfinalresult);
                 break;

            }else{
                 component.set("v.previousScenarioStage","");
                component.set("v.previousScenariolastRundate","");
            }

        }

    },

    onReportGenerate : function(component,event,helper){ 
        var instancenamevalue = component.get("v.instanceName");
                    console.log("instanceName=+=+="+instancenamevalue);
        var teamValue = component.get("v.selectedValue");
        var slectedScenario = component.get("v.scenarioSelectedValue");
        var selectedPrevScenario = component.get("v.previousScenarioValue");
        var scenarioOpts = component.get("v.scenariooptions");
        console.log(scenarioOpts);
        for(var ctr=0;ctr<scenarioOpts.length;ctr++){
            if(scenarioOpts[ctr].value == slectedScenario){
            console.log(scenarioOpts[ctr].label);
            component.set("v.ScenarioName",scenarioOpts[ctr].label) ;  
            }
        }
        var previousScenariooptions = component.get("v.previousScenariooptions");
        console.log(previousScenariooptions);
        component.set("v.PrevScenarioName",'Select Scenario');
        for(var ctr=0;ctr<previousScenariooptions.length;ctr++){
            if(previousScenariooptions[ctr].value == selectedPrevScenario){
            console.log(previousScenariooptions[ctr].label); 
              component.set("v.PrevScenarioName",previousScenariooptions[ctr].label) ; 
            }
        }   
        console.log("SCENARIO VALUE"+slectedScenario);
        
        
        if(teamValue.trim()!='' && teamValue!="Select Team" && teamValue!='undefined' )
          {
              console.log("inside TEAM");
            var sname = component.find("mySelect");
	        $A.util.removeClass(sname , 'errorInput_in');
              
            if(slectedScenario.trim()!='' && slectedScenario!="Select Scenario" && slectedScenario!='undefined')
            {
                 console.log("inside SCENARIO 1");
                var s1name = component.find("scen_select");
	            $A.util.removeClass(s1name , 'errorInput_in');
                
               /* if(selectedPrevScenario.trim()!='' && selectedPrevScenario!="Select Scenario" && selectedPrevScenario!='undefined')
                {*/
                    console.log("inside SCENARIO 2");
                   // var s2name = component.find("previousScenarioSelect");
	               // $A.util.removeClass(s2name , 'errorInput');
                    
                    console.log("teamValue"+teamValue);
                    console.log("slectedScenario"+slectedScenario);
                    var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth()+1; //January is 0!
                    var yyyy = today.getFullYear();
                    
                    if(dd<10) {
                        dd = '0'+dd
                    } 
                    
                    if(mm<10) {
                        mm = '0'+mm
                    } 
                    
                    today = yyyy + '/' + mm + '/' + dd;
                    
                    console.log("showdateTime value ::::: "+today);
                    
                    component.set("v.showdateTime",today);
                    
                    var diagnosis = component.find('diagnosisdiv');
                    console.log("diagnosis value :: "+diagnosis);
                    
                    var date = new Date();
                    var time = date.toTimeString().split(' ')[0].split(':');
                    var StringTime = (time[0]+':'+time[1]).toString();
                    console.log(time[0]+':'+time[1]);
                    component.set("v.showtime",StringTime);
                    
                    $A.util.removeClass(diagnosis, 'slds-hide');
                    
                    
                    var currentScenarioId = component.get("v.selectedCurrentScenarioValue");
                    var previousScenarioId = component.get("v.selectedPreviousScenarioValue");
                    var currentScenName = component.get("v.ScenarioName");
                    var prevScenName = component.get("v.PrevScenarioName");
                    var instancename = component.get("v.instanceName");
                    var selectedTeam=component.get("v.selectedValue"); 
                    var url=component.get("v.url");
                    var columns=component.get("v.columns");
                    //alert(selectedTeam);
                    console.log("instanceName="+instancename);
                    var reportId = 5;
                   // component.set("v.columns",jsonInstance.columns);
                    
                   var actionColumns=component.get("c.getColumns");
                    actionColumns.setParams({"team":selectedTeam}); 
                    actionColumns.setCallback(this, function(response){
                        
                         var state = response.getState();
                         if(component.isValid() && state==="SUCCESS"){
                             var columns= response.getReturnValue();   
                             component.set("v.columns",columns);
                              console.log('currentScenarioId for webservice:::::: '+currentScenarioId);
                                console.log('previousScenarioId for webservice:::::: '+previousScenarioId);
                                console.log("SCENARIO NAME RECEIVED"+prevScenName);
                                var e = $A.get('e.c:generateBarCharts');
                                e.setParams({
                                    "currentScenarioId": currentScenarioId,
                                    "previousScenarioId": previousScenarioId,
                                    "currentScenarioName": currentScenName,
                                    "previousScenarioName": prevScenName,
                                    "instanceNameValue":instancename,
                                    "teamName":selectedTeam,
                                    "url":url,
                                  //  "columns":columns,
                                    "btnClicked":true
                                    
                                });
                                e.fire(); 
                                var errorIcn = component.find("errorIcon");
                                $A.util.addClass(errorIcn,'slds-hide');
                         }
                    });
                    $A.enqueueAction(actionColumns);
                    
                   
                
                     
              
        }else{
             console.log("SELECT SCENARIO 1");
             var s1name = component.find("scen_select");
	         $A.util.addClass(s1name , 'errorInput_in');
              component.set("v.popoverMessage","Chart cannot be generated"); 
              var errorIcn = component.find("errorIcon");
             $A.util.removeClass(errorIcn,'slds-hide');

        }
        }else{
            console.log("SELECT TEAM");
            var sname = component.find("mySelect");
	        $A.util.addClass(sname , 'errorInput_in');
            component.set("v.popoverMessage","Chart cannot be generated");
            var errorIcn = component.find("errorIcon");
            $A.util.removeClass(errorIcn,'slds-hide');

            
        }
    },
    
    getInstanceValue : function(component, event, helper) {
        
       console.log("inside getInstance method");
        //var selectedTeam=component.get("v.selectedValue"); 
       var action = component.get("c.getInstanceNameAndUrl");
        //action.setParams({"team":''}); 
       action.setCallback(this, function(response){ 
           var instanceName = response.getState();
           console.log("instancestate",instanceName);
           if(instanceName==="SUCCESS")
           {
               console.log('getInstace called ');
                //var instanceNameval = response.getReturnValue();
               var jsonInstance=response.getReturnValue();
               console.log("instanceName===="+jsonInstance.instancename);
                component.set("v.instanceName",jsonInstance.instancename);
               component.set("v.url",jsonInstance.url);
              
               console.log(component.get("v.instanceName"));
           }else{
               console.log("Failed with state:  "+ state);
           }
       });
        $A.enqueueAction(action);
    },
    
      handleMouseEnter : function(component, event, helper) {
                console.log(" called handleMouseEnter");
                
                var popover = component.find("popover");
                $A.util.removeClass(popover,'slds-hide');
     },
                
    handleMouseLeave : function(component, event, helper) {
        
        var popover = component.find("popover");
        $A.util.addClass(popover,'slds-hide');
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },


})