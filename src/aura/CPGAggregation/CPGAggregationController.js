({
    doInit: function(component,event,helper){
        component.showSpinner();
        component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0 !important;}   .forceStyle .viewport.oneHeader.desktop {z-index:0 !important;} .forceStyle.desktop .viewport{overflow:hidden}");
        var $j = jQuery.noConflict(); 
        //alert($j(".close").length);
        $j("#divAggrParent .close").off("click").on("click",function(){
              var object=$j(this);
              var index= $j("#divAggrParent .close").index(object);
              if(index>0){
                 $j(this).parent().parent().remove();
              } 
             else{
                 var errorIcn = component.find("errorIcon");
                 $A.util.removeClass(errorIcn,'slds-hide');
                 
                 component.set('v.popoverMessage','You cannot delete first row of Aggregate'); 
             }
              
              
          });
                
                    
        $j("#divAggrParent .selectField:last").bind("change",function(){
            var objDropdown = $j(this);
            var selectedColumn = $j(this).val();
            helper.setAggregateFunctionOptions(component,event,objDropdown,selectedColumn);
            component.set('v.selectedColumn',selectedColumn);
            component.disableAggregationLevel();
        });
        console.log("Id"+component.get("v.scenarioRuleInstanceId"));
        
        var actionBusinessRuleId = component.get("c.getBusinessRuleId");
        actionBusinessRuleId.setParams({ 
            "scenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
        actionBusinessRuleId.setCallback(this, function(response){
            
            var state = response.getState();
            console.log('state-->'+state);
            if(component.isValid() && state==="SUCCESS")
            {    
                var response= response.getReturnValue();  
                console.log("VAlue received"+response);
                component.set('v.businessRuleTypeId',response);
                component.getFields();
            }
            else{
                component.set("v.mode","Create");
                component.getFields();
                console.log("Failed with state:  "+ state);
            }
        });
        $A.enqueueAction(actionBusinessRuleId); 
        component.getTableName();
        
    },
    disableAggregationLevel:function(component,event,helper){
        
        var j$ = jQuery.noConflict(); 
        var selectedColumn= component.get('v.selectedColumn');
        
        j$("#columndiv li").each(function(index){
            var li=j$(this);
            var isPresent=false;
                     j$("#divAggrParent .selectField").each(function(index){
                           if(li.find("span.fieldName").text()==j$(this).val()){
                                isPresent=true;
                              }
                            
                     });
            if(isPresent){
                li.find("input:checkbox").removeAttr("checked");
                li.find("input:checkbox").attr("disabled","disabled");
            }
            else{
                li.find("input:checkbox").removeAttr("disabled");
            }
         
                    
          });

       
        
        
    },
    getFields : function(component, event, helper) {
        console.log("inside getFields controller");  
        var scenarioRuleInstanceId=component.get("v.scenarioRuleInstanceId");
        var action = component.get("c.GetSourceAndDerivedFields");
        
        action.setParams({"scenarioRuleInstanceDetailsId":scenarioRuleInstanceId}); 
        action.setCallback(this, function(response){
            var state = response.getState();  
            if(component.isValid() && state==="SUCCESS")
            {
                console.log(response.getReturnValue());
                //MapString to listString
                var itemsSample = response.getReturnValue();  
                 component.set("v.columndata",itemsSample); 
                 console.log("FIELDS CALLED");
                 console.log(component.get("v.columndata"));
                 component.getAggregateFunctions();
                
                
            }
            else{
                console.log("Failed with state:  "+ state);
            }
        })
        $A.enqueueAction(action); 
    },
    
    getTableName : function(component, event, helper) {
        
        if(component.get("v.scenarioRuleInstanceId")=="" 
           || component.get("v.scenarioRuleInstanceId")=='undefined'){
            component.set("v.tableName","No Table Found");
        }
        else{
            var action = component.get("c.getInputTableName");
            action.setParams({"scenarioRuleInstanceDetailsId": component.get("v.scenarioRuleInstanceId")}); 
            action.setCallback(this, function(response){
                var state = response.getState();
                console.log(state);  
                if(component.isValid() && state==="SUCCESS"){
                    console.log("Table Value");
                    console.log(response.getReturnValue());
                    //MapString to listString
                    var itemsSample = response.getReturnValue();
                    if(itemsSample!=="" || itemsSample!=="undefined"){
                        
                        for(var ctr=0;ctr<itemsSample.length;ctr++){
                            if(itemsSample[ctr][common.getKey(component,"ds_type__c")]== 'I'){
                                component.set("v.inputTable",itemsSample[ctr][common.getKey(component,"Table_Display_Name__c")]);
                            }
                            else{
                                component.set("v.outputTable",itemsSample[ctr][common.getKey(component,"Table_Display_Name__c")]);
                            } 
                        } 
                        
                    } 
                    else{  
                        component.set("v.tableName","No Table Present");
                    } 
                }
                else{
                    console.log("Failed with state:  "+ state);
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    getAggregateFunctions : function(component,event,helper){
        
        console.log("inside getAggregateFunctions method ");  
        var action = component.get("c.getAggregateFunctionsList");
        
        action.setCallback(this, function(response){ 
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS"){
                
                console.log("Values from AggregationFunction"+response.getReturnValue());
                var aggregateFunctionOptions=response.getReturnValue();
                component.set("v.aggregateFunctionOptions",aggregateFunctionOptions);
                console.log("aggregationFunctions in controller"+component.get("v.aggregateFunctionOptions"));
                component.populateFieldsToAggregate();
            }   
            else{
                console.log("Failed with state:  "+ state);
            }
        });
        $A.enqueueAction(action);
        
    },
    populateFieldsToAggregate : function(component,event,helper){
        console.log("inside populateFieldsToAggregate");
        var arrOptions=[];
        var arrAggreLevelOptions=[];
        var columndata=component.get("v.columndata");
        console.log("columndata==");
        console.log(columndata);
        for(var ctr=0;ctr<columndata.length;ctr++){
           
                arrOptions.push("<option  dataType=\""+columndata[ctr].dataType
                                +"\"  value=\""+columndata[ctr].fieldName+"\" >"
                                +columndata[ctr].fieldName+"</option>");  
            
           // if(columndata[ctr].valueType=="Discrete"){
                arrAggreLevelOptions.push("<li style='margin-top:2px; padding-left: 3px;' class='slds-is-relative slds-p-right_large slds-truncate'> ");
                arrAggreLevelOptions.push("<div class='slds-form-element'><div class='slds-form-element__control'>");
                arrAggreLevelOptions.push("<span class='slds-checkbox'>");
                arrAggreLevelOptions.push("<input class='check' type='checkbox' id='"+columndata[ctr].Id+"' name='"+columndata[ctr].Id+"' /><label class='slds-checkbox__label' for='"+columndata[ctr].Id+"'><span class='slds-checkbox_faux'></span>");
                arrAggreLevelOptions.push("<span class='slds-form-element__label  slds-truncate fieldName' style='width: calc(100% - 1rem);display: inline-block;margin: 0px;'>"+columndata[ctr].fieldName+"</span>");
                arrAggreLevelOptions.push("</label></span></div></div></li>");
            // } 
            
        }
         console.log("arrAggreLevelOptions");
         console.log(arrAggreLevelOptions);
        var $j = jQuery.noConflict(); 
        $j("#divAggrParent .selectField:last").html(arrOptions.join('')); 
        var objDropdown = $j("#divAggrParent .selectField:last");
        var selectedColumn = $j("#divAggrParent .selectField:last").val();
        helper.setAggregateFunctionOptions(component,event,objDropdown,selectedColumn);
        $j("#columndiv ul").append(arrAggreLevelOptions.join(""));
        if(component.get("v.mode")=="Update"){
            component.getExistingDetails();
        }
        else{
            component.hideSpinner();
        }
        
    },
    
    getExistingDetails : function(component, event, helper) {
        
        console.log('Inisde getExistingDetails');
        var action = component.get("c.GetAggregationType");
        action.setParams({"businessRuleId": component.get("v.businessRuleTypeId")}); 
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('state of getExistingDetails-->'+state);
            if(component.isValid() && state==="SUCCESS")
            {
                
                var itemsSample = response.getReturnValue();
                console.log(itemsSample);
                if(itemsSample[0].lstWbusinessRules){
                    component.set("v.ruleName",itemsSample[0].lstWbusinessRules[0].Name);
                 }
                if(itemsSample[0].lstWAggregateDetails[0]){
                    component.set("v.aggName",itemsSample[0].lstWAggregateDetails[0][common.getKey(component,"Aggregate_Field__c")]);
                    
                    var $j=jQuery.noConflict(); 
                    $j("#divAggrParent .selectField:last").val(itemsSample[0].lstWAggregateDetails[0][common.getKey(component,"Field_Name__c")]); 
                    console.log($j("#divAggrParent .selectFunction:last").val);
                    helper.setAggregateFunctionOptions(component,event,$j("#divAggrParent .selectField:last"),itemsSample[0].lstWAggregateDetails[0][common.getKey(component,"Field_Name__c")]);
                     $j("#divAggrParent .selectFunction:last").val(itemsSample[0].lstWAggregateDetails[0][common.getKey(component,"Aggregate_Function__c")]);
                    for(var ctr=1;ctr<itemsSample[0].lstWAggregateDetails.length;ctr++){
                        component.createAggregateFields(); 
                        $j("#divAggrParent .selectField:last").val(itemsSample[0].lstWAggregateDetails[ctr][common.getKey(component,"Field_Name__c")]);
                        $j("#divAggrParent .slds-p-bottom_small:last").find(".slds-input:eq(0)").val(itemsSample[0].lstWAggregateDetails[ctr][common.getKey(component,"Aggregate_Field__c")]);
                        var objDropdown = $j("#divAggrParent .selectField:last");
                        var selectedColumn = $j("#divAggrParent .selectField:last").val();
                        console.log(selectedColumn);                       
                        helper.setAggregateFunctionOptions(component,event,objDropdown,selectedColumn);
                        $j("#divAggrParent .selectFunction:last").val(itemsSample[0].lstWAggregateDetails[ctr][common.getKey(component,"Aggregate_Function__c")]); 
                        
                    } } 
                for(var ctr1=0;ctr1<itemsSample[0].lstWAggregateRuleLevel.length;ctr1++){
                    console.log("VALUESSS"+itemsSample[0].lstWAggregateRuleLevel[ctr1][common.getKey(component,"group_field__c")]);
                    $j("#columndiv li").each(function(index){
                        
                        if($j(this).find("span.fieldName").text()==itemsSample[0].lstWAggregateRuleLevel[ctr1][common.getKey(component,"group_field__c")]){
                           $j(this).find("input:checkbox").prop("checked",true);
                        }
                    })
                    
                }
                component.disableAggregationLevel();
                
                
            }else{
                console.log("Failed with state:  "+ state);
            }
            component.hideSpinner();
        });
        $A.enqueueAction(action); 
        window.setTimeout( $A.getCallback(function() {
            console.log('Calling');   
        }), 200
                         ); 
    },
    createAggregateFields:function(component, event, helper) {
        
        console.log("inside createAggregateFields method");
        var $j = jQuery.noConflict();
        $j("#divAggrParent .slds-p-bottom_small:last").clone(true).appendTo("#divAggrParent");
        
        $j("#divAggrParent .slds-p-bottom_small:last").find(".slds-input:eq(0)").val('');
        
        
        console.log($j("#divAggrParent .slds-p-bottom_small").length);
        var x = $j("#divAggrParent .slds-p-bottom_small").length;
        $j("#divAggrParent .slds-p-bottom_small:last").find(".slds-float_right:eq(0)").html($j("#divAggrParent .slds-p-bottom_small").length);
        $j("#divAggrParent .slds-p-bottom_small .slds-button-group:last").find(".close").removeClass("slds-hide");
        
        for(var ctr=0;ctr< x-1;ctr++)
        {
            $j("#divAggrParent .slds-p-bottom_small .slds-button-group:eq("+ctr+")").find(".down").removeClass("slds-hide");
        }
        if(x>1)
        {
            $j("#divAggrParent .slds-p-bottom_small .slds-button-group:last").find(".up").removeClass("slds-hide"); 
        }
        
        $j(".selectRank:last").off("change").on("change",function(){
            helper.setOrderOptions(component,event,$j(this),$j(this).val());
        }); 
        $j("#divAggrParent .close").off("click").on("click",function(){
              var object=$j(this);
              var index= $j("#divAggrParent .close").index(object);
              if(index>0){
                $j(this).parent().parent().parent().remove();
                $j(this).parent().parent().remove();
                $j(this).remove();
              } 
             else{
                 var errorIcn = component.find("errorIcon");
                 $A.util.removeClass(errorIcn,'slds-hide');
                 component.set('v.popoverMessage','You cannot delete first row of Aggregate'); 
             }
              
              
          });
        $j(".up").off("click").on("click",function(){
            
            var currentDiv= $j(this).parent().parent();
            var prevDiv=currentDiv.prev();
            if(prevDiv.length>0){
                prevDiv.before(currentDiv);
            }
            var currentDivRankno = currentDiv.find('.rankBy').html();
            var prevDivRankno = prevDiv.find('.rankBy').html();
            currentDiv.find('.rankBy').html(prevDivRankno);
            prevDiv.find('.rankBy').html(currentDivRankno);
            
        });
         $j(".down").off("click").on("click",function(){
             var currentDiv= $j(this).parent().parent();
             var nextDiv=currentDiv.next();
             if(nextDiv.length>0){
               nextDiv.after(currentDiv);
             }
             var currentDivRankno = currentDiv.find('.rankBy').html();
                var nextDivRankno = nextDiv.find('.rankBy').html();
            currentDiv.find('.rankBy').html(nextDivRankno);
            nextDiv.find('.rankBy').html(currentDivRankno);
           
          });
        component.disableAggregationLevel();
    },
   
    restrictInputsRule :function(component, event, helper){
        console.log("inside restrictInputsRule");  
        var text=component.get("v.ruleName"); 
        text = text.replace(/(?!^-)[^0-9a-zA-Z_\s]/g, ''); 
        console.log(text); 
        component.set("v.ruleName",text);
        
    },
    
    saveDetails : function(component,event,helper){
        
        var errorIcn = component.find("errorIcon");
        var j$=jQuery.noConflict(); 
        console.log("SAVE DETAILS CALLED");
        var continuityFlag = false;
        var fieldmissingFlag = true;
        var ruleNameFlag = false;
         var ruleN = component.get("v.ruleName");
        console.log(ruleN);
         if(ruleN){
             if(ruleN.trim()){
            ruleNameFlag =true;
              }
        }
        j$("#columndiv li").each(function(index){
            
            if(j$(this).find("input:checkbox").prop("checked")){
                continuityFlag = true;
            }
        });
       
        
        var jsonBusinessRules={}; 
        var arrAggrRuleDetails=[];
        var arrRuleLevelDetails=[];
        var arrBusinessRuleFieldMapDetail=[];
        var ruleLevelIndex=1;
       
        var columns=component.get("v.coldataValues");
        var keyBusinessRules=common.getKey(component,"Business_Rules__c");
        jsonBusinessRules[keyBusinessRules]={};
        jsonBusinessRules[keyBusinessRules]["Name"]= ruleN;
        jsonBusinessRules[keyBusinessRules][common.getKey(component,"Is_Derived__c")]=true;
        jsonBusinessRules[keyBusinessRules][common.getKey(component,"Is_Mandatory__c")]=true;
        jsonBusinessRules[keyBusinessRules][common.getKey(component,"ScenarioRuleInstanceDetails__c")]=component.get("v.scenarioRuleInstanceId");
        
        j$("#divAggrParent .slds-p-bottom_small").each(function(index){ 
            var jsonAggrRuleDetails={};
            jsonAggrRuleDetails[common.getKey(component,"Seq_No__c")]=index+1; 
            if(j$(this).find("input.slds-input").val().trim()){ 
                console.log(arrAggrRuleDetails);
               for(var ctr=0;ctr<arrAggrRuleDetails.length;ctr++){
                    if(arrAggrRuleDetails[ctr][common.getKey(component,"Aggregate_Field__c")] == j$(this).find("input.slds-input").val() ){
                        fieldmissingFlag = false;
                        component.set("v.popoverMessage","Duplicate field name");  
                        $A.util.removeClass(errorIcn,'slds-hide'); 
                        return;
                    }  
                    console.log("Duplicate check===>"+j$(this).find("input.slds-input").val());
                }
                jsonAggrRuleDetails[common.getKey(component,"Aggregate_Field__c")]=j$(this).find("input.slds-input").val();
                jsonAggrRuleDetails[common.getKey(component,"Field_Name__c")]=j$(this).find("select.selectField").val();
                jsonAggrRuleDetails[common.getKey(component,"Aggregate_Function__c")]=j$(this).find("select.selectFunction").val();
                jsonAggrRuleDetails[common.getKey(component,"Data_Type__c")]="Numeric";
                jsonAggrRuleDetails[common.getKey(component,"Value_Type__c")]="Continuous";
                arrAggrRuleDetails.push(jsonAggrRuleDetails);
                 
                
            }else{
                fieldmissingFlag = false;
                component.set("v.popoverMessage","Please enter derived field name"); 
                
                $A.util.removeClass(errorIcn,'slds-hide');
                var sname = component.find("name_text");
                $A.util.removeClass(sname , 'errorInput');
                return;
                
            }
            
            
            var jsonBusinessRuleFieldMapDetail={};
            
                for(var ctr=0;ctr<columns.length;ctr++){
                        if(columns[ctr].fieldName.toLowerCase()==j$(this).find("select.selectField").val().toLowerCase()
                           && columns[ctr].variableType=="Derived"){
                              jsonBusinessRuleFieldMapDetail[common.getKey(component,"IO_Flag__c")]="I";
                              jsonBusinessRuleFieldMapDetail[common.getKey(component,"Param_Name__c")]=j$(this).find("select.selectField").val().toLowerCase();
                              jsonBusinessRuleFieldMapDetail[common.getKey(component,"Param_Data_Type__c")]=columns[ctr].dataType;
                              jsonBusinessRuleFieldMapDetail[common.getKey(component,"Param_Type__c")]="Derived";
                              arrBusinessRuleFieldMapDetail.push(jsonBusinessRuleFieldMapDetail);
                            break;
                        }
                }
                
              jsonBusinessRuleFieldMapDetail[common.getKey(component,"IO_Flag__c")]="O";
              jsonBusinessRuleFieldMapDetail[common.getKey(component,"Param_Name__c")]=j$(this).find("input.slds-input").val().toLowerCase();
              jsonBusinessRuleFieldMapDetail[common.getKey(component,"Param_Data_Type__c")]="Numeric";
              jsonBusinessRuleFieldMapDetail[common.getKey(component,"Param_Type__c")]="Derived";
              arrBusinessRuleFieldMapDetail.push(jsonBusinessRuleFieldMapDetail);    
           
            // arrBusinessRuleFieldMapDetails.push(jsonAggrRuleDetails[common.getKey(component,"Aggregate_Field__c")]);
            
        });
        j$("#columndiv li").each(function(index){
            
            if(j$(this).find("input:checkbox").prop("checked")){
                var idVal = component.get("v.columndata");
                var jsonRuleLevel={};
                jsonRuleLevel[common.getKey(component,"salesForce_Id")]=j$(this).find("input.check").attr('id');

                 for(var i=0;i<idVal.length;i++){
                    
                    if(idVal[i].Id == j$(this).find("input.check").attr('id')){
                        jsonRuleLevel[common.getKey(component,"tb_col_nm__c")]= idVal[i].tblColName;
                        jsonRuleLevel[common.getKey(component,"variable_type__c")]= idVal[i].variableType;
                    }
                 }
                jsonRuleLevel[common.getKey(component,"Display_Seq_No__c")]=ruleLevelIndex;
                jsonRuleLevel[common.getKey(component,"group_field__c")]=j$(this).find("span.fieldName").text();
                jsonRuleLevel[common.getKey(component,"Is_selected__c")]=true;
                arrRuleLevelDetails.push(jsonRuleLevel);
                
               
                for(var ctr=0;ctr<columns.length;ctr++){
                        if(columns[ctr].fieldName.toLowerCase()==j$(this).find("span.fieldName").text().toLowerCase()
                           && columns[ctr].variableType=="Derived"){
                              jsonBusinessRuleFieldMapDetail[common.getKey(component,"IO_Flag__c")]="I";
                              jsonBusinessRuleFieldMapDetail[common.getKey(component,"Param_Name__c")]=j$(this).find("span.fieldName").text().toLowerCase();
                              jsonBusinessRuleFieldMapDetail[common.getKey(component,"Param_Data_Type__c")]=columns[ctr].dataType;
                              jsonBusinessRuleFieldMapDetail[common.getKey(component,"Param_Type__c")]="Derived";
                              arrBusinessRuleFieldMapDetail.push(jsonBusinessRuleFieldMapDetail);
                            break;
                        }
                }
                
                ++ruleLevelIndex;
            }
        });
        
        
        
        console.log(JSON.stringify(jsonBusinessRules));
        console.log(JSON.stringify(arrAggrRuleDetails));
        console.log(JSON.stringify(arrRuleLevelDetails));
        console.log(JSON.stringify(arrBusinessRuleFieldMapDetail));
        
        if(ruleNameFlag){
        if(continuityFlag )
        {
            if(fieldmissingFlag){
                component.showSpinner();
                if(component.get("v.mode")=="Update")
                {
                    
                    jsonBusinessRules[common.getKey(component,"Business_Rules__c")]["Id"]= component.get("v.businessRuleTypeId");
                    var actionSave = component.get("c.updateAgg"); 
                    var param={strJsonBusinessRules:JSON.stringify(jsonBusinessRules),
                               strJsonAggrRuleDetails:JSON.stringify(arrAggrRuleDetails),
                               strJsonRuleLevelDetails:JSON.stringify(arrRuleLevelDetails),
                               strJsonBRFMDetails:JSON.stringify(arrBusinessRuleFieldMapDetail)};
                    actionSave.setParams(param);
                    
                    
                    
                    actionSave.setCallback(this, function(response){
                        
                        var state = response.getState();
                        console.log(state);
                        if(component.isValid() && state==="SUCCESS"){
                            component.close();
                        }  
                        else{
                            console.log("Failed with state:  "+ state);
                        } 
                    });
                    $A.enqueueAction(actionSave);
                    
                }
                else{
                    
                    console.log("BRFMD JSON");
                    console.log(JSON.stringify(arrBusinessRuleFieldMapDetail));
                    var actionSave = component.get("c.saveAgg");                    
                    var param={strJsonBusinessRules:JSON.stringify(jsonBusinessRules),
                               strJsonAggrRuleDetails:JSON.stringify(arrAggrRuleDetails),
                               strJsonRuleLevelDetails:JSON.stringify(arrRuleLevelDetails),
                               strJsonBRFMDetails:JSON.stringify(arrBusinessRuleFieldMapDetail)};
                    actionSave.setParams(param);
                    
                    
                    
                    actionSave.setCallback(this, function(response){
                        
                        var state = response.getState();
                        console.log(state);
                        if(component.isValid() && state==="SUCCESS"){
                            component.close();
                        }  
                        else{
                            console.log("Failed with state:  "+ state);
                        } 
                    });
                    $A.enqueueAction(actionSave);
                    
                }
               component.hideSpinner();           
            }
                else{ 
                    
                    $A.util.removeClass(errorIcn,'slds-hide');
                }  
            } else{
                //alert("Please select Aggregation Level");
                component.set("v.popoverMessage","Please check aggregation level"); 
                var errorIcn = component.find("errorIcon");
                $A.util.removeClass(errorIcn,'slds-hide');
                
            }
        }else{
                component.set("v.popoverMessage","Please enter rule name"); 
                var errorIcn = component.find("errorIcon");
                $A.util.removeClass(errorIcn,'slds-hide');
            
               var sname = component.find("name_text");
                $A.util.addClass(sname , 'errorInput');
            }
            
        },
    handleMouseEnter : function(component, event, helper) {
                console.log(" called handleMouseEnter");
               
                var popover = component.find("popover");
                $A.util.removeClass(popover,'slds-hide');
     },
                
    handleMouseLeave : function(component, event, helper) {
        
        var popover = component.find("popover");
        $A.util.addClass(popover,'slds-hide');
    },
                    
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
                        
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
                            
    closeButton : function(component, event, helper) {
                 component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");               
                                console.log('Inside close');
                                var scenarioInstID =component.get("v.scenarioRuleInstanceId");
                                console.log(scenarioInstID);
                                var action = component.get("c.getScenarioID");
                                action.setParams({ "ScenarioRuleInstanceId" : scenarioInstID});
                                
                                action.setCallback(this, function(response){ 
                                    var state = response.getState();
                                    if(component.isValid() && state==="SUCCESS"){
                                        var arrListValues=response.getReturnValue();
                                        console.log("navigating to CPGBusinessRuleCanvas");
                                        var evt = $A.get("e.force:navigateToComponent");
                                        evt.setParams({
                                            componentDef : "c:CPGbusinessRuleCanvas",
                                            componentAttributes: {
                                                record_Id : arrListValues[0][common.getKey(component,"Scenario_Id__c")],
                                                viewcanvas : "Y",
                                                workSpaceId : component.get("v.workSpaceId"),
                                                "namespace":component.get("v.namespace"),
                                                summarizedViewFlag : component.get("v.summarizedViewFlag")
                                            }
                                        });
                                        evt.fire();
                                        
                                    }
                                    else{
                                        console.log("Failed with state:  "+ state);
                                    }
                                })
                                $A.enqueueAction(action);
                                
                                
                            }  ,
    destoryCmp : function (component, event, helper) {
        
        component.destroy();
    }
        
        
        
        
        
    })