({
  setInfoText: function(component, labels) {
    if (labels.length == 0) {
      component.set("v.infoText", "Select an option...");
    }
    if (labels.length == 1) {
      component.set("v.infoText", labels[0]);
    }
    else if (labels.length > 1) {
      var label = '';
      labels.forEach(function(element) {
          label += element + ', ' 
      });
    label = label.slice(0, label.length - 2);
      component.set("v.infoText",label);
    }
  },

  getSelectedValues: function(component){
    var options = component.get("v.options_");
    var values = [];
    if(options!=undefined || options!=null || options!=''){
      options.forEach(function(element) {
        if (element.selected) { 
          values.push(element.value);
        }
      });
    }
    return values;
  },

  clearSelectedValues : function(component){
    var options = component.get("v.options");
    //console.log(options);
    var values = [];
    if(options!=undefined || options!=null || options!=''){
      options.forEach(function(element) {
        if (element.selected) { 
          element.selected = false;
        }
        values.push(element);
      });
    }
    component.set('v.options',values);
    component.set('v.options_',values);    
  },

  getSelectedLabels: function(component){
    var options = component.get("v.options_");
    var labels = [];
      if(options!=undefined || options!=null || options!=''){
      options.forEach(function(element) {
        if (element.selected) {
          labels.push(element.label);
        }
      });
  }
    return labels;
  },
  despatchSelectChangeEvent: function(component,values){
      var compEvent = component.getEvent("selectChange");
      console.log(' event values ');
      console.log(values);
      compEvent.setParams({ "values": values });
      compEvent.fire();
  },

  setOptions : function(component,event,helper){
     // if(!$A.util.isUndefined(event))
        // event.stopPropagation();
    var options2 = component.get("v.options2");
    // console.log('options2');
    // console.log(options2);
    var options = [];
    var optionKeys = {};
    var hasSelectedValues = false;
    var optionsSet = new Set(options2);
    var optionsSet2 = new Set();
    var i = 0;
    for (let value of optionsSet){
      if( value != '' &&  value!= null && value!= 'null' && value!= undefined ){
        var value_ = value.toUpperCase();
        if(!optionsSet2.has(value_)){
          options.push(new Object({ value :  value_, label : value_}));
          optionsSet2.add(value_);
        }
        optionKeys[value_] = i;
        i++;
      }
    }
    // for (var i = 0; ( i < options2.length ) ; i++) {
    //   if( options2[i]!= '' && options2[i]!= null && options2[i]!= 'null' && options2[i] != undefined )
    //     options.push(new Object({ value :  options2[i], label :  options2[i]}));
    //     optionKeys[options2[i]] = i;
    // }   
    var selectedValues = component.get("v.selectedValues");
    if(selectedValues != undefined && selectedValues.length > 0 ){
      hasSelectedValues = true;
      var valueArray = selectedValues.split(',');
      for (var j = 0; ( j < valueArray.length ) ; j++) {
        if(optionKeys.hasOwnProperty(valueArray[j])){
          var arrayIndex = optionKeys[valueArray[j]];
          var opt_ = options[arrayIndex];
          opt_['selected'] = true;
          options[arrayIndex] = opt_;
        }
      }
    }
    if( hasSelectedValues ){
      component.set("v.infoText",selectedValues);
    }
    component.set("v.options",options);
    component.set("v.options_",options);
  }

})
