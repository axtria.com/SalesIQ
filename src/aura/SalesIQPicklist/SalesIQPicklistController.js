({
    doInit: function(component, event, helper) 
    {
      console.log('init picklist');
      component.picklistData();
      //var scId = component.get("v.scId");  
    },

    getPicklistData : function(component,event,helper)
    {
      console.log('picklist data--->');
      var scId = component.get("v.scId");
      var selectedColumn = component.get("v.fieldName");
      if(scId == null || scId == undefined || selectedColumn == null || selectedColumn == undefined)
      {
        return;
      }
      console.log(selectedColumn);
      var myAction = component.get("c.getInstanceNameAndURL");
      myAction.setParams({
          "scenarioRuleInstanceId" : scId
      });
      myAction.setCallback(this, function(response)
      {
        var state = response.getState();
        if(component.isValid() && state==="SUCCESS")
        { 
          var result = response.getReturnValue();
          // var selectedColumn = component.get("v.fieldName");
          if(selectedColumn == null || selectedColumn == undefined)
            return;

          
          var is_managed = (component.getType().split(':')[0] == 'c' ) ? 'false' : 'true';
          //SSL IMPL
          //var finalURL = result[0]+'?columns='+selectedColumn+'&'+'tableName='+result[1]+'&'+'dbname='+result[2].replace('/','')+'&'+'where_clause='+''+'&'+'finalTables='+result[3]+'&'+'countryId='+result[4]+'&'+'dataSetId='+result[5]+'&'+'scenarioId='+result[6]+'&'+'is_managed='+is_managed;
          

          var urlParamsMaps = {};
          urlParamsMaps['finalUrl'] = result[0];
          urlParamsMaps['columns'] = selectedColumn;
          urlParamsMaps['tableName'] = result[1];
          urlParamsMaps['dbname'] = result[2].replace('/','');
          urlParamsMaps['where_clause'] = '';
          urlParamsMaps['finalTables'] = result[3];
          urlParamsMaps['countryId'] = result[4];
          urlParamsMaps['dataSetId'] = result[5];
          urlParamsMaps['scenarioId'] = result[6];
          urlParamsMaps['is_managed'] = is_managed;



          //console.log('finalURL :' + finalURL);
          console.log('urlParamsMaps :');
          console.log(urlParamsMaps);
          //var actionColumnValues = component.get('c.getColumnValue');
          var actionColumnValues = component.get('c.getColumnValues');

          actionColumnValues.setParams({
              "urlParamsMap" :urlParamsMaps
          });

          actionColumnValues.setCallback(this, function(response)
          {
              var state = response.getState();
              if(component.isValid() && state==="SUCCESS")
              { 
               
                  var result = response.getReturnValue();
                  console.log('result'+ result);
                  console.log(result);

                try{
                 

                  

                  var jsonOptions= JSON.parse(result);
                  var options_ = Object.values(jsonOptions)[0].sort();

                  component.set("v.options2",options_);
                  var options2 = options_;
                  var options = [];
                  var optionKeys = {};
                  var hasSelectedValues = false;
                  var optionsSet = new Set(options_);
                  console.log('optionsSet');
                  console.log(optionsSet);
                  var optionsSet2 = new Set();
                  var i = 0;
                  for (let value of optionsSet){
                    if( value != '' &&  value!= null && value!= 'null' && value!= undefined ){
                      var value_ = value;
                      if(!optionsSet2.has(value_)){
                        options.push(new Object({ value :  value_, label : value_}));
                        optionsSet2.add(value_);
                        optionKeys[value_] = i;
                        i++;
                      }
                    
                    }
                  }

                  var selectedValues = component.get("v.selectedValues");
                  if(selectedValues != undefined && selectedValues.length > 0 )
                  {
                    hasSelectedValues = true;
                    var valueArray = selectedValues.split(',');
                    for (var j = 0; ( j < valueArray.length ) ; j++) {
                      if(optionKeys.hasOwnProperty(valueArray[j])){
                        var arrayIndex = optionKeys[valueArray[j]];
                        var opt_ = options[arrayIndex];
                        opt_['selected'] = true;
                        options[arrayIndex] = opt_;
                      }
                    }
                  }
                  if( hasSelectedValues )
                  {
                    component.set("v.infoText",selectedValues);
                  }
                  component.set("v.options",options);
                  component.set("v.options_",options);
              }
              catch(e){
                console.log(e);
                console.log('result');
                console.log(result);
                var toastEvent = $A.get("e.force:showToast");
                var ns = ( component.getType().split(':')[0] == 'c' ) ? 'c' : component.getType().split(':')[0];
                var errorlabel = $A.get('$Label.'+ns+'.webservice_not_available');
                toastEvent.setParams({
                    message: errorlabel,
                    messageTemplate: errorlabel,
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
              }
            }
            });
            $A.enqueueAction(actionColumnValues);      
        }
        else
        {
          //alert('error');
        }
      });
      $A.enqueueAction(myAction);
      console.log(component.get("v.scId"));
      console.log(component.get("v.fieldName"));
      var options2 = component.get("v.options2");

      
    },

    toggleDropdown: function(component, event, helper) {
      if(!$A.util.isUndefined(event))
        event.stopPropagation();

      if(!$A.util.isUndefined(event.currentTarget.btnid)){
        if ( event.currentTarget.btnid = 'btn-dropdown-picklist' ){
          var dropdown = component.get("v.dropdownOver");
            if(dropdown){
                //component.set("v.options_",component.get("v.options"));
                component.set("v.options_",component.get("v.options_"));
                component.set("v.dropdownOver",false);
                var mainDiv = component.find('main-div');
                $A.util.removeClass(mainDiv, 'slds-is-open');
            }else{
              component.set("v.dropdownOver",true);
              var mainDiv = component.find('main-div');
              $A.util.addClass(mainDiv, 'slds-is-open');
              window.setTimeout(function(){ 
                component.find("searchText").getElement().focus();
            }, 50);
          }
        }
      }
    },

    closeDropdown: function(component, event, helper) {
        
        console.log('closing dropdown');
        //component.set("v.options_",component.get("v.options"));
        //component.set("v.options_",component.get("v.options"));
        component.set("v.dropdownOver",false);
        var mainDiv = component.find('main-div');
        if(!$A.util.isUndefined(mainDiv)){
          $A.util.removeClass(mainDiv, 'slds-is-open');
        }
          var options = component.get("v.options");
        //SET FILTERED OPTIONS
      var selectedValues  = component.get("v.selectedValues");
      var selectedValuesSet = new Set();
        //DO VALIDATION
        try{    
            var inpCmp = component.find('picklistBtn');
            
            debugger;
            if(event.getParam("doValidation")){
                
                if(selectedValues == undefined || selectedValues == ''){
					$A.util.addClass(inpCmp, 'slds-has-error'); 
                }else{
                    $A.util.removeClass(inpCmp, 'slds-has-error'); 
                }
            }else{
                 $A.util.removeClass(inpCmp, 'slds-has-error'); 
            }
        }catch(e){
        }
      if(selectedValues != undefined && selectedValues.length > 0 ){
        selectedValuesSet = new Set(selectedValues.split(','));
      }
      options.forEach( function(element) 
      {
        if (selectedValuesSet.has(element.value)) 
        {
          element.selected =  true;
        }
        else
        {
          element.selected = false;
        }  
        
      }); 
      component.set("v.options_",options); 
    },

    handleSelection: function(component, event, helper) {
        if(!$A.util.isUndefined(event))
          event.stopPropagation();

        //GET SELECTED VALUES

        var isMultiSelect = component.get("v.isMultiSelect");
        var item = event.currentTarget;
        component.set("v.dropdownOver",true);
        //var searchDiv = component.find('searchBoxIcon');
        //var checkDiv = component.find('checkBoxIcon');
    
        //$A.util.addClass(searchDiv, 'slds-hide');
        //$A.util.removeClass(checkDiv, 'slds-hide');
        var options = component.get("v.options_");

        var selectedValues  = component.get("v.selectedValues");
        var selectedValuesSet = new Set();
        if(selectedValues != undefined && selectedValues.length > 0 ){
          selectedValuesSet = new Set(selectedValues.split(','));
        }
        

        if (item && item.dataset) {
          var value = item.dataset.value;
          var selected = item.dataset.selected;
          

          options.forEach( function(element) {
            selectedItems = element.selected;
            if (element.value == value) {
              element.selected = selected == "true" ? false : true;
              if(element.selected){
                if(!selectedValuesSet.has(element.value)){
                  selectedValuesSet.add(element.value);
                }
              }else if(!element.selected){
                if(selectedValuesSet.has(element.value)){
                  selectedValuesSet.delete(element.value);
                }
              }
            }


            if(!isMultiSelect){
              if(element.value == value){
                selectedValuesSet = new Set();
                selectedValuesSet.add(element.value);
              }
              if (!(element.value == value) && element.selected){
                element.selected = false;
              }
            }
          });

          //component.set('v.selectedValuesSet',selectedValuesSet);
          component.set("v.options_", options);
          //var labels = helper.getSelectedLabels(component);
          var selValuesArray = Array.from(selectedValuesSet);
          console.log('selValuesArray');
          console.log(selValuesArray);
          helper.setInfoText(component,selValuesArray);
          component.set("v.selectedItems",selValuesArray);
          component.set("v.selectedValues",selValuesArray.join(','));
          helper.despatchSelectChangeEvent(component,selValuesArray);

        }


    },

    searchOptions: function(component, event, helper) {
      var searchText = component.get('v.searchText');
      //var options = component.get("v.options");
      //FILTER AND SEARCH ALL OPTIONS
      var options = component.get("v.options");


      var filteropts = options.filter( function (item){
        if(!$A.util.isUndefined(item.value) && !$A.util.isUndefined(searchText))
          return (~item.value.toLowerCase().indexOf(searchText.toLowerCase()));
      });


      //SET FILTERED OPTIONS
      var selectedValues  = component.get("v.selectedValues");
        var selectedValuesSet = new Set();
        if(selectedValues != undefined && selectedValues.length > 0 ){
          selectedValuesSet = new Set(selectedValues.split(','));
        }
        

          

          filteropts.forEach( function(element) 
          {
            if (selectedValuesSet.has(element.value)) 
            {
              element.selected =  true;
            }
            else
            {
              element.selected = false;
            }  
            
          }); 
          component.set("v.options_",filteropts); 

      var clearDiv = component.find('clearSearchIcon');

      if(searchText == '' || searchText.length == 0){
          $A.util.addClass( clearDiv, 'slds-hide');
      }else{
          $A.util.removeClass( clearDiv, 'slds-hide');        
      }
    },

    clearSearch: function(component, event, helper) {
      console.log('clicked clear');
      if(!$A.util.isUndefined(event))
        event.stopPropagation();
      
      component.set('v.searchText','');
      var clearDiv = component.find('clearSearchIcon');
      $A.util.addClass(clearDiv, 'slds-hide');
      var options = component.get("v.options");
      //component.set("v.options_",options);
      component.set("v.options",options);


      //var filteropts =  component.get("v.options_");

      //SET FILTERED OPTIONS
      var selectedValues  = component.get("v.selectedValues");
      var selectedValuesSet = new Set();
      if(selectedValues != undefined && selectedValues.length > 0 ){
        selectedValuesSet = new Set(selectedValues.split(','));
      }
      options.forEach( function(element) 
      {
        if (selectedValuesSet.has(element.value)) 
        {
          element.selected =  true;
        }
        else
        {
          element.selected = false;
        }  
        
      }); 
      component.set("v.options_",options); 

    },


    clearSelection: function(component, event, helper) {
        component.set('v.searchText','');
       
        //var checkDev = component.find('checkBoxIcon');
        $A.util.addClass(checkDev, 'slds-hide');

        //var searchDiv = component.find('searchBoxIcon');
        //$A.util.removeClass(searchDiv, 'slds-hide');

        //var options = component.get("v.options_");
        var options = component.get("v.options2_");
          options.forEach(function(element) {
            if (element.selected) {
                element.selected = false ;    
            }
          });
        
        component.set("v.options_", options);

        var labels = helper.getSelectedLabels(component);
        helper.setInfoText(component,labels);
        component.set("v.selectedItems",labels);
        component.set("v.selectedValues",labels.join(','));
    }
  },

  doneRenderingFunc : function(component,event,helper)
  {
    alert('done');
      if(!component.get("v.isDoneRendering"))
      {

          helper.setOptions(component, event, helper);
          // var filterWrapper;
          // if(component.get("v.conditionWrapper") != null)
          // {
              // filterWrapper = JSON.parse(JSON.stringify(component.get("v.conditionWrapper")));
              // if(filterWrapper.criterias.length > 0)
              // {
                  // var picklistCmp = component.find('picklistcmp');
                  // picklistCmp.initPicklist();
                  // var initChildEvt = $A.get("e.c:initPicklistQueryBuilderEvent");
                  // initChildEvt.fire();
                  component.set("v.isDoneRendering",true);
              // }
          // }    
      }
      
  }
})
