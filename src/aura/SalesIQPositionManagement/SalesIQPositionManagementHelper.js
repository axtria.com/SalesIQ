({
	getLocaleDateFormats: function(component) 
    {
        var action = component.get("c.getLocaleDateFormats");
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue() != null)
            {
                var formatList = response.getReturnValue();
                component.set("v.dateFormat", formatList[0]); 
                component.set("v.dateTimeFormat", formatList[1]); 
            }
        });
        $A.enqueueAction(action); 
    },

    logException : function(component, errors) 
    {
        var errorMessage = errors[0].message;
        component.set("v.showConfirm",false);
        var namespace = component.get("v.namespace");
        var errorLabel;
        if(namespace != '')
        {
            errorLabel = $A.get("$Label.AxtriaSalesIQTM.Unexpected_error_notification");
        }
        else
        {
            errorLabel = $A.get("$Label.c.Unexpected_error_notification");
        }

        if (errors[0] && errors[0].message) 
        {
            var action = component.get("c.logException");
            
            action.setParams
            ({
                message : errorMessage,
                module : 'Position Management'
            });
            action.setCallback(this,function(response)
            {
                var state = response.getState();
                if(state === 'SUCCESS')
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: errorMessage,
                        type : 'Error'
                        
                    });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }
        else
        {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                mode: 'dismissible',
                message: errorLabel,
                type : 'error'
                
            });
            toastEvent.fire();
        }
    }
})