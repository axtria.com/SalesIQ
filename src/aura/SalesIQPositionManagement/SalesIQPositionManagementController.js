({
	onRender: function(component,event,helper)
	{
        console.log('onRender called : '+component.get("v.doRender"));
        console.log('active -- '+component.get("v.isActive"));
        
        helper.getLocaleDateFormats(component);
        if(component.get("v.isActive") == true && component.get("v.doRender") == true)
        {
        	component.set("v.modalStyle", ".forceStyle .viewport .oneHeader {z-index:0 !important;  } .uiDatePicker {top: inherit !important; bottom : 0px !important}");
        	
        	component.set("v.doRender",false);
        	component.set("v.showBaseMapping", false);
        	
	        var orgNamespaceAction = component.get("c.getOrgNamespace");
  			orgNamespaceAction.setCallback(this,function(response)
	        {
	        	var state = response.getState();
	            console.log('Namespace : ',response.getReturnValue());
	            if(state == 'SUCCESS' && response.getReturnValue() != null)
	            {
	            	component.set("v.namespace",response.getReturnValue());
	            }

	            console.log('Action : '+component.get("v.action"));
		        console.log('Selected Position : ' + component.get("v.selectedPosition"));
		        

		        if(component.get("v.action") != 'Filter Position'){
		        	var action = component.get("c.initPositionModal");

			        action.setParams
			        ({
			        	action : component.get("v.action"),
		                positionId : component.get("v.selectedPosition"),
		                teamInstanceId : component.get("v.selectedTeamInstance")
		            });
			        
			        action.setCallback(this,function(response)
			        {
			            var state = response.getState();
			            console.log('state : '+state);
			            console.log('response : ',response.getReturnValue());
			            if(state == 'SUCCESS' && response.getReturnValue() != null)
			            {
			            	console.log('set wrapper values --');
			            	var recordsDetails = response.getReturnValue();
			            	console.log('Config data : ',recordsDetails);
			            	component.set("v.popupWrapper", recordsDetails);
			            	console.log('orgNamespaceAction : '+component.get("v.namespace"));
			            	//console.log('Hierarchy_Level__c : '+recordsDetails.position[component.get("v.namespace")+'Hierarchy_Level__c']);
			            	//Get base position data for Overlay position
			            	if(component.get("v.action") != 'Edit Employee'){
				            	if(recordsDetails.position[component.get("v.namespace")+'Hierarchy_Level__c'] == '1' && component.get("v.teamType") == 'Overlay' && (component.get("v.action") == 'Create Position' || component.get("v.action") == 'Edit Overlay Association'))
				            	{
				            		console.log('Fetching base position data');
				            		component.set("v.showBaseMapping", true);
				            		var getBasePositionAction = component.get("c.getBasePositionData");
					            	getBasePositionAction.setParams
					            	({
						                positionId : component.get("v.selectedPosition"),
						                teamInstanceId : component.get("v.selectedTeamInstance"),
						                action : component.get("v.action")
						            });
						            
						            getBasePositionAction.setCallback(this,function(getBasePositionResponse)
						            {
						            	if(getBasePositionResponse.getState() == 'SUCCESS' && getBasePositionResponse.getReturnValue() != null)
						            	{
						            		component.set("v.basePositionWrapperList", getBasePositionResponse.getReturnValue());
						            	}
						            	else
						            	{
						            		helper.logException(component,getBasePositionResponse.getError());
						            	}
						            });
						            $A.enqueueAction(getBasePositionAction);
				            	}
			            	}
			            }
			            else 
			            {
			            	helper.logException(component,response.getError());
			            }
			        });
			        
			        $A.enqueueAction(action);
		        }
		        else{
		        	console.log('--- render querybuilder component --- ');
		        	component.set("v.showFilterPopup",true);
		        	

		        	var operatorAction = component.get("c.getOperatorValues");
		        	operatorAction.setCallback(this,function(response){
		        		console.log('response -- ',response.getReturnValue());
		        		if(response.getState() == 'SUCCESS' && response.getReturnValue() != null){
		        			var value = response.getReturnValue();
		        			//console.log('value : ',value[0].lstOfOperators);
		        			//value = value[0].lstOfOperators;

		        			value.forEach(function(element){
		        				var operatorsList = element.lstOfOperators ;
		        				operatorsList.forEach(function(operatorW){
		        					var val = operatorW.operatorTranslated;
		        					if(val.toLowerCase().indexOf('label.')>=0){
		        						var label ;
                                    	if(component.get("v.namespace") != '')
                                        	label= "$Label.AxtriaSalesIQTM." + val.split('.')[1];
                                        else
                                        	label = "$Label.c." + val.split('.')[1];
                                        // $Label.c.Equal_Operator
                                    // $Label.c.Not_Equal_Operator
                                    // $Label.c.In_Operator
                                    // $Label.c.Not_In_Operator
                                    // $Label.c.Is_Null_Operator
                                    // $Label.c.Is_Not_Null_Operator
                                    // $Label.c.Contains_Operator
                                    // $Label.c.Does_Not_Contains_Operator
                                    // $Label.c.Begins_With_Operator
                                    // $Label.c.Ends_With_Operator
                                    // $Label.c.Between_Operator
                                    // $Label.c.Greater_Or_Equal_Operator
                                    // $Label.c.Greater_Than_Operator
                                    // $Label.c.Less_Or_Equal_Operator
                                    // $Label.c.Less_Than_Operator

	                                    component.set('v.labelAttr',$A.getReference(label));
	                                    if(component.get('v.labelAttr') == ""){
	                                        $A.get('e.force:refreshView').fire();
	                                    }
	                                    val = component.get('v.labelAttr');

		        					}
		        					operatorW.operatorTranslated = val;
		        				});

		        			});
		        			component.set("v.listOfOperators",value);
		        			
		        			console.log('---- operators from Pos Manag:',value);
		        		}
		        		
		        	});
		        	$A.enqueueAction(operatorAction);

		        	console.log('---- render columns name ---- ');
		        	console.log('query ---- '+component.get("v.queryString"));
		        	var columnAction = component.get("c.createColumnWrapperData");
		        	columnAction.setParams({
		        		queryString : component.get("v.queryString")
		        	});
		        	columnAction.setCallback(this,function(response){
		        		if(response.getState() == 'SUCCESS' && response.getReturnValue() != null){
		        			component.set("v.columnNames", response.getReturnValue());
		        			console.log('---columns : ',component.get("v.columnNames"));

		        		}
		        	});
		        	$A.enqueueAction(columnAction);

		        	console.log('---- render columns name ---- ');
		        	var filterWrapperAction = component.get("c.initializeBlock");
		        	filterWrapperAction.setCallback(this,function(response){
		        		if(response.getState() == 'SUCCESS' && response.getReturnValue() != null){
		        			component.set("v.filterWrapper", response.getReturnValue());
		        			console.log('---filterWrapper : ',component.get("v.filterWrapper"));

		        		}
		        	});
		        	$A.enqueueAction(filterWrapperAction);

		        	var childComponent = component.find("queryBuilderAssignment");
             		childComponent.init();

		        	
		        }
		        
	        });
	        $A.enqueueAction(orgNamespaceAction);
        }
    },
    customizeDate : function (component,event,helper) {
        console.log('---customizeDate---- ');
        var eventSource = event.getSource();
        var objWrapper = component.get("v.popupWrapper");
		var posAttList = objWrapper.positionAttributes;
		var ns = component.get("v.namespace");
		console.log(objWrapper);
		console.log(posAttList);
		var attributeArray = []; 
		var finalDate;

		try{
			//position does not have 15or16 rule but a different rule. if start date then 1st day of the month. if end date then last day of the month
			for(var attribute of posAttList){
				console.log('date');
				if(Object.values(attribute)[0][0]!= null && Object.values(attribute)[0][0].fieldDataType.toLowerCase().indexOf('date')!=-1 && Object.values(attribute)[0][0].date15Or16Rule!=null){
					//if(Object.values(attribute)[0][0].date15Or16Rule!=null){
					if(Object.values(attribute)[0][0].date15Or16Rule.split(":")[1].toLowerCase().indexOf('startdate')!=-1 || Object.values(attribute)[0][0].date15Or16Rule.split(":")[1].toLowerCase().indexOf('enddate')!=-1 ){
						attributeArray.push(Object.values(attribute)[0][0]);
					}

				}
				if(Object.values(attribute)[0][1]!=null && Object.values(attribute)[0][1].fieldDataType.toLowerCase().indexOf('date')!=-1 && Object.values(attribute)[0][1].date15Or16Rule!=null){
					if(Object.values(attribute)[0][1].date15Or16Rule.split(":")[1].toLowerCase().indexOf('startdate')!=-1 || Object.values(attribute)[0][1].date15Or16Rule.split(":")[1].toLowerCase().indexOf('enddate')!=-1 ){
						attributeArray.push(Object.values(attribute)[0][1]);
					}
				}


				//ADDED CHANGE EFFECTIVE DATE VALIDATION SIMPS 1280
					if(Object.values(attribute)[0][0]!= null && Object.values(attribute)[0][0].fieldDataType.toLowerCase().indexOf('date')!=-1 && Object.values(attribute)[0][0].fieldApiName == 'Change Effective Date' && Object.values(attribute)[0][0].changeEffectiveDateValidation != undefined && Object.values(attribute)[0][0].changeEffectiveDateValidation)
				{
					attributeArray.push(Object.values(attribute)[0][0]);
				}else if(Object.values(attribute)[0][1]!= null && Object.values(attribute)[0][1].fieldDataType.toLowerCase().indexOf('date')!=-1 && Object.values(attribute)[0][1].fieldApiName == 'Change Effective Date' && Object.values(attribute)[0][1].changeEffectiveDateValidation != undefined && Object.values(attribute)[0][1].changeEffectiveDateValidation)
				{
					attributeArray.push(Object.values(attribute)[0][1]);
				}
			}
			console.log('attributeArray',attributeArray);

			for(var val of attributeArray){
				if(val.date15Or16Rule!=null ){

					var ns = component.get("v.namespace");
					var fieldClass = event.getSource().get("v.class");
					fieldClass = fieldClass.split(' ')[0];

					
					console.log('fieldClass :',fieldClass);
					console.log('val ',val);
					if(val.date15Or16Rule.split(":")[1].toLowerCase().indexOf('startdate')!=-1 && val.isEditable && val.fieldApiName.indexOf(fieldClass) != -1){
						console.log('changing start date');
		                var stDate = val.fieldValue;
		                stDate = new Date(stDate);

		                var gmtStrtdate = new Date(stDate.getUTCFullYear(), stDate.getUTCMonth(), stDate.getUTCDate(), stDate.getUTCHours(), stDate.getUTCMinutes(), stDate.getUTCSeconds()); 
		                //console.log('stDate :'+stDate);                
		                finalDate = new Date(gmtStrtdate.getFullYear(), gmtStrtdate.getMonth(), 1);
		                var dd = finalDate.getDate();
		                var mm = finalDate.getMonth()+1; //As January is 0.
		                var yyyy = finalDate.getFullYear();
		                var dateString = yyyy+'-'+mm+'-'+dd;
		                console.log('dateString',dateString);
		                val.fieldValue = dateString;
		                break;
	                
	            	}
	            	else if(val.date15Or16Rule.split(":")[1].toLowerCase().indexOf('enddate')!=-1 && val.isEditable && val.fieldApiName.indexOf(fieldClass) != -1){
	            		console.log('changing end date');
		                var endDate = val.fieldValue;
		                endDate = new Date(endDate);

		                var gmtEnddate = new Date(endDate.getUTCFullYear(), endDate.getUTCMonth(), endDate.getUTCDate(), endDate.getUTCHours(), endDate.getUTCMinutes(), endDate.getUTCSeconds()); 
                		
               			
		                finalDate = new Date(gmtEnddate.getFullYear(), gmtEnddate.getMonth()+1, 0);
		                var dd = finalDate.getDate();
			            var mm = finalDate.getMonth()+1; //As January is 0.
			            var yyyy = finalDate.getFullYear();
			            var dateString = yyyy+'-'+mm+'-'+dd;
			            console.log('dateString',dateString);
			            val.fieldValue = dateString;
		                break;
	            	}

	            	
	            }

	            if( val.fieldApiName == 'Change Effective Date' && val.changeEffectiveDateValidation != undefined && val.changeEffectiveDateValidation ){
	            	
	            		console.log('changing change effective date');
		                var stDate = val.fieldValue;
		                stDate = new Date(stDate);

		                var gmtStrtdate = new Date(stDate.getUTCFullYear(), stDate.getUTCMonth(), stDate.getUTCDate(), stDate.getUTCHours(), stDate.getUTCMinutes(), stDate.getUTCSeconds()); 
		                //console.log('stDate :'+stDate);                
		                finalDate = new Date(gmtStrtdate.getFullYear(), gmtStrtdate.getMonth(), 1);
		                var dd = 1;
		                var mm = finalDate.getMonth()+1; //As January is 0.
		                var yyyy = finalDate.getFullYear();
		                var dateString = yyyy+'-'+mm+'-'+dd;
		                console.log('dateString change effective date',dateString);
		                val.fieldValue = dateString;
		                break;

	            	}
			}
		}catch(e){
			console.log(e);
		}

		component.set("v.popupWrapper",objWrapper);

    },
    submitRequest: function(component,event,helper)
    {       
	    console.log('##### checkBusinessRuleRunning called');
	    var namespace = component.get("v.namespace");

	    if(component.get("v.action") == 'Filter Position'){
	    	if(component.get("v.isActive") == true && component.get("v.doSubmit") == true){
	    		//component.set("v.errorList",[]);
	    		//component.set("v.showError",false);
				//component.set("v.popOverClass", 'slds-rise-from-ground');
				//component.set("v.showConfirm",true);
            	//component.set("v.doSubmit",false);
            	var queryBuilder = component.find("queryBuilderAssignment");
            	//queryBuilder.viewQuery();
            	queryBuilder.getQuery();
	    	}
	    }else{

	    	//Check the business rule is running or not
	    	var checkBusinessRule = component.get("c.isBusinessRuleRunning");
		    checkBusinessRule.setParams
	        ({
	            teamInstanceID : component.get("v.selectedTeamInstance"),
	        });
        	checkBusinessRule.setCallback(this,function(response){
		    	var state = response.getState();
		        var result = response.getReturnValue();	 
		        console.log('#### result ' + result);
		        
		        if(state === 'SUCCESS' && result != null)
		        {
		        	if(result == true){
		        		var toastEvent = $A.get("e.force:showToast");
		        		var warningMessage;
		        		if(namespace != ''){
		        			warningMessage = $A.get("$Label.AxtriaSalesIQTM.Business_Rule_Running_Warning");
		        		}
		        		else
		        		{
		        			warningMessage = $A.get("$Label.c.Business_Rule_Running_Warning")
		        		}
		                toastEvent.setParams({
		                    mode: 'dismissible',
		                    message: warningMessage,
		                    type : 'Warning'	                        
		                });
		                toastEvent.fire();
		               //Close popup and reset the properties
			        	component.set("v.isActive", false);
				    	component.set("v.doRender", true);
				    	component.set("v.doSubmit", true);
				    	component.set("v.doConfirm", true);
				    	component.set("v.showConfirm",false);
				    	component.set("v.showError",false);
				    	component.set("v.errorMessage",'');
				    	component.set("v.errorList",'');
		        	}else{
		        		console.log('submitRequest called : '+component.get("v.doSubmit"));		    	
				    	console.log('namespace : '+namespace);
						if(component.get("v.isActive") == true && component.get("v.doSubmit") == true)
						{
							var objWrapper = component.get("v.popupWrapper");
							var posAttList = objWrapper.positionAttributes;
							var noChange = true;
							var blankValues = false;
							var noOfCols = 0;
							var noChangeInParentPosition = false;
				
							var inputCmp = component.find("inputFld");
							var errorCmp = component.find("errorTxt");
							var colNo = -1;
							var blankColIndex = new Set();
							var editableColNames = new Array();
				
							component.set("v.showError",false);
							component.set("v.popOverClass", 'slds-rise-from-ground');
							console.log('posAttList ', posAttList);

							var relatedPositionType = '';
							for (var row = 0; row < posAttList.length; row++) 
							{
								var rowData = posAttList[row].rowDataConfig;
								for (var col = 0; col < rowData.length; col++) 
								{
									if(rowData[col].fieldApiName.toLowerCase() == "related_position_type__c"){
										relatedPositionType = rowData[col].fieldValue;
									}

									if(rowData[col].isEditable)
									{
										colNo = colNo+1;
										noOfCols = noOfCols+1;
										editableColNames.push(rowData[col].fieldLabel);
										if(rowData[col].fieldDataType == 'Lookup')
										{
											console.log('lookupData : ',rowData[col].lookupData);
											if(rowData[col].isRequired){
												if(rowData[col].lookupData == null)
												{
													blankValues = true;
													blankColIndex.add(colNo);
													document.getElementById('lookupDiv').style.border='2px solid #c23934';
												}
											}
											if(rowData[col].lookupData != null)
											{
												console.log('New lookupData : '+rowData[col].lookupData.text);
												console.log('New lookupData val : '+rowData[col].lookupData.val);
												console.log('Old lookupData : '+rowData[col].oldFieldValue);
												if(rowData[col].oldFieldValue != rowData[col].lookupData.text){
													
													noChange = false;
													noChangeInParentPosition = false;

												}
												else
												{
													console.log('--- inside else ---');
													if(component.get("v.action") == 'Edit Position Hierarchy' ){
														noChangeInParentPosition = true;
													}
												}
											}
										}
										else
										{
											console.log('New Value : '+rowData[col].fieldValue);
											console.log('Old Value : '+rowData[col].oldFieldValue);
											if(rowData[col].isRequired)
											{
												if(rowData[col].fieldValue == undefined || rowData[col].fieldValue == '')
												{
													blankValues = true;
													rowData[col].fieldValue = '';
													blankColIndex.add(colNo);
												}
											}
											if((rowData[col].fieldValue != rowData[col].oldFieldValue && rowData[col].fieldDataType !='Boolean')||(rowData[col].fieldDataType=='Boolean' && (rowData[col].checkboxValue).toString()!= rowData[col].oldFieldValue))
											{
												noChange = false;
											}
										}
									}
								}
							}
							
							console.log('noOfCols : '+noOfCols);
							console.log('editableColNames : '+editableColNames);
				
							for(var col=0 ; col < noOfCols ; col++)
							{
								if(blankColIndex.has(col))
								{
									console.log('I am col # '+col);
									console.log(editableColNames[col]);
									if(inputCmp.length != undefined)
									{
										$A.util.addClass(inputCmp[col] , 'errorInput');
									}
									else
									{
										$A.util.addClass(inputCmp , 'errorInput');
									}
									if(errorCmp.length != undefined)
									{
										errorCmp[col].set("v.value",editableColNames[col]+' is required');
									}
									else
									{
										errorCmp.set("v.value",editableColNames[col]+' is required');
									}
								}else
								{
									if(inputCmp != undefined && inputCmp.length != undefined)
									{
										$A.util.removeClass(inputCmp[col] , 'errorInput');
									}
									else
									{
										$A.util.removeClass(inputCmp , 'errorInput');
									}
									if(errorCmp.length != undefined)
									{
										errorCmp[col].set("v.value",'');
									}
									else
									{
										errorCmp.set("v.value",'');
									}
								}
							}
				
							var changeBaseMapping = false;
							// check change in overlay mapping 
							var action = component.get("v.action");
							console.log('action --'+action);
							var mirrorBasePosition = false;
							if(action == 'Create Position' && relatedPositionType == "Mirror"){
								var baseWrapper = component.get("v.basePositionWrapperList");
								var noOfChanges = 0;
								for(var i=0; i < baseWrapper.length; i++)
								{
									if(baseWrapper[i].isChecked){
										noOfChanges++;
									}
								}
								if(noOfChanges != 1)
									mirrorBasePosition = true;
							}

							if(action == 'Edit Overlay Association')
							{
								var baseWrapper = component.get("v.basePositionWrapperList");
								for(var i=0; i < baseWrapper.length; i++)
								{
								
									if(((baseWrapper[i].isChecked == true &&  baseWrapper[i].isDefaultChecked == true) || (baseWrapper[i].isChecked == false &&  baseWrapper[i].isDefaultChecked == false)) && !changeBaseMapping){
										console.log('inside nochange');
										changeBaseMapping = false;
										
									}
									else
									{
										console.log('chnage in mapping');
										changeBaseMapping = true;
										
									}
								}
								if(!changeBaseMapping)
								{
									noChange = true;
							    }
							}
							
							if(action == 'Delete Position'){
								noChange = false;
							}
							
							if(blankValues)
							{
								if(namespace != ''){
									component.set("v.errorMessage",$A.get("$Label.AxtriaSalesIQTM.Required_fields_missing"));
								}else{
									component.set("v.errorMessage",$A.get("$Label.c.Required_Fields_Missing"));
								}
								
								component.set("v.showError",true);
							}
							else if(noOfCols > 0 && (noChange || noChangeInParentPosition))
							{
								console.log('noChange :'+noChange);
								console.log('noChangeInParentPosition :'+noChangeInParentPosition);
								if(namespace != ''){
									component.set("v.errorMessage",$A.get("$Label.AxtriaSalesIQTM.No_Changes_Edit_Position_Popup"));
								}else{
									component.set("v.errorMessage",$A.get("$Label.c.No_Changes_Edit_Position_Popup"));
								}
								
								component.set("v.showError",true);
							}
							else if(mirrorBasePosition){
								component.set("v.errorMessage","For Mirror Position Creation - One base position needs to be selected.");
								component.set("v.showError",true);
							}
							else
							{
								//No client side error. Check for server side validations
								console.log('-- edit emplloyee -- '+component.get("v.action"));
								component.set("v.errorMessage",'');
								var checkValidationAction = component.get("c.validatePosition");
						        checkValidationAction.setParams
						        ({
						        	action : component.get("v.action"),
						            teamInstanceId : component.get("v.selectedTeamInstance"),
						            objectRecordsWrapperString : JSON.stringify(component.get("v.popupWrapper")),
		                            objectBaseWrapperString : JSON.stringify(component.get("v.basePositionWrapperList")),
		                            dateFormat : component.get("v.dateFormat")
						        });
						        checkValidationAction.setCallback(this,function(checkValidationResponse)
						        {
						            if(checkValidationResponse.getState() == 'SUCCESS' && checkValidationResponse.getReturnValue() != null && checkValidationResponse.getReturnValue() != undefined)
						            {
						            	var validationResult = checkValidationResponse.getReturnValue();

						            	console.log('validationResult :'+validationResult);
						            	if(validationResult.indexOf('Success') != -1)
						            	{
						            		if(validationResult.length > 1){
						            			validationResult.pop()
						            			component.set("v.errorList",validationResult);
						            		}
						            		//Server side validation passed. Show confirmation message and enable confirm button
							            	component.set("v.showConfirm",true);
							            	component.set("v.doSubmit",false);
						            		var baseWrapper = component.get("v.basePositionWrapperList");
						            		console.log(baseWrapper);
				
						            		//new code added
						            		var inputCmp = component.find("inputFld");
						            		if(inputCmp != undefined){
					            				if(inputCmp.length != undefined)
					            				{
					            					console.log('input field');
					            					for(var i=0; i < inputCmp.length; i++)
						            				{
					            						inputCmp[i].set("v.disabled",true);
					            					}
					            				}
				            				}
				
						            		var chkBox = component.find("chkId");
						            		for(var i=0; i < baseWrapper.length; i++)
						            		{
						            			if(baseWrapper[i].isDefaultDisabled == false)
						            			{
						            				console.log('defaultdisable istrue');
						            				if(chkBox != undefined)
						            				{
							            				if(chkBox.length != undefined)
							            				{
							            					console.log(chkBox[i]);
							            					chkBox[i].set("v.disabled",true);
							            				}
						            				}
						            			}
						            		}
						            		component.set("v.errorList",[]);
						            	}
						            	else
						            	{
						            		//Server side validation failed
						            		component.set("v.showError",true);
						            		component.set("v.errorList",validationResult);
						            		component.set("v.popOverClass", 'slds-rise-from-ground');
						            	}
						            }
						            else
						            {
						            	helper.logException(component,checkValidationResponse.getError());
						            }
						        });
						        $A.enqueueAction(checkValidationAction);
							}
					    }
		          	}
    			}
		        else if (state === "ERROR" || result == null) 
		        {
		          helper.logException(component, response.getError());
		        }	
			});
        	$A.enqueueAction(checkBusinessRule);
	    }
	    
    },

    confirmRequest: function(component,event,helper)
    {

    	let button = event.getSource();
    	button.set('v.disabled',true);

    	console.log('confirmRequest called : '+component.get("v.doConfirm"));
    	var namespace = component.get("v.namespace");
    	console.log('namespace : '+namespace);
		if(component.get("v.isActive") == true && component.get("v.doConfirm") == true)
		{
			component.set("v.doConfirm",false);

			if(component.get("v.action") != 'Filter Position'){

				var submitAction = component.get("c.confirmSubmitRequest");

		        submitAction.setParams
		        ({
		        	action : component.get("v.action"),
		            positionId : component.get("v.selectedPosition"),
		            teamInstanceId : component.get("v.selectedTeamInstance"),
		            positionDataWrapper : JSON.stringify(component.get("v.popupWrapper")),
		            basePositionsWrapper : JSON.stringify(component.get("v.basePositionWrapperList")),
	                profileMap1 : JSON.stringify(component.get("v.profileMap")),
	                dateFormat : component.get("v.dateFormat")

		        });
	        
		        console.log('action : '+component.get("v.action"));
	            console.log('positionId : '+component.get("v.selectedPosition"));
	            console.log('teamInstanceId : '+component.get("v.selectedTeamInstance"));
	            console.log('wrapper : ',component.get("v.popupWrapper"));
	        
		        submitAction.setCallback(this,function(response)
		        {
		            console.log('state : '+response.getState());
		            console.log('Return : '+response.getReturnValue());
		            var res = response.getReturnValue();
		            if(response.getState() == 'SUCCESS' && res != null && res != undefined)
		            {
	            		var crToast = $A.get("e.force:showToast");

	            		var crSubmitMsg;
	            		if(namespace != ''){
	            			crSubmitMsg = $A.get("$Label.AxtriaSalesIQTM.Change_Request_Submitted");
	            		}
	            		else
	            		{
	            			crSubmitMsg = $A.get("$Label.c.Change_Request_Submitted")
	            		}

	            		if(component.get("v.action") == 'Edit Employee'){
	            			//Change Request {0} has been submitted.
	            			crSubmitMsg = 'Changes on {0} saved successfully.'
	            		}

	            		var retResponse = response.getReturnValue();
					    crToast.setParams
					    ({
					    	mode: 'dismissible',
	        				type: 'Success',
					        message: crSubmitMsg,
					        messageTemplate: crSubmitMsg,
					        messageTemplateData:  [
	                            {
	                            url: retResponse[1],
	                            label: retResponse[0]
	                            }
	                        ]
					    });
					    crToast.fire();
					    
		            	//Close popup and reset the properties
		            	component.set("v.isActive", false);
				    	component.set("v.doRender", true);
				    	component.set("v.doSubmit", true);
				    	component.set("v.doConfirm", true);
				    	component.set("v.showConfirm",false);
				    	component.set("v.showError",false);
				    	component.set("v.errorMessage",'');
				    	component.set("v.errorList",'');

				    	// call refresh event
			    	    var refreshDataTable = component.getEvent("refreshComponentEvent");
						refreshDataTable.fire();
		            }
		            else
		            {
		            	helper.logException(component,response.getError());
			    	    component.set("v.showConfirm",false);
			    	    component.set("v.showError",false);
		            }
		        });
	        
	        	$A.enqueueAction(submitAction);
			}else{
				console.log('--- calling filter submit action ---');
				console.log('teaminstance  - '+component.get("v.selectedTeamInstance"));
				console.log('filterwrapper - ',component.get("v.filterwrapper"));
				console.log('queryString - '+component.get("v.queryString"));

				console.log('parsedquery -'+component.get("v.parsedQuery"));
				console.log('parsedJSONQuery -'+component.get("v.parsedJSONQuery"));

				var refreshDataTable = component.getEvent("refreshComponentEvent");
				refreshDataTable.setParams({
					"filterQueryWhereClause" : component.get("v.parsedJSONQuery"),
					"restrictListView" : true
				});
				
				refreshDataTable.fire();

				component.set("v.isActive", false);

				component.set("v.showConfirm",false);
		                component.set("v.doSubmit",true);
		                component.set("v.doConfirm",true);
		                component.set("v.errorMessage",'');
		    	        component.set("v.errorList",'');
		                component.set("v.doRender",true);

			}
         
            
	    }
    },

    cancelRequest: function(component)
    {
    	//helper.closePopup();
    	console.log('closePopup called');
    	component.set("v.isActive", false);
    	component.set("v.doRender", true);
    	component.set("v.doSubmit", true);
    	component.set("v.doConfirm", true);
    	component.set("v.showConfirm",false);
    	component.set("v.showError",false);
    	component.set("v.errorMessage",'');
    	component.set("v.errorList",'');
    }, 

    hideShowErrorPopup: function(component)
    {
    	var activeClass = component.get("v.popOverClass");
		if(activeClass == 'slds-rise-from-ground')
			component.set("v.popOverClass", 'slds-fall-into-ground');
		else
			component.set("v.popOverClass", 'slds-rise-from-ground');
    },

    closeToast : function(component,event,helper)
	{
	    var toast = component.find("showHideInfoConfirm");    
	    $A.util.addClass(toast,'slds-fall-into-ground');
	    $A.util.removeClass(toast,'slds-rise-from-ground');
	},

    onSelectBasePosition : function(component, event)
    {
    	console.log('onSelectBasePosition called');
    	var wrapper = component.get("v.popupWrapper");
    	var baseWrapper = component.get("v.basePositionWrapperList");
    	var isMirror = wrapper.isMirror;
    	
    	console.log('isMirror : '+isMirror);
    	
    	var chkBox = component.find("chkId");

    	if(chkBox.length>1)
    	{
    		for(var i=0; i < baseWrapper.length ;i++)
    		{
				if(event.getSource() != chkBox[i] && isMirror && chkBox[i].get("v.value") == true)
				{
					console.log('i : '+i);
					chkBox[i].set("v.value",false);
				}
			}
    	}
    },

    /******* Function to call External JavaScript Function which is used outside the managed package for outside package functionalites ******/
    externalJS : function(component, event, helper){
      	//window.computeExternal();
      	var item = event.getParam("selectedItem")
      	window.computeExternal(JSON.parse(JSON.stringify(item)),component,event,helper);

    },

    closeConfirm : function(component, event)
    {
    	console.log('closeConfirm called');
    	component.set("v.showConfirm",false);
    	component.set("v.doSubmit",true);
    	//Disable all input boxes
    	var baseWrapper = component.get("v.basePositionWrapperList");
    	var inputCmp = component.find("inputFld");
    	if(inputCmp != undefined)
		{
			if(inputCmp.length != undefined){
				for(var i=0; i < inputCmp.length; i++){
					inputCmp[i].set("v.disabled",false);
				}
			}
		}

		var chkBox = component.find("chkId");
		for(var i=0; i < baseWrapper.length; i++)
		{
			console.log(baseWrapper[i].isDefaultDisabled);
			if(baseWrapper[i].isDefaultDisabled == false)
			{
				console.log('defaultdisable istrue');
				if(chkBox != undefined)
				{
    				if(chkBox.length != undefined){
    					chkBox[i].set("v.disabled",false);
    				}
				}
			}
		}
    },

    renderBasePosition: function(component, event,helper){
    	var ns = component.get("v.namespace");
    	console.log(' ____selectPicklistValue ');
    	console.log(event);
    	var evntsource = event.getSource();
		var picklistValue = evntsource.get("v.value"); 
		console.log(picklistValue.toLowerCase());
		if(picklistValue.toLowerCase() == 'mirror'){
			console.log('Inside mirror');
			var wrapper = component.get("v.popupWrapper");
			wrapper.isMirror = true;
			component.set("v.popupWrapper", wrapper);
			var getMirroAction = component.get("c.getMirrorPositionData");
        	getMirroAction.setParams
        	({
                positionId : component.get("v.selectedPosition"),
                teamInstanceId : component.get("v.selectedTeamInstance"),
                action : component.get("v.action")
            });
            
            getMirroAction.setCallback(this,function(getBasePositionResponse)
            {
            	if(getBasePositionResponse.getState() == 'SUCCESS' && getBasePositionResponse.getReturnValue() != null)
            	{
            		console.log('CallBack received');
            		console.log(getBasePositionResponse.getReturnValue());
            		component.set("v.basePositionWrapperList", getBasePositionResponse.getReturnValue());
            		component.set("v.showBaseMapping", true);
            	}
            	else
            	{
            		helper.logException(component,getBasePositionResponse.getError());
            	}
            });
            $A.enqueueAction(getMirroAction);
    		var objWrapper = component.get("v.popupWrapper");
    		var posAttList = objWrapper.positionAttributes;
    		for(var i=0; i<posAttList.length; i++){
				console.log(posAttList[i]);
				for(var j=0; j< posAttList[i].rowDataConfig.length ;j++){
					if(posAttList[i].rowDataConfig[j].fieldApiName == ns + 'Position_Category__c' ){
						posAttList[i].rowDataConfig[j].isEditable = false;
						posAttList[i].rowDataConfig[j].fieldValue = 'Field Position';
					}
				}
			}
			objWrapper.positionAttributes = posAttList;
			component.set("v.popupWrapper",objWrapper );
		}
		else if(picklistValue.toLowerCase() == 'base'){
    		var objWrapper = component.get("v.popupWrapper");
    		var wrapper = component.get("v.popupWrapper");
			wrapper.isMirror = true;
			component.set("v.popupWrapper", wrapper);
    		var posAttList = objWrapper.positionAttributes;
    		for(var i=0; i<posAttList.length; i++){
				console.log(posAttList[i]);
				for(var j=0; j< posAttList[i].rowDataConfig.length ;j++){
					if(posAttList[i].rowDataConfig[j].fieldApiName == ns + 'Position_Category__c' ){
						posAttList[i].rowDataConfig[j].isEditable = true;
						posAttList[i].rowDataConfig[j].fieldValue = 'Field Position';
					}
				}
			}
			component.set("v.basePositionWrapperList", null);
			component.set("v.showBaseMapping", false);
			objWrapper.positionAttributes = posAttList;
			component.set("v.popupWrapper",objWrapper );
		}
    },

    selectPicklistValue : function(component,event){
    	console.log(' ____selectPicklistValue ');
    	console.log(event);
    	var evntsource = event.getSource();
		var picklistValue = evntsource.get("v.value"); 
		console.log(picklistValue);

		var objWrapper = component.get("v.popupWrapper");

		var posAttList = objWrapper.positionAttributes;
		for(var i=0; i<posAttList.length; i++){
			console.log(posAttList[i]);
			for(var j=0; j< posAttList[i].rowDataConfig.length ;j++){
				if(posAttList[i].rowDataConfig[j].fieldApiName == 'Position_Type__c'){
					console.log(objWrapper.nonfieldPositionType);
					if(picklistValue == 'Non-Field Position'){
						posAttList[i].rowDataConfig[j].isEditable = true;
						posAttList[i].rowDataConfig[j].fieldDataType = 'Picklist';
						posAttList[i].rowDataConfig[j].fieldValue = '';
						posAttList[i].rowDataConfig[j].oldFieldValue = '';
						posAttList[i].rowDataConfig[j].picklistValues = objWrapper.nonfieldPositionType;
					}
					else if(picklistValue == 'Field Position'){
						var type = [];
						type.push(objWrapper.fieldPositionType);
						posAttList[i].rowDataConfig[j].isEditable = false;
						posAttList[i].rowDataConfig[j].fieldDataType = 'text';
						posAttList[i].rowDataConfig[j].picklistValues = type;
					}
				}

			}
			
		}
		objWrapper.positionAttributes = posAttList;
		component.set("v.popupWrapper",objWrapper );

    },

    updateFilterWrapper : function(component,event,helper){
    	console.log('--updateFilterWrapper ');
    	var isValid = event.getParam("isValid");
      	var errMsg = event.getParam("errorMsg");
      	var stagingWhereClause = event.getParam("stagingWhereClause");
      	var whereClause = event.getParam("whereClause");
      	var sourceWhereClause = event.getParam("sourceWhereClause");
      	var parsedJson = event.getParam("parsedJson");
        console.log('stagingWhereClause - ',stagingWhereClause); 
        console.log('whereClause - ',whereClause);
        console.log('sourceWhereClause - ',sourceWhereClause);
        console.log('parsedJson - ',parsedJson);

        if(errMsg != undefined && errMsg != '' && errMsg != null){
        	component.set("v.errorList",errMsg);
	    	component.set("v.showError",true);
	    	component.set("v.popOverClass", 'slds-rise-from-ground');
	    	component.set("v.showConfirm",false);
	    	component.set("v.doSubmit",true);
        }else{
        	component.set("v.errorList",[]);
	    	component.set("v.showError",false);
	    	component.set("v.popOverClass", 'slds-rise-from-ground');
	    	component.set("v.showConfirm",true);
	    	component.set("v.doSubmit",false);
        }
     
    },


    /*resetFilterCondition : function(component,event,helper){
    	component.set("v.filterQueryWhereClause", '');

    	var refreshDataTable = component.getEvent("refreshComponentEvent");
		refreshDataTable.setParams({
			"filterQueryWhereClause" : component.get("v.parsedJSONQuery")
		});
		
		refreshDataTable.fire();

		component.set("v.isActive", false);

    }
*/

})
