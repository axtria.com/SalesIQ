({
    
    populateChart : function(component, event, helper) {
        try{
            var $j = jQuery.noConflict();
            component.set("v.Spinner", true);
             var params = event.getParam('arguments');
            if (params) {
            var currentScenarioId =params.currentScenarioId ;
             var previousScenarioId = params.previousScenarioId;
             var currentinstance =  params.instanceNameValue;
             var teamName=params.teamName;
             var url= params.url;
             var instName=params.instName;
             component.set("v.prevScenName", params.prevScenarioName);
             component.set("v.currScenName", params.currScenarioName);   
            
        
        
        
        if(instName){
            instName = instName.replace('/','');
        }       
        console.log('currentScenarioId inside populate charts ::: '+currentScenarioId);
        console.log('previousScenarioId inside populate charts ::: '+previousScenarioId);        
        
        var action = component.get("c.generateVisualizationReport");
        var res = url+'?scenario_1='+currentScenarioId+'&scenario_2='+previousScenarioId+'&tab_id=overlap_sum&team='+teamName+'&context='+instName
        console.log('strUrl->'+res);
        
        action.setParams({
            "url": res
        });
        
        action.setCallback(this,function(response)
                           {
                               var state = response.getState();
                               if(state === 'SUCCESS' && response.getReturnValue != null)
                               {
                                   var resultsMap = response.getReturnValue();
                                   console.log("webservice response");
                                   console.log(resultsMap);
                                   var noData = component.find("noData");
                                   $A.util.addClass(noData,'slds-hide');
                                   var div1 = component.find("div1");
                                   $A.util.removeClass(div1,'slds-hide');
                                   
                                   try{
                                       console.log('inside first bar chart');
                                       var labels = [];
                                       var data = [];
                                       
                                       console.log('resultsMap');
                                       console.log(resultsMap);
                                       
                                       console.log("get results map value");
                                       component.set("v.resultsMap",resultsMap);
                                       console.log('--result');
                                       console.log(component.get("v.resultsMap"));
                                       
                                       component.afterFirstChartRendering();
                                       
                                       for(var i=0;i<resultsMap["CPG_Report_%target_overlap"].length;i++){
                                           labels.push(resultsMap["CPG_Report_%target_overlap"][i].range_txt.toLowerCase());
                                           data.push(resultsMap["CPG_Report_%target_overlap"][i].count);
                                       }
                                       
                                       console.log('labels:::::: '+labels);
                                       console.log('data:::::: '+data);
                                       Highcharts.chart('containerBarChart1', {
                                           chart: {
                                               type: 'column',
                                               ignoreHiddenSeries: false
                                           },
                                           title: {
                                               text: '% Target Overlap by Territories'
                                           },
                                           subtitle: {
                                               text: ''
                                           },
                                           xAxis: {
                                               categories: 
                                               labels
                                               ,
                                               crosshair: true,
                                               lineColor: '#000',
                                               //gridLineColor: '#CCC',
                                               //gridLineWidth: 1,
                                               lineWidth: 1,
                                           },
                                           yAxis: {
                                               min: 0,
                                               lineColor: '#000',
                                               lineWidth: 1,
                                               title: {
                                                   text: '# of Territories', 
                                                   style: {                    
                                                       fontWeight:'bold',                    
                                                   }
                                               },
                                               labels: {
                                                   style: {                    
                                                       fontSize:'12px',                    
                                                   }
                                               }
                                           },
                                           tooltip: {
                                               headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                               // pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                               pointFormat: '<tr>' +
                                               '<td style="padding:0"><b>{point.y}</b></td></tr>',
                                               footerFormat: '</table>',
                                               shared: true,
                                               useHTML: true
                                           },
                                           plotOptions: {
                                               column: {
                                                   pointPadding: 0.2,
                                                   borderWidth: 0
                                               }
                                           },
                                           credits:{
                                               enabled:false
                                           } ,
                                           series: [{
                                               name: '% Overlap ',
                                               data: data
                                               
                                           },]
                                                    });
                                                    }
                                                    catch(err){
                                                    component.set("v.Spinner", false);
                                                    var noData = component.find("noData");
                                                    $A.util.removeClass(noData,'slds-hide');
                                                    var div1 = component.find("div1");
                                                    $A.util.addClass(div1,'slds-hide');
                                                    console.log('--err ' + err);
                                                    }
                                                    
                                                    }else{
                                                    component.set("v.Spinner", false);
                                                    var noData = component.find("noData");
                                                    $A.util.removeClass(noData,'slds-hide');
                                                    var div1 = component.find("div1");
                                                    $A.util.addClass(div1,'slds-hide');
                                                    }
                                                    
                                                    });
                                                    
        $A.enqueueAction(action);
                                                    }     
         }
         catch(ex) {console.log('error venn-->'+ex) }                                                    
                                                    
                                                    
                                                    },
                                                    
                                                    
                                                    
                                                    afterFirstChartRendering : function(component,event,helper){
                                                    
                                                    try{
                                                    
                                                    // var canvas = component.find("BarChartID").getElement();
                                                    // canvas.height = 400;
                                                    console.log('inside bar chart');
                                                    
                                                    var labels = [];
                                           var data = [];
                                           
                                           var resultsMap = component.get("v.resultsMap");
                                           if(!resultsMap){
                                           component.set("v.Spinner", false);
                                       }
                                                        console.log('resultsMap');
                                       console.log(resultsMap);
                                       
                                       for(var i=0;i<resultsMap.CPG_Report_target_count_difference.length;i++){
                                           labels.push(resultsMap.CPG_Report_target_count_difference[i].range_txt.toLowerCase());
                                           data.push(resultsMap.CPG_Report_target_count_difference[i].count);
                                       }
                                       
                                       console.log('labels for second chart:::::: '+labels);
                                       console.log('data for second chart:::::: '+data);
                                       
                                       component.set("v.Spinner", false);
                                       
                                       var resultsMap = component.get("v.resultsMap");
                                       component.set("v.resultsMap",resultsMap);
                                       component.loadVennChart();
                                       Highcharts.chart('containerBarChart2', {
                                           chart: {
                                               type: 'column',
                                               ignoreHiddenSeries: false
                                           },
                                           title: {
                                               text: 'Target Count Differences'
                                           },
                                           subtitle: {
                                               text: ''
                                           },
                                           xAxis: {
                                               categories: 
                                               labels
                                               ,
                                               crosshair: true,
                                               lineColor: '#000',
                                               lineWidth: 1,
                                               
                                               //gridLineColor: '#CCC',
                                               //gridLineWidth: 1,
                                           },
                                           yAxis: {
                                               min: 0,
                                               lineColor: '#000',
                                               lineWidth: 1,
                                               title: {
                                                   text: '# of Territories',  
                                                   style: {                    
                                                       fontWeight:'bold',                    
                                                   }
                                               },
                                               labels: {
                                                   style: {                    
                                                       fontSize:'12px',                    
                                                   }
                                               }
                                           },
                                           tooltip: {
                                               headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                                               //pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                               pointFormat: '<tr>' +  
                                               '<td style="padding:0"><b>{point.y}</b></td></tr>',
                                               footerFormat: '</table>',
                                               shared: true,
                                               useHTML: true
                                           },
                                           plotOptions: {
                                               column: {
                                                   pointPadding: 0.2,
                                                   borderWidth: 0
                                               }
                                           },
                                           credits:{
                                               enabled:false
                                           } ,
                                           series: [{
                                               name: 'Difference in Count of Targets',
                                               data: data
                                               
                                           },]
                                                    });
                                                    }
                                                    catch(err){
                                                    console.log('--err ' + err);
                                                    }
                                                    
                                                    },
                                                    
                                                    
                                                    loadVennChart : function(component, event, helper)
                                                    {
                                                              
                                                    console.log('inside load Venn Chart method');
                                                    var previousScenarioId = event.getParam("previousScenarioId");
                                                    
                                                    try{
                                                    var x1_axis ;
                                                    var r1 ;
                                                    var x2_axis ;
                                                    var r2;
                                                    var resultsMap = component.get("v.resultsMap");
                                                    if(resultsMap.CPG_Report_common_targets[0]){ 
                                           console.log("commontargets",+resultsMap.CPG_Report_common_targets[0].common_targets);
                                           console.log("targets added",+resultsMap.CPG_Report_common_targets[0].targets_added);
                                           console.log("targets dropped",+resultsMap.CPG_Report_common_targets[0].targets_dropped);
                                           component.set("v.commontargets",resultsMap.CPG_Report_common_targets[0].common_targets);
                                           component.set("v.targetsadded",resultsMap.CPG_Report_common_targets[0].targets_added);
                                           component.set("v.targetsdropped",resultsMap.CPG_Report_common_targets[0].targets_dropped);
                                           
                                           var varTable = component.find("vennTable");
                                           $A.util.removeClass(varTable,'slds-hide');
                                       }
                                                        
                                                        var added_targets = component.get("v.commontargets") + component.get("v.targetsadded");
                                       var dropped_targets = component.get("v.commontargets") + component.get("v.targetsdropped");
                                       var total_targets = added_targets + dropped_targets - component.get("v.commontargets");
                                       var commonTargetsPercentage=(component.get("v.commontargets")/(component.get("v.commontargets") + component.get("v.targetsdropped")))*100;
                                       var targetsAddedPercentage=(component.get("v.targetsadded")/(component.get("v.commontargets") + component.get("v.targetsadded")))*100;
                                       var targetsDroppedPercentage=(component.get("v.targetsdropped")/(component.get("v.commontargets") + component.get("v.targetsdropped")))*100;
                                       
                                       
                                       jQuery(".Compare_scenarios").text('Targets Dropped');
                                       jQuery(".base_scenario").text('Targets Added');
                                       jQuery(".common_targets").text("Common Targets");
                                       jQuery("#spnTotalBaseSceTargets").text(common.myToLocaleInteger(added_targets));
                                       jQuery("#spnTotalCompareSceTargets").text(common.myToLocaleInteger(dropped_targets));
                                       jQuery("#spnCompareSce1").html(component.get("v.prevScenName"));
                                       jQuery("#spnCompareSce2").html(component.get("v.prevScenName"));    
                                       jQuery("#spnBaseSce").html(component.get("v.currScenName"));
                                       var $j = jQuery.noConflict();
                                       
                                       var targetsAddedlabel=component.get("v.targetsadded")==0?' ':common.myToLocaleInteger(component.get("v.targetsadded"));
                                       var targetsDroppedlabel=component.get("v.targetsdropped")==0?' ':common.myToLocaleInteger(component.get("v.targetsdropped"));
                                       var commonTargetslabel=component.get("v.commontargets")==0?' ':common.myToLocaleInteger(component.get("v.commontargets"));
                                       var sets=[
                                           {sets:["Base"], figure: added_targets, label: targetsAddedlabel, size: added_targets},
                                           {sets:["Compare"], figure:dropped_targets, label: targetsDroppedlabel, size: dropped_targets},
                                           {sets:["Base","Compare"], figure: component.get("v.commontargets"), label: commonTargetslabel, size: component.get("v.commontargets")}
                                       ];
                                       
                                       var chart = venn.VennDiagram()
                                       .width(350)
                                       .height(350);
                                       
                                       $j("#tdCommonTargetsInput").text(common.myToLocaleInteger(component.get("v.commontargets")));
                                       $j("#tdTargetsAddedInput").text(common.myToLocaleInteger(component.get("v.targetsadded")));
                                       $j("#tdTargetsDroppedInput").text(common.myToLocaleInteger(component.get("v.targetsdropped")));
                                       
                                        $j("#spnCommonTargets").text(common.round(commonTargetsPercentage));
          $j("#spnTargetsAdded").text(common.round(targetsAddedPercentage));
          $j("#spnTargetsDropped").text(common.round(targetsDroppedPercentage));
         
                             
                                       var div = d3.select("#vennChart").datum(sets).call(chart);         
                                   }
                                   catch(err)
                                   {
                                       //alert(err);
                                       console.log('--err ' + err);
                                   }
                                   
                                   
                               },             
                                   showSpinner: function(component, event, helper) {
                                       // make Spinner attribute true for display loading spinner 
                                       component.set("v.Spinner", true); 
                                   },
                                       
                                       // this function automatic call by aura:doneWaiting event 
                                       hideSpinner : function(component,event,helper){
                                           // make Spinner attribute to false for hide loading spinner    
                                           component.set("v.Spinner", false);
                                       },                      
                                           
                           })