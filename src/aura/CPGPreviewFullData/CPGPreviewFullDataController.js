({
    doinit : function(component, event, helper) {
        component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0 !important;}   .forceStyle .viewport.oneHeader.desktop {z-index:0 !important;} .forceStyle.desktop .viewport{overflow:hidden}");
        component.set("v.isDataAvailable",false);
        component.showSpinner();
        component.aggFunctionValues();
        console.log('Inside Init');
        component.set("v.parsedQuery",'');
        component.set("v.parsedJSONQuery",'');
        var btnIcon1 = component.find("btnIcon1");
        $A.util.addClass(btnIcon1,'slds-is-selected');
        //var $j = jQuery.noConflict();
        //$j("#table-1").DataTable().destroy();
        //  console.log("inside table destroy");
        // $j("#table-1").remove();
        // $j('#tableContainer').append("<table id='table-1'> </table>");   
        //} 
        /*Reports code
         * Aakash
         */
        var division = component.find("myDivDiv");
        $A.util.addClass(division,'slds-hide');
        var divisionPie = component.find("myDivPie");
        $A.util.addClass(divisionPie,'slds-hide'); 
        
        //var tab1 = component.find('ReportTabId');
        var TabOnedata = component.find('reportdata');
		// $A.util.addClass(tab1, 'slds-is-active');
        $A.util.addClass(TabOnedata, 'slds-hide');
        /* Start For Model height*/
        /*var $j = jQuery.noConflict();
        var topPadding = $j('.slds-modal__container').css('padding-top').replace("px", "");
        var headerHeight = $j('.slds-modal__header').height();
        var modelHeight = $j(document).height()-((topPadding*2)+headerHeight);
        //console.log('topPadding'+topPadding1);
        $j('.slds-model-body').height(modelHeight);*/
        /* End For Model height*/
        
        
        
        var metaDataAction = component.get("c.getDataForUnderLyingData");
        console.log(component.get("v.scenarioRuleInstanceId"));
        metaDataAction.setParams({ "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
        metaDataAction.setCallback(this, function(response) {
            
            var state = response.getState();
            if(state == "SUCCESS"){
                component.componentTypeName();
                var metaDataActionResult = response.getReturnValue();
                if(!$A.util.isEmpty(metaDataActionResult) && !$A.util.isUndefined(metaDataActionResult)){
                    component.set("v.dataSourceDetails",response.getReturnValue());
                    var colaction = component.get("c.GetSourceAndDerivedFields");
                    colaction.setParams({ "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
                    colaction.setCallback(this, function(response) {
                        console.log(component.get("v.scenarioRuleInstanceId"));
                        var state = response.getState();
                        if(state == "SUCCESS"){
                            var result = response.getReturnValue();
                            if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                                component.set("v.columnDetail",response.getReturnValue());
                                console.log('columnDetail');
                                console.log(component.get("v.columnDetail"));
                                
                                    var columnNames = [];
                                    var numericColumnNames =[];
                                    var discreteColumnNames = [];
                                    console.log(component.get("v.columnDetail")); 
                                    var colDetail = component.get("v.columnDetail");

                                    var SelField;
                                    var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
                                    console.log('namespace val' +namespace);
                                    if(namespace != ''){
                                        
                                        SelField = $A.get("$Label.AxtriaSalesIQTM.Select_Field");

                                     }else{
                                        SelField = $A.get("$Label.c.Select_Field");
                                     }

                                    columnNames.push({label: SelField});  
                                    numericColumnNames.push({label:SelField});
                                    discreteColumnNames.push({label:SelField});
                                    
                                    var columnNameSet = new Set();
                                    var numericNameSet = new Set();
                                    var discreteNameSet = new Set();
                                    for(var ctr=0; ctr<colDetail.length; ctr++){  
                                        
                                        if(!columnNameSet.has(colDetail[ctr].columnName)){
                                            columnNameSet.add(colDetail[ctr].columnName);
                                            columnNames.push({label:colDetail[ctr].columnName,datatype:colDetail[ctr].dataType});
                                        }
                                        
                                        if(colDetail[ctr].dataType.toLowerCase()=="numeric"){
                                            if(!numericNameSet.has(colDetail[ctr].columnName)){
                                                numericColumnNames.push({label:colDetail[ctr].columnName,datatype:colDetail[ctr].dataType}); 
                                                numericNameSet.add(colDetail[ctr].columnName);
                                            }    
                                        }
                                        
                                        if(colDetail[ctr].valueType=="Discrete"){
                                            if(!discreteNameSet.has(colDetail[ctr].columnName)){
                                                discreteColumnNames.push({label:colDetail[ctr].columnName,datatype:colDetail[ctr].dataType});  
                                                discreteNameSet.add(colDetail[ctr].columnName);
                                            }
                                        }    
                                    }
                                    console.log('--columnNameSet');
                                    console.log(columnNameSet);
                            
                                    console.log("Values are:");
                                    console.log(columnNames);
                                    
                                    component.set("v.custIDOptions",columnNames);
                                    component.set("v.custIDOptions2",columnNames);
                                    component.set("v.custIDOptions3",columnNames);
                                    component.set("v.custIDOptions4",columnNames);
                                    component.set("v.custIDOptions5",columnNames);
                                    component.set("v.fieldNameBarOptions",discreteColumnNames);
                                    component.set("v.fieldNameSumBarOptions",numericColumnNames);
                                    component.set("v.barGroupByOptions",discreteColumnNames); 
                                    component.set("v.fieldNamePieOptions",discreteColumnNames); 
                                    component.set("v.fieldNameSumPieOptions",discreteColumnNames); 
                                    component.set("v.pieValueOptions",numericColumnNames); 
                                    
                                    
                                
                            }
                            var actionTabName = component.get("c.getTableName");
                            actionTabName.setParams({ "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
                            actionTabName.setCallback(this, function(response){ 
                                var stateTabName = response.getState();
                                if(stateTabName==="SUCCESS")
                                { 
                                    var result = response.getReturnValue();
                                    var tableAttr = result.split(',');
                                    var tableName = tableAttr[0];
                                    component.set("v.tableName",tableName);
                                    component.set("v.tableDisplayName",tableAttr[1]);
                                    
                                    var actionInstanceName = component.get("c.getInstanceNameAndURL");
                                    actionInstanceName.setCallback(this, function(response){ 
                                        var instanceName = response.getState();
                                        if(instanceName==="SUCCESS")
                                        {
                                            //var tableName = response.getReturnValue();
                                            console.log('Inside CPGPreviewNamme');
                                            console.log(response.getReturnValue());
                                            var instanceNameANdURL = response.getReturnValue();
                                            component.set("v.instanceName",instanceNameANdURL[0]);
                                            component.set("v.instanceURL",instanceNameANdURL[1]);
                                             component.set("v.reportsUrl",instanceNameANdURL[2]);
                                            component.set("v.contextName",instanceNameANdURL[3]);
                                            component.set("v.exporlAllUrl",instanceNameANdURL[4]);
                                            console.log('before calling componentTypeName method'); 
                                            console.log(component.get("v.reportsUrl"));
                                            console.log(component.get("v.contextName"));
                                            //component.hideSpinner();
                                            component.createDataTable();
                                            component.createMetaDataTable();
                                            component.metaDataInformation();
                                            
                                        }
                                        else{
                                            console.log("Failed with state:  "+ state);
                                        }
                                    });
                                    $A.enqueueAction(actionInstanceName);
                                }
                                else{
                                    console.log("Failed with state:  "+ state);
                                }
                            });
                            $A.enqueueAction(actionTabName);
                            
                             var actionFileType = component.get("c.getFileType");
                            actionFileType.setParams({ "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
                            actionFileType.setCallback(this, function(response){ 
                                var stateVal = response.getState();

                                if(stateVal==="SUCCESS")
                                {
                                    var resultFile = response.getReturnValue();
                                    console.log("FileType result", resultFile);
                                    //var tableAttr = result.split(',');
                                    //var tableName = tableAttr[0];
                                    component.set("v.fileTypeVal",resultFile);
                                    //component.set("v.tableDisplayName",tableAttr[1]);
                                }
                                else{
                                    console.log("Failed with state:  "+ state);
                                }
                            });
                            
                            $A.enqueueAction(actionFileType);
                            
                        }else if(state == "ERROR"){
                            console.log('Error in calling server side action');
                        }
                    });
                    $A.enqueueAction(colaction);
                }
            }
            else if(state == "ERROR"){
                console.log('Error in calling server side action');
            }
        });
        $A.enqueueAction(metaDataAction);
    },
    
    componentTypeName : function(component,event,helper){
        
        var action = component.get("c.getComponentTypeMasterName");
        
        action.setParams
        ({
            "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")
        });
        action.setCallback(this, function(response)
        { 
            var state = response.getState();
            if(state==="SUCCESS")
            {
                console.log('inside componentTypeName success');
                var componentLabel = response.getReturnValue();
                component.set("v.componentLabel",componentLabel);
               
                var action = component.get("c.getScenarioID");
                action.setParams({ 
                    "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")
                });
                
                action.setCallback(this, function(response){ 
                    var state = response.getState();
                    if(component.isValid() && state==="SUCCESS"){
                        
                        var sceID = response.getReturnValue();
                        console.log('sceID');
                        console.log(sceID);
                        
                        var scenarioName = sceID[0][common.getKey(component,"Scenario_Id__r")]["Name"];
                        console.log('scenarioName ::: '+scenarioName);
                        component.set('v.scenarioName',scenarioName);
                    }
                    else{
                        console.log("Failed with state:  "+ state);
                    }
                })
                $A.enqueueAction(action);
            }
        });
        $A.enqueueAction(action);
        
         
        var action = component.get("c.getScenarioStatus");
        action.setParams({ "scenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log("ScenarioRuleInstanceID",component.get("v.scenarioRuleInstanceId"));
            console.log(response.getReturnValue().toUpperCase());
            console.log("SCENARIO STATUS");
            console.log(response.getState());
            component.set('v.scenarioStatus',response.getReturnValue().toUpperCase());
            if(state === 'SUCCESS' && response.getReturnValue() != null){
                if(response.getReturnValue().toUpperCase()!="SUCCESS") {
                    helper.hideExportAllButton(component,event,helper);
                } 
            }
        });
        
        $A.enqueueAction(action);
        
    },
    
    carouselFirstTab:function(component, event, helper){
        var carouselFirstTab = component.find('carouselFirstTab');
        /*$A.util.toggleClass(carouselFirstTab, 'slds-is-open');*/
    },

    carouselSecondTab:function(component, event, helper){
        
        var carouselSecondTab = component.find('carouselSecondTab');
        /*$A.util.toggleClass(carouselSecondTab, 'slds-is-open');*/
    },

    createDataTable : function(component, event, helper)
    {
        //component.showSpinner();
         
        var sessionTable;
        var tableName = component.get("v.tableName");
        var instanceURL = component.get("v.instanceURL");
        var instanceName = component.get("v.instanceName");
        var $j = jQuery.noConflict();
        console.log("tableName"+tableName);
        console.log("instanceURL"+instanceURL);
        console.log("instanceName"+instanceName);
        var searchVal;
        var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
        console.log('namespace val' +namespace);
        if(namespace != ''){
            
            searchVal = $A.get("$Label.AxtriaSalesIQTM.Search_Label");

         }else{
            searchVal = $A.get("$Label.c.Search_Label");
         }

        
        var action = component.get("c.getCalloutResponse");
        console.log(component.get('v.instanceName'));
        console.log(component.get('v.instanceURL')); 
        var res = instanceURL+tableName+instanceName; 
        console.log('strUrl->'+res);
        
        action.setParams({
            "url": instanceURL,
            "tableName": tableName,
            "scenarioRuleInstanceId":component.get("v.scenarioRuleInstanceId")
        });
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                component.hideSpinner();
                var arrHtml=[];
                var state = response.getState();
                var rowCount; 
                var result = response.getReturnValue().data;
                
                var timestamp_columns_vals = response.getReturnValue().timestamp_columns; 
                console.log(timestamp_columns_vals);
                var numeric_columns_vals = response.getReturnValue().numeric_columns; 
                console.log(numeric_columns_vals);

                console.log('Outside column json');
                var coulmnJsonResult = [];

                var columnHeaders = response.getReturnValue().columns;
                console.log('columnHeaders+++', columnHeaders);
                if(columnHeaders != undefined) {
                    for(var key in columnHeaders){
                        coulmnJsonResult.push({"data":columnHeaders[key],"title":columnHeaders[key],"defaultcontent":" "});
                    }
                    console.log('coulmnJsonResult+++', coulmnJsonResult);
                }
                if(response.getReturnValue().data != null)
                {
                    var totalCount = response.getReturnValue().totalRows;

                    component.callNumerFormatter(response.getReturnValue().totalRows, 'totalRowsCount');
                    //component.set("v.totalNoOfRows",totalCount);
                }
                /*else
                {
                   coulmnJsonResult.push({"data":'ID',"title":'ID'});
                }*/

                console.log('+++++++++++++ Columns are');
                console.log(coulmnJsonResult);
                console.log('+++++++++++++ Data is');
                console.log(result);



                console.log('DateTimeFormat ', component.get("v.dateFormat"));
               /* var dateFormat = component.get("v.dateFormat");
                var yearIndex;
                var monthIndex;
                var dayIndex;
                var dateList = [];
                if(dateFormat.indexOf('.') != -1){
                    var splitedString = dateFormat.split('.');
                    for(var inner = 0; inner < splitedString.length; inner++){
                        dateList.push(splitedString[inner]);
                    }
                }else if(dateFormat.indexOf('/') != -1){
                    var splitedString = dateFormat.split('/');
                    for(var inner = 0; inner < splitedString.length; inner++){
                        dateList.push(splitedString[inner]);
                    }
                }else if(dateFormat.indexOf('-') != -1){
                    var splitedString = dateFormat.split('-');
                    for(var inner = 0; inner < splitedString.length; inner++){
                        dateList.push(splitedString[inner]);
                    }
                }

                for(var i=0; i<dateList.length; i++){
                    var index = dateList[i];
                    if(index.indexOf('yy') != -1 || index.indexOf('YY') != -1){
                        yearIndex = i;
                    }
                    if(index.indexOf('m') != -1 || index.indexOf('M') != -1){
                        monthIndex = i;
                    }
                    if(index.indexOf('d') != -1 || index.indexOf('D') != -1){
                        dayIndex = i;
                    }
                }*/
               // var numCol = [];
                //var datescol = [];
                var numObject = {};
                var dateObject = {};
                console.log('timestamp_columns_vals ', timestamp_columns_vals);
                console.log('numeric_columns_vals ', numeric_columns_vals);
                for(var outer = 0; outer < result.length; outer++){

                    if(timestamp_columns_vals.length > 0){
                        for(var inner = 0; inner < timestamp_columns_vals.length; inner++){
                            var value = result[outer][timestamp_columns_vals[inner]];
                            if( value == null || value == 'NaT' || value == 'NaN' || value == undefined )
                                value = '';
                            
                            if(dateObject[timestamp_columns_vals[inner]] != undefined){
                                dateObject[timestamp_columns_vals[inner]].push(value);
                            }
                            else{
                                dateObject[timestamp_columns_vals[inner]] = [];
                                dateObject[timestamp_columns_vals[inner]].push(value);
                            }
                        }
                    }
                    else
                        dateObject = null;
                    
                    if(numeric_columns_vals.length > 0){
                        for(var loop = 0; loop < numeric_columns_vals.length; loop++){
                            var value = result[outer][numeric_columns_vals[loop]];
                            if(value == null)
                                value = '';

                            if(numObject[numeric_columns_vals[loop]] != undefined){
                                numObject[numeric_columns_vals[loop]].push(value);
                            }  
                            else{
                                numObject[numeric_columns_vals[loop]] = [];
                                numObject[numeric_columns_vals[loop]].push(value);
                            }
                        }
                    }
                    else
                        numObject = null;
                }
                    
                console.log('numObject', numObject);
                console.log('dateObject',dateObject);

                /*var actionNumber = component.get("c.getFormatedNumbersMap");
                actionNumber.setParams({
                "numberString": JSON.stringify(numObject),
                });
                actionNumber.setCallback(this,function(response)
                {
                    var returnVar = response.getReturnValue();
                    console.log('returnVar', returnVar);
                });
                $A.enqueueAction(actionNumber);*/

              /*  var action = component.get("c.getformatedDates");
                action.setParams({
                    "datesVal": datescol,
                });
                action.setCallback(this,function(response)
                {
                    var state = response.getState();
                    if(state === 'SUCCESS' && response.getReturnValue != null)
                    {
                         
                        var arrHtml=[];
                        var state = response.getState();
                        var rowCount; 
                        var result1 = response.getReturnValue();
                        console.log(result1);
                        for(var outer = 0; outer < result.length; outer++){
                           for(var inner = 0; inner < timestamp_columns_vals.length; inner++){
                                for(var counter= 0 ; counter<result1.length ; counter++ ){
                                   result[outer][timestamp_columns_vals[inner]] = result1[counter];
                                }
                            }
                        }*/

                        var action = component.get("c.convertedData");
                        action.setParams({
                            "dateData": dateObject == null ? '' : JSON.stringify(dateObject),
                            "numberData" : numObject == null ? '' : JSON.stringify(numObject)
                        });

                        action.setCallback(this,function(response)
                        {
                            var state = response.getState();
                            if(state === 'SUCCESS' && response.getReturnValue != null)
                            {
                             
                                var arrHtml=[];
                                var state = response.getState();
                                var rowCount; 
                                var result1 = response.getReturnValue();
                                console.log('result1');
                                console.log(result1);
                                for(var outer = 0; outer < result.length; outer++){
                                    if(timestamp_columns_vals.length > 0){
                                        for(var inner = 0; inner < timestamp_columns_vals.length; inner++){
                                            result[outer][timestamp_columns_vals[inner]] = result1[0][timestamp_columns_vals[inner]][outer];
                                        }
                                    }

                                    if(numeric_columns_vals.length > 0){
                                        for(var inner = 0; inner < numeric_columns_vals.length; inner++){
                                            result[outer][numeric_columns_vals[inner]] = result1[1][numeric_columns_vals[inner]][outer];
                                        }
                                    }
                                }
                                
                        

                        var tablename = component.get("v.tableName");
                        var flName = component.get("v.componentLabel");
                        
                        if(component.get("v.tabNameisUnderlyingTab"))
                        {

                        if(result && result.length>0){
                           component.set( "v.underlyingDataTable1",result);
                           console.log('Intializing Datatable');
                           sessionTable = $j('.UnderlyingTable').DataTable(
                           {
                               "drawCallback": function( settings ) {
                                   var showButtons = component.find("exportButtonsIds");
                                   console.log(showButtons);
                                   $A.util.removeClass(showButtons, "slds-hide");
                               },
                               
                               "info": false,
                               "search": {
                                   "regex": true,
                                   "smart": false,
                               },
                               "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, 200]],
                               destroy : true,
                               "processing": false,
                               "dom": '<"div-pg"><"div-search"<"slds-form-element__control slds-float_right"<"slds-select_container"l">">f><"div-tbl">t<"bottom-info"i><"clear">', // f search, p :- pagination , l:- page length 
                               "data": result,
                               //"order" : [[2,"asc"]],
                               scrollX : true,
                               scrollY:     "310px",
                               
                               scrollCollapse: true,
                               //stateSave: true,
                               
                               "language":
                               {
                                   "emptyTable": "Loading Data",
                                   "sLengthMenu": "_MENU_",
                                   "search": "",
                                   "searchPlaceholder": searchVal + '...',
                               },
                               
                                buttons: [{
                                   //extend: isIE ? 'csvHtml5' : 'excelHtml5',
                                   extend : 'csv',
                                   text : '',
                                   title : flName, 
                                    
                                }],
                                columns:coulmnJsonResult,
                                columnDefs :
                                [
                                    {
                                       targets:  '_all',
                                       defaultContent : '',
                                       "createdCell": function (td, cellData, rowData, row, col)
                                       {   
                                           $j(td).html(cellData);
                                       } 
                                    }
                                ],
                               
                                "createdRow": function ( row, data, index ) 
                                {
                                   $j(row).addClass('slds-hint-parent');
                                   $j('td', row).addClass('slds-text-align_left slds-truncate');
                                   $j('thead > tr> th').addClass('slds-is-sortable slds-is-resizable slds-text-title_caps');
                                   $j('thead > tr').addClass('slds-text-title_caps ');
                                   
                                },

                               "initComplete": function( settings, json ) 
                               {
                                console.log('calling initComplete--------------');
                                var thElements = document.getElementsByTagName("td");
                                for(var i = 0; i < thElements.length ; i++){
                                    thElements[i].title = thElements[i].innerHTML;
                                }
                               } 
                           }); 
                           component.set("v.isDataAvailable",false);
                           component.set("v.sessionTable",sessionTable);
                        }
                        else{
                            component.set("v.isDataAvailable",true);
                            console.log(component.get("v.isDataAvailable"));
                            var tableVisibility = component.find("DataTableId");
                            $A.util.addClass(tableVisibility,'slds-hide');
                            if(tableName.indexOf("t_fin_do")>-1 ){
                               component.set("v.textMsg","No Data Found");
                               var reportTabData = component.find("reportdata");
                               $A.util.addClass(reportTabData,'invisible');
                            }
                            else{   
                                var action = component.get("c.getScenarioStatus");
                                action.setParams({ "scenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
                                action.setCallback(this, function(response) {
                                    var state = response.getState();
                                    if(state === 'SUCCESS' && response.getReturnValue() != null){
                                        component.set("v.scenarioStatus",response.getReturnValue());
                                        if(response.getReturnValue().toUpperCase()=="SUCCESS"){
                                            component.set("v.textMsg","No Data Found");
                                            var reportTabData = component.find("reportdata");
                                            $A.util.addClass(reportTabData,'invisible');
                                        }
                                        else{

                                            var reportTabData = component.find("reportdata");
                                            $A.util.addClass(reportTabData,'invisible');
                                            var tableDisplayName = component.get("v.componentLabel");
                                            component.set("v.textMsg","Please Run the Scenario in order to view the Data Preview of \""+tableDisplayName+"\"");
                                        }
                                    }
                                });
                                $A.enqueueAction(action);
                            }    
                        }
                      }
                    } 
                    else if(state == "ERROR"){
                        component.set("v.isDataAvailable",true);
                        component.set("v.textMsg","No Data Found");
                        var reportTabData = component.find("reportdata");
                               $A.util.addClass(reportTabData,'invisible');
                        console.log('Error in calling server side action');
                    }
                    component.hideSpinner(); 
                });
                $A.enqueueAction(action);   

                    } else if(state == "ERROR"){
                    
                        console.log('Error in calling server side action');
                        component.hideSpinner();
                        var tableDisplayName = component.get("v.componentLabel");
                        component.set("v.textMsg","Please Run the Scenario in order to view the Data Preview of \""+tableDisplayName+"\"");
                        var reportTabData = component.find("reportdata");
                               $A.util.addClass(reportTabData,'invisible');
                    }
                 
                });
                $A.enqueueAction(action);

                
               
    },


    callNumerFormatter : function(component, event) {
        var params = event.getParam('arguments');
        if (params) {
            var numberValue = params.param1;
            numberValue = numberValue + '';
            console.log("numberValue: " + numberValue);

            var type = params.param2;
            console.log("type: " + type);

            var arr = [];
            arr.push(numberValue);

            console.log('array --> ' , arr);
              
            var actionNumberFormat = component.get("c.getformatedNumbers");
            actionNumberFormat.setParams({
                "numberVal": arr,
            });
            actionNumberFormat.setCallback(this,function(response)
            {
                var state = response.getState();
                if(state === 'SUCCESS' && response.getReturnValue != null)
                {
                    var result1 = response.getReturnValue();
                    console.log(result1); 

                    if(type == 'fieldCount') {
                        component.set('v.noOfFields',response.getReturnValue()[0]);
                    }
                    if(type == 'totalRowsCount') {
                        component.set('v.totalNoOfRows',response.getReturnValue()[0]);
                    }
                    
                }else{
                    console.log("ERROR");
                }     

            });
            $A.enqueueAction(actionNumberFormat);
        }
    },
    
    createMetaDataTable : function(component, event, helper)
    {
        
        var sessionTable1;
        var $j = jQuery.noConflict();
        var columnDetails = component.get("v.columnDetail");
        console.log(columnDetails);
        
        var result = columnDetails;
        var coulmnJsonResult = [];
        
        if(result != null)
        {
            var element = result[0];
            console.log(element);
            for(var key in element){
                if(key=='dataType'){
                    coulmnJsonResult.push({"data":key,"title":'Data Type',defaultContent:''});
                }else if(key=='fieldName'){
                    coulmnJsonResult.push({"data":key,"title":'Field Name',defaultContent:''});
                }else if(key=='valueType'){
                    coulmnJsonResult.push({"data":key,"title":'Value Type',defaultContent:''});
                }else if(key=='variableType'){
                    coulmnJsonResult.push({"data":key,"title":'Variable Type',defaultContent:''});
                }
            }
        }
        else
        {
            coulmnJsonResult.push({"data":'ID',"title":'ID'});
        }
        
        
        if(result && result.length>0){
            sessionTable1 = $j('.MetaDataTable').DataTable(
            {
                "paging":   false,
                "info":     false,
                "search": false,
                destroy : true,
                "processing": false,
                "dom": '<"div-tbl">t<"bottom-info"> ',
                "data": result,
                //"order" : [[2,"asc"]],
                scrollX : true,
                scrollY:     "195px",
                scrollCollapse: true,
                "language":
                {
                    "emptyTable": "Loading Data",
                    "sLengthMenu": "_MENU_"
                },
                
                columns:coulmnJsonResult,
                columnDefs :
                [
                    {
                        targets:  '_all',
                        defaultContent : '',
                        "createdCell": function (td, cellData, rowData, row, col)
                        {   
                            
                            $j(td).html(cellData);
                        }   
                    }
                    
                ],
                
                "createdRow": function ( row, data, index ) 
                {
                    $j(row).addClass('slds-hint-parent');
                    $j('td', row).addClass('slds-text-align_left slds-truncate');
                    $j('thead > tr> th').addClass('slds-is-sortable slds-is-resizable slds-text-title_caps');
                    $j('thead > tr').addClass('slds-text-title_caps ');
                    
                },

                "initComplete": function( settings, json ) {
                    console.log('calling initComplete--------------');
                    var thElements = document.getElementsByTagName("td");
                    for(var i = 0; i < thElements.length ; i++){
                        thElements[i].title = thElements[i].innerHTML;
                    }
                } 
            }); 
        }else{
            var tableVisibility = component.find("carouselSecondTab");
            $A.util.addClass(tableVisibility,'slds-hide');
        }
    },
    
    ReportsTab : function(component, event, helper) {
        console.log("inside Reports tab");
        component.showSpinner();
        var tab1 = component.find('ReportId');
        var TabOnedata = component.find('ReportTabId');
        
        var reportID = component.find('reportdata');
        
        var tab2 = component.find('UnderlyingDataId');
        var TabTwoData = component.find('UnderlyingTabId');
        var tab3 = component.find('MetaDataInfoId');
        var TabThreeData = component.find('MetaDataTabId');
        
        //show and Active fruits tab
        $A.util.addClass(tab1, 'slds-is-active');
        $A.util.addClass(TabOnedata, 'slds-show');
        $A.util.removeClass(TabOnedata, 'slds-hide');
        $A.util.removeClass(reportID,'slds-hide');
        // Hide and deactivate others tab
        $A.util.removeClass(tab2, 'slds-is-active');
        $A.util.removeClass(TabTwoData, 'slds-show');
        $A.util.addClass(TabTwoData, 'slds-hide');
        
        $A.util.removeClass(tab3, 'slds-is-active');
       // $A.util.removeClass(TabThreeData, 'slds-show');
        $A.util.addClass(TabThreeData, 'slds-hide');

        helper.hideExportButton(component, event, helper);
        helper.hideExportAllButton(component, event, helper);
        component.hideSpinner();
    },

    control : function(component, event, helper) {
        
        var division = component.find("myDivDiv");
        $A.util.addClass(division,'slds-hide');
        var controlDivision = component.find("controlDiv");
        $A.util.removeClass(controlDivision,'slds-hide');
        var PieDivision = component.find("myDivPie");
        $A.util.addClass(PieDivision,'slds-hide');
        
        var btnIcon1 = component.find("btnIcon1");
        var btnIcon2 = component.find("btnIcon2");
        var btnIcon3 = component.find("btnIcon3");
        $A.util.addClass(btnIcon1,'slds-is-selected');
        $A.util.removeClass(btnIcon2,'slds-is-selected');
        $A.util.removeClass(btnIcon3,'slds-is-selected');
        console.log("inside control");
        console.log(component.get("v.underlyingDataTable1"));
    },

    bar : function(component, event, helper) {
        console.log("inside bar method");
        var division = component.find("myDivDiv");
        $A.util.removeClass(division,'slds-hide');
        var controlDivision = component.find("controlDiv");
        $A.util.addClass(controlDivision,'slds-hide');
        var PieDivision = component.find("myDivPie");
        $A.util.addClass(PieDivision,'slds-hide');
        var btnIcon1 = component.find("btnIcon1");
        var btnIcon2 = component.find("btnIcon2");
        var btnIcon3 = component.find("btnIcon3");
        $A.util.removeClass(btnIcon1,'slds-is-selected');
        $A.util.addClass(btnIcon2,'slds-is-selected');
        $A.util.removeClass(btnIcon3,'slds-is-selected');
        //CALL BAR CHARTS
        component.set("v.fieldNameBar","Select Field"); 
        component.set("v.fieldNameSummarizedBar","Select Field"); 
        component.set("v.barGroupByValue","Select Field"); 


        helper.clearChartsDiv();

        helper.resetFilterComponent(component,event,helper);
    },
    
    pie : function(component, event, helper) {
        console.log("inside pie method");
        var division = component.find("myDivDiv");
        $A.util.addClass(division,'slds-hide');
        var controlDivision = component.find("controlDiv");
        $A.util.addClass(controlDivision,'slds-hide');
        var PieDivision = component.find("myDivPie");
        $A.util.removeClass(PieDivision,'slds-hide');
        
         var btnIcon1 = component.find("btnIcon1");
        var btnIcon2 = component.find("btnIcon2");
        var btnIcon3 = component.find("btnIcon3");
        $A.util.removeClass(btnIcon1,'slds-is-selected');
        $A.util.removeClass(btnIcon2,'slds-is-selected');
        $A.util.addClass(btnIcon3,'slds-is-selected');
        component.set("v.fieldNamePie","Select Field"); 
        component.set("v.fieldNameSummarizedPie","Select Field"); 
        component.set("v.pieValue","Select Field"); 

        helper.clearChartsDiv();
        helper.resetFilterComponent(component,event,helper);
    },
    
    aggFunctionValues : function(component, event, helper){
       var action = component.get("c.getAggregateFunctionsList"); 
        action.setCallback(this, function(response){ 
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS"){
                console.log("FUNCTION VALUES");
                console.log( response.getReturnValue());
                var aggregateFunctionOptions=response.getReturnValue();
                component.set("v.aggFuncValues",aggregateFunctionOptions);
            }else{
                console.log("Failed with state:  "+ state);
            }
        });
        $A.enqueueAction(action);
    },
    
    onCustIDChange : function(component, event, helper) {
        
        console.log("inside customer id value change");
        console.log(component.get("v.custIDOptions"));
        component.set("v.outputValue","");
        var custValues = component.get("v.custIDOptions");
        var typeData;
        var aggOptions=[];
        var aggFuncValues = component.get("v.aggFuncValues");

        for(var ctr=0;ctr<custValues.length;ctr++){            
            //columnNames.push({label:colDetail[ctr].fieldName,datatype:colDetail[ctr].dataType});
            if(custValues[ctr].datatype!= undefined){
                if(custValues[ctr].label==component.get("v.customerIDValue")){
                    console.log(custValues[ctr].datatype);
                    typeData = custValues[ctr].datatype.toLowerCase();
                } 
            }
        } 

           
           var SelFunction;
            var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
            console.log('namespace val' +namespace);
            if(namespace != ''){
                
                SelFunction = $A.get("$Label.AxtriaSalesIQTM.Select_Function");

             }else{
                SelFunction = $A.get("$Label.c.Select_Function");
             }                           

        if(typeData=="numeric"){
            aggOptions.push({selectVal:SelFunction , selectLabel :SelFunction});
            console.log("inside if Yes its numeric");
            for(var ctr1=0;ctr1<aggFuncValues.length;ctr1++){
                if(aggFuncValues[ctr1][common.getKey(component,"Field_Type__c")]=="Numeric"){
                    var func = {selectVal:aggFuncValues[ctr1][common.getKey(component,"Functions__c")]};
                    console.log('func val'+func);
                    console.log(func);

                    //aggOptions.push({selectVal:aggFuncValues[ctr1][common.getKey(component,"Functions__c")]});
                    
                    component.set("v.dynamicLabel", $A.getReference("$Label.c." + func.selectVal));
                    // if(component.get("v.dynamicLabel") == ""){
                    //     $A.get('e.force:refreshView').fire();
                    // }
                    //console.log('val ',  $A.getReference("$Label.c." + func.selectVal) + ' ' + component.get("v.dynamicLabel"));
                    aggOptions.push({selectVal:func.selectVal, selectLabel : func.selectVal });
                }
            }
            console.log("aggOptions");
            console.log(aggOptions);
            component.set("v.aggregateFunctionOptions",aggOptions);
            component.set("v.aggFunctionsValue", SelFunction);
        }
                
        if(typeData == "text" || typeData == "multivalue"){
            aggOptions.push({selectVal:SelFunction , selectLabel :SelFunction});
            for(var ctr2=0;ctr2<aggFuncValues.length;ctr2++){
                if(aggFuncValues[ctr2][common.getKey(component,"Functions__c")]=="Count" && aggFuncValues[ctr2][common.getKey(component,"Field_Type__c")]=="Numeric"){
                    //aggOptions.push({selectVal:aggFuncValues[ctr2][common.getKey(component,"Functions__c")]});
                    var func = {selectVal:aggFuncValues[ctr2][common.getKey(component,"Functions__c")]};
                    component.set("v.dynamicLabel", $A.getReference("$Label.c." + func.selectVal));
                    aggOptions.push({selectVal:func.selectVal, selectLabel : func.selectVal });
                }
            } 
            component.set("v.aggregateFunctionOptions",aggOptions);
            component.set("v.aggFunctionsValue", SelFunction);
            
        } 
        if(!typeData){
            component.set("v.aggregateFunctionOptions","");
             component.set("v.outputValue","");
             component.set("v.aggFunctionsValue", "");
        }
        console.log("aggregationFunctions in controller");
        console.log(component.get("v.aggregateFunctionOptions"));
    }, 
                 
    onCustIDChange2 : function(component, event, helper) {
        //$Label.c.Count
        //$Label.c.Sum
        //$Label.c.Max
        //$Label.c.Min
        //$Label.c.Avg
     
        console.log("inside customer id value change");
        console.log(component.get("v.custIDOptions2")); 
        component.set("v.outputValue2","");
        var custValues = component.get("v.custIDOptions2");
        var typeData;
        var aggOptions=[];
        var aggFuncValues = component.get("v.aggFuncValues");

       
        for(var ctr=0;ctr<custValues.length;ctr++){            
            //columnNames.push({label:colDetail[ctr].fieldName,datatype:colDetail[ctr].dataType});
            if(custValues[ctr].datatype!= undefined){
                if(custValues[ctr].label==component.get("v.customerIDValue2")){
                    console.log(custValues[ctr].datatype);
                    typeData = custValues[ctr].datatype.toLowerCase();
                }}
        }

          var SelFunction;
            var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
            console.log('namespace val' +namespace);
            if(namespace != ''){
                
                SelFunction = $A.get("$Label.AxtriaSalesIQTM.Select_Function");

             }else{
                SelFunction = $A.get("$Label.c.Select_Function");
             } 

        if(typeData=="numeric"){
             aggOptions.push({selectVal:SelFunction , selectLabel :SelFunction});
            console.log("inside if Yes its numeric");
            for(var ctr1=0;ctr1<aggFuncValues.length;ctr1++){
                if(aggFuncValues[ctr1][common.getKey(component,"Field_Type__c")]=="Numeric"){
                    //aggOptions.push({selectVal:aggFuncValues[ctr1][common.getKey(component,"Functions__c")]}); 
                     var func = {selectVal:aggFuncValues[ctr1][common.getKey(component,"Functions__c")]};
                    component.set("v.dynamicLabel", $A.getReference("$Label.c." + func.selectVal));
                    aggOptions.push({selectVal:func.selectVal, selectLabel : func.selectVal });
                }
            }
            console.log("aggOptions");
            console.log(aggOptions);
            component.set("v.aggregateFunctionOptions2",aggOptions); 
            component.set("v.aggFunctionsValue2", SelFunction);
        }
        if(typeData=="text"){
             aggOptions.push({selectVal:SelFunction , selectLabel :SelFunction});
            for(var ctr2=0;ctr2<aggFuncValues.length;ctr2++){
                if(aggFuncValues[ctr2][common.getKey(component,"Functions__c")]=="Count" && aggFuncValues[ctr2][common.getKey(component,"Field_Type__c")]=="Numeric"){
                    //aggOptions.push({selectVal:aggFuncValues[ctr2][common.getKey(component,"Functions__c")]});
                     var func = {selectVal:aggFuncValues[ctr2][common.getKey(component,"Functions__c")]};
                    component.set("v.dynamicLabel", $A.getReference("$Label.c." + func.selectVal));
                    aggOptions.push({selectVal:func.selectVal, selectLabel : func.selectVal });
                    
                }
            } 
            component.set("v.aggregateFunctionOptions2",aggOptions);
            component.set("v.aggFunctionsValue2", SelFunction);
            
        }
         if(!typeData){
                component.set("v.aggregateFunctionOptions2","");
              component.set("v.outputValue2","");
              component.set("v.aggFunctionsValue2", "");
            }
        console.log("aggregationFunctions in controller");
        console.log(component.get("v.aggregateFunctionOptions2")); 
    },
    
    onCustIDChange3 : function(component, event, helper) {
         
        console.log("inside customer id value change");
        console.log(component.get("v.custIDOptions3"));
         component.set("v.outputValue3","");
        var custValues = component.get("v.custIDOptions3");
        var typeData;
        var aggOptions=[];
        var aggFuncValues = component.get("v.aggFuncValues");


        for(var ctr=0;ctr<custValues.length;ctr++){            
            //columnNames.push({label:colDetail[ctr].fieldName,datatype:colDetail[ctr].dataType});

            if(custValues[ctr].datatype!= undefined){ 
                if(custValues[ctr].label==component.get("v.customerIDValue3")){
                    console.log(custValues[ctr].datatype);
                    typeData = custValues[ctr].datatype.toLowerCase();
                }}
        }

         var SelFunction;
            var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
            console.log('namespace val' +namespace);
            if(namespace != ''){
                
                SelFunction = $A.get("$Label.AxtriaSalesIQTM.Select_Function");

             }else{
                SelFunction = $A.get("$Label.c.Select_Function");
             } 
        if(typeData=="numeric"){
             aggOptions.push({selectVal:SelFunction , selectLabel :SelFunction});
            console.log("inside if Yes its numeric");
            for(var ctr1=0;ctr1<aggFuncValues.length;ctr1++){
                if(aggFuncValues[ctr1][common.getKey(component,"Field_Type__c")]=="Numeric"){
                    //aggOptions.push({selectVal:aggFuncValues[ctr1][common.getKey(component,"Functions__c")]});
                     var func = {selectVal:aggFuncValues[ctr1][common.getKey(component,"Functions__c")]};
                    component.set("v.dynamicLabel", $A.getReference("$Label.c." + func.selectVal));
                    aggOptions.push({selectVal:func.selectVal, selectLabel : func.selectVal }); 
                }
            }
            console.log("aggOptions");
            console.log(aggOptions);
            component.set("v.aggregateFunctionOptions3",aggOptions); 
            component.set("v.aggFunctionsValue3", SelFunction);
        }
        if(typeData=="text"){
            aggOptions.push({selectVal:SelFunction , selectLabel :SelFunction});
            for(var ctr2=0;ctr2<aggFuncValues.length;ctr2++){
                if(aggFuncValues[ctr2][common.getKey(component,"Functions__c")]=="Count" && aggFuncValues[ctr2][common.getKey(component,"Field_Type__c")]=="Numeric"){
                    //aggOptions.push({selectVal:aggFuncValues[ctr2][common.getKey(component,"Functions__c")]});
                     var func = {selectVal:aggFuncValues[ctr2][common.getKey(component,"Functions__c")]};
                    component.set("v.dynamicLabel", $A.getReference("$Label.c." + func.selectVal));
                    aggOptions.push({selectVal:func.selectVal, selectLabel : func.selectVal });
                    
                }
            } 
            component.set("v.aggregateFunctionOptions3",aggOptions);
            component.set("v.aggFunctionsValue3", SelFunction);
            
        }
        if(!typeData){
                        component.set("v.aggregateFunctionOptions3","");
                     component.set("v.outputValue3","");
                     component.set("v.aggFunctionsValue3", "");
                    }
        console.log("aggregationFunctions in controller");
        console.log(component.get("v.aggregateFunctionOptions3")); 
    },
    
    onCustIDChange4 : function(component, event, helper) {
        
        console.log("inside customer id value change");
        console.log(component.get("v.custIDOptions4")); 
        component.set("v.outputValue4","");
        var custValues = component.get("v.custIDOptions4");
        var typeData;
        var aggOptions=[];
        var aggFuncValues = component.get("v.aggFuncValues");
 
        for(var ctr=0;ctr<custValues.length;ctr++){            
            //columnNames.push({label:colDetail[ctr].fieldName,datatype:colDetail[ctr].dataType});

           if(custValues[ctr].datatype!= undefined){  
            if(custValues[ctr].label==component.get("v.customerIDValue4")){
                console.log(custValues[ctr].datatype);
                typeData = custValues[ctr].datatype.toLowerCase();
            }}
        }

         var SelFunction;
            var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
            console.log('namespace val' +namespace);
            if(namespace != ''){
                
                SelFunction = $A.get("$Label.AxtriaSalesIQTM.Select_Function");

             }else{
                SelFunction = $A.get("$Label.c.Select_Function");
             } 
        if(typeData=="numeric"){
             aggOptions.push({selectVal:SelFunction , selectLabel :SelFunction});
            console.log("inside if Yes its numeric");
            for(var ctr1=0;ctr1<aggFuncValues.length;ctr1++){
                if(aggFuncValues[ctr1][common.getKey(component,"Field_Type__c")]=="Numeric"){
                    //aggOptions.push({selectVal:aggFuncValues[ctr1][common.getKey(component,"Functions__c")]}); 
                     var func = {selectVal:aggFuncValues[ctr1][common.getKey(component,"Functions__c")]};
                    component.set("v.dynamicLabel", $A.getReference("$Label.c." + func.selectVal));
                    aggOptions.push({selectVal:func.selectVal, selectLabel : func.selectVal});
                }
            }
            console.log("aggOptions");
            console.log(aggOptions);
            component.set("v.aggregateFunctionOptions4",aggOptions);
            component.set("v.aggFunctionsValue4", SelFunction); 
        }
        if(typeData=="text"){
             aggOptions.push({selectVal:SelFunction , selectLabel :SelFunction});
            for(var ctr2=0;ctr2<aggFuncValues.length;ctr2++){
                if(aggFuncValues[ctr2][common.getKey(component,"Functions__c")]=="Count" && aggFuncValues[ctr2][common.getKey(component,"Field_Type__c")]=="Numeric"){
                    //aggOptions.push({selectVal:aggFuncValues[ctr2][common.getKey(component,"Functions__c")]});
                     var func = {selectVal:aggFuncValues[ctr2][common.getKey(component,"Functions__c")]};
                    component.set("v.dynamicLabel", $A.getReference("$Label.c." + func.selectVal));
                    aggOptions.push({selectVal:func.selectVal, selectLabel : func.selectVal});
                    
                }
            } 
            component.set("v.aggregateFunctionOptions4",aggOptions);
            component.set("v.aggFunctionsValue4", SelFunction);
            
        }
        if(!typeData){
           component.set("v.aggregateFunctionOptions4","");
            component.set("v.outputValue4","");
            component.set("v.aggFunctionsValue4", "");
        }
        console.log("aggregationFunctions in controller");
        console.log(component.get("v.aggregateFunctionOptions4")); 
    },
    
    onCustIDChange5 : function(component, event, helper) {
      
        console.log("inside customer id value change");
        console.log(component.get("v.custIDOptions5")); 
        component.set("v.outputValue5",""); 
        var custValues = component.get("v.custIDOptions5");
        var typeData;
        var aggOptions=[];
        var aggFuncValues = component.get("v.aggFuncValues");

        for(var ctr=0;ctr<custValues.length;ctr++){            
            //columnNames.push({label:colDetail[ctr].fieldName,datatype:colDetail[ctr].dataType});

            if(custValues[ctr].datatype!= undefined){ 
            if(custValues[ctr].label==component.get("v.customerIDValue5")){
                console.log(custValues[ctr].datatype);
                typeData = custValues[ctr].datatype.toLowerCase();
            }}
        }

         var SelFunction;
            var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
            console.log('namespace val' +namespace);
            if(namespace != ''){
                
                SelFunction = $A.get("$Label.AxtriaSalesIQTM.Select_Function");

             }else{
                SelFunction = $A.get("$Label.c.Select_Function");
             } 
        if(typeData=="numeric"){
             aggOptions.push({selectVal:SelFunction , selectLabel :SelFunction});
            console.log("inside if Yes its numeric");
            for(var ctr1=0;ctr1<aggFuncValues.length;ctr1++){
                if(aggFuncValues[ctr1][common.getKey(component,"Field_Type__c")]=="Numeric"){
                    //aggOptions.push({selectVal:aggFuncValues[ctr1][common.getKey(component,"Functions__c")]}); 
                     var func = {selectVal:aggFuncValues[ctr1][common.getKey(component,"Functions__c")]};
                    component.set("v.dynamicLabel", $A.getReference("$Label.c." + func.selectVal));


                    aggOptions.push({selectVal:func.selectVal, selectLabel : func.selectVal});
                }
            }
            console.log("aggOptions");
            console.log(aggOptions);
            component.set("v.aggregateFunctionOptions5",aggOptions); 
            component.set("v.aggFunctionsValue5", SelFunction);
        }
         if(typeData=="text"){
             aggOptions.push({selectVal:SelFunction , selectLabel :SelFunction});
            for(var ctr2=0;ctr2<aggFuncValues.length;ctr2++){
                if(aggFuncValues[ctr2][common.getKey(component,"Functions__c")]=="Count" && aggFuncValues[ctr2][common.getKey(component,"Field_Type__c")]=="Numeric"){
                    //aggOptions.push({selectVal:aggFuncValues[ctr2][common.getKey(component,"Functions__c")]});
                     var func = {selectVal:aggFuncValues[ctr2][common.getKey(component,"Functions__c")]};
                    component.set("v.dynamicLabel", $A.getReference("$Label.c." + func.selectVal));
                    aggOptions.push({selectVal:func.selectVal, selectLabel : func.selectVal});
                    
                }
            } 
            component.set("v.aggregateFunctionOptions5",aggOptions);
            component.set("v.aggFunctionsValue5", SelFunction);
            
        }
         if(!typeData){
           component.set("v.aggregateFunctionOptions5","");
           component.set("v.outputValue5","");
           component.set("v.aggFunctionsValue5", "");
           }
        console.log("aggregationFunctions in controller");
        console.log(component.get("v.aggregateFunctionOptions5")); 
    },
    
    onFuncChange : function(component, event, helper) {
        console.log("inside function value change");
        console.log(component.get("v.aggFunctionsValue"));
        console.log(component.get("v.reportsUrl"));
        console.log(component.get("v.contextName"));
        var reportURL = component.get("v.reportsUrl");
        var instanceURL = component.get("v.instanceURL");
        var contextValue = component.get("v.contextName");
         var buttonNo = component.set("v.buttonNo","1");
        var func = component.get("v.aggFunctionsValue");
        
        var custField = component.get("v.customerIDValue");
        var action = component.get("c.webSeriveResponseMethod");
        var table =component.get("v.tableName"); 
        var click = "clickedbutton1";
        component.getFromWebservice(custField,func,click); 
    },
    
     onFuncChange2 : function(component, event, helper) {
        console.log("inside function value change");
        console.log(component.get("v.aggFunctionsValue2"));
        console.log(component.get("v.reportsUrl"));
        console.log(component.get("v.contextName"));
        var reportURL = component.get("v.reportsUrl");
        var contextValue = component.get("v.contextName");
        var buttonNo = component.set("v.buttonNo","2");
        var func = component.get("v.aggFunctionsValue2"); 
        var custField = component.get("v.customerIDValue2");
         
        var action = component.get("c.webSeriveResponseMethod");
        var table =component.get("v.tableName");
        var click = "clickedbutton2"; 
        component.getFromWebservice(custField,func,click);  
    }, 
    
    onFuncChange3 : function(component, event, helper) {
        console.log("inside function value change");
        console.log(component.get("v.aggFunctionsValue3"));
        console.log(component.get("v.reportsUrl"));
        console.log(component.get("v.contextName"));
        var reportURL = component.get("v.reportsUrl");
        var contextValue = component.get("v.contextName");
        var buttonNo = component.set("v.buttonNo","3");
        var func = component.get("v.aggFunctionsValue3"); 
        var custField = component.get("v.customerIDValue3");
         
        var action = component.get("c.webSeriveResponseMethod");
        var table =component.get("v.tableName"); 
        var click = "clickedbutton3";
        component.getFromWebservice(custField,func,click);  
    }, 
    
    onFuncChange4 : function(component, event, helper) {
        console.log("inside function value change");
        console.log(component.get("v.aggFunctionsValue4"));
        console.log(component.get("v.reportsUrl"));
        console.log(component.get("v.contextName"));
        var reportURL = component.get("v.reportsUrl");
        var contextValue = component.get("v.contextName");
        var buttonNo = component.set("v.buttonNo","4");
        var func = component.get("v.aggFunctionsValue4"); 
        var custField = component.get("v.customerIDValue4");
         
        var action = component.get("c.webSeriveResponseMethod");
        var table =component.get("v.tableName");
        var click = "clickedbutton4";  
        component.getFromWebservice(custField,func,click); 
    }, 
    
    onFuncChange5 : function(component, event, helper) {
        console.log("inside function value change");
        console.log(component.get("v.aggFunctionsValue5"));
        console.log(component.get("v.reportsUrl"));
        console.log(component.get("v.contextName"));
        var reportURL = component.get("v.reportsUrl");
        var contextValue = component.get("v.contextName");
        var buttonNo = component.set("v.buttonNo","5");
        var func = component.get("v.aggFunctionsValue5"); 
        var custField = component.get("v.customerIDValue5");
         
        var action = component.get("c.webSeriveResponseMethod");
        var table =component.get("v.tableName");
        var click = "clickedbutton5";
        component.getFromWebservice(custField,func,click);   
    }, 

    controlChartWebserviceCaller : function(component, event, helper) {
       console.log("inside controlchart webservice call");
       
        var valValue;
        var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
        console.log('namespace val' +namespace);
        if(namespace != ''){
            
            valValue = $A.get("$Label.AxtriaSalesIQTM.Value");

         }else{
            valValue = $A.get("$Label.c.Value");
         }

       var query = '';
       var params = event.getParam('arguments');
        if (params) {
            var param1 = params.param1;
            var param2 = params.param2;
            var param3 = params.param3;

            if(param3 == "clickedbutton1")
                query = component.get("v.query1");
            else if(param3 == "clickedbutton2")
                query = component.get("v.query2");
            else if(param3 == "clickedbutton3")
                query = component.get("v.query3");
            else if(param3 == "clickedbutton4")
                query = component.get("v.query4");
            else if(param3 == "clickedbutton5")
                query = component.get("v.query5");
            // add your code here
        console.log("param1"+param1);
        console.log("param2"+param2);
        console.log("param3"+param3);
        var reportURL = component.get("v.reportsUrl");
        var instanceURL = component.get("v.instanceURL");

        var contextValue = component.get("v.contextName");
        var table =component.get("v.tableName");  
        var cusField = param1; 
        var funcField = param2;

        /* 
        var action = component.get("c.webSeriveResponseMethodQueryBuilder");
        
        
        //var res = 'http://35.164.63.126:5000/CPG_report_tab_webservice/CPGData/home/?scenario_1='+currentScenarioId+'&scenario_2='+previousScenarioId+'&tab_id='+tab_id+'&team='+teamName+'&context='+currentins; 
        var res = reportURL+'preview/?tableName='+table+'&context='+contextValue+'&fieldName='+cusField+'&function='+funcField+'&condition=';
        //var url='preview/?tableName=t_fin_do3&context=brms_dev&fieldName='+cusField+'&function='+funcField+'&condition=';
 
        console.log('strUrl->'+res);
        if(query){
            action.setParams({
                "url": res,
                "query" : query
            });
        }
        else{
               action.setParams({
            "url": res,
            "query" : '1=1'
        }); 
       }
       */

       var action = component.get("c.getCalloutResponseforControlChart");

         if(query!=null && query!=""){
            action.setParams({
                "url": instanceURL,
                "tableName" : table,
                "fieldName" : cusField,
                "function" : funcField,
                "whereClause" : query
            });
        }
        else{
               action.setParams({
             "url": instanceURL,
             "tableName" : table,
             "fieldName" : cusField,
             "function" : funcField,
             "whereClause" : '1=1'
        }); 
       }
        
        action.setCallback(this,function(response)
        {
             var state = response.getState();
             console.log(response.getReturnValue());

             if(component.isValid() && state==="SUCCESS" && response.getReturnValue()!=""){

                var actionNumberFormat = component.get("c.getformatedNumbers");
                actionNumberFormat.setParams({
                    "numberVal": response.getReturnValue(),
                });
                actionNumberFormat.setCallback(this,function(response)
                {
                    var state = response.getState();
                    if(state === 'SUCCESS' && response.getReturnValue != null)
                    {
                         
                        var arrHtml=[];
                        var state = response.getState();
                        var rowCount; 
                        var result1 = response.getReturnValue();
                        console.log(result1);
                        // for(var outer = 0; outer < result.length; outer++){
                        //    for(var inner = 0; inner < timestamp_columns_vals.length; inner++){
                        //         for(var counter= 0 ; counter<result1.length ; counter++ ){
                        //            result[outer][timestamp_columns_vals[inner]] = result1[counter];
                        //         }
                        //     }
                        // }

                        console.log('RESPONSE'+response.getReturnValue());
                        var valueOutputavg='';
                        if(component.get("v.buttonNo")=="1" || param3=="clickedbutton1"){
                           var valueOutput = response.getReturnValue();
                          /*                 if(valueOutput % 1 != 0){
                                               valueOutputavg = valueOutput.toFixed(4);
                                           }else{
                                               valueOutputavg = valueOutput;
                                           } */
                           component.set("v.outputValue",valueOutput);
                        }
                        else if(component.get("v.buttonNo")=="2" || param3=="clickedbutton2"){
                             var valueOutput = response.getReturnValue();
                          /*                 if(valueOutput % 1 != 0){
                                               valueOutputavg = valueOutput.toFixed(4);
                                           }else{
                                               valueOutputavg = valueOutput;
                                           } */
                           component.set("v.outputValue2",valueOutput);
                        }
                        else if(component.get("v.buttonNo")=="3" || param3=="clickedbutton3"){
                           var valueOutput = response.getReturnValue();
                          /*                 if(valueOutput % 1 != 0){
                                               valueOutputavg = valueOutput.toFixed(4);
                                           }else{
                                               valueOutputavg = valueOutput;
                                           } */
                           component.set("v.outputValue3",valueOutput);
                        }
                        else if(component.get("v.buttonNo")=="4" || param3=="clickedbutton4"){
                              var valueOutput = response.getReturnValue();
                          /*                 if(valueOutput % 1 != 0){
                                               valueOutputavg = valueOutput.toFixed(4);
                                           }else{
                                               valueOutputavg = valueOutput;
                                           } */
                           component.set("v.outputValue4",valueOutput);
                        } 
                        else if(component.get("v.buttonNo")=="5" || param3=="clickedbutton5"){
                           var valueOutput = response.getReturnValue();
                          /*                 if(valueOutput % 1 != 0){
                                               valueOutputavg = valueOutput.toFixed(4);
                                           }else{
                                               valueOutputavg = valueOutput;
                                           } */
                           component.set("v.outputValue5",valueOutput);
                        }

                            }
                        });
                                $A.enqueueAction(actionNumberFormat);

             }else{
                console.log("ERROR");
                 if(component.get("v.buttonNo")=="1" || param3=="clickedbutton1"){
                       component.set("v.outputValue",valValue);
                    }
                    else if(component.get("v.buttonNo")=="2" || param3=="clickedbutton2"){
                       component.set("v.outputValue2",valValue);
                    }
                    else if(component.get("v.buttonNo")=="3" || param3=="clickedbutton3"){
                       component.set("v.outputValue3",valValue);
                    }
                    else if(component.get("v.buttonNo")=="4" || param3=="clickedbutton4"){
                       component.set("v.outputValue4",valValue);
                    }
                    else if(component.get("v.buttonNo")=="5" || param3=="clickedbutton5"){
                       component.set("v.outputValue5",valValue);
                    }
             }
            /*
            if(component.isValid() && state==="SUCCESS"){
                console.log('RESPONSE'+response.getReturnValue());
                var valueOutputavg='';
                if(component.get("v.buttonNo")=="1" || param3=="clickedbutton1"){
                   var valueOutput = Number(response.getReturnValue());
                                   if(valueOutput % 1 != 0){
                                       valueOutputavg = valueOutput.toFixed(4);
                                   }else{
                                       valueOutputavg = valueOutput;
                                   } 
                   component.set("v.outputValue",valueOutputavg);
                }
                else if(component.get("v.buttonNo")=="2" || param3=="clickedbutton2"){
                    var valueOutput = Number(response.getReturnValue());
                                   if(valueOutput % 1 != 0){
                                        valueOutputavg = valueOutput.toFixed(4);
                                   }else{
                                       valueOutputavg = valueOutput;
                                   } 
                   component.set("v.outputValue2",valueOutputavg);
                }
                else if(component.get("v.buttonNo")=="3" || param3=="clickedbutton3"){
                    var valueOutput = Number(response.getReturnValue());
                                   if(valueOutput % 1 != 0){
                                        valueOutputavg = valueOutput.toFixed(4);
                                   }else{
                                       valueOutputavg = valueOutput;
                                   } 
                   component.set("v.outputValue3",valueOutputavg);
                }
                else if(component.get("v.buttonNo")=="4" || param3=="clickedbutton4"){
                    var valueOutput = Number(response.getReturnValue());
                                   if(valueOutput % 1 != 0){
                                        valueOutputavg = valueOutput.toFixed(4);
                                   }else{
                                       valueOutputavg = valueOutput;
                                   } 
                   component.set("v.outputValue4",valueOutputavg);
                } 
                else if(component.get("v.buttonNo")=="5" || param3=="clickedbutton5"){
                    var valueOutput = Number(response.getReturnValue());
                                   if(valueOutput % 1 != 0){
                                        valueOutputavg = valueOutput.toFixed(4);
                                   }else{
                                       valueOutputavg = valueOutput;
                                   } 
                   component.set("v.outputValue5",valueOutputavg);
                }

             }
             else{
                    if(component.get("v.buttonNo")=="1" || param3=="clickedbutton1"){
                       component.set("v.outputValue","value");
                    }
                    else if(component.get("v.buttonNo")=="2" || param3=="clickedbutton2"){
                       component.set("v.outputValue2","value");
                    }
                    else if(component.get("v.buttonNo")=="3" || param3=="clickedbutton3"){
                       component.set("v.outputValue3","value");
                    }
                    else if(component.get("v.buttonNo")=="4" || param3=="clickedbutton4"){
                       component.set("v.outputValue4","value");
                    }
                    else if(component.get("v.buttonNo")=="5" || param3=="clickedbutton5"){
                       component.set("v.outputValue5","value");
                    }
                     console.log("Failed with state:  "+ state);                  
                } */
         })
        $A.enqueueAction(action); 
         }
    },


    onBarFuncChange : function(component, event, helper) {
        console.log("inside bar function value change");
        component.set("v.chartMode","Bar");

        var reportURL = component.get("v.reportsUrl");
        var contextValue = component.get("v.contextName");
        var fieldVal = component.get("v.fieldNameBar"); 
        var sumValue = component.get("v.fieldNameSummarizedBar");
        var grpByVal = component.get("v.barGroupByValue");
        var instanceURL = component.get("v.instanceURL");
        var table =component.get("v.tableName");
        var urlParams = {}; 
        urlParams['serviceUrl'] = instanceURL;
        
        helper.hideNoDataMessage(component, event, helper,"NodataMsg");
        
        if((fieldVal && fieldVal!="Select Field")&&(sumValue && sumValue!="Select Field")&&(grpByVal && grpByVal!="Select Field")){

            urlParams['tableName'] = table;
            urlParams['fieldName'] = fieldVal;
            urlParams['valueToSum'] = sumValue;
            urlParams['groupBy'] = grpByVal;

        }else{

            if((fieldVal && fieldVal!="Select Field")&&(sumValue && sumValue!="Select Field")){
                urlParams['tableName'] = table;
                urlParams['fieldName'] = fieldVal;
                urlParams['valueToSum'] = sumValue;
                urlParams['groupBy'] = '';

            }else{
                d3.selectAll("myDiv").remove();
                helper.hideNoDataMessage(component, event, helper,"NodataMsg");
            }
        } 
        
        var action = component.get("c.getCalloutResponseforBarChart");
        if(component.get("v.chartMode") =="Bar" && component.get("v.queryBar"))
        {
            action.setParams({
               "urlParams": urlParams,
               "whereClause" : component.get("v.queryBar") 
            });
        }
        else{
            action.setParams({
                "urlParams": urlParams,
                "whereClause" : '1=1'
            }); 
        }

        action.setCallback(this,function(response){
           var state = response.getState();
           if(component.isValid() && state==="SUCCESS"){

                    helper.hideNoDataMessage(component, event, helper,"NodataMsg");     
                    var barResult = response.getReturnValue();
                    console.log(barResult);
                    //GROUPED DATA CHART
                        if((fieldVal && fieldVal!="Select Field")&&(sumValue && sumValue!="Select Field")&&(grpByVal && grpByVal!="Select Field")){

                        var chart_data = barResult["CPG_Report_preview_summary_chart "].filter( function(obj){
                                if(obj[fieldVal] != "0" && ( obj[fieldVal] != undefined || obj[fieldVal] != null ) ) return obj;
                            });
                
                            if(fieldVal == grpByVal){
                                helper.drawBarChart(chart_data,fieldVal,grpByVal);
                            }else{
                                helper.drawGroupedBarChart(chart_data,fieldVal,grpByVal);
                            }
                            component.set("v.chartMode","");
                            helper.hideNoDataMessage(component, event, helper,"NodataMsg");   
                        }
                        else{
                        //SIMPLE BAR CHART
                            var chart_data = barResult["CPG_Report_preview_summary_chart "].filter( function(obj){
                               if(obj[fieldVal] != "0" && ( obj[fieldVal] != undefined || obj[fieldVal] != null ) ) return obj;
                            });
                            helper.drawBarChart(chart_data,fieldVal,grpByVal);
                            component.set("v.chartMode","");
                            helper.hideNoDataMessage(component, event, helper,"NodataMsg");
                        }
            }
            else{
               console.log("Failed with state:  "+ state);
               helper.clearChartsDiv();
               helper.showNoDataMessage(component, event, helper,"NodataMsg");
           }
        })
        $A.enqueueAction(action); 
    },
    
    onPieFuncChange : function(component, event, helper) {
        console.log("inside pie function value change");
        component.set("v.chartMode","Pie");
        console.log(component.get("v.fieldNamePie"));
        console.log(component.get("v.fieldNameSummarizedPie")); 
        console.log(component.get("v.pieValue"));
       
        var reportURL = component.get("v.reportsUrl");
        var contextValue = component.get("v.contextName");
        var grp1Val = component.get("v.fieldNamePie"); 
        var grp2Value = component.get("v.fieldNameSummarizedPie");
        var sumValue = component.get("v.pieValue"); 
        var instanceURL = component.get("v.instanceURL");

        var table =component.get("v.tableName");  
        var labels = [];
        var labels2 = [];
        var labels3 = [];
        var labels4 = [];
        var labelVal = []; 
        var data1 =[];
        
        var nomsgData = component.find("NodataMsgPie");
        $A.util.addClass(nomsgData,'slds-hide');
         
        var grpByClause='' ;
        //var action = component.get("c.webSeriveResponseMethodChart");
        var action = component.get("c.getCalloutResponseforPieChart"); 

        //SSL CHANGES
        var urlParams = {};
        urlParams['serviceUrl'] = instanceURL;
 
        if(sumValue && sumValue!="Select Field"){
             if((grp1Val && grp1Val!="Select Field")&&(grp2Value && grp2Value!="Select Field")){

                  if(grp1Val == grp2Value){
                        urlParams['tableName'] = table;
                        urlParams['groupBy1'] = grp1Val;
                        urlParams['groupBy2'] = '';
                        urlParams['valueToSum'] = sumValue;

                    }else{
                        urlParams['tableName'] = table;
                        urlParams['groupBy1'] = grp1Val;
                        urlParams['groupBy2'] = grp2Value;
                        urlParams['valueToSum'] = sumValue;
                    }
             }else{
                 if(grp1Val && grp1Val!="Select Field"){
                    
                        urlParams['tableName'] = table;
                        urlParams['groupBy1'] = grp1Val;
                        urlParams['groupBy2'] = '';
                        urlParams['valueToSum'] = sumValue;

                 }else{
                     if(grp2Value && grp2Value!="Select Field"){

                        urlParams['tableName'] = table;
                        urlParams['groupBy1'] = '';
                        urlParams['groupBy2'] = grp2Value;
                        urlParams['valueToSum'] = sumValue;  

                     }else{
                         console.log("Not selected required Values");
                     }
                 }
             }
         }else{
             console.log("Not selected required Values");
         }
         



        if(component.get("v.chartMode") =="Pie" && component.get("v.queryPie")){
            action.setParams({
                "urlParams": urlParams,
                "whereClause" : component.get("v.queryPie") 
            });
        }
        else{
            action.setParams({
                "urlParams": urlParams,
                "whereClause" : '1=1'
            }); 
       }
        
        action.setCallback(this,function(response)
                           {
                               var state = response.getState();
                               if(component.isValid() && state==="SUCCESS"){
                                   console.log(response.getReturnValue()); 
                                    if(response.getReturnValue()){
                                  
                                   var pieResult = response.getReturnValue();
                                console.log('pieResult');
                                   console.log(pieResult);
                                 
                                   var hasValues = false;

                                    var chart_data = pieResult["CPG_Report_preview_pie_chart "].filter( function(obj){
                                       if(obj[grp1Val] != "0" && ( obj[grp1Val] != undefined || obj[grp1Val] != null ) ){
                                            hasValues = obj.sum > 0 ? true : hasValues;
                                            return obj;
                                       }  
                                    });


                                if(hasValues){
                                    if((grp1Val && grp1Val!="Select Field")&&(grp2Value && grp2Value!="Select Field")){
                                    
                                    if(grp1Val == grp2Value){
                                        //drawPieChart
                                        helper.drawPieChart(chart_data,'',grp1Val,grp2Value);
                                      }
                                      else{
                                            helper.drawGroupedPieChart(chart_data,'',grp1Val,grp2Value);
                                        
                                        }
                                    }else{
                                        helper.drawPieChart(chart_data,'',grp1Val,grp2Value);
                                    }

                                    component.set("v.chartMode","");

                                }else{
                                    
                                    d3.select('#siq_bar_chart').html("");
                                    d3.select('#siq_legend').html("");
                                    d3.select('#siq_pie_chart').html("");
                                    d3.select('#siq_pie_legend').html("");
                                    component.set("v.chartMode","");
                                    component.set("v.noDataMsgPie","NO DATA FOUND");
                                    var nomsgData = component.find("NodataMsgPie");
                                    $A.util.removeClass(nomsgData,'slds-hide');
                                }
                                    
                                        
                                    }else{
                                        console.log("Failed with state:  "+ state);
                                        d3.select('#siq_bar_chart').html("");
                                        d3.select('#siq_legend').html("");
                                        d3.select('#siq_pie_chart').html("");
                                        d3.select('#siq_pie_legend').html("");

                                         component.set("v.chartMode","");
                                        component.set("v.noDataMsgPie","NO DATA FOUND");
                                        var nomsgData = component.find("NodataMsgPie");
                                        $A.util.removeClass(nomsgData,'slds-hide');
                               }}
                               else{
                                   console.log("Failed with state:  "+ state);
                                    //Plotly.purge('myDiv2');
                                    jQuery("#siq_pie_chart").empty();
                                    component.set("v.chartMode","");
                                   var nomsgData = component.find("NodataMsgPie");
                                        $A.util.removeClass(nomsgData,'slds-hide');
                               }
                           })
        $A.enqueueAction(action); 
     },
                           
    callCriteriaLogic : function(component, event, helper) {
        
        console.log("fileTypeVALUE");
        console.log(component.get("v.fileTypeVal"));
        console.log("inside navigate component");
        component.set("v.chartMode","Control");
        var whichOne = event.getSource().getLocalId();
        console.log(whichOne);
         var errorIcn = component.find("errorIcon1");
            $A.util.addClass(errorIcn,'slds-hide');
        if(whichOne=="buttonRow1"){
            var custID = component.get("v.customerIDValue");
            var aggFunc = component.get("v.aggFunctionsValue");
            var buttonNo = component.set("v.buttonNo","1");
            console.log('--persistblock ', component.get("v.persistBlock1"));

            var childComponent = component.find("childFilter");
            if(component.get("v.persistBlock1") != "[]"){
                console.log('--inside persist 1');
                childComponent.setWrapBlock(component.get("v.persistBlock1"));    
            }
            else{
                //childComponent.callblankWrapBlock();
                //childComponent.reintialise();

                 childComponent.init(); 
                 childComponent.set("v.parsedJSONQuery",component.get("v.updatedWrapperButton1"));
                 childComponent.set("v.parsedQuery",component.get("v.parsedQuery1"));
                 childComponent.set("v.errorMsg",component.get("v.errMsg1"));
                 //component.showSpinner();

            }

        } else if(whichOne=="buttonRow2"){
            var custID = component.get("v.customerIDValue2");
            var aggFunc = component.get("v.aggFunctionsValue2");
            var buttonNo = component.set("v.buttonNo","2"); 

            var childComponent = component.find("childFilter");
            if(component.get("v.persistBlock2") != "[]"){
                console.log('--inside persist 2');                
                childComponent.setWrapBlock(component.get("v.persistBlock2"));
            }
            else{
                //childComponent.callblankWrapBlock();
               // childComponent.reintialise();
               childComponent.init();
               childComponent.set("v.parsedJSONQuery",component.get("v.updatedWrapperButton2"));
               childComponent.set("v.parsedQuery",component.get("v.parsedQuery2"));
               childComponent.set("v.errorMsg",component.get("v.errMsg2"));
            }
        } else if(whichOne=="buttonRow3"){
            var custID = component.get("v.customerIDValue3");
            var aggFunc = component.get("v.aggFunctionsValue3");
             var buttonNo = component.set("v.buttonNo","3");
             console.log('--persistblock ', component.get("v.persistBlock3"));

            var childComponent = component.find("childFilter");
            if(component.get("v.persistBlock3") != "[]"){
                console.log('--inside persist 3');
                childComponent.setWrapBlock(component.get("v.persistBlock3"));    
            }
            else{
                //childComponent.reintialise();
                   childComponent.init();
                   childComponent.set("v.parsedJSONQuery",component.get("v.updatedWrapperButton3"));
                   childComponent.set("v.parsedQuery",component.get("v.parsedQuery3"));
                   childComponent.set("v.errorMsg",component.get("v.errMsg3"));
            }
        } else if(whichOne=="buttonRow4"){
            var custID = component.get("v.customerIDValue4");
            var aggFunc = component.get("v.aggFunctionsValue4");
             var buttonNo = component.set("v.buttonNo","4");
             console.log('--persistblock ', component.get("v.persistBlock4"));

            var childComponent = component.find("childFilter");
            if(component.get("v.persistBlock4") != "[]"){
                console.log('--inside persist 4');
                childComponent.setWrapBlock(component.get("v.persistBlock4"));    
            }
            else
                //childComponent.reintialise();
                 childComponent.init();
               childComponent.set("v.parsedJSONQuery",component.get("v.updatedWrapperButton4"));
               childComponent.set("v.parsedQuery",component.get("v.parsedQuery4"));
              childComponent.set("v.errorMsg",component.get("v.errMsg4"));
        } else {
            var custID = component.get("v.customerIDValue5");
            var aggFunc = component.get("v.aggFunctionsValue5");
             var buttonNo = component.set("v.buttonNo","5");
             console.log('--persistblock ', component.get("v.persistBlock5"));

            var childComponent = component.find("childFilter");
            if(component.get("v.persistBlock5") != "[]"){
                console.log('--inside persist 5');
                childComponent.setWrapBlock(component.get("v.persistBlock5"));    
            }
            else
                //childComponent.reintialise();
                 childComponent.init();
               childComponent.set("v.parsedJSONQuery",component.get("v.updatedWrapperButton5"));
               childComponent.set("v.parsedQuery",component.get("v.parsedQuery5"));
               childComponent.set("v.errorMsg",component.get("v.errMsg5"));
        }
        component.set("v.fieldValue",custID);
        component.set("v.functionaValue",aggFunc);              
        console.log(component.get("v.scenarioRuleInstanceId"));  
        console.log(custID );
        console.log(aggFunc); 
        console.log(component.get("v.scenarioId"));
        console.log(component.get("v.workSpaceId"));
        console.log(component.get("v.namespace"));
        var modalDiv = component.find("modal2");
        $A.util.removeClass(modalDiv,'slds-hide');
    },
    
    callCriteriaLogicBar : function(component, event, helper) {
        console.log("Filter Criteria for Bar clicked"); 
        component.set("v.chartMode","Bar");
        var modalDiv = component.find("modal2");
        $A.util.removeClass(modalDiv,'slds-hide');
        var whichOne = event.getSource().getLocalId();
        console.log(whichOne);
        if(whichOne=="buttonBar"){
           component.set("v.buttonNo","barButton");
           console.log('--persistblock ', component.get("v.persistBlockBar"));

            var childComponent = component.find("childFilter");
            if(component.get("v.persistBlockBar") != "[]"){
                console.log('--inside persist persistBlockBar');
                childComponent.setWrapBlock(component.get("v.persistBlockBar"));    
            }
            else
                childComponent.init();
            childComponent.set("v.parsedJSONQuery",component.get("v.updatedWrapperButtonBar"));
               childComponent.set("v.parsedQuery",component.get("v.parsedQueryBar"));
               childComponent.set("v.errorMsg",component.get("v.errMsgBar"));
           //component.getFields(); 
        }
         
    },
    callCriteriaLogicPie : function(component, event, helper) {
        console.log("Filter Criteria for Pie clicked"); 
        component.set("v.chartMode","Pie");
        var modalDiv = component.find("modal2");
        $A.util.removeClass(modalDiv,'slds-hide');
        var whichOne = event.getSource().getLocalId();
        console.log(whichOne);
        if(whichOne=="buttonPie"){
           component.set("v.buttonNo","pieButton");
           console.log('--persistblock ', component.get("v.persistBlockPie"));

            var childComponent = component.find("childFilter");
            if(component.get("v.persistBlockPie") != "[]"){
                console.log('--inside persist persistBlockPie');
                childComponent.setWrapBlock(component.get("v.persistBlockPie"));    
            }
            else
               // childComponent.reintialise();
               childComponent.init();
               childComponent.set("v.parsedJSONQuery",component.get("v.updatedWrapperButtonPie"));
               childComponent.set("v.parsedQuery",component.get("v.parsedQueryPie"));
               childComponent.set("v.errorMsg",component.get("v.errMsgPie"));
          // component.getFields(); 
        }
    
    },
   
     getFieldsDetails : function(component, event, helper) {
        //var params = event.getParam('arguments');
        console.log("get fields called");  
        var $j = jQuery.noConflict();    

        var colDetail = component.get("v.custIDOptions");
        console.log('--colDetail');
        console.log(colDetail);
        var columnNames = [];              

        for(var ctr=1; ctr<colDetail.length; ctr++){ 

            if(colDetail[ctr].datatype == "Numeric"){
                columnNames.push({id: colDetail[ctr].label, label:colDetail[ctr].label, type: 'double',operators: ['equal', 'not_equal', 'is_null', 'is_not_null'],value:{}});
            }else{
                columnNames.push({id: colDetail[ctr].label, label:colDetail[ctr].label, type: 'string',operators: ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'],value:{}});
            }
        }
        console.log('--columnNames'); 
        console.log(columnNames);

        $j("#builder").queryBuilder({
    	    plugins: ["bt-tooltip-errors"],
            filters : columnNames 
    	});
    /****************************************************************
    						Triggers and Changers QueryBuilder
 *****************************************************************/

	$j("#btn-get").on("click", function() {
	  var result = $j("#builder").queryBuilder("getRules");
	  if (!$j.isEmptyObject(result)) {
	  }
	  else{
	  	console.log("invalid object :");
	  }
	  console.log(result);
	});

	$j("#btn-reset").on("click", function() {
	  $j("#builder").queryBuilder("reset");
	});

	$j("#btn-set").on("click", function() {
	 // $('#builder').queryBuilder('setRules', result);
	  var result = $j("#builder").queryBuilder("getRules");
	  if (!$j.isEmptyObject(result)) {
	  	rules_basic = result;
      }}
	);
   
	$j("#builder").on("getRules.queryBuilder.filter", function(e) {
		//$log.info(e.value);
	});
         if(component.get("v.buttonNo")=="1" && component.get("v.resControlRule")){
               $j('#builder').queryBuilder('setRules',component.get("v.resControlRule"));  
        }
         if(component.get("v.buttonNo")=="2" && component.get("v.resControlRule2")){
               $j('#builder').queryBuilder('setRules',component.get("v.resControlRule2"));  
        }
         if(component.get("v.buttonNo")=="3" && component.get("v.resControlRule3")){
               $j('#builder').queryBuilder('setRules',component.get("v.resControlRule3"));  
        }
         if(component.get("v.buttonNo")=="4" && component.get("v.resControlRule4")){
               $j('#builder').queryBuilder('setRules',component.get("v.resControlRule4"));  
        }
         if(component.get("v.buttonNo")=="5" && component.get("v.resControlRule5")){
               $j('#builder').queryBuilder('setRules',component.get("v.resControlRule5"));  
        }
         if(component.get("v.buttonNo")=="barButton" && component.get("v.resBarRule")){
               $j('#builder').queryBuilder('setRules',component.get("v.resBarRule"));  
        }
         if(component.get("v.buttonNo")=="pieButton" && component.get("v.resPieRule")){
               $j('#builder').queryBuilder('setRules',component.get("v.resPieRule"));  
        }
         
          
         },
  
    getQueryClicked : function(component, event, helper) {
    var $j = jQuery.noConflict();
    var resSql = $j('#builder').queryBuilder('getSQL');
    console.log(resSql); 
     console.log("CONDITION = ["+resSql['sql']+"]");
        var whereQuery = "CONDITION = ["+resSql['sql']+"]";
        var modalDiv = component.find("modalViewQuery");
        $A.util.removeClass(modalDiv,'slds-hide');
        component.set("v.whereQuery",whereQuery);
    },
    
    okClicked : function(component, event, helper) {
        var $j = jQuery.noConflict();
        var modalDiv = component.find("modalViewQuery");
        $A.util.addClass(modalDiv,'slds-hide'); 
        
     },
    
    //yesClicked : function(component, event, helper) {
    handleComponentEvent : function(component, event, helper) {
        console.log("handleComponentEvent catched");
        
        var modalDiv = component.find("modal2");
        $A.util.addClass(modalDiv,'slds-hide'); 
        
        var message = event.getParam("outputFilter");
        var wrapBlock = event.getParam("persistWrapBlock");
        var buttonClicked = component.get("v.buttonNo");
        console.log('--buttonClicked ' + buttonClicked);
        if(buttonClicked == "1" || buttonClicked == 1 || buttonClicked == '1'){
            console.log('--inside button clicked 1');
            component.set("v.persistBlock1", wrapBlock);
            component.set("v.query1", message);
        }
        else if(buttonClicked == "2" || buttonClicked == 2 || buttonClicked == '2'){
            console.log('--inside button clicked 2');
            component.set("v.persistBlock2", wrapBlock);
            component.set("v.query2", message);
        }
        else if(buttonClicked == "3" || buttonClicked == 3 || buttonClicked == '3'){
            console.log('--inside button clicked 3');
            component.set("v.persistBlock3", wrapBlock);
            component.set("v.query3", message);
        }
        else if(buttonClicked == "4" || buttonClicked == 4 || buttonClicked == '4'){
            console.log('--inside button clicked 4');
            component.set("v.persistBlock4", wrapBlock);
            component.set("v.query4", message);
        }
        else if(buttonClicked == "5" || buttonClicked == 5 || buttonClicked == '5'){
            console.log('--inside button clicked 5');
            component.set("v.persistBlock5", wrapBlock);
            component.set("v.query5", message);
        }
        else if(buttonClicked == "barButton"   || buttonClicked == 'barButton'){
            console.log('--inside button clicked barButton');
            component.set("v.persistBlockBar", wrapBlock);
            component.set("v.queryBar", message);
        }
        else if(buttonClicked == "pieButton" ||   buttonClicked == 'pieButton'){
            console.log('--inside button clicked pieButton');
            component.set("v.persistBlockPie", wrapBlock);
            component.set("v.queryPie", message);
        }
        
        console.log(JSON.parse(JSON.stringify(wrapBlock)));
        console.log("--Query", message);
        
        component.set("v.query",message);        
        component.getQueryParser();
    },
    
    QueryParser  : function(component, event, helper) {
        console.log("inside getQueryParser method");
         var instanceURL = component.get("v.instanceURL");
         var table =component.get("v.tableName");        
        var wherequery = '';
        var buttonClicked = component.get("v.buttonNo");

        var valValue;
        var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
        console.log('namespace val' +namespace);
        if(namespace != ''){
            
            valValue = $A.get("$Label.AxtriaSalesIQTM.Value");

         }else{
            valValue = $A.get("$Label.c.Value");
         }

        console.log('--buttonClicked ' + buttonClicked);

        if(buttonClicked == "1" || buttonClicked == 1 || buttonClicked == '1'){
            console.log('--inside button clicked 1');
            wherequery = component.get("v.query1");
        }
        else if(buttonClicked == "2" || buttonClicked == 2 || buttonClicked == '2'){
            console.log('--inside button clicked 2');
            wherequery = component.get("v.query2");
        }
        else if(buttonClicked == "3" || buttonClicked == 3 || buttonClicked == '3'){
            console.log('--inside button clicked 3');
            wherequery = component.get("v.query3");
        }
        else if(buttonClicked == "4" || buttonClicked == 4 || buttonClicked == '4'){
            console.log('--inside button clicked 4');
            wherequery = component.get("v.query4");
        }
        else if(buttonClicked == "5" || buttonClicked == 5 || buttonClicked == '5'){
            console.log('--inside button clicked 5');
            wherequery = component.get("v.query5");
        }
        else if(buttonClicked == "barButton" || buttonClicked == 'barButton'){
            console.log('--inside button clicked 5');
            wherequery = component.get("v.queryBar");
        }
        else if(buttonClicked == "pieButton" || buttonClicked == 'pieButton'){
            console.log('--inside button clicked 5');
            wherequery = component.get("v.queryPie");
        }

        console.log('--final query ', wherequery);

        component.set("v.wherequery",wherequery);

        if(component.get("v.buttonNo")=="1"){
             component.set("v.resControlRule",wherequery);  
        }
        else if(component.get("v.buttonNo")=="2"){
             component.set("v.resControlRule2",wherequery);  
        }
        else if(component.get("v.buttonNo")=="3"){
             component.set("v.resControlRule3",wherequery);  
        }
        else if(component.get("v.buttonNo")=="4"){
             component.set("v.resControlRule4",wherequery);  
        }
        else if(component.get("v.buttonNo")=="5"){
             component.set("v.resControlRule5",wherequery);  
        }
        else if(component.get("v.buttonNo")=="barButton"){
             component.set("v.resBarRule",wherequery);  
        }
        else if(component.get("v.buttonNo")=="pieButton"){
             component.set("v.resPieRule",wherequery);  
        }
                
        var reportURL = component.get("v.reportsUrl");
        var contextValue = component.get("v.contextName");
        var table =component.get("v.tableName"); 
        
        if(component.get("v.chartMode")=="Control" ){
            var cusField = component.get("v.fieldValue"); 
            var funcField = component.get("v.functionaValue"); 
            var action = component.get("c.getCalloutResponseforControlChart");
            
            

         if(wherequery!=null && wherequery!=""){
            action.setParams({
                "url": instanceURL,
                "tableName" : table,
                "fieldName" : cusField,
                "function" : funcField,
                "whereClause" : wherequery
            });
        }
        else{
               action.setParams({
             "url": instanceURL,
             "tableName" : table,
             "fieldName" : cusField,
             "function" : funcField,
             "whereClause" : '1=1'
        }); 
       }
            
            /*    
            var res = reportURL+'preview/?tableName='+table+'&context='+contextValue+'&fieldName='+cusField+'&function='+funcField+'&condition=';
            console.log('strUrl->'+res);

            if(wherequery != '' && wherequery != null && wherequery != '1=1'){
                action.setParams({
                    "url": res,
                    "query" : wherequery
                });
            }
            else{
                action.setParams({
                    "url": res,
                    "query" : '1=1'
                }); 
            }
            */
                
            action.setCallback(this,function(response){
                
                var state = response.getState();
                if(component.isValid() && state==="SUCCESS"){

                   //
                    var actionNumberFormat = component.get("c.getformatedNumbers");
                    actionNumberFormat.setParams({
                    "numberVal": response.getReturnValue()
                });
                actionNumberFormat.setCallback(this,function(response)
                {
                    var state = response.getState();
                    if(state === 'SUCCESS' && response.getReturnValue != null)
                    {
                         
                        var arrHtml=[];
                        var state = response.getState();
                        var rowCount; 
                        var result1 = response.getReturnValue();
                        console.log(result1);
                        // for(var outer = 0; outer < result.length; outer++){
                        //    for(var inner = 0; inner < timestamp_columns_vals.length; inner++){
                        //         for(var counter= 0 ; counter<result1.length ; counter++ ){
                        //            result[outer][timestamp_columns_vals[inner]] = result1[counter];
                        //         }
                        //     }
                        // }

                        console.log('RESPONSE'+response.getReturnValue());
                        var valueOutputavg='';
                        if(component.get("v.buttonNo")=="1"){
                           var valueOutput = response.getReturnValue();
                           component.set("v.outputValue",valueOutput);
                        }
                        else if(component.get("v.buttonNo")=="2"){
                            var valueOutput = response.getReturnValue();
                            component.set("v.outputValue2",valueOutput);
                        }
                        else if(component.get("v.buttonNo")=="3"){
                           var valueOutput = response.getReturnValue();
                           component.set("v.outputValue3",valueOutput);
                        }
                        else if(component.get("v.buttonNo")=="4"){
                              var valueOutput = response.getReturnValue();
                              component.set("v.outputValue4",valueOutput);
                        } 
                        else {
                            var valueOutput = response.getReturnValue();
                              component.set("v.outputValue5",valueOutput);
                        }

                            }
                        });
                                $A.enqueueAction(actionNumberFormat);
                   }else{
                      console.log("ERROR");
                      if(component.get("v.buttonNo")=="1" || param3=="clickedbutton1"){
                           component.set("v.outputValue",valValue);
                        }
                        else if(component.get("v.buttonNo")=="2" || param3=="clickedbutton2"){
                           component.set("v.outputValue2",valValue);
                        }
                        else if(component.get("v.buttonNo")=="3" || param3=="clickedbutton3"){
                           component.set("v.outputValue3",valValue);
                        }
                        else if(component.get("v.buttonNo")=="4" || param3=="clickedbutton4"){
                           component.set("v.outputValue4",valValue);
                        }
                        else if(component.get("v.buttonNo")=="5" || param3=="clickedbutton5"){
                           component.set("v.outputValue5",valValue);
                        }
             }


                  /* console.log(response.getReturnValue());
                        
                    if(component.get("v.buttonNo")=="1"){
                        var valueOutput = Number(response.getReturnValue());
                        if(valueOutput % 1 != 0){
                            var valueOutputavg = valueOutput.toFixed(4);
                        }else{
                            var valueOutputavg = valueOutput;
                        } 
                       component.set("v.outputValue",valueOutputavg);
                    }
                    else if(component.get("v.buttonNo")=="2"){
                        var valueOutput = Number(response.getReturnValue());
                        if(valueOutput % 1 != 0){
                            var valueOutputavg = valueOutput.toFixed(4);
                        }else{
                            var valueOutputavg = valueOutput;
                        } 
                        component.set("v.outputValue2",valueOutputavg);
                    }
                    else if(component.get("v.buttonNo")=="3"){
                        var valueOutput = Number(response.getReturnValue());
                        if(valueOutput % 1 != 0){
                           var valueOutputavg = valueOutput.toFixed(4);
                        }else{
                           var valueOutputavg = valueOutput;
                        } 
                        component.set("v.outputValue3",valueOutputavg);
                    }else if(component.get("v.buttonNo")=="4"){
                        var valueOutput = Number(response.getReturnValue());
                        if(valueOutput % 1 != 0){
                           var valueOutputavg = valueOutput.toFixed(4);
                        }else{
                           var valueOutputavg = valueOutput;
                        } 
                        component.set("v.outputValue4",valueOutputavg);
                    }
                    else{
                        var valueOutput = Number(response.getReturnValue());
                        if(valueOutput % 1 != 0){
                            var valueOutputavg = valueOutput.toFixed(4);
                        }else{
                            var valueOutputavg = valueOutput;
                        } 
                        component.set("v.outputValue5",valueOutputavg);
                    }                  
                }
                else{
                   console.log("Failed with state:  "+ state); 
                }*/
            });
            $A.enqueueAction(action);   
        }

        if(component.get("v.chartMode") =="Bar"){
            component.onBarFuncChange();
        }
        if(component.get("v.chartMode") =="Pie"){
            component.onPieFuncChange();
        }
    },
    
    cancelClicked : function(component, event, helper) {
        var $j = jQuery.noConflict();
        var modalDiv = component.find("modal2");
        $A.util.addClass(modalDiv,'slds-hide'); 

        if(component.get("v.notValidQuery") == true){
            var buttonClicked = component.get("v.buttonNo");
            console.log('--buttonClicked ' + buttonClicked);
            if(buttonClicked == "1" || buttonClicked == 1 || buttonClicked == '1'){
                console.log('--inside button clicked 1');
                component.set("v.persistBlock1", "[]");
                component.set("v.query1", '1=1');
            }
            else if(buttonClicked == "2" || buttonClicked == 2 || buttonClicked == '2'){
                console.log('--inside button clicked 2');
                component.set("v.persistBlock2", "[]");
                component.set("v.query2", '1=1');
            }
            else if(buttonClicked == "3" || buttonClicked == 3 || buttonClicked == '3'){
                console.log('--inside button clicked 2');
                component.set("v.persistBlock3", "[]");
                component.set("v.query3", '1=1');
            }
            else if(buttonClicked == "4" || buttonClicked == 4 || buttonClicked == '4'){
                console.log('--inside button clicked 2');
                component.set("v.persistBlock4", "[]");
                component.set("v.query4", '1=1');
            }
            else if(buttonClicked == "5" || buttonClicked == 5 || buttonClicked == '5'){
                console.log('--inside button clicked 2');
                component.set("v.persistBlock5", "[]");
                component.set("v.query5", '1=1');
            }
            else if(buttonClicked == "barButton"   || buttonClicked == 'barButton'){
                console.log('--inside button clicked 2');
                component.set("v.persistBlockBar", "[]");
                component.set("v.queryBar", '1=1');
            }
            else if(buttonClicked == "pieButton" ||   buttonClicked == 'pieButton'){
                console.log('--inside button clicked 2');
                component.set("v.persistBlockPie", "[]");
                component.set("v.queryPie", '1=1');
            }

            component.getQueryParser();
        }
        //$j('#builder').queryBuilder('reset');
     },
    
    UnderlyingDataTab : function(component, event, helper) {
        component.showSpinner();
        helper.showExportButton(component, event, helper);
         
         
         

        if(component.get('v.scenarioStatus') != 'SUCCESS'){
            helper.hideExportAllButton(component, event, helper);
        }else{
            helper.showExportAllButton(component, event, helper);
        }

        
        var tab1 = component.find('ReportId');
        var TabOnedata = component.find('ReportTabId');
        
        var tab2 = component.find('UnderlyingDataId');
        var TabTwoData = component.find('UnderlyingTabId');
        //console.log('TabTwoData'+TabTwoData);
        var tab3 = component.find('MetaDataInfoId');
        var TabThreeData = component.find('MetaDataTabId');
        
        $A.util.addClass(tab2, 'slds-is-active');
        $A.util.removeClass(TabTwoData, 'slds-hide');
        $A.util.addClass(TabTwoData, 'slds-show');
        // Hide and deactivate others tab
        $A.util.removeClass(tab1, 'slds-is-active');
        $A.util.removeClass(TabOnedata, 'slds-show');
        $A.util.addClass(TabOnedata, 'slds-hide');
        
        $A.util.removeClass(tab3, 'slds-is-active');
       // $A.util.removeClass(TabThreeData, 'slds-show');
        $A.util.addClass(TabThreeData, 'slds-hide');
        component.set("v.tabNameisUnderlyingTab",true);
        component.createDataTable();
        //component.hideSpinner();
        
        
    },
    MetaDataTab : function(component, event, helper) {
        helper.hideExportButton(component, event, helper);
        helper.hideExportAllButton(component, event, helper);

        var tab1 = component.find('ReportId');
        var TabOnedata = component.find('ReportTabId');
        
        var tab2 = component.find('UnderlyingDataId');
        var TabTwoData = component.find('UnderlyingTabId');
        //console.log('TabTwoData'+TabTwoData);
        var tab3 = component.find('MetaDataInfoId');
        var TabThreeData = component.find('MetaDataTabId');
        
        //show and Active color Tab
        $A.util.addClass(tab3, 'slds-is-active');
        $A.util.removeClass(TabThreeData, 'slds-hide');
        //$A.util.addClass(TabThreeData, 'slds-show');
        // Hide and deactivate others tab
        $A.util.removeClass(tab1, 'slds-is-active');
        $A.util.removeClass(TabOnedata, 'slds-show');
        $A.util.addClass(TabOnedata, 'slds-hide');
        
        $A.util.removeClass(tab2, 'slds-is-active');
        $A.util.removeClass(TabTwoData, 'slds-show');
        $A.util.addClass(TabTwoData, 'slds-hide');
       
        //component.metaDataInformation();
        //component.createMetaDataTable();
        
        
    },
    
    // ## function call on Click on the "Download As CSV" Button. 
    downloadExcel : function(component, event, helper)
    {
        var $j = jQuery.noConflict();
        //COMPONENT ERROR FIX
        var sessionTable = $j("[class*='UnderlyingTable']").DataTable();
        //var sessionTable = $j('#table-1').DataTable();
        sessionTable.buttons('.buttons-csv').trigger(); 
    },
    
    //Show Column details in Metadata Info Tab 
    /* columnDetails : function(component, event, helper)
    {
        var colaction = component.get("c.getColumnInformation");
        colaction.setCallback(this, function(response) {
                var state = response.getState();
                if(state == "SUCCESS"){
                    var result = response.getReturnValue();
                    if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                        component.set("v.columnDetail",response.getReturnValue());
                    }
                }else if(state == "ERROR"){
                    console.log('Error in calling server side action');
                }
            });
        $A.enqueueAction(colaction);
    },*/
    
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    
    
    closeButton : function(component, event, helper) {
        component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
        var $j = jQuery.noConflict();
        var sceId=component.get("v.scenarioId"); 
        var action = component.get("c.getScenarioID");
        action.setParams({ "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
        
        action.setCallback(this, function(response){ 
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS"){
                var table=component.set("v.sessionTable");
                //var table = $j('#table-1').DataTable();
                if(table){
                    table.destroy();
                }
                
                var arrListValues=response.getReturnValue();
                console.log(common.getKey(component,"Scenario_Id__c"));
                console.log('record_id :::: '+arrListValues[0][common.getKey(component,"Scenario_Id__c")]);
                
                var evt = $A.get("e.force:navigateToComponent");
                evt.setParams({
                    componentDef : "c:CPGbusinessRuleCanvas",
                    componentAttributes: {
                        record_Id : arrListValues[0][common.getKey(component,"Scenario_Id__c")],
                        scenarioId : arrListValues[0][common.getKey(component,"Scenario_Id__c")],
                        viewcanvas : "Y",
                        workSpaceId : component.get("v.workSpaceId"),
                        namespace:component.get("v.namespace"),
                        summarizedViewFlag : component.get("v.summarizedViewFlag")
                    },
                    isredirect : true
                });
                evt.fire();
                component.destroy();
                
            }
            else{
                console.log("Failed with state:  "+ state);
            }
        })
        $A.enqueueAction(action); 
    },
    
    destoryCmp : function (component, event, helper) {
        component.destroy();
    },
    
    metaDataInformation : function (component, event, helper) {
        console.log('metaDataInformation');
        console.log(component.get('v.componentLabel'));
        var metaDataAction = component.get("c.getDataForUnderLyingData");
        metaDataAction.setParams({ "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
        metaDataAction.setCallback(this, function(response) {
            var state = response.getState();
            if(state == "SUCCESS"){
                var metaDataActionResult = response.getReturnValue();
                if(!$A.util.isEmpty(metaDataActionResult) && !$A.util.isUndefined(metaDataActionResult)){
                    component.set("v.dataSourceDetails",response.getReturnValue());
                    var colaction = component.get("c.GetSourceAndDerivedFields");
                    colaction.setParams({ "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
                    colaction.setCallback(this, function(response) {
                        var state = response.getState();
                        if(state == "SUCCESS"){
                            var result = response.getReturnValue();
                            if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                                console.log("preview Values===>");
                                console.log(response.getReturnValue());
                                component.set("v.columnDetail",response.getReturnValue());
                                console.log("Size+++"+response.getReturnValue().length);
                                
                                component.callNumerFormatter(response.getReturnValue().length, 'fieldCount');
                                
                            }
                            var actionTabName = component.get("c.getTableName");
                            actionTabName.setParams({ "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
                            actionTabName.setCallback(this, function(response){ 
                                var stateTabName = response.getState();

                                if(stateTabName==="SUCCESS")
                                {
                                    var result = response.getReturnValue();
                                    var tableAttr = result.split(',');
                                    var tableName = tableAttr[0];
                                    component.set("v.tableName",tableName);
                                    component.set("v.tableDisplayName",tableAttr[1]);
                                }
                                else{
                                    console.log("Failed with state:  "+ state);
                                }
                            });
                            $A.enqueueAction(actionTabName);
                            
                        }else if(state == "ERROR"){
                            console.log('Error in calling server side action');
                        }
                    });
                    $A.enqueueAction(colaction);
                }
            }else if(state == "ERROR"){
                console.log('Error in calling server side action');
            }
            component.hideSpinner();
        });
        $A.enqueueAction(metaDataAction);
    },
     
     downloadPreviewFiles: function (component,event,helper){
        
        console.log('inside downloadPreviewFiles');
        
        //var componentLabel = component.get("v.componentLabel"); 

        var tablename = component.get("v.tableName");
        var folderName = 'Scenario_Input';
        var flName = tablename.substr(2,tablename.length)+'.csv';
       
        /*if(componentLabel == 'Customer Universe Starting Point')
        {
            var folderName = 'Scenario_Input';
            var filename = 'call_plan_univ_';
            var scenStatus=component.get("v.scenarioStatus");   
        }
        
        else if(componentLabel == 'Final Call Plan')
        {
            var folderName = 'Scenario_Input';
            var filename = 'final_call_plan_';
        }
          else if(componentLabel == 'Call Plan Summary')
            {
                var folderName = 'Scenario_Input';
                var filename = 'call_plan_summ_';
            }
        else if(componentLabel == 'Account Master')
            {
                var folderName = 'Scenario_Input';
                var filename = 'accnt_mstr_';
            }
        else if(componentLabel == 'Eligible Account Master')
            {
                var folderName = 'Scenario_Input';
                var filename = 'elgbl_accnt_mstr_';
            }
        else if(componentLabel == 'ZIP-Terr')
            {
                var folderName = 'Scenario_Input';
                var filename = 'zip_terr_';
            }
        else if(componentLabel == 'Account Alignment')
            {
                var folderName = 'Scenario_Input';
                var filename = 'accnt_algnmnt_';
            }
        else if(componentLabel == 'Alignment Summary')
            {
                var folderName = 'Scenario_Input';
                var filename = 'algnmnt_smmry_';
            }
        
        var scenarioName = component.get('v.scenarioName');
        var scenarioNameLower = scenarioName.toLowerCase();
        var scenarioNameModified = scenarioNameLower.replace("-", "_")+'.csv';
        console.log('scenarioNameModified::: '+scenarioNameModified);
        var finalFileName='';
        if(folderName!="Input_Files"){
            finalFileName=filename+scenarioNameModified;
        } 
        else{
            finalFileName=filename;
        }*/
         var exportAllURL = component.get("v.exporlAllUrl");  
         console.log('exportAllURL'+exportAllURL);
         //var table =component.get("v.tableName"); 
        var action = component.get('c.getexportAlldetails');
         action.setParams({
            "url": exportAllURL,
            "tableName": tablename 
        });
        action.setCallback(this,function(response)
                           {
                               var state = response.getState();
                               
                               if(state === 'SUCCESS' && response.getReturnValue() != null)
                               {
                                   var cs=response.getReturnValue();
                                   console.log('cs value ::: '+cs);
                                   console.log(cs);
                                  
                                  // var url = cs[common.getKey(component,"bucketurl__c")]+'/'+cs[common.getKey(component,"foldername__c")]+'/'+folderName+'/'+flName;
                                  //var url = cs[0]+'/'+cs[1]+'/'+folderName+'/'+flName;
                                  
                                  // console.log('url :::: '+url);
                                   window.open(cs, '_self',false);
                               }else{
                                   console.log("error on getBucket details");
                               }
                           });
        
        $A.enqueueAction(action);
        
        
    },

    submitQuery : function(component, event, helper) {
        var childComponent = component.find("childFilter");
        childComponent.getQuery();
    },

    updateFilterWrapper : function(component, event, helper) {
        var updatedWrapper = event.getParam("parsedJson");
        var whereClause = event.getParam("whereClause");
        var isValid = event.getParam("isValid");
        var errMsg = event.getParam("errorMsg");
        var stagingClause = event.getParam("stagingWhereClause");
        var parsedQuery = component.get("v.parsedQuery", '');
        
        console.log(updatedWrapper);
        console.log(isValid);
        console.log(stagingClause);

        //if(isValid == 'true' || !isValid){
            
            var childComponent = component.find("childFilter");
            $A.util.addClass(childComponent,'slds-hide');
            var modalDiv = component.find("modal2");
            $A.util.addClass(modalDiv,'slds-hide');
        //}
        // else{
        //     component.set('v.popoverMessage1',errMsg);
        //     var errorIcn = component.find("errorIcon1");
        //     $A.util.removeClass(errorIcn,'slds-hide');
        //     return;
        // }

        var splitStr = stagingClause.split('\'');
        var finalString = '';
        for(var ind=0; ind<splitStr.length; ind++){
            console.log('--inside loop ', splitStr[ind]);
            console.log('--intermediate string ' + finalString);
            finalString = finalString + splitStr[ind] + "\'\'";
        }
        finalString = finalString.slice(0,-2);

        console.log(component.get("v.buttonNo"));
        if(component.get("v.buttonNo")=='1'){
            component.set("v.query1",finalString);
            component.set("v.updatedWrapperButton1",updatedWrapper);
            component.set("v.parsedQuery1",whereClause);
            component.set("v.errMsg1",errMsg);
            component.getQueryParser();
        }
        else if(component.get("v.buttonNo")=='2'){
            component.set("v.query2",finalString);
            component.set("v.updatedWrapperButton2",updatedWrapper);
            component.set("v.parsedQuery2",whereClause);
            component.set("v.errMsg2",errMsg);
            component.getQueryParser();
        }
        else if(component.get("v.buttonNo")=='3'){
            component.set("v.query3",finalString);
            component.set("v.updatedWrapperButton3",updatedWrapper);
            component.set("v.parsedQuery3",whereClause);
            component.set("v.errMsg3",errMsg);
            component.getQueryParser();
        }else if(component.get("v.buttonNo")=='4'){
            component.set("v.query4",finalString);
            component.set("v.updatedWrapperButton4",updatedWrapper);
            component.set("v.parsedQuery4",whereClause);
            component.set("v.errMsg4",errMsg);
            component.getQueryParser();
        }else if(component.get("v.buttonNo")=='5'){
            component.set("v.query5",finalString);
            component.set("v.updatedWrapperButton5",updatedWrapper);
            component.set("v.parsedQuery5",whereClause);
            component.set("v.errMsg5",errMsg);
            component.getQueryParser();
        }else if(component.get("v.buttonNo")=='barButton'){
            component.set("v.queryBar",finalString);
            component.set("v.updatedWrapperButtonBar",updatedWrapper);
            component.set("v.parsedQueryBar",whereClause);
            component.set("v.errMsgBar",errMsg);
            component.getQueryParser();
        }else if(component.get("v.buttonNo")=='pieButton'){
            component.set("v.queryPie",finalString);
            component.set("v.updatedWrapperButtonPie",updatedWrapper);
            component.set("v.parsedQueryPie",whereClause);
            component.set("v.errMsgPie",errMsg);
            component.getQueryParser();
       // }
         //component.hideSpinner();
    }
       //component.hideSpinner();

    },

    handleMouseEnter1 : function(component, event, helper) {
        var popover = component.find("popover1");
        $A.util.removeClass(popover,'slds-hide');
    },
       //make a mouse leave handler here
    handleMouseLeave1 : function(component, event, helper) {
        var popover = component.find("popover1");
        $A.util.addClass(popover,'slds-hide');
    },

    closePicklistDropdown : function(component, event, helper) {
        console.log('close filter criteria');
        var ev = event.target;
        var cls = ev.classList[0];
        if(!String(cls).includes('slds-m-right_x-small') &&  !String(cls).includes('slds-input')){
            $A.get("e.c:modalCloseEvent").fire();  
        } 
      }

    
})