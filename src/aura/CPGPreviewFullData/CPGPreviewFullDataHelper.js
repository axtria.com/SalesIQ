({

    hideExportButton :  function (component,event,helper){
        var cmpBtn = component.find("downloadButton");
        $A.util.addClass(cmpBtn, "slds-hide");
    },

    showExportButton :  function (component,event,helper){
        var cmpBtn = component.find("downloadButton");
        $A.util.removeClass(cmpBtn, "slds-hide");
    },

    hideExportAllButton :  function (component,event,helper){
        var cmpBtn = component.find("exportAllBtn");
        $A.util.addClass(cmpBtn, "slds-hide");
    },

    showExportAllButton :  function (component,event,helper){
        var cmpBtn = component.find("exportAllBtn");
        $A.util.removeClass(cmpBtn, "slds-hide");
    },

    doValidation:function(component, event, helper){     
        var errMessage=component.get("v.errMessage");
        console.log('errMessage'+errMessage);
        var isValid = component.get("v.isValid");
        console.log('isValid'+isValid);
        console.log("errorMessage",errMessage );
        if(!isValid || isValid=='false'){
            component.set('v.popoverMessage1',errMessage);
            var errorIcn = component.find("errorIcon1");
            $A.util.removeClass(errorIcn,'slds-hide');
        }else{
            $A.util.addClass(errorIcn,'slds-hide');
        }
    },

    drawGroupedBarChart : function(chart_json, summarizeByKey, groupedByKey){
        var data = chart_json;

        var margin ={top: 20, right: 20, bottom: 30, left: 60},
        width = 900 - margin.left - margin.right,
        height = 300 - margin.top - margin.bottom;

        var x0 = d3.scaleBand()
        .rangeRound([0, width])
        .paddingInner(0.1);

        var x1 = d3.scaleBand()
        .padding(0.05);

        var y = d3.scaleLinear()
        .rangeRound([height, 0]);

        var color = d3.scaleOrdinal(d3.schemeCategory20c);


        d3.select('#siq_bar_chart').html("");
        d3.select('#siq_legend').html("");
        d3.select('#siq_pie_chart').html("");
        d3.select('#siq_pie_legend').html("");


        d3.selectAll('#siq_bar_chart').classed("slds-hide", false);
        d3.selectAll('#siq_legend').classed("slds-hide", false);


        var svg = d3.select('#siq_bar_chart')
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        
        var nested_data = d3.nest()
        .key(function(d) { return d[groupedByKey]; })
        .entries(data);

        var nested_data_sum = d3.nest()
        .key(function(d) { return d[groupedByKey]; })
        .rollup(function(leaves) { return  d3.sum(leaves, function(d) {
            if (d.key == 0 || d.key == "0"){
                return;
            }
            else if(d.sum == "undefined") 
                return "Others";
            else 
                return d.sum;
        })})
        .entries(data);

        console.log('nested_data');
        console.log(nested_data);
        console.log(nested_data_sum);


        var tooltip = d3.select('#siq_bar_chart') 
        .append('div')                                
        .attr('class', 'tooltip');

        tooltip.append('div')                         
        .attr('class', 'label_tooltip');                   

        tooltip.append('div')                  
        .attr('class', 'count'); 
        tooltip.append('div') 
        .attr('class', 'percent'); 

        var grouped_keys = data.map( d =>  d[summarizeByKey] );

       

        var categoriesNames = nested_data.map( function(d) { 
            if(d.key == "undefined")
                return "Others";
            else
                return d.key; 
        });

        console.log(' ========categoriesNames==========');
        console.log(categoriesNames);

        var categorySet = new Set();

        if(categoriesNames!= undefined && categoriesNames.length > 0){
            categorySet =  new Set(categoriesNames);
        }


        var rateNames = nested_data[0].values.map(function(d) { return d[summarizeByKey]; });

        x0.domain(categoriesNames);
        x1.domain(grouped_keys).rangeRound([0, x0.bandwidth()]);

        console.log(' MAX VAL :'+d3.max(data, function(d) { return +d.sum}) );

        var max_val = d3.max(data, function(d) { return +d.sum});
        if (max_val > 0 ){
            //draw graph
             y.domain([0, d3.max(data, function(d) { return +d.sum}) ]).nice();


        var slice = svg.selectAll(".slice")
        .data(nested_data)
        .enter().append("g")
        .attr("class", "g")
        .attr("transform",function(d) { return "translate(" + x0(d.key) + ",0)"; });

        slice.selectAll("rect")
        .data(function(d) { return d.values; })
        .enter().append("rect")
        .attr("width", x1.bandwidth())
        .attr("x", function(d) { return x1(d[summarizeByKey]); })
        .style("fill", function(d) { return color(d[groupedByKey]+' : '+d[summarizeByKey] + ' : ' + d.sum) })
        .attr("y", function(d) { return y(0); })
        .attr("height", function(d) { return height - y(0); })
        .on("mouseover", function(d) {
            
            d3.select(this).style("fill", d3.rgb(color(d[groupedByKey]+' : '+d[summarizeByKey] + ' : ' + d.sum)).darker(2));

            tooltip.select('.label_tooltip').html(d[summarizeByKey]); 
            var val =  parseFloat(d.sum).toFixed(2);            
            tooltip.select('.count').html('' + val); 

            tooltip.style('display', 'block'); 

            var coords = d3.mouse(this);
            console.log(coords);

            var h =  d3.select('#siq_bar_container_div').node().getBoundingClientRect().height - 10;

            tooltip.style('top', (coords[1]+h) + 'px'); 
            tooltip.style('left', d3.event.pageX -100 + 'px'); 


        })
        .on("mouseout", function(d) {
          d3.select(this).style("fill",color(d[groupedByKey]+' : '+d[summarizeByKey] + ' : ' + d.sum));
          tooltip.style('display', 'none');

      })
        .on("mousemove", function(d) {
            d3.select(this).style("fill", d3.rgb(color(d[groupedByKey]+' : '+d[summarizeByKey] + ' : ' + d.sum)).darker(2));
            tooltip.select('.label_tooltip').html(d[summarizeByKey]); 
            var val = parseFloat(d.sum).toFixed(2);          
            tooltip.select('.count').html('' + val); 

            tooltip.style('display', 'block'); 

            var coords = d3.mouse(this);

            var h =  d3.select('#siq_bar_container_div').node().getBoundingClientRect().height - 10;

            tooltip.style('top', (coords[1]+h) + 'px'); 
            tooltip.style('left', d3.event.pageX -100 + 'px');  


        });



        slice.selectAll("rect")
        .transition()
        .delay(function (d) {return Math.random()*1000;})
        .duration(1000)
        .attr("y", function(d) { return y(d.sum); })
        .attr("height", function(d) { return height - y(d.sum); });


        svg.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x0))
        .selectAll(".tick line").attr("stroke", "#fff");



        svg.append("g")
        .attr("class", "axis")
        .call(d3.axisLeft(y).ticks(null, "s"))
        .append("text")
        .attr("x", 2)
        .attr("y", y(y.ticks().pop()) + 0.5)
        .attr("dy", "0.32em")
        .attr("fill", "#000")
        .attr("font-weight", "bold")
        .attr("text-anchor", "start")
        .text("Sum");




                var legendRectSize = 15; // defines the size of the colored squares in legend
                var legendSpacing = 3; 

                var legend_data = color.domain();
                if(nested_data_sum != undefined ){
                    //legend_data = nested_data_sum.concat(color.domain());
                }
                
                var final_ = new Array();
            
                nested_data_sum.forEach( function(element){
                    final_.push(element);
                    legend_data.forEach( function(ele){
                        var gp = ele.split(' : ')[0];
                        if(gp == element.key ){
                            final_.push(ele);
                        }
                    }) 
                    
                });
               
                console.log('final_');
                console.log(final_);


                var legend2 = d3.select('#siq_legend') 
                .append('svg') 
                .attr('height', 18 * final_.length ) 
                .append('g')
                .attr('class','siq_legend')
                .attr('height',height -10)
                .attr('overflow','visible')
                .selectAll('.legend')
                .data(final_)
                .enter() 
                .append('g') 
                .attr('class', 'legend')
                .attr('transform', function(d, i) {                   
                    var height = legendRectSize + legendSpacing;
                    var offset =  height * color.domain().length / 2;
                    var horz = 0; 
                    var vert = i * height + 2; 
                    if(d.value ==  undefined ) 
                        horz = 20; 

                    return 'translate(' + horz + ',' + vert + ')';
                });

        // adding colored squares to legend

        try{
            legend2.append('rect')                                 
            .attr('width', legendRectSize)                    
            .attr('height', legendRectSize)                    
            .style('fill', function(d){return d3.rgb(color(d)) }) 
            .style('stroke', function(d){return d3.rgb(color(d)) }) 
            .on('click', function(d) {  })
            .on("mouseover", function(d) {
                tooltip.style('display', 'block'); 
                tooltip.style('top', d3.event.pageY - 215 + 'px'); 
                tooltip.style('left', d3.event.pageX - 65 + 'px'); 

                if(d.split != undefined){
                    tooltip.select('.label_tooltip').html(d.split(':')[1]); 
                    var str = d.split(':')[2]; 
                    var val =  parseFloat(str.trim()).toFixed(2);            
                    tooltip.select('.count').html(val);
                }else{
                    tooltip.select('.label_tooltip').html(d.key);
                    var val =  parseFloat(d.value).toFixed(2);             
                    tooltip.select('.count').html(val);
                }
                
            })
            .on("mouseout", function(d) { 
                tooltip.style('display', 'none');
            });

            legend2.append('text')                                    
            .attr('x', legendRectSize + legendSpacing)
            .attr('y', legendRectSize - legendSpacing)
            .text(function(d) { 
                var label = '';
                if(categorySet.has(d.key))
                    label = d.key + ' : ' + d.value
                else
                    label = d.split(' : ')[1]+ ' : ' + d.split(' : ')[2];

      
                var arr = label.split(':');
                var str = arr[0];
                if(str.length > 10){
                    str = str.substring(0,8) + '...';
                }
                
                var val = parseFloat(arr[1].trim()).toFixed(2);
                return str + ' : ' + val;

            }); 

        }catch(EXP){
            console.log(EXP)
        }

        

        

        }else{
            this.clearChartsDiv();
        }

    },

drawBarChart : function(chart_json, summarizeByKey, groupedByKey){
        //let limit_x_axis = 40;
        //var data = chart_json.slice(0,limit_x_axis);
        var data = chart_json;
        console.log(data);

        //DEFINE CHART MARGINS
        var margin ={top: 20, right: 20, bottom: 30, left: 60},
        width = 900 - margin.left - margin.right,
        height = 300 - margin.top - margin.bottom;


        //DEFINE X AXIS SCALE TYPE : DISCRETE
        var x = d3.scaleBand()
        .rangeRound([0, width])
        .paddingInner(0.1);

        //DEFINE Y AXIS SCALE TYPE : LINEAR
        var y = d3.scaleLinear()
        .rangeRound([height, 0]);



        //DEFINE COLOR SCALE TYPE 
        var color = d3.scaleOrdinal(d3.schemeCategory20c);

        
        //ADD D3 CHART SVG TO CHART DIV

        d3.select('#siq_bar_chart').html("");
        d3.select('#siq_legend').html("");
        d3.select('#siq_pie_chart').html("");
        d3.select('#siq_pie_legend').html("");

        d3.selectAll('#siq_bar_chart').classed("slds-hide", false);
        d3.selectAll('#siq_legend').classed("slds-hide", false);

        var svg = d3.select('#siq_bar_chart')
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var tooltip = d3.select('#siq_bar_chart') 
        .append('div')                                   
        .attr('class', 'tooltip'); 

        tooltip.append('div')                           
        .attr('class', 'label_tooltip');                   
        tooltip.append('div')                  
        .attr('class', 'count'); 
        tooltip.append('div') 
        .attr('class', 'percent'); 



        var specialities = data.map(function(d) { return d[summarizeByKey]; });
        x.domain(specialities);

        var data_max_val = d3.max(data, function(d) { return +d.sum});

        if ( data_max_val == 0 || data_max_val == undefined ){
            
            console.log('clear charts');
            d3.selectAll('#siq_bar_chart').html("");
            d3.selectAll('#siq_legend').html("");
            d3.selectAll('#siq_pie_chart').html("");
            d3.selectAll('#siq_pie_legend').html("");


            d3.selectAll('#siq_bar_chart').classed("slds-hide", true);
            d3.selectAll('#siq_legend').classed("slds-hide", true);
            d3.selectAll('#siq_pie_chart').classed("slds-hide", true);
            d3.selectAll('#siq_pie_legend').classed("slds-hide", true);

            d3.selectAll('#NodataMsg').classed("slds-hide", false);


            return;
        }else{
             d3.selectAll('#NodataMsg').classed("slds-hide", true);
        }

        

        y.domain([0, d3.max(data, function(d) { return +d.sum}) ]).nice();



       // append the rectangles for the bar chart
       svg.selectAll(".bar")
       .data(data)
       .enter().append("rect")
       .attr("class", "bar")
       .attr("x", function(d) { return x(d[summarizeByKey]); })
       .style("fill", function(d) { return color(d[summarizeByKey] + ' : ' + d.sum) })
       .attr("width", x.bandwidth())
       .attr("z-index", 99999)
       .attr("y", function(d) { return y(d.sum); })
       .attr("height", function(d) { return height - y(d.sum); })
       .on("mouseover", function(d) {
        d3.select(this).style("fill", d3.rgb(color(d[summarizeByKey] + ' : ' + d.sum)).darker(2));
        tooltip.select('.label_tooltip').html(d[summarizeByKey]);           
        var val = parseFloat(d.sum).toFixed(2);        
        tooltip.select('.count').html('' + val); 

        tooltip.style('display', 'block'); 

        var coords = d3.mouse(this);

        var h =  d3.select('#siq_bar_container_div').node().getBoundingClientRect().height - 10;

        tooltip.style('top', coords[1]+h + 'px'); 
        tooltip.style('left', coords[0] + 'px'); 


    })
       .on("mouseout", function(d) { 
        d3.select(this).style("fill", color(d[summarizeByKey] + ' : ' + d.sum));
        tooltip.style('display', 'none');
    })
       .on("mousemove", function(d) {
        d3.select(this).style("fill", d3.rgb(color(d[summarizeByKey] + ' : ' + d.sum)).darker(2));
        tooltip.select('.label_tooltip').html(d[summarizeByKey]);           
        var val =  parseFloat(d.sum).toFixed(2);  
                       
        tooltip.select('.count').html('' + val); 

        tooltip.style('display', 'block'); 

        var coords = d3.mouse(this);

        var h =  d3.select('#siq_bar_container_div').node().getBoundingClientRect().height - 10;

        tooltip.style('top', coords[1]+h + 'px'); 
        tooltip.style('left', coords[0] + 'px'); 
    });



       if(data.length > 8){
        svg.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x).tickValues([]))

    }else{
        svg.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));
    }


    svg.append("g")
    .attr("class", "axis")
    .call(d3.axisLeft(y).ticks(null, "s"))
    .append("text")
    .attr("x", 2)
    .attr("y", y(y.ticks().pop()) + 0.5)
    .attr("dy", "0.32em")
    .attr("fill", "#000")
    .attr("font-weight", "bold")
    .attr("text-anchor", "start")
    .text("Sum");


                var legendRectSize = 15; // defines the size of the colored squares in legend
                var legendSpacing = 3; 


                var legend2 = d3.select('#siq_legend') 
                .append('svg') 
                .attr('height', 18 * data.length ) 
                .append('g')
                .attr('class','siq_legend')
                .attr('height',height -10)
                .attr('overflow','visible')
                .selectAll('.legend')
                .data(color.domain())
                .enter() 
                .append('g') 
                .attr('class', 'legend')
                .attr('transform', function(d, i) {                   
                    var height = legendRectSize + legendSpacing;
                    var offset =  height * color.domain().length / 2;
                    var horz = 0; 
                    var vert = i * height;           
                    return 'translate(' + horz + ',' + vert + ')';
                });

        // adding colored squares to legend
        legend2.append('rect')                                 
        .attr('width', legendRectSize)                    
        .attr('height', legendRectSize)                    
        .style('fill', color) 
        .style('stroke', color) 
        .on("mouseover", function(d) {

            tooltip.select('.label_tooltip').html(d.split(':')[0]);   
            var str = d.split(':')[1];
            var val = parseFloat(str.trim()).toFixed(2); 
            tooltip.select('.count').html(val);            
            tooltip.style('display', 'block'); 
            tooltip.style('top', d3.event.pageY - 215 + 'px'); 
            tooltip.style('left', d3.event.pageX - 58 + 'px'); 
        })
        .on("mouseout", function(d) { 

            tooltip.style('display', 'none');
        });

        legend2.append('text')                                    
        .attr('x', legendRectSize + legendSpacing)
        .attr('y', legendRectSize - legendSpacing)
        .text(function(d) { 
            //return d;
            try{
                   var label = d.split(':');
                   var str = label[0];
                   if(str.length > 10){
                     str = str.substring(0,8) + '...';
                   }
                    var val =  parseFloat(label[1]).toFixed(2);  
                    return str + ' : ' + val;
            }catch(e){}
            
        }); 





    },

    drawGroupedPieChart : function(chart_json,valueKey, groupByKey1, groupByKey2){

        console.log('draw grouped pie chart');

        let limit_x_axis = 20;
        chart_json = chart_json.slice(0,limit_x_axis);

        var width = 500;
        var height = 300;
        var margin ={top: 20, right: 20, bottom: 30, left: 60},
        width = 900 - margin.left - margin.right,
        height = 350 - margin.top - margin.bottom;

        var outer_pie_end_radius = 140;
        var inner_pie_radius = 85;
        var outer_pie_start_radius = 90;

        var dataset = chart_json;
  
        var legendRectSize = 15; // defines the size of the colored squares in legend
        var legendSpacing = 3; // defines spacing between squares


        var nested_data = d3.nest()
        .key(function(d) { 
            if(d[groupByKey2] == "undefined") 
                return "Others";
            else 
                return d[groupByKey2];
        })
        .rollup(function(leaves) { return  d3.sum(leaves, function(d) {
            if(d.sum == "undefined") 
                return "Others";
            else 
                return d.sum;
        })})
        .entries(chart_json);

        var colorX = d3.scaleOrdinal(d3.schemeCategory20c);
        var color = d3.scaleOrdinal(d3.schemeCategory20);
        console.log(nested_data);
        console.log('nested_data');




        d3.select('#siq_bar_chart').html("");
        d3.select('#siq_legend').html("");
        d3.select('#siq_pie_chart').html("");
        d3.select('#siq_pie_legend').html("");
        d3.selectAll('#siq_pie_chart').classed("slds-hide", false);
        d3.selectAll('#siq_pie_legend').classed("slds-hide", false);


        var svg = d3.select('#siq_pie_chart') 
        .append('svg') 
        .attr('width', width)
        .attr('height', height) 
        .append('g')
        .attr('transform', 'translate(' + (width / 2) + ',' + (height / 2) + ')');

        var cat_offset = 10;



        var arc2 = d3.arc()
            .innerRadius(outer_pie_start_radius) 
            .outerRadius(outer_pie_end_radius);


 

        nested_data

        var pie2 = d3.pie() // start and end angles of the segments
        .value(function(d) { return d.value; }) 
        .sort(null);

        var tooltip = d3.select('#siq_pie_chart') 
          .append('div')                                
          .attr('class', 'tooltip');

        tooltip.append('div')                        
          .attr('class', 'label_tooltip');                         

          tooltip.append('div')                  
          .attr('class', 'count'); 
          tooltip.append('div') 
          .attr('class', 'percent'); 


          nested_data.forEach(function(d) {
              d.value = +d.value; 
              d.enabled = true; 
          });

          


      

        // var path2 = svg.selectAll('path') 
        //   .data(pie2(nested_data)) 
        //   .enter() 
        //   .append('path') 
        //   .attr('d', arc2)
        //   .attr('fill', function(d) { return d3.rgb(colorX( d.data.key + ' : ' + d.data.value )); }) 
        //   .each(function(d) { this._current - d; });


          var g = svg.selectAll(".arc")
                  .data(pie2(nested_data))
                .enter().append("g")
                  .attr("class", "arc");

                g.append("path")
                  .attr("d", arc2)
                  .style("fill", function(d) { return d3.rgb(color(d.data.key + ' : ' + d.data.value))  })
                  .on('mouseover', function(d) {   
                    var total = d3.sum(dataset.map(function(d) {        
                        return (d.enabled) ? d.sum : 0;               
                    }));                                                      
                    var percent = Math.round(1000 * d.data.sum / total) / 10;
                    d3.select(this).style("fill", d3.rgb(color(d.data.key + ' : ' + d.data.value)).darker(2));
                    tooltip.select('.label_tooltip').html(d.data.key);  
                    var val = parseFloat(d.data.value).toFixed(2);     
                    tooltip.select('.count').html(val); 

                    tooltip.style('display', 'block'); 

                    var coords = d3.mouse(this);

                    tooltip.style('top', coords[1]+220 + 'px'); 
                    tooltip.style('left', coords[0]+250 + 'px');  


                })
                .on('mouseout', function(d) {              
                  tooltip.style('display', 'none');
                  d3.select(this).style("fill", d3.rgb(color(d.data.key + ' : ' + d.data.value)));
                });



        /*ADD INNER GROUPED CHART*/
            
 

            var data2 = chart_json;
            data2.forEach(function(d) {
              d.sum = +d.sum; 
              d.enabled = true; 
            });

            console.log('inner chart data');
            console.log(data2);

            var wedge = d3.arc()
            .innerRadius(0) 
            .outerRadius(inner_pie_radius);

            var pie = d3.pie() 
            .value(function(d) { return d.sum; }) 
            .sort(null);

            // var path = svg.selectAll('path') 
            // .data(pie(data2)) 
            // .enter() 
            // .append('path') 
            // .attr('d', arc)
            // .attr('fill', function(d) { 
            //     return d3.rgb(color( d.data[groupByKey2] + ' : ' + d.data[groupByKey1] + ' : ' + d.data.sum));
            // }) 
            // .each(function(d) { this._current - d; });

                var g = svg.selectAll(".wedge")
                  .data(pie(data2))
                .enter().append("g")
                  .attr("class", "wedge");

                g.append("path")
                  .attr("d", wedge)
                  .style("fill", function(d) { return d3.rgb( color( d.data[groupByKey2] + ' : ' + d.data[groupByKey1] + ' : ' + d.data.sum) ); })
                  .on('mouseover', function(d) {   
                    var total = d3.sum(dataset.map(function(d) {        
                        return (d.enabled) ? d.sum : 0;               
                    }));                                                      
                    var percent = Math.round(1000 * d.data.sum / total) / 10;
                    d3.select(this).style("fill", d3.rgb(color( d.data[groupByKey2] + ' : ' + d.data[groupByKey1] + ' : ' + d.data.sum)).darker(2));
                    tooltip.select('.label_tooltip').html(d.data[groupByKey1]);
                    var val = parseFloat(d.data.sum).toFixed(2);           
                    tooltip.select('.count').html(val); 

                    tooltip.style('display', 'block'); 

                    var coords = d3.mouse(this);

                    tooltip.style('top', coords[1]+220 + 'px'); 
                    tooltip.style('left', coords[0]+250 + 'px');  


                })
                .on('mouseout', function(d) {              
                  tooltip.style('display', 'none');
                  d3.select(this).style("fill", d3.rgb(color( d.data[groupByKey2] + ' : ' + d.data[groupByKey1] + ' : ' + d.data.sum)));
                });

           


            var final_ = new Array();
            var keyset = new Set();
            
            nested_data.forEach( function(element){
                keyset.add(element.key);
                if(element.key!=undefined)
                    final_.push(element.key+' : '+element.value);
                data2.forEach( function(ele){
                    if(ele[groupByKey2] == element.key ){
                        final_.push(ele[groupByKey2]+' : '+ele[groupByKey1]+' : '+ele.sum);
                    }
                }) ;
            });

            console.log('console.log(groupByKey2);');
            console.log(groupByKey2);
            console.log(groupByKey1);




           var path = svg.selectAll(".path");
            path.on('mouseover', function(d) {   
                var total = d3.sum(dataset.map(function(d) {        
                    return (d.enabled) ? d.sum : 0;               
                }));                                                      
                var percent = Math.round(1000 * d.data.sum / total) / 10;
                d3.select(this).style("fill", d3.rgb(color( d.data[groupByKey2] + ' : ' + d.data[groupByKey1] + ' : ' + d.data.sum)).darker(2));
                tooltip.select('.label_tooltip').html(d.data[groupByKey1]);   
                var val = parseFloat(d.data.sum).toFixed(2);        
                tooltip.select('.count').html(val); 

                tooltip.style('display', 'block'); 

                var coords = d3.mouse(this);

                tooltip.style('top', coords[1]+220 + 'px'); 
                tooltip.style('left', coords[0]+250 + 'px');  


            }); 
            path.on('mouseout', function(d) {              
              tooltip.style('display', 'none');
              d3.select(this).style("fill", d3.rgb(color( d.data[groupByKey2] + ' : ' + d.data[groupByKey1] + ' : ' + d.data.sum)));
          });
            /*///////////////////////*/


            // path.on('mouseover', function(d) {                                         
            //     d3.select(this).style("fill", d3.rgb( color( d.data[groupByKey2] + ' : ' + d.data.sum )).darker(2));
            //     tooltip.select('.label_tooltip').html(d.data[groupByKey2] );           
            //     tooltip.select('.count').html(d.data.sum); 
            //     tooltip.style('display', 'block'); 
            //     var coords = d3.mouse(this);
            //     tooltip.style('top', coords[1]+220 + 'px'); 
            //     tooltip.style('left', coords[0]+250 + 'px');  
            // }); 

            // path.on('mouseout', function(d) {              
            //       tooltip.style('display', 'none');
            //       d3.select(this).style("fill", d3.rgb(color( d.data.key + ' : ' + d.data.value)));
            // });


            // return label_tooltip




  
  console.log('color');
  console.log(color.domain());

var legend2 = d3.select('#siq_pie_legend') 
.append('svg') 
.attr('height', 18 * final_.length ) 
.append('g')
.attr('class','siq_legend')
.attr('height',height -10)
.attr('overflow','visible')
.selectAll('.legend')
.data(final_)
.enter() 
.append('g') 
.attr('class', 'legend')
.attr('transform', function(d, i) {                   
    var height = legendRectSize + legendSpacing;
    var offset =  height * final_ / 2;
    var horz = 0; 
    var vert = i * height + 2;   
    var keyset = new Set(Object.values(nested_data));
    if(d.split(' : ').length == 3){
        horz = 20; 
    }
    return 'translate(' + horz + ',' + vert + ')';
});


// adding colored squares to legend
legend2.append('rect')                                 
.attr('width', legendRectSize)                    
.attr('height', legendRectSize)                    
.style('fill',  color) 
.style('stroke', color)
.on("mouseover", function(d) {
    console.log(d);
    tooltip.style('display', 'block'); 
    tooltip.style('top', d3.event.pageY - 215 + 'px'); 
    tooltip.style('left', d3.event.pageX - 65 + 'px'); 

    try{
        var valueArray = d.split(':');
        if(valueArray != undefined && valueArray.length == 3){
            tooltip.select('.label_tooltip').html(valueArray[1]);             
            var val = parseFloat(valueArray[2].trim()).toFixed(2);
            tooltip.select('.count').html(val);
        }else{
            tooltip.select('.label_tooltip').html(valueArray[0]);           
            var val = parseFloat(valueArray[1].trim()).toFixed(2);
            tooltip.select('.count').html(val);
        } 
    }catch(e){}

   

})
.on("mouseout", function(d) { 
    tooltip.style('display', 'none');
});
// .on('click', function(label) {
//     var rect = d3.select(this);
//     var enabled = true; 
//     var totalEnabled = d3.sum(nested_data.map(function(d) { 
//       return (d.enabled) ? 1 : 0;
//   }));

//     if (rect.attr('class') === 'disabled') { 
//       rect.attr('class', ''); 
//   } else {
//       if (totalEnabled < 2) return; 
//       rect.attr('class', 'disabled');
//       enabled = false; 
//   }

//   pie.value(function(d) { 
//       if ( ( d.key + ' : ' + d.value ) === label) d.enabled = enabled; // if entry label matches legend label
//         return (d.enabled) ? d.value : 0; // update enabled property and return count or 0 based on the entry's status
//     });

//   path = path.data(pie(nested_data)); 

//   path.transition() 
//   .duration(750)
//       .attrTween('d', function(d) { // 'd' specifies the d attribute that we'll be animating
//         var interpolate = d3.interpolate(this._current, d); // this = current path element
//         this._current = interpolate(0); // interpolate between current value and the new value of 'd'
//         return function(t) {
//           return arc(interpolate(t));
//       };
//   });
//   });

    legend2.append('text')                                    
    .attr('x', legendRectSize + legendSpacing)
    .attr('y', legendRectSize - legendSpacing)
    .text(function(d) { 
        try{
            
            var keyset = new Set(Object.values(nested_data));
            var label = '';
            if(d.split(' : ').length == 2)
                label = d.split(' : ')[0] + ' : ' + d.split(' : ')[1];
            else
                label = d.split(' : ')[1]+ ' : ' + d.split(' : ')[2];


      
            var arr = label.split(':');
            var str = arr[0];
            if(str.length > 10){
                str = str.substring(0,8) + '...';
            }
            var val = parseFloat(arr[1].trim()).toFixed(2);

            return str + ' : ' + val;

        }catch(e){}

    }); 
},


drawPieChart : function(chart_json,valueKey, groupByKey1, groupByKey2){

    console.log('draw pie chart');

    jQuery("#siq_pie_chart").empty();

    var width = 500;
    var height = 300;


    var margin ={top: 20, right: 20, bottom: 30, left: 60},
    width = 900 - margin.left - margin.right,
    height = 350 - margin.top - margin.bottom;
        // a circle chart needs a radius
        var radius = 140;

        
        var dataset = chart_json;
        // legend dimensions    
        var legendRectSize = 15; // defines the size of the colored squares in legend
        var legendSpacing = 3; // defines spacing between squares
        
        
        var color = d3.scaleOrdinal(d3.schemeCategory20c);


        d3.select('#siq_bar_chart').html("");
        d3.select('#siq_legend').html("");
        d3.select('#siq_pie_chart').html("");
        d3.select('#siq_pie_legend').html("");


        d3.selectAll('#siq_pie_chart').classed("slds-hide", false);
        d3.selectAll('#siq_pie_legend').classed("slds-hide", false);



        

        var svg = d3.select('#siq_pie_chart') 
        .append('svg') 
        .attr('width', width)
        .attr('height', height) 
        .append('g')
        .attr('transform', 'translate(' + ((width / 2)+30) + ',' + (height / 2) + ')'); 



        var arc = d3.arc()
        .innerRadius(0) 
        .outerRadius(radius);

        var pie = d3.pie() 
        .value(function(d) { return d.sum; }) 
        .sort(null);

        var tooltip = d3.select('#siq_pie_chart')
        .append('div')                                     
        .attr('class', 'tooltip'); 

        tooltip.append('div')                       
        .attr('class', 'label_tooltip');                        

        tooltip.append('div')                  
        .attr('class', 'count'); 
        tooltip.append('div') 
        .attr('class', 'percent'); 


        dataset.forEach(function(d) {
          d.sum = +d.sum; 
          d.enabled = true; 
      });

        var path = svg.selectAll('path') 
        .data(pie(dataset)) 
        .enter() 
        .append('path') 
        .attr('d', arc)
        .attr('fill', function(d) { return color( d.data[groupByKey1] + ' : ' + d.data.sum); }) 
        .each(function(d) { this._current - d; });


        path.on('mouseover', function(d) {   

            var total = d3.sum(dataset.map(function(d) {        
                return (d.enabled) ? d.sum : 0;               
            }));                                                      
            var percent = Math.round(1000 * d.data.sum / total) / 10; 
            
            //tooltip.select('.label_tooltip').html(d.data[groupByKey1]);           
            //tooltip.select('.count').html('' + d.data.sum);            
            //tooltip.style('display', 'block');  


            d3.select(this).style("fill", d3.rgb(color( d.data[groupByKey1] + ' : ' + d.data.sum)).darker(2));
            tooltip.select('.label_tooltip').html(d.data[groupByKey1]);  
            var val = parseFloat(d.data.sum).toFixed(2);        
            tooltip.select('.count').html(val);          
          

            tooltip.style('display', 'block'); 

            var coords = d3.mouse(this);

            //var h =  d3.select('#siq_bar_container_div').node().getBoundingClientRect().height - 10;

            tooltip.style('top', coords[1]+220 + 'px'); 
            tooltip.style('left', coords[0]+250 + 'px');  


        }); 

        path.on('mouseout', function(d) {              
          tooltip.style('display', 'none');
          d3.select(this).style("fill", d3.rgb(color( d.data[groupByKey1] + ' : ' + d.data.sum)));
      });


// return label



var legend2 = d3.select('#siq_pie_legend') 
.append('svg') 
.attr('height', 18 * dataset.length ) 
.append('g')
.attr('class','siq_legend')
.attr('height',height -10)
.attr('overflow','visible')
.selectAll('.legend')
.data(color.domain())
.enter() 
.append('g') 
.attr('class', 'legend')
.attr('transform', function(d, i) {                   
    var height = legendRectSize + legendSpacing;
    var offset =  height * color.domain().length / 2;
    var horz = 0; 
    var vert = i * height;           
    return 'translate(' + horz + ',' + vert + ')';
});

// adding colored squares to legend
legend2.append('rect')                                 
.attr('width', legendRectSize)                    
.attr('height', legendRectSize)                    
.style('fill', color) 
.style('stroke', color)
.on("mouseover", function(d) {

    tooltip.select('.label_tooltip').html(d.split(':')[0]); 
    var str = d.split(':')[1];
    var val = parseFloat(str.trim()).toFixed(2);        
    tooltip.select('.count').html(val);                    
    tooltip.style('display', 'block'); 
    tooltip.style('top', d3.event.pageY - 215 + 'px'); 
    tooltip.style('left', d3.event.pageX - 65 + 'px'); 


})
.on("mouseout", function(d) { 

    tooltip.style('display', 'none');
})
.on('click', function(label) {
    var rect = d3.select(this);
    var enabled = true; 
    var totalEnabled = d3.sum(dataset.map(function(d) { 
      return (d.enabled) ? 1 : 0;
  }));

    console.log(totalEnabled);

    if (rect.attr('class') === 'disabled') { // if class is disabled
      rect.attr('class', ''); // remove class disabled
    } else { // else
      if (totalEnabled < 2) return; // if less than two color(d[summarizeByKey] + ' : ' + d.sum)s are flagged, exit
      rect.attr('class', 'disabled'); // otherwise flag the square disabled
      enabled = false; // set enabled to false
  }

  pie.value(function(d) { 
      if ( ( d[groupByKey1] + ' : ' + d.sum ) === label ) d.enabled = enabled; // if entry label matches legend label
        return (d.enabled) ? d.sum : 0; // update enabled property and return count or 0 based on the entry's status
    });

  path = path.data(pie(dataset)); 

  path.transition() 
  .duration(750)
      .attrTween('d', function(d) { // 'd' specifies the d attribute that we'll be animating
        var interpolate = d3.interpolate(this._current, d); // this = current path element
        this._current = interpolate(0); // interpolate between current value and the new value of 'd'
        return function(t) {
          return arc(interpolate(t));
      };
  });
  });


legend2.append('text')                                    
.attr('x', legendRectSize + legendSpacing)
.attr('y', legendRectSize - legendSpacing)
.text(function(d) { 
    //return d; 
    try{
        var arr = d.split(':');
        var str = arr[0];
        if(str.length > 10){
            str = str.substring(0,8) + '...';
        }
        var val = parseFloat(arr[1].trim()).toFixed(2);
        return str + ' : ' + val;
    }catch(e){

    }
    
}); 









},

clearChartsDiv : function(){
    console.log('clear charts');
    d3.selectAll('#siq_bar_chart').html("");
    d3.selectAll('#siq_legend').html("");
    d3.selectAll('#siq_pie_chart').html("");
    d3.selectAll('#siq_pie_legend').html("");


    d3.selectAll('#siq_bar_chart').classed("slds-hide", true);
    d3.selectAll('#siq_legend').classed("slds-hide", true);
    d3.selectAll('#siq_pie_chart').classed("slds-hide", true);
    d3.selectAll('#siq_pie_legend').classed("slds-hide", true);


},

hideNoDataMessage : function(component,event,helper,auraId){
    var nomsgData = component.find(auraId);
    $A.util.addClass(nomsgData,'slds-hide'); 
},

showNoDataMessage : function(component,event,helper,auraId){
    var nomsgData = component.find(auraId);
    $A.util.removeClass(nomsgData,'slds-hide'); 
},

resetFilterComponent : function(component,event,helper){
    try{
        var childComponent = component.find("childFilter");

        component.set("v.parsedJSONQuery","");
        component.set("v.parsedQuery","");
        component.set("v.errorMsg",""); 

        component.set("v.updatedWrapperButtonPie","");
        component.set("v.parsedQueryPie","");
        component.set("v.errMsgPie",""); 

        component.set("v.updatedWrapperButtonBar","");
        component.set("v.parsedQueryBar","");
        component.set("v.errMsgBar",""); 

        component.set("v.queryBar",'');
        component.set("v.queryPie",'');

        childComponent.init(); 
        childComponent.set("v.parsedJSONQuery","");
        childComponent.set("v.parsedQuery","");
        childComponent.set("v.errorMsg",""); 

       debugger;
    }catch(e){

    }
    
}






})