({
    hideToastNotification: function(component,componentId){
        window.setTimeout(
            $A.getCallback(function() {
                var toast = component.find(componentId);
                $A.util.addClass(toast,'slds-hide');
            }), 10000
        );
    },

    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    },

    toggleClassInverse: function(component,componentId,className) {
        component.find('DataTable').deselectCheckbox();
        component.set("v.selectedCheckboxList",[]);
        var detailCard = component.find('position_detail');
        detailCard.hideDetailCard();
        var detailCard2 = component.find('position_employee_detail');
        detailCard2.hideDetailCard();
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    }
})