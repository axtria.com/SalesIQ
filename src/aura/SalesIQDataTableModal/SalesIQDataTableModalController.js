({
	closePopups :  function(component, event, helper) 
  {

    var DataTable = component.find('DataTable');
    DataTable.closeDropDown();
    if(document.querySelector('[id*="_treeContainer"]')!=null){
      document.querySelectorAll('[id*="_treeContainer"]')[0].className = 'slds-tree_container slds-hide' ;
    }
    /*if((event.path != undefined || event.path != null) && (event.path[0].className != null || event.path[0].className != undefined) && event.path[0].parentElement != null)
    {
      if(event.path[0].parentElement.className != 'brdCrmbPopOut' && event.path[0].className.indexOf('hierarchyButton')== -1 && event.path[0].className.indexOf('icon-nodes') == -1 && event.path[0].id != "treeSearchBox")
      {
        var DataTable = component.find('DataTable');
        DataTable.closeDropDown();
        if(document.querySelector('[id*="_treeContainer"]')!=null)
          document.querySelectorAll('[id*="_treeContainer"]')[0].className = 'slds-tree_container slds-hide' ;
      }
    } 
    else if((event.target != undefined || event.target != null) && event.target.parentElement != null)
    {
      if(event.target.nodeName == 'svg' || event.target.nodeName == 'use')
      {
        if(event.target.parentElement.nodeName == 'svg' || event.target.parentElement.nodeName == 'use')
        {
          if(event.target.parentElement.className.baseVal != 'brdCrmbPopOut' && event.target.className.baseVal.indexOf('hierarchyButton')== -1 && event.target.id != "treeSearchBox" && event.target.className.baseVal.indexOf('icon-nodes') == -1)
          {
            var DataTable = component.find('DataTable');
            DataTable.closeDropDown();
            if(document.querySelector('[id*="_treeContainer"]')!=null)
              document.querySelectorAll('[id*="_treeContainer"]')[0].className = 'slds-tree_container slds-hide' ;
          }
        }
        else if(event.target.parentElement.className != 'brdCrmbPopOut' && event.target.className.baseVal.indexOf('hierarchyButton')== -1 && event.target.id != "treeSearchBox" && event.target.className.baseVal.indexOf('icon-nodes') == -1)
        {
          var DataTable = component.find('DataTable');
          DataTable.closeDropDown();
          if(document.querySelector('[id*="_treeContainer"]')!=null)
            document.querySelectorAll('[id*="_treeContainer"]')[0].className = 'slds-tree_container slds-hide' ;
        }
      }
      else if(event.target.parentElement.className != 'brdCrmbPopOut' && event.target.className.indexOf('hierarchyButton')== -1 && event.target.id != "treeSearchBox" && event.target.className.indexOf('icon-nodes') == -1)
      {
        var DataTable = component.find('DataTable');
        DataTable.closeDropDown();
        if(document.querySelector('[id*="_treeContainer"]')!=null)
          document.querySelectorAll('[id*="_treeContainer"]')[0].className = 'slds-tree_container slds-hide' ;
      }
    }*/
	},

  hideShowErrorPopup: function(component)
  {
    var activeClass = component.get("v.popOverClass");
    var toast = component.find("error_toast");
    $A.util.toggleClass(toast,'slds-hide');
  },
	
	showpositionpopupfunction :  function(component, event, helper)
  {
    // var contentHeight = jQuery("#positiondialog #modal-datatable").height();
    // var headerHeight = jQuery(".forceListViewManagerHeader").outerHeight();
    // var searchBarHeight = jQuery("#searchBar").outerHeight();

    var contentHeight = document.querySelector("#positiondialog #modal-datatable").clientHeight -   parseInt(window.getComputedStyle(document.querySelector("#positiondialog #modal-datatable")).getPropertyValue("padding-top").replace('px','')) - parseInt(window.getComputedStyle(document.querySelector("#positiondialog #modal-datatable")).getPropertyValue("padding-bottom").replace('px',''));
    // var headerHeight = document.getElementsByClassName("forceListViewManagerHeader")[0].clientHeight;
    // var searchBarHeight = document.getElementById('searchBar').clientHeight;


    // jQuery("#datatableDiv").css({'height':(contentHeight - (headerHeight + searchBarHeight) ),'overflow-y':'auto'});
    // document.getElementById("datatableDiv").style.height = contentHeight - (headerHeight) + 'px';
    // document.getElementById("datatableDiv").style.overflowY = 'auto';


    var DataTable = component.find('DataTable');
    DataTable.setModalTableHeight();

    var assigneeId = event.getParam("recordId");
    component.set("v.assigneeId",assigneeId);
    var detailCard = component.find('position_detail');
    detailCard.hideDetailCard();
    var detailCard2 = component.find('position_employee_detail');
    detailCard2.hideDetailCard();
    helper.toggleClass(component,'backdrop','slds-backdrop--');
		helper.toggleClass(component,'positiondialog','slds-fade-in-');

    // var contentHeight = jQuery("#positiondialog #modal-datatable").height();
    
    // jQuery("#rightTab").css({'height':contentHeight,'overflow-y':'auto'});
    document.getElementById('rightTab').style.height = contentHeight+'px';
    document.getElementById('rightTab').style.overflowY = 'auto';

    // add margin when add employee from prosition universe is called
    // if(component.get("v.moduleName") == 'Position Universe')
      // document.getElementById('searchBarDiv').style.margin = '10px !important';
      // jQuery("#searchBarDiv").css({'margin':'10px !important'});
  },

  droppopupfunction: function(component,event,helper)
  {   
    var warningIcon = component.find('warning-icon');
    var toast = component.find("error_toast");
    $A.util.addClass(toast,'slds-hide');
    $A.util.addClass(warningIcon,'slds-hide');

    var detailCard = component.find('position_detail');
    detailCard.hideDetailCard();

    var detailCard2 = component.find('position_employee_detail');
    detailCard2.hideDetailCard();  

    var DataTable = component.find('DataTable');
    DataTable.closeDropDown();

    helper.toggleClassInverse(component,'backdrop','slds-backdrop--');
	  helper.toggleClassInverse(component,'positiondialog','slds-fade-in-');
  },

  addAssignment : function(component,event,helper)
  {
    var selectedCheckboxList = component.get("v.selectedCheckboxList");
    var warningIcon = component.find('warning-icon');
    var toast = component.find("error_toast");

    var DataTable = component.find('DataTable');
    DataTable.closeDropDown();

    if(selectedCheckboxList.length < 1)
    {
      $A.util.removeClass(toast,'slds-hide');
      $A.util.removeClass(warningIcon,'slds-hide');
      helper.hideToastNotification(component,"error_toast");
    }
    else
    {
      $A.util.addClass(toast,'slds-hide');
      $A.util.addClass(warningIcon,'slds-hide');
      if(selectedCheckboxList.length > 0)
      {
        var evt = $A.get("e.c:addAssignment");
        evt.setParams
        ({
            "selectedCheckboxList": component.get("v.selectedCheckboxList")
        });            
        evt.fire();
      }

      helper.toggleClassInverse(component,'backdrop','slds-backdrop--');
      helper.toggleClassInverse(component,'positiondialog','slds-fade-in-');
    }
  },

  closeToast : function(component,event,helper)
  {
    var toast = component.find("error_toast");
    $A.util.addClass(toast,'slds-hide');
  },

  showDetailCard : function(component, event, helper) 
  {    
    var no_position_text = component.find("no_position_text");

    $A.util.addClass(no_position_text,"slds-hide");
      
    var recordId = event.getParam('recordId');
    component.set("v.recordId",recordId);

    var detailCard = component.find('position_detail');
    detailCard.getData();

    var assignmentCard = component.find('position_employee_detail');
    assignmentCard.getData();

    
    var listView = component.find('modalListView');
    listView.slider('hide');

    var DataTable = component.find('DataTable');
    // DataTable.setModalTableHeight();
    var cardheader,assignmentcardheader;

    var namespace = component.get("v.namespace");
    var positionDetailsText;
    if(namespace != '')
    {
        positionDetailsText = $A.get("$Label.AxtriaSalesIQTM.Position");

    }
    else
    {
        positionDetailsText = $A.get("$Label.c.Position");
        
    }
    if(component.get("v.moduleName") == 'Employee Universe')
    {
        cardheader = {"label" : positionDetailsText+" Details" , "icon" : "custom:custom13"};
        assignmentcardheader = {"label" : "Employee Assignment" , "icon" : "custom:custom13"};
    }
    else
    if(component.get("v.moduleName") == 'Position Universe')
    {
        cardheader = {"label" : "Employee Details" , "icon" : "custom:custom13"};
        assignmentcardheader = {"label" : "Employee Assignment" , "icon" : "custom:custom13"}; 
    }
    component.set("v.positionCardHeader",cardheader);

    
    component.set("v.assignmentCardHeader",assignmentcardheader);
  },

  hideDetailCard : function(component, event, helper)
  {
    component.set("v.recordId","");

    var detailCard = component.find('position_detail');
    detailCard.hideDetailCard();

    var detailCard2 = component.find('position_employee_detail');
    detailCard2.hideDetailCard();
    
    var listView = component.find('modalListView');
    listView.slider('hide');

    var no_position_text = component.find("no_position_text");
    $A.util.removeClass(no_position_text,"slds-hide");
  },

  leftslider : function(component,event,helper)
  {
	  var modalListView =  component.find('modalListView');
	  modalListView.slider('left');
  },
  rightslider : function(component,event,helper)
  {
    
    var modalListView =  component.find('modalListView');
    modalListView.slider('right');
    
  },
  viewChange : function(component,event,helper)
  {
      var datatable = component.find('DataTable');
      var viewId = event.getParam("viewId");
      var viewName = event.getParam("viewName");
      //var globalId = component.getGlobalId();
        // var $j = jQuery.noConflict();
        // $j('#cardName').text(viewName);
      // document.getElementById(globalId+'cardName') 
      component.set("v.selectedViewName",viewName); 
      datatable.changeView(viewId);
  },
  hideListViewButtons: function(component,event,helper)
  {
    var anchorButton = component.find('anchorButton');
    $A.util.addClass(anchorButton,'slds-hide');
  },
  showListViewButtons: function(component,event,helper)
  {
    var anchorButton = component.find('anchorButton');
    $A.util.removeClass(anchorButton,'slds-hide');
  },

  openModalConfirmationPopUp : function(component, event, helper) 
  {
    var recordId = component.get("v.recordId");

    var targetCmp = component.find('modalConfirmationPopUp');
    var body = targetCmp.get("v.body");

    if(component.get("v.moduleName") == 'Employee Universe')
    {
        $A.createComponent
        (
            "c:SalesIQMessageDialog",
            {
                "body": "Are you sure you want to continue? All unsaved progress will be discarded.",
                "recordId" : component.get("v.recordId"),
                "onconfirm" : component.getReference("c.navigateToEmployeeUniverse"),
            },
            function(msgBox)
            {                
                if (component.isValid()) {
                    var targetCmp = component.find('modalConfirmationPopUp');
                    var body = targetCmp.get("v.body");
                    body.push(msgBox);
                    targetCmp.set("v.body", body);
                }
            }
        );
    }
    else if(component.get("v.moduleName") == 'Position Universe')
    {
        $A.createComponent
        (
            "c:SalesIQMessageDialog",
            {
                "body": "Are you sure you want to continue? All unsaved progress will be discarded.",
                "recordId" : component.get("v.recordId"),
                "onconfirm" :  component.getReference("c.navigateToPositionUniverse") 
            },
            function(msgBox)
            {                
                if (component.isValid()) 
                {
                    var targetCmp = component.find('modalConfirmationPopUp');
                    var body = targetCmp.get("v.body");
                    body.push(msgBox);
                    targetCmp.set("v.body", body);
                }
            }
        );
    }
  },

  navigateToPositionUniverse : function(component,event,helper)
  {  
      var currentState = history.state;
      history.pushState(currentState, "Position_Universe" , "" );
      var evt = $A.get("e.force:navigateToComponent");
      evt.setParams({
          componentDef : "c:SalesIQPositionUniverse",
          componentAttributes: {
              objectName: "Position__c",
              loadRecent: "false",
              selectedTeamInstance : component.get("v.selectedTeamInstance")
          },
          isredirect : true
      });
      evt.fire();
      document.getElementsByTagName("body")[0].style.overflow = "visible";
  },

  navigateToEmployeeUniverse : function(component,event,helper)
  {  
      var currentState = history.state;
      history.pushState(currentState, "Employee_Universe" , "" );
      var evt = $A.get("e.force:navigateToComponent");
      evt.setParams({
          componentDef : "c:SalesIQEmployeeUniverse",
          componentAttributes: {
              objectName: "Employee__c",
              loadRecent: "false"
          },
          isredirect : true
      });
      evt.fire();
      document.getElementsByTagName("body")[0].style.overflow = "visible";
  },

  updateCountryAttributes: function(component, event, helper) 
  {
      var datatable = component.find('DataTable');
      var countryId = event.getParam('countryId');
      datatable.updateCountryAttributes(countryId);
  },

})