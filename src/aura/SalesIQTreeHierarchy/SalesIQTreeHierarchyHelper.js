({
    getTreeRecords : function(component,event) 
    {
        console.log('init getTreeRecords :'+component.get("v.countryId"));
        var country = component.get("v.countryId");

        if(country == '' || country == null || country == undefined){
            console.log("country id is not present : Tree Hierarchy");
            return;
        }

        var count = 1;
    	var action = component.get("c.getUserRoles");

        //get position products
        var selectedProductId = component.get('v.selectedProductId');

        var selectedRuleType = 'Position';

       

        var productList = component.get('v.teamProductIds');
        var selected_team_instance = component.get('v.selectedTeamInstance');
        var execute_tree_method = true;
        if(selected_team_instance == null || selected_team_instance == undefined || selected_team_instance== '' || selected_team_instance == 'undefined' ){
            execute_tree_method = false;
        }

        console.log('teamInstance :',component.get('v.selectedTeamInstance'));

        action.setParams({
            "selectedTeamInstance" : component.get('v.selectedTeamInstance'),
            "moduleName" : component.get("v.moduleName"),
            "countryId" : country,
            "selectedRuleType" : selectedRuleType,
            "teamProductIds" : productList,
            "scenarioRuleInstanceId" : component.get('v.scenarioRuleInstanceId')
        });

        action.setCallback(this, function(response) 
        {
            var selNodes = component.get('v.setSelectedNodes');
            var setSelectedNodes = new Set();

            if( selNodes != undefined && selNodes != null && selNodes.length > 0 ){
                setSelectedNodes = new Set(selNodes.split(','));
                component.set('v.selectedNodesCount', setSelectedNodes.size);   
            }else{
                component.set('v.selectedNodesCount', 0);
            }            


            console.log('setSelectedNodes ::'+selNodes);
             
            var roles = {}, results, pendingCRMap;
            var positionToProductMap;

            if(component.isValid() && response.getState() === "SUCCESS") 
            {
                var wrapper = response.getReturnValue()
                pendingCRMap = wrapper.pendingCRMap;
                results = wrapper.positionList;
                positionToProductMap =  wrapper.mapPositonProducts;
                //console.log(positionToProductMap);

                component.set('v.positionToProductMap',wrapper.mapPositonProducts);
                var setPositionIds = new Set();
                if(positionToProductMap!= undefined || positionToProductMap != null){
                    if( positionToProductMap[ selectedProductId ] != undefined || positionToProductMap[selectedProductId ] != null ){
                        setPositionIds = new Set( positionToProductMap[ selectedProductId ] );
                    }            
                }


                roles[undefined] = { Name: "Root", PositionType:'', items: [], Level : '' , RGB:'', count:count, PositionLevel : '',pendingCRList : []};
                var levelVal = 0 ;
                count = count+1;
                var ns = component.get("v.namespace");

                results.forEach(function(v) {
                    if(levelVal < parseInt(v[ns+'Hierarchy_Level__c']))
                        levelVal = parseInt(v[ns+'Hierarchy_Level__c']);
                });
                levelVal = levelVal + 1;
                component.set("v.topLevel",levelVal);

               var isReadOnly = component.get("v.viewMode") == "ReadOnly" ? true : false;
                results.forEach(function(v) {
                    var isSelectedNode;
                    var _isNodeDisabled = false;
                    var h_lvl = parseInt(v[ns+'Hierarchy_Level__c']);
                    if(( h_lvl == 0 || h_lvl == 1  ) && !( selectedProductId == undefined || selectedProductId == null || selectedProductId == '' ) ){
                        _isNodeDisabled =  !setPositionIds.has(v.Id);
                        if( setSelectedNodes.has(v.Id) && _isNodeDisabled ){
                            setSelectedNodes.delete(v.Id);
                        }
                    }
                    if( setSelectedNodes != undefined && setSelectedNodes != null )
                        isSelectedNode = setSelectedNodes.has(v.Id);


                    var colorRgb = v[ns+'RGB__c'];
                    if(colorRgb != undefined) {
                        var r = colorRgb.split(",")[0];
                        var g = colorRgb.split(",")[1];
                        var b = colorRgb.split(",")[2];
                        colorRgb = r + ',' +  g + ',' + b;
                    }

                    var isSelectedNode;
                    if( setSelectedNodes != undefined && setSelectedNodes != null )
                        isSelectedNode = setSelectedNodes.has(v.Id);

                        if(parseInt(v[ns+'Hierarchy_Level__c']) == 0)
                            roles[v.Id] = { Name: v[ns+'Client_Territory_Name__c'] , PositionType: v[ns+'Related_Position_Type__c'], items: [], Level : levelVal - (1), RGB: colorRgb, Id : v.Id, count:count, PositionLevel : 1, pendingCRList : pendingCRMap[v.Id], isSelectedNode : isSelectedNode, isVisible : true, isNodeDisabled : _isNodeDisabled, searchable : true };
                        else
                            roles[v.Id] = { Name: v[ns+'Client_Territory_Name__c'] , PositionType: v[ns+'Related_Position_Type__c'], items: [], Level : levelVal - (parseInt(v[ns+'Hierarchy_Level__c'])), RGB: colorRgb, Id:v.Id, count:count, PositionLevel : v[ns+'Hierarchy_Level__c'], pendingCRList : pendingCRMap[v.Id], isSelectedNode : isSelectedNode, isVisible : true, isNodeDisabled : _isNodeDisabled, searchable : true };


                    count = count+1;
                });
                results.forEach(function(v) {
                    if(v[ns+'Parent_Position__c'] != undefined && roles[v[ns+'Parent_Position__c']] != undefined){
                        roles[v[ns+'Parent_Position__c']].items.push(roles[v.Id]);
                    } else {
                      roles[undefined].items.push(roles[v.Id]); 
                    }
                });
                
                console.log('11111111111111111111');
                console.log(roles[undefined].items);
                component.set("v.nodes", roles[undefined].items);
                component.set("v.initNodes",roles[undefined].items);
            } 
            else 
            {
                var namespace = component.get("v.namespace");
                var uapErrorLabel,errorLabel;

                var errors = response.getError();
                var errorMessage = errors[0].message;
                
                if(namespace != '')
                {
                    uapErrorLabel = $A.get("$Label.AxtriaSalesIQTM.No_UAP_Assigned");
                    errorLabel = $A.get("$Label.AxtriaSalesIQTM.Unexpected_error_notification");
                }
                else
                {
                    errorLabel = $A.get("$Label.c.Unexpected_error_notification");
                    uapErrorLabel = $A.get("$Label.c.No_UAP_Assigned");
                }
                
                if(errorMessage.indexOf(uapErrorLabel) != -1 )
                {
                    return;
                }
                if (errors[0] && errors[0].message) 
                {
                    var action = component.get("c.logException");
        
                    action.setParams({
                        message : errorMessage,
                        module : component.get("v.moduleName")
                    });

                    action.setCallback(this,function(response)
                    {
                        var state = response.getState();
                        if(state === 'SUCCESS')
                        {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                mode: 'dismissible',
                                message: errorLabel,
                                type : 'error'
                                
                            });
                            toastEvent.fire();
                        }
                    });
                    $A.enqueueAction(action);
                } 
                else 
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: errorLabel,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                }
            }
        });

        // ankur needs to recheck this condition 
        // if(execute_tree_method)
            $A.enqueueAction(action);
    },

    setSelectedValues : function(component,helper,nodes){
        var setSelectedNodes = component.get('v.setSelectedNodes');

        if(setSelectedNodes == undefined || setSelectedNodes == null ){
            setSelectedNodes = new Set();
        }

        if(nodes != undefined && nodes[0]!= undefined && nodes[0].isSelectedNode && nodes[0].PositionLevel == '1'){
             setSelectedNodes.add(nodes[0].Id);
        }
        component.set('v.setSelectedNodes',setSelectedNodes);
        
        Object.values(nodes).forEach(
        function(element){ 
            if(element.isSelectedNode && element.PositionLevel != undefined && element.PositionLevel == '1'){
                setSelectedNodes.add(element.Id);
            }  
            if(element.items != undefined && element.items.length > 0){
               helper.setSelectedValues(component,helper,element.items);
            }   
        });
        component.set('v.setSelectedNodes',setSelectedNodes);  
        console.log(setSelectedNodes);
    },


    markChildrenNodes: function(component,helper,nodeId,isSelected){
        var updatedTree = helper.findAndModifyAll( component.get('v.nodes')[0], nodeId,isSelected);
        //console.log(updatedTree);
        component.set('v.nodes',[ updatedTree ]);
        var selNodes = helper.computeSelectedNodes(updatedTree, component.get('v.setSelectedNodes'));
        console.log(selNodes);

        if( selNodes.length > 0){
            component.set('v.setSelectedNodes', selNodes); 
            component.set('v.selectedNodesCount', selNodes.split(',').length);   
        }else{
                component.set('v.selectedNodesCount', 0);
            }            

        
    },

    findAndModifyAll : function (tree,nodeId,isSelected) {
      let found = false;
      const selected = isSelected;
      function innerFunc(tree, nodeId, found, isSelected) {
        let findSuccess = false;
        if( ( tree['Id'] == nodeId ) || found ){
            findSuccess = true;

            if(tree['isNodeDisabled'] != undefined && !tree['isNodeDisabled']){
               tree['isSelectedNode'] = selected;  
            }
           
            found = true;
        }else{
           findSuccess = false; 
        }


        if (findSuccess) {
            found = true;
            if (tree.hasOwnProperty('items')) {
              for (let n of tree['items']) {
                innerFunc(n, nodeId, found, selected);
              }
            }
        } 
        else if (tree.hasOwnProperty('items')) {
          for (let n of tree['items']) {
            innerFunc(n, nodeId, findSuccess, selected);
          }
        }
      }
      innerFunc (tree, nodeId, found, selected);
      return tree;
    },

    computeSelectedNodes : function ( tree, selNodes ){
        let found = false;
       
        var setSelectedNodes = new Set();

        if( selNodes != undefined && selNodes != null && selNodes.length > 0 ){
            setSelectedNodes = new Set(selNodes.split(','));
        } 
        var selectednodes = new Array();
        function innerFunc(tree) {
            let findSuccess = false;
            if( tree['isSelectedNode'] && ( tree['PositionLevel'] == 1 )){
                setSelectedNodes.add(tree['Id']);
            }else if( !tree['isSelectedNode'] && ( tree['PositionLevel'] == 1 ) ){
                setSelectedNodes.delete(tree['Id']);
            }
            else{
               if (tree.hasOwnProperty('items')) {
                  for (let n of tree['items']) {
                    innerFunc( n );
                  }
                }
            }
        }
        innerFunc ( tree );
        return Array.from(setSelectedNodes).join(',');
    },
    
   

    markVisitedNodes : function(tree,finalNodes){
         function _recMarkVisitedNodes(tree, finalNodes) {
            tree['isVisible'] = finalNodes.has(tree['Id']) ? true : false;
            if (tree.hasOwnProperty('items')) {
              for (let n of tree['items']) {
                _recMarkVisitedNodes(n, finalNodes);
              }
            }
          }
          _recMarkVisitedNodes (tree, finalNodes);
          return tree;
    },

    searchTreeNodes : function (tree,searchKey){
        /*
        * METHOD TO SEARCH NODES
        * FIND ALL NODES IN CONTAINS
        * MARK ALL CHILD NODES AS VISITED IS PARENT IS SELECTED
        * DFS TRAVERSAL
        */
        var finalNodesSet = new Set();
        let isParentFound = false;
        let parentNodeId;
        let isChildFound = false;
        var currentDepth = 0;

        var parentNodesStack = new Array();
        var finalNodesSet = new Set();
        var idx = -1;
        var len = -2;
        var foundChildNode = false;

          function _recSearchNodes(tree,idx,len) {
            var isFound = tree['Name'].toLowerCase().includes(searchKey.toLowerCase());
            //currentDepth = tree['PositionLevel'];
            //var isSearchable = tree['searchable'];
            parentNodesStack.push(tree['Id']);
            if( (idx + 1 == len ) && ( tree['PositionLevel'] == "1" ) && !foundChildNode && !isFound ) {
                parentNodesStack.pop();
            }else if(isFound){
                finalNodesSet = new Set(parentNodesStack);
                foundChildNode = true;
            }
            else if( tree['PositionLevel'] == "1" && !isFound ){
                parentNodesStack.pop();
            }
             
            if (tree.hasOwnProperty('items')) {
             for (idx = 0;idx < tree['items'].length; idx++) {
                 len = tree['items'].length;
                 tree['items'][idx]['ParentNodeId'] = tree['Id'];
                 foundChildNode = false;
                _recSearchNodes(tree['items'][idx],idx,len);
              }
            }
            // if(currentDepth > 1 ){
            //     isChildFound = false;
            // }

            // if( (idx + 1 == len ) && ( tree['PositionLevel'] == "1" ) && !isFound ) {
            //     finalNodesSet.delete(parentNodeId);
            // }

            // if( currentDepth > 1 && ( !isFound && !isParentFound) && isSearchable ){
            //     finalNodesSet.add(tree['Id']);
            //     isParentFound = false;
            // }

            // if( currentDepth > 1 && isFound  && isSearchable){
            //     finalNodesSet.add(tree['Id']);
            //     isParentFound = true;
            // }

            // if( currentDepth == 1 && isFound && isSearchable){
            //     finalNodesSet.add(tree['Id']);
            //     finalNodesSet.add(parentNodeId);
            //     isChildFound = true;

            // }else if(isChildFound){
            //     isChildFound = true;
            // }

            // if( currentDepth == 1 && !isParentFound && !isFound && !isChildFound && isSearchable){
            //     finalNodesSet.delete(parentNodeId);
            // }else if(currentDepth == 1 && !isParentFound && isFound && isSearchable){
            //     finalNodesSet.add(parentNodeId);
            // }

            // if(isParentFound && isSearchable){
            //     finalNodesSet.add( tree['Id']);
            // }
           
            // if (tree.hasOwnProperty('items')) { 
            //     parentNodeId = tree['Id'];
            //     for (idx = 0;idx < tree['items'].length; idx++){
            //         len = tree['items'].length;
            //          _recSearchNodes(tree['items'][idx],isParentFound,parentNodeId,idx,len);
            //     }
            // }
          }

        //START WITH ROOT NODE AND TRAVERSE THROUGH EACH LEVEL 
        //TOP DOWN TRAVERSAL
        _recSearchNodes (tree,idx,len);
        return finalNodesSet;
    },


    activatePositonProductNodes : function(component,tree,setPositionIds,enableProductCheck){

        var selNodes = component.get('v.setSelectedNodes');
        var setSelectedNodes = new Set();
        if( selNodes != undefined && selNodes != null && selNodes.length > 0 ){
                setSelectedNodes = new Set(selNodes.split(','));
        }
        
        function _activatePositonProductNodes(tree) {
            if(tree['PositionLevel'] == 1 && enableProductCheck){
                tree['isNodeDisabled'] =  !setPositionIds.has( tree['Id'] );  
                if(setSelectedNodes.has(tree['Id']) && tree['isNodeDisabled'] ){
                    setSelectedNodes.delete(tree['Id']);
                    tree['isSelectedNode'] = false;
                }
            }else{
                 tree['isNodeDisabled'] = false;
            }
           
            if (tree.hasOwnProperty('items')) {
              for (let n of tree['items']) {
                _activatePositonProductNodes(n);
              }
            }
          }

            //if( selNodes != undefined && selNodes != null && selNodes.length > 0 ){
              //  setSelectedNodes = new Set(selNodes.split(','));

            //}else{
                //component.set('v.selectedNodesCount', 0);
            //} 

            _activatePositonProductNodes(tree);

        component.set( "v.setSelectedNodes", Array.from(setSelectedNodes).join(','));
        component.set( "v.selectedNodesCount",setSelectedNodes.size);

        return [tree];
    },


})