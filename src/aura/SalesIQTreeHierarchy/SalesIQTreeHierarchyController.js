({
    getNamespace : function(component,event,helper)
    {
        var action = component.get("c.getOrgNamespace");

        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var ns = response.getReturnValue();
                component.set("v.namespace",ns);
            }    
        });

        action.setBackground();
        $A.enqueueAction(action);   

    },

    refreshTreeHierarchy : function(component, event, helper) 
    {
        component.set("v.nodes",{});
        var params = event.getParam('arguments');
        if(params) {
            component.set("v.countryId", params.countryId);
        }        

        helper.getTreeRecords(component,event);
    },

    closeTreeHierarchy : function(component,event,helper)
    {
        
        //document.getElementById("treeSearchBox").value = "";
        var globalId = component.getGlobalId();
        document.getElementById(globalId + "_treeContainer").className = 'slds-hide';
    },

    openTreeHierarchy : function(component,event,helper)
    {
        // Code change to use plain javascript rather than JQuery to improve performance
        var finalHeight=0;
        var globalId = component.getGlobalId();
        document.getElementById(globalId + "_treeContainer").className = 'slds-tree_container' ;
          var searchKey =  document.getElementById("treeSearchBox").value ;
        console.log('value is',searchKey);
      if((searchKey != undefined || searchKey !='') && searchKey.length == 0)
           $A.util.addClass(component.find('clearIcon'), "slds-hide");
      else
          $A.util.removeClass(component.find('clearIcon'), "slds-hide");    

        if(document.querySelectorAll('.slds-tree__item.tree_Item.slds-is-selected').length == 0) {
            if(component.get("v.moduleName") == 'Call Plan') {
                var allItems = document.querySelectorAll('.slds-tree .slds-tree__item');
                for(var i=0; i<allItems.length; i++) {
                    if(allItems[i].firstChild.className.indexOf("slds-button") == -1) {
                        allItems[i].className = 'slds-tree__item tree_Item slds-grid slds-grid_vertical-align-center slds-is-selected';
                        break;
                    }
                }
            } else {
                if(document.querySelectorAll('.slds-tree .slds-tree__item')[0] != undefined)
                    document.querySelectorAll('.slds-tree .slds-tree__item')[0].className = 'slds-tree__item tree_Item slds-grid slds-grid_vertical-align-center slds-is-selected';
            }
        }
        var headerList = document.getElementsByClassName("forceListViewManagerHeader");
        var i=0;
        if(headerList.length > 1 && headerList != undefined) 
        {
            for(;i<headerList.length;i++)
            {
                if(headerList[i].offsetHeight > 0)
                {
                    break;
                }
            }   
        }

        //var headerHeight = jQuery(".forceListViewManagerHeader").outerHeight();
        var contentHeight, headerHeight;

        if(document.getElementsByClassName("forceListViewManagerHeader") != undefined && document.getElementsByClassName("forceListViewManagerHeader")[i] != undefined){
          headerHeight = document.getElementsByClassName("forceListViewManagerHeader")[i].offsetHeight;
        }
        if(component.get("v.moduleName") == 'Position Universe')
        {
            //var contentHeight = jQuery(".cSalesIQPositionUniverse").eq(1).height();
            
            
            if(component.get("v.namespace") != '') {
                //contentHeight = jQuery(".AxtriaSalesIQTMSalesIQPositionUniverse").eq(1).height();
                if(document.getElementsByClassName("AxtriaSalesIQTMSalesIQPositionUniverse") != undefined && document.getElementsByClassName("AxtriaSalesIQTMSalesIQPositionUniverse")[1] != undefined)
                {
                    contentHeight = document.getElementsByClassName("AxtriaSalesIQTMSalesIQPositionUniverse")[1].offsetHeight;
                }
                        
            
            }
            else
            {
                if(document.getElementsByClassName("cSalesIQPositionUniverse") != undefined && document.getElementsByClassName("cSalesIQPositionUniverse")[1] != undefined){
                  contentHeight = document.getElementsByClassName("cSalesIQPositionUniverse")[1].offsetHeight;

                }
            }

            //finalHeight = contentHeight - ( headerHeight - 58);
            //if(finalHeight <= 0)
            finalHeight = 290;
        }
        else if(component.get("v.moduleName") == 'Employee Universe')
        {
            //var contentHeight = jQuery("#positiondialog #modal-datatable").outerHeight();
            var contentHeight = document.getElementById("positiondialog").childNodes[0].childNodes[1].offsetHeight;
            finalHeight = contentHeight - (headerHeight + 98);
            if(navigator.userAgent.match(/Trident.*rv\:11\./))
            {
                finalHeight = finalHeight - 5;
            }
            if(finalHeight <= 0)
            {
                if(navigator.userAgent.match(/Trident.*rv\:11\./))
                {
                    finalHeight = 210;
                }
                else
                    finalHeight = 228 ;
            }
        }
        else if(component.get("v.moduleName") == 'Call Plan')
        {
            
            // headerHeight = document.getElementsByClassName("callPlanHeaderCard")[0].offsetHeight;

            if(component.get("v.namespace") != '') 
            {
                if(!!navigator.userAgent.match(/Trident.*rv\:11\./))
                {
                    headerHeight =  document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[3].offsetHeight + document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[4].offsetHeight;
                }
                else
                {
                    headerHeight =  document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[2].offsetHeight + document.getElementsByClassName("AxtriaSalesIQTMSalesIQDataTable_V2")[3].offsetHeight;
                }
                contentHeight = document.getElementsByClassName("AxtriaSalesIQTMSalesIQCallPlan")[1].offsetHeight;
            }
            else
            {
                if(!!navigator.userAgent.match(/Trident.*rv\:11\./))
                {
                    headerHeight =  document.getElementsByClassName("cSalesIQDataTable_V2")[3].offsetHeight + document.getElementsByClassName("cSalesIQDataTable_V2")[4].offsetHeight;
                }
                else
                {
                    headerHeight =  document.getElementsByClassName("cSalesIQDataTable_V2")[2].offsetHeight + document.getElementsByClassName("cSalesIQDataTable_V2")[3].offsetHeight;
                }
                
                contentHeight = document.getElementsByClassName("cSalesIQCallPlan")[1].offsetHeight;
            }
            finalHeight = contentHeight - headerHeight;
            
            if(finalHeight <= 0)
                finalHeight = 310 ;
        }

       document.getElementById(globalId + "_treeUL").style.height = finalHeight+'px' ;

  	},
    
     clearSearch :function(component,event,helper)
    {
        var globalId = component.getGlobalId();
        console.log('value in',document.getElementById("treeSearchBox").value);
        document.getElementById("treeSearchBox").value = "";
        $A.util.addClass(component.find('clearIcon'), "slds-hide");
        component.clearSearch();
    },

  	searchTree: function(component, event,helper) 
  	{
        var searchKey =  document.getElementById("treeSearchBox").value ;
        console.log('value is',searchKey);
         if((searchKey != undefined || searchKey !='') && searchKey.length == 0)
        {
            $A.util.addClass(component.find('clearIcon'), "slds-hide");
        }
        else
        {
            $A.util.removeClass(component.find('clearIcon'), "slds-hide");
        }

		var myEvent = $A.get("e.c:treeSearchEvent");
        myEvent.setParams({
        	"searchKey": searchKey
        });
        myEvent.fire();
  	},


    searchKeyChange: function(component, event, helper) 
    {        
        if(component.get('v.isSearchInPlace')){
            var timer = component.get('v.timer');
            clearTimeout(timer);
            function doneTyping () {
                console.log('called in place search');
                component.set('v.nodes',[ helper.markVisitedNodes( component.get('v.initNodes')[0], helper.searchTreeNodes(component.get('v.initNodes')[0],event.getParam("searchKey")) ) ]);
            }

            var timer = setTimeout(function(){
                doneTyping();
                clearTimeout(timer);
                component.set('v.timer', null);
            }, 200);
            component.set('v.timer', timer);  
   
        }
        else
        {

        console.log('tree hierarchy searchKeyChange'+ component.get("v.countryId"))
		var count = 1;
        var ns = component.get("v.namespace");
        if(ns == null)
            ns = '';
		var searchKey = event.getParam("searchKey");
        console.log('key is',searchKey);
         if(searchKey != undefined && searchKey.length == 0)
        {
            $A.util.addClass(component.find('clearIcon'), "slds-hide");
        }
        else
        {
            $A.util.removeClass(component.find('clearIcon'), "slds-hide");
        }
	    var action = component.get("c.findByName");
	    action.setParams({
	      "searchKey": searchKey,
          "selectedTeamInstance" : component.get('v.selectedTeamInstance'),
          "moduleName" : component.get("v.moduleName"),
          "countryId" : component.get("v.countryId")
 	    });

        action.setCallback(this, function(response) 
        {

            var selNodes = component.get('v.setSelectedNodes');
            var setSelectedNodes = new Set();

            if( selNodes != undefined && selNodes != null && selNodes.length > 0 ){
                setSelectedNodes = new Set(selNodes.split(','));
                component.set('v.selectedNodesCount', setSelectedNodes.size);  
            }else{
                component.set('v.selectedNodesCount', 0);
            }            

            var roles = {}, results = [],pendingCRMap;
            if(component.isValid() && response.getState() === "SUCCESS") 
            {
                pendingCRMap = response.getReturnValue().pendingCRMap;
                results = response.getReturnValue().positionList;
                roles[undefined] = { Name: "Root", items: [], Level : '' , RGB:'', count:count, PositionLevel : '',pendingCRList : []};
                var levelVal = 0 ;
                count = count+1;
                var ns = component.get("v.namespace");

                results.forEach(function(v) {
                    if(levelVal < parseInt(v[ns+'Hierarchy_Level__c']))
                        levelVal = parseInt(v[ns+'Hierarchy_Level__c']);
                });
                levelVal = levelVal + 1;
                component.set("v.topLevel",levelVal);

                results.forEach(function(v) {
                    var colorRgb = v[ns+'RGB__c'];
                    if(colorRgb != undefined) {
                        var r = colorRgb.split(",")[0];
                        var g = colorRgb.split(",")[1];
                        var b = colorRgb.split(",")[2];
                        colorRgb = r + ',' +  g + ',' + b;
                    }

                    var isSelectedNode;
                    if( setSelectedNodes != undefined && setSelectedNodes != null )
                        isSelectedNode = setSelectedNodes.has(v.Id);



                    if(parseInt(v[ns+'Hierarchy_Level__c']) == 0)
                        roles[v.Id] = { Name: v[ns+'Client_Territory_Name__c'], items: [], Level : levelVal - (1), RGB: colorRgb, Id : v.Id, count:count, PositionLevel : 1, pendingCRList : pendingCRMap[v.Id], isSelectedNode : isSelectedNode,isVisible : true};
                    else
                        roles[v.Id] = { Name: v[ns+'Client_Territory_Name__c'], items: [], Level : levelVal - (parseInt(v[ns+'Hierarchy_Level__c'])), RGB: colorRgb, Id:v.Id, count:count, PositionLevel : v[ns+'Hierarchy_Level__c'], pendingCRList : pendingCRMap[v.Id], isSelectedNode : isSelectedNode,isVisible : true};


                    count = count+1;
                });
                results.forEach(function(v) {
                    if(v[ns+'Parent_Position__c'] != undefined && roles[v[ns+'Parent_Position__c']] != undefined){
                        roles[v[ns+'Parent_Position__c']].items.push(roles[v.Id]);
                    } else {
                      roles[undefined].items.push(roles[v.Id]); 
                    }
                });
                component.set("v.nodes", roles[undefined].items);
            } 
            else 
            {
                var namespace = component.get("v.namespace");
                var errorLabel;
                if(namespace != '')
                {
                    errorLabel = $A.get("$Label.AxtriaSalesIQTM.Unexpected_error_notification");
                }
                else
                {
                    errorLabel = $A.get("$Label.c.Unexpected_error_notification");
                }

                var errors = response.getError();
                var errorMessage = errors[0].message;
                
                if (errors[0] && errors[0].message) 
                {
                    var action = component.get("c.logException");
        
                    action.setParams({
                        message : errorMessage,
                        module : component.get("v.moduleName")
                    });

                    action.setCallback(this,function(response)
                    {
                        var state = response.getState();
                        if(state === 'SUCCESS')
                        {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                mode: 'dismissible',
                                message: errorLabel,
                                type : 'error'
                                
                            });
                            toastEvent.fire();
                        }
                    });
                    $A.enqueueAction(action);
                } 
                else 
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: errorLabel,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                }
            }
        });
    
            $A.enqueueAction(action);

        }
    },

    breadcrumbChange1 : function(component,event,helper)
    {
        var selectedTeamInstance = component.get('v.selectedTeamInstance');
        //event.getParam("teamInstanceId"); 

        var namespace = component.get("v.namespace");
        var allpositiontext;
        if(namespace != '')
        {
            allpositiontext = $A.get("$Label.AxtriaSalesIQTM.All_Positions");

        }
        else
        {
            allpositiontext = $A.get("$Label.c.All_Positions");
            
        }
        //allpositiontext = allpositiontext.replace(' ','');
       // allpositiontext = allpositiontext.toLowerCase();
        console.log('allpositiontext ::'+allpositiontext);
        console.log('allpositiontext ::'+selectedTeamInstance);
        //if(selectedTeamInstance.indexOf(allpositiontext) == -1 ){
            component.set("v.selectedTeamInstance",selectedTeamInstance); 
            helper.getTreeRecords(component,event);
            jQuery('#treeSearchBox').val('');
        //}
    },

    breadcrumbChange : function(component,event,helper)
    {
        var selectedTeamInstance =  event.getParam("teamInstanceId"); 

        var namespace = component.get("v.namespace");
        var allpositiontext;
        if(namespace != '')
        {
            allpositiontext = $A.get("$Label.AxtriaSalesIQTM.All_Positions");

        }
        else
        {
            allpositiontext = $A.get("$Label.c.All_Positions");
            
        }
        //allpositiontext = allpositiontext.replace(' ','');
       // allpositiontext = allpositiontext.toLowerCase();
        console.log('allpositiontext ::'+allpositiontext);
        console.log('allpositiontext ::'+selectedTeamInstance);
        //if(selectedTeamInstance.indexOf(allpositiontext) == -1 ){
            component.set("v.selectedTeamInstance",selectedTeamInstance); 
            helper.getTreeRecords(component,event);
            jQuery('#treeSearchBox').val('');
        //}
    },

    updateSelectedNodes :function(component,event,helper){
        var selectednode =  event.getParam("selectedValuesString");
        var isSelectedNode = event.getParam("isSelectedNode");
        helper.markChildrenNodes(component,helper,selectednode,isSelectedNode);
    },

    updateNodesOnProductSelection :function(component,event,helper){
        var productMap = component.get('v.positionToProductMap');
        var selectedProductId = component.get('v.selectedProductId');
        var enableProductCheck = ( selectedProductId == undefined || selectedProductId == null || selectedProductId == '' ) ? false : true;
        var setPositionIds = new Set( productMap[ selectedProductId ] );    
        component.set( 'v.nodes', helper.activatePositonProductNodes(component, component.get('v.nodes')[0], setPositionIds, enableProductCheck ));
    },

    updateTree : function (component){
        //SHOW SELECTED NODES AND THEIR PARENTS
        console.log(' update tree called ::::'); 
        var selNodes = component.get('v.setSelectedNodes');
        var setSelectedNodes = new Set();

        if( selNodes != undefined && selNodes != null && selNodes.length > 0 ){
            setSelectedNodes = new Set(selNodes.split(','));
            component.set('v.selectedNodesCount', setSelectedNodes.size);   
        }else{
            component.set('v.selectedNodesCount', 0);
        }   

        function showSelectedNodes(tree,setSelectedNodes){
            var parentNodesStack = new Array();
            var finalNodesSet = new Set();
            var idx = -1;
            var len = -2;
            var foundChildNode = false;
          function _recShowSelectedNodes(tree, setSelectedNodes,idx,len) {
            parentNodesStack.push(tree['Id']);
            if( (idx + 1 == len ) && ( tree['PositionLevel'] == "1" ) && !foundChildNode &&  !(setSelectedNodes.has(tree['Id'])) ) {
                parentNodesStack.pop();
             
            }else if( setSelectedNodes.has(tree['Id']) ){
                finalNodesSet = new Set(parentNodesStack);
                foundChildNode = true;
            }
            else if( tree['PositionLevel'] == "1" && !setSelectedNodes.has(tree['Id']) ){
                parentNodesStack.pop();
            }
             
            if (tree.hasOwnProperty('items')) {
             for (idx = 0;idx < tree['items'].length; idx++) {
                 len = tree['items'].length;
                 tree['items'][idx]['ParentNodeId'] = tree['Id'];
                 foundChildNode = false;
                _recShowSelectedNodes(tree['items'][idx], setSelectedNodes,idx, len);
              }
            }
          }
          _recShowSelectedNodes (tree, setSelectedNodes,idx, len);
          return markVisitedNodes(tree,finalNodesSet);
        }
    
        function  markVisitedNodes(tree,finalNodesSet){
          function _recMarkVisitedNodes(tree, finalNodesSet) {
            tree['isVisible'] = finalNodesSet.has(tree['Id']) ? true : false;
            tree['searchable'] = tree['isVisible'];
            if (tree.hasOwnProperty('items')) {
              for (let n of tree['items']) {
                _recMarkVisitedNodes(n, finalNodesSet);
              }
            }
          }
          _recMarkVisitedNodes (tree, finalNodesSet);
          return tree;
        }
        
        component.set("v.nodes",[ showSelectedNodes( component.get('v.initNodes')[0] ,setSelectedNodes) ]);
        
    }
})