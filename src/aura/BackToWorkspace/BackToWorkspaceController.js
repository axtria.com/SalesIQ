({
	doinit : function(component, event, helper) {
        var currentState = history.state;
        history.pushState(currentState, "Workspaces" , "" );
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:SalesIQWorkspace",
            isredirect : true
        });
        evt.fire();
	}
})