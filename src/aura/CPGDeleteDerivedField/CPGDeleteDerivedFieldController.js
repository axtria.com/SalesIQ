({

    doInit: function(component, event, helper) {
    component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0 !important;}   .forceStyle .viewport.oneHeader.desktop {z-index:0 !important;} .forceStyle.desktop .viewport{overflow:hidden}");
    },

    yesClicked : function(component, event, helper) {
    component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
    if(component.get("v.derivedFieldType")=="Category"){
      var ruleId = component.get("v.businessRuleId");
      var scenarioInsId = component.get("v.scenarioRuleInstanceId");
      var action = component.get("c.DeleteCategoryBusinessRule");
      action.setParams({ "businessRuleId" : ruleId,"scenarioRuleInstanceDetailsId":scenarioInsId, "columnName":component.get('v.columnName') });
      action.setCallback(this, function(response){ 
      var state = response.getState();
      if(component.isValid() && state==="SUCCESS")
      {
       var arrRuleTypes=response.getReturnValue();
       component.set('v.arrRuleTypes',arrRuleTypes);
       component.handleResponse();

      }
        else{
        console.log("Failed with state:  "+ state);
        }
      })
      $A.enqueueAction(action);
    }
    if(component.get("v.derivedFieldType")=="Computation"){
    component.deleteCompute();
    }  
    if(component.get("v.derivedFieldType")=="Rank"){
    component.deleteRank();
    } 
    if(component.get("v.derivedFieldType")=="CallBalancing"){
    component.deleteCallBalancingRule();
    }
    },


    noClicked : function(component, event, helper) {
      component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
      if(component.get("v.derivedFieldType")=="CallBalancing"){
      var evt = $A.get("e.force:navigateToComponent");
      evt.setParams({
      componentDef : "c:CPGCallPlanBalancingRules",
      componentAttributes: {
         
         "scenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId"),
         workSpaceId: component.get("v.workSpaceId"),
         "namespace":component.get("v.namespace"),
         summarizedViewFlag : component.get("v.summarizedViewFlag")
      }
      });
      evt.fire();
      }else{
      var evt = $A.get("e.force:navigateToComponent");
      evt.setParams({
      componentDef : "c:CPGDerivedFieldsList",
      componentAttributes: {
         "scenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId"),
         workSpaceId: component.get("v.workSpaceId"),
         "namespace":component.get("v.namespace"),
         summarizedViewFlag : component.get("v.summarizedViewFlag")
      }
      });
      evt.fire();
      }
    },

    deleteComputationField	: function(component, event, helper) { 
      component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");  
      var ruleId = component.get("v.businessRuleId");
      var scenarioInsId = component.get("v.scenarioRuleInstanceId");
      var action = component.get("c.DeleteComputationType");
      action.setParams({ 
      'businessRuleId' : ruleId,
      'scenarioRuleInstanceDetailsId' : scenarioInsId,
      'columnName':component.get('v.columnName'),
      summarizedViewFlag : component.get("v.summarizedViewFlag") 
      });
      action.setCallback(this, function(response){ 
      var state = response.getState();
        if(component.isValid() && state==="SUCCESS")
        {
         var arrRuleTypes=response.getReturnValue();
         component.set('v.arrRuleTypes',arrRuleTypes);
         component.handleResponse();
        }
        else{
        console.log("Failed with state:  "+ state);
        }
      })
      $A.enqueueAction(action);
    },

    handleResponse:function(component,event,helper){
        var opts=[];
        var arrRuleTypes=JSON.parse(JSON.stringify(component.get('v.arrRuleTypes')) ) ;
        if(arrRuleTypes[0].lstData.length>0){
            console.log(arrRuleTypes[0].lstData[0].Id);
            for(var ctr=0;ctr<arrRuleTypes[0].lstData.length;ctr++){
              if(arrRuleTypes[0].lstData[ctr].Id){
              opts.push({Id:arrRuleTypes[0].lstData[ctr].Id});
              } 
            }
            component.deletionError(opts);
        }else{
              if(component.get("v.derivedFieldType")=="CallBalancing"){
                var evt = $A.get("e.force:navigateToComponent");
                evt.setParams({
                componentDef : "c:CPGCallPlanBalancingRules",
                componentAttributes: { 
                  scenarioRuleInstanceId : component.get("v.scenarioRuleInstanceId"),
                  workSpaceId: component.get("v.workSpaceId"),
                  "namespace":component.get("v.namespace"),
                  summarizedViewFlag : component.get("v.summarizedViewFlag")
               }
               });
               evt.fire();
              }else{
                console.log("Deleted Computation Field Successfully");
                var evt = $A.get("e.force:navigateToComponent");
                evt.setParams({
                componentDef : "c:CPGDerivedFieldsList",
                componentAttributes: { 
                   "scenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId"),
                   workSpaceId: component.get("v.workSpaceId"),
                   "namespace":component.get("v.namespace"),
                   summarizedViewFlag : component.get("v.summarizedViewFlag")
               }
               });
               evt.fire();
              }
         
        }   

    },

deleteRankField : function(component, event, helper) { 
    component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");  
    var ruleId = component.get("v.businessRuleId");
    var scenarioInsId = component.get("v.scenarioRuleInstanceId");
    var action = component.get("c.deleteRankFields");
    action.setParams({ 
    'businessRuleId' : ruleId,
    'scenarioRuleInstanceDetailsId' : scenarioInsId,
    'columnName':component.get('v.columnName') 
    });
    action.setCallback(this, function(response){ 
    var state = response.getState();
    if(component.isValid() && state==="SUCCESS")
      {
      var arrRuleTypes=response.getReturnValue();
      component.set('v.arrRuleTypes',arrRuleTypes);
      component.handleResponse();
      }
      else{
      console.log("Failed with state:  "+ state);
      }
    })
    $A.enqueueAction(action);
},

deleteCallBalancingRule : function(component, event, helper) { 
    component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");  
    var ruleId = component.get("v.businessRuleId");
    var scenarioInsId = component.get("v.scenarioRuleInstanceId");
    var action = component.get("c.deleteCallBalanceRule");
    action.setParams({ 
    'businessRuleId' : ruleId,
    'scenarioRuleInstanceDetailsId' : scenarioInsId
    });
    action.setCallback(this, function(response){ 
    var state = response.getState();
      if(component.isValid() && state==="SUCCESS")
      {
      var arrRuleTypes=response.getReturnValue();
      component.set('v.arrRuleTypes',arrRuleTypes);
      component.set('v.componentName','CallBalancingRule');
      component.handleResponse();
      }
      else{
      console.log("Failed with state:  "+ state);
      }
    })
    $A.enqueueAction(action);
},

deletionErrorMsg : function(component, event, helper) {  
    console.log("Inside deletionErrorMsg method"); 
    var params = event.getParam('arguments');
    if (params) {
      var param1 = params.param1;
      var BRids=[];
      for(var ctr=0;ctr<param1.length;ctr++){
        if(param1[ctr].Id){
          BRids.push(param1[ctr].Id); 
        } 
      }
      var action = component.get("c.deleteComputationErrorRules");
      action.setParams({ 
      strJsonbusinessRuleIds : BRids.join(',')

      });
      action.setCallback(this, function(response){ 
      var state = response.getState();
      if(component.isValid() && state==="SUCCESS")
      {
        var BUNamesList=[];
        var arrListValues=response.getReturnValue();
        for(var ctr=0;ctr<arrListValues.length;ctr++){

        BUNamesList.push(arrListValues[ctr].brName);

      }
      component.set("v.BUNames",BUNamesList);
      var evt = $A.get("e.force:navigateToComponent");
      evt.setParams({
      componentDef : "c:CPGDeletionErrorMeesage",
      componentAttributes: {
      "componentName": component.get("v.componentName"),
      "scenarioRuleInstanceId":   component.get("v.scenarioRuleInstanceId"), 
      "BRName":   component.get("v.BUNames"),
      workSpaceId: component.get("v.workSpaceId"),
      "namespace":component.get("v.namespace"),
      summarizedViewFlag : component.get("v.summarizedViewFlag")
      }
      });
      evt.fire();

      }
      else{
      console.log("Failed with state:  "+ state);
      }
      })
      $A.enqueueAction(action);
    }
},


})