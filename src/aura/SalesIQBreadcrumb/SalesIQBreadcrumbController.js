({
    doInit: function(component, event, helper) 
    {
        //component.getNamespace();
        //console.log("doInit called : breadcrumb  " + component.get("v.countryId"));

        var namespace = component.get("v.namespace");
        var allpositiontext;
        if(namespace != '')
        {
            allpositiontext = $A.get("$Label.AxtriaSalesIQTM.All_Positions");

        }
        else
        {
            allpositiontext = $A.get("$Label.c.All_Positions");
            
        }
        //allpositiontext = allpositiontext.replace(' ','');
        //allpositiontext = allpositiontext.toLowerCase();
        console.log('allpositiontext in breadcrumb ::'+allpositiontext);
        component.set("v.allPositionText",allpositiontext);
        
        var params = event.getParam('arguments');
        if(params) {
            component.set("v.countryId", params.countryId);
        }
        var country = component.get("v.countryId");
        

        if(country == '' || country == null || country == undefined){
            console.log("country id is not present : breadcrumb");
            return;
        }
        
        console.log("country id is present : breadcrumb");
        var action = component.get("c.getBreadcrumbTeamInstance");
        action.setParams
        ({
          "moduleName" : component.get("v.moduleName"),
          "countryId" : country
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS")
            {
                var ns = component.get("v.namespace");

                //MapString to listString
                var itemsSample = response.getReturnValue();   
                var listTeam = [];
                var teamToteamInstance = [];
                
                Object.keys(itemsSample).forEach(function(key){
                    listTeam.push(key);
                });

                for(var i =0; i<listTeam.length; i++)
                {
                    var teamInstancesNames = [];
                    for(var j =0;j<itemsSample[listTeam[i]].length;j++)
                    {
                        console.log(itemsSample[listTeam[i]][j]);

                        if(component.get("v.moduleName") == 'Call Plan') 
                        {
                            teamInstancesNames.push(itemsSample[listTeam[i]][j][ns+'Scenarios__r'][0][ns+'Scenario_Name__c']);
                            // SPD-4442
                            // + ' ' + itemsSample[listTeam[i]][j]['Name']
                        } 
                        else 
                        {
                            if(itemsSample[listTeam[i]][j][ns+'Scenario__c'] == null || itemsSample[listTeam[i]][j][ns+'Scenario__c'] == undefined)
                            {
                                teamInstancesNames.push(itemsSample[listTeam[i]][j]['Name']);
                            }
                            else
                            {
                                teamInstancesNames.push(itemsSample[listTeam[i]][j][ns+'Scenario__r'][ns+'Scenario_Name__c']);
                                // SPD-4442
                                // + ' ' + itemsSample[listTeam[i]][j]['Name']
                            }
                        }
                        
                    }
                    teamToteamInstance.push({ "team" : listTeam[i],"teamName":itemsSample[listTeam[i]][0][ns+'Team__r']['Name'] ,"teamInstances" : itemsSample[listTeam[i]] , "teamInstancesNames" : teamInstancesNames});

                }
                var selectedTeamInstance = component.get("v.selectedTeamInstance");

                var idx1 = 0;
                var idx2 = 0;
                if(selectedTeamInstance != '' || selectedTeamInstance != null)
                {
                    selectedTeamInstance = selectedTeamInstance.split('_')[1];
                    console.log('selectedTeamInstance : '+selectedTeamInstance);
                    console.log('teamToteamInstance ',teamToteamInstance);
                    for(var i = 0;i<teamToteamInstance.length;i++)
                    {
                        if(teamToteamInstance[i].team == selectedTeamInstance){
                            idx1 = i;
                        }
                        /*for(var j = 0;j<teamToteamInstance[i].teamInstances.length;j++)
                        {
                            console.log('-->' + teamToteamInstance[i].teamInstances[j].Id);
                            if(selectedTeamInstance == teamToteamInstance[i].teamInstances[j].Id)
                            {
                                idx1 = i;
                                idx2 = j;
                                break;

                            }
                        }*/
                    }
                }
                component.set("v.items", listTeam);     
                component.set("v.teamInstances", teamToteamInstance);


                var allpositionViewAction = component.get("c.getAllPositionViewCustomSetting");
                allpositionViewAction.setCallback(this,function(response)
                {
                    var state = response.getState();
                    if(state === 'SUCCESS')
                    {
                        console.log('-- return :'+response.getReturnValue());
                        component.set("v.showOnlyAllPosition",response.getReturnValue());
                    }
                });
                $A.enqueueAction(allpositionViewAction);
              
                var allpositiontext = component.get("v.allPositionText");
                var teamInstances = component.get("v.teamInstances");
                component.set("v.selectedTeamInstance",allpositiontext);
                component.set("v.selectedTeam",teamInstances[idx1].team);
                component.set("v.selectedTeamName",teamInstances[idx1].teamName);
                if(component.get("v.moduleName") != 'Call Plan')
                    component.set("v.selectedTeamInstanceName",allpositiontext);
                else{
                    component.set("v.selectedTeamInstanceName",teamInstances[idx1].teamInstancesNames[0]);
                }
                component.set("v.selectedTeamInstanceId", allpositiontext+'_'+teamInstances[idx1].team);


            }
            else if (state === "ERROR" || response.getReturnValue == null)
            {
                
                var namespace = component.get("v.namespace");
                var errorLabel,uapErrorLabel;
                if(namespace != '')
                {
                    errorLabel = $A.get("$Label.AxtriaSalesIQTM.Unexpected_error_notification");
                    uapErrorLabel = $A.get("$Label.AxtriaSalesIQTM.No_UAP_Assigned");
                }
                else
                {
                    errorLabel = $A.get("$Label.c.Unexpected_error_notification");
                    uapErrorLabel = $A.get("$Label.c.No_UAP_Assigned");
                }


                var errors = response.getError();
                var errorMessage = errors[0].message;
                
                if(errorMessage.indexOf(uapErrorLabel) != -1)
                {
                    return;
                }
                if (errors[0] && errors[0].message) 
                {
                    var action = component.get("c.logException");
        
                    action.setParams({
                        message : errorMessage,
                        module : component.get("v.moduleName")
                    });

                    action.setCallback(this,function(response)
                    {
                        var state = response.getState();
                        if(state === 'SUCCESS')
                        {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                mode: 'dismissible',
                                message: errorLabel,
                                type : 'error'
                                
                            });
                            toastEvent.fire();
                        }
                    });
                    $A.enqueueAction(action);
                } 
                else 
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: errorLabel,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                }
            }
        })
        
        $A.enqueueAction(action); 

    },

    getNamespace : function(component,event,helper)
    {

        var namespace = component.get("v.namespace");
        var allpositiontext;
        if(namespace != '')
        {
            allpositiontext = $A.get("$Label.AxtriaSalesIQTM.All_Positions");

        }
        else
        {
            allpositiontext = $A.get("$Label.c.All_Positions");
            
        }
        //allpositiontext = allpositiontext.replace(' ','');
        //allpositiontext = allpositiontext.toLowerCase();
        console.log('allpositiontext in breadcrumb ::'+allpositiontext);
        component.set("v.allpositiontext",allpositiontext+'');

        var action = component.get("c.getOrgNamespace");

        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var ns = response.getReturnValue();
                component.set("v.namespace",ns);
            }    
        });
        $A.enqueueAction(action);   
    },

    onTeamInstanceSelection : function(component,event,helper){
    	var teamInstances = component.get("v.teamInstances");

    	var instancetarget = event.target.id;
        var idx1;
        var idx2;
    	if(instancetarget != undefined)
    	{
            var namespace = component.get("v.namespace");
            var allpositiontext;
            if(namespace != '')
            {
                allpositiontext = $A.get("$Label.AxtriaSalesIQTM.All_Positions");

            }
            else
            {
                allpositiontext = $A.get("$Label.c.All_Positions");
                
            }
            //allpositiontext = allpositiontext.replace(' ','');
            //allpositiontext = allpositiontext.toLowerCase();
            console.log('allpositiontext ::'+allpositiontext);
            if(instancetarget.indexOf(allpositiontext) != -1){
                var teamIndex = instancetarget.split(allpositiontext+'_')[1];
                component.set("v.selectedTeamInstance",allpositiontext+'_'+teamInstances[teamIndex].team);
                component.set("v.selectedTeam",teamInstances[teamIndex].team);
                component.set("v.selectedTeamName",teamInstances[teamIndex].teamName);
                component.set("v.selectedTeamInstanceName",allpositiontext);
                console.log('Breadcrumb event fire : countryId:--> ' + component.get('v.countryId'));
                var evt = $A.get("e.c:breadcrumbChangeEvent");
                    evt.setParams({
                    teamInstanceId : allpositiontext+'_'+teamInstances[teamIndex].team,
                    countryId : component.get('v.countryId')
                });           
                evt.fire();
            }
            else{
        		idx1 = instancetarget.split('_')[1];
        		idx2 = instancetarget.split('_')[2];

                component.set("v.selectedTeamInstance",teamInstances[idx1].teamInstances[idx2]);
                component.set("v.selectedTeam",teamInstances[idx1].team);
                component.set("v.selectedTeamName",teamInstances[idx1].teamName);
                component.set("v.selectedTeamInstanceName",teamInstances[idx1].teamInstancesNames[idx2]);
                console.log('Breadcrumb event fire : countryId:--> ' + component.get('v.countryId'));
                var evt = $A.get("e.c:breadcrumbChangeEvent");
                    evt.setParams({
                    teamInstanceId : teamInstances[idx1].teamInstances[idx2].Id,
                    countryId : component.get('v.countryId')
                });           
                evt.fire();
            }
    	}
    },
    
    handlepositionEvent : function(component, event,helper) {
          
        var params = event.getParam('arguments');
        var recordId = params.recordId;

        var selectedTeamInstance = component.get("v.selectedTeamInstance");
        var action1 = component.get("c.getParentPosition");
        action1.setParams({
            positionid : recordId,
            selectedTeamInstance: selectedTeamInstance.Id,
            moduleName : component.get("v.moduleName"),
            countryId : component.get("v.countryId")
        });
        
        action1.setCallback(this, function(response){
            var state = response.getState();
            var moduleName = component.get("v.moduleName");
            if(component.isValid() && state==="SUCCESS")
            { 
                var positionNamesMap = [];
                var positionList = response.getReturnValue().reverse();
                for(var i = 0;i<positionList.length;i++)
                {
                	var truncatedValue = "";
                	console.log('moduleName' + moduleName);
                	if(positionList[i].length>15 && moduleName=="Employee Universe")
                    {
	                	truncatedValue = positionList[i].substr(0,5) +" ... "+positionList[i].substr(positionList[i].length-5,positionList[i].length);
	                }else if(positionList[i].length>15 && moduleName=="Position Universe"){
	                	truncatedValue = positionList[i].substr(0,5) +" .... "+positionList[i].substr(positionList[i].length-5,positionList[i].length);
	                }else if(positionList[i].length>15 && moduleName== 'Call Plan'){
                        truncatedValue = positionList[i].substr(0,5) +" .... "+positionList[i].substr(positionList[i].length-5,positionList[i].length);
                    }
                    else{
	                	truncatedValue = positionList[i];
	                }
	                positionNamesMap.push({"key":truncatedValue,"value":positionList[i]});
                }
                component.set("v.positionnames", positionNamesMap);
            }
            else
            {
                var errors = response.getError();
                var errorMessage = errors[0].message;
                var namespace = component.get("v.namespace");
                var errorLabel = '';
                if(component.get("v.namespace") != '') 
                {
                    errorLabel = $A.get("$Label.AxtriaSalesIQTM.Unexpected_error_notification");
                }
                else
                {
                    errorLabel = $A.get("$Label.c.Unexpected_error_notification");
                }
                if (errors[0] && errors[0].message) 
                {
                    var action = component.get("c.logException");
        
                    action.setParams({
                        message : errorMessage,
                        module : component.get("v.moduleName")
                    });

                    action.setCallback(this,function(response)
                    {
                        var state = response.getState();
                        if(state === 'SUCCESS')
                        {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                mode: 'dismissible',
                                message: errorLabel,
                                type : 'error'
                                
                            });
                            toastEvent.fire();
                        }
                    });
                    $A.enqueueAction(action);
                } 
                else if (state === "ERROR" || response.getReturnValue == null)
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: errorLabel,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                }
            }    
        })
        $A.enqueueAction(action1); 
    },

    closeBreadcrumb : function(component,event,helper)
    {
        var breadcrumb = component.find('breadcrumb');
        $A.util.addClass(breadcrumb,'slds-hide');
    },

    openBreadcrumb : function(component,event,helper)
    {
        var breadcrumb = component.find('breadcrumb');
        $A.util.removeClass(breadcrumb,'slds-hide');
    },

    onTeamSelection : function(component,event,helper)
    {
        /*var teamlist = component.find('teamlist');
        $A.util.toggleClass(teamlist,"openteamlist");*/

        console.log('--- onTeamSelection -- ');

        document.getElementById('teamlist').classList.add('openteamlist');
        component.ShowDropdown(event);
    },

    onHoverShowDropdown: function(component, event, helper) {

        try {
            
            var $j = jQuery.noConflict();
            setTimeout(function(){ 
                $j("#magicDropDownContainerL").css('visibility', 'visible');
                if ($j("#magicDropDownContainerL").hasClass('slds-hide')) {
                    $j("#magicDropDownContainerL").removeClass('slds-hide');
                }
             }, 200);
        
            


        } catch (e) {

        }
    },


    onTeamLeave: function(component, event, helper) {


        var is_ipad = (navigator.platform == 'iPad') ? true : false;
        var $j = jQuery.noConflict();

        if (!is_ipad)
            $j("#magicDropDownContainerL").css('visibility', 'hidden');
    },

    onHoverTeam : function(component, event, helper){
        
        try{
            var $j = jQuery.noConflict();
            var divid = event.currentTarget.getAttribute('id');
            var div_no = divid.split('_')[1];

            if (div_no != undefined && div_no != '') {
                $j('#wrap_' + div_no).css({
                    "top": $j(event.currentTarget).closest("li")
                        .position().top,
                    "left": "180px"
                });
            }

        }catch(e){
            console.log(e);
        }  
        
    },

    closeTeamDropDown : function(component,event,helper)
    {
        try{
            document.getElementById('teamlist').classList.remove('openteamlist');
        }catch(e){
            //console.log(e);
        }
       
    },

    updateCountryAttributes : function(component, event, helper){
        console.log('updateCountryAttributes event : breadcrumb :'+event.getParam("countryId"));
        component.set("v.countryId", event.getParam("countryId"));
        component.initialize();
    },
})