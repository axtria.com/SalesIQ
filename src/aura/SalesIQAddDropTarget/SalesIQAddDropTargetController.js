({
    //$Label.c.Account_Universe
    //$Label.c.Aligned_Approved_Customer
    //$Label.c.Team_Universe
    //$Label.AxtriaSalesIQTM.Account_Universe
    //$Label.AxtriaSalesIQTM.Aligned_Approved_Customer
    //$Label.AxtriaSalesIQTM.Team_Universe

    doInit : function(component,event,helper)
    {
        var action = component.get("c.getOrgNamespace");

        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var ns = response.getReturnValue();
                component.set("v.namespace",ns);
                var action = component.get("c.getModuleOptions");
                action.setParams({
                    "selectedTeamInstance" : component.get("v.selectedTeamInstance"),
                    "moduleName" : component.get("v.moduleName")
                });
                action.setCallback(this, function(response)
                {
                    var state = response.getState();
                    if (component.isValid() && state === "SUCCESS" && response.getReturnValue() != null) 
                    {
                        var namespace = component.get("v.namespace");
                        console.log(response.getReturnValue());
                        var retResponse = response.getReturnValue();
                        var subInterfaceOptions = [];
                        Object.keys(retResponse).forEach(function(key) 
                        {
                            var labelReference;
                            if(namespace != '')
                            {
                                labelReference = $A.getReference("$Label.AxtriaSalesIQTM." + retResponse[key]);
                            }
                            else
                            {
                                labelReference = $A.getReference("$Label.c." + retResponse[key]);
                            }
                            component.set("v.tempLabelAttr", labelReference);
                            var dynamicLabel = component.get("v.tempLabelAttr");
                            var subInterfaceOption = {'value' : key, 'label':dynamicLabel};
                            subInterfaceOptions.push(subInterfaceOption);
                        });  
                        console.log('add drop init');
                        console.log(subInterfaceOptions);
                        component.set("v.subInterfaceOptions",subInterfaceOptions);
                    }
                    else if (state === "ERROR" || response.getReturnValue() == null) 
                    {
                        console.log('ERROR Add drop init');   
                    }
                });
                $A.enqueueAction(action);
            }    
        });


        action.setBackground();
        $A.enqueueAction(action); 
    },

    getNamespace : function(component,event,helper)
    {
        var action = component.get("c.getOrgNamespace");

        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var ns = response.getReturnValue();
                component.set("v.namespace",ns);
            }    
        });


        action.setBackground();
        $A.enqueueAction(action);   

    },

    refreshUniverse : function(component, event, helper){
        component.set("v.updatedRecs", '');
        component.set("v.selectedCheckboxList", []);
    	
        var addDropComponent = component.find("addDropTable");
        addDropComponent.initializeDataTable();
    },

    /*updateCountryAttributes : function(component, event, helper){
        var addDropDataTable = component.find('addDropTable');
        var params = event.getParam('arguments');
        var countryId = '';
        if(params)
            countryId = params.countryId;
        addDropDataTable.updateCountryAttributes(countryId);
    },*/

    saveTargets : function(component, event, helper){
        var namespace = component.get("v.namespace");
        var checkLock = component.get("c.isScenarioLock");
        checkLock.setParams({"scenarioId" : component.get("v.scenarioID")});
        checkLock.setCallback(this,function(response){
            if(response.getReturnValue() == true || response.getReturnValue() == 'true'){
                var message;
                if(namespace != '' && namespace != undefined) 
                    message = $A.get("$Label.AxtriaSalesIQTM.Business_Rule_Running_Warning");
                else
                    message = $A.get("$Label.c.Business_Rule_Running_Warning");

                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    mode: 'dismissible',
                    type: 'Warning',
                    message: message,
                    messageTemplate: message,
                    messageTemplateData: []
                });
                toastEvent.fire();
                console.log('toastEvent', toastEvent);
                return;
            }
            console.log(component.get("v.selectedCheckboxList"));
            console.log('selectedposition--->' + component.get("v.selectedPosition"));
            console.log('selectedposition--->' + component.get("v.selectedTeamInstance"));

            console.log('updatedRecs--->');
            console.log(component.get("v.updatedRecs"));
            if(component.get("v.selectedCheckboxList").length == 0)
            {
                var warningIcon = component.find('warning-icon');
                var toast = component.find("error_toast");

                $A.util.removeClass(toast,'slds-hide');
                $A.util.removeClass(warningIcon,'slds-hide');
                return;
            }
            
            var datatableComp = component.find("addDropTable");
            var mandatoryFieldsMessage = '';
            if(datatableComp != undefined)
            {
                datatableComp.validateMandatoryRules();
                mandatoryFieldsMessage = datatableComp.get("v.mandatoryFieldsErrorMessage");
                console.log('error message is :'+mandatoryFieldsMessage);
                if(mandatoryFieldsMessage != '')
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        type: 'error',
                        message: mandatoryFieldsMessage,
                        messageTemplate: mandatoryFieldsMessage,
                        messageTemplateData: []
                    });
                    toastEvent.fire();

                    return;
                }
                else
                {
                    if(component.get("v.selectedCheckboxList").length == 0)
                    {
                        var warningIcon = component.find('warning-icon');
                        var toast = component.find("error_toast");

                        $A.util.removeClass(toast,'slds-hide');
                        $A.util.removeClass(warningIcon,'slds-hide');
                        return;
                    }
                    else{
                        var warningIcon = component.find('warning-icon');
                        var toast = component.find("error_toast");

                        $A.util.addClass(toast,'slds-hide');
                        $A.util.addClass(warningIcon,'slds-hide');
                    }

                    var pendingRecords = component.get("c.submissionPendingRecords");
                    pendingRecords.setParams({
                        "selectedTeamInstanceId" : component.get("v.selectedTeamInstance"),
                        "selectedPositionId" : component.get("v.selectedPosition")
                    });

                    pendingRecords.setCallback(this, function(response)
                    {
                        var state = response.getState();
                        if (component.isValid() && state === "SUCCESS") 
                        {
                            if((component.get("v.selectedCheckboxList").length + response.getReturnValue()) > component.find("addDropTable").get("v.columnStructure").maximumRecordsSaved){
                                var toastMessage = $A.get("$Label.c.Permissible_Save_Limit_Call_Plan")
                                if(component.get("v.namespace") != '')
                                    toastMessage = $A.get("$Label.AxtriaSalesIQTM.Permissible_Save_Limit_Call_Plan")

                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    mode: 'dismissible',
                                    type: 'error',
                                    message: toastMessage,
                                    messageTemplate: toastMessage,
                                    messageTemplateData: ['' + component.find("addDropTable").get("v.columnStructure").maximumRecordsSaved + '']
                                });
                                toastEvent.fire();
                                return;
                            }

                            var updateTargets = component.get("c.updateTargets");
                            updateTargets.setParams({
                                "subInterface" : component.get("v.selectedSubInterface"),
                                "updatedRecs" : JSON.stringify(component.get("v.updatedRecs")),
                                "selectedRecords" : component.get("v.selectedCheckboxList"),
                                "selectedPositionId" : component.get("v.selectedPosition"),
                                "selectedTeamInstanceId" : component.get("v.selectedTeamInstance")
                            });
                        
                        
                            updateTargets.setCallback(this, function(response){
                                var state = response.getState();
                                if (component.isValid() && state === "SUCCESS") 
                                {
                                    console.log('saveAction--->');
                                    console.log(response.getReturnValue());
                                    var evt = component.getEvent("refreshDataTable");
                                    evt.setParams({
                                        calledFromEvent : true
                                    });           
                                    evt.fire();
                                    var evt = component.getEvent("refreshKpiEvent");           
                                    evt.fire();
                                    component.dropPopup();

                                    var toastEvent = $A.get("e.force:showToast");
                                    toastEvent.setParams({
                                        mode: 'dismissible',
                                        message: response.getReturnValue(),
                                        type : 'Success'
                                        
                                    });
                                    toastEvent.fire();
                                }
                                else if (state === "ERROR" || response.getReturnValue() == null) 
                                {
                                    helper.logException(component, response.getError());
                                }
                            });
                            $A.enqueueAction(updateTargets);
                        }
                        else if (state === "ERROR" || response.getReturnValue() == null) 
                        {
                            helper.logException(component, response.getError());
                        }
                    });
                    $A.enqueueAction(pendingRecords);
                }
            }
        });
        $A.enqueueAction(checkLock);    
    },

    showpositionpopupfunction : function(component,event,helper)
    {
        
        //var addDropDataTable = component.find('addDropTable');
        var params = event.getParam('arguments');
        var countryId = '';
        if(params)
            countryId = params.countryId;
        
        component.set("v.countryId", countryId);
        //addDropDataTable.updateCountryAttributes(countryId);

        component.reloadData();
        helper.toggleClass(component,'backdrop','slds-backdrop--');
        helper.toggleClass(component,'addTargetModal','slds-fade-in-');

    },

    droppopupfunction : function(component,event,helper)
    {   
        helper.toggleClassInverse(component,'backdrop','slds-backdrop--');
        helper.toggleClassInverse(component,'addTargetModal','slds-fade-in-');
        component.set("v.showModal", false);
    },

    closeToast : function(component,event,helper)
    {
        var toast = component.find("error_toast");
        $A.util.addClass(toast,'slds-hide');
    },

    hideShowErrorPopup: function(component)
    {
        var toast = component.find("error_toast");
        $A.util.toggleClass(toast,'slds-hide');
    }
})