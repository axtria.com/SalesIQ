({
	doInit : function(component, event, helper) {
	component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0 !important;}   .forceStyle .viewport.oneHeader.desktop {z-index:0 !important;} .forceStyle.desktop .viewport{overflow:hidden}");
       var BRNameValues =  component.get("v.BRName");
        component.set("v.BRules",BRNameValues);
	},
    
   okClicked : function(component, event, helper){
    component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
    var componentName = component.get('v.componentName');
    if(componentName=='CallBalancingRule'){

          var evt = $A.get("e.force:navigateToComponent");
                    evt.setParams({
                    componentDef : "c:CPGCallPlanBalancingRules",
                    componentAttributes: { 
                      scenarioRuleInstanceId : component.get("v.scenarioRuleInstanceId"),
                      workSpaceId: component.get("v.workSpaceId"),
                      namespace:component.get("v.namespace")
                   }
                   });
          evt.fire();
    }else{
        var evt = $A.get("e.force:navigateToComponent");
             evt.setParams({
                componentDef : "c:CPGDerivedFieldsList",
                componentAttributes: {
                      
                     "scenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId"),
                     workSpaceId: component.get("v.workSpaceId"),
                      namespace:component.get("v.namespace")
                }
            });
            evt.fire();
  }
  }
})