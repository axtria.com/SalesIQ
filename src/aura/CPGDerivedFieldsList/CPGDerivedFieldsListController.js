({
doInit: function(component, event, helper) {  
    component.showSpinner(); 
    component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0 !important;}   .forceStyle .viewport.oneHeader.desktop {z-index:0 !important;} .forceStyle.desktop .viewport{overflow:hidden}");
    component.getTableName();
    component.createDataTablecall();
},
    createDataTable : function(component, event, helper){
         
        var html='<div class="slds-button-group" role="group">';
        html=html+'<button class="slds-button slds-button_icon slds-button_icon-border-filled edit" aria-pressed="false" title="Edit">';
        html=html+'<span class="slds-icon_container slds-icon-utility-edit" data-aura-rendered-by="138:0"><span data-aura-rendered-by="141:0" class="lightningPrimitiveIcon" data-aura-class="lightningPrimitiveIcon">';
        html=html+'<div class="icon-edit"></div>';
        html=html+'</span></span><span class="slds-assistive-text">Edit</span>';
        html=html+'</button>';
        html=html+'<button class="slds-button slds-button_icon slds-button_icon-border-filled delete" aria-pressed="false" title="Delete">';
        html=html+'<span class="slds-icon_container slds-icon-utility-delete" data-aura-rendered-by="145:0"><span data-aura-rendered-by="148:0" class="lightningPrimitiveIcon" data-aura-class="lightningPrimitiveIcon">';
        html=html+'<div class="icon-delete"></div></span><!--render facet: 149:0--></span><span class="slds-assistive-text">Delete</span>'; 
        html=html+'</button>';
        html=html+'</div>';
        var opts = [];
        var action = component.get("c.getDerivedFieldListData");
        action.setParams({ "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(response.getReturnValue()!== null && state==="SUCCESS"){
                var arrListValues=response.getReturnValue();
                if(arrListValues.length>0){
                    for(var ctr=0;ctr<arrListValues.length;ctr++){
                        opts.push({Rule:arrListValues[ctr].businessRuleName,
                                   FieldName:arrListValues[ctr].paramName ,
                                   Type: arrListValues[ctr].businessRuleTypeMasterName,
                                   DataType:arrListValues[ctr].paramDataType,
                                   ValueType:arrListValues[ctr].paramType, BusinessRuleId:arrListValues[ctr].businessRuleId});
                    }
                }
                if(opts.length>0){
                    var sessionTable;
                    var $j = jQuery.noConflict();
                    var arrHtml=[];
                    var result = opts;
                    var coulmnJsonResult = [];
                    
                    var element = result[0]; 
                    for(var key in element){
                        if(key == 'BusinessRuleId'){
                            coulmnJsonResult.push({"data":key,"title":key,defaultContent:'', visible:false});
                        }else
                            coulmnJsonResult.push({"data":key,"title":key,defaultContent:''});
                        
                    }
                    coulmnJsonResult.push({"title":' ', defaultContent :' '});
                    var columnNames=[];
                    
                    if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result)){
                        var columnsIndex =0 ;
                        for(var columnsCtr in result[0]){
                            columnNames.push(columnsCtr);
                            columnsIndex++;
                            
                        }
                    }
                    console.log( $j('#table-1'));
                    sessionTable = $j('#table-1').DataTable(
                        {
                            "processing": false,
                            "dom": '<"div-search"><"div-tbl">t<"bottom-info"> ', // f search, p :- pagination , l:- page length 
                            "data": result,
                            paging:false,
                            //"order" : [[1,"asc"]],
                            scrollX : true,
                            scrollY:  "300px",
                            destroy : true,
                            //"ordering": true,
                            scrollCollapse: true,
                            "ordering": false,
                            
                            "language":
                            {
                                "emptyTable": "Loading Data"
                            },
                            
                            buttons: [{
                                extend : 'csv',
                                text : '',
                                title : 'Call Plan',  
                            }],
                            columns:coulmnJsonResult,
                            "columnDefs": [
                                {  "targets": 'no-sort'  },
                                
                               /* {
                                     targets : [0,1,2,3,4],
                                    "createdCell":function (td, cellData, rowData, row, col)
                                    {
                                        var selectHtml = '<div class="tooltip">'+cellData+'<span class="tooltiptext">'+cellData+'</span></div>';
                                        $j(td).html(selectHtml); 
                                        //$j(td).width('50px');
                                        
                                    }
                                  
                                  },*/
                                {
                                    targets : 6,
                                    "createdCell":function (td, cellData, rowData, row, col)
                                    { 
                                        $j(td).html(html); 
                                        $j(td).width('50px');
                                        
                                    }
                                    
                                }
                                
                            ],
                            "createdRow": function ( row, data, index ) 
                            {
                                $j(row).addClass('slds-hint-parent');
                                $j('td', row).addClass('slds-text-align_left slds-truncate');
                                $j('thead > tr> th').addClass(' slds-text-title_caps');
                                $j('thead > tr').addClass('slds-text-title_caps ');
                                
                            } 
                        });  
                    
                    component.set("v.sessionTable",sessionTable);
                    $j("#table-1 .edit").on("click", function(){
                        
                        var idx = sessionTable.cell( $j(this).parents('td') ).index().row;
                        var row = sessionTable.rows(idx).data();
                        console.log(row[0].BusinessRuleId);
                        var brid = row[0].BusinessRuleId;
                        var type = row[0].Type;
                        console.log("type"+type);
                        if(type == 'Category'){
                            var scenRuleInstID = component.get("v.scenarioRuleInstanceId");
                            component.navigateToCategory(brid,scenRuleInstID); 
                        }
                        if(type == 'Rank'){
                            var scenRuleInstID = component.get("v.scenarioRuleInstanceId");
                            component.navigateToRank(brid,"Update",scenRuleInstID);
                            
                        }
                        if(type == 'Compute'){
                            component.navigate(brid,"Update");}
                    });
                    
                    $j("#table-1 .delete").on("click", function(){
                        var scenRuleInstID = component.get("v.scenarioRuleInstanceId");
                        var idx = sessionTable.cell( $j(this).parents('td') ).index().row;
                        var row = sessionTable.rows(idx).data();
                        console.log(row[0]);
                        var brid = row[0].BusinessRuleId;
                        var type = row[0].Type;
                        var columnName=row[0].FieldName;
                        if(type == 'Category'){ 
                            component.navigateToDeleteCategaory(brid,scenRuleInstID,columnName);
                        }
                        if(type == 'Compute'){ 
                            component.navigateToDeleteCompute(brid,scenRuleInstID,columnName);
                            
                        }
                        if(type == 'Rank'){ 
                            var scenRuleInstID = component.get("v.scenarioRuleInstanceId");
                            component.navigateToDeleteRank(brid,scenRuleInstID,columnName);
                            
                        }
                        
                    });
                    
                    
                }
                else{
                    var tableVisibility = component.find("tableAuraId1");
                    $A.util.addClass(tableVisibility,'slds-hide');
                    component.set("v.textMsg","No Data Found");
                }
                
            }
            else{
                console.log("Failed with state:  "+ state);
            }
            component.hideSpinner();
        });
        $A.enqueueAction(action);
        
    },

navigateToComputation :function(component, event, helper) {  

    console.log("inside navigate component");
    var evt = $A.get("e.force:navigateToComponent");
    evt.setParams({
    componentDef : "c:CPGComputationDerivedField",
    componentAttributes: {
    mode:   "Create",
    scenarioRuleInstanceId: component.get("v.scenarioRuleInstanceId"), 
    workSpaceId: component.get("v.workSpaceId"),
    namespace:component.get("v.namespace")
    }
    });
    evt.fire();

},

navigateToComputationUpdate :function(component, event, helper) {  
    
    var params = event.getParam('arguments');
    if (params) {
    var param1 = params.param1;
    var param2 = params.param2;
    var evt = $A.get("e.force:navigateToComponent");
    evt.setParams({
    componentDef : "c:CPGComputationDerivedField",
    componentAttributes: {
    mode:   param2,
    scenarioRuleInstanceId: component.get("v.scenarioRuleInstanceId"),
    businessRuleTypeId: param1,
    workSpaceId: component.get("v.workSpaceId"),
    namespace:component.get("v.namespace")
    }
    });
    evt.fire();
    }
},

closeButton : function(component, event, helper) {
    component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
    var sceId=component.get("v.scenarioId"); 
    var action = component.get("c.getScenarioID");
    action.setParams({ "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});

    action.setCallback(this, function(response){ 
    var state = response.getState();
    if(component.isValid() && state==="SUCCESS")
    {

    var arrListValues=response.getReturnValue();
    var evt = $A.get("e.force:navigateToComponent");
    evt.setParams({
    componentDef : "c:CPGbusinessRuleCanvas",
    componentAttributes: {
    record_Id : arrListValues[0][common.getKey(component,"Scenario_Id__c")],
    viewcanvas : "Y",
    workSpaceId : component.get("v.workSpaceId"),
    summarizedViewFlag : component.get("v.summarizedViewFlag")
    }
    });
    evt.fire();

    }
    else{
    console.log("Failed with state:  "+ state);
    }
    });
    $A.enqueueAction(action); 
},


navigateToCategoryUpdate :function(component, event, helper) {  

    var params = event.getParam('arguments');
    if (params) {
    var param1 = params.param1;
    var param2 = params.param2;

    // add your code here
    var evt = $A.get("e.force:navigateToComponent");
    evt.setParams({
    componentDef : "c:CPGCallPlanning",
    componentAttributes: {

    businessRuleId: param1,
    scenarioRuleInstanceId  :param2,
    workSpaceId: component.get("v.workSpaceId"),
    namespace:component.get("v.namespace")
    }
    });
    evt.fire();
    }

},

navigateToRankUpdate :function(component, event, helper) { 
    
    var params = event.getParam('arguments');
    if (params) {
    var param1 = params.param1;
    var param2 = params.param2;
    var param3 = params.param3;
    var evt = $A.get("e.force:navigateToComponent");
    evt.setParams({
    componentDef : "c:CPGRankingCreateDerivedField",
    componentAttributes: {
    mode:   param2,
    businessRuleTypeId: param1,
    scenarioRuleInstanceId :param3,
    workSpaceId: component.get("v.workSpaceId"),
    namespace:component.get("v.namespace")
    }
    });
    $A.get('e.force:refreshView').fire(); 
    evt.fire();
    }

},

navigateToDeleteFieldCategaory :function(component, event, helper) {  
    var params = event.getParam('arguments');
    if (params) {
    var param1 = params.param1;
    var param3 = params.param3;
    var evt = $A.get("e.force:navigateToComponent");
    evt.setParams({
    componentDef : "c:CPGDeleteDerivedField",
    componentAttributes: {
    businessRuleId: param1,
    derivedFieldType : "Category",
    scenarioRuleInstanceId : component.get("v.scenarioRuleInstanceId"),
    columnName:param3,
    workSpaceId: component.get("v.workSpaceId"),
    namespace:component.get("v.namespace")
    }
    });
    evt.fire();
    }

},

navigateToDeleteComputationField :function(component, event, helper) {  

    console.log("inside navigateToDeleteFieldCategaory component");
    var params = event.getParam('arguments');
    if (params) {
    var param1 = params.param1;
    var param2 = params.param2;
    var param3 = params.param3;
    var evt = $A.get("e.force:navigateToComponent");
    evt.setParams({
    componentDef : "c:CPGDeleteDerivedField",
    componentAttributes: {
    businessRuleId: param1,
    derivedFieldType : "Computation",
    scenarioRuleInstanceId : param2,
    columnName:param3,
    workSpaceId: component.get("v.workSpaceId"),
    namespace:component.get("v.namespace")
    }
    });
    evt.fire();

    }

},

navigateToDeleteRankField : function(component, event, helper) {  

    console.log("inside navigateToDeleteRankField component");
    var params = event.getParam('arguments');
    if (params) {
    var param1 = params.param1;
    var param2 = params.param2;
    var param3 = params.param3;
    var evt = $A.get("e.force:navigateToComponent");
    evt.setParams({
    componentDef : "c:CPGDeleteDerivedField",
    componentAttributes: {

    //scenarioId : component.get("v.scenarioId"),
    businessRuleId: param1,
    derivedFieldType : "Rank",
    scenarioRuleInstanceId : param2,
    columnName:param3,
    workSpaceId: component.get("v.workSpaceId"),
    namespace:component.get("v.namespace")
    }
    });
    evt.fire();
    }

},

getTableName : function(component, event, helper) {

    if(component.get("v.scenarioRuleInstanceId")==="" || component.get("v.scenarioRuleInstanceId")==='undefined'){
    component.set("v.tableName","No Table Found");
    }else{
      var action = component.get("c.getInputTableName");
      action.setParams({"scenarioRuleInstanceDetailsId": component.get("v.scenarioRuleInstanceId")}); 
      action.setCallback(this, function(response){
      var state = response.getState();
      if(component.isValid() && state==="SUCCESS")
      {
      var itemsSample = response.getReturnValue();
        if(itemsSample==="" || itemsSample==="undefined"){
        component.set("v.tableName","");
        } else{  
        component.set("v.tableName",itemsSample[0][common.getKey(component,"Table_Display_Name__c")]);
        } 
      }else{
        console.log("Failed with state:  "+ state);
        }
      });
      $A.enqueueAction(action);
    }
},

showSpinner: function(component, event, helper) {
// make Spinner attribute true for display loading spinner 
component.set("v.Spinner", true); 
},

// this function automatic call by aura:doneWaiting event 
hideSpinner : function(component,event,helper){
// make Spinner attribute to false for hide loading spinner    
component.set("v.Spinner", false);
},
destoryCmp : function (component, event, helper) {
 //var sessionTable=component.get("v.sessionTable");   
 //sessionTable.destroy();   
 component.destroy();
}


});