({
    navigateToDelete:function(component, event, helper,businessRuleId,ruleName,ruleType) {  
    
     console.log("inside navigateToDelete helper");
            
	            var evt = $A.get("e.force:navigateToComponent");
		     	evt.setParams({
		        componentDef : "c:CPGDeleteDerivedField",
		        componentAttributes: {
		             businessRuleId: businessRuleId,
		             derivedFieldType : 'CallBalancing',
		             scenarioRuleInstanceId : component.get("v.scenarioRuleInstanceId"),
                     columnName:ruleName,
                     workSpaceId: component.get("v.workSpaceId"),
                    namespace:component.get("v.namespace")
		        }
		    });
		    evt.fire();
       
    
    },
})