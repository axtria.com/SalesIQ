({
    initPage : function(component, event, helper){ 
        console.log('Inside init');
        component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0 !important;}   .forceStyle .viewport.oneHeader.desktop {z-index:0 !important;} .forceStyle.desktop .viewport{overflow:hidden}");  
        component.showSpinner();
        var action = component.get("c.getcallBalancingRuleName");
        action.setParams({ 
            "scenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
        action.setCallback(this, function(data){
            
            var response= data.getReturnValue();  
            component.set("v.callBalancingRuleWrapper",response);
            console.log(component.get("v.callBalancingRuleWrapper"));
            component.createDataTable();
            component.hideSpinner();
        })
        $A.enqueueAction(action);
    },
    balancingRuleTab : function(component, event, helper) {
        var tab1 = component.find('balacingRuleAuraId');
        var Tab1Data = component.find('balancingRuleId');
        var tab2 = component.find('balancingConfAuraId');
        var Tab2Data = component.find('balancingConfId');
        
        $A.util.addClass(tab1, 'slds-is-active');
        $A.util.removeClass(Tab1Data, 'slds-hide');
        $A.util.addClass(Tab1Data, 'slds-show');
        
        $A.util.removeClass(tab2, 'slds-is-active');
        $A.util.removeClass(Tab2Data, 'slds-show');
        $A.util.addClass(Tab2Data, 'slds-hide');
        
        
    },
    balancingConfTab : function(component, event, helper) {
        var tab1 = component.find('balacingRuleAuraId');
        var Tab1Data = component.find('balancingRuleId');
        var tab2 = component.find('balancingConfAuraId');
        var Tab2Data = component.find('balancingConfId');
        
        $A.util.removeClass(tab1, 'slds-is-active');
        $A.util.addClass(tab2, 'slds-is-active');
        $A.util.addClass(Tab1Data, 'slds-hide');
        $A.util.removeClass(Tab1Data, 'slds-show');
        $A.util.addClass(Tab2Data, 'slds-show');
        var isCallPlanBalancingCalled=component.get("v.isCallPlanBalancingCalled");
        if(!isCallPlanBalancingCalled){
            var childId = component.find('callBalanceConfigId');
            childId.LoadCallPlanBalancingRules();
            isCallPlanBalancingCalled=true;
            component.set("v.isCallPlanBalancingCalled",isCallPlanBalancingCalled);
        }        
    },
    validateExistingGuardRails: function(component, event, helper){
         var action = component.get("c.ValidateGuardRails");
        action.setParams({ "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
        
        action.setCallback(this, function(response){ 
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS"){
                var data=response.getReturnValue();
                if(data){
                   component.balancingConfTab();
                }
                else{
                    //pooja to add
                    //alert('Aggregation and Guard Rails are mandatory');
                    component.set("v.popoverMessageError","Aggregation and Guard Rails are mandatory");
                    var errorIcn = component.find("errorIcon");
                    $A.util.removeClass(errorIcn,'slds-hide'); 
                    return false;
                }
            }
        })
        $A.enqueueAction(action);
        
    },
    createDataTable : function(component, event, helper)
    {
        
        console.log('Inisde Call Plan Balancing createDataTable');
        var sessionTable;
        var $j = jQuery.noConflict();
        var result = component.get("v.callBalancingRuleWrapper");
        
        var parsedResult = JSON.parse(JSON.stringify( result, ["businessRuleId","ruleName","ruleType","level"] , 4));
        console.log(parsedResult);
        
        console.log(result);
        var coulmnJsonResult = [];
        var element = parsedResult[0];
        
        for(var key in element){
            if(key=='ruleName')
                coulmnJsonResult.push({"data":key,"title":'Rule Name',defaultContent:''});
            if(key=='ruleType')
                coulmnJsonResult.push({"data":key,"title":'Rule Type',defaultContent:''});
            if(key=='level')
                coulmnJsonResult.push({"data":key,"title":'Level',defaultContent:''});
        }
        coulmnJsonResult.push({"data":'',"title":'',defaultContent:''});
        var html='<div class="slds-button-group" role="group">';
        html+='<button class="slds-button slds-button_icon slds-button_icon-border-filled edit" aria-pressed="false" title="Edit">';
        html+='<span class="slds-icon_container slds-icon-utility-edit" data-aura-rendered-by="138:0"><span data-aura-rendered-by="141:0" class="lightningPrimitiveIcon" data-aura-class="lightningPrimitiveIcon">';
        html+='<div class="icon-edit"></div>';
        html+='</span><!--render facet: 142:0--></span><span class="slds-assistive-text">Edit</span>';
        html+='</button>';
        html+='<button class="slds-button slds-button_icon slds-button_icon-border-filled delete" aria-pressed="false" title="Delete">';
        html+='<span class="slds-icon_container slds-icon-utility-delete" data-aura-rendered-by="145:0"><span data-aura-rendered-by="148:0" class="lightningPrimitiveIcon" data-aura-class="lightningPrimitiveIcon">';
        html+='<div class="icon-delete"></div>';
        html+='</span><!--render facet: 149:0--></span><span class="slds-assistive-text">Delete</span>';
        html+='</button>';
        html+='</div>';
        if(result && result.length>0){
            sessionTable = $j('#table-1').DataTable(
                {
                    
                    "search": false,
                    destroy : true,
                    stateSave: true,
                    "processing": false,
                    "dom": '<"div-tbl">t<"bottom-info"> ', // f search, p :- pagination , l:- page length 
                    "data": parsedResult,
                    paging:false,
                    "ordering": true,
                    scrollX : true,
                    scrollY: "325px",
                    "responsive": true,
                    colReorder: {
                        order: [2,3,1]
                    },
                    scrollCollapse: true,
                    "language":
                    {
                        "emptyTable": "Loading Data"
                    },
                    
                    buttons: [{
                        extend : 'csv',
                        text : '',
                        title : 'Call Plan',  
                    }],
                    columns:coulmnJsonResult,
                    "columnDefs": [
                        { "orderable": false, "targets": [1,2,3]  },
                        {
                            targets : 3,
                            "createdCell":function (td, cellData, rowData, row, col)
                            {
                                
                                $j(td).html(html);
                            }
                        }
                    ],
                    "createdRow": function ( row, data, index ) 
                    {
                        $j(row).addClass('slds-hint-parent');
                        $j('td', row).addClass('slds-text-align_left slds-truncate');
                        $j('thead > tr> th').addClass('slds-is-sortable slds-is-resizable slds-text-title_caps slds-line-height_reset');
                        $j('thead > tr').addClass('slds-text-title_caps ');
                        
                    },
                    
                    
                });
            $j("#table-1 .delete").on("click", function(){
                console.log('Inside deelte');
                var idx = sessionTable.cell( $j(this).parents('td') ).index().row;
                console.log(idx);
                var row = sessionTable.rows(idx).data();
                console.log(row);
                var businessRuleId = row[0].businessRuleId;
                console.log(businessRuleId);
                var ruleType = row[0].ruleType;
                console.log(ruleType);
                var ruleName=row[0].ruleName;
                console.log(ruleName);
                helper.navigateToDelete(component, event, helper,businessRuleId,ruleName,ruleType);
                
                
            });  
            
            $j("#table-1 .edit").on("click", function(){
                var idx = sessionTable.cell( $j(this).parents('td') ).index().row;
                var row = sessionTable.rows(idx).data();
                var brid = row[0].businessRuleId;
                var evt = $A.get("e.force:navigateToComponent");
                evt.setParams({
                    componentDef : "c:CPGCallPlanning",
                    componentAttributes: {
                        scenarioRuleInstanceId: component.get("v.scenarioRuleInstanceId"), 
                        workSpaceId: component.get("v.workSpaceId"),
                        ComponentName: 'CallBalancing', //CallBalancing
                        businessRuleId:brid,
                        namespace: component.get("v.namespace"),
                        summarizedViewFlag : component.get("v.summarizedViewFlag") 
                    }
                });
                evt.fire();
            });
        }else{
            component.set("v.textMsg","No Data Found");
        }
        
    },
    
    closeButton : function(component, event, helper) {
        component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
        var action = component.get("c.getScenarioID");
        action.setParams({ "ScenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId")});
        
        action.setCallback(this, function(response){ 
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS"){
                var arrListValues=response.getReturnValue();
                
                var evt = $A.get("e.force:navigateToComponent");
                evt.setParams({
                    componentDef : "c:CPGbusinessRuleCanvas",
                    componentAttributes: {
                        record_Id : arrListValues[0][common.getKey(component,"Scenario_Id__c")],
                        viewcanvas : "Y",
                        workSpaceId : component.get("v.workSpaceId"),
                        namespace:component.get("v.namespace"),
                        summarizedViewFlag : component.get("v.summarizedViewFlag")
                    }
                });
                evt.fire();
                
            }else{
                console.log("Failed with state:  "+ state);
            }
        })
        $A.enqueueAction(action);
    },
    ValidateRules: function(component, event, helper) {  
        var action = component.get("c.ValidateRuleData");
        action.setParams({ "scenarioInstanceId" : component.get("v.scenarioRuleInstanceId")});
        
        action.setCallback(this, function(response){ 
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS"){
                var data=response.getReturnValue();
                if(data){
                    var evt = $A.get("e.force:navigateToComponent");
                    evt.setParams({
                        componentDef : "c:CPGCallPlanning",
                        componentAttributes: {
                            // mode:   "Create",
                            scenarioRuleInstanceId: component.get("v.scenarioRuleInstanceId"), 
                            workSpaceId: component.get("v.workSpaceId"),
                            ComponentName: 'CallBalancing' ,
                            namespace:component.get("v.namespace"), //CallBalancing
                            summarizedViewFlag : component.get("v.summarizedViewFlag")
                            //scenarioId: component.get("v.scenarioId")
                        }
                    });
                    evt.fire();
                }
                else{
                    //pooja to add
                    //alert('Aggregation and Rank rules are mandatory');
                    component.set("v.popoverMessageError","Aggregation and Rank rules are mandatory");
                    var errorIcn = component.find("errorIcon");
                    $A.util.removeClass(errorIcn,'slds-hide'); 
                    return false;
                }
            }
        })
        $A.enqueueAction(action);
    },
    addBalancingRule :function(component, event, helper) {  
        component.ValidateRules();      
        
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        console.log('Inside spinner');
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    destoryCmp : function (component, event, helper) {
        component.destroy();
    },
    handleMouseEnter : function(component, event, helper) {
        var popover = component.find("popover");
        $A.util.removeClass(popover,'slds-hide');
    },
    //make a mouse leave handler here
    handleMouseLeave : function(component, event, helper) {
        var popover = component.find("popover");
        $A.util.addClass(popover,'slds-hide');
    },
    
    
})