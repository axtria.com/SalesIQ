({
doInit : function(component, event, helper) 
{
  // $Label.AxtriaSalesIQTM.Label_All
  // $Label.c.Label_All
  component.showSpinner();
    
 // component.getTopAffiliationLevel();

  let af_textFlag = JSON.parse(component.get('v.affiliationTextFlag'));
  let af_ex_flag = JSON.parse(component.get('v.alreadyExistingRuleFlag'));

  console.log('affiliationTextFlag :'+af_textFlag);
  console.log('alreadyExistingRuleFlag :'+af_ex_flag);
  console.log('name :'+ component.get('v.rulename'));


    var treeHierarchyComp = component.find("treeHierarchy");
    var cr_id = component.get("v.countryId");
    console.log('cr_id');
    console.log(cr_id);
    treeHierarchyComp.refreshTree(component.get("v.countryId"));

    if(component.get('v.ruleLevel') == 'Team'){
      var divTree = component.find("treeHierarchy_div");
       $A.util.addClass(divTree, 'slds-hide');
      var disabledButton = component.find("disabledButton"); 
      $A.util.removeClass(disabledButton, 'slds-hide');
    }
   

    var scenarioRuleInstanceId = component.get('v.scenarioRuleInstanceId');
    console.log('scenarioRuleInstanceId :: '+scenarioRuleInstanceId);
     var action = component.get("c.getInputOutputDatasources");
    action.setParams({
      scenarioRuleInstanceId : scenarioRuleInstanceId
    });

        var namespace = component.get("v.namespace");
        
        action.setCallback(this, function(response) {
          var state = response.getState();
          if(state === 'SUCCESS'){

           var datasourcesList = response.getReturnValue();
           var dataSetMap=[];
           var outputdatasetMap = [];
           for(var i=0;i<datasourcesList.length;i++)
           {
            if(datasourcesList[i][common.getKey(component,"ds_type__c")] == 'O'){
             outputdatasetMap.push({'Id':datasourcesList[i][common.getKey(component,"dataset_id__c")],
              'tableName':datasourcesList[i][common.getKey(component,"Table_Display_Name__c")]});
           }
           else{
             dataSetMap.push({'Id':datasourcesList[i][common.getKey(component,"dataset_id__c")],
              'tableName':datasourcesList[i][common.getKey(component,"Table_Display_Name__c")]});
           }
         }

         if((dataSetMap.length == 4 || dataSetMap.length == 6) && outputdatasetMap.length == 1 && af_ex_flag )
         {

           component.set("v.dataSetMap", dataSetMap);
           component.set("v.outputdatasetMap", outputdatasetMap);

           // if(component.get())
            console.log('selectedBusinessRuleId'+component.get('selectedBusinessRuleId'));
            var action2=component.get('c.callExistingBusinessRules');
               action2.setParams({
                scenarioId : component.get('v.scenarioId'),
                scenarioRuleInstanceId : scenarioRuleInstanceId,
                selectedBusinessRuleId : component.get('v.selectedBusinessRuleId')
               });
               action2.setCallback(this, function(data){
                  var state = data.getState();
                  if(state === 'SUCCESS')
                  {
                       var updatedBRAffiliationWrapperList = data.getReturnValue();
                       if(updatedBRAffiliationWrapperList != null){
                        component.set('v.BRAffiliationWrapper',updatedBRAffiliationWrapperList); 
                        //component.set('v.rulename',updatedBRAffiliationWrapperList[0]['Name']);
                        component.set('v.alreadyExistingRuleFlag',true);
                        component.addBlockOnUpdate();
                        console.log('rule present');
                       }
                       else{
                          component.set('v.alreadyExistingRuleFlag',false);
                          console.log('rule not present');
                          helper.setAffilationAttributes(component,event,helper);
                       }
                      
                  }
                  else{
                    console.log('error '+state);
                    component.showErrorToast();
                  }

                });

               $A.enqueueAction(action2);

         }else if(!af_ex_flag && (dataSetMap.length == 4 || dataSetMap.length == 6) && outputdatasetMap.length == 1 ){
            component.set("v.dataSetMap", dataSetMap);
            component.set("v.outputdatasetMap", outputdatasetMap);
            console.log('rule not present.');
            helper.setAffilationAttributes(component,event,helper);
         }

         else
         {

           component.showErrorToast();
         } 


        if(!af_ex_flag){
          component.set('v.alreadyExistingRuleFlag',false);
          var childComponent = component.find("affilationQueryBuilder1");
             childComponent.init();
          var childComponent2 = component.find("affilationQueryBuilder2");
             childComponent2.init();
        }
      }
      else{
        console.log('error :: '+state);
      }   

    });
    $A.enqueueAction(action);

    



},

getAffiliationLevel : function(component,event,helper)
{
    var action=component.get('c.getHierarchyLevel');
    action.setParams({
    scenarioId : component.get('v.scenarioId'),
    // scenarioRuleInstanceId : scenarioRuleInstanceId
    });
    action.setCallback(this, function(data)
    {
      var state = data.getState();
      if(state === 'SUCCESS')
      {
        var opts = [];
        var allVal;
        var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
        console.log('namespace val' +namespace);
        if(namespace != ''){
              
              allVal = $A.get("$Label.AxtriaSalesIQTM.Label_All");
              
        }else{
              allVal = $A.get("$Label.c.Label_All");
        }
        opts.push({label: allVal , value: allVal});
        if(data.getReturnValue() != null)
        {
          for(var i=1;i<= data.getReturnValue();i++)
          { 
            opts.push({label: ""+i, value: ""+i});
          }
        }
        // component.set("v.topLevel",data.getReturnValue());
        
        component.set("v.levelOpts", opts);
        console.log('topLevel--->>>' + data.getReturnValue());
        console.log(opts);
      }
      else
      {
        console.log('error '+state);
        component.showErrorToast();
      }

    });

    $A.enqueueAction(action); 
},

//   getQueryConditions:function(component,event,helper){

//      var action2=component.get('c.getInitialClauses');
//                action2.setParams({
//                 ruleType : component.find('affiliationselect').get("v.value"),
//                });
//                action2.setCallback(this, function(data)
//                {
//                 var state = data.getState();
//                 if(state === 'SUCCESS'){
//                   var affiliationRulesMetadataList = data.getReturnValue();
//                   console.log('affiliationRulesMetadataList');
//                   console.log(affiliationRulesMetadataList);
//                }

//                    else{
//                     console.log('error '+state);
//                 }

//                 });

//                $A.enqueueAction(action2);
// },

addABlock:function(component,event,helper){
      component.set("v.criteriaNumber",component.get("v.criteriaNumber") + 1);
      var firstComp;
      var secondComp;
      var len = component.get("v.componentsArray").length;
    
     // alert(len);
      $A.createComponent(
        "c:BRQueryBuilder",
        {
            "scenarioRuleInstanceId":component.get("v.scenarioRuleInstanceId"),
             "parsedQuery":'',
             "parsedJSONQuery":'',
            "aura:id":"affilationQueryBuilder"+(len+3),
            "namespace" : component.get("v.namespace"),
            "fileType":component.get("v.fileType")
            // "namespace":component.get("v.namespace")
        },
        function(newCmp, status, errorMessage){
            if (status === "SUCCESS") {
              firstComp = newCmp;
              //componentList.push(newCmp);
                /*component.set("v.var1", newCmp);
                var childComponent = component.find("affilationQueryBuilder3"+1);
                 childComponent.init();*/
            }
            else if (status === "INCOMPLETE") {
                console.log("No response from server or client is offline.")
            }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
            }
        }
    );

  $A.createComponent(
        "c:BRQueryBuilder",
        {
            "scenarioRuleInstanceId":component.get("v.scenarioRuleInstanceId"),
             "parsedQuery":'',
            "parsedJSONQuery":'',
            "aura:id":"affilationQueryBuilder"+(len+4),
            "namespace" : component.get("v.namespace"),
            "fileType":component.get("v.fileType")
            // "namespace":component.get("v.namespace")
        },
        function(newCmp, status, errorMessage){
            if (status === "SUCCESS") {
                secondComp = newCmp;
            }
            else if (status === "INCOMPLETE") {
                console.log("No response from server or client is offline.")
            }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
            }
        }
    );
    // if()
    var componentList = component.get("v.componentsArray");
    componentList.push(firstComp);
    componentList.push(secondComp);
    component.set("v.componentsArray", componentList);

    // var childComponent = component.find("affilationQueryBuilder"+(len+3));
                 componentList[componentList.length - 1].init();
                 componentList[componentList.length - 2].init();
    // var childComponent2 = component.find("affilationQueryBuilder"+(len+4));
                 // childComponent2.init();
},

deleteABlock:function(component,event,helper)
{
  component.set("v.criteriaNumber",component.get("v.criteriaNumber") - 1);
  // var eventSearch = event;

  // component.find('affilationQueryBuilder4').set("v.aura:id", 'sample');
  // console.log('find ', component.find('sample') )MKmar;

 // return;
  console.log(component.get("v.componentsArray"));
  var index = '';
  if(event.target.nodeName == 'svg' || event.target.nodeName == 'use')
  {
    index = event.target.parentNode.parentNode.className.split('index_')[1];
    console.log(event);
  }
  else
  {
    index = event.target.className.split('index_')[1];
  }

  if(index == undefined){
   index = event.target.parentNode.parentElement.className.split('index_')[1]
  }
 
  console.log('index------->'+index);
   
        // component.find('affilationQueryBuilder'+(parseInt(index)+2)).destroy();
        // component.find('affilationQueryBuilder'+(parseInt(index)+3)).destroy();
        $j = jQuery.noConflict();
        $j(".index_"+(parseInt(index))).addClass('slds-hide');
    
   
 
  var componentArrayList = component.get("v.componentsArray");
  console.log('componentArrayList ::: ',componentArrayList);

  if(component.get('v.alreadyExistingRuleFlag') == false)
  {

        if(componentArrayList.length <=2)
        {
          componentArrayList = [];
        }
        else
        { 
          componentArrayList.splice((parseInt(index) - 3), 2);
        }
         component.set("v.componentsArray", componentArrayList);
  }

  else{
           componentArrayList.splice((parseInt(index)- 1), 2);
           component.set("v.componentsArray", componentArrayList);
  }
  
  
},

    closeButton : function(component, event, helper) {

        $j = jQuery.noConflict();
        $j('.deleteClassPopOver').removeClass('slds-hide');
                
    },

    navigateToRulesComponent : function(component, event, helper) {
      component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
      var record_Id = component.get('v.record_Id');
      var workSpaceId = component.get('v.workSpaceId');
         var evt = $A.get("e.force:navigateToComponent");
         evt.setParams({
         componentDef : "c:BRAssignmentRule",
         componentAttributes: {
          scenarioId: component.get('v.scenarioId'),
          record_Id : record_Id,
          workSpaceId : workSpaceId,
          scenarioRuleInstanceId : component.get('v.scenarioRuleInstanceId'),
          summarizedViewFlag :true,
          namespace : component.get('v.namespace'),
          brModuleName : 'BRAffilationRules'
        }

      });

      
      try{
          var treeHierarchyComp = component.find("treeHierarchy");
          treeHierarchyComp.destroy();
      }catch(e){
          console.log(e);
      }
      
      evt.fire();
      component.destroy();
                
    },

    showErrorToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        var dsnotavailable = $A.get("$Label.c.Datasources_Not_Available");
        toastEvent.setParams({
            message:dsnotavailable,
            messageTemplate: dsnotavailable,
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
    },

  cancelCancel:function(component, event, helper) {
       $j = jQuery.noConflict();
        $j('.deleteClassPopOver').addClass('slds-hide');
    },

  saveAffiliationRules : function(component,event,helper){
   
   component.set('v.errorMessageArray',[]);
   component.set('v.whereClauseArray',[]);
   component.set('v.stagingClauseArray',[]);
   component.set('v.sourceQueryArray',[]);
   component.set('v.parsedJsonQueryArray',[]);

   component.set('v.disable_save', true);
   component.showSpinner();

    var componentsArrayLength = (component.get("v.componentsArray")).length;
    var componentList = component.get("v.componentsArray");
    if(component.get('v.alreadyExistingRuleFlag') == false){
       var childComponent = component.find("affilationQueryBuilder1");
          childComponent.getQuery();
       var childComponent = component.find("affilationQueryBuilder2");
          childComponent.getQuery();
      for(var i=0;i<componentsArrayLength;i++){
          // var childComponent = component.find("affilationQueryBuilder"+i);
          // childComponent.getQuery();
          componentList[i].getQuery();
     } 
    }
    else{
      for(var i=0;i<componentsArrayLength;i++){
           componentList[i].getQuery();
     } 
    }
     
       
  },

  navigateToAssignmentComponent : function(component, event, helper) {


  },

  setRulesWrapper : function(component,event,helper){

     var ruleAttributes = component.get('v.ruleAttributes');

     var sel_pos = component.get('v.selectedPositions');
     component.set('v.disable_save', true);

     if( component.get('v.ruleLevel') == 'Team'){
        sel_pos = '';
     }

     var action = component.get('c.setAffiliationRulesWrapper');
               action.setParams({
                rulename : component.get('v.rulename'),
                ruleLevel : component.get('v.ruleLevel'),
                ruleType : ruleAttributes['affiliationRuleType'],
                geographicPreference : ruleAttributes['geographicPreference'],
                level : ruleAttributes['affiliationLevel'],
                selectedPositions : sel_pos,
                parsedJsonQueryList : component.get('v.parsedJsonQueryArray'),
                whereClauseList : component.get('v.whereClauseArray'),
                stagingClauseList : component.get('v.stagingClauseArray'),
                sourceQueryList : component.get('v.sourceQueryArray')
               });
               action.setCallback(this, function(data)
               {
                var state = data.getState();
                if(state === 'SUCCESS'){
                 
                  var valuesList = data.getReturnValue();
                  console.log('valuesList');
                  console.log(valuesList);

               component.set('v.BRAffiliationWrapper',valuesList);
               console.log('valuesList ::: '+valuesList);
               console.log('stringified BRAffiliationWrapper :: '+JSON.stringify(component.get('v.BRAffiliationWrapper')));
                

              var action2=component.get('c.saveFinalValues');
               action2.setParams({
                rulename : component.get('v.rulename'),
                BRAffiliationWrapperList : JSON.stringify(component.get('v.BRAffiliationWrapper')),
                scenarioRuleInstanceId : component.get('v.scenarioRuleInstanceId'),
                scenarioId : component.get('v.scenarioId'),
                alreadyExistingRuleFlag : component.get('v.alreadyExistingRuleFlag'),
                selectedBusinessRuleId : component.get('v.selectedBusinessRuleId')
               });
               action2.setCallback(this, function(data)
               {
                var state = data.getState();
                if(state === 'SUCCESS'){
                  var successFlag = data.getReturnValue();
                  console.log('successFlag');
                  console.log(successFlag);
                  $A.enqueueAction(component.get('c.showSuccessToast'));
                  $A.enqueueAction(component.get('c.navigateToRulesComponent'));
                    component.set('v.disable_save', false);
                    component.hideSpinner();
               }

                   else{
                    console.log('error '+state);
                    component.set('v.disable_save', false);
                    component.hideSpinner();
                }

                });

               $A.enqueueAction(action2);
              
              
                   }

                   else{
                    console.log('error '+state);
                    component.set('v.disable_save', false);
                    component.hideSpinner();
                }

                });

               $A.enqueueAction(action);
     
  },

  updateFilterWrapper : function (component, event, helper) {

    console.log('called');
      
       var validateEvent = $A.get("e.c:modalCloseEvent");
		validateEvent.setParams({
            'doValidation': true
        }).fire();

      var parsedJsonQueryArray = component.get('v.parsedJsonQueryArray');
      var whereClauseArray = component.get('v.whereClauseArray');
      var stagingClauseArray = component.get('v.stagingClauseArray');
      var errorMessageArray = component.get('v.errorMessageArray');
      var sourceQueryArray = component.get('v.sourceQueryArray');

      var parsedJsonQuery = event.getParam("parsedJson");
      var whereClause = event.getParam("whereClause");
      var isValid = event.getParam("isValid");
      var errMsg = event.getParam("errorMsg");
      var stagingClause = event.getParam("stagingWhereClause");
      var sourceQuery = event.getParam("sourceWhereClause");


      var setErrors = component.get('v.setErrors');

      if( setErrors == undefined || setErrors == null ){
        setErrors = new Set();
      }

      parsedJsonQueryArray.push(parsedJsonQuery);
      whereClauseArray.push(whereClause);
      stagingClauseArray.push(stagingClause);
      sourceQueryArray.push(sourceQuery)
      if(errMsg != ''){
        errorMessageArray.push(errMsg);
        setErrors.add(errMsg);
      }
    
      component.set('v.setErrors',setErrors);


     component.set('v.parsedJsonQueryArray',parsedJsonQueryArray);
     component.set('v.whereClauseArray',whereClauseArray);
     component.set('v.stagingClauseArray',stagingClauseArray);
     component.set('v.errorMessageArray',errorMessageArray);
     component.set('v.sourceQueryArray',sourceQueryArray);
     component.set('v.isErrorOccured',false);

      console.log('parsedJsonQueryArray :: '+parsedJsonQueryArray);
      console.log('whereClauseArray :: '+whereClauseArray);
      console.log('sourceQueryArray :: '+sourceQueryArray);

      var componentsArrayLength = (component.get("v.componentsArray")).length;
      console.log('componentsArrayLength ----> '+componentsArrayLength);
      if(component.get('v.alreadyExistingRuleFlag') == false)
        {
         if((whereClauseArray.length) == (componentsArrayLength + 2))
         {
              if(component.get('v.errorMessageArray').length > 0)
                {
                  var errorMessage = '';
                  
                  var errorMessageArray = component.get('v.errorMessageArray');
                  for(var i=0;i<errorMessageArray.length;i++){

                        if(errorMessageArray[i] != ''){
                          errorMessage = errorMessageArray[i];
                        }
                  }
                  helper.doValidation(component, event, helper, setErrors);

                //component.set('v.popoverMessage',errorMessage);

                 //var errorIcn = component.find("errorIcon1");
                 //$A.util.removeClass(errorIcn,'slds-hide');
                }
                else
                {
                  //component.setRulesWrapper();
                  //$A.enqueueAction(component.get('c.doValidation'));
                  helper.doValidation(component, event, helper, setErrors);
                }  
          }
        }

      else
      {
        if((whereClauseArray.length) == (componentsArrayLength))
         {
             if(component.get('v.errorMessageArray').length != 0)
                {
                   var errorMessage = '';
                  
                  var errorMessageArray = component.get('v.errorMessageArray');
                  for(var i=0;i<errorMessageArray.length;i++)
                  {
                        if(errorMessageArray[i] != ''){
                          errorMessage = errorMessageArray[i];
                        }
                  }
                  helper.doValidation(component, event, helper, setErrors);
                 //component.set('v.popoverMessage',errorMessage);
                 //var errorIcn = component.find("errorIcon1");
                 //$A.util.removeClass(errorIcn,'slds-hide');
                }

                else
                {
                  //component.setRulesWrapper();
                  helper.doValidation(component, event, helper, setErrors);
                 
                }
         }
      }


      //do rulename and positon validation
      //helper.doValidation(component, event, helper, setErrors);
     
    },

    showSuccessToast : function(component, event, helper) {
      var toastEvent = $A.get("e.force:showToast");
      var successlabel = $A.get("$Label.c.changes_successfully_saved");
      toastEvent.setParams({
        message: successlabel,
        messageTemplate: successlabel,
        duration:' 5000',
        key: 'info_alt',
        type: 'success',
        mode: 'pester'
      });
      toastEvent.fire();
    },
     handleMouseEnter1 : function(component, event, helper) {
          var popover = component.find("popover1");
          $A.util.removeClass(popover,'slds-hide');
      },
         //make a mouse leave handler here
      handleMouseLeave1 : function(component, event, helper) {
          var popover = component.find("popover1");
          $A.util.addClass(popover,'slds-hide');
      },

      closePicklistDropdown : function(component, event, helper) {
        console.log('close af rules');
        var ev = event.target;
        var cls = ev.classList[0];
        console.log('cls :'+cls);
        var exception_class_set = new Set(['icn','slds-button','icon-nodes','uiInput','icon-nodes','slds-tree__item','slds-m-right_x-small','slds-input','slds-icon','arrowIcon',undefined,'slds-is-expanded','slds-truncate','slds-tree']);

        if(!exception_class_set.has(cls)){
          console.log('close event called');
          $A.get("e.c:modalCloseEvent").fire();    
        }
        let clear_error_exception_class_set = new Set(['slds-button','arrowIcon',undefined,'slds-is-expanded','slds-truncate','slds-tree']);

        if(!clear_error_exception_class_set.has(cls)){
           $A.enqueueAction(component.get('c.clearErrors'));   
        }

        // if(!String(cls).includes('slds-m-right_x-small') &&  !String(cls).includes('slds-input')){
        //     $A.get("e.c:modalCloseEvent").fire();  
        // } 
      },

    opentreehierarchy  : function(component,event,helper){
        var treeHierarchyComp = component.find("treeHierarchy");
        var cr_id = component.get("v.countryId");
        console.log('cr_id');
        console.log(cr_id);
        treeHierarchyComp.openTree();
    },

    onRuleTypeChange : function(component,event,helper){
      if(component.get('v.affiliationTextFlag') == true){
          component.set('v.affiliationTextFlag',false);
      //  component.set('v.defaultJSONQuery','{"bindClause":"AND","criterias":[{"anotherValue":"","dataType":"text","field":"accountname","innerConditionWrapper":{},"isGroup":false,"operator":"equals","value":"HCA"}]}');
      }
      else if(component.get('v.affiliationTextFlag') == false){
        component.set('v.affiliationTextFlag',true);
      //  component.set('v.defaultJSONQuery','{"bindClause":"AND","criterias":[{"anotherValue":"","dataType":"text","field":"accountname","innerConditionWrapper":{},"isGroup":false,"operator":"equals","value":"HCP"}]}');
       }
    },
    
     showSpinner : function(component, event, helper) {
      //$A.util.removeClass(component.find("assignmentSpinner"), "slds-hide");
      component.set("v.Spinner", true);
     },

     hideSpinner : function(component, event, helper) {
      //$A.util.addClass(component.find("assignmentSpinner"), "slds-hide");
      component.set("v.Spinner", false);
     },

    handleRendering : function(component, event) {

          var message = event.getParam("message");
          console.log("Value Received----" +message);
          if(message == 'Loaded'){
            component.hideSpinner();
          }
         // component.set('v.pageRederingCheck',message);
    },

    onRuleLevelChange : function(component, event, helper) {

        console.log(component.get("v.ruleLevel"));
        if(component.get("v.ruleLevel") == "Position"){

          var treeHierarchyComp = component.find("treeHierarchy_div");
          $A.util.removeClass(treeHierarchyComp, 'slds-hide');

          var disabledButton = component.find("disabledButton"); 
          $A.util.addClass(disabledButton, 'slds-hide');
        //  var selectionmsg1 = component.find("auraIDPositions"); 
          //$A.util.removeClass(selectionmsg1, 'slds-hide');

          //GET CURRENT PRODUCT POSITION MAPPING
        
          
        }else{

            var treeHierarchyComp = component.find("treeHierarchy_div");
            $A.util.addClass(treeHierarchyComp, 'slds-hide');

            var disabledButton = component.find("disabledButton"); 
            $A.util.removeClass(disabledButton, 'slds-hide');

          //  var selectionmsg1 = component.find("auraIDPositions"); 
           // $A.util.addClass(selectionmsg1, 'slds-hide');

        }   

      },

    addBlockOnUpdate : function(component,event,helper){
      
      var firstComp;
      var secondComp;
      var BRAffiliationWrapper = component.get('v.BRAffiliationWrapper');
      var parsedBRAffiliationWrapper = JSON.parse(JSON.stringify(component.get('v.BRAffiliationWrapper')));
      var ruleAttributes = component.get('v.ruleAttributes');
     //
      ///component.find('affiliationselect').set("v.value","Top Down");
      //
      ///component.find('levelSelect').set("v.value","All");
      // component.set("v.level",BRAffiliationWrapper[0].levelSelect);
      //component.find('geographicSelect').set("v.value",BRAffiliationWrapper[0].geographicPreference);
      if(BRAffiliationWrapper[0].geographicPreference == 'Align in Universe'){
        component.set('v.geographicPreference','Align In Universe');
      }
      else{
        component.set('v.geographicPreference','Align In Geo');
      }

      //if(component.find('affiliationselect').get("v.value") == 'Top Down'){

      if( ruleAttributes['affiliationRuleType'] != undefined && ruleAttributes['affiliationRuleType'] == 'Top Down' ){
        component.set('v.affiliationTextFlag',false);
      }
      else{
        component.set('v.affiliationTextFlag',true);
      }
      
      var len = parsedBRAffiliationWrapper[0].parsedJsonQuery.length;

            for(var i=1;i<len+1;i++){

               $A.createComponent(
                "c:BRQueryBuilder",
                {
                    "scenarioRuleInstanceId":component.get("v.scenarioRuleInstanceId"),
                     "parsedQuery":parsedBRAffiliationWrapper[0].whereClauseQuery[i-1],
                    "namespace" : component.get("v.namespace"),
                    "fileType": component.get("v.fileType"),
                    "parsedJSONQuery":parsedBRAffiliationWrapper[0].parsedJsonQuery[i-1],
                    "aura:id":"affilationQueryBuilder"+(i+2),
                },
                function(newCmp, status, errorMessage){
                    if (status === "SUCCESS") {
                      firstComp = newCmp;
                      var componentList = component.get("v.componentsArray");
                      componentList.push(firstComp);
                      component.set("v.componentsArray", componentList);
                      var childComponent = component.find("affilationQueryBuilder"+(i+2));
                         childComponent.init();
                    }
                    else if (status === "INCOMPLETE") {
                        console.log("No response from server or client is offline.")
                    }
                        else if (status === "ERROR") {
                            console.log("Error: " + errorMessage);
                    }
                }
            );
            }
           
           
      },


      restrictInputsRule :function(component, event, helper){
          var ruleName=component.find("ruleName");
          var text=ruleName.get('v.value'); 
          var newtext = text.replace(/(?!^-)[^0-9a-zA-Z_\s]/g, ''); 
          //component.set(ruleName, newtext);        
          ruleName.set('v.value', newtext); 
          console.log('newtext value :: '+newtext);   
      },

      clearErrors : function(component,event,helper){
        try{
            //REMOVED brmsTemplateOption
            //var inputs = ['inputScenarioName','InputSelectAlignType','InputSelectAffiliationNetwork','scenarioTypeOpts','createTypeSelect','InputSelectTeamIns','InputSelectGeoLayer','initialTeamSelect', 'filePath','selDataSource'];
            var inputs = ['ruleName','treeHierarchy']; 
            for(var index in inputs){
                var inputCmp = component.find(inputs[index]);
                if(!$A.util.isUndefined(inputCmp)){
                        //inputCmp.set("v.errors", []);   
                        $A.util.removeClass(inputCmp, 'erroroccured');
                        //component.set('v.hasValidationFailed',false);
                      //component.set("v.errorList",[]);   
                } 
            }
        }catch(Exception){
            console.log('Exception'+Exception);
        }   

    },




})