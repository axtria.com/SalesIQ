({
	 doValidation : function(component, event, helper, setErrors){
        //var errMessage='';
       	var errorList =  component.get('v.errorMessageArray');
        var errMessage=component.get("v.errMessage");
        //var isValid = component.get("v.isValid");       
        var ruleName = component.get('v.rulename');

        //var seterrors = component.get('v.setErrors');
        if(setErrors == undefined || setErrors == null)
          setErrors = new Set();
        //var assignmentvalues = component.get('v.BRMRuleInfoWrapper.ruleValue');

        var noneVal;
        var namespace =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0];
        console.log('namespace val' +namespace);
        if(namespace != ''){
            noneVal = $A.get("$Label.AxtriaSalesIQTM.None");    
        }else{
            noneVal = $A.get("$Label.c.None");
        }
        var errorIcn = component.find("errorIcon1");
        
        var rulenames = component.get('v.rulenames');
    


        if(( component.get('v.selectedNodesVals') == 0 ) && ( component.get("v.ruleLevel") == "Position" )) {
            errMessage = $A.get("$Label.c.No_Position_Selected_Label");
            errorList.push(errMessage);
            setErrors.add(errMessage);
            var treecmp = component.find('treeHierarchy');
            $A.util.addClass(treecmp , 'erroroccured');
        }else{
            var treecmp = component.find('treeHierarchy');
            $A.util.removeClass(treecmp , 'erroroccured');
        }

        if(!ruleName || ruleName==undefined || ruleName.trim() == ''){
            errMessage = $A.get("$Label.c.Please_enter_rule_name");
            errorList.push(errMessage);
            var sname = component.find('ruleName');
            $A.util.addClass(sname , 'erroroccured');
            setErrors.add(errMessage);
            //setErrors.add(errMessage);
        }else{
            var setNames = new Set(rulenames);
            if( component.get("v.alreadyExistingRuleFlag") && setNames.has(ruleName.toLowerCase()) ){
                setNames.delete(ruleName.toLowerCase());
            }
            //Check dupelicate rule names
            if(setNames.has(ruleName.toLowerCase())){
                errMessage = 'Rule with name '+ruleName +' already exists.';
                // errorList.push(errMessage);
                // var sname = component.find('ruleName');
                // $A.util.addClass(sname , 'erroroccured');
                
                errorList.push(errMessage);
                var sname = component.find('ruleName');
                $A.util.addClass(sname , 'erroroccured');
                setErrors.add(errMessage);
            }
            //Check dupelicate rule names
            // var setNames = new Set(rulenames);
            // if(setNames.has(ruleName.toLowerCase())){
            //     errMessage = 'Rule with name '+ruleName +' already exists.';
            //     errorList.push(errMessage);
            //     var sname = component.find('ruleName');
            //     $A.util.addClass(sname , 'erroroccured');
            //     setErrors.add(errMessage);
            // }
        }    

        if(errorList.length > 0){
            try{
                component.set('v.isErrorOccured',true);
                $A.util.removeClass(errorIcn,'slds-hide');
                component.set('v.popoverMessage','');
                var finalErrorSet = new Set(errorList.concat(Array.from(setErrors)));
                component.set('v.errorMessageArray',Array.from(finalErrorSet));
                var errorIcn = component.find("errorIcon1");
                $A.util.removeClass(errorIcn,'slds-hide');
                
                component.set('v.disable_save', false);
                component.hideSpinner();

            }catch(e){

            }
           
        }

        else{
          //component.set('v.isConditionValid',true);
            var errorIcn = component.find("errorIcon1");
            $A.util.addClass(errorIcn,'slds-hide');
            var sname = component.find('ruleName');
            component.set('v.popoverMessage','');
            $A.util.removeClass(sname , 'erroroccured');
            component.set('v.errorMessageArray',new Array());
            component.set('v.isErrorOccured',false);

            //save rules on valid input 
            component.setRulesWrapper();
            // var expname = component.find("assnselect");
            // $A.util.removeClass(expname , 'erroroccured');
        }
    },

    setAffilationAttributes : function(component,event,helper){
        var ruleAttributes = component.get('v.ruleAttributes');
        if( ruleAttributes['affiliationRuleType'] != undefined && ruleAttributes['affiliationRuleType'] == 'Top Down' ){
            component.set('v.affiliationTextFlag',false);
          }
          else{
            component.set('v.affiliationTextFlag',true);
          }
    }
})