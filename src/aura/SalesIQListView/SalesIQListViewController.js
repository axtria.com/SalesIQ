({
    doInit : function(component,event,helper)
    {
        console.log('do init of List view');
        component.getNamespace();
        component.set("v.countryId", helper.getCookieValue(component,event,helper));
        helper.checkCountryAccess(component,event,helper);
    },

    getNamespace : function(component,event,helper)
    {
        var action = component.get("c.getOrgNamespace");

        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var ns = response.getReturnValue();
                component.set("v.namespace",ns);
            }    
        });

        action.setBackground();
        $A.enqueueAction(action);   
    },

    refreshListViews : function(component,event,helper)
    {
        var params = event.getParam('arguments');
        var setView = true;
        if(params)
            setView = params.setView;

        console.log('params -',params);
        
        helper.getRecords(component,event,helper,setView); 
    },


    changeView : function(component,event,helper)
    {
        var selectedViewTarget = event.target.id;

        if(selectedViewTarget)
        {
            var idx = selectedViewTarget.split('_')[1];
            var wrapperList = component.get("v.ListViewWrapper");
            var selectedview =  wrapperList[idx].listViewId;
            
            component.set("v.selectedView", selectedview);
            console.log('query in changeview :',wrapperList[idx].queryString);
            component.set("v.queryString",wrapperList[idx].queryString);

            var evt = component.getEvent("ViewChange");
            evt.setParams({ 
            "viewId": selectedview,
            "viewName" : wrapperList[idx].listViewName,
            "whereClausePACP" :wrapperList[idx].whereClausePACP,
            "queryString" : wrapperList[idx].queryString,
            "invokeFromListView" : true
            });
            evt.fire();
        }
    },

    breadcrumbChange : function(component,event,helper)
    {
        component.set("v.selectedTeamInstance",event.getParam("teamInstanceId"));
        component.set("v.countryId",event.getParam("countryId"));
        component.set("v.selectedPosition",'');
        helper.getRecords(component,event,helper,false);
    },

    nodeSelectedClick : function(component,event,helper)
    {
        var selectedPosition = event.getParam("selectedTreeNode");
        component.set("v.selectedPosition",selectedPosition);
        helper.getRecords(component,event,helper,false);
    },

    
    slider : function(component,event,helper)
    {
        var params = event.getParam('arguments');
        var direction = params.direction;
        var $j = jQuery.noConflict();
        var counter = component.get("v.clickCount");
        if(counter==1){
            return false;
        }
        counter = 1;
        component.set("v.clickCount",counter);
        var listCount =  $j("#salesIQListView ul li").length;
        
        var w1 = $j("#salesIQListView").outerWidth();
        var w2 = ($j("#salesIQListView ul li").outerWidth()*$j(".salesIQListView ul li").length)+(parseFloat($j("#salesIQListView ul li").css('margin-right'))*$j(".salesIQListView ul li").length);
        $j(".salesIQListView ul").css({'width':w2});
        var listWidth = $j("#salesIQListView ul li").outerWidth();
        
        var extraWidth = (parseInt(w2)-parseInt(w1));
        var visible = Math.round(w1/listWidth);
        var nonVisible = listCount - visible;
        
        if(direction=='left' && extraWidth >= 0){
            if($j("#salesIQListView").position().left + (extraWidth/(nonVisible)) <= (3*listCount)){
                $j("#salesIQListView").animate({left: $j("#salesIQListView").position().left + (extraWidth/(nonVisible))+'px'},100);
            }
            counter = 0;
            component.set("v.clickCount",counter);
        }else if(direction=='right' && extraWidth>0){
            if(Math.abs(Math.ceil($j("#salesIQListView").position().left)) + (3*listCount) < extraWidth){
                $j("#salesIQListView").animate({left: $j("#salesIQListView").position().left - (extraWidth/(nonVisible))+'px'},100);
            }
            counter = 0;
            component.set("v.clickCount",counter);
        }else if(direction=='hide'){
            if(extraWidth > 0)
            {
                var evt = component.getEvent("showButtons");
                evt.fire();
                document.getElementById("salesIQListView").classList.remove('slds-grid--align-end');
                document.getElementById('summaryCardText').style.width = (w1+'px');
                document.getElementById('summaryCardText').style.float =  "none";
                document.querySelector(".salesIQListView ul").style.float =  "none";
            }
            if(extraWidth < 0  && listWidth != null)
            {
                var evt = component.getEvent("hideButtons");
                evt.fire();
                document.getElementById("salesIQListView").classList.add('slds-grid--align-end');
                document.getElementById('summaryCardText').style.width = (w2+'px');
                document.getElementById('summaryCardText').style.float =  "right";
                document.querySelector(".salesIQListView ul").style.float =  "right";
            }
            $j("#salesIQListView").animate({left:'0px'},100);
            counter = 0;
            component.set("v.clickCount",counter);
        }


    },

    doneRendering : function(component,event,helper)
    {
        if(component.get("v.isDoneRendering") == false)
        {
            if(component.find('listLi') != undefined)
            {

                var $j = jQuery.noConflict();
                if($j(".salesIQListView").length>1)
                {
                    for(var i=0;i<$j(".salesIQListView").length;i++)
                    {
                        if($j(".salesIQListView").eq(i).outerWidth() == 0)
                        {
                            $j(".salesIQListView").eq(i).remove();
                            $j(".summaryCardText").eq(i).remove();
                        }
                    } 
                }
                // var w1 = $j(".salesIQListView").outerWidth();
                // var w2 = $j(".salesIQListView ul").outerWidth();
                // var listWidth = $j(".salesIQListView ul li").outerWidth();
                if(document.getElementById("salesIQListView") != null && document.querySelector(".salesIQListView ul li") != null)
                {
                    /*var w1 = document.getElementById("salesIQListView").clientWidth;
                    var w2 = document.querySelector(".salesIQListView ul").clientWidth;
                    var listWidth = document.querySelector(".salesIQListView ul li").clientWidth;*/
					
					var w1 = $j(".salesIQListView").outerWidth();
	                var w2 = ($j("#salesIQListView ul li").outerWidth()*$j(".salesIQListView ul li").length)+(parseFloat($j("#salesIQListView ul li").css('margin-right'))*$j(".salesIQListView ul li").length);
	                $j(".salesIQListView ul").css({'width':w2});
	                var listWidth = $j(".salesIQListView ul li").outerWidth();
                    component.set("v.isDoneRendering",true);
                }
                
                var extraWidth = (parseInt(w2)-parseInt(w1));
                if(extraWidth < 1)
                {
                    var evt = component.getEvent("hideButtons");
                    evt.fire();
                    document.getElementById("salesIQListView").classList.add('slds-grid--align-end');
                    document.getElementById('summaryCardText').style.width = (w2+'px');
                    document.getElementById('summaryCardText').style.float =  "right";
                    document.querySelector(".salesIQListView ul").style.float =  "right";
                }
            } 
        }
    }
})