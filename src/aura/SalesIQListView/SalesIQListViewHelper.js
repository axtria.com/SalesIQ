({
    getRecords : function(component,event,helper,setView) 
    {
        var action = component.get("c.getListViews");
        console.log("list view countryid :"+component.get("v.countryId"));
        
        action.setParams({
            objectName : component.get("v.objectName"),
            selectedTeamInstance : component.get("v.selectedTeamInstance"),
            selectedPosition : component.get("v.selectedPosition"),
            moduleName : component.get("v.moduleName"),
            countryId : component.get("v.countryId")
        });
       
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var retResponse = response.getReturnValue();
                component.set("v.ListViewWrapper" , retResponse);
                
                /*if(component.get("v.loadRecent") == "false" && component.get("v.selectedView") != '' && component.get("v.selectedView") != null && setView == true)
                {
                    var selectedView = component.get("v.selectedView");
                    console.log('list view get records::' + selectedView);
                    for(i=0;i<retResponse.length;i++)
                    {
                        if(selectedView == retResponse[i].listViewId)
                        {
                            var selectedViewName = retResponse[i].listViewName;
                            component.set("v.selectedViewName", selectedViewName);
                            component.set("v.listViewFilter", retResponse[i].whereClausePACP);

                            // NOTE: Fire this event only in the case of Call Plan, i.e where SalesIQDataTable_V2 component is used
                            if(component.get("v.moduleName") == 'Call Plan') {
                                var evt = component.getEvent("ViewChange");
                                evt.setParams({ 
                                        "viewId": selectedView,
                                        "viewName" : selectedViewName,
                                        "whereClausePACP" :component.get("v.listViewFilter")
                                });
                                evt.fire();
                            }
                            break;
                        }
                    }
                }
                else*/ 
                if(component.get("v.loadRecent") == "false" && retResponse.length > 0 )
                {
                    var selectedview =  retResponse[0].listViewId;
                    var selectedViewName = retResponse[0].listViewName;
                    component.set("v.selectedViewName", selectedViewName);
                    component.set("v.selectedView", selectedview);
                    component.set("v.listViewFilter", retResponse[0].whereClausePACP);
                    component.set("v.queryString", retResponse[0].queryString);

                    console.log('queryString listView -',component.get("v.queryString"));

                    // NOTE: Fire this event only in the case of Call Plan, i.e where SalesIQDataTable_V2 component is used
                    if(component.get("v.moduleName") == 'Call Plan') {
                        var evt = component.getEvent("ViewChange");
                        evt.setParams({ 
                                "viewId": selectedview,
                                "viewName" : selectedViewName,
                                "whereClausePACP" :component.get("v.listViewFilter")
                                //"queryString" : component.get("v.listViewQueryString")
                        });
                        evt.fire();
                    }
                    else{
                        var evt = component.getEvent("ViewChange");
                        evt.setParams({ 
                        "viewId": selectedview,
                        "viewName" : selectedViewName,
                        "whereClausePACP" :component.get("v.listViewFilter"),
                        "queryString" : component.get("v.queryString")

                        });
                        evt.fire();
                    }
                }
                component.set("v.isDoneRendering",false);
            }
            else if (state === "ERROR" || response.getReturnValue() == null) 
            { 
                helper.logException(component, response.getError());
            }
        });

        $A.enqueueAction(action); 
    },

    updateCountryAttributes : function(component,event,helper){
        console.log('calling event update country flag from List view Controller :'+component.get("v.countryId"));
        var countryFlagEvent = component.getEvent("updateCountryFlag");
        countryFlagEvent.setParams({
            countryId: component.get("v.countryId")
        }).fire();
    },

    getCookieValue : function(component,event,helper){
        var regex = new RegExp('[; ]'+'apex__CountryID'+'=([^\\s;]*)');
        var cookieValue = (' '+document.cookie).match(regex);
        return (cookieValue!=null || cookieValue!= undefined)?unescape(cookieValue[1]):'';
    },

    checkCountryAccess : function(component,event,helper){
        var action = component.get("c.checkCountryAccess");
        action.setParams
        ({
            country : component.get("v.countryId")              
        });
    
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            var result = response.getReturnValue();             
            if(state === 'SUCCESS' && result != null)
            {
               if(result.Success){
                    var cookieStr = 'apex__CountryID' + "=" + escape(result.Success) + "; " + ";domain=.force.com;path=/";
                    document.cookie = cookieStr;
                    component.set("v.countryId",result.Success);
                    helper.getRecords(component,event,helper,true); 
                    // call the event to send country Id and init all components
                    helper.updateCountryAttributes(component,event,helper);
                }else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: result.Error,
                        type : 'Error'                          
                    });
                   toastEvent.fire();
                   //component.hideSpin(); 
               }
            }
            else if (state === "ERROR" || result == null) 
            { 
                helper.logException(component, response.getError());
            }
        });
        $A.enqueueAction(action);
    },

    logException : function(component, errors) {

        var errorLabel = '', uapErrorLabel = '';
        var errorMessage = errors[0].message;
        if(component.get("v.namespace") != '')
        {
            errorLabel = $A.get("$Label.AxtriaSalesIQTM.Unexpected_error_notification");
            uapLabel = $A.get("$Label.AxtriaSalesIQTM.No_UAP_Assigned");
        }
        else
        {
            uapLabel = $A.get("$Label.c.No_UAP_Assigned");
            errorLabel = $A.get("$Label.c.Unexpected_error_notification");
        }

        if(errorMessage.indexOf(uapLabel) != -1)
        {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                mode: 'dismissible',
                message: uapLabel,
                type : 'error'
                
            });
            toastEvent.fire();
            return;
        }

        if (errors[0] && errors[0].message) 
        {
            var action = component.get("c.logException");
        
            action.setParams({
                message : errorMessage,
                module : component.get("v.moduleName")
            });

            action.setCallback(this,function(response)
            {
                var state = response.getState();
                if(state === 'SUCCESS')
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: errorLabel,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }
        else 
        {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                mode: 'dismissible',
                message: errorLabel,
                type : 'error'
            });
            toastEvent.fire();
        }
    }
})