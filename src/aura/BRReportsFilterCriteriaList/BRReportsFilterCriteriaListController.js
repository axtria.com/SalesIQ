({
    loadsColumnsName : function(component, event, helper){  
        var action = component.get('c.getColumnsData');
        action.setParams({ 
            "scenarioId" : component.get("v.scenarioRuleInstanceId")
        });     
        action.setCallback(this, function(data){
            var response= data.getReturnValue();  
            console.log(response);
            var listOfColumnNames =[];
            var listOfOperators = [];
            var listOfAndOrOps = [];
           // listOfColumnNames.push({label:"Select Field",value:"Select Field"});
            for(var i=0;i<response[0].lstColumns.length;i++){
                listOfColumnNames.push({label:response[0].lstColumns[i].ds_col_name__c,value:response[0].lstColumns[i].ds_col_name__c});
                listOfOperators.push({label:response[0].lstOfOperators[i],value:response[0].lstOfOperators[i]});
                listOfAndOrOps.push({label:response[0].operatorsAndOr[i],value:response[0].operatorsAndOr[i]});
            }
            component.set("v.seqNo",response[0].seqNo); 
            component.set("v.lstOfColumnNames",listOfColumnNames); 
            component.set('v.selectedColumnName',response[0].lstColumns[0].ds_col_name__c);
            component.set('v.selectedOperator',listOfOperators[0]);
            component.set('v.lstOfOperators',listOfOperators);
            component.set('v.selectedAndOrOperator',listOfAndOrOps[0]);
            component.set('v.lstOfAndOrOperators',listOfAndOrOps);
                           
        })
        $A.enqueueAction(action);
    },

    closeButton : function(component, event, helper) {
         var sceId=component.get("v.scenarioId");    
         var evt = $A.get("e.force:navigateToComponent");
         evt.setParams({
                componentDef : "c:CPGDerivedFieldsList",
                componentAttributes: {
                     "scenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceDetailsId")
                }
         });
         $A.get('e.force:refreshView').fire();
         evt.fire();
    },
    deleteCriteria : function(component, event, helper){
        var idxB = component.get("v.indexBlock");
        console.log(idxB);
        var idxG = component.get("v.indexGrp");
        var idxC = component.get("v.indexCri");
       // var addEvent= component.getEvent('AddCriteriaEvent');
        var addEvent= $A.get('e.c:CPGAddFilterGroupEvent');
        addEvent.setParams({
            'dataindex':idxB+'_'+idxG+'_'+idxC+'@DeleteCriteria'          
        });
        addEvent.fire();
       
	},
    onColumnChange :function(component,event) {         
        var strDataType = event.getSource().get("v.value");
        var addEvent= $A.get('e.c:CPGAddFilterGroupEvent');
        addEvent.setParams({
            'dataindex':'Test@OnColumnChange@'+strDataType          
        });
        addEvent.fire();
    },
    onOperatorChange:function(component,event) {      
        var idxB = component.get("v.indexBlock");
        console.log(idxB);
        var idxG = component.get("v.indexGrp");
        var idxC = component.get("v.indexCri");   
        var strDataType = event.getSource().get("v.value");
        var addEvent= $A.get('e.c:CPGAddFilterGroupEvent');
        addEvent.setParams({
            'dataindex':idxB+'_'+idxG+'_'+idxC+'@OnOperatorChange'          
        });
        addEvent.fire();
    },
    
})