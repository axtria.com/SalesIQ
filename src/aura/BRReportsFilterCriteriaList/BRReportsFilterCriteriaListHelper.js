({
    ToggleCollapseHandler: function(component, event) {  
        var idx = event.currentTarget.getAttribute("data-index");
        //console.log(idx);
        var container = document.getElementById("Dummy_"+idx);         
        
        var isExpandable = $A.util.hasClass(container, "slds-is-open");      
        if(isExpandable){            
            $A.util.removeClass(container, 'slds-is-open');
        }
        else{            
            $A.util.addClass(container, 'slds-is-open'); 
        }
    },
    
    addCriteria : function(component,event) {  
        component.getEvent("AddCriteriaEvt").fire();          
    },

    

    addGroup:function(component,event){  
        var idx = event.currentTarget.getAttribute("data-recId");
        console.log(idx);
        var idG= parseInt(idx.split('_')[0]);
        var idB= parseInt(idx.split('_')[1]);
        var nwG = parseInt(idG+1);
        console.log('grpBind_'+nwG+'_'+idB);        
        var wrapperBlock= component.get('v.wrapperBlock');
        wrapperBlock[idB].lstWrapGroup[idG].selGroupOp='And';
        var action =  component.get("c.AddAGroup");             
        action.setCallback(this, function(data){            
            var state = data.getState();
            console.log(state);
            if (state === "SUCCESS") {              
                var response= data.getReturnValue();  //brand new grp               
                console.log(response);                
                component.set('v.wrapperBlock['+idB+'].lstWrapGroup['+nwG+']',response);
                //code to update sequence
                var block = wrapperBlock[idB]; // each block                
                var criteriaLogic='';
                var lstGroup= block.lstWrapGroup; // lstGrp under each block
                var criSeq=1;
                for(var k=0; k < lstGroup.length; k++){
                    var grp = lstGroup[k];
                    var lstCriteria = grp.lstwrapCriteria; //list of criteria
                    criteriaLogic+= '(';
                    for(var j=0; j < lstCriteria.length; j++){
                        var cri = lstCriteria[j];
                        cri.seqNo=criSeq;
                        criSeq++;
                        if(cri.criName != '-')
                            criteriaLogic+=' '+cri.criName+' '+cri.seqNo;
                        else
                            criteriaLogic+=cri.seqNo;
                    }
                    criteriaLogic+=') ';                    
                    if(k!=lstGroup.length-1)
                        criteriaLogic+= lstGroup[k].selGroupOp+' ';
                }
                //code to update sequence ends 
                block.criteriaLogic=criteriaLogic;               
                component.set('v.wrapperBlock',wrapperBlock);                
                      
            }           
            else if (state === "ERROR") {
                var errors = response.getError();
                console.log(errors);
            }                              
        });
        $A.enqueueAction(action); 
        var modal = document.getElementById('grpOp_'+idG+'_'+idB);
        $A.util.removeClass(modal, 'hide');
        $A.util.addClass(modal, 'display');         
    },
})