({
    updateCountryAttributes : function(component,event,helper){
        //SET ORG NAMESPACE ON INIT
        component.set('v.namespace',(( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0]+'__'));

        var action = component.get("c.getCountryAttributes");
        var params = event.getParam('arguments');
        if(params) {
            action.setParams({
                countryId : params.countryId
            });
        }else{
            action.setParams({
                countryId : event.getParam("countryId")
            });
        }

        var namespace = component.get("v.namespace");

        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){   
                console.log(response);
                var attrs = response.getReturnValue().split(',');
                var countryName = attrs[0];
                component.set("v.countryName",countryName);
                component.set("v.countryFlag",attrs[1]);
                var customCountrylabel = attrs[3];
                countryName = countryName.split(' ').join('_');
                var isCustomValue = ( attrs[2] == 'true' ) ? true : false;
                var label;
                //var label = "$Label.c." + countryName;
                if(isCustomValue){
                    if(customCountrylabel != undefined || customCountrylabel != null || customCountrylabel != ''){
                        label =  "$Label.c." + customCountrylabel;
                    }
                }else{
                    if(namespace != ''){
                        label =  "$Label."+namespace.slice(0,-2)+"."+countryName;
                    }else{
                        label =  "$Label.c." + countryName;
                    }
                }
                
                component.set('v.labelAttr',$A.getReference(label));
                if(component.get('v.labelAttr') == ""){
                    $A.get('e.force:refreshView').fire();
                }else{
                    var c = component.get("v.labelAttr");
                    component.set("v.countryName",c);
                }
            }
        });
        $A.enqueueAction(action);
    }
})