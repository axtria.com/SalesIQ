({
	doInit : function(component, event, helper) {

        var ns = ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0]+'__';

		var recordId = component.get("v.recordId");
		console.log('recId'+recordId);
		var action = component.get("c.getWorkspaceName");
        action.setParams({ 
            recordId : recordId
        });

        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") {
                var workspace = response.getReturnValue();
                
                component.set("v.isUniversal",workspace[ns+'isUniversal__c']);
                
                component.set("v.workspace",response.getReturnValue());
            }
    	});
        $A.enqueueAction(action);

	},

	backToWorkspace : function(component, event, helper){
		var currentState = history.state;
        history.pushState(currentState, "Workspaces" , "" );
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:SalesIQWorkspace",
            isredirect : true
        });
        evt.fire();
	},

	openModal: function(component, event, helper) {
      component.set("v.isOpen", true);
  	},
 
	closeModal: function(component, event, helper) {
	   	component.set("v.isOpen", false);
	},

    updateCountryAttributes : function(component, event, helper){
        console.log('Scenario Header component update component attributes' + event.getParam("recordId"));
        component.set("v.countryId", event.getParam("recordId"));

        var flag = component.find('flagComponent');
        flag.updateCountryAttributes(event.getParam("recordId"));
    }
})