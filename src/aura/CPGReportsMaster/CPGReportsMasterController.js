({
       doInit : function(component, event, helper) {
        var currentScenarioId = event.getParam("currentScenarioId");
        var previousScenarioId = event.getParam("previousScenarioId");
        var ScenarioName = event.getParam("currentScenarioName");
        var PrevScenarioName = event.getParam("previousScenarioName");
        var currentInstance = event.getParam("instanceNameValue");
        var teamName = event.getParam("teamName");
        var url=event.getParam("url");
        
        var btnClicked=event.getParam("btnClicked");
        component.set("v.currentScenarioId",currentScenarioId);
        component.set("v.previousScenarioId",previousScenarioId) ;
         component.set("v.ScenarioName",ScenarioName) ;
         component.set("v.PrevScenarioName",PrevScenarioName) ;
         component.set("v.currentInstance",currentInstance) ;
        component.set("v.teamName",teamName) ;
        component.set("v.url",url) ;
        //component.set("v.columns",columns) ;
        component.set('v.hidetab',true);
        if((previousScenarioId==='' || previousScenarioId===undefined || PrevScenarioName==undefined || PrevScenarioName=='Select Scenario')){
            
            var getOverLapTabId1 = component.find('OverlapTab');
            $A.util.removeClass(getOverLapTabId1, 'slds-show');
            $A.util.addClass(getOverLapTabId1, 'slds-hide');
            var getSegmentTabId1 = component.find('SegmentTab');
            $A.util.removeClass(getSegmentTabId1, 'slds-show');
            $A.util.addClass(getSegmentTabId1, 'slds-hide');
            var stackedbarchart1 = component.find('tab-scoped-3');

            window.setTimeout(function(){
                var listidOfStackBarChart = document.getElementById('listitem3');
            $A.util.addClass(listidOfStackBarChart,'slds-is-active');
            },2000)

            $A.util.removeClass(stackedbarchart1, 'slds-hide');
            $A.util.addClass(stackedbarchart1, 'slds-show');
            component.loadStackedBarChart();
            var barchart1 = component.find('tab-scoped-1');
            var listidOfBar = document.getElementById('listitem1');
            $A.util.removeClass(listidOfBar,'slds-is-active');
            $A.util.removeClass(barchart1, 'slds-show');
            $A.util.addClass(barchart1, 'slds-hide');
            var stackedChartTab = document.getElementById('listitem4');
            $A.util.removeClass(stackedChartTab,'slds-is-active');
            var stackedchart = component.find('tab-scoped-4');
            $A.util.removeClass(stackedchart, 'slds-show');
            $A.util.addClass(stackedchart, 'slds-hide');
        }else{
            var getOverLapTabId = component.find('OverlapTab');
            $A.util.removeClass(getOverLapTabId, 'slds-hide');
            $A.util.addClass(getOverLapTabId, 'slds-show');
            
            var getSegmentTabId = component.find('SegmentTab');
            $A.util.removeClass(getSegmentTabId, 'slds-hide');
            $A.util.addClass(getSegmentTabId, 'slds-show');
            
        
        var barchart = component.find('tab-scoped-1');
        var venndiagram = component.find('tab-scoped-2');
        var stackedbarchart = component.find('tab-scoped-3');
        var dataTable = component.find('tab-scoped-4');
        var crossTable = component.find('tab-scoped-5');
        
        var listid1 = document.getElementById('listitem1');
        var listid2 = document.getElementById('listitem2');
        var listid3 = document.getElementById('listitem3');
        var listid4 = document.getElementById('listitem4');
        var listid5 = document.getElementById('listitem5');
        
        
        $A.util.removeClass(listid2,'slds-is-active');
        $A.util.removeClass(listid3,'slds-is-active');
        $A.util.removeClass(listid4,'slds-is-active');
        $A.util.removeClass(listid5,'slds-is-active');
        $A.util.addClass(listid1,'slds-is-active');
        
        $A.util.removeClass(barchart, 'slds-hide');
        $A.util.addClass(barchart, 'slds-show');
        $A.util.removeClass(venndiagram, 'slds-show');
        $A.util.addClass(venndiagram, 'slds-hide');
        $A.util.removeClass(stackedbarchart, 'slds-show');
        $A.util.addClass(stackedbarchart, 'slds-hide');
        $A.util.removeClass(dataTable, 'slds-show');
        $A.util.addClass(dataTable, 'slds-hide');
        $A.util.removeClass(crossTable, 'slds-show');
        $A.util.addClass(crossTable, 'slds-hide');
        component.loadBarChart();    
        }
        
    },
    
    showSelectedItem : function(component, event, helper) {
       
        console.log('inside div');
        
        var barchart = component.find('tab-scoped-1');
        var venndiagram = component.find('tab-scoped-2');
        var stackedbarchart = component.find('tab-scoped-3');
        var dataTable = component.find('tab-scoped-4');
        var crossTable = component.find('tab-scoped-5');
        
        var listid1 = document.getElementById('listitem1');
        var listid2 = document.getElementById('listitem2');
        var listid3 = document.getElementById('listitem3');
        var listid4 = document.getElementById('listitem4');
        var listid5 = document.getElementById('listitem5');
        
        var componentId = event.target.getAttribute("id");
        console.log(componentId);
        
        if(componentId == 'tab-scoped-1__item'){
            
            $A.util.addClass(listid1,'slds-is-active');
            $A.util.removeClass(listid2,'slds-is-active');
            $A.util.removeClass(listid3,'slds-is-active');
            $A.util.removeClass(listid4,'slds-is-active');
            $A.util.removeClass(listid5,'slds-is-active');
            
            $A.util.removeClass(barchart, 'slds-hide');
            $A.util.addClass(barchart, 'slds-show');
            $A.util.removeClass(venndiagram, 'slds-show');
            $A.util.addClass(venndiagram, 'slds-hide');
            
            $A.util.addClass(stackedbarchart, 'slds-hide');
            $A.util.removeClass(stackedbarchart, 'slds-show');
            $A.util.removeClass(dataTable, 'slds-show');
            $A.util.addClass(dataTable, 'slds-hide');
            $A.util.removeClass(crossTable, 'slds-show');
            $A.util.addClass(crossTable, 'slds-hide');
        }
        
        if(componentId == 'tab-scoped-2__item'){
            
            $A.util.removeClass(listid1,'slds-is-active');
            $A.util.addClass(listid2,'slds-is-active');
            $A.util.removeClass(listid3,'slds-is-active');
            $A.util.removeClass(listid4,'slds-is-active');
            $A.util.removeClass(listid5,'slds-is-active');
            
            $A.util.removeClass(barchart, 'slds-show');
            $A.util.addClass(barchart, 'slds-hide');
            $A.util.removeClass(venndiagram, 'slds-hide');
            $A.util.addClass(venndiagram, 'slds-show');
            $A.util.removeClass(stackedbarchart, 'slds-show');
            $A.util.addClass(stackedbarchart, 'slds-hide');
            $A.util.removeClass(dataTable, 'slds-show');
            $A.util.addClass(dataTable, 'slds-hide');
            $A.util.removeClass(crossTable, 'slds-show');
            $A.util.addClass(crossTable, 'slds-hide');
            
        }
        
        if(componentId == 'tab-scoped-3__item'){
            
            $A.util.removeClass(listid1,'slds-is-active');
            $A.util.removeClass(listid2,'slds-is-active');
            $A.util.addClass(listid3,'slds-is-active');
            $A.util.removeClass(listid4,'slds-is-active');
            $A.util.removeClass(listid5,'slds-is-active');
            
            $A.util.removeClass(barchart, 'slds-show');
            $A.util.addClass(barchart, 'slds-hide');
            $A.util.removeClass(venndiagram, 'slds-show');
            $A.util.addClass(venndiagram, 'slds-hide');
            $A.util.removeClass(stackedbarchart, 'slds-hide');
            $A.util.addClass(stackedbarchart, 'slds-show');
            $A.util.removeClass(dataTable, 'slds-show');
            $A.util.addClass(dataTable, 'slds-hide');
            $A.util.removeClass(crossTable, 'slds-show');
            $A.util.addClass(crossTable, 'slds-hide');
            
        }
        
        if(componentId == 'tab-scoped-4__item'){
            
            $A.util.removeClass(listid1,'slds-is-active');
            $A.util.removeClass(listid2,'slds-is-active');
            $A.util.removeClass(listid3,'slds-is-active');
            $A.util.addClass(listid4,'slds-is-active');
            $A.util.removeClass(listid5,'slds-is-active');
            
            $A.util.removeClass(barchart, 'slds-show');
            $A.util.addClass(barchart, 'slds-hide');
            $A.util.removeClass(venndiagram, 'slds-show');
            $A.util.addClass(venndiagram, 'slds-hide');
            $A.util.removeClass(stackedbarchart, 'slds-show');
            $A.util.addClass(stackedbarchart, 'slds-hide');
            $A.util.removeClass(dataTable, 'slds-hide');
            $A.util.addClass(dataTable, 'slds-show');
            $A.util.removeClass(crossTable, 'slds-show');
            $A.util.addClass(crossTable, 'slds-hide');
            
        }
        
        if(componentId == 'tab-scoped-5__item'){
            
            $A.util.removeClass(listid1,'slds-is-active');
            $A.util.removeClass(listid2,'slds-is-active');
            $A.util.removeClass(listid3,'slds-is-active');
            $A.util.removeClass(listid4,'slds-is-active');
            $A.util.addClass(listid5,'slds-is-active');
            
            $A.util.removeClass(barchart, 'slds-show');
            $A.util.addClass(barchart, 'slds-hide');
            $A.util.removeClass(venndiagram, 'slds-show');
            $A.util.addClass(venndiagram, 'slds-hide');
            $A.util.removeClass(stackedbarchart, 'slds-show');
            $A.util.addClass(stackedbarchart, 'slds-hide');
            $A.util.removeClass(dataTable, 'slds-show');
            $A.util.addClass(dataTable, 'slds-hide');
            $A.util.removeClass(crossTable, 'slds-hide');
            $A.util.addClass(crossTable, 'slds-show');
        }
        
    },
    
    loadBarChart : function(component,event,helper){
        
         var childId = component.find('barChart');
         var currentScen = component.get("v.currentScenarioId");
         var prevScen = component.get("v.previousScenarioId");
         var currentScenName = component.get("v.ScenarioName");
         var prevScenName = component.get("v.PrevScenarioName");
         var currentInst = component.get("v.currentInstance");
          var teamName = component.get("v.teamName");
          var currentInstance=component.get("v.currentInstance");
         var url = component.get("v.url");
         childId.populateChart(currentScen,prevScen,currentInst,teamName,url,currentInstance,prevScenName,currentScenName);
    },
    
    loadVennDiagram : function(component,event,helper){
        
         var childId = component.find('venndiagramval');
         var currentScen = component.get("v.currentScenarioId");
         var prevScen = component.get("v.previousScenarioId");
         var currentScenName = component.get("v.ScenarioName");
         var prevScenName = component.get("v.PrevScenarioName");
         var currentInst = component.get("v.currentInstance");
          var teamName = component.get("v.teamName");
         var url = component.get("v.url");
         //var columns = component.get("v.columns");
         childId.Load(currentScen,prevScen,currentScenName,prevScenName,currentInst,teamName,url,'');
    },
    
    loadStackedBarChart : function(component,event,helper){
        
        var stackedbarchildId = component.find('stackedBarchart');
        var currentScen = component.get("v.currentScenarioId");
        var prevScen = component.get("v.previousScenarioId");
        var currentInst = component.get("v.currentInstance");
         var teamName = component.get("v.teamName");
        var url = component.get("v.url");
        
        stackedbarchildId.LoadStackedBarChart(currentScen,prevScen,currentInst,teamName,url);
    },
    
    callStackChart : function(component,event,helper){
        
         var stackedchildId = component.find('stackedchart');
         var currentScen = component.get("v.currentScenarioId");
         var prevScen = component.get("v.previousScenarioId");
        var currentInst = component.get("v.currentInstance");
        var teamName = component.get("v.teamName");
        var url = component.get("v.url");
        console.log('teamName::::'+teamName);
         stackedchildId.loadStackChart(currentScen,prevScen,currentInst,teamName,url);
    },
    loadSBChart :function(component,event,helper){
        // var stackedbarchildId = component.find('sbchart');
        // stackedbarchildId.LoadStackedBarChart();
    }
})