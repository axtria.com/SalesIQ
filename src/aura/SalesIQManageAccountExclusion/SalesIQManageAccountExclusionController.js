({

	doInit : function(component,event,helper) {
		component.getNamespace();

		var orgNameSpace = component.get("c.getOrgNamespace");

        orgNameSpace.setCallback(this,function(response)
        {
            var state = response.getState();
            
            var namespace = response.getReturnValue();
            component.set("v.namespace",namespace);    
        	
	        console.log('##operation type' , component.get("v.operationType"));

	        if(component.get("v.operationType") == 'Edit'){
	        	var action = component.get("c.fetchAccountExclusion");
	        	action.setParams({recordId : component.get("v.selectedAccountExclusionId")});
	        	action.setCallback(this, function(response){
	        		var state = response.getState();
	        		if(state == 'SUCCESS')
	        		{	
	        			console.log('##Account Exclusion ', response.getReturnValue());
	        			component.set("v.accountExclusion", response.getReturnValue());

	        			var actionCountry = component.get("c.fetchCountry");
				        actionCountry.setParams({recordId : component.get("v.countryId")});
				        actionCountry.setCallback(this, function(response){
				        	var state = response.getState();
				    		console.log('##state ', state);
				    		if(state == 'SUCCESS')
				    		{	
				    			console.log('##Country record ', response.getReturnValue());
				    			var country = response.getReturnValue();
				    			var accountExclusion = component.get("v.accountExclusion");
				    			console.log('##accountExclusion ', accountExclusion);

				    			if(accountExclusion[namespace + 'Status__c'].toLowerCase() == 'active' ){
				    				component.set("v.isStatusActive", true);
				    			}
				    			else if(accountExclusion[namespace + 'Status__c'].toLowerCase() == 'inactive' ){
				    				component.set("v.isStatusActive", false);
				    			}

				    			if(country[namespace + 'Is_Blocked_For_all__c']){
				    				component.set("v.disableBlockForAll", true);
				    				accountExclusion[namespace + 'Is_Blocked_For_all__c'] = true;
				    			}
				    			else
				    				component.set("v.disableBlockForAll", false);
				    			component.set("v.accountExclusion", accountExclusion);
				    		}
				    		else if(state === "INCOMPLETE" || state == "ERROR")
				        	{
				        		helper.logException(component, response.getError());
				        	}
				        });
				        $A.enqueueAction(actionCountry);
	        		}
	        		else if(state === "INCOMPLETE" || state == "ERROR")
		        	{
		        		helper.logException(component, response.getError());
		        	}
	        	});
	        	$A.enqueueAction(action);
	        }
	        else{
	        	var actionCountry = component.get("c.fetchCountry");
		        actionCountry.setParams({recordId : component.get("v.countryId")});
		        actionCountry.setCallback(this, function(response){
		        	var state = response.getState();
		    		console.log('##state ', state);
		    		if(state == 'SUCCESS')
		    		{
		    			var country = response.getReturnValue();
		    			console.log('country ', country);
		    			var accountExclusion = component.get("v.accountExclusion");
		    			console.log('##accountExclusion ', accountExclusion);
		    			accountExclusion[namespace + 'Is_Blocked_For_all__c'] = country[namespace + 'Is_Blocked_For_all__c'];
		    			accountExclusion[namespace + 'Status__c'] = 'Active';
		    			console.log('##accountExclusion ', accountExclusion);
		    			if(country[namespace + 'Is_Blocked_For_all__c'])
		    				component.set("v.disableBlockForAll", true);
		    			else
		    				component.set("v.disableBlockForAll", false);
		    			component.set("v.accountExclusion", accountExclusion);
		    		}
		    		else if(state === "INCOMPLETE" || state == "ERROR")
		        	{
		        		helper.logException(component, response.getError());
		        	}
		        });
		        $A.enqueueAction(actionCountry);
	        }

        });

        orgNameSpace.setBackground();
        $A.enqueueAction(orgNameSpace);
    },

    getNamespace : function(component,event,helper)
    {
        var action = component.get("c.getOrgNamespace");

        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var ns = response.getReturnValue();
                component.set("v.namespace",ns);
            }    
        });

        action.setBackground();
        $A.enqueueAction(action);   
    },
 	
 	saveaccountExclusion : function(component, event, helper) {
 		var namespace = component.get("v.namespace");
 		var modType = component.get("v.operationtype");
        var accountExclusion = component.get("v.accountExclusion");
        console.log('accountExclusion ', JSON.parse(JSON.stringify(accountExclusion)));
       
        component.runValidation();
    	if(component.get("v.Validated") == true)
    	{
	        component.set("v.refreshTable", 'false');
	        if(accountExclusion[namespace + 'Account__c'] != undefined)
	        {
				accountExclusion[namespace + 'Account__c'] = accountExclusion[namespace + 'Account__c'].val;
	        }
	        
	        component.set("v.accountExclusion", accountExclusion);
	        var action = component.get("c.saveAccountExclusion");  

	        action.setParams({
	        	accountExclusionObj : component.get("v.accountExclusion"), 
	        	operationType : modType
	        });

	        action.setCallback(this, function(response)
	        {
	        	var state= response.getState();
	        	if(state == "SUCCESS")
	        	{
	        		console.log('Account Exclusion inserted object ', response.getReturnValue());
	        		component.set("v.Validated", false);
	        		component.set("v.isOpen" , 'false');
	        		component.set("v.refreshTable", 'true');

		        	var toastEvent = $A.get("e.force:showToast");
				    toastEvent.setParams({
				            title: 'Success!',
				            type: 'success',
				        message: response.getReturnValue()
		    		});
				    toastEvent.fire();

				    component.set("v.accountExclusion", "{'sobjectType' : 'Account_Exclusion__c', 'Reason_Code__c': '', 'Block_Type__c': '','Product_Master__c' : '','Account_Number__c' : '' , 'Account__c':'', 'Product__c' : '', 'Status__c' : ''}");
				    component.set("v.recordId", "");
				    component.set("v.operationtype", "");
	        	}
	        	else if(state === "INCOMPLETE" || state == "ERROR")
	        	{
	        		helper.logException(component, response.getError());
	        	}	        
	       	});
	        $A.enqueueAction(action);
    	}    
    },

    checkValidation : function(component, event, helper){
    	var namespace = component.get("v.namespace");
    	var accountExclusion = component.get("v.accountExclusion");
    	console.log("inside validation" , accountExclusion);
    	
    	var $j = jQuery.noConflict();
    	var errorCmp;
    	var isValidated = true;

		if(accountExclusion[namespace + 'Status__c'] == "")
		{	
			errorCmp = component.find("Statuserror");
			$A.util.removeClass(errorCmp, 'slds-hide');
			errorCmp.set("v.value", "Fill this field");	
			isValidated = false;
		}
		else
		{
			$j('.statusField').removeClass('errorField');
			$A.util.addClass(component.find("Statuserror"), 'slds-hide');
		}

		if(accountExclusion[namespace + 'Account__c'] == undefined || accountExclusion[namespace + 'Account__c'] == ""){
			errorCmp = component.find("Accounterror");
			$A.util.removeClass(errorCmp, 'slds-hide');
			errorCmp.set("v.value", "Fill this field");	
			isValidated = false;
		}
		else
		{
			errorCmp = component.find("Accounterror");
			$A.util.addClass(errorCmp, 'slds-hide');
		}

		if(accountExclusion[namespace + 'Reason_Code__c'].trim() == ""){
			errorCmp = component.find("Codeerror");
			$A.util.removeClass(errorCmp, 'slds-hide');
			errorCmp.set("v.value", "Fill this field");	
			isValidated = false;
		}
		else
		{
			errorCmp = component.find("Codeerror");
			$A.util.addClass(errorCmp, 'slds-hide');
		}

		if(accountExclusion[namespace + 'Block_Type__c'] == ""){
			errorCmp = component.find("Typeerror");
			$A.util.removeClass(errorCmp, 'slds-hide');
			errorCmp.set("v.value", "Fill this field");	
			isValidated = false;
		}
		else
		{
			errorCmp = component.find("Typeerror");
			$A.util.addClass(errorCmp, 'slds-hide');
		}

		if(!accountExclusion[namespace + 'Is_Blocked_For_all__c']){
			if(accountExclusion.Product_Master__c == ""){
				errorCmp = component.find("Producterror");
				$A.util.removeClass(errorCmp, 'slds-hide');
				errorCmp.set("v.value", "Fill this field");	
				isValidated = false;
			}
			else
			{
				errorCmp = component.find("Producterror");
				$A.util.addClass(errorCmp, 'slds-hide');
			}
		}

		if(isValidated)
			component.set("v.Validated", true);
    },

    openModalPopup : function(component, event, helper) {
	    component.set("v.isOpen" , 'true');
      	var params = event.getParams('arguments');
        console.log('New',params);
	 	var countryID ='';
	 	if(params!=undefined)
	 	{
		 	countryID=params.countryID;
		 	console.log('New Lag'+countryID);
	 	}	 	
	},

	toggleProduct : function(component, event, helper){
		var namespace = component.get("v.namespace");
		var accountExclusion = component.get("v.accountExclusion");
		if(accountExclusion[namespace + 'Is_Blocked_For_all__c']){
			accountExclusion[namespace + 'Product_Master__c'] = '';
			component.set("v.accountExclusion", accountExclusion);
		}
	},

    closeModalPopup : function(component, event, helper) {
   		component.set("v.isOpen" , 'false');
	},

	saveAndNewPopup : function(component) {
		firstPromise.then(
		    // resolve handler
	        $A.getCallback(function(result) {
	            component.openModalPopup();
	            //return anotherPromise();
	        }),

		    // reject handler
	        $A.getCallback(function(error) {
	            console.log("Promise was rejected: ", error);
	            return errorRecoveryPromise();
	        })
		)
	    .then(
	        // resolve handler
	        $A.getCallback(function() {
	        	component.openModalPopup();
	           // return yetAnotherPromise();
	        })
	    );
    }
})