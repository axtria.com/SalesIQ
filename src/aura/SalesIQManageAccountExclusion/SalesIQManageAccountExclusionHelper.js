({
    logException : function(component, errors) {
        var errorLabel = '';
        if(component.get("v.namespace") != '') 
        {
            errorLabel = $A.get("$Label.AxtriaSalesIQTM.Unexpected_error_notification");
        }
        else
        {
            errorLabel = $A.get("$Label.c.Unexpected_error_notification");
        }

        if (errors[0] && errors[0].message) 
        {
            var errorMessage = errors[0].message;
            var action = component.get("c.logException");
        
            action.setParams({
                message : errorMessage,
                module : component.get("v.moduleName")
            });

            action.setCallback(this,function(response)
            {
                var state = response.getState();
                if(state === 'SUCCESS')
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: errorLabel,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }
        else 
        {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                mode: 'dismissible',
                message: errorLabel,
                type : 'error'
            });
            toastEvent.fire();
        }
    }
})