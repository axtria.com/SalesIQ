({
    doInit : function(component,event,helper)
    {
        // SIQ-3928
        //document.body.setAttribute('style', 'overflow: hidden;');
        // component.set("v.modalStyle", ".forceStyle.desktop .viewport {overflow:hidden !important} .slds-page-header__detail-row{display: none !important;}");
         //REMOVE UNUSED-CONFLICTING CSS FROM BROWSER
        try{
            jQuery('link[href*="DataTableCSS.css"]').remove();
            jQuery('#magicDropDownContainerL').remove();
        }catch(e){

        }
        component.getNamespace();

        var namespace = component.get("v.namespace");
        var allpositiontext;
        if(namespace != '')
        {
            allpositiontext = $A.get("$Label.AxtriaSalesIQTM.All_Positions");

        }
        else
        {
            allpositiontext = $A.get("$Label.c.All_Positions");
            
        }
        //allpositiontext = allpositiontext.replace(' ','');
        //allpositiontext = allpositiontext.toLowerCase();
        console.log('allpositiontext in datatable ::'+allpositiontext);
        component.set("v.allpositiontext",allpositiontext);

        var objectName = component.get("v.objectName");
        component.set("v.countryId", helper.getCookieValue(component,event,helper));

        // using an existing application event only for Scenario list page, so that ScenarioHeaderComponent can get country Id
         
        if(component.get("v.objectName") == 'Account_Exclusion__c'){
             var countryFlagEvent = component.getEvent("updateCountryFlag");
             countryFlagEvent.setParams({
                            countryId: component.get("v.countryId")
                        }).fire(); 
        }
        
        if( component.get("v.objectName") == 'Scenario__c') 
        {
         /*  component.set("v.disableMenuItem",false)
        }
        if( component.get("v.objectName") == 'Account_Exclusion__c') 
        {*/
            var evt = $A.get("e.c:onAssignPositionClick");
            if(evt != undefined) {
                evt.setParams({ "recordId": component.get("v.countryId")}).fire();
            }
        }
        
        if(objectName == 'Workspace__c' || objectName == 'Country__c'){   
            var action = component.get("c.checkCountryAccess");
            action.setParams
            ({
                country : component.get("v.countryId"),           
            });
    
            action.setCallback(this,function(response)
            {
                var state = response.getState();
                var result = response.getReturnValue();             
                if(state === 'SUCCESS' && result != null)
                {
                   if(result.Success)
                   {
                        var cookieStr = 'apex__CountryID' + "=" + escape(result.Success) + "; " + ";domain=.force.com;path=/";
                        document.cookie = cookieStr;
                        component.set("v.countryId",result.Success);
                        component.doInitialize();
                        if(document.getElementById('modal-datatable') == null || document.getElementById('modal-datatable').clientHeight == 0)
                        {
                            component.setDataTableHeight();
                        }

                        var countryFlagEvent = component.getEvent("updateCountryFlag");
                        countryFlagEvent.setParams({
                            countryId: component.get("v.countryId")
                        }).fire();

                    }else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            mode: 'dismissible',
                            message: result.Error,
                            type : 'Error'                          
                        });
                       toastEvent.fire();
                       component.hideSpin(); 
                   }
                }
                else if (state === "ERROR" || result == null) 
                { 
                    component.hideSpin(); 
                    helper.logException(component, response.getError());
                }
            });
            $A.enqueueAction(action);
        }
        else
        {
            component.doInitialize();
            component.set("v.loadBreadCrumb", 'true');
            component.set("v.loadTreeHierarchy", 'true');
            if(component.get("v.objectName") == 'Position__c' && component.get("v.moduleName") != 'Position Universe'){
                if(document.getElementById('treediv'))
                    document.getElementById('treediv').style.display = 'none';
                component.set("v.showMenuList", true);
            }
            /*else{
                if(document.getElementById('treediv'))
                    document.getElementById('treediv').style.display = 'block';
                component.set("v.showMenuList", false);
            }*/

            if(document.getElementById('modal-datatable') == null || !(document.getElementById('modal-datatable').clientHeight > 0))
            {
                component.setDataTableHeight();
            }
        } 

        var advanceSearchAction = component.get("c.getAdvanceSearchConfig");
        advanceSearchAction.setCallback(this,function(response){

            if(response.getState() != null && response.getReturnValue() != null){
                //var configData  = response.getReturnValue();
                console.log('config data -'+response.getReturnValue());
                if(component.get("v.objectName") == 'Position__c')
                    component.set("v.EnableAdvanceSearch",response.getReturnValue());
            }
        }); 
        $A.enqueueAction(advanceSearchAction);              
    },
    
    doInitialize : function(component,event,helper){
        console.log('Inside Do Initialize');
        var recent = component.get("v.loadRecent");
        $A.util.addClass(component.find('clearIcon'), "slds-hide");
        if(recent == "True")
        {
            helper.getRecentData(component,event,helper)
        }
        else if(component.get("v.listViewId") == '' || component.get("v.listViewId") == null)
        {
            helper.getFirstView(component,event,helper);
        }  
        else
        {
            var listViewId = component.get("v.listViewId");
            component.changeView(listViewId);
        }
    },

    hidePopup : function (component, event, helper) 
    {
        var $j  = jQuery.noConflict();
        $j("#magicDropDownContainerL").addClass('slds-hide');        
    },
    showMenuPopover : function (component, event, helper) 
    {
        var $j  = jQuery.noConflict();
        var globalId = component.getGlobalId();

        try{
            $j("#magicDropDownContainerL").addClass('slds-hide');  
            //var current_table_height = document.getElementById(globalId+"tableSection").offsetHeight;
           // var current_table_height = component.setDataTableHeight(component, event, helper) ;

            //var menuDivHeight = 120;
            //current_table_height += menuDivHeight;
            //document.getElementById(globalId+"tableSection").style.height = current_table_height +'px';
        }catch(e){

        }

    },

    setModalTableHeight : function(component,event,helper)
    {
        var globalId = component.getGlobalId();
        var searchBarHeight = document.getElementById(globalId+"searchBar").clientHeight;
        var contentHeight = document.querySelector("#positiondialog #modal-datatable").clientHeight -   parseInt(window.getComputedStyle(document.querySelector("#positiondialog #modal-datatable")).getPropertyValue("padding-top").replace('px','')) - parseInt(window.getComputedStyle(document.querySelector("#positiondialog #modal-datatable")).getPropertyValue("padding-bottom").replace('px',''));
        var headerDiv =  document.getElementsByClassName("forceListViewManagerHeader");
        var headerHeight;
        
        try{
            
             headerHeight = headerDiv[0].clientHeight;
            if(headerHeight == 0)
            {
                for(var i=0;i<headerDiv.length;i++)
                {
                    headerHeight = headerDiv[i].clientHeight;
                    if(headerHeight != 0)
                    {
                        break;
                    }
                }
            }
           
        } catch(e){
            
        }
        
        if(navigator.userAgent.match(/Trident.*rv\:11\./)){
            searchBarHeight = searchBarHeight + 5;
        }
        document.getElementById(globalId+"datatableContainer").style.height = contentHeight - (headerHeight + searchBarHeight) + 'px';
        document.getElementById(globalId+"datatableContainer").style.overflowY = 'auto';
    },
      calculateWidth: function(component, event, helper) {
        
        var childObj = event.target
        var mouseStart=event.clientX;
        component.set("v.currentEle", childObj);
        component.set("v.mouseStart",mouseStart);
        // Stop text selection event so mouse move event works perfectlly.
        if(event.stopPropagation) event.stopPropagation();
        if(event.preventDefault) event.preventDefault();
        event.cancelBubble=true; 
        event.returnValue=false;  
    },
    
    setNewWidth: function(component, event, helper) {
        var currentEle = component.get("v.currentEle");
        if( currentEle != null && currentEle.tagName ) {
            var parObj = currentEle;
            while(parObj.parentNode.tagName != 'TH') {
                if( parObj.className == 'slds-resizable__handle')
                    currentEle = parObj;    
                parObj = parObj.parentNode;
                count++;
            }
            var count = 1; 
            var mouseStart = component.get("v.mouseStart");
            var oldWidth = parObj.offsetWidth;  // Get the width of DIV
            var newWidth = oldWidth + (event.clientX - parseFloat(mouseStart));
            component.set("v.newWidth", newWidth);
            currentEle.style.right = ( oldWidth - newWidth ) +'px';
            component.set("v.currentEle", currentEle);
        }
    },
    // We are setting the width which is just changed by the mouse move event     
    resetColumn: function(component, event, helper) {
        // Get the component which was used for the mouse move
        if( component.get("v.currentEle") !== null ) {
            var newWidth = component.get("v.newWidth"); 
            var currentEle = component.get("v.currentEle").parentNode.parentNode; // Get the DIV
            var parObj = currentEle.parentNode; // Get the TH Element
            parObj.style.width = newWidth+'px';
            currentEle.style.width = newWidth+'px';
            console.log(newWidth);
            component.get("v.currentEle").style.right = 0; // Reset the column devided 
            component.set("v.currentEle", null); // Reset null so mouse move doesn't react again
        }
    },

    setDataTableHeight : function(component,event,helper)
    {
        // console.log('----> siddhant22--->');
        // console.log(document.querySelector("#datatableDiv #datatableContainer").clientHeight);
        var globalId = component.getGlobalId();
        // var searchBarHeight = document.querySelector("#datatableDiv #searchBar").clientHeight;
        var searchBarHeight = 0;
        if(document.getElementById(globalId + 'searchBar') != null)
        {
            searchBarHeight = document.getElementById(globalId + 'searchBar').clientHeight;
        }

        var contentHeight = 0;
        if(document.getElementsByClassName("slds-template__container") != null)
        {
            contentHeight = document.getElementsByClassName("slds-template__container")[0].clientHeight;
        }
        else
        {
            contentHeight = 548;
        }



        var forceBrandBand_h;
        try{
            var forceBrandBand_div =  document.getElementsByClassName("forceBrandBand");
            var forceBrandBand_div_19 = document.getElementsByClassName("forceBrandBand")['brandBand_1'];

            if(forceBrandBand_div_19 == undefined)
            {
                for(var i=0;i<forceBrandBand_div.length;i++)
                {
                    forceBrandBand_h= forceBrandBand_div[i]['clientHeight'];
                    if(forceBrandBand_h > 0){
                        break;
                    }
                }  
            }else{
                forceBrandBand_h = forceBrandBand_div_19['brandBand_1']['clientHeight'];
            }

            console.log('forceBrandBand_h :'+forceBrandBand_h)
        }catch(e){
            console.log(e);
        }


        if(component.get("v.loadBreadCrumb") == 'true' || component.get("v.loadTreeHierarchy") == 'true')
            searchBarHeight = searchBarHeight + 30;
        var headerDiv =  document.getElementsByClassName("forceListViewManagerHeader");
        var headerHeight;
        if( headerDiv != undefined && headerDiv[0] != undefined && headerDiv[0].clientHeight != undefined) {
            headerHeight = headerDiv[0].clientHeight;
            if(headerHeight == 0)
            {
                for(var i=0;i<headerDiv.length;i++)
                {
                    headerHeight = headerDiv[i].clientHeight;
                    if(headerHeight != 0)
                    {
                        break;
                    }
                }
            }
           
            if(component.get("v.moduleName") == 'Employee Universe'){
                if( document.getElementById(globalId+"datatableContainer") != null && document.getElementById(globalId+"datatableContainer") != undefined )
                    {
                        //tableHeight = headerHeight + searchBarHeight+25
                        document.getElementById(globalId+"datatableContainer").style.height = 
                        //"calc(100vh - "+tableHeight+"px)"
                        forceBrandBand_h - (headerHeight + searchBarHeight)-30 + 'px';
                    }
            }
            else{
                if( document.getElementById(globalId+"datatableContainer") != null && document.getElementById(globalId+"datatableContainer") != undefined )
                    {
                        //tableHeight = headerHeight + searchBarHeight+25
                        document.getElementById(globalId+"datatableContainer").style.height  =  //"calc(100vh - "+tableHeight+"px)"
                        forceBrandBand_h - (headerHeight + searchBarHeight)-30 + 'px';
                    }
            }
        }
      
        // if(component.get("v.moduleName") == 'Employee Universe')
        //     document.getElementById(globalId+"datatableContainer").style.height = contentHeight + 30 - (headerHeight + searchBarHeight) + 'px';
        // else
        //     document.getElementById(globalId+"datatableContainer").style.height = contentHeight - (headerHeight + searchBarHeight) + 'px';
        if( document.getElementById(globalId+"datatableContainer") != null && document.getElementById(globalId+"datatableContainer") != undefined ){
           // document.getElementById(globalId+"datatableContainer");
           // debugger;
            document.getElementById(globalId+"datatableContainer").style.overflowY = 'auto';
            //return  document.getElementById("table1").offsetHeight;
            //
            return 10;
             
        }
    },
    checkExtensionsButton :function(component, event ,helper){
        //checking external buttons 
        var action1 = component.get("c.checkExtensionButtons");
        
        action1.setParams({
                moduleName:component.get("v.moduleName")
        });

        action1.setCallback(this,function(response){
            var state =  response.getState();
            var result = response.getReturnValue();             
            if(state === 'SUCCESS' && result != null){
               console.log(result);
               if(result.length>0){
                    var results=[];

                    for(var i=0;i<result.length;i++){
                        var namespace=component.get("v.namespace");
                        results.push({"Name":result[i][namespace+"Package_Extension__r"][namespace+"ClassName__c"],"Label": result[i]["Label"], "Behavior": result[i][namespace+"Behavior__c"], "Style": result[i][namespace+"Style__c"]});
                    }
                    console.log('results');
                    console.log(results);
                    component.set('v.rosterExtensionButtonList',results);
               }
            }
            else if (state === "ERROR" || result == null) 
            { 
                component.hideSpin(); 
                helper.logException(component, response.getError());
            }
        });
        $A.enqueueAction(action1);
     },
    getNamespace : function(component,event,helper)
    {
        var action = component.get("c.getOrgNamespace");

        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var ns = response.getReturnValue();
                component.set("v.namespace",ns);
                if(component.get("v.showCustomButtons"))
                    component.extensionsButton();
            }
            else if (state === "ERROR" || response.getReturnValue() == null) 
            {
                console.log('ERROR: namespace callback failed');
            }
            // ERROR state will not work as we can't get the label without namespace
            /*else if (state === "ERROR" || response.getReturnValue() == null) 
            {
                var errorLabel = $A.get("$Label.c.Unexpected_error_notification");
                var errors = response.getError();
                var errorMessage = errors[0].message;
                if (errors[0] && errors[0].message) 
                {
                    var action = component.get("c.logException");
        
                    action.setParams({
                        message : errorMessage,
                        module : component.get("v.moduleName")
                    });

                    action.setCallback(this,function(response)
                    {
                        var state = response.getState();
                        if(state === 'SUCCESS')
                        {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                mode: 'dismissible',
                                message: errorLabel,
                                type : 'error'
                                
                            });
                            toastEvent.fire();
                        }
                    });
                    $A.enqueueAction(action);
                } 
                else 
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: errorLabel,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                }
            }*/    
        });


        action.setBackground();
        $A.enqueueAction(action);   

    },

    viewChange: function(component, event, helper) 
    {

        console.log('event in viewchange datatable  -- ');
        component.deselectCheckbox();
        //reload lightning grid in helper while fetching records for clicked summary cards
        component.set("v.loadTable", 'false');

        var params = event.getParam('arguments');
        if (params) {
            var tableView = params.selectedView;
            var queryString = params.queryString;
            console.log('filter viewchange : '+params.invokeFromListView);
        }
        
        var filterquery = component.get("v.filterQueryWhereClause");
        component.set("v.listViewId",tableView);

        component.set("v.queryString",queryString);
        component.set("v.invokeFromListView", params.invokeFromListView);

        helper.getRecords(component,event,helper);
        let globalId = component.getGlobalId();
        if(document.getElementById(globalId+'datatableContainer') != null && document.getElementById(globalId+'datatableContainer') != undefined )
            document.getElementById(globalId+'datatableContainer').scrollTop = 0;
        var ViewChangeFilterRefreshMessage;
      
        if(params.invokeFromListView==true && filterquery.includes("value")) 
        {
             if(component.get("v.namespace") != '') {
   		ViewChangeFilterRefreshMessage = $A.get("$Label.AxtriaSalesIQTM.ViewChangeFilterRefreshMessage");
        		}
            else
                {
		ViewChangeFilterRefreshMessage = $A.get("$Label.c.ViewChangeFilterRefreshMessage");
        		}
        }
        var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: ViewChangeFilterRefreshMessage,
                        type : 'success'
                    });
        toastEvent.fire();
        component.getEvent("resetFilterConditions").fire();               
                 
        
    },

    refreshData: function(component, event, helper) {

        var params = event.getParam('arguments');
        console.log('params -',params);
        if(params)
           var filterQueryWhereClause = params.filterQueryWhereClause;

        component.set("v.filterQueryWhereClause", filterQueryWhereClause);

        console.log('-- refreshdate in datatable -- ',component.get("v.filterQueryWhereClause"));

        helper.getRecords(component,event,helper);

        if(component.get("v.showTreeHierarchy") == 'true') 
        {
            var treeHierarchyComp = component.find("treeHierarchy");
            treeHierarchyComp.refreshTree(component.get("v.countryId"));
        }
    },

    next : function(component,event,helper)
    {
        var columnsList = component.get("v.dataTableWrapper.columnsList");
        if(((component.get("v.dataTableWrapper.pageNumber")) * component.get("v.dataTableWrapper.pageSize")) >= component.get("v.dataTableWrapper.totalRecords"))
        {
            return;
        }

        var action = component.get("c.getNextData");
        var pageNumber = component.get("v.dataTableWrapper.pageNumber") + 1;
        
        if(component.get("v.dataTableWrapper.queryString") == null && component.get("v.dataTableWrapper.columnsList") == null)
            return ;
        
        action.setParams({
            objectName : component.get("v.objectName"),
            pageNumber : pageNumber,
            querystring : component.get("v.dataTableWrapper.queryString"),
            columnNames : component.get("v.dataTableWrapper.columnsList"),
            typeList : component.get("v.dataTableWrapper.dataTypesList"),
            selectedPosition : component.get("v.selectedPosition"),
            selectedTeamInstance : component.get("v.selectedTeamInstance"),
            moduleName : component.get("v.moduleName"),
            "countryId": component.get("v.countryId")
        });

        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var retResponse = response.getReturnValue();
                var tempDataMapList = retResponse.dataMap;

                var dataMap = component.get("v.dataTableWrapper.dataMap");
                for(var j=0;j<tempDataMapList.length;j++) 
                {
                    dataMap.push(tempDataMapList[j]);
                }
                component.set("v.dataTableWrapper.pageNumber", pageNumber);
                component.set("v.dataTableWrapper.dataMap", dataMap);
                
                // document.body.scrollTop = 0; // For Safari
                // document.documentElement.scrollTop = 0; 
                // document.getElementById(globalId+'datatableContainer').scrollTop = 0;

                var load_more_card = component.find('load_more_card');
                if(((component.get("v.dataTableWrapper.pageNumber")) * component.get("v.dataTableWrapper.pageSize")) >= component.get("v.dataTableWrapper.totalRecords"))
                {
                    $A.util.addClass(load_more_card, "slds-hide");
                }
                else
                {
                     $A.util.removeClass(load_more_card, "slds-hide");
                }

                if(component.get("v.firstColumn") == 'Checkbox')
                {
                    component.getDisabledCheckbox(component,event);
                }
            }
            else if (state === "ERROR" || response.getReturnValue() == null) 
            {
                helper.logException(component, response.getError());
            }    
        });

        action.setBackground();
        $A.enqueueAction(action);
    },

    sortColumn : function(component,event,helper)
    {
        component.showSpin();
        //component.deselectCheckbox();
        var selectedColumn = event.target.id;
        if(selectedColumn)
        {
            var columnsArray = component.get("v.dataTableWrapper.columnsArray");
            
            var columnsList = component.get("v.dataTableWrapper.columnsList");

            selectedColumn = parseInt(selectedColumn.split('_')[1]);
            var direction = columnsArray[selectedColumn].dir;

            var action = component.get("c.getSortData");
            
            action.setParams({
                objectName : component.get("v.objectName"),
                columnName : columnsArray[selectedColumn].data,
                direction : direction,
                querystring : component.get("v.dataTableWrapper.queryString"),
                columnNames : component.get("v.dataTableWrapper.columnsList"),
                typeList : component.get("v.dataTableWrapper.dataTypesList"),
                selectedPosition : component.get("v.selectedPosition"),
                selectedTeamInstance : component.get("v.selectedTeamInstance"),
                moduleName : component.get("v.moduleName"),
                "countryId" : component.get("v.countryId")
            });

            action.setCallback(this,function(response)
            {
                var state = response.getState();
                if(state === 'SUCCESS' && response.getReturnValue != null)
                {
                    var retResponse = response.getReturnValue();
                    var retRecords = retResponse.dataMap;
                    var fields = retResponse.columnsList ;
                    var labels = retResponse.columnLabels;
                    var querystring = retResponse.queryString;
                    var typeList = component.get("v.dataTableWrapper.dataTypesList");

                    for(var i =0;i<columnsArray.length;i++)
                    {
                        if(i == selectedColumn)
                        {
                            if(columnsArray[i].dir == 'ASC')
                            {
                                columnsArray[i].dir = 'DESC';
                            }
                            else
                            {
                                columnsArray[i].dir = 'ASC';
                            }
                            columnsArray[i].selected = true;
                            component.set("v.dataTableWrapper.sortColumn",columnsArray[i].title);
                            component.set("v.empSortColumn",columnsArray[i].title);
                        }
                        else
                        {
                            if(typeList[columnsArray[i].data] == 'DATETIME' || typeList[columnsArray[i].data] == 'DATE' || typeList[columnsArray[i].data] == 'NUMBER')
                                columnsArray[i].dir = 'ASC';
                            else
                                columnsArray[i].dir = 'DESC';
                            columnsArray[i].selected = false;
                        }
                        
                    }

                    component.set("v.dataTableWrapper.dataMap",retRecords)
                    component.set("v.dataTableWrapper.pageNumber" , 1);
                    component.set("v.dataTableWrapper.queryString" , querystring);
                    component.set("v.dataTableWrapper.columnsArray",columnsArray);
		    
                    helper.updateBreadcrumb(component,event,"true");
                    component.deselectCheckbox();
                    if(component.get("v.firstColumn") == 'Checkbox')
                        component.getDisabledCheckbox(component,event);

                    component.hideSpin();
                }

                else if (state === "ERROR" || response.getReturnValue() == null) 
                {
                    component.hideSpin(); 
                    helper.logException(component, response.getError());
                }    
            });


            action.setBackground();
            $A.enqueueAction(action);
        }
        else
        {
            component.hideSpin();
        }
    },    

    clearSearch :function(component,event,helper)
    {
        var globalId = component.getGlobalId();
        document.getElementById(globalId+'_text-input-01').value = "";
        $A.util.addClass(component.find('clearIcon'), "slds-hide");
        component.clearSearch();
    },

    searchKeyChange : function(component,event,helper)
    {

        component.set("v.loadTable", 'false');
        
        console.log('-- calling searchKeyChange ---');

        var timer = component.get('v.timer');
            clearTimeout(timer);
            function doneTyping () {
                console.log('called in place search');
                var searchText = '';
                var globalId = component.getGlobalId();
                component.set("v.loadTable",'false');
                searchText = document.getElementById(globalId+'_text-input-01').value;

                if(searchText != undefined && searchText.length == 0)
                {
                    $A.util.addClass(component.find('clearIcon'), "slds-hide");
                }
                else
                {
                    $A.util.removeClass(component.find('clearIcon'), "slds-hide");
                }
                component.deselectCheckbox();
                var action = component.get("c.getSearchData");
                action.setParams({
                    searchText : searchText,
                    querystring : component.get("v.dataTableWrapper.queryString"),
                    searchString : component.get("v.dataTableWrapper.searchString"),
                    objectName : component.get("v.objectName"),
                    columnNames : component.get("v.dataTableWrapper.columnsList"),
                    typeList : component.get("v.dataTableWrapper.dataTypesList"),
                    selectedPosition : component.get("v.selectedPosition"),
                    selectedTeamInstance : component.get("v.selectedTeamInstance"),
                    moduleName : component.get("v.moduleName"),
                    "countryId" : component.get("v.countryId")
                });

                action.setCallback(this,function(response)
                {
                    var state = response.getState();
                    if(state === 'SUCCESS' && response.getReturnValue != null)
                    {
                        var retResponse = response.getReturnValue();
                        console.log('retResponse :',retResponse);
                        var table_body = component.find('table1');
                        var card = component.find('no_record_card');
                        
                        var retRecords = retResponse.dataMap;
                        var querystring = retResponse.queryString;

                        if(retRecords.length == 0)
                        {
                            $A.util.removeClass(card, "slds-hide");
                            $A.util.addClass(table_body, "slds-hide");  
                        }
                        else
                        {
                            $A.util.removeClass(table_body, "slds-hide");
                            $A.util.addClass(card, "slds-hide");
                        }

                        component.set("v.dataTableWrapper.dataMap", retRecords);
                        component.set("v.dataTableWrapper.dataGridList", retResponse.dataGridList);
                        component.set("v.dataTableWrapper.pageNumber" , 1);
                        component.set("v.dataTableWrapper.queryString" , querystring);
                        component.set("v.dataTableWrapper.searchString",retResponse.searchString);
                        component.set("v.dataTableWrapper.totalRecords" , retResponse.totalRecords);
                        component.set("v.dataTableWrapper.pageSize" , retResponse.pageSize);

                        console.log('--- 111111');
                        if(component.get("v.firstColumn") == 'Checkbox')
                            component.getDisabledCheckbox(component,event);


                        component.set("v.loadTable",'true');
                        helper.updateBreadcrumb(component,event,"true");
                        window.setTimeout(
                            $A.getCallback(function() {
                                console.log('Calling'); 
                                var posActionEvent = component.getEvent("setPositionAction");
                                var editEmployeeHeader = 'Dynamic Style';
                                
                                posActionEvent.setParams({
                                    "actionType": editEmployeeHeader,
                                });               

                                posActionEvent.fire();
                            }), 200
                        );

                        

                        var load_more_card = component.find('load_more_card');
                        if(((component.get("v.dataTableWrapper.pageNumber")) * component.get("v.dataTableWrapper.pageSize")) >= component.get("v.dataTableWrapper.totalRecords"))
                        {
                            $A.util.addClass(load_more_card, "slds-hide");
                        }
                        else
                        {
                             $A.util.removeClass(load_more_card, "slds-hide");
                        }

                    }

                    else if (state === "ERROR" || response.getReturnValue() == null) 
                    {
                        if(state === "ERROR")
                        {
                            helper.logException(component, response.getError());
                        }
                        if(response.getReturnValue() == null)
                        {
                            var table_body = component.find('table1');
                            $A.util.addClass(table_body, "slds-hide");
                        }
                    }    
                });


                action.setBackground();
                $A.enqueueAction(action);
                
            }

            var timer = setTimeout(function(){
                doneTyping();
                clearTimeout(timer);
                component.set('v.timer', null);
            }, 200);
            component.set('v.timer', timer);  
        
    },

    selectRecord : function(component,event,helper)
    {
        var selected_record = event.target.id;
        if(selected_record)
        {
            var i = parseInt(selected_record.split('_')[1]);
            var j = parseInt(selected_record.split('_')[2]);
            var dataMap = component.get("v.dataTableWrapper.dataMap");
            var recordId = dataMap[i][j].referenceId;
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": recordId,
                "isredirect" : true
                });
            navEvt.fire();
        }    
    },

    export : function(component,event,helper)
    {
        var action = component.get("c.getExport");
        console.log('-- calling export in datatable----');
        console.log('1 :'+component.get("v.dataTableWrapper.queryString"));
        console.log('2 :'+component.get("v.dataTableWrapper.columnsList"));
        console.log('3 :',component.get("v.dataTableWrapper.dataTypesList"));
        
        action.setParams({
            objectName : component.get("v.objectName"),
            querystring : component.get("v.dataTableWrapper.queryString"),
            columnNames : component.get("v.dataTableWrapper.columnsList"),
            typeList : component.get("v.dataTableWrapper.dataTypesList"),
            unassignedterrcode : '00000'
 
        });

        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var retResponse = response.getReturnValue();
                var retRecords = retResponse.dataArray;
                
                var dataList = retRecords;
                var columnsList = component.get("v.dataTableWrapper.columnsArray");
                var data = [];
                var headerArray = [];
                var csvContentArray = [];
                //headerArray.push('Sr.No.');
                //Fill out the Header of CSV
                for(var i = 0;i<columnsList.length;i++)
                {
                    headerArray.push(columnsList[i].title);
                }
                data.push(headerArray);
                //var sno = 0;
                for(var i=0;i<dataList.length;i++)
                {
                    //Check for records selected by the user
                    //Initialize the temperory array
                    var tempArray = [];
                    //use parseInt to perform math operation
                    //sno = parseInt(sno) + parseInt(1);
                    //tempArray.push('"'+sno+'"');
                    for (var j = 0; j < dataList[i].length; j++) 
                    {   
                        tempArray.push('"'+dataList[i][j]+'"');

                    }
                    data.push(tempArray); 
                }

                
                
                for(var j=0;j<data.length;j++)
                {
                    var dataString = data[j].join(",");
                    csvContentArray.push(dataString);
                }
                var csvContent = csvContentArray.join("\r\n");
                
                //Generate a file name
                var PositionName;
                //var namespace = component.get("v.dataTableWrapper.orgNamespace");

                if(component.get("v.namespace") != '') {
                        PositionName = $A.get("$Label.AxtriaSalesIQTM.All_Positions");
                        
                    }
                    else
                    {
                        PositionName = $A.get("$Label.c.All_Positions");
                        
                    }
                
                var columnsArray = component.get("v.dataTableWrapper.columnsArray");
                var dataMap = component.get("v.dataTableWrapper.dataMap");
                
                for(var i=0;i<columnsArray.length;i++)
                {   
                     
                    if (columnsArray[i].data.indexOf(component.get("v.namespace")+'Team_iD__r.Name') != -1)
                        {
                            var team = i;
                            break;
                        }
                }
                
                if(team != undefined)
                {
                    var Team_Name = dataMap[0][team].data;

                }
                else
                {
                    var Team_Name = '';
                }

                var moduleName = component.get("v.moduleName");
                if(component.get("v.moduleName") == 'Employee Universe')
                {
                    var fileName = component.get("v.dataTableWrapper.objectLabel") + " UNIVERSE";
                    fileName += ".csv";
                }
                else if(component.get("v.moduleName") == 'Position Universe')
                {
                    var fileName = Team_Name + " " + PositionName;
                    fileName += ".csv";

                }
                
                //Initialize file format you want csv or xls
                var uri = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csvContent);
                
                if (navigator.msSaveBlob) 
                { // IE 10+
                    var blob = new Blob([csvContent],{type: "text/csv;charset=utf-8;"});
                    navigator.msSaveBlob(blob, fileName);
                }
                else
                {
                    // Download file
                    // you can use either>> window.open(uri);
                    // but this will not work in some browsers
                    // or you will not get the correct file extension    
                    var link = document.createElement("a");

                    //link.download to give filename with extension
                    //link.download = fileName;
                    link.setAttribute('download',fileName);
                    //To set the content of the file
                    link.href = uri;
                    
                    //set the visibility hidden so it will not effect on your web-layout
                    link.style = "visibility:hidden";
                    
                    //this part will append the anchor tag and remove it after automatic click
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                }
            }

            else if (state === "ERROR" || response.getReturnValue() == null) 
            {
                
                helper.logException(component, response.getError());
            }    
        });


        $A.enqueueAction(action);
    },

    showSpinner : function (component, event, helper) 
    {
        var spinner_container = component.find('spinner_container');
        $A.util.removeClass(spinner_container, "slds-hide"); 
    },

    hideSpinner : function (component, event, helper) 
    {
        var spinner_container =component.find('spinner_container');

        $A.util.addClass(spinner_container, "slds-hide");        
    },

    showMoreSpinner : function (component, event, helper)
    {
        var spinner_container = component.find('load_spinner_container');
        $A.util.removeClass(spinner_container, "slds-hide"); 
    },

    hideMoreSpinner : function (component, event, helper) 
    {
        console.log('-- hideMoreSpinner --');
        var spinner_container = component.find('load_spinner_container');

        $A.util.addClass(spinner_container, "slds-hide");        
    }, 
    

    menuEdit : function(component,event,helper)
    {
        if(!event.target) return;
        if(event.target.parentNode === undefined) return;

        var idx = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        if(!idx) return; //there is NO data associated
        var ix = parseInt(idx);
        var dataMap = component.get("v.dataTableWrapper.dataMap");
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        for(var i=0;i<columnsArray.length;i++)
        {
            if (columnsArray[i].data == 'Name')
            {
                break;
            }
        }
        var recordId = dataMap[ix][i].referenceId;

        var customEdit = component.get("c.getEditEmployeeConfigSetting");
        customEdit.setCallback(this,function(response){
            if(response.getState() == 'SUCCESS' && response.getReturnValue() != null){
                console.log('-----  '+response.getReturnValue());
                if(response.getReturnValue() != 'true'){
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": recordId
                    });
                    navEvt.fire();
                }else{
                    console.log('-- calling custom edit ---');
                    var posActionEvent = component.getEvent("setPositionAction");
                    var editEmployeeHeader = 'Edit Employee';
                    

                    console.log('query in position action - ',component.get("v.queryString"));

                    posActionEvent.setParams({
                        "actionType": editEmployeeHeader,
                        "modalTitle": editEmployeeHeader,
                        "selectedPosition": recordId
                        
                    });               

                    posActionEvent.fire();
                }
            }
        });
        $A.enqueueAction(customEdit);
        
    },

    menuManageAssignment : function(component,event,helper)
    {
        
        if(!event.target) return;
        if(event.target.parentNode === undefined) return;

        
        component.set('v.referencedObject','table grid');
        component.set("v.loadTable", 'false');

        var idx = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        if(!idx) return; //there is NO data associated
        var ix = parseInt(idx);
        var dataMap = component.get("v.dataTableWrapper.dataMap");
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        for(var i=0;i<columnsArray.length;i++)
        {
            if (columnsArray[i].data == 'Name')
            {
                break;
            }

        }

        var moduleName = component.get("v.moduleName");
        var recordId = dataMap[ix][i].referenceId;
        var startD = new Date(); 
        startD.setMonth(startD.getMonth()-6);
        startD.setDate(1);
        var endD = new Date(); 
        endD.setMonth(endD.getMonth()+6);
        endD.setDate(0);

         if(moduleName=="Employee Universe"){
            console.log('--recordId--');
            console.log(recordId);
            var action = component.get("c.getSelectedTeamInstance");
            action.setParams({
                "employeeId" : recordId
            });
            action.setCallback(this,function(response)
            {
                var state = response.getState();
                if(state === 'SUCCESS')
                {   
                    if(response.getReturnValue != null || response.getReturnValue !=''){
                        var selectedTeamInstance = response.getReturnValue();
                        component.set("v.selectedTeamInstance",selectedTeamInstance);
                    }
                    var evt = $A.get("e.force:navigateToComponent");
                    evt.setParams({
                        componentDef : "c:SalesIQManageAssignment",
                        componentAttributes: {
                            recordId : recordId,
                            assigneeName : dataMap[ix][i].data,
                            startDate : startD,
                            endDate : endD,
                            moduleName : moduleName,
                            selectedTeamInstance : component.get("v.selectedTeamInstance"),
                            selectedListView : component.get("v.listViewId"),
                            namespace : component.get("v.namespace"),
                            profileMap : component.get("v.profileMap")
                        },
                        isredirect : true
                    });
                    evt.fire();
                }
                else if (state === "ERROR" || response.getReturnValue() == null) 
                {
                    console.log('ERROR: getSelectedTeamInstance callback failed');
                }    
            });
            $A.enqueueAction(action);
            
        }
        else{
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:SalesIQManageAssignment",
                componentAttributes: {
                    recordId : recordId,
                    assigneeName : dataMap[ix][i].data,
                    startDate : startD,
                    endDate : endD,
                    moduleName : moduleName,
                    selectedTeamInstance : component.get("v.selectedTeamInstance"),
                    selectedListView : component.get("v.listViewId"),
                    namespace : component.get("v.namespace"),
                    profileMap : component.get("v.profileMap")
                },
                isredirect : true
            });
            evt.fire();
        }
    },

    menuScenarioAssignment : function(component,event,helper)
    {
           
        if(!event.target) return;
        if(event.target.parentNode === undefined) return;

        var idx = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        if(!idx) return; //there is NO data associated
        var ix = parseInt(idx);
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        var dataMap = component.get("v.dataTableWrapper.dataMap");
        var namespace = component.get("v.dataTableWrapper.orgNamespace");
        
        for(var i=0;i<columnsArray.length;i++)
        {
            if (columnsArray[i].data.indexOf(namespace+'Team_Name') != -1)
            {
                var team = i;
            }
            else
            if (columnsArray[i].data.indexOf(namespace+'Team_Instance') != -1)
            {
                var team_instance = i;
            }
            else
            if (columnsArray[i].data.indexOf(namespace+'Request_Process_Stage') != -1)
            {
                var request_stage = i;
            }
            else
            if (columnsArray[i].data.indexOf(namespace+'Scenario_Status__c') != -1)
            {
                var scenario_status = i;
            }
        }
        

        if(team != undefined)
        {
            var Team_Id = dataMap[ix][team].referenceId;
        }
        else
        {
            var Team_Id = '';
        }
        if(team_instance != undefined)
        {
           var Team_Instance_Id = dataMap[ix][team_instance].referenceId;
        }
        else
        {
           var Team_Instance_Id = '';
        }

        //code updated by - Lagnika
        var showScenarioBt = 'true';
        var url = '/apex/'+namespace+'ScenarioAlignment?selectedTeam=' + Team_Id + '&selectedCycle=' + Team_Instance_Id + '&showScenarioBt='+ showScenarioBt;
               
        if(request_stage != undefined && scenario_status != undefined)
        {
            if(dataMap[ix][request_stage].data == 'Ready' && dataMap[ix][scenario_status].data != 'Inactive')
            {        
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                "url": url
                });
                urlEvent.fire();
                      
            }
        }
    },
    
    menuRerunScenarioValidation : function(component,event,helper){
        if(!event.target) return;
        if(event.target.parentNode === undefined) return;

        var idx = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        console.log('idx : '+idx);
        if(!idx) return; //there is NO data associated
        var ix = parseInt(idx);
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        var dataMap = component.get("v.dataTableWrapper.dataMap");
        var namespace = component.get("v.dataTableWrapper.orgNamespace");
        
        for(var i = 0 ; i <  columnsArray.length ; i++){
            if (columnsArray[i].data.indexOf(namespace+'Team_Instance') != -1){
                var Team_Instance_Id = dataMap[ix][i].referenceId;
                console.log('Team_Instance_Id : '+Team_Instance_Id);
                //Update Request_Process_Stage to Validadtion In Queue for scenario
                var action = component.get("c.setValidationInQueue");
                action.setParams({
                    "teamInstanceId" : Team_Instance_Id
                });
                action.setCallback(this,function(response){
                    var state = response.getState();
                    console.log('state : '+state);
                    if(state == 'SUCCESS' && response.getReturnValue() != null){
                        console.log('response : '+response.getReturnValue());
                        if(response.getReturnValue() != 'error'){
                            //Refresh the workspace
                            var url = '/'+response.getReturnValue();
                            var urlEvent = $A.get("e.force:navigateToURL");
                            urlEvent.setParams({"url": url});
                            urlEvent.fire();
                        }
                    }
                });
                $A.enqueueAction(action);
            }
        }
    },
    
    menuDeleteScenario : function(component,event,helper){

        console.log(component.get("v.recordId"));
        var action = component.get("c.setDeleteInQueue");
        action.setParams({
            "scenarioID" : component.get("v.recordId")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            console.log('state : '+state);
            if(state == 'SUCCESS' && response.getReturnValue() != null){
                console.log('response : '+response.getReturnValue());
                if(response.getReturnValue() != 'error'){
                    //Refresh the workspace
                    var url = '/'+response.getReturnValue();
                    var urlEvent = $A.get("e.force:navigateToURL");
                    urlEvent.setParams({"url": url});
                    urlEvent.fire();

                    var deleteScenarioSuccessMessage;
                    if(component.get("v.namespace") != '') {
                        deleteScenarioSuccessMessage = $A.get("$Label.AxtriaSalesIQTM.Delete_Scenario_Toast_Message");
                        
                    }
                    else
                    {
                        deleteScenarioSuccessMessage = $A.get("$Label.c.Delete_Scenario_Toast_Message");
                        
                    }

                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: deleteScenarioSuccessMessage,
                        type : 'success'
                    });
                    toastEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);

        
    },

    menuCallPlan : function(component,event,helper){

      if(!event.target) return;
        if(event.target.parentNode === undefined) return;

        var idx = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        if(!idx) return; //there is NO data associated
        var ix = parseInt(idx);
        var referenceWrapperList = component.get("v.dataTableWrapper.referenceWrapperList");
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        var dataMap = component.get("v.dataTableWrapper.dataMap");
        var dataArray = component.get("v.dataTableWrapper.dataArray");
        var namespace = component.get("v.dataTableWrapper.orgNamespace");
        
        for(var i=0;i<columnsArray.length;i++)
        {
            if (columnsArray[i].data.indexOf('Team_Name') != -1)
            {
                var team = i;
            }
            else
            if (columnsArray[i].data.indexOf('Team_Instance') != -1)
            {
                var team_instance = i;
            }
            else
            if (columnsArray[i].data.indexOf('Request_Process_Stage') != -1)
            {
                var request_stage = i;
            }
            else
            if (columnsArray[i].data.indexOf('Scenario_Status__c') != -1)
            {
                var scenario_status = i;
            }
        }
        

        if(team != undefined)
        {
            var Team_Id = dataMap[ix][team].referenceId;
        }
        else
        {
            var Team_Id = '';
        }
        if(team_instance != undefined)
        {
           var Team_Instance_Id = dataMap[ix][team_instance].referenceId;
        }
        else
        {
           var Team_Instance_Id = '';
        }

       
        if(request_stage != undefined && scenario_status != undefined)
        {
            if(dataMap[ix][request_stage].data == 'Ready' && dataMap[ix][scenario_status].data != 'Inactive')
            {        
                var evt = $A.get("e.force:navigateToComponent");
                evt.setParams({
                    componentDef : "c:SalesIQCallPlan",
                    componentAttributes: {
                        selectedTeamInstance : Team_Instance_Id
                    }
                });
                evt.fire();
            }
        }  
    },

    menuCanvas : function(component,event,helper){
        console.log('inside menucanvas');
        if(!event.target) return;
        if(event.target.parentNode === undefined) return;

        var idx = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        if(!idx) return; //there is NO data associated
        var ix = parseInt(idx);
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        var dataMap = component.get("v.dataTableWrapper.dataMap");
        var dataArray = component.get("v.dataTableWrapper.dataArray");
        var namespace = component.get("v.dataTableWrapper.orgNamespace");
        namespace = namespace.replace("__","");
        console.log(dataMap);
        console.log(columnsArray);
        var scenarioStage = '';
        var team_instance = '';
        
        var scenario_Id = '' ;
        for(var i=0;i<columnsArray.length;i++)
        {
            if(columnsArray[i].data.indexOf('Name') != -1){

                if(dataMap[ix][i].isLink == 'FORMULA')
                {
                    //scenario_Id = dataMap[ix][i].referenceId.split('sObject')[1].split('view')[0].replace('/','').replace('/','');
                    scenario_Id = dataMap[ix][i]['data'].split('/')[1].split(' ')[0].split('"')[0];
               }else{   
                    //scenario_Id = dataMap[ix][i].referenceId;
                    scenario_Id = dataMap[ix][i]['data'];
                }

                break;
            }
        }
        
        var workspace_Id = component.get("v.recordId");
        var viewCanvas = "Y";                         
        var viewcanvas = component.set("v.viewcanvas",viewCanvas);
        var Scenario_Type = "Call Plan Generation";

        for(var i=0;i<columnsArray.length;i++)
        {
            if(columnsArray[i].data.indexOf('Scenario_Type__c') != -1)
            {
                Scenario_Type = dataMap[ix][i].data;
                break;
            }
        }
       
        console.log("record_Id :::::::::::::::::::::: "+scenario_Id);
        console.log("workspace_Id ----- "+workspace_Id);
        var evt = $A.get("e.force:navigateToComponent");

            evt.setParams({
                componentDef : "c:CPGbusinessRuleCanvas",
                componentAttributes: {
                    record_Id: scenario_Id, 
                    viewcanvas : viewCanvas,
                    workSpaceId : workspace_Id ,
                    ScenarioType : Scenario_Type,
                    scenarioStage : scenarioStage,
                    countryId : component.get("v.countryId")
                    // viewCanvas : team_instance
                },
                isredirect : true
             });
             evt.fire();
    
    },
    
    selectCheckbox : function(component,event,helper)
    {
        if(!event.target) return;
        var idx = event.target.id;
        if(!idx) return; //there is NO data associated
        idx = idx.split('add-checkbox-')[1];
        var ix = parseInt(idx);
        var dataMap = component.get("v.dataTableWrapper.dataMap");
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        for(var i=0;i<columnsArray.length;i++)
        {
            if (columnsArray[i].data == 'Name')
            {
                break;
            }

        }
        var recordId = dataMap[ix][i].referenceId;
        component.set("v.selectedPosData", dataMap[ix][i]);

        if(component.get("v.selectedCheckboxList").indexOf(recordId) != -1 && ((component.get('v.moduleName') == "Position Universe" && component.get("v.objectName") == 'Employee__c') ||  (component.get('v.moduleName') == "Employee Universe" && component.get("v.objectName") == 'Position__c')))
        {
            component.deselectCheckbox();
            return;
        }
        

        var selectedCheckboxList = [];
        var globalId = component.getGlobalId();
        if(document.getElementById(globalId+'_add-checkbox-'+ix).checked)
        {
            selectedCheckboxList.push(recordId); 
            if(document.getElementById('leftTab') != null && document.getElementById('rightTab') != null)
            {
                document.getElementById('rightTab').classList.remove('slds-hide');
                document.getElementById('leftTab').classList.remove('slds-size--12-of-12');
                document.getElementById('leftTab').classList.add('slds-size--9-of-12');
                if(document.getElementsByClassName('legendPanel')[0] && document.getElementsByClassName('legendPanel')[0].offsetHeight > 0)
                {
                  document.getElementsByClassName('legendPanel')[0].style.display = 'none';
                }
                else
                {
                  if(document.getElementsByClassName('legendPanel').length > 1)
                    document.getElementsByClassName('legendPanel')[1].style.display = 'none';
                }
            }
            
            var rowLength = dataMap[ix].length;
            var menuActionsColumn = dataMap[ix][rowLength-1];
            var evt = component.getEvent("selectCheckbox");
            evt.setParams({
                "recordId" : recordId,
                "actionState" : menuActionsColumn.isCreate,
                "teamInstanceId" : component.get("v.dataTableWrapper.teamInstance"),
                "teamType" : component.get("v.dataTableWrapper.teamType")
            });
            evt.fire();
            if(component.get("v.showPositionHierarchy") == 'true')
            {
                var positionhierarchy = component.find('positionhierarchy');
                positionhierarchy.openBreadcrumb();
                positionhierarchy.checkboxSelect(recordId);
            }    
            component.set("v.globalActionState", menuActionsColumn.isCreate);
        }
        else
        {
            selectedCheckboxList = [];
            if(document.getElementById('leftTab') != null && document.getElementById('rightTab') != null)
            {
                document.getElementById('rightTab').classList.add('slds-hide');
                document.getElementById('leftTab').classList.remove('slds-size--9-of-12');
                document.getElementById('leftTab').classList.add('slds-size--12-of-12');
            }
            
            var evt = component.getEvent("deselectCheckboxEvt");
            evt.fire();

        }
        component.set("v.selectedCheckboxList",selectedCheckboxList);
    },

    deselectCheckbox : function(component,event,helper)
    {
        var $j = jQuery.noConflict();
        $j('input[type="radio"]:checked').attr('checked',false); 
        $j("#rightTab").addClass('slds-hide');
        $j("#leftTab").removeClass('slds-size--9-of-12');
        $j("#leftTab").addClass('slds-size--12-of-12');
        if(document.getElementById('leftTab') != null && document.getElementById('rightTab') != null)
        {
            if(document.getElementsByClassName('legendPanel')[0] && document.getElementsByClassName('legendPanel')[0].style.display == 'none')
            {
              document.getElementsByClassName('legendPanel')[0].style.display = 'inline-block';
            }
            else
            {
              if(document.getElementsByClassName('legendPanel').length > 1)
                document.getElementsByClassName('legendPanel')[1].style.display = 'inline-block';
            }
        }    
        
        if(component.get("v.showPositionHierarchy") == 'true')
        {
            if(component.get("v.loadBreadCrumb") == 'true'){
              var positionhierarchy = component.find('positionhierarchy');
              positionhierarchy.closeBreadcrumb();
            }
        }
        var evt = component.getEvent("deselectCheckboxEvt");
        evt.fire();
        component.set("v.selectedCheckboxList",[]);
    },

    breadcrumbChange : function(component,event,helper)
    {
        component.set("v.selectedTeamInstance",event.getParam("teamInstanceId")); 
        component.set("v.selectedTeam", event.getParam("teamId"))
        component.set("v.countryId",event.getParam("countryId")); 
        component.set("v.selectedPosition","");

        var selectedTeamInstance = event.getParam("teamInstanceId"); 

        
        var allpositiontext = component.get("v.allpositiontext");
        
        console.log('allpositiontext ::'+allpositiontext);
        if(selectedTeamInstance.indexOf(allpositiontext) != -1){
            //component.set("v.loadTreeHierarchy", 'false');
           // document.getElementById('treediv').style.display = 'none';
           component.set("v.loadTreeHierarchy", 'true')
            component.set("v.showMenuList", true);
        }
        else{
            component.set("v.globalActionState", 'Hide');
            if(component.get("v.showTreeHierarchy") == 'true') 
            document.getElementById('treediv').style.display = 'block';
            //component.set("v.loadTreeHierarchy", 'true');
            component.set("v.showMenuList", false);
        }
        component.deselectCheckbox();
        helper.getRecords(component,event,helper);
        component.getEvent("resetFilterConditions").fire();
        component.refreshData();
        
        
    },

    treeSelect : function(component,event,helper) 
    {
        component.deselectCheckbox();
        
        var selectedPosition = event.getParam("selectedTreeNode"); 
        component.set("v.selectedPosition",selectedPosition);
        helper.getRecords(component,event,helper);
    },

    navigateRecord : function(component,event,helper)
    {
        document.body.setAttribute('style', 'overflow: auto;');
        // component.set("v.modalStyle", ".forceStyle.desktop .viewport {overflow:auto !important}");
        var $j = jQuery.noConflict();
        if(($j('.slds-context-bar__item a[title="Employee Universe"]').parent().hasClass('slds-is-active')))
            $j('.slds-context-bar__item a[title="Employee Universe"]').parent().removeClass('slds-is-active');
    },

    destoryCmp : function (component, event, helper) {
        component.destroy();
    },

    createPositionAction : function(component,event,helper)
    {
        var a = component.get('c.hidePopup');
        $A.enqueueAction(a);
        var posActionEvent = component.getEvent("setPositionAction");
        var createPopupHeader ;
        if(component.get("v.namespace") != '') {
            createPopupHeader = $A.get("$Label.AxtriaSalesIQTM.CreatePositionPopupHeader");
        }
        else
        {
            createPopupHeader = $A.get("$Label.c.CreatePositionPopupHeader") ;
        }

        posActionEvent.setParams({
            "actionType": 'Create Position',
            "modalTitle": createPopupHeader,
            "selectedPosition":component.get("v.selectedPosData").referenceId,
            "teamInstance": component.get("v.selectedPosData").positionTeamInstanceId,
            "teamType" : component.get("v.dataTableWrapper.teamType")
        });               

        posActionEvent.fire();
    },


    filterPositionAction : function(component,event,helper)
    {
        var a = component.get('c.hidePopup');
        $A.enqueueAction(a);
        var posActionEvent = component.getEvent("setPositionAction");
        var filterPopupHeader ;
        if(component.get("v.namespace") != '') {
            filterPopupHeader = $A.get("$Label.AxtriaSalesIQTM.FilterPositionPopupHeader");
        }
        else{
            filterPopupHeader = $A.get("$Label.c.FilterPositionPopupHeader");
        }

        console.log('query in position action - ',component.get("v.queryString"));

        posActionEvent.setParams({
            "actionType": 'Filter Position',
            "modalTitle": filterPopupHeader,
            "selectedPosition":component.get("v.selectedPosData").referenceId,
            "teamInstance": component.get("v.selectedPosData").positionTeamInstanceId,
            "teamType" : component.get("v.dataTableWrapper.teamType"),
            "queryString" : component.get("v.queryString")
        });               

        posActionEvent.fire();

        /*
        
        var columnName;
        var columnWrapperAction = component.get("c.createColumnWrapperData");
        
        columnWrapperAction.setCallback(this,function(response)
        {
            var state = response.getState();
            console.log('1---- ',response.getReturnValue);
                if(state === 'SUCCESS' && response.getReturnValue != null)
                {
                    columnName = response.getReturnValue();
                }
        });
        $A.enqueueAction(columnWrapperAction);


        var operators = [];
        operators.push('equals');
        operators.push('not equals');
        operators.push('contains');
        operators.push('not contains');
        operators.push('in');
        operators.push('less than');
        component.set("v.listOfOperators",operators);
        console.log('operators --- ',operators);


        component.set("v.showFilterPositionPopup", true);
        console.log('columnName ---  ',columnName);
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:BRQueryBuilder",
            componentAttributes: {
                columnNames : columnName,
                listOfOperators : operators
            },
            isredirect : true
        });

        evt.fire();
        */
        console.log('--- BRQueryBuilder called ----');
        
    },

    updatePositionAction : function(component, event, helper){
        console.log('updatePositionAction called');
        if(!event.target) return;
        if(event.target.parentNode === undefined) return;

        var idx = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        console.log('idx : '+idx);

        if(!idx) return; //there is NO data associated
        var row = parseInt(idx);
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        for(var col = 0 ; col < columnsArray.length ; col++){
            console.log(columnsArray[col].data);
            if (columnsArray[col].data == 'Name'){
                break;
            }
        }

        console.log(row + ', '+ col);
        var editPopupHeader ;
        if(component.get("v.namespace") != '') {
            editPopupHeader = $A.get("$Label.AxtriaSalesIQTM.EditPositionPopupHeader");
        }
        else
        {
             editPopupHeader = $A.get("$Label.c.EditPositionPopupHeader") ;
        }
        component.invokePositionAction('Update Position Attributes',editPopupHeader,row,col);
    },

    deletePositionAction : function(component, event, helper){
        console.log('deletePositionAction called');
        
        if(!event.target) return;
        if(event.target.parentNode === undefined) return;

        var idx = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        console.log('idx : '+idx);

        if(!idx) return; //there is NO data associated
        var row = parseInt(idx);
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        for(var col = 0 ; col < columnsArray.length ; col++){
            console.log(columnsArray[col].data);
            if (columnsArray[col].data == 'Name'){
                break;
            }
        }

        console.log(row + ', '+ col);
        var deletePopupHeader ;
        if(component.get("v.namespace") != '') {
            deletePopupHeader = $A.get("$Label.AxtriaSalesIQTM.DeletePositionPopupHeader");
        }
        else
        {
            deletePopupHeader = $A.get("$Label.c.DeletePositionPopupHeader") ;
        }
        component.invokePositionAction('Delete Position',deletePopupHeader,row,col);
    },

    editOverlayAction : function(component, event, helper){
        console.log('editOverlayAction called');
        if(!event.target) return;
        if(event.target.parentNode === undefined) return;

        var idx = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        console.log('idx : '+idx);

        if(!idx) return; //there is NO data associated
        var row = parseInt(idx);
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        for(var col = 0 ; col < columnsArray.length ; col++){
            console.log(columnsArray[col].data);
            if (columnsArray[col].data == 'Name'){
                break;
            }
        }

        console.log(row + ', '+ col);
        component.invokePositionAction('Edit Overlay Association',event.target.innerText,row,col);
    },

    changeHierarchyPositionAction : function(component, event, helper){
        console.log('changeHierarchyPositionAction called');
        if(!event.target) return;
        if(event.target.parentNode === undefined) return;

        var idx = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        console.log('idx : '+idx);

        if(!idx) return; //there is NO data associated
        var row = parseInt(idx);
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        for(var col = 0 ; col < columnsArray.length ; col++){
            console.log(columnsArray[col].data);
            if (columnsArray[col].data == 'Name'){
                break;
            }
        }

        console.log(row + ', '+ col);
        var parentChangePopupHeader ;
        if(component.get("v.namespace") != '') {
            parentChangePopupHeader = $A.get("$Label.AxtriaSalesIQTM.Hierarchy_Change_PopUp_Header");
        }
        else
        {
            parentChangePopupHeader = $A.get("$Label.c.Hierarchy_Change_PopUp_Header");
        }
        component.invokePositionAction('Edit Position Hierarchy',parentChangePopupHeader,row,col);
    },

    menuPositionAction : function (component, event, helper) {
        console.log('menuPositionAction called');
        var params = event.getParam('arguments');
        var positionAction = params.selectedAction;
        var modalTitle = params.titleText;
        var ix = params.rowIndex;
        var iy = params.colIndex;

        var dataMap = component.get("v.dataTableWrapper.dataMap");
        console.log('dataMap',dataMap);
        var positionTeamInstanceId = dataMap[ix][iy].positionTeamInstanceId;
        console.log('positionTeamInstanceId',positionTeamInstanceId);
        console.log('ix',ix);
        console.log('iy',iy);
        var recordId = dataMap[ix][iy].referenceId;
        console.log('recordId : '+recordId);
        console.log('modalTitle : '+modalTitle);
        console.log('positionAction : '+positionAction);
        console.log('selectedTeamInstance -'+component.get("v.dataTableWrapper.teamInstance"));

        // before calling popupevent check for pending CR in edit/delete/update/ChangeHierarchy 

        var action = component.get("c.validatePositionActions");
        action.setParams({
            action : positionAction,
            selectedPosition : recordId,
            selectedTeamInstance : positionTeamInstanceId
        });

        console.log('teamtype --'+component.get("v.dataTableWrapper.teamType"));
        action.setCallback(this,function(response)
        {
            console.log('Validations checks ****** ');
            var state = response.getState();
            console.log(response.getReturnValue());
            if(state == 'SUCCESS' && response.getReturnValue() != null){
                var result = response.getReturnValue();
                var arr = result.split(';');
                console.log(arr);
                if(result != '' && result != null && positionAction != 'Create Position'){
                    // show toast message
                    console.log('inside toast message');
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'sticky',
                        message: arr,
                        messageTemplate : arr[0],
                        messageTemplateData : [
                            arr[1],
                            {
                                url : arr[2],
                                label : arr[1]
                            }
                        ],
                        type : 'error'
                    });
                    toastEvent.fire();

                }else{
                    console.log('#### posActionEvent');
                    var posActionEvent = component.getEvent("setPositionAction");
                    posActionEvent.setParams({"actionType": positionAction ,
                                              "modalTitle": modalTitle ,
                                              "selectedPosition":recordId,
                                              "teamInstance": positionTeamInstanceId,
                                              "teamType" : component.get("v.dataTableWrapper.teamType")});
                    posActionEvent.fire();
                    console.log('#### posActionEvent fired');
                }
            }else{
                var posActionEvent = component.getEvent("setPositionAction");
                 posActionEvent.setParams({"actionType": positionAction ,
                                          "modalTitle": modalTitle ,
                                          "selectedPosition":recordId,
                                          "teamInstance": positionTeamInstanceId,
                                          "teamType" : component.get("v.dataTableWrapper.teamType")});
                posActionEvent.fire();
            }
        });

         $A.enqueueAction(action);

        console.log('position calling action event ');
    },

    closeDropDown : function(component,event,helper)
    {
        if(component.get("v.showPositionHierarchy") == 'true') 
        { 
            var positionhierarchy = component.find('positionhierarchy');
            positionhierarchy.closeTeamDropDown();
        }  
        if(component.get("v.showTreeHierarchy") == 'true') 
        {
            var treeHierarchy = component.find('treeHierarchy');
            treeHierarchy.closeTree();
        }
    },

    workspaceEdit : function(component,event,helper)
    {
        if(!event.target) return;
        if(event.target.parentNode === undefined) return;

        var idx = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        if(!idx) return; //there is NO data associated
        var ix = parseInt(idx);
        var dataMap = component.get("v.dataTableWrapper.dataMap");
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        for(var i=0;i<columnsArray.length;i++)
        {
            if (columnsArray[i].data == 'Name')
            {
                break;
            }
        }
        var recordId = dataMap[ix][i].referenceId;
        console.log('--->' + recordId);
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams
        ({
             "recordId": recordId
        });
        editRecordEvent.fire();
    },
    
    switchToCountry : function(component,event,helper)
    {
      console.log('##### Inside switchToCountry ');
        if(!event.target) return;
        if(event.target.parentNode === undefined) return;

        var idx = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        if(!idx) return; //there is NO data associated
        var ix = parseInt(idx);
        
        console.log('--->>> classList');
        console.log(document.getElementsByClassName('menu_'+idx)[0].classList);
        if(document.getElementsByClassName('menu_'+idx)[0].classList.contains('visible'))
        {
            document.getElementsByClassName('menu_'+idx)[0].classList.remove('visible');
        }

        var dataMap = component.get("v.dataTableWrapper.dataMap");
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        for(var i=0;i<columnsArray.length;i++)
        {
            if (columnsArray[i].data == 'Name')
            {
                break;
            }
        }
        var recordId = dataMap[ix][i].referenceId;
        console.log('###### recordId ' + recordId);
        
        //Setting the cookie to set the countryID as soon as user click on Switch to Country.
        cookieStr = 'apex__CountryID' + "=" + escape(recordId) + "; " + ";domain=.force.com;path=/";
        document.cookie = cookieStr;
        
        var action = component.get("c.setCountryForUser");
        
        action.setParams
            ({
                countryID : recordId,                
            });
            
            
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            console.log('message : ',response);
            var responseMessage = response.getReturnValue();
            if(state == 'SUCCESS' && response.getReturnValue() != null)
            {
                component.set("v.countryId",recordId);
                component.refreshData();
                var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                mode: 'dismissible',
                                message: responseMessage,
                                type : 'success'
                                
                            });
                            toastEvent.fire();
            }
        });
        $A.enqueueAction(action);

    },

    deleteConfirmationPopUp : function(component,event,helper)
    {
        if(!event.target) return;
        if(event.target.parentNode === undefined) return;

        var idx = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        if(!idx) return; //there is NO data associated
        var ix = parseInt(idx);
        var dataMap = component.get("v.dataTableWrapper.dataMap");
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        var ns =  ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0] + '__';

        for(var i=0;i<columnsArray.length;i++)
        {
            if (columnsArray[i].data == 'Name')
            {
                break;
            }
            else if (columnsArray[i].data == ns+'Scenario_Name_Formula__c')
            {
                break;
            }
        }
        var recordId = dataMap[ix][i].referenceId;
        //component.set("v.recordId",recordId);
        var targetCmp = component.find('comfirmationPopUp');
        $A.util.removeClass(targetCmp, 'slds-hide');
        console.log('tt is',targetCmp);
        var body = targetCmp.get("v.body");

        if(component.get("v.moduleName") == 'Workspace Universe')
        {
            component.set("v.recordId",recordId);
            var modalMessage,modalLabel;
            
            if(component.get("v.namespace") != '') {
                modalMessage = $A.get("$Label.AxtriaSalesIQTM.Delete_Workspace_Popup_Message");
                modalLabel = $A.get("$Label.AxtriaSalesIQTM.HEADER_DELETE_WORKSPACE");
            }
            else
            {
                modalMessage = $A.get("$Label.c.Delete_Workspace_Popup_Message");
                modalLabel = $A.get("$Label.c.HEADER_DELETE_WORKSPACE");
            }
            console.log('here::' + modalMessage);
            $A.createComponent
            (
                "c:SalesIQMessageDialog",
                {
                    "body": modalMessage,
                    "recordId" : recordId,
                    "onconfirm" : component.getReference("c.recordDelete"),
                    "showHeader" : "true",
                    "headerLabel" : modalLabel
                },
                function(msgBox)
                {                
                    if (component.isValid()) {
                        var targetCmp = component.find('comfirmationPopUp');
                        $A.util.removeClass(targetCmp, 'slds-hide');
                        console.log('in msg',targetCmp);
                        
                        var body = targetCmp.get("v.body");
                        body.push(msgBox);
                        targetCmp.set("v.body", body);
                    }
                }
            );
        }
        else{
            console.log('--- delete scenario ---- ');
            var scenarioId = recordId.split('/')[1].split(' ')[0].replace('"','');
            console.log('scenarioId - '+scenarioId);
            component.set("v.recordId",scenarioId);
            var deleteScenarioModalMessage;
            
            if(component.get("v.namespace") != '') {
                deleteScenarioModalMessage = $A.get("$Label.AxtriaSalesIQTM.Delete_Scenario_Confirmation_Message");
                
            }
            else
            {
                deleteScenarioModalMessage = $A.get("$Label.c.Delete_Scenario_Confirmation_Message");
                
            }
            $A.createComponent
            (
                "c:SalesIQMessageDialog",
                {
                    "body": deleteScenarioModalMessage,
                    "recordId" : scenarioId,
                    "onconfirm" : component.getReference("c.menuDeleteScenario")
                    //"showHeader" : "true",
                    //"headerLabel" : 'Delete'
                },
                function(msgBox)
                {                
                    if (component.isValid()) {
                        var targetCmp = component.find('comfirmationPopUp');
                        var body = targetCmp.get("v.body");
                        body.push(msgBox);
                        targetCmp.set("v.body", body);
                    }
                }
            );
        }
    },

    editAccountExclusion : function(component, event, helper)
    {
        if(!event.target) return;
        if(event.target.parentNode === undefined) return;
        console.log('Delete_Popup')
        var idx = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        if(!idx) return; //there is NO data associated
        var ix = parseInt(idx);
        var dataMap = component.get("v.dataTableWrapper.dataMap");
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        for(var i=0;i<columnsArray.length;i++)
        {
            if (columnsArray[i].data == 'Name')
            {
                break;
            }
        }

        var evt = component.getEvent("editAccountExclusion");
        evt.setParams({
            "recordId" : dataMap[ix][i].referenceId,
            "operationType" : "Edit"
        });
        evt.fire();
    },

    deleteConfirmationPopUp_AccountExclusion : function(component,event,helper)
    {
        if(!event.target) return;
        if(event.target.parentNode === undefined) return;
        console.log('Delete_Popup')
        var idx = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        if(!idx) return; //there is NO data associated
        var ix = parseInt(idx);
        var dataMap = component.get("v.dataTableWrapper.dataMap");
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        for(var i=0;i<columnsArray.length;i++)
        {
            if (columnsArray[i].data == 'Name')
            {
                break;
            }
        }
        var recordId = dataMap[ix][i].referenceId;
        component.set("v.recordId",recordId);
        var targetCmp = component.find('comfirmationPopUp');
        $A.util.removeClass(targetCmp, 'slds-hide');
        var body = targetCmp.get("v.body");

       // if(component.get("v.moduleName") == 'Workspace Universe')
        //{
            var modalMessage,modalLabel;
            
            modalMessage = 'Are you sure you want to delete?';
            modalLabel = 'Are you sure you want to delete?';
            /*if(component.get("v.namespace") != '') {
                modalMessage = $A.get("$Label.AxtriaSalesIQTM.Delete_Generic_Message");
                modalLabel = $A.get("$Label.AxtriaSalesIQTM.Delete_Generic_Message");
            }
            else
            {
                modalMessage = $A.get("$Label.c.Delete_Generic_Message");
                modalLabel = $A.get("$Label.c.Delete_Generic_Message");
            }*/
            console.log('here::' + modalMessage);
            $A.createComponent
            (
                "c:SalesIQMessageDialog",
                {
                    "body": modalMessage,
                    "recordId" : recordId,
                    "onconfirm" : component.getReference("c.recordDelete"),
                    "showHeader" : "true",
                    "headerLabel" : modalLabel
                },
                function(msgBox)
                {                
                    if (component.isValid()) {
                        var targetCmp = component.find('comfirmationPopUp');
                         $A.util.removeClass(targetCmp, 'slds-hide');
                        var body = targetCmp.get("v.body");
                        body.push(msgBox);
                        targetCmp.set("v.body", body);
                    }
                }
            );
       // }
    },

    recordDelete : function(component,event,helper)
    {
        var recordId = component.get("v.recordId");
        var action = component.get("c.deleteRecord");
        action.setParams({
            recordId : recordId,
            objectName : component.get("v.objectName")
        });
        action.setCallback(this,function(response)
        {
            var state = response.getState();

            if(state === 'SUCCESS' && response.getReturnValue() != null)
            {
                var retResponse = response.getReturnValue();
                console.log('delete--->' + retResponse);
                if(retResponse == 'Success')
                {
                    
                    helper.getRecords(component,event);
                    var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                mode: 'dismissible',
                                message: 'Workspace has been deleted',
                                type : 'succeess'
                                
                            });
                            toastEvent.fire();
                    var targetCmp = component.find('comfirmationPopUp');
                    //console.log('comp',targetCmp);
                    $A.util.addClass(targetCmp, 'slds-hide');
                    //component.dropPopup();
                }
                else 
                {
                    var action = component.get("c.logException");
                    
                    action.setParams({
                        message : retResponse,
                        module : component.get("v.moduleName")
                    });

                    action.setCallback(this,function(response)
                    {
                        var state = response.getState();
                        if(state === 'SUCCESS')
                        {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                mode: 'dismissible',
                                message: retResponse,
                                type : 'error'
                                
                            });
                            toastEvent.fire();
                        }
                    });
                   var dismissActionPanel = $A.get("e.force:closeQuickAction");
                   dismissActionPanel.fire();
                    $A.enqueueAction(action);
                }   
            }
            else if (state === "ERROR" || response.getReturnValue() == null) 
            {
                helper.logException(component, response.getError());
            }    
        });

        action.setBackground();
        $A.enqueueAction(action);
    },

    getDisabledCheckbox : function(component,event)
    {
        if(component.get("v.isMasterTeamInstance") == 'false' && component.get("v.objectName") == 'Position__c')
            return;

        var disableCheckboxConditions = component.get("v.disableCheckboxConditions");
        var queryString = component.get("v.dataTableWrapper.queryString");
        
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        var objectName = component.get("objectName");
        var action = component.get("c.getDisabledCheckboxList");
        action.setParams({
            disableCheckboxConditions : component.get("v.disableCheckboxConditions"),
            objectName : component.get("v.objectName")
        });
        action.setCallback(this,function(response)
        {
            var state = response.getState();

            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var retResponse = response.getReturnValue();
                for(var i=0;i<columnsArray.length;i++)
                {
                    if (columnsArray[i].data == 'Name')
                    {
                        break;
                    }

                }
                var f = 0;
                var globalId = component.getGlobalId();
                var selectedCheckboxList = component.get("v.selectedCheckboxList");
                var dataMap = component.get("v.dataTableWrapper.dataMap");
                for(var idx=0;idx<dataMap.length;idx++)
                {
                    if(retResponse.indexOf(dataMap[idx][i].referenceId) == -1)
                    {
                        document.getElementById(globalId+'_add-checkbox-' + idx).disabled = false;
                    }
                    if(selectedCheckboxList.length > 0)
                    {
                        if(dataMap[idx][i].referenceId === selectedCheckboxList[0])
                        {
                            document.getElementById(globalId+'_add-checkbox-' + idx).checked = true;
                            component.set("v.selectedPosData", dataMap[idx][i]);

                        }else{
                            //console.log('--- 11111----');
                            component.set("v.selectedPosData", dataMap[idx][i]);

                        }
                        f = 1;
                    }
                    if(retResponse.indexOf(dataMap[idx][i].referenceId) == -1 && f==0 && component.get('v.moduleName') == "Position Universe" && component.get("v.objectName") == 'Position__c')
                    {
                        if(dataMap[idx] != undefined){
                            var rowLength = dataMap[idx].length;
                            var menuActionsColumn = dataMap[idx][rowLength-1];
                            var selectedCheckboxList = [];
                            selectedCheckboxList.push(dataMap[idx][i].referenceId); 
                            var evt = component.getEvent("selectCheckbox");
                            evt.setParams({
                                "recordId" : dataMap[idx][i].referenceId,
                                "actionState" : menuActionsColumn.isCreate,
                                "teamInstanceId" : component.get("v.dataTableWrapper.teamInstance"),
                                "teamType" : component.get("v.dataTableWrapper.teamType")
                            });
                            evt.fire();
                            component.set("v.globalActionState", menuActionsColumn.isCreate);
                            if(component.get("v.showPositionHierarchy") == 'true')
                            {
                                var positionhierarchy = component.find('positionhierarchy');
                                positionhierarchy.openBreadcrumb();
                                positionhierarchy.checkboxSelect(dataMap[idx][i].referenceId);
                            }
                            document.getElementById(globalId+'_add-checkbox-' + idx).checked = true;

                            component.set("v.selectedCheckboxList",selectedCheckboxList);
                            f = 1;
                        }
                    }
                }
            }

            else if (state === "ERROR" || response.getReturnValue() == null) 
            {
                helper.logException(component, response.getError());
            }    
        });


        action.setBackground();
        $A.enqueueAction(action);
    },
    
    escapeHTML : function(component, event){

        var objectName = component.get("v.objectName");
        console.log('1111111111111');
        console.log(objectName);
        if(objectName == 'Scenario__c'){
            var textContent = event.target.text;
            var nodes = event.target.parentNode;
            if(nodes.nodeName == 'DIV'){
                var attrList =  nodes.attributes;
                for(var i =0; i<attrList.length; i++){
                    if(attrList[i].name == 'title'){
                           nodes.attributes[i].nodeValue = textContent;
                    }
                }
            }
        }

        //This code is added for Country tab's Flag tool tip.
        //Here we will fetch value from image Formula in which alt value will be shown.
        if(objectName == 'Country__c'){
           var nodes = event.target.parentNode;
           var nodes1 = event.target;
           var text = '';
           if(nodes1.nodeName == 'IMG'){
             console.log('IMG');
             var attrs = nodes1.attributes;
             console.log(attrs);
             for(var i=0 ; i<attrs.length; i++){
                if(attrs[i].name == 'alt'){
                   text = nodes1.attributes[i].nodeValue ;
                }
             }
           }
            if(nodes.nodeName == 'DIV'){
                var attrList =  nodes.attributes;
                for(var i =0; i<attrList.length; i++){
                    if(attrList[i].name == 'title'){
                        nodes.attributes[i].nodeValue = text;
                    }
                }
            }
        }
    },

    updateCountryAttributes : function(component, event, helper)
    {
        var params = event.getParam('arguments');
        var countryId = '';
        if(params)
            countryId = params.countryId;

        component.set("v.countryId",countryId);
        console.log('updateCountryAttributes datatable ---> ' + component.get("v.countryId"));

        var positionhierarchy = component.find('positionhierarchy');
        if(positionhierarchy)
        {
            positionhierarchy.initialize(component.get("v.countryId"));
        }
        var treeHierarchy = component.find('treeHierarchy');
        if(treeHierarchy)
        {
            treeHierarchy.refreshTree(component.get("v.countryId"));
        }
    },
    openCustomPage : function(component, event ,helper){
         /*var urlEvent = $A.get("e.force:navigateToURL");
         urlEvent.setParams({
                "url": "/apex/"+event.target.name+'?selectedRecord='+component.get("v.selectedCheckboxList")+'&selectedTeamInstance='+component.get("v.selectedTeamInstance")+'&moduleName='+component.get("v.moduleName")
         });
          urlEvent.fire();*/

        window.open('/apex/'+event.target.name+'?selectedRecord='+component.get("v.selectedCheckboxList")+'&selectedTeamInstance='+component.get("v.selectedTeamInstance")+'&moduleName='+component.get("v.moduleName"),"_blank");
    
    },
    openCustomModal : function(component,event,helper){
        console.log('flag reset');
        component.set("v.openCustomModalPopupFlag",true);
        var action = component.get("c.callExternalClassMethod");
        action.setParams({
                selectedRecord : component.get("v.countryId"),  
                selectedTeamInstance : component.get("v.selectedTeamInstance"),
                moduleName  : component.get("v.moduleName")          
        });
        action.setCallback(this,function(response){
            var state =  response.getState();
            var result = response.getReturnValue();             
            if(state === 'SUCCESS' && result != null){
               console.log(result);
               if(result!=null){
                    for(var header in result){
                        document.getElementById('customHeader').innerHTML = header;
                        document.getElementById('customPopupContent').innerHTML=result[header];
                    }
                    
               }
            }else if (state === "ERROR" || result == null) { 
                component.hideSpin(); 
                helper.logException(component, response.getError());
            }
        });
        $A.enqueueAction(action);
        //$A.util.addClass(component.find('customModal'), "slds-show");
    },
    closeCustomModal : function(component,event,helper){
        component.set("v.openCustomModalPopupFlag",false);
    },

    resetFilterCondition : function(component,event,helper){
        var a = component.get('c.hidePopup');
        $A.enqueueAction(a);
        component.set("v.filterQueryWhereClause", '');

        //console.log('filterQueryWhereClause:  '+component.get("v.filterQueryWhereClause"));

        component.refreshData();

        /*var refreshDataTable = component.getEvent("refreshComponentEvent");
        refreshDataTable.setParams({
            "filterQueryWhereClause" : component.get("v.parsedJSONQuery")
        });
        
        refreshDataTable.fire();
*/
        component.set("v.isActive", false);
		component.getEvent("resetFilterConditions").fire();
        
        

    },
     sendEmailNotification : function(component, event, helper){
         debugger;
         
         if(!event.target) return;
        if(event.target.parentNode === undefined) return;

        var idx = event.target.getAttribute("data-data") || event.target.parentNode.getAttribute("data-data");
        if(!idx) return; //there is NO data associated
        var ix = parseInt(idx);
        var dataMap = component.get("v.dataTableWrapper.dataMap");
        var columnsArray = component.get("v.dataTableWrapper.columnsArray");
        for(var i=0;i<columnsArray.length;i++)
        {
            if (columnsArray[i].data == 'Name')
            {
                break;
            }

        }

        var recordId = dataMap[ix][i].referenceId;
        var actionSendEmail = component.get("c.sendEmail");
        actionSendEmail.setParams({
             employeeID : recordId
        });
        
        actionSendEmail.setCallback(this,function(response)
        {                     
             var state = response.getState();
             component.set("v.sendEmail",response.getReturnValue());
            if(response.getState() == 'SUCCESS' && response.getReturnValue() != 'error' && response.getReturnValue() != undefined){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                                mode: 'dismissible',
                                message: 'Mail sent successfully',
                                type : 'success'                            
                    });
                    toastEvent.fire();
            }
            
        });
      
      $A.enqueueAction(actionSendEmail); 
    },

    reloadLightningGrid : function(component, event, handler){

        console.log('-- calling reloadLightningGrid ----');
        console.log(event.getParam("actionType"));
        var actionTypeValue = event.getParam("actionType");

        console.log(component.find("lightningtable1"));

        

        component.set("v.loadTable", 'true');

        if(actionTypeValue == 'Dynamic Style'){
            console.log(component.find('lightningtable1'));
            
            var lightningGridTable = component.find('lightningtable1');
            console.log(lightningGridTable);
            
            
            
            var interfaceStyle = component.get("v.legendStyleList");
            console.log('interfaceStyle :',interfaceStyle);
            var ns = component.get("v.namespace");
            if(lightningGridTable != undefined){
                var dataSet = lightningGridTable.get("v.data");
                console.log('dataSet :',dataSet);
                console.log('page :'+lightningGridTable.get("v.pageNumber"));
                var elements = lightningGridTable.elements;
                if(elements != undefined && elements != null){
                    var childNodes = elements[0].children;
                    for (var i = 0; i < childNodes.length; i++) {
                        if(childNodes[i].className == 'slds-card__body'){
                            var tableBody = childNodes[i];
                            var rowData = tableBody.children[0].children[0].getElementsByTagName('tr');
                            console.log(rowData.length);
                            for (var j = 0; j < rowData.length; j++) {
                                if(rowData[j].className == 'slds-hint-parent'){
                                    console.log(rowData[j]);
                                    for (var style = 0; style < interfaceStyle.length; style++) {
                                        if(interfaceStyle[style].name == dataSet[j-1][ns+'Field_Status__c']){
                                            console.log('--- 1 ----',rowData[j]);
                                            rowData[j].style = interfaceStyle[style].style;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    for (var i = 0; i < childNodes.length; i++) {
                        if(childNodes[i].className == 'slds-card__body'){
                            var tableBody = childNodes[i];
                            var rowData = tableBody.children[0].children[0].getElementsByTagName('tr');
                            console.log(rowData.length);
                            for (var j = 0; j < rowData.length; j++) {
                                if(rowData[j].className == 'slds-hint-parent'){
                                    console.log('rowData[j] :',rowData[j]);
                                    var hyperlinkElements = rowData[j];
                                    console.log('hyperlinkElements :',hyperlinkElements);
                                    for(var k=0; k < rowData[j].children.length; k++){
                                        if(rowData[j].children[k].getElementsByTagName('a')[0] != undefined){
                                            console.log('-- inside hyperlink ---');
                                            rowData[j].children[k].getElementsByTagName('a')[0].removeAttribute('title');

                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                }
                
            }
        }
        

    }
})
