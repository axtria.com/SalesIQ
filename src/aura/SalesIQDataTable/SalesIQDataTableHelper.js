({
    getRecords : function(component,event,helper) 
    {
        console.log('######## getRecords SalesIQ Data Table Helper - ' + component.get("v.selectedTeamInstance"));
        component.showSpin();
        console.log('-- module Name :'+component.get("v.moduleName"));
        if(component.get("v.referencedObject") == 'Position Employee')
            component.set("v.showMenuList", false);
        
        console.log('-- showMenuList :'+component.get("v.showMenuList"));
        
        console.log('query - '+component.get("v.queryString"));
        var disclaimer = component.get("c.getCustomSettingforDisclaimer");
        disclaimer.setCallback(this,function(response){
            if(response.getState() === 'SUCCESS' && response.getReturnValue() != null && response.getReturnValue() != '')
            {
                component.set("v.disclaimer",response.getReturnValue());
            }
        });
    
        $A.enqueueAction(disclaimer);

        //get org profile map 
        var profileAction = component.get("c.getProfileNameMap");
        profileAction.setCallback(this,function(response){
            console.log(response.getReturnValue());
            var profileMap = response.getReturnValue();
            
            console.log('------------- in else loop');
            //$Label.c.System_Administrator
            //$Label.c.Rep
            //$Label.c.RM
            //$Label.c.HR
            //$Label.c.HO
            //$Label.c.Standard_User
            //var convertedProfileMap = {};
            Object.keys(profileMap).forEach(function(key) {
                console.log(key, profileMap[key]);
                var profileKey = profileMap[key];
                for (var i = 0; i < profileMap[key].length; i++) {
                    var temp = profileMap[key][i].split(' ').join('_');
                    var profileName = "$Label.c." + temp;
                    console.log('profileName : '+profileName);
                    var profileReference = $A.getReference(profileName);
                    component.set("v.tempProfileName", profileReference);
                    profileKey[i] = component.get("v.tempProfileName");
                }
            });

            console.log('---- profile map ----');
            console.log(profileMap);
            console.log(component.get("v.filterQueryWhereClause"));
            console.log(component.get("v.invokeFromListView"));

            if(component.get("v.invokeFromListView") != undefined){
                console.log('1111111111111111111111');
                if(component.get("v.invokeFromListView") == true){
                    console.log('22222222222222222');
                    component.set("v.filterQueryWhereClause",'');
                    component.set("v.invokeFromListView",false);
                }
            }

            component.set("v.profileMap", profileMap);

            console.log( 'profileMap ' , JSON.stringify(component.get("v.profileMap")));
            
            var listViewId = component.get("v.listViewId");
            var action = component.get("c.getData");
            action.setParams({
                listViewId : listViewId,
                objectName : component.get("v.objectName"),
                recordId : component.get("v.recordId"),
                selectedPosition : component.get("v.selectedPosition"),
                selectedTeamInstance : component.get("v.selectedTeamInstance"),
                selectedTeam : component.get("v.selectedTeam"),
                moduleName : component.get("v.moduleName"),
                countryId : component.get("v.countryId"),
                profileString : JSON.stringify(component.get("v.profileMap")),
                filterQueryWhereClause : component.get("v.filterQueryWhereClause")

            });

            action.setCallback(this,function(response)
            {
                var state = response.getState();
                //console.log('state :'+state);
                var responseValue = response.getReturnValue(); 
                //console.log('responseValue' , responseValue);
                //console.log( responseValue.errorText +' : error');
                if(state === 'SUCCESS' && response.getReturnValue != null )
                {
                    if(responseValue.errorText != undefined && responseValue.errorText.indexOf('Query format') != -1){

                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            mode: 'dismissible',
                            message: responseValue.errorText,
                            type : 'error'
                            
                        });
                        toastEvent.fire();
                        component.hideSpin();
                        
                    }
                    else{
                            var table_body = component.find('table1');
                            var card = component.find('no_record_card');

                            var retResponse = response.getReturnValue();
                            var retRecords = retResponse.dataMap;
                            var fields = retResponse.columnsList ;
                            var labels = retResponse.columnLabels;
                            // var referenceWrapperList = retResponse.referenceWrapperList;
                            var typeList = retResponse.dataTypesList;

                            console.log('typeList :',typeList);
                            var interfaceStyleConfList = retResponse.listInterfaceStyConf;

                            if(interfaceStyleConfList == undefined || interfaceStyleConfList == null || interfaceStyleConfList.length == 0){
                                if(document.getElementById('legendPanel') != null){
                                    document.getElementById('legendPanel').style.display = 'none';
                                }
                            }
                            else{
                                var listStyleObjects = [];
                                var namespace = component.get("v.namespace");
                                for(var index = 0; index < interfaceStyleConfList.length; index++)
                                    listStyleObjects.push({'name' : interfaceStyleConfList[index].Name , 'style' : interfaceStyleConfList[index][namespace + 'Style__c'] });

                                component.set("v.legendStyleList", listStyleObjects);
                            }

                            if(retRecords.length == 0)
                            {
                                $A.util.removeClass(card, "slds-hide");
                                $A.util.addClass(table_body, "slds-hide");  
                            }
                            else
                            {
                                $A.util.removeClass(table_body, "slds-hide");
                                $A.util.addClass(card, "slds-hide");
                            }

                            component.set("v.dataTableWrapper",retResponse);
                            component.set("v.dataTableWrapper.queryString" , retResponse.queryString);
                            component.set("v.dataTableWrapper.pageNumber" , 1);
                            component.set("v.dataTableWrapper.pageSize" , retResponse.pageSize);
                            // component.set("v.dataTableWrapper.dataArray", retRecords);
                            component.set("v.dataTableWrapper.objectLabel" , retResponse.objectLabel);
                            
                            

                            var columnsArray = [];

                            for(var i =0;i<labels.length;i++)
                            {
                                if(typeList[fields[i]] == 'DATETIME' || typeList[fields[i]] == 'DATE' || typeList[fields[i]] == 'NUMBER')
                                {
                                    if(i == 0)
                                        columnsArray.push({'title' : labels[i] , 'data' : fields[i] , 'dir' : 'ASC' ,'selected' : true});
                                    else
                                        columnsArray.push({'title' : labels[i] , 'data' : fields[i] , 'dir' : 'ASC' ,'selected' : false});
                                }
                                else
                                {
                                    if(i == 0)
                                        columnsArray.push({'title' : labels[i] , 'data' : fields[i] , 'dir' : 'ASC' ,'selected' : true});

                                    else
                                        columnsArray.push({'title' : labels[i] , 'data' : fields[i] , 'dir' : 'DESC' ,'selected' : false});
                                }
                            }

                            var globalId = component.getGlobalId();
                            var searchTextBox = document.getElementById(globalId+'_text-input-01');
                            if(searchTextBox != null)
                            searchTextBox.value = '';
                            
                            component.set("v.dataTableWrapper.columnsArray" , columnsArray);
                            component.set("v.dataTableWrapper.searchString",'');
                            component.set("v.dataTableWrapper.totalRecords" , retResponse.totalRecords);
                            component.set("v.dataTableWrapper.dataTypesList",typeList);
                            component.set("v.colTypeList", typeList);
                            component.set("v.dataTableWrapper.sortColumn",columnsArray[0].title);
                            component.set("v.isMasterTeamInstance",retResponse.isMasterTeamInstance);

                            console.log('---- ref object'+component.get("v.referencedObject"));
                            
                            if(component.get("v.moduleName") != 'Employee Universe'){
                                component.set("v.referencedObject", '');
                            }else{
                                if(component.get("v.referencedObject") != 'Position Employee'){
                                    component.set("v.referencedObject", 'lightning grid');
                                    component.set("v.loadTable", 'true');
                                }
                            }
                            
                            var load_more_card = component.find('load_more_card');

                            if(((component.get("v.dataTableWrapper.pageNumber")) * component.get("v.dataTableWrapper.pageSize")) > component.get("v.dataTableWrapper.totalRecords"))
                            {
                                $A.util.addClass(load_more_card, "slds-hide");
                            }
                            else
                            {
                                 $A.util.removeClass(load_more_card, "slds-hide");
                            }


                            this.updateBreadcrumb(component,event,"true");
                            if(component.get("v.firstColumn") == 'Checkbox')
                                component.getDisabledCheckbox(component,event);
                            
                            //SIQ-4616 - calling event to render colors in employee module
                            window.setTimeout(
                                $A.getCallback(function() {
                                    console.log('Calling'); 
                                    var posActionEvent = component.getEvent("setPositionAction");
                                    var editEmployeeHeader = 'Dynamic Style';
                                    
                                    posActionEvent.setParams({
                                        "actionType": editEmployeeHeader,
                                    });               

                                    posActionEvent.fire();
                                }), 200
                            );
                            component.hideSpin();   
                    }
                }

                else if (state === "ERROR" || response.getReturnValue() == null) 
                {
                    component.hideSpin();                
                    var errors = response.getError();
                    var errorMessage = errors[0].message;

                    var namespace = component.get("v.namespace");
                    var errorLabel,uapErrorLabel;
                    if(namespace != '')
                    {
                        uapLabel = $A.get("$Label.AxtriaSalesIQTM.No_UAP_Assigned");
                    }
                    else
                    {
                        uapLabel = $A.get("$Label.c.No_UAP_Assigned");
                    }

                    if(errorMessage.indexOf(uapLabel) != -1)
                    {
                        var hideDiv = component.find('hideDiv');
                        $A.util.removeClass(hideDiv,'slds-hide');
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            mode: 'dismissible',
                            message: uapLabel,
                            type : 'error'
                            
                        });
                        toastEvent.fire();
                        return;
                    }

                    this.logException(component, response.getError());
                }    
               
            });
            action.setBackground();
            $A.enqueueAction(action);  
        });
        $A.enqueueAction(profileAction);      

    },

    getRecentData : function(component,event,helper)
    {
        var action = component.get("c.getRecentData");
        
        action.setParams({
            recordId : component.get("v.recordId"),
            objectName : component.get("v.objectName"),
            selectedPosition : component.get("v.selectedPosition"),
            selectedTeamInstance : component.get("v.selectedTeamInstance"),
            moduleName : component.get("v.moduleName")
        });

        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var retResponse = response.getReturnValue();
                var retRecords = retResponse.dataMap;
                var fields = retResponse.columnsList ;
                var labels = retResponse.columnLabels;
                var table_body = component.find('table1');
                var card = component.find('no_record_card');
                // var referenceWrapperList = retResponse.referenceWrapperList;
                var typeList = retResponse.dataTypesList;
                var load_more_card = component.find('load_more_card');
                
                if(retRecords.length == 0)
                {
                    $A.util.removeClass(card, "slds-hide");
                    $A.util.addClass(table_body, "slds-hide");  
                }
                else
                {
                    $A.util.removeClass(table_body, "slds-hide");
                    $A.util.addClass(card, "slds-hide");
                }

                component.set("v.dataTableWrapper" , retResponse);
                component.set("v.dataTableWrapper.queryString" , retResponse.queryString);
                component.set("v.dataTableWrapper.dataArray", retRecords);
                component.set("v.dataTableWrapper.pageNumber" , 1);
                component.set("v.dataTableWrapper.searchString",'');
                component.set("v.dataTableWrapper.totalRecords" , retResponse.totalRecords);
                component.set("v.dataTableWrapper.pageSize" , retResponse.pageSize);
                component.set("v.dataTableWrapper.dataTypesList",typeList);
                component.set("v.colTypeList", typeList);
                component.set("v.dataTableWrapper.objectLabel" , retResponse.objectLabel);
                

                var columnsArray = [];
                var dataArray = [];

                if(((component.get("v.dataTableWrapper.pageNumber")) * component.get("v.dataTableWrapper.pageSize")) > component.get("v.dataTableWrapper.totalRecords"))
                {
                    $A.util.addClass(load_more_card, "slds-hide");
                }

                for(var i =0;i<labels.length;i++)
                {
                    if(typeList[fields[i]] == 'DATETIME' || typeList[fields[i]] == 'DATE' || typeList[fields[i]] == 'NUMBER')
                        columnsArray.push({'title' : labels[i] , 'data' : fields[i] , 'dir' : 'ASC' ,'selected' : false});
                    else
                        columnsArray.push({'title' : labels[i] , 'data' : fields[i] , 'dir' : 'DESC' ,'selected' : false});
                }

                component.set("v.dataTableWrapper.columnsArray" , columnsArray);

                this.updateBreadcrumb(component,event,"false");
                
                if(component.get("v.firstColumn") == 'Checkbox')
                    component.getDisabledCheckbox(component,event); 
                component.hideSpin();

            }

            else if (state === "ERROR" || response.getReturnValue() == null) 
            {
                
                component.hideSpin();
                this.logException(component, response.getError());
            }    
        });
        action.setBackground();
        $A.enqueueAction(action);   
    },

    getFirstView : function(component,event,helper)
    {
        var action = component.get("c.getFirstListView");
        
        action.setParams({
            objectName : component.get("v.objectName")
        });


        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var listView = response.getReturnValue();
                component.changeView(listView);
            }

            else if (state === "ERROR" || response.getReturnValue() == null) 
            {
                this.logException(component, response.getError());
            }    
        });
        action.setBackground();
        $A.enqueueAction(action);
    },

    updateBreadcrumb: function(component, event, updateView) 
    {
        var dataTableWrapper = component.get("v.dataTableWrapper");
        // component.set("v.dataTableWrapper",dataTableWrapper);

        // var updateView = event.getParam("updateView");
        //component.set("v.updateView",updateView);
        if(updateView == "true" && component.get('v.showListBreadcrumb') == 'true')
        {
          var sortedOutput = component.find('sorted-by-text');
          $A.util.removeClass(sortedOutput,'slds-hide');

          var filterColumns = dataTableWrapper.filterColumns;
          if(filterColumns)
          {
            if(filterColumns.length < 1)
            {
              var filterOutput = component.find('filtered-by-text');
              $A.util.addClass(filterOutput,'slds-hide');
            }
            else
            {
              var filterOutput = component.find('filtered-by-text');
              $A.util.removeClass(filterOutput,'slds-hide');
            }
          }
        }

        else
        {
          var filterOutput = component.find('filtered-by-text');
          $A.util.addClass(filterOutput,'slds-hide');

          var sortedOutput = component.find('sorted-by-text');
          $A.util.addClass(sortedOutput,'slds-hide');
        }
    },

    getCookieValue : function(component,event,helper){
        var regex = new RegExp('[; ]'+'apex__CountryID'+'=([^\\s;]*)');
        var cookieValue = (' '+document.cookie).match(regex);
        return (cookieValue!=null || cookieValue!= undefined)?unescape(cookieValue[1]):'';
    },

    logException : function(component, errors) {
        var errorLabel = '';
        if(component.get("v.namespace") != '') 
        {
            errorLabel = $A.get("$Label.AxtriaSalesIQTM.Unexpected_error_notification");
        }
        else
        {
            errorLabel = $A.get("$Label.c.Unexpected_error_notification");
        }

        if (errors[0] && errors[0].message) 
        {
            var errorMessage = errors[0].message;
            var action = component.get("c.logException");
        
            action.setParams({
                message : errorMessage,
                module : component.get("v.moduleName")
            });

            action.setCallback(this,function(response)
            {
                var state = response.getState();
                if(state === 'SUCCESS')
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: errorLabel,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }
        else 
        {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                mode: 'dismissible',
                message: errorLabel,
                type : 'error'
            });
            
            toastEvent.fire();
        }
    }
})