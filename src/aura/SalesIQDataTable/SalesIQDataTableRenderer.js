({
    afterRender : function( component, helper ) {

        this.superAfterRender();

        var didScroll = false;
        var globalId = component.getGlobalId();

        // window.onscroll = function() 
        // {
        //     didScroll = true;
        // }; 

        if(document.getElementById('modal-datatable') != null && document.getElementById('modal-datatable').clientHeight > 0)
        {   
            document.getElementById(globalId+'datatableContainer').onscroll = function()
            {
                if(!document.getElementById('positiondialog').classList.contains('slds-fade-in-hide'))
                {
                    didScroll = true;
                }
                else
                {
                    didScroll = false;
                }
            };
            
        }
        else
        {
            if(document.getElementById(globalId+'datatableContainer') != null && document.getElementById(globalId+'datatableContainer') != undefined ){
                document.getElementById(globalId+'datatableContainer').onscroll = function()
                {
                    didScroll = true;
                };
            }
        }

        
        
        var scrollCheckIntervalId = setInterval( $A.getCallback( function() 
        {
            if ( didScroll && component.isValid() ) 
            {
                didScroll = false;
                if(document.getElementById('datatableDiv') != null)
                {
                    var top = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop || document.getElementById(globalId+'datatableContainer').scrollTop;
                    var scrollHeight = document.getElementById(globalId+'datatableContainer').scrollHeight;
                    if(scrollHeight <= (top + document.getElementById(globalId + 'datatableContainer').clientHeight))
                    {
                        component.next();
                    }
                }
                else
                {
                    var top = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop || document.getElementById(globalId+'datatableContainer').scrollTop;
                    var scrollHeight = document.getElementById(globalId+'datatableContainer').scrollHeight;
                    console.log(top);
                    console.log(scrollHeight);
                    console.log(document.getElementById(globalId + 'datatableContainer').clientHeight);
                    // var scrollHeight = Math.max(document.documentElement.scrollHeight,document.body.scrollHeight);
                    if(scrollHeight <= (top + document.getElementById(globalId + 'datatableContainer').clientHeight))
                    {
                        component.next();
                    }
                }    
            }
        }), 200 );

        component.set( 'v.scrollCheckIntervalId', scrollCheckIntervalId );
    },

    unrender : function( component, helper ) {

        this.superUnrender();

        var scrollCheckIntervalId = component.get( 'v.scrollCheckIntervalId' );

        if ( !$A.util.isUndefinedOrNull( scrollCheckIntervalId ) ) {
            window.clearInterval( scrollCheckIntervalId );
        }

    }
})