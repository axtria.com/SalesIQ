({
    afterRendering: function(component, helper) {
        var line_stroke_width = 1;
        var line_stroke = '#D3D3D3';
        var marker_fill = '#D3D3D3';
        var markerWidth = 20;
        var markerHeight = 20;
        //var viewBox = '0 0 13 13';
        var componentHeight = 50;
        var componentWidth = 50;
        var componentdiff = 10;
        var componentTop = 47;
        var stroke_opacity = 0.9;
        var fill_opacity = 1;
        var startingVal_top = 10;
        var vertical_Diff = 140;
        
        var scenarioCanvasInstanceList = component.get("v.scenarioCanvasInstanceList");
        var line = "";
        var marker = "";
        var svg_height = 0;
        var svg_width = 0;
        for (var i =0;i<scenarioCanvasInstanceList.length;i++){
            
            var level = scenarioCanvasInstanceList[i][common.getKey(component,"Component_Level__c")] ;
            var parentLevel = scenarioCanvasInstanceList[i][common.getKey(component,"Parent_Level__c")]; 
            var parentLevel_array = parentLevel.split(',');
            var parentDisplayOrder = scenarioCanvasInstanceList[i][common.getKey(component,"Parent_Display_Order__c")];
            var parentDisplayOrder_array = parentDisplayOrder.split(',');
            var displayOrder = scenarioCanvasInstanceList[i][common.getKey(component,"Component_Display_Order__c")];
            var twowayarrows = scenarioCanvasInstanceList[i][common.getKey(component,"Is_Editable__c")];
            var topval = startingVal_top+(vertical_Diff*(level-1));
            if(svg_height<topval) {
                svg_height = topval+componentHeight+componentdiff;
            }
            var leftval = 35 + (141 * (displayOrder-1));
            if(svg_width<leftval){
                svg_width = leftval+componentWidth;
            }
            var emval = 1;
            var length = parentDisplayOrder_array.length;
            var XAxis1, XAxis2, YAxis1, YAxis2;
                        for(var k= 0; k<length;k++ ){
                //console.log('for');
                if(parentLevel_array[k] != 0) {
                    //console.log('par');
                    var arrowValue = level+''+displayOrder+''+k;
                    if(level==parentLevel_array[k]  ){
                        
                        if(parentDisplayOrder_array[k]<displayOrder){
                            console.log(' left to right arrow on same level');
                            XAxis1 = (35 + (141 * (parentDisplayOrder_array[k]-1)))+componentWidth+componentdiff;
                            XAxis2 = leftval-(componentdiff);
                            YAxis1 = topval+(componentHeight/2);
                            YAxis2 = YAxis1; 
                            //var refx = (XAxis2-XAxis1)/2;
                            var refx = 3;
                            if(twowayarrows){
                                marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refx+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";  
                            }
                            line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis2+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\"></line>"
                        } else {
                            console.log(' right to left arrow on same level');                            
                            XAxis1 = (35 + (141 * (parentDisplayOrder_array[k]-1)))-componentdiff;
                            XAxis2 = leftval+componentWidth+componentdiff;
                            YAxis1 = topval+(componentHeight/2);
                            YAxis2 = YAxis1; 
                            //var refx = (XAxis1-XAxis2)/2;
                            var refx = 3;
                            if(twowayarrows){
                                marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refx+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";  
                            }
                            line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis2+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\"></line>"
                            
                        }
                    }
                    else if(level>parentLevel_array[k]) {
                        if(parentDisplayOrder_array[k] ==displayOrder ){
                            console.log('With EQUAL /  top to bottom arrow');
                            XAxis1 = (35 + (141 * (parentDisplayOrder_array[k]-1)))+(componentWidth/2);
                            XAxis2 = leftval-(componentdiff);
                            YAxis2 = topval - componentdiff;
                            var YAxis_bottom2 = topval+(componentHeight/2);
                            var XAxis_bottom1 = XAxis1+(line_stroke_width/2);
                            YAxis1 = (startingVal_top+(141*(parentLevel_array[k]-1)))+componentHeight+componentTop; 
                            //var refY = (YAxis2-YAxis1)/2;
                            var refY = 3;
                            if(twowayarrows){
                                marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refY+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";              
                            }
                            line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis1+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\" ></line>"
                        
                        }
                        else if(parentDisplayOrder_array[k] < displayOrder){
                            console.log(parentDisplayOrder_array[k]+"parentDisplayOrder_array[k]---"+displayOrder+"displayOrder")
                            console.log('parentDisplayOrder smaller to displayOrder  top to bottom arrow ');
                            XAxis1 = (35 + (141 * (parentDisplayOrder_array[k]-1)))+componentWidth+componentdiff;
                            YAxis1 = (startingVal_top+(141*(parentLevel_array[k]-1)))+(componentHeight/2); 
                            
                            XAxis2 = 35 + (141 * (displayOrder-1))+(line_stroke_width/2)+(componentWidth/2);
                            YAxis2 = topval-(componentdiff);
                            
                            var XAxis_bottom2 = XAxis2-(line_stroke_width/2);
                            var YAxis_bottom2 = YAxis1-(line_stroke_width/2);
                            //var refx = (YAxis2-YAxis1)/2;
                            var refx = 3;
                            if(twowayarrows){
                                marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refx+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";                              
                            }
                            line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis2+"\" y2=\""+YAxis1+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+"></line>"                           
                            line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis_bottom2+"\" y1=\""+YAxis_bottom2+"\" x2=\""+XAxis_bottom2+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\" ></line>"                            
                        }
                        else {
                            console.log(' top to bottom arrow ');
                            XAxis1 = (35 + (141 * (parentDisplayOrder_array[k]-1)))+(componentWidth/2);
                            XAxis2 = leftval-(componentdiff);
                            YAxis2 = topval+(componentHeight/2)+(line_stroke_width/2);
                            var YAxis_bottom2 = topval+(componentHeight/2);
                            var XAxis_bottom1 = XAxis1+(line_stroke_width/2);
                            YAxis1 = (startingVal_top+(141*(parentLevel_array[k]-1)))+componentHeight+componentTop; 
                            //var refx = (XAxis2-XAxis1)/2;
                            var refx = 3;
                            if(twowayarrows){
                                marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refx+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";                              
                            }
                            line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis1+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" ></line>"
                            
                            line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis_bottom1+"\" y1=\""+YAxis_bottom2+"\" x2=\""+XAxis2+"\" y2=\""+YAxis_bottom2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\"></line>"
                        }
                    }
                        else if(level<parentLevel_array[k]) {
                            if(parentDisplayOrder_array[k] == displayOrder ){
                                console.log('With EQUAL /  bottom to top arrow'); 
                                XAxis1 = (35 + (141 * (parentDisplayOrder_array[k]-1)))+(componentWidth/2);
                                XAxis2 = leftval+componentWidth+componentdiff;
                                YAxis2 = topval+componentTop+componentHeight;
                                //var YAxis_bottom2 = YAxis2+(line_stroke_width/2);
                                YAxis1 = (startingVal_top+(141*(parentLevel_array[k]-1)))+componentdiff; 
                                var refx = 3;
                                if(twowayarrows){
                                    marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refx+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";  
                                }
                                line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis1+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\"></line>"
                                
                            } else if(parentDisplayOrder_array[k] < displayOrder){
                                console.log('parentDisplayOrder smaller to displayOrder /  bottom to top arrow'); 
                                XAxis1 = leftval-componentdiff;
                                XAxis2 = (35 + (141 * (parentDisplayOrder_array[k]-1)))+(componentWidth/2);
                       
                                YAxis1 = topval+(componentHeight/2);
                                YAxis2 = (startingVal_top+(141*(parentLevel_array[k]-1)))-componentdiff; 
                                
                                var YAxis_bottom1 = YAxis1-(line_stroke_width/2);
                                var refx = 3;
                                if(twowayarrows){
                                    console.log(twowayarrows+"twowayarrows")
                                    marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refx+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";  
                                }
                                line = line + "<line stroke-opacity='"+stroke_opacity+"'  x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis2+"\" y2=\""+YAxis1+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+"></line>"                            
                                line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis2+"\" y1=\""+YAxis_bottom1+"\" x2=\""+XAxis2+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\"></line>"
                                
                                
                                /*console.log(parentDisplayOrder_array[k]+"parentDisplayOrder_array[k]---"+displayOrder+"displayOrder")
                                console.log('parentDisplayOrder smaller to displayOrder afterViewRendering top to bottom arrow ');
                                XAxis1 = (35 + (141 * (parentDisplayOrder_array[k]-1)))+componentWidth+componentdiff;
                                YAxis1 = (startingVal_top+(141*(parentLevel_array[k]-1)))+(componentHeight/2); 
                                
                                XAxis2 = 35 + (141 * (displayOrder-1))+(componentdiff/2)+(componentWidth/2);
                                YAxis2 = topval-(componentdiff);
                                
                                var XAxis_bottom2 = XAxis2-(componentdiff/2);
                                var YAxis_bottom2 = YAxis1-(line_stroke_width/2);
                                var refx = (YAxis2-YAxis1)/2;
                                if(twowayarrows){
                                    marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refx+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";                              
                                }
                                line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis2+"\" y2=\""+YAxis1+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+"></line>"
                               
                                line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis_bottom2+"\" y1=\""+YAxis_bottom2+"\" x2=\""+XAxis_bottom2+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\" ></line>"
                            */
                            }
                            else {
                                console.log(' bottom to top arrow'); 
                                XAxis1 = (35 + (141 * (parentDisplayOrder_array[k]-1)))+(componentWidth/2);
                                XAxis2 = leftval+componentWidth+componentdiff;
                                YAxis2 = topval+componentHeight-componentdiff;
                                var YAxis_bottom2 = YAxis2+(line_stroke_width/2);
                                YAxis1 = (startingVal_top+(141*(parentLevel_array[k]-1)))+componentdiff; 
                                var refx = 3;
                                if(twowayarrows){
                                    console.log(twowayarrows+"twowayarrows")
                                    marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refx+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";  
                                }
                                line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis1+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+"></line>"
                                line = line + "<line stroke-opacity='"+stroke_opacity+"'  x1=\""+XAxis1+"\" y1=\""+YAxis_bottom2+"\" x2=\""+XAxis2+"\" y2=\""+YAxis_bottom2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\"></line>"
                            }
                        }
                }
            }

        }
        var resultsToast = $A.get("e.force:showToast");
        var svg = component.find("svg_content");
        svg.getElement().innerText = " <svg id='svg' width=\""+svg_width+"\" height=\""+svg_height+"\"><defs>"+marker+"</defs>"+line+"</svg>";
        var value = svg.getElement().innerText;
        value = value.replace("<![CDATA[", "").replace("]]>", "");
        svg.getElement().innerHTML = value;
        document.getElementById('canvas').style.height=svg_height+componentTop+'px';
        document.getElementById('canvas').style.width=svg_width+88+'px';
        document.getElementById('businessFrame').style.width=svg_width+88+'px';
        var scenarioName='';
        var action = component.get("c.getScenarioName");
        action.setParams({
            scenarioId : component.get("v.scenarioId"),
        });
        action.setCallback(this,function(response){
                               var state = response.getState();
                               
                               if(state === 'SUCCESS' && response.getReturnValue() != null){
                                   scenarioName=response.getReturnValue();
                                       resultsToast.setParams({
                                        "title": "Success!",
                                        "type": "Success",
                                        "message":'Scenario '+scenarioName +' has been created successfully.',
                                    });
                                    resultsToast.fire();  
                              }
             component.hideSpinner();
        });
        
        $A.enqueueAction(action);
    },

    refreshStage: function(component,event)
    {
        var scenarioStage = event.getParam("infoToPass");
        if(scenarioStage) {
            component.set("v.scenarioStage", scenarioStage);
        }
    },

    getOrgNamespace: function(component,event,helper){
         var action = component.get("c.getOrgNamespaceServer");
         action.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue() != null)
            {
                var namespace=response.getReturnValue();
                component.set("v.namespace",namespace);
            }
            component.doInit();
         });
        $A.enqueueAction(action);
        
    },
    
    doInit : function(component,event,helper)
    {
        component.showSpinner();
        console.log('inside do init');
        
      //  helper.updateCountryAttributes(component,event,helper);
        
        var src=' ';
        var sPageURL = decodeURIComponent(window.location.href);
        var sURLVariables = sPageURL.split('&');
        var sParameterName;
        var scenarioId;
        var viewcanvasFlagVal;
        var businessRuleTemplateId;
        var ScenarioType;
        var workSpaceId;

        for (var i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

                console.log(sParameterName[0]);
                console.log(sParameterName[1]);
 
            if(sParameterName[0]=='c__src')
                src=sParameterName[1];
             if(sParameterName[0].indexOf('c__record_Id')!=-1)
                scenarioId=sParameterName[1];
            if(sParameterName[0]=='c__viewcanvas')
                viewcanvasFlagVal=sParameterName[1];
            if(sParameterName[0]=='c__workSpaceId')
                workSpaceId=sParameterName[1];
            if(sParameterName[0]=='c__ScenarioType')
                ScenarioType=sParameterName[1];
            if(sParameterName[0]=='c__countryId')
                component.set("v.countryId",sParameterName[1]);
          

            
        }

       
        component.set('v.isScenarioStatusSucess',true);
        if(src==' '){
            scenarioId = component.get("v.scenarioId");
            viewcanvasFlagVal = component.get("v.viewcanvas");
            businessRuleTemplateId = component.get("v.businessRuleTemplateId");
            ScenarioType = component.get("v.ScenarioType");
            workSpaceId = component.get("v.workSpaceId");
        }else{
            component.set("v.workSpaceId",workSpaceId);
            component.set("v.scenarioId",scenarioId);
            component.set("v.record_Id",scenarioId);
        }
        //console.log('windowViewMode-normal',$j('windowViewMode-normal').height());
        console.log('workSpaceId Id when we enter ::: '+workSpaceId);
        console.log('Scenario Id when we enter ::: '+scenarioId);
        console.log('viewcanvasFlagVal ::: '+viewcanvasFlagVal);
        console.log('scenarioStage ::: '+component.get("v.scenarioStage"));
        var ns = component.get("v.namespace");
        
        if(scenarioId == null || scenarioId == undefined || scenarioId == ''){
            scenarioId = component.get("v.record_Id");
        }
        // console.log('ScenarioType ::: '+ScenarioType);
        // console.log('businessRuleTemplateId ::: '+businessRuleTemplateId);
        
        var action = component.get("c.checkScenarioExistence");
        action.setParams({
            scenarioId : scenarioId,
        });
        action.setCallback(this,function(response)
       {
           var state = response.getState();
           
           if(state === 'SUCCESS' && response.getReturnValue != null)
           {
               var existingScenarioDetailsList = response.getReturnValue();
               if(existingScenarioDetailsList.length > 0)
               {
                    if(ns != '')
                    {
                        ns = ns+'__';
                    }
                    // component.set("v.isRunLocked",!existingScenarioDetailsList[0][ns+'Scenario_Id__r'][ns+'Is_Locked__c'] && (existingScenarioDetailsList[0][ns+'Scenario_Id__r'][ns+'Last_Sync_Success_Date__c'] != null));
                    
                    if(existingScenarioDetailsList[0][ns+'Scenario_Id__r'][ns+'Last_Promote_Success_Date__c'] == null || existingScenarioDetailsList[0][ns+'Scenario_Id__r'][ns+'Last_Promote_Success_Date__c'] == undefined)
                    {
                        component.set("v.isDeltaLocked",false);
                    }
                    else
                    {
                        component.set("v.isDeltaLocked",true);
                    }
                    component.set("v.ruleExecStatus",existingScenarioDetailsList[0][ns+'Scenario_Id__r'][ns+'Rule_Execution_Status__c']);
                    component.set('v.requestProcessStage',existingScenarioDetailsList[0][ns+'Scenario_Id__r'][ns+'Request_Process_Stage__c']);
                   component.set("v.viewcanvas","Y");
                   if(component.get("v.ScenarioType") == null || component.get("v.ScenarioType") == '')
                    {
                        ScenarioType = existingScenarioDetailsList[0][ns+'Scenario_Id__r'][ns+'Scenario_Type__c'];
                        component.set("v.ScenarioType",ScenarioType);
                        //COMPONENT ERROR TO GET COUNTRY ID FROM SCENARIO
                        component.set("v.countryId",existingScenarioDetailsList[0][ns+'Scenario_Id__r'][ns+'Country__c']);
                        // if(ns!=''){
                            
                        // }else{
                        //     ScenarioType = existingScenarioDetailsList[0]['Scenario_Id__r']['Scenario_Type__c'];
                        //     component.set("v.ScenarioType",ScenarioType);
                        //     //COMPONENT ERROR TO GET COUNTRY ID FROM SCENARIO
                        //     component.set("v.countryId",existingScenarioDetailsList[0]['Scenario_Id__r']['Country__c']);
                        // }
                    }
               }
               
               else
               {
                   //   component.set("v.viewcanvas","N");
               }
               component.set("v.scenarioId",scenarioId);
               component.set("v.businessRuleTemplateId",businessRuleTemplateId); 
               component.createInstance();
               component.runLocked();
               // component.checkScenarioStatus();
               // component.ruleExecutionStatus();
           }
       });
        
        $A.enqueueAction(action);
        
    },

    runLocked : function(component,event,helper)
    {
       var action = component.get("c.checkRunLocked");
       action.setParams({
           scenarioId : component.get("v.scenarioId")
       });
       
        action.setCallback(this,function(response)
        {
            var state = response.getState();
                              
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                component.set("v.isRunLocked",response.getReturnValue());   
            }
             else{
                console.log("Failed with state:  "+ state);
            }
        });
       
       $A.enqueueAction(action); 
    },

   ruleExecutionStatus : function(component,event,helper){
            
       var action = component.get("c.getRuleExecutionStatus");
       action.setParams({
           scenarioId : component.get("v.scenarioId")
       });
       
        action.setCallback(this,function(response)
        {
            var state = response.getState();
                              
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var statusVal = response.getReturnValue();
                console.log("statusVal");
                console.log(statusVal);
                console.log(component.get("v.requestProcessStage"));
                component.set("v.ruleExecStatus",statusVal);   
            }
             else{
                console.log("Failed with state:  "+ state);
            }
        });
       
       $A.enqueueAction(action);
    },
    
    createInstance : function(component,event,helper){
        
        var scenarioId = component.get("v.scenarioId");
        var businessRuleTemplateId = component.get("v.businessRuleTemplateId");

        console.log('businessRuleTemplateId ::: '+businessRuleTemplateId);
        console.log('scenarioId ::: '+scenarioId);
        console.log('view canvas value :: '+component.get("v.viewcanvas"));
        
        if(component.get("v.viewcanvas") == 'Y')
        {
            if(component.get('v.summarizedViewFlag') == true){

            var action = component.get("c.getScenarioViewInstanceSummarized");
            action.setParams({
                scenarioId : component.get('v.scenarioId'),
            });
            action.setCallback(this,function(response)
            {
               var state = response.getState();
               
               if(state === 'SUCCESS' && response.getReturnValue != null)
               {
                   var scenarioViewObjList = response.getReturnValue();
                   if(scenarioViewObjList.length > 0){
                        component.set("v.scenarioViewObjList",scenarioViewObjList);
                        component.populateViewCanvasRow(); 
                   }
                    
                   else{
                        component.set('v.summarizedViewFlag',false);
                        component.createInstance();
                   }
                    var action = component.get("c.getScenarioViewDetails");
                   action.setParams({
                       scenarioId : scenarioId,
                   });
                   
                    action.setCallback(this,function(response)
                    {
                        var state = response.getState();
                                          
                        if(state === 'SUCCESS' && response.getReturnValue != null)
                        {
                            var scenarioInstanceListView = response.getReturnValue();
                            console.log('scenarioInstanceListView');
                            console.log(scenarioInstanceListView);
                              
                            if(scenarioInstanceListView.length > 0)
                            {
                                component.set("v.viewheader",true);
                                component.set('v.scenarioName',scenarioInstanceListView[1]);
                                component.set('v.scenarioStartDate',scenarioInstanceListView[2]);
                                component.set("v.scenarioEndDate",scenarioInstanceListView[3]);
                                component.set("v.lastModifiedby",scenarioInstanceListView[4]);
                                component.set("v.scenarioStatus",scenarioInstanceListView[5]);
                                component.set("v.scenarioStage",scenarioInstanceListView[6]);
                                component.set("v.selectedTeamInstance",scenarioInstanceListView[7]);
                                component.set("v.selectedTeam",scenarioInstanceListView[8]);
                                component.set("v.requestProcessStage",scenarioInstanceListView[9]);
                            }
                          //  component.populateViewCanvasRow(); 
                          //  component.hideSpinner();
                        }
                    });
                   
                   $A.enqueueAction(action);
                }
            }); 
            
            $A.enqueueAction(action);                
        }

        else{
                  if(component.get("v.scenarioId") != null){
                scenarioId =  component.get("v.scenarioId");
            }
            else{
                scenarioId = component.get("v.record_Id");
            }
            // var scenarioId = component.get("v.record_Id");
            console.log('record_Id --------> '+scenarioId);
            var action = component.get("c.getScenarioViewInstance");
            action.setParams({
                scenarioId : scenarioId,
            });
            action.setCallback(this,function(response)
            {
               var state = response.getState();
               
               if(state === 'SUCCESS' && response.getReturnValue != null)
               {
                   var scenarioViewObjList = response.getReturnValue();
                   component.set("v.scenarioViewObjList",scenarioViewObjList);
                   var action = component.get("c.getScenarioViewDetails");
                   action.setParams({
                       scenarioId : scenarioId,
                   });
                   
                    action.setCallback(this,function(response)
                    {
                        var state = response.getState();
                                          
                        if(state === 'SUCCESS' && response.getReturnValue != null)
                        {
                            var scenarioInstanceListView = response.getReturnValue();
                            console.log('scenarioInstanceListView');
                            console.log(scenarioInstanceListView);
                              
                            if(scenarioInstanceListView.length > 0)
                            {
                                component.set("v.viewheader",true);
                                component.set('v.scenarioName',scenarioInstanceListView[1]);
                                component.set('v.scenarioStartDate',scenarioInstanceListView[2]);
                                component.set("v.scenarioEndDate",scenarioInstanceListView[3]);
                                component.set("v.lastModifiedby",scenarioInstanceListView[4]);
                                component.set("v.scenarioStatus",scenarioInstanceListView[5]);
                                component.set("v.scenarioStage",scenarioInstanceListView[6]);
                                component.set("v.selectedTeamInstance",scenarioInstanceListView[7]);
                                component.set("v.selectedTeam",scenarioInstanceListView[8]);
                                component.set("v.requestProcessStage",scenarioInstanceListView[9]);
                                 if(scenarioInstanceListView[10] != undefined){
                                var stringValue = scenarioInstanceListView[10];
                                var boolValue = stringValue.toLowerCase() == 'true' ? true : false; 
                                //component.set("v.enableAffiliationFlag", boolValue);
                                }
                            }
                            component.populateViewCanvasRow(); 
                           // component.hideSpinner();
                        }
                    });
                   
                   $A.enqueueAction(action);
                }
            }); 
            
            $A.enqueueAction(action);

        }

           // // component.toggleTemplateView()
           //  console.log('inside view canvas');
          
        }
        
        else if(scenarioId != '' && businessRuleTemplateId != '')
        {
            console.log('inside create canvas');
            var action = component.get("c.getBusinessRuleDetails");
            action.setParams
            ({
                businessRuleTemplateId : businessRuleTemplateId,
            });
            action.setCallback(this,function(response)
           {
               var state = response.getState();
               if(state === 'SUCCESS' && response.getReturnValue != null)
               {
                   var businessRulesDetailsList = response.getReturnValue();
                   console.log('businessRulesDetailsList');
                   console.log(businessRulesDetailsList);
                   
                   if(businessRulesDetailsList.length > 0)
                   {
                       
                       component.set("v.businessRulesDetailsList",businessRulesDetailsList);
                       
                       var action = component.get("c.createScenarioRuleInstance");
                       action.setParams({
                           defaultbusinessRulesDetailsList : businessRulesDetailsList,
                           scenarioId : scenarioId,
                       });
                       action.setCallback(this,function(response)
                        {
                          var state = response.getState();
                          if(state === 'SUCCESS' && response.getReturnValue != null)
                          {
                              var scenarioCanvasInstanceList = response.getReturnValue();
                              console.log('scenarioCanvasInstanceList');
                              console.log(scenarioCanvasInstanceList);
                              console.log('scenarioId');
                              console.log(scenarioId);
                              
                              if(scenarioCanvasInstanceList.length > 0)
                              {
                                  
                                  var action = component.get("c.getDataset");
                                  action.setParams
                                  ({
                                      scenarioCanvasInstanceList : scenarioCanvasInstanceList,
                                      scenarioId : scenarioId
                                  });  
                                  
                                action.setCallback(this,function(response)
                                {
                                    var state = response.getState();
                                    if(state === 'SUCCESS' && response.getReturnValue != null)
                                    {

                                        var scenarioDetailsList = response.getReturnValue();
                                        console.log('---------- getDataset');
                                        console.log(scenarioDetailsList);

                                        if(scenarioDetailsList.length > 0){
                                            
                                            /*component.set('v.scenarioName',scenarioDetailsList[0][common.getKey(component,"Scenario_Name__c")]);
                                            component.set('v.scenarioStartDate',scenarioDetailsList[0][common.getKey(component,"Effective_Start_Date__c")]);
                                            component.set ('v.scenarioEndDate',scenarioDetailsList[0][common.getKey(component,"Effective_End_Date__c")]);
                                            component.set('v.lastModifiedby',scenarioDetailsList[0].LastModifiedBy.Name);*/

                                            component.set("v.viewheader",true);
                                            component.set('v.scenarioName',scenarioDetailsList[1]);
                                            component.set('v.scenarioStartDate',scenarioDetailsList[2]);
                                            component.set("v.scenarioEndDate",scenarioDetailsList[3]);
                                            component.set("v.lastModifiedby",scenarioDetailsList[4]);
                                            component.set("v.scenarioStatus",scenarioDetailsList[5]);
                                            component.set("v.scenarioStage",scenarioDetailsList[6]);
                                            component.set("v.selectedTeamInstance",scenarioDetailsList[7]);
                                            component.set("v.requestProcessStage",scenarioDetailsList[8]);
                                            //component.set("v.enableAffiliationFlag",scenarioDetailsList[9]);

                                        }

                                        //var scenarioId = component.get('v.scenarioId');
                                        //component.set("v.scenarioId",scenarioId);

                                        console.log('calling perform action');

                                    if(scenarioCanvasInstanceList.length > 0)
                                    {
                                     var performaction = component.get("c.getcomputeRules");
                                     performaction.setParams({
                                         scenarioCanvasInstanceList : scenarioCanvasInstanceList
                                     });
                                     
                                     performaction.setCallback(this,function(response)
                                       {
                                           var state = response.getState();
                                           if(state === 'SUCCESS' && response.getReturnValue != null)
                                           {
                                               var returnval = response.getReturnValue();
                                               component.set("v.scenarioCanvasInstanceList",scenarioCanvasInstanceList);
                                               component.populateRow();
                                           }
                                           else{
                                               component.set("v.scenarioCanvasInstanceList",scenarioCanvasInstanceList);
                                               component.populateRow();
                                           }
                                           
                                       });
                                     
                                     $A.enqueueAction(performaction); 
                                    }
                                    else
                                    {
                                     alert('scenarioCanvasInstanceList for business Rules is empty');
                                    }

                                    }

                                });
                                  
                                  $A.enqueueAction(action);
                              }
                              
                              else
                              {
                                  alert('Scenario Canvas Instance List is empty');
                              }
                          }
                          
                        });
                       
                       $A.enqueueAction(action);  
                   }
               }
           });
            
            $A.enqueueAction(action);
            
        }
        
        else{
            alert('Critical values missing');
        }
    },
    
    populateRow: function (component) 
    {            
        var startingVal_top = 10;
        var vertical_Diff = 140;
        var scenarioCanvasInstanceList = component.get("v.scenarioCanvasInstanceList");
        console.log('scenarioCanvasInstanceList');
        console.log(scenarioCanvasInstanceList);
        var scenarioId = component.get('v.scenarioId');
        
        console.log('inside populate row');
        
        var action = component.get("c.getComponentDetails");
        action.setParams({
            scenarioCanvasInstanceList : scenarioCanvasInstanceList,
            scenarioId : scenarioId,
        });
        action.setCallback(this,function(response)
                           {
                               var state = response.getState();
                               if(state === 'SUCCESS' && response.getReturnValue != null)
                               {
                                   var componentMappingList = response.getReturnValue();
                                   component.set('v.componentMappingList',componentMappingList);
                                   var scenarioCanvasInstanceList = component.get("v.scenarioCanvasInstanceList");
                                   var componentMappingList = component.get("v.componentMappingList");
                                   
                                   var newComponents = [];
                                   var dict = [];
                                   var statusicon = [];
                                   var twowaycommunication = [];
                                   for (var i =0;i<componentMappingList.length;i++) 
                                   {
                                       dict.push({
                                           icon:  componentMappingList[i][common.getKey(component,"DisplayIcon__c")],
                                           iconCls:componentMappingList[i][common.getKey(component,"DisplayIconClass__c")]
                                       });
                                   }
                                   
                                   // statusicon.push({  
                                   //      icon:  "utility:check",            
                                   // });
                                   
                                   for (var i =0;i<scenarioCanvasInstanceList.length;i++) 
                                   {
                                       
                                       statusicon.push({
                                           iconReceived:  scenarioCanvasInstanceList[i][common.getKey(component,"Status__c")],
                                       });

                                        twowaycommunication.push({
                                            twoway: scenarioCanvasInstanceList[i][common.getKey(component,"Is_Editable__c")]
                                       });
                                       
                                       
                                       var iconNameMap = {};
                                       
                                       if(iconNameMap['In Progress'] == undefined)
                                       {
                                           iconNameMap['In Progress'] = {};
                                           iconNameMap['In Progress']['iconName'] = "utility:"+"threedots";     
                                           iconNameMap['In Progress']['iconClass'] = "status queue" ;
                                           iconNameMap['In Progress']['title'] = "In Progress" ;
                                           iconNameMap['Success'] = {};
                                           iconNameMap['Success']['iconName'] = "utility:"+"check";     
                                           iconNameMap['Success']['iconClass'] = "status success" ;
                                           iconNameMap['Success']['title'] = "Success" ;
                                           iconNameMap['Pending'] = {};
                                           iconNameMap['Pending']['iconName'] = "action:"+"defer";     
                                           iconNameMap['Pending']['iconClass'] = "status pending" ;
                                           iconNameMap['Pending']['title'] = "Pending" ;
                                           iconNameMap['Error Occured'] = {};
                                           iconNameMap['Error Occured']['iconName'] = "utility:"+"error";     
                                           iconNameMap['Error Occured']['iconClass'] = "status error" ; 
                                           iconNameMap['Error Occured']['title'] = "Error" ;        
                                       }
                                       
                                       
                                       console.log('+++++++++++++ Map Values');
                                       console.log(iconNameMap);
                                       
                                       console.log('statusicon ::::');
                                       console.log(statusicon);
                                       var level = scenarioCanvasInstanceList[i][common.getKey(component,"Component_Level__c")] ;
                                       var parentDisplayOrder = scenarioCanvasInstanceList[i][common.getKey(component,"Parent_Display_Order__c")];
                                       var displayOrder = scenarioCanvasInstanceList[i][common.getKey(component,"Component_Display_Order__c")];
                                       var topval = startingVal_top+(vertical_Diff*(level-1));
                                       var leftval = 35 + (141 * (displayOrder-1));

                                       console.log('blur val :: '+scenarioCanvasInstanceList[i][common.getKey(component,"Display_Icon_Style__c")]);
                                       var myClass = '';
                                       if(scenarioCanvasInstanceList[i][common.getKey(component,"Display_Icon_Style__c")] != undefined)
                                       {
                                            if(scenarioCanvasInstanceList[i][common.getKey(component,"Display_Icon_Style__c")] == 'blur')
                                            {
                                                myClass = dict[i].iconCls + ' blurring';
                                            }
                                            else
                                            {
                                                myClass = dict[i].iconCls + ' boxing';
                                            }
                                       }
                                       else
                                       {
                                            myClass = dict[i].iconCls;
                                       }
                                       
                                       
                                       newComponents.push(["aura:html", {
                                           "tag": "div",
                                           "HTMLAttributes": {
                                               "class": myClass,
                                               "id" : "flowchartWindow_"+level+"_"+displayOrder,
                                               "style": "top : "+topval+"px; left: "+leftval+"px;",
                                               "onclick":component.getReference("c.clickButton"),
                                               "title":scenarioCanvasInstanceList[i][common.getKey(component,"ComponentTypeLabel__c")],
                                               "data-label":level
                                               
                                           }
                                           
                                       }]);
                                       
                                       newComponents.push(["aura:html", {
                                           "tag": "div",
                                           "HTMLAttributes": {
                                               "class": "info"
                                           }
                                           
                                       }]);
                                       
                                       
                                       newComponents.push(["lightning:icon",
                                                           {
                                                               "iconName": "utility:"+dict[i].icon,
                                                               "size": "small",
                                                               "class": "comp_icon"
                                                           }]);
                                       
                                       newComponents.push(["aura:html", {
                                           "tag": "div",
                                           "body": scenarioCanvasInstanceList[i][common.getKey(component,"ComponentTypeLabel__c")],
                                           "HTMLAttributes": {
                                               "class": "text" 
                                           }
                                           
                                       }]);
                                       
                                       newComponents.push(["lightning:icon",           
                                                           {           
                                                               "iconName": iconNameMap[statusicon[i].iconReceived]['iconName'],         
                                                               "size": "x-small",         
                                                               "class": iconNameMap[statusicon[i].iconReceived]['iconClass'],
                                                               "title": iconNameMap[statusicon[i].iconReceived]['title']
                                                           }]);
                                       
                                   }
                                   $A.createComponents(
                                       newComponents,
                                       function (components, status, errorMessage) {
                                           if (status === "SUCCESS") 
                                           {
                                               console.log(status);
                                               console.log('inside create component');
                                               var pageBody = component.get("v.body");
                                               for (var i = 0; i < components.length; i += 5) 
                                               {
                                                   var outerDiv = components[i];
                                                   var infoDiv = components[i+1]
                                                   var icon = components[i + 2];
                                                   var innerDiv = components[i+3];
                                                   var iconDiv = components[i+4];
                                                   var infoBody = infoDiv.get("v.body");
                                                   infoBody.push(icon);
                                                   infoBody.push(innerDiv);
                                                   infoBody.push(iconDiv);
                                                   infoDiv.set("v.body",infoBody)
                                                   var tdBody = outerDiv.get("v.body");
                                                   tdBody.push(infoDiv);
                                                   outerDiv.set("v.body", tdBody);
                                                   pageBody.push(outerDiv);
                                               }
                                               component.set("v.body", pageBody);
                                               component.afterRendering();
                                               
                                           }
                                           else 
                                           {
                                               
                                           }
                                       }
                                   )
                                   
                               }
                               else
                               {
                               }
                           }); 
        
        $A.enqueueAction(action);
        
        
    },
    
    clickButton : function(component,event,helper)
    {
        
        var scenarioId;
        var viewcanvasflag = component.get("v.viewcanvas");
        if(viewcanvasflag == 'Y')
        {
            if(component.get("v.scenarioId") != null)
            {
                scenarioId =  component.get("v.scenarioId");
                console.log('scenarioId ::: '+scenarioId);
                console.log('inside if');
            }
            else
            {
                scenarioId = component.get("v.record_Id");
                console.log('scenarioId ::: '+scenarioId);
                console.log('inside else of if');
            }
        }
        
        else
        {
            scenarioId = component.get('v.scenarioId');
            console.log('scenarioId ::: '+scenarioId);
            console.log('inside else');
        }

        if(component.get('v.summarizedViewFlag') == true){
            var componentid = event.currentTarget.id;
            component.set('v.summComponentId',componentid);
            component.getSummarizedViewOrder();
        }
        else{
             var workSpaceId = component.get("v.workSpaceId");
        console.log('workSpaceId when we click component button :::: '+workSpaceId);
        var componentId = event.currentTarget.id;
        /*componentId = componentId.substring(15,componentId.length);
        console.log('componentId::: '+componentId);
        var data_label = event.currentTarget.getAttribute('data-label');
        console.log('data_label value ::: '+data_label);*/
        var arrIds=componentId.split("_");
        console.log('2nd elemet-->'+arrIds[1]);
        console.log('3rd elemet-->'+arrIds[2]);
        componentId=arrIds[2];
        var data_label=arrIds[1];
        var scenarioAction = component.get("c.getScenarioInstanceViewListDetails");
        scenarioAction.setParams({
            scenarioId : scenarioId,
            /*componentId : componentId,
            data_label : data_label*/
            componentId : arrIds[2],
            data_label : arrIds[1]
        });
        
        scenarioAction.setCallback(this,function(response)
                                   {
                                       console.log('state:::: '+response.getState());
                                       var scenarioInstanceDetailsViewList = response.getReturnValue();
                                       console.log('scenarioInstanceDetailsViewList');
                                       console.log(scenarioInstanceDetailsViewList);
                                       
                                       for(var i = 0;i<scenarioInstanceDetailsViewList.length;i++){
                                           
                        var componentDisplayOrder = scenarioInstanceDetailsViewList[i][common.getKey(component,"Component_Display_Order__c")];
                        var componentLevel = scenarioInstanceDetailsViewList[i][common.getKey(component,"Component_Level__c")];
                                           
                                           console.log('inside getScenarioInstanceViewListDetails action success');
                                           console.log('componentDisplayOrder ::: '+componentDisplayOrder);
                                           if(componentDisplayOrder == componentId 
                                              && componentLevel == data_label){
                                               
                                               var scenarioRuleInstanceIdReceived = scenarioInstanceDetailsViewList[i].Id;
                                               component.set("v.scenarioRuleInstanceIdReceived",scenarioRuleInstanceIdReceived);
                                               console.log('scenarioRuleInstanceIdReceived ::: '+scenarioRuleInstanceIdReceived);
                                               var componentDefinition =  scenarioInstanceDetailsViewList[i][common.getKey(component,"Component_Type_Master__r")][common.getKey(component,"Component_Definition__c")];
                                               component.set("v.componentDefinition",componentDefinition);
                                               console.log('componentDefinition ::: '+componentDefinition);
                                               component.navigateToRespectiveComponents();
                                               
                                           }
                                       }
                                       
                                       
                                   });
        
        $A.enqueueAction(scenarioAction); 
        }
        
      
        
    },

    getSummarizedViewOrder : function(component,event,helper){
        var workSpaceId = component.get("v.workSpaceId");
        console.log('workSpaceId when we click component button :::: '+workSpaceId);
        var componentId = component.get('v.summComponentId');
        /*componentId = componentId.substring(15,componentId.length);
        console.log('componentId::: '+componentId);
        var data_label = event.currentTarget.getAttribute('data-label');
        console.log('data_label value ::: '+data_label);*/
        var arrIds=componentId.split("_");
        console.log('2nd elemet-->'+arrIds[1]);
        console.log('3rd elemet-->'+arrIds[2]);
        componentId=arrIds[2];
        var data_label=arrIds[1];
        var scenarioAction = component.get("c.getScenarioInstanceViewListDetailsSummarized");
        scenarioAction.setParams({
            scenarioId : component.get('v.scenarioId'),
            /*componentId : componentId,
            data_label : data_label*/
            componentId : arrIds[2],
            data_label : arrIds[1]
        });
        
        scenarioAction.setCallback(this,function(response)
                                   {
                                       console.log('state:::: '+response.getState());
                                       var scenarioInstanceDetailsViewList = response.getReturnValue();
                                       console.log('scenarioInstanceDetailsViewList');
                                       console.log(scenarioInstanceDetailsViewList);
                                       
                                       for(var i = 0;i<scenarioInstanceDetailsViewList.length;i++){
                                           var componentDisplayOrder;
                                           var componentLevel;
                                           if(component.get('v.summarizedViewFlag') == true){
                                                 componentDisplayOrder = scenarioInstanceDetailsViewList[i][common.getKey(component,"Component_Display_Order_Summarize__c")];
                                                 componentLevel = scenarioInstanceDetailsViewList[i][common.getKey(component,"Component_Level_Summarize__c")];
                                           }
                                           // componentDisplayOrder = scenarioInstanceDetailsViewList[i][common.getKey(component,"Component_Display_Order__c")];
                                           // componentLevel = scenarioInstanceDetailsViewList[i][common.getKey(component,"Component_Level__c")];
                                           console.log('inside getScenarioInstanceViewListDetails action success');
                                           console.log('componentDisplayOrder ::: '+componentDisplayOrder);
                                           if(componentDisplayOrder == componentId 
                                              && componentLevel == data_label){
                                               
                                               var scenarioRuleInstanceIdReceived = scenarioInstanceDetailsViewList[i].Id;
                                               component.set("v.scenarioRuleInstanceIdReceived",scenarioRuleInstanceIdReceived);
                                               console.log('scenarioRuleInstanceIdReceived ::: '+scenarioRuleInstanceIdReceived);
                                               var componentDefinition =  scenarioInstanceDetailsViewList[i][common.getKey(component,"Component_Type_Master__r")][common.getKey(component,"Component_Definition__c")];
                                               component.set("v.componentDefinition",componentDefinition);
                                               console.log('componentDefinition ::: '+componentDefinition);
                                               component.navigateToRespectiveComponents();
                                               
                                           }
                                       }
                                       
                                       
                                   });
        
        $A.enqueueAction(scenarioAction); 
    },
    
    navigateToRespectiveComponents: function(component,event,helper){
        
        
        var scenarioRuleInstanceIdReceived2 = component.get("v.scenarioRuleInstanceIdReceived");
        var componentDefinitionReceived = component.get("v.componentDefinition");
        var scenarioId = component.get('v.scenarioId');
        var workSpaceId = component.get("v.workSpaceId");
        console.log('workspace Id ::::::: '+workSpaceId);
        console.log('scenarioRuleInstanceIdReceived2:::::: '+scenarioRuleInstanceIdReceived2);
        console.log('scenarioId:::::: '+scenarioId);
        console.log('componentDefinitionReceived :: '+componentDefinitionReceived);
        var navigateCmp='c:';
        if(component.get("v.namespace")){
            navigateCmp=component.get("v.namespace")+':';
        }

        let ruleComponent = componentDefinitionReceived;
        componentDefinitionReceived =  ( componentDefinitionReceived == 'BRAssignmentRule' || componentDefinitionReceived == 'BRAffilationRules' ) ? 'BRAssignmentRule': componentDefinitionReceived;
        navigateCmp=navigateCmp+componentDefinitionReceived; 
        
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : navigateCmp,
            componentAttributes: {
                scenarioId: scenarioId,
                scenarioRuleInstanceId: scenarioRuleInstanceIdReceived2,
                workSpaceId : workSpaceId,
                namespace:component.get("v.namespace"),
                summarizedViewFlag:component.get('v.summarizedViewFlag'),
                brModuleName : ruleComponent
            },
            isredirect : true
        });
        evt.fire(); 
    },
    
    
    
    update : function (component,event,helper) {
        console.log('refresh::');
        component.destroy();
        //$A.get('e.force:refreshView').fire();
        // body...
    },
    
    onBatchRun : function(component,event,helper){
        
        console.log('inside batch run');
        
        var scenarioId;
        var viewcanvasflag = component.get("v.viewcanvas");
        if(viewcanvasflag == 'Y'){
            scenarioId = component.get('v.record_Id'); 
        }
        else{
            scenarioId = component.get('v.scenarioId');
        }
        
        var action = component.get("c.callRun");
        action.setParams({
            scenarioId : scenarioId
        });
        action.setCallback(this,function(response)
       {
           var state = response.getState();
           if(state === 'SUCCESS' && response.getReturnValue != null)
           {
               // alert('Success');
               //  component.populateRow();
               //   var evt = $A.get("e.force:navigateToComponent");
               // evt.setParams({
               //     componentDef : "c:SalesIQDataTableController",
               //     isredirect : true
               //  });
               //  evt.fire();
               
               console.log('inside batch run success');
               
               var navEvt = $A.get("e.force:navigateToSObject");
               var workSpaceId = component.get("v.workSpaceId");
               console.log('workSpaceId :::: '+workSpaceId);
               navEvt.setParams({
                   "recordId": workSpaceId,
                   "isredirect" : true
               });
               navEvt.fire();
               component.destroy();
           }
       });
        $A.enqueueAction(action);
    },
    
    
    populateViewCanvasRow: function (component) {
        var startingVal_top = 10;
        var vertical_Diff = 140;
        
        console.log('inside populateViewCanvasRow');
        var scenarioCanvasInstanceList = component.get("v.scenarioViewObjList");
        var scenarioId = component.get('v.scenarioId');
        
        var action = component.get("c.getComponentDetails");
        action.setParams({
            scenarioCanvasInstanceList : scenarioCanvasInstanceList,
            scenarioId : scenarioId,
        });
        action.setCallback(this,function(response)
                           {
                               var state = response.getState();
                               if(state === 'SUCCESS' && response.getReturnValue != null)
                               {
                                   var componentMappingList = response.getReturnValue();
                                   console.log('componentMappingList ------------->>>>>');
                                   console.log(componentMappingList);
                                   component.set('v.componentMappingList',componentMappingList);
                                   var scenarioCanvasInstanceList = component.get("v.scenarioViewObjList");
                                   var componentMappingList = component.get("v.componentMappingList");
                                   
                                   // Array of components to create
                                   var newComponents = [];
                                   var dict = [];
                                   var statusicon = [];
                                   var twowaycommunication = [];
                                   for (var i =0;i<componentMappingList.length;i++) 
                                   {
                                       dict.push({
                                           // key:   scenarioCanvasInstanceList[i].Component_Type__c,
                                           icon:  componentMappingList[i][common.getKey(component,"DisplayIcon__c")],
                                           iconCls:componentMappingList[i][common.getKey(component,"DisplayIconClass__c")]
                                       });
                                   }
                                   
                                   // statusicon.push({
                                   //      icon:  "utility:check",
                                   // });
                                   
                                   for (var i =0;i<scenarioCanvasInstanceList.length;i++) 
                                   {
                                       
                                       var level;
                                       var parentDisplayOrder;
                                       var displayOrder;
                                       
                                       if(component.get("v.summarizedViewFlag") == true)
                                       {
                                            level = scenarioCanvasInstanceList[i][common.getKey(component,"Component_Level_Summarize__c")] ;
                                            parentDisplayOrder = scenarioCanvasInstanceList[i][common.getKey(component,"Parent_Display_Order_Summarize__c")];
                                            displayOrder = scenarioCanvasInstanceList[i][common.getKey(component,"Component_Display_Order_Summarize__c")];
                                       }

                                       else
                                       {
                                            level = scenarioCanvasInstanceList[i][common.getKey(component,"Component_Level__c")] ;
                                            parentDisplayOrder = scenarioCanvasInstanceList[i][common.getKey(component,"Parent_Display_Order__c")];
                                            displayOrder = scenarioCanvasInstanceList[i][common.getKey(component,"Component_Display_Order__c")];
                                       }
                                       var status = "In Progress";
                                       //console.log('Level '+level );
                                       var topval = startingVal_top+(vertical_Diff*(level-1));
                                       var leftval = 35 + (141 * (displayOrder-1));

                                       twowaycommunication.push({
                                            twoway: scenarioCanvasInstanceList[i][common.getKey(component,"Is_Editable__c")]
                                       });
                                       
                                       
                                       statusicon.push({
                                           iconReceived:  scenarioCanvasInstanceList[i][common.getKey(component,"Status__c")],
                                       });
                                       
                                       
                                       var iconNameMap = {};
                                       
                                       if(iconNameMap['In Progress'] == undefined)
                                       {
                                           iconNameMap['In Progress'] = {};
                                           iconNameMap['In Progress']['iconName'] = "utility:"+"threedots";     
                                           iconNameMap['In Progress']['iconClass'] = "status queue" ;
                                           iconNameMap['In Progress']['title'] = "In Progress" ;
                                           iconNameMap['Success'] = {};
                                           iconNameMap['Success']['iconName'] = "utility:"+"check";     
                                           iconNameMap['Success']['iconClass'] = "status success" ;
                                           iconNameMap['Success']['title'] = "Success" ;
                                           iconNameMap['Pending'] = {};
                                           iconNameMap['Pending']['iconName'] = "action:"+"defer";     
                                           iconNameMap['Pending']['iconClass'] = "status pending" ;
                                           iconNameMap['Pending']['title'] = "Pending" ;
                                           iconNameMap['Error Occured'] = {};
                                           iconNameMap['Error Occured']['iconName'] = "utility:"+"error";     
                                           iconNameMap['Error Occured']['iconClass'] = "status error" ; 
                                           iconNameMap['Error Occured']['title'] = "Error" ;     
                                       }
                                       
                                       console.log('+++++++++++++ Map Values');
                                       console.log(iconNameMap);
                                       
                                       console.log('statusicon ::::');
                                       console.log(statusicon);

                                       console.log('blur val :: '+scenarioCanvasInstanceList[i][common.getKey(component,"Display_Icon_Style__c")]);
                                       var myClass = '';
                                       if(scenarioCanvasInstanceList[i][common.getKey(component,"Display_Icon_Style__c")] != undefined)
                                       {
                                            if(scenarioCanvasInstanceList[i][common.getKey(component,"Display_Icon_Style__c")] == 'blur')
                                            {
                                                myClass = dict[i].iconCls + ' blurring';
                                            }
                                            else
                                            {
                                                myClass = dict[i].iconCls + ' boxing';
                                            }
                                       }
                                       else
                                       {
                                            myClass = dict[i].iconCls;
                                       }
                                       
                                       
                                       newComponents.push(["aura:html", {
                                           "tag": "div",
                                           "HTMLAttributes": {
                                               "class": myClass,
                                              // "id" : "flowchartWindow_"+level+"_"+displayOrder,
                                            "id" : component.get("v.summarizedViewFlag") == true?"flowchartWindowsumm_"+level+"_"+displayOrder:"flowchartWindow_"+level+"_"+displayOrder,
                                               "style": "top : "+topval+"px; left: "+leftval+"px;",
                                               "onclick":component.getReference("c.clickButton"),
                                               "title":scenarioCanvasInstanceList[i][common.getKey(component,"ComponentTypeLabel__c")],
                                               "data-label":level
                                               
                                           }
                                           
                                       }]);
                                       
                                       newComponents.push(["aura:html", {
                                           "tag": "div",
                                           "HTMLAttributes": {
                                               "class": "info"
                                           }
                                           
                                       }]);
                                       
                                       
                                       newComponents.push(["lightning:icon",
                                                           {
                                                               "iconName": "utility:"+dict[i].icon,
                                                               "size": "small",
                                                               "class": "comp_icon"
                                                           }]);
                                       
                                       newComponents.push(["aura:html", {
                                           "tag": "div",
                                           "body": scenarioCanvasInstanceList[i][common.getKey(component,"ComponentTypeLabel__c")],
                                           "HTMLAttributes": {
                                               "class": "text" 
                                           }
                                           
                                       }]);
                                       
                                       newComponents.push(["lightning:icon",
                                                           {
                                                               "iconName": iconNameMap[statusicon[i].iconReceived]['iconName'],         
                                                               "size": "x-small",         
                                                               "class": iconNameMap[statusicon[i].iconReceived]['iconClass'],
                                                               "title": iconNameMap[statusicon[i].iconReceived]['title']
                                                           }]);
                                       
                                   }
                                   $A.createComponents(
                                       newComponents,
                                       function (components, status, errorMessage) {
                                           if (status === "SUCCESS") 
                                           {
                                               console.log(status);
                                               console.log('inside create component');
                                               var pageBody = component.get("v.body");
                                               for (var i = 0; i < components.length; i += 5) 
                                               {
                                                   var outerDiv = components[i];
                                                   var infoDiv = components[i+1]
                                                   var icon = components[i + 2];
                                                   var innerDiv = components[i+3];
                                                   var iconDiv = components[i+4];
                                                   var infoBody = infoDiv.get("v.body");
                                                   infoBody.push(icon);
                                                   infoBody.push(innerDiv);
                                                   infoBody.push(iconDiv);
                                                   infoDiv.set("v.body",infoBody)
                                                   var tdBody = outerDiv.get("v.body");
                                                   tdBody.push(infoDiv);
                                                   
                                                   outerDiv.set("v.body", tdBody);
                                                   
                                                   pageBody.push(outerDiv);
                                               }
                                               
                                               component.set("v.body", pageBody);
                                               var scenarioViewObjList = component.get("v.scenarioViewObjList");
                                               console.log('scenarioCanvasInstanceList before calling afetrviewrendering');
                                               console.log(scenarioViewObjList);
                                               component.set("v.scenarioId",scenarioId);
                                               component.afterViewRendering();
                                              // component.hideSpinner();
                                           }
                                           else // Report any error
                                           {
                                               //   //  this.displayToast("Error", "Failed to create list components.");
                                               console.log('error');
                                               component.hideSpinner();
                                           }
                                       }
                                   )
                                   
                               }
                               else
                               {
			       	component.updateCountryAttributes();
                               }
                           }); 
        
        $A.enqueueAction(action);
        
        
    },
    
    afterViewRendering: function(component, helper) {
        var line_stroke_width = 1;
        var line_stroke = '#D3D3D3';
        var marker_fill = '#D3D3D3';
        var markerWidth = 20;
        var markerHeight = 20;
        //var viewBox = '0 0 13 13';
        var componentHeight = 50;
        var componentWidth = 50;
        var componentdiff = 10;
        var componentTop = 47;
        var stroke_opacity = 0.9;
        var fill_opacity = 1;
        var startingVal_top = 10;
        var vertical_Diff = 140;
        
        console.log('ínside after render');
        var scenarioId = component.get('v.scenarioId');
        //  var canvasList = component.get("v.canvasList");
        var scenarioViewObjList = component.get("v.scenarioViewObjList");
        console.log('scenarioViewObjList::::::::::::::::::::::::::::::');
        console.log(scenarioViewObjList);
        var line = "";
        var marker = "";
        var svg_height = 0;
        var svg_width = 0;
        for (var i =0;i<scenarioViewObjList.length;i++){

            var level;
            var parentLevel;
            var parentDisplayOrder;
            var displayOrder;

            if(component.get('v.summarizedViewFlag') == true){
              level = scenarioViewObjList[i][common.getKey(component,"Component_Level_Summarize__c")] ;  
            }
            else{
              level = scenarioViewObjList[i][common.getKey(component,"Component_Level__c")] ;
            }
            
            var displayIconStyle = scenarioViewObjList[i][common.getKey(component,"Display_Icon_Style__c")] ;
            //console.log('level :::: '+level);
            if(component.get('v.summarizedViewFlag') == true){
                parentLevel = scenarioViewObjList[i][common.getKey(component,"Parent_Level_Summarize__c")];
            }
            else{
                parentLevel = scenarioViewObjList[i][common.getKey(component,"Parent_Level__c")];   
            } 
            var parentLevel_array = parentLevel.split(',');
            //console.log('parentLevel_array :::: '+parentLevel_array);
            if(component.get('v.summarizedViewFlag') == true){
                 parentDisplayOrder = scenarioViewObjList[i][common.getKey(component,"Parent_Display_Order_Summarize__c")];
            }
            else{
                 parentDisplayOrder = scenarioViewObjList[i][common.getKey(component,"Parent_Display_Order__c")];
            }
           
            //console.log('parent DO :::: '+parentDisplayOrder);
            var parentDisplayOrder_array = parentDisplayOrder.split(',');
            //console.log('parentDisplayOrder_array_______ '+parentDisplayOrder_array);
            if(component.get('v.summarizedViewFlag') == true){
                displayOrder = scenarioViewObjList[i][common.getKey(component,"Component_Display_Order_Summarize__c")];
            }
            else{
                displayOrder = scenarioViewObjList[i][common.getKey(component,"Component_Display_Order__c")];
            }
            
            var twowayarrows = scenarioViewObjList[i][common.getKey(component,"Is_Editable__c")];
            //console.log('Own DO :::: '+displayOrder);
            // var status = "In Progress";
            //var startingVal_top = 99;
            var topval = startingVal_top+(vertical_Diff*(level-1));
            if(svg_height<topval) {
                svg_height = topval+componentHeight+componentdiff;
                console.log('inside svg height compared to topval');
            }
            var leftval = 35 + (141 * (displayOrder-1));
            if(svg_width<leftval){
                svg_width = leftval+componentWidth;
            }
            var emval = 1;
            var length = parentDisplayOrder_array.length;
            //console.log('length::::::::::::::: '+length);
            var XAxis1, XAxis2, YAxis1, YAxis2;
            for(var k= 0; k<length;k++ ){
                //console.log('for');
                if(parentLevel_array[k] != 0) {
                    //console.log('par');
                    var arrowValue = level+''+displayOrder+''+k;
                    if(level==parentLevel_array[k]  ){
                        
                        if(parentDisplayOrder_array[k]<displayOrder){
                            console.log('afterViewRendering left to right arrow on same level');
                            XAxis1 = (35 + (141 * (parentDisplayOrder_array[k]-1)))+componentWidth+componentdiff;
                            XAxis2 = leftval-(componentdiff);
                            YAxis1 = topval+(componentHeight/2);
                            YAxis2 = YAxis1; 
                            //var refx = (XAxis2-XAxis1)/2;
                            var refx = 3;
                            if(twowayarrows){
                                marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refx+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";  
                            }
                            line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis2+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\"></line>"
                        } else {
                            console.log('afterViewRendering right to left arrow on same level');                            
                            XAxis1 = (35 + (141 * (parentDisplayOrder_array[k]-1)))-componentdiff;
                            XAxis2 = leftval+componentWidth+componentdiff;
                            YAxis1 = topval+(componentHeight/2);
                            YAxis2 = YAxis1; 
                            //var refx = (XAxis1-XAxis2)/2;
                            var refx = 3;
                            if(twowayarrows){
                                marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refx+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";  
                            }
                            line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis2+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\"></line>"
                            
                        }
                    }
                    else if(level>parentLevel_array[k]) {
                        if(parentDisplayOrder_array[k] ==displayOrder ){
                            console.log('With EQUAL / afterViewRendering top to bottom arrow');
                            XAxis1 = (35 + (141 * (parentDisplayOrder_array[k]-1)))+(componentWidth/2);
                            XAxis2 = leftval-(componentdiff);
                            YAxis2 = topval - componentdiff;
                            var YAxis_bottom2 = topval+(componentHeight/2);
                            var XAxis_bottom1 = XAxis1+(line_stroke_width/2);
                            YAxis1 = (startingVal_top+(141*(parentLevel_array[k]-1)))+componentHeight+componentTop; 
                            //var refY = (YAxis2-YAxis1)/2;
                            var refY = 3;
                            if(twowayarrows){
                                marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refY+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";              
                            }
                            line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis1+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\" ></line>"
                        
                        }
                        else if(parentDisplayOrder_array[k] < displayOrder){
                            console.log(parentDisplayOrder_array[k]+"parentDisplayOrder_array[k]---"+displayOrder+"displayOrder")
                            console.log('parentDisplayOrder smaller to displayOrder afterViewRendering top to bottom arrow ');
                            XAxis1 = (35 + (141 * (parentDisplayOrder_array[k]-1)))+componentWidth+componentdiff;
                            YAxis1 = (startingVal_top+(141*(parentLevel_array[k]-1)))+(componentHeight/2); 
                            
                            XAxis2 = 35 + (141 * (displayOrder-1))+(line_stroke_width/2)+(componentWidth/2);
                            YAxis2 = topval-(componentdiff);
                            
                            var XAxis_bottom2 = XAxis2-(line_stroke_width/2);
                            var YAxis_bottom2 = YAxis1-(line_stroke_width/2);
                            //var refx = (YAxis2-YAxis1)/2;
                            var refx = 3;
                            if(twowayarrows){
                                marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refx+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";                              
                            }
                            line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis2+"\" y2=\""+YAxis1+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+"></line>"                           
                            line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis_bottom2+"\" y1=\""+YAxis_bottom2+"\" x2=\""+XAxis_bottom2+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\" ></line>"                            
                        }
                        else {
                            console.log('afterViewRendering top to bottom arrow ');
                            XAxis1 = (35 + (141 * (parentDisplayOrder_array[k]-1)))+(componentWidth/2);
                            XAxis2 = leftval-(componentdiff);
                            YAxis2 = topval+(componentHeight/2)+(line_stroke_width/2);
                            var YAxis_bottom2 = topval+(componentHeight/2);
                            var XAxis_bottom1 = XAxis1+(line_stroke_width/2);
                            YAxis1 = (startingVal_top+(141*(parentLevel_array[k]-1)))+componentHeight+componentTop; 
                            //var refx = (XAxis2-XAxis1)/2;
                            var refx = 3;
                            if(twowayarrows){
                                marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refx+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";                              
                            }
                            line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis1+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" ></line>"
                            
                            line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis_bottom1+"\" y1=\""+YAxis_bottom2+"\" x2=\""+XAxis2+"\" y2=\""+YAxis_bottom2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\"></line>"
                        }
                    }
                        else if(level<parentLevel_array[k]) {
                            if(parentDisplayOrder_array[k] == displayOrder ){
                                console.log('With EQUAL / afterViewRendering bottom to top arrow'); 
                                XAxis1 = (35 + (141 * (parentDisplayOrder_array[k]-1)))+(componentWidth/2);
                                XAxis2 = leftval+componentWidth+componentdiff;
                                YAxis2 = topval+componentTop+componentHeight;
                                //var YAxis_bottom2 = YAxis2+(line_stroke_width/2);
                                YAxis1 = (startingVal_top+(141*(parentLevel_array[k]-1)))+componentdiff; 
                                var refx = 3;
                                if(twowayarrows){
                                    marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refx+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";  
                                }
                                line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis1+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\"></line>"
                                
                            } else if(parentDisplayOrder_array[k] < displayOrder){
                                console.log('parentDisplayOrder smaller to displayOrder / afterViewRendering bottom to top arrow'); 
                                XAxis1 = leftval-componentdiff;
                                XAxis2 = (35 + (141 * (parentDisplayOrder_array[k]-1)))+(componentWidth/2);
                       
                                YAxis1 = topval+(componentHeight/2);
                                YAxis2 = (startingVal_top+(141*(parentLevel_array[k]-1)))-componentdiff; 
                                
                                var YAxis_bottom1 = YAxis1-(line_stroke_width/2);
                                var refx = 3;
                                if(twowayarrows){
                                    console.log(twowayarrows+"twowayarrows")
                                    marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refx+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";  
                                }
                                line = line + "<line stroke-opacity='"+stroke_opacity+"'  x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis2+"\" y2=\""+YAxis1+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+"></line>"                            
                                line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis2+"\" y1=\""+YAxis_bottom1+"\" x2=\""+XAxis2+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\"></line>"
                                
                                
                                /*console.log(parentDisplayOrder_array[k]+"parentDisplayOrder_array[k]---"+displayOrder+"displayOrder")
                                console.log('parentDisplayOrder smaller to displayOrder afterViewRendering top to bottom arrow ');
                                XAxis1 = (35 + (141 * (parentDisplayOrder_array[k]-1)))+componentWidth+componentdiff;
                                YAxis1 = (startingVal_top+(141*(parentLevel_array[k]-1)))+(componentHeight/2); 
                                
                                XAxis2 = 35 + (141 * (displayOrder-1))+(componentdiff/2)+(componentWidth/2);
                                YAxis2 = topval-(componentdiff);
                                
                                var XAxis_bottom2 = XAxis2-(componentdiff/2);
                                var YAxis_bottom2 = YAxis1-(line_stroke_width/2);
                                var refx = (YAxis2-YAxis1)/2;
                                if(twowayarrows){
                                    marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refx+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";                              
                                }
                                line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis2+"\" y2=\""+YAxis1+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+"></line>"
                               
                                line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis_bottom2+"\" y1=\""+YAxis_bottom2+"\" x2=\""+XAxis_bottom2+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\" ></line>"
                            */
                            }
                            else {
                                console.log('afterViewRendering bottom to top arrow'); 
                                XAxis1 = (35 + (141 * (parentDisplayOrder_array[k]-1)))+(componentWidth/2);
                                XAxis2 = leftval+componentWidth+componentdiff;
                                YAxis2 = topval+componentHeight-componentdiff;
                                var YAxis_bottom2 = YAxis2+(line_stroke_width/2);
                                YAxis1 = (startingVal_top+(141*(parentLevel_array[k]-1)))+componentdiff; 
                                var refx = 3;
                                if(twowayarrows){
                                    console.log(twowayarrows+"twowayarrows")
                                    marker = marker + "<marker fill="+marker_fill+" id=\"arrow"+arrowValue+"\" markerWidth="+markerWidth+" markerHeight="+markerHeight+" refx="+refx+" refy=\"3\" orient=\"auto\" markerUnits=\"strokeWidth\" viewBox=\"0 0 13 13\"  fill-opacity='"+fill_opacity+"' ><path style=\"stroke: none;\" d=\"M0,0 L0,6 L6,3\" /></marker>";  
                                }
                                line = line + "<line stroke-opacity='"+stroke_opacity+"' x1=\""+XAxis1+"\" y1=\""+YAxis1+"\" x2=\""+XAxis1+"\" y2=\""+YAxis2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+"></line>"
                                line = line + "<line stroke-opacity='"+stroke_opacity+"'  x1=\""+XAxis1+"\" y1=\""+YAxis_bottom2+"\" x2=\""+XAxis2+"\" y2=\""+YAxis_bottom2+"\" stroke="+line_stroke+" stroke-width="+line_stroke_width+" marker-end=\"url(#arrow"+arrowValue+")\"></line>"
                            }
                        }
                }
            }
        }
        if(component.get('v.summarizedViewFlag') == true){
           component.hideSpinner(); 
        }
        
        var svg = component.find("svg_content");
        svg.getElement().innerText = " <svg id='svg' width=\""+svg_width+"\" height=\""+svg_height+"\"><defs>"+marker+"</defs>"+line+"</svg>";
        var value = svg.getElement().innerText;
        value = value.replace("<![CDATA[", "").replace("]]>", "");
        svg.getElement().innerHTML = value;
        document.getElementById('canvas').style.height=svg_height+componentTop+'px';
        document.getElementById('canvas').style.width=svg_width+88+'px';
        document.getElementById('businessFrame').style.width=svg_width+88+'px';
        component.set("v.scenarioId",scenarioId);
        // alert(component.get("v.countryId"));
       //var flagComponent = component.find('flagComponent');
      // if(flagComponent != undefined){
      //  flagComponent.updateCountryAttributes(component.get("v.countryId"));
      //  }
        component.updateCountryAttributes();
        component.hideSpinner(); 
      
    }, 
    
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    
    onBackButtonClick : function(component,event,helper){
        
        // var evt = $A.get("e.force:navigateToComponent");
        //    evt.setParams({
        //        componentDef : "c:SalesIQDataTable",
        //        // componentAttributes: {
        //        //     scenarioId: scenarioId,
        //        //     scenarioRuleInstanceId: scenarioRuleInstanceIdReceived2
        //        // },
        //        isredirect : true
        //     });
        //     evt.fire();
        var navEvt = $A.get("e.force:navigateToSObject");
        var workSpaceId = component.get("v.workSpaceId");
        console.log('workSpaceId :::: '+workSpaceId);
        navEvt.setParams({
            "recordId": workSpaceId,
            "isredirect" : true
        });
        navEvt.fire();
        component.destroy();
    },

    onRunLogClick : function(component,event,helper)
    {
      var evt = $A.get("e.force:navigateToComponent");
     
    //  var scenarioRuleInstanceIdReceived2 = component.get("v.scenarioRuleInstanceIdReceived");
      var scenarioId = component.get('v.scenarioId');
      var workSpaceId = component.get("v.workSpaceId");

    //  console.log('scenarioRuleInstanceIdReceived2 ::: '+scenarioRuleInstanceIdReceived2);
      console.log('scenarioId ::: '+scenarioId);
      console.log('workSpaceId ::: '+workSpaceId);

        evt.setParams({
            componentDef : "c:CPGRunLog",
            componentAttributes: {
                scenarioId: scenarioId,
             //   scenarioRuleInstanceId: scenarioRuleInstanceIdReceived2,
                workSpaceId : workSpaceId
            },
            isredirect : true
        });
        evt.fire(); 
    },

    checkScenarioStatus:function(component,event,helper)
    {
            
            var actionScenarioStatus = component.get("c.getScenarioStatus");
            console.log(component.get("v.scenarioId"));
            actionScenarioStatus.setParams({ "scenarioId" : component.get("v.scenarioId")});
            actionScenarioStatus.setCallback(this, function(response) {
                var state = response.getState();
                if(state === 'SUCCESS'){
                    console.log('checkScenarioStatus');
                    console.log(response.getReturnValue());
                         var status = response.getReturnValue();
                         component.set('v.scenarioStatus',status);
                         console.log(component.get('v.scenarioStatus'));
                }else{
                      console.log("Failed with state:  "+ state);
                }
            });
            
            $A.enqueueAction(actionScenarioStatus);
    },

    openSettings : function(component,event,helper)
    {
        $A.createComponent(
            "c:SalesIQCallPlanScenarioSetting",
            {
                "aura:id": "callPlanSetting",
                "selectedTeamInstance" : component.get("v.selectedTeamInstance"),
                "scenarioId" : component.get("v.record_Id"),
                "moduleName" : "Canvas"
            },
            function(settingComponent, status, errorMessage)
            {                
                var targetCmp = component.find('callPlanScenarioSetting');
                var body = targetCmp.get("v.body");
                body.push(settingComponent);
                targetCmp.set("v.body", body);
            }
        );
    },

    loadCallPlan : function (component,event){
        console.log('------- load call plan');
        console.log(component.get("v.selectedTeamInstance"));

        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:SalesIQCallPlan",
            componentAttributes: {
                selectedTeamInstance : component.get("v.selectedTeamInstance") 
            }
        });
        evt.fire();
        component.destroy();


    },
    loadAlignment : function (component,event){
        console.log('loading alignment...');
        console.log(component.get("v.selectedTeamInstance"));

        var showScenarioBt = 'true';
        var ns = component.get("v.namespace"), url;
        if(ns!=''){
            url = '/apex/'+ns+'__'+'ScenarioAlignment?selectedTeam=' + component.get("v.selectedTeam")  + '&selectedCycle=' + component.get("v.selectedTeamInstance")  + '&showScenarioBt='+ showScenarioBt;
        }else{
            url = '/apex/'+'ScenarioAlignment?selectedTeam=' + component.get("v.selectedTeam")  + '&selectedCycle=' + component.get("v.selectedTeamInstance")  + '&showScenarioBt='+ showScenarioBt;
        }
        
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
        "url": url
        });
        urlEvent.fire();
        component.destroy();
        
    },

    loadBlockingRules : function (component,event){
        console.log('------- load product blocking rules list');
        console.log(component.get("v.selectedTeamInstance"));

        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:SalesIQAccountExclusion",
            
        });
        evt.fire();
        component.destroy();


    },

    openAffiliationComponent : function(component,event,helper){
        // var scenarioRuleInstanceIdReceived2 = component.get("v.scenarioRuleInstanceIdReceived");a1X1N00000E2z0pUAB
        // var componentDefinitionReceived = component.get("v.componentDefinition");
         var action1=component.get('c.getDetailsId');
               action1.setParams({
                scenarioId : component.get('v.scenarioId')
               });
               action1.setCallback(this, function(data)
               {
                var state = data.getState();
                if(state === 'SUCCESS'){
                   var scenarioRuleInstanceId = data.getReturnValue();
                    component.set('v.scenarioRuleInstanceId',scenarioRuleInstanceId);
                    var componentDefinitionReceived = 'BRAffilationRules';
                    var scenarioId = component.get('v.scenarioId');
                    var workSpaceId = component.get("v.workSpaceId");
                    var scenarioRuleInstanceIdReceived2 = component.get('v.scenarioRuleInstanceId');
                    console.log('workspace Id ::::::: '+workSpaceId);
                    console.log('scenarioRuleInstanceIdReceived2:::::: '+scenarioRuleInstanceIdReceived2);
                    console.log('scenarioId:::::: '+scenarioId);
                    console.log('componentDefinitionReceived :: '+componentDefinitionReceived);
                    var navigateCmp='c:';
                    if(component.get("v.namespace")){
                        navigateCmp=component.get("v.namespace")+':';
                    }
                    navigateCmp=navigateCmp+componentDefinitionReceived; 
                    var evt = $A.get("e.force:navigateToComponent");
                    evt.setParams({
                        componentDef : navigateCmp,
                        componentAttributes: {
                            scenarioId: scenarioId,
                            scenarioRuleInstanceId: scenarioRuleInstanceIdReceived2,
                            workSpaceId : workSpaceId,
                            namespace:component.get("v.namespace")
                        },
                        isredirect : true
                    });
                    evt.fire(); 
                }

                   else{
                    console.log('error '+state);
                }

                });

               $A.enqueueAction(action1);

      //  var scenarioRuleInstanceIdReceived2 = 'a1X1N00000E2z0pUAB';
       

    },

    onaffiliationView : function(component,event,helper){

            
             var action1=component.get('c.getViewAffiliationId');
               action1.setParams({
                scenarioId : component.get('v.scenarioId')
               });
               action1.setCallback(this, function(data)
               {
                var state = data.getState();
                if(state === 'SUCCESS'){
                   var viewAffiliationId = data.getReturnValue();
                    // for(var i=0;i<viewAffiliationDetailsList.length;i++){
                        component.set('v.scenarioRuleInstanceId',viewAffiliationId); 
                       // componentDefinitionReceived = CPGPreviewFullData;
                   // }
                    
                    var componentDefinitionReceived = 'CPGPreviewFullData';
                    var scenarioId = component.get('v.scenarioId');
                    var workSpaceId = component.get("v.workSpaceId");
                    var scenarioRuleInstanceIdReceived2 = component.get('v.scenarioRuleInstanceId');
                    console.log('workspace Id ::::::: '+workSpaceId);
                    console.log('scenarioRuleInstanceIdReceived2:::::: '+scenarioRuleInstanceIdReceived2);
                    console.log('scenarioId:::::: '+scenarioId);
                    console.log('componentDefinitionReceived :: '+componentDefinitionReceived);
                    var navigateCmp='c:';
                    if(component.get("v.namespace")){
                        navigateCmp=component.get("v.namespace")+':';
                    }
                    navigateCmp=navigateCmp+componentDefinitionReceived; 
                    var evt = $A.get("e.force:navigateToComponent");
                    evt.setParams({
                        componentDef : navigateCmp,
                        componentAttributes: {
                            scenarioId: scenarioId,
                            scenarioRuleInstanceId: scenarioRuleInstanceIdReceived2,
                            workSpaceId : workSpaceId,
                            namespace:component.get("v.namespace")
                        },
                        isredirect : true
                    });
                    evt.fire(); 
                }

                   else{
                    console.log('error '+state);
                }

                });

               $A.enqueueAction(action1);
    },
    

     openAffiliationNetwork : function(component,event,helper){

            
             var action1=component.get('c.getAffiliationNetworkId');
               action1.setParams({
                scenarioId : component.get('v.scenarioId')
               });
               action1.setCallback(this, function(data)
               {
                var state = data.getState();
                if(state === 'SUCCESS'){
                   var affiliationNetworkId = data.getReturnValue();
                    // for(var i=0;i<viewAffiliationDetailsList.length;i++){
                        component.set('v.scenarioRuleInstanceId',affiliationNetworkId); 
                       // componentDefinitionReceived = CPGPreviewFullData;
                   // }
                    
                    var componentDefinitionReceived = 'CPGPreviewFullData';
                    var scenarioId = component.get('v.scenarioId');
                    var workSpaceId = component.get("v.workSpaceId");
                    var scenarioRuleInstanceIdReceived2 = component.get('v.scenarioRuleInstanceId');
                    console.log('workspace Id ::::::: '+workSpaceId);
                    console.log('scenarioRuleInstanceIdReceived2:::::: '+scenarioRuleInstanceIdReceived2);
                    console.log('scenarioId:::::: '+scenarioId);
                    console.log('componentDefinitionReceived :: '+componentDefinitionReceived);
                    var navigateCmp='c:';
                    if(component.get("v.namespace")){
                        navigateCmp=component.get("v.namespace")+':';
                    }
                    navigateCmp=navigateCmp+componentDefinitionReceived; 
                    var evt = $A.get("e.force:navigateToComponent");
                    evt.setParams({
                        componentDef : navigateCmp,
                        componentAttributes: {
                            scenarioId: scenarioId,
                            scenarioRuleInstanceId: scenarioRuleInstanceIdReceived2,
                            workSpaceId : workSpaceId,
                            namespace:component.get("v.namespace")
                        },
                        isredirect : true
                    });
                    evt.fire(); 
                }

                   else{
                    console.log('error '+state);
                }

                });

               $A.enqueueAction(action1);
    },

    runFullPromote:function(component,event,helper)
    {
        var scenarioId;
        var viewcanvasflag = component.get("v.viewcanvas");
        if(viewcanvasflag == 'Y'){
            scenarioId = component.get('v.record_Id'); 
        }
        else{
            scenarioId = component.get('v.scenarioId');
        }
        
        var action = component.get("c.runFullandPromote");
        action.setParams({
            scenarioId : scenarioId
        });
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {   
               console.log('inside batch run success');
               
               var navEvt = $A.get("e.force:navigateToSObject");
               var workSpaceId = component.get("v.workSpaceId");
               console.log('workSpaceId :::: '+workSpaceId);
               navEvt.setParams({
                   "recordId": workSpaceId,
                   "isredirect" : true
               });
               navEvt.fire();
               component.destroy();
            }    
        });
        $A.enqueueAction(action);
    },

    runDelta:function(component,event,helper)
    {
        var scenarioId;
        var viewcanvasflag = component.get("v.viewcanvas");
        if(viewcanvasflag == 'Y'){
            scenarioId = component.get('v.record_Id'); 
        }
        else{
            scenarioId = component.get('v.scenarioId');
        }
        
        var action = component.get("c.callDelta");
        action.setParams({
            scenarioId : scenarioId
        });
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {   
               console.log('inside batch run success');
               
               var navEvt = $A.get("e.force:navigateToSObject");
               var workSpaceId = component.get("v.workSpaceId");
               console.log('workSpaceId :::: '+workSpaceId);
               navEvt.setParams({
                   "recordId": workSpaceId,
                   "isredirect" : true
               });
               navEvt.fire();
               component.destroy();
            }    
        });
        $A.enqueueAction(action);
    },

    confirmRunFullPromote : function(component,event,helper){
        var scenarioId;
        var viewcanvasflag = component.get("v.viewcanvas");
        if(viewcanvasflag == 'Y'){
            scenarioId = component.get('v.record_Id'); 
        }
        else{
            scenarioId = component.get('v.scenarioId');
        }

        var action = component.get("c.isChangeRequestPending");
        action.setParams({
            scenarioId : scenarioId,
            isDelta : false
        });
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue() != null){   
                var reponseMap = response.getReturnValue();
                if(reponseMap['isCRPending'] == 'true'){
                    var pendingToast = $A.get("e.force:showToast");
                    pendingToast.setParams({
                        "type": "Error",
                        "message": reponseMap['crPendingMessage'],
                        "duration" : 8000
                    });
                    pendingToast.fire();
                }
                else{
                    component.set('v.promoteConfirmationMessage', reponseMap['promoteConfirmationMessage'] );
                    component.set("v.isDeltaPromoteCalled", false);
                    component.set("v.isFullPromoteCalled", true);
                    if(reponseMap['isProductBlock'] == 'false'){
                        helper.openConfirmationModal(component,event,helper);
                    }else{
                        component.runFullPromote();
                    }
                }
            }     
        });
        $A.enqueueAction(action);
    },

    runDeltaPromote:function(component,event,helper){
        var scenarioId;
        var viewcanvasflag = component.get("v.viewcanvas");
        if(viewcanvasflag == 'Y'){
            scenarioId = component.get('v.record_Id'); 
        }
        else{
            scenarioId = component.get('v.scenarioId');
        }

        var action = component.get("c.runDeltaAndPromote");
        action.setParams({
            scenarioId : scenarioId
        });
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {   
               console.log('inside batch run success');
               
               var navEvt = $A.get("e.force:navigateToSObject");
               var workSpaceId = component.get("v.workSpaceId");
               console.log('workSpaceId :::: '+workSpaceId);
               navEvt.setParams({
                   "recordId": workSpaceId,
                   "isredirect" : true
               });
               navEvt.fire();
               component.destroy();
            }    
        });
        $A.enqueueAction(action);
    },

    confirmRunDeltaPromote:function(component,event,helper)
    {
        var scenarioId;
        var viewcanvasflag = component.get("v.viewcanvas");
        if(viewcanvasflag == 'Y'){
            scenarioId = component.get('v.record_Id'); 
        }
        else{
            scenarioId = component.get('v.scenarioId');
        }

        var action = component.get("c.isChangeRequestPending");
        action.setParams({
            scenarioId : scenarioId,
            isDelta : true
        });

        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue() != null){   
                var reponseMap = response.getReturnValue();
                if(reponseMap['isCRPending'] == 'true'){
                    var pendingToast = $A.get("e.force:showToast");
                    pendingToast.setParams({
                        "type": "Error",
                        "message": reponseMap['crPendingMessage'],
                        "duration": 8000
                    });
                    pendingToast.fire();
                }
                else{
                    component.set('v.promoteConfirmationMessage', reponseMap['promoteConfirmationMessage'] );
                    component.set("v.isDeltaPromoteCalled", true);
                    component.set("v.isFullPromoteCalled", false);
                    if( reponseMap['isProductBlock'] == 'false'){
                        helper.openConfirmationModal(component,event,helper);
                    }else{
                        component.runDeltaPromote();
                    }
                }
            }   
        });
        $A.enqueueAction(action);
    },

    toggleTemplateView : function(component,event,helper){

        if(component.get('v.summarizedViewFlag') == false){
           component.set('v.summarizedViewFlag',true); 
        }

       else{
           component.set('v.summarizedViewFlag',false); 
        }
        component.showSpinner();
        
        component.set('v.body','');
        if(component.get("v.viewcanvas") == "Y" && component.get('v.summarizedViewFlag') == true){

            var action = component.get("c.getScenarioViewInstanceSummarized");
            action.setParams({
                scenarioId : component.get('v.scenarioId'),
            });
            action.setCallback(this,function(response)
            {
               var state = response.getState();
               
               if(state === 'SUCCESS' && response.getReturnValue != null)
               {
                   var scenarioViewObjList = response.getReturnValue();
                   if(scenarioViewObjList.length > 0){
                        component.set("v.scenarioViewObjList",scenarioViewObjList);
                        component.populateViewCanvasRow(); 
                   }
                    
                   else{
                        component.set('v.summarizedViewFlag',false);
                        component.createInstance();
                   }
                  // component.hideSpinner();
                }
            }); 
            
            $A.enqueueAction(action);
        }

        else{
            component.createInstance();
        }
      //  component.hideSpinner();
    },

    advMouseOver : function(component,event,helper){
         console.log("advMouseOver called");
       var advButton = document.getElementById("advButton");
       advButton.classList.remove('slds-button_neutral'); 
       advButton.classList.add('slds-button_brand');
       advButton.classList.remove('background-color:white;');
       //advButton.classList.add('background-color:blue;');
       // var advIcon = component.find("advIcon").get("v.class");
        //$A.util.removeClass(advIcon,'macroIcons');
        //$A.util.addClass(advIcon,'macroIconsmouseover');
        //advIcon = macroIconsmouseover;
         var advIcon = document.getElementById("advIcon");
         advButton.classList.remove('macroIcons'); 
         advButton.classList.add('macroIconsmouseover');
       // advIcon.class = "macroIconsmouseover";
    },

    basicMouseOver : function(component,event,helper){
       console.log("basicMouseOver called");
       var advButton = document.getElementById("basicButton");
       advButton.classList.remove('slds-button_neutral'); 
       advButton.classList.add('slds-button_brand');
       advButton.classList.remove('background-color:white;');
       var advIcon = document.getElementById("basicIcon");
       advButton.classList.remove('campaignIcons'); 
       advButton.classList.add('campaignIconmouseover');
       // advIcon.class = "macroIconsmouseover";
    },

    basicMouseleave :function(component,event,helper){
       console.log("basicMouseleave called");
       var advButton = document.getElementById("basicButton");
       advButton.classList.add('slds-button_neutral'); 
       advButton.classList.remove('slds-button_brand');
       advButton.classList.add('background-color:white;');
       var advIcon = document.getElementById("basicIcon");
       advButton.classList.remove('campaignIconmouseover'); 
       advButton.classList.add('campaignIcons');
       // advIcon.class = "macroIconsmouseover";
    },

    advMouseleave :function(component,event,helper){
       console.log("advMouseleave called");
       var advButton = document.getElementById("advButton");
       advButton.classList.add('slds-button_neutral'); 
       advButton.classList.remove('slds-button_brand');
       advButton.classList.add('background-color:white;');
       var advIcon = document.getElementById("advIcon");
       advButton.classList.remove('macroIconsmouseover'); 
       advButton.classList.add('macroIcons');
       // advIcon.class = "macroIconsmouseover";
    },

    openConfirmationModal : function(component,event,helper){
        helper.openConfirmationModal(component,event,helper);
    },

    closeConfirmationModal : function(component,event,helper){
        helper.closeConfirmationModal(component,event,helper);
    },


     viewAlignmentRules :function(component, event, helper){
       console.log("viewAlignmentRules called");
        var alignRules = component.find("dropdownAlignment");
        console.log(alignRules);
       //var alignRules = document.getElementById(""); 
       //$A.util.toggleClass(alignRules, "slds-show");
       $A.util.toggleClass(alignRules, "slds-is-open");
    },
    
    updateCountryAttributes : function(component,event,helper){
        //GET ORG NAMESPACE ON INIT
        var namespace = ( component.getType().split(':')[0] == 'c' ) ? '' : component.getType().split(':')[0]+'__';
        var action = component.get("c.getCountryAttributes");
        
        action.setParams({
            countryId : component.get("v.countryId")
        });

        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){   
                console.log(response);
                var attrs = response.getReturnValue().split(',');
                var countryName = attrs[0];
                component.set("v.countryName",countryName);
                component.set("v.countryFlag",attrs[1]);
                var customCountrylabel = attrs[3];
                countryName = countryName.split(' ').join('_');
                var isCustomValue = ( attrs[2] == 'true' ) ? true : false;
                var label;
                if(isCustomValue){
                    if(customCountrylabel != undefined || customCountrylabel != null || customCountrylabel != ''){
                        label =  "$Label.c." + customCountrylabel;
                    }
                }else{
                    if(namespace != ''){
                        label =  "$Label."+namespace.slice(0,-2)+"."+countryName;
                    }else{
                        label =  "$Label.c." + countryName;
                    }
                }
                component.set('v.labelAttr',$A.getReference(label));
                if(component.get('v.labelAttr') == ""){
                    $A.get('e.force:refreshView').fire();
                }else{
                    var c = component.get("v.labelAttr");
                    component.set("v.countryName",c);
                }
            }
        });
        $A.enqueueAction(action);
    }
    
    
})