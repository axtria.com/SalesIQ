({  	
    openConfirmationModal : function(component,event,helper){
        var confirmPromoteModal = component.find('confirmPromoteModal');
        $A.util.removeClass(confirmPromoteModal, 'slds-hide');
    },

    closeConfirmationModal : function(component,event,helper){
        var confirmPromoteModal = component.find('confirmPromoteModal');
        $A.util.addClass(confirmPromoteModal, 'slds-hide');
        component.set("v.isDeltaPromoteCalled", false);
        component.set("v.isFullPromoteCalled", false);
    }
})