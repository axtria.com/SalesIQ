({
	helperMethod : function(component,event,data,index) 
	{
		var Needle, arc, arcEndRad, arcStartRad, barWidth, chart, chartInset, degToRad, el, endPadRad, height, i, margin, needle, numSections, padRad, percToDeg, percToRad, radius, ref, sectionIndx, sectionPerc, startPadRad, svg, totalPercent, width,percent,startValue,endValue,endPercent;
		if(data.originalValue == 0 && data.proposedValue == 0)
		{
			percent = 0;
		
			startValue = 0;
			endValue = 100;
			endPercent = 2 + data.thresholdMax;
			document.getElementById('changeText'+index).textContent = (data.proposedValue-data.originalValue).toFixed(0);
			document.getElementById('changeText'+index).title = (data.proposedValue-data.originalValue).toFixed(0);
		}
		else
		{
			if(data.originalValue == 0)
			{
				endPercent = Math.abs((data.proposedValue)) > data.thresholdMax? Math.abs((data.proposedValue)):data.thresholdMax + 2;
				percent = 0.5 + ((data.proposedValue)/100)*50/endPercent;
				document.getElementById('changeText'+index).textContent = (data.proposedValue-data.originalValue).toFixed(0);
				document.getElementById('changeText'+index).title = (data.proposedValue-data.originalValue).toFixed(0);
				startValue = Math.abs((data.proposedValue)) > data.thresholdMax?0 - Math.abs(data.proposedValue):0-endPercent;
				endValue = Math.abs((data.proposedValue)) > data.thresholdMax?Math.abs(data.proposedValue):endPercent;
			}
			else
			{
				endPercent = Math.max(2 + data.thresholdMax,Math.abs(((data.proposedValue - data.originalValue)/data.originalValue))*100);
				percent = 0.5 + ((data.proposedValue - data.originalValue)/data.originalValue)*50/endPercent;	
				document.getElementById('changeText'+index).textContent =  (data.proposedValue-data.originalValue).toFixed(0) + ' ('+(((data.proposedValue - data.originalValue)/data.originalValue)*100).toFixed(1) + '%)';
				document.getElementById('changeText'+index).title =  (data.proposedValue-data.originalValue).toFixed(0) + ' ('+(((data.proposedValue - data.originalValue)/data.originalValue)*100).toFixed(1) + '%)';
				startValue = data.originalValue - endPercent*(data.originalValue)/100;
				endValue = data.originalValue + endPercent*(data.originalValue)/100;
			}
		}
		var prevPercent = 0.5;
		var prevPercentList = component.get("v.percentList");
		// if(prevPercentList)
		if(prevPercentList[index])
		{
			prevPercent = prevPercentList[index];
			prevPercentList.splice(index, 1, percent);
		}
		else
		{
			prevPercentList.splice(index, 0, percent);
		}
		
		// var prevPercent = component.get("v.percentList[index]");
		
		if(index == 0)
		{
			console.log('---> prevPercent' + prevPercent);
			console.log('percent---> ' + percent);
		}
		component.set("v.percentList",prevPercentList);
		
		var secPercList = [];
		for(var i = 1;i<=5;i++)
		{
			if(i==1 || i==5)
				secPercList.push((endPercent - data.thresholdMax)/100);
			else if(i==2 || i==4)
				secPercList.push((data.thresholdMax - data.thresholdWarningMax)/100);
			else if(i == 3)
				secPercList.push((data.thresholdWarningMax*2)/100);
		} 

		var $j = jQuery.noConflict();
		barWidth = 6;

		numSections = 5;
		padRad = 0.05;
		chartInset = 10;
		totalPercent = 0.75

		var el = $j('#chart-gaugeID'+index);
		height = 68;
		width = 140;
		radius = 60;
		// radius = Math.max(width, height) / 2;

		el = d3.select('#chart-gaugeID'+index);
		svg = el.append('svg').attr('width', width).attr('height', height).attr("class", "chart_svg");
		chart = svg.append('g').attr('transform', "translate(70,65)");

		percToDeg = function(perc) 
		{
			return perc*360;
		};

		percToRad = function(perc) {
		return degToRad(percToDeg(perc));
		};

		degToRad = function(deg) {
		return deg * Math.PI / 180;
		};

		var scale = d3.scaleLinear().range([0,1]).domain([startValue, endValue]);
		
		var tickData = [];
		// for(var k = startValue;k<=endValue+1;k=k+(endValue-startValue)/4)
		// {
		// 	tickData.push(k);
		// }

		var factor = data.originalValue;
		var xAxis = d3.axisTop().scale(scale).tickValues(tickData);

		for (sectionIndx = i = 1, ref = numSections; 1 <= ref ? i <= ref : i >= ref; sectionIndx = 1 <= ref ? ++i : --i) 
		{
		    arcStartRad = percToRad(totalPercent);
		    arcEndRad = arcStartRad + percToRad((secPercList[sectionIndx-1]/2)*50/endPercent);
		    totalPercent += (secPercList[sectionIndx-1]/2)*50/endPercent;
		    arc = d3.arc().outerRadius(radius - chartInset).innerRadius(radius - chartInset - barWidth).startAngle(arcStartRad).endAngle(arcEndRad);
		    chart.append('path')
		    .attr('class', function(d)
		    {
		    	if(sectionIndx==1 || sectionIndx==5)
					return "arc error chart-color" + sectionIndx;
				else if(sectionIndx==2 || sectionIndx==4)
					return "arc warning chart-color" + sectionIndx;
				else if(sectionIndx == 3)
					return "arc acceptable chart-color" + sectionIndx;
		    	
		    })
		    .attr('d', arc)
		    // .on('click',function(e)
		    // {
		    // 	component.toogleTooltipMethod(index);
      //   		document.getElementById('detailTooltip'+index).style.top = d3.event.clientY + 15 + 'px';
      //   		if(component.get("v.isinline") == 'no')
      //   		{
      //   			document.getElementById('detailTooltip'+index).style.left = d3.event.clientX - jQuery('#detailTooltip'+index).width() + 25 + 'px';
      //   		}
      //   		else
      //   		{
      //   			document.getElementById('detailTooltip'+index).style.left = d3.event.clientX  - 25 + 'px';
      //   		}
        		
		    // });
		}

		//**********************************************************************************************
		//SHENGYU CHANGES IN TICKS
		// if(data.proposedValue != data.originalValue)
			tickData.push(data.proposedValue);
		var lg = svg.append('g')
			.attr('class', 'label')
			.attr('transform',"rotate(-90) translate(-65,70)");

			lg.selectAll('text')
			.data(tickData)
			.enter().append('text')
			.text(d3.format('d'))
			.style("font-size",function(d) 
			{
				if(Math.round(d) == Math.round(data.originalValue) || Math.round(d) == Math.round(data.proposedValue))
				{
					return '0.6em';
				}
				else
				{
					return '0.5em';
				}
			})
			.style("fill",function(d) 
			{
				if(Math.round(d) == Math.round(data.originalValue) || Math.round(d) == Math.round(data.proposedValue))
				{
					return 'black';
				}
				else
				{
					return 'lightgrey';
				}
			})
			.attr('transform', function(d) 
			{
				if(Math.round(d) != Math.round(data.proposedValue))
				{
					var ratio = scale(d);
					var newAngle = (ratio * 180);
					var newRadius = 35;
					// console.log('--->'+index+'--->' + newAngle )
					return 'rotate(90) translate('+ (-newRadius*(Math.cos(degToRad(newAngle))) - barWidth - 2) +','+ -newRadius*(Math.sin(degToRad(newAngle))) +')';
				}
				else
				{
					console.log('d--->>>' + d);
					var ratio = scale(d);
					var newAngle = (ratio * 180);
					var newRadius = 58;
					// console.log('--->'+index+'--->' + newAngle )
					return 'rotate(90) translate('+ (-newRadius*(Math.cos(degToRad(newAngle))) - barWidth + 3) +','+ (-newRadius*(Math.sin(degToRad(newAngle))) + 5) +')';

				}
				
			})
			.each(function(d) 
    		{
            	var self = d3.select(this),
            	textLength = self.node().getComputedTextLength();
            	var text = self.text();
            	if(textLength > 14 && (percent > 0.9 || percent < 0.1))
            	{
            		if(textLength > 20)
            		{
            			text = text.slice(0, -2);
                    	self.text(text + '...');	
            		}
                    
                    textLength = self.node().getComputedTextLength();

            		self.attr('transform', function(d) 
            		{
						var ratio = scale(d);
						var newAngle = (ratio * 180);
						var newRadius = percent<0.1?65:57;
						// if(percent>0.9)
						// 	newRadius = 55;
						return 'rotate(90) translate('+ (-newRadius*(Math.cos(degToRad(newAngle))) - barWidth + 1) +','+ (-newRadius*(Math.sin(degToRad(newAngle))) + 2) +')';
					})
            	}
            	else if(percent > 0.9 || percent < 0.1)
            	{
            		self.attr('transform', function(d) 
            		{
						var ratio = scale(d);
						var newAngle = (ratio * 180);
						var newRadius = 58;
						// if(percent>0.9)
						// 	newRadius = 55;
						return 'rotate(90) translate('+ (-newRadius*(Math.cos(degToRad(newAngle))) - barWidth + 3) +','+ (-newRadius*(Math.sin(degToRad(newAngle))) + 3) +')';
					})
            	}

        	})
        	.append("title")
	        .text(d3.format('d'));

			lg.selectAll('line')
			.data(tickData)
			.enter()
			.append('line')
			.attr('x1',function(d) {
				var ratio = scale(d);
				if(ratio == 0)
					return 1;
				else if(ratio == 1)
					return -1;
				else return 0;

			})
			.attr('x2',function(d) {
				var ratio = scale(d);
				if(ratio == 0)
					return 1;
				else if(ratio == 1)
					return -1;
				else return 0;
			})
			.attr('y1',function(d) 
			{
				if(Math.round(d) == Math.round(data.originalValue) || Math.round(d) == Math.round(data.proposedValue))
				{
					return -18;
				}
				else
				{
					return -10;
				}
			})
			.attr('y2',function(d) 
			{
				if(Math.round(d) == Math.round(data.originalValue) || Math.round(d) == Math.round(data.proposedValue))
				{
					return -6;
				}
				else
				{
					return -7;
				}
			})
			.attr('stroke',function(d) 
			{
				if(Math.round(d) == Math.round(data.originalValue) || Math.round(d) == Math.round(data.proposedValue))
				{
					return 'black';
				}
				else
				{
					return 'lightgrey';
				}
			})
			// .style('stroke-dasharray', function(d)
			// {
			// 	if(Math.round(d) == Math.round(data.proposedValue) && Math.round(data.originalValue) != Math.round(data.proposedValue))
			// 	{
			// 		return '2,2';
			// 	}
			// 	else
			// 	{
			// 		return 'unset';
			// 	}
			// })
			.style('opacity',function(d) 
			{
				if(Math.round(d) == Math.round(data.originalValue) || Math.round(d) == Math.round(data.proposedValue))
				{
					return 1;
				}
				else
				{
					return 0.8;
				}
			})
			.attr('transform', function(d) {
				var ratio = scale(d);
				var newAngle = (ratio * 180);
				return 'rotate(' +newAngle +') translate(0,' +(-radius + barWidth + chartInset + chartInset) +')';
			});


			//******************************************************************


	 
		// 	Needle = (function() {
		// 	    function Needle(len, radius1) {
		// 	      this.len = len;
		// 	      this.radius = radius1;
		// 	    }


		// 	    Needle.prototype.drawOn = function(el, perc) 
		// 	    {

		// 	      el.append('circle').attr('class', 'needle-center'+index).attr('id', 'needle-center_'+index).attr('cx', 0).attr('cy', 0).attr('r', this.radius - 0.5).on('click',function(e)
		// 		    {
		// 		    	component.toggleProposedPopoverMethod(index,percent);
		// 		    })
		// 	      .style('cursor','pointer');
		// 	      return el.append('path').attr('class', 'needle'+index).attr('id', 'needle_'+index).attr('d', this.mkCmd(perc)).on('click',function(e)
		// 		    {
		// 		    	// document.getElementById('UpdatedValuePopover_'+index).classList.remove('slds-hide');
		// 		    	component.toggleProposedPopoverMethod(index,percent);
		// 		    }).style('cursor','pointer');
		// 	    };

		// 	Needle.prototype.animateOn = function(el, perc) 
		// 	{
		// 		var self;
		// 		self = this;

		// 		return el
		// 		.transition()
		// 		.delay(500)
		// 		.ease(d3.easeLinear)
		// 		.selectAll('.needle'+index)
		// 		.duration(3000)
		// 		.tween('progress', function() 
		// 		{
		// 		    return function(percentOfPercent) 
		// 		    {
		// 				var progress;
		// 				progress = prevPercent + percentOfPercent*(perc - prevPercent);
		// 				var need = document.getElementsByClassName("needle"+index);
		// 				var needCenter = document.getElementsByClassName("needle-center"+index);
		// 				if(need && need[0])
		// 				{
		// 					need[0].parentNode.removeChild(need[0]);
		// 					needCenter[0].parentNode.removeChild(needCenter[0]);
		// 				}
							
		// 				needle.drawOn(chart, progress);
		// 				return d3.select(this).attr('d', self.mkCmd(progress));
		// 		    };
		// 		});
		// 	};

		//     Needle.prototype.mkCmd = function(perc) {
		// 		var centerX, centerY, leftX, leftY, rightX, rightY, thetaRad, topX, topY;
		// 		thetaRad = percToRad(perc/2);
		// 		centerX = 0;
		// 		centerY = 0;
		// 		topX = centerX - this.len * Math.cos(thetaRad);
		// 		topY = centerY - this.len * Math.sin(thetaRad);
		// 		leftX = centerX - this.radius * Math.cos(thetaRad - Math.PI / 2);
		// 		leftY = centerY - this.radius * Math.sin(thetaRad - Math.PI / 2);
		// 		rightX = centerX - this.radius * Math.cos(thetaRad + Math.PI / 2);
		// 		rightY = centerY - this.radius * Math.sin(thetaRad + Math.PI / 2);
		// 		return "M " + leftX + " " + leftY + " L " + topX + " " + topY + " L " + rightX + " " + rightY;
		//     };
		//     return Needle;

		// })();

		// needle = new Needle(30, 2.5);
		// // needle.drawOn(chart, 0.5);
		
		// needle.drawOn(chart, prevPercent);
		// needle.animateOn(chart, percent);
		
		console.log('Percentage is');
		console.log(percent + '----->' + percToDeg(percent/2));
		// document.getElementById('UpdatedValuePopover_'+index).classList.add('slds-hide');
    },

    getKpiCardDetails: function(component,event,helper) 
    {
    	var action = component.get("c.getKPICardsDetail");
    	console.log('is Inline--> ' + component.get("v.isinline"));
        action.setParams
        ({
            "crType" : component.get("v.crType"),
            "selectedTeamInstance" : component.get("v.selectedTeamInstance"),
            "selectedPosition" : component.get("v.selectedPosition"),
            "recordId" : component.get("v.recordId"),
            "countryId" : component.get("v.countryId")
        });
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                if(response.getReturnValue() == null)
                {
                    component.set("v.KPICardsWrapperList",[]);
                    var evt = component.getEvent("hideButtonsKpi");
                    evt.fire();

                    if(document.getElementById("summaryCardTextKpi") != null)
                        document.getElementById("summaryCardTextKpi").style.display = "none";

                    return;
                }
              
                var KPICardsWrapperList = response.getReturnValue();
                component.set("v.KPICardsWrapperList",KPICardsWrapperList);

                if(KPICardsWrapperList.length == 0)
                {
                    var evt = component.getEvent("hideButtonsKpi");
                    evt.fire();
                }
              

                var charts = document.getElementsByClassName('chart_svg');
                if(charts.length > 0)
                {
                    for(var j = 0;j<charts.length;j++)
                    {
                        if(!$A.util.isUndefinedOrNull( charts[j] ))
                        {
                          charts[j].parentNode.removeChild(charts[j]);
                        }
                    }
                }
                component.set("v.isDoneRendering",false);
                if(document.getElementById("summaryCardTextKpi") != null)
                    document.getElementById("summaryCardTextKpi").style.display = "none";
            }
            else
            {
                var evt = component.getEvent("hideButtonsKpi");
                evt.fire();
                helper.logException(component, response.getError());
            }
        }); 

        $A.enqueueAction(action);
    },

    logException : function(component, errors) {
        var errorLabel = '', uapLabel='';
        
        if(component.get("v.namespace") != '')
        {
            errorLabel = $A.get("$Label.AxtriaSalesIQTM.Unexpected_error_notification");
            uapLabel = $A.get("$Label.AxtriaSalesIQTM.No_UAP_Assigned");
        }
        else
        {
            uapLabel = $A.get("$Label.c.No_UAP_Assigned");
            errorLabel = $A.get("$Label.c.Unexpected_error_notification");
        }

        var errorMessage = errors[0].message;
        if(errorMessage.indexOf(uapLabel) != -1)
        {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                mode: 'dismissible',
                message: uapLabel,
                type : 'error'
                
            });
            toastEvent.fire();
            return;
        }

        if (errors[0] && errors[0].message) 
        {
            var action = component.get("c.logException");
        
            action.setParams({
                message : errorMessage,
                module : component.get("v.moduleName")
            });

            action.setCallback(this,function(response)
            {
                var state = response.getState();
                if(state === 'SUCCESS')
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: errorLabel,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                }
            });
            $A.enqueueAction(action);
        }
        else 
        {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                mode: 'dismissible',
                message: errorLabel,
                type : 'error'
            });
            toastEvent.fire();
        }
    }
})
