({
    doInit : function(component, event, helper) 
    {
        var params = event.getParam('arguments');
        if(params) {
            component.set("v.countryId", params.countryId);
        }
        
        console.log('kpicards doInit: '+ component.get("v.countryId"));
        var country = component.get("v.countryId");
        var inlineRecordId = component.get("v.recordId");
        if((inlineRecordId != '' && inlineRecordId != undefined) || (country != '' && country != undefined))
            helper.getKpiCardDetails(component, event, helper);

    },

    toggleTooltip : function(component,event,helper,index)
    {
        var idx = 0;
        var f = 0;
        if(event && event.target)
        {
            idx= event.target.id.split('_')[1];
            f = 1;
        }
        else
        {
            var params = event.getParam('arguments');
            idx = params.index;
        }
        if(document.getElementById('detailTooltip'+idx).className.indexOf('slds-hide') == -1)
        {
            document.getElementById('detailTooltip'+idx).classList.add('slds-hide');
        }
        else
        {
            // jQuery('.detailTooltip').addClass('slds-hide');
            var detailTooltipList = document.getElementsByClassName('detailTooltip');

            for(var i = 0;i<detailTooltipList.length;i++)
            {
                // if(!updatedPopoverList[i].classList.contains('slds-hide'))
                // {
                //     updatedPopoverList[i].classList.add('slds-hide');
                // }
                if(!detailTooltipList[i].classList.contains('slds-hide'))
                {
                    detailTooltipList[i].classList.add('slds-hide');
                }
            }
            document.getElementById('detailTooltip'+idx).classList.remove('slds-hide');
            // document.getElementById('UpdatedValuePopover_'+idx).classList.add('slds-hide');
            if(f == 1)
            {
                // console.log('X::::'+ event.pageX + 'Y::;'+event.pageY + 'width:::' + jQuery('#detailTooltip'+idx).width());
            
                if(component.get("v.isinline") == 'no')
                {
                    document.getElementById('detailTooltip'+idx).style.top = event.pageY + 18 + 'px';
                    document.getElementById('detailTooltip'+idx).style.left = event.pageX - jQuery('#detailTooltip'+idx).width() + 25 + 'px';
                }
                else
                {
                  document.getElementById('detailTooltip'+idx).style.left = event.pageX - 25 + 'px';
                  document.getElementById('detailTooltip'+idx).style.top = event.clientY + 20 + 'px';
                }
            }
        }
    },

    // toggleProposedPopover : function(component,event,helper,index)
    // {
    //     var idx = 0;
    //     var percent = 0;
    //     if(event && event.target)
    //     {
    //         idx= event.target.id.split('_')[1];
    //     }
    //     else
    //     {
    //         var params = event.getParam('arguments');
    //         idx = params.index;
    //         percent = params.percent;
    //     }
    //     if(document.getElementById('UpdatedValuePopover_'+idx).classList.value.indexOf('slds-hide') == -1)
    //     {
    //         document.getElementById('UpdatedValuePopover_'+idx).classList.add('slds-hide');
    //     }
    //     else
    //     {
    //         document.getElementById('UpdatedValuePopover_'+idx).classList.remove('slds-hide');
    //         var newRadius = 52;
    //         var angle = percent * Math.PI;
    //         var angleDeg = percent * 180;
    //         var popoverWidth = jQuery('#UpdatedValuePopover_'+idx).outerWidth();
    //         var popoverHeight = jQuery('#UpdatedValuePopover_'+idx).outerHeight();
    //         var needleTop = jQuery('#needle-center_'+idx).position().top;
    //         if(component.get("v.isinline") == 'yes')
    //         {
    //           needleTop = jQuery('#needle-center_'+idx).position().top - jQuery(document).scrollTop();
    //         }
    //         var needleLeft = jQuery('#needle-center_'+idx).position().left;
    //         var popoverTop = -newRadius*(Math.sin(angle)) + needleTop - (popoverWidth/2);
    //         var popoverLeft = -newRadius*(Math.cos(angle)) +  needleLeft - (popoverWidth/2);
    //         console.log('left---> '+ idx + ':::' + needleLeft + ' :::: ' + -newRadius*(Math.cos(Math.round(angle))) + ' :::: ' + popoverLeft);
    //         // if(angleDeg > 90)
    //         // {
    //         //   document.getElementById('UpdatedValuePopoverBody_'+idx).style.transform = "rotate(180deg)";
    //         // }
    //         document.getElementById('UpdatedValuePopover_'+idx).style.transformOrigin = (popoverWidth/2) +"px "+(popoverHeight+11.3)+"px 0px";
    //         document.getElementById('UpdatedValuePopover_'+idx).style.transform = "rotate("+(angleDeg-90)+"deg)";
    //         document.getElementById('UpdatedValuePopover_'+idx).style.top = popoverTop + 'px';
    //         document.getElementById('UpdatedValuePopover_'+idx).style.left = popoverLeft + 'px';
    //     }
    // },

    sliderKpi : function(component,event,helper)
    {
        var params = event.getParam('arguments');
        var direction = params.direction;
        console.log('direction--->' + direction);
        var $j = jQuery.noConflict();
        var counter = component.get("v.clickCount");
        if(counter==1){
            return false;
        }
        counter = 1;
        component.set("v.clickCount",counter);
        var listCount =  $j("#salesiqkpicards ul li").length;
        
        var w1 = $j("#salesiqkpicards").outerWidth();
        var w2 = ($j("#salesiqkpicards ul li").outerWidth()*$j(".salesiqkpicards ul li").length)+(parseFloat($j("#salesiqkpicards ul li").css('margin-right'))*($j(".salesiqkpicards ul li").length-1));
        $j(".salesiqkpicards ul").css({'width':w2});
        var listWidth = $j("#salesiqkpicards ul li").outerWidth();
        
        var extraWidth = (parseInt(w2)-parseInt(w1));
        var visible = Math.round(w1/listWidth);
        var nonVisible = listCount - visible;

        // var updatedPopoverList = document.getElementsByClassName('UpdatedValuePopover');
        var detailTooltipList = document.getElementsByClassName('detailTooltip');

        for(var i = 0;i<detailTooltipList.length;i++)
        {
            // if(!updatedPopoverList[i].classList.contains('slds-hide'))
            // {
            //     updatedPopoverList[i].classList.add('slds-hide');
            // }
            if(!detailTooltipList[i].classList.contains('slds-hide'))
            {
                detailTooltipList[i].classList.add('slds-hide');
            }
        }
        
        if(direction=='left' && extraWidth >= 0)
        {
            if($j("#salesiqkpicards").position().left + (extraWidth/(nonVisible)) <=0 )
            {
                $j("#salesiqkpicards").animate({left: $j("#salesiqkpicards").position().left + Math.round((extraWidth/(nonVisible)))+'px'},100);
            }
            counter = 0;
            component.set("v.clickCount",counter);
        }
        else if(direction=='right' && extraWidth>0)
        {
            if(Math.abs(Math.ceil($j("#salesiqkpicards").position().left)) < extraWidth)
            {
                $j("#salesiqkpicards").animate({left: $j("#salesiqkpicards").position().left - Math.round((extraWidth/(nonVisible)))+'px'},100);
            }
            counter = 0;
            component.set("v.clickCount",counter);
        }
        else if(direction=='hide')
        {
            if(extraWidth > 0)
            {
                var evt = component.getEvent("showButtonsKpi");
                evt.fire();
                document.getElementById("salesiqkpicards").classList.remove('slds-grid--align-end');
                document.querySelector(".salesiqkpicards ul").style.float =  "none";
            }
            if(extraWidth < 0  && listWidth != null)
            {
                var evt = component.getEvent("hideButtonsKpi");
                evt.fire();
                document.getElementById("salesiqkpicards").classList.add('slds-grid--align-end');
                document.querySelector(".salesiqkpicards ul").style.float =  "right";
            }
            $j("#salesiqkpicards").animate({left:'0px'},100);
            counter = 0;
            component.set("v.clickCount",counter);
        }


    },

    doneRenderingFunc : function(component,event,helper)
    {
    	var KPICardsWrapperList = component.get("v.KPICardsWrapperList");
    	var isDoneRendering = component.get("v.isDoneRendering");
    	if(!isDoneRendering)
	    {
            if(document.getElementById("chart-gaugeID0") != null)
	    	{
                if(document.getElementById("summaryCardTextKpi") != null)
                    document.getElementById("summaryCardTextKpi").style.display = "block";
                
                if(document.getElementsByClassName('salesiqkpicards').length > 1)
                {
                    for(var j=0;j<document.getElementsByClassName('salesiqkpicards').length;j++)
                    {
                        if(document.getElementsByClassName('salesiqkpicards')[j].clientWidth == 0)
                            document.getElementsByClassName('salesiqkpicards')[j].parentNode.removeChild(document.getElementsByClassName('salesiqkpicards')[j]);
                    }
                }
                
                var $j = jQuery.noConflict();
                $j(document).scroll(function() 
                {
                    for(var i=0;i<KPICardsWrapperList.length;i++)
                    {
                        if(document.getElementById('detailTooltip'+i) != null && document.getElementById('detailTooltip'+i).className.indexOf('slds-hide') == -1)
                        {
                            document.getElementById('detailTooltip'+i).classList.add('slds-hide');
                        }
                    } 
                   
                });
                if(document.querySelector(".salesiqkpicards ul li") != null)
                {
                    var w1 = $j(".salesiqkpicards").outerWidth();
                    var w2 = ($j("#salesiqkpicards ul li").outerWidth()*$j(".salesiqkpicards ul li").length)+(parseFloat($j("#salesiqkpicards ul li").css('margin-right'))*($j(".salesiqkpicards ul li").length-1));
                    $j(".salesiqkpicards ul").css({'width':w2});
                    var listWidth = $j(".salesiqkpicards ul li").outerWidth();
                }
          
                var extraWidth = (parseInt(w2)-parseInt(w1));
                if(extraWidth < 1)
                {
                    var evt = component.getEvent("hideButtonsKpi");
                    evt.fire();
                    if(component.get('v.isinline') != 'yes')
                    {
                        document.getElementById("salesiqkpicards").classList.add('slds-grid--align-end');
                        document.querySelector(".salesiqkpicards ul").style.float =  "right";
                        if(document.getElementById("summaryCardTextKpi") != null) {
                            document.getElementById("summaryCardTextKpi").style.width = w2+"px";
                            document.getElementById("summaryCardTextKpi").style.float =  "right";
                        }
                    }
                }
                else
                {
                    var evt = component.getEvent("showButtonsKpi");
                    evt.fire();
                    document.getElementById("salesiqkpicards").classList.remove('slds-grid--align-end');
                    document.querySelector(".salesiqkpicards ul").style.float =  "none";
                    if(document.getElementById("summaryCardTextKpi") != null) {
                        document.getElementById("summaryCardTextKpi").style.width = "initial";
                        document.getElementById("summaryCardTextKpi").style.float =  "none";
                    }
                }
          
                for(var i=0;i<KPICardsWrapperList.length;i++)
                {
                    helper.helperMethod(component,event,KPICardsWrapperList[i],i);
                    if(KPICardsWrapperList[i].guardrailType == 'Normal')
                    {
                        document.getElementById('detailTooltip'+i).style.backgroundColor = '#ffffff';
                        document.getElementById('tooltipMessageType'+i).textContent = 'Info';
                        document.getElementById('tooltipTable'+i).style.borderBottom = '1px solid black';
                        document.getElementById('tooltipTable'+i).style.borderTop = '1px solid black';
                        document.getElementById('tooltipMessage'+i).style.color = 'black';
                        $j('#closeToolTipIcon_'+i).css('color','rgb(112, 110, 107)');
                        $j('#tooltipTable'+i+' tr th').each(function()
                        {
                            this.style.color = 'black';
                            this.style.borderLeft = '1px solid black';
                        });
                        $j('#tooltipTable'+i+' tr td').each(function(){
                            this.style.color = 'black';
                            this.style.borderLeft = '1px solid black';
                            this.style.borderTop = '1px solid black';
                        });
                        $j('#warning-svg_'+i).removeClass('icon-warning').addClass('icon-info');
                        // document.getElementById('warningDivId'+i).style.display = 'none';

                    }  
                    else
                    {
                        document.getElementById('detailTooltip'+i).style.backgroundColor = KPICardsWrapperList[i].color;
                        document.getElementById('warningDivId'+i).style.display = 'inline-block';
                        document.getElementById('tooltipMessage'+i).style.color = 'white';
                        console.log('tooltip--->>>>>>');
                        console.log($j('#tooltipTable'+i+' tr th'));
              
                        document.getElementById('tooltipMessageType'+i).textContent = KPICardsWrapperList[i].guardrailType;
                        $j('#warning-svg_'+i).css('color',KPICardsWrapperList[i].color);
                    }

                    component.set("v.isDoneRendering",true);	
                }
	    	} else {
                if(document.getElementById("summaryCardTextKpi") != null)
                    document.getElementById("summaryCardTextKpi").style.display = "none";
	    	}
	    }	
    }
})