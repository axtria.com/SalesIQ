({
    closePopups :  function(component, event, helper) 
    {
        if((event.target != undefined || event.target != null) && (event.target.className != undefined || event.target.className != null))
        {   
            var $j = jQuery.noConflict(); 
            if(event.target.nodeName == 'svg' || event.target.nodeName == 'use' || event.target.nodeName == 'path' || event.target.nodeName == 'text')
            {
                if(event.target.className.baseVal.indexOf('errorPopupButton') == -1)
                {
                    var dataTable = component.find('dataTable_v2');
                    dataTable.closeErrorPopover();
                    
                }
            }
            else if(event.target.className && event.target.className.indexOf('errorPopupButton') == -1)
            {
                var dataTable = component.find('dataTable_v2');
                dataTable.closeErrorPopover();
            }
            if(event.target.nodeName == 'svg' || event.target.nodeName == 'use' || event.target.nodeName == 'path' || event.target.nodeName == 'text')
            {
                if(event.target.className.baseVal && event.target.className.baseVal.indexOf('icon-map-signs') == -1 && event.target.className.baseVal.indexOf('legendButton') == -1 && !$j(event.target).closest("div").hasClass('legendPopOver'))
                {
                    var dataTable = component.find('dataTable_v2');
                    dataTable.closeLegend();
                }
            }
            else if(event.target.className && event.target.className.indexOf('icon-map-signs') == -1 && event.target.className.indexOf('legendButton') == -1 && !$j(event.target).closest("div").hasClass('legendPopOver'))
            {
                var dataTable = component.find('dataTable_v2');
                dataTable.closeLegend();
            }   
        }    
        
        
        if((event.path != undefined || event.path != null) && event.path[0].parentElement != null)
        {
            if(event.path[0].nodeName == 'svg' || event.path[0].nodeName == 'use' || event.path[0].nodeName == 'path' || event.path[0].nodeName == 'text') {
                if(event.path[0].parentElement.className.baseVal != 'brdCrmbPopOut' && event.path[0].className.baseVal.indexOf('hierarchyButton')== -1 && event.path[0].id != "treeSearchBox" && event.path[0].className.baseVal.indexOf('icon-nodes') == -1) {
                    var dataTable = component.find('dataTable_v2');
                    dataTable.closeDropDown();
                    document.querySelectorAll('[id*="_treeContainer"]')[0].className = 'slds-tree_container slds-hide' ;
                }
            } 
            else if(event.path[0].parentElement.className != 'brdCrmbPopOut' && event.path[0].className.indexOf('hierarchyButton')== -1 && event.path[0].className.indexOf('icon-nodes') == -1 && event.path[0].id != "treeSearchBox")
            {
                var dataTable = component.find('dataTable_v2');
                dataTable.closeDropDown();
                document.querySelectorAll('[id*="_treeContainer"]')[0].className = 'slds-tree_container slds-hide' ;
            }
        } 
        else if((event.target != undefined || event.target != null) && event.target.parentElement != null)
        {
            if(event.target.nodeName == 'svg' || event.target.nodeName == 'use' || event.target.nodeName == 'path' || event.target.nodeName == 'text')
            {
                if(event.target.parentElement.nodeName == 'svg' || event.target.parentElement.nodeName == 'use' || event.target.parentElement.nodeName == 'path' || event.target.parentElement.nodeName == 'text')
                {
                    if(event.target.parentElement.className.baseVal != 'brdCrmbPopOut' && event.target.className.baseVal.indexOf('hierarchyButton')== -1 && event.target.id != "treeSearchBox" && event.target.className.baseVal.indexOf('icon-nodes') == -1)
                    {
                      var dataTable = component.find('dataTable_v2');
                      dataTable.closeDropDown();
                      document.querySelectorAll('[id*="_treeContainer"]')[0].className = 'slds-tree_container slds-hide' ;
                    }
                }
        
                else if(event.target.parentElement.className != 'brdCrmbPopOut' && event.target.className.baseVal.indexOf('hierarchyButton')== -1 && event.target.id != "treeSearchBox" && event.target.className.baseVal.indexOf('icon-nodes') == -1)
                {
                    var dataTable = component.find('dataTable_v2');
                    dataTable.closeDropDown();
                    document.querySelectorAll('[id*="_treeContainer"]')[0].className = 'slds-tree_container slds-hide' ;
                }
            }
            else if(event.target.parentElement.className != 'brdCrmbPopOut' && event.target.className.indexOf('hierarchyButton')== -1 && event.target.id != "treeSearchBox" && event.target.className.indexOf('icon-nodes') == -1)
            {
                var dataTable = component.find('dataTable_v2');
                dataTable.closeDropDown();
                document.querySelectorAll('[id*="_treeContainer"]')[0].className = 'slds-tree_container slds-hide' ;
            }
        }
    }, 

    leftslider : function(component,event,helper)
    {
      var modalListView =  component.find('listViewComp');
      modalListView.slider('left');
    },
    rightslider : function(component,event,helper)
    {
      
      var modalListView =  component.find('listViewComp');
      modalListView.slider('right');
      
    },

    breadcrumbChange : function(component,event,helper)
    {
        var kpiComponent = component.find("kpiCardsSection");
        var $j = jQuery.noConflict();
        var globalId = component.getGlobalId();
        if($j(document.getElementById(globalId+'_KpiCardsParentDiv')).height() > 0)
        {
            kpiComponent.set("v.selectedTeamInstance",event.getParam("teamInstanceId")); 
            kpiComponent.set("v.countryId",event.getParam("countryId")); 
            kpiComponent.set("v.selectedPosition",'');
            kpiComponent.refreshCharts(event.getParam("countryId"));
        }
        
       
        
        // console.log('breadcrumb change capture in KPI Card ' + component.get("v.selectedTeamInstance") +  '  ' + component.get("v.countryId") + component.get("v.recordId"));
        // helper.getKpiCardDetails(component, event, helper);
    },

    nodeSelectedClick : function(component,event,helper)
    {
        var kpiComponent = component.find("kpiCardsSection");
        var selectedPosition = event.getParam("selectedTreeNode");
        kpiComponent.set("v.selectedPosition",selectedPosition); 
        kpiComponent.refreshCharts(component.get("v.countryId"));
    },

    hideListViewButtons: function(component,event,helper)
    {
    var anchorButton = component.find('anchorButton');
    $A.util.addClass(anchorButton,'slds-hide');
    },

    showListViewButtons: function(component,event,helper)
    {
    var anchorButton = component.find('anchorButton');
    $A.util.removeClass(anchorButton,'slds-hide');
    },

    openSettings : function(component, event, helper) {
        $A.createComponent(
            "c:SalesIQCallPlanScenarioSetting",
            {
                "aura:id": "callPlanSetting",
                "selectedTeamInstance" : component.get("v.selectedTeamInstance"),
                "scenarioId" : component.get("v.scenarioID")
            },
            function(settingComponent, status, errorMessage)
            {                
                var targetCmp = component.find('callPlanScenarioSetting');
                var body = targetCmp.get("v.body");
                body.push(settingComponent);
                targetCmp.set("v.body", body);
            }
        );
    },

    refreshCards : function(component, event, helper){
        var kpiComponent = component.find("kpiCardsSection");
        kpiComponent.refreshCharts(component.get("v.countryId"));
        var listViewComp = component.find("listViewComp");
        listViewComp.refreshListViews(false);
    },
    leftsliderKpi : function(component,event,helper)
    {
        var modalKpiView =  component.find('kpiCardsSection');
        modalKpiView.sliderKpi('left');
    },

    rightsliderKpi : function(component,event,helper)
    {
        var modalKpiView =  component.find('kpiCardsSection');
        modalKpiView.sliderKpi('right');
    },

    refreshComponents : function(component,event,helper)
    {
        console.log('refreshComponents handller');
        var kpiComponent = component.find("kpiCardsSection");
        kpiComponent.refreshCharts();

        var scenarioStage = event.getParam("infoToPass");
        if(scenarioStage) {
            component.set("v.scenarioStage", scenarioStage);
        }
    },
    hideListViewButtonsKpi: function(component,event,helper)
    {
       var anchorButtonKpi = component.find('anchorButtonKpi');
       $A.util.addClass(anchorButtonKpi,'slds-hide');
    },
    showListViewButtonsKpi: function(component,event,helper)
    {
      var anchorButtonKpi = component.find('anchorButtonKpi');
      $A.util.removeClass(anchorButtonKpi,'slds-hide');
    },
    
    viewChange: function(component, event, helper) 
    {
        var datatable = component.find('dataTable_v2');
        var viewId = event.getParam("viewId");
        var viewName = event.getParam("viewName");
        var whereClausePACP = event.getParam("whereClausePACP");
        
        component.set("v.selectedViewName",viewName);
        component.set("v.whereClausePACP",whereClausePACP);
        console.log('view change called, SalesIQ Call Plan Controller (view id and where clause) ' + viewId + ' - ' + viewName + ' :: ' + whereClausePACP);
        datatable.changeView(viewId, whereClausePACP);
    },

    updateCountryAttributes: function(component, event, helper) 
    {
        console.log('Caught update attribute - Call Plan Controller');
        var countryId = event.getParam('countryId');
        component.set("v.countryId",countryId);
        var datatable = component.find('dataTable_v2');
        datatable.updateCountryAttributes(countryId);

        var kpiComponent = component.find("kpiCardsSection");
        kpiComponent.refreshCharts(component.get("v.countryId"));

        var flag = component.find('flagComponent');
        flag.updateCountryAttributes(countryId);
    },

    loadCallPlanRules : function(component)
    {
        var scenario_Id = component.get("v.scenarioID");
        var viewCanvas = "Y";                         
        var workspace_Id = component.get("v.workspaceID");
        //var selectedRecordId = component.set("v.recordId",scenario_Id);
        var viewcanvas = 'Y';
        var Scenario_Type = "Call Plan Generation";
       
        console.log("record_Id :::::::::::::::::::::: "+scenario_Id);
        console.log("workspace_Id ----- "+workspace_Id);
        var evt = $A.get("e.force:navigateToComponent");

        evt.setParams({
            componentDef : "c:CPGbusinessRuleCanvas",
            componentAttributes: {
                record_Id: scenario_Id, 
                viewcanvas : viewCanvas,
                workSpaceId : workspace_Id ,
                ScenarioType : Scenario_Type,
                selectedTeamInstance : component.get("v.selectedTeamInstance") 
            },
            isredirect : true
         });
        evt.fire();
    }
})