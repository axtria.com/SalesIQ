({
    
    doInit : function(component, event, helper) {
        console.log("doInit called");
        console.log("Scenario instance ID received");
        console.log(component.get("v.scenarioRuleInstanceId"));
        var scenarioRuleInstanceId = component.get("v.scenarioRuleInstanceId");
                
        //component.getAssignmentBusinessRules();
      	var action = component.get("c.getInputOutputDatasources");
      	action.setParams({
        	scenarioRuleInstanceId : scenarioRuleInstanceId
      	});
        
        action.setCallback(this, function(response) {
          var state = response.getState();
          if(state === 'SUCCESS'){

           var datasourcesList = response.getReturnValue();
           var dataMap = {};
           var dataSetMap=[];
           var outputdatasetMap = [];
           for(var i=0;i<datasourcesList.length;i++)
           {
            if(datasourcesList[i][common.getKey(component,"ds_type__c")] == 'O'){
             outputdatasetMap.push({'Id':datasourcesList[i][common.getKey(component,"dataset_id__c")],
              'tableName':datasourcesList[i][common.getKey(component,"Table_Display_Name__c")]});
           }
           else{
             dataSetMap.push({'Id':datasourcesList[i][common.getKey(component,"dataset_id__c")],
              'tableName':datasourcesList[i][common.getKey(component,"Table_Display_Name__c")]});
             
            // var a = datasourcesList[i]['dataset_id__r']['Name'];
             var a = datasourcesList[i][common.getKey(component,"Table_Display_Name__c")];
             var b = datasourcesList[i][common.getKey(component,"table_name__c")] ;
             dataMap[a] = b;
           }
         }

          component.set('v.dataMap',dataMap);
          console.log('dataMap::: ',dataMap);
              console.log('dataSetMap::: ',dataSetMap);
              console.log('outputdatasetMap::: ',outputdatasetMap);

         if(dataSetMap.length == 2 && outputdatasetMap.length == 1)
         {
           
           component.set("v.dataSetMap", dataSetMap);
           component.set("v.outputdatasetMap", outputdatasetMap);

            
         }

         else
         {
          component.showErrorToast();
        }  

      }

      else{

        console.log('error :: '+state);
      }   

    });
            
      $A.enqueueAction(action);
      
      component.mergeDetails();
        /*
        var actionTabName = component.get("c.getUnionFieldDetails");
        actionTabName.setParams({ "scenarioInstId" : component.get("v.scenarioRuleInstanceId")});
        actionTabName.setCallback(this, function(response){ 
            var stateTabName = response.getState();
            if(stateTabName==="SUCCESS")
            { 
                var result = response.getReturnValue();
                component.set("v.unionVal", result);     
            }
            else{
                console.log("Failed with state:  "+ state);
            }
        });
        $A.enqueueAction(actionTabName);
        */
    },
    
    closeActionButton : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:CPGbusinessRuleCanvas",
            componentAttributes: {
                record_Id : component.get('v.scenarioId'),
                viewcanvas : "Y",
                workSpaceId : component.get('v.workSpaceId'),
                summarizedViewFlag : component.get("v.summarizedViewFlag")
            }
        });
        evt.fire();
    },
    
     showErrorToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        var dsnotavailable = $A.get("$Label.c.Datasources_Not_Available");
        toastEvent.setParams({
          message:dsnotavailable,
          messageTemplate: dsnotavailable,
          duration:' 5000',
          key: 'info_alt',
          type: 'error',
          mode: 'pester'
        });
        toastEvent.fire();
      },
    
    mergeDetails : function(component, event, helper){
         var actionTabName = component.get("c.getUnionFieldDetails");
         actionTabName.setParams({ "scenarioInstId" : component.get("v.scenarioRuleInstanceId")});
         actionTabName.setCallback(this, function(response){ 
           var stateTabName = response.getState();
           if(stateTabName==="SUCCESS")
                { 
                    var result = response.getReturnValue();
                    component.set("v.unionVal", result);     
                }
                else{
                    console.log("Failed with state:  "+ state);
                }
          });
       	  $A.enqueueAction(actionTabName);
    }
})