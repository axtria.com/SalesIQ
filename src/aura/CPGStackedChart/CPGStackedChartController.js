({
    loadStackChart : function(component, event, helper) {
        
       
        component.set("v.Spinner", true); 
        var params = event.getParam('arguments');
        if (params) {
            var param1 = params.param1;
            var param2 = params.param2;
            var param3 = params.param3;
            var param4 = params.param4;
            var param5 = params.param5;
            component.set("v.currentScenarioId",param1);
            component.set("v.previousScenarioId",param2) ;
            component.set("v.currentInstance",param3) ;
            component.set("v.teamName",param4) ;
            component.set("v.url",param5) ;
            console.log("currentScenarioId is =="+param1); 
            console.log("previousScenarioId is =="+param2); 
        }
        
        if(!param1  || param1=='undefined' ){
            component.set("v.Spinner", false); 
        }
        else{
            console.log('inside load Stack Chart method');
            var currentScenarioId = component.get("v.currentScenarioId");
            //var currentScenarioId = 'a0If4000001pIU2EAM';
            var previousScenarioId = component.get("v.previousScenarioId");
            if(component.get("v.currentInstance")){
                var currentins =   component.get("v.currentInstance").replace('/','');
            }
            var teamName = component.get("v.teamName");
            console.log(teamName);
            var url=component.get("v.url");
            var action = component.get("c.generateVisualizationReport");
            var res = url+'?scenario_1='+currentScenarioId+'&scenario_2='+previousScenarioId+'&tab_id=workload_dis&team='+teamName+'&context='+currentins; 
            console.log('strUrl->'+res);
            
            action.setParams({
                "url": res
            });
            
            action.setCallback(this,function(response)
                               {
                                   var state = response.getState();
                                   if(state === 'SUCCESS' && response.getReturnValue != null)
                                   {
                                       var stackedchartresultsMap = response.getReturnValue();
                                       console.log("webservice response");
                                       console.log(stackedchartresultsMap);
                                       component.set("v.stackedchartresultsMap",stackedchartresultsMap);
                                       component.getCallsStackedChart();
                                       var noData = component.find("noData");
                                       $A.util.addClass(noData,'slds-hide');
                                       var div1 = component.find("div1"); 
                                       $A.util.removeClass(div1,'slds-hide');              
                                       
                                       try{
                                           var categories = [];
                                           var graphData={};
                                           
                                           var json={"key":"","values":[]};
                                           var legendName1;
                                           var legendName2;
                                           for(var ctrKeys=0;ctrKeys<stackedchartresultsMap.KEYS.length;ctrKeys++){
                                               graphData[stackedchartresultsMap.KEYS[ctrKeys].column_name]=[];
                                           }
                                           for(var i=0;i<stackedchartresultsMap.CPG_Report_1.length;i++){
                                               
                                               for(var ctrKeys=0;ctrKeys<stackedchartresultsMap.KEYS.length;ctrKeys++){
                                                   var key=stackedchartresultsMap.KEYS[ctrKeys].column_name;
                                                   graphData[key].push(stackedchartresultsMap.CPG_Report_1[i][key]);
                                                }
                                               
                                           }
                                           
                                           var arrSeries=[];
                                           for(var ctrKeys=0;ctrKeys<stackedchartresultsMap.KEYS.length;ctrKeys++){
                                               var key=stackedchartresultsMap.KEYS[ctrKeys].column_name;
                                               if(ctrKeys==0){
                                                   categories=graphData[key];  
                                               }
                                               else{
                                                   arrSeries.push({
                                                       name: key,
                                                       data: graphData[key]
                                                   });
                                               }
                                               
                                               
                                           }
                                        
                                           Highcharts.chart('stackedchart', {
                                               chart: {
                                                   type: 'column',
                                                    ignoreHiddenSeries: false
                                               },
                                               title: {
                                                   text: 'Distribution of Call Plan Targets'
                                               },
                                               xAxis: {
                                                   lineColor: '#000',
                                                   lineWidth: 1,
                                                   title: {
                                                       text: 'Territory'
                                                   },
                                                   categories: categories
                                               },
                                               yAxis: {
                                                   min: 0,
                                                   lineColor: '#000',
                                                   lineWidth: 1,
                                                   title: {
                                                       text: 'Total Targets'
                                                   },
                                                   stackLabels: {
                                                      style: {
                                                           fontWeight: 'bold',
                                                           color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                                                       }
                                                   }
                                               },
                                               legend: {
                                                   align: 'right',
                                                   x: -30,
                                                   verticalAlign: 'top',
                                                   y: 25,
                                                   floating: true,
                                                   backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                                                   borderColor: '#CCC',
                                                   borderWidth: 1,
                                                   shadow: false
                                               },
                                               tooltip: {
                                                   formatter: function () {
                                                       return '<b>'+this.x + '</b><br/>'+this.series.name+': '+this.y+
                                                           '<br/>Total: '+ this.point.stackTotal ;}
                                               },
                                               plotOptions: {
                                                   column: {
                                                       stacking: 'normal',
                                                       dataLabels: {
                                                           // enabled: true,
                                                           color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                                                       }
                                                   }
                                               },
                                               series: arrSeries,
                                               credits:{
                                                   enabled:false
                                               } 
                                               
                                           });
                                           component.set("v.Spinner", false);
                                           
                                       }
                                       catch(err)
                                       {   
                                           component.set("v.Spinner", false);
                                           var noData = component.find("noData");
                                           $A.util.removeClass(noData,'slds-hide');
                                           var div1 = component.find("div1");
                                           $A.util.addClass(div1,'slds-hide');
                                           console.log('--err ' + err);
                                       }
                                       
                                   }
                                   
                                   else{
                                       component.set("v.Spinner", false);
                                       var noData = component.find("noData");
                                       $A.util.removeClass(noData,'slds-hide');
                                       var div1 = component.find("div1");
                                       $A.util.addClass(div1,'slds-hide');
                                   }
                                   
                               });
            
            $A.enqueueAction(action);            
        }
    },
    
    getCallsStackedChart : function(component,event,helper)
    {
          
        var stackedchartresultsMap = component.get("v.stackedchartresultsMap");
        console.log("webservice response");
        console.log(stackedchartresultsMap);        
        try{
            var categories = [];
            var graphData={};
            
            var json={"key":"","values":[]};
            var legendName1;
            var legendName2;
             for(var ctrKeys=0;ctrKeys<stackedchartresultsMap.KEYS.length;ctrKeys++){
                 graphData[stackedchartresultsMap.KEYS[ctrKeys].column_name]=[];
             }
            for(var i=0;i<stackedchartresultsMap.CPG_Report_2.length;i++){
                
                for(var ctrKeys=0;ctrKeys<stackedchartresultsMap.KEYS.length;ctrKeys++){
                    var key=stackedchartresultsMap.KEYS[ctrKeys].column_name;
                    graphData[key].push(stackedchartresultsMap.CPG_Report_2[i][key]);
                }
                
            }
            
            var arrSeries=[];
            for(var ctrKeys=0;ctrKeys<stackedchartresultsMap.KEYS.length;ctrKeys++){
                var key=stackedchartresultsMap.KEYS[ctrKeys].column_name;
                if(ctrKeys==0){
                    categories=graphData[key];  
                }
                else{
                    arrSeries.push({
                        name: key,
                        data: graphData[key]
                    });
                }
                
                
            }
                                        
                                        
            
            Highcharts.chart('callbasedstackedchart', {
                chart: {
                    type: 'column',
                     ignoreHiddenSeries: false
                },
                title: {
                    text: 'Distribution of Total Calls'
                },
                xAxis: {
                    lineColor: '#000',
                    lineWidth: 1,
                    title: {
                        text: 'Territory'
                    },
                    categories: categories
                },
                yAxis: {
                    min: 0,
                    lineColor: '#000',
                    lineWidth: 1,
                    title: {
                        text: 'Total Calls'
                    },
                    stackLabels: {
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'top',
                    y: 25,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    formatter: function () {
                        return '<b>'+this.x + '</b><br/>'+this.series.name+': '+this.y+
                            '<br/>Total: '+ this.point.stackTotal ;}
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    }
                },
                series: arrSeries,
                credits:{
                    enabled:false
                } 
                
            });
            component.set("v.Spinner", false);
            
        }
        catch(err)
        {   
            component.set("v.Spinner", false);
            var noData = component.find("noData");
            $A.util.removeClass(noData,'slds-hide');
            var div1 = component.find("div1");
            $A.util.addClass(div1,'slds-hide');
            console.log('--err ' + err);
        }
    },
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    
})