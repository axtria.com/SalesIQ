({
	doInit : function(component,event,helper)
	{
	    
	    var action = component.get("c.getPositionEditEnableCustomSetting");
		action.setCallback(this,function(response)
	    {
	    	var result = response.getReturnValue();	            
            if(response.getState() === 'SUCCESS' && result != null)
            {
            	component.set("v.showMenuList", result);
            }
	    });
	    $A.enqueueAction(action);
		if(component.get("v.recordId") != '')
			helper.getDetails(component,event);
	},

	getData : function(component,event,helper)
	{
		if(component.get("v.recordId") != '')
		{
			helper.getDetails(component,event);
			component.showDetailCard();
		}
	},

	openConfirmationPopUp : function(component, event, helper) {
        
        var changesPerformed = component.get("v.changesPerformed");
        var isCRPending = component.get("v.isCRPending");
        var namespace = component.get("v.namespace");
        var modalMessage;
        if(namespace != '')
        {
            modalMessage = $A.get("$Label.AxtriaSalesIQTM.Dicard_Changes_Popup_Message");
        }
        else
        {
        	 modalMessage = $A.get("$Label.c.Dicard_Changes_Popup_Message");
        }
		if(changesPerformed && !isCRPending)
        {
	        component.set("v.modalMessage",modalMessage);
	        $A.createComponent(
	            "c:SalesIQMessageDialog",
	            {
	                "body": component.get("v.modalMessage"),
	                "recordId" : component.get("v.recordId"),
	                "moduleName" : component.get("v.moduleName"),
	                "onconfirm" : component.getReference("c.navigateToRecord")
	            },
	            function(msgBox){                
	                if (component.isValid()) {
	                    var targetCmp = component.find('ModalDialogPlaceholder');
	                    var body = targetCmp.get("v.body");
	                    body.push(msgBox);
	                    targetCmp.set("v.body", body);
	                }
	            }
	        );
	    } else {
	    	var currentState = history.state;
	    	if(component.get("v.moduleName") == 'Employee Universe')
        		history.pushState(currentState, "Employee_Universe" , "" );
	    	else
        		history.pushState(currentState, "Position_Universe" , "" );

	    	var recordId = component.get("v.recordId");
	        var navEvt = $A.get("e.force:navigateToSObject");
	        navEvt.setParams({
	            "recordId": recordId,
	            isredirect : true
	        });
	        navEvt.fire(); 
	    }
    },
   
	showDetailsCard : function(component,event,helper)
	{	
		var details_card = component.find("details_card");
		$A.util.removeClass(details_card,"slds-hide");
	},

	hideDetailsCard : function(component,event,helper)
	{
		var details_card = component.find("details_card");
		$A.util.addClass(details_card,"slds-hide");
	},

	navigateToRecord : function(component,event,helper)
    {
        var currentState = history.state;
        if(component.get("v.moduleName") == 'Employee Universe')
            history.pushState(currentState, "Employee_Universe" , "" );
        else
            history.pushState(currentState, "Position_Universe" , "" );
         document.body.setAttribute('style', 'overflow: auto;');
        var recordId = component.get("v.recordId");
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId,
            isredirect : true
        });
        navEvt.fire(); 
    },

    navigateRecord : function(component,event,helper)
    {
    	document.body.setAttribute('style', 'overflow: auto;');
    }
})