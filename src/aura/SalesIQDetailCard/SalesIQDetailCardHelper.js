({
    getDetails : function(component,event)
    {

        var recordId = component.get("v.recordId");
        var objectName = component.get("v.objectName");
        var referenceField = component.get("v.referenceField");
        var interfaceName = component.get("v.interfaceName");
        var isRelated = component.get("v.isRelated");
        var ns = component.get("v.namespace");

        var action = component.get("c.getRecordData");

        action.setParams({
            recordId : recordId,
            objectName : objectName,
            isRelated : isRelated,
            interfaceName : interfaceName,
            referenceField : referenceField
        });
       
        action.setCallback(this,function(response)
        {
            
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {

                var retResponse = response.getReturnValue();

                component.set("v.detailCardWrapper",retResponse);

                var detailsList = []; 
                var recordsList = [];
                var refMap = retResponse.refMap;
                var recordDetailsList = retResponse.recordDetails;
                var apiLabelMap = retResponse.apiLabelMap;
                var formattedDateMap = retResponse.formattedDateMap;

                for(var i = 0;i<recordDetailsList.length;i++)
                {
                    detailsList = [];
                    var recordDetails = recordDetailsList[i];
                    Object.keys(apiLabelMap).forEach(function(key) 
                    {
                        if(!refMap[key])
                        {
                            
                            if(recordDetails[key] == undefined)
                            {
                                detailsList.push({api:key, record:'', label : apiLabelMap[key], isLink: false, linkUrl: ''});
                            }
                            else if(key == ns+'Employee_ID__c')
                            {
                                detailsList.push({api:key, record:recordDetails[key], label:apiLabelMap[key], isLink : true, linkUrl: recordDetails['Id']});
                            }
                            else
                            {
                                if(formattedDateMap[recordDetails['Id']][key] != undefined)
                                {
                                    detailsList.push({api:key, record:formattedDateMap[recordDetails['Id']][key], label:apiLabelMap[key], isLink : false, linkUrl: ''});
                                }
                                else
                                detailsList.push({api:key, record:recordDetails[key], label:apiLabelMap[key], isLink : false, linkUrl: ''});
                            }
                        }
                        else
                        {
                            if(recordDetails[key.split('.')[0]] != undefined)
                                detailsList.push({api:key, record : recordDetails[key.split('.')[0]]['Name'], label : apiLabelMap[key], isLink : true, linkUrl: recordDetails[key.split('.')[0]]['Id']});
                            else
                                detailsList.push({api:key, record : '', label : apiLabelMap[key], isLink : false, linkUrl: ''});
                        }

                    });
                    if(component.get("v.isRelated") == 'true')
                    {
                        if(component.get("v.interfaceName") == 'POSITION EMPLOYEE')
                        {
                            if(component.get("v.moduleName") == 'Employee Universe')
                            recordsList.push({list:detailsList,name: retResponse.recordDetails[i][ns+'Employee__r']['Name'], id:retResponse.recordDetails[i]['Id']});
                            else if(component.get("v.moduleName") == 'Position Universe')
                                recordsList.push({list:detailsList,name: retResponse.recordDetails[i][ns+'Position__r']['Name'], id:retResponse.recordDetails[i]['Id']});
                        }
                        else
                            recordsList.push({list:detailsList,name: retResponse.recordDetails[i].Name, id:retResponse.recordDetails[i]['Id']});
                        
                    }
                    else
                    {
                        recordsList.push({list:detailsList,name: retResponse.recordDetails[i].Name, id:recordId});
                    }
                    
                }    
                

                component.set("v.recordsList",recordsList);
                
            }

            else if (state === "ERROR" || response.getReturnValue == null) 
            {
                var errorLabel;
                if(ns != '')
                {
                    errorLabel = $A.get("$Label.AxtriaSalesIQTM.Unexpected_error_notification");
                }
                else
                {
                    errorLabel = $A.get("$Label.c.Unexpected_error_notification") 
                }
                var errors = response.getError();
                var errorMessage = errors[0].message;
                if (errors[0] && errors[0].message) 
                {
                    var action = component.get("c.logException");
        
                    action.setParams({
                        message : errorMessage,
                        module : component.get("v.moduleName")
                    });

                    action.setCallback(this,function(response)
                    {
                        var state = response.getState();
                        if(state === 'SUCCESS')
                        {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                mode: 'dismissible',
                                message: errorLabel,
                                type : 'error'
                                
                            });
                            toastEvent.fire();
                        }
                    });
                    $A.enqueueAction(action);
                } 
                else 
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: errorLabel,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                }
            } 


        });

        $A.enqueueAction(action);        

    }
})