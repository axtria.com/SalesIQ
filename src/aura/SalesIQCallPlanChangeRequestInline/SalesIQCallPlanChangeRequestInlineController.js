({
	leftslider : function(component,event,helper)
    {
      var modalListView =  component.find('kpiCardsSection');
      modalListView.sliderKpi('left');
    },
    rightslider : function(component,event,helper)
    {
      
      var modalListView =  component.find('kpiCardsSection');
      modalListView.sliderKpi('right');
      
    },

    hideListViewButtons: function(component,event,helper)
    {
    var anchorButton = component.find('anchorButton');
    $A.util.addClass(anchorButton,'slds-hide');
    },

    showListViewButtons: function(component,event,helper)
    {
    var anchorButton = component.find('anchorButton');
    $A.util.removeClass(anchorButton,'slds-hide');
    },
})