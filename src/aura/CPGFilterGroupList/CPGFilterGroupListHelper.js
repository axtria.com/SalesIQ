({
	ToggleCollapseHandler: function(component, event) {  
        var idx = event.currentTarget.getAttribute("data-index");
        //console.log(idx);
        var container = document.getElementById("Dummy_"+idx);         
        
        var isExpandable = $A.util.hasClass(container, "slds-is-open");      
        if(isExpandable){            
            $A.util.removeClass(container, 'slds-is-open');
        }
        else{            
            $A.util.addClass(container, 'slds-is-open'); 
        }
    },
    showPopupHelper: function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className + 'hide');
        $A.util.addClass(modal, className + 'open');                
    },
    
})