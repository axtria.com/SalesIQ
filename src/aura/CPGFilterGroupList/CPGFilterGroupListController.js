({
     HideShow: function(component, event, helper) { 
        helper.ToggleCollapseHandler(component, event);
    },
    addACriteria:function(component, event,helper){        
        var addEvent= $A.get('e.c:CPGAddFilterGroupEvent');
        var idx = event.currentTarget.getAttribute("data-recId");
        addEvent.setParams({'dataindex':idx+'@AddCriteria'});
        addEvent.fire();
    },
    addAGroup:function(component, event,helper){
        var addGrpEvent= $A.get('e.c:CPGAddFilterGroupEvent');
        var idx = event.currentTarget.getAttribute("data-recId");
        addGrpEvent.setParams({'dataindex':idx+'@AddGroup'});
        addGrpEvent.fire();
    },
    addADelete:function(component, event,helper){
        var adddeleteEvent= $A.get('e.c:CPGAddFilterGroupEvent');
        var idx = event.currentTarget.getAttribute("data-recId");
        adddeleteEvent.setParams({'dataindex':idx+'@DeleteGroup'});
        adddeleteEvent.fire();
    },
    btnANDClick:function(component, event,helper){
        var addEvent= $A.get('e.c:CPGAddFilterGroupEvent');
        var idx = event.currentTarget.getAttribute("data-recId");
        console.log(idx);
        var idG= parseInt(idx.split('_')[0]);
        var idB= parseInt(idx.split('_')[1]);
        var btnOR = document.getElementById('btnOR_'+idx);
        console.log(btnOR);
        var btnAnd = document.getElementById('btnAnd_'+idx);
        console.log(btnAnd);
        $A.util.removeClass(btnOR,'slds-is-selected');
        $A.util.addClass(btnAnd,'slds-is-selected');
        addEvent.setParams({'dataindex':idx+'@AndClicked'});
        addEvent.fire();
    },
    btnORClick:function(component, event,helper){
        var orEvent= $A.get('e.c:CPGAddFilterGroupEvent');
        var idx = event.currentTarget.getAttribute("data-recId");
        var idG= parseInt(idx.split('_')[0]);
        var idB= parseInt(idx.split('_')[1]);
        console.log(idx);
        var btnOR = document.getElementById('btnOR_'+idx);
        //console.log(btnOR);
        var btnAnd = document.getElementById('btnAnd_'+idx);
        $A.util.removeClass(btnAnd,'slds-is-selected');
        $A.util.addClass(btnOR,'slds-is-selected');
        orEvent.setParams({'dataindex':idx+'@OrClicked'});
        orEvent.fire();
    },
    
    
    
    
    
    
})