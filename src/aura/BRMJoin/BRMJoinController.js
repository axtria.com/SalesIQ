({
    doInit : function(component, event, helper) {
      	
        var ns = component.get("v.namespace");
        console.log('ns:::: '+ns);
        console.log('inside join doInit');
        
        component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0 !important;}   .forceStyle .viewport.oneHeader.desktop {z-index:0 !important;} .forceStyle.desktop .viewport{overflow:hidden}");
        var hidesave = component.find("outputButton");
        $A.util.removeClass(hidesave,'slds-hide');
        var scenarioId = component.get('v.scenarioId');
        var workspaceId = component.get('v.workSpaceId');
        var scenarioRuleInstanceId = component.get('v.scenarioRuleInstanceId');
        console.log('scenarioId::: '+scenarioId);

        var action = component.get("c.getInputOutputDatasources");
        action.setParams({
          scenarioRuleInstanceId : scenarioRuleInstanceId
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === 'SUCCESS'){

                var datasourcesList = response.getReturnValue();
                var dataMap = {};
                var dataSetMap=[];
                var outputdatasetMap = [];
                for(var i=0;i<datasourcesList.length;i++)
                {
                    if(datasourcesList[i][common.getKey(component,"ds_type__c")] == 'O'){
                        outputdatasetMap.push({'Id':datasourcesList[i][common.getKey(component,"dataset_id__c")],
                            'tableName':datasourcesList[i][common.getKey(component,"Table_Display_Name__c")]});
                    }
                    else{
                        dataSetMap.push({'Id':datasourcesList[i][common.getKey(component,"dataset_id__c")],
                            'tableName':datasourcesList[i][common.getKey(component,"Table_Display_Name__c")]});
                     
                        var a = datasourcesList[i][common.getKey(component,"Table_Display_Name__c")];
                        var b = datasourcesList[i][common.getKey(component,"table_name__c")] ;
                        dataMap[a] = b;
                    }
                }

                component.set('v.dataMap',dataMap);
                console.log('dataMap::: ',dataMap);

                if(dataSetMap.length == 2 && outputdatasetMap.length == 1)
                {

                    component.set("v.dataSetMap", dataSetMap);
                    component.set("v.outputdatasetMap", outputdatasetMap);

                    component.getFieldsList();

                    var actionBusinessRuleId = component.get("c.getBusinessRuleId");
                    actionBusinessRuleId.setParams({ 
                        scenarioId : scenarioId,
                        scenarioRuleInstanceId : scenarioRuleInstanceId
                    });

                    actionBusinessRuleId.setCallback(this, function(data){
                        var response= data.getReturnValue();
                        console.log('business Rule Id:: '+response);
                        if(response == null){
                            console.log('No existing business Rule found');
                            component.set("v.updateFlag","N");
                        }
                        else{
                            component.set("v.updateFlag","Y");
                            console.log(response);
                            component.set('v.businessRuleId',response);
                            if(component.get("v.businessRuleId") != '' || component.get("v.businessRuleId") != undefined){
                                component.whereClausValues();
                                component.initializesavedValues();
                            }
                        } 
                    });
                    $A.enqueueAction(actionBusinessRuleId);
                }
                else
                {
                    component.showErrorToast();
                }
            }
            else {
                console.log('error state :: '+state);
                console.log('error :: '+ response.getReturnValue());
            }   
        });

        $A.enqueueAction(action);
    },

    showErrorToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        var dsnotavailable = $A.get("$Label.c.Datasources_Not_Available");
        toastEvent.setParams({
            message:dsnotavailable,
            messageTemplate: dsnotavailable,
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
    },

    showToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        var duplicateFields = $A.get("$Label.c.Duplicate_Join_Fields_cannot_be_applied");
        toastEvent.setParams({
            message:duplicateFields,
            messageTemplate: duplicateFields,
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
    },

    showSuccessToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        var changessaved = $A.get("$Label.c.changes_successfully_saved");
        toastEvent.setParams({
            message: changessaved,
            messageTemplate: changessaved,
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
    },
      
    showErrorSavingToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        var errorValues = $A.get("$Label.c.Error_Saving_Values_Please_Contact_System_Admin");
        toastEvent.setParams({
            message:errorValues,
            messageTemplate: errorValues,
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
    },

    initializesavedValues : function(component,event,helper){

        var actionRuleName = component.get("c.getinitializedValues");
        actionRuleName.setParams({ 
            businessRuleId : component.get("v.businessRuleId"),
            firstdefaultdatasource :component.get("v.dataSetMap")[0].tableName,
            seconddefaultdatasource :component.get("v.dataSetMap")[1].tableName
        });

        actionRuleName.setCallback(this, function(data){
        try{
            var joinexpobj= data.getReturnValue(); 
            console.log('joinexpobj', joinexpobj);

            var whereValue = component.get("v.whereVal");
            console.log("whereVal in initializesavedvalues" +whereValue);
            if(joinexpobj != null && joinexpobj != undefined && joinexpobj.defaultexpression!=null && joinexpobj.defaultexpression!=undefined)
            {
                if(whereValue!='null' && whereValue!=undefined)
                {
                    component.set("v.defaultexpression",joinexpobj.defaultexpression +' where '+whereValue); 
                }else{
                    component.set("v.defaultexpression",joinexpobj.defaultexpression);
                }
            }

            component.set("v.executeExpression",joinexpobj.executeexpression);
            component.find('ruleName').set('v.value',joinexpobj.ruleName);
            component.createPicklists(joinexpobj.selectedPicklistValues);
            component.get("v.picklistValuesList");
            var expression = component.get("v.defaultexpression");
            //COMPONENT ERROR FIX
            if( expression == null || $A.util.isUndefined(expression) ){
                return;
            }
                  
            if(expression.includes('INNER JOIN')) {
                var cmpTarget = component.find('InnerJoin');
                $A.util.addClass(cmpTarget, "slds-is-selected");
                $A.util.addClass(cmpTarget, "active");
                component.set("v.previousJointypeval",'INNER JOIN');

            }
            else if(expression.includes('LEFT JOIN')){
                var cmpTarget = component.find('leftJoin');
                $A.util.addClass(cmpTarget, "slds-is-selected");
                $A.util.addClass(cmpTarget, "active");
                component.set("v.previousJointypeval",'LEFT JOIN');
            }
            else if(expression.includes('RIGHT JOIN')){
                var cmpTarget = component.find('rightJoin');
                $A.util.addClass(cmpTarget, "slds-is-selected");
                $A.util.addClass(cmpTarget, "active");
                component.set("v.previousJointypeval",'RIGHT JOIN');
            }
            else if(expression.includes('FULL OUTER JOIN')){
                var cmpTarget = component.find('fullOuterJoin');
                $A.util.addClass(cmpTarget, "slds-is-selected");
                $A.util.addClass(cmpTarget, "active");
                component.set("v.previousJointypeval",'FULL OUTER JOIN');
            }

            console.log('defaultexpression :: '+component.get("v.defaultexpression"));
            console.log('executeexpression :: '+component.get("v.executeExpression"));
            console.log('ruleName :: '+component.find('ruleName').get("v.value"));
            console.log('picklistValuesList :: ');
            console.log(component.get("v.picklistValuesList"));

        }catch(e){}
        
        })
        $A.enqueueAction(actionRuleName);
    },

    createPicklists : function (component,event,helper)
    {
        var params = event.getParam('arguments');
        var valuesList = [];
        if(params)
            valuesList = params.valuesList;

        console.log('--->>>>>valuesList:::');
        console.log(valuesList);

        var picklistValuesList = component.get("v.picklistValuesList");
        if(picklistValuesList == null)
            picklistValuesList = {};
        
        var listedItems = component.get("v.listedItems");
        var secondlistedItems = component.get("v.secondlistedItems");
        var i=1;
        for(i;i<=valuesList.length;i+=2)
        {
            picklistValuesList[''+(i)] = valuesList[i-1];
            picklistValuesList[''+(i+1)] = valuesList[i];
            var newComponents = [];
            newComponents.push(["aura:html", {
                "tag": "div",
                "HTMLAttributes": {
                    "class": "slds-grid slds-p-top_small  slds-p-right_xx-large addRow rowNo_"+i,
                }
            }]);

            newComponents.push(["aura:html", {
                "tag": "div",
                "HTMLAttributes": {
                    "class": "slds-frame slds-size--6-of-12 slds-p-right_x-small",
                }
            }]);
            
            newComponents.push(["aura:html", {
                "tag": "div",
                "HTMLAttributes": {
                    "class": "slds-frame slds-size--6-of-12",
                }
            }]);
           
            newComponents.push(["aura:html", {
                "tag": "div",
                "HTMLAttributes": {
                    "class": "slds-frame slds-size--1-of-12 del_btn button_"+i,
                    "onclick":component.getReference("c.removeRow"),
                    "aura:id": "buttonremove_"+1
                }
            }]);


            newComponents.push(["aura:html", {
                "tag": "div",
                "HTMLAttributes": {
                    "class": "slds-form-element__control"
                }
            }]);

            newComponents.push(["aura:html", {
                "tag": "div",
                "HTMLAttributes": {
                    "class": "slds-form-element__control"
                }
            }]);

            newComponents.push(["aura:html", {
                "tag": "button",
                "HTMLAttributes": {
                    "class": "slds-button slds-m-left_none slds-m-right_none slds-button--icon-border "
                }
            }]);
      
            newComponents.push(["lightning:icon", {
                "iconName": "utility:dash",
                "size": "x-small",
                "class": "slds-button slds-m-left_none slds-m-right_none slds-button--icon-border",
            }]);

            newComponents.push(["c:strike_picklist", {
                "name":"FDSelect_"+i ,
                "class":"label_remove baseScenario", 
                "label":"" ,
                "aura:id":"fd_select_"+i,
                "value":valuesList[i-1],
                "searchable":"true",
                "placeholder":"Select Field",
                "onchange":component.getReference("c.onFieldSelection") 
            }]);
            
            newComponents.push(["c:strike_picklist", {
                "name":"FDSelect_"+(i+1) ,
                "class":"label_remove baseScenario", 
                "label":"" ,
                "aura:id":"fd_select_"+(i+1),
                "value":valuesList[(i)],
                "searchable":"true",
                "placeholder":"Select Field",
                "onchange":component.getReference("c.onFieldSelection") 
            }]);

            for(var listIndex = 0;listIndex<listedItems.length;listIndex++)
            {
                newComponents.push(["c:strike_option",
                {
                    "value":listedItems[listIndex].value,
                    "label":listedItems[listIndex].value
                }]);
            }

            for(var listIndex = 0;listIndex<secondlistedItems.length;listIndex++)
            {
                newComponents.push(["c:strike_option",
                {
                    "value":secondlistedItems[listIndex].value,
                    "label":secondlistedItems[listIndex].value
                }]);
            }

            $A.createComponents(
              newComponents,
              function (components, status, errorMessage) 
              {
                 if (status === "SUCCESS") 
                 {
                     console.log(status);
                     console.log('inside create component');
                     var joincontainer = component.find('joincontainer');
                     var pageBody = joincontainer.get("v.body");
                     // for (var i = 0; i < components.length; i += 3) 
                     // {
                           var outerDiv = components[0];
                           var innerdiv1 = components[1];
                           var innerdiv11 = components[2];
                           var innerdiv111 = components[3]
                           var innerdiv2 = components[4];
                           var innerdiv22 = components[5];
                           var removeButton = components[6];
                           var ligthningIcon = components[7];
                           var strikePicklist = components[8];
                           var strikePicklist2 = components[9];
                           var strikePicklistBody = strikePicklist.get("v.body");
                           var i = 10;
                           for(i=10;i<listedItems.length+10;i++)
                           {
                              var strikeOption = components[i];
                              strikePicklistBody.push(strikeOption);
                           }
                         
                         strikePicklist.set("v.body",strikePicklistBody);
                         var innerDiv2Body = innerdiv2.get("v.body");
                         innerDiv2Body.push(strikePicklist);
                         innerdiv2.set("v.body",innerDiv2Body);
                         
                         var innerDiv1Body = innerdiv1.get("v.body");
                         innerDiv1Body.push(innerdiv2);
                         innerdiv1.set("v.body",innerDiv1Body);


                         var strikePicklistBody2 = strikePicklist2.get("v.body");
                         for(var j=i;j<secondlistedItems.length+i;j++)
                         {
                            var strikeOption2 = components[j];
                            strikePicklistBody2.push(strikeOption2);
                         }
                         
                         strikePicklist2.set("v.body",strikePicklistBody2);
                         var innerDiv22Body = innerdiv22.get("v.body");
                         innerDiv22Body.push(strikePicklist2);
                         innerdiv22.set("v.body",innerDiv22Body);
                         
                         var innerDiv11Body = innerdiv11.get("v.body");
                         innerDiv11Body.push(innerdiv22);
                         innerdiv11.set("v.body",innerDiv11Body);

                          var removeButtonBody = removeButton.get("v.body");
                          removeButtonBody.push(ligthningIcon);
                          removeButton.set("v.body",removeButtonBody);
                          var innerDiv111Body = innerdiv111.get("v.body");
                          innerDiv111Body.push(removeButton);
                          innerdiv111.set("v.body",innerDiv111Body);

                         var outerDivBody = outerDiv.get("v.body");
                         outerDivBody.push(innerdiv1);
                         outerDivBody.push(innerdiv11);
                         outerDivBody.push(innerdiv111);
                         
                         outerDiv.set("v.body", outerDivBody);
                         
                         pageBody.push(outerDiv);
                    
                     joincontainer.set("v.body", pageBody);
                  }   
              }); 
        }
        component.set("v.picklistValuesList",picklistValuesList);
        console.log('in create picklists---->>>');
        console.log(picklistValuesList);
        console.log(component.get("v.picklistValuesList"));
        component.set("v.rowIndex",i)
        
      },

      getchosenfields : function(component,event,helper){

        console.log('inside get chosen fields');
        var scenarioRuleInstanceId = component.get('v.scenarioRuleInstanceId');
        var action = component.get("c.getupdatedfields");
              action.setParams({ 
                  "scenarioRuleInstanceId" : scenarioRuleInstanceId
              });
              action.setCallback(this, function(data){
                  var wrapperList= data.getReturnValue(); 
                  console.log('wrapperList');
                  console.log(wrapperList);
                  component.set('v.JoinFieldsWrapperList',wrapperList);
                  
                   component.createMetaDataTable();                   
              })
              $A.enqueueAction(action);
      },

      getFieldsList : function(component,event,helper){
        
     
         var fieldDataTypeMap = {};

      	 var action = component.get("c.callDatasourceFields");
      	 action.setParams({
      	 	selectedId : component.get("v.dataSetMap")[0].Id
      	 });
              action.setCallback(this, function(response) {
                  var state = response.getState();
                  if(state === 'SUCCESS'){
                      console.log('inside callDatasourceFields');
                           var fieldsList = response.getReturnValue();
                           console.log('fieldsList');
                           console.log(fieldsList);
                           var ns = component.get("v.namespace");

                     		component.set("v.fieldsList",fieldsList);
                           var Opts=[];
          	for(var i=0;i<fieldsList.length;i++){
          		Opts.push({'value':fieldsList[i][common.getKey(component,"ds_col_name__c")],'label':fieldsList[i][common.getKey(component,"ds_col_name__c")],
                'datatype':fieldsList[i][common.getKey(component,"datatype__c")]});

             var fieldName = fieldsList[i][common.getKey(component,"ds_col_name__c")];
             var dataType = fieldsList[i][common.getKey(component,"datatype__c")] ;
             fieldDataTypeMap[fieldName] = dataType;
          	}
          	console.log('Opts');
          	console.log(Opts);

             component.set('v.fieldDataTypeMap',fieldDataTypeMap);

             console.log('fieldDataTypeMap ::: '+fieldDataTypeMap);
         
          	component.set("v.listedItems", Opts);	
            component.getSecondDatasourceFieldsList();
            
             }else{
                        console.log("Failed with state:  "+ state);
                  }
              });
              
              $A.enqueueAction(action);


      },

      getSecondDatasourceFieldsList : function(component,event,helper){

         var ns = component.get("v.namespace");
         var secondFieldDataTypeMap = {};
        // var secondDsfieldId = component.get("v.seconddatasoureselectedId");
         var action = component.get("c.callDatasourceFields");
         action.setParams({
          selectedId : component.get("v.dataSetMap")[1].Id
         });
              action.setCallback(this, function(response) {
                  var state = response.getState();
                  if(state === 'SUCCESS'){
                           var fieldsList = response.getReturnValue();
              
                        component.set("v.fieldsList",fieldsList);
                           var Opts=[];
            for(var i=0;i<fieldsList.length;i++){
              Opts.push({'value':fieldsList[i][common.getKey(component,"ds_col_name__c")],'label':fieldsList[i][common.getKey(component,"ds_col_name__c")],
             'datatype':fieldsList[i][common.getKey(component,"datatype__c")]});

             var secondDsFieldName = fieldsList[i][common.getKey(component,"ds_col_name__c")];
             var secondDsDataType = fieldsList[i][common.getKey(component,"datatype__c")] ;
             secondFieldDataTypeMap[secondDsFieldName] = secondDsDataType;
            }
            console.log('Opts');
            console.log(Opts);

             component.set('v.secondFieldDataTypeMap',secondFieldDataTypeMap);

             console.log('secondFieldDataTypeMap ::: '+secondFieldDataTypeMap);
         
            component.set("v.secondlistedItems", Opts); 
              
                          
                  }else{
                        console.log("Failed with state:  "+ state);
                  }
              });
              
              $A.enqueueAction(action);


      },

      onFieldSelection : function(component,event,helper)
      {

      	console.log('first DS field select click');
      	var picklistValuesList = component.get("v.picklistValuesList");
      	if(picklistValuesList == null)
      		picklistValuesList = {};
      	var index = event.getSource().get("v.name").split('_')[1];
      	picklistValuesList[index] = event.getSource().get("v.value");
      	component.set("v.picklistValuesList",picklistValuesList);
      	var rowIndex = component.get("v.rowIndex");
      		console.log("fieldName ::: ");console.log(picklistValuesList);
      },

       onSecondFieldSelection : function(component,event,helper){

      	console.log('second DS field select click');

      		var secondfieldName = event.getSource().get("v.value");
      		console.log("secondfieldName ::: "+secondfieldName);
      		var secondDS_fieldName = component.set("v.secondDSField",secondfieldName); 
      },

      addjoin : function(component,event,helper){

      	  var addjointype= $A.get('e.c:addjointype');
          var idx = event.currentTarget.getAttribute("data-attribute");
          console.log('idx :: '+idx);
          addjointype.setParams({'dataattribute':idx});
          addjointype.fire();
          component.set("v.jointypeval",idx);
          
          component.generateExpression();
          var previousJoinType = component.get("v.previousJointypeval");
          //COMPONENT ERROR FIX
          if( previousJoinType == null || $A.util.isUndefined(previousJoinType) ){
            return;
          }

          if(previousJoinType.includes('INNER JOIN')){
              var cmpTarget = component.find('InnerJoin');
               $A.util.removeClass(cmpTarget, "slds-is-selected");
                $A.util.removeClass(cmpTarget, "active");

          }else if(previousJoinType.includes('LEFT JOIN')){
            var cmpTarget = component.find('leftJoin');
              $A.util.removeClass(cmpTarget, "slds-is-selected");
              $A.util.removeClass(cmpTarget, "active");
          }else if(previousJoinType.includes('RIGHT JOIN')){
            var cmpTarget = component.find('rightJoin');
              $A.util.removeClass(cmpTarget, "slds-is-selected");
              $A.util.removeClass(cmpTarget, "active");
          }else if(previousJoinType.includes('FULL OUTER JOIN')){

              var cmpTarget = component.find('fullOuterJoin');
             $A.util.removeClass(cmpTarget, "slds-is-selected");
             $A.util.removeClass(cmpTarget, "active");
          }

      },

  	generateExpression : function(component,event,helper){
  	var ns = component.get("v.namespace");
  		var jointypeval = component.get("v.jointypeval");
      var whereValue = component.get("v.whereVal");
  		var selectedValue = component.get("v.dataSetMap")[0].tableName;
  		var dsselectedValue = component.get("v.dataSetMap")[1].tableName;
  		var selectedPicklistValues = component.get("v.picklistValuesList");
     if( selectedPicklistValues == null || selectedPicklistValues == undefined)
     {
        component.set("v.defaultexpression",'');    
     }
     
     else
     {

         var ds1 = component.get("v.dataSetMap")[0].Id;
      var ds2 = component.get("v.dataSetMap")[1].Id;
      console.log('selectedPicklistValues');
      console.log(selectedPicklistValues);
      
      var piclistValues = new Set();
      if( (selectedPicklistValues.length % 2) == 1){
        for(var index=1; index<selectedPicklistValues.length; index = index + 2){
          piclistValues.add(selectedPicklistValues[index] + '=' + selectedPicklistValues[index + 1]); 
        }
        console.log(((selectedPicklistValues.length -1) / 2));
        if( ((selectedPicklistValues.length -1) / 2) != piclistValues.size){
          component.showToast();
          component.set("v.defaultexpression",'');
        return;
        }
        
      }

       /* Data Type Check for Join Conditions */

      var fieldDataTypeMap = JSON.parse(JSON.stringify(component.get('v.fieldDataTypeMap')));
      console.log('fieldDataTypeMap :: ',fieldDataTypeMap);
      var secondFieldDataTypeMap = JSON.parse(JSON.stringify(component.get('v.secondFieldDataTypeMap')));
      console.log('secondFieldDataTypeMap :: ',secondFieldDataTypeMap);

      var dataTypeArray = [];
      var messageerror = '';

      for(var i=1;i<selectedPicklistValues.length - 1;i++){

        if(fieldDataTypeMap[selectedPicklistValues[i]] ==  secondFieldDataTypeMap[selectedPicklistValues[i + 1]]){
          dataTypeArray = [];
          component.set('v.dataTypeArray',dataTypeArray);
         continue;
        }
        else{
        messageerror = 'Selected fields '+selectedPicklistValues[i]+' & '+selectedPicklistValues[i + 1]+' data types do not match';
        dataTypeArray.push(messageerror);
        component.set('v.dataTypeArray',dataTypeArray);
        break;
        }
        
        
      }

      var defaultexpression = selectedValue +' '+ jointypeval +' '+ dsselectedValue +' '+ "ON "+ selectedValue +'.'+ selectedPicklistValues['1']+' = '+dsselectedValue+'.'+selectedPicklistValues['2'];
      if(selectedPicklistValues['1'] == undefined || selectedPicklistValues['2'] == undefined)
      {
          defaultexpression = '';
            if(whereValue!='null' && whereValue!=undefined)
            {
              component.set("v.defaultexpression",defaultexpression +' where '+whereValue);
            }
            else
            {
              component.set("v.defaultexpression",defaultexpression);  
            }

          //component.set("v.defaultexpression",defaultexpression);
          return;
      }
      
      var rowIndex = component.get("v.rowIndex");
      for(var i = 3;i<rowIndex;i++)
      {

        if(i%2 != 0)
        {        
          if(selectedPicklistValues[''+i] == undefined || selectedPicklistValues[''+(i+1)] == undefined)
            continue; 
          defaultexpression = defaultexpression + ' AND ' + selectedValue+'.'+  selectedPicklistValues[''+i];
        }
        else
        {
          if(selectedPicklistValues[''+i] == undefined)
            continue;
          defaultexpression = defaultexpression +' = '+dsselectedValue+'.'+selectedPicklistValues[''+i];
        }
      }

      if(whereValue!='null' && whereValue!=undefined)
      {
       component.set("v.defaultexpression",defaultexpression+' where '+whereValue);
      }else{
       component.set("v.defaultexpression",defaultexpression);
      }
      //component.set("v.defaultexpression",defaultexpression);
        console.log('defaultexpression:: '+defaultexpression);
        var tempPickListObj = {};
        for(var i=1;i<selectedPicklistValues.length;i++)
        {
          tempPickListObj[''+i] = selectedPicklistValues[i];
        }
        var picklist = JSON.parse(JSON.stringify(tempPickListObj));
        var finalPickListarray = [];
        for (var key in picklist) {
          finalPickListarray.push(picklist[key]);
        }

        var datalistArray = [];
        datalistArray.push(ds1);
        datalistArray.push(ds2);

        console.log(picklist);
        console.log('picklist');
        console.log('finalPickListarray');
        console.log(finalPickListarray);
          var action2=component.get('c.generateexecuteexpression');
                          action2.setParams({ 
                              "picklistValuesList" : finalPickListarray,
                              "datalistArray" : datalistArray
                          });
                          action2.setCallback(this, function(data){
                              var tablenamedetails= data.getReturnValue();
                              console.log('tablenamedetails');
                              console.log(tablenamedetails);

                              console.log('--dataMap');
                              console.log(component.get("v.dataMap"));

                              var executeExpression = defaultexpression;
                              for(var index=0; index < tablenamedetails.length; index++){
                                executeExpression = executeExpression.replace(tablenamedetails[index][common.getKey(component,"ds_col_name__c")], tablenamedetails[index][common.getKey(component,"tb_col_nm__c")]);
                              }

                              var dataSet = JSON.parse(JSON.stringify(component.get("v.dataMap")));

                              Object.keys( dataSet ).forEach(function( key ){
                                executeExpression = executeExpression.replace(new RegExp(key, 'gi'),dataSet[key]);
                              });

                              
                             console.log('--executeExpression');
                             console.log(executeExpression);
                             component.set("v.executeExpression",executeExpression);

                          })
                          
                          $A.enqueueAction(action2); 

     }
     
  		},

        closeButton : function(component, event, helper) {

           // $j = jQuery.noConflict();
            //$j('.deleteClassPopOver').removeClass('slds-hide');
           var confirmBox = component.find("deletepopover");
          $A.util.removeClass(confirmBox, "slds-hide");
          var backdropPopup = component.find("backdropPopup");
          $A.util.removeClass(backdropPopup, "slds-hide");
          // console.log('Inside close');
          // component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
          //         var scenarioId = component.get('v.scenarioId');
          //         var workSpaceId = component.get('v.workSpaceId');
          //         var evt = $A.get("e.force:navigateToComponent");
          //            evt.setParams({
          //               componentDef : "c:CPGbusinessRuleCanvas",
          //               componentAttributes: {
          //             record_Id : scenarioId,
          //             viewcanvas : "Y",
          //             workSpaceId : workSpaceId,
          //             summarizedViewFlag : component.get("v.summarizedViewFlag")
          //               }
          //           });
          //           evt.fire();
                  
      },

       okayCancel : function(component, event, helper) {

        console.log('Inside close');
          component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
                  var scenarioId = component.get('v.scenarioId');
                  var workSpaceId = component.get('v.workSpaceId');
                  var evt = $A.get("e.force:navigateToComponent");
                     evt.setParams({
                        componentDef : "c:CPGbusinessRuleCanvas",
                        componentAttributes: {
                      record_Id : scenarioId,
                      viewcanvas : "Y",
                      workSpaceId : workSpaceId,
                      summarizedViewFlag : component.get("v.summarizedViewFlag")
                        }
                    });
                    evt.fire();

       },

       cancelCancel:function(component, event, helper) {
         var confirmBox = component.find("deletepopover");
          $A.util.addClass(confirmBox, "slds-hide");
           var backdropPopup = component.find("backdropPopup");
          $A.util.addClass(backdropPopup, "slds-hide");
    },

      fieldsTabSelect : function(component,event,helper){

      	alert('tab selected');
      },

      onJoinTabSelect : function(component,event,helper){
        var showsave = component.find("outputButton");
         $A.util.removeClass(showsave,'slds-hide');
          var applyJoin = component.find("applyjoinButton");
        $A.util.removeClass(applyJoin,'slds-hide');


      },

      tabClick:function(component,event,helper)
      {
        var showsave = component.find("outputButton");
        $A.util.addClass(showsave,'slds-hide');

        var applyJoin = component.find("applyjoinButton");
        $A.util.addClass(applyJoin,'slds-hide');

        var ruleName = component.find('ruleName').get('v.value');
        ruleName = ruleName != undefined ? ruleName.trim() : '';
        var defaultexpression = component.get('v.defaultexpression');
        defaultexpression = defaultexpression != undefined ? defaultexpression : '';

        if(component.get("v.updateFlag") == "N"){
          var selectedPicklistValues = component.get("v.picklistValuesList");
          console.log('selectedPicklistValues length::: '+selectedPicklistValues.length);
          component.doPageValidation();
          var firstdatasource = component.get("v.dataSetMap")[0].Id;
          var seconddatasource = component.get("v.dataSetMap")[1].Id;
          var executeexpression = component.get("v.executeExpression");
          var tempPickListObj = {};
          for(var i=1;i<selectedPicklistValues.length;i++)
          {
            tempPickListObj[''+i] = selectedPicklistValues[i];
          }

          if(defaultexpression == '' || ruleName == '' || selectedPicklistValues.length % 2 == 0 || component.get('v.dataTypeArray').length > 0)
          {
            component.find("tabs").set("v.selectedTabId", "join");
          }
          else
          {
            var stringifiedpicklistvalues = JSON.stringify(tempPickListObj);
            var picklistdata = [];
            console.log('stringifiedpicklistvalues--------->');
            console.log(stringifiedpicklistvalues);
            picklistdata.push(stringifiedpicklistvalues);
            component.set('v.stringifiedpicklistvalues',stringifiedpicklistvalues);
            console.log('picklistdata');
            console.log(picklistdata);
            console.log('picklistdata length:::: '+picklistdata.length);

            if(ruleName != '' && defaultexpression != ''){
              var action=component.get('c.getJoinExpression');
              action.setParams({ 
                  "ruleName" : ruleName,
                  "defaultexpression" : defaultexpression,
                  "selectedPicklistValues" : component.get('v.stringifiedpicklistvalues'),
                  "firstdatasource" : firstdatasource,
                  "seconddatasource" : seconddatasource,
                  "executeexpression" : executeexpression
              });
              action.setCallback(this, function(data){
                var expressionvalues= data.getReturnValue();
                console.log('expressionvalues');
                console.log(expressionvalues);
                component.set('v.JoinExpressionWrapperList',expressionvalues);
              })
              $A.enqueueAction(action);    
            }
            
            var scenarioRuleInstanceId = component.get('v.scenarioRuleInstanceId');
            console.log('scenarioRuleInstanceId :: '+scenarioRuleInstanceId);
            console.log('inside tab click');

            var DS1selectedId = component.get("v.dataSetMap")[0].Id;
            var DS2selectedId = component.get("v.dataSetMap")[1].Id;
            
            console.log('DS1selectedId :: '+DS1selectedId);
            console.log('DS2selectedId :: '+DS2selectedId);

            var action2=component.get('c.getColumns');
            action2.setParams({ 
              "scenarioRuleInstanceId" : scenarioRuleInstanceId,
              "DS1selectedId" : DS1selectedId,
              "DS2selectedId" : DS2selectedId
            });
            action2.setCallback(this, function(data){
              var lstColumnData= data.getReturnValue();
              console.log('lstColumnData');
              console.log(component.get("v.JoinFieldsWrapperList"));
              if(lstColumnData.length > 0){
              component.set('v.JoinFieldsWrapperList',lstColumnData);
                component.createMetaDataTable();
              }
            })
            $A.enqueueAction(action2);
          }
        }

        else if(component.get("v.updateFlag") == "Y")
        {
          var selectedPicklistValues = component.get("v.picklistValuesList");
          console.log('selectedPicklistValues length::: '+selectedPicklistValues.length);
          var action=component.get('c.getJoinExpression');
          action.setParams({ 
            "ruleName" : ruleName,
            "defaultexpression" : defaultexpression,
            "selectedPicklistValues" : JSON.stringify(component.get("v.picklistValuesList")),
            "firstdatasource" : component.get("v.dataSetMap")[0].Id,
            "seconddatasource" : component.get("v.dataSetMap")[1].Id,
            "executeexpression" : component.get("v.executeExpression")
          });
          action.setCallback(this, function(data){
            var expressionvalues= data.getReturnValue();
            console.log('expressionvalues');
            console.log(expressionvalues);
            component.set('v.JoinExpressionWrapperList',expressionvalues);
            if(defaultexpression == '' || ruleName == '' || selectedPicklistValues.length % 2 == 0 || component.get('v.dataTypeArray').length > 0)
            {
              component.find("tabs").set("v.selectedTabId", "join");
              component.updateValidation();
            }
            else{
              component.updateValidation();
              component.getchosenfields();
            }
          })
          $A.enqueueAction(action); 
        }    
      },

       createMetaDataTable : function(component, event, helper)
      {
          console.log('Inside Required Function');
          var sessionTable;
          var $j = jQuery.noConflict();
          var columnDetails = component.get("v.JoinFieldsWrapperList");
          console.log('columnDetails');
          console.log(columnDetails);
          
          var result = columnDetails;
          var coulmnJsonResult = [];
          var updateFlag = component.get("v.updateFlag");

          	var parsedResult = JSON.parse(JSON.stringify( result, ["statusFlag","datasourceName","datasourceColumnName"] , 3)); 
              var element = parsedResult[0];
              for(var key in element){
              	if(key=='statusFlag'){
                      coulmnJsonResult.push({"data":key,"title":'',defaultContent:''});
                 }
                
                 else if(key=='datasourceName'){
                      coulmnJsonResult.push({"data":key,"title":'DataSource',defaultContent:''});
                 }
                 else if(key=='datasourceColumnName'){
                      coulmnJsonResult.push({"data":key,"title":'Fields',defaultContent:''});
                 }
               
                 console.log('coulmnJsonResult ::: ');
                 console.log(coulmnJsonResult);
                  
              }
              console.log(result);
              console.log($j('#table-2'));

             


              sessionTable = $j('.slds-table').DataTable(
              {
                  
                  "searching": false,
                   destroy : true,
                  "dom": '<"div-tbl">t<"bottom-info"> ', 
                  columns:coulmnJsonResult,
                  "data": result,
                   paging:false,
                  'orderable': false,
                  "language":
                  {
                      "emptyTable": "No Data Available"
                  },
                  "createdRow": function( row, data, dataIndex ) {
                    if(!data.isVisible){
                        $j(row).addClass('slds-hide'); 
                    }
                  },
                 
                  columns:coulmnJsonResult,
                  columnDefs :
                  [
                            
                              {
                                  targets : [0],
                                  "visible" : true,
                                  'searchable': false,
                                  'orderable': false,
                                  "createdCell":function (td, cellData, rowData, row, col)
                                  {   
                                        var parsehtml;
                                        if(rowData.statusFlag == true){
                                           parsehtml="<input type='checkbox' name='options' id='checkbox_"+row+"' disabled='true' checked='checked' value='" + rowData.statusFlag + "' />";
                                        }
                                        else if(rowData.statusFlag == false){
                                           parsehtml="<input type='checkbox' name='options' id='checkbox_"+row+"' disabled='true' value='" + rowData.statusFlag + "'/>";
                                        }
                                        $j(td).html(parsehtml);  
                                        
                                  }
                              },
                              {
                                targets : [1],
                                "createdCell":function (td, cellData, rowData, row, col)
                                  {
                                        var tdx = rowData.datasourceName;   
                                        $j(td).html(tdx);  
                                  }
                              },

                               {
                                targets : [2],
                                "createdCell":function (td, cellData, rowData, row, col)
                                  {   
                                      $j(td).html(rowData.datasourceColumnName);
                                  }
                              }
                               
                            ],
                  
              });



      },

       restrictInputsRule :function(component, event, helper){
          var ruleName=component.find("ruleName");
          var text=ruleName.get('v.value'); 
          var newtext = text.replace(/(?!^-)[^0-9a-zA-Z_\s]/g, ''); 
          //component.set(ruleName, newtext);        
          ruleName.set('v.value', newtext); 
          console.log('newtext value :: '+newtext);   
      },

     
      

      saveUpdateFieldValues : function(component,event,helper){

       var isValid = component.get("v.isValid");
       if(isValid == 'false') return;
      	console.log('inside Join Fields Save method');
        var ruleName = component.find('ruleName').get('v.value');
        var scenarioRuleInstanceId = component.get('v.scenarioRuleInstanceId');
        var scenarioId = component.get('v.scenarioId');
        var jointypeval = component.get("v.jointypeval");
        console.log('rule Name::: '+ruleName);
        console.log('scenarioRuleInstanceId::: '+scenarioRuleInstanceId);
        console.log('scenarioId::: '+scenarioId);
        component.updateJoinFieldsWrapper();
        var $j=jQuery.noConflict();
        var joinFieldsWrapperupdatedvalue = component.get("v.JoinFieldsWrapperList");
      
        var JoinExpressionWrapperList = component.get('v.JoinExpressionWrapperList');
        if(JoinExpressionWrapperList == undefined || JoinExpressionWrapperList[0] == undefined || JoinExpressionWrapperList.length == 0){
                var toastEvent = $A.get("e.force:showToast");
                var duplicateFields = $A.get("$Label.c.WARNING_JOIN_SELECT_FIELD");
                toastEvent.setParams({
                    message:duplicateFields,
                    messageTemplate: duplicateFields,
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'warning',
                    mode: 'pester'
                });
                toastEvent.fire();
            
        }else{

            JoinExpressionWrapperList[0].defaultexpression = JoinExpressionWrapperList[0].defaultexpression.split('where')[0];
            component.set("v.JoinExpressionWrapperList",JoinExpressionWrapperList);

            console.log('joinFieldsWrapper after updating checkboxes :: ');
            console.log(component.get("v.JoinFieldsWrapperList"));

            if(component.get("v.updateFlag") == "N"){

            var action = component.get("c.createJoinBusinessRule");
             action.setParams({
              ruleName : ruleName,
              scenarioRuleInstanceId : scenarioRuleInstanceId,
              scenarioId : scenarioId,
              joinFieldsWrapper :  JSON.stringify(joinFieldsWrapperupdatedvalue),
              JoinExpressionWrapperList : JSON.stringify(JoinExpressionWrapperList),
              jointypeval : jointypeval

             });
                  action.setCallback(this, function(response) {
                      var state = response.getState();
                      if(state === 'SUCCESS')
                      {
                         var finaljoinexpressionList = response.getReturnValue();
                           console.log('finaljoinexpressionList');
                           console.log(finaljoinexpressionList);
                           console.log(finaljoinexpressionList.length); 
                           component.showSuccessToast();    
                          component.okayCancel();                                  
                      }
                      else{
                        component.showErrorSavingToast();
                          }
                      });
                  
                  $A.enqueueAction(action);
                }

                else{
                         /* In case of saving the updated values for Join */
                        var businessRuleId = component.get('v.businessRuleId');
                        var joinFieldsWrapperupdatedvalue = component.get("v.JoinFieldsWrapperList");
                        var ruleName = component.find('ruleName').get('v.value');
                        var JoinExpressionWrapperList = component.get('v.JoinExpressionWrapperList');
                        JoinExpressionWrapperList[0].defaultexpression = JoinExpressionWrapperList[0].defaultexpression.split('where')[0];
                         component.set("v.JoinExpressionWrapperList",JoinExpressionWrapperList);
                        console.log('joinFieldsWrapperupdatedvalue');
                        console.log(joinFieldsWrapperupdatedvalue);
                        console.log('JoinExpressionWrapperList');
                        console.log(JoinExpressionWrapperList);
                        var selectedPicklistValues = JoinExpressionWrapperList[0].selectedPicklistValues;
                        var tempPickListObj = {};
                        for(var i=1;i<=selectedPicklistValues.length;i++)
                        {
                          tempPickListObj[''+i] = selectedPicklistValues[i-1];
                        }
                        var tempList = [];
                        tempList.push(JSON.stringify(tempPickListObj));
                        JoinExpressionWrapperList[0].selectedPicklistValues = tempList;
                        console.log('JoinExpressionWrapperList222');
                        console.log(JoinExpressionWrapperList);
                        var action2 = component.get("c.saveUpdatedValues");
                         action2.setParams({
                          joinFieldsWrapper :  JSON.stringify(joinFieldsWrapperupdatedvalue),
                          JoinExpressionWrapperList : JSON.stringify(JoinExpressionWrapperList),
                          businessRuleId : businessRuleId,
                          scenarioId : scenarioId,
                          jointypeval : jointypeval,
                          scenarioRuleInstanceId : scenarioRuleInstanceId,
                          ruleName : ruleName,
                          whereClaus : component.get("v.whereFull")
                         });
                          action2.setCallback(this, function(response) {
                            
                              var state = response.getState();
                              if(state === 'SUCCESS')
                              {
                                 var finalupdatedexpressionList = response.getReturnValue();
                                 console.log('finalupdatedexpressionList');
                                 console.log(finalupdatedexpressionList);
                                 if(response.getReturnValue() != null)
                                 {
                                  component.showErrorSavingToast();
                                 } 
                                 else
                                 {
                                 $j('.slds-table').DataTable().destroy();
                                 component.showSuccessToast();   
                                 component.okayCancel();
                                 } 
                              }         
                              else
                              {
                                console.log("Failed with state:  "+ state);
                              }
                          });
                          
                          $A.enqueueAction(action2);

                }

        }

      },

        updateJoinFieldsWrapper:function(component,event,helper){
            var $j = jQuery.noConflict();
            var joinFieldsWrapperList = component.get("v.JoinFieldsWrapperList");
            console.log('joinFieldsWrapper');
           // console.log(joinFieldsWrapper);
             var joinFieldsWrapper=JSON.parse(JSON.stringify(joinFieldsWrapperList));

             var myTable = $j('.slds-table').DataTable({
              "searching": false,
              "paging" : false,
              "destroy" : true,
              "info" : false,
             }

              );
             for(var ctr=0;ctr<joinFieldsWrapper.length;ctr++)
             {
                if($j(myTable.cell(ctr,0).node()).children('input').eq(0).prop('checked'))
                {
                  joinFieldsWrapper[ctr].statusFlag=true;
                  // myTable.cell(ctr,0).data(true);
                }
                else
                {
                  joinFieldsWrapper[ctr].statusFlag=false;
                }  
             }

             
             component.set('v.JoinFieldsWrapperList',joinFieldsWrapper);
             console.log("joinFieldsWrapper after updating ::: "+joinFieldsWrapper);
             console.log(joinFieldsWrapper);
      },

      addRow:function(component,event,helper)
      {
      	var rowIndex = component.get("v.rowIndex");
      	var listedItems = component.get("v.listedItems");
      	var secondlistedItems = component.get("v.secondlistedItems");
      	
      	 // Array of row to create
  		var newComponents = [];
  		var dict = [];

  		newComponents.push(["aura:html", {
  		   "tag": "div",
  		   "HTMLAttributes": {
  		       "class": "slds-grid slds-p-top_small  slds-p-right_xx-large addRow rowNo_"+rowIndex,
  		       
  		   }
  		   
  		}]);

  		newComponents.push(["aura:html", {
  		   "tag": "div",
  		   "HTMLAttributes": {
  		       "class": "slds-frame slds-size--6-of-12 slds-p-right_x-small",
  		   }
  		   
  		}]);
  		newComponents.push(["aura:html", {
  		   "tag": "div",
  		   "HTMLAttributes": {
  		       "class": "slds-frame slds-size--6-of-12",
  		   }
  		   
  		}]);
      newComponents.push(["aura:html", {
         "tag": "div",
         "HTMLAttributes": {
             "class": "slds-frame slds-size--1-of-12 del_btn button_"+rowIndex,
             "onclick":component.getReference("c.removeRow"),

         }
         
      }]);


  		newComponents.push(["aura:html",
  		                   {
  		                   	"tag": "div",
  		   "HTMLAttributes": {
  		       "class": "slds-form-element__control"
  		   }
  		                       
  		                   }]);
  		newComponents.push(["aura:html",
  		                   {
  		                   	"tag": "div",
  		   "HTMLAttributes": {
  		       "class": "slds-form-element__control"
  		   }
  		                       
  		                   }]);

      newComponents.push(["aura:html",
                         {
                          "tag": "button",
         "HTMLAttributes": {
             "class": "slds-button slds-m-left_none slds-m-right_none slds-button--icon-border "
         }

        }]);
      newComponents.push(["lightning:icon",
         {
             "iconName": "utility:dash",
             "size": "x-small",
             "class": "slds-button slds-m-left_none slds-m-right_none slds-button--icon-border ",
         }]);
  		newComponents.push(["c:strike_picklist",
         {
             "name":"FDSelect_"+rowIndex ,
             "class":"label_remove baseScenario", 
             "label":"" ,
             "aura:id":"fd_select_"+rowIndex,
              "value":"",
              "searchable":"true",
              "placeholder":"Select Field",
              "onchange":component.getReference("c.onFieldSelection") 
         }]);
  		newComponents.push(["c:strike_picklist",
         {
             "name":"FDSelect_"+(rowIndex+1) ,
             "class":"label_remove baseScenario", 
             "label":"" ,
             "aura:id":"fd_select_"+(rowIndex+1),
              "value":"",
              "searchable":"true",
              "placeholder":"Select Field",
              "onchange":component.getReference("c.onFieldSelection") 
         }]);
  		//var listIndex = 0;
  		for(var listIndex = 0;listIndex<listedItems.length;listIndex++)
  		{
  			newComponents.push(["c:strike_option",
  	       {
  	           "value":listedItems[listIndex].value,
  	           "label":listedItems[listIndex].value
  	            // "onchange:"{!c.onFieldSelection}" 
  	       }]);
  		}
  		for(var listIndex = 0;listIndex<secondlistedItems.length;listIndex++)
  		{
  			newComponents.push(["c:strike_option",
  	       {
  	          "value":secondlistedItems[listIndex].value,
  	           "label":secondlistedItems[listIndex].value
  	            // "onchange:"{!c.onFieldSelection}" 
  	       }]);
  		}	

  		$A.createComponents(
  		newComponents,
  		function (components, status, errorMessage) 
  		{
  		   if (status === "SUCCESS") 
  		   {
  		       console.log(status);
  		       console.log('inside create component');
  		       var joincontainer = component.find('joincontainer');
  		       var pageBody = joincontainer.get("v.body");
  		       
  		           var outerDiv = components[0];
  		           var innerdiv1 = components[1];
  		           var innerdiv11 = components[2];
                 var innerdiv111 = components[3]
  		           var innerdiv2 = components[4];
  		           var innerdiv22 = components[5];
                 var removeButton = components[6];
                 var ligthningIcon = components[7];
  		           var strikePicklist = components[8];
  		           var strikePicklist2 = components[9];
  		           var strikePicklistBody = strikePicklist.get("v.body");
  		           var i = 10;
  		           for(i=10;i<listedItems.length+10;i++)
  		           {
  		           		var strikeOption = components[i];
  		           		strikePicklistBody.push(strikeOption);
  		           }
  		           		           
  		           strikePicklist.set("v.body",strikePicklistBody);
  		           var innerDiv2Body = innerdiv2.get("v.body");
  		           innerDiv2Body.push(strikePicklist);
  		           innerdiv2.set("v.body",innerDiv2Body);
  		           
  		           var innerDiv1Body = innerdiv1.get("v.body");
  		           innerDiv1Body.push(innerdiv2);
  		           innerdiv1.set("v.body",innerDiv1Body);

  		           var strikePicklistBody2 = strikePicklist2.get("v.body");
  		           for(var j=i;j<secondlistedItems.length+i;j++)
  		           {
  		           		var strikeOption2 = components[j];
  		           		strikePicklistBody2.push(strikeOption2);
  		           }
  		          
  		           strikePicklist2.set("v.body",strikePicklistBody2);
  		           var innerDiv22Body = innerdiv22.get("v.body");
  		           innerDiv22Body.push(strikePicklist2);
  		           innerdiv22.set("v.body",innerDiv22Body);
  		           
  		           var innerDiv11Body = innerdiv11.get("v.body");
  		           innerDiv11Body.push(innerdiv22);
  		           innerdiv11.set("v.body",innerDiv11Body);
                 
                 var removeButtonBody = removeButton.get("v.body");
                 removeButtonBody.push(ligthningIcon);
                 removeButton.set("v.body",removeButtonBody);
                 var innerDiv111Body = innerdiv111.get("v.body");
                 innerDiv111Body.push(removeButton);
                 innerdiv111.set("v.body",innerDiv111Body);

  		           var outerDivBody = outerDiv.get("v.body");
  		           outerDivBody.push(innerdiv1);
  		           outerDivBody.push(innerdiv11);
                 outerDivBody.push(innerdiv111);
  		           
  		           outerDiv.set("v.body", outerDivBody);
  		           
  		           pageBody.push(outerDiv);
  		       
  		       joincontainer.set("v.body", pageBody);
  		    }   
  		});
  		rowIndex = rowIndex + 2;
  		component.set("v.rowIndex",rowIndex); 
  		  
      },

       doPageValidation:function(component, event, helper){
          console.log('doPageValidation');
          var errMessage='';

          var ruleName = component.find('ruleName').get('v.value');
          var defaultexpression = component.get("v.defaultexpression");
          var selectedPicklistValues = component.get("v.picklistValuesList");
          var dataTypeArray = component.get("v.dataTypeArray");
          if(ruleName==undefined || ruleName.trim() == ''){
              errMessage ='Please enter rule name';
              var sname = component.find("ruleName");
              $A.util.addClass(sname , 'errorInput');
            }
         
          else if(!defaultexpression || defaultexpression == undefined){
            errMessage = "Expression cannot be empty";
            var expname = component.find("expression");
            $A.util.addClass(expname , 'errorInput');
          }
           else if(selectedPicklistValues.length % 2 == 0){
            errMessage = "Combination of Join Fields is not Valid";
            var spname = component.find("selectvals");
            $A.util.addClass(spname , 'errorInput');
          }
          else if(dataTypeArray.length > 0){
            errMessage = dataTypeArray[0];
           var spname = component.find("selectvals");
           $A.util.addClass(spname , 'errorInput');
          }
          if(errMessage !=''){
             component.set('v.isValid','false');
             component.set('v.popoverMessage',errMessage);
             var errorIcn = component.find("errorIcon1");
             $A.util.removeClass(errorIcn,'slds-hide');

         }
          else{
             component.set('v.isValid','true');
             var errorIcn = component.find("errorIcon1");
            $A.util.addClass(errorIcn,'slds-hide');
          }
        
      },
      handleMouseEnter : function(component, event, helper) {
          var popover = component.find("popover");
          $A.util.removeClass(popover,'slds-hide');
      },
         //make a mouse leave handler here
      handleMouseLeave : function(component, event, helper) {
          var popover = component.find("popover");
          $A.util.addClass(popover,'slds-hide');
      },
      handleMouseEnter1 : function(component, event, helper) {
          var popover = component.find("popover1");
          $A.util.removeClass(popover,'slds-hide');
      },
         //make a mouse leave handler here
      handleMouseLeave1 : function(component, event, helper) {
          var popover = component.find("popover1");
          $A.util.addClass(popover,'slds-hide');
      },

        updateValidation:function(component, event, helper){
          console.log('doPageValidation');
          var errMessage='';
          var expressionwrapper = component.get("v.JoinExpressionWrapperList");
          var selectedPicklistValues = component.get("v.picklistValuesList");
          var dataTypeArray = component.get("v.dataTypeArray");

          console.log('expressionwrapper');
          console.log(expressionwrapper);

          var ruleName = component.find('ruleName').get('v.value');
          var defaultexp = expressionwrapper[0].defaultexpression;
          console.log('defaultexp::: '+defaultexp);
          if(ruleName == undefined || !ruleName.trim()){
              errMessage ='Please enter rule name';
              var sname = component.find("ruleName");
              $A.util.addClass(sname , 'errorInput');
            }
             
          else if(!defaultexp){
              errMessage ='Expression cannot be empty';
              var expname = component.find("expression");
              $A.util.addClass(expname , 'errorInput');
            }
          else if(selectedPicklistValues.length % 2 == 0){
            errMessage = "Combination of Join Fields is not Valid";
            var spname = component.find("selectvals");
            $A.util.addClass(spname , 'errorInput');
          }
          else if(dataTypeArray.length > 0){
            errMessage = dataTypeArray[0];
           var spname = component.find("selectvals");
           $A.util.addClass(spname , 'errorInput');
          }

          if(errMessage !=''){
             component.set('v.isValid','false');
             component.set('v.popoverMessage',errMessage);
             var errorIcn = component.find("errorIcon1");
             $A.util.removeClass(errorIcn,'slds-hide');

         }
          else{
             component.set('v.isValid','true');
             var errorIcn = component.find("errorIcon1");
             $A.util.addClass(errorIcn,'slds-hide');
          }
        
      },
      handleMouseEnter : function(component, event, helper) {
          var popover = component.find("popover");
          $A.util.removeClass(popover,'slds-hide');
      },
         //make a mouse leave handler here
      handleMouseLeave : function(component, event, helper) {
          var popover = component.find("popover");
          $A.util.addClass(popover,'slds-hide');
      },
      handleMouseEnter1 : function(component, event, helper) {
          var popover = component.find("popover1");
          $A.util.removeClass(popover,'slds-hide');
      },
         //make a mouse leave handler here
      handleMouseLeave1 : function(component, event, helper) {
          var popover = component.find("popover1");
          $A.util.addClass(popover,'slds-hide');
      },

      removeRow : function(component,event,helper){

       console.log('remove--->');
       var $j=jQuery.noConflict();
         console.log($j(".removerowcls .addRow"));
       var picklistValuesList = component.get("v.picklistValuesList");
         console.log(picklistValuesList);
       var buttonIdx = event.currentTarget.classList[3].split('_')[1];
      	 var rowIndex = component.get("v.rowIndex");
         console.log(rowIndex);
         $j(".removerowcls .rowNo_"+buttonIdx).remove();
         for(var i=buttonIdx;i<=rowIndex;i+=2)
         {
            $j(".removerowcls .rowNo_"+rowIndex).removeClass('rowNo_'+rowIndex).addClass('rowNo_'+(rowIndex-2));
            $j("button_"+rowIndex).removeClass('button_'+rowIndex).addClass('button_'+(rowIndex-2));
            picklistValuesList.splice(buttonIdx,1);
            picklistValuesList.splice(buttonIdx,1);
         }
      	 component.set('v.rowIndex',rowIndex - 2);
         component.set("v.picklistValuesList",picklistValuesList);  
      	 
         console.log('rowIndex :: '+rowIndex);
         console.log('picklistvaluesList--->');
         console.log(picklistValuesList);
         
      },

      whereClausValues : function(component,event,helper){

     console.log("whereClausValues called");
        var actionTabName = component.get("c.getQueryClauseDetails");
        actionTabName.setParams({ "businessRuleId" : component.get("v.businessRuleId")});
        actionTabName.setCallback(this, function(response){ 
            var stateTabName = response.getState();
            if(stateTabName==="SUCCESS")
            { 
                var result = response.getReturnValue();
                
                component.set("v.whereFull",result);
                component.set("v.whereVal", result.split(':')[0]); 
                component.set("v.whereExeVal",result.split(':')[1]);
                console.log("where execution Val" +result.split(':')[1]) ;
                console.log("where Values"+ result) ;  
            }
            else{
                console.log("Failed with state:  "+ state);
            }
        });
        $A.enqueueAction(actionTabName);
    },

      // fieldsTabValidationCheck : function(component,event,helper)
      // {
      //   console.log('--> fields tab focus');
      //   if(component.get("v.defaultexpression") == '' || component.get("v.defaultexpression") == null || component.get("v.value") == '' || component.get("v.value") == null)
      //   {
      //     component.find("tabs").set("v.selectedTabId", "join");
      //   }
      //   console.log(component.get("v.defaultexpression"));
      //   console.log(component.get("v.value"));
      //  // component.tabClick();
      // }
  })