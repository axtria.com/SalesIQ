({
afterloadscript:function(component,event,helper){
		//component.getQueryData();
		
},
getQueryData : function(component,event,helper)
{

	var existingblock = component.get("v.wrapperBlock");
	console.log(existingblock);
	
	var $j = jQuery.noConflict();
	console.log('in get builder');
	var idB = component.get('v.index');
     var filterJson = [];
     var columndetails = component.get("v.columnDetails");
     console.log('--columndetails---');
     console.log(columndetails);
     //component.getColumnValues();
     var arrcolumnValues = [];
     var selValues;
     for(var i=0;i<columndetails.length;i++){
         	if(columndetails[i].colDatatype=='Numeric'){
                


          //  console.log('--After service call--');
            filterJson.push({id:columndetails[i].tableColumnName,label:columndetails[i].tableColumnName,type:'double',operators: ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null','less','less_or_equal','greater','greater_or_equal'],values:{}});
			}else{
    	 	filterJson.push({id:columndetails[i].tableColumnName,label:columndetails[i].tableColumnName,type:'string',operators: ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'],value:{}});
    	 }

     }
     console.log('**JSON**');
     console.log(filterJson);
     //Check If existing rule

     var rules_basic;
     var checkExistingRule = component.get('v.checkRuleExists');
     console.log(checkExistingRule);
     if(checkExistingRule==false)
     {
     	console.log('in if-->>');
     	console.log(filterJson);
     	console.log(rules_basic);
     		$j('#filtergrp').queryBuilder('destroy');
	     	$j('#filtergrp').queryBuilder({
	     	filters: filterJson,
		    rules: rules_basic
	 	});
     }else
     {
     	rules_basic = JSON.parse(existingblock.criteriaLogic);
     	console.log('in else-->>');
     	console.log(filterJson);
     	console.log(rules_basic);
     	$j('#filtergrp').queryBuilder('destroy');
	     $j('#filtergrp').queryBuilder({
	     	filters: filterJson,
		    rules: rules_basic
		 }); 
     }
	 
	 
},
handleDisplayButton: function(component,event,helper){
	var $j = jQuery.noConflict();
	var idB = component.get('v.index');
	var isButtonDisplay = event.getParam("isdisplayButton");
	var sqlexpression;
	var blockJson;
	console.log('--button pressed--->'+isButtonDisplay);
	component.set('v.isDisplayPressed',isButtonDisplay);
	var isbuttonPressed = component.get('v.isDisplayPressed');
    if(isbuttonPressed){
     	var result = $j('#filtergrp').queryBuilder('getSQL');
     	if (result.sql.length) {
	    sqlexpression =  result.sql;
	    component.set('v.sqlExpression',sqlexpression);
	    blockJson = $j('#filtergrp').queryBuilder('getRules');
	  }
    }
     var wrapBlock= component.get('v.wrapperBlock'); 
     wrapBlock.criteriaLogic = JSON.stringify(blockJson, null, 2);
     wrapBlock.executionLogic = sqlexpression;
     console.log(wrapBlock);
     
},

getColumnValues: function(component,event,helper){
        var columndetails = component.get("v.columnDetails");
        var filterJson1= [];
        for(var i=0;i<columndetails.length;i++){
            if(columndetails[i].colDatatype=='Numeric' && columndetails[i].variabletype=='Source'){
            var columnName = columndetails[i].tableColumnName;
            var url = 'http://35.164.63.126/brms_dev?col='+columnName+'&table=t_fin_do3';
            console.log(url);
            var action = component.get("c.getTableContents");
            action.setParams
            ({
                "url" : url
            });
            action.setCallback(this, function(response){ 
            var result = response.getState();
            if(result==="SUCCESS")
            {
                
                arrcolumnValues = response.getReturnValue();
                console.log('--values--');
                console.log(arrcolumnValues);
                selValues = '{'
                for(var j=0;j<arrcolumnValues.length;j++){
                    selValues = selValues + j+':'+'\''+arrcolumnValues[j]+'\'';
                }
                selValues = selValues + '}';
                console.log(selValues);
            }
            else{
                console.log("Failed with state:  "+ result);
            }
        });
        $A.enqueueAction(action);
        }
    }
    console.log('Numeric array');
    console.log(filterJson1);
},

})