({
	
   restrictInputsField :function(component, event, helper){
    console.log("inside restrictInputsField"); 
    console.log(component.find("name_text"));
    var text=component.get("v.fieldName"); 
    text = text.replace(/(?!^-)[^0-9\w\s]/g, ''); 
    console.log(text); 
    component.set("v.fieldName", text);
    var globalID = (component.find('name_text').getGlobalId());
    var cursorPosition = document.getElementById(globalID);
    console.log(cursorPosition);
   
   },
   
   restrictInputsRule :function(component, event, helper){
    console.log("inside restrictInputsRule"); 
    console.log(component.find("name_text"));
    var text=component.get("v.ruleName"); 
    text = text.replace(/(?!^-)[^0-9a-zA-Z_\s]/g, ''); 
    console.log(text); 
    component.set("v.ruleName", text);
   
   },
         
   getNamespacePrefix: function(component, event, helper) {
    var orgNamespaceAction = component.get("c.getOrgNamespace");
    orgNamespaceAction.setCallback(this,function(response)
      {
        var state = response.getState();
          console.log('Namespace : ',response.getReturnValue());
          if(state == 'SUCCESS' && response.getReturnValue() != null)
          {
            component.set("v.namespace",response.getReturnValue());
            component.setLabels();
          }
      });
      $A.enqueueAction(orgNamespaceAction);

   },

   setLabels: function(component, event, helper) {
      console.log('Inside setLabels');
      var namespace = component.get("v.namespace");
      if(namespace != ''){
          component.set("v.comp_Derived_Field",$A.get("$Label.AxtriaSalesIQTM.comp_Derived_Field")); 
          component.set("v.gen_Rule_Name",$A.get("$Label.AxtriaSalesIQTM.gen_Rule_Name")); 
          component.set("v.gen_Field_Name",$A.get("$Label.AxtriaSalesIQTM.gen_Field_Name")); 
          component.set("v.gen_Data_Type",$A.get("$Label.AxtriaSalesIQTM.gen_Data_Type"));
          component.set("v.gen_Value_Type",$A.get("$Label.AxtriaSalesIQTM.gen_Value_Type"));
          component.set("v.gen_Fields",$A.get("$Label.AxtriaSalesIQTM.gen_Fields"));
          component.set("v.comp_Text_Area_Placeholder",$A.get("$Label.AxtriaSalesIQTM.comp_Text_Area_Placeholder")); 
          component.set("v.comp_Operator_Type",$A.get("$Label.AxtriaSalesIQTM.comp_Operator_Type"));
          component.set("v.comp_Operator",$A.get("$Label.AxtriaSalesIQTM.comp_Operator"));
          component.set("v.comp_Function_Details",$A.get("$Label.AxtriaSalesIQTM.comp_Function_Details"));
          component.set("v.comp_Syntax",$A.get("$Label.AxtriaSalesIQTM.comp_Syntax"));
          component.set("v.comp_Brief_Description",$A.get("$Label.AxtriaSalesIQTM.comp_Brief_Description"));
        }else{
          component.set("v.comp_Derived_Field",$A.get("$Label.c.comp_Derived_Field")); 
          component.set("v.gen_Rule_Name",$A.get("$Label.c.gen_Rule_Name")); 
          component.set("v.gen_Field_Name",$A.get("$Label.c.gen_Field_Name")); 
          component.set("v.gen_Data_Type",$A.get("$Label.c.gen_Data_Type"));
          component.set("v.gen_Value_Type",$A.get("$Label.c.gen_Value_Type"));
          component.set("v.gen_Fields",$A.get("$Label.c.gen_Fields"));
          component.set("v.comp_Text_Area_Placeholder",$A.get("$Label.c.comp_Text_Area_Placeholder")); 
          component.set("v.comp_Operator_Type",$A.get("$Label.c.comp_Operator_Type"));
          component.set("v.comp_Operator",$A.get("$Label.c.comp_Operator"));
          component.set("v.comp_Function_Details",$A.get("$Label.c.comp_Function_Details"));
          component.set("v.comp_Syntax",$A.get("$Label.c.comp_Syntax"));
          component.set("v.comp_Brief_Description",$A.get("$Label.c.comp_Brief_Description"));
        }

   },

	 doInit: function(component, event, helper) {
     component.showSpinner();    
    component.getNamespacePrefix();
	 var caretPos = 1;
	 //component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:0} .forceStyle.desktop .viewport{overflow:hidden}");
     component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0 !important;}   .forceStyle .viewport.oneHeader.desktop {z-index:0 !important;} .forceStyle.desktop .viewport{overflow:hidden}");
              console.log("inside controller");
              console.log("mode1");
              console.log(component.get("v.mode"));
              var scenarioRuleInstanceDetailsId = component.get("v.scenarioRuleInstanceId"); 
              console.log("scenarioRuleInstanceDetailsId value=="+scenarioRuleInstanceDetailsId);
              //console.log("scenarioId value===="+component.get("v.scenarioId"));
             //component.getTableName();
             
             var action = component.get("c.getTypesList");
             action.setCallback(this, function(response){
             var state = response.getState();  
             //component.getComputationType();
              console.log("mode");
              console.log(component.get("v.mode"));
              component.getOperatortypeValues();  
              component.getRuleTypes(); 
              component.getFieldValues();
              component.getTableName();
              //alert(component.get("v.mode"));
             
              if(component.get("v.mode")=="Update"){
              component.getExistingDetails(); 
              }else{
                 component.set("v.expressionArea",""); 
                 component.hideSpinner(); 
              } 
             
              if(component.isValid() && state==="SUCCESS")
             {
                 var ruleList = [];
                 var dataTypeList = [];
                 var valueTypeList = [];
                 dataTypeList.push({label:"Select Data Type",value:"Select Data Type"});
                 valueTypeList.push({label:"Select Value Type",value:"Select Value Type"});
                console.log("getTypesList called=====>");
                 console.log(response.getReturnValue()); 
                var itemsSample = response.getReturnValue();    
                console.log(itemsSample);
                var arrSplitters=[];
                var arrOperators=[]; 
                for(var ctr=0;ctr<itemsSample.length;ctr++){
                   /* if(itemsSample[ctr][common.getKey(component,"Type__c")]=="Rule Type")
                    {
                        ruleList.push({label:itemsSample[ctr][common.getKey(component,"Type__c")], value: itemsSample[ctr][common.getKey(component,"Value__c")]});
                       component.set("v.ruleOptions",ruleList);
                    }*/
                    if(itemsSample[ctr][common.getKey(component,"Type__c")]=="Splitters")
                    {
                      //arrSplitters.push(itemsSample[ctr][common.getKey(component,"Value__c")]);   
                      component.set("v.splitter",itemsSample[ctr][common.getKey(component,"Value__c")]);
                
                    }
                    if(itemsSample[ctr][common.getKey(component,"Type__c")]=="operators")
                    {
                         //arrOperators.push(itemsSample[ctr][common.getKey(component,"Value__c")]); 
                          component.set("v.operator",itemsSample[ctr][common.getKey(component,"Value__c")]);
                    }
                    component.get("v.businessRuleTypeId");
                    console.log("BusinessRuleID=="+component.get("v.businessRuleTypeId"));
                    if(itemsSample[ctr][common.getKey(component,"Type__c")]=="Data Type")
                    {
                        dataTypeList.push({label:itemsSample[ctr][common.getKey(component,"Type__c")], value: itemsSample[ctr][common.getKey(component,"Value__c")]});
                       component.set("v.datatypeoptions",dataTypeList);
                    }
                    
                    if(itemsSample[ctr][common.getKey(component,"Type__c")]=="Value Type")
                    {
                        valueTypeList.push({label:itemsSample[ctr][common.getKey(component,"Type__c")], value: itemsSample[ctr][common.getKey(component,"Value__c")]});
                       component.set("v.valueoptions",valueTypeList);
                    }
                   // opts.push({label:itemsSample[ctr].Label, value: itemsSample[ctr].Id});
                    
                  }
                 /*console.log("arrSplitters---->");
                 console.log(arrSplitters);*/
                 //alert(component.get("v.splitter"));
                 //alert(component.get("v.operator"));
                
                
             }
              else{
                console.log("Failed with state:  "+ state);
           }
         })
          $A.enqueueAction(action);
     },
    
     getRuleTypeValues : function(component, event, helper) {
        console.log("inside getRuleTypeValues method");  
        var action = component.get("c.getBusinessRuleTypeMaster");
        action.setParams({ "componentTypeMasterId" : component.get("v.componentTypeMasterId")});

         action.setCallback(this, function(response){ 
         var state = response.getState();
             if(component.isValid() && state==="SUCCESS")
               {
                var opts = [];
                var arrRuleTypes=response.getReturnValue();
                console.log("ARRAY EXPRESSION");
                console.log(arrRuleTypes);
               // opts.push({label:"Select an Option",value: "Select an Option"}) ;
                 for(var ctr=0;ctr<arrRuleTypes.length;ctr++){
                     if(arrRuleTypes[ctr].Name=='Compute'){
                        opts.push({label:arrRuleTypes[ctr].Id, value: arrRuleTypes[ctr].Name, selected:arrRuleTypes[ctr].Name});
                        console.log("default value:"+arrRuleTypes[ctr].Id);
                        component.set("v.selectedRule",arrRuleTypes[ctr].Id);}
                     else{
                    opts.push({label:arrRuleTypes[ctr].Id, value: arrRuleTypes[ctr].Name});
                         }
                    
                  }
                component.set("v.ruleOptions",opts);
                console.log("Rule Values are:");
                console.log(opts);
                   
                /*console.log("RuleValues");
                console.log(opts);*/
                
            }
             else{
                console.log("Failed with state:  "+ state);
            }
         })
          $A.enqueueAction(action);
    },
    
  /* getComputationType:function(component,event,helper){
        
        console.log("inside getComputationType method");  
        var action = component.get("c.GetComputationType");
        action.setParams({ businessRuleTypeId : "a1hf4000000hXoyAAE" });
        action.setCallback(this, function(response){ 
         var state = response.getState();
            if(component.isValid() && state==="SUCCESS")
            {
                
               console.log(response.getReturnValue());
                                
            }
              else{
                console.log("Failed with state:  "+ state);
            }
         })
          $A.enqueueAction(action);
        
    },*/
    
    getOperatortype : function(component, event, helper) {
        console.log("inside operationtype select method");
        var action = component.get("c.getOperatorTypeValues");
        action.setCallback(this, function(response){
             var state = response.getState();
            if(component.isValid() && state==="SUCCESS")
            {
            
                var opts = [];
                var arrOperatorTypes=[];
                var arrExpressions=response.getReturnValue();
                console.log("Operator values:==");  
                console.log(arrExpressions);
                component.set("v.operators",response.getReturnValue());
                helper.initialize(component);
               // alert(component.get("v.operators"));
                console.log("CHECK");
                console.log(arrExpressions);
                for(var arrCtr=0;arrCtr<arrExpressions.length;arrCtr++){
                    if(arrOperatorTypes.indexOf(arrExpressions[arrCtr][common.getKey(component,"Operator_Type__c")])==-1){
                        arrOperatorTypes.push(arrExpressions[arrCtr][common.getKey(component,"Operator_Type__c")]);
                    }  
                 }
                 opts.push({label: 'Select an Option', value: 'Select an Option'});
                 for(var ctr=0;ctr<arrOperatorTypes.length;ctr++){ 
                     
                     opts.push({label:arrOperatorTypes[ctr],value: arrOperatorTypes[ctr]});
                     component.set("v.typeoptions", opts);                
                    }
                  }
              else{
                console.log("Failed with state:  "+ state);
            }
         })
          $A.enqueueAction(action);
    },
    
    callDependentOpr : function(component, event, helper) {
        console.log('dependent type method called : ',component.get("v.selectedType"));
        
        var selectedType = component.get("v.selectedType");
        console.log('selectedType-->'+selectedType);
        var oprSelect = component.find("oprmySelect");
        var opts = [] ;
        if(selectedType != 'None'){
       		var action = component.get("c.getOperators"); 
           // console.log('action-->'+action);
            action.setParams({oprtype : component.get("v.selectedType")});
            action.setCallback(this, function(response) {
                var state = response.getState();
                if(state == "SUCCESS"){
                    console.log( response.getReturnValue());
                    opts = response.getReturnValue(); 
                    //component.set("v.operators",opts);
                   // alert('asa');
                    var output=[];
                    output.push({label: 'Select an Option', value: 'Select an Option'});
                    for(var i=0;i<opts.length;i++){
                       // console.log(opts[i])
                      // output.push(opts[i]);
                        output.push({label: opts[i][common.getKey(component,"Operator__c")], value: opts[i][common.getKey(component,"Syntax__c")],description: opts[i][common.getKey(component,"Brief_Description__c")]});
                    }
                    console.log('output');
                    console.log(output);
                    component.set("v.options", output);
                     
                }
           	 });
             $A.enqueueAction(action);
       }
    },
    
    onRuleChange : function(component, event, helper) {
    
    console.log("inside onRuleChange method");
    var ruleVal = component.get("v.selectedRule");
    console.log(ruleVal);
    var ruleText;  
   var ruleOptions= component.get("v.ruleOptions");
   console.log(ruleOptions[0]);
   for(var ctr=0;ctr<ruleOptions.length;ctr++){
     if(ruleOptions[ctr].label==ruleVal){
        ruleText=ruleOptions[ctr].value
        break;
     }
   }
    console.log("Rule val is = "+ruleVal);
    if(ruleText=='Category'){
          console.log("its category"+component.get("v.scenarioRuleInstanceId"));
           var evt = $A.get("e.force:navigateToComponent");
		     evt.setParams({
		         //componentDef : "c:CPGCategory",
               componentDef : "c:CPGCallPlanning",
		          componentAttributes: {
		            scenarioRuleInstanceId : component.get("v.scenarioRuleInstanceId"),
                   workSpaceId: component.get("v.workSpaceId"),
                    "ComponentName":"Category",
                     namespace:component.get("v.namespace"),
                     summarizedViewFlag : component.get("v.summarizedViewFlag")
                    
		        }
		    });
		    evt.fire();
    
    }
    if(ruleText=='Rank'){
          console.log("its rank");
           var evt = $A.get("e.force:navigateToComponent");
		     evt.setParams({
		        componentDef : "c:CPGRankingCreateDerivedField",
		        componentAttributes: {
		              mode: "Create",
		              scenarioRuleInstanceId : component.get("v.scenarioRuleInstanceId"),
                  workSpaceId: component.get("v.workSpaceId"),
                     namespace:component.get("v.namespace"),
                     summarizedViewFlag : component.get("v.summarizedViewFlag")
		        }
		    });
		    evt.fire();
    
    }
    
     if(ruleText=='Compute'){
          console.log("its compute");
           var evt = $A.get("e.force:navigateToComponent");
		     evt.setParams({
		        componentDef : "c:CPGComputationDerivedField",
		        componentAttributes: {
		              mode: "Create",
                      scenarioRuleInstanceId : component.get("v.scenarioRuleInstanceId"),
                      workSpaceId: component.get("v.workSpaceId"),
                     namespace:component.get("v.namespace"),
                     summarizedViewFlag : component.get("v.summarizedViewFlag") 
		        }
		    });
		    evt.fire();
    
    }
    
    },
    
    operationcall : function(component, event, helper) {
        var oprsyn;
        var oprdesc;
        var operatorval = component.get("v.selectedValue");
       // var operatorval = operatorvalue.fontcolor("green");
        console.log("operatorval"+operatorval); 
        //component.set("v.expressionArea",operatorval);
        var optionvalue = component.get("v.options");
        console.log("optionValues");
        console.log(optionvalue.length);
        for(var i=0;i<optionvalue.length;i++){
            //console.log(optionvalue[i].description);
            if(optionvalue[i].value==operatorval)
            {   
              if(optionvalue[i].value != 'Select an Option'){
                console.log("inside required loop");
                console.log(optionvalue[i].description);
                console.log(optionvalue[i].value);
                //console.log(optionvalue[i].Comments__c);
                oprsyn = optionvalue[i].value;
                oprdesc = optionvalue[i].description;
                }
            }
        }
        console.log("syntax value"+oprsyn);
        component.set("v.functionSyntax",oprsyn);
        component.set("v.functionDesc",oprdesc);
        
         var textExp = component.get("v.expressionArea");
        if(!textExp){
              if(operatorval != 'Select an Option'){
             component.set("v.expressionArea",operatorval); 
             }  
            }else
            {
               if(operatorval != 'Select an Option'){
               component.set("v.expressionArea",operatorval); 
               
                var exp = textExp+' ';
                var expression = exp.concat(operatorval);
                component.set("v.expressionArea",expression);
              }
            }
            
             var funcDetails = component.find('divFunctionDetails');
             $A.util.removeClass(funcDetails,'slds-hide');
             
             var funcFullDetails = component.find('divFunctionFullDetails');
             $A.util.removeClass(funcFullDetails,'slds-hide');
             
    },
    
    getFields : function(component, event, helper) {
             console.log("inside getFields controller");  
             /*var sampledata = [{"fieldName":"gattexlead_flag","fieldType":"source","dataType":"Numeric"}
                           ,{"fieldName":"callactivity_flag","fieldType":"Derived","dataType":"Text"}];
             component.set("v.columndata",sampledata);*/
           var scenarioRuleInstanceDetailsId=component.get("v.scenarioRuleInstanceId");
           var action = component.get("c.GetSourceAndDerivedFields");
           action.setParams({"scenarioRuleInstanceDetailsId":scenarioRuleInstanceDetailsId}); 
             action.setCallback(this, function(response){
                 var state = response.getState();  
                 if(component.isValid() && state==="SUCCESS")
                 {
                    console.log(response.getReturnValue());
                    //MapString to listString
                    var itemsSample = response.getReturnValue();   
                    component.set("v.columndata",itemsSample);
                    helper.columns=component.get("v.columndata"); 
                    console.log("FIELDS CALLED");
                }
                  else{
                    console.log("Failed with state:  "+ state);
               }
         })
          $A.enqueueAction(action); 
    },
    
    getTableName : function(component, event, helper) {
     if(component.get("v.scenarioRuleInstanceId")=="" || component.get("v.scenarioRuleInstanceId")=='undefined'){
            component.set("v.tableName","No Table Found");
          }else{
    		var action = component.get("c.getInputTableName");
            action.setParams({"scenarioRuleInstanceDetailsId":component.get("v.scenarioRuleInstanceId")}); 
             action.setCallback(this, function(response){
                 var state = response.getState();
                 console.log(state);  
                 if(component.isValid() && state==="SUCCESS")
                 {
                    console.log("Table Value");
                    console.log(response.getReturnValue());
                    //MapString to listString
                    var itemsSample = response.getReturnValue();
                     if(itemsSample=="" || itemsSample=="undefined"){
                     component.set("v.tableName","");
                    } else{    
                    component.set("v.tableName",itemsSample[0][common.getKey(component,"Table_Display_Name__c")]); 
                    }
                }else{
                    console.log("Failed with state:  "+ state);
                }
         });
          $A.enqueueAction(action);
     }
    },
    
    columnSelect : function(component, event, helper) {
        console.log("inside columnselect controller");
        var colValue = event.getSource().get("v.value");
        console.log(event );
        var textExp = component.get("v.expressionArea");
       //  var operatorDetails = component.get("v.selectedValue");
        
            if(!textExp){
             component.set("v.expressionArea",colValue);   
            }else
            {
                var exp = textExp+' ';
               // var colValuewithBrace = "["+colValue+"]";
                var expression = exp.concat(colValue);
                component.set("v.expressionArea",expression);
            }
        
        //component.set("v.expressionArea",colValue);
    },

    saveDetails : function(component, event, helper) {
        
              var textAreaValue = component.find("textAreaID").get("v.value");
              console.log(textAreaValue);

         console.log("save icon clicked");
         console.log(component.get("v.mode"));
         var RuleType = component.get("v.selectedRule"); 
         console.log(RuleType);
         var DerivedFRule = component.get("v.ruleName"); 
         console.log(DerivedFRule);
         var DerivedFName = component.get("v.fieldName");  
         console.log(DerivedFName);
         var DerivedFType = component.get("v.selectedDataType"); 
         console.log(DerivedFType);
         var DerivedFValueType = component.get("v.selectedValueType"); 
         console.log(DerivedFValueType); 
         
         var DerivedFExpression = component.get("v.expressionArea");
         console.log(DerivedFExpression);
         var errorMsg = "";
         var msgdiv = component.find('divmsg');
         /*if (!DerivedFRule && !DerivedFName && !DerivedFExpression) {  
            // Set error
            console.log("inside if of DerivedFRule ERROR msg");
            errorMsg += "PLEASE ENTER DERIVED RULE NAME AND FIELD NAME \n";
         }*/
         
         var sname1 = component.find("ruleAuraId");
         $A.util.removeClass(sname1 , 'errorInput');
         var sname2 = component.find("name_text");
	     $A.util.removeClass(sname2 , 'errorInput');
         var sname3 = component.find("derivedFieldType");
	     $A.util.removeClass(sname3 , ' slds-has-error');
	     var sname4 = component.find("ValueType");
	     $A.util.removeClass(sname4 , ' slds-has-error'); 
	     var sname5 = component.find("textAreaID");
	     $A.util.removeClass(sname5 , 'slds-has-error');
	     
        
        
	     if(DerivedFRule == undefined || !DerivedFRule.trim()){
	      component.set("v.popoverMessage","Please enter derived rule name");
           
            var sname = component.find("ruleAuraId");
            $A.util.addClass(sname , 'errorInput');
            
            var errorIcn = component.find("errorIcon");
            $A.util.removeClass(errorIcn,'slds-hide');
	     
	     }
	     
	     else if((DerivedFName == undefined || !DerivedFName.trim() ) && DerivedFRule){
	          component.set("v.popoverMessage","Please enter derived field name");
           
	            var sname = component.find("name_text");
	            $A.util.addClass(sname , 'errorInput');
	            
	            var errorIcn = component.find("errorIcon");
	            $A.util.removeClass(errorIcn,'slds-hide');
	     
	     }
	     else if(!DerivedFType && DerivedFName && DerivedFRule){
	     
	    
	          component.set("v.popoverMessage","Please select derived field data type");
           
	            var sname = component.find("derivedFieldType");
	            $A.util.addClass(sname , 'slds-has-error');
	            
	            var errorIcn = component.find("errorIcon");
	            $A.util.removeClass(errorIcn,'slds-hide');
	     
	     }
	     else if(DerivedFType && DerivedFName && DerivedFRule && DerivedFType == "Select Data Type"){
	         
	            component.set("v.popoverMessage","Please select derived field data type");
           
	            var sname = component.find("derivedFieldType");
	            $A.util.addClass(sname , ' slds-has-error');
	            
	            var errorIcn = component.find("errorIcon");
	            $A.util.removeClass(errorIcn,'slds-hide');
	        
	     }
	     else if(!DerivedFValueType && DerivedFType && DerivedFName && DerivedFRule ){
	         component.set("v.popoverMessage","Please select derived field value type");
           
	            var sname = component.find("ValueType");
	            $A.util.addClass(sname , ' slds-has-error');
	            
	            var errorIcn = component.find("errorIcon");
	            $A.util.removeClass(errorIcn,'slds-hide');
	     }
	     else if(DerivedFValueType && DerivedFType && DerivedFName && DerivedFRule && DerivedFValueType == "Select Value Type"){
	        
	         component.set("v.popoverMessage","Please select derived field value type");
           
	            var sname = component.find("ValueType");
	            $A.util.addClass(sname , ' slds-has-error');
	            
	            var errorIcn = component.find("errorIcon");
	            $A.util.removeClass(errorIcn,'slds-hide');
	     
	     }
	     else if(!DerivedFExpression && DerivedFValueType && DerivedFType && DerivedFName && DerivedFRule ){
	          component.set("v.popoverMessage","Please enter expression");
               
                var sname = component.find("textAreaID");
	            $A.util.addClass(sname , 'slds-has-error');
	            
	            var errorIcn = component.find("errorIcon");
	            $A.util.removeClass(errorIcn,'slds-hide');
	     
	     }
       else if(textAreaValue.trim()==''){
            component.set("v.popoverMessage","Please enter expression");
               errorMsg='Not valid';
                var sname = component.find("textAreaID");
              $A.util.addClass(sname , 'slds-has-error');
              
              var errorIcn = component.find("errorIcon");
              $A.util.removeClass(errorIcn,'slds-hide');
       }
	     else{
              helper.validateExpression(component,DerivedFExpression);  
              errorMsg +=component.get("v.expressionMessage");
                            console.log("error msg="+component.get("v.expressionMessage"));
              if(component.get("v.expressionMessage")!=="Valid" || component.get("v.expressionMessage")=="undefined"  ){
              
                console.log("inside if of error msg");
                component.set("v.popoverMessage",component.get("v.expressionMessage")); 
                console.log("popover message assigned" +component.get("v.popoverMessage"));
                
                var sname = component.find("textAreaID");
	            $A.util.addClass(sname , 'slds-has-error');
                
                var errorIcn = component.find("errorIcon");
	            $A.util.removeClass(errorIcn,'slds-hide');  
	           
                
              }else{
               var errorIcn = component.find("errorIcon");
	            $A.util.addClass(errorIcn,'slds-hide');
              
              }
            }
        if(errorMsg!='Valid'){
        	$A.util.removeClass(msgdiv,'slds-hide');
            component.set("v.errormsg", errorMsg );
        }
        else{
            component.showSpinner();
        	$A.util.addClass(msgdiv,'slds-hide');
            component.set("v.errormsg","");
            var businessRulekey=common.getKey(component,"Business_Rules__c");
            
            var jsonBusinessRules={};
            var arrComputationRuleDetail=[];
            var arrBusinessRuleFieldMapDetail=[]; 
            //jsonBusinessRules[common.getKey(component,"Business_Rules__c")][common.getKey(component,"Business_Rule_Type_Master__c")]=RuleType;
           // alert(component.get("v.selectedRule"));
            jsonBusinessRules[businessRulekey]={};
            jsonBusinessRules[businessRulekey][common.getKey(component,"Business_Rule_Type_Master__c")]=component.get("v.selectedRule"); 
            jsonBusinessRules[businessRulekey]["Name"]=DerivedFRule;
            jsonBusinessRules[businessRulekey][common.getKey(component,"Is_Derived__c")]=true;
            jsonBusinessRules[businessRulekey][common.getKey(component,"Is_Mandatory__c")]=true;
            jsonBusinessRules[businessRulekey][common.getKey(component,"ScenarioRuleInstanceDetails__c")]=component.get("v.scenarioRuleInstanceId");
             console.log(jsonBusinessRules);
            var dispExprKey=common.getKey(component,"Display_expression__c");
            var execExprKey=common.getKey(component,"Execute_expression__c");
            var derFieldNameKey=common.getKey(component,"Derived_field_name__c");
            var valTypeKey=common.getKey(component,"Value_Type__c");
            var dataTypeKey=common.getKey(component,"Data_Type__c");
            var jsonComputationRuleDetail={};
            jsonComputationRuleDetail[dispExprKey]=DerivedFExpression;
            jsonComputationRuleDetail[execExprKey]=component.get("v.executeExpression");
            jsonComputationRuleDetail[derFieldNameKey]=DerivedFName;
            jsonComputationRuleDetail[valTypeKey]=DerivedFValueType;
            jsonComputationRuleDetail[dataTypeKey]=DerivedFType;
            arrComputationRuleDetail.push(jsonComputationRuleDetail);
            console.log(arrComputationRuleDetail);
            
            var ioFlagKey=common.getKey(component,"IO_flag__c");
            var paramDataTypeKey=common.getKey(component,"Param_Data_Type__c");
            var paramNameKey=common.getKey(component,"Param_Name__c");
            var paramTypeKey=common.getKey(component, "Param_type__c");
            var jsonBusinessRuleFieldMapDetail={};
            jsonBusinessRuleFieldMapDetail[ioFlagKey]="O";
            jsonBusinessRuleFieldMapDetail[paramNameKey]=DerivedFName;
            jsonBusinessRuleFieldMapDetail[paramDataTypeKey]=DerivedFType;
            jsonBusinessRuleFieldMapDetail[paramTypeKey]="Derived";
            
             
            arrBusinessRuleFieldMapDetail.push(jsonBusinessRuleFieldMapDetail);
            for(var ctr=0;ctr<helper.derivedColumns.length;ctr++){
                var jsonBusinessRuleFieldMapDetail={};
                jsonBusinessRuleFieldMapDetail[ioFlagKey]="I";
                jsonBusinessRuleFieldMapDetail[paramNameKey]=helper.derivedColumns[ctr];
                jsonBusinessRuleFieldMapDetail[paramDataTypeKey]=helper.derivedColumnsDataType[ctr];
                jsonBusinessRuleFieldMapDetail[paramTypeKey]="Derived";
               
                arrBusinessRuleFieldMapDetail.push(jsonBusinessRuleFieldMapDetail);                                                
            }
           console.log("Json console");
           console.log(JSON.stringify(jsonBusinessRules));
           console.log(JSON.stringify(arrComputationRuleDetail));
           console.log(JSON.stringify(arrBusinessRuleFieldMapDetail)); 
            var action;
            if(component.get("v.mode")=="Update"){
                jsonBusinessRules[businessRulekey]["Id"]=component.get("v.businessRuleTypeId");
		        action = component.get("c.UpdateComputationType");
		     }
            else{
                action = component.get("c.SaveComputationType");
             }
            
           action.setParams({strJsonBusinessRules:JSON.stringify(jsonBusinessRules),
		                          strJsonComputationRuleDetail:JSON.stringify(arrComputationRuleDetail),
		                          strJsonBusinessRuleFieldMapDetails:JSON.stringify(arrBusinessRuleFieldMapDetail)});
		                         // var self = this;
		        
            action.setCallback(this, function(response) {
                console.log("hello");
                console.log(response.getReturnValue()); 
                var data= response.getReturnValue();
                component.set("v.expressionMessage",data.status);
                var state = response.getState();  
                if(component.isValid() && state==="SUCCESS")
                {
                    var itemsSample = response.getReturnValue();  
                   
                    $A.util.removeClass(msgdiv,'slds-hide');
                    component.set("v.errormsg", data.status );
                    console.log("data status"+data.status);
                    if(data.status=="error"){
                        console.log(data.msg);
                        component.set("v.popoverMessage",data.msg);
                        console.log("popover message assigned" +component.get("v.popoverMessage"));
                
                       // var sname = component.find("textAreaID");
                       // $A.util.addClass(sname , 'slds-has-error');
                        
                        var errorIcn = component.find("errorIcon");
                        $A.util.removeClass(errorIcn,'slds-hide'); 
                    }
                    else{
                        component.set("v.ruleName",""); 
                        component.set("v.fieldName","");
                        component.set("v.selectedDataType","Select Data Type");
                        component.set("v.selectedValueType","Select Value Type");
                        component.set("v.expressionArea","");
                        component.getFieldValues();
                         component.closeButton();
                    }
                   
                    
                }
                else{
                    console.log("Failed with state:  "+ state);
                }
                component.hideSpinner();
		        });
		        
		        $A.enqueueAction(action);  
		          
        }
       
      
    } ,
    
    saveComputationType:function(component,event,helper,jsonBusinessRules,arrComputationRuleDetail,
                                arrBusinessRuleFieldMapDetail){
        console.log('saveComputationType');
        console.log(jsonBusinessRules);
        
        
    },
    
     getExistingDetails : function(component, event, helper) {
            console.log("inside getExistingDetails controller");
         console.log("BusinessRuleID=="+component.get("v.businessRuleTypeId"));
            var action = component.get("c.GetComputationType");
            action.setParams({"businessRuleTypeId":component.get("v.businessRuleTypeId")}); 
             action.setCallback(this, function(response){
                 var state = response.getState();
                 console.log(state);  
                 if(component.isValid() && state==="SUCCESS")
                 {
                    console.log(response.getReturnValue());
                    //MapString to listString
                    var itemsSample = response.getReturnValue(); 
                    console.log(itemsSample[0][common.getKey(component,"Business_Rule_FieldMap_Details__r")][0].Name);
                    component.set("v.ruleName",itemsSample[0].Name);
                    
                    console.log(itemsSample[0][common.getKey(component,"Computation_Rule_Details__r")][0][common.getKey(component,"Derived_field_name__c")]);
                    component.set("v.fieldName",itemsSample[0][common.getKey(component,"Computation_Rule_Details__r")][0][common.getKey(component,"Derived_field_name__c")]);
                    
                    console.log(itemsSample[0][common.getKey(component,"Computation_Rule_Details__r")][0][common.getKey(component,"Display_expression__c")]);
                    component.set("v.expressionArea",itemsSample[0][common.getKey(component,"Computation_Rule_Details__r")][0][common.getKey(component,"Display_expression__c")]);
                    
                    console.log(itemsSample[0][common.getKey(component,"Computation_Rule_Details__r")][0][common.getKey(component,"Data_Type__c")]);
                    component.set("v.selectedDataType",itemsSample[0][common.getKey(component,"Computation_Rule_Details__r")][0][common.getKey(component,"Data_Type__c")]);
                     
                    console.log(itemsSample[0][common.getKey(component,"Computation_Rule_Details__r")][0][common.getKey(component,"Value_Type__c")]);
                    component.set("v.selectedValueType",itemsSample[0][common.getKey(component,"Computation_Rule_Details__r")][0][common.getKey(component,"Value_Type__c")]);
                 
                    console.log("checkpoint");
                    component.hideSpinner(); 
                }
                  else{
                    console.log("Failed with state:  "+ state);
               }
         })
          $A.enqueueAction(action); 
    },
    
    validation : function(component, event, helper) {
    var DerivedFExpression = component.get("v.expressionArea");
    helper.validateExpression(component,DerivedFExpression);
    
    },
    
    closeButton : function(component, event, helper) {
    console.log("inside closeButton method");
    component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
    var sceId=component.get("v.scenarioId");    
     var evt = $A.get("e.force:navigateToComponent");
		     evt.setParams({
		        componentDef : "c:CPGDerivedFieldsList",
		        componentAttributes: {
                   // "scenarioId":component.get("v.scenarioId"),
		             "scenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId"),
                     workSpaceId: component.get("v.workSpaceId"),
                     namespace:component.get("v.namespace"),
                     summarizedViewFlag : component.get("v.summarizedViewFlag")
		        }
		    });
          // $A.get('e.force:refreshView').fire();
		    evt.fire();
     },
     
    handleMouseEnter : function(component, event, helper) {
        var popover = component.find("popover");
        $A.util.removeClass(popover,'slds-hide');
    },
    //make a mouse leave handler here
    handleMouseLeave : function(component, event, helper) {
        var popover = component.find("popover");
        $A.util.addClass(popover,'slds-hide');
    },
    
     showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
    
    getCursorPosition : function(component,event,helper){
    var caretPos = 1;
    console.log("inside getCursorPosition method");
   // var txt = component.find("textAreaID");
   // console.log(txt);
   // caretPos = txt[0].selectionStart;
    //console.log(caretPos);
    }

    
})