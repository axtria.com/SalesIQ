({  
    functions:[],
    operators:[],
    splitters:["(",")",","],
    derivedFieldDataType:'',
    expression:'',
    trimmedExpression:'',
    executeExpression:'',
    component:'',
    subExpr:[],
    currentColumnDataType:'',
    openingBrackets:"(",
    closingBrackets:")",
    openingBracketsCount:0,
    closingBracketsCount:0,
    arrExpressions:[],
    arrIndex:0,
    noOfUserDefinedParameters:0,
    operatorType:'',
    logicalOperators:['>','<','='],
    columns:'',
    derivedColumns:[],
    derivedColumnsDataType:[],
    initialize:function(component){
        var ops=component.get("v.operators");
        console.log(ops);
        this.populateFunctionsAndOperators(component,ops);
    },
    
    populateFunctionsAndOperators:function(component,ops){
        
        for(var ctr=0;ctr<ops.length;ctr++){
            if(!ops[ctr][common.getKey(component,"isFunction__c")]){this.operators.push(ops[ctr][common.getKey(component,"Operator__c")]);
                                       this.splitters.push(ops[ctr][common.getKey(component,"Operator__c")]);}
            if(ops[ctr][common.getKey(component,"isFunction__c")]){
               var item=[];
               item["Name"]=ops[ctr][common.getKey(component,"Operator__c")];
                if(ops[ctr][common.getKey(component,"Parameters__c")]){
                    var jsnParams=JSON.parse(ops[ctr][common.getKey(component,"Parameters__c")]);
                    item["Parameters"]=jsnParams;
                }  
               item["NoOfParameters"]=ops[ctr][common.getKey(component,"NoOfParameters__c")];
               item["ReturnType"]=ops[ctr][common.getKey(component,"Return__c")];
               item["DbFunctionName"]=ops[ctr][common.getKey(component,"DbFunction_Name__c")]; 
               item["OperatorType"] =ops[ctr][common.getKey(component,"Operator_Type__c")];
               this.functions.push(item);
            }
        }
        
    },
    
    validateExpression : function(component,item,callback){
         this.derivedColumnsDataType=[];
         this.derivedColumns=[];
        var isValid=false;
        //this.columns=
        this.derivedFieldDataType=component.get("v.selectedDataType");
        this.expression=item.trim(); 
        this.trimmedExpression=item.trim().replace(/\s/g,''); 
        this.component=component; 
        if(this.validatePattern()){
            if(this.convertExpressionToArray()){
                if(this.checkExpressionSyntax(false,'',0,0,'')){
                    for(var ctrIndex=0;ctrIndex<this.arrExpressions.length;ctrIndex++){
                        for(var ctrColumns=0;ctrColumns<this.columns.length;ctrColumns++){
                          if(isNaN(this.arrExpressions[ctrIndex])
                             && this.columns[ctrColumns].fieldName.toLowerCase()
                             ==this.arrExpressions[ctrIndex].toLowerCase()){
                            this.arrExpressions[ctrIndex]=this.columns[ctrColumns].tblColName;
                            break;  
                          }
                        }
                    } 
                    
                    component.set("v.executeExpression",this.arrExpressions.join("~"));
                    console.log(this.arrExpressions.join("~"));
                    this.setErrorMessage('Valid');   
                    isValid=true;
                }
               
            }
            
        }
        
        this.resetVariables();
        return isValid;
    },
    
    validatePattern:function(){
        
          var arrInvalidPatterns=['()',')(','(+','(-','(*','(/','(>','(<','(=','(,',',)','+)','-)',
                                  '>)','<)','=>','*)','/)',',,',
                                 ',+',',-',',*',',/',',>',',=',
                                  ',<',',^','+,','-,','*,','/,','>,','=,','<,','<>','^,','=<','=)'];
          var isValidPattern=true;
        
          if(this.trimmedExpression[0]==')'){
            isValidPattern=false;
            this.setErrorMessage('Expression cannot start with )');
            return false;
          }
          if(this.operators.indexOf(this.trimmedExpression[0])>-1){
            isValidPattern=false;
            this.setErrorMessage('Expression cannot start with '+this.expression[0]);
            return false;
          }
          if(this.trimmedExpression.search(/[-+*\/][-+*\/]/g)>-1){
            this.setErrorMessage('An operator cannot follow another operator');
            isValidPattern=false;
            return false;
        }
        if(this.trimmedExpression.search(/[-+*\/],/g)>-1){
            this.setErrorMessage('comma cannot follow an operator');
            isValidPattern=false;
            return false;
        }
        if(this.trimmedExpression.search(/,[-+*\/]/g)>-1){
            this.setErrorMessage('operator cannot follow a comma');
            isValidPattern=false;
            return false;
        }
        if(this.trimmedExpression.search(/\)\w/g)>-1){
            this.setErrorMessage('operand cannot follow a closing bracket');
            isValidPattern=false;
            return false;
        } 
        
        
        for(var ctrInvalidPatterns=0;ctrInvalidPatterns<arrInvalidPatterns.length;ctrInvalidPatterns++){
            console.log(ctrInvalidPatterns);
             if(this.trimmedExpression.indexOf(arrInvalidPatterns[ctrInvalidPatterns])>-1){
                console.log(arrInvalidPatterns[ctrInvalidPatterns]); 
                isValidPattern=false; 
                this.setErrorMessage(arrInvalidPatterns[ctrInvalidPatterns] +' is not valid'); 
                break;
            }
         }
        
         if(!isValidPattern){
           return false;
         }
         else{
           isValidPattern=this.validatePatternAgainstDataType();
         }
       return isValidPattern;
        
    },
    
     //Resets the helper variables.
    resetVariables:function(){
        this.openingBracketsCount=0;
        this.closingBracketsCount=0;
        this.arrExpressions=[];
        this.arrIndex=0;
        //this.operandCount=0;
        this.subExpr=[];
        this.executeExpression='';
        this.noOfUserDefinedParameters=0;
        
       
        //this.arrFunction=[];
        
    },
    
    validatePatternAgainstDataType:function(){
       if(this.derivedFieldDataType=="Text"){
           if(this.expression.search(/[-+*><=\/]/g)>-1){
              this.setErrorMessage('Cannot contain operators');  
              return false; 
           }
       }
       return true;
   },
    
    convertExpressionToArray:function(){
        
       var isValid=true;
       for(var ctr=0;ctr<this.expression.length;ctr++){
           console.log('convertExpressionToArray ctr->'+this.expression[ctr]);
           if(this.expression[ctr]!=''){
                //skipping = after >
                if(ctr>0 && this.expression[ctr]=="=" &&
                   (this.expression[ctr-1]==">" || this.expression[ctr-1]=="<")){
                    continue;
                }
                if(this.isSplitter(ctr)){
                    console.log('splitter');
                    this.processExpressionForSplitter(this.expression[ctr],ctr);
                }
                else if(!this.isSplitter(ctr)){
                console.log('not splitter');
                if(!this.processExpressionForNonSplitter(this.expression[ctr],ctr)){
                    isValid=false;
                    break;  
                 }
            }
           }    
             
        }
       if(!isValid){
           return false;
       }
      if(this.openingBracketsCount!=this.closingBracketsCount){
           this.setErrorMessage('Brackets do not match');
           return false;
       }
       console.log("array") ;
       console.log(this.arrExpressions); 
       return true;
       
    },
    checkExpressionSyntax:function(isParameter,parameterType,noOfParameters,paramIndex,operatorType){
        var fieldType;
        if(isParameter){fieldType=parameterType;}
        else{fieldType=this.derivedFieldDataType;} 
        
        if(this.isOperand(this.arrExpressions[this.arrIndex])){
            if(!this.validateDataTypeOfOperand(fieldType)){
                return false;
            }
            if(!isParameter){
                if(this.isNextElementOpeningBracket()){
                    this.setErrorMessage('( cannot follow an operand');
                    return false;
                }   
            }
        }
        else if(this.isOperator(this.arrIndex) ){
           if(!this.isExpressionArrayHaveNextElement()){
               this.setErrorMessage('An operator must be followed by an operand or function');
               return false;
           } 
            else{
                if(this.validateConsecutiveOperatorCheck()){
                    if(isParameter && operatorType=="Logical") {
                        if(this.logicalOperators.indexOf(this.arrExpressions[this.arrIndex])==-1){
                            this.setErrorMessage("Parameters do no have logical operators");
                            return false;
                          }
                    } 
                }
                else{
                    return false;
                }
                
            }
            
       }
           /* else if(isParameter && this.isOperator(this.arrIndex) && this.isNextElementComma()){
                this.setErrorMessage(this.arrExpressions[this.arrIndex]+" is not valid");
            } */
        else if(this.isFunction(this.arrExpressions[this.arrIndex])){
           if(!this.isExpressionArrayHaveNextElement() && !this.isNextElementOpeningBracket()){
              
               this.setErrorMessage('Function '+this.arrExpressions[this.arrIndex]+' is not complete');
               return false;
           }
           else{
               if(!this.validateFunction()){
                   return false;
               }
              
           }
       }
       if(this.isExpressionArrayHaveNextElement()){
             if(!isParameter){
                 if(this.isNextElementComma()){
                     this.setErrorMessage('Comma cannot follow '+this.arrExpressions[this.arrIndex]);
                     return false;
                 }
                   this.incrementCounterToNextElement();
                   return this.checkExpressionSyntax(false,"",0,0,'');
               }
               else if(isParameter){
                   /*if(this.isCurrentElementClosingBracket()){
                       return true;
                   }*/
                   if(this.checkPatternForFunctionParameter(noOfParameters,paramIndex)){
                        return true;
                    }
                   else{
                       this.incrementCounterToNextElement();
                       return this.checkExpressionSyntax(true,parameterType
                                                         ,noOfParameters,paramIndex,operatorType);
                    }
               }   
        }
        return true;
        
    },
    validateConsecutiveOperatorCheck:function(){
        var isValid=true;
        if(this.operators.indexOf(this.arrExpressions[this.arrIndex+1])>-1){
            this.setErrorMessage('An operator cannot follow another operator');
            isValid=false;
        }
        return isValid;
    },
    checkPatternForFunctionParameter:function(noOfParameters,paramIndex){
       /* if(noOfParameters==999 && this.isNextElementComma() ){
            return true;
        }
        else if(noOfParameters==999 && this.isNextElementClosingBracket()){
            return true;
        }*/
        /*if (noOfParameters!=999 && paramIndex==noOfParameters-1 
            && this.isExpressionArrayHaveNextElement() && this.isNextElementComma()){
                  return true; 
         }
        else if(noOfParameters!=999 && paramIndex==noOfParameters 
                && this.isExpressionArrayHaveNextElement() && this.isNextElementClosingBracket()){
            return true;       
        }*/
        if(this.isExpressionArrayHaveNextElement() && (this.isNextElementClosingBracket()
                                                       || this.isNextElementComma())){
           return true;
           }
        return false;
   },
    validateFunction:function(){
        var noOfParameters=0;
        //var noOfUserDefinedParameters=0; 
        var returnType='';
        var functionName=''; 
        var dbFunctionName=''; 
        var index=0;
        var parameterType='';
        var isValid=true;
        var paramCount;
        var operatorType;
        for (var ctr=0;ctr<this.functions.length;ctr++ ) {
            index=ctr;
            if (this.functions[ctr].Name.toLowerCase() == this.arrExpressions[this.arrIndex].toLowerCase()) {
                noOfParameters=this.functions[ctr].NoOfParameters;
                returnType=this.functions[ctr].ReturnType;
                functionName=this.arrExpressions[this.arrIndex];
                if(this.functions[ctr].DbFunctionName){
                    dbFunctionName=this.functions[ctr].DbFunctionName;
                }
                if(this.functions[ctr].OperatorType){
                    operatorType=this.functions[ctr].OperatorType;
                }
                break;
            }
        }
        
        if(returnType!=this.derivedFieldDataType) {
            this.setErrorMessage('Function '+functionName +' is not consistent with derived field data type');
            return false;
        }  
        this.incrementCounterToNextElement();//skip function
        this.incrementCounterToNextElement();//skip opening bracket 
        if(parseInt(noOfParameters)!=999){
          isValid=this.validatePatternForFiniteParameters(noOfParameters,index);
        }
        else{
          isValid=this.validatePatternForInfiniteParameters(operatorType,returnType,noOfParameters,index);
        }
         
        return isValid;
    
    },
    validatePatternForInfiniteParameters:function(operatorType,returnType,noOfParameters,index){
        var isValid=false;
        
        isValid=this.validateFunctionParameter(operatorType,returnType,index,noOfParameters);
        if(!isValid){return false;}
        if(this.isNextElementClosingBracket()){
            return true;
        }
        while(!this.isNextElementClosingBracket()){
            isValid=this.validateFunctionParameter(operatorType,returnType,index,noOfParameters);
            if(!isValid){break;}
            this.incrementCounterToNextElement();
            if(this.isCurrentElementClosingBracket()){break;	}
            if(this.isExpressionArrayHaveNextElement()){
              this.incrementCounterToNextElement();  
            }
            
        }
        if(this.isNextElementClosingBracket()){
            isValid=this.validateFunctionParameter(operatorType,returnType,index,noOfParameters);
            if(!isValid){return false;}
            this.incrementCounterToNextElement();
             
        }
        return isValid;
        
    },
    
    validatePatternForFiniteParameters:function(noOfParameters,index){
      var  isValid=true;  
      var parameterType;  
      var paramCount=0;  
        
         for(var ctrParameters=0;ctrParameters<noOfParameters;++ctrParameters) {
               
               parameterType=this.functions[index].Parameters[ctrParameters].DataType;
               if(!this.validateFunctionParameter('',parameterType,ctrParameters+1,noOfParameters)){
                      isValid=false;
                      break;
                }
              else{
                    this.incrementCounterToNextElement();
                     ++paramCount;
              
                }
              //this.incrementCounterToNextElement();
            if(ctrParameters<noOfParameters-1){
                if(!this.isExpressionArrayHaveNextElement()){
                    this.setErrorMessage('Function '+name+' is not complete');
                    isValid=false;
                    
                }
                else{
                    if(!this.isCurrentElementComma()){
                        this.setErrorMessage('Function '+name+' is not complete');
                        isValid=false;
                       
                    }
                    else{
                          if(!this.isExpressionArrayHaveNextElement()){
                              this.setErrorMessage('Function '+name+' is not complete');
                              isValid=false;
                          } 
                          else{  
                             this.incrementCounterToNextElement();   
                          }   
                    }
                }
                
            }
            }
            
            if(!isValid){return isValid};
             if(isValid){
                if(noOfParameters!=paramCount)  {
                    this.setErrorMessage('Parameter mismatch for function '+this.arrExpressions[this.arrIndex]);
                    return false;
                }   
             }
            if(!this.isCurrentElementClosingBracket()){
                  this.setErrorMessage('Function '+name+' is not complete');
                  return false;
            }
        return true;
    },
    validateFunctionParameter:function(operatorType,parameterType,index,noOfParameters){
         if(!this.isFunction(this.arrExpressions[this.arrIndex]) 
            && !this.isColumn(this.arrExpressions[this.arrIndex]) 
            && (this.isNextElementComma() || this.isNextElementClosingBracket())){
              return  this.validateAgainstDerivedFieldDataType();
            
         }
        else if(this.isFunction(this.arrExpressions[this.arrIndex])){
            	return (this.validateFunction());
        	}
        else if(this.isColumn(this.arrExpressions[this.arrIndex])){
             var isValid=this.validateColumnDataTypeAgainstDerivedFieldDataType();  
             if(!isValid){return false;}
             else{
                 if(this.isNextElementComma() || this.isNextElementClosingBracket()){
                     return true;
                 }
                 else{
                     this.incrementCounterToNextElement();
                     return(this.checkExpressionSyntax(true,parameterType,noOfParameters,index,operatorType));
                 }
             }
        }
        else{
                return(this.checkExpressionSyntax(true,parameterType,noOfParameters,index,operatorType));
        }
        
    },
    validateColumnDataTypeAgainstDerivedFieldDataType:function(){
        if(this.derivedFieldDataType!=this.currentColumnDataType){
            this.setErrorMessage("Data type of "+this.arrExpressions[this.arrIndex]+" is not consistent with derived field data type");
            return false;
        }   
        return true;
    },
    validateAgainstDerivedFieldDataType:function(){
         if(this.isExpressionArrayHaveNextElement() 
               && (this.isNextElementComma()|| this.isNextElementClosingBracket())){
               if(this.derivedFieldDataType=="Text" 
                  && (this.arrExpressions[this.arrIndex].indexOf("'")==-1 
                      && this.arrExpressions[this.arrIndex].indexOf("\"")==-1)) {
                   this.setErrorMessage("Data type of "+this.arrExpressions[this.arrIndex]+" is not consistent with derived field data type");
                    return false;
                }
                if(this.derivedFieldDataType=="Numeric" 
                   && (this.arrExpressions[this.arrIndex].indexOf("'")>=0 
                      || this.arrExpressions[this.arrIndex].indexOf("\"")>=0)){
                    this.setErrorMessage("Data type of "+this.arrExpressions[this.arrIndex]+" is not consistent with derived field data type");
                    return false;
                }
                
          }
        
        return true;
    },
    isOperator:function(index){
       return (this.operators.indexOf(this.arrExpressions[index])>-1);
        
    },
    validatePatternAfterOperand:function(){
        if(this.isExpressionArrayHaveNextElement()){
           if(this.isNextElementOpeningBracket()){
                this.setErrorMessage('Misplaced '+this.openingBrackets +' after '
                                    +this.arrExpressions[this.arrIndex]);
                return false;
            
           } 
           if(this.isNextElementComma()){
                this.setErrorMessage(', cannot follow an operator');
                return false;
            
           }  
            
       }  
    },
    validateDataTypeOfOperand:function(dataType){
         if(dataType=="Text"){
             if(!this.isColumn(this.arrExpressions[this.arrIndex]) &&
              !isNaN(this.arrExpressions[this.arrIndex])  ){
              
               this.setErrorMessage("Data Type of "+this.arrExpressions[this.arrIndex]+" is not consistent with derived field data type")
               return false;
           }
           if(this.isColumn(this.arrExpressions[this.arrIndex]) 
              && this.currentColumnDataType=="Numeric"   ){
               this.setErrorMessage("Data Type of "+this.arrExpressions[this.arrIndex]+" is not consistent with derived field data type")
               return false;
           }
         }
         else if(dataType=="Numeric"){
             if(isNaN(this.arrExpressions[this.arrIndex])){
                if(this.isColumn(this.arrExpressions[this.arrIndex])
                   && this.currentColumnDataType=="Text" ){
                    this.setErrorMessage("Data Type of "+this.arrExpressions[this.arrIndex]+" is not consistent with derived field data type")
                    return false;
                }
                if(!this.isColumn(this.arrExpressions[this.arrIndex])){
                    this.setErrorMessage("Data Type of "+this.arrExpressions[this.arrIndex]+" is not consistent with derived field data type")
                    return false; 
                } 
                      
             }
         }
        return true;
    },
    
    isExpressionArrayHaveNextElement:function(){
        return (this.arrExpressions.length>[this.arrIndex+1]);
    },
    isNextElementOpeningBracket:function(){
      return   (this.arrExpressions[this.arrIndex+1]==this.openingBrackets);
    },
    isNextElementClosingBracket:function(){
      return   (this.arrExpressions[this.arrIndex+1]==this.closingBrackets);
    },
    isCurrentElementClosingBracket:function(){
      return   (this.arrExpressions[this.arrIndex]==this.closingBrackets);
    },
    isNextElementComma:function(){
      return   (this.arrExpressions[this.arrIndex+1]==",");
    },
    isCurrentElementComma:function(){
      return   (this.arrExpressions[this.arrIndex]==",");
    },
    
    processExpressionForSplitter:function(item,ctr){
        //Checking for = after > or < and combining them as single operator >= or <=
        if(item==">" || item=="<"){
            if(this.isExpressionHaveNextElement(ctr) && this.expression[ctr+1]=="="){
                item=item+"=";
               }
        }
        this.addToExpressionArray(item);
        this.incrementOpeningAndClosingBracketsCounter(item);
        console.log('processExpressionForSplitter completed');
        return true;
    },
    
    processExpressionForNonSplitter:function(item,ctr){
        this.pushItemInSubExpressionArray(item);
        if(this.isExpressionHaveNextElement(ctr) && this.isNextElementSplitter(ctr)){
           
              return (this.addFromSubExpressionToExpressionArray());
        }
        else if(!this.isExpressionHaveNextElement(ctr)){
              
                return(this.addFromSubExpressionToExpressionArray());
        }
        console.log('processExpressionForNonSplitter completed');
        return true;
    },
    incrementOpeningAndClosingBracketsCounter:function(item){
        if(item==this.openingBrackets){
             ++this.openingBracketsCount;
         } 
         if(item==this.closingBrackets){
            ++this.closingBracketsCount;
         }   
    },
    addFromSubExpressionToExpressionArray:function(){
        var item=this.subExpr.join('').trim();
        if(isNaN(item) && !this.isColumn(item) && !this.isString(item) 
           && !this.isFunction(item)){
           this.setErrorMessage(item +" is not valid");
            return false;
        }
       this.addToExpressionArray(item.trim());
       this.emptySubExpressionArray();
        return true;
    },
    addToExpressionArray:function(item){
        this.arrExpressions.push(item.trim());
    },
    emptySubExpressionArray:function(){
        this.subExpr=[];
    },
    pushItemInSubExpressionArray:function(item){
      this.subExpr.push(item);        
    },
    isOperand:function(item){
      return(!this.isFunction(item) && !this.isItemSplitter(item));
    },
    isItemSplitter:function(item){
      return (this.splitters.indexOf(item)>-1);
    },
    isColumn:function(item){
        console.log('isColumn called');
        var isColumn=false;
        if(isNaN(item) && item!=undefined){
             for(var ctrColumns=0;ctrColumns<this.columns.length;ctrColumns++){
                if(this.columns[ctrColumns].fieldName.toLowerCase()==item.toLowerCase()){
                    isColumn=true;
                    this.currentColumnDataType=this.columns[ctrColumns].dataType;
                    if(this.columns[ctrColumns].variableType=='Derived'){
                       if(this.derivedColumns.indexOf(this.columns[ctrColumns].fieldName.toLowerCase())==-1){
                        this.derivedColumns.push(this.columns[ctrColumns].fieldName.toLowerCase());
                         this.derivedColumnsDataType.push(this.columns[ctrColumns].dataType);
                       } 
                    }
                    
                }
             }
        } 
      
        console.log('isColumn called= '+isColumn);
      return isColumn;
    } ,
    isString:function(item){
        var isValid=false;
       if(item!='\'' && item!='"'){ 
          // if((item.startsWith("\"") && item.endsWith("\"") ) ){
              //||(item.startsWith("'") && item.endsWith("'")) ){
           //if(item.startsWith("'") && item.endsWith("'")){
           if(item.indexOf("'")==0 && item.indexOf("'", item.length - "'".length) !== -1){
              if(item.replace(/""/g,"").replace(/''/g,"").trim().length>0){
                    isValid= true;
                 }
                 
             }
        }
        console.log('isstring called= '+isValid);
       return isValid;
   },
   isFunction:function(item){
       if(item){
           for (var ctr=0;ctr<this.functions.length;ctr++ ) {
            if (this.functions[ctr].Name.toLowerCase() == item.toLowerCase()) {
                return true;
            }
        }
     }
       
      return false ; 
        
    }, 
   isExpressionHaveNextElement:function(index){
        var ctr=index+1;
        return (this.expression.length>ctr);
    },
   isNextElementSplitter:function(index){
        var ctr=index+1;
        return (this.splitters.indexOf(this.expression[ctr])>-1);
    },
    
   isSplitter:function(index){
      return (this.splitters.indexOf(this.expression[index])>-1);
    },
    
    incrementCounterToNextElement:function(){
         ++this.arrIndex;
    } ,
    
    setErrorMessage:function(message){
         this.component.set("v.expressionMessage",message);
         //alert(message);
    },
    
  
    
    
    
    } 
})