({
  
  doInit : function (component,event,helper){
        
        component.showSpinner();
        var $j = jQuery.noConflict(); 
         $j("#divRankParent .close").off("click").on("click",function(){
              var object=$j(this);
              var index= $j("#divRankParent .close").index(object);
              if(index>0){
                 $j(this).parent().parent().remove();
              } 
             else{
                 var errorIcn = component.find("errorIcon");
                 $A.util.removeClass(errorIcn,'slds-hide');
                 component.set('v.isValid','false');
                 component.set('v.popoverMessage','You cannot delete first row of Rank'); 
             }
              
              
           });
      $j(".selectRank:last").on("change",function(){
          helper.setOrderOptions(component,event,$j(this),$j(this).val());
          
          });  
     

      component.set("v.cssStyle", ".forceStyle .viewport .oneHeader.slds-global-header_container{z-index:0 !important;}   .forceStyle .viewport.oneHeader.desktop {z-index:0 !important;} .forceStyle.desktop .viewport{overflow:hidden}");
      component.getInputTableName();
      component.getBusinessRuleTypeValues();  
      component.getTypesListData();
      component.getFieldValues();  
      
},
    
   getInputTableName:function (component, event, helper) {
      var actionInputTableName = component.get("c.getInputTabName");
      var scenarioRuleInstanceId = component.get("v.scenarioRuleInstanceId");
      actionInputTableName.setParams({"scenarioRuleInstanceDetailsId":scenarioRuleInstanceId}); 
      actionInputTableName.setCallback(this, function(response){
                 var state = response.getState();
                 if(component.isValid() && state==="SUCCESS")
                 {
                    var itemsSample = response.getReturnValue();   
                    component.set("v.tableName",itemsSample[0][common.getKey(component,"Table_Display_Name__c")]); 
                }else{
                    console.log("Failed with state:  "+ state);
                }
      });
         
      $A.enqueueAction(actionInputTableName);
  },  
    
   getBusinessRuleTypeValues : function(component, event, helper) {
        var action = component.get("c.getBusinessRuleTypeMaster");
        action.setParams({ "componentTypeMasterId" : component.get("v.componentTypeMasterId")});
        action.setCallback(this, function(response){ 
        var state = response.getState();
        if(component.isValid() && state==="SUCCESS")
        {
                var opts = [];
                var arrRuleTypes=response.getReturnValue();
                  for(var ctr=0;ctr<arrRuleTypes.length;ctr++){
                    if(arrRuleTypes[ctr].Name=="Rank"){
                        opts.push({label:arrRuleTypes[ctr].Id, value: arrRuleTypes[ctr].Name,selected:arrRuleTypes[ctr].Id});
                        component.set("v.selectedType",arrRuleTypes[ctr].Id);  
                      }else{
                        opts.push({label:arrRuleTypes[ctr].Id, value: arrRuleTypes[ctr].Name});
                      }
                  }
                component.set("v.typeoptions",opts); 
                
        }else{
                console.log("Failed with state:  "+ state);
        }
        });
        $A.enqueueAction(action);
    },
    
   getTypesListData:function(component, event, helper){
   
      var actionTypeList = component.get("c.getTypesList");
      actionTypeList.setCallback(this, function(response){
      var state = response.getState();  
    
      if(component.isValid() && state==="SUCCESS")
         {
             var ruleList = [];
             var dataTypeList = [];
             var valueTypeList = [];
             var itemsSample = response.getReturnValue();    
             var selectedDataType='';
             var selectedValueType='';
             for(var ctr=0;ctr<itemsSample.length;ctr++){
                    if(itemsSample[ctr][common.getKey(component,"Type__c")]=="Data Type")
                    {
                        if(itemsSample[ctr][common.getKey(component,"Value__c")] == 'Numeric'){
                            dataTypeList.push({label:itemsSample[ctr][common.getKey(component,"Value__c")], value: itemsSample[ctr][common.getKey(component,"Value__c")]});
                          component.set("v.datatypeoptions",dataTypeList);
                          if(selectedDataType==''){
                            selectedDataType=itemsSample[ctr][common.getKey(component,"Value__c")];
                          }
                        }
                    }
                    
                    if(itemsSample[ctr][common.getKey(component,"Type__c")]=="Value Type")
                    {
                        valueTypeList.push({label:itemsSample[ctr][common.getKey(component,"Value__c")], value: itemsSample[ctr][common.getKey(component,"Value__c")]});
                       component.set("v.valueoptions",valueTypeList);
                       if(selectedValueType==''){
                         selectedValueType=itemsSample[ctr][common.getKey(component,"Value__c")];
                       }
                    }
             }
            component.set("v.selectedDataType", selectedDataType);

            component.set("v.selectedValueType", selectedValueType);
            
            var stringList = [];
            for(var ctr=0;ctr<itemsSample.length;ctr++){
                  if(itemsSample[ctr][common.getKey(component,"Type__c")]=="Text" || itemsSample[ctr][common.getKey(component,"Type__c")]=="Numeric" 
                     || itemsSample[ctr][common.getKey(component,"Type__c")]=="Date"){
                      stringList.push({label:itemsSample[ctr].Label, value: itemsSample[ctr][common.getKey(component,"Value__c")],type:itemsSample[ctr][common.getKey(component,"Type__c")]});
                  }
            }  
            component.set("v.orderByList",stringList);
            
            
         }
         else{
            console.log("Failed with state:  "+ state);
          }
     });
     
      $A.enqueueAction(actionTypeList);
  },
    
   getFieldValues : function(component, event, helper) {
             console.log('Inside getFieldValues');
             var scenarioRuleInstanceId = component.get("v.scenarioRuleInstanceId");
             var actionGetFields = component.get("c.GetSourceAndDerivedFields");
             actionGetFields.setParams({"scenarioRuleInstanceDetailsId":scenarioRuleInstanceId}); 
             actionGetFields.setCallback(this, function(response){
              
                var state = response.getState();  
                if(component.isValid() && state==="SUCCESS"){
                      var discreteFields = [];
                      var selectedRankLevel = '';
                      var items = response.getReturnValue(); 
                          component.set("v.items",items); 
                          for(var i=0;i<items.length;i++){
                              if(items[i].valueType== 'Discrete'){
                                discreteFields.push(items[i]);  
                                if(selectedRankLevel==''){
                                 selectedRankLevel=items[i].fieldName;
                               }
                                
                              }
                          } 
                          component.set("v.selectedField",selectedRankLevel);
                         component.set("v.fieldsList",discreteFields);
                         component.populateRankValues();
                      
                      
                }
                 else{
                        console.log("Failed with state:  "+ state);
                }
              });
              $A.enqueueAction(actionGetFields); 
  },
    
    populateRankValues:function(component,event,helper) {
       
        console.log('Inside populateRankValues');
        var items=component.get("v.items");
        var arrOptions=[];
        for(var ctr=0;ctr<items.length;ctr++){
                 if(ctr===0){
                   arrOptions.push("<option dataType=\""+items[0].dataType+"\" value=\""+items[0].fieldName+"\" selected='selected'>"+items[0].fieldName+"</option>")  
                 }
                 else{
                   arrOptions.push("<option dataType=\""+items[0].dataType+"\" value=\""+items[ctr].fieldName+"\">"+items[ctr].fieldName+"</option>")  
                 }
              
        }
       
        var $j = jQuery.noConflict(); 
        $j("#divRankParent .selectRank:last").html(arrOptions.join('')); 
        var objDropdown = $j("#divRankParent .selectRank:last");
        var selectedColumn = $j("#divRankParent .selectRank:last").val();
        helper.setOrderOptions(component,event,objDropdown,selectedColumn);
       if(component.get("v.mode")=="Update"){
            component.getExistingDetails();
        }
        else{
            component.hideSpinner();
        } 
        
        
    },
    
    getExistingDetails : function(component, event, helper) {
      

            console.log('Inisde getExistingDetails');
            var action = component.get("c.GetRankType");
            action.setParams({"businessRuleId":component.get("v.businessRuleTypeId")}); 
            action.setCallback(this, function(response){
            var state = response.getState();
            if(component.isValid() && state==="SUCCESS")
                 {
                    var itemsSample = response.getReturnValue(); 
                    console.log(itemsSample);
                    component.set("v.ruleName",itemsSample[0].lstWbusinessRules[0].Name);
                    
                    component.set("v.fieldName",itemsSample[0].lstWrankRules[0][common.getKey(component,"Field_Name__c")]);
                    
                    component.set("v.selectedDataType",itemsSample[0].lstWrankRules[0][common.getKey(component,"data_type__c")]);
                     
                    component.set("v.selectedValueType",itemsSample[0].lstWrankRules[0][common.getKey(component,"value_type__c")]);
                     
                    component.set("v.selectedField",itemsSample[0].lstWrankPartitions[0][common.getKey(component,"Partition_Field__c")]);
                    var $j=jQuery.noConflict(); 
                     $j("#divRankParent .selectRank:last").val(itemsSample[0].lstWrankOrder[0][common.getKey(component,"Order_Field__c")]);
                       helper.setOrderOptions(component,event,$j("#divRankParent .selectRank:last"),itemsSample[0].lstWrankOrder[0][common.getKey(component,"Order_Field__c")]);
                    $j("#divRankParent .selectOrder:last").val(itemsSample[0].lstWrankOrder[0][common.getKey(component,"Order_Clause__c")]);
                    for(var ctr=1;ctr<itemsSample[0].lstWrankOrder.length;ctr++){
                         component.createRankFields();
                        
                        $j("#divRankParent .selectRank:last").val(itemsSample[0].lstWrankOrder[ctr][common.getKey(component,"Order_Field__c")]);
                         var objDropdown = $j("#divRankParent .selectRank:last");
                        var selectedColumn = $j("#divRankParent .selectRank:last").val();
                        helper.setOrderOptions(component,event,objDropdown,selectedColumn);
                        $j("#divRankParent .selectOrder:last").val(itemsSample[0].lstWrankOrder[ctr][common.getKey(component,"Order_Clause__c")]); 
                    }
                  }else{
                    console.log("Failed with state:  "+ state);
                    }
                component.hideSpinner();
            });
            $A.enqueueAction(action); 
    },
    
    createRankFields:function(component, event, helper) {
          
          var $j = jQuery.noConflict();
          $j("#divRankParent .slds-p-bottom_small:last").clone().appendTo("#divRankParent");
        
         $j("#divRankParent .slds-p-bottom_small:eq(1)").find(".slds-float_right:eq(0)").html("Then By");
        
          helper.setOrderOptions(component,event,$j("#divRankParent .selectRank:last"),$j("#divRankParent .selectRank:last").val());
          $j(".selectRank:last").on("change",function(){
            helper.setOrderOptions(component,event,$j(this),$j(this).val());
          }); 
          
          $j("#divRankParent .close").off("click").on("click",function(){
                 var object=$j(this);
                  var index= $j("#divRankParent .close").index(object);
                  if(index>0){
                     $j(this).parent().parent().remove();
                  } 
                 else{
                     var errorIcn = component.find("errorIcon");
                     $A.util.removeClass(errorIcn,'slds-hide');
                     component.set('v.isValid','false');
                     component.set('v.popoverMessage','You cannot delete first row of Rank'); 
                 }
              
              
           });
          $j(".up").off("click").on("click",function(){
          
               var currentDiv= $j(this).parent().parent();
               var prevDiv=currentDiv.prev();
               if(prevDiv.length>0){
                prevDiv.before(currentDiv);
               }
               $j("#divRankParent .slds-p-bottom_small:eq(0)").find(".slds-float_right:eq(0)").html("Rank By");
               $j("#divRankParent .slds-p-bottom_small:eq(1)").find(".slds-float_right:eq(0)").html("Then By");
              
             
               
         });

          $j(".down").off("click").on("click",function(){
             var currentDiv= $j(this).parent().parent();
             var nextDiv=currentDiv.next();
             if(nextDiv.length>0){
               nextDiv.after(currentDiv);
             }
             $j("#divRankParent .slds-p-bottom_small:eq(0)").find(".slds-float_right:eq(0)").html("Rank By");
             $j("#divRankParent .slds-p-bottom_small:eq(1)").find(".slds-float_right:eq(0)").html("Then By");
            
          });
    },

    closeButton : function(component, event, helper) {
       component.set("v.cssStyle", ".forceStyle .viewport.oneHeader.desktop {z-index:5} .forceStyle.desktop .viewport{overflow:visible}");
         var sceId=component.get("v.scenarioId");    
         var evt = $A.get("e.force:navigateToComponent");
         evt.setParams({
                componentDef : "c:CPGDerivedFieldsList",
                componentAttributes: {
                     "scenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId"),
                     workSpaceId: component.get("v.workSpaceId"),
                    "namespace":component.get("v.namespace"),
                    summarizedViewFlag : component.get("v.summarizedViewFlag")
                }
         });
         $A.get('e.force:refreshView').fire();
         evt.fire();
    },
 

    saveDetails : function(component, event, helper) {
        
        var rankRuleName = component.get("v.ruleName");
        var rankFieldName = component.get("v.fieldName"); 
        var dataType = component.get("v.selectedDataType");  
        var valueType = component.find("aura_valueType").get("v.value");  
        var rankMethodology = component.find("aura_rankMethd").get("v.value"); 
        var ranklevel = component.find("rankLevel").get("v.value"); 
        var msgdiv = component.find('divmsg');    
        component.restrictForSameValueInRankOrder();
        helper.validateRule(component, event, helper); 
        var errorMsg = component.get("v.errorMsg");  
        console.log('errorMsg-->>'+errorMsg)
        if(errorMsg){
          component.set("v.errorMsg", errorMsg );
          component.set("v.popoverMessage",errorMsg);
          console.log("popover message assigned" +component.get("v.popoverMessage"));
          var errorIcn = component.find("errorIcon");
          $A.util.removeClass(errorIcn,'slds-hide'); 
        }else{  
               component.showSpinner();
                component.set("v.errorMsg","");
                //var jsonBusinessRules={"Business_Rules__c":{}};
                var businessRulekey=common.getKey(component,"Business_Rules__c");
                var jsonBusinessRules={};
                var arrRankRuleDetail=[];
                var arrRankPartitionDetail=[]; 
                var arrRankOrder=[];
                var arrBusinessRuleFieldMapDetail=[]; 
                jsonBusinessRules[businessRulekey]={};
                jsonBusinessRules[businessRulekey][common.getKey(component,"Business_Rule_Type_Master__c")]=component.get("v.selectedType"); 
                jsonBusinessRules[businessRulekey]["Name"]=rankRuleName;
                jsonBusinessRules[businessRulekey][common.getKey(component,"Is_Derived__c")]=true;
                jsonBusinessRules[businessRulekey][common.getKey(component,"Is_Mandatory__c")]=true;
                jsonBusinessRules[businessRulekey][common.getKey(component,"ScenarioRuleInstanceDetails__c")]=component.get("v.scenarioRuleInstanceId");
                var rankRuleNamekey = common.getKey(component,"Name");
                var rankMethodologykey = common.getKey(component,"rank_methodology__c");
                var rankFieldNamekey = common.getKey(component,"Field_Name__c");
                var valueTypekey = common.getKey(component,"value_type__c");
                var dataTypekey = common.getKey(component,"data_type__c");
                var jsonRankRuleDetail={};
                jsonRankRuleDetail[rankRuleNamekey]=rankRuleName;
                jsonRankRuleDetail[rankMethodologykey]=rankMethodology;
                jsonRankRuleDetail[rankFieldNamekey]=rankFieldName;
                jsonRankRuleDetail[valueTypekey]=valueType;
                jsonRankRuleDetail[dataTypekey]=dataType;
                arrRankRuleDetail.push(jsonRankRuleDetail);
                console.log(arrRankRuleDetail);
                var rankLevelkey = common.getKey(component,"Partition_Field__c");
                var jsonRankPartitionDetail={};
                jsonRankPartitionDetail[rankLevelkey]=ranklevel;
                arrRankPartitionDetail.push(jsonRankPartitionDetail);
                var columns=component.get("v.items");
                var jsonBusinessRuleFieldMapDetail={};
                var IOflagKey = common.getKey(component,"IO_flag__c");
                var paramNameKey = common.getKey(component,"Param_Name__c");
                var paramDataTypeKey = common.getKey(component,"Param_Data_Type__c");
                var paramTypeKey = common.getKey(component,"Param_type__c");

                for(var ctr=0;ctr<columns.length;ctr++){
                        if(columns[ctr].fieldName.toLowerCase()==ranklevel.toLowerCase()
                           && columns[ctr].variableType=="Derived"){
                           jsonBusinessRuleFieldMapDetail[IOflagKey]="I";
                           jsonBusinessRuleFieldMapDetail[paramNameKey]=ranklevel.toLowerCase();
                           jsonBusinessRuleFieldMapDetail[paramDataTypeKey]=columns[ctr].dataType;
                           jsonBusinessRuleFieldMapDetail[paramTypeKey]="Derived";
                           arrBusinessRuleFieldMapDetail.push(jsonBusinessRuleFieldMapDetail);
                           break;
                        }
                }
                var j$ = jQuery.noConflict(); 
                j$("#divRankParent .slds-p-bottom_small").each(
                function(index){
                            var json={};
                            var displaySeq = common.getKey(component,"Display_Seq_No__c");
                            var orderField = common.getKey(component,"Order_Field__c");
                            var orderClause = common.getKey(component,"Order_Clause__c");
                            json[displaySeq]=index+1;
                            json[orderField]=j$(this).find("select.selectRank").val();
                    
                            
                    /*for(var ctr=0;ctr<columns.length;ctr++){
                        if(columns[ctr].fieldName.toLowerCase()==j$(this).find("select.selectRank").val().toLowerCase()
                           && columns[ctr].variableType=="Derived"){
                              jsonBusinessRuleFieldMapDetail[IOflagKey]="I";
                              jsonBusinessRuleFieldMapDetail[paramNameKey]=j$(this).find("select.selectRank").val().toLowerCase();
                              jsonBusinessRuleFieldMapDetail[paramDataTypeKey]=columns[ctr].dataType;
                              jsonBusinessRuleFieldMapDetail[paramTypeKey]="Derived";
                              arrBusinessRuleFieldMapDetail.push(jsonBusinessRuleFieldMapDetail);
                              break;
                        }
                    }*/
                    
                    
                    
                            json[orderClause]=j$(this).find("select.selectOrder").val();
                            arrRankOrder.push(json);
                        }
                )
                jsonBusinessRuleFieldMapDetail[IOflagKey]="O";
                jsonBusinessRuleFieldMapDetail[paramNameKey]=rankFieldName;
                jsonBusinessRuleFieldMapDetail[paramDataTypeKey]=dataType;
                jsonBusinessRuleFieldMapDetail[paramTypeKey]="Derived";
                arrBusinessRuleFieldMapDetail.push(jsonBusinessRuleFieldMapDetail);
                var action;
                if(component.get("v.mode")=="Update"){
                    jsonBusinessRules[businessRulekey]["Id"]=component.get("v.businessRuleTypeId");
                    //jsonBusinessRules.Business_Rules__c["Id"]=component.get("v.businessRuleTypeId");
                    action = component.get("c.UpdateRule");
                }else{
                    action = component.get("c.insertRule");
                }
                action.setParams({strJsonBusinessRules:JSON.stringify(jsonBusinessRules),
                                  strJsonRankRuleDetail:JSON.stringify(arrRankRuleDetail),
                                  strJsonRankPartitionDetails:JSON.stringify(arrRankPartitionDetail),
                                  strJsonRankOrderDetails:JSON.stringify(arrRankOrder),
                                  strJsonBusinessRuleFieldMapDetails:JSON.stringify(arrBusinessRuleFieldMapDetail),
                                  scenarioInstanceId:component.get("v.scenarioRuleInstanceId")});
                action.setCallback(this, function(response) {
                    var data= response.getReturnValue();
                    var state = response.getState();  
                    if(component.isValid() && state==="SUCCESS")
                    {

                        var itemsSample = response.getReturnValue(); 
                        component.set("v.errorMsg", data.status );
                        if(data.status=="error"){
                            component.set("v.popoverMessage",data.msg);
                            console.log("popover message assigned" +component.get("v.popoverMessage"));
              
                        
                            var errorIcn = component.find("errorIcon");
                            $A.util.removeClass(errorIcn,'slds-hide'); 
                        }
                        else{
                             //$A.util.removeClass(msgdiv,'slds-hide'); 
                            component.closeButton();
                            component.set("v.ruleName",""); 
                            component.set("v.fieldName","");
                        }
                       
                        
                    }
                    else{
                        console.log("Failed with state:  "+ state);
                    }
                   component.hideSpinner(); 
                });
                
            $A.enqueueAction(action);  
                  
        }
      
    },

    handleDestroy:function(component,event,helper){} ,
    destoryCmp : function (component, event, helper) {
        component.destroy();
    },


   // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },

    restrictInputsField :function(component, event, helper){
      var text=component.get("v.fieldName"); 
      text = text.replace(/(?!^-)[^0-9\w\s]/g, ''); 
      component.set("v.fieldName", text);
      var globalID = (component.find('fName').getGlobalId());
      var cursorPosition = document.getElementById(globalID);
    
    },
   
    restrictInputsRule :function(component, event, helper){
      var text=component.get("v.ruleName"); 
      text = text.replace(/(?!^-)[^0-9a-zA-Z_\s]/g, ''); 
      component.set("v.ruleName", text);
   
    },

    restrictForSameValueInRankOrder : function(component, event, helper){
      var j$ = jQuery.noConflict(); 
      var columnName;
     
      component.set("v.isRankOrderNameSame",false);
      console.log('Length-->'+j$("#divRankParent .selectRank").length);
      j$("#divRankParent .selectRank").each(
              function(index){
                     var current=j$(this);
                        j$("#divRankParent .selectRank").each(

                            function(index2){
                              if(index2>index){
                                if(current.val()==j$(this).val()){
                                      component.set("v.isRankOrderNameSame",true); 
                                      return false;
                                }

                               }
                                
                            }
                          );
                        if(component.get("v.isRankOrderNameSame")==true){
                             return false;
                        }
              }
        )
      console.log(component.get("v.isRankOrderNameSame"));
    },
    onRuleChange : function(component, event, helper) {    
    console.log("inside onRuleChange method");
    var ruleVal = event.currentTarget.value; ////component.get("v.selectedType");
    var ruleText;  
   var ruleOptions=component.get("v.typeoptions");
   console.log(ruleOptions[0]);
   for(var ctr=0;ctr<ruleOptions.length;ctr++){
     if(ruleOptions[ctr].label==ruleVal){
        ruleText=ruleOptions[ctr].value;
        break;
     }
   }
    console.log("Rule val is = "+ruleVal);
        if(ruleText=='Category'){
          
            var sceId=component.get("v.scenarioId");    
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                //componentDef : "c:CPGCategory",
                componentDef : "c:CPGCallPlanning",
                componentAttributes: {
                    "scenarioRuleInstanceId" : component.get("v.scenarioRuleInstanceId"),
                     workSpaceId: component.get("v.workSpaceId"),
                    "ComponentName":"Category",
                    "namespace":component.get("v.namespace"),
                    summarizedViewFlag : component.get("v.summarizedViewFlag")
                }
            });
          evt.fire();   
            
        }
        
        else if(ruleText=='Rank'){
            console.log("its rank");
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:CPGRankingCreateDerivedField",
                componentAttributes: {
                    mode: "Create",
                    scenarioRuleInstanceId : component.get("v.scenarioRuleInstanceId"),
                    workSpaceId: component.get("v.workSpaceId"),
                    "namespace":component.get("v.namespace"),
                    summarizedViewFlag : component.get("v.summarizedViewFlag")
                }
            });
            $A.get('e.force:refreshView').fire();
            evt.fire();
            
        }
        
            else if(ruleText=='Compute'){
          console.log("its compute");
           var evt = $A.get("e.force:navigateToComponent");
                   evt.setParams({
                      componentDef : "c:CPGComputationDerivedField",
                      componentAttributes: {
                            mode: "Create", 
		             		scenarioRuleInstanceId : component.get("v.scenarioRuleInstanceId"),
                    workSpaceId: component.get("v.workSpaceId"),
                    "namespace":component.get("v.namespace"),
                    summarizedViewFlag : component.get("v.summarizedViewFlag")
                      }
                  });
       $A.get('e.force:refreshView').fire();
                  evt.fire();
    
    }
    
    },

    handleMouseEnter : function(component, event, helper) {
        var popover = component.find("popover");
        $A.util.removeClass(popover,'slds-hide');
    },
    //make a mouse leave handler here
    handleMouseLeave : function(component, event, helper) {
        var popover = component.find("popover");
        $A.util.addClass(popover,'slds-hide');
    },



})