({
  setOrderOptions : function(component,event,objDropdown,selectedColumn) {
    
        console.log('Inisde setOrderOptions');
        var selecteddataType;
        var orderOptions=[];
        var $j = jQuery.noConflict();
       // var objDropdown = $j("#divRankParent .selectRank:last");
       // var selectedColumn = $j("#divRankParent .selectRank:last").val();
        var items=component.get("v.items");

        var orderByList=component.get("v.orderByList");
        for(var ctr=0;ctr<items.length;ctr++){
                if(items[ctr].fieldName==selectedColumn){
                    selecteddataType=items[ctr].dataType;
                    break;
                }
        }
        orderOptions=[];
        for(var ctr=0;ctr<orderByList.length;ctr++){
            if(selecteddataType==orderByList[ctr].type){
              orderOptions.push("<option value='"+orderByList[ctr].value+"'>"+orderByList[ctr].label+"</option>") ;
                
            }
        }
        var select=objDropdown.parent().parent().parent().parent().next().find("select");
        var lengthVals = orderOptions.join("");

        if(lengthVals.length >0) {
          select.html(orderOptions.join(""));
        }
        else{
        console.log('====== No Values');
        }
  },
    validateRule : function(component,event,helper) {
        console.log('Inside helper validation');
        var errorMsg = '';
        var rankRuleName = component.get("v.ruleName");
        var rankFieldName = component.get("v.fieldName");  
        var dataType = component.get("v.selectedDataType"); 
        var checkRankOrderNameSame = component.get("v.isRankOrderNameSame");
        
        if(rankRuleName == undefined || !rankRuleName.trim()){
            errorMsg += 'Rule Name, ';
            //$A.util.addClass(rankRuleName , 'errorInput');
            component.set("v.popoverMessage","Please enter derived rule name");
            var sname = component.find("rName");
            $A.util.addClass(sname , 'errorInput');
            
            var errorIcn = component.find("errorIcon");
            $A.util.removeClass(errorIcn,'slds-hide');

        }else if(rankFieldName == undefined || !rankFieldName.trim()){
            errorMsg += 'Field Name, ';
            component.set("v.popoverMessage","Please enter derived field name");
            var sname = component.find("fName");
            $A.util.addClass(sname , 'errorInput');
            
            var errorIcn = component.find("errorIcon");
            $A.util.removeClass(errorIcn,'slds-hide');
        }else if(checkRankOrderNameSame){
             component.set("v.popoverMessage","Rank Order Name cannot be equal");
            errorMsg += 'Rank Order Name cannot be equal, ';
            //$A.util.addClass(checkRankOrderNameSame , 'errorInput');
        }
         errorMsg = errorMsg.slice(0, errorMsg.length-2);
         var errorThrown = 'Required field(s) missing : '+errorMsg;
        if(errorMsg){
             component.set("v.errorMsg",errorThrown);
         }
        else{
            component.set("v.errorMsg","");
        }
        
    },
    validatePattern:function(){
        
          var arrInvalidPatterns=['()',')(','(+','(-','(*','(/','(,',',)','+)','-)','*)','/)'];
          var isValidPattern=true;
        
          if(this.expression[0]==')'){
            isValidPattern=false;
            this.setErrorMessage('Expression cannot start with )');
            return false;
          }
          if(this.operators.indexOf(this.expression[0])>-1){
            isValidPattern=false;
            this.setErrorMessage('Expression cannot start with '+this.expression[0]);
            return false;
          }
          if(this.expression.search(/[-+*\/][-+*\/]/g)>-1){
            this.setErrorMessage('An operator cannot follow another operator');
            isValidPattern=false;
            return false;
        }
        if(this.expression.search(/[-+*\/],/g)>-1){
            this.setErrorMessage('comma cannot follow an operator');
            isValidPattern=false;
            return false;
        }
        if(this.expression.search(/,[-+*\/]/g)>-1){
            this.setErrorMessage('operator cannot follow a comma');
            isValidPattern=false;
            return false;
        }
        if(this.expression.search(/\)\w/g)>-1){
            this.setErrorMessage('operand cannot follow a closing bracket');
            isValidPattern=false;
            return false;
        } 
        
        for(var ctrInvalidPatterns=0;ctrInvalidPatterns<arrInvalidPatterns.length;ctrInvalidPatterns++){
            if(this.expression.indexOf(arrInvalidPatterns[ctrInvalidPatterns])>-1){
                console.log(arrInvalidPatterns[ctrInvalidPatterns]); 
                isValidPattern=false; 
                this.setErrorMessage(arrInvalidPatterns[ctrInvalidPatterns] +' is not valid'); 
                break;
            }
         }
        
         if(!isValidPattern){
           return false;
         }
         else{
           isValidPattern=this.validatePatternAgainstDataType();
         }
       return isValidPattern;
        
    },
})