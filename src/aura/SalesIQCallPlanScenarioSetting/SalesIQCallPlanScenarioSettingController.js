({  
    doInit: function(component,event,helper)
    {
        component.set("v.showSpinner", true);
        var callPlanSection = component.find('callPlanSection');
        console.log(callPlanSection);
        $A.util.addClass(callPlanSection, 'slds-fade-in-open');
        $A.util.removeClass(callPlanSection,'slds-fade-in-hide');

        if(component.get("v.moduleName") == 'Canvas')
        {
            var dataMappingSection = component.find('dataMappingSection');
            $A.util.addClass(dataMappingSection, 'slds-is-open');
            $A.util.removeClass(dataMappingSection,'slds-hide');
        }

        var orgNamespaceAction = component.get("c.getOrgNamespace");
        orgNamespaceAction.setCallback(this,function(response)
        {
            var state = response.getState();
            console.log('Namespace : ',response.getReturnValue());
            if(state == 'SUCCESS' && response.getReturnValue() != null)
            {
                component.set("v.namespace",response.getReturnValue());

                component.set("v.modalStyle", ".forceStyle .viewport .oneHeader {z-index:0 !important;  } .uiDatePicker {top: unset !important; bottom : 0px !important}");
        
                var action = component.get("c.initCallPlan");

                action.setParams
                ({
                    scenarioId : component.get("v.scenarioId")
                });
                action.setCallback(this,function(response)
                {
                    var state = response.getState();
                    console.log('state : '+state);
                    console.log('response : ',response.getReturnValue());
                    if(state == 'SUCCESS' && response.getReturnValue() != null)
                    {
                        var ns = component.get("v.namespace");
                        var recordsDetails = response.getReturnValue();
                        console.log(recordsDetails.brmsMetadataList);
                        console.log(recordsDetails.pacpMetaDataList);
                        var alreadyMappedFields = recordsDetails.alreadyMappedField;
                        console.log('alreadyMappedFields--->>>');
                        console.log(alreadyMappedFields);
                        var requiredFields = ['Account__c','Position__c','isTarget__c','isTarget_Updated__c','isTarget_Approved__c','Call_Frequency_Approved__c','Call_Frequency_Updated__c','Call_Frequency__c'];
                        var dataMappingList = [];

                        for(var i=0;i<alreadyMappedFields.length;i++)
                        {
                            if(requiredFields.indexOf(alreadyMappedFields[i].pacpField) != -1)
                            {
                                dataMappingList.push(alreadyMappedFields[i])
                            }
                        }
                        

                        for(var i=0;i<requiredFields.length;i++)
                        {
                            var dataMap = {};
                            dataMap.pacpField = requiredFields[i];
                            if(alreadyMappedFields)
                            {
                                if(alreadyMappedFields[requiredFields[i]] == '--None--' || alreadyMappedFields[requiredFields[i]] == undefined)
                                {
                                    dataMap.systemField = '--None--';
                                }
                                else
                                {
                                    dataMap.systemField = alreadyMappedFields[requiredFields[i]];
                                }
                            }
                            else
                            {
                                dataMap.systemField == '--None--';
                            }
                            dataMap.isRequired = true;
                            dataMappingList.push(dataMap);
                        }

                        Object.keys(alreadyMappedFields).forEach(function(key) 
                        {
                            if(requiredFields.indexOf(key) == -1)
                            {
                                var dataMap = {};
                                dataMap.pacpField = key;
                                dataMap.systemField = alreadyMappedFields[key];
                                dataMap.isRequired = false;
                                dataMappingList.push(dataMap);
                            }
                        });
                        console.log('dataMappingList--->>>');
                        console.log(dataMappingList);

                        /*for(var j=0; j<pacpMetaDataRecords.length;j++){
                            var dataMap = {};
                            if(alreadyMappedFields != undefined){
                                if(!(alreadyMappedFields[pacpMetaDataRecords[j]] == '')){
                                    dataMap.pacpField = pacpMetaDataRecords[j];
                                    dataMap.systemField = alreadyMappedFields[pacpMetaDataRecords[j]];
                                }
                                dataMap.isRequired = false;
                                dataMappingList.push(dataMap);
                            }
                            
                        }*/

                        
                        component.set("v.dataMappingList",dataMappingList);
                        component.set("v.callPlanWrapper",recordsDetails);

                        var scenarioJsRecord = {"id": recordsDetails.scenarioRecord.Id, 
                                                "name": recordsDetails.scenarioRecord[ns+'Scenario_Name__c'] , 
                                                "stage": recordsDetails.scenarioRecord[ns+'Scenario_Stage__c'],  
                                                "source": recordsDetails.scenarioRecord[ns+'Scenario_Source__c'], 
                                                "teamInstance": recordsDetails.scenarioRecord[ns+'Team_Instance__r'].Name, 
                                                "team": recordsDetails.scenarioRecord[ns+'Team_Instance__r'][ns+'Team__r'].Name,
                                                "crTracking": recordsDetails.scenarioRecord[ns+'CR_Tracking__c'],
                                                "teamInstanceConfig": recordsDetails.scenarioRecord[ns+'Team_Instance_Config__c'],
                                                "tableViewConfig": recordsDetails.scenarioRecord[ns+'Table_View_Config__c'],
                                                "tableColumnConfig": recordsDetails.scenarioRecord[ns+'Table_Column_Config__c'],
                                                "guardRailConfig": recordsDetails.scenarioRecord[ns+'KPI_Card_Guardrail_Config__c'],
                                                "approvalConfig": recordsDetails.scenarioRecord[ns+'Approval_Config__c'],
                                                "summaryCardConfig": recordsDetails.scenarioRecord[ns+'Summary_Cards_Config__c']
                                            };
                        component.set("v.scenario", scenarioJsRecord);
                        
                        
                        var businessRuleSource = '';
                        if(ns != '')
                            businessRuleSource = $A.get("$Label.AxtriaSalesIQTM.CPG_Scenario_Source_Type");
                        else
                            businessRuleSource = $A.get("$Label.c.CPG_Scenario_Source_Type");

                        // Display data mapping section if scenario is from business rule and open from Call plan
                        if(scenarioJsRecord.source == businessRuleSource && component.get("v.moduleName") != 'Canvas') {
                            $A.util.removeClass(component.find('dataMappingSection'),'slds-hide');
                            $A.util.addClass(component.find('scenarioConfigSection'), 'slds-is-open');
                            $A.util.removeClass(component.find('scenarioConfigSection'),'slds-hide');
                        }
                        // File type Scenario
                        else if(component.get("v.moduleName") != 'Canvas') {
                            $A.util.addClass(component.find('scenarioConfigSection'), 'slds-is-open');
                            $A.util.removeClass(component.find('scenarioConfigSection'),'slds-hide');
                        }
                        
                        component.set("v.scenarioStage", scenarioJsRecord.stage);
                        console.log(component.get("v.scenarioStage"));
                        
                        if(scenarioJsRecord.stage != 'Collaboration'){
                            component.set("v.disableChecklist", true);
                        }

                        if(scenarioJsRecord.stage == 'Design'){
                            component.set("v.disabled" ,true);
                            var progress_design = component.find('progress_design');
                            $A.util.addClass(progress_design, 'slds-is-active');
                        }

                        else if(scenarioJsRecord.stage == 'Collaboration'){
                            var progress_collaboration = component.find('progress_collaboration');
                            $A.util.addClass(progress_collaboration, 'slds-is-active');
                        }

                        else if(scenarioJsRecord.stage == 'Published'){
                            var progress_publishing = component.find('progress_publishing');
                            $A.util.addClass(progress_publishing, 'slds-is-active');
                        }

                        else if(scenarioJsRecord.stage == 'Live'){
                            var progress_live = component.find('progress_live');
                            $A.util.addClass(progress_live, 'slds-is-active');
                        }

                        if(scenarioJsRecord.stage == 'Past'){
                            var progress_past = component.find('progress_past');
                            $A.util.addClass(progress_past, 'slds-is-active');
                        }
                        
                        component.set("v.showSpinner", false);
                        
                    }else{
                        console.log('error----------');
                    }
                });
                $A.enqueueAction(action);
            }
        });
        $A.enqueueAction(orgNamespaceAction);
    },   

    cancelRequest: function(component)
    {
        /*console.log('closePopup called');
        
        var callPlanSection = component.find('callPlanSection');
        $A.util.removeClass(callPlanSection, 'slds-fade-in-open');
        $A.util.addClass(callPlanSection,'slds-fade-in-hide');
        component.set("v.message", '');
        component.set("v.disabled", false);
        component.set("v.disableChecklist", false);
        component.set("v.showConfirmBtn", false);*/

        component.destroy();   
    },

    enableScenarioSharing : function(component){
        console.log('Enable Scenario Sharing');
    },

    openTeamInstanceRecord : function(component){
        console.log('open team instance record page');
        var namespace = component.get("v.namespace");
        var listView = component.get("c.getListViewID");
         listView.setParams({
            objectName : namespace+"CR_Team_Instance_Config__c",
            viewName : "Call Plan Table Configuration"
            
        });
        listView.setCallback(this,function(response){
            var state = response.getState();
            console.log(response.getReturnValue());
            if(state == 'SUCCESS' && response.getReturnValue() != null)
            {
                console.log('state :::: '+response.getReturnValue());
                var id = response.getReturnValue();
                window.open('/one/one.app#/sObject/'+namespace+'CR_Team_Instance_Config__c/list?filterName='+id);
            }else{
                 window.open('/one/one.app#/sObject/'+namespace+'CR_Team_Instance_Config__c/home');
            }
        });
        $A.enqueueAction(listView);
    },

    openDependencyControlRecord: function(component){
        console.log('open Dependency Control Record');
        var namespace = component.get("v.namespace");
        var action = component.get("c.getListViewID");
            action.setParams({
                objectName :namespace+"Dependency_Control__c",
                viewName : "All"
            });
            action.setCallback(this,function(response){
                var state=response.getState();
                console.log(state);
                console.log(response.getReturnValue());
                if(state=='SUCCESS' && response.getReturnValue()!='' && response.getReturnValue()!= null)
                {
                    var id=response.getReturnValue();
                    window.open('/one/one.app#/sObject/'+namespace+'Dependency_Control__c/list?filterName='+id);
                }else{
                    window.open('/one/one.app#/sObject/'+namespace+'Dependency_Control__c/home');
                }
            });
            $A.enqueueAction(action);
    },

    openPACPRecordPage : function(component){
        var namespace = component.get("v.namespace");
        var action = component.get("c.getListViewID");
        action.setParams({
            objectName :namespace+"Position_Account_Call_Plan__c",
            viewName : "All"
        });
        action.setCallback(this,function(response){
            var state=response.getState();
            console.log(state);
            console.log(response.getReturnValue());
            if(state=='SUCCESS' && response.getReturnValue()!='' && response.getReturnValue()!= null)
            {
                var id=response.getReturnValue();
                window.open('/one/one.app#/sObject/'+namespace+'Position_Account_Call_Plan__c/list?filterName='+id);
            }else{
                window.open('/one/one.app#/sObject/'+namespace+'Position_Account_Call_Plan__c/home');
            }
        });
        $A.enqueueAction(action);
    },

    openTeamInstanceObjectAttrRecord : function(component){
        console.log('open team instance object attr record');
        var namespace = component.get("v.namespace");
        var action = component.get("c.getListViewID");
         action.setParams({
            objectName : namespace+"Team_Instance_Object_Attribute__c",
            viewName : "Call Plan"
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            console.log(state);
            console.log(response.getReturnValue());
            if(state == 'SUCCESS' && response.getReturnValue() != '' && response.getReturnValue() != null)
            {
                console.log('state :::: '+response.getReturnValue());
                var id = response.getReturnValue();
                window.open('/one/one.app#/sObject/'+namespace+'Team_Instance_Object_Attribute__c/list?filterName='+id);
            }else{
                 window.open('/one/one.app#/sObject/'+namespace+'Team_Instance_Object_Attribute__c/home');
            }
        });
        $A.enqueueAction(action);
    },

    openCRApprovalRecordPage: function(component){
        console.log('open CR Approval config record');
        var namespace = component.get("v.namespace");
        var listView = component.get("c.getListViewID");
         listView.setParams({
            objectName : namespace+"CR_Approval_Config__c",
            viewName : "Call Plan"
        });
        listView.setCallback(this,function(response){
            var state = response.getState();
            if(state == 'SUCCESS' && response.getReturnValue() != '')
            {
                console.log('state :::: '+response.getReturnValue());
                var id = response.getReturnValue();
                window.open('/one/one.app#/sObject/'+namespace+'CR_Approval_Config__c/list?filterName='+id);
            }else{
                 window.open('/one/one.app#/sObject/'+namespace+'CR_Approval_Config__c/home');
            }
        });
        $A.enqueueAction(listView);
    },

    openCIMConfigRecordPage : function(component){
        console.log('open CIM config record');
        var namespace = component.get("v.namespace"); 
        var listView = component.get("c.getListViewID");
         listView.setParams({
            objectName : namespace+"CIM_Config__c",
            viewName : "Call Plan"
        });
        listView.setCallback(this,function(response){
            var state = response.getState();
            if(state == 'SUCCESS' && response.getReturnValue() != '')
            {
                console.log('state :::: '+response.getReturnValue());
                var id = response.getReturnValue();
                window.open('/one/one.app#/sObject/'+namespace+'CIM_Config__c/list?filterName='+id);
            }else{
                 window.open('/one/one.app#/sObject/'+namespace+'CIM_Config__c/home');
            }
        });
        $A.enqueueAction(listView);
    },

    checkMappingValidations : function (component,event,helper) 
    {
        var action = component.get("c.checkSaveValidations");
        action.setParams({
            mappedFieldsString : JSON.stringify(component.get("v.dataMappingList")),
            scenarioStage : component.get("v.scenarioStage"),
            scenarioId : component.get("v.scenarioId")
        });
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state == 'SUCCESS')
            {
                var namespace = component.get("v.namespace");
                //var changeDone = 'false';
                var changes = component.get("v.message");
                //var isError = 'false';

                var error = '';
                console.log('Save Response--->>'+response.getReturnValue());
                if(response.getReturnValue() == 'Success')
                {
                    //changeDone = 'true';
                    //isError = 'false';
                    component.set("v.isError", false);
                    //error = '';

                    changes += '<p> Field mapping updated or stage changed. </p>';   
                    component.set("v.showConfirmBtn", true);
                    component.set("v.message", changes);
                    component.set("v.disabled", true);
                    component.set("v.disableChecklist", true);
                }
                else if(response.getReturnValue() == 'Duplicate')
                {
                    //changeDone = 'true';
                    //isError = 'true';
                    component.set("v.isError", true);
                    error += '<p> Duplicate mapping </p>';
                    //component.set("v.showConfirmBtn", false);
                    //component.set("v.popOverClass", 'slds-rise-from-ground');
                }
                else if(response.getReturnValue() == 'Required Fields Missing')
                {
                    //changeDone = 'true';
                    //isError = 'true';
                    component.set("v.isError", true);

                    if(namespace != '')
                        error += '<p>'+$A.get("$Label.AxtriaSalesIQTM.Required_Fields_Missing")+'</p>';
                    else
                        error += '<p>'+$A.get("$Label.c.Required_Fields_Missing")+'</p>';

                    //error += '<p>Required Fields Missing.</p>';
                    //component.set("v.showConfirmBtn", false);
                    //component.set("v.popOverClass", 'slds-rise-from-ground');
                }
                else if(response.getReturnValue() == 'Nothing to update')
                {
                    /*changeDone = 'false';
                    isError = 'false';
                    component.set("v.isError", false);
                    changes += '<p>No change.</p>'
                    component.set("v.showConfirmBtn", false);*/

                    component.set("v.isError", true);
                    error += '<p>No changes done</p>'
                    //component.set("v.showConfirmBtn", false);
                    //component.set("v.popOverClass", 'slds-rise-from-ground');
                }

                if(component.get("v.isError")) {
                    component.set("v.showConfirmBtn", false);
                    component.set("v.popOverClass", 'slds-rise-from-ground');
                    component.set("v.disabled", true);
                    component.set("v.disableChecklist", true);
                }

                component.set("v.message", changes);
                component.set("v.error", error);
            }
            else
            {
                console.log('save design error');
            }
        });
        $A.enqueueAction(action);

    },

    updateScenarioRecord: function(component) 
    {
        var ns = component.get("v.namespace");
        var scenarioJsRecord = component.get("v.scenario");
        var callPlanWrapper = component.get("v.callPlanWrapper");
        var scenarioRecordNew = callPlanWrapper.scenarioRecord;
        scenarioRecordNew[ns+'Scenario_Stage__c'] = scenarioJsRecord.stage ;
        scenarioRecordNew[ns+'CR_Tracking__c'] = scenarioJsRecord.crTracking ;
        scenarioRecordNew[ns+'Team_Instance_Config__c'] = scenarioJsRecord.teamInstanceConfig ;
        scenarioRecordNew[ns+'Table_View_Config__c'] = scenarioJsRecord.tableViewConfig ;
        scenarioRecordNew[ns+'Table_Column_Config__c'] = scenarioJsRecord.tableColumnConfig ;
        scenarioRecordNew[ns+'KPI_Card_Guardrail_Config__c'] = scenarioJsRecord.guardRailConfig ;
        scenarioRecordNew[ns+'Approval_Config__c'] = scenarioJsRecord.approvalConfig ;
        scenarioRecordNew[ns+'Summary_Cards_Config__c'] = scenarioJsRecord.summaryCardConfig ;

        callPlanWrapper.scenarioRecord = scenarioRecordNew;
        component.set("v.callPlanWrapper", callPlanWrapper);
    },

    saveRequest : function(component,event)
    {
        console.log('set scenario checkbox value');
        /*var oldValueRecord = component.get("v.oldScenarioRecord");*/

        component.updateScenarioRecord();
        var newValueRecord = component.get("v.callPlanWrapper").scenarioRecord;
        var namespace = component.get("v.namespace");
        // setting this variable from all goto methods
        var newStage = component.get("v.scenarioStage");
        
        /*var changeDone = 'false';
        var isError = 'false';
        var error = '';
        var allChecklistChecked = 'false';*/
        var changes = '', businessRuleSource = '';

        //console.log(newValueRecord[namespace+'CR_Tracking__c']);
        //console.log(component.get("v.scenarioStage"));
        console.log(JSON.stringify(component.get("v.dataMappingList")));

        if(namespace != '') {
            changes = '<p>'+$A.get("$Label.AxtriaSalesIQTM.Save_Confirmation_Start")+'</p>';
            businessRuleSource = $A.get("$Label.AxtriaSalesIQTM.CPG_Scenario_Source_Type");
        } 
        else {
            changes = '<p>'+$A.get("$Label.c.Save_Confirmation_Start")+'</p>';
            businessRuleSource = $A.get("$Label.c.CPG_Scenario_Source_Type");
        }

        component.set("v.message",changes);
       
        // save on design or change from design to collaboration state
        if((newValueRecord[namespace+'Scenario_Stage__c'] == 'Design' || newStage == 'Collaboration') && newValueRecord[namespace+"Scenario_Source__c"] == businessRuleSource)
        {
            //if(newValueRecord[namespace+'Scenario_Stage__c'] != 'Collaboration')
                component.checkMappingValidations();
            /*if(newValueRecord[namespace+'Scenario_Stage__c'] != newStage)
            {
                changeDone = 'true';
                isError = 'false';
                component.set("v.isError", false);
                error = '';
                if(namespace != '')
                    changes += '<p>'+$A.get("$Label.AxtriaSalesIQTM.Stage_Changed_Confirmation")+'</p>';
                else
                    changes += '<p>'+$A.get("$Label.c.Stage_Changed_Confirmation")+'</p>';
            }

            console.log(component.get("v.message"));
            console.log(component.get("v.error"));
            component.checkConfigurationList();*/
        }

        // if previous stage was configuration or new stage is changed to published
        else if(newValueRecord[namespace+'Scenario_Stage__c'] == 'Collaboration' || newValueRecord[namespace+'Scenario_Stage__c'] == 'Publishing' || newStage == 'Publishing')
        {
            component.checkConfigurationList();
        }
    },

    confirmRequest : function(component){
        console.log('confirm request called');
        component.set("v.showSpinner", true);
        var action = component.get("c.confirmScenarioRequest");
        
        action.setParams
        ({
            newScenarioRecord : component.get("v.callPlanWrapper").scenarioRecord,
            scenarioStage : component.get('v.scenarioStage'),
            mappedFieldsString : JSON.stringify(component.get("v.dataMappingList"))
            
        });
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            console.log('state : '+state);
            console.log('response : ',response.getReturnValue());
            if(state == 'SUCCESS')
            {
                console.log('Successs---------');
                var refreshDataTable = component.getEvent("refreshComponentEvent");
                refreshDataTable.setParams({infoToPass : component.get('v.scenarioStage')});
                refreshDataTable.fire();
                 
                var scenarioJsRecord =  component.get("v.scenario");
                scenarioJsRecord.stage = component.get('v.scenarioStage');
                component.set("v.scenario", scenarioJsRecord);

                //component.set("v.scenarioRecordStage",component.get('v.scenarioStage'));
                component.set("v.showSpinner", false);
                //component.set('v.showCallPlanScenarioSetting', false);

                component.destroy();
            }
        });

        $A.enqueueAction(action);
    },

    closeConfirm : function(component){
        component.set("v.showConfirmBtn",false);
        component.set("v.disabled", false);
        component.set("v.disableChecklist", false);
        component.set("v.message", '');
    },

    goToPulishingStage : function(component){
        console.log('go topublishing stage called ----');
       
        var progress_publishing = component.find('progress_publishing');
        $A.util.addClass(progress_publishing, 'slds-is-active');   
        
        var progress_live = component.find('progress_live');
        $A.util.removeClass(progress_live, 'slds-is-active');

        var progress_collaboration = component.find('progress_collaboration');
        $A.util.removeClass(progress_collaboration, 'slds-is-active');

        var progress_design = component.find('progress_design');
        $A.util.removeClass(progress_design , 'slds-is-active');

        component.set("v.scenarioStage", 'Published');
        // enable the configuration checkboxes
        component.set("v.disabled", false);
    },

    gotoDesignStage : function(component){

        var progress_design = component.find('progress_design');
        $A.util.addClass(progress_design , 'slds-is-active');

        var progress_publishing = component.find('progress_publishing');
        $A.util.removeClass(progress_publishing, 'slds-is-active');

        var progress_live = component.find('progress_live');
        $A.util.removeClass(progress_live, 'slds-is-active');

        var progress_collaboration = component.find('progress_collaboration');
        $A.util.removeClass(progress_collaboration, 'slds-is-active');

        component.set("v.scenarioStage", 'Design');
        component.set("v.disabled", true);
        component.set("v.disableChecklist", true);
    },

    gotoCollaborationStage: function(component){
        var progress_collaboration = component.find('progress_collaboration');
        $A.util.addClass(progress_collaboration, 'slds-is-active');

        var progress_publishing = component.find('progress_publishing');
        $A.util.removeClass(progress_publishing, 'slds-is-active');

        var progress_live = component.find('progress_live');
        $A.util.removeClass(progress_live, 'slds-is-active');

        var progress_design = component.find('progress_design');
        $A.util.removeClass(progress_design , 'slds-is-active');
        
        component.set("v.scenarioStage", 'Collaboration');
        // enable the configuration checkboxes
        //component.set("v.disabled", false);
    },

    hideShowErrorPopup: function(component)
    {
        var activeClass = component.get("v.popOverClass");
        console.log(activeClass);
        if(activeClass == 'slds-rise-from-ground')
            component.set("v.popOverClass", 'slds-fall-into-ground');
        else
            component.set("v.popOverClass", 'slds-rise-from-ground');

        component.set("v.showConfirmBtn",false);
    },

    buttonMouseOver : function(component,event,helper)
    {
        var buttonId = event.target.id;
        if(buttonId)
        {
            jQuery('#'+buttonId+'Popover').addClass('slds-show').removeClass('slds-hide');
        }
    },

    buttonMouseOut : function(component,event,helper)
    {
        var buttonId = event.target.id;
        if(buttonId)
        {
            jQuery('#'+buttonId+'Popover').removeClass('slds-show').addClass('slds-hide');
        }
    },

    toggleConfirgurationSection : function(component,event,helper)
    {   
        var $j = jQuery.noConflict();
        $j(event.target).closest('.slds-section').toggleClass('slds-is-open');
        if($j(event.target).closest('.slds-section').hasClass('slds-is-open'))
        {
            $j(event.target).closest('.slds-section').find('.slds-section__content').show();
        }
        else
        {
            $j(event.target).closest('.slds-section').find('.slds-section__content').hide();
        }    
    },

    addNewRow : function(component,event,helper)
    {
        var dataMappingList = component.get("v.dataMappingList");
        var dataMap = {};
        dataMap.pacpField = '--None--';
        dataMap.systemField = '--None--';
        dataMap.isRequired = false;
        dataMappingList.push(dataMap);
        component.set("v.dataMappingList",dataMappingList);
        console.log('add--->');
        console.log(component.get("v.dataMappingList"));
    },

    checkConfigurationList : function(component,event,helper)
    {
        // check for changes in configuration list and cr tracking
        console.log(component.get("v.message"));
        var oldValueRecord = component.get("v.callPlanWrapper");
        var newValueRecord = oldValueRecord.scenarioRecord;
        var changeDone = 'false';
        var isError = 'false';
        var namespace = component.get("v.namespace");
        var newStage = component.get("v.scenarioStage");
        var error = '';
        // var changes = '';
        var changes = component.get("v.message");
        var allChecklistChecked = 'false';
        if(oldValueRecord.crTrackingCheckbox != newValueRecord[namespace+'CR_Tracking__c'] )
        {
            if(isError != 'true')
            {
                if(namespace != '')
                    changes += '<p>'+$A.get("$Label.AxtriaSalesIQTM.CR_Traking_Confirm")+'</p>';
                else
                    changes += '<p>'+$A.get("$Label.c.CR_Traking_Confirm")+'</p>';

                changeDone = 'true';
                isError = 'false';
            }
            else
            {
                changeDone = 'true';
                isError = 'true';
                component.set("v.isError", true);
                error += '<p>There are some errors in the saved values. Please fix errors and try for CR Tracking change</p>';
                component.set("v.popOverClass", 'slds-rise-from-ground');
            }
        }

        if(oldValueRecord.teamInstanceCheckbox != newValueRecord[namespace+'Team_Instance_Config__c'] || 
            oldValueRecord.tableColumnCheckbox != newValueRecord[namespace+'Table_Column_Config__c'] ||
            oldValueRecord.summaryCardsCheckbox != newValueRecord[namespace+'Summary_Cards_Config__c'] || 
            oldValueRecord.tableViewCheckbox != newValueRecord[namespace+'Table_View_Config__c']|| 
            oldValueRecord.kpiCheckbox != newValueRecord[namespace+'KPI_Card_Guardrail_Config__c'] || 
            oldValueRecord.approvalConfigCheckbox != newValueRecord[namespace+'Approval_Config__c'])
        {

            if(newStage != 'Published')
            {
                console.log('configuration checklist update ');
                changeDone = 'true';
                isError = 'false';
                changes += '<p>Configuration Checklist is changed</p>';
            }
        }

        if(newStage == 'Published')
        {
            console.log('aaaaaaaaaaaaa');
            if(newValueRecord[namespace+'Team_Instance_Config__c'] == true && 
                newValueRecord[namespace+'Table_View_Config__c'] == true && 
                newValueRecord[namespace+'Table_Column_Config__c'] == true &&
                newValueRecord[namespace+'Summary_Cards_Config__c'] == true &&
                newValueRecord[namespace+'KPI_Card_Guardrail_Config__c'] == true && 
                newValueRecord[namespace+'Approval_Config__c'] == true)
            {

                console.log('passed from checklist ');
                allChecklistChecked = 'true';
                changeDone = 'true';
                isError = 'false';
                component.set("v.isError", false);
                error = '';
                if(namespace != '')
                    changes += '<p>'+$A.get("$Label.AxtriaSalesIQTM.Stage_Changed_Confirmation")+'</p>';
                else
                    changes += '<p>'+$A.get("$Label.c.Stage_Changed_Confirmation")+'</p>';
            }
            else
            {
                changeDone = 'true';
                isError = 'true';
                component.set("v.isError", true);
                error += '<p>Please select all checkbox in configuration list, then try for stage change.</p>';
                component.set("v.popOverClass", 'slds-rise-from-ground');
            }
        }

        if(changeDone == 'true' && isError == 'false')
        {
            component.set("v.isError", false);
            component.set("v.showConfirmBtn", true);
            component.set("v.disabled" , true);
            component.set("v.disableChecklist", true);
        }
        else
        {
            if(isError == 'true')
            {
                component.set("v.isError", true);
                component.set("v.showConfirmBtn", false);
                component.set("v.popOverClass", 'slds-rise-from-ground');
            }
            else
            {
                component.set("v.isError", true);
                error += '<p>No changes done</p>'
                component.set("v.showConfirmBtn", false);
                component.set("v.popOverClass", 'slds-rise-from-ground');
            }
            
        }
        console.log('changes ::: '+changes);
        component.set("v.message" , changes);
        component.set("v.error", error);
    }
})
