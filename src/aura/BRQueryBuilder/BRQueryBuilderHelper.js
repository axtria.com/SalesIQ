({
    fillColumnValues : function(component, event, helper){
    	var opts = [];
        var arrOperators = [];
    	var action = component.get("c.createColumnWrapperData");
        console.log('fileType ::: '+component.get("v.fileType"));
        action.setParams({
            "scenarioRuleInstanceId":component.get("v.scenarioRuleInstanceId"),
            "fileType":component.get("v.fileType")
        }); 
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(component.isValid() && state==="SUCCESS")
            {    
                component.set("v.columnNames", response.getReturnValue());
                console.log(response.getReturnValue());
                var actiondataOptns = component.get("c.getOperatorValues");
                actiondataOptns.setCallback(this, function(data){
                    var state = data.getState();            
                    if(component.isValid() && state==="SUCCESS")
                    {
                        var response= data.getReturnValue();
                        
                        response.forEach(function(element){
                            var operatorsList = element.lstOfOperators ;
                            operatorsList.forEach(function(operatorW){
                                var val = operatorW.operatorTranslated;
                                if(val.toLowerCase().indexOf('label.')>=0){
                                    var label ;
                                    if(component.get("v.namespace") != '')
                                        label= "$Label.AxtriaSalesIQTM." + val.split('.')[1];
                                    else
                                        label = "$Label.c." + val.split('.')[1];
                                    // $Label.c.Equal_Operator
                                    // $Label.c.Not_Equal_Operator
                                    // $Label.c.In_Operator
                                    // $Label.c.Not_In_Operator
                                    // $Label.c.Is_Null_Operator
                                    // $Label.c.Is_Not_Null_Operator
                                    // $Label.c.Contains_Operator
                                    // $Label.c.Does_Not_Contains_Operator
                                    // $Label.c.Begins_With_Operator
                                    // $Label.c.Ends_With_Operator
                                    // $Label.c.Between_Operator
                                    // $Label.c.Greater_Or_Equal_Operator
                                    // $Label.c.Greater_Than_Operator
                                    // $Label.c.Less_Or_Equal_Operator
                                    // $Label.c.Less_Than_Operator

                                    component.set('v.labelAttr',$A.getReference(label));
                                    if(component.get('v.labelAttr') == ""){
                                        $A.get('e.force:refreshView').fire();
                                    }
                                    val = component.get('v.labelAttr');
                                }
                                operatorW.operatorTranslated = val;
                            });
                        });

                        component.set("v.listOfOperators",response);
                        console.log(response);
                        helper.intialize(component, event, helper);
                    } else {
                        console.log("Operators load failed "+ data.getErrors()[0]);
                    }
                                
                });
                $A.enqueueAction(actiondataOptns);
            }
            else{
                console.log("Columns load failed "+ response.getErrors()[0]);
            }
        });
        $A.enqueueAction(action);
    },

    intialize : function(component, event, helper){

        var action = component.get("c.initializeBlock");
        action.setParams({
            queryJSON :component.get("v.parsedJSONQuery")
        });
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null){
                var result = response.getReturnValue();
                console.log('--result ', result);
                component.set("v.filterWrapper", result);

                var qbDiv = component.find('queryBuilderContainerDiv');
                $A.util.removeClass(qbDiv ,"slds-hide");
                var childBuilder = component.find('childQueryBuilder');
                childBuilder.initChild();
                // var initChildEvt = $A.get("e.c:initPicklistQueryBuilderEvent");
                // initChildEvt.fire();
               // $A.util.addClass(qbDiv , 'slds-show');

            }    
        });
        $A.enqueueAction(action);
    },

    doFilterValidation:function(component, event, helper){
        
        var namespace = component.get("v.namespace");
        var filterWrapper = component.get("v.filterWrapper");
        
        var errorList = [];
        helper.doIterativeValidation(helper, filterWrapper, namespace, errorList);

        console.log(errorList);

        // code to remove duplicate error message
        var obj = {};
        var ret_arr = [];
        for (var i = 0; i < errorList.length; i++) {
            obj[errorList[i]] = true;
        }
        for (var key in obj) {
            ret_arr.push(key);
        }
        // ------------- duplicate removal code ends------ //

        errorList = ret_arr ;

        if(errorList.length > 0){
            component.set('v.isValid','false');
            component.set('v.errorMsg',errorList[0]);
        }
        else {
            component.set('v.isValid','true');
            component.set('v.errorMsg','');
        }
    },

    doIterativeValidation : function(helper, filterWrapper, namespace, errorList){
        //COMPONENT ERROR FIX
        //var cri = filterWrapper.criterias; $A.util.isUndefined(rulename)
        var errMessage = '';
        if( filterWrapper == null || $A.util.isUndefined(filterWrapper)){
            return;
        }
        var cri = filterWrapper['criterias']; 

        if(cri == null || $A.util.isUndefined(cri) || cri.length == 0){
            if(cri.length == 0){
                 if(namespace!=''){
                        errMessage = $A.get("$Label.AxtriaSalesIQTM.Field_Value_Blank") ;
                    }else{
                        errMessage=$A.get("$Label.c.Field_Value_Blank") ;
                    }
                    errorList.push(errMessage);
            }else{
                return;
            }
        }
  
        //var cri = filterWrapper.criterias; 
        console.log(cri);
        for(var i=0;i<cri.length;i++){
            if(!cri[i].isGroup) {
                if(cri[i].field==''){
                    if(namespace!=''){
                        errMessage = $A.get("$Label.AxtriaSalesIQTM.Field_Value_Blank") ;
                    }else{
                        errMessage=$A.get("$Label.c.Field_Value_Blank") ;
                    }
                    errorList.push(errMessage);
                    break;
                }

                if(cri[i].operator == '') {
                    if(namespace!=''){
                        errMessage = $A.get("$Label.AxtriaSalesIQTM.Operator_Value_Blank") ;
                    }else{
                        errMessage=$A.get("$Label.c.Operator_Value_Blank") ;
                    }
                    errorList.push(errMessage);
                    break;
                }

                if(cri[i].dataType == 'text' && (cri[i].operator!='is_not_null' && cri[i].operator!='is_null')){
                    if(cri[i].value.trim()==''){
                            if(namespace!=''){
                                errMessage = $A.get("$Label.AxtriaSalesIQTM.Field_Value_Blank") ;
                            }else{
                                errMessage=$A.get("$Label.c.Field_Value_Blank") ;
                            }
                            errorList.push(errMessage);
                            break;
                        }

                    // if(cri[i].operator=='in' || cri[i].operator=='not_in'){
                    //     var checkPattern = /^[A-Za-z\d,\s]+$/;
                    //     var inPattern=checkPattern.test(cri[i].value);
                    //     if(!inPattern){
                            
                    //         if(namespace!=''){
                    //             errMessage = $A.get("$Label.AxtriaSalesIQTM.Only_Alphanumeric_Data") ;
                    //         }else{
                    //             errMessage=$A.get("$Label.c.Only_Alphanumeric_Data") ;
                    //         } 
                    //         errorList.push(errMessage);
                    //         break;  
                    //     }
                    // }else{
                    //     var textPattern = /^([a-zA-Z0-9 _]+)$/;
                    //     var pattern=textPattern.test(cri[i].value);
                    //     if(!pattern){
                            
                    //         if(namespace!=''){
                    //             errMessage = $A.get("$Label.AxtriaSalesIQTM.Only_Alphanumeric_Data") ;
                    //         }else{
                    //             errMessage=$A.get("$Label.c.Only_Alphanumeric_Data") ;
                    //         }
                    //         errorList.push(errMessage);
                    //         break;   
                    //     }
                    // }
                }

                if(cri[i].dataType == 'numeric' && (cri[i].operator!='is_not_null' && cri[i].operator!='is_null')){
                    var textPattern = /^-?\d+\.?\d*$/;
                    if(cri[i].operator=='between'){
                        if(cri[i].anotherValue.trim()==''){
                            if(namespace!=''){
                                errMessage = $A.get("$Label.AxtriaSalesIQTM.Field_Value_Blank") ;
                            }else{
                                errMessage=$A.get("$Label.c.Field_Value_Blank") ;
                            }
                            errorList.push(errMessage);
                            break;
                        }

                        var anotherpattern=textPattern.test(cri[i].anotherValue);
                        if(!anotherpattern){
                            if(namespace!=''){
                                errMessage = $A.get("$Label.AxtriaSalesIQTM.Only_Numeric_Data") ;
                            }else{
                                errMessage=$A.get("$Label.c.Only_Numeric_Data") ;
                            } 
                            errorList.push(errMessage);
                            break;  
                        }
                    }

                    if(cri[i].value.trim()==''){
                        if(namespace!=''){
                            errMessage = $A.get("$Label.AxtriaSalesIQTM.Field_Value_Blank") ;
                        }else{
                            errMessage=$A.get("$Label.c.Field_Value_Blank") ;
                        }
                        errorList.push(errMessage);
                        break;
                    }
                    
                    if(cri[i].operator=='in' || cri[i].operator=='not_in'){
                        //var checkPattern = /^[\d,\s]+$/;
                        var checkPattern = /^(\d+(?:,\d+){0,9})?$/;
                        var inPattern=checkPattern.test(cri[i].value);
                        // if(!inPattern){
                            
                        //     if(namespace!=''){
                        //         errMessage = $A.get("$Label.AxtriaSalesIQTM.Only_Alphanumeric_Data") ;
                        //     }else{
                        //         errMessage=$A.get("$Label.c.Only_Alphanumeric_Data") ;
                        //     } 
                        //     errorList.push(errMessage);
                        //     break;  
                        // }
                    }
                    else{
                        var pattern=textPattern.test(cri[i].value);
                        if(!pattern){
                            if(namespace!=''){
                                errMessage = $A.get("$Label.AxtriaSalesIQTM.Only_Numeric_Data") ;
                            }else{
                                errMessage=$A.get("$Label.c.Only_Numeric_Data") ;
                            }
                            errorList.push(errMessage);
                            break;   
                        }
                    }
                    
                }
            } else {
                helper.doIterativeValidation(helper, cri[i].innerConditionWrapper, namespace, errorList);
            }
        }
        
    }
})