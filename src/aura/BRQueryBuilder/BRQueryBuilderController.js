({
    doInit : function(component,event,helper){
        console.log('--- Inside doInit --- ')
        if(component.get("v.moduleName") == '')
            component.fillColumnValues(component,event,helper);
        else{
            console.log('--- render querybuilder component --- ')

            console.log('-- operators :',component.get("v.listOfOperators"));

            /*var operatorAction = component.get("c.getOperatorValuesPosition");
            operatorAction.setCallback(this,function(response){
                console.log('response -- ',response.getReturnValue());
                if(response.getState() == 'SUCCESS' && response.getReturnValue() != null){
                    var value = response.getReturnValue();
                    console.log('value : ',value[0].lstOfOperators);
                    value = value[0].lstOfOperators;
                    component.set("v.listOfOperators",value);
                    component.set("v.showFilterPopup",true);
                    console.log('---- operators :',component.get("v.listOfOperators"));
                }
                
            });
            $A.enqueueAction(operatorAction);

            console.log('---- render columns name ---- ');
            var columnAction = component.get("c.createColumnWrapperData");
            columnAction.setCallback(this,function(response){
                if(response.getState() == 'SUCCESS' && response.getReturnValue() != null){
                    component.set("v.columnNames", response.getReturnValue());
                    console.log('---columns : ',component.get("v.columnNames"));

                }
            });
            $A.enqueueAction(columnAction);
*/
            helper.intialize(component, event, helper);
        }
    },

    viewQuery : function(component, event, helper){
        helper.doFilterValidation(component, event, helper);
        console.log(JSON.parse(JSON.stringify(component.get("v.filterWrapper"))));
        console.log('--viewQuery called');
        component.set("v.parsedJSONQuery",JSON.stringify(component.get("v.filterWrapper")));
        var action = component.get('c.generateQuery');

        var brChild = component.find("childQueryBuilder");
        console.log(brChild.get("v.columnNames"));
        console.log(brChild.get("v.listOfOperators"));
        action.setParams({
            filterWrapper : JSON.stringify(component.get("v.filterWrapper")),
            columnNames : JSON.stringify(brChild.get("v.columnNames")),
            listOfOperators : JSON.stringify(brChild.get("v.listOfOperators"))
        });
        action.setCallback(this, function(data){
            var response= data.getReturnValue();
            component.set("v.parsedQuery", response[0]);
            component.set("v.stagingQuery",response[1]);
        });
        $A.enqueueAction(action);  
    },

    getQuery : function(component, event, helper){
        helper.doFilterValidation(component, event, helper);
        console.log(JSON.parse(JSON.stringify(component.get("v.filterWrapper"))));
        console.log('--getQuery called');
        component.set("v.parsedJSONQuery",JSON.stringify(component.get("v.filterWrapper")));
        var action = component.get('c.generateQuery');

        var brChild = component.find("childQueryBuilder");
        console.log(brChild.get("v.columnNames"));
        console.log(brChild.get("v.listOfOperators"));
        try{
            brChild.checkBlankValues();
            console.log('check blanks');
        }catch(e){
            console.log(e);
        }
        action.setParams({
            filterWrapper : JSON.stringify(component.get("v.filterWrapper")),
            columnNames : JSON.stringify(brChild.get("v.columnNames")),
            listOfOperators : JSON.stringify(brChild.get("v.listOfOperators"))
        });
        action.setCallback(this, function(data){
            var response= data.getReturnValue();
            console.log('--response ', response);
            //var parseQuery_ = '',stagingQuery_ = '',sourceQuery_ = ''; 
            //COMPONENT ERROR FIX 1927
            //revert
            //if( response != null ){
            //parseQuery_ = response[0];
            //stagingQuery_ = response[1];
            //sourceQuery_ = response[2];
            //}
            
            component.set( "v.parsedQuery", response[0] );
            component.set( "v.stagingQuery", response[1] );
            component.set( "v.sourceQuery", response[2] );

            var cmpEvent = component.getEvent("updateWrapper");
            cmpEvent.setParams({
            "parsedJson" : component.get("v.parsedJSONQuery"),
            "isValid" : component.get("v.isValid"),
            "errorMsg" : component.get("v.errorMsg"),
            "whereClause" : component.get("v.parsedQuery"),
            "stagingWhereClause" : component.get("v.stagingQuery"),
            "sourceWhereClause" : component.get("v.sourceQuery")
             });

            cmpEvent.fire();
            
        });
        $A.enqueueAction(action);  
    },

    fillColumnValues : function(component, event, helper){
        var opts = [];
        var arrOperators = [];
        var action = component.get("c.createColumnWrapperData");
        console.log('fileType ::: '+component.get("v.fileType"));
        action.setParams({
            "scenarioRuleInstanceId":component.get("v.scenarioRuleInstanceId"),
            "fileType":component.get("v.fileType")
        }); 
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(component.isValid() && state==="SUCCESS")
            {    
                component.set("v.columnNames", response.getReturnValue());
                console.log(response.getReturnValue());
                var actiondataOptns = component.get("c.getOperatorValues");
                actiondataOptns.setCallback(this, function(data){
                    var state = data.getState();            
                    if(component.isValid() && state==="SUCCESS")
                    {
                        var response1= data.getReturnValue();
                        
                        response1.forEach(function(element){
                            var operatorsList = element.lstOfOperators ;
                            operatorsList.forEach(function(operatorW){
                                var val = operatorW.operatorTranslated;
                                if(val.toLowerCase().indexOf('label.')>=0){
                                    var label ;
                                    if(component.get("v.namespace") != '')
                                        label= "$Label.AxtriaSalesIQTM." + val.split('.')[1];
                                    else
                                        label = "$Label.c." + val.split('.')[1];
                                    // $Label.c.Equal_Operator
                                    // $Label.c.Not_Equal_Operator
                                    // $Label.c.In_Operator
                                    // $Label.c.Not_In_Operator
                                    // $Label.c.Is_Null_Operator
                                    // $Label.c.Is_Not_Null_Operator
                                    // $Label.c.Contains_Operator
                                    // $Label.c.Does_Not_Contains_Operator
                                    // $Label.c.Begins_With_Operator
                                    // $Label.c.Ends_With_Operator
                                    // $Label.c.Between_Operator
                                    // $Label.c.Greater_Or_Equal_Operator
                                    // $Label.c.Greater_Than_Operator
                                    // $Label.c.Less_Or_Equal_Operator
                                    // $Label.c.Less_Than_Operator

                                    component.set('v.labelAttr',$A.getReference(label));
                                    if(component.get('v.labelAttr') == ""){
                                        $A.get('e.force:refreshView').fire();
                                    }
                                    val = component.get('v.labelAttr');
                                }
                                operatorW.operatorTranslated = val;
                            });
                        });

                        component.set("v.listOfOperators",response1);
                        console.log(response1);
                        component.intialize(component, event, helper);
                    } else {
                        console.log("Operators load failed "+ data.getErrors()[0]);
                    }
                                
                });
                $A.enqueueAction(actiondataOptns);
            }
            else{
                // console.log("Columns load failed "+ response.getErrors()[0]);
            }
        });
        $A.enqueueAction(action);
    },

    intialize : function(component, event, helper){

        console.log('--- init brquerybuilder ----- ');

        var action = component.get("c.initializeBlock");
        action.setParams({
            queryJSON :component.get("v.parsedJSONQuery")
        });
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null){
                var result = response.getReturnValue();
                console.log('--result ', result);
                // alert('hi');
                component.set("v.filterWrapper", result);

                //INIT MULTIPICKLIST VALUES

                // for(var i=0;i<result['criterias'].length;i++){
                //     console.log('multipicklist check');
                //     console.log(result['criterias'][i]);
                //     var colValueType = (result['criterias'][i]['colValueType']).toLowerCase();
                //     if(colValueType == 'discrete'){
                //         //REINIT VIA SELECT CHANGE EVENT EVENT
                //         console.log('reintializing picklist');
                //         $A.get("e.c:initializeMultiPicklist").fire();
                //     }
                // }
                // alert('hi2');
                
                // console.log('event fired--->>>');

                var qbDiv = document.getElementsByClassName("queryBuilderContainerDiv");
                for(var i=0;i<qbDiv.length;i++)
                {
                    if(qbDiv[i].classList.contains('slds-hide'))
                    {
                        qbDiv[i].classList.remove('slds-hide');
                    }
                }
                // $A.util.removeClass(qbDiv ,"slds-hide");
                var childBuilder = component.find('childQueryBuilder');
                childBuilder.initChild();
                // var initChildEvt = $A.get("e.c:initPicklistQueryBuilderEvent");
                // initChildEvt.fire();
               // $A.util.addClass(qbDiv , 'slds-show');

            }    
        });
        $A.enqueueAction(action);
    },
})