({
    doInit : function(component,event,helper)
    {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:SalesIQEmployeeUniverse",
            componentAttributes: {
                objectName: "Employee__c",
                loadRecent : "false"
            },
            isredirect : true

        });
        $A.get("e.force:closeQuickAction").fire();
        evt.fire();

    }
})