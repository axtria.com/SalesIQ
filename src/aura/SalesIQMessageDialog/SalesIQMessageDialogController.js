({
    defaultCloseAction : function(component, event, helper) 
    {  
        if(component.get("v.moduleName") == 'Call Plan')
        {
            if(event.target && !(event.target.classList.contains('confirmButton')))
            {
                var evt = component.getEvent("refreshDataTable");
                evt.setParams({
                    calledFromEvent : true
                });           
                evt.fire();
                var evt = component.getEvent("refreshKpiEvent");           
                evt.fire();
                
                var message;
                if(component.get("v.namespace") != '')
                    message = $A.get("$Label.AxtriaSalesIQTM.CALL_PLAN_SAVE_MESSAGE");
                else
                    message = $A.get("$Label.c.CALL_PLAN_SAVE_MESSAGE");

                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    mode: 'dismissible',
                    type: 'Success',
                    message: message,
                    messageTemplate: message,
                    messageTemplateData: []
                });
                toastEvent.fire();
            }
            
        }
        component.destroy();
    },
})