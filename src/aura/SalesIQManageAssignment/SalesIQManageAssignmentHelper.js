({
    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    },

    toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },

    getLocaleDateFormats: function(component,event) {
        var action = component.get("c.getLocaleDateFormats");
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue() != null)
            {
                var formatList = response.getReturnValue();
                component.set("v.dateFormat", formatList[0]); 
                component.set("v.dateTimeFormat", formatList[1]); 
            }
        });
        $A.enqueueAction(action); 
    },

    redirectToUniverse: function(component) {
        if(component.get("v.moduleName") == 'Employee Universe')
        {
           /* var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:SalesIQEmployeeUniverse",
                componentAttributes: {
                    objectName: "Employee__c",
                    loadRecent : "false",
                    selectedListView : component.get("v.selectedListView")
                },
                isredirect : true
            });
            evt.fire();
            document.getElementsByTagName("body")[0].style.overflow = "visible";*/
            
            if(component.get("v.externalInvokeWorkbench") == 'false'){
                var evt = $A.get("e.force:navigateToComponent");
                evt.setParams({
                    componentDef : "c:SalesIQEmployeeUniverse",
                    componentAttributes: {
                        objectName: "Employee__c",
                        loadRecent : "false",
                        selectedListView : component.get("v.selectedListView")
                    },
                    isredirect : true
                });
                evt.fire();
                document.getElementsByTagName("body")[0].style.overflow = "visible";
            }else if(component.get("v.externalInvokeWorkbench") == 'true'){
                // redirect to workbench 
                var action = component.get("c.getExternalComponentDetail");
                action.setCallback(this,function(response)
                {
                    var state = response.getState();
                    if(state === 'SUCCESS' && response.getReturnValue() != '')
                    {
                        console.log(response.getReturnValue());
                        var evt = $A.get("e.force:navigateToComponent");
                        evt.setParams({
                            componentDef : response.getReturnValue().split(';')[0],
                            componentAttributes: {
                                objectName: response.getReturnValue().split(';')[1],
                                recordId : component.get("v.recordId"),
                                isSuccess : component.get("v.isSuccess"),
                                feeId : component.get("v.workbenchId"),
                                crEmployeeFeedId : component.get("v.workbenchId"),
                                crID : component.get("v.crID"),
                                CardClicked : component.get("v.CardClicked"),
                                CurrentCard : component.get("v.CurrentCard")
                            },
                            isredirect : true
                        });
                        evt.fire();
                         document.getElementsByTagName("body")[0].style.overflow = "visible";
                    }
                });
                $A.enqueueAction(action); 
            }
        }
        else if(component.get("v.moduleName") == 'Position Universe')
        {
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:SalesIQPositionUniverse",
                componentAttributes: {
                    objectName: "Position__c",
                    loadRecent : "false",
                    selectedTeamInstance : component.get("v.selectedTeamInstance"),
                    selectedListView : component.get("v.selectedListView")
                },
                isredirect : true
            });
            evt.fire();
            document.getElementsByTagName("body")[0].style.overflow = "visible";
        }
    },

    getAssignments : function(component,event)
    {   
        if(!document.getElementById('timeline').classList.contains("slds-hide"))
            component.showSpin();   
        var ns = component.get("v.namespace");
        var recordId = component.get("v.recordId");

        var action = component.get("c.getAssignments");

        var assignmentList = [];
        var positionList = [];
        var primaryList = [];


        action.setParams({
            recordId : recordId,
            moduleName : component.get("v.moduleName")
        });
       
        action.setCallback(this,function(response)
        {
            var state = response.getState();
           
            if(state === 'SUCCESS' && response.getReturnValue()[0] != null)
            {
                
                var no_assignment_text = component.find("no_assignment_text");
                $A.util.addClass(no_assignment_text,"slds-hide");
                var retResp = response.getReturnValue();

                component.set("v.ObjectDetailWrapperList",retResp);
                var assigneeName = component.get("v.assigneeName");

                var cardheader = {"label" : assigneeName , "icon" : "custom:custom15"};
                component.set("v.cardHeader",cardheader);

                if(document.getElementById('timeline').classList.contains("slds-hide"))
                {
                    /*var dataTable = component.find('DataTable');
                    dataTable.initializeDataTable();*/
                    component.set("v.displayHistoryGrid", true);
                }
                if(!document.getElementById('timeline').classList.contains("slds-hide"))
                {
                    this.createTimelineData(component,event);   
                    component.hideSpin();
                }

            }

            else if (state === "ERROR" || response.getReturnValue == null) 
            {
                component.hideSpin();
                var namespace = component.get("v.namespace");
                var errorLabel;
                if(namespace != '')
                {
                    errorLabel = $A.get("$Label.AxtriaSalesIQTM.Unexpected_error_notification");
                }
                else
                {
                    errorLabel = $A.get("$Label.c.Unexpected_error_notification");
                }
                var errors = response.getError();
                var errorMessage = errors[0].message;
                if (errors[0] && errors[0].message) 
                {
                    var action = component.get("c.logException");
        
                    action.setParams({
                        message : errorMessage,
                        module : component.get("v.moduleName")
                    });

                    action.setCallback(this,function(response)
                    {
                        var state = response.getState();
                        if(state === 'SUCCESS')
                        {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                mode: 'dismissible',
                                message: errorLabel,
                                type : 'error'
                                
                            });
                            toastEvent.fire();
                        }
                    });
                    $A.enqueueAction(action);
                } 
                else 
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: errorLabel,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                }
            } 
            else if(response.getReturnValue.length == 0)
            {
                if(document.getElementById('timeline').classList.contains("slds-hide"))
                {
                    component.set("v.displayHistoryGrid", true);
                }
                if(!document.getElementById('timeline').classList.contains("slds-hide"))
                {
                    var no_assignment_text = component.find("no_assignment_text");
                    $A.util.removeClass(no_assignment_text,"slds-hide");
                    var startD = new Date(component.get("v.startDate"));
                    var endD = new Date(component.get("v.endDate"));
                    assignmentList.push({position : 'Unassigned' , startDate : startD.getTime(), endDate : endD.getTime() , assignmentType : 'Unassigned' , status : 'Unassigned'});                
                    component.set("v.assignmentList",assignmentList);
                    var assigneeName = component.get("v.assigneeName");
                    var cardheader = {"label" : assigneeName , "icon" : "custom:custom15"};
                    component.set("v.cardHeader",cardheader);
                    this.drawTimeline(component,event,assignmentList,[],[]);
                    component.hideSpin();
                }
            }
            
            // enable the reset button back
            var resetButton = event.target;
            if(resetButton != undefined)
                resetButton.disabled = false;
        });

        $A.enqueueAction(action);  

    },
    
    getPendingChangeRequests : function(component,event)
    {

        var recordId = component.get("v.recordId");    
        var crAction = component.get("c.getPendingChangeRequests");
        var ns = component.get("v.namespace");
        crAction.setParams({
            recordId : recordId,
            moduleName : component.get("v.moduleName")
        });

        crAction.setCallback(this,function(response)
        {
            var state = response.getState();
           
            if(state === 'SUCCESS')
            {
                var $j = jQuery.noConflict();
                var retResp = response.getReturnValue();
                if(retResp.length > 0)
                {
                    var submitButton = $j("#submitButton").attr('disabled','disabled');
                    var resetButton = $j("#resetButton").attr('disabled','disabled');
                    var addButton = $j("#assignButton").attr('disabled','disabled');
                    component.set("v.errorList",retResp);
                    var pending_CR_popover = component.find('pending_CR_popover');
                    $A.util.removeClass(pending_CR_popover,'slds-hide');
                    var pending_CR_icon = component.find('pending_CR_icon');
                    $A.util.removeClass(pending_CR_icon,'slds-hide'); 
                    component.set("v.isCRPending",true);
                }
                else
                {
                    var submitButton = $j("#submitButton").removeAttr('disabled');
                    var resetButton = $j("#resetButton").removeAttr('disabled');
                    var addButton = $j("#assignButton").removeAttr('disabled');
                    component.set("v.isCRPending",false);
                }
                

            }

            else if (state === "ERROR" || response.getReturnValue == null) 
            {
                component.hideSpin();
                var namespace = component.get("v.namespace");
                var errorLabel;
                if(namespace != '')
                {
                    errorLabel = $A.get("$Label.AxtriaSalesIQTM.Unexpected_error_notification");
                }
                else
                {
                    errorLabel = $A.get("$Label.c.Unexpected_error_notification");
                }
                var errors = response.getError();
                var errorMessage = errors[0].message;
                if (errors[0] && errors[0].message) 
                {
                    var action = component.get("c.logException");
        
                    action.setParams({
                        message : errorMessage,
                        module : component.get("v.moduleName")
                    });

                    action.setCallback(this,function(response)
                    {
                        var state = response.getState();
                        if(state === 'SUCCESS')
                        {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                mode: 'dismissible',
                                message: errorLabel,
                                type : 'error'
                                
                            });
                            toastEvent.fire();
                        }
                    });
                    $A.enqueueAction(action);
                } 
                else 
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: errorLabel,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                }
            } 
            
        });

        $A.enqueueAction(crAction);
    },

    createTimelineData : function(component,event)
    {
        var assignmentList = [];
        var positionList = [];
        var primaryList = [];
        var ns = component.get("v.namespace");
         var startD = new Date(component.get("v.startDate"));
        var endD = new Date(component.get("v.endDate"));
        
        var ObjectDetailWrapperList = component.get("v.ObjectDetailWrapperList");
        console.log('ObjectDetailWrapperList line 353: ',ObjectDetailWrapperList);

        //to get namespace(getting from parent)
        // var regex = /__/g ;
        // var objStr = JSON.stringify(ObjectDetailWrapperList[0].record);
        // var tempList = objStr.split(',');
        // for(var i = 0;i<tempList.length;i++)
        // {
        //     var apiname = tempList[i].split(':')[0];
        //     if(apiname.match(regex)!=null)
        //     {
        //         if(apiname.match(regex).length == 1)
        //         {
        //             ns = '';
        //             break;
        //         }
        //         else
        //         {
        //             ns = apiname.substring(apiname.indexOf('"')+1,apiname.indexOf('__')+2);
        //             break;
        //         }
        //     }
        // }        
        console.log('ns::' + ns);
        // component.set("v.namespace",ns);

        var j = 1;
        for(var i = 0;i<ObjectDetailWrapperList.length;i++)
        {
            if(ObjectDetailWrapperList[i].isNew == true)
            {
                console.log('details : ',ObjectDetailWrapperList[i]);
                console.log(ObjectDetailWrapperList[i].record[ns + 'Effective_Start_Date__c']);
                console.log(ObjectDetailWrapperList[i].record[ns + 'Effective_End_Date__c']);
                console.log(new Date(ObjectDetailWrapperList[i].record[ns + 'Effective_Start_Date__c']).getTime());

                var stDate,endDate;
                if(ObjectDetailWrapperList[i].record[ns + 'Effective_Start_Date__c'].indexOf('-') != -1){
                    var arr1 = ObjectDetailWrapperList[i].record[ns + 'Effective_Start_Date__c'].split('-');
                    stDate = new Date(arr1[0],arr1[1] - 1, arr1[2]);
                    
                }else{
                    stDate = new Date(ObjectDetailWrapperList[i].record[ns + 'Effective_Start_Date__c']);
                }

                console.log('stDate :'+stDate.getTime());

                if(ObjectDetailWrapperList[i].record[ns + 'Effective_End_Date__c'].indexOf('-') != -1){
                    var arr2 = ObjectDetailWrapperList[i].record[ns + 'Effective_End_Date__c'].split('-');
                    endDate = new Date(arr2[0],arr2[1] - 1, arr2[2]);
                }else{
                    endDate = new Date(ObjectDetailWrapperList[i].record[ns + 'Effective_End_Date__c']);
                }

                var offsetInMinutesendDate = endDate.getTimezoneOffset() ;
                stDate.setSeconds(stDate.getSeconds() + (stDate.getTimezoneOffset()*60));
                endDate.setSeconds(endDate.getSeconds() + (endDate.getTimezoneOffset()*60));

                console.log('endDate :'+endDate.getTime());

                var assignObject = {position : ObjectDetailWrapperList[i].record[ns + 'Position__r']['Name'] ,clientPositionCode : ObjectDetailWrapperList[i].record[ns + 'Position__r'][ns + 'Client_Position_Code__c'], startDate : stDate.getTime() , endDate : endDate.getTime() , assignmentType : ObjectDetailWrapperList[i].record[ns + 'Assignment_Type__c'] , status : 'Assigned' , positionId : ObjectDetailWrapperList[i].record[ns +'Position__c'] + '_' + j , assignmentId : j,employee : ObjectDetailWrapperList[i].record[ns + 'Employee__r']['Name']};
                j= j+1;
            }

            else
            {
                console.log('details : ',ObjectDetailWrapperList[i]);
                console.log(ObjectDetailWrapperList[i].record[ns + 'Effective_Start_Date__c']);
                console.log(ObjectDetailWrapperList[i].record[ns + 'Effective_End_Date__c']);

                console.log(new Date(ObjectDetailWrapperList[i].record[ns + 'Effective_Start_Date__c']).getTime());

                var assignObject = {position : ObjectDetailWrapperList[i].record[ns + 'Position__r']['Name'] ,clientPositionCode : ObjectDetailWrapperList[i].record[ns + 'Position__r'][ns + 'Client_Position_Code__c'] ,startDate : new Date(ObjectDetailWrapperList[i].record[ns + 'Effective_Start_Date__c']).getTime() , endDate : new Date(ObjectDetailWrapperList[i].record[ns + 'Effective_End_Date__c']).getTime() , assignmentType : ObjectDetailWrapperList[i].record[ns + 'Assignment_Type__c'] , status : 'Assigned' ,positionId : ObjectDetailWrapperList[i].record[ns + 'Position__c'] + '_0',assignmentId : ObjectDetailWrapperList[i].record['Id'],employee : ObjectDetailWrapperList[i].record[ns + 'Employee__r']['Name']};
            }


            if(ObjectDetailWrapperList[i].record[ns + 'Assignment_Type__c'] != undefined && ObjectDetailWrapperList[i].record[ns + 'Assignment_Type__c'].indexOf('Primary') != -1)
            {
                if(assignObject.endDate > startD.getTime() && assignObject.startDate < endD.getTime() && assignObject.startDate <= assignObject.endDate)
                    primaryList.push(assignObject);
            }
            // if (assignObject.endDate > startD.getTime() && assignObject.startDate < endD.getTime() && assignObject.startDate <= assignObject.endDate) 
            // {
                assignmentList.push(assignObject);
            // }
            
        }

        if(primaryList.length > 1)
        {
            primaryList.sort(function(a, b) 
            {
                if ( a.startDate < b.startDate )
                  return -1;
                if ( a.startDate > b.startDate)
                  return 1;
                return 0;
                // return a.startDate > (b.startDate);
            });
        }

        if(ObjectDetailWrapperList.length > 1)
        {
            ObjectDetailWrapperList.sort(function(a, b) 
            {
                if ( a.record[ns + 'Assignment_Type__c'] < b.record[ns + 'Assignment_Type__c'] )
                  return -1;
                if ( a.record[ns + 'Assignment_Type__c'] > b.record[ns + 'Assignment_Type__c'] )
                  return 1;
                return 0;
                // return a.record[ns + 'Assignment_Type__c'] > (b.record[ns + 'Assignment_Type__c']);
            });
        }
        component.set("v.ObjectDetailWrapperList",ObjectDetailWrapperList);

       
        

        var tmpList1= [];
        if(primaryList.length < 1)
        {
            tmpList1.push({position : "Unassigned" , startDate : startD.getTime() , endDate :  endD.getTime() , assignmentType : "Unassigned" , status : 'Unassigned'});
        }
        else
        {

            if(primaryList[0].startDate > startD.getTime())
            {
                tmpList1.push({position : "Unassigned" , startDate : startD.getTime() , endDate : primaryList[0].startDate - (1000 * 60 * 60 * 24) , assignmentType : "Unassigned" , status : 'Unassigned'});
            }
            var j = 0;
            if(primaryList.length > 1)
            {
                for(var i = 0;i<primaryList.length - 1;i++)
                {
                    if(primaryList[i+1].startDate > primaryList[i].endDate)
                    {
                        if(parseInt((primaryList[i+1].startDate - primaryList[i].endDate) / (1000 * 60 * 60 * 24)) > 1)
                        {
                            tmpList1.push({position : "Unassigned" , startDate : primaryList[i].endDate + (1000 * 60 * 60 * 24) , endDate : primaryList[i+1].startDate - (1000 * 60 * 60 * 24) , assignmentType : "Unassigned" , status : 'Unassigned'});
                        }
                    }
                    if(primaryList[i].endDate > primaryList[i+1].endDate)
                    {
                        j = i;
                    }
                    else
                    {
                        j = i+1;
                    }
                }
            }
            if(primaryList[j].endDate < endD.getTime())
            {
               tmpList1.push({position : "Unassigned" , startDate : primaryList[j].endDate + (1000 * 60 * 60 * 24) , endDate : endD.getTime(), assignmentType : "Unassigned" , status : 'Unassigned'}); 
            }

        }

        if(assignmentList.length > 1)
        {
            assignmentList.sort(function(a, b) 
            {
                if ( a.assignmentType < b.assignmentType )
                  return -1;
                if ( a.assignmentType > b.assignmentType )
                  return 1;
                return 0;
                // console.log(a.assignmentType +'--->'+ b.assignmentType) + ');
                // return a.assignmentType > (b.assignmentType);
            });
        }

        if(!Date.prototype.adjustDate)
        {
            Date.prototype.adjustDate = function(days){
                var date;

                days = days || 0;

                if(days === 0){
                    date = new Date( this.getTime() );
                } else if(days > 0) {
                    date = new Date( this.getTime() );

                    date.setDate(date.getDate() + days);
                } else {
                    date = new Date(
                        this.getFullYear(),
                        this.getMonth(),
                        this.getDate() - Math.abs(days),
                        this.getHours(),
                        this.getMinutes(),
                        this.getSeconds(),
                        this.getMilliseconds()
                    );
                }

                this.setTime(date.getTime());
                return this;
            };
        }

        component.set("v.assignmentList",assignmentList);

        var tempList = []
        var tempList = JSON.parse(JSON.stringify(tmpList1));
        if(component.get("v.moduleName") == 'Position Universe')
        {
            this.drawPositionTimeline(component,event,tempList,assignmentList);
        }
        else if(component.get("v.moduleName") == 'Employee Universe')
        {
            this.drawTimeline(component,event,tempList,assignmentList);

        }
    },


    drawTimeline : function(component,event,tempList,assignmentList)
    {
        var ns = component.get("v.namespace");
        var $j = jQuery.noConflict();
        var margin = {top: 30, right: 20, bottom: 30, left: 20};
        // var width = $j('.slds-modal__container').width() * 0.75 - margin.left - margin.right ;
        // var height =  $j('.slds-modal__content').height()  - margin.top - margin.bottom;

        var width = document.getElementsByClassName('slds-modal__container')[0].clientWidth * 0.75 - margin.left - margin.right ;
        var height =  document.getElementsByClassName('slds-modal__content')[0].clientHeight  - margin.top - margin.bottom - document.getElementById('timelineChangeDiv').clientHeight;
        
        // var modalheight = $j('.slds-modal__container').height();
        // var modalwidth = $j('.slds-modal__container').width();
        // console.log(modalheight + ':::' + modalwidth);

        var assignStatus = {"Assigned" : "bar-assigned","Unassigned" : "bar-unassigned"};

        var assignmentTypes = ["Test"];
        var posNames = [];
        var chart2List = [];
        var posIds = [];
        var assignIds = [];

        console.log('assignmentList :',assignmentList);
        for(var i = 0;i<assignmentList.length;i++)
        {
            if(assignmentList[i].status == 'Assigned')
            {
                posNames.push(assignmentList[i].position);
                
                posIds.push(assignmentList[i].positionId);
                
            }
            chart2List.push(assignmentList[i]);
            assignIds.push(assignmentList[i].assignmentId);
        }
        
        var uniquePositions = posNames.filter(function(item, i, ar){ return ar.indexOf(item) === i; });
        var uniquePositionIds = posIds.filter(function(item, i, ar){ return ar.indexOf(item) === i; });
        var uniqueAssignIds = assignIds.filter(function(item, i, ar){ return ar.indexOf(item) === i; });
        console.log(uniqueAssignIds);

        var format = "%b";

        var keyFunctionTimeline = function(d) {
            return d.startDate + "Test" + d.endDate;
        };

        var rectTransformTimeline = function(d) {
            if( x(d.startDate) < 0)
                return "translate(" + 0 + "," + 40 + ")";
            else
                return "translate(" + x(d.startDate) + "," + 40 + ")";
        };


        var rectTransformTimelineEnd = function(d) {
            if( x(d.endDate) < 0)
                return "translate(" + 0 + "," + 40 + ")";
            else
                return "translate(" + x(d.endDate) + "," + 40 + ")";
        };

        var keyFunctionAssignment = function(d) {
            if(d == undefined)
                return;

            return d.startDate + d.assignmentId + d.endDate;
        };

        var rectTransformAssignment = function(d) {
            
            var startD = new Date(component.get("v.startDate"));
            if(d == undefined)
                return;

            var startDate = d.startDate;
            if(startDate < startD.getTime())
                startDate = startD.getTime();

            var position = d.positionId; 
            return "translate(" + x(startDate) + "," + y2(d.assignmentId) + ")";

        };


        var rectTransformAssignmentEnd = function(d) 
        {
            
            if(d == undefined)
                return;
            if(d.endDate > endD.getTime())
                return "translate(" + x(endD.getTime()) + "," + y2(d.assignmentId) + ")";
            else
                return "translate(" + x(d.endDate) + "," + y2(d.assignmentId) + ")";
        };

        var setAssignmentColor = function(d){
            console.log('---- set color -----');
            console.log(d);
            if(d.assignmentType == 'Secondary'){
                return "rgb(255, 154, 60)";
            }
            else if(d.assignmentType == 'Primary'){
                return "rgb(4, 132, 75)";
            }else{
                return "rgb( 217, 100, 94)";
            }
            
        };

        var startD = new Date(component.get("v.startDate"));
        var endD = new Date(component.get("v.endDate"));

        var options = {  
            year: 'numeric',
            month: 'short',
            day: 'numeric',
        };
        document.getElementById('periodSpan').textContent = startD.toLocaleString('en-us', options) + ' - ' + endD.toLocaleString('en-us', options);

        var x = d3.scaleTime().domain([startD, endD]).range([0, width]);
        var y = d3.scaleBand().domain(assignmentTypes).range([ 0, height ]).padding(0.1);
        var y2 = d3.scaleBand().domain(uniqueAssignIds).range([ 120, height ]).padding(0.1);

        var color = d3.scaleOrdinal(d3.schemeCategory20b);

       

        var legendRectSize = 12;                                  
        var legendSpacing = 8; 

        var midDates = [];
        var midMonths = [];
        var years = [];
         
        var loopDate = new Date(startD.getFullYear(),startD.getMonth(),startD.getDate(),0,0,0);
        while(loopDate < endD) 
        {
            loopDate.setDate(15);
            midDates.push(new Date(loopDate.getFullYear(),loopDate.getMonth(),loopDate.getDate(),0,0,0));
            loopDate.setMonth(loopDate.getMonth()+1);
            if(loopDate.getMonth() == 6)
            {
                midMonths.push(new Date(loopDate.getFullYear(),loopDate.getMonth(),1,0,0,0));
            }
        }
        var xAxis = d3.axisTop().scale(x).tickValues(midDates).tickFormat(d3.timeFormat("%b")).tickPadding(".5");
        var yearAxis = d3.axisTop().scale(x).tickValues(midMonths).tickFormat(d3.timeFormat("%Y"));
        var qAxis = d3.axisTop().scale(x).tickValues( midDates).tickPadding(".5")
        .tickFormat(function(date)
        {
            if (d3.timeMonth(date).getMonth() == 1) 
            {
                return "Q1";
            } 
            else if(d3.timeMonth(date).getMonth() == 4)
            {
                return "Q2";
            }
            else if(d3.timeMonth(date).getMonth() == 7)
            {
                return "Q3";
            }
            else if(d3.timeMonth(date).getMonth() == 10)
            {
                return "Q4";
            }
        });

        var empName = component.get("v.assigneeName");

        var svg = d3.select("#timeline")
        .append("svg")
        .attr("class", "chart_" + empName)
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("class", "gantt-chart")
        .attr("width", width )
        .attr("height", height)
        .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");

        svg.append("g")
        .attr("class", "year axis")
        .attr("transform", "translate(0, 0)")
        .transition()
        .call(yearAxis);

        svg.append("g")
        .attr("class", "q axis")
        .attr("transform", "translate(0, 20)")
        .transition()
        .call(qAxis);

        svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0, 40)")
        .transition()
        .call(xAxis);

        var xAxisGrid = d3.axisTop().scale(x).ticks(midDates.length - 1).tickSize(-height, 0).tickSizeOuter(0);
        var qAxisGrid = d3.axisTop().scale(x).ticks(midDates.length/4).tickSize(-height-20, 0);

        svg.append("g").classed('xgrid', true).attr("transform", "translate(0, 20)").transition().call(xAxisGrid);
        svg.append("g").classed('qgrid', true).attr("transform", "translate(0, 0)").transition().call(qAxisGrid);

        // svg.selectAll(".xgrid .tick:last-child").attr("display", "none");

        svg.selectAll(".gantt-chart")
        .data(tempList, keyFunctionTimeline).enter()
        .append("rect")
        .attr("class", function(d){ 
            if(assignStatus[d.status] == null){ return "bar";}
            return assignStatus[d.status];
        }) 
        .attr("y", 10)
        .attr("transform", rectTransformTimeline)
        .attr("height", function(d) { return 40; })
        .attr("rx", 3)
        .attr("ry", 3)
        /*.attr('fill', function(d, i) {
            console.log('d.status 22:: '+d.status);
            return color(d.status);
        })*/
        .attr('fill',setAssignmentColor)
        .attr("width", function(d) { 
            if(d.endDate > endD.getTime())
                return (x(endD.getTime()) - x(d.startDate));
            else
                return (x(d.endDate) - x(d.startDate)); 
        })
        .append("title")
        .text(function(d) 
        { 
            var mystring ;
            if(d.startDate < startD.getTime() && d.endDate < startD.getTime())
                mystring = '';
            else if(d.startDate > endD.getTime() && d.endDate > endD.getTime())
                mystring = '';
            else
            {
                if(ns != '')
                {
                    mystring = $A.get("$Label.AxtriaSalesIQTM.Unassigned_Label");
                }
                else
                    mystring = $A.get("$Label.c.Unassigned_Label");
            }
            return mystring; 
        });

        svg.selectAll(".gantt-chart")
        .data(tempList,keyFunctionTimeline).enter()
        .append("text")
        .attr("transform", rectTransformTimeline)
        .attr("class","slds-truncate")
        .attr("id", function(d,i){ 
            return 'timelinetextid_' + i;
        })
        .attr("y", 35)
        .attr("x",0)
        .attr("font-size", "12px")
        .attr("fill","white")
        .text(function(d) 
        { 
            var mystring ;
            if(d.startDate < startD.getTime() && d.endDate < startD.getTime())
                mystring = '';
            else if(d.startDate > endD.getTime() && d.endDate > endD.getTime())
                mystring = '';
            else if (d.endDate - d.startDate < ( 35 * 1000 * 60 * 60 * 24))
                mystring = '';
            else
            {
                if(ns != '')
                {
                    mystring = $A.get("$Label.AxtriaSalesIQTM.Unassigned_Label");
                }
                else
                    mystring = $A.get("$Label.c.Unassigned_Label");
            }
            return mystring; 
        })
        .on('mouseover',function(d,i){
            $j('#timelinetextid_' + i).css("cursor","default");
        })
        .each(function(d) 
        {
            var self = d3.select(this),
            textLength = self.node().getComputedTextLength(),
            text = self.text();
            if(textLength < ((x(d.endDate) - x(d.startDate))))
            {
                var diff = (x(d.endDate) - x(d.startDate)) - textLength ;
                console.log(diff/2);
                self.attr('x',diff/2);
            }
            else
            {
                while (textLength > ((Math.min(x(d.endDate),x(endD.getTime())) - Math.max(x(d.startDate),x(startD.getTime()))) - 10) && text.length > 0) 
                {
                    text = text.slice(0, -1);
                    self.text(text + '...');
                    textLength = self.node().getComputedTextLength();

                }
                self.attr('x',5);
            }
        })
        .append("title")
        .text(function(d) 
        { 
            var mystring ;
            if(d.startDate < startD.getTime() && d.endDate < startD.getTime())
                mystring = '';
            else if(d.startDate > endD.getTime() && d.endDate > endD.getTime())
                mystring = '';
            else
            {
                if(ns != '')
                {
                    mystring = $A.get("$Label.AxtriaSalesIQTM.Unassigned_Label");
                }
                else
                    mystring = $A.get("$Label.c.Unassigned_Label");
            }
            return mystring; 
        });    

        svg.selectAll(".gantt-chart")
        .data(tempList,keyFunctionTimeline).enter()
        .append("text")
        .attr("transform", rectTransformTimeline)
        .attr("y", 20)
        .attr("x",5)
        .attr("id", function(d,i){ 
            return 'timelinestartid_' + i;
        })
        .attr("fill","white")
        .attr("font-size", "8px")
        .text(function(d) { 
            console.log(component.get("v.dateFormat"));
            var mystring = '';
            console.log('---- 111111111111' +d.startDate);
            
            if(d.startDate < startD.getTime() || d.endDate - d.startDate < ( 25 * 1000 * 60 * 60 * 24))
                mystring = '';
            else{
                // code change for date format - localization 
                var dateFormat = component.get("v.dateFormat");
                dateFormat = dateFormat.replace(/y+|Y+/, '');
                dateFormat = dateFormat.substring(0,dateFormat.length-1);
                var dt = new Date(d.startDate);
                console.log('dt before offset 893 :'+dt);
                //if(component.get("v.isEnabledDateRule") != 'true'){
                var offsetInMinutes = dt.getTimezoneOffset() ;
                dt.setSeconds(dt.getSeconds() + (dt.getTimezoneOffset()*60));
                //}
                
                console.log('dt line 893 :'+dt);
                if(component.get("v.dateFormat").indexOf('.') !== -1){
                    //mystring = (new Date(d.startDate).getMonth()+1) + '.' + new Date(d.startDate).getDate(); 
                    mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                   mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                    mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                }
                
            }
            console.log('mystring :: '+mystring);
            return mystring; 
        })
        .on('mouseover',function(d,i){
            $j('#timelinestartid_' + i).css("cursor","default");
        });

        svg.selectAll(".gantt-chart")
        .data(tempList,keyFunctionTimeline).enter()
        .append("text")
        .attr("transform", rectTransformTimelineEnd)
        .attr("y", 20)
        .attr("x", -25)
        .attr("font-size", "8px")
        .attr("fill","white")
        .attr("id", function(d,i){ 
            return 'timelineendid_' + i;
        })
        .text(function(d) { 
            var mystring = '';
            console.log('---- 222222222222222222');
            if(d.endDate > endD.getTime() || d.endDate - d.startDate < ( 45 * 1000 * 60 * 60 * 24))
                mystring = '';
            else{

                var dateFormat = component.get("v.dateFormat");
                dateFormat = dateFormat.replace(/y+|Y+/, '');
                dateFormat = dateFormat.substring(0,dateFormat.length-1);
                console.log('d.endDate :'+d.endDate);
                var dt = new Date(d.endDate);
                //if(component.get("v.isEnabledDateRule") != 'true'){
                    var offsetInMinutes = dt.getTimezoneOffset() ;
                    dt.setSeconds(dt.getSeconds() + (dt.getTimezoneOffset()*60));
                //}
                
                console.log('-- end Date :'+dt);
                if(component.get("v.dateFormat").indexOf('.') !== -1){
                    mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                   mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                    mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                }
                
                //mystring = (new Date(d.endDate).getMonth()+1) + '/' + new Date(d.endDate).getDate(); 
            }
            console.log('mystring 22 :'+mystring);
            return mystring; 
        })
        .on('mouseover',function(d,i){
            $j('#timelineendid_' + i).css("cursor","default");
        });


        svg.append("g")
        .attr("class", "gantt-chart2")
        .attr("width", width)
        .attr("height", height)
        .attr("transform", "translate(0,0)");  

        var chart3 = svg.selectAll(".gantt-chart2")
        .append('g')
        .attr("class","assignmentBar")
        
        chart3.selectAll(".assignmentBar")
        .data(chart2List,keyFunctionAssignment).enter()
        .append("rect")
        .attr("id", function(d,i){ 
            return 'id_' + i;
        })
        .attr("class", function(d){ 
            if(d.status == "Assigned")
            { 

                if(d.assignmentType == 'Primary'){
                    if(d.endDate == d.startDate)
                        return "bar-assignment highlightbar"
                    else
                        return "bar-assignment";
                }
                else{
                    console.log('222222222222');
                    if(d.endDate == d.startDate)
                        return "bar-secondary highlightbar"
                    else{

                        return "bar-secondary";
                    }
                    //return "bar-secondary";
                }
                /*if(d.endDate == d.startDate)
                    return "bar-assignment highlightbar"
                else
                    return "bar-assignment";

                if(d.assignmentType == 'Primary'){
                    return "bar-assignment";
                }else{
                    return "bar-secondary";
                }*/
            }
            else{
                console.log('unassigned 11111');
               return "bar-unassigned"; 
            } 
            return;
        })
        .on('click',function(d,i){
            console.log('444444444 on popup click ------');
            component.openPopover(i);
        })
        .on('mouseover',function(d,i){
            if(d.startDate != d.endDate)
            {
                $j('#id_' + i).addClass('highlightbar');
            }
            
        })
        .on("mouseout", function(d,i){
            if(d.startDate != d.endDate)
            {
                $j('#id_' + i).removeClass('highlightbar');
            }
            
        })
        .attr("y", 0)
        .attr("transform", rectTransformAssignment)
        .attr("height", function(d) 
        {        
            return Math.min(120,(height / (uniqueAssignIds.length * 2))); 
        })
        .attr("rx", 3)
        .attr("ry", 3)
        .attr("fill",setAssignmentColor) // added on 15th nov
        .attr("width", function(d) {
            console.log('1 : ',d);
            var endDate,startDate;
            if(d.endDate == d.startDate)
                return 1;

            if(d.endDate > endD.getTime())
                endDate = endD.getTime();
            else
                endDate = d.endDate;
            
            if(d.startDate < startD.getTime())
                startDate = startD.getTime();
            else
                startDate = d.startDate;

            return (x(endDate) - x(startDate)); 
        })
        .append("title")
        .text(function(d) 
        {   
            console.log('=d=',d);
            var mystring ;
            if(d.startDate < startD.getTime() && d.endDate < startD.getTime())
                mystring = '';
            else if(d.startDate > endD.getTime() && d.endDate > endD.getTime())
                mystring = '';
            else {
                //mystring = d.position + ', ('+d.assignmentType+')';
                console.log('-- text 1');
                mystring = d.position+'('+d.clientPositionCode+ ')' + ' : '+ d.assignmentType; 
              }
            return mystring; 
        });

        chart3.selectAll(".assignmentBar")
        .data(chart2List,keyFunctionAssignment).enter()
        .append("text")
        .attr("transform", rectTransformAssignment)
        .attr("class","slds-truncate")
        .attr("id", function(d,i){ 
            return 'textid_' + i;
        })
        .attr("y", 25)
        .attr("x",5)
        .attr("font-size", "12px")
        .attr("fill", "White") // text color position name 
        .text(function(d) 
        { 
            var mystring ;
            if(d.startDate < startD.getTime() && d.endDate < startD.getTime())
                mystring = '';
            else if(d.startDate > endD.getTime() && d.endDate > endD.getTime())
                mystring = '';
            else if ((Math.min(d.endDate,endD.getTime()) - Math.max(d.startDate,startD.getTime())) < ( 35 * 1000 * 60 * 60 * 24))
                mystring = '';
            else{
                //mystring = d.position + ', ('+d.assignmentType+')';
                console.log('-- text 2');
                 mystring = d.position+'('+d.clientPositionCode+ ')' + ' : '+ d.assignmentType; 
            }
            if((height / (uniqueAssignIds.length * 2)) < 25)
                mystring = ''; 
            return mystring; 
        })
        .on('click',function(d,i){
            console.log('55555555555 on popup click ------');
            component.openPopover(i);
        })
        .on('mouseover',function(d,i){
            $j('#id_' + i).addClass('highlightbar');
            $j('#textid_' + i).css("cursor","pointer");
        })
        .on("mouseout", function(d,i){
            //$j('#id_' + i).removeClass('highlightbar');
        })
        .each(function(d) 
        {
            var self = d3.select(this),
            textLength = self.node().getComputedTextLength(),
            text = self.text();
            while (textLength > ((Math.min(x(d.endDate),x(endD.getTime())) - Math.max(x(d.startDate),x(startD.getTime()))) - 10) && text.length > 0) 
            {
                text = text.slice(0, -1);
                self.text(text + '...');
                textLength = self.node().getComputedTextLength();
            }
        })
        .append("title")
        .text(function(d) 
        { 
            var mystring ;
            if(d.startDate < startD.getTime() && d.endDate < startD.getTime())
                mystring = '';
            else if(d.startDate > endD.getTime() && d.endDate > endD.getTime())
                mystring = '';
            else{
                // mystring = d.position + ', ('+d.assignmentType+')'; 
                console.log('-- text 3');
                    mystring = d.position+'('+d.clientPositionCode+ ')' + ' : '+ d.assignmentType; 
                }
            
            return mystring; 
        });
        


        chart3.selectAll(".assignmentBar")
        .data(chart2List,keyFunctionAssignment).enter()
        .append("text")
        .attr("transform", rectTransformAssignment)
        .attr("y", 10)
        .attr("x",function(d)
        {
            if(new Date(d.startDate).toISOString().slice(0,10) < new Date(startD).toISOString().slice(0,10) && d.endDate > startD.getTime())
            {
                return 17;
            }
            else
                return 5;
        })
        .attr("id", function(d,i){ 
            return 'startid_' + i;
        })
        .attr("font-size", "8px")
        .attr("fill", "White") //1
        .text(function(d) 
        { 
            var mystring = '';
            console.log('---- 3333333333333333');
            if(new Date(d.startDate).toISOString().slice(0,10) < new Date(startD).toISOString().slice(0,10))
            {
                if((Math.min(d.endDate,endD.getTime()) - Math.max(d.startDate,startD.getTime())) > ( 25 * 1000 * 60 * 60 * 24))
                {
                    var dateFormat = component.get("v.dateFormat");
                    dateFormat = dateFormat.replace(/y+|Y+/, '');
                    dateFormat = dateFormat.substring(0,dateFormat.length-1);
                    var dt = new Date(startD);
                    var offsetInMinutes = dt.getTimezoneOffset();
                    dt.setSeconds(dt.getSeconds() + (dt.getTimezoneOffset()*60));
                    console.log('dt line 1163:'+dt);
                    if(component.get("v.dateFormat").indexOf('.') !== -1){
                        mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                       mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                        mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }
                }
                else
                {
                    // if(d.endDate > startD.getTime())
                    //     mystring = '\u2190';
                    // else
                        mystring = '';
                }   
            }   
            else
            {
                if((Math.min(d.endDate,endD.getTime()) - Math.max(d.startDate,startD.getTime())) > ( 25 * 1000 * 60 * 60 * 24))
                {
                    console.log('---- 555555555555555555');
                    console.log('start -'+d.startDate);
                    var dateFormat = component.get("v.dateFormat");
                    dateFormat = dateFormat.replace(/y+|Y+/, '');
                    dateFormat = dateFormat.substring(0,dateFormat.length-1);
                    var dt = new Date(d.startDate);
                    
                    //if(component.get("v.isEnabledDateRule") != 'true'){
                        var offsetInMinutes = dt.getTimezoneOffset() ;
                        dt.setSeconds(dt.getSeconds() + (dt.getTimezoneOffset()*60));
                    //}
                    console.log('dt :'+dt);
                    if(component.get("v.dateFormat").indexOf('.') !== -1){
                        //mystring = (new Date(d.startDate).getMonth()+1) + '.' + new Date(d.startDate).getDate(); 
                        mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                       mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                        mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }
                }
                else
                {
                    mystring = '';
                }
            }       
            return mystring;
            // var mystring = '';
            // if(d.startDate < startD.getTime() || (Math.min(d.endDate,endD.getTime()) - Math.max(d.startDate,startD.getTime())) < ( 25 * 1000 * 60 * 60 * 24))
            //     mystring = '';
            // else
            //     mystring = (new Date(d.startDate).getMonth()+1) + '/' + new Date(d.startDate).getDate(); 
            // return mystring; 
        })
        .on('click',function(d,i){
            console.log('666666666 on popup click ------');
            component.openPopover(i);
        })
        .on('mouseover',function(d,i){
            $j('#id_' + i).addClass('highlightbar');
            $j('#startid_' + i).css("cursor","pointer");
        })
        .on("mouseout", function(d,i){
            //$j('#id_' + i).removeClass('highlightbar');
        })
        .append("tspan")
        .attr("x",5)
        .style("font-size","10px")
        .text(function(d)
        {
            if(new Date(d.startDate).toISOString().slice(0,10) < new Date(startD).toISOString().slice(0,10) && d.endDate > startD.getTime())
            {   
                console.log('in date--->');
                console.log(new Date(d.startDate).toISOString());
                console.log(new Date(startD).toISOString());
                // console.log();
                return '\u2190 ';
            }
            else
                return '';
        });

        chart3.selectAll(".assignmentBar")
        .data(chart2List,keyFunctionAssignment).enter()
        .append("text")
        .attr("transform", rectTransformAssignmentEnd)
        .attr("y", 10)
        .attr("x", function(d)
        {
            if(d.endDate > endD.getTime())
            {
                if((Math.min(d.endDate,endD.getTime()) - Math.max(d.startDate,startD.getTime())) > ( 60 * 1000 * 60 * 60 * 24))
                {
                   return -55;
                }
                else
                {
                   return -10;
                }   
            }
            else
            {
                return -50;
            }  
        })
        .attr("font-size", "8px")
        .attr("fill","White") //2
        .attr("id", function(d,i){ 
            return 'endid_' + i;
        })
        .text(function(d) 
        { 
            var mystring = '';
            if(d.endDate > endD.getTime())
            {
                if((Math.min(d.endDate,endD.getTime()) - Math.max(d.startDate,startD.getTime())) > (60 * 1000 * 60 * 60 * 24))
                {
                    console.log('---- 666666666666666');
                    var dateFormat = component.get("v.dateFormat");
                    dateFormat = dateFormat.replace(/y+|Y+/, '');
                    dateFormat = dateFormat.substring(0,dateFormat.length-1);
                    var dt = new Date(d.endDate);
                    
                    var offsetInMinutes = dt.getTimezoneOffset() ;
                    dt.setSeconds(dt.getSeconds() + (dt.getTimezoneOffset()*60));

                    console.log('dt :'+dt);
                    if(component.get("v.dateFormat").indexOf('.') !== -1){
                        mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                       mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                        mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }
                    //mystring = (new Date(endD).getMonth()+1) + '/' + new Date(endD).getDate() + '/' + new Date(endD).getFullYear();
                    /*if(component.get("v.dateFormat").indexOf('.') !== -1){
                        mystring = (new Date(d.endD).getMonth()+1) + '.' + new Date(d.endD).getDate(); 
                    }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                        mystring = (new Date(d.endD).getMonth()+1) + '/' + new Date(d.endD).getDate(); 
                    }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                        mystring = (new Date(d.endD).getMonth()+1) + '-' + new Date(d.endD).getDate(); 
                    }*/
                }
                else
                {
                    // if(d.startDate < endD.getTime())
                    //     mystring = '\u2192';
                    // else
                        mystring = '';
                }   
            }   
            else
            {
                if((Math.min(d.endDate,endD.getTime()) - Math.max(d.startDate,startD.getTime())) > ( 60 * 1000 * 60 * 60 * 24))
                {

                    console.log('---- 7777777777');
                    var dateFormat = component.get("v.dateFormat");
                    dateFormat = dateFormat.replace(/y+|Y+/, '');
                    dateFormat = dateFormat.substring(0,dateFormat.length-1);
                    var dt = new Date(d.endDate);
                    console.log('dt :'+dt);
                    var offsetInMinutes = dt.getTimezoneOffset() ;
                    //if(component.get("v.isEnabledDateRule") != 'true'){
                    dt.setSeconds(dt.getSeconds() + (dt.getTimezoneOffset()*60));
                    //}
                    console.log('end date line 1324 :'+dt);
                    if(component.get("v.dateFormat").indexOf('.') !== -1){
                        mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                       mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                        mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }
                     //mystring = (new Date(d.endDate).getMonth()+1) + '/' + new Date(d.endDate).getDate()+ '/' + new Date(d.endDate).getFullYear();
                    /* if(component.get("v.dateFormat").indexOf('.') !== -1){
                        mystring = (new Date(d.endDate).getMonth()+1) + '.' + new Date(d.endDate).getDate(); 
                    }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                        mystring = (new Date(d.endDate).getMonth()+1) + '/' + new Date(d.endDate).getDate(); 
                    }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                        mystring = (new Date(d.endDate).getMonth()+1) + '-' + new Date(d.endDate).getDate(); 
                    }*/
                }
                else
                {
                    mystring = '';
                }
            }       
            return mystring; 
        })
        .on('click',function(d,i){
            console.log('7777 on popup click ------');
            component.openPopover(i);
        })
        .on('mouseover',function(d,i){
            $j('#id_' + i).addClass('highlightbar');
            $j('#endid_' + i).css("cursor","pointer");
        })
        .on("mouseout", function(d,i){
            //$j('#id_' + i).removeClass('highlightbar');
        })
        .append("tspan")
        .style("font-size","10px")
        .text(function(d)
        {
            if(d.endDate > endD.getTime() && d.startDate < endD.getTime())
            {   
                return ' \u2192';
            }
        });

        var y2values=height,y2values1;
        var today = new Date().getTime();

        svg.append("line")
        .attr("x1", x(today))  //<<== change your code here
        .attr("y1", 0)
        .attr("x2", x(today))  //<<== and here
        .attr("y2", y2values + 20)
        .style("stroke-width", 1)
        .style("stroke", "#D9645E")
        .style("fill", "none");
    },

    drawPositionTimeline : function(component,event,tempList,assignmentList)
    {
        var ns = component.get("v.namespace");
        var $j = jQuery.noConflict();
        var margin = {top: 30, right: 20, bottom: 30, left: 20};
        // var width = $j('.slds-modal__container').width() * 0.75 - margin.left - margin.right ;
        // var height =  $j('.slds-modal__content').height()  - margin.top - margin.bottom;

        var width = document.getElementsByClassName('slds-modal__container')[0].clientWidth * 0.75 - margin.left - margin.right ;
        var height =  document.getElementsByClassName('slds-modal__content')[0].clientHeight  - margin.top - margin.bottom - document.getElementById('timelineChangeDiv').clientHeight;

        // var modalheight = $j('.slds-modal__container').height();
        // var modalwidth = $j('.slds-modal__container').width();
        // console.log(modalheight + ':::' + modalwidth);

        var assignStatus = {"Assigned" : "bar-assigned","Unassigned" : "bar-unassigned"};

        var assignmentTypes = ["Test"];
        var empNames = [];
        var chart2List = [];
        var posIds = [];
        var assignIds = [];


        for(var i = 0;i<assignmentList.length;i++)
        {
            if(assignmentList[i].status == 'Assigned')
            {
                empNames.push(assignmentList[i].employee);
                
                posIds.push(assignmentList[i].positionId);
                
            }
            chart2List.push(assignmentList[i]);
            assignIds.push(assignmentList[i].assignmentId);
        }
        
        var uniqueEmployees = empNames.filter(function(item, i, ar){ return ar.indexOf(item) === i; });
        var uniquePositionIds = posIds.filter(function(item, i, ar){ return ar.indexOf(item) === i; });
        var uniqueAssignIds = assignIds.filter(function(item, i, ar){ return ar.indexOf(item) === i; });
        console.log(uniqueAssignIds);

        var format = "%b";

        var keyFunctionTimeline = function(d) {
            return d.startDate + "Test" + d.endDate;
        };

        var rectTransformTimeline = function(d) {
            if( x(d.startDate) < 0)
                return "translate(" + 0 + "," + 40 + ")";
            else
                return "translate(" + x(d.startDate) + "," + 40 + ")";
        };

        var rectTransformTimelineEnd = function(d) 
        {
            if( x(d.endDate) < 0)
                return "translate(" + 0 + "," + 40 + ")";
            else
                return "translate(" + x(d.endDate) + "," + 40 + ")";
        };

        var keyFunctionAssignment = function(d) {
            if(d == undefined)
                return;

            return d.startDate + d.assignmentId + d.endDate;
        };

        var rectTransformAssignment = function(d) {
            
            var startD = new Date(component.get("v.startDate"));
            if(d == undefined)
                return;

            var startDate = d.startDate;
            if(startDate < startD.getTime())
                startDate = startD.getTime();

            var position = d.positionId; 
            return "translate(" + x(startDate) + "," + y2(d.assignmentId) + ")";

        };

        var setAssignmentColor = function(d){
            console.log('---- set color -----');
            console.log(d);
            if(d.assignmentType == 'Secondary'){
                return "rgb(255, 154, 60)";
            }
            else if(d.assignmentType == 'Primary'){
                return "rgb(4, 132, 75)";
            }else{
                return "rgb( 217, 100, 94)";
            }
            
        }


        var rectTransformAssignmentEnd = function(d) {
            
            if(d == undefined)
                return;
            if(d.endDate > endD.getTime())
                return "translate(" + x(endD.getTime()) + "," + y2(d.assignmentId) + ")";
            else
                return "translate(" + x(d.endDate) + "," + y2(d.assignmentId) + ")";
        };

        // var startD = new Date(component.get("v.startDate"));
        // var endD = new Date(component.get("v.endDate"));

        var startD = new Date(component.get("v.startDate"));
        var endD = new Date(component.get("v.endDate"));

        var options = {  
            year: 'numeric',
            month: 'short',
            day: 'numeric',
        };
        document.getElementById('periodSpan').textContent = startD.toLocaleString('en-us', options) + ' - ' + endD.toLocaleString('en-us', options);

        var x = d3.scaleTime().domain([startD, endD]).range([0, width]);
        var y = d3.scaleBand().domain(assignmentTypes).range([ 0, height ]).padding(0.1);
        var y2 = d3.scaleBand().domain(uniqueAssignIds).range([ 120, height ]).padding(0.1);

        var color = d3.scaleOrdinal(d3.schemeCategory20b);

       

        var legendRectSize = 12;                                  
        var legendSpacing = 8; 

        var midDates = [];
        var midMonths = [];
        var years = [];
         
        var loopDate = new Date(startD.getFullYear(),startD.getMonth(),startD.getDate(),0,0,0);
        while(loopDate < endD) 
        {
            loopDate.setDate(15);
            midDates.push(new Date(loopDate.getFullYear(),loopDate.getMonth(),loopDate.getDate(),0,0,0));
            loopDate.setMonth(loopDate.getMonth()+1);
            if(loopDate.getMonth() == 6)
            {
                midMonths.push(new Date(loopDate.getFullYear(),loopDate.getMonth(),1,0,0,0));
            }
        }
        var xAxis = d3.axisTop().scale(x).tickValues(midDates).tickFormat(d3.timeFormat("%b")).tickPadding(".5");
        var yearAxis = d3.axisTop().scale(x).tickValues(midMonths).tickFormat(d3.timeFormat("%Y"));
        var qAxis = d3.axisTop().scale(x).tickValues( midDates).tickPadding(".5")
        .tickFormat(function(date)
        {
            if (d3.timeMonth(date).getMonth() == 1) 
            {
                return "Q1";
            } 
            else if(d3.timeMonth(date).getMonth() == 4)
            {
                return "Q2";
            }
            else if(d3.timeMonth(date).getMonth() == 7)
            {
                return "Q3";
            }
            else if(d3.timeMonth(date).getMonth() == 10)
            {
                return "Q4";
            }
        });

        var empName = component.get("v.assigneeName");

        var svg = d3.select("#timeline")
        .append("svg")
        .attr("class", "chart_" + empName)
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("class", "gantt-chart")
        .attr("width", width )
        .attr("height", height)
        .attr("transform", "translate(" + margin.left + ", " + margin.top + ")");

        svg.append("g")
        .attr("class", "year axis")
        .attr("transform", "translate(0, 0)")
        .transition()
        .call(yearAxis);

        svg.append("g")
        .attr("class", "q axis")
        .attr("transform", "translate(0, 20)")
        .transition()
        .call(qAxis);

        svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0, 40)")
        .transition()
        .call(xAxis);

        var xAxisGrid = d3.axisTop().scale(x).ticks(midDates.length - 1).tickSize(-height, 0).tickSizeOuter(0);
        var qAxisGrid = d3.axisTop().scale(x).ticks(midDates.length/4).tickSize(-height-20, 0);

        svg.append("g").classed('xgrid', true).attr("transform", "translate(0, 20)").transition().call(xAxisGrid);
        svg.append("g").classed('qgrid', true).attr("transform", "translate(0, 0)").transition().call(qAxisGrid);

        // svg.selectAll(".xgrid .tick:last-child").attr("display", "none");
        svg.selectAll(".gantt-chart")
        .data(tempList, keyFunctionTimeline).enter()
        .append("rect")
        .attr("class", function(d){ 
            if(assignStatus[d.status] == null){ return "bar";}
            return assignStatus[d.status];
        }) 
        .attr("y", 10)
        .attr("transform", rectTransformTimeline)
        .attr("height", function(d) { return 40; })
        .attr("rx", 3)
        .attr("ry", 3)
        /*.attr('fill', function(d, i) {
            console.log('d.status :: '+d.status);
            return color(d.status);
        })*/
        .attr('fill',setAssignmentColor)
        .attr("width", function(d) { 
            if(d.endDate > endD.getTime())
                return (x(endD.getTime()) - x(d.startDate));
            else
                return (x(d.endDate) - x(d.startDate)); 
        })
        .append("title")
        .text(function(d) 
        { 
            var mystring ;
            if(d.startDate < startD.getTime() && d.endDate < startD.getTime())
                mystring = '';
            else if(d.startDate > endD.getTime() && d.endDate > endD.getTime())
                mystring = '';
            else
            {
                if(ns != '')
                {
                    mystring = $A.get("$Label.AxtriaSalesIQTM.Unassigned_Label");
                }
                else
                    mystring = $A.get("$Label.c.Unassigned_Label");
            }
            return mystring; 
        });

        svg.selectAll(".gantt-chart")
        .data(tempList,keyFunctionTimeline).enter()
        .append("text")
        .attr("transform", rectTransformTimeline)
        .attr("class","slds-truncate")
        .attr("id", function(d,i){ 
            return 'timelinetextid_' + i;
        })
        .attr("y", 35)
        .attr("x",0)
        .attr("font-size", "12px")
        .attr("fill","white")
        .text(function(d) 
        { 
            var mystring ;
            if(d.startDate < startD.getTime() && d.endDate < startD.getTime())
                mystring = '';
            else if(d.startDate > endD.getTime() && d.endDate > endD.getTime())
                mystring = '';
            else if (d.endDate - d.startDate < ( 35 * 1000 * 60 * 60 * 24))
                mystring = '';
            else
            {
                if(ns != '')
                {
                    mystring = $A.get("$Label.AxtriaSalesIQTM.Unassigned_Label");
                }
                else
                    mystring = $A.get("$Label.c.Unassigned_Label");
            }
            return mystring; 
        })
        .on('mouseover',function(d,i){
            $j('#timelinetextid_' + i).css("cursor","default");
        })
        .each(function(d) 
        {
            var self = d3.select(this),
            textLength = self.node().getComputedTextLength(),
            text = self.text();
            if(textLength < ((x(d.endDate) - x(d.startDate))))
            {
                var diff = (x(d.endDate) - x(d.startDate)) - textLength ;
                console.log(diff/2);
                self.attr('x',diff/2);
            }
            else
            {
                while (textLength > ((Math.min(x(d.endDate),x(endD.getTime())) - Math.max(x(d.startDate),x(startD.getTime()))) - 10) && text.length > 0) 
                {
                    text = text.slice(0, -1);
                    self.text(text + '...');
                    textLength = self.node().getComputedTextLength();

                }
                self.attr('x',5);
            }            
        })
        .append("title")
        .text(function(d) 
        { 
            var mystring ;
            if(d.startDate < startD.getTime() && d.endDate < startD.getTime())
                mystring = '';
            else if(d.startDate > endD.getTime() && d.endDate > endD.getTime())
                mystring = '';
            else
            {
                if(ns != '')
                {
                    mystring = $A.get("$Label.AxtriaSalesIQTM.Unassigned_Label");
                }
                else
                    mystring = $A.get("$Label.c.Unassigned_Label");
            }
            return mystring; 
        });    

        svg.selectAll(".gantt-chart")
        .data(tempList,keyFunctionTimeline).enter()
        .append("text")
        .attr("transform", rectTransformTimeline)
        .attr("y", 20)
        .attr("x",5)
        .attr("id", function(d,i){ 
            return 'timelinestartid_' + i;
        })
        .attr("fill","white")
        .attr("font-size", "8px")
        .text(function(d) { 
            var mystring = '';
            console.log('---- 888888888888');
            if(d.startDate < startD.getTime() || d.endDate - d.startDate < ( 25 * 1000 * 60 * 60 * 24))
                mystring = '';
            else{


                 var dateFormat = component.get("v.dateFormat");
                dateFormat = dateFormat.replace(/y+|Y+/, '');
                dateFormat = dateFormat.substring(0,dateFormat.length-1);
                var dt = new Date(d.startDate);
                //if(component.get("v.isEnabledDateRule") != 'true'){
                    var offsetInMinutes = dt.getTimezoneOffset() ;
                    dt.setSeconds(dt.getSeconds() + (dt.getTimezoneOffset()*60));
               // }
                console.log('d.startDate :'+dt);
                if(component.get("v.dateFormat").indexOf('.') !== -1){
                    mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                   mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                    mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                }
                /*if(component.get("v.dateFormat").indexOf('.') !== -1){
                        mystring = (new Date(d.startDate).getMonth()+1) + '.' + new Date(d.startDate).getDate(); 
                    }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                        mystring = (new Date(d.startDate).getMonth()+1) + '/' + new Date(d.startDate).getDate(); 
                    }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                        mystring = (new Date(d.startDate).getMonth()+1) + '-' + new Date(d.startDate).getDate(); 
                    }*/
                //mystring = (new Date(d.startDate).getMonth()+1) + '/' + new Date(d.startDate).getDate(); 
            }
            return mystring; 
        })
        .on('mouseover',function(d,i){
            $j('#timelinestartid_' + i).css("cursor","default");
        });

        svg.selectAll(".gantt-chart")
        .data(tempList,keyFunctionTimeline).enter()
        .append("text")
        .attr("transform", rectTransformTimelineEnd)
        .attr("y", 20)
        .attr("x", -25)
        .attr("font-size", "8px")
        .attr("fill","white")
        .attr("id", function(d,i){ 
            return 'timelineendid_' + i;
        })
        .text(function(d) { 
            var mystring = '';
            if(d.endDate > endD.getTime() || d.endDate - d.startDate < ( 45 * 1000 * 60 * 60 * 24))
                mystring = '';
            else{
                /*if(component.get("v.dateFormat").indexOf('.') !== -1){
                        mystring = (new Date(d.endDate).getMonth()+1) + '.' + new Date(d.endDate).getDate(); 
                    }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                        mystring = (new Date(d.endDate).getMonth()+1) + '/' + new Date(d.endDate).getDate(); 
                    }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                        mystring = (new Date(d.endDate).getMonth()+1) + '-' + new Date(d.endDate).getDate(); 
                    }*/

                console.log('---- 9999999999999');
                var dateFormat = component.get("v.dateFormat");
                dateFormat = dateFormat.replace(/y+|Y+/, '');
                dateFormat = dateFormat.substring(0,dateFormat.length-1);
                console.log('d.endDate :'+d.endDate);
                var dt = new Date(d.endDate);
                //if(component.get("v.isEnabledDateRule") != 'true'){
                    var offsetInMinutes = dt.getTimezoneOffset() ;
                    dt.setSeconds(dt.getSeconds() + (dt.getTimezoneOffset()*60));
                //}
                console.log('d.endDate :'+dt);
                
                if(component.get("v.dateFormat").indexOf('.') !== -1){
                    //mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate());
                }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                   //mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                   mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate());
                }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                   // mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                   mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate());
                }
                //mystring = (new Date(d.endDate).getMonth()+1) + '/' + new Date(d.endDate).getDate(); 
            }
            return mystring; 
        })
        .on('mouseover',function(d,i){
            $j('#timelineendid_' + i).css("cursor","default");
        });


        svg.append("g")
        .attr("class", "gantt-chart2")
        .attr("width", width)
        .attr("height", height)
        .attr("transform", "translate(0,0)");  

        var chart3 = svg.selectAll(".gantt-chart2")
        .append('g')
        .attr("class","assignmentBar")
        
        chart3.selectAll(".assignmentBar")
        .data(chart2List,keyFunctionAssignment).enter()
        .append("rect")
        .attr("id", function(d,i){ 
            return 'id_' + i;
        })
        .attr("class", function(d){ 
            if(d.status == "Assigned")
            {
                console.log('d :::',d.assignmentType);
                if(d.assignmentType == 'Primary'){
                    if(d.endDate == d.startDate)
                        return "bar-assignment highlightbar"
                    else
                        return "bar-assignment";
                }
                else{
                    console.log('222222222222');
                    if(d.endDate == d.startDate)
                        return "bar-secondary highlightbar"
                    else
                        return "bar-secondary";
                    //return "bar-secondary";
                }
            }
            else{ 
                console.log('unassigned 222222');
                return "bar-unassigned";
            }
            return;
        })
        .attr("fill",setAssignmentColor)
        .on('click',function(d,i){
            console.log('88888 on popup click ------',i);
            component.openPopover(i);
        })
        .on('mouseover',function(d,i){
            if(d.startDate != d.endDate)
            {
                $j('#id_' + i).addClass('highlightbar');
            }
            
        })
        .on("mouseout", function(d,i){
            if(d.startDate != d.endDate)
            {
                $j('#id_' + i).removeClass('highlightbar');
            }
            
        })
        .attr("y", 0)
        .attr("transform", rectTransformAssignment)
        .attr("height", function(d) 
        {        
            return Math.min(120,(height / (uniqueAssignIds.length * 2))); 
        })
        .attr("rx", 3)
        .attr("ry", 3)
        .attr("width", function(d) {

            var endDate,startDate;
            if(d.endDate == d.startDate)
                return 1;

            if(d.endDate > endD.getTime())
                endDate = endD.getTime();
            else
                endDate = d.endDate;
            
            if(d.startDate < startD.getTime())
                startDate = startD.getTime();
            else
                startDate = d.startDate;

            return (x(endDate) - x(startDate)); 
        })
        .append("title")
        .text(function(d) 
        { 
            var mystring ;
            if(d.startDate < startD.getTime() && d.endDate < startD.getTime())
                mystring = '';
            else if(d.startDate > endD.getTime() && d.endDate > endD.getTime())
                mystring = '';
            else
                mystring = d.employee + ', ('+d.assignmentType+')'; 
            return mystring; 
        });

        chart3.selectAll(".assignmentBar")
        .data(chart2List,keyFunctionAssignment).enter()
        .append("text")
        .attr("transform", rectTransformAssignment)
        .attr("class","slds-truncate")
        .attr("id", function(d,i){ 
            return 'textid_' + i;
        })
        .attr("y", 25)
        .attr("x",5)
        .attr("font-size", "12px")
        .attr("fill", "white")
        .text(function(d) 
        { 
            var mystring ;
            if(d.startDate < startD.getTime() && d.endDate < startD.getTime())
                mystring = '';
            else if(d.startDate > endD.getTime() && d.endDate > endD.getTime())
                mystring = '';
            else if ((Math.min(d.endDate,endD.getTime()) - Math.max(d.startDate,startD.getTime())) < ( 35 * 1000 * 60 * 60 * 24))
                mystring = '';
            else
                mystring = d.employee + ', ('+d.assignmentType+')'; 
            if((height / (uniqueAssignIds.length * 2)) < 25)
                mystring = '';
            return mystring; 
        })
        .on('click',function(d,i){
            console.log('11111 on popup click ------' +i);
            component.openPopover(i);
        })
        .on('mouseover',function(d,i){
            $j('#id_' + i).addClass('highlightbar');
            $j('#textid_' + i).css("cursor","pointer");
        })
        .on("mouseout", function(d,i){
            //$j('#id_' + i).removeClass('highlightbar');
        })
        .each(function(d) 
        {
            var self = d3.select(this),
            textLength = self.node().getComputedTextLength(),
            text = self.text();
            while (textLength > ((Math.min(x(d.endDate),x(endD.getTime())) - Math.max(x(d.startDate),x(startD.getTime()))) - 10) && text.length > 0) 
            {
                text = text.slice(0, -1);
                self.text(text + '...');
                textLength = self.node().getComputedTextLength();
            }
        })
        .append("title")
        .text(function(d) 
        { 
            var mystring ;
            if(d.startDate < startD.getTime() && d.endDate < startD.getTime())
                mystring = '';
            else if(d.startDate > endD.getTime() && d.endDate > endD.getTime())
                mystring = '';
            else
                mystring = d.employee + ', ('+d.assignmentType+')'; 
            return mystring; 
        });
        


        chart3.selectAll(".assignmentBar")
        .data(chart2List,keyFunctionAssignment).enter()
        .append("text")
        .attr("transform", rectTransformAssignment)
        .attr("y", 10)
        .attr("x",function(d)
        {
            if(new Date(d.startDate).toISOString().slice(0,10) < new Date(startD).toISOString().slice(0,10) && d.endDate > startD.getTime())
            {
                return 17;
            }
            else
                return 5;
        })
        .attr("id", function(d,i){ 
            return 'startid_' + i;
        })
        .attr("font-size", "8px")
        .attr("fill", "white")
        .text(function(d) 
        { 
            console.log('hhhhhhhhhhhhhhhhh');
            var mystring = '';

            if(new Date(d.startDate).toISOString().slice(0,10) < new Date(startD).toISOString().slice(0,10))
            {
                 console.log('===d.startDate======'+d.startDate);
                if((Math.min(d.endDate,endD.getTime()) - Math.max(d.startDate,startD.getTime())) > ( 25 * 1000 * 60 * 60 * 24))
                {
                    var dateFormat = component.get("v.dateFormat");
                    dateFormat = dateFormat.replace(/y+|Y+/, '');
                    dateFormat = dateFormat.substring(0,dateFormat.length-1);
                    var dt = new Date(startD);
                    console.log('===startD======'+startD);
                    console.log('-- new startdate :'+new Date(d.startDate));
                    if(component.get("v.dateFormat").indexOf('.') !== -1){
                        mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                       mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                        mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }
                    /*if(component.get("v.dateFormat").indexOf('.') !== -1){
                        mystring = (new Date(startD).getMonth()+1) + '.' + new Date(startD).getDate(); 
                    }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                        mystring = (new Date(startD).getMonth()+1) + '/' + new Date(startD).getDate(); 
                    }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                        mystring = (new Date(startD).getMonth()+1) + '-' + new Date(startD).getDate(); 
                    }*/
                    //mystring = (new Date(startD).getMonth()+1) + '/' + new Date(startD).getDate() + '/' + new Date(startD).getFullYear();
                }
                else
                {
                    // if(d.endDate > startD.getTime())
                    //     mystring = '\u2190';
                    // else
                        mystring = '';
                }   
            }   
            else
            {
                if((Math.min(d.endDate,endD.getTime()) - Math.max(d.startDate,startD.getTime())) > ( 25 * 1000 * 60 * 60 * 24))
                {
                    console.log('---- ggggggggggggggg');
                    var dateFormat = component.get("v.dateFormat");
                    dateFormat = dateFormat.replace(/y+|Y+/, '');
                    dateFormat = dateFormat.substring(0,dateFormat.length-1);
                    var dt = new Date(d.startDate);
                    //if(component.get("v.isEnabledDateRule") != 'true'){
                        var offsetInMinutes = dt.getTimezoneOffset() ;
                        dt.setSeconds(dt.getSeconds() + (dt.getTimezoneOffset()*60));
                    //}
                    
                    console.log('dt 2034:'+dt);
                    if(component.get("v.dateFormat").indexOf('.') !== -1){
                        mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                       mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                        mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }
                    /*if(component.get("v.dateFormat").indexOf('.') !== -1){
                        mystring = (new Date(d.startDate).getMonth()+1) + '.' + new Date(d.startDate).getDate(); 
                    }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                        mystring = (new Date(d.startDate).getMonth()+1) + '/' + new Date(d.startDate).getDate(); 
                    }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                        mystring = (new Date(d.startDate).getMonth()+1) + '-' + new Date(d.startDate).getDate(); 
                    }*/
                    // mystring = (new Date(d.startDate).getMonth()+1) + '/' + new Date(d.startDate).getDate()+ '/' + new Date(d.startDate).getFullYear();
                }
                else
                {
                    mystring = '';
                }
            }       
            return mystring;
            // var mystring = '';
            // if(d.startDate < startD.getTime() || (Math.min(d.endDate,endD.getTime()) - Math.max(d.startDate,startD.getTime())) < ( 25 * 1000 * 60 * 60 * 24))
            //     mystring = '';
            // else
            //     mystring = (new Date(d.startDate).getMonth()+1) + '/' + new Date(d.startDate).getDate(); 
            // return mystring; 
        })
        .on('click',function(d,i){
            console.log('2222222 on popup click ------');
            component.openPopover(i);
        })
        .on('mouseover',function(d,i){
            $j('#id_' + i).addClass('highlightbar');
            $j('#startid_' + i).css("cursor","pointer");
        })
        .on("mouseout", function(d,i){
            //$j('#id_' + i).removeClass('highlightbar');
        })
        .append("tspan")
        .attr("x",5)
        .style("font-size","10px")
        .text(function(d)
        {
            if(new Date(d.startDate).toISOString().slice(0,10) < new Date(startD).toISOString().slice(0,10) && d.endDate > startD.getTime())
            {   
                return '\u2190 ';
            }
        });

        chart3.selectAll(".assignmentBar")
        .data(chart2List,keyFunctionAssignment).enter()
        .append("text")
        .attr("transform", rectTransformAssignmentEnd)
        .attr("y", 10)
        .attr("x", function(d)
        {
            if(d.endDate > endD.getTime())
            {
                if((Math.min(d.endDate,endD.getTime()) - Math.max(d.startDate,startD.getTime())) > ( 60 * 1000 * 60 * 60 * 24))
                {
                   return -55;
                }
                else
                {
                   return -10;
                }   
            }
            else
            {
                return -50;
            }  
        })
        .attr("font-size", "8px")
        .attr("fill", "white")
        .attr("id", function(d,i){ 
            return 'endid_' + i;
        })
        .text(function(d) 
        { 
            console.log('jjjjjjjjjjjjjjj');
            var mystring = '';
            if(d.endDate > endD.getTime())
            {
                if((Math.min(d.endDate,endD.getTime()) - Math.max(d.startDate,startD.getTime())) > (60 * 1000 * 60 * 60 * 24))
                {
                   
                    var dt = new Date(d.endDate);
                    //if(component.get("v.isEnabledDateRule") != 'true'){
                        var offsetInMinutes = dt.getTimezoneOffset() ;
                        dt.setSeconds(dt.getSeconds() + (dt.getTimezoneOffset()*60));
                    //}

                     console.log('==== endD===='+dt);
                    var dateFormat = component.get("v.dateFormat");
                    dateFormat = dateFormat.replace(/y+|Y+/, '');
                    dateFormat = dateFormat.substring(0,dateFormat.length-1);
                    if(component.get("v.dateFormat").indexOf('.') !== -1){
                        mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                       mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                        mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }
                    //mystring = (new Date(endD).getMonth()+1) + '/' + new Date(endD).getDate() + '/' + new Date(endD).getFullYear();
                }
                else
                {
                    // if(d.startDate < endD.getTime())
                    //     mystring = '\u2192';
                    // else
                        mystring = '';
                }   
            }   
            else
            {
                if((Math.min(d.endDate,endD.getTime()) - Math.max(d.startDate,startD.getTime())) > ( 60 * 1000 * 60 * 60 * 24))
                {
                    console.log('---- kkkkkkkkkkkkkkkkk');
                     var dateFormat = component.get("v.dateFormat");
                    dateFormat = dateFormat.replace(/y+|Y+/, '');
                    dateFormat = dateFormat.substring(0,dateFormat.length-1);
                    var dt = new Date(d.endDate);
                    var offsetInMinutes = dt.getTimezoneOffset() ;
                    dt.setSeconds(dt.getSeconds() + (dt.getTimezoneOffset()*60));
                    if(component.get("v.dateFormat").indexOf('.') !== -1){
                        mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }else if(component.get("v.dateFormat").indexOf('/') !== -1){
                       mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }else if(component.get("v.dateFormat").indexOf('-') !== -1){
                        mystring = dateFormat.replace(/m+|M+/,(dt.getMonth()+1)).replace(/d+|D+/,dt.getDate())
                    }
                    // mystring = (new Date(d.endDate).getMonth()+1) + '/' + new Date(d.endDate).getDate()+ '/' + new Date(d.endDate).getFullYear();
                }
                else
                {
                    mystring = '';
                }
            }       
            return mystring; 
        })
        .on('click',function(d,i){
            console.log('3333333333 on popup click ------');
            component.openPopover(i);
        })
        .on('mouseover',function(d,i){
            $j('#id_' + i).addClass('highlightbar');
            $j('#endid_' + i).css("cursor","pointer");
        })
        .on("mouseout", function(d,i){
            //$j('#id_' + i).removeClass('highlightbar');
        })
        .append("tspan")
        .style("font-size","10px")
        .text(function(d)
        {
            if(d.endDate > endD.getTime() && d.startDate < endD.getTime())
            {   
                return ' \u2192';
            }
        });

        var y2values=height,y2values1;
        var today = new Date().getTime();

        svg.append("line")
        .attr("x1", x(today))  //<<== change your code here
        .attr("y1", 0)
        .attr("x2", x(today))  //<<== and here
        .attr("y2", y2values + 20)
        .style("stroke-width", 1)
        .style("stroke", "#D9645E")
        .style("fill", "none");

    }
})