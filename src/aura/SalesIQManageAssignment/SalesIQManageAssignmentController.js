({
    employeeEdit : function(component,event,helper)
    {
        var recordId = component.get("v.recordId");
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId
            });
        navEvt.fire();
    },

    showSpinner : function (component, event, helper) 
    {
        var spinner_container = component.find('spinner_container');
        $A.util.removeClass(spinner_container, "slds-hide"); 
    },

    hideSpinner : function (component, event, helper) 
    {
        var spinner_container =component.find('spinner_container');
        $A.util.addClass(spinner_container, "slds-hide");        
    },

    getNamespace : function(component,event,helper)
    {
        var action = component.get("c.getOrgNamespace");

        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var ns = response.getReturnValue();
                component.set("v.namespace",ns);
            }    
        });


        action.setBackground();
        $A.enqueueAction(action);   


        var colorAction = component.get("c.getAssignmentColors");
        colorAction.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue() != null)
            {
                var colorMap = response.getReturnValue();
                console.log('colormap ',colorMap);
                component.set("v.colorMap",colorMap);
            }    
        });

        $A.enqueueAction(colorAction); 

        var dateRuleAction = component.get("c.getDateRuleSetting");
        dateRuleAction.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                console.log('-- date rule :'+response.getReturnValue())
                component.set("v.isEnabledDateRule",response.getReturnValue());
            }    
        });

        $A.enqueueAction(dateRuleAction);   

    },

    showChart : function(component, event, helper) 
    {   
        // component.showSpin();
        component.set("v.displayHistoryGrid", false);
        document.getElementById('timelineSlider').classList.remove("slds-hide");
        document.getElementById('timeline').classList.remove("slds-hide");
        $A.util.addClass(component.find('chartIcon'),'slds-is-selected');
        $A.util.removeClass(component.find('tableIcon'),'slds-is-selected');

        // helper.createTimelineData(component,event);

        if(component.get("v.ObjectDetailWrapperList").length == 0)
            $A.util.removeClass(component.find("no_assignment_text"),"slds-hide");
        else
            $A.util.addClass(component.find("no_assignment_text"),"slds-hide");
        var ObjectDetailWrapperList = component.get("v.ObjectDetailWrapperList");
        console.log('ObjectDetailWrapperList line 88:',ObjectDetailWrapperList);
        var empName = component.get("v.assigneeName");
        var charts = document.getElementsByClassName("chart_"+empName);
        var myString = "chart_" + empName;
        var charts = document.getElementsByClassName(myString);
        if(charts.length > 0)
        {
            for(var i = 0;i<charts.length;i++)
            {
                if(!$A.util.isUndefinedOrNull( charts[i] ))
                {
                    charts[i].parentNode.removeChild(charts[i]);
                }
            }
        }

        helper.createTimelineData(component,event);
        var $j = jQuery.noConflict();
        for(var i =0;i<ObjectDetailWrapperList.length;i++)
        {
            if(ObjectDetailWrapperList[i].isRowValidationFailed == true)
            {
                //$j(".bar-assignment").eq(i).addClass('errorBar');
                if(document.getElementsByClassName("bar-assignment")[i] != undefined)
                    document.getElementsByClassName("bar-assignment")[i].classList.add('errorBar');
            }
            else
            {
                if(document.getElementsByClassName("bar-assignment")[i] != undefined)
                    document.getElementsByClassName("bar-assignment")[i].classList.remove('errorBar');
            
            }
            /*else
            {
                $j(".bar-assignment").eq(i).removeClass('errorBar');
            }*/
        }

        
    },

    showTable : function(component, event, helper) {

        if(component.get("v.moduleName") == 'Employee Universe')
            component.set("v.referencedObject", 'Employee'); 
        else if(component.get("v.moduleName") == 'Position Universe')
            component.set("v.referencedObject", 'Position');

        document.getElementById('timelineSlider').classList.add("slds-hide");
        document.getElementById('timeline').classList.add("slds-hide");
        $A.util.addClass(component.find('tableIcon'),'slds-is-selected');
        $A.util.removeClass(component.find('chartIcon'),'slds-is-selected');
        $A.util.addClass(component.find("no_assignment_text"),"slds-hide");
        component.set("v.displayHistoryGrid", true);

        console.log('---- object');
        console.log(component.get("v.referencedObject"));
    },

    showModal: function(component, event, helper) {

        component.set("v.modalStyle", ".forceStyle .viewport .oneHeader {z-index:0 !important;  } .uiDatePicker {top: inherit !important; bottom : 0px !important}");

        component.getNamespace();
        var recordId = component.get("v.recordId");
        var details_card = component.find("details_card");
        details_card.showDetailCard();

        helper.getLocaleDateFormats(component,event);
        helper.getAssignments(component,event);
        helper.getPendingChangeRequests(component,event);
        var assignmentList = component.get("v.assignmentList");
        
        var targetCmp = component.find('comfirmationPopUp');
        var body = targetCmp.get("v.body");
            
        helper.toggleClass(component,'backdrop','slds-backdrop--');
        helper.toggleClass(component,'modaldialog','slds-fade-in-');
    },

    openPopover : function(component,event) {

        component.set("v.changesPerformed",true);
        // var $j = jQuery.noConflict();
        console.log('inside openPopover ---');
        var params = event.getParam('arguments');
        var idx ;
        if (params) 
            idx = params.idx;

        console.log('index -- '+idx);
        var detailPopover = component.find('detailPopover');
        detailPopover.showPopup(idx);

        var yesBtn = document.getElementById("yesBtn");
        yesBtn.disabled = false;
        var yesButton = component.find("yesButton");
        var noButton = component.find("noButton");
        var assignButton = component.find("assignButton");
        var addButton = component.find("submitButton");
        var resetButton = component.find("resetButton");
        var cancelButton = component.find("cancelButton");
        var successtoast = component.find("validation_success_popover");
        $A.util.addClass(yesButton,'slds-hide');
        $A.util.addClass(noButton,'slds-hide');
        $A.util.removeClass(addButton,'slds-hide');
        $A.util.removeClass(assignButton,'slds-hide');
        $A.util.removeClass(resetButton,'slds-hide');
        $A.util.removeClass(cancelButton,'slds-hide');
        $A.util.addClass(successtoast,'slds-hide');
        var toast = component.find("validation_failed_popover");    
        $A.util.addClass(toast,'slds-hide');
    },


    addAssignment : function(component,event,helper)
    {
        component.showSpin();
        if(document.getElementById('timeline').classList.contains("slds-hide"))
            component.set("v.displayHistoryGrid", false);
        var selectedCheckboxList = event.getParam("selectedCheckboxList");
        var assigneeId = component.get("v.recordId");
        var defAssignmentType = component.get("v.assignmentType"); //added by gurpreet
        console.log('assignmentType gp ');
        console.log(defAssignmentType);
        var ObjectDetailWrapperList = component.get("v.ObjectDetailWrapperList");
        var ns = component.get("v.namespace")
        var moduleName = component.get("v.moduleName");

        var action = component.get("c.addAssignments");

        var yesButton = component.find("yesButton");
        var noButton = component.find("noButton");
        var addButton = component.find("submitButton");
        var resetButton = component.find("resetButton");
        var cancelButton = component.find("cancelButton");
        var successtoast = component.find("validation_success_popover");
        $A.util.addClass(yesButton,'slds-hide');
        $A.util.addClass(noButton,'slds-hide');
        $A.util.removeClass(addButton,'slds-hide');
        $A.util.removeClass(resetButton,'slds-hide');
        $A.util.removeClass(cancelButton,'slds-hide');
        $A.util.addClass(successtoast,'slds-hide');

        var toast = component.find("validation_failed_popover");    
        $A.util.addClass(toast,'slds-hide');

        var changesPerformed = component.get("v.changesPerformed");
            if(changesPerformed)
            {
                document.getElementById('submitButton').disabled = false;
                document.getElementById('resetButton').disabled = false;
                var addButton = component.find("submitButton");
                var resetButton = component.find("resetButton");
                $A.util.removeClass(addButton,'hideSubmit');
                $A.util.removeClass(resetButton,'hideReset');
            }

        action.setParams({
            selectedIds : selectedCheckboxList,
            recordId : assigneeId,
            moduleName : moduleName,
            defaultAssignmentType : defAssignmentType,
        });
       
        action.setCallback(this,function(response)
        {
            
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var retResponse = response.getReturnValue();
                if(document.getElementById('timeline').classList.contains("slds-hide"))
                    component.set("v.displayHistoryGrid", true);
                
                for(var i = 0;i<retResponse.length;i++)
                {
                    ObjectDetailWrapperList.push(retResponse[i]);
                }
                if(ObjectDetailWrapperList.length > 1)
                {
                    ObjectDetailWrapperList.sort(function(a, b) 
                    {
                        if ( a.record[ns + 'Assignment_Type__c'] < b.record[ns + 'Assignment_Type__c'] )
                          return -1;
                        if ( a.record[ns + 'Assignment_Type__c'] > b.record[ns + 'Assignment_Type__c'] )
                          return 1;
                        return 0;
                    });
                }
                
                component.set("v.ObjectDetailWrapperList",ObjectDetailWrapperList);

               
                if(!document.getElementById('timeline').classList.contains("slds-hide"))
                {
                    var no_assignment_text = component.find("no_assignment_text");
                    $A.util.addClass(no_assignment_text,"slds-hide");

                    var empName = component.get("v.assigneeName");
                    var charts = document.getElementsByClassName("chart_"+empName);
                    var myString = "chart_" + empName;
                    var charts = document.getElementsByClassName(myString);
                    if(charts.length > 0)
                    {
                        for(var i = 0;i<charts.length;i++)
                        {
                            if(!$A.util.isUndefinedOrNull( charts[i] ))
                            {
                                charts[i].parentNode.removeChild(charts[i]);
                            }
                        }
                    }

                    helper.createTimelineData(component,event);
                    var $j = jQuery.noConflict();
                    for(var i =0;i<ObjectDetailWrapperList.length;i++)
                    {
                        if(ObjectDetailWrapperList[i].isRowValidationFailed == true)
                        {
                            //$j(".bar-assignment").eq(i).addClass('errorBar');
                            if(document.getElementsByClassName("bar-assignment")[i] != undefined)
                                document.getElementsByClassName("bar-assignment")[i].classList.add('errorBar');
                        }
                        else
                        {
                            if(document.getElementsByClassName("bar-assignment")[i] != undefined)
                                document.getElementsByClassName("bar-assignment")[i].classList.remove('errorBar');
                        
                        }
                    }
                }
                
                    
                component.hideSpin();
                
            }
            else if (state === "ERROR" || response.getReturnValue == null) 
            {
                var errorLabel;
                var ns = component.get("v.namespace");
                if(ns != '')
                {
                    errorLabel = $A.get("$Label.AxtriaSalesIQTM.Unexpected_error_notification");
                }
                else
                {
                    errorLabel = $A.get("$Label.c.Unexpected_error_notification");
                }
                
                var errors = response.getError();
                var errorMessage = errors[0].message;
                if (errors[0] && errors[0].message) 
                {
                    var action = component.get("c.logException");
        
                    action.setParams({
                        message : errorMessage,
                        module : component.get("v.moduleName")
                    });

                    action.setCallback(this,function(response)
                    {
                        var state = response.getState();
                        if(state === 'SUCCESS')
                        {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                mode: 'dismissible',
                                message: errorLabel,
                                type : 'error'
                                
                            });
                            toastEvent.fire();
                        }
                    });
                    $A.enqueueAction(action);
                } 
                else 
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: errorLabel,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                }
            } 
        });

        $A.enqueueAction(action); 
                
    },

    timelineSlider : function(component,event,helper)
    {
        component.showSpin();
        // var spinner_container = component.find('spinner_container');
        // $A.util.removeClass(spinner_container, "slds-hide");
        // document.getElementsByClassName("ma_spinner_container")[0].classList.remove('slds-hide');
        console.log(event.currentTarget.id);
        // console.log(event.getSource().get("v.class"));
        console.log(event.target);

        var empName = component.get("v.assigneeName");
        var charts = document.getElementsByClassName("chart_"+empName);
        var myString = "chart_" + empName;
        var charts = document.getElementsByClassName(myString);
        var startD = new Date(component.get("v.startDate"));
        var endD = new Date(component.get("v.endDate"));
        if(event.currentTarget.id.indexOf('left') != -1)
        {
            startD.setMonth(startD.getMonth()-12);
            // startD.setDate(1);
            endD.setMonth(endD.getMonth() - 12);
            // endD.setDate(0);
        }
        else if(event.currentTarget.id.indexOf('right') != -1)
        {
            startD.setMonth(startD.getMonth() + 12);
            // startD.setDate(1);
            endD.setMonth(endD.getMonth() + 12);
            // endD.setDate(0);
        }
        else
        {
            startD = new Date(); 
            startD.setMonth(startD.getMonth()-6);
            startD.setDate(1);
            endD = new Date(); 
            endD.setMonth(endD.getMonth()+6);
            endD.setDate(0);
        }
        
        component.set("v.startDate",startD);
        component.set("v.endDate",endD);
        var ObjectDetailWrapperList = component.get("v.ObjectDetailWrapperList");
        // var warningicon = component.find('validation_failed_icon');
        // var toast = component.find("validation_failed_popover");
        // $A.util.addClass(warningicon,'slds-hide');
        // $A.util.addClass(toast,'slds-hide');
        // component.set("v.changesPerformed",false);

        if(charts.length > 0)
        {
            for(var i = 0;i<charts.length;i++)
            {
                if(!$A.util.isUndefinedOrNull( charts[i] ))
                {
                    charts[i].parentNode.removeChild(charts[i]);
                }
            }
        }

        if(ObjectDetailWrapperList.length == 0)
        {
            var no_assignment_text = component.find("no_assignment_text");
            $A.util.removeClass(no_assignment_text,"slds-hide");
            var assignmentList = [];
            var startD = new Date(component.get("v.startDate"));
            var endD = new Date(component.get("v.endDate"));
            assignmentList.push({position : 'Unassigned' , startDate : startD.getTime(), endDate : endD.getTime() , assignmentType : 'Unassigned' , clientPositionCode : '' , status : 'Unassigned'});
            component.set("v.errorList",[]);
            var toast = component.find("validation_failed_popover");
            var warningIcon = component.find('validation_failed_icon');
            $A.util.addClass(toast,'slds-hide');
            $A.util.addClass(warningIcon,'slds-hide');
             document.getElementById('submitButton').disabled = false;
            helper.drawTimeline(component,event,assignmentList,[],[]);
        }
        else
        {
            console.log('ObjectDetailWrapperList :',ObjectDetailWrapperList);
            helper.createTimelineData(component,event);
            var $j = jQuery.noConflict();
            for(var i =0;i<ObjectDetailWrapperList.length;i++)
            {
                if(ObjectDetailWrapperList[i].isRowValidationFailed == true)
                {
                    //$j(".bar-assignment").eq(i).addClass('errorBar');
                    if(document.getElementsByClassName("bar-assignment")[i] != undefined)
                        document.getElementsByClassName("bar-assignment")[i].classList.add('errorBar');
                }
                else
                {
                    if(document.getElementsByClassName("bar-assignment")[i] != undefined)
                        document.getElementsByClassName("bar-assignment")[i].classList.remove('errorBar');
                
                }
            }
        }
        component.hideSpin();
       // document.getElementsByClassName('ma_spinner_container')[0].classList.add('slds-hide'); 

    },

    // timelineRightSlider : function(component,event,helper)
    // {
    //     console.log(event.target.id);
    //     console.log(event.target);
    //     var empName = component.get("v.assigneeName");
    //     var charts = document.getElementsByClassName("chart_"+empName);
    //     var myString = "chart_" + empName;
    //     var charts = document.getElementsByClassName(myString);
    //     var startD = new Date(component.get("v.startDate"));
    //     var endD = new Date(component.get("v.endDate"));
    //     startD.setMonth(startD.getMonth()+12);
    //     startD.setDate(1);
    //     endD.setMonth(endD.getMonth()+12);
    //     endD.setDate(1);
    //     component.set("v.startDate",startD);
    //     component.set("v.endDate",endD);
    //     var ObjectDetailWrapperList = component.get("v.ObjectDetailWrapperList");
    //     // var warningicon = component.find('validation_failed_icon');
    //     // var toast = component.find("validation_failed_popover");
    //     // $A.util.addClass(warningicon,'slds-hide');
    //     // $A.util.addClass(toast,'slds-hide');
    //     // component.set("v.changesPerformed",false);

    //     if(charts.length > 0)
    //     {
    //         for(var i = 0;i<charts.length;i++)
    //         {
    //             if(!$A.util.isUndefinedOrNull( charts[i] ))
    //             {
    //                 charts[i].parentNode.removeChild(charts[i]);
    //             }
    //         }
    //     }

    //     if(ObjectDetailWrapperList.length == 0)
    //     {
    //         var no_assignment_text = component.find("no_assignment_text");
    //         $A.util.removeClass(no_assignment_text,"slds-hide");
    //         var assignmentList = [];
    //         var startD = new Date(component.get("v.startDate"));
    //         var endD = new Date(component.get("v.endDate"));
    //         assignmentList.push({position : 'Unassigned' , startDate : startD.getTime(), endDate : endD.getTime() , assignmentType : 'Unassigned' , status : 'Unassigned'});
    //         component.set("v.errorList",[]);
    //         $A.util.addClass(toast,'slds-hide');
    //         $A.util.addClass(warningIcon,'slds-hide');
    //          document.getElementById('submitButton').disabled = false;
    //         helper.drawTimeline(component,event,assignmentList,[],[]);
    //     }
    //     else
    //     {
    //         helper.createTimelineData(component,event);
    //         var $j = jQuery.noConflict();
    //         for(var i =0;i<ObjectDetailWrapperList.length;i++)
    //         {
    //             if(ObjectDetailWrapperList[i].isRowValidationFailed == true)
    //             {
    //                 $j(".bar-assignment").eq(i).addClass('errorBar');
    //             }
    //             else
    //             {
    //                 $j(".bar-assignment").eq(i).removeClass('errorBar');
    //             }
    //         }
    //     }

    // },

    removeAssignment : function(component,event,helper)
    {
        var idx = event.getParam("idx");
        var emp_Id = component.get("v.recordId");
        var ObjectDetailWrapperList = component.get("v.ObjectDetailWrapperList");

        var toast = component.find("validation_failed_popover");    
        $A.util.addClass(toast,'slds-hide');
        var warningIcon = component.find('validation_failed_icon');
        var toast = component.find("validation_failed_popover");
        ObjectDetailWrapperList.splice(idx, 1);

        var ns = component.get("v.namespace");

        var changesPerformed = component.get("v.changesPerformed");

        console.log('changesPerformed' + changesPerformed);
        console.log('ObjectDetailWrapperList.length' + ObjectDetailWrapperList.length);
        console.log('----AAA--' , ObjectDetailWrapperList);
        var newLength =0;
        for(var i=0;i<ObjectDetailWrapperList.length ;i++){
            if(ObjectDetailWrapperList[i]['isNew']==true){
                        newLength ++;
            }
        }
          
            if(newLength < 1)
            {   
                
                if(changesPerformed)
                {
                    document.getElementById('submitButton').disabled = true;
                    document.getElementById('resetButton').disabled = true;
                    var addButton = component.find("submitButton");
                    var resetButton = component.find("resetButton");
                    $A.util.addClass(addButton,'hideSubmit');
                    $A.util.addClass(resetButton,'hideReset');
                }
            }
            

        if(ObjectDetailWrapperList.length > 1)
        {
            ObjectDetailWrapperList.sort(function(a, b) 
            {
                if ( a.record[ns + 'Assignment_Type__c'] < b.record[ns + 'Assignment_Type__c'] )
                  return -1;
                if ( a.record[ns + 'Assignment_Type__c'] > b.record[ns + 'Assignment_Type__c'] )
                  return 1;
                return 0;
            });
        }
        var hasError = false;
        var error_list = [];
        ObjectDetailWrapperList.forEach(function(item)
        {
            if(item.isRowValidationFailed)
            {
                hasError = true;
                item.recordDetails.forEach(function(fieldWrapper)
                {
                    // console.log(fieldWrapper.validationErrorMessage);
                    if(fieldWrapper.isEditable)
                    {
                        if(fieldWrapper.validationErrorMessage.length > 0)
                        {
                            for(var i =0;i<fieldWrapper.validationErrorMessage.length;i++)
                            {
                                error_list.push(fieldWrapper.validationErrorMessage[i]);
                            }
                        }
                    }
                    
                });
                
            }
        });
        component.set("v.errorList",error_list);
        if(!hasError)
        {
            component.set("v.errorList",[]);
            $A.util.addClass(toast,'slds-hide');
            $A.util.addClass(warningIcon,'slds-hide');
            document.getElementById('submitButton').disabled = false;
        }

        component.set("v.ObjectDetailWrapperList",ObjectDetailWrapperList);

        
        
        var empName = component.get("v.assigneeName");
        var charts = document.getElementsByClassName("chart_"+empName);
        var myString = "chart_" + empName;
        var charts = document.getElementsByClassName(myString);
        if(charts.length > 0)
        {
            for(var i = 0;i<charts.length;i++)
            {
                if(!$A.util.isUndefinedOrNull( charts[i] ))
                {
                    charts[i].parentNode.removeChild(charts[i]);
                }
            }
        }
        if(ObjectDetailWrapperList.length == 0)
        {
            var no_assignment_text = component.find("no_assignment_text");
            $A.util.removeClass(no_assignment_text,"slds-hide");
            var assignmentList = [];
            var startD = new Date(component.get("v.startDate"));
            var endD = new Date(component.get("v.endDate"));
            assignmentList.push({position : 'Unassigned' , startDate : startD.getTime(), endDate : endD.getTime() , assignmentType : 'Unassigned' , clientPositionCode : '' , status : 'Unassigned'});
            component.set("v.errorList",[]);
            $A.util.addClass(toast,'slds-hide');
            $A.util.addClass(warningIcon,'slds-hide');
             document.getElementById('submitButton').disabled = false;
            helper.drawTimeline(component,event,assignmentList,[],[]);
        }
        else
        {
            helper.createTimelineData(component,event);
            var $j = jQuery.noConflict();
            for(var i =0;i<ObjectDetailWrapperList.length;i++)
            {
                if(ObjectDetailWrapperList[i].isRowValidationFailed == true)
                {
                    //$j(".bar-assignment").eq(i).addClass('errorBar');
                    if(document.getElementsByClassName("bar-assignment")[i] != undefined)
                        document.getElementsByClassName("bar-assignment")[i].classList.add('errorBar');
                }
                else
                {
                    if(document.getElementsByClassName("bar-assignment")[i] != undefined)
                        document.getElementsByClassName("bar-assignment")[i].classList.remove('errorBar');
                
                }
            }
        }
    },

    assignpositionpopup:function(component)
    {
        component.set("v.changesPerformed",true);
        var evt = $A.get("e.c:onAssignPositionClick");
        evt.setParams({
            "recordId": component.get("v.recordId")
        });            
        evt.fire();
    },
       
    openConfirmationPopUp : function(component, event, helper) 
    {
        var namespace = component.get("v.namespace");
        var modalMessage;
        if(namespace != '')
        {
            modalMessage = $A.get("$Label.AxtriaSalesIQTM.Dicard_Changes_Popup_Message");
        }
        else
        {
            modalMessage = $A.get("$Label.c.Dicard_Changes_Popup_Message");
        }
        var changesPerformed = component.get("v.changesPerformed");
        var isCRPending = component.get("v.isCRPending");
        if(changesPerformed && !isCRPending)
        {
            component.set("v.modalMessage",modalMessage);
            var recordId = component.get("v.recordId");

            var targetCmp = component.find('comfirmationPopUp');
            var body = targetCmp.get("v.body");

            if(component.get("v.moduleName") == 'Employee Universe')
            {
                
                if(component.get("v.externalInvokeWorkbench") == 'true'){
                    // FOR WORKBENCH FLOW IN IMPLEMENTATION - CALLING EXTERNAL COMPONENT
                    var action = component.get("c.getExternalComponentDetail");
                    action.setCallback(this,function(response)
                    {
                        var state = response.getState();
                        if(state === 'SUCCESS' && response.getReturnValue() != '')
                        {
                            console.log(response.getReturnValue());
                            var evt = $A.get("e.force:navigateToComponent");
                            evt.setParams({
                                componentDef : response.getReturnValue().split(';')[0],
                                componentAttributes: {
                                    objectName: response.getReturnValue().split(';')[1],
                                    recordId : component.get("v.recordId"),
                                    isSuccess : component.get("v.isSuccess"),
                                    feeId : component.get("v.workbenchId"),
                                    crEmployeeFeedId : component.get("v.workbenchId"),
                                    crID : component.get("v.crID"),
                                    CardClicked : component.get("v.CardClicked"),
                                    CurrentCard : component.get("v.CurrentCard")
                                },
                                isredirect : true
                            });
                            evt.fire();
                             document.getElementsByTagName("body")[0].style.overflow = "visible";
                        }
                    });
                    $A.enqueueAction(action); 

                }else{
                    // FOR EMPLOYEE UNIVERSE FLOW IN SALESIQ
                    $A.createComponent
                    (
                        "c:SalesIQMessageDialog",
                        {
                            "body": component.get("v.modalMessage"),
                            "recordId" : component.get("v.recordId"),
                            "onconfirm" : component.getReference("c.navigateToEmployeeUniverse"),
                        },
                        function(msgBox)
                        {                
                            if (component.isValid()) {
                                var targetCmp = component.find('comfirmationPopUp');
                                var body = targetCmp.get("v.body");
                                body.push(msgBox);
                                targetCmp.set("v.body", body);
                            }
                        }
                    );
                }
               /* $A.createComponent
                (
                    "c:SalesIQMessageDialog",
                    {
                        "body": component.get("v.modalMessage"),
                        "recordId" : component.get("v.recordId"),
                        "onconfirm" : component.getReference("c.navigateToEmployeeUniverse"),
                    },
                    function(msgBox)
                    {                
                        if (component.isValid()) {
                            var targetCmp = component.find('comfirmationPopUp');
                            var body = targetCmp.get("v.body");
                            body.push(msgBox);
                            targetCmp.set("v.body", body);
                        }
                    }
                );*/
            }
            else if(component.get("v.moduleName") == 'Position Universe')
            {
                $A.createComponent
                (
                    "c:SalesIQMessageDialog",
                    {
                        "body": component.get("v.modalMessage"),
                        "recordId" : component.get("v.recordId"),
                        "onconfirm" : component.getReference("c.navigateToPositionUniverse"),
                    },
                    function(msgBox)
                    {                
                        if (component.isValid()) {
                            var targetCmp = component.find('comfirmationPopUp');
                            var body = targetCmp.get("v.body");
                            body.push(msgBox);
                            targetCmp.set("v.body", body);
                        }
                    }
                );
            }
        }
        else {
            //component.set("v.isSuccess",'');
            helper.redirectToUniverse(component);
        }
    },


    onValuesSubmit : function(component,event,helper)
    {    
        console.log('##### checkBusinessRuleRunning called');
        //Check the business rule is running or not
        var checkBusinessRule = component.get("c.isBusinessRuleRunning");
        checkBusinessRule.setParams
        ({
            teamInstanceID : component.get("v.selectedTeamInstance"),
        });
        checkBusinessRule.setCallback(this,function(response){
            var state = response.getState();
            var result = response.getReturnValue();  
            console.log('#### result ' + result);        
            var namespace = component.get("v.namespace");
            if(state === 'SUCCESS' && result != null){
                if(result == true){
                    var toastEvent = $A.get("e.force:showToast");
                    var warningMessage;
                    if(namespace != ''){
                        warningMessage = $A.get("$Label.AxtriaSalesIQTM.Business_Rule_Running_Warning");
                    }
                    else
                    {
                        warningMessage = $A.get("$Label.c.Business_Rule_Running_Warning")
                    }
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: warningMessage,
                        type : 'Warning'                            
                    });
                    toastEvent.fire();
                   //Close popup and reset the properties
    
                }else{
                    // disable the submit button
                    var submitButton = event.target;
                    if(submitButton != undefined)
                        submitButton.disabled = true;   
            
                    if(!((event.keyCode == 13 && event.target.id == 'modaldialog') || event.target.id == 'submitButton'))
                    {
                        // enable the submit button
                        if(submitButton != undefined)
                            submitButton.disabled = false;
                        return;
                    }
                    var $j = jQuery.noConflict();
            
                    var ObjectDetailWrapperList = component.get("v.ObjectDetailWrapperList");
                    

                    if(ObjectDetailWrapperList.length < 1) 
                    {
                        // enable the submit button
                        var warningIcon = component.find('validation_failed_icon');
                        var toast = component.find("validation_failed_popover");
                        var errorList = [];
                        var noChangeLabel;
                        if(namespace != '')
                        {
                            noChangeLabel = $A.get("$Label.AxtriaSalesIQTM.ManageAssignment_No_Change");
                        }
                        else
                        {
                            noChangeLabel = $A.get("$Label.c.ManageAssignment_No_Change");
                        }
                        errorList.push(noChangeLabel);
                        component.set("v.errorList",errorList);
                        $A.util.removeClass(toast,'slds-hide');
                        $A.util.removeClass(warningIcon,'slds-hide');
            
                        if(submitButton != undefined)
                            submitButton.disabled = false;
            
                        return;
                    }
                   

            
                    ObjectDetailWrapperList.forEach(function(item)
                    {
                        item.recordDetails.forEach(function(fieldWrapper)
                        {
                            if(fieldWrapper.isEditable) 
                            {
                                if(fieldWrapper.fieldDataType == 'percent')
                                {
                                    if(fieldWrapper.fieldValue == null){
                                        fieldWrapper.fieldValue = '1.0000';                
                                    } 
                                }
                            }
                        });

                    });

                    var ObjectDetailValues = JSON.stringify(component.get("v.ObjectDetailWrapperList"));
                     console.log('ObjectDetailValues :');
                    console.log(ObjectDetailValues);
            
                    var action = component.get("c.checkValidations");
                     action.setParams({
                        recordDetailsJSON : ObjectDetailValues,
                        moduleName : component.get("v.moduleName")
                    });
            
            
                    action.setCallback(this,function(response)
                    {
                        
                        var state = response.getState();
                        if(state === 'SUCCESS' && response.getReturnValue != null)
                        {
                            var retResponse = response.getReturnValue();
                            var error_list = [];
                            if(retResponse.length > 0)
                                error_list = retResponse.filter(function(item, i, ar){ return ar.indexOf(item) === i; });
                            component.set("v.errorList", null);
            
                            var warningIcon = component.find('validation_failed_icon');
                            var toast = component.find("validation_failed_popover");
                            var successtoast = component.find("validation_success_popover");
                            var yesButton = component.find("yesButton");
                            var noButton = component.find("noButton");
                            var addButton = component.find("submitButton");
                            var resetButton = component.find("resetButton");
                            var cancelButton = component.find("cancelButton");
                            var assignButton = component.find("assignButton");
            
                            var ulErrors = component.find('listoferrors');
            
            
                            if(error_list.length > 2 && error_list[error_list.length - 2].toLowerCase() == 'error')
                            {
                                var ObjectDetailWrapperList = JSON.parse(error_list[error_list.length - 1]);
                                for(var i =0;i<ObjectDetailWrapperList.length;i++)
                                {
                                    if(ObjectDetailWrapperList[i].isRowValidationFailed == true)
                                    {
                                        //$j(".bar-assignment").eq(i).addClass('errorBar');
                                        if(document.getElementsByClassName("bar-assignment")[i] != undefined)
                                            document.getElementsByClassName("bar-assignment")[i].classList.add('errorBar');
                                    }
                                    else
                                    {
                                        if(document.getElementsByClassName("bar-assignment")[i] != undefined)
                                            document.getElementsByClassName("bar-assignment")[i].classList.remove('errorBar');
                                    
                                    }
                                }
                                console.log(error_list[error_list.length - 2]);
                                error_list.splice(error_list.length - 2,2);
                                component.set("v.ObjectDetailWrapperList",ObjectDetailWrapperList);
                                component.set("v.errorList",error_list);
                                $A.util.removeClass(toast,'slds-hide');
                                $A.util.removeClass(warningIcon,'slds-hide');
                            }
                            else
                            { 

                                var ObjectDetailWrapperList = JSON.parse(error_list[error_list.length - 1]);
                                component.set("v.ObjectDetailWrapperList",ObjectDetailWrapperList);
                                for(var i =0;i<ObjectDetailWrapperList.length;i++)
                                {
                                    if(ObjectDetailWrapperList[i].isRowValidationFailed == true)
                                    {
                                        //$j(".bar-assignment").eq(i).addClass('errorBar');
                                        if(document.getElementsByClassName("bar-assignment")[i] != undefined)
                                            document.getElementsByClassName("bar-assignment")[i].classList.add('errorBar');
                                    }
                                    else
                                    {
                                        if(document.getElementsByClassName("bar-assignment")[i] != undefined)
                                            document.getElementsByClassName("bar-assignment")[i].classList.remove('errorBar');
                                    
                                    }
                                }

                                if(error_list[error_list.length - 2].toLowerCase() == 'warning'){
                                    error_list.splice(error_list.length - 2,2);
                                    component.set("v.errorList",error_list);
                                }
            
                                $A.util.removeClass(successtoast,'slds-hide');
                                $A.util.addClass(toast,'slds-hide');
                                $A.util.addClass(warningIcon,'slds-hide');
                                $A.util.removeClass(yesButton,'slds-hide');
                                $A.util.removeClass(noButton,'slds-hide');
                                $A.util.addClass(addButton,'slds-hide');
                                $A.util.addClass(resetButton,'slds-hide');
                                $A.util.addClass(cancelButton,'slds-hide');
                                $A.util.addClass(assignButton,'slds-hide');
                                
                            }
                        }    
                        else if (state === "ERROR" || response.getReturnValue == null) 
                        {
                            var errorLabel;
                            var namespace = component.get("v.namespace");
                            if(namespace != '')
                            {
                                errorLabel = $A.get("$Label.AxtriaSalesIQTM.Unexpected_error_notification");
                            }
                            else
                            {
                                errorLabel = $A.get("$Label.c.Unexpected_error_notification");
                            }
                            
                            var errors = response.getError();
                            var errorMessage = errors[0].message;
                            if (errors[0] && errors[0].message) 
                            {
                                var action = component.get("c.logException");
                                action.setParams({
                                    message : errorMessage,
                                    module : component.get("v.moduleName")
                                });
            
                                action.setCallback(this,function(response)
                                {
                                    var state = response.getState();
                                    if(state === 'SUCCESS')
                                    {
                                        var toastEvent = $A.get("e.force:showToast");
                                        // Removed generic message for the case of "Submit button" --> message: errorLabel
                                        toastEvent.setParams({
                                            mode: 'dismissible',
                                            message: errorMessage,
                                            type : 'error'
                                        });
                                        toastEvent.fire();
                                    }
                                });
                                $A.enqueueAction(action);
                            } 
                            else 
                            {
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    mode: 'dismissible',
                                    message: errorLabel,
                                    type : 'error'
                                    
                                });
                                toastEvent.fire();
                            }
                        } 
            
                        // enable the submit button
                        if(submitButton != undefined)
                            submitButton.disabled = false;
                    });
            
                    $A.enqueueAction(action);
              }
            } 
               else if (state === "ERROR" || result == null) 
               {
                  helper.logException(component, response.getError());
               }
      });
        $A.enqueueAction(checkBusinessRule);
    },

    onValuesReset : function(component,event,helper)
    {
        var empName = component.get("v.assigneeName");
        var charts = document.getElementsByClassName("chart_"+empName);
        var myString = "chart_" + empName;
        var charts = document.getElementsByClassName(myString);
        var warningicon = component.find('validation_failed_icon');
        var toast = component.find("validation_failed_popover");
        $A.util.addClass(warningicon,'slds-hide');
        $A.util.addClass(toast,'slds-hide');
        component.set("v.changesPerformed",false);

        var changesPerformed = component.get("v.changesPerformed");
            if(!changesPerformed)
            {
                document.getElementById('submitButton').disabled = true;
                document.getElementById('resetButton').disabled = true;
                var addButton = component.find("submitButton");
                $A.util.addClass(addButton,'hideSubmit'); 
                var resetButton = component.find("resetButton");
                $A.util.addClass(resetButton,'hideReset');   
            }

        if(charts.length > 0)
        {
            for(var i = 0;i<charts.length;i++)
            {
                if(!$A.util.isUndefinedOrNull( charts[i] ))
                {
                    charts[i].parentNode.removeChild(charts[i]);
                }
            }
        }
        
        var resetButton = event.target;
        if(resetButton != undefined)
            resetButton.disabled = true;
        
        component.set("v.ObjectDetailWrapperList",[]);
        if(document.getElementById('timeline').classList.contains("slds-hide"))
            component.set("v.displayHistoryGrid", false);
        helper.getAssignments(component,event);
    },

    handlePopoverChange : function(component,event,helper)
    {
        var ns = component.get("v.namespace");
        var positionEmployeeWrapperList = component.get("v.ObjectDetailWrapperList") ;
        var error_list = [];
        var anyRowValidationFailed = false;
        positionEmployeeWrapperList.forEach(function(item)
        {
            var anyValidationFailed = false;
            var isDateValid = false;
            item.recordDetails.forEach(function(fieldWrapper)
            {
                if(fieldWrapper.isEditable) 
                {
                    if(fieldWrapper.fieldDataType == 'Date')
                    {
                        if(fieldWrapper.isRequired)
                        {
                            if(fieldWrapper.fieldValue == undefined || fieldWrapper.fieldValue == '') 
                            {
                                fieldWrapper.isValidationFailed = true;
                                var errorMessage;
                                if(ns != '')
                                {
                                    errorMessage = $A.get("$Label.AxtriaSalesIQTM.PositionFieldBlankPrompt");
                                }
                                else
                                {
                                    errorMessage = $A.get("$Label.c.PositionFieldBlankPrompt");
                                }
                                var reg = new RegExp("\\{0\\}", "gm");
                                errorMessage = errorMessage.replace(reg, fieldWrapper.fieldLabel);
                                fieldWrapper.validationErrorMessage = [errorMessage];
                                error_list.push(errorMessage);
                                anyValidationFailed = true ;
                                item.isRowValidationFailed = true;
                                return;
                            }
                        }

                        
                        //console.log('fieldWrapper.fieldValue' + (fieldWrapper.fieldValue));*/

                        if(fieldWrapper.fieldValue != undefined)
                        {
                             var dateValues = fieldWrapper.fieldValue.split('-');
                             if(dateValues != undefined && dateValues.length > 2)
                             {

                                var dateVal = new Date(dateValues[0], dateValues[1] - 1 , dateValues[2]);
                                console.log('dateVal'+dateVal);
                                if(dateVal != undefined && dateVal != null && dateVal != '')
                                {
                                    isDateValid = true;
                                }
                            }
                        }else
                        {
                            isDateValid = false;
                        }

                        console.log('--R1--' + (!isNaN(Date.parse(fieldWrapper.fieldValue))));
                        console.log('--R2--' + (fieldWrapper.fieldValue != parseFloat(fieldWrapper.fieldValue)));
                        console.log('--R3--' + (!isNaN(parseFloat(fieldWrapper.fieldValue))));
                        console.log('--R4--' + isDateValid);

                        if(!(!isNaN(Date.parse(fieldWrapper.fieldValue)) && fieldWrapper.fieldValue != parseFloat(fieldWrapper.fieldValue) && !isNaN(parseFloat(fieldWrapper.fieldValue)) || isDateValid ))  
                        {
                            console.log('line 1142');
                            fieldWrapper.isValidationFailed = true;
                            var errorMessage;
                            if(ns != '')
                            {
                                errorMessage = $A.get("$Label.AxtriaSalesIQTM.Invalid_Field_Value");
                            }
                            else
                            {
                                errorMessage = $A.get("$Label.c.Invalid_Field_Value") ;
                            }
                            fieldWrapper.validationErrorMessage = [errorMessage];  
                            error_list.push(errorMessage); 
                        }
                        else
                        {   
                            console.log('line 1158');
                            var errorMessage;
                            if(ns != '')
                            {
                                errorMessage = $A.get("$Label.AxtriaSalesIQTM.EffectiveStartDateLessThanEnd");
                            }
                            else
                            {
                                errorMessage = $A.get("$Label.c.EffectiveStartDateLessThanEnd") ;
                            }
                            if(fieldWrapper.fieldApiName == ns+'Effective_End_Date__c')
                            {
                                console.log('11- '+item.record[ns+'Effective_Start_Date__c']);
                                console.log('22 - '+item.record[ns+'Effective_End_Date__c']);
                                
                                //var tempEndDate = new Date(fieldWrapper.fieldValue);;
                                var tempEndDate;
                                var tempStartDate;
                                if(item.record[ns+'Effective_End_Date__c'].indexOf('-') != -1){
                                    var arr1 = item.record[ns+'Effective_End_Date__c'].split('-');
                                    tempEndDate = new Date(arr1[0], arr1[1] - 1, arr1[2]);
                                    console.log('tempEndDate :'+tempEndDate);
                                }else{
                                   tempEndDate = new Date(item.record[ns+'Effective_End_Date__c']); 
                                }

                                if(item.record[ns+'Effective_Start_Date__c'].indexOf('-') != -1){
                                    var arr2 = item.record[ns+'Effective_Start_Date__c'].split('-');
                                    tempStartDate = new Date(arr2[0], arr2[1] - 1, arr2[2]);
                                    console.log('tempStartDate :'+tempStartDate);
                                }else{
                                    tempStartDate = new Date(item.record[ns+'Effective_End_Date__c']);
                                }

                               
                                if(tempStartDate != null && tempStartDate > tempEndDate)
                                {
                                    
                                    console.log('--inside line 1091--');
                                    /*var sd = new Date(item.record[ns+'Effective_Start_Date__c']);
                                    var ed = new Date(fieldWrapper.fieldValue);

                                    var offsetInMinutes = sd.getTimezoneOffset() ;
                                    sd.setSeconds(sd.getSeconds() + (sd.getTimezoneOffset()*60));
                                    ed.setSeconds(ed.getSeconds() + (ed.getTimezoneOffset()*60));*/

                                    var dateFormat = component.get("v.dateFormat");
                                    var foramattedSD, foramattedED ;

                                    console.log('dateFormat :'+dateFormat);
                                    //var stDate = new Date(item.record[ns+'Effective_Start_Date__c']);
                                    //var offsetInMinutes = tempStartDate.getTimezoneOffset() ;
                                    //tempStartDate.setSeconds(tempStartDate.getSeconds() + (tempStartDate.getTimezoneOffset()*60));

                                    //var endDate = new Date(fieldWrapper.fieldValue);
                                    //tempEndDate.setSeconds(tempEndDate.getSeconds() + (tempEndDate.getTimezoneOffset()*60));

                                    // @todo: only limited formats are supported, for rest of the formats else will get executed
                                    if(dateFormat == 'M/d/yyyy') {
                                        /*foramattedSD = sd.getMonth()+1 + '/' + sd.getDate() + '/' + sd.getFullYear() ;
                                        foramattedED = ed.getMonth()+1 + '/' + ed.getDate() + '/' + ed.getFullYear() ;*/

                                        foramattedSD =  dateFormat.replace(/y+|Y+/,tempStartDate.getFullYear()).replace(/M+|m+/,(tempStartDate.getMonth()+1)).replace(/d+|D+/,tempStartDate.getDate() );
                                        foramattedED = dateFormat.replace(/y+|Y+/,tempEndDate.getFullYear()).replace(/M+|m+/,(tempEndDate.getMonth()+1)).replace(/d+|D+/,tempEndDate.getDate() );

                                    } else if(dateFormat == 'd/M/yyyy') {
                                        foramattedSD = sd.getDate() + '/' + sd.getMonth()+1 + '/' + sd.getFullYear() ;
                                        foramattedED = ed.getDate() + '/' + ed.getMonth()+1 + '/' + ed.getFullYear() ;
                                    
                                    } else if(dateFormat == 'dd/MM/yyyy') {
                                        var dateString = (sd.getDate() < 10) ? '0' + sd.getDate().toString() : sd.getDate().toString();
                                        var monthString = ((sd.getMonth()+1) < 10) ? '0' + (sd.getMonth()+1).toString() : (sd.getMonth()+1).toString();
                                        foramattedSD = dateString + '/' + monthString + '/' + sd.getFullYear() ;

                                        dateString = (ed.getDate() < 10) ? '0' + ed.getDate().toString() : ed.getDate().toString();
                                        monthString = ((ed.getMonth()+1) < 10) ? '0' + (ed.getMonth()+1).toString() : (ed.getMonth()+1).toString();
                                        foramattedED = dateString + '/' + monthString + '/' + ed.getFullYear() ;

                                    } else if(dateFormat == 'MM/dd/yyyy') {
                                        var dateString = (sd.getDate() < 10) ? '0' + sd.getDate().toString() : sd.getDate().toString();
                                        var monthString = (sd.getMonth() < 10) ? '0' + sd.getMonth().toString() : sd.getMonth().toString();
                                        foramattedSD = monthString + '/' + dateString + '/' + sd.getFullYear() ;

                                        dateString = (ed.getDate() < 10) ? '0' + ed.getDate().toString() : ed.getDate().toString();
                                        monthString = (ed.getMonth() < 10) ? '0' + ed.getMonth().toString() : ed.getMonth().toString();
                                        foramattedED = monthString + '/' + dateString + '/' + ed.getFullYear() ;

                                    } else {
                                        foramattedSD = sd.getFullYear() + '/' + sd.getMonth()+1 + '/' + sd.getDate() ;
                                        foramattedED = ed.getFullYear() + '/' + ed.getMonth()+1 + '/' + ed.getDate() ;
                                    }

                                    console.log('foramattedSD :'+foramattedSD);
                                    console.log('foramattedED :'+foramattedED);

                                    fieldWrapper.isValidationFailed = true;
                                    // start date
                                    errorMessage = errorMessage.replace(new RegExp("\\{0\\}", "gm"), foramattedSD);
                                    // end date
                                    errorMessage = errorMessage.replace(new RegExp("\\{1\\}", "gm"), foramattedED);
                                    fieldWrapper.validationErrorMessage = [errorMessage];
                                    error_list.push(errorMessage);
                                } 
                                else
                                {
                                    console.log('--inside line 1157--');
                                    item.record[fieldWrapper.fieldApiName] =  fieldWrapper.fieldValue ;
                                    fieldWrapper.isValidationFailed = false;
                                    fieldWrapper.validationErrorMessage = [];
                                }
                            }
                            else if(fieldWrapper.fieldApiName == ns+'Effective_Start_Date__c')
                            {
                                console.log('33- '+item.record[ns+'Effective_End_Date__c'] );
                                console.log('44 - '+fieldWrapper.fieldValue);
                                
                                var endDateTemp = new Date(item.record[ns+'Effective_End_Date__c']);
                                //var startDateTemp = new Date(fieldWrapper.fieldValue);
                                var startDateTemp = item.record[ns+'Effective_Start_Date__c'];
                                if(item.record[ns+'Effective_End_Date__c'] != null && startDateTemp > endDateTemp)
                                {
                                    console.log('--inside line 1169--');
                                    /*var ed = new Date(item.record[ns+'Effective_End_Date__c']);
                                    var sd = new Date(fieldWrapper.fieldValue);

                                    console.log('ed : ',ed);
                                    console.log('sd : ',sd);

                                    var offsetInMinutes = sd.getTimezoneOffset() ;
                                    sd.setSeconds(sd.getSeconds() + (sd.getTimezoneOffset()*60));
                                    ed.setSeconds(ed.getSeconds() + (ed.getTimezoneOffset()*60));
                                    */
                                    var dateFormat = component.get("v.dateFormat");
                                    var foramattedSD, foramattedED ;

                                    console.log('dateFormat :'+dateFormat);

                                    /*var stDate = new Date(item.record[ns+'Effective_Start_Date__c']);
                                    stDate.setSeconds(stDate.getSeconds() + (stDate.getTimezoneOffset()*60));
                                    console.log('stDate :'+stDate);
                                    
                                    var endDate = new Date(item.record[ns+'Effective_End_Date__c']);
                                    endDate.setSeconds(endDate.getSeconds() + (endDate.getTimezoneOffset()*60));
                                    console.log('endDate :'+endDate);*/

                                    // @todo: only limited formats are supported, for rest of the formats else will get executed
                                    if(dateFormat == 'M/d/yyyy') {
                                        //foramattedSD = stDate.getMonth()+1 + '/' + stDate.getDate() + '/' + stDate.getFullYear() ;
                                        //foramattedED = ed.getMonth()+1 + '/' + ed.getDate() + '/' + ed.getFullYear() ;
                                        foramattedSD =  dateFormat.replace(/y+|Y+/,startDateTemp.getFullYear()).replace(/M+|m+/,(startDateTemp.getMonth()+1)).replace(/d+|D+/,startDateTemp.getDate() );
                                        foramattedED = dateFormat.replace(/y+|Y+/,endDateTemp.getFullYear()).replace(/M+|m+/,(endDateTemp.getMonth()+1)).replace(/d+|D+/,endDateTemp.getDate() );

                                        console.log('foramattedSD:'+foramattedSD);
                                        console.log('foramattedED :'+foramattedED);


                                    } else if(dateFormat == 'd/M/yyyy') {
                                        /*foramattedSD = stDate.getDate() + '/' + stDate.getMonth()+1 + '/' + stDate.getFullYear() ;
                                        foramattedED = ed.getDate() + '/' + ed.getMonth()+1 + '/' + ed.getFullYear() ;*/

                                        foramattedSD =  dateFormat.replace(/y+|Y+/,startDateTemp.getFullYear()).replace(/M+|m+/,(startDateTemp.getMonth()+1)).replace(/d+|D+/,startDateTemp.getDate() );
                                        foramattedED = endDate.replace(/y+|Y+/,endDateTemp.getFullYear()).replace(/M+|m+/,(endDateTemp.getMonth()+1)).replace(/d+|D+/,endDateTemp.getDate() );
                                    
                                    } else if(dateFormat == 'dd/MM/yyyy') {
                                        var dateString = (startDateTemp.getDate() < 10) ? '0' + startDateTemp.getDate().toString() : startDateTemp.getDate().toString();
                                        var monthString = ((endDateTemp.getMonth()+1) < 10) ? '0' + (endDateTemp.getMonth()+1).toString() : (endDateTemp.getMonth()+1).toString();
                                        //foramattedSD = dateString + '/' + monthString + '/' + stDate.getFullYear() ;

                                        dateString = (ed.getDate() < 10) ? '0' + ed.getDate().toString() : ed.getDate().toString();
                                        monthString = ((ed.getMonth()+1) < 10) ? '0' + (ed.getMonth()+1).toString() : (ed.getMonth()+1).toString();
                                        //foramattedED = dateString + '/' + monthString + '/' + ed.getFullYear() ;

                                        foramattedSD =  dateFormat.replace(/y+|Y+/,stDate.getFullYear()).replace(/M+|m+/,monthString).replace(/d+|D+/,dateString);
                                        foramattedED = endDate.replace(/y+|Y+/,stDate.getFullYear()).replace(/M+|m+/,monthString).replace(/d+|D+/,dateString);

                                    } else if(dateFormat == 'MM/dd/yyyy') {
                                        var dateString = (stDate.getDate() < 10) ? '0' + stDate.getDate().toString() : stDate.getDate().toString();
                                        var monthString = (stDate.getMonth() < 10) ? '0' + stDate.getMonth().toString() : stDate.getMonth().toString();
                                        //foramattedSD = monthString + '/' + dateString + '/' + stDate.getFullYear() ;

                                        dateString = (ed.getDate() < 10) ? '0' + ed.getDate().toString() : ed.getDate().toString();
                                        monthString = (ed.getMonth() < 10) ? '0' + ed.getMonth().toString() : ed.getMonth().toString();
                                        //foramattedED = monthString + '/' + dateString + '/' + ed.getFullYear() ;

                                        foramattedSD =  dateFormat.replace(/y+|Y+/,stDate.getFullYear()).replace(/M+|m+/,monthString).replace(/d+|D+/,dateString);
                                        foramattedED = endDate.replace(/y+|Y+/,stDate.getFullYear()).replace(/M+|m+/,monthString).replace(/d+|D+/,dateString);

                                    } else {
                                        /*foramattedSD = stDate.getFullYear() + '/' + stDate.getMonth()+1 + '/' + stDate.getDate() ;
                                        foramattedED = ed.getFullYear() + '/' + ed.getMonth()+1 + '/' + ed.getDate() ;*/

                                        foramattedSD =  dateFormat.replace(/y+|Y+/,stDate.getFullYear()).replace(/M+|m+/,(stDate.getMonth()+1)).replace(/d+|D+/,stDate.getDate() );
                                        foramattedED = endDate.replace(/y+|Y+/,stDate.getFullYear()).replace(/M+|m+/,(endDate.getMonth()+1)).replace(/d+|D+/,endDate.getDate() );
                                    }

                                    
                                    console.log('foramattedSD ',foramattedSD);
                                    console.log('foramattedED ',foramattedED);

                                    fieldWrapper.isValidationFailed = true;
                                    // start date
                                    errorMessage = errorMessage.replace(new RegExp("\\{0\\}", "gm"), foramattedSD);
                                    // end date
                                    errorMessage = errorMessage.replace(new RegExp("\\{1\\}", "gm"), foramattedED);
                                    fieldWrapper.validationErrorMessage = [errorMessage];
                                    error_list.push(errorMessage);
                                }
                                else
                                {
                                    console.log('--inside line 1252--');
                                    item.record[fieldWrapper.fieldApiName] =  fieldWrapper.fieldValue ;
                                    fieldWrapper.isValidationFailed = false;
                                    fieldWrapper.validationErrorMessage = [];
                                }
                            }
                            else
                            {
                                console.log('--inside line 1260--');
                                item.record[fieldWrapper.fieldApiName] =  fieldWrapper.fieldValue ;
                                fieldWrapper.isValidationFailed = false;
                                fieldWrapper.validationErrorMessage = [];
                            }
                            
                        }
                    }
                    else if(fieldWrapper.fieldDataType.toLowerCase().indexOf('percent') !=-1){
                        console.log('percent error');
                        if(fieldWrapper.fieldValue != undefined){
                            var num =  fieldWrapper.fieldValue;
                            console.log('num % 1 : ',num % 1);
                            if( isNaN(num % 1) || ( !isNaN(num % 1) && !(parseFloat(num) > 0 && parseFloat(num)<=1)) ){
                                errorMessage = fieldWrapper.fieldLabel +' should lie between 0 and 1.';
                                fieldWrapper.validationErrorMessage = [errorMessage];
                                error_list.push(errorMessage);
                                anyValidationFailed = true ;
                                item.isRowValidationFailed = true;
                                return;
                            }
                             // Added by Kirti to save the Split_Percentage value in Position Employee on 01-06-2020 SIMPS - 1541
                            else {
                                if(fieldWrapper.fieldValue == undefined || fieldWrapper.fieldValue == '')
                                item.record[fieldWrapper.fieldApiName] = undefined;
                                else
                                    item.record[fieldWrapper.fieldApiName] =  fieldWrapper.fieldValue ;

                                fieldWrapper.isValidationFailed = false;
                                fieldWrapper.validationErrorMessage = [];
                            }      
                        }

                    }
                    
                    else if(fieldWrapper.fieldDataType == 'DateTime')
                    {
                        if(fieldWrapper.isRequired)
                        {
                            if(fieldWrapper.fieldValue == undefined || fieldWrapper.fieldValue == '') 
                            {
                                fieldWrapper.isValidationFailed = true;
                                var errorMessage;
                                if(ns != '')
                                {
                                    errorMessage = $A.get("$Label.AxtriaSalesIQTM.PositionFieldBlankPrompt");
                                }
                                else
                                {
                                    errorMessage = $A.get("$Label.c.PositionFieldBlankPrompt") ;
                                }
                                var reg = new RegExp("\\{0\\}", "gm");
                                errorMessage = errorMessage.replace(reg, fieldWrapper.fieldLabel);
                                fieldWrapper.validationErrorMessage = [errorMessage];
                                error_list.push(errorMessage);
                                anyValidationFailed = true ;
                                item.isRowValidationFailed = true;
                                return;
                            }
                        }
                        if(!(!isNaN(Date.parse(fieldWrapper.fieldValue)) && fieldWrapper.fieldValue != parseFloat(fieldWrapper.fieldValue) && !isNaN(parseFloat(fieldWrapper.fieldValue))))     
                        {
                            fieldWrapper.isValidationFailed = true;
                            var errorMessage;
                            if(ns != '')
                            {
                                errorMessage = $A.get("$Label.AxtriaSalesIQTM.Invalid_Field_Value");
                            }
                            else
                            {
                                errorMessage  = $A.get("$Label.c.Invalid_Field_Value");
                            }
                            fieldWrapper.validationErrorMessage = [errorMessage];   
                            error_list.push(errorMessage);
                        }
                    }    
                    else if(fieldWrapper.isRequired) 
                    {
                        // if field is required, add mandatory error
                        if(fieldWrapper.fieldValue == undefined || fieldWrapper.fieldValue == '') {
                            fieldWrapper.isValidationFailed = true;
                            var errorMessage;
                            if(ns != '')
                            {
                                errorMessage = $A.get("$Label.AxtriaSalesIQTM.PositionFieldBlankPrompt");
                            }
                            else
                            {
                                errorMessage = $A.get("$Label.c.PositionFieldBlankPrompt") ;
                            }
                            var reg = new RegExp("\\{0\\}", "gm");
                            errorMessage = errorMessage.replace(reg, fieldWrapper.fieldLabel);
                            fieldWrapper.validationErrorMessage = [errorMessage];
                            error_list.push(errorMessage);
                            item.isRowValidationFailed = true;
                        }
                        else {
                            // set the value back to record and clean the validation errors
                            item.record[fieldWrapper.fieldApiName] =  fieldWrapper.fieldValue ;
                            fieldWrapper.isValidationFailed = false;
                            fieldWrapper.validationErrorMessage = [];
                        }
                    } 
                    else {
                        // if editable field is set to null
                        if(fieldWrapper.fieldValue == undefined || fieldWrapper.fieldValue == '')
                            item.record[fieldWrapper.fieldApiName] = undefined;
                        else
                            item.record[fieldWrapper.fieldApiName] =  fieldWrapper.fieldValue ;

                        fieldWrapper.isValidationFailed = false;
                        fieldWrapper.validationErrorMessage = [];
                    }
                }

                if(fieldWrapper.isValidationFailed)
                    anyValidationFailed = true ;
            });

            item.isRowValidationFailed = anyValidationFailed;
            if(anyValidationFailed)
            {
                anyRowValidationFailed = true;
                // component.set("v.errorList",error_list);
                // var warningIcon = component.find('validation_failed_icon');
                // var toast = component.find("validation_failed_popover");
                // $A.util.removeClass(toast,'slds-hide');
                // $A.util.removeClass(warningIcon,'slds-hide');
                // // $j('#submitButton').prop('disabled', true);
                // document.getElementById('submitButton').disabled = true;
            }
            // else
            // {
            //     // component.set("v.errorList",[]);
            //     // var warningIcon = component.find('validation_failed_icon');
            //     // var toast = component.find("validation_failed_popover");
            //     // $A.util.addClass(toast,'slds-hide');
            //     // $A.util.addClass(warningIcon,'slds-hide');
            //     //  // $j('#submitButton').prop('disabled', false);
            //     //  document.getElementById('submitButton').disabled = false;
            // }
        });

        if(anyRowValidationFailed)
        {
            component.set("v.errorList",error_list);
            var warningIcon = component.find('validation_failed_icon');
            var toast = component.find("validation_failed_popover");
            $A.util.removeClass(toast,'slds-hide');
            $A.util.removeClass(warningIcon,'slds-hide');
            // $j('#submitButton').prop('disabled', true);
            document.getElementById('submitButton').disabled = true;
        }
        else
        {
            component.set("v.errorList",[]);
            var warningIcon = component.find('validation_failed_icon');
            var toast = component.find("validation_failed_popover");
            $A.util.addClass(toast,'slds-hide');
            $A.util.addClass(warningIcon,'slds-hide');
             // $j('#submitButton').prop('disabled', false);
             document.getElementById('submitButton').disabled = false;
        }
        


        if(error_list.length == 0)
        {
            var message;
            if(ns != '')
            {
                message = $A.get("$Label.AxtriaSalesIQTM.Assignment_Detail_Change");
            }
            else
            {
                message = $A.get("$Label.c.Assignment_Detail_Change");
            }

            var changesPerformed = component.get("v.changesPerformed");
            if(changesPerformed)
            {
                document.getElementById('submitButton').disabled = false;
                var addButton = component.find("submitButton");
                $A.util.removeClass(addButton,'hideSubmit');
                document.getElementById('resetButton').disabled = false;
                var resetButton = component.find("resetButton");
                $A.util.removeClass(resetButton,'hideReset');

            }
            
                    
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                mode: 'dismissible',
                type: 'Success',
                message: message
            });
            toastEvent.fire();
        }

        if(positionEmployeeWrapperList.length > 1)
        {
            positionEmployeeWrapperList.sort(function(a, b) 
            {
                if ( a.record[ns + 'Assignment_Type__c'] < b.record[ns + 'Assignment_Type__c'] )
                  return -1;
                if ( a.record[ns + 'Assignment_Type__c'] > b.record[ns + 'Assignment_Type__c'] )
                  return 1;
                return 0;
            });
        }

        component.set("v.ObjectDetailWrapperList", positionEmployeeWrapperList) ;

        var empName = component.get("v.assigneeName");
        var charts = document.getElementsByClassName("chart_"+empName);
        
        if(charts.length > 0)
        {
            for(var i = 0;i<charts.length;i++)
            {
                if(!$A.util.isUndefinedOrNull( charts[i] ))
                {
                    charts[i].parentNode.removeChild(charts[i]);
                }
               
            }
        }
        helper.createTimelineData(component,event);

        for(var i =0;i<positionEmployeeWrapperList.length;i++)
        {
            if(positionEmployeeWrapperList[i].isRowValidationFailed == true)
            {
                if(document.getElementsByClassName("bar-assignment")[i] != undefined)
                    document.getElementsByClassName("bar-assignment")[i].classList.add('errorBar');
            }
            else
            {
                if(document.getElementsByClassName("bar-assignment")[i] != undefined)
                    document.getElementsByClassName("bar-assignment")[i].classList.remove('errorBar');
            }
        }
    },

    closeToast : function(component,event,helper)
    {
        var toast = component.find("validation_failed_popover");    
        $A.util.addClass(toast,'slds-hide');
        var successtoast = component.find("validation_success_popover");
        $A.util.addClass(successtoast,'slds-hide');
        var CRtoast = component.find("pending_CR_popover");
        $A.util.addClass(CRtoast,'slds-hide');
    },

    yesAction : function(component,event,helper)
    {
        var $j = jQuery.noConflict();
        console.log('profile map :',component.get("v.profileMap"));

        var profileMap;
        if(component.get("v.profileMap") != undefined){
            profileMap = JSON.parse(JSON.stringify(component.get("v.profileMap")));
            console.log('profileMap : ',profileMap[0]);
        }

        var ObjectDetailValues = JSON.stringify(component.get("v.ObjectDetailWrapperList"));
        var ns = component.get("v.namespace");
        var action = component.get("c.saveAssignments");
        action.setParams({
            recordDetailsJSON : ObjectDetailValues,
            moduleName : component.get("v.moduleName"),
            profileString : JSON.stringify(component.get("v.profileMap"))
        });

        var yesButton = event.target;
        if(yesButton != undefined)
            yesButton.disabled = true;

        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if(state === 'SUCCESS' && response.getReturnValue != null)
            {
                var retResponse = response.getReturnValue();
                var successtoast = component.find("validation_success_popover");
                var CRtoast = component.find("change_request_success_toast");
                $A.util.addClass(successtoast,'slds-hide');
                if(retResponse[0] == "True")
                {

                    $A.util.removeClass(CRtoast,'slds-hide');
                    var empName = component.get("v.assigneeName");
                    var charts = document.getElementsByClassName("chart_"+empName);
                    var myString = "chart_" + empName;
                    var charts = document.getElementsByClassName(myString);
                    if(charts.length > 0)
                    {
                        for(var i = 0;i<charts.length;i++)
                        {
                            if(!$A.util.isUndefinedOrNull( charts[i] ))
                            {
                                charts[i].parentNode.removeChild(charts[i]);
                            }
                           
                        }
                    }
                    // helper.getAssignments(component,event);
                    var changeReqObj = JSON.parse(retResponse[1]);
                    
                    var message;
                    if(ns != '')
                    {
                        message = $A.get("$Label.AxtriaSalesIQTM.Change_Request_Submitted");
                    }
                    else
                    {
                        message = $A.get("$Label.c.Change_Request_Submitted")
                    }
                    var reg = new RegExp("\\{0\\}", "gm");
                    var labelString = '';
                    var crList = [];
                    Object.keys(changeReqObj).forEach(function(key,index) 
                    {
                        labelString = labelString + ' {' + index + '},' ;
                        var crObj = {'label':key,'url':changeReqObj[key]};
                        var crId = changeReqObj[key].split('sObject/')[1].replace('/view','');
                        console.log('crId :: ',crId);
                        crList.push(crObj);
                        component.set("v.crID", crId);
                    });
                    labelString = labelString.substring(0, labelString.length-1);
                    message = message.replace(reg, labelString);
                    console.log(message);
                    
                    component.set("v.isSuccess", 'success');
                    var toastEvent = $A.get("e.force:showToast");

                    toastEvent.setParams({
                        mode: 'dismissible',
                        type: 'Success',
                        message: message,
                        messageTemplate: message,
                        messageTemplateData: crList
                    });
                    toastEvent.fire();

                    component.set("v.isCRPending",true);
                    helper.redirectToUniverse(component);
                }
                else
                {
                    var myText = "ERROR : " + retResponse[0];

                    if(myText.indexOf(': []') == -1){
                        myText = myText.split(':')[0];
                    }else if(myText.indexOf('[]') == -1){
                        myText = myText.split('[')[0];
                    }

                    console.log('myText :'+myText);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: myText,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                }
            }    
            else if (state === "ERROR" || response.getReturnValue == null) 
            {
                
                if(yesButton != undefined)
                    yesButton.disabled = false;

                var errorLabel;
                if(ns != '')
                {
                    errorLabel = $A.get("$Label.AxtriaSalesIQTM.Unexpected_error_notification");
                }
                else
                {
                    errorLabel = $A.get("$Label.c.Unexpected_error_notification");
                }
                var errors = response.getError();
                var errorMessage = errors[0].message;
                if (errors[0] && errors[0].message) 
                {
                    var action = component.get("c.logException");
        
                    action.setParams({
                        message : errorMessage,
                        module : component.get("v.moduleName")
                    });

                    action.setCallback(this,function(response)
                    {
                        var state = response.getState();
                        if(state === 'SUCCESS')
                        {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                mode: 'dismissible',
                                message: errorLabel,
                                type : 'error'
                                
                            });
                            toastEvent.fire();
                        }
                    });
                    $A.enqueueAction(action);
                } 
                else 
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        message: errorLabel,
                        type : 'error'
                        
                    });
                    toastEvent.fire();
                }
            } 

        });

        $A.enqueueAction(action);
    },

    openToastMessage : function(component,event,helper){
        var toast = component.find("validation_failed_popover");
        $A.util.toggleClass(toast,'slds-hide');
    },

    openCRToastMessage : function(component,event,helper){
        var toast = component.find("pending_CR_popover");
        $A.util.toggleClass(toast,'slds-hide');
    },

    noAction : function(component,event,helper)
    {
        var yesButton = component.find("yesButton");
        var noButton = component.find("noButton");
        var addButton = component.find("submitButton");
        var resetButton = component.find("resetButton");
        var cancelButton = component.find("cancelButton");
        var assignButton = component.find("assignButton");
        var successtoast = component.find("validation_success_popover");
        console.log('---yesButton--',yesButton);
        //yesButton.set("v.disabled",false);
        var yesBtn = document.getElementById("yesBtn");
        yesBtn.disabled = false;
        $A.util.addClass(yesButton,'slds-hide');
        $A.util.addClass(noButton,'slds-hide');
        $A.util.removeClass(addButton,'slds-hide');
        $A.util.removeClass(resetButton,'slds-hide');
        $A.util.removeClass(cancelButton,'slds-hide');
        $A.util.removeClass(assignButton,'slds-hide');
        $A.util.addClass(successtoast,'slds-hide');
    },

    closeCRToast : function(component,event,helper)
    {
        var successtoast = component.find("change_request_success_toast");
        $A.util.addClass(successtoast,'slds-hide');
    },

    navigateToEmployeeUniverse : function(component,event,helper)
    {  
        var currentState = history.state;
        history.pushState(currentState, "Employee_Universe" , "" );
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:SalesIQEmployeeUniverse",
            componentAttributes: {
                objectName: "Employee__c",
                loadRecent: "false",
                selectedListView : component.get("v.selectedListView")
            },
            isredirect : true
        });
        evt.fire();
        document.getElementsByTagName("body")[0].style.overflow = "visible";
    },

    navigateToPositionUniverse : function(component,event,helper)
    {  
        var currentState = history.state;
        history.pushState(currentState, "Position_Universe" , "" );
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:SalesIQPositionUniverse",
            componentAttributes: {
                objectName: "Position__c",
                loadRecent: "false",
                selectedTeamInstance : component.get("v.selectedTeamInstance"),
                selectedListView : component.get("v.selectedListView")

            },
            isredirect : true
        });
        evt.fire();
        document.getElementsByTagName("body")[0].style.overflow = "visible";
    }
})