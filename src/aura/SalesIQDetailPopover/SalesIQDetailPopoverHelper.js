({  	
    showPopupHelper: function(component, componentId, className)
    {
        var popover = component.find(componentId);
        if(className.indexOf('backdrop') != -1)
        {
            $A.util.removeClass(popover, className + 'hide');
            $A.util.addClass(popover, className + 'open');
        }
        else
        {
            $A.util.removeClass(popover, className + 'hide');
            $A.util.addClass(popover, className + 'show');
            $A.util.addClass(popover,'popover-custom');
            document.getElementById('mdpop').style.display = 'block';
        }
        
    },

    hidePopupHelper: function(component, componentId, className){
            var popover = component.find(componentId);
            if(className.indexOf('backdrop') != -1)
            {
                $A.util.addClass(popover, className + 'hide');
                $A.util.removeClass(popover, className+'open');
            }
            else
            {
                $A.util.addClass(popover, className + 'hide');
                $A.util.removeClass(popover, className+'show');
                document.getElementById('mdpop').style.display = 'none';
            }
    },

    customizeDescriptionHelper : function(component,event,helper,id){
        console.log('---customiseRoleDescription---- ');
        var id = event.target.id.split("_")[0];
        var recordDetails = component.get("v.recordDetails");
        console.log('recordDetails ');
        console.log(recordDetails);
        for (var attribute of recordDetails){
            console.log('attribute');
            console.log(attribute);
            if( (attribute['fieldApiName']).includes('Description__c')){
                for(var data of attribute['lookupDataList']){
                    console.log('data',data);
                    if(data['val'] == id){
                        attribute['fieldValue']= data['text'];
                        break;
                    }
                }
                break;
            }
        }
        component.set("v.recordDetails",recordDetails);
        component.set("v.changesPerformed","true");
        
    },
    
})