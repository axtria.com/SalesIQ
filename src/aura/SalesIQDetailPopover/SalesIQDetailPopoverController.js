({
    showpopupfunction :  function(component, event, helper){
    
        var params = event.getParam('arguments');
        var idx ;
        if (params) 
            idx = params.idx;

        idx = parseInt(idx);
        console.log('idx : '+idx);
        component.set("v.idx",idx);
        component.set("v.changesPerformed","false");
        var ObjectDetailWrapperList = component.get("v.ObjectDetailWrapperList");
        var objectDetails = ObjectDetailWrapperList[idx];

        console.log("objectDetails");
        console.log(objectDetails);
        console.log(objectDetails.recordDetails);

        for (var value of objectDetails.recordDetails){
            console.log('value : ',value);
            if(value['fieldDataType'].toLowerCase().indexOf('percent') !=-1){
                if(value['fieldValue']=='' || value['fieldValue']==null){
                    value['fieldValue'] = '1.0000';
                }                
            } 
            else if(value['fieldDataType'].toLowerCase().indexOf('reference') != -1){
            
                if(value['lookupDataList'].length > 0){
                    component.set("v.lookupDetailsList", value['lookupDataList']);
                    if(value['fieldValue']!='' && value['fieldValue']!=null){
                        
                        for (var data of value['lookupDataList']){
                            console.log('data',data);
                            if(data['val']==value['fieldValue']){
                                component.set("v.SearchKeyword",data['text']);
                                component.set("v.searchText", 'false');
                                break;
                            }
                        }      
                    }
                    else{
                       
                        component.set("v.SearchKeyword",'');
                        component.set("v.searchText", 'false');
                    }
                }                             
            }
            else if(value['fieldDataType'].toLowerCase().indexOf('picklist')!= -1 && value['isRequired'] ){
                if(value['fieldValue'].toLowerCase().indexOf('primary')!= -1){
                    component.set("v.isPrimary","true");
                }
                else{
                    component.set("v.isPrimary","false");
                }
            }
        }

        component.set("v.recordDetails", objectDetails.recordDetails);
        component.set("v.objectDetails", objectDetails);
        var ns = component.get("v.namespace");

        var recordId = '';
        if(component.get("v.moduleName") == 'Employee Universe')
            recordId = ObjectDetailWrapperList[idx].record[ns+'Position__c'];
        else if(component.get("v.moduleName") == 'Position Universe')
            recordId = ObjectDetailWrapperList[idx].record[ns+'Employee__c'];

        document.getElementById('positionName').href = "#/sObject/"+recordId+"/view";
        var divPop = document.getElementById('mdpop');

        var bar_assignment;
        // code commemted and assignment bar is picked by index passed from parent component
        /*if(document.getElementsByClassName('bar-secondary') != undefined){
            bar_assignment = document.getElementsByClassName('bar-secondary')[idx];
        }
        if(document.getElementsByClassName('bar-secondary highlightbar') != undefined){
            bar_assignment = document.getElementsByClassName('bar-secondary highlightbar')[idx];
        }
        if(document.getElementsByClassName('bar-assignment') != undefined){
            bar_assignment = document.getElementsByClassName('bar-assignment')[idx];
        }*/


        bar_assignment = document.getElementById('id_'+idx);

        console.log('-- bar_assignment --',bar_assignment);
        
        var moveX = bar_assignment.getBoundingClientRect().left;
        var moveY = bar_assignment.getBoundingClientRect().top + 30;


        var modalHeight = document.getElementsByClassName('slds-modal')[0].clientHeight;
        var modalWidth = document.getElementsByClassName('slds-modal')[0].clientWidth;
        var popoverHeight = document.getElementById('mdpop').clientHeight;
        var popoverWidth = document.getElementById('mdpop').clientWidth;

        var el_style  = window.getComputedStyle(divPop),
        el_display    = el_style.display,
        el_position   = el_style.position,
        el_visibility = el_style.visibility;
        
        divPop.style.visibility = 'hidden';
        //divPop.style.display    = 'block';
        var $j = jQuery.noConflict();
        $j("#mdpop").attr("style","display:block !important");
        
        document.getElementsByTagName("body")[0].style.overflow = "hidden";
        popoverHeight = divPop.clientHeight;
        popoverWidth = divPop.clientWidth;

        // reverting to the original values
        divPop.style.display    = el_display;
        // divPop.style.position   = el_position;
        divPop.style.visibility = el_visibility;

        if(moveX + popoverWidth + 120 > modalWidth)
            moveX = moveX - (moveX + popoverWidth - modalWidth) - 120;

        if(moveY + popoverHeight > modalHeight)
            moveY = moveY - (moveY + popoverHeight - modalHeight);

        var xPos = moveX + "px";
        if(ObjectDetailWrapperList[idx].isRowValidationFailed)
        {
            divPop.style.top = "inherit";
            divPop.style.bottom = "50px";
        }
        else
        {
            divPop.style.bottom = "inherit";
            divPop.style.top = (moveY-50)+"px";
        }
        divPop.style.left = xPos;
        
        helper.showPopupHelper(component, 'modaldialogPopover', 'slds-');
        helper.showPopupHelper(component,'popoverbackdrop','slds-backdrop--');
    },

    droppopupfunction: function(component,event,helper)
    {
        helper.hidePopupHelper(component, 'modaldialogPopover', 'slds-');
        helper.hidePopupHelper(component, 'popoverbackdrop', 'slds-backdrop--');
        var idx = component.get("v.idx");
        var recordDetails = component.get("v.recordDetails");

        console.log('--- inside sroppopup function ----');
        console.log(recordDetails);
        
        component.set("v.objectDetails.recordDetails", recordDetails);
        var objectDetails = component.get("v.objectDetails");
        var ObjectDetailWrapperList = component.get("v.ObjectDetailWrapperList");
        ObjectDetailWrapperList[idx] = objectDetails;
        component.set("v.ObjectDetailWrapperList",ObjectDetailWrapperList);

        if(component.get("v.changesPerformed") == "true")
        {
            var evt = component.getEvent("popoverChange");
            evt.fire();
        }
    },

    removeAssignment : function(component,event,helper)
    {
        helper.hidePopupHelper(component, 'modaldialogPopover', 'slds-');
        helper.hidePopupHelper(component, 'popoverbackdrop', 'slds-backdrop--');
        var idx = component.get("v.idx");
        component.set("v.SearchKeyword","");
        var ObjectDetailWrapperList = component.get("v.ObjectDetailWrapperList");
        component.set("v.ObjectDetailWrapperList",ObjectDetailWrapperList);
        var evt = component.getEvent("removeAssignment");
        evt.setParams({
            "idx" : idx
        });
        evt.fire();
        
    },

 

    searchLookupData : function(component,event,helper){
        console.log('event searchRole ',event);
        var eventSource = event.getSource();
        var searchword = eventSource.get("v.value");
        searchword =searchword.toLowerCase();
        var resultSearchList=[];
        var found = false;
        console.log('searchword ',searchword);
        var searchList = component.get("v.lookupDetailsList");
        console.log('searchList',searchList);
        for(var lookupdata of searchList ){
            console.log('lookupdata',lookupdata);
            if(lookupdata['text'].toLowerCase().includes(searchword)){
                resultSearchList.push(lookupdata);
                found=true;
            }

        }

        console.log('resultSearchList',resultSearchList);
        component.set("v.templookupDetailsList",resultSearchList);
        if(found == true){
            component.set("v.searchText", 'true');
        }
        else{
            component.set("v.searchText", 'false');
        }

    },

    onSelectLookupData : function(component,event,helper){
        console.log("---onSelectLookupData ---");
        console.log('--AK-- event.target:',event.target);
        var id = event.target.id.split("_")[0];
        var value = event.target.id.split("_")[1];
        component.set("v.SearchKeyword",value);
        component.set("v.searchText", 'false');
        helper.customizeDescriptionHelper(component,event,helper,id);
        
        var ns = component.get("v.namespace");       
        var recordDetails = component.get("v.recordDetails");
        console.log(recordDetails); 
        for (var attribute of recordDetails){
            if(attribute['fieldDataType'].toLowerCase().indexOf('reference')!=-1){
                attribute['fieldValue'] = id;
                attribute['lookupDataValue'] = value;
            }
        }      

        console.log('-- date after change :',component.get("v.recordDetails"));
        component.set("v.recordDetails",recordDetails);
        component.set("v.changesPerformed","true");
    },

    clearSelection : function(component, event, helper){
        console.log('clearSelection called');
        var resultSearchList = [];
        component.set("v.SearchKeyword","");
        component.set("v.templookupDetailsList",resultSearchList);
        //to clear description 
        var recordDetails = component.get("v.recordDetails");

        for (var attribute of recordDetails){
       
            if( attribute['fieldDataType'].toLowerCase().indexOf('dependent') != -1){
                attribute['fieldValue'] = '';  
            }
            else if(attribute['fieldDataType'].toLowerCase().indexOf('reference') != -1){
                console.log('attribute inside clear :',attribute);
                attribute['fieldValue'] = '';
                attribute['lookupDataValue'] = '';
                attribute['fieldValue'] = '';
            }
        }
        component.set("v.recordDetails",recordDetails);
        component.set("v.changesPerformed","true");

    } ,

    customizeDate : function (component,event,helper) {

        console.log('---customizeDate---- ');
        var ns = component.get("v.namespace");

        //temp
        var classlist = event.getSource().get("v.class");
        var $j = jQuery.noConflict();
        if(classlist.indexOf('errorField') != -1)
        {
            var idx = classlist.split(' ')[2].split('_')[1];
        } 
        else
        {
            var idx = classlist.split(' ')[1].split('_')[1];
        }   
        
        var recordDetails = component.get("v.recordDetails");
        var fieldWrapper = recordDetails[idx];
        console.log(fieldWrapper);
        var index = component.get("v.idx");
        var item = component.get("v.ObjectDetailWrapperList")[index];
        console.log(fieldWrapper.fieldValue + ':::' + fieldWrapper.fieldApiName);
        console.log(recordDetails);
        var dateFormat = component.get("v.dateFormat");
        var fetchedDate;

        if(fieldWrapper.fieldDataType.toLowerCase().indexOf('date')!=-1){
            var finalDate;
            if(fieldWrapper.date15Or16Rule!=null){
                if(fieldWrapper.date15Or16Rule.split(":")[1].toLowerCase().indexOf('startdate')!=-1){
                    var stDate = new Date(Date.parse(fieldWrapper.fieldValue));
                    console.log('=fieldValue='+fieldWrapper.fieldValue);
                    stDate.setSeconds(stDate.getSeconds() + (stDate.getTimezoneOffset()*60));
                    console.log('stDate ',stDate);
                    
                    //stDate = dateFormat.replace(/y+|Y+/,fetchedDate.getFullYear()).replace(/M+|m+/,(fetchedDate.getMonth()+1)).replace(/d+|D+/,fetchedDate.getDate()); 
                                       
                    if(stDate.getDate()<16){
                         //finalDate = new Date(stDate.getFullYear(), stDate.getMonth(), 1);
                        //finalDate = dateFormat.replace(/y+|Y+/,stDate.getFullYear()).replace(/M+|m+/,(stDate.getMonth()+1)).replace(/d+|D+/,1); 
                        finalDate = stDate.getFullYear() +'-'+(stDate.getMonth()+1)+'-'+1;
                    }
                    else {
                        if((stDate.getMonth()+1)<12){
                            finalDate = stDate.getFullYear() +'-'+(stDate.getMonth()+2)+'-'+1;
                            console.log('==FINAL='+finalDate);
                        }
                        else{
                            finalDate = (stDate.getFullYear()+1) +'-'+((stDate.getMonth()+2)%12)+'-'+1;
                            console.log('==FINAL2='+finalDate);
                        }

                    }
                   

                    console.log('stDate :'+stDate);

                }
                else if(fieldWrapper.date15Or16Rule.split(":")[1].toLowerCase().indexOf('enddate')!=-1){
                    
                    var endDate = new Date(Date.parse(fieldWrapper.fieldValue));
                    console.log('before endDate : '+endDate);
                    endDate.setSeconds(endDate.getSeconds() + (endDate.getTimezoneOffset()*60));
                    console.log('after  endDate : '+endDate);
                    //endDate = new Date(endDate);
                    
                    if((endDate.getDate()<16)) {

                        if((endDate.getMonth()<1)) {
                        
                            console.log('=endDate'+endDate.getDate());
                            console.log('endDate.getMonth() : '+endDate.getMonth());
                            var tempDate = new Date(endDate.getFullYear(), endDate.getMonth(), 0);
                            finalDate = (endDate.getFullYear()-1) +'-'+(endDate.getMonth()+12)+'-'+tempDate.getDate();
                            console.log('=finalDate= :'+finalDate);
                        }
                        else {
                            var tempDate = new Date(endDate.getFullYear(), endDate.getMonth(), 0);
                            finalDate = endDate.getFullYear() +'-'+(tempDate.getMonth()+1) +'-'+ tempDate.getDate();
                        }
                          
                    }

                    else{

                        console.log('endDate :'+endDate);
                        var tempDate = new Date(endDate.getFullYear(), (endDate.getMonth()+1), 0);

                        //tempDate.setSeconds(tempDate.getSeconds() + (tempDate.getTimezoneOffset()*60));

                        console.log('tempDate :'+tempDate);

                        finalDate = endDate.getFullYear() +'-'+(tempDate.getMonth()+1) +'-'+ tempDate.getDate();
                        
                    }
                        
                }

                
                console.log('finalDate2',finalDate);
                
                fieldWrapper.fieldValue = finalDate;
            }
  
        }
        recordDetails[idx] = fieldWrapper;
        component.set("v.recordDetails",recordDetails);
        component.set("v.changesPerformed","true");
        /*if(fieldWrapper.fieldApiName == ns+"Effective_Start_Date__c" || fieldWrapper.fieldApiName == ns+"Effective_End_Date__c"){
            var finalDate;
            if(fieldWrapper.fieldApiName == ns+"Effective_Start_Date__c"){
                var stDate = fieldWrapper.fieldValue;
                stDate = new Date(stDate);                    
                if(stDate.getDate()<16){
                     finalDate = new Date(stDate.getFullYear(), stDate.getMonth(), 1);
                }
                else{
                    finalDate = new Date(stDate.getFullYear(), stDate.getMonth()+1, 1);
                }
                
            }
            else if(fieldWrapper.fieldApiName == ns+"Effective_End_Date__c"){
                var endDate = fieldWrapper.fieldValue;
                endDate = new Date(endDate);
                if(endDate.getDate() < 16){
                    finalDate = new Date(endDate.getFullYear(), endDate.getMonth(), 0);
                }
                else{
                    finalDate = new Date(endDate.getFullYear(), endDate.getMonth() + 1, 0);
                }
            }

            var dd = finalDate.getDate();
            var mm = finalDate.getMonth()+1; //As January is 0.
            var yyyy = finalDate.getFullYear();

            var dateString = component.get("v.dateFormat");
            console.log('dateString',dateString);
            dateString = dateString.replace("M",mm);
            dateString = dateString.replace("d",dd);
            dateString = dateString.replace("yyyy",yyyy);
            var dateString=yyyy+'-'+mm+'-'+dd;
            console.log('dateString',dateString);
            fieldWrapper.fieldValue=dateString;
        }           

        recordDetails[idx] = fieldWrapper;
        component.set("v.recordDetails",recordDetails);
        component.set("v.changesPerformed","true"); */ 

    },

   

    valueChange : function(component,event,helper)
    {
        console.log('valueChange :');
        component.set("v.searchText",false)//added by gurpreet
        var classlist = event.getSource().get("v.class");
        var $j = jQuery.noConflict();
        var ns = component.get("v.namespace");
        if(classlist.indexOf('errorField') != -1)
        {
            var idx = classlist.split(' ')[2].split('_')[1];
        } 
        else
        {
            var idx = classlist.split(' ')[1].split('_')[1];
        }   
        
        var recordDetails = component.get("v.recordDetails");
        var fieldWrapper = recordDetails[idx];
        var index = component.get("v.idx");
        var item = component.get("v.ObjectDetailWrapperList")[index];
        console.log(fieldWrapper.fieldValue + ':::' + fieldWrapper.fieldApiName);

        //code added for split percentage to be 1.0000 and uneditable if assingment type is primary
        for (var value of recordDetails){
            console.log('value1');
            console.log(value);
            if(value['fieldDataType'].toLowerCase().indexOf('picklist')!= -1 && value['isRequired']){
                if(value['fieldValue'].toLowerCase().indexOf('primary')!= -1){
                    console.log("here");
                    for (var value of recordDetails){
                        if(value['fieldDataType'].toLowerCase().indexOf('percent') !=-1){
                            value['fieldValue']='1.0000';
                            component.set("v.isPrimary","true");
                            break;
                        }
                    }
                }
                else{
                    component.set("v.isPrimary","false");
                } 
                break;
            }
        }
        component.set("v.recordDetails",recordDetails);
        //end----------

        console.log('---abc--- recordDetails',recordDetails);
        if(classlist.indexOf('dateFieldError') != -1 || classlist.indexOf('dateField') != -1 || classlist.indexOf('dateTimeFieldError') != -1 || classlist.indexOf('dateTimeField') != -1)
        {
            var dateValue = event.getSource().get("v.value");
            if(!isNaN(Date.parse(dateValue)) && dateValue != parseFloat(dateValue) && !isNaN(parseFloat(dateValue)) || dateValue == '')     
            {
                component.set("v.changesPerformed","true");
            }
            else
            {
                var errorMessage = '';
                fieldWrapper.isValidationFailed = true;
                
                if(ns != '')
                {
                    errorMessage = $A.get("$Label.AxtriaSalesIQTM.Invalid_Field_Value") ;
                }
                else
                {
                    errorMessage = $A.get("$Label.c.Invalid_Field_Value") ;
                }
                fieldWrapper.validationErrorMessage = [errorMessage];
                component.set("v.changesPerformed","true"); 
                recordDetails[idx] = fieldWrapper;
                component.set("v.recordDetails",recordDetails);
                return;
            }

        }

        if(fieldWrapper.isEditable) 
        {
            if(fieldWrapper.isRequired) 
            {
                console.log('- inside blank validation -'+fieldWrapper.fieldValue);
                // if field is required, add mandatory error
                if(fieldWrapper.fieldValue == undefined || fieldWrapper.fieldValue == '') 
                {
                    fieldWrapper.isValidationFailed = true;
                    var errorMessage;
                    if(ns != '')
                    {
                        errorMessage = $A.get("$Label.AxtriaSalesIQTM.PositionFieldBlankPrompt") ;
                    }
                    else
                    {
                        errorMessage = $A.get("$Label.c.PositionFieldBlankPrompt") ;
                    }
                    var reg = new RegExp("\\{0\\}", "gm");
                    errorMessage = errorMessage.replace(reg, fieldWrapper.fieldLabel);
                    fieldWrapper.validationErrorMessage = [errorMessage];
                    recordDetails[idx] = fieldWrapper;
                    component.set("v.recordDetails",recordDetails);
                    item.isRowValidationFailed = true;
                }
                else 
                {
                    fieldWrapper.isValidationFailed = false;
                    fieldWrapper.validationErrorMessage = [];
                }
                
            } 
            else if(item.record[fieldWrapper.fieldApiName] != fieldWrapper.fieldValue)
            {
                fieldWrapper.isValidationFailed = false;
                fieldWrapper.validationErrorMessage = [];
            }
        }
        component.set("v.recordDetails",recordDetails);
        component.set("v.changesPerformed","true");        
    },

    navigateRecord:function(component,event,helper)
    {
        document.body.setAttribute('style', 'overflow: auto;');
    }
})