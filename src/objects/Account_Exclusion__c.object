<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.Name</formula>
        <label>Account Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Account Exclusions</relationshipLabel>
        <relationshipName>Account_Exclusions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Block_Type__c</fullName>
        <externalId>false</externalId>
        <label>Block Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Implicit</fullName>
                    <default>false</default>
                    <label>Implicit</label>
                </value>
                <value>
                    <fullName>Explicit</fullName>
                    <default>false</default>
                    <label>Explicit</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Country__c</fullName>
        <externalId>false</externalId>
        <formula>CASESAFEID(Account__r.Country__c)</formula>
        <label>Country</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Is_Exclusion_Batch_Run__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Is Exclusion Batch Run</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Blocked_For_all__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Is Blocked For all</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Exclusion_Batch_Run_Text__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Is_Exclusion_Batch_Run__c=true,'True','False')</formula>
        <label>Is Exclusion Batch Run</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product_Master__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product__c</referenceTo>
        <relationshipLabel>Account Exclusions</relationshipLabel>
        <relationshipName>Account_Exclusions</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product_Name__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Product_Master__r.Name == '', 'All',Product_Master__r.Name)</formula>
        <label>Product Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Reason_Code__c</fullName>
        <externalId>false</externalId>
        <label>Reason Code</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Rule_Name__c</fullName>
        <description>Name of the rule attached</description>
        <externalId>false</externalId>
        <label>Rule Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Inactive</fullName>
                    <default>false</default>
                    <label>Inactive</label>
                </value>
                <value>
                    <fullName>Active</fullName>
                    <default>false</default>
                    <label>Active</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>Account Exclusion</label>
     <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Account_Name__c</columns>
        <columns>Product_Name__c</columns>
        <columns>Is_Exclusion_Batch_Run_Text__c</columns>
        <columns>Status__c</columns>
        <columns>Block_Type__c</columns>
        <columns>Rule_Name__c</columns>
        <columns>Reason_Code__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>AEX-{000}</displayFormat>
        <label>Account Exclusion Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Account Exclusions</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
