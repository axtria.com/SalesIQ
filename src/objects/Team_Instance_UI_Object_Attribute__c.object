<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Information about all the UI Screens  available in Implementation according to Team Cycles</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Attribute_Display_Name__c</fullName>
        <description>Attribute Display Name in UI Screen</description>
        <externalId>false</externalId>
        <label>Attribute Display Name</label>
        <length>80</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Comments__c</fullName>
        <description>Comments</description>
        <externalId>false</externalId>
        <label>Comments</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Default_Sort__c</fullName>
        <description>Default Sort of the column</description>
        <externalId>false</externalId>
        <label>Default Sort</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Display_Column_Order__c</fullName>
        <description>Attribute display Name</description>
        <externalId>false</externalId>
        <label>Display Column Order</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Effective_End_Date__c</fullName>
        <description>Effective End Date for the Record</description>
        <externalId>false</externalId>
        <label>Effective End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Effective_Start_Date__c</fullName>
        <description>Effective StartDate for the Record</description>
        <externalId>false</externalId>
        <label>Effective Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Obj_Attr_Team_Instance__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Lookup to Object_Attribute_Team_Instance Object</description>
        <externalId>false</externalId>
        <label>Obj Attr Team Instance</label>
        <referenceTo>Team_Instance_Object_Attribute__c</referenceTo>
        <relationshipLabel>Team Instance UI Object Attribute</relationshipLabel>
        <relationshipName>UI_Object_Attr_Team_Ins</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Secondary_Sort__c</fullName>
        <description>Secondary Sort of the Column</description>
        <externalId>false</externalId>
        <label>Secondary Sort</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Team_Instance_UI_Object_Attribute_Name__c</fullName>
        <externalId>false</externalId>
        <label>Team Instance UI Object Attribute Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>User_Interface_Master__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>Lookup to UI_Interface_Master</description>
        <externalId>false</externalId>
        <label>User Interface Master</label>
        <referenceTo>User_Interface_Master__c</referenceTo>
        <relationshipName>UI_Object_Attr_Team_Ins</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Width__c</fullName>
        <description>Width of Column in UI</description>
        <externalId>false</externalId>
        <label>Width</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>isEnabled__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Specify whther Attribute is enabled specific to Team Cycle in UI</description>
        <externalId>false</externalId>
        <label>isEnabled</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>isStatic__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Specify whther Attribute is static specific to Team Cycle in UI</description>
        <externalId>false</externalId>
        <label>isStatic</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Team Instance UI Object Attribute</label>
    <nameField>
        <label>Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Team Instance UI Object Attribute</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
