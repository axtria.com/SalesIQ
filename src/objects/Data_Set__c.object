<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Data Set</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Base_Data_Set__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Base Data Set</label>
        <referenceTo>Data_Set__c</referenceTo>
        <relationshipLabel>Data Sets</relationshipLabel>
        <relationshipName>Data_Sets</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Column_Filter_Destination__c</fullName>
        <externalId>false</externalId>
        <label>Column Filter Destination</label>
        <length>1000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Column_Filter_Source__c</fullName>
        <externalId>false</externalId>
        <label>Column Filter Source</label>
        <length>1000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Data_Filter_Column_Destination__c</fullName>
        <externalId>false</externalId>
        <label>Data Filter Column Destination</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Data_Filter_Column_Source__c</fullName>
        <description>To keep the table column name (staging table column) which will filter out the records. This column must be preset in the Data Set column detail of this data set.</description>
        <externalId>false</externalId>
        <label>Data Filter Column Source</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Data_Filter_Operator__c</fullName>
        <externalId>false</externalId>
        <label>Data Filter Operator</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Data_Filter_Value__c</fullName>
        <externalId>false</externalId>
        <label>Data Filter Value</label>
        <length>200</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Data_Set_Id__c</fullName>
        <description>External Id</description>
        <displayFormat>DS{0}</displayFormat>
        <externalId>true</externalId>
        <label>Data Set Ext Id</label>
        <trackTrending>false</trackTrending>
        <type>AutoNumber</type>
    </fields>
    <fields>
        <fullName>Data_Set_Object_Name__c</fullName>
        <externalId>false</externalId>
        <label>Data Set Object Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Deduping_Attributes__c</fullName>
        <externalId>false</externalId>
        <label>Deduping Attributes</label>
        <length>1000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Deduping_Rules__c</fullName>
        <externalId>false</externalId>
        <label>Deduping Rules</label>
        <length>9000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Full_Load_Always__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Full Load Always</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Master__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Is Master</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Is_Upstream__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Is Upstream</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Output_Rules__c</fullName>
        <externalId>false</externalId>
        <label>Output Rules</label>
        <length>4000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Parent_Data_Set__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Parent Data Set</label>
        <referenceTo>Data_Set__c</referenceTo>
        <relationshipLabel>Data Sets (Parent Data Set)</relationshipLabel>
        <relationshipName>Data_Sets1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SalesIQ_Internal__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>SalesIQ Internal</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Scenario__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Scenario Lookup being added for Joins Component</description>
        <externalId>false</externalId>
        <label>Scenario</label>
        <referenceTo>Scenario__c</referenceTo>
        <relationshipLabel>Data Sets</relationshipLabel>
        <relationshipName>Data_Sets</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>destination__c</fullName>
        <description>destination</description>
        <externalId>false</externalId>
        <label>Destination</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>dst_conn_str__c</fullName>
        <description>Connection string</description>
        <externalId>false</externalId>
        <label>Dst Conn Str</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>dst_path__c</fullName>
        <externalId>false</externalId>
        <label>Dst Path</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>is_internal__c</fullName>
        <defaultValue>false</defaultValue>
        <description>is internal</description>
        <externalId>false</externalId>
        <label>is internal</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>period__c</fullName>
        <description>period</description>
        <externalId>false</externalId>
        <label>Period</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>source__c</fullName>
        <description>source</description>
        <externalId>false</externalId>
        <label>Source</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>src_conn_str__c</fullName>
        <description>Connection string</description>
        <externalId>false</externalId>
        <label>Src Conn Str</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>src_path__c</fullName>
        <externalId>false</externalId>
        <label>Src Path</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>version__c</fullName>
        <description>version</description>
        <externalId>false</externalId>
        <label>Version</label>
        <length>5</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Data Set</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Data_Set_Id__c</columns>
        <columns>is_internal__c</columns>
        <columns>version__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Data Set Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Data Sets</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
