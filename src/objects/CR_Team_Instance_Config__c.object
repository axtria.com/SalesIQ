<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Holds information about Change Requests specific Configuration</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Bulk_Edit_Allowed__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Bulk Edit Allowed</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Change_Request_Types__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Change Request Type</label>
        <referenceTo>Change_Request_Type__c</referenceTo>
        <relationshipLabel>CR_Team_Instance_Configs</relationshipLabel>
        <relationshipName>CR_Team_Instance_Configs</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Comments__c</fullName>
        <externalId>false</externalId>
        <label>Comments</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Config_Value__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Config Value</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Configuration_Name__c</fullName>
        <externalId>false</externalId>
        <label>Configuration Name</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Configuration_Type__c</fullName>
        <externalId>false</externalId>
        <label>Configuration Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Hierarchy Configuration</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Layer Configuration</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Non Field Position Type</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Data Table Configuration</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Configuration_Value__c</fullName>
        <externalId>false</externalId>
        <label>Configuration Value</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Create__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Creation allowed or not at the corresponding level</description>
        <externalId>false</externalId>
        <label>Create</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Delete__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Delete allowed or not at corresponding hierarchy level</description>
        <externalId>false</externalId>
        <label>Delete</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Edit__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Edit allowed or not at corresponding hierarchy level</description>
        <externalId>false</externalId>
        <label>Edit Overlay Association</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Fixed_Column_From_Left__c</fullName>
        <externalId>false</externalId>
        <label>Fixed Column From Left</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Fixed_Column_From_Right__c</fullName>
        <externalId>false</externalId>
        <label>Fixed Column From Right</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Hierarchy_Change__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Hierarchy Change</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Hierarchy_Level_Number__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL(Configuration_Type__c,&apos;Hierarchy Configuration&apos;), VALUE(Configuration_Name__c), 0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Hierarchy Level Number</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>IsWrapText__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>IsWrapText</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>MaximumRecordsSaved__c</fullName>
        <externalId>false</externalId>
        <label>Maximum Records Saved</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Page_Size__c</fullName>
        <externalId>false</externalId>
        <label>Page Size</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Pagination_on__c</fullName>
        <defaultValue>false</defaultValue>
        <description>enable pagination?</description>
        <externalId>false</externalId>
        <label>Pagination on</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Review_Allowed__c</fullName>
        <defaultValue>false</defaultValue>
        <description>If set to true, Rejection of Change Request sends the CR back for review and all the changes are not reverted.</description>
        <externalId>false</externalId>
        <label>Review Allowed</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>RowHeight__c</fullName>
        <externalId>false</externalId>
        <label>Row Height</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Team_Instance__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Lookup to Team Instance Object</description>
        <externalId>false</externalId>
        <label>Team Instance</label>
        <referenceTo>Team_Instance__c</referenceTo>
        <relationshipLabel>CR_Team_Instance_Configs</relationshipLabel>
        <relationshipName>CR_Team_Instance_Configs</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Update_Position_Attributes__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Update Position Attributes</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>CR Team Instance Config</label>
    <listViews>
        <fullName>Add_Target_Table_Configuration</fullName>
        <columns>NAME</columns>
        <columns>Configuration_Name__c</columns>
        <columns>Configuration_Type__c</columns>
        <columns>Configuration_Value__c</columns>
        <columns>Bulk_Edit_Allowed__c</columns>
        <columns>Fixed_Column_From_Left__c</columns>
        <columns>Fixed_Column_From_Right__c</columns>
        <columns>Page_Size__c</columns>
        <columns>Pagination_on__c</columns>
        <columns>Review_Allowed__c</columns>
        <columns>Team_Instance__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Configuration_Type__c</field>
            <operation>equals</operation>
            <value>Date Table Configuration</value>
        </filters>
        <filters>
            <field>Configuration_Name__c</field>
            <operation>equals</operation>
            <value>Add Target</value>
        </filters>
        <label>Add Target Table Configuration</label>
    </listViews>
    <listViews>
        <fullName>Call_Plan_Table_Configuration</fullName>
        <columns>NAME</columns>
        <columns>Configuration_Name__c</columns>
        <columns>Configuration_Type__c</columns>
        <columns>Configuration_Value__c</columns>
        <columns>Bulk_Edit_Allowed__c</columns>
        <columns>Fixed_Column_From_Left__c</columns>
        <columns>Fixed_Column_From_Right__c</columns>
        <columns>Page_Size__c</columns>
        <columns>Pagination_on__c</columns>
        <columns>Review_Allowed__c</columns>
        <columns>Team_Instance__c</columns>
        <columns>IsWrapText__c</columns>
        <columns>MaximumRecordsSaved__c</columns>
        <columns>RowHeight__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Configuration_Type__c</field>
            <operation>equals</operation>
            <value>Data Table Configuration</value>
        </filters>
        <filters>
            <field>Configuration_Name__c</field>
            <operation>equals</operation>
            <value>Call Plan</value>
        </filters>
        <label>Call Plan Table Configuration</label>
    </listViews>
    <listViews>
        <fullName>Hierarchy_Configuration</fullName>
        <columns>NAME</columns>
        <columns>Change_Request_Types__c</columns>
        <columns>Configuration_Name__c</columns>
        <columns>Config_Value__c</columns>
        <columns>Team_Instance__c</columns>
        <columns>Configuration_Value__c</columns>
        <columns>Configuration_Type__c</columns>
        <columns>Create__c</columns>
        <columns>Delete__c</columns>
        <columns>Edit__c</columns>
        <columns>Hierarchy_Change__c</columns>
        <columns>Update_Position_Attributes__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Configuration_Type__c</field>
            <operation>equals</operation>
            <value>Hierarchy Configuration</value>
        </filters>
        <label>Hierarchy Configuration</label>
    </listViews>
    <listViews>
        <fullName>Non_Field_Position_Type</fullName>
        <columns>NAME</columns>
        <columns>Configuration_Name__c</columns>
        <columns>Configuration_Value__c</columns>
        <columns>Configuration_Type__c</columns>
        <columns>Create__c</columns>
        <columns>Update_Position_Attributes__c</columns>
        <columns>Delete__c</columns>
        <columns>Hierarchy_Change__c</columns>
        <columns>Edit__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Configuration_Type__c</field>
            <operation>equals</operation>
            <value>Non Field Position Type</value>
        </filters>
        <label>Non Field Position Type</label>
    </listViews>
    <nameField>
        <displayFormat>CRTIC-{000000000}</displayFormat>
        <label>CR Team Instance Config  Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>CR Team Instance Configs</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
