/**************************************************************************************************************
* Name          :   UpdateCredOnEsriPostgres
* Author        :   Ayush Rastogi
* Since         :   27/01/2020
**************************************************************************************************************/
trigger UpdateCredOnEsriPostgres on ETL_Config__c(after insert, after update) {
    for (ETL_Config__c EtlCofig: Trigger.new) {

        if (EtlCofig.Name == SalesIQGlobalConstants.ETLCONFIG_MAP_DOWNLOAD && (Trigger.oldMap == null || (Trigger.oldMap.get(EtlCofig.Id).SF_UserName__c != ETLCofig.SF_UserName__c ||
                Trigger.oldMap.get(EtlCofig.Id).SF_Password__c != ETLCofig.SF_Password__c || Trigger.oldMap.get(EtlCofig.Id).end_Point__c != ETLCofig.end_Point__c))) {

            if (ETLCofig.end_point__c != null) {
                system.debug('inside if');
                ETLConfigDataHandler.getETLConfig(ETLCofig.id,ETLCofig.SF_UserName__c,ETLCofig.SF_Password__c,ETLCofig.S3_Security_Token__c);
                //ETLConfigDataHandler.getETLConfig();
            } else {
                trigger.new[0].addError(System.Label.Endpoint_does_not_exist);
            }

        }

    }

}