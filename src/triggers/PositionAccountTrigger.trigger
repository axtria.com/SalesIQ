/*****************************************************************************************************
Name            :       PositionAccountTrigger
Description     :       Trigger on Position_Account__c object.
                        Aggregate Extents on Position Team Instance (Territory level) on Account movement.
Last Updated By :       Raghvendra Rathore - 29/02/2016
******************************************************************************************************/
trigger PositionAccountTrigger on Position_Account__c (after delete, after insert, after undelete, after update, before insert,before update) {
    //if value of IsStopTrigger__c is false then trigger won't execute
    if(TriggerContol__c.getValues('PositionAccountTrigger') != null && !TriggerContol__c.getValues('PositionAccountTrigger').IsStopTrigger__c)
    {
        if(trigger.isAfter)
        {
            if(trigger.isInsert || trigger.isUnDelete)
            {
                //Update extents on Position Team Instance on Position Account Insert
                PositionAccountTriggerHandler.rollupExtentsOnTerritory(trigger.new, new map<Id,Position_Account__c>());
            }
            
            if(trigger.isUpdate)
            {
                //Update extents on Position Team Instance on Position Account update
                PositionAccountTriggerHandler.rollupExtentsOnTerritory(trigger.new, trigger.oldMap);
            }
        
            if(trigger.isDelete)
            {
                //Update extents on Position Team Instance on Position Account delete
                PositionAccountTriggerHandler.rollupExtentsOnTerritory(trigger.old, new map<Id,Position_Account__c>());
            }
            //UPDATE LAST MODIFIED DATES OF RELATED ACCOUNT FOR DELTA QUALIFICATION
            if(trigger.isInsert || trigger.isUpdate || trigger.isDelete){
                Set<Id> positionAccountIds = new Set<Id>();
                Set<Id> accountIds = new Set<Id>();
                List<Account> deltaModifiedAccounts = new List<Account>();
                try{
                    for( Position_Account__c positon_account_obj :trigger.new){
                         if(positon_account_obj.Account__c != null && !accountIds.contains(positon_account_obj.Account__c)){
                             deltaModifiedAccounts.add(new Account(Id = positon_account_obj.Account__c));
                             accountIds.add(positon_account_obj.Account__c);
                         }
                    }
                    //DML DUMMTY UPDATE TO MODIFY SYS MOD STAMP AND LAST MODIFIED DATE ON ACCOUNT RECORD
                    update deltaModifiedAccounts;    
                }
                catch(Exception e){
                    system.debug(e);
                }
            }
        }
        else
        {
            if(trigger.isInsert){
                Map<String,Position_Team_Instance__c> posTeamMap = new Map<String,Position_Team_Instance__c>();
                Set<Id> posList = new Set<Id>();
                for(Position_Account__c pa:trigger.new) {
                    posList.add(pa.Position__c);
                }

                List<Position_Team_Instance__c> posTeamList = [select Position_ID__c,Team_Instance_ID__c, Team_Instance_ID__r.IC_EffstartDate__c, Team_Instance_ID__r.IC_EffEndDate__c from Position_Team_Instance__c where Position_ID__c in: posList];
                for(Position_Team_Instance__c posTeam:posTeamList)
                {
                    posTeamMap.put(posTeam.Position_ID__c,posTeam);
                    
                }

                for(Position_Account__c posAcc : trigger.new)
                {
                    if(posAcc.Position_Team_Instance__c == null) {
                        posAcc.Position_Team_Instance__c = posTeamMap.get(posAcc.Position__c).Id;

                        if(posAcc.Effective_Start_Date__c == null)
                            posAcc.Effective_Start_Date__c = posTeamMap.get(posAcc.Position__c).Team_Instance_ID__r.IC_EffstartDate__c;
                            
                        if(posAcc.Effective_End_Date__c == null)
                            posAcc.Effective_End_Date__c = posTeamMap.get(posAcc.Position__c).Team_Instance_ID__r.IC_EffEndDate__c;
                        
                        // added code so on Promote from BRMS, it will populate proposed position
                        posAcc.Proposed_Position__c = posAcc.Position__c;
                    }
                }
            }
        }
    }
}