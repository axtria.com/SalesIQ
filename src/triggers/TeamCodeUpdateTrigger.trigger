/*@author : Ankur Dohare
  @date : 23 AUG 2017  
  @description : This trigger will fire while updating the Team and will update Team_Code__c field.
  @Param1: event, when trigger will execute
  return : void 
 */ 
trigger TeamCodeUpdateTrigger on Team__c (before Insert) {
	if(TriggerContol__c.getValues('ValidateExistingSharedZIPTrigger') != null && TriggerContol__c.getValues('ValidateExistingSharedZIPTrigger').IsStopTrigger__c) return ;
    if(trigger.isInsert){
        SalesIQUtility.updateTeamCode(trigger.new);
    }
}