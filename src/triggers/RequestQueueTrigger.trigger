trigger RequestQueueTrigger on Activity_Log__c (after insert,after update) {   

	//CHECK IF JOB STATUS IN QUEUE IS ERROR AND HAS CHANGE REQUEST
	//MARK ALL OTHER QUEUED JOBS OF THE ERRORED SCENARIO AS ERROR
    if(Trigger.isAfter && Trigger.isUpdate)
    {
        for(Activity_Log__c queueObj : trigger.new){
            String objectAPIName = SalesIQGlobalConstants.NAME_SPACE+'Activity_Log__c';
            Id recordTypeId = Schema.getGlobalDescribe().get(objectAPIName).getDescribe().getRecordTypeInfosByName().get('Scenario Execution').getRecordTypeId();

            if( trigger.oldMap.get(queueObj.Id).Job_Status__c == SalesIQGlobalConstants.IN_PROGRESS && queueObj.Job_Status__c  == SalesIQGlobalConstants.ERROR && queueObj.Change_Request__c != null && queueObj.RecordTypeId == recordTypeId ){
                //UPDATE ALL QUEUE THAT ARE NOT STARTED 
                RequestQueueTriggerHandler.markFailScenariosInQueue(queueObj);
            }
        }
		
		/*
		* SIQ-2891 | 11/14/2018
		* Update Rule_Execution_Status__c on Change Request when BRMS service for position level rules is completed.
		* Find parent CR with all related Request Queue records. If all the related Request Queue records have Job_Status__c Success
		* Update the CR.Rule_Execution_Status__c to Success or Error based on Request Queue Job_Status__c
		*/
		map<Id,string> crStatusMap = new map<Id,string>();
		map<Id,Id> crTeamInstanceMap = new map<Id,Id>();
		map<Id,Id> crScenarioMap = new map<Id,Id>();
		for(Activity_Log__c req : trigger.new){
		    system.debug('#### new Job_Status__c : '+req.Job_Status__c);
            system.debug('#### old Job_Status__c : '+trigger.oldMap.get(req.Id).Job_Status__c);
            		crTeamInstanceMap.put(req.Change_Request__c,req.Scenario__c); 
			if(req.Request_Type__c == 'Position Level' && (req.Job_Status__c == 'Success' || req.Job_Status__c == 'Error') && trigger.oldMap.get(req.Id).Job_Status__c == 'In Progress'){
				crStatusMap.put(req.Change_Request__c,req.Job_Status__c);
			}
			if(req.Request_Type__c == 'Position Level' && (req.Job_Status__c == 'In Progress') && (trigger.oldMap.get(req.Id).Job_Status__c == 'Not Started' || trigger.oldMap.get(req.Id).Job_Status__c == 'Queued')){
				crStatusMap.put(req.Change_Request__c,req.Job_Status__c);
			}
		}
		for(Scenario__c sce :[Select id,Team_Instance__c FROM Scenario__c WHERE id in: crTeamInstanceMap.values()]){
		    crScenarioMap.put(sce.id,sce.Team_Instance__c);
		}
		if(crStatusMap.size() > 0){
			/*If Job status is changed to success and Request Queue has more records in pending or in progress stage, do not update CR status to Success*/
			for(Change_Request__c cr : [SELECT Id, Rule_Execution_Status__c, (SELECT Id, Job_Status__c, Change_Request__c FROM Requests_Queue__r WHERE Job_Status__c NOT IN ('Success','Error')) FROM Change_Request__c WHERE Id IN : crStatusMap.keyset()]){
				if(cr.Requests_Queue__r.size() > 0 && crStatusMap.get(cr.Id) == 'Success'){
					crStatusMap.remove(cr.Id);
				}
			}
		}
		if(crStatusMap.size() > 0){
			list<Change_Request__c> crToUpdate = new list<Change_Request__c>();
			for(Id crId : crStatusMap.keyset()){
			    
				crToUpdate.add(new Change_Request__c(Id = crId, Rule_Execution_Status__c = crStatusMap.get(crId)));
				System.debug('######crTeamInstanceMap.get(crId)'+crScenarioMap.get(crTeamInstanceMap.get(crId)));
				if(crStatusMap.get(crId)=='Success')
				    database.executeBatch(new UpdatePositionAccountSharedBatch(crScenarioMap.get(crTeamInstanceMap.get(crId))));
				
			}
			
			update crToUpdate;
		}
    }

    //AFTER INSERT OF A NEW QUEUE RECORD, UPDATE ALL EXISTING NOT STARTED TO QUEUED STATUS
    if( Trigger.isAfter && ( Trigger.isInsert || Trigger.isUpdate ) ){
        RequestQueueTriggerHandler.queueIdsToAvoidRecursion.addAll(trigger.newMap.keyset());
        RequestQueueTriggerHandler.updateRequestQueue();
    }


    //EXISITNG ACTIVITY LOG JOB FOR ALIGMENT MODULE
    if(Trigger.New[0].Request_Type__c=='Current Hierarchy')
    {
        if(Trigger.isInsert)
        {
            Integer flag=0;
            List<Activity_Log__c> l= [SELECT CreatedDate,Id,Interface_Name__c,Job_Status__c,Name,Output__c,Position__r.Client_Position_Code__c, Position__r.Hierarchy_Level__c,Request_Type__c,Team_Instance__r.Name,Position__r.Position_Type__c FROM Activity_Log__c WHERE Request_Type__c='Current Hierarchy' AND Module__c='Map Print'];
            for(Activity_Log__c s:l)
            {
                if(s.Id!=Trigger.New[0].id)
                {
                    if(s.Job_Status__c=='Pending' ||s.Job_Status__c=='In Progress')
                    flag=1;
                }
            }
  
            if(flag==0)
            {
                
                Database.executeBatch( new PrintMapHierarchyBatch(Trigger.New[0].id));
                //FutureMethodPrint.processRecords(Trigger.New[0].id);
            }  
        }
        if(Trigger.isUpdate && (Trigger.New[0].Job_Status__c=='Completed' || Trigger.New[0].Job_Status__c=='Failed')){
	// List<Activity_Log__c> activityList = [SELECT CreatedDate,Id,OwnerId,LastModifiedById,Interface_Name__c,Job_Status__c,Name,Output__c,Position__r.Client_Position_Code__c, Position__r.Hierarchy_Level__c,Request_Type__c,Team_Instance__r.Name,Position__r.Position_Type__c FROM Activity_Log__c WHERE Job_Status__c='Completed' order by CreatedDate Asc];

            //SalesIQUtility.updateNotification('ActivityLog', Trigger.New);
            List<Activity_Log__c> pendingList= [SELECT CreatedDate,Id,Interface_Name__c,Job_Status__c,Name,Output__c,Position__r.Client_Position_Code__c, Position__r.Hierarchy_Level__c,Request_Type__c,Team_Instance__r.Name,Position__r.Position_Type__c FROM Activity_Log__c WHERE Request_Type__c='Current Hierarchy' AND Module__c='Map Print' AND Job_Status__c='Pending' order by CreatedDate Asc];
            if(!pendingList.isEmpty()){
                Database.executeBatch( new PrintMapHierarchyBatch(pendingList[0].id));
            }
        }
    }   
}