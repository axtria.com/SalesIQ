trigger AccountAffiliationTrigger on Account_Affiliation__c (after insert,after update, after delete) {
    //FOR ACCOUNT AFFILIATION NEWTWORK INSERT,UPDATE AND DELETE LAST MODIFIED DATE ON REALTED ACCOUNT OBJECT.
    List<Account> deltaModifiedAccounts = new List<Account>();
    Set<Id> accountIds = new Set<Id>();
    try{
        for(Account_Affiliation__c account_affiliation_obj:trigger.new){
            
            Boolean isParentExisitingInList = accountIds.contains(account_affiliation_obj.Parent_Account__c);
            Boolean isChildExisitingInList = accountIds.contains(account_affiliation_obj.Account__c);
            
            if(account_affiliation_obj.Account__c != null && !( isParentExisitingInList || isChildExisitingInList ) ){
              deltaModifiedAccounts.add(new Account(Id = account_affiliation_obj.Account__c));
              accountIds.add(account_affiliation_obj.Account__c);
            }
            if(account_affiliation_obj.Parent_Account__c != null && !( isParentExisitingInList || isChildExisitingInList )){
              deltaModifiedAccounts.add(new Account(Id = account_affiliation_obj.Parent_Account__c));
              accountIds.add(account_affiliation_obj.Parent_Account__c);
            }
        }
        //DML DUMMTY UPDATE TO MODIFY SYS MOD STAMP AND LAST MODIFIED DATE ON ACCOUNT RECORD
         system.debug ('deltaModifiedAccounts BY AccountAffiliationTrigger ---+++--' + deltaModifiedAccounts.size() );
         update deltaModifiedAccounts;
    }
    catch(Exception e){
        system.debug(e);
    }
}