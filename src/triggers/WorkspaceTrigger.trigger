trigger WorkspaceTrigger on Workspace__c (before delete , before insert) {
  //Validation for workscenario delete when child scenario exists.
  String workspaceName; Id userPersistanceCountryId; Id workspaceCountryId;
  Workspace__c workspace;
  if(Trigger.isDelete && Trigger.isBefore){
    for(Workspace__c ws : trigger.old){
      if(ws.Number_of_Scenarios__c > 0){
            ws.addError(system.Label.Workspace_Multiple_Scenarios);
      }
    }
  }
  //INSERT COUNTRY IF AND VALIDATE FOR DUPLICATE WORKSPACE 
  if(Trigger.isInsert && Trigger.isBefore){
    for(Workspace__c ws : trigger.new){
      workspaceName = ws.Name;
      workspaceCountryId = ws.Country__c;
      workspace = ws;
    }
    //GET COUNTRY ID OF LOGGED IN USER WHEN USER BEFORE INSERTING WORKSPACE
    list<User_Persistence__c> userPersistenceList = SalesIQUtility.getUserPersistenceByUserID(UserInfo.getUserId());
    if(userPersistenceList != null && userPersistenceList.size() > 0){
        if(userPersistenceList[0].Workspace_Country__c!=null){
          userPersistanceCountryId = userPersistenceList[0].Workspace_Country__c;
        }
    }
    //SET COUNTRY ID FROM USER PERSISTANCE ON CREATING NEW WORKSPACE
    if( userPersistanceCountryId!=null ){ 
      for(Workspace__c ws : trigger.new){
          //ADDED CHECK IF THE WORKSPACE IN CUSTOMER EXCLUSION 
          if( !ws.Country__r.Enable_Account_Exclusion__c && !ws.isUniversal__c ){
            ws.Country__c = userPersistanceCountryId;   
          }
      }
    }
    //CHECK DUPLICATE WORKSPACE NAME
    Boolean isDuplicate = ScenarioTriggerHandler.checkDuplicateWorkspaceName(userPersistanceCountryId,workspaceCountryId,workspaceName);
    if(isDuplicate){
        workspace.addError(system.Label.Workspace_Duplicate_Name);
    }
  }
}