trigger InquiryCommentTrigger on Inquiry_Comment__c (before insert, before update, before delete) {

	if(TriggerContol__c.getValues('InquiryCommentTrigger') != null && TriggerContol__c.getValues('InquiryCommentTrigger').IsStopTrigger__c) return ;
	
    Set<Id> inquiryIds = new Set<Id>();
    List<Inquiry_Comment__c> inquiryComments ;
    if(trigger.isDelete)
    	inquiryComments = trigger.old ;
    else
    	inquiryComments = trigger.new ;

    for(Inquiry_Comment__c commentObj : inquiryComments){
    	inquiryIds.add(commentObj.Inquiry__c);
    }

    Map<Id, Inquiry__c> relatedInquiries = InquiryUtility.getInquiryDetails(inquiryIds);

    for(Inquiry_Comment__c commentObj : inquiryComments) {
		Inquiry__c parentInquiry = relatedInquiries.get(commentObj.Inquiry__c) ;
    	if(parentInquiry.Status__c == SalesIQGlobalConstants.INQUIRY_STATUS_CLOSED || parentInquiry.Status__c == SalesIQGlobalConstants.INQUIRY_STATUS_CANCELLED)
    		commentObj.addError(String.format(System.Label.Comment_not_allowed_to_change, new List<String>{parentInquiry.Status__c}));
    }
}