trigger ProductTrigger on Product__c (before insert, before delete) {
	if(TriggerContol__c.getValues('TeamInstanceProductTrigger') != null && TriggerContol__c.getValues('TeamInstanceProductTrigger').IsStopTrigger__c) return ;
	if(trigger.isDelete){
	    ProductManagementUtility.removeTeamInstanceProducts(trigger.old);
	}

	if(Trigger.isInsert && Trigger.isBefore){
	    set<string> productNames = new set<string>();
	    for(Product__c pr : trigger.new){
	      productNames.add(pr.Name.toLowerCase());
	    }
	    set<string> existingProduct = new set<string>();
	    for(Product__c prd : [select id, Name from Product__c where Name in : productNames]){
	      existingProduct.add(prd.Name.toLowerCase());
	    }
	    if(existingProduct.size() > 0){
	      for(Product__c prd : trigger.new){
	        if(existingProduct.contains(prd.Name.toLowerCase())){
	          prd.addError('Product with this name already exists');
	        }
	      }
	    }
	}
}