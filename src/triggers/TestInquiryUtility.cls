@isTest
private class TestInquiryUtility {
    
	static testMethod void TestInquiry() {
	    Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
        List<User> userlst = new List<User>();
        User objUser = new User(Alias = 'Test', Email='testadmin@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = p.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='testadmin@testorg.com');
        userlst.add(objUser);
        
        User objManager = new User(Alias = 'Test2', Email='testmanager@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName='Manager', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = p.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='testmanager@testorg.com');
        userlst.add(objManager);
        insert userlst;
        
        
        
        
        List<Employee__c> lstEmp = new List<Employee__c>();

        Inquiry_SLA_Setting__c setting = new Inquiry_SLA_Setting__c();
        setting.Resolution_Days_Count__c = 5;
        setting.name = 'inquirysetting';
        setting.Priority__c = 'High';
        setting.Category__c = 'Position';
        setting.Sub_Category__c = 'Call Plan';
        setting.Profile_Name__c = 'System Administrator';
        insert setting;
        
        
        
        Inquiry__c inq1 = new Inquiry__c();
        inq1.status__c = SalesIQGlobalConstants.INQUIRY_STATUS_OPEN;
        inq1.OwnerId = UserInfo.getUserId();
        inq1.Priority__c = 'High';
        inq1.Category__c = 'Position';
        inq1.Sub_Category__c = 'Call Plan';
        inq1.Previous_Assignee__c = UserInfo.getUserId();
        inq1.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get(SalesIQGlobalConstants.INQUIRY_RT_OPEN).getRecordTypeId(); 
        //insert inq1;
        
        
        Employee__c manager = new Employee__c();
	    manager.isSalesforceUser__c = True;
	    manager.user__c = UserInfo.getUserId();
	    manager.FirstName__c = 'emp';
	    manager.Last_Name__c = 'lastName';
	    lstEmp.add(manager);
	    
	    
	    Employee__c emp = new Employee__c();
	    emp.isSalesforceUser__c = True;
	    emp.user__c = UserInfo.getUserId();
	    emp.FirstName__c = 'firstName';
	    emp.Last_Name__c = 'LastName';
	    //emp.Manager__r.User__c = UserInfo.getUserId();
	    lstEmp.add(emp);
	    
	    insert lstEmp;
	    
	    	test.startTest();
	    	    InquiryUtility objConst = new InquiryUtility();
	    	    InquiryUtility.assignEmployee();
	    	    InquiryUtility.assignDate(inq1);
	    	    InquiryUtility.getRecordType(SalesIQGlobalConstants.INQUIRY_RT_OPEN);
	    	    InquiryUtility.getInquiryHOQueue();
	    	    InquiryUtility.getInquiryHOQueueMembers();
	    	    InquiryUtility.getSystemAdminProfile();
	    	test.stopTest();
	    
	    
	    
	}

}