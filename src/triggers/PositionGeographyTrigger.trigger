/*******************************************************************************************************
Name            :       PositionGeographyTrigger
Description     :       Trigger on Position_Geography__c object.
                        Aggregate Extents on Position Team Instance (Territory level) on ZIP movement.
                        Update Geography string on Position Team Instance on ZIP movement.
Last Updated By :       Raghvendra Rathore - 29/02/2016
*******************************************************************************************************/

trigger PositionGeographyTrigger on Position_Geography__c (after delete, after insert, after undelete, after update) {
    //if value of IsStopTrigger__c is false then trigger won't execute
    if(TriggerContol__c.getValues('PositionGeographyTrigger') != null && !TriggerContol__c.getValues('PositionGeographyTrigger').IsStopTrigger__c){    
        
        //Update extents on Position Team Instance on Position Geography Insert
        if(trigger.isInsert || trigger.isUnDelete){
            System.debug('--insert called');
            /* This condition is added for Talend. when talend puts data , it will make Restrict_Position_Geography_Trigger__c
               field true and the following conditions will not execute then.
            */
            if(!trigger.new[0].Restrict_Position_Geography_Trigger__c){
	            set<Id> posGeoIds = new set<Id>();
	            for(Position_Geography__c posgeoObj :  trigger.new){
	                posGeoIds.add(posgeoObj.Id);
	            }
	            if(bulktriggerStop.stopRun!=true)
	             HandlerPosGeo.rollupExtentsOnTerritoryFuture(posGeoIds);
            }
        }
        //Update extents on Position Team Instance on Position Geography Update    
        if(trigger.isUpdate){
            Team_Instance__c selectedTeamInstance = [select Alignment_Period__c from Team_Instance__c where Id =: trigger.new[0].Team_Instance__c limit 1];
            System.debug('-- old map ' + trigger.oldMap);
            // Update trigger should not be initiated in case only shared columns are getting updated because there is no need to calculate extents again.
            boolean doUpdate = true;
            for(Position_Geography__c pos : trigger.new){
	            /* This condition is added for Talend. when talend update Restrict_Position_Geography_Trigger__c 
	               trigger should not work
	            */
	           if(trigger.oldMap.get(pos.Id).Restrict_Position_Geography_Trigger__c != pos.Restrict_Position_Geography_Trigger__c){
                  doUpdate = false;
                  break;
               }
               
               
               //do not call rollupExtentsOnTerritoryFuture when position geography is updated for future team instance 
               	if(selectedTeamInstance.Alignment_Period__c == SalesIQGlobalConstants.FUTURE_TEAM_CYCLE_TYPE){
               		doUpdate = false;
               	}
            }

            
            if(doUpdate){
                set<Id> posTeaInsIds = new set<Id>();
                set<Id> positionIds = new set<Id>();
    
                for(String keyPosGeo : trigger.oldMap.keyset()){
                    if(trigger.oldMap.get(keyPosGeo).Position_Id_External__c!=null){
                        posTeaInsIds.add(trigger.oldMap.get(keyPosGeo).Position_Team_Instance__c);
                        positionIds.add(trigger.oldMap.get(keyPosGeo).Position_Id_External__c);
                    }
                }
                System.debug('--posTeaInsIds ' + posTeaInsIds);
                System.debug('--positionIds ' + positionIds);
    
                list<set<Id>> posGeosList = new list<set<Id>>();
                posGeosList.add(posTeaInsIds);
                posGeosList.add(positionIds);
    
                HandlerPosGeo.rollupExtentsOnTerritory(trigger.oldMap.keyset(), posGeosList);
                //HandlerPosGeo.rollupExtentsOnTerritoryFuture(trigger.oldMap.keyset());
            }
        }
        //Update extents on Position Team Instance on Position Geography Delete
        if(trigger.isDelete){
            
            //Updated the call to non-Future method to avoid exception while deleting PosGeo in ZIP sharing delete PosGeo workFlow. PosGeos are deleted via 
            //the Batch, which inturn cannot call future method
            
            set<Id> posTeaInsIds = new set<Id>();
            set<Id> positionIds = new set<Id>();

            for(String keyPosGeo : trigger.oldMap.keyset()){
                if(trigger.oldMap.get(keyPosGeo).Position_Id_External__c!=null){
                    posTeaInsIds.add(trigger.oldMap.get(keyPosGeo).Position_Team_Instance__c);
                    positionIds.add(trigger.oldMap.get(keyPosGeo).Position_Id_External__c);
                }
            }
            System.debug('--posTeaInsIds ' + posTeaInsIds);
            System.debug('--positionIds ' + positionIds);

            list<set<Id>> posGeosList = new list<set<Id>>();
            posGeosList.add(posTeaInsIds);
            posGeosList.add(positionIds);

            HandlerPosGeo.rollupExtentsOnTerritory(trigger.oldMap.keyset(), posGeosList);
        }

    }
}