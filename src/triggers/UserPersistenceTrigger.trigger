/**************************************************************************************************************
* Name          :   UserPersistenceTrigger
* Description   :   Apex Trigger for User_Persistence__c
                    Create User and its default country records. 
* Author        :   Gaurav
* Since         :   12/01/2018
**************************************************************************************************************/
trigger UserPersistenceTrigger on User_Persistence__c (before insert, before update) {
    
    if(Trigger.isInsert){
      system.debug('######### Before Insert');
      User_Persistence__c userPeristObj = new User_Persistence__c();
      Boolean countryAccess;      
        for(User_Persistence__c userPerist : trigger.new){
            userPeristObj = userPerist;           
        }
        system.debug('########### userPeristObj ' + userPeristObj); 
        //First check whether the user record exist or not.
        list<User_Persistence__c> userPerst = checkUserPeristenceExist(userPeristObj.User__c);
        if(userPerst != null && userPerst.size() >0){
         trigger.new[0].addError(System.Label.User_Already_Exist);
       }else{
        //Else Check whether User has access to Country.
        if(!string.isBlank(userPeristObj.Default_Country__c)){
            countryAccess = checkCountryInSharingSetting(new list<string>{userPeristObj.Default_Country__c},userPeristObj.User__c);
              system.debug('########### countryAccess ' + countryAccess); 
              if(countryAccess == false){
                  system.debug('########### You do not have access to this country ' + trigger.new[0]); 
                  trigger.new[0].addError(System.Label.No_Country_Access);
              }
            
        }
          
        }
    }
    
    if(trigger.isUpdate){
      User_Persistence__c userPeristObj = new User_Persistence__c();
      Boolean countryAccess;
        for(User_Persistence__c userPerist : trigger.new){
            userPeristObj = userPerist;
        }
        if(trigger.oldMap.get(trigger.new[0].id).Default_Country__c != trigger.new[0].Default_Country__c ){  
           countryAccess = checkCountryInSharingSetting(new list<string>{userPeristObj.Default_Country__c},userPeristObj.User__c);
           if(countryAccess == false){
             trigger.new[0].addError(System.Label.No_Country_Access);
           }
        }else if(trigger.oldMap.get(trigger.new[0].id).User__c != trigger.new[0].User__c){   
             list<User_Persistence__c> userPerst = checkUserPeristenceExist(userPeristObj.User__c);
             if(userPerst != null && userPerst.size()>0){
              trigger.new[0].addError(System.Label.User_Already_Exist);
             }
         }
    }
 //This method will check whether the default country user is inserting or updating is the sharing rule.   
 private Boolean checkCountryInSharingSetting(list<string> defaultCountry , string userID){
  system.debug('############## Inside checkCountryInSharingSetting ' + userID);
    //Groups directly associated to user
  Set<Id> groupwithUser = new Set<Id>();
  
  //Populating the Group with User with GroupId we are filtering only  for Group of Type Regular,Role and RoleAndSubordinates
  for(GroupMember  u :[select groupId from GroupMember where UserOrGroupId =:userID])
  {
    groupwithUser.add(u.groupId);
    }
    system.debug('############# groupwithUser ' + groupwithUser);
    system.debug('############# defaultCountry ' + defaultCountry);
    //list<Country__Share> sharedCountries = new list<Country__Share>();

    string soql = 'SELECT Id, ParentId FROM Country__Share where UserOrGroupId in : groupwithUser and ParentId in : defaultCountry'; 
    //sharedCountries = database.query(soql);
    List<SObject> objectRecords = database.query(soql);


    //system.debug('############### sharedCountries ' + sharedCountries);
    system.debug('############### sharedCountries ' + objectRecords);

     list<country__c> defaultCountryRecord =  new  list<country__c>();
    //if(sharedCountries !=null && sharedCountries.size()>0){
    if(objectRecords !=null && objectRecords.size()>0){
        defaultCountryRecord = SalesIQUtility.getCountryByCountryId(defaultCountry);
    }
     system.debug('######## defaultCountryRecord ' + defaultCountryRecord);
     if(defaultCountryRecord != null && defaultCountryRecord.size() > 0){
        return true;
     }else{
        return false;
     }
  } 
 
 //This method will check whether user exist in the User Persistence or not. 
 private list<User_Persistence__c> checkUserPeristenceExist(string userID){
    list<User_Persistence__c> userPerst = SalesIQUtility.getUserPersistenceByUserID(userID);    
    return userPerst;
  }
}