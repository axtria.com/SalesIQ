/**************************************************************************************************************
* Name          :   ChangeRequestTrigger
* Description   :   Apex Trigger for Change_Request__c
                    Create Change Request Approver Detail records for all the Approvers for the Request. 
* Author        :   Raghvendra Rathore
* Since         :   4-Dec-2015
* Version       :   1.0
**************************************************************************************************************/
trigger ChangeRequestTrigger on Change_Request__c (after delete, after insert, after update, before insert) {
    
    //If value of IsStopTrigger__c is false then trigger won't execute
    if(TriggerContol__c.getValues('ChangeRequestTrigger') != null && TriggerContol__c.getValues('ChangeRequestTrigger').IsStopTrigger__c) return ;
    
    // before insert code added to assign country in cr before insert --- multi country support functionality    
    if(trigger.isBefore && trigger.isInsert){
       
        for(Change_Request__c cr : trigger.new){
            if(String.isBlank(cr.Country__c)){
                Team_Instance__c teamInstanceRecord = SalesIQUtility.getTeamInstanceById(cr.Team_Instance_ID__c);
                if(teamInstanceRecord != null){
                    cr.Country__c = teamInstanceRecord.Team__r.Country__c;
                }
            }
            System.debug('change request before insert ---- '+cr);
        }
       
    }
    else if(trigger.isAfter){
      
    
    if(trigger.isInsert){
        try{
            list<SalesIQ_Logger__c> lstLogger = new list<SalesIQ_Logger__c>();  
            
            boolean doSyncWithESRIServer = true;
            boolean fireTrigger = false;
            string RecordTypeId='';
            string RequestStatus='';
            string RequestType='';
            Id TeamInstanceId;
            boolean isFutureActive = false;
            string countryID = '';
            string movedZIPs='';
            string executionStatus = '';
            
            list<string> reqIds = new list<string>();
            set<Id> crIdsSet = new set<Id>();
            for(Change_Request__c cR : trigger.new){
                //Check on which type of request Trigger needs to be fire
                reqIds.add(cR.id);
                crIdsSet.add(cR.id);
                RecordTypeId = cR.RecordTypeID__c;
                RequestStatus = cR.Status__c;
                RequestType  = cR.Request_Type_Change__c;
                isFutureActive = (cR.Change_Effective_Date__c > system.today())?true:false;
	            TeamInstanceId = cR.Team_Instance_ID__c;
	            countryID   = cR.Country__c;
                executionStatus = cR.Execution_Status__c;
                if(cR.Request_Type_Change__c == SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP ||cR.Request_Type_Change__c==SalesIQGlobalConstants.MOVEMENT_TYPE_ACCOUNT){
                    fireTrigger = true;
                }
                
                if(cR.Request_Type_Change__c == SalesIQGlobalConstants.CREATE_POSITION || cR.Request_Type_Change__c == SalesIQGlobalConstants.CREATE_OVERLAY_POSITION || cR.Request_Type_Change__c == SalesIQGlobalConstants.UPDATE_POSITION_ATTRIBUTES || cR.Request_Type_Change__c == SalesIQGlobalConstants.UPDATE_POSITION || cR.Request_Type_Change__c == SalesIQGlobalConstants.UPDATE_OVERLAY_POSITION || cR.Request_Type_Change__c == SalesIQGlobalConstants.DELETE_POSITION || cR.Request_Type_Change__c == SalesIQGlobalConstants.EMPLOYEE_ASSIGNMENT || RequestType == SalesIQGlobalConstants.HIERARCHY_CHANGE || RequestType == SalesIQGlobalConstants.HIERARCHY_CHANGE_OVERLAY){
                    fireTrigger = false;
                    doSyncWithESRIServer = false;
                }
                
                if(cR.Request_Type_Change__c == SalesIQGlobalConstants.MOVEMENT_TYPE_ACCOUNT){
                    doSyncWithESRIServer = false;
                    fireTrigger = true;
                }
            }
            system.debug('RequestType '+RequestType);
            system.debug('executionStatus '+executionStatus);
            if(RequestType==SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP && (executionStatus == SalesIQGlobalConstants.BULK_CR_NEW || executionStatus==SalesIQGlobalConstants.BULK_CR_COMPLETED || executionStatus==SalesIQGlobalConstants.BULK_CR_ERROR_OCCURED)){
                
                lstLogger.add(SalesIQLogger.prepareSalesIQLoggerData(RequestType +' Started' ,'', RequestType,reqIds[0] ));
                insert lstLogger;
                Database.executeBatch(new CreateBulkMovementRequestChange(reqIds));  
                
            
            }           
            
            
            if(fireTrigger){
                string alignmentPeriod = SalesIQUtility.getTeamInstanceById(TeamInstanceId).Alignment_Period__c;
                system.debug('*************** Inside IF ChangeRequestTrigger ' + RequestType);
                if(RequestType != SalesIQGlobalConstants.MOVEMENT_TYPE_CALL_PLAN && RequestType!=SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP){
                    system.debug('************ Request Type not equal to Call Plan ' + RequestType);
                    ChangeRequestTriggerHandler.createApproverDetails(trigger.new);
                    ChangeRequestTriggerHandler.CreateCIMChangeRequestImpact(trigger.new, reqIds);
                    
                    if(trigger.new[0].Request_Type_Change__c != SalesIQGlobalConstants.MOVEMENT_TYPE_CALL_PLAN){
                        //Added new if condition because in case of pending request "updateRequestSummary" was not calling
                        //now updateRequestSummary method will always call in case of Current Team Cycle and Current Date
                        if(trigger.new[0].Status__c == SalesIQGlobalConstants.REQUEST_STATUS_APPROVED){
                            /*if there is a rules mismatch between source and destination position then
                             Esri syncing job will not be called - BRMS Job would sync the sfdc and esri data*/
                            if(trigger.new[0].Position_Rules_Mismatch__c=='true' && trigger.new[0].Rule_Execution_Status__c=='Pending')
                                doSyncWithESRIServer =false;
                            
                            ChangeRequestTriggerHandler.ApproveRejectRequest(reqIds,trigger.new[0],SalesIQGlobalConstants.REQUEST_STATUS_APPROVED,0);
                            ChangeRequestTriggerHandler.callPythonCentroidService(reqIds);
                            
                        }
                        
                        if(!(alignmentPeriod == SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE && isFutureActive)){
                        	ChangeRequestTriggerHandler.updateRequestSummary(trigger.new, reqIds);
                        }
                    }
                    movedZIPs = ChangeRequestTriggerHandler.getZIPString(trigger.new[0]);
                    if(!string.isBlank(movedZIPs) && !System.isBatch()){
                        //string customMetricValue  = SalesIQUtility.getCustomMetric(trigger.new[0].Source_Position__c,trigger.new[0].Destination_Position__c,TeamInstanceId, movedZIPs, '',trigger.new[0].Status__c,trigger.new[0], trigger.new[0].Request_Type_Change__c,SalesIQLogger.ALIGNMENT_MODULE);
                        AlignmentUtility.customMetricCallOut(trigger.new[0].Source_Position__c,trigger.new[0].Destination_Position__c,trigger.new[0].Status__c,movedZIPs,TeamInstanceId, trigger.new[0].Request_Type_Change__c,'Alignment');
                        /*if(!customMetricValue.contains('table')){
                            SalesIQLogger.logErrorMessage(customMetricValue, SalesIQLogger.CHANGE_REQUEST);
                        }*/
                    }

                    if(doSyncWithESRIServer){
                        Id jobId = System.System.enqueueJob(new QueueESRIBatchProcess(reqIds));
                    }

                    
                                       
                }
            }
            if(trigger.new[0].Request_Type_Change__c != SalesIQGlobalConstants.MOVEMENT_TYPE_CALL_PLAN  && RequestType!=SalesIQGlobalConstants.MOVEMENT_TYPE_BULK_ZIP){
                ChangeRequestTriggerHandler.updateCRStatusOnPosition(trigger.new,new map<Id,Change_Request__c>());
            }
           
        }
        catch(SecurityUtil.SecurityException ex){
            trigger.new[0].addError(ex.getMessage());
        }
        catch(exception ex){
            // This trigger not handled for bulk, so adding error on first record only.
            if(ex.getStackTraceString().contains('()') )
                trigger.new[0].addError(ex.getMessage());
            else    
                trigger.new[0].addError(ex.getStackTraceString());
        }
    }
        
    else if(trigger.isUpdate){
        try{
        	boolean fireTrigger = false;
    		
    		for(Change_Request__c req : trigger.new){
    			if(trigger.oldMap.get(req.Id).Status__c != req.Status__c){
            		fireTrigger = true;
            	}
    		}
        	if(fireTrigger){
	            //system.assertEquals('update called', 'y') ;   
	            boolean doSyncWithESRIServer = true;
	            Id TeamInstanceId;
	            Change_Request__c ChngReq = new Change_Request__c();
	            set<id> RosterRequestId = new set<id>();
	            string reqStatus;            
	            list<string> reqIds = new list<string>();
	            set<Id> crIdsSet = new set<Id>();
	            string RequestType='';
	            string RequestStatus =''; 
	            boolean isFutureActive = false;
                string movedZIPs='';
	            
	            list<Change_Request__c> callPlanRequests = new list<Change_Request__c>();
	            
	            list<Change_Request__c> rosterChangeRequestList = new list<Change_Request__c>();
	            for(Change_Request__c req : trigger.new){
	                ChngReq = req;
	                reqIds.add(req.id);
	                crIdsSet.add(req.Id);
	                RequestType = req.Request_Type_Change__c;               
	                RequestStatus = req.Status__c;
	                isFutureActive = (req.Change_Effective_Date__c > system.today())?true:false;
	                TeamInstanceId = req.Team_Instance_ID__c;
	                
	                if(req.Request_Type_Change__c != SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP){
	                    doSyncWithESRIServer = false;
	                }
	                 
	                if(req.Status__c != SalesIQGlobalConstants.REQUEST_STATUS_PENDING && (req.Request_Type_Change__c == SalesIQGlobalConstants.CREATE_OVERLAY_POSITION || req.Request_Type_Change__c == SalesIQGlobalConstants.UPDATE_OVERLAY_POSITION )){
	                    doSyncWithESRIServer = true;
	                }
	                //ESRI Syncing is required if ZIPs and Accounts are moving to unassigned territory (Cascade delete) while deleting position
	                if(req.Status__c != SalesIQGlobalConstants.REQUEST_STATUS_PENDING && req.Request_Type_Change__c == SalesIQGlobalConstants.DELETE_POSITION && req.AllZips__c != null && req.AllZips__c != ''){
	                    doSyncWithESRIServer = true;
	                }
	
	                if(req.Status__c != SalesIQGlobalConstants.REQUEST_STATUS_PENDING && (req.Request_Type_Change__c == SalesIQGlobalConstants.CREATE_POSITION || req.Request_Type_Change__c == SalesIQGlobalConstants.CREATE_OVERLAY_POSITION || req.Request_Type_Change__c == SalesIQGlobalConstants.UPDATE_POSITION_ATTRIBUTES || req.Request_Type_Change__c == SalesIQGlobalConstants.UPDATE_POSITION || req.Request_Type_Change__c == SalesIQGlobalConstants.UPDATE_OVERLAY_POSITION || req.Request_Type_Change__c == SalesIQGlobalConstants.DELETE_POSITION || req.Request_Type_Change__c == SalesIQGlobalConstants.EMPLOYEE_ASSIGNMENT || req.Request_Type_Change__c == SalesIQGlobalConstants.HIERARCHY_CHANGE || req.Request_Type_Change__c == SalesIQGlobalConstants.HIERARCHY_CHANGE_OVERLAY)){                 
	                    RosterRequestId.add(req.id);   
	                    rosterChangeRequestList.add(req);                    
	                }
	                
	                else if(req.Status__c != SalesIQGlobalConstants.REQUEST_STATUS_PENDING && trigger.oldMap.get(req.Id).Status__c != req.Status__c && (req.Request_Type_Change__c == SalesIQGlobalConstants.MOVEMENT_TYPE_ACCOUNT || req.Request_Type_Change__c == SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP) ){
	                    reqStatus = req.Status__c;
	                }
	            }

                Team_Instance__c objTeamInstance =  SalesIQUtility.getTeamInstanceById(TeamInstanceId);
	                
	            string alignmentPeriod = objTeamInstance.Alignment_Period__c;
                string assignmentStatus = SalesIQGlobalConstants.POSITION_GEOGRAPHY_FUTURE_ACTIVE;                

                if(objTeamInstance.Alignment_Period__c == SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE){
                    assignmentStatus = SalesIQGlobalConstants.POSITION_GEOGRAPHY_ACTIVE;
                }
	                
	            //Roster case
	            if(RosterRequestId != null && RosterRequestId.size() > 0){
	                ChangeRequestTriggerHandler.checkRosterRequest(rosterChangeRequestList, RosterRequestId, RequestType, RequestStatus);
	                
	                system.debug('#### alignmentPeriod : '+alignmentPeriod);
	                system.debug('#### isFutureActive : '+isFutureActive);
	                system.debug(Logginglevel.ERROR,'#### RequestType : '+RequestType);
	                //Sync ESRI with future active hierarchy changes approved request
	                if((!(alignmentPeriod == SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE && isFutureActive)) && RequestStatus == SalesIQGlobalConstants.REQUEST_STATUS_APPROVED){
	                    if(RequestType == SalesIQGlobalConstants.HIERARCHY_CHANGE || RequestType == SalesIQGlobalConstants.HIERARCHY_CHANGE_OVERLAY){
	                        system.debug('#### Invoking ProcessFutureZIPAssignmentBatch');
	                        string jobId = database.executeBatch(new ProcessFutureZIPAssignmentBatch(RosterRequestId,RequestType,alignmentPeriod),100);
	                    }
	                }
	            }
	             
                system.debug(Logginglevel.ERROR,'#### reqIds : '+reqIds);
	            if(reqIds != null && reqIds.size() > 0){
                    id submissionPendingdestination = ChngReq.Destination_position__c;
                     integer iCountSubmissionPending=0;
	                if(RequestType == SalesIQGlobalConstants.MOVEMENT_TYPE_ACCOUNT || RequestType == SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP){
	                    system.debug('*****ApproveRejectRequest function called' +RequestType);                       

                        //SIQ-937                                                
                        //below will be used for Submission Pending case. When User going to reject any CR
                        //id submissionPendingdestination = ChngReq.Destination_position__c;
                        system.debug('submissionPendingdestination '+submissionPendingdestination);
                        system.debug('assignmentStatus '+assignmentStatus);                        

                        iCountSubmissionPending = SalesIQUtility.getCountForSubmissionPending(new set<id>{submissionPendingdestination}, new list<string>{assignmentStatus}, RequestType);                      

                        /*if there is a rules mismatch between source and destination position then
                             Esri syncing job will not be called - BRMS Job would sync the sfdc and esri data*/
                        if(trigger.new[0].Position_Rules_Mismatch__c=='true' && trigger.new[0].Rule_Execution_Status__c=='Pending')
                            doSyncWithESRIServer =false;

	                    list<string> tobeUpdatedSPPosGeo = ChangeRequestTriggerHandler.ApproveRejectRequest(reqIds,ChngReq,reqStatus,iCountSubmissionPending);
                        system.debug('tobeUpdatedSPPosGeo '+tobeUpdatedSPPosGeo);
	                    ChangeRequestTriggerHandler.updateRequestSummary(trigger.new,reqIds);
	                    if(!(alignmentPeriod == SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE && isFutureActive)){
                            ChangeRequestTriggerHandler.processAccountAlignmentType(crIdsSet);
	                    }

                        movedZIPs = ChangeRequestTriggerHandler.getZIPString(trigger.new[0]);
                        //Used System.isBatch() because when ProcessAccountExclusion Batch run. and when we reject the CR from there
                        //This trigger gets called. As a result of this we cann't call future callout from triiger becasue this trigger execute
                        //becasue of Batch. SO below code should not execute if trigger called from batch
                        if(!string.isBlank(movedZIPs) && !System.isBatch()){
                            AlignmentUtility.customMetricCallOut(trigger.new[0].Source_Position__c,trigger.new[0].Destination_Position__c,trigger.new[0].Status__c,movedZIPs,TeamInstanceId, trigger.new[0].Request_Type_Change__c,'Request');
                        }
	                
                        if(doSyncWithESRIServer ){
                            //SIQ-397
                            //If Send_Back_to_Revision__c is true and no SP position geography record found, that sending CR to SP mode on
                            system.debug('iCountSubmissionPending '+iCountSubmissionPending);
                            system.debug('RequestType '+RequestType);
                            system.debug('objTeamInstance.Send_Back_to_Revision__c '+objTeamInstance.Send_Back_to_Revision__c);
                            if(trigger.new[0].Status__c!=SalesIQGlobalConstants.REQUEST_STATUS_APPROVED  && objTeamInstance.Allow_Submission_Pending__c==true  && iCountSubmissionPending==0 && RequestType == SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP && objTeamInstance.Send_Back_to_Revision__c ){
                                System.debug('##########updating Position Change status');
                                Id jobId = System.System.enqueueJob(new QueueESRIBatchProcess(tobeUpdatedSPPosGeo,new list<String>(),true,SalesIQGlobalConstants.REQUEST_STATUS_SUBMISSION_PENDING,''));
                            }else{
                                Id jobId = System.System.enqueueJob(new QueueESRIBatchProcess(reqIds));
                            }
                        }
                        ChangeRequestTriggerHandler.callPythonCentroidService(reqIds);
	                }

                    if(doSyncWithESRIServer && (RequestType == SalesIQGlobalConstants.CREATE_OVERLAY_POSITION || RequestType == SalesIQGlobalConstants.UPDATE_OVERLAY_POSITION)){
                        Id jobId = System.System.enqueueJob(new QueueESRIBatchProcess(reqIds));
                    }

                    system.debug(Logginglevel.ERROR,'#### doSyncWithESRIServer : '+doSyncWithESRIServer);
                    if(doSyncWithESRIServer && RequestType == SalesIQGlobalConstants.DELETE_POSITION && objTeamInstance.IsCascadeDeleteAssignments__c){
                        system.debug(Logginglevel.ERROR,'#### Sync with esri for cascade delete');
                        Id jobId = System.System.enqueueJob(new QueueESRIBatchProcess(reqIds));
                    }
	            
	                if(trigger.new[0].Request_Type_Change__c != SalesIQGlobalConstants.MOVEMENT_TYPE_CALL_PLAN ){
                        if((iCountSubmissionPending==0 || iCountSubmissionPending>0) && objTeamInstance.Allow_Submission_Pending__c==true
                            &&  (trigger.new[0].Request_Type_Change__c == SalesIQGlobalConstants.MOVEMENT_TYPE_ACCOUNT || trigger.new[0].Request_Type_Change__c == SalesIQGlobalConstants.MOVEMENT_TYPE_ZIP)  
                            && (RequestStatus == SalesIQGlobalConstants.REQUEST_STATUS_REJECTED || RequestStatus ==SalesIQGlobalConstants.REQUEST_STATUS_CANCELLED)) {
                            //SIQ-937
                            //If during reject config are set then we'll update the status of destination psoition to SP state
                            SalesiqUtility.updatePositionChangeStatus(new List<id>{submissionPendingdestination},SalesIQGlobalConstants.REQUEST_STATUS_SUBMISSION_PENDING);
                        }else{
                            ChangeRequestTriggerHandler.updateCRStatusOnPosition(trigger.new,trigger.oldMap);
                        }
                        
                        
	                }
        	    }
            }
        }catch(SecurityUtil.SecurityException ex){
            trigger.new[0].addError(ex.getMessage());
        }
        catch(exception ex){
            if(ex.getStackTraceString().contains('()') )
                trigger.new[0].addError(ex.getMessage());
            else    
                trigger.new[0].addError(ex.getStackTraceString());
        }  
    }
        
    if(TriggerContol__c.getValues('CallPlanChangeRequestTrigger') != null && !TriggerContol__c.getValues('CallPlanChangeRequestTrigger').IsStopTrigger__c){
        //Call plan related execution
        if(trigger.isInsert){ 
            try{
                system.debug('************CallPlanChangeRequestTrigger Insert');
                boolean fireTrigger = false;          
                string RequestType='';
                list<string> reqIds = new list<string>();
                for(Change_Request__c cR : trigger.new){
                    //Check on which type of request Trigger needs to be fire                
                    RequestType  = cR.Request_Type_Change__c;
                    if(RequestType == SalesIQGlobalConstants.MOVEMENT_TYPE_CALL_PLAN || RequestType == SalesIQGlobalConstants.REQUEST_TYPE_CALL_PLAN){ 
                        fireTrigger = true;
                    } 
                }
                if(fireTrigger && trigger.isAfter){ 
                    if(RequestType == SalesIQGlobalConstants.MOVEMENT_TYPE_CALL_PLAN || RequestType == SalesIQGlobalConstants.REQUEST_TYPE_CALL_PLAN){                   
                         //ChangeRequestTriggerHandler.createCRCallPlan(trigger.new);
                        ChangeRequestTriggerHandler.createCRCallPlanNew(trigger.new);
                    }
                }
            }
            catch(SecurityUtil.SecurityException ex){
                trigger.new[0].addError(ex.getMessage());
            }
            catch(exception ex){
                if(ex.getStackTraceString().contains('()') )
                    trigger.new[0].addError(ex.getMessage());
                else    
                    trigger.new[0].addError(ex.getStackTraceString());
            }  
        }
            
        if(trigger.isUpdate){
            try {
                system.debug('************CallPlanChangeRequestTrigger Update');
                Id TeamInstanceId;
                Change_Request__c ChngReq = new Change_Request__c();
                set<id> RosterRequestId = new set<id>();
                string reqStatus;            
                list<string> reqIds = new list<string>();
                string RequestType='';
                string RequestStatus =''; 
                
                list<Change_Request__c> callPlanRequests = new list<Change_Request__c>();
                
                for(Change_Request__c req : trigger.new){
                    ChngReq = req;
                    reqIds.add(req.id);
                    RequestType = req.Request_Type_Change__c;               
                    RequestStatus = req.Status__c; 
                    if(req.Status__c != 'Pending' && trigger.oldMap.get(req.Id).Status__c != req.Status__c && req.Request_Type_Change__c == SalesIQGlobalConstants.REQUEST_TYPE_CALL_PLAN){
                        system.debug('In call plan change function');                   
                        if(req.status__c == 'Approved' || req.status__c == 'Rejected'|| req.status__c == 'Cancelled' || req.status__c ==  'Recalled'){                        
                            callPlanRequests.add(req);
                        }                
                    } 
                } 
                //Kindly do not commnet this line   callPlanRequests
                if(callPlanRequests!=null && callPlanRequests.size() > 0){
                    System.debug('callPlanRequests --- '+callPlanRequests);
                    ChangeRequestTriggerHandler.processCallPlanRequest(callPlanRequests);
                    //ChangeRequestTriggerHandler.updatePositionAccountCallPlan(callPlanRequests);
                } 
            }
            catch(SecurityUtil.SecurityException ex){
                trigger.new[0].addError(ex.getMessage());
            }
            catch(exception ex){
                if(ex.getStackTraceString().contains('()') )
                    trigger.new[0].addError(ex.getMessage());
                else    
                    trigger.new[0].addError(ex.getStackTraceString());
            }
        }                 
    }
}
}