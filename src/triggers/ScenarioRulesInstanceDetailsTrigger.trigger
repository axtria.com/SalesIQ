trigger ScenarioRulesInstanceDetailsTrigger on Scenario_Rule_Instance_Details__c(after update) 
{
	if(trigger.isUpdate)
	{
		Set<Id> scIdList = new Set<Id>();
		Set<Id> errorScIdList = new Set<Id>();
		for(Scenario_Rule_Instance_Details__c scRuleDetails : trigger.new)
		{
			if(scRuleDetails.Status__c == SalesIQGlobalConstants.ERROR_OCCURED)
				errorScIdList.add(scRuleDetails.Scenario_Id__c);
		}
		
		if(errorScIdList.size() > 0)
		{
			List<Scenario__c> updateErrorScnearioList = new List<Scenario__c> ();
			for(Id updateErrorSc : errorScIdList)
			{
				Scenario__c mysc = new Scenario__c(Id = updateErrorSc,Rule_Execution_Status__c = CPGConstants.ERROR,Run_Response_Details__c = System.Label.Rule_Excecution_Failed);
				updateErrorScnearioList.add(mysc);
			}
			update updateErrorScnearioList;
		}
	}	
}