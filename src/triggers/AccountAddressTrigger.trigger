trigger AccountAddressTrigger on Account_Address__c (after insert,after update, after delete) {
    //FOR EACH ACCOUNT ADDRESSS INSERT,UPDATE AND DELETE LAST MODIFIED DATE ON REALTED ACCOUNT OBJECT.
    List<Account> deltaModifiedAccounts = new List<Account>();
    Set<Id> accountIds = new Set<Id>();
    
    try{
        for(Account_Address__c account_address_obj:trigger.new){
            if(account_address_obj.Account__c != null && !accountIds.contains(account_address_obj.Account__c)){
              deltaModifiedAccounts.add(new Account(Id = account_address_obj.Account__c));
              accountIds.add(account_address_obj.Account__c);
            }
        }
        //DML DUMMTY UPDATE TO MODIFY SYS MOD STAMP AND LAST MODIFIED DATE ON ACCOUNT RECORD
        system.debug ('deltaModifiedAccounts BY AccountAddressTrigger ---+++--' + deltaModifiedAccounts.size() );
        update deltaModifiedAccounts;
    }
    catch(Exception e){
        system.debug(e);
    }
}