trigger InquiryTrigger on Inquiry__c (before update, before insert) {

    if(TriggerContol__c.getValues('InquiryTrigger') != null && TriggerContol__c.getValues('InquiryTrigger').IsStopTrigger__c) return ;

    if(trigger.isUpdate){
        
        Id currentUserManager = InquiryUtility.assignEmployee();
        for(Inquiry__c inqObj : trigger.new){
            
            String previousStatus = Trigger.oldMap.get(inqObj.Id).Status__c;
            String currentStatus = inqObj.Status__c;

            /***********************************************************
            1. Restricting any changes on a Closed and Cancelled Inquiry
            ************************************************************/
            if(previousStatus == SalesIQGlobalConstants.INQUIRY_STATUS_CLOSED || previousStatus == SalesIQGlobalConstants.INQUIRY_STATUS_CANCELLED){
                inqObj.addError(String.format(System.Label.Comment_not_allowed_to_change, new List<String>{previousStatus}));
            }
            
            /***********************************************************
            2. Only record owner is allowed to make any changes on Inquiry
            ************************************************************/
            else if( !(inqObj.OwnerId == UserInfo.getUserId() || (inqObj.OwnerId == InquiryUtility.getInquiryHOQueue() && InquiryUtility.getInquiryHOQueueMembers().contains(UserInfo.getUserId())) )
                    &&!(inqObj.CreatedById == UserInfo.getUserId() && (currentStatus == SalesIQGlobalConstants.INQUIRY_RT_OPEN || currentStatus == SalesIQGlobalConstants.INQUIRY_STATUS_CANCELLED)) )
            { 
                inqObj.addError(System.Label.Inquiry_not_allowed_to_update);     
            }
            

            // if Inquiry is marked closed/cancelled, update the record type
            if( currentStatus == SalesIQGlobalConstants.INQUIRY_STATUS_CLOSED || currentStatus == SalesIQGlobalConstants.INQUIRY_STATUS_CANCELLED ){
                inqObj.RecordTypeId = InquiryUtility.getRecordType(SalesIQGlobalConstants.INQUIRY_RT_CLOSE);
                
            }
            else if(currentStatus == SalesIQGlobalConstants.INQUIRY_STATUS_REOPEN || currentStatus == SalesIQGlobalConstants.INQUIRY_STATUS_RESOLVED ){
                //change approver
                id temp;
                temp = inqObj.OwnerId;
                inqObj.OwnerId = inqObj.Previous_Assignee__c;
                inqObj.Previous_Assignee__c = temp;
            }            
            
        }
    }
    
    
    if(trigger.isInsert){
        
        Id currentUserManager = InquiryUtility.assignEmployee();
        for(Inquiry__c inqObj : trigger.new){
            // ASSIGN A MANAGER
            inqObj.OwnerId = currentUserManager;   
            //CALCULATE ESTIMATED RESOLUTION DATE
            // @todo: Take out this query outside loop
            inqObj.Expected_Resolution_Date__c = InquiryUtility.assignDate(inqObj);
            inqObj.Previous_Assignee__c = UserInfo.getUserId();
            inqObj.Status__C = SalesIQGlobalConstants.INQUIRY_STATUS_OPEN;
            inqObj.RecordTypeId = InquiryUtility.getRecordType(SalesIQGlobalConstants.INQUIRY_RT_OPEN);
            
        }
    }
}