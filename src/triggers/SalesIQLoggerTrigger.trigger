trigger SalesIQLoggerTrigger on SalesIQ_Logger__c (after update, after insert) 
{
    //ON MASTER SYNC ERROR/SUCCESS, RESUME QUEUE EXECUTION 
    for(SalesIQ_Logger__c loggerRecord:trigger.new)
    {
        try
        {

            if( loggerRecord.Status__c == SalesIQGlobalConstants.SUCCESS
                && loggerRecord.Module__c == SalesIQLogger.BRMS_MODULE 
                && loggerRecord.Type__c == CPGConstants.QUEUE_LOGGER_TYPE_MASTER  )
                {
                    Salesiqutility.resumeScenarioExecution();
                  // break;
                }
                else if( loggerRecord.Status__c == SalesIQGlobalConstants.ERROR
                    && loggerRecord.Module__c == SalesIQLogger.BRMS_MODULE 
                    && loggerRecord.Type__c == CPGConstants.QUEUE_LOGGER_TYPE_MASTER  )
                {
                    //RESTORE SCENARIO QUEUE TO SUCCESS
                    Salesiqutility.restoreScenariosOnSyncFail();
                   // break;
                }
                
        }
        catch(Exception e){
            system.debug('Exception in Logger Object');
            system.debug(e);
        }

    }
  
}