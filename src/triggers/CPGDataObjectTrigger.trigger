trigger CPGDataObjectTrigger on Data_Object__c(after insert,after update,before update) 
{
    if(Trigger.isUpdate && Trigger.isAfter) {
        
        // Code block for calculating affilaition network depth 
        for(Data_Object__c dataObject: Trigger.new) {
            if(Trigger.oldMap.get(dataObject.Id).status__c == SalesIQGlobalConstants.IN_PROGRESS
                && dataObject.status__c == SalesIQGlobalConstants.SUCCESS){
                
                String ns=SalesIQGlobalConstants.getOrgNameSpace();
                if(dataObject.Data_Set_Object_Name__c == ns+'Account_Affiliation__c')
                {
                    List<String> sourceColumnList =new List<String>{ns+'Affiliation_Network__c',ns+'Parent_Account__c',ns+'Account__c'};
                    
                    Id recordId = Schema.SObjectType.SalesIQ_Logger__c.getRecordTypeInfosByName().get(CPGConstants.QUEUE_LOGGER_RECORDTYPE).getRecordTypeId();
                    SalesIQ_Logger__c accountNetworkDepthlog = new SalesIQ_Logger__c(Module__c=SalesIQLogger.BRMS_MODULE, Status__c=SalesIQGlobalConstants.IN_PROGRESS, Type__c= CPGConstants.AFFILIATION_NETWORK_DEPTH);
                    accountNetworkDepthlog.RecordtypeId= recordId;
                    insert accountNetworkDepthlog;
                    
                    String networkfield;
                    String parentfield;
                    String accountfield;
                    List<Data_Set_Column_Detail__c> dataSetColumnList=[SELECT Source_Column__c, tb_col_nm__c FROM Data_Set_Column_Detail__c WHERE dataset_id__c=:dataObject.dataset_id__c and Source_Column__c IN :sourceColumnList];    
                    for(Data_Set_Column_Detail__c ds:dataSetColumnList){
                       if(ds.Source_Column__c==ns+'Affiliation_Network__c')
                            networkfield=ds.tb_col_nm__c ;
                       if(ds.Source_Column__c==ns+'Parent_Account__c')
                            parentfield=ds.tb_col_nm__c ;
                       if(ds.Source_Column__c==ns+'Account__c')
                            accountfield=ds.tb_col_nm__c ;
                    }

                    BRScenarioSchedulerHandler.pythonAffliation(dataObject.final_Table__c,networkfield,parentfield,accountfield,accountNetworkDepthlog.id);
                }
            }
        }
    }
}