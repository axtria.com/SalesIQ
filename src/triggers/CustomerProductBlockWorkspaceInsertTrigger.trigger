trigger CustomerProductBlockWorkspaceInsertTrigger on Country__C (after insert, after update) {
    for(Country__c country : trigger.new){
        system.debug('CustomerProductBlockWorkspaceInsertTrigger '+country + '  country.Enable_Account_Exclusion__c ' +country.Enable_Account_Exclusion__c);
        if(country.Enable_Account_Exclusion__c){
    	   ScenarioTriggerHandler.insertCustomerBlockWorkspace(country.id);
        }
    } 	
}