trigger ScenarioTrigger on Scenario__c (after update, after delete,before update, before insert) {
    system.debug('scenarioTrigger Fired');
    /*check if Scenario trigger is restricted to be executed, skip the execution*/
    if(TriggerContol__c.getValues('ScenarioTrigger') != null && TriggerContol__c.getValues('ScenarioTrigger').IsStopTrigger__c) return ;
    
    String scenarioType;
    String previousScenarioStatus;
    String updatedScenarioStatus;
    String scenarioId;
    String templateId;
    String scenarioSource = '';
    Id sourceTeamInstanceId;

    if(Trigger.isAfter && Trigger.isUpdate)
    {
        Scenario__c scenario;
        Set<Id> scenarioIdSet = new Set<Id>();
        Set<Id> scenarioIdSetUncheck = new Set<Id>();
        Map<Id,String> queueIdtoStatusMap = new Map<Id,String>();

        for(Scenario__c s : trigger.new)
        {

        // code updated by lagnika for updating business rule scenario in publishing stage - readonly alignment needs to be updated hence introduced source check
           if(s.Scenario_Source__c =='Business Rule' 
                && s.Scenario_Stage__c != SalesIQGlobalConstants.PUBLISHING && s.Scenario_Stage__c != SalesIQGlobalConstants.COLLABORATION ) return;

            scenarioType = s.Scenario_Type__c;
            previousScenarioStatus = trigger.oldMap.get(s.Id).Request_Process_Stage__c;
            updatedScenarioStatus = s.Request_Process_Stage__c;
            scenarioId = s.Id;
            templateId = s.Business_Rule_Template__c;
            scenarioSource = s.Scenario_Source__c;
            sourceTeamInstanceId = s.Source_Team_Instance__c;
            system.debug('scenarioType is='+scenarioType);
            system.debug('previousScenarioStatus :'+previousScenarioStatus);
            system.debug('updatedScenarioStatus :'+updatedScenarioStatus);
            system.debug('scenarioId :'+scenarioId);
            system.debug('templateId :'+templateId);
            
            //Is this a new scenario created from existing scenario?
            if(s.Team_Instance__c != null && trigger.oldMap.get(s.Id).Team_Instance__c == null && s.Scenario_Stage__c == SalesIQGlobalConstants.DESIGN && s.Request_Process_Stage__c == 'In Queue' && s.Scenario_Source__c !='Business Rule'){
                system.debug('#### New Alignment scenario created');
                scenario = s;
            }
            else if(s.Team_Instance__c != null && trigger.oldMap.get(s.Id).Team_Instance__c == null && s.Scenario_Stage__c == SalesIQGlobalConstants.COLLABORATION && s.Request_Process_Stage__c == 'In Queue' && s.Scenario_Source__c !='Business Rule'){
                system.debug('#### New Call plan scenario created');
                scenario = s;
            }else if(s.Team_Instance__c != null && s.Scenario_Stage__c == SalesIQGlobalConstants.DESIGN && s.Request_Process_Stage__c == 'Validation In Queue' && (trigger.oldMap.get(s.Id).Request_Process_Stage__c == 'Validation Failed' || trigger.oldMap.get(s.Id).Request_Process_Stage__c == 'Error Occured') && s.Scenario_Source__c !='Business Rule'){
                system.debug('#### Rerun scenario validation');
                scenario = s;
            }

            //Handle Affiliation related changes on Scenario post creation
            if(trigger.oldMap.get(s.Id).Enable_Affiliation_Based_Alignment__c != s.Enable_Affiliation_Based_Alignment__c){
                if(s.Enable_Affiliation_Based_Alignment__c == true){
                    system.debug('Value of Enable Affiliation changed to true');
                    scenarioIdSet.add(s.Id);
                }
                if(s.Enable_Affiliation_Based_Alignment__c == false){
                    system.debug('Value of Enable Affiliation changed to FALSE');
                    scenarioIdSetUncheck.add(s.Id);
                }
            }
            if(s.Request_Queue__c != null && ( s.Rule_Execution_Status__c == SalesIQGlobalConstants.SUCCESS || s.Rule_Execution_Status__c == SalesIQGlobalConstants.IN_PROGRESS || s.Rule_Execution_Status__c == SalesIQGlobalConstants.ERROR) ){
		        List<Scenario__c> scenarioList = [select id,Name from scenario__c];
                //SalesIQUtility.updateNotification('scenario', scenarioList);
                queueIdtoStatusMap.put(s.Request_Queue__c,s.Rule_Execution_Status__c);
            }
        }

        //SIQ 2933
        //UPDATE ACTIVITY LOG JOB STATUS
        if (queueIdtoStatusMap.size() > 0 ){
            RequestQueueTriggerHandler.updateJobStatus(queueIdtoStatusMap);
        }
        if(scenarioIdSet.size()>0){
            List<Scenario_Rule_Instance_Details__c> sridObj = [Select Is_Visible_Summarize__c,Is_Visible__c,Component_Display_Order__c,Component_Display_Order_Summarize__c,Parent_Display_Order__c,Parent_Display_Order_Summarize__c,ComponentTypeLabel__c  from Scenario_Rule_Instance_Details__c where Scenario_Id__c in :scenarioIdSet and (ComponentTypeLabel__c = 'Affiliation' or ComponentTypeLabel__c = 'Affiliation Network' or ComponentTypeLabel__c = 'Final Output')];

            system.debug('scenarioIdSet srid values----'+sridObj);
            List<Scenario_Rule_Instance_Details__c> sridFinalOutputObj = new List<Scenario_Rule_Instance_Details__c>();
            for(Scenario_Rule_Instance_Details__c s : sridObj ){
                if(s.ComponentTypeLabel__c == 'Affiliation'){
                    s.Is_Visible_Summarize__c = true;
                    s.Is_Visible__c = true;
                }
                if(s.ComponentTypeLabel__c == 'Affiliation Network'){
                    s.Is_Visible_Summarize__c = true;
                    s.Is_Visible__c = true;
                }
                if(s.ComponentTypeLabel__c == 'Final Output'){
                    s.Component_Display_Order__c = s.Component_Display_Order__c + 1;
                    s.Component_Display_Order_Summarize__c = s.Component_Display_Order_Summarize__c + 1 ;
                    s.Parent_Display_Order__c = String.valueOf(Integer.valueof(s.Parent_Display_Order__c) + 1);
                    s.Parent_Display_Order_Summarize__c = String.valueOf(Integer.valueof(s.Parent_Display_Order_Summarize__c) + 1);
                }
             
                sridFinalOutputObj .add(s);
            }
            system.debug('Final changes----'+ sridFinalOutputObj );
            update sridFinalOutputObj ;

            for(Id scenarioId : scenarioIdSet) {
                ScenarioCtlr.updateScenarioDataObjectMap(scenarioId);
            }
        }
        
        if(scenarioIdSetUncheck.size()>0){
            List<Scenario_Rule_Instance_Details__c> sridObj = [Select Is_Visible_Summarize__c,Is_Visible__c,Component_Display_Order__c,Component_Display_Order_Summarize__c,Parent_Display_Order__c,Parent_Display_Order_Summarize__c,ComponentTypeLabel__c  from Scenario_Rule_Instance_Details__c where Scenario_Id__c in :scenarioIdSetUncheck and (ComponentTypeLabel__c = 'Affiliation' or ComponentTypeLabel__c = 'Affiliation Network' or ComponentTypeLabel__c = 'Final Output')];
            system.debug('scenarioIdSet srid values----'+sridObj);

            List<Scenario_Rule_Instance_Details__c> sridFinalOutputObj = new List<Scenario_Rule_Instance_Details__c>();
            for(Scenario_Rule_Instance_Details__c s : sridObj ){
                if(s.ComponentTypeLabel__c == 'Affiliation'){
                    s.Is_Visible_Summarize__c = false;
                    s.Is_Visible__c = false;
                }
                if(s.ComponentTypeLabel__c == 'Affiliation Network'){
                    s.Is_Visible_Summarize__c = false;
                    s.Is_Visible__c = false;
                }
                if(s.ComponentTypeLabel__c == 'Final Output'){
                    s.Component_Display_Order__c = s.Component_Display_Order__c - 1;
                    s.Component_Display_Order_Summarize__c = s.Component_Display_Order_Summarize__c - 1 ;
                    s.Parent_Display_Order__c = String.valueOf(Integer.valueof(s.Parent_Display_Order__c) - 1);
                    s.Parent_Display_Order_Summarize__c = String.valueOf(Integer.valueof(s.Parent_Display_Order_Summarize__c) - 1);
                }
             
                sridFinalOutputObj .add(s);
            }
            system.debug('Final changes----'+ sridFinalOutputObj );
            update sridFinalOutputObj ;

            for(Id scenarioId : scenarioIdSetUncheck) {
                ScenarioCtlr.updateScenarioDataObjectMap(scenarioId);
            }
        }

        system.debug('#### InitiateETLProcess for 1 insertSalesIQScenarioDataObjects');
        if(previousScenarioStatus != null && updatedScenarioStatus != null && scenarioSource != 'Business Rule'){
            
            // SIMPS-878 & SIQ-3285, call the data sync if Run and Promote is off in case of copy scenario also
            List<Server_Thresholds__c> copyScenarioAutoExecution = [SELECT Load_Count__c FROM Server_Thresholds__c where Name = 'Skip Copy Auto Execution'];

            if( scenarioType == 'Alignment Scenario' && templateId != null &&  
                previousScenarioStatus.equals(SalesIQGlobalConstants.CIM_READY) && updatedScenarioStatus.equals(SalesIQGlobalConstants.READY)) 
            {
                system.debug('#### InitiateETLProcess for insertSalesIQScenarioDataObjects');
                ScenarioTriggerHandler.insertSalesIQScenarioDataObjects(scenarioId, templateId);
            }

            else if(scenarioType == 'Alignment Scenario' && templateId != null && sourceTeamInstanceId != null && !copyScenarioAutoExecution.isEmpty() &&
                !previousScenarioStatus.equals(SalesIQGlobalConstants.CIM_READY) && !previousScenarioStatus.equals(SalesIQGlobalConstants.READY) &&
                updatedScenarioStatus.equals(SalesIQGlobalConstants.READY)) 
            {
                // Alignment scenario and Skip of Copy scenario auto execution is off
                system.debug('####Sync the scenario level data for COPY SCENARIO');
                ScenarioTriggerHandler.insertSalesIQScenarioDataObjects(scenarioId, templateId);
            }
        }

        system.debug('#### scenario : '+scenario);
        if(scenario != null && scenarioSource != 'Business Rule'){
            //Is there any job running for copy process? If yes, let it finish else invoke a new copy job
            if(scenarioType == 'Alignment Scenario' && ScenarioTriggerHandler.isValidScenario(new list<string>{SalesIQGlobalConstants.DESIGN}, new list<string>{SalesIQGlobalConstants.INQUIRY_STATUS_INPROGRESS}, scenarioType, new set<id>{})){
                if(scenario.Scenario_Source__c == 'Existing Scenario'){
                    system.debug('#### InitiateETLProcess for copy');
                    ScenarioTriggerHandler.InitiateETLProcessV2(scenario, 'copy');
                }else if(scenario.Scenario_Source__c == 'File' && scenario.Scenario_Type__c=='Alignment Scenario'){
                    system.debug('#### InitiateETLProcess for insert Alignment Scenario');
                    ScenarioTriggerHandler.InitiateETLProcessV2(scenario, 'insert');
                }else{
                    system.debug('#### Do nothing');
                }
                
            }
            /*else if(scenarioType == 'Alignment Scenario' && previousScenarioStatus.equals(SalesIQGlobalConstants.INQUIRY_STATUS_INPROGRESS) && updatedScenarioStatus.equals(SalesIQGlobalConstants.READY) && templateId != null ) {
                    system.debug('#### InitiateETLProcess for copy')
                    ScenarioTriggerHandler.insertSalesIQScenarioDataObjects(scenarioId, templateId);
            }*/
            else if(scenarioType == 'Call Plan Generation' && ScenarioTriggerHandler.isValidScenario(new list<string>{SalesIQGlobalConstants.COLLABORATION}, new list<string>{SalesIQGlobalConstants.INQUIRY_STATUS_INPROGRESS}, scenarioType, new set<id>{})){
                system.debug('#### Inside CallPlan Insert');
                if(scenario.Scenario_Source__c == 'File'&& scenario.Scenario_Type__c=='Call Plan Generation'){
                    system.debug('#### InitiateETLProcess for insert Call Plan Scenario');
                    ScenarioTriggerHandler.InitiateETLProcess(scenario, 'callplaninsert');
                }
                else{
                    system.debug('#### Do nothing');
                }
            }
            else{
                system.debug('#### Old job in progress');
            }
        }
        
        boolean pickNextScenarioToCopy = false;
        boolean pickNextDeleteJob = false;
        boolean IsEmployeeAssignmentBatchExecute = false;
        boolean startDeleteScenario = false;
        boolean callCIMPopulateTrigger=false;
        boolean pickNextCallPlanScenarioToCopy=false;
         
        for(Scenario__c sc : trigger.new){
            //Is any running job for copy is completed? If yes, check for other jobs in queue.
            //On finish of a copy job Talend will change the requset process stage from 'In Progress' to 'Ready' or 'Error Occured'
            system.debug('#### new Scenario_Stage__c : '+sc.Scenario_Stage__c);
            system.debug('#### old Request_Process_Stage__c : '+trigger.oldMap.get(sc.Id).Request_Process_Stage__c);
            system.debug('#### new Request_Process_Stage__c : '+sc.Request_Process_Stage__c);
            if(sc.Scenario_Stage__c == SalesIQGlobalConstants.DESIGN && sc.Scenario_Type__c == 'Alignment Scenario' && trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.IN_PROGRESS && (sc.Request_Process_Stage__c == SalesIQGlobalConstants.READY || sc.Request_Process_Stage__c == SalesIQGlobalConstants.ERROR_OCCURED)){
                system.debug('Running job completed with '+sc.Request_Process_Stage__c+' stage');
                pickNextScenarioToCopy = true;
            }
            else if(sc.Scenario_Stage__c == SalesIQGlobalConstants.COLLABORATION && sc.Scenario_Type__c == 'Call Plan Generation' && trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.IN_PROGRESS && (sc.Request_Process_Stage__c == SalesIQGlobalConstants.READY || sc.Request_Process_Stage__c == SalesIQGlobalConstants.ERROR_OCCURED)){
                system.debug('Running job completed with '+sc.Request_Process_Stage__c+' stage');
                pickNextCallPlanScenarioToCopy = true;
            }
            
            if(sc.Scenario_Stage__c == SalesIQGlobalConstants.DESIGN){
                system.debug('Delete scenario in Design stage with status '+trigger.oldMap.get(sc.Id).Request_Process_Stage__c);
                //Is this a Delete scenario request? On Delete, we change the Request Process Stage from 'Ready' to 'Delete In Queue'.
                if((trigger.oldMap.get(sc.Id).Request_Process_Stage__c == 'Validation Failed' || trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.READY || trigger.oldMap.get(sc.Id).Request_Process_Stage__c == 'Partial Delete' || trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.ERROR_OCCURED) && sc.Request_Process_Stage__c == SalesIQGlobalConstants.DELETE_IN_QUEUE){
                    //Is there any job running for delete scenario? If yes, let it finish else invoke a new delete job.
                    if(ScenarioTriggerHandler.isValidScenario(new list<string>{SalesIQGlobalConstants.DESIGN,SalesIQGlobalConstants.COLLABORATION}, new list<string>{SalesIQGlobalConstants.DELETE_IN_PROGRESS}, sc.Scenario_Type__c, new set<id>{sc.Id})){
                        startDeleteScenario = true;
                        scenario = sc;
                    }
                }
            }
	    if((sc.Request_Process_Stage__C == SalesIQGlobalConstants.READY && sc.Scenario_Status__c == 'Active') || (sc.Request_Process_Stage__C == SalesIQGlobalConstants.ERROR_OCCURED  && sc.Scenario_Status__c == 'Inactive')){
                   //SalesIQUtility.updateNotification('scenario', new list<Scenario__c>{sc});
                   }
            else if(sc.Scenario_Stage__c == SalesIQGlobalConstants.COLLABORATION){
                system.debug('Delete scenario in Design stage with status '+trigger.oldMap.get(sc.Id).Request_Process_Stage__c);
                //Is this a Delete scenario request? On Delete, we change the Request Process Stage from 'Ready' to 'Delete In Queue'.
                if(((trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.READY && sc.Scenario_Status__c == 'Inactive') || trigger.oldMap.get(sc.Id).Request_Process_Stage__c == 'Partial Delete') && sc.Request_Process_Stage__c == SalesIQGlobalConstants.DELETE_IN_QUEUE){
                    //Is there any job running for delete scenario? If yes, let it finish else invoke a new delete job.
                    if(ScenarioTriggerHandler.isValidScenario(new list<string>{SalesIQGlobalConstants.DESIGN,SalesIQGlobalConstants.COLLABORATION}, new list<string>{SalesIQGlobalConstants.DELETE_IN_PROGRESS}, sc.Scenario_Type__c, new set<id>{sc.Id})){
                        startDeleteScenario = true;
                        scenario = sc;
                    }
                }
            }
            if(sc.Scenario_Status__c == 'Active'){
                if(sc.Scenario_Stage__c == SalesIQGlobalConstants.DESIGN){
                    system.debug('in DESIGN');
                    //Is any running job for delete is completed with an error? If yes, check for other jobs in queue.
                    //On finish of a delete job with error Talend will change the requset process stage from 'Delete In Progress' to 'Delete Error Occured'
                    //if(trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.DELETE_IN_PROGRESS && sc.Request_Process_Stage__c == SalesIQGlobalConstants.DELETE_ERROR_OCCURED){
                    if((trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.DELETE_IN_PROGRESS && sc.Request_Process_Stage__c == SalesIQGlobalConstants.DELETE_ERROR_OCCURED) || (sc.Request_Process_Stage__c == SalesIQGlobalConstants.DELETE_IN_QUEUE && trigger.oldMap.get(sc.Id).Request_Process_Stage__c!= sc.Request_Process_Stage__c)){
                        pickNextDeleteJob = true;
                    }
                    if(trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.IN_PROGRESS && (sc.Request_Process_Stage__c == SalesIQGlobalConstants.CIM_READY || sc.Request_Process_Stage__c == SalesIQGlobalConstants.READY) &&  sc.Scenario_Type__c != 'Call Plan Generation'){
                       callCIMPopulateTrigger = true;
                        scenario = sc;
                    }
                    if(trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.CIM_READY && (sc.Request_Process_Stage__c == SalesIQGlobalConstants.READY || sc.Request_Process_Stage__c == SalesIQGlobalConstants.ERROR_OCCURED)){
                        pickNextScenarioToCopy = true;
                    }
					if((trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.IN_PROGRESS ||  sc.Request_Process_Stage__c == SalesIQGlobalConstants.CIM_READY ) && Trigger.newMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.READY && sc.Source_Team_Instance__c != null && sc.Team_Instance__c != null && sc.Scenario_Source__c == 'Existing Scenario')
                    {
                    ScenarioTriggerHandler.validateCopyScenarioData(sc.Source_Team_Instance__c,sc.Team_Instance__c);
                    }
                }
                else if(sc.Scenario_Stage__c == SalesIQGlobalConstants.COLLABORATION){
                    system.debug('in COLLABORATION');
                    
                    // when scenario stage is changed to collaboration CR_Tracking should be set to true by default
                    if(sc.CR_Tracking__c == false && trigger.oldMap.get(sc.Id).CR_Tracking__c == false && trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.READY && trigger.newMap.get(sc.Id).Scenario_Stage__c == SalesIQGlobalConstants.COLLABORATION){
                        ScenarioTriggerHandler.UpdateScenarioCRTracking(sc);
                    }

                    System.debug('trigger.oldMap.get(sc.Id) : '+trigger.oldMap.get(sc.Id));
                    System.debug('sc : '+sc);

                    if(trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.IN_PROGRESS && (sc.Request_Process_Stage__c == SalesIQGlobalConstants.CIM_READY || sc.Request_Process_Stage__c == SalesIQGlobalConstants.READY) && sc.Scenario_Type__c == 'Call Plan Generation'){
                       callCIMPopulateTrigger = true;
                        scenario = sc;
                    }
                    
                    //Is employee assignment just enabled?
                    //Employee assignment is changed from false to true and Request Process Stage is changed from 'Ready' to 'In Progress'
                    if(sc.Employee_Assignment__c == true && trigger.oldMap.get(sc.Id).Employee_Assignment__c == false && (sc.Request_Process_Stage__c==SalesIQGlobalConstants.IN_QUEUE || sc.Request_Process_Stage__c == SalesIQGlobalConstants.IN_PROGRESS) && trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.READY){
                        //If yes, Invoke the batch class to copy employee assignments.
                        IsEmployeeAssignmentBatchExecute = true;
                    }
                    //Is this a Delete scenario request? On Delete, we change the Request Process Stage from 'Ready' to 'Delete In Queue'.
                    if((trigger.oldMap.get(sc.Id).Request_Process_Stage__c == 'Validation Failed' || trigger.oldMap.get(sc.Id).Request_Process_Stage__c == 'Partial Delete' || trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.READY || trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.ERROR_OCCURED) && sc.Request_Process_Stage__c == SalesIQGlobalConstants.DELETE_IN_QUEUE){
                        //Is there any job running for delete scenario? If yes, let it finish else invoke a new delete job.
                        if(ScenarioTriggerHandler.isValidScenario(new list<string>{SalesIQGlobalConstants.DESIGN,SalesIQGlobalConstants.COLLABORATION}, new list<string>{SalesIQGlobalConstants.DELETE_IN_PROGRESS}, sc.Scenario_Type__c, new set<id>{sc.Id} )){
                            startDeleteScenario = true;
                            scenario = sc;
                        }
                    }
                    //Is any running job for delete is completed with an error? If yes, check for other jobs in queue.
                    //On finish of a delete job with error Talend will change the requset process stage from 'Delete In Progress' to 'Delete Error Occured'
                    //if(trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.DELETE_IN_PROGRESS && sc.Request_Process_Stage__c == SalesIQGlobalConstants.DELETE_ERROR_OCCURED){
                    if((trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.DELETE_IN_PROGRESS && sc.Request_Process_Stage__c == SalesIQGlobalConstants.DELETE_ERROR_OCCURED)|| (sc.Request_Process_Stage__c == SalesIQGlobalConstants.DELETE_IN_QUEUE && trigger.oldMap.get(sc.Id).Request_Process_Stage__c!= sc.Request_Process_Stage__c)){
                        pickNextDeleteJob = true;
                    }
                }
                else if(sc.Scenario_Stage__c==SalesIQGlobalConstants.PUBLISHING){
                    system.debug('in PUBLISHING');
                    //Is employee assignment just enabled?
                    //Employee assignment is changed from false to true and Request Process Stage is changed from 'Ready' to 'In Progress'
                    if(sc.Employee_Assignment__c != trigger.oldMap.get(sc.Id).Employee_Assignment__c && sc.Employee_Assignment__c == true && trigger.oldMap.get(sc.Id).Employee_Assignment__c==false && sc.Request_Process_Stage__c == SalesIQGlobalConstants.INQUIRY_STATUS_INPROGRESS && trigger.oldMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.READY){
                        //If yes, Invoke the batch class to copy employee assignments.
                        IsEmployeeAssignmentBatchExecute = true;
                    }
                    // make all operations as readonly while request process stage == ready (To be discuss with Lagnika)
                    if(trigger.newMap.get(sc.Id).Request_Process_Stage__c == SalesIQGlobalConstants.READY || trigger.newMap.get(sc.Id).Request_Process_Stage__c == 'Success'){
                        ScenarioTriggerHandler.updateTeamInstanceActionsReadOnly(trigger.new);
                    }
                }
                
            }
        }
        system.debug('#### pickNextScenarioToCopy : '+pickNextScenarioToCopy);
        system.debug('#### startDeleteScenario : '+startDeleteScenario);
        system.debug('#### pickNextDeleteJob : '+pickNextDeleteJob);
        
        if(pickNextScenarioToCopy){
            //A job for copy is just completed and we are fetching other jobs in queue to process one by one
            list<Scenario__c> lstQueueScenario = ScenarioUtility.getScenarioByRequestName(new list<string>{SalesIQGlobalConstants.DESIGN}, new list<string>{SalesIQGlobalConstants.IN_QUEUE}, 'Alignment Scenario', new set<id>{});
            if(lstQueueScenario != null && lstQueueScenario.size() > 0){
                Scenario__c queuedScenario = lstQueueScenario[0];
                system.debug('#### Next scenario picked : '+queuedScenario.Id);
                if(queuedScenario.Source_Team_Instance__c != null){
                    ScenarioTriggerHandler.InitiateETLProcessV2(queuedScenario, 'copy');
                }else{
                    ScenarioTriggerHandler.InitiateETLProcessV2(queuedScenario, 'insert');
                }
            }
            // update user position record - active and assign position (To be check with Lagnika)
            ScenarioTriggerHandler.updateUserPosition(trigger.new);
        }
        else if(pickNextCallPlanScenarioToCopy){
            list<Scenario__c> lstQueueScenario = ScenarioUtility.getScenarioByRequestName(new list<string>{SalesIQGlobalConstants.COLLABORATION}, new list<string>{SalesIQGlobalConstants.IN_QUEUE}, 'Call Plan Generation', new set<id>{});
            if(lstQueueScenario != null && lstQueueScenario.size() > 0){
                Scenario__c queuedScenario = lstQueueScenario[0];
                system.debug('#### Next scenario picked : '+queuedScenario.Id);
                ScenarioTriggerHandler.InitiateETLProcessV2(queuedScenario, 'callplaninsert');
            }
            // update user position record - active and assign position (To be check with Lagnika)
            ScenarioTriggerHandler.updateUserPosition(trigger.new);
        }

        else if(callCIMPopulateTrigger){
            CIMPositionMatrixSummaryUpdateCtlr cim=new CIMPositionMatrixSummaryUpdateCtlr(scenario.Team_Instance__c,scenario.id);
        }
        else if(IsEmployeeAssignmentBatchExecute){
            //Employee assignment is just enabled for a scenario.
            //When Employee Assignment is enabled for a scenario, all other scenarios for same source team instance will be marked inactive.
            /*ScenarioTriggerHandler.UpdateScenarioToInactive(trigger.new);*/
            
            //Find out all the scenarios in queue for employee assignment
            list<Scenario__c> lstQueueScenario = ScenarioUtility.getScenarioByRequestName(new list<string>{SalesIQGlobalConstants.COLLABORATION,SalesIQGlobalConstants.PUBLISHING}, new list<string>{SalesIQGlobalConstants.INQUIRY_STATUS_INPROGRESS}, 'Alignment Scenario', new set<id>{});
            if(lstQueueScenario != null && lstQueueScenario.size() > 0){
                //Invoke batch job CopyPositionEmployeeBatch to copy employee assignment for all the scenarios in queue for employee assignment.
                ScenarioTriggerHandler.updateLatestEmployeeAssignment(trigger.new);
                Id batchJobId = Database.executeBatch(new CopyPositionEmployeeBatch(trigger.new), 200);
                //Id batchJobId = Database.executeBatch(new EnableEmployeeAssignmentBatch(lstQueueScenario), 200);

                System.debug('1111111- IsEmployeeAssignmentBatchExecute end  ');
            }
        }else if(startDeleteScenario){
            system.debug('#### Initate delete process : '+scenario);
            //Delete scenario request arrived. Invoke Delete talend job.
            if(scenario != null){
                ScenarioTriggerHandler.InitiateETLProcessV2(scenario, 'delete');
            }
        }else if(pickNextDeleteJob){
            //A job for delete scenario is just finished. Is there any other scenario in queue for delete. If yes, invoke delete job.
            ScenarioTriggerHandler.deleteScenarioInQueue();
        }
    }

    //Once a scenario is deleted, check for other scenarios in "Delete In Queue" status and invoke delete job. 
    if(Trigger.isAfter && Trigger.isDelete){
        for(Scenario__c sc : Trigger.old){
            if( sc.Request_Process_Stage__c == SalesIQGlobalConstants.DELETE_IN_PROGRESS || sc.Request_Process_Stage__c ==  'Delete Successful' ){
                ScenarioTriggerHandler.deleteScenarioInQueue();
            }
        }
        RequestQueueTriggerHandler.deleteQueueRecordsOnScenarioDelete();
    }else if(Trigger.isAfter && Trigger.isUpdate){
        for(Scenario__c sc : Trigger.new){
            if( sc.Request_Process_Stage__c ==  'Partial Delete' ){
                ScenarioTriggerHandler.deleteScenarioInQueue();
            }
        }
        RequestQueueTriggerHandler.deleteQueueRecordsOnScenarioDelete();
    }

	/*Code Added By Harkirat Singh  SIQ-1563*/
	// Scenario_Stage__c condition added for delete in queue for brms scenarios SIQ -3810
    if(Trigger.isBefore && Trigger.isUpdate && trigger.new[0].Scenario_Stage__c != SalesIQGlobalConstants.DELETE_IN_PROGRESS){
        //SIQ 1022
        for(Scenario__c scenario: trigger.new) {
            if( scenario.Business_Rule_Template__c != null && scenario.Request_Process_Stage__c == 'Ready' && ( scenario.Rule_Execution_Status__c == SalesIQGlobalConstants.READY_FOR_RUN || scenario.Rule_Execution_Status__c == null ) &&
                scenario.Last_Sync_Success_Date__c == null && scenario.Source_Team_Instance__c <> null && scenario.Last_Run_Date__c == null
                ){
                
                // controll auto run and promote of copy scenario with custom setting, SIMPS-878 & SIQ-3285
                List<Server_Thresholds__c> copyScenarioAutoExecution = [SELECT Load_Count__c FROM Server_Thresholds__c where Name = 'Skip Copy Auto Execution'];

                if(copyScenarioAutoExecution.isEmpty()) {
                    scenario.Promote_Mode__c=SalesIQGlobalConstants.TEXT_RUN_FULL_PROMOTE;
                    scenario.Rule_Execution_Status__c = SalesIQGlobalConstants.QUEUED;
                    scenario.Run_Response_Details__c = null;
                }

                //POSITION LEVEL UPDATE : SYNC POSITION LEVEL BUSINESS RULES
                CPGUtility.updateCopiedPositionLevelRules(scenario.id,scenario.Source_Team_Instance__c,scenario.Team_Instance__c);
            }

        }

        Map<String, Id> recordTypeMapByName = new Map<String, Id>();
        Set<Id> recordTypes = new Set<Id>();
        for(RecordType rt : [select Id, DeveloperName from RecordType where DeveloperName =: SalesIQGlobalConstants.SCENARIO_RECORDTYPE_ALIGNMENT or DeveloperName =: SalesIQGlobalConstants.SCENARIO_RECORDTYPE_BLOCK or DeveloperName =: SalesIQGlobalConstants.SCENARIO_RECORDTPYE_CALL_PLAN]) {
            recordTypeMapByName.put(rt.DeveloperName, rt.Id);
            recordTypes.add(rt.Id);
        }

        // query only Alignment, call plan and customer block scenario, and to avoid recursion
        List<Scenario__c> scenarioList=[Select Country__c, Team_Instance__c, RecordType.DeveloperName,RecordTypeId,Run_Response_Details__c,Request_Queue__c, Request_Queue__r.Change_Request__c, Request_Queue__r.Request_Type__c, Rule_Execution_Status__c, Promote_Mode__c FROM Scenario__c WHERE Rule_Execution_Status__c=:SalesIQGlobalConstants.QUEUED and RecordTypeId in :recordTypes and Id not in :trigger.newMap.keySet() and Id not in :ScenarioTriggerHandler.secnarioIdsToAvoidRecursion order By RecordType.DeveloperName DESC];
        
        Set<String> statusSet = new Set<String>{SalesIQGlobalConstants.SUCCESS, SalesIQGlobalConstants.READY_FOR_RUN, '', SalesIQGlobalConstants.ERROR, SalesIQGlobalConstants.QUEUED};
        Integer alreadyInProgress = [Select count() FROM Scenario__c WHERE Rule_Execution_Status__c NOT IN: statusSet and Id not in :trigger.newMap.keySet()];
        system.debug('already in progress :: ' + alreadyInProgress);
    
        Set<Id> scenarioExecSet = new Set<Id>();
        Set<Id> requestQueueSet = new Set<Id>(); 
        Set<Id> queriedScenarios = (new Map<Id, Scenario__c>(scenarioList)).keySet();
        for(Scenario__c scenario: trigger.new) {
            ScenarioTriggerHandler.secnarioIdsToAvoidRecursion.add(scenario.Id);
            if(scenario.Rule_Execution_Status__c == SalesIQGlobalConstants.QUEUED && recordTypes.contains(scenario.RecordTypeId) && !queriedScenarios.contains(scenario.Id)) {
                scenarioList.add(scenario);
                requestQueueSet.add(scenario.Request_Queue__c);
            }

            //if(scenario.Rule_Execution_Status__c == SalesIQGlobalConstants.IN_PROGRESS) {
            system.debug('Exec Status:: ' + scenario.Rule_Execution_Status__c);
            if(!statusSet.contains(scenario.Rule_Execution_Status__c)) {
                scenarioExecSet.add(scenario.Id);
            }
            
            // if clause to unlock a scenario when status is to Success or Error
            // trigger.oldMap.get(scenario.Id).Rule_Execution_Status__c == SalesIQGlobalConstants.IN_PROGRESS
            if(trigger.newMap.get(scenario.Id).Rule_Execution_Status__c == SalesIQGlobalConstants.ERROR || trigger.newMap.get(scenario.Id).Rule_Execution_Status__c == CPGConstants.SUCCESS)
            {
                scenario.Is_Locked__c = false;
            }
        }

        if(scenarioList.isEmpty()) return;

        List<Server_Thresholds__c> thresholdList=[SELECT Load_Count__c FROM Server_Thresholds__c where Name = 'Execute Scenario'];
        List<Scenario__c> toBeUpdatedScenarios = new List<Scenario__c>();

        Integer thresholdVal;
        if(!thresholdList.isEmpty())
            thresholdVal =Integer.valueOf(thresholdList[0].Load_Count__c);
        else
            thresholdVal = 1;
        
        system.debug('progress in trigger :: ' + scenarioExecSet);
        Integer execVal = alreadyInProgress + scenarioExecSet.size();
        List<SalesIQ_Logger__c> loggerRecord= [Select Type__c,Status__c,RecordTypeId FROM SalesIQ_Logger__c WHERE Type__c=: CPGConstants.QUEUE_LOGGER_TYPE_MASTER and RecordType.DeveloperName=: CPGConstants.QUEUE_LOGGER_RECORDTYPE_API order by CreatedDate DESC LIMIT 1];
        
        Map<Id, Activity_Log__c> requestQueueMap = new Map<Id, Activity_Log__c>([Select Change_Request__c, Scenario__c, Request_Type__c from Activity_Log__c where Id in: requestQueueSet]);
        if((loggerRecord.size()>0 && loggerRecord[0].Status__c=='Success') || loggerRecord.size()==0){
            system.debug('Exec count: ' + execVal + '  threshold val: ' + thresholdVal);
            if(execVal<thresholdVal)
            {
                Integer i = 0;
                for(Integer j = 0 ; j < thresholdVal-execVal ; j++){
                    if(i < scenarioList.size()){
                        Boolean runScenario = true;
                        if(scenarioList[i].RecordTypeId==recordTypeMapByName.get(SalesIQGlobalConstants.SCENARIO_RECORDTYPE_ALIGNMENT)){
                            // move this 
                            List<Scenario__c> customerBlockScenario = [Select id,Rule_Execution_Status__c FROM Scenario__c WHERE Country__c=:scenarioList[i].Country__c and RecordType.DeveloperName =: SalesIQGlobalConstants.SCENARIO_RECORDTYPE_BLOCK];
                            if( (customerBlockScenario.size()==0) || ( customerBlockScenario.size()>0 && ( customerBlockScenario[0].Rule_Execution_Status__c =='Success'||  customerBlockScenario[0].Rule_Execution_Status__c == null ||  customerBlockScenario[0].Rule_Execution_Status__c == ''))){
                                runScenario=true;
                            }
                            else{
                                runScenario=false;
                            }
                        }
                        if(runScenario==true){
                            system.debug('--True-- ' + scenarioList[i].Id);

                            Boolean isChangeRequestPending = false;
                            if(scenarioList[i].Team_Instance__c != null && scenarioList[i].RecordTypeId == recordTypeMapByName.get(SalesIQGlobalConstants.SCENARIO_RECORDTYPE_ALIGNMENT)){
                                
                                if(scenarioList[i].Promote_Mode__c == SalesIQGlobalConstants.TEXT_RUN_FULL_PROMOTE || scenarioList[i].Promote_Mode__c == SalesIQGlobalConstants.TEXT_RUN_DELTA_PROMOTE) {
                                    String dependentCR;
                                    if(scenarioList[i].Request_Queue__c != null) {
                                        if(requestQueueMap.containsKey(scenarioList[i].Request_Queue__c)) {
                                            dependentCR = requestQueueMap.get(scenarioList[i].Request_Queue__c).Change_Request__c;
                                        } else {
                                            dependentCR = scenarioList[i].Request_Queue__r.Change_Request__c;
                                        }
                                    }

                                    if(dependentCR == null) {
                                        List<Change_Request__c> crList = SalesIQUtility.isChangeRequestPendingonTeamInstance(new List<String>{scenarioList[i].Team_Instance__c},new List<String>{SalesIQGlobalConstants.CR_TYPE_CALL_PLAN,SalesIQGlobalConstants.CR_TYPE_EMP_ASSIGNMENT,SalesIQGlobalConstants.CR_TYPE_ROSTER});
                                        if(crList.size() > 0){
                                            List<String> crNameList = new List<String>();
                            				for(Change_Request__c cr:crList)
                            		        {
                            		            crNameList.add(cr.Name);
                            		        }
                                            isChangeRequestPending=true;
                                            scenarioList[i].Rule_Execution_Status__c = SalesIQGlobalConstants.ERROR;
        	                                scenarioList[i].Run_Response_Details__c = System.Label.Change_Request_Pending_On_Scenario + ':'+ String.join(crNameList,',');
                                            j--;
                                            
                                        }
                                    }
                                }
                            }

                            if(!isChangeRequestPending){

                                if(scenarioList[i].Promote_Mode__c==SalesIQGlobalConstants.TEXT_RUN_DELTA)
                                {
                                    system.debug('Run Delta Mode execute called');
                                    CPGCanvasCtrl.updateScenarioDataObjectMap(scenarioList[i].id,true);
                                    Database.executeBatch(new CPGExpressionParserBatch(scenarioList[i].id,false,false), 200);
                                }
                                else if(scenarioList[i].Promote_Mode__c==SalesIQGlobalConstants.TEXT_RUN_DELTA_PROMOTE)
                                {
                                    system.debug('Run Delta Promote Mode execute called');
                                    CPGCanvasCtrl.updateScenarioDataObjectMap(scenarioList[i].id,true);
                                    Database.executeBatch(new CPGExpressionParserBatch(scenarioList[i].id,true,false), 200);
                                }
                                else if(scenarioList[i].Promote_Mode__c==SalesIQGlobalConstants.TEXT_RUN_FULL_PROMOTE)
                                {
                                    system.debug('Run Full and Promote Mode execute called');
                                    CPGCanvasCtrl.updateScenarioDataObjectMap(scenarioList[i].id,false);
                                    Database.executeBatch(new CPGExpressionParserBatch(scenarioList[i].id,true,true), 200);
                                }
                                else if(scenarioList[i].Promote_Mode__c==SalesIQGlobalConstants.TEXT_RUN_FULL)
                                {
                                    system.debug('Run Full Mode execute called');
                                    CPGCanvasCtrl.updateScenarioDataObjectMap(scenarioList[i].id,false);
                                    Database.executeBatch(new CPGExpressionParserBatch(scenarioList[i].id), 200);
                                }
                                
                                if(trigger.newMap.containsKey(scenarioList[i].Id)) {
                                    Scenario__c record = trigger.newMap.get(scenarioList[i].Id);
                                    record.Rule_Execution_Status__c = SalesIQGlobalConstants.IN_PROGRESS;
                                    record.Run_Response_Details__c = null;
                                    record.Last_Run_Date__c=system.now();
                                    record.Is_Locked__c=true;

                                    //ScenarioTriggerHandler.secnarioIdsToAvoidRecursion.add(record.Id);
                                } else {
                                    Scenario__c record = scenarioList[i];
                                    record.Rule_Execution_Status__c = SalesIQGlobalConstants.IN_PROGRESS;
                                    record.Run_Response_Details__c = null;
                                    record.Last_Run_Date__c=system.now();
                                    record.Is_Locked__c=true;
                                    toBeUpdatedScenarios.add(record);
                                }
                            }
                            
                        }else{
                            system.debug('--Error-- ' + scenarioList[i].Id);
                            if(trigger.newMap.containsKey(scenarioList[i].Id)) {
                                Scenario__c record = trigger.newMap.get(scenarioList[i].Id);
                                record.Rule_Execution_Status__c=SalesIQGlobalConstants.ERROR;
                                record.Run_Response_Details__c=System.Label.Exclusion_Scenario_errored_out;

                                //ScenarioTriggerHandler.secnarioIdsToAvoidRecursion.add(record.Id);
                            } else {
                                Scenario__c record = scenarioList[i];
                                record.Rule_Execution_Status__c=SalesIQGlobalConstants.ERROR;
                                record.Run_Response_Details__c=System.Label.Exclusion_Scenario_errored_out;
                                toBeUpdatedScenarios.add(record);
                            }
                            j--;
                        }    
                        i=i+1;
                    }
                }
                if(!toBeUpdatedScenarios.isEmpty())
                    update toBeUpdatedScenarios;
            }
        }
        // else{
        //     if(loggerRecord[0].Status__c=='Error') {
        //         for(Scenario__c scenario:scenarioList) {
        //             if(trigger.newMap.containsKey(scenario.Id)) {
        //                 Scenario__c record = trigger.newMap.get(scenario.Id);
        //                 record.Rule_Execution_Status__c=SalesIQGlobalConstants.ERROR;
        //                 record.Run_Response_Details__c=System.Label.Master_sync_failed;

        //                 ScenarioTriggerHandler.secnarioIdsToAvoidRecursion.add(record.Id);
        //             } else {
        //                 Scenario__c record = scenario;
        //                 record.Rule_Execution_Status__c=SalesIQGlobalConstants.ERROR;
        //                 record.Run_Response_Details__c=System.Label.Master_sync_failed;
        //                 toBeUpdatedScenarios.add(record);
        //             }

        //         }
        //     } else {
        //         for(Scenario__c scenario:scenarioList) {

        //             if(trigger.newMap.containsKey(scenario.Id)) {
        //                 Scenario__c record = trigger.newMap.get(scenario.Id);
        //                 record.Run_Response_Details__c=System.Label.Master_sync_running;

        //                 //ScenarioTriggerHandler.secnarioIdsToAvoidRecursion.add(record.Id);
        //             } else {
        //                 Scenario__c record = scenario;
        //                 record.Run_Response_Details__c=System.Label.Master_sync_running;
        //                 toBeUpdatedScenarios.add(record);
        //             }
                    
        //         }
        //     }

        //     // update 
        //     if(!toBeUpdatedScenarios.isEmpty())
        //         update toBeUpdatedScenarios;
        // }
    }
    //siq:4409
    if(Trigger.isAfter && Trigger.isUpdate && trigger.new[0].Scenario_Status__c == SalesIQGlobalConstants.SCENARIO_STATUS_ACTIVE && trigger.new[0].Scenario_Stage__c == SalesIQGlobalConstants.DESIGN && trigger.new[0].Request_Process_Stage__c == SalesIQGlobalConstants.READY && trigger.new[0].EnableEmployeeAssignment__c ==true){
            system.debug('--Inside Scenario Trigger-- Enable Employee assignment');
            list<Scenario__c> scLi=[Select id from scenario__c where id =:trigger.new[0].id];
            ScenarioTriggerHandler.collabrationScenario(scLi[0]);
    }
}