trigger PositionTrigger on Position__c (after Update) {
    if(TriggerContol__c.getValues('PositionTrigger') != null && !TriggerContol__c.getValues('PositionTrigger').IsStopTrigger__c){
        if(trigger.isUpdate){
            string posCode;
            string posID;
            string posName;
            string teamNameCode;
            string teamType;
            
            set<Id> positionIds = new set<Id>();
            for(Position__c Pos : trigger.new){
                if(Pos.Name != trigger.oldMap.get(pos.Id).Name){
                   posCode = Pos.Client_Position_Code__c;
                   posName = Pos.Name;
                   posID = Pos.Id;
                   break;
                }
                //Find all the position where employee has been changed
                if(pos.Employee__c != trigger.oldMap.get(pos.Id).Employee__c){
                	positionIds.add(pos.Id);
                }
            }
            list<Position__c> lsPos = [select Team_Instance__r.Team_Instance_Code__c, Team_iD__r.Name , Team_iD__r.Type__c from Position__c where id =:posID];
            for(Position__c pos : lsPos){
                teamNameCode =  pos.Team_Instance__r.Team_Instance_Code__c;
                teamType =  pos.Team_iD__r.Type__c;
            }
            
            /** Updation of Position Team Instance's parent position id **/
            set<string> posIds = new set<string>();
            set<Id> teamInstances = new set<Id>();

            string posNames = '';

            map<string,string> mapOldNewPosName = new map<string,string>();

            map<string,string> mapPosIdANDparentId = new map<string,string>();
            for(position__c posnew : trigger.new){
                if(posnew.Parent_Position__c != trigger.oldMap.get(posnew.Id).Parent_Position__c){
                	posIds.add((string)posnew.id);
                	mapPosIdANDparentId.put((string) posnew.id, posnew.parent_position__c);
            	}

                /** Variables for Updation of sharedWith Field on Position Geography **/
                if(posnew.Name != trigger.oldMap.get(posnew.Id).Name){
                    teamInstances.add(posnew.Team_Instance__c);

                    posNames = posNames + trigger.oldMap.get(posnew.Id).Name + ';';
                    mapOldNewPosName.put(trigger.oldMap.get(posnew.Id).Name, posnew.Name);
                }
            }
            
            if(posIds.size() > 0){
            	list<position_team_instance__c> posTeamInsList = [select id, Parent_Position_ID__c, position_id__c from position_team_instance__c where position_id__c in: posIds];
	            for(position_team_instance__c posIns : posTeamInsList){
	            	posIns.Parent_Position_ID__c = mapPosIdANDparentId.get(posIns.position_id__c);
	            }
	            
	            if(posTeamInsList.size() > 0){
	            	update posTeamInsList;
	            }	
            }
            /** END of Updation of Position Team Instance's parent position id **/
            
            if(PosCode !=null && PosCode !=''){
            	if(teamType != 'Account'){
                   SyncWithESRIBatch.updateTerritoryNameinESRI(posCode,PosName,teamNameCode);
            	}
            }
            
            //Update User_Access_Permission__c on primary assignment change in Manage Assignment
            if(positionIds.size() > 0){
	            map<Id,Position__c> userPositionMap = new map<Id,Position__c>();
	            for(Position__c pos : [Select id, Team_Instance__c, Employee__c, Employee__r.USER__c, Effective_End_Date__c  from Position__c where id in : positionIds and Employee__r.User__c != null]){
	            	userPositionMap.put(pos.Employee__r.USER__c,pos);
	            }
	            
	            set<Id> userIds = new set<Id>();
	            set<Id> teamInstanceIds = new set<Id>();
	            
	            for(Position__c pos : userPositionMap.values()){
	            	userIds.add(pos.Employee__r.User__c);
	            	teamInstanceIds.add(pos.Team_Instance__c);
	            }
	            
	            map<Id,User_Access_Permission__c> userPositionAccessMap = new map<Id,User_Access_Permission__c>();
	            
	            for(User_Access_Permission__c uap : [Select id, Team_Instance__c, User__c, Position__c, Effective_Start_Date__c, Effective_End_Date__c 
	            									 from User_Access_Permission__c where User__c in : userIds and Team_Instance__c in : teamInstanceIds]){
	            	userPositionAccessMap.put(uap.User__c,uap);
	            }
	            
	            list<User_Access_Permission__c> userPositionsToUpsert = new list<User_Access_Permission__c>();
	            for(Id userId : userPositionMap.keyset()){
	            	User_Access_Permission__c uap;
	            	if(userPositionAccessMap.containsKey(userId)){
	            		uap = userPositionAccessMap.get(userId);
	            	}else{
	            		uap = new User_Access_Permission__c();
	            		uap.User__c = userId;
	            		uap.Team_Instance__c = userPositionMap.get(userId).Team_Instance__c;
	            	}
	            	uap.Position__c = userPositionMap.get(userId).Id;
            		uap.Map_Access_Position__c = userPositionMap.get(userId).Id;
	            	uap.Is_Active__c = true;
	            	uap.Effective_Start_Date__c = System.today();
	            	uap.Effective_End_Date__c = userPositionMap.get(userId).Effective_End_Date__c;
	            	uap.Sharing_Type__c = 'Implicit';
	            	userPositionsToUpsert.add(uap);
	            }
	            if(userPositionsToUpsert.size() > 0){
	            	upsert userPositionsToUpsert;
	            }
            }

            /** Updation of sharedWith Field on Position Geography **/
            /*if(teamInstances.size() > 0){

                posNames = posNames.removeEnd(';');

                system.debug('--posNames ' + posNames);
                system.debug('--mapPosTeamANDname ' + mapOldNewPosName);

                list<Position_Geography__c> listPosGeo = [select Id, SharedWith__c, Position__c from Position_Geography__c where Team_Instance__c in: teamInstances
                                                     and SharedWith__c  includes (:posNames)];
                system.debug('--listPosGeo ' + listPosGeo);
                

                for(Position_Geography__c posGeo : listPosGeo){
                    string sharedWith = posGeo.SharedWith__c;
                    for(string posNameOld : posNames.split(',')){
                        if(sharedWith.contains(posNameOld))
                            sharedWith = sharedWith.replace(posNameOld, mapOldNewPosName.get(posNameOld));
                    }

                    posGeo.SharedWith__c = sharedWith;
                    system.debug('--sharedWith ' + sharedWith);
                }

                update listPosGeo;
            }*/
        }
    }
}