trigger AssignedEmployeeTrigger on Employee_Status__c (after insert, before insert,after update) {
	if(TriggerContol__c.getValues('AssignedEmployeeTrigger') != null && !TriggerContol__c.getValues('AssignedEmployeeTrigger').IsStopTrigger__c){
		if(EmployeeStatusTriggerHandler.runtrigger){
			if(trigger.Isbefore){
				 if (Trigger.isInsert) {
				 	EmployeeStatusTriggerHandler.EmployeeStatusResponse Response = EmployeeStatusTriggerHandler.EmployeeStatusEvent(trigger.new);
					system.debug('Response trigger'+Response);
					if(Response.Error!=null && Response.Error!=''){
						if(!Test.isRunningTest()){
							trigger.new[0].addError(Response.Error);
						}						
					} 
				 }
		    }		    
		}		
	}  
}