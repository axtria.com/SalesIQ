trigger TeamInstanceProductTrigger on Team_Instance_Product__c (after insert, after delete) {
        if(TriggerContol__c.getValues('TeamInstanceProductTrigger') != null && TriggerContol__c.getValues('TeamInstanceProductTrigger').IsStopTrigger__c) return ;

        if(trigger.isInsert){
            ProductManagementUtility.addPositionProducts(trigger.new);
        }  
        if(trigger.isDelete){
            ProductManagementUtility.removePositionProducts(trigger.old);
        }
}