/*@author : Arun Kumar
  @date : 22 July 2017  
  @description : This trigger will fire while updating the Team Instance. If that team Instance has some shared
  ZIPs then user can not reset to ture.
  Along with this user can't allow sharing for Account terr  team and Current Team Instance
  @Param1: event, when trigger will execute
  return : void 
 */ 

trigger ValidateExistingSharedZIPTrigger on Team_Instance__c (before update, before insert,after update) 
{    
    if(TriggerContol__c.getValues('ValidateExistingSharedZIPTrigger') != null && !TriggerContol__c.getValues('ValidateExistingSharedZIPTrigger').IsStopTrigger__c)
    {
    	if(trigger.isBefore)
    	{
	        set<id> setTeamInstance = new set<id>();
			set<id> setScenarioIds = new set<id>();
	        Map<id,Team_Instance__c> mapTeamInstance = new map<id,Team_Instance__c>();
	        string alignmentType ;
			if(trigger.isUpdate){
				try{
		            set<Id> teamInstanceIds = new set<Id>();
		            for(Team_Instance__c teamIns:trigger.new){
						teamInstanceIds.add(teamIns.Id);
						if(teamIns.Scenario__c != null){
							setScenarioIds.add(teamIns.Scenario__c);
						}
		            }

		            map<Id,list<Position__c>> positionMap = new map<Id,list<Position__c>>();
		            for(Position__c pos : [Select id, Team_Instance__c From Position__c WHERE Change_Status_del__c=:SalesIQGlobalConstants.REQUEST_STATUS_SUBMISSION_PENDING and Team_Instance__c IN : teamInstanceIds]){
		            	if(positionMap.containsKey(pos.Team_Instance__c)){
		            		positionMap.get(pos.Team_Instance__c).add(pos);
		            	}else{
		            		positionMap.put(pos.Team_Instance__c, new list<Position__c>{pos});
		            	}
		            }

		            for(Team_Instance__c teamIns:trigger.new){
		                
		                if(trigger.oldMap.get(teamIns.Id).Allow_Submission_Pending__c ==true && teamIns.Allow_Submission_Pending__c==false)
		                {
		                    
		                	/* DO WE NEED THIS SOQL INSIDE FOR LOOP ?? */
		                	/*Raghav - SOQL not allowed in loop. Hence commented below query and used positionMap*/
		                    //List<Position__c> posList=[Select id From Position__c WHERE Change_Status_del__c=:SalesIQGlobalConstants.REQUEST_STATUS_SUBMISSION_PENDING and Team_Instance__c=:teamIns.id];
		                    if(positionMap.get(teamIns.Id).size() > 0){
		                        trigger.new[0].addError(System.Label.Submission_Pending_On_Team_Instance);
		                    }
						}
					}

					system.debug('#### setScenarioIds : '+setScenarioIds);
					
					map<Id,Scenario__c> scenarioMap = new map<Id,Scenario__c>([SELECT Id, Scenario_Stage__c FROM Scenario__c WHERE Id IN : setScenarioIds]);
		            
					for(Team_Instance__c teamIns : trigger.new){
		            	alignmentType = teamIns.Alignment_Type__c;
						if(!scenarioMap.containsKey(teamIns.Scenario__c) || (scenarioMap.get(teamIns.Scenario__c).Scenario_Stage__c != SalesIQGlobalConstants.PAST && scenarioMap.get(teamIns.Scenario__c).Scenario_Stage__c != SalesIQGlobalConstants.PUBLISHING)){
							if(alignmentType == SalesIQGlobalConstants.ALIGNMENT_TYPE_ZIP){
								if((trigger.oldMap.get(teamIns.Id).Restrict_ZIP_Share__c != teamIns.Restrict_ZIP_Share__c || 
									trigger.oldMap.get(teamIns.Id).Shared_Position_Threshold__c > teamIns.Shared_Position_Threshold__c) &&
									trigger.oldMap.get(teamIns.Id).Restrict_ZIP_Share__c == false){
								   
									setTeamInstance.add(teamIns.Id);
									mapTeamInstance.put(teamIns.Id,teamIns );
							   	}
							}
							if(alignmentType == SalesIQGlobalConstants.ALIGNMENT_TYPE_ACCOUNT){
								if((trigger.oldMap.get(teamIns.Id).EnableAccountSharing__c != teamIns.EnableAccountSharing__c || 
									trigger.oldMap.get(teamIns.Id).Account_Shared_Position_Threshold__c > teamIns.Account_Shared_Position_Threshold__c) &&
									trigger.oldMap.get(teamIns.Id).EnableAccountSharing__c == true){
																	
									setTeamInstance.add(teamIns.Id);
									mapTeamInstance.put(teamIns.Id,teamIns );
							   	}
							}else{
								if(((trigger.oldMap.get(teamIns.Id).EnableAccountSharing__c != teamIns.EnableAccountSharing__c || 
									trigger.oldMap.get(teamIns.Id).Account_Shared_Position_Threshold__c > teamIns.Account_Shared_Position_Threshold__c)&&
									trigger.oldMap.get(teamIns.Id).EnableAccountSharing__c == true)
									||	
									((trigger.oldMap.get(teamIns.Id).Restrict_ZIP_Share__c != teamIns.Restrict_ZIP_Share__c || 
									 trigger.oldMap.get(teamIns.Id).Shared_Position_Threshold__c > teamIns.Shared_Position_Threshold__c) &&
									 trigger.oldMap.get(teamIns.Id).Restrict_ZIP_Share__c == false)){
										
									setTeamInstance.add(teamIns.Id);
									mapTeamInstance.put(teamIns.Id,teamIns );
							   	}
							}
						}
		            }
					
					if(setTeamInstance != null && setTeamInstance.size() > 0){
						
					    integer iCount = SalesIQUtility.getSharedZIPByTeamInstance(setTeamInstance,alignmentType);
						if(iCount > 0){
							for(string sTeamInstance:setTeamInstance){
							    if(mapTeamInstance.get(sTeamInstance).Alignment_Type__c != SalesIQGlobalConstants.ALIGNMENT_TYPE_HYBRID){
									//trigger.new[0].addError(String.format(System.Label.ZIP_Unshare_On_TeamInstance, new List<String>{mapTeamInstance.get(sTeamInstance).Alignment_Type__c,mapTeamInstance.get(sTeamInstance).Alignment_Type__c}));
							    }else{
							   		//if(scenarioMap.get(trigger.newMap.get(sTeamInstance).Scenario__c).Scenario_Stage__c != SalesIQGlobalConstants.PUBLISHING)
							   			//trigger.new[0].addError(String.format(System.Label.ZIP_Unshare_On_TeamInstance, new List<String>{System.Label.ZIP+'/'+System.Label.Account,System.Label.ZIP+'/'+System.Label.Account}));
							    }
							}
						}
					}
				}catch(exception ex){
					system.debug('exception in ValidateExistingSharedZIPTrigger '+ex.getMessage());
				}
			}

			/*********************NEW CODE FOR TI INSTANCE CODE UPDATE ***********************************/
			if(trigger.isInsert){
				SalesIQUtility.updateTeamInstanceCode(trigger.new);
			}
			/********************************************************************************************/
    	}    		
	}

	if(trigger.isAfter)
    {
    	set<id> scIdSet = new set<id>();
    	for(Team_Instance__c teamIns : trigger.new)
    	{
    		if(trigger.oldMap.get(teamIns.Id).Alignment_Period__c == SalesIQGlobalConstants.FUTURE_TEAM_CYCLE_TYPE && teamIns.Alignment_Period__c == SalesIQGlobalConstants.CURRENT_TEAM_CYCLE_TYPE)
    		{
    			scIdSet.add(teamIns.Scenario__c);
    		}
    	}
		//SIMPS - 1468 commented code in case of cycle cut over - scenario should not be clear of sync date
    	/*List<Scenario__c> scList = new List<Scenario__c>();
		for(Id scId:scIdSet)
		{
			scList.add(new Scenario__c(id=scId,Last_Sync_Success_Date__c = null,Last_Promote_Success_Date__c = null));
		}
		update scList;*/
    }

}
