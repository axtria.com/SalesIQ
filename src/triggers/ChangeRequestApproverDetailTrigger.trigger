/************************************************************************************
    Name        :   ChangeRequestApproverDetailTrigger.trigger
    Object      :   Change_Request_Approver_Detail__c
    Description :   Trigger on Change_Request_Approver_Detail__c object 
************************************************************************************/
trigger ChangeRequestApproverDetailTrigger on Change_Request_Approver_Detail__c (after insert, after update, after delete) {
    
}