trigger AccountTrigger on Account (after insert,after update){
   if(TriggerContol__c.getValues('AccountTrigger') != null && !TriggerContol__c.getValues('AccountTrigger').IsStopTrigger__c){
     if(Trigger.isAfter){
          if(Trigger.IsInsert || Trigger.IsUpdate){
            if(!Test.isRunningTest()){
                Map<String,Boolean> accountAddressMap=new Map<String,Boolean>();
                set<String> zips= new set<String>();
                 
                for(Account acc:Trigger.New){
                  zips.add(acc.BillingPostalCode);
                }
                  
                List<Account_Address__c> accountAddressList = [Select id,Pincode__c,Account__c FROM Account_Address__c WHERE Pincode__c in:zips];
                for(Account_Address__c accAdd:accountAddressList){
                  accountAddressMap.put(accAdd.Pincode__c,True);
                }
                
                List<Account_Address__c> insertaccountAddressList= new List<Account_Address__c>();
                for(Account acc:Trigger.New){
                    System.debug('####'+accountAddressMap.keySet().contains(acc.BillingPostalCode));
                    if(!accountAddressMap.keySet().contains(acc.BillingPostalCode)){
                        Account_Address__c acAdd =new Account_Address__c(Account__c=acc.id,Pincode__c=acc.BillingPostalCode);
                        insertaccountAddressList.add(acAdd);
                    }     
                }
                  
                System.debug('#############'+insertaccountAddressList);
                if(!insertaccountAddressList.isEmpty())
                  insert insertaccountAddressList;
            }
          }
      }
   }
}