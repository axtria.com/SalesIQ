trigger BusinessRulesTrigger on Business_Rules__c(after insert,after update) 
{
	// List<Business_Rules__c> brList = [select id, status__c,Scenario__c from Business_Rules__c where id in: trigger.new];
	Set<Id> listScenario = new Set<Id>();
	for(Business_Rules__c br : trigger.new)
	{
		if(br.Status__c == CPGConstants.READY)
		{
			listScenario.add(br.Scenario__c);
		}
	}
    
    List<Scenario__c> scUpdateList = [select id, Rule_Execution_Status__c,Last_Promote_Success_Date__c from Scenario__c where id in: listScenario];
    for(Scenario__c sc : scUpdateList)
    {
    	if(sc.Last_Promote_Success_Date__c != null)
    	{
    		sc.Last_Promote_Success_Date__c = null;
    	}
    	sc.Rule_Execution_Status__c = CPGConstants.READY;
    }

    update scUpdateList;
}