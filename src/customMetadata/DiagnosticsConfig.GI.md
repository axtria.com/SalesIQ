<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>GI</label>
    <protected>false</protected>
    <values>
        <field>Columns__c</field>
        <value xsi:type="xsd:string">compare_tier,TIER 1,TIER 2,TIER 3,PROSPECT,not_in_valuation,not_in_universe,total</value>
    </values>
    <values>
        <field>Team__c</field>
        <value xsi:type="xsd:string">GI</value>
    </values>
</CustomMetadata>
