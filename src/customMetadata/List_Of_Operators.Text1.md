<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Concatenate</label>
    <protected>false</protected>
    <values>
        <field>Brief_Description__c</field>
        <value xsi:type="xsd:string">Joins the text strings given in the argument into one text string</value>
    </values>
    <values>
        <field>Comments__c</field>
        <value xsi:type="xsd:string">The string used in the operation (operands) can be a field, a constant or another expression (that returns a string)</value>
    </values>
    <values>
        <field>DbFunction_Name__c</field>
        <value xsi:type="xsd:string">Concat</value>
    </values>
    <values>
        <field>IsCustom__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>NoOfParameters__c</field>
        <value xsi:type="xsd:double">999.0</value>
    </values>
    <values>
        <field>Operator_Type__c</field>
        <value xsi:type="xsd:string">Text</value>
    </values>
    <values>
        <field>Operator__c</field>
        <value xsi:type="xsd:string">Concatenate</value>
    </values>
    <values>
        <field>Parameters__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Return__c</field>
        <value xsi:type="xsd:string">Text</value>
    </values>
    <values>
        <field>Syntax__c</field>
        <value xsi:type="xsd:string">Concatenate (string1, string2,…)</value>
    </values>
    <values>
        <field>isFunction__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
