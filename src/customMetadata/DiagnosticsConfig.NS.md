<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NS</label>
    <protected>false</protected>
    <values>
        <field>Columns__c</field>
        <value xsi:type="xsd:string">compare_tier,9-10,7-8,5-6,2-4,0,ic_ineligible,not_in_universe,total</value>
    </values>
    <values>
        <field>Team__c</field>
        <value xsi:type="xsd:string">NS</value>
    </values>
</CustomMetadata>
