<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OR</label>
    <protected>false</protected>
    <values>
        <field>Brief_Description__c</field>
        <value xsi:type="xsd:string">Checks whether at least one expression given as input is TRUE, and returns value of 1 if any of the expressions is TRUE.</value>
    </values>
    <values>
        <field>Comments__c</field>
        <value xsi:type="xsd:string">All the expressions given as input should have a TRUE / FALSE (1/0) output</value>
    </values>
    <values>
        <field>DbFunction_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>IsCustom__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>NoOfParameters__c</field>
        <value xsi:type="xsd:double">999.0</value>
    </values>
    <values>
        <field>Operator_Type__c</field>
        <value xsi:type="xsd:string">Logical</value>
    </values>
    <values>
        <field>Operator__c</field>
        <value xsi:type="xsd:string">OR</value>
    </values>
    <values>
        <field>Parameters__c</field>
        <value xsi:type="xsd:string">999</value>
    </values>
    <values>
        <field>Return__c</field>
        <value xsi:type="xsd:string">Numeric</value>
    </values>
    <values>
        <field>Syntax__c</field>
        <value xsi:type="xsd:string">OR (expression 1, expression 2,….)</value>
    </values>
    <values>
        <field>isFunction__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
