<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Button</label>
    <protected>false</protected>
    <values>
        <field>Behavior__c</field>
        <value xsi:type="xsd:string">Open new page in new tab</value>
    </values>
    <values>
        <field>Interface__c</field>
        <value xsi:type="xsd:string">Table</value>
    </values>
    <values>
        <field>Package_Extension__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Style__c</field>
        <value xsi:type="xsd:string">font-size:14px;</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">slds-button_neutral</value>
    </values>
    <values>
        <field>isDisabled__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
