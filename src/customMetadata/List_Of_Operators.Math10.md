<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>^</label>
    <protected>false</protected>
    <values>
        <field>Brief_Description__c</field>
        <value xsi:type="xsd:string">Returns a value which is number1 to the power of number2</value>
    </values>
    <values>
        <field>Comments__c</field>
        <value xsi:type="xsd:string">The numbers used in the operation (operands) can be a field, a constant or another expression (that returns a number)</value>
    </values>
    <values>
        <field>DbFunction_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>IsCustom__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>NoOfParameters__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Operator_Type__c</field>
        <value xsi:type="xsd:string">Math</value>
    </values>
    <values>
        <field>Operator__c</field>
        <value xsi:type="xsd:string">^</value>
    </values>
    <values>
        <field>Parameters__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Return__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Syntax__c</field>
        <value xsi:type="xsd:string">number1 ^ number2</value>
    </values>
    <values>
        <field>isFunction__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
