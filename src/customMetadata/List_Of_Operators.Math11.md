<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>SQRT</label>
    <protected>false</protected>
    <values>
        <field>Brief_Description__c</field>
        <value xsi:type="xsd:string">Returns the square root of the number</value>
    </values>
    <values>
        <field>Comments__c</field>
        <value xsi:type="xsd:string">The numbers used in the operation (operands) can be a field, a constant or another expression (that returns a number)</value>
    </values>
    <values>
        <field>DbFunction_Name__c</field>
        <value xsi:type="xsd:string">SQRT</value>
    </values>
    <values>
        <field>IsCustom__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>NoOfParameters__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Operator_Type__c</field>
        <value xsi:type="xsd:string">Math</value>
    </values>
    <values>
        <field>Operator__c</field>
        <value xsi:type="xsd:string">SQRT</value>
    </values>
    <values>
        <field>Parameters__c</field>
        <value xsi:type="xsd:string">[{&quot;DataType&quot;:&quot;Numeric&quot;}]</value>
    </values>
    <values>
        <field>Return__c</field>
        <value xsi:type="xsd:string">Numeric</value>
    </values>
    <values>
        <field>Syntax__c</field>
        <value xsi:type="xsd:string">SQRT (number)</value>
    </values>
    <values>
        <field>isFunction__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
