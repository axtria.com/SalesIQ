<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>greaterOrEqualOperand</label>
    <protected>false</protected>
    <values>
        <field>DataType__c</field>
        <value xsi:type="xsd:string">Numeric</value>
    </values>
    <values>
        <field>Operator_Label__c</field>
        <value xsi:type="xsd:string">Label.Greater_Or_Equal_Operator</value>
    </values>
    <values>
        <field>PostgresOperands__c</field>
        <value xsi:type="xsd:string">&gt;=</value>
    </values>
    <values>
        <field>SalesforceOperands__c</field>
        <value xsi:type="xsd:string">greater_or_equal</value>
    </values>
    <values>
        <field>SourceOperands__c</field>
        <value xsi:type="xsd:string">&gt;=</value>
    </values>
    <values>
        <field>sequence__c</field>
        <value xsi:type="xsd:double">12.0</value>
    </values>
</CustomMetadata>
