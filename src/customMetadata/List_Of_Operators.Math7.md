<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>&lt;</label>
    <protected>false</protected>
    <values>
        <field>Brief_Description__c</field>
        <value xsi:type="xsd:string">Compares both numbers, returns 1 if the first number is less than the second number. Returns 0 otherwise. Can also be used to compare dates.</value>
    </values>
    <values>
        <field>Comments__c</field>
        <value xsi:type="xsd:string">The numbers used in the operation (operands) can be a field, a constant or another expression (that returns a number)</value>
    </values>
    <values>
        <field>DbFunction_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>IsCustom__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>NoOfParameters__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Operator_Type__c</field>
        <value xsi:type="xsd:string">Logical</value>
    </values>
    <values>
        <field>Operator__c</field>
        <value xsi:type="xsd:string">&lt;</value>
    </values>
    <values>
        <field>Parameters__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Return__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Syntax__c</field>
        <value xsi:type="xsd:string">number1 &lt; number2</value>
    </values>
    <values>
        <field>isFunction__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
