<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BRUrl</label>
    <protected>false</protected>
    <values>
        <field>BRMS_Type__c</field>
        <value xsi:type="xsd:string">BRUrl</value>
    </values>
    <values>
        <field>BRMS_Value__c</field>
        <value xsi:type="xsd:string"></value>
    </values>
</CustomMetadata>
