<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>isNotNullOperand</label>
    <protected>false</protected>
    <values>
        <field>DataType__c</field>
        <value xsi:type="xsd:string">TextNumeric</value>
    </values>
    <values>
        <field>Operator_Label__c</field>
        <value xsi:type="xsd:string">Label.Is_Not_Null_Operator</value>
    </values>
    <values>
        <field>PostgresOperands__c</field>
        <value xsi:type="xsd:string">is not null</value>
    </values>
    <values>
        <field>SalesforceOperands__c</field>
        <value xsi:type="xsd:string">is_not_null</value>
    </values>
    <values>
        <field>SourceOperands__c</field>
        <value xsi:type="xsd:string">!= null</value>
    </values>
    <values>
        <field>sequence__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
</CustomMetadata>
