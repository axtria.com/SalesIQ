import psycopg2, pickle, os, traceback, numpy, networkx, pandas, DBconfig # DBconfig is a custom module
from flask import Flask, request, jsonify, render_template
from simple_salesforce import Salesforce
import pandas as pd 
import numpy as np

app = Flask(__name__) # Flask app Initializer

neighbor_file_dir = 'F://PythonAppSalesIQ//'

@app.route('/api/validateContiguityANDDoughnut', methods = ['POST']) 			# listens to the route 'http://host/validateContiguityANDDoughnut' via provided post method.
def validateContiguityANDDoughnut(): 										# This function gets invoked when there is a request on the above route
	try: 
		source 				= request.form['sourcePositionCode'		].split(';')[0] 			# Fetching data from POST Request
		requested_zips 		= request.form['selectedZips'			].split(',')  		# Fetching data from POST Request
		team_instance 		= request.form['selectedCycleCode'		].split(';')[0] 			# Fetching data from POST Request
		zipter 				= request.form['ZIPTerrTableName'		].split(';')[0] 			# Fetching data from POST Request
		destination 		= request.form['destinationPositionCode'].split(';')[0]  		# Fetching data from POST Request
		doDoughnutCheck 	= request.form['doDoughnutCheck'	] 				# Fetching data from POST Request
		doContiguityCheck 	= request.form['doContiguityCheck'	] 			# Fetching data from POST Request

		try:
			file_name = request.form['geoFileName']
		except:
			file_name = 'US_ZIP'

		zip_neighbors = pickle.load(open(neighbor_file_dir+file_name+'.p',"rb"))					# Loads file in which neighbor zips of all zips are stored in format --> z1 [(z1,z2),(z1,z3),(z1,z4)] in pd.DataFrame

		c = psycopg2.connect(database = zipter.split('.')[0], user = DBconfig.user, password = DBconfig.password, host = DBconfig.host).cursor() 		# Connects DB by using credentials from DBConfig.py

		def contiguity():																															# Function to check contiguity
			c.execute("select geography_name from " + zipter + " where level1_code='"+destination+"' and team_instance_name='"+team_instance+"'") 	# Fetches all ZIPs of destination territory
			dest_zips = set(numpy.array(c.fetchall()).squeeze()) 			# Squeezes dimentions to rank 1 and then converts into a set
			Gdata = zip_neighbors[zip_neighbors[0].isin(requested_zips)]	# Gets neighboring data of requested zips

			nodes = list(Gdata[0]).copy()									# Copies requested_zips to nodes
			edges = []														# Initialize an empty list to store edges
			def cflat(x):													# This funtion will take a row of neighboring data as input; 
				for i in x[1]:													# Iterate in list of edge tuples 
					if i in nodes: edges.append((x[0],i))						# If 2nd element in tuple belongs to requested zips then append the tuple in edges.
			_ = Gdata.apply(cflat, axis=1)										# Calls cflat() by row

			G = networkx.Graph()											# Declares an empty unweighted undirected graph
			G.add_nodes_from(nodes)											# Add nodes to the graph
			G.add_edges_from(edges)											# Add edges to the graph

			all_neighbors = set()											# Declares an empty set to store neighbors of graph component
			def neighbors(x):												# This function takes a graph component neighbor data as input; and appends all the level 1 neighbors to all_neighbors set
				for i in x:													# Iterates in list of edge tuples
					all_neighbors.add(i)									# appends 2nd element of edge tuple into all_neighbors set

			components = list(networkx.connected_components(G))				# Stores component of the graph in a list named components

			if len(dest_zips) == 0:
				if len(components) == 1:
					return '0'
				else:
					return '103'

			for component in components:									# Iterates in each component
				Cdata = Gdata[Gdata[0].isin(component)]						# Gathers neigbor data of component

				all_neighbors = set()										# Declares an empty set to store neighbors of the component
				_ = Cdata[1].apply(neighbors)								# Calls neighbors() on Cdata row by row to get neighbor data in all_neighbors set

				if len(all_neighbors.intersection(dest_zips)) == 0:			# Checks if neighbors has ZIPs in destination territory
					return '103'											# Error code for contiguity break
			return '0'														# Code for valid contiguity

		def doughnut():
			c.execute("select geography_name from " + zipter + " where level1_code='"+source+"' and team_instance_name='"+team_instance+"'") 		# Fetches all ZIPs of source territory
			source_zips = numpy.array(c.fetchall()).squeeze() 				# Squeezes dimentions to rank 1 numpy array

			
			Gdata = zip_neighbors[zip_neighbors[0].isin(requested_zips)]	# Gets neighboring data of requested zips
			nodes = list(Gdata[0]).copy()									# Copies requested_zips to nodes
			def neighbors(x):												# This function list of edge tuples as input; appends all level 1 neighbors to nodes 
				for i in x:													# Iterates in list of edge tuple
					nodes.append(i)										# Appends 2nd element of each edge tuple to nodes
			_ = Gdata[1].apply(neighbors)									# Calls neighbors() row by row on neighbor data

			nodes = pandas.Series(list(set(nodes)))							# Convert nodes into pd.Series of unique element
			nodes = nodes[nodes.isin(source_zips)]							# Filters nodes of source territory

			Gdata = zip_neighbors[zip_neighbors[0].isin(nodes)] 			# Prepares data for graph (Filter zip_neighbors on requested source_Position_Code)
			edges = []														# Initialize an empty list to store edges

			def dflat(x):													# This function takes neighbor data as input; and appends graph edges to edges list
				for i in x[1]:													# Iterates in list of edge tuples
					if i in list(nodes): edges.append((x[0],i))					# Appends valid edges to the edges list		
			_ = Gdata.apply(dflat, axis=1)										# Calls dflat() row by row on graph data

			G = networkx.Graph()											# Declares an empty unweighted undirected graph
			G.add_nodes_from(list(nodes))									# Add nodes to the graph
			G.add_edges_from(edges)											# Add edges to the graph
			N1 = networkx.number_connected_components(G)					# Calculates number of components in the graph

			G.remove_nodes_from(requested_zips)								# Remove requested ZIPs from the graph
			N2 = networkx.number_connected_components(G)					# Recalculates number of components in th graph

			if N2 <= N1: return '0'											# Valid move if number of components has NOT increaced
			else: return '104'												# Doughnut if number of components has increased

		if 	 doContiguityCheck == 'true'  and doDoughnutCheck == 'false': return contiguity()		# Only Contiguity Check
		elif doContiguityCheck == 'false' and doDoughnutCheck == 'true' : return doughnut()		# Only Doughnut Check
		elif doContiguityCheck == 'true'  and doDoughnutCheck == 'true' :							# Both Doughnut and Contiguity Check
			temp = int(doughnut()) + int(contiguity())											# Adds code of both Doughnut and Contiguity Checks
			if temp == 207: return '105'														# If both are present return 105 error code
			else: return str(temp)																# else return results of Doughnut and Contiguity Check
	except: return traceback.format_exc()

@app.route('/api/create',methods=['GET'])
def create():
	try:
		country = request.args.get('filename')
		table = request.args.get('table')
		c = psycopg2.connect(database = table.split('.')[0], user = DBconfig.user, password = DBconfig.password, host = DBconfig.host).cursor()
		c.execute("select zip, neighbor_zip from "+table)
		a = pandas.DataFrame(c.fetchall())
		a.loc[:,1] = a[1].apply(lambda x: x.replace(' ','').split(','))
		pickle.dump(a,open(neighbor_file_dir+country+'.p','wb'))
		return 'success'
	except:
		return traceback.format_exc()

@app.route('/api/CalculateNumberOfFaces', methods = ['POST'])
def CalculateNumberOfFaces():
    try:
        counterTeamInstances = request.form['counterTeamInstances'].split(',')
        source_Id            = request.form['source_Id']
        destination_Id       = request.form['destination_Id']
        baseTeamInstance     = request.form['selectedCycle']
        selectedZips         = set(request.form['selectedZips'].split(','))
        customMetricCIMId    = request.form['customMetricCIMId']
        movementType         = request.form['movementType']
        pageName             = request.form['pageName']
        status               = request.form['status']
        orgURL               = request.form['orgURL']
        session_id           = request.form['accessToken']
        ns                   = request.form['namespace'] 

        sf = Salesforce(instance_url= orgURL, session_id= session_id)

        def generate_HTML_In_View():
            try:
                # query for Name and Position Code of Source and destinaion
                query_position_info  = "Select Id, Name, "+ns+"Client_Position_code__c from "+ns+"Position__c where Id = '" + source_Id + "' or Id = '" + destination_Id + "'"
                result_position_info = pd.DataFrame(sf.query_all(query_position_info)['records'])

                Source_Name      = result_position_info.loc[result_position_info['Id'] == source_Id,     'Name'].to_string(index=False)
                Destination_Name = result_position_info.loc[result_position_info['Id'] == destination_Id,'Name'].to_string(index=False)
                Source_Code      = result_position_info.loc[result_position_info['Id'] == source_Id,     ns+'Client_Position_Code__c'].to_string(index=False)
                Destination_Code = result_position_info.loc[result_position_info['Id'] == destination_Id,ns+'Client_Position_Code__c'].to_string(index=False)

                # Query for team instance alignment period and teamcycle name
                query_team_instance_info  = "select id,"+ns+"Alignment_Period__c, "+ns+"Team_Cycle_Name__c from "+ns+"team_instance__c where id in " + str(tuple(counterTeamInstances+[baseTeamInstance]))
                result_team_instance_info = pd.DataFrame(sf.query_all(query_team_instance_info)['records'])

                base_alignment_period = result_team_instance_info.loc[result_team_instance_info['Id'] == baseTeamInstance,ns+'Alignment_Period__c'].to_string(index=False)
                base_cycle_name       = result_team_instance_info.loc[result_team_instance_info['Id'] == baseTeamInstance,ns+'Team_Cycle_Name__c'].to_string(index=False)
                
                query_unassigned_zips  = "Select id from "+ns+"position__c where "+ns+"client_position_code__c='00000' and "+ns+"Team_Instance__c in " + str(tuple(counterTeamInstances+[baseTeamInstance]))
                result_unassigned_zips = pd.DataFrame(sf.query_all(query_unassigned_zips)['records'])
                
                #Query for BEFORE custom metric indexes of source and destination
                query_custom_metric  = "select Id,"+ns+"Original__c,"+ns+"CIM_Config__r."+ns+"Display_Name__c,"+ns+"Position_Team_Instance__r."+ns+"Position_id__c from "+ns+"CIM_Position_Metric_summary__c where "+ns+"Cim_config__c  = '"+ customMetricCIMId +"' and "+ns+"Team_Instance__c= '"+ baseTeamInstance +"' and "+ns+"Position_Team_Instance__r."+ns+"Position_id__c in "+ str((source_Id,destination_Id))
                result_custom_metric = pd.DataFrame(sf.query_all(query_custom_metric)['records'])

                if len(result_custom_metric) == 0:
                    return '201'
                   
                result_custom_metric[ns+'Position_id__c'] = result_custom_metric[ns+'Position_Team_Instance__r'].apply(lambda x: x[ns+'Position_ID__c'])

                metric_name        = result_custom_metric.loc[0,ns+'CIM_Config__r'][ns+'Display_Name__c']
                source_before      = result_custom_metric.loc[result_custom_metric[ns+'Position_id__c'] == source_Id,     ns+'Original__c'].to_string(index=False)
                destination_before = result_custom_metric.loc[result_custom_metric[ns+'Position_id__c'] == destination_Id,ns+'Original__c'].to_string(index=False)

                def get_alignment_status(x):
                    if   x == 'Current':return 'Active'
                    elif x == 'Future' :return 'Future Active'

                # Query to get ZIPS of Source and Destination
                query_source_and_destination_zips  = "select "+ns+"geography__r.Name, "+ns+"Proposed_Position__c from "+ns+"position_geography__c where "+ns+"Assignment_status__c = '"+ get_alignment_status(base_alignment_period) +"' and "+ns+"Team_instance__c = '"+ baseTeamInstance +"' and ( "+ns+"Proposed_Position__c = '"+ source_Id +"' or "+ns+"Proposed_Position__c = '"+ destination_Id + "' )"
                result_source_and_destination_zips = pd.DataFrame(sf.query_all(query_source_and_destination_zips)['records'])
                if len(result_source_and_destination_zips) == 0:
                    return '202'
                result_source_and_destination_zips[ns+'Geography__r'] = result_source_and_destination_zips[ns+'Geography__r'].apply(lambda x: x['Name'])

                source_zips      = set(result_source_and_destination_zips.loc[result_source_and_destination_zips[ns+'Proposed_Position__c'] == source_Id,     ns+'Geography__r'].unique()) - selectedZips
                destination_zips = set(result_source_and_destination_zips.loc[result_source_and_destination_zips[ns+'Proposed_Position__c'] == destination_Id,ns+'Geography__r'].unique()) | selectedZips

                source_after = 0
                destination_after = 0

                for TI in counterTeamInstances:
                    query_counter_zipter  = "select "+ns+"geography__r.Name, "+ns+"Proposed_Position__c from "+ns+"position_geography__c where "+ns+"Assignment_status__c = '"+ get_alignment_status(result_team_instance_info.loc[result_team_instance_info['Id'] == TI,ns+'Alignment_Period__c'].to_string(index=False)) +"' and "+ns+"Team_instance__c = '"+ TI +"' and "+ns+"Proposed_Position__c not in " + str(tuple(result_unassigned_zips['Id'])).replace(",)",")")
                    print(query_counter_zipter)
                    result_counter_zipter = pd.DataFrame(sf.query_all(query_counter_zipter)['records'])
                    if len(result_counter_zipter) == 0:
                        return '202'
                    result_counter_zipter[ns+'Geography__r'] = result_counter_zipter[ns+'Geography__r'].apply(lambda x: x['Name'])
                    
                    source_after      += result_counter_zipter.loc[result_counter_zipter[ns+'Geography__r'].isin(source_zips),     ns+'Proposed_Position__c'].nunique()
                    destination_after += result_counter_zipter.loc[result_counter_zipter[ns+'Geography__r'].isin(destination_zips),ns+'Proposed_Position__c'].nunique()

                if Source_Code      == '00000': source_before      = source_after      = 0
                if Destination_Code == '00000': destination_before = destination_after = 0
                    
                html = """<table cellpadding="1" cellspacing="1" border="1" id="table-example-1" style="border-top:none !important;">  <thead> <tr><th rowspan="2">Select</th><th rowspan="2">Territory Code</th><th rowspan="2">Territory Name</th><th colspan="2" style="border-bottom: 0px !important; text-align: center; border-left: 1px solid #d8dde6;">"""+ metric_name +"""</th></tr><tr><th style="border-left: 1px solid #d8dde6; text-align: center;">"""+ base_cycle_name +"""</th><th style="border-left: 1px solid #d8dde6; text-align: center;">Proposed</th></tr></thead><tbody><tr><td style="border-right: 1px solid #d8dde6;">Source</td><td style="border-right: 1px solid #d8dde6;">""" + Source_Code + """</td><td style="border-right: 1px solid #d8dde6;">""" + Source_Name + """</td><td style=" text-align: right; border-left: 1px solid #d8dde6; padding-right: 25px;">"""+ str(source_before) +"""</td><td style=" text-align: right; border-left: 1px solid #d8dde6; padding-right: 25px;">""" + str(source_after) + """</td></tr><tr><td style="border-right: 1px solid #d8dde6;">Destination</td><td style="border-right: 1px solid #d8dde6;">""" + Destination_Code + """</td><td style="border-right: 1px solid #d8dde6;">""" + Destination_Name + """</td><td style=" text-align: right; border-left: 1px solid #d8dde6; padding-right: 25px;">""" + str(destination_before) + """</td><td style=" text-align: right; border-left: 1px solid #d8dde6; padding-right: 25px;">""" + str(destination_after) + """</td></tr></tbody></table>"""
                return html.replace("Series([], )","0")
            except:
                return traceback.format_exc()


        def update_CIMSummary_For_Pending_Request():
            try:
                # query for Name and Position Code of Source and destinaion
                query_position_info  = "Select Id, Name, "+ns+"Client_Position_code__c from "+ns+"Position__c where Id = '" + source_Id + "' or Id = '" + destination_Id + "'"
                result_position_info = pd.DataFrame(sf.query_all(query_position_info)['records'])

                Source_Name      = result_position_info.loc[result_position_info['Id'] == source_Id,     'Name'].to_string(index=False)
                Destination_Name = result_position_info.loc[result_position_info['Id'] == destination_Id,'Name'].to_string(index=False)
                Source_Code      = result_position_info.loc[result_position_info['Id'] == source_Id,     ns+'Client_Position_Code__c'].to_string(index=False)
                Destination_Code = result_position_info.loc[result_position_info['Id'] == destination_Id,ns+'Client_Position_Code__c'].to_string(index=False)

                # Query for team instance alignment period and teamcycle name
                query_team_instance_info  = "select id,"+ns+"Alignment_Period__c, "+ns+"Team_Cycle_Name__c from "+ns+"team_instance__c where id in " + str(tuple(counterTeamInstances+[baseTeamInstance]))
                result_team_instance_info = pd.DataFrame(sf.query_all(query_team_instance_info)['records'])

                base_alignment_period = result_team_instance_info.loc[result_team_instance_info['Id'] == baseTeamInstance,ns+'Alignment_Period__c'].to_string(index=False)
                base_cycle_name       = result_team_instance_info.loc[result_team_instance_info['Id'] == baseTeamInstance,ns+'Team_Cycle_Name__c'].to_string(index=False)
                
                query_unassigned_zips  = "Select id from "+ns+"position__c where "+ns+"client_position_code__c='00000' and "+ns+"Team_Instance__c in " + str(tuple(counterTeamInstances+[baseTeamInstance]))
                result_unassigned_zips = pd.DataFrame(sf.query_all(query_unassigned_zips)['records'])

                #Query for BEFORE custom metric indexes of source and destination
                query_custom_metric  = "select Id,"+ns+"Original__c,"+ns+"CIM_Config__r."+ns+"Display_Name__c,"+ns+"Position_Team_Instance__r."+ns+"Position_id__c from "+ns+"CIM_Position_Metric_summary__c where "+ns+"Cim_config__c  = '"+ customMetricCIMId +"' and "+ns+"Team_Instance__c= '"+ baseTeamInstance +"' and "+ns+"Position_Team_Instance__r."+ns+"Position_id__c in "+ str((source_Id,destination_Id))
                result_custom_metric = pd.DataFrame(sf.query_all(query_custom_metric)['records'])

                if len(result_custom_metric) == 0:
                    return '201'
                   
                result_custom_metric[ns+'Position_id__c'] = result_custom_metric[ns+'Position_Team_Instance__r'].apply(lambda x: x[ns+'Position_ID__c'])

                metric_name        = result_custom_metric.loc[0,ns+'CIM_Config__r'][ns+'Display_Name__c']
                source_before      = result_custom_metric.loc[result_custom_metric[ns+'Position_id__c'] == source_Id,ns+'Original__c'     ].to_string(index=False)
                source_cim_id      = result_custom_metric.loc[result_custom_metric[ns+'Position_id__c'] == source_Id,'Id'              ].to_string(index=False)
                destination_before = result_custom_metric.loc[result_custom_metric[ns+'Position_id__c'] == destination_Id,ns+'Original__c'].to_string(index=False)
                destination_cim_id = result_custom_metric.loc[result_custom_metric[ns+'Position_id__c'] == destination_Id,'Id'         ].to_string(index=False)


                def get_alignment_status(x):
                    if   x == 'Current':return 'Active'
                    elif x == 'Future' :return 'Future Active'

                # Query to get ZIPS of Source and Destination
                query_source_and_destination_zips  = "select "+ns+"geography__r.Name, "+ns+"Proposed_Position__c from "+ns+"position_geography__c where "+ns+"Assignment_status__c = '"+ get_alignment_status(base_alignment_period) +"' and "+ns+"Team_instance__c = '"+ baseTeamInstance +"' and ( "+ns+"Proposed_Position__c = '"+ source_Id +"' or "+ns+"Proposed_Position__c = '"+ destination_Id + "' )"
                result_source_and_destination_zips = pd.DataFrame(sf.query_all(query_source_and_destination_zips)['records'])
                if len(result_source_and_destination_zips) == 0:
                    return '202'
                result_source_and_destination_zips[ns+'Geography__r'] = result_source_and_destination_zips[ns+'Geography__r'].apply(lambda x: x['Name'])

                source_zips      = set(result_source_and_destination_zips.loc[result_source_and_destination_zips[ns+'Proposed_Position__c'] == source_Id,     ns+'Geography__r'].unique()) - selectedZips
                destination_zips = set(result_source_and_destination_zips.loc[result_source_and_destination_zips[ns+'Proposed_Position__c'] == destination_Id,ns+'Geography__r'].unique()) | selectedZips

                source_after = 0
                destination_after = 0

                for TI in counterTeamInstances:
                    query_counter_zipter  = "select "+ns+"geography__r.Name, "+ns+"Proposed_Position__c from "+ns+"position_geography__c where "+ns+"Assignment_status__c = '"+ get_alignment_status(result_team_instance_info.loc[result_team_instance_info['Id'] == TI,ns+'Alignment_Period__c'].to_string(index=False)) +"' and "+ns+"Team_instance__c = '"+ TI +"' and "+ns+"Proposed_Position__c not in " + str(tuple(result_unassigned_zips['Id'])).replace(",)",")")
                    result_counter_zipter = pd.DataFrame(sf.query_all(query_counter_zipter)['records'])
                    if len(result_counter_zipter) == 0:
                        return '202'
                    result_counter_zipter[ns+'Geography__r'] = result_counter_zipter[ns+'Geography__r'].apply(lambda x: x['Name'])
                    
                    source_after      += result_counter_zipter.loc[result_counter_zipter[ns+'Geography__r'].isin(source_zips),     ns+'Proposed_Position__c'].nunique()
                    destination_after += result_counter_zipter.loc[result_counter_zipter[ns+'Geography__r'].isin(destination_zips),ns+'Proposed_Position__c'].nunique()

                if Source_Code      == '00000': source_before      = source_after      = 0
                if Destination_Code == '00000': destination_before = destination_after = 0   

                html = """<table cellpadding="1" cellspacing="1" border="1" id="table-example-1" style="border-top:none !important;">  <thead> <tr><th rowspan="2">Select</th><th rowspan="2">Territory Code</th><th rowspan="2">Territory Name</th><th colspan="2" style="border-bottom: 0px !important; text-align: center; border-left: 1px solid #d8dde6;">"""+ metric_name +"""</th></tr><tr><th style="border-left: 1px solid #d8dde6; text-align: center;">"""+ base_cycle_name +"""</th><th style="border-left: 1px solid #d8dde6; text-align: center;">Proposed</th></tr></thead><tbody><tr><td style="border-right: 1px solid #d8dde6;">Source</td><td style="border-right: 1px solid #d8dde6;">""" + Source_Code + """</td><td style="border-right: 1px solid #d8dde6;">""" + Source_Name + """</td><td style=" text-align: right; border-left: 1px solid #d8dde6; padding-right: 25px;">"""+ str(source_before) +"""</td><td style=" text-align: right; border-left: 1px solid #d8dde6; padding-right: 25px;">""" + str(source_after) + """</td></tr><tr><td style="border-right: 1px solid #d8dde6;">Destination</td><td style="border-right: 1px solid #d8dde6;">""" + Destination_Code + """</td><td style="border-right: 1px solid #d8dde6;">""" + Destination_Name + """</td><td style=" text-align: right; border-left: 1px solid #d8dde6; padding-right: 25px;">""" + str(destination_before) + """</td><td style=" text-align: right; border-left: 1px solid #d8dde6; padding-right: 25px;">""" + str(destination_after) + """</td></tr></tbody></table>"""

                update = [{'Id':source_cim_id,ns+'Proposed__c':source_after},{'Id':destination_cim_id,ns+'Proposed__c':destination_after}]

                # Query to get affected positions in counter team instances
                query_affected_positions  = "select "+ns+"Proposed_Position__c,"+ns+"Team_Instance__c from "+ns+"position_geography__c where "+ns+"Team_Instance__c in "+ str(tuple(counterTeamInstances)).replace(",)",")") + " and "+ns+"geography__r.Name in " + str(tuple(selectedZips)).replace(",)",")")
                result_affected_positions = pd.DataFrame(sf.query_all(query_affected_positions)['records'])
                del result_affected_positions['attributes']
                result_affected_positions = result_affected_positions.drop_duplicates().reset_index(drop=True)

                # Query to get zips of affected positions
                query_affected_positions_zips  = "select "+ns+"geography__r.Name,"+ns+"Team_Instance__c,"+ns+"Proposed_Position__c from "+ns+"position_geography__c where "+ns+"Team_Instance__c in "+ str(tuple(result_affected_positions[ns+'Team_Instance__c'])).replace(",)",")") +" and "+ns+"Proposed_Position__c in "+str(tuple(result_affected_positions[ns+'Proposed_Position__c'])).replace(",)",")")
                result_affected_positions_zips = pd.DataFrame(sf.query_all(query_affected_positions_zips)['records'])
                del result_affected_positions_zips['attributes']
                result_affected_positions_zips[ns+'Geography__r'] = result_affected_positions_zips[ns+'Geography__r'].apply(lambda x: x['Name'])

                # Query to get zipter of base team instance
                query_base_zipter  = "select "+ns+"geography__r.Name,"+ns+"Proposed_Position__c from "+ns+"position_geography__c where "+ns+"Team_Instance__c = '"+baseTeamInstance+"' and "+ ns+"Proposed_Position__c not in "+ str(tuple(result_unassigned_zips['Id'])).replace(",)",")")
                result_base_zipter = pd.DataFrame(sf.query_all(query_base_zipter)['records'])
                result_base_zipter[ns+'Geography__r'] = result_base_zipter[ns+'Geography__r'].apply(lambda x: x['Name'])

                # getting face count of each affected position in counter team instances
                face_count = result_affected_positions_zips.groupby([ns+'Team_Instance__c',ns+'Proposed_Position__c']).agg(lambda x: result_base_zipter.loc[result_base_zipter[ns+'Geography__r'].isin(x),ns+'Proposed_Position__c'].nunique()).reset_index()
                face_count.rename(columns={ns+'Geography__r':'fcount',ns+'Proposed_Position__c':ns+'Position_ID__c'},inplace=True)

                # Query to get customMetric Id of each counter team instances
                query_CustomMetric_Ids  = "select Id,"+ns+"Team_Instance__c from "+ns+"CIM_config__c where "+ns+"Is_Custom_Metric__c = true and "+ns+"Team_Instance__c in "+ str(tuple(counterTeamInstances)).replace(",)",")") +" and "+ns+"Enable__c = true and "+ns+"Change_Request_Type__r."+ns+"CR_Type_Name__c = '"+movementType+"'"
                result_CustomMetric_Ids = pd.DataFrame(sf.query_all(query_CustomMetric_Ids)['records'])
                del result_CustomMetric_Ids['attributes']

                # Query to get CIM_Position_Metric_summary__c Id of each affected counter position
                query_CIM_Position_Metric_summary_Id  = "select Id,"+ns+"Team_Instance__c, "+ns+"Position_Team_Instance__r."+ns+"Position_id__c from "+ns+"CIM_Position_Metric_summary__c where "+ns+"Cim_config__c in "+ str(tuple(result_CustomMetric_Ids['Id'])).replace(",)",")") +" and "+ns+"Position_Team_Instance__r."+ns+"Position_id__c in " + str(tuple(face_count[ns+'Position_ID__c'])).replace(",)",")")
                result_CIM_Position_Metric_summary_Id = pd.DataFrame(sf.query_all(query_CIM_Position_Metric_summary_Id)['records'])
                result_CIM_Position_Metric_summary_Id[ns+'Position_ID__c'] = result_CIM_Position_Metric_summary_Id[ns+'Position_Team_Instance__r'].apply(lambda x: x[ns+'Position_ID__c'])
                del result_CIM_Position_Metric_summary_Id['attributes'],result_CIM_Position_Metric_summary_Id[ns+'Position_Team_Instance__r']

                update_values = pd.merge(result_CIM_Position_Metric_summary_Id,face_count,on=[ns+'Team_Instance__c',ns+'Position_ID__c'])

                _ = update_values.apply(lambda x: update.append({'Id':x['Id'],ns+'Proposed__c':x['fcount']}), axis=1)
                _update_response = eval("sf.bulk."+ns+"CIM_Position_Metric_summary__c.update(update)")

                return html.replace("Series([], )","0")
            except:
                return traceback.format_exc()
                

        def update_CIMSummary_For_Update_Request(updateColumns):
            try:
                # query for Name and Position Code of Source and destinaion
                query_position_info  = "Select Id, Name, "+ns+"Client_Position_code__c from "+ns+"Position__c where Id = '" + source_Id + "' or Id = '" + destination_Id + "'"
                result_position_info = pd.DataFrame(sf.query_all(query_position_info)['records'])

                Source_Name      = result_position_info.loc[result_position_info['Id'] == source_Id,     'Name'].to_string(index=False)
                Destination_Name = result_position_info.loc[result_position_info['Id'] == destination_Id,'Name'].to_string(index=False)
                Source_Code      = result_position_info.loc[result_position_info['Id'] == source_Id,     ns+'Client_Position_Code__c'].to_string(index=False)
                Destination_Code = result_position_info.loc[result_position_info['Id'] == destination_Id,ns+'Client_Position_Code__c'].to_string(index=False)

                # Query for team instance alignment period and teamcycle name
                query_team_instance_info  = "select id,"+ns+"Alignment_Period__c, "+ns+"Team_Cycle_Name__c from "+ns+"team_instance__c where id in " + str(tuple(counterTeamInstances+[baseTeamInstance]))
                result_team_instance_info = pd.DataFrame(sf.query_all(query_team_instance_info)['records'])

                base_alignment_period = result_team_instance_info.loc[result_team_instance_info['Id'] == baseTeamInstance,ns+'Alignment_Period__c'].to_string(index=False)
                base_cycle_name       = result_team_instance_info.loc[result_team_instance_info['Id'] == baseTeamInstance,ns+'Team_Cycle_Name__c' ].to_string(index=False)
                
                query_unassigned_zips  = "Select id from "+ns+"position__c where "+ns+"client_position_code__c='00000' and "+ns+"Team_Instance__c in " + str(tuple(counterTeamInstances+[baseTeamInstance]))
                result_unassigned_zips = pd.DataFrame(sf.query_all(query_unassigned_zips)['records'])
                
                #Query for BEFORE custom metric indexes of source and destination
                query_custom_metric  = "select Id,"+ns+"Original__c,"+ns+"CIM_Config__r."+ns+"Display_Name__c,"+ns+"Position_Team_Instance__r."+ns+"Position_id__c from "+ns+"CIM_Position_Metric_summary__c where "+ns+"Cim_config__c  = '"+ customMetricCIMId +"' and "+ns+"Team_Instance__c= '"+ baseTeamInstance +"' and "+ns+"Position_Team_Instance__r."+ns+"Position_id__c in "+ str((source_Id,destination_Id))
                result_custom_metric = pd.DataFrame(sf.query_all(query_custom_metric)['records'])

                if len(result_custom_metric) == 0:
                    return '201'
                   
                result_custom_metric[ns+'Position_id__c'] = result_custom_metric[ns+'Position_Team_Instance__r'].apply(lambda x: x[ns+'Position_ID__c'])

                metric_name        = result_custom_metric.loc[0,ns+'CIM_Config__r'][ns+'Display_Name__c']
                source_before      = result_custom_metric.loc[result_custom_metric[ns+'Position_id__c'] == source_Id,ns+'Original__c'     ].to_string(index=False)
                source_cim_id      = result_custom_metric.loc[result_custom_metric[ns+'Position_id__c'] == source_Id,'Id'              ].to_string(index=False)
                destination_before = result_custom_metric.loc[result_custom_metric[ns+'Position_id__c'] == destination_Id,ns+'Original__c'].to_string(index=False)
                destination_cim_id = result_custom_metric.loc[result_custom_metric[ns+'Position_id__c'] == destination_Id,'Id'         ].to_string(index=False)


                def get_alignment_status(x):
                    if   x == 'Current':return 'Active'
                    elif x == 'Future' :return 'Future Active'

                # Query to get ZIPS of Source and Destination
                query_source_and_destination_zips  = "select "+ns+"geography__r.Name, "+ns+"Position__c from "+ns+"position_geography__c where "+ns+"Assignment_status__c = '"+ get_alignment_status(base_alignment_period) +"' and "+ns+"Team_instance__c = '"+ baseTeamInstance +"' and ( "+ns+"Proposed_Position__c = '"+ source_Id +"' or "+ns+"Proposed_Position__c = '"+ destination_Id + "' )"
                result_source_and_destination_zips = pd.DataFrame(sf.query_all(query_source_and_destination_zips)['records'])
                if len(result_source_and_destination_zips) == 0:
                    return '202'
                result_source_and_destination_zips[ns+'Geography__r'] = result_source_and_destination_zips[ns+'Geography__r'].apply(lambda x: x['Name'])

                if (status == 'Rejected' or  status   == 'Cancelled') : 
                    source_zips      = set(result_source_and_destination_zips.loc[result_source_and_destination_zips[ns+'Position__c'] == source_Id,     ns+'Geography__r'].unique()) 
                    destination_zips = set(result_source_and_destination_zips.loc[result_source_and_destination_zips[ns+'Position__c'] == destination_Id,ns+'Geography__r'].unique()) 
                else:
                    source_zips      = set(result_source_and_destination_zips.loc[result_source_and_destination_zips[ns+'Position__c'] == source_Id,     ns+'Geography__r'].unique()) - selectedZips
                    destination_zips = set(result_source_and_destination_zips.loc[result_source_and_destination_zips[ns+'Position__c'] == destination_Id,ns+'Geography__r'].unique()) | selectedZips

                source_after = 0
                destination_after = 0

                for TI in counterTeamInstances:
                    query_counter_zipter  = "select "+ns+"geography__r.Name, "+ns+"Position__c from "+ns+"position_geography__c where "+ns+"Assignment_status__c = '"+ get_alignment_status(result_team_instance_info.loc[result_team_instance_info['Id'] == TI,ns+'Alignment_Period__c'].to_string(index=False)) +"' and "+ns+"Team_instance__c = '"+ TI +"' and "+ns+"Position__c not in " + str(tuple(result_unassigned_zips['Id'])).replace(",)",")")
                    result_counter_zipter = pd.DataFrame(sf.query_all(query_counter_zipter)['records'])
                    if len(result_counter_zipter) == 0:
                        return '202'
                    result_counter_zipter[ns+'Geography__r'] = result_counter_zipter[ns+'Geography__r'].apply(lambda x: x['Name'])
                    
                    source_after      += result_counter_zipter.loc[result_counter_zipter[ns+'Geography__r'].isin(source_zips),     ns+'Position__c'].nunique()
                    destination_after += result_counter_zipter.loc[result_counter_zipter[ns+'Geography__r'].isin(destination_zips),ns+'Position__c'].nunique()

                if Source_Code      == '00000': source_before      = source_after      = 0
                if Destination_Code == '00000': destination_before = destination_after = 0

                html = """<table cellpadding="1" cellspacing="1" border="1" id="table-example-1" style="border-top:none !important;">  <thead> <tr><th rowspan="2">Select</th><th rowspan="2">Territory Code</th><th rowspan="2">Territory Name</th><th colspan="2" style="border-bottom: 0px !important; text-align: center; border-left: 1px solid #d8dde6;">"""+ metric_name +"""</th></tr><tr><th style="border-left: 1px solid #d8dde6; text-align: center;">"""+ base_cycle_name +"""</th><th style="border-left: 1px solid #d8dde6; text-align: center;">Proposed</th></tr></thead><tbody><tr><td style="border-right: 1px solid #d8dde6;">Source</td><td style="border-right: 1px solid #d8dde6;">""" + Source_Code + """</td><td style="border-right: 1px solid #d8dde6;">""" + Source_Name + """</td><td style=" text-align: right; border-left: 1px solid #d8dde6; padding-right: 25px;">"""+ str(source_before) +"""</td><td style=" text-align: right; border-left: 1px solid #d8dde6; padding-right: 25px;">""" + str(source_after) + """</td></tr><tr><td style="border-right: 1px solid #d8dde6;">Destination</td><td style="border-right: 1px solid #d8dde6;">""" + Destination_Code + """</td><td style="border-right: 1px solid #d8dde6;">""" + Destination_Name + """</td><td style=" text-align: right; border-left: 1px solid #d8dde6; padding-right: 25px;">""" + str(destination_before) + """</td><td style=" text-align: right; border-left: 1px solid #d8dde6; padding-right: 25px;">""" + str(destination_after) + """</td></tr></tbody></table>"""

                update = []
                for col in updateColumns:
                    update += [{'Id':source_cim_id,col:source_after},{'Id':destination_cim_id,col:destination_after}]

                # Query to get affected positions in counter team instances
                query_affected_positions  = "select "+ns+"Position__c,"+ns+"Team_Instance__c from "+ns+"position_geography__c where "+ns+"Team_Instance__c in "+ str(tuple(counterTeamInstances)).replace(",)",")") + " and "+ns+"geography__r.Name in " + str(tuple(selectedZips)).replace(",)",")")
                result_affected_positions = pd.DataFrame(sf.query_all(query_affected_positions)['records'])
                del result_affected_positions['attributes']
                result_affected_positions = result_affected_positions.drop_duplicates().reset_index(drop=True)

                # Query to get zips of affected positions
                query_affected_positions_zips  = "select "+ns+"geography__r.Name,"+ns+"Team_Instance__c,"+ns+"Position__c from "+ns+"position_geography__c where "+ns+"Team_Instance__c in "+ str(tuple(result_affected_positions[ns+'Team_Instance__c'])).replace(",)",")") +" and "+ns+"Position__c in "+str(tuple(result_affected_positions[ns+'Position__c'])).replace(",)",")")
                result_affected_positions_zips = pd.DataFrame(sf.query_all(query_affected_positions_zips)['records'])
                del result_affected_positions_zips['attributes']
                result_affected_positions_zips[ns+'Geography__r'] = result_affected_positions_zips[ns+'Geography__r'].apply(lambda x: x['Name'])

                # Query to get zipter of base team instance
                query_base_zipter  = "select "+ns+"geography__r.Name,"+ns+"Position__c from "+ns+"position_geography__c where "+ns+"Team_Instance__c = '"+baseTeamInstance+"' and "+ns+"Position__c not in " + str(tuple(result_unassigned_zips['Id'])).replace(",)",")")
                result_base_zipter = pd.DataFrame(sf.query_all(query_base_zipter)['records'])
                result_base_zipter[ns+'Geography__r'] = result_base_zipter[ns+'Geography__r'].apply(lambda x: x['Name'])

                # getting face count of each affected position in counter team instances
                face_count = result_affected_positions_zips.groupby([ns+'Team_Instance__c',ns+'Position__c']).agg(lambda x: result_base_zipter.loc[result_base_zipter[ns+'Geography__r'].isin(x),ns+'Position__c'].nunique()).reset_index()
                face_count.rename(columns={ns+'Geography__r':'fcount',ns+'Position__c':ns+'Position_ID__c'},inplace=True)

                # Query to get customMetric Id of each counter team instances
                query_CustomMetric_Ids  = "select Id,"+ns+"Team_Instance__c from "+ns+"CIM_config__c where "+ns+"Is_Custom_Metric__c = true and "+ns+"Team_Instance__c in "+ str(tuple(counterTeamInstances)).replace(",)",")") +" and "+ns+"Enable__c = true and "+ns+"Change_Request_Type__r."+ns+"CR_Type_Name__c = '"+movementType+"'"
                result_CustomMetric_Ids = pd.DataFrame(sf.query_all(query_CustomMetric_Ids)['records'])
                del result_CustomMetric_Ids['attributes']

                # Query to get CIM_Position_Metric_summary__c Id of each affected counter position
                query_CIM_Position_Metric_summary_Id  = "select Id,"+ns+"Team_Instance__c, "+ns+"Position_Team_Instance__r."+ns+"Position_id__c from "+ns+"CIM_Position_Metric_summary__c where "+ns+"Cim_config__c in "+ str(tuple(result_CustomMetric_Ids['Id'])).replace(",)",")") +" and "+ns+"Position_Team_Instance__r."+ns+"Position_id__c in " + str(tuple(face_count[ns+'Position_ID__c'])).replace(",)",")")
                result_CIM_Position_Metric_summary_Id = pd.DataFrame(sf.query_all(query_CIM_Position_Metric_summary_Id)['records'])
                result_CIM_Position_Metric_summary_Id[ns+'Position_ID__c'] = result_CIM_Position_Metric_summary_Id[ns+'Position_Team_Instance__r'].apply(lambda x: x[ns+'Position_ID__c'])
                del result_CIM_Position_Metric_summary_Id['attributes'],result_CIM_Position_Metric_summary_Id[ns+'Position_Team_Instance__r']

                update_values = pd.merge(result_CIM_Position_Metric_summary_Id,face_count,on=[ns+'Team_Instance__c',ns+'Position_ID__c'])

                for col in updateColumns:
                    _ = update_values.apply(lambda x: update.append({'Id':x['Id'],col:x['fcount']}), axis=1)

                _update_response = eval("sf.bulk."+ns+"CIM_Position_Metric_summary__c.update(update)")

                return html.replace("Series([], )","0")
            except:
                return traceback.format_exc()


        if   (status == 'View'     and pageName == 'Alignment') or  (status   == 'Pending' and pageName == 'Request'): return generate_HTML_In_View()
        elif (status == 'Pending'  and pageName == 'Alignment'):                                                       return update_CIMSummary_For_Pending_Request()
        elif (status == 'Rejected' or  status   == 'Cancelled') and (pageName == 'Request'):                           return update_CIMSummary_For_Update_Request([ns+'Proposed__c'])
        elif (status == 'Approved' and pageName == 'Request'):                                                         return update_CIMSummary_For_Update_Request([ns+'Approved__c'])
        elif (status == 'Approved' and pageName == 'Alignment'):                                                       return update_CIMSummary_For_Update_Request([ns+'Approved__c',ns+'Proposed__c'])
        else: return "Invalid Request"
    except:
        return traceback.format_exc()

if __name__ == '__main__':
	app.run()