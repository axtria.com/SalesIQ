-- FUNCTION: public.fn_execute_scenario(text, text, boolean, boolean, text, text, text, text, text, text)

-- DROP FUNCTION public.fn_execute_scenario(text, text, boolean, boolean, text, text, text, text, text, text);

CREATE OR REPLACE FUNCTION public.fn_execute_scenario(
	p_t_scn_id text,
	p_t_rule_mode text,
	p_b_do_promote boolean,
	p_b_is_delta boolean,
	p_t_compare_dataset_id text,
	p_t_cust_univ_tbl_nm text,
	p_t_cust_univ_dataset_id text,
	p_t_upstream_tbl_nm text,
	p_t_upstream_dataset_id text,
	p_filter_condition text)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare

l_t_error 								text ; -- Error variable
l_t_fn_name 							text := 'fn_execute_scenario'; -- function name variable 
l_r_scenario_details 					RECORD ; -- scenario detail
l_r_table_status 						RECORD;
l_r_io_tables 							RECORD ;
l_t_input_table 						text ;
l_t_output_table 						text ;
l_t_rule_mode 							text := p_t_rule_mode ;
c_t_run_mode 							text := 'Run';
c_t_debug_mode 							text := 'Debug';
l_t_scn_id 								text := p_t_scn_id ;
c_i_error_flag 							integer := 0 ;
l_i_create_table_status 				integer := 0;
l_t_dynamic_query 						text ;
l_i_scn_detail_count 					integer ;
l_i_scn_ds_count 						integer ;
c_t_in_progress_status 					text := 'In Progress' ;
l_r_tables_details 						RECORD ;
l_b_is_internal 						boolean ;
l_t_dataset 							text ;
l_t_str_insert_data 					text ;
l_t_colm 								text ;
l_t_join_sel_expr 						text;
l_t_join_expr 							text;
l_t_sql_query			 				text := '';
/* new variable added for Issue Fix BRM-141 */
l_i_bsness_rul_status_count 			integer ;
l_i_bsness_rul_count 					integer ;
l_i_scn_ins_status_count 				integer ;
l_i_scn_ins_count 						integer ;
/* new variable added for call planing */
l_r_config_call_balancing_map 			RECORD ;
l_r_aggr_detail 						RECORD ;
l_t_config_call_balancing_detail_id 	text;
l_t_constraint_1_value 					text;
l_t_constraint_1_goal 					text;
l_t_constraint_2_value 					text;
l_t_constraint_2_goal 					text;
l_r_balancing_constraint_rule_map 		RECORD ;
l_r_business_rule_cl_balanc 			RECORD ;
l_t_debug_table_name 					text;
l_t_aggregate_level 					text;                  
l_t_agg_scn_rule_instance 				text ;    
l_t_cust_priority 						text ;
l_t_priority_order 						text ;  
l_t_br_type_master 						text ; 
c_t_inc_reach 							text := 'Increase Reach';
c_t_dec_frec 							text := 'Decrease Frequency';
c_t_dec_reach 							text := 'Decrease Reach';
c_t_inc_frec 							text := 'Increase Frequency';
l_t_select_exp 							text;
l_t_where_exp 							text;
l_t_grp_by_exp 							text;
l_t_key_col 							text;
l_r_final_call_plan_ds 					RECORD;
l_t_agg_level_count 					integer ;
l_t_agg_level_counter 					integer := 1;
l_i_call_bal_retrn_flg 					integer ;
c_t_join 								text := 'Join' ;
c_t_assignment 							text := 'Assignment Rules';
c_t_call_balancing 						text := 'Call Balancing';
c_t_affiliation 						text :='Affiliation';
c_t_merge 								text :='Merge';
--l_t_join_return_flg text;
l_i_return_flg							integer;
l_i_join_return_flg 					integer;
l_i_merge_return_flg 					integer;
l_t_scn_rule_instance 					RECORD;
flag 									integer;
l_t_data_filter 						text;
l_t_srid_stage 							text;
l_i_table_check1 						integer;
l_i_table_check2 						integer;
flag2 									integer; --used if customer metric file is 0 records
l_t_table_name 							text;  --used if customer metric file is 0 records
l_i_table_record_count 					integer;  --used if customer metric file is 0 records
l_i_metric_return_flag 					integer; --used if customer metric file is 0 records
l_t_sql_querry 							text; --used if customer metric file is 0 records
--scenario_rule_instance_details_id used for V4 template in order to extract customer metrics column names
l_t_srid 								text; 
l_t_ds 									text; --dataset_id used for V4 template in order to extract customer metrics column names
l_t_colm2 								text; --dataset columns used for V4 template in order to extract customer metrics column names
l_promote_parameters  					text;
i										integer :=0;
l_last_scn_id   						text;
l_t_affiliation_delta_output  			text;
l_t_min_exec_seq 						integer;
--Run Log
l_org_id 								text :='BRMS' ;
l_scenario_run_id 						text :=  p_t_scn_id ; 
l_log_type 								text;
l_log_level_basic 						text := 'Basic' ; 
l_log_level_detailed 					text := 'Detailed' ; 
l_run_status_in_progress 				text :='In Progress';
l_run_status_complete 					text :='Complete';
l_run_status_error 						text :='Error';
l_line_no 								text ; 
l_function_name 						text ;
l_log_type_info 						text :='Information';
l_log_type_warning 						text :='Warning';
l_log_type_error 						text :='Error';
l_input_count 							integer ;
l_output_count 							integer ;
l_t_update_cycle_input					integer ; 
BEGIN
		
		select fn_update_cycle()  into i;
		raise notice 'i -> % ', i;
		i=0;
		RAISE NOTICE 'p_t_scn_id -> %', p_t_scn_id;
		IF p_t_scn_id = 'a0tf4000003oV9KAAU' then 			
			RETURN 'Unsuccess';
		END IF;
		
		/* check if t_scn_rule_instance_details table not have any data for given scenario */
		flag := 0;
		flag2 :=1;
		SELECT COUNT(1) 
		  FROM t_scn_rule_instance_details 
		WHERE scenario_id = l_t_scn_id 
		  INTO l_i_scn_detail_count ;
		  
		l_promote_parameters := 'p_t_scn_id - '||p_t_scn_id || ' p_t_rule_mode - '||p_t_rule_mode ||
								' p_b_do_promote - '||p_b_do_promote ||	' p_b_is_delta -'||p_b_is_delta ||
								' p_t_compare_dataset_id - '||p_t_compare_dataset_id || 
								' p_t_cust_univ_tbl_nm -'||p_t_cust_univ_tbl_nm||
								' p_t_cust_univ_dataset_id - '||p_t_cust_univ_dataset_id||
								' p_t_upstream_tbl_nm - '||p_t_upstream_tbl_nm ||
								' p_t_upstream_dataset_id - '||p_t_upstream_dataset_id||
								' p_filter_condition - '||p_filter_condition ;
								
		INSERT INTO t_log(scenario_id, log, line_no, function_name)
			 VALUES (l_t_scn_id, 'Scenario Execution Start - Promote - '||l_promote_parameters, '154', l_t_fn_name);
		
		Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
						l_log_type_info  , l_log_level_basic , l_run_status_complete,
						'Scenario Started with parameters : - '||l_promote_parameters ,  
						159 , l_t_fn_name) ; 
		
		select fn_update_cycle_team_instance(l_t_scn_id) into l_t_update_cycle_input ; 
		
		---- IF Level 1
		IF (l_i_scn_detail_count = 0 or l_t_update_cycle_input =-1) THEN 
			perform fn_status_update_fail(l_t_scn_id);
			c_i_error_flag :=1 ;
			-- if for scenario does not exist
			if l_i_scn_detail_count = 0 then 
				RAISE NOTICE 'No scenario detail exists';
				  
				INSERT INTO t_log(scenario_id, log, line_no, function_name)
					 VALUES ( l_t_scn_id, 'No scenario detail exists. Count : '|| l_i_scn_detail_count, '169', l_t_fn_name);
				
				Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
							l_log_type_info  , l_log_level_basic , l_run_status_complete, 'No Rule Exists - Exiting' ,
							 173 , l_t_fn_name) ; 
			else 
				raise notice 'Update Cycle for Team Instance Failed';
				INSERT INTO t_log(scenario_id, log, line_no, function_name)
					 VALUES ( l_t_scn_id, 'Update Cycle for Team Instance Failed : '|| l_i_scn_detail_count, '169', l_t_fn_name);
				
				Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
							l_log_type_info  , l_log_level_basic , l_run_status_complete, 'No Rule Exists - Update Cycle for Team Instance Failed' ,
							 173 , l_t_fn_name) ; 
			end if;
			-- end if for scenario does not exist
		---- ELSE Level 1
		ELSE
			
			
			UPDATE t_dataset_rule_map drm SET table_name = LOWER(REPLACE(table_name,'-','_'))
			  FROM t_scn_rule_instance_details srd
			 WHERE drm.scenario_rule_instance_id = srd.sfdc_id 
			   AND srd.scenario_id = l_t_scn_id ;

		-- Create tables required for execution     
			RAISE NOTICE 'Create tables required for execution start';
			
			INSERT INTO t_log(scenario_id, log, line_no, function_name)
				 VALUES (l_t_scn_id, 'Create tables required for execution start', '186', l_t_fn_name);
				 
			Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
						l_log_type_info  , l_log_level_basic , l_run_status_complete, 
						'Create tables required for execution start' ,
						191 , l_t_fn_name) ; 

			FOR l_r_tables_details IN
				SELECT DISTINCT
				   drm.dataset_id,
				   drm.table_name 
				FROM
				   t_dataset ds,
				   t_dataset_rule_map drm,
				   t_scn_rule_instance_details srd 
				WHERE
				   ds.sfdc_id = drm.dataset_id 
				   AND drm.scenario_rule_instance_id = srd.sfdc_id 
				   AND ds.is_internal = true 
				   AND srd.scenario_id = l_t_scn_id

			LOOP 
				RAISE NOTICE 'Create tables';

				INSERT INTO t_log(scenario_id, log, line_no, function_name)
					 VALUES (l_t_scn_id,'Create tables t_scn_rule_instance_details - ' || l_r_tables_details.dataset_id, '205', l_t_fn_name);

				PERFORM fn_create_table(l_r_tables_details.dataset_id,l_r_tables_details.table_name, 'staging_tbl');
				
				Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
								l_log_type_info  , l_log_level_basic , l_run_status_complete, 
								'Create tables table name '||l_r_tables_details.table_name ||'  t_scn_rule_instance_details - ' || l_r_tables_details.dataset_id ,
								211 , l_t_fn_name) ;
				
				
			END LOOP; 
			
			
			RAISE NOTICE 'Create tables required for execution End';

			INSERT INTO t_log(scenario_id, log, line_no, function_name)
				 VALUES (l_t_scn_id,'Create tables required for execution End', '221', l_t_fn_name);
				 
			Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
						l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Create tables required for execution End' ,
						 224 , l_t_fn_name) ; 
		-- Code for customer metric	
		/*	SELECT  drm.table_name
			  FROM t_dataset ds, t_dataset_rule_map drm ,t_scn_rule_instance_details srd
			 WHERE ds.sfdc_id = drm.dataset_id 
			   AND drm.scenario_rule_instance_id = srd.sfdc_id 
			   AND ds.is_internal = false 
			   AND srd.scenario_id = l_t_scn_id
			   AND ds.salesiq_internal = false
			   INTO l_t_table_name;
			   

			Execute Format ('select count(*) from %I', l_t_table_name) into l_i_table_record_count;
		--SELECT COUNT(*) FROM ' || l_t_table_name  || ' INTO  l_i_table_record_count ;
				RAISE NOTICE '%',l_i_table_record_count;
		 
		--execute (l_t_sql_querry);
			 INSERT INTO t_log(scenario_id, log, line_no, function_name)
				 VALUES (l_t_scn_id,'Customer metric modificatiobn end', '169', l_i_table_record_count);
			IF (l_i_table_record_count = 0 ) THEN 
			select fn_external_table_fill(l_t_table_name, l_t_scn_id) into l_i_metric_return_flag;
			INSERT INTO t_log(scenario_id, log, line_no, function_name)
				 VALUES (l_t_scn_id,'Customer metric modificatiobn end', '172', l_i_metric_return_flag);
			
			IF (l_i_metric_return_flag = 1) THEN
			flag2 :=1 ;	
			ELSEIF (l_i_metric_return_flag = -1) THEN 
			flag2 :=0;
			END IF;
			INSERT INTO t_log(scenario_id, log, line_no, function_name)
				 VALUES (l_t_scn_id,'Customer metric modificatiobn end', '175', flag2);
			END IF;
				 
		-- Code end for customer metric	*/

			/* Traverse the business rules for given scenario */

			RAISE NOTICE 'Traverse the business rules for given scenario start';

			INSERT INTO t_log(scenario_id, log, line_no, function_name)
				 VALUES (l_t_scn_id,'Traverse the business rules for given scenario start', '132', l_t_fn_name);

		
			SELECT
			   COUNT(*) 
			FROM
			   information_schema.tables 
			where
			   table_name in 
			   (
				  SELECT
					 dsrm.table_name 
				  FROM
					 t_dataset_rule_map as dsrm,
					 t_dataset as ds,
					 t_scn_rule_instance_details as srid,
					 t_component_type_master as ctm 
				  where
					 dsrm.dataset_id = ds.sfdc_id 
					 and srid.component_type_master = ctm.sfdc_id 
					 and dsrm.scenario_rule_instance_id = srid.sfdc_id 
					 and srid.parent_display_order = '0' 
					 and srid.scenario_id = l_t_scn_id 
					 and ds.is_internal = false
			   )
			   into l_i_table_check1;
			 
			SELECT
			   count(distinct dsrm.dataset_id) 
			FROM
			   t_dataset_rule_map as dsrm,
			   t_dataset as ds,
			   t_scn_rule_instance_details as srid,
			   t_component_type_master as ctm 
			where
			   dsrm.dataset_id = ds.sfdc_id 
			   and srid.component_type_master = ctm.sfdc_id 
			   and dsrm.scenario_rule_instance_id = srid.sfdc_id 
			   and srid.parent_display_order = '0' 
			   and srid.scenario_id = l_t_scn_id 
			   and ds.is_internal = false into l_i_table_check2; 
			
			
			
			Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
						l_log_type_info  , l_log_level_basic , l_run_status_complete, 
						'Traverse the business rules for given scenario start - Tables in Scenario - '||l_i_table_check1||' , Datasets- '||l_i_table_check2  , 310 , l_t_fn_name) ; 
		   
			if(l_i_table_check1 != l_i_table_check2) then
			
					flag := 1;
					
					FOR l_r_table_status
					IN 
					SELECT DISTINCT
					   dsrm.dataset_id,
					   dsrm.scenario_rule_instance_id,
					   dsrm.table_name,
					   ds.is_internal,
					   srid.scenario_id,
					   srid.business_rule_template_id,
					   srid.component_type_label,
					   srid.parent_display_order,
					   ctm.name 
					FROM
					   t_dataset_rule_map as dsrm,
					   t_dataset as ds,
					   t_scn_rule_instance_details as srid,
					   t_component_type_master as ctm 
					WHERE
					   dsrm.dataset_id = ds.sfdc_id 
					   and srid.component_type_master = ctm.sfdc_id 
					   and dsrm.scenario_rule_instance_id = srid.sfdc_id 
					   and srid.parent_display_order = '0' 
					   and srid.scenario_id = l_t_scn_id 
					   and ds.is_internal = false
					
					LOOP
							
							l_t_sql_query := 'Select count(*) from information_schema.tables  where table_name =''' 
												|| l_r_table_status.table_name || '''';
						
							EXECUTE (l_t_sql_query) into l_i_table_check1;
							 
							IF(l_i_table_check1 = 0) THEN
							
								EXECUTE 'UPDATE t_scn_rule_instance_details  
												SET status = ''Error Occured''
												WHERE status = ''In Progress''
												AND sfdc_id   = ''' || l_r_table_status.scenario_rule_instance_id || '''' ;
								
								Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
									l_log_type_info  , l_log_level_basic , l_run_status_complete, 
									'Dataset id - '||l_i_table_check1||' does not have a table created '   ,
									360 , l_t_fn_name) ; 
											
							ELSE
							 
								EXECUTE 'UPDATE t_scn_rule_instance_details  
												SET status = ''Success'' 
											  WHERE status = ''In Progress''
												AND sfdc_id   = ''' || l_r_table_status.scenario_rule_instance_id || '''' ;
							end if;
									
					END LOOP;
							
				
			end if;
			--Code for Component status update during run end- by Anuj    
			 
			RAISE NOTICE 'Component Status update code end';
			INSERT INTO t_log(scenario_id, log, line_no, function_name)
				 VALUES (l_t_scn_id,'Component Status update code end', '277', l_t_fn_name);
				 
			-- bug fixing for Delta Execution 
			-- find the minimum exec sequence id for the first input 
			SELECT
			   exec_seq::integer 
			FROM
			   (
				  SELECT
					 min(srid.exec_seq) exec_seq 
				  FROM
					 t_scn_rule_instance_details as srid,
					 t_component_type_master as ctm 
				  WHERE
					 srid.scenario_id = l_t_scn_id 
					 AND srid.component_type_master = ctm.sfdc_id 
			   ) aa 
			INTO l_t_min_exec_seq ;
			 
			INSERT INTO t_log(scenario_id, log, line_no, function_name)
				 VALUES (l_t_scn_id,'Exec Seq of the First Component - '|| l_t_min_exec_seq, '311', l_t_fn_name);
				 
			Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
							l_log_type_info  , l_log_level_basic , l_run_status_complete, 
							'Scenario Rule Execution Start'   ,
							407 , l_t_fn_name) ; 	
			 
			 FOR l_r_scenario_details IN
				SELECT distinct
				   srid.sfdc_id AS scn_rule_instance_details_id,
				   srid.status,
				   ctm.name AS component_type_label,
				   srid.exec_seq,
				   srid.component_type_label AS srid_stage 
				FROM
				   t_scn_rule_instance_details as srid,
				   t_component_type_master as ctm 
				WHERE
				   srid.scenario_id = l_t_scn_id 
				   AND srid.component_type_master = ctm.sfdc_id 
				ORDER BY
				   srid.exec_seq ASC

			LOOP
					l_t_srid_stage = l_r_scenario_details.srid_stage;
					---- IF Level 2
					

					INSERT INTO t_log(scenario_id, log, line_no, function_name)
						 VALUES (l_t_scn_id,'Business Rules Start for Component - '|| l_r_scenario_details.component_type_label||' Sfdc_id -' || l_t_srid_stage, '375', flag);
						 
					Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
									l_log_type_info  , l_log_level_basic , l_run_status_complete,
									'Business Rules Start for Component - '|| l_r_scenario_details.component_type_label||' Sfdc_id -' || l_t_srid_stage   ,
									377 , l_t_fn_name) ; 
					IF(flag = 0 AND flag2 = 1 ) then
					
					IF (l_r_scenario_details.status = c_t_in_progress_status) THEN
					
						/* check if t_dataset_rule_map table not have any data for scn_rule_instance_details_id */
					
						Raise Notice 'check if t_dataset_rule_map table not have any data for scn_rule_instance_details_id';
						SELECT
						   COUNT(1) 
						FROM
						   t_dataset_rule_map 
						WHERE
						   scenario_rule_instance_id = l_r_scenario_details.scn_rule_instance_details_id 
						INTO l_i_scn_ds_count;
						
						---- IF Level 3
						IF (l_i_scn_ds_count = 0 ) THEN 

							RAISE NOTICE '%',l_i_scn_ds_count;
							RAISE NOTICE 'No dataset exist';

							INSERT INTO t_log(scenario_id, log, line_no, function_name)
								 VALUES (l_t_scn_id,'No dataset exist', '312', l_t_fn_name);
							
							Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 'No dataset exist '   ,
										 463, l_t_fn_name) ; 

						 
						---- ELSE Level 3
						ELSE    

							/*  input and output tables for business rules */

							FOR l_r_io_tables IN 
								SELECT DISTINCT
								   drm.dataset_id,
								   drm.ds_type,
								   drm.table_name,
								   ds.is_internal 
								FROM
								   t_dataset ds,
								   t_dataset_rule_map drm 
								WHERE
								   ds.sfdc_id = drm.dataset_id 
								   AND drm.scenario_rule_instance_id = l_r_scenario_details.scn_rule_instance_details_id

							LOOP

								INSERT INTO t_log(scenario_id, log, line_no, function_name)
									 VALUES (l_t_scn_id,'input and output tables for business rules - '||l_r_io_tables.table_name, '487', l_t_fn_name);
								
								Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 
										'Business Rule - '|| l_r_scenario_details.scn_rule_instance_details_id || ' & input and output tables for business rules - '||l_r_io_tables.table_name   ,
										 491, l_t_fn_name) ;
								---- IF Level 4
								IF (l_r_io_tables.ds_type = 'I') THEN
									l_t_input_table := l_r_io_tables.table_name ;
									l_b_is_internal := l_r_io_tables.is_internal ;
									l_t_dataset := l_r_io_tables.dataset_id ;

									RAISE NOTICE '%', l_t_dataset;
									--perform fn_create_table(l_r_io_tables.dataset_id,l_t_input_table,'staging_tbl');
								
								---- ELSEIF Level 4
								ELSEIF (l_r_io_tables.ds_type = 'O') THEN 
									l_t_output_table:= l_r_io_tables.table_name ; 
									--perform fn_create_table(l_r_io_tables.dataset_id,l_t_output_table,'staging_tbl');
								
								---- END IF Level 4
								END IF; -- l_r_io_tables.ds_type

							END LOOP ;
							
							RAISE NOTICE '%',l_t_str_insert_data;
							
							---- IF Level 5
							
							
							IF (l_b_is_internal = false ) THEN 
								/*RAISE NOTICE '%',l_b_is_internal;
								select scenario_rule_instance_id
								from t_dataset_rule_map drm
								where dataset_id= l_t_dataset
								AND drm.scenario_rule_instance_id = l_r_scenario_details.scn_rule_instance_details_id 
								into l_t_srid;*/
								
								select dataset_id 
								  from  t_dataset_rule_map 
								where  scenario_rule_instance_id = l_r_scenario_details.scn_rule_instance_details_id 
								  and ds_type = 'O' into l_t_ds;
								
								SELECT string_agg(tb_col_nm, ',' order by column_order) 
								  FROM t_ds_col_detail 
								 WHERE dataset_id =  l_t_ds 
								 INTO l_t_colm;
								  
								select dataset_id 
								from t_dataset_rule_map
								where scenario_rule_instance_id= l_r_scenario_details.scn_rule_instance_details_id 
								and ds_type='I'
								into l_t_ds;
								
								SELECT string_agg(tb_col_nm, ',' order by column_order) 
								  FROM t_ds_col_detail 
								 WHERE dataset_id =  l_t_ds 
								  INTO l_t_colm2;
								  
								
								/* Filter condition added to pull external data according to filter*/
								
								select
								   tsd.filter_expression_destn 
								from
								   t_scn_data_object_map tsd,
								   t_dataset ds 
								where
								   tsd.data_set = ds.base_data_set 
								   and ds.sfdc_id = l_t_dataset 
								   and scenario = l_t_scn_id into l_t_data_filter;
								
								if l_t_data_filter is null then
									l_t_data_filter = ' 1=1';
								end if;

								l_t_str_insert_data := 'Truncate Table ' || l_t_output_table || ';' ||
														'insert into '|| l_t_output_table || '(' ||  l_t_colm || ') select  ' || l_t_colm2 || 
														' from ' || l_t_input_table || ' where ' || l_t_data_filter;
								
								INSERT INTO t_log(scenario_id, log, line_no, function_name)
									 VALUES (l_t_scn_id,l_t_str_insert_data, 'copy', l_t_fn_name);
									
								Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
											l_log_type_info  , l_log_level_basic , l_run_status_complete, 
											'Datasource Details : - scenario_rule_instance_id ->'||l_r_scenario_details.scn_rule_instance_details_id ||' Dataset ID -> '||l_t_dataset ||' l_t_data_filter -> '||l_t_data_filter    ,
											572 , l_t_fn_name) ; 
											
								Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
											l_log_type_info  , l_log_level_basic , l_run_status_complete, 
											'Datasource Details : - scenario_rule_instance_id ->'||l_r_scenario_details.scn_rule_instance_details_id ||' Dataset ID -> '||l_t_dataset ||' Query -> '||l_t_str_insert_data  ,
											577 , l_t_fn_name) ;
											
								RAISE NOTICE '%',l_t_colm;
								RAISE NOTICE '%',l_t_str_insert_data;
								EXECUTE(l_t_str_insert_data);

								INSERT INTO t_log(scenario_id, log, line_no, function_name)
									 VALUES (l_t_scn_id,'data inserted from customer universe table to output table- '|| l_t_output_table, '412', l_t_fn_name);
								
								-- Start Bug fix for qualifing the affiliate's hierarchy in delta
								if p_b_is_delta = 'True' and l_r_scenario_details.exec_seq::integer =l_t_min_exec_seq  then
										
										INSERT INTO t_log(scenario_id, log, line_no, function_name)
										 VALUES (l_t_scn_id,'Customer Master - affiliation delta function start- '|| l_t_output_table, 
												 '441', l_t_fn_name);	
										Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
											l_log_type_info  , l_log_level_basic , l_run_status_complete, 
											'Affiliation Delta Function Execition Begins ' ,
											596 , l_t_fn_name) ;
											
										select fn_affiliation_delta(l_t_scn_id ,l_r_scenario_details.scn_rule_instance_details_id    ) into l_t_affiliation_delta_output;
										
										INSERT INTO t_log(scenario_id, log, line_no, function_name)
										 VALUES (l_t_scn_id,'Customer Master - affiliation delta function end- '|| l_t_output_table, 
												 '441', l_t_fn_name);
										Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
											l_log_type_info  , l_log_level_basic , l_run_status_complete, 
											'Affiliation Delta Function Execition Ends ' ,
											606 , l_t_fn_name) ;
								end if;
								-- Start Bug fix for qualifing the affiliate's hierarchy in delta
							---- END IF Level 5
							END IF; -- l_b_is_internal

						---- END IF Level 3
						END IF; -- l_i_scn_ds_count
						RAISE NOTICE 'Rule Execution Starts';
						INSERT INTO t_log(scenario_id, log, line_no, function_name)
							values ( l_t_scn_id, 'Rule Execution Starts' , 421, l_t_fn_name);
							
						Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
											l_log_type_info  , l_log_level_basic , l_run_status_complete, 
											'SCN Instance ID -> '||l_r_scenario_details.scn_rule_instance_details_id || ' Rule Execution Starts ' ,
											621 , l_t_fn_name) ;
											
						/* Call balancing */
						---- IF Level 6
						IF (l_r_scenario_details.component_type_label = 'Call Balancing') THEN
						
							INSERT INTO t_log(scenario_id, log, line_no, function_name)
								 VALUES ( l_t_scn_id, 'Run call balancing start with scn_rule_instance_id : '|| 
														l_r_scenario_details.scn_rule_instance_details_id || ',' || l_t_input_table || ',' ||
														l_t_output_table || ',' || l_t_rule_mode || ',' || c_t_run_mode || ',' || c_t_debug_mode ||
														',' || l_t_scn_id || ',' || c_i_error_flag, '631', l_t_fn_name);

							Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 
										'Run call balancing start with scn_rule_instance_id : '|| 
										l_r_scenario_details.scn_rule_instance_details_id || ',' || l_t_input_table || ',' ||
										l_t_output_table || ',' || l_t_rule_mode || ',' || c_t_run_mode || ',' || c_t_debug_mode ||
										',' || l_t_scn_id || ',' || c_i_error_flag   ,
										 639, l_t_fn_name) ;
					
							/*
							SELECT fn_execute_call_balancing(l_r_scenario_details.scn_rule_instance_details_id, l_t_input_table, l_t_output_table, 
															l_t_rule_mode, c_t_run_mode, c_t_debug_mode, l_t_scn_id,c_i_error_flag ) 
							  INTO l_i_call_bal_retrn_flg;
							*/
							SELECT fn_call_balancing(l_r_scenario_details.scn_rule_instance_details_id, l_t_input_table, l_t_output_table, 
													 l_t_rule_mode, c_t_run_mode, c_t_debug_mode, l_t_scn_id, c_i_error_flag) 
							  INTO l_i_call_bal_retrn_flg;

							SELECT fn_call_balancing(l_r_scenario_details.scn_rule_instance_details_id, l_t_input_table, l_t_output_table, 
													 l_t_rule_mode, c_t_run_mode, c_t_debug_mode, l_t_scn_id, c_i_error_flag) 
							  INTO l_i_call_bal_retrn_flg;

							INSERT INTO t_log(scenario_id, log, line_no, function_name)
								 VALUES (l_t_scn_id, 'Run call balancing end with scn_rule_instance_id : ' || 
													 l_r_scenario_details.scn_rule_instance_details_id, '233', l_t_fn_name);
							
							
							l_t_str_insert_data := 'select count (0) from '||l_t_input_table ;
									execute l_t_str_insert_data  into l_input_count ;
									
									l_t_str_insert_data := 'select count (0) from '||l_t_output_table ;
									execute l_t_str_insert_data  into l_output_count ;
							---- IF Level 7
							IF (l_i_call_bal_retrn_flg = 1) THEN

								EXECUTE 'UPDATE t_scn_rule_instance_details  
											SET status = ''Success'' 
										  WHERE status = ''In Progress'' 
											AND sfdc_id   = ''' || l_r_scenario_details.scn_rule_instance_details_id || '''' ;
						
								Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Call Balancing Executed Successfully for SCN_ID - '||l_r_scenario_details.scn_rule_instance_details_id ||  ' & Input Count - '|| l_input_count || ' &  output count - '||l_output_count ,
										 555, l_t_fn_name) ;

							---- ELSEIF Level 7
							ELSEIF (l_i_call_bal_retrn_flg = -1) THEN

								EXECUTE 'UPDATE t_scn_rule_instance_details 
											SET status = ''Error Occured'' 
										  WHERE  status = ''In Progress'' 
											AND sfdc_id   = ''' || l_r_scenario_details.scn_rule_instance_details_id || '''' ;
								Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
											l_log_type_info  , l_log_level_basic , l_run_status_complete, 
											'Error-  call balancing failed with scn_rule_instance_id : '|| 
											l_r_scenario_details.scn_rule_instance_details_id || ',' || l_t_input_table || ',' ||
											l_t_output_table || ',' || l_t_rule_mode || ',' || c_t_run_mode || ',' || c_t_debug_mode ||
											',' || l_t_scn_id || ',' || c_i_error_flag   ,
											 391, l_t_fn_name) ; 
								flag := 1;
							---- END IF Level 7
							END IF;
						
						/* Join Rule */
						---- ELSEIF Level 6
						ELSEIF (l_r_scenario_details.component_type_label = c_t_join) THEN
							
							INSERT INTO t_log(scenario_id, log, line_no, function_name)
							values ( l_t_scn_id,'Fetch Join Rule - '||l_r_scenario_details.scn_rule_instance_details_id ,699, l_t_fn_name);
							
							Perform fn_log ( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										     l_log_type_info  , l_log_level_basic , l_run_status_complete, 
										     'Fetch Join Rule - SCN Instance ID - '||l_r_scenario_details.scn_rule_instance_details_id 
											 ||' output table name - '||l_t_output_table,
										     705, l_t_fn_name) ; 
										 
							SELECT fn_join_rule (l_r_scenario_details.scn_rule_instance_details_id, l_t_output_table) 
							  INTO l_i_join_return_flg;

							l_t_str_insert_data := 'select count (0) from '||l_t_input_table ;
							EXECUTE l_t_str_insert_data  into l_input_count ;
									
							l_t_str_insert_data := 'select count (0) from '||l_t_output_table ;
							EXECUTE l_t_str_insert_data  into l_output_count ;
									
							---- IF Level 8
							IF (l_i_join_return_flg = 1) THEN
								EXECUTE 'UPDATE t_scn_rule_instance_details 
											SET status = ''Success'' 
										  WHERE status = ''In Progress'' 
											AND sfdc_id   = ''' || l_r_scenario_details.scn_rule_instance_details_id || '''' ;
								Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Join Rule Executed Succesfully for SCN_ID - '||l_r_scenario_details.scn_rule_instance_details_id ||  ' &  Input Count - '|| l_input_count || '  & output count - '||l_output_count ,
										 724, l_t_fn_name) ;
							---- ELSE IF Level 8
							ELSEIF (l_i_join_return_flg = -1) THEN
								EXECUTE 'UPDATE t_scn_rule_instance_details 
											SET status = ''Error Occured'' 
										  WHERE status = ''In Progress'' 
											AND sfdc_id  = ''' || l_r_scenario_details.scn_rule_instance_details_id || '''' ;
								Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Error - Join Rule Failed for SCN_ID - '||l_r_scenario_details.scn_rule_instance_details_id ||  ' & Input Count - '|| l_input_count || '  & output count - '||l_output_count ,
										 555, l_t_fn_name) ;
								flag := 1;
							---- END IF Level 8
							END IF;

							/*select select_execute_expression, join_execute_expression
								from t_br_joinrule where scenario = l_t_scn_id
								into l_t_join_sel_expr, l_t_join_expr ;

							-- form dynamic query
							l_t_sql_query = l_t_sql_query || 'insert into ' || l_t_output_table || '( ' || l_t_join_sel_expr
											|| ' ) select '|| l_t_join_sel_expr || ' from (' || l_t_join_expr || ')' ;
							  INSERT INTO t_log(
							  scenario_id, log, line_no, function_name)
							  VALUES ( l_t_scn_id, 'query : '|| l_t_sql_query, '269', l_t_fn_name);
									  

							execute(l_t_sql_query) ;*/
							
							/* Assignment Rule */
						---- ELSEIF Level 6
						ELSEIF (l_r_scenario_details.component_type_label = c_t_assignment ) THEN
							
							INSERT INTO t_log(scenario_id, log, line_no, function_name)
							values ( l_t_scn_id,'Fetch Assignment Rule - '||l_r_scenario_details.scn_rule_instance_details_id ,512, l_t_fn_name);
							
							Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 
										  'Fetch Assignment Rule - '||l_r_scenario_details.scn_rule_instance_details_id ||' output table name - '||l_t_output_table,
										 663, l_t_fn_name) ; 
							SELECT fn_assignment_rule (l_r_scenario_details.scn_rule_instance_details_id, l_t_output_table) INTO l_i_return_flg;

							l_t_str_insert_data := 'select count (0) from '||l_t_input_table ;
									execute l_t_str_insert_data  into l_input_count ;
									
									l_t_str_insert_data := 'select count (0) from '||l_t_output_table ;
									execute l_t_str_insert_data  into l_output_count ;
									
							---- IF Level 8
							IF (l_i_return_flg = 1) THEN
								EXECUTE 'UPDATE t_scn_rule_instance_details 
											SET status = ''Success'' 
										  WHERE status = ''In Progress'' 
											AND sfdc_id   = ''' || l_r_scenario_details.scn_rule_instance_details_id || '''' ;
							Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Assignment Rule Executed Successfully for SCN_ID - '||l_r_scenario_details.scn_rule_instance_details_id ||  ' & Input Count - '|| l_input_count || ' & output count - '||l_output_count ,
										 555, l_t_fn_name) ;
							---- ELSE IF Level 8
							ELSEIF (l_i_return_flg = -1) THEN
								EXECUTE 'UPDATE t_scn_rule_instance_details 
											SET status = ''Error Occured'' 
										  WHERE status = ''In Progress'' 
											AND sfdc_id  = ''' || l_r_scenario_details.scn_rule_instance_details_id || '''' ;
							Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Error - Assignment Rule Failed for SCN_ID - '||l_r_scenario_details.scn_rule_instance_details_id ||  ' & Input Count - '|| l_input_count || ' & output count - '||l_output_count ,
										 555, l_t_fn_name) ;
								flag := 1;
							---- END IF Level 8
							END IF;
							
							
						ELSEIF (l_r_scenario_details.component_type_label = c_t_affiliation ) THEN
							INSERT INTO t_log(scenario_id, log, line_no, function_name)
							values ( l_t_scn_id,'Fetch affiliation Rule - '||l_r_scenario_details.scn_rule_instance_details_id ,512, l_t_fn_name);
							
							Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 
										  'Fetch affiliation Rule - '||l_r_scenario_details.scn_rule_instance_details_id ||' output table name - '||l_t_output_table,
										 708, l_t_fn_name) ;
							-- For 7.0 testing
							SELECT fn_affiliation_rule(l_r_scenario_details.scn_rule_instance_details_id, l_t_output_table) INTO l_i_return_flg;
							--SELECT fn_affiliation_rule_new7 (l_r_scenario_details.scn_rule_instance_details_id, l_t_output_table) INTO l_i_return_flg;
							-- select 1 into l_i_return_flg ;
								l_t_str_insert_data := 'select count (0) from '||l_t_input_table ;
									execute l_t_str_insert_data  into l_input_count ;
									
									l_t_str_insert_data := 'select count (0) from '||l_t_output_table ;
									execute l_t_str_insert_data  into l_output_count ;
									
							---- IF Level 8
							IF (l_i_return_flg = 1) THEN
								EXECUTE 'UPDATE t_scn_rule_instance_details 
											SET status = ''Success'' 
										  WHERE status = ''In Progress'' 
											AND sfdc_id   = ''' || l_r_scenario_details.scn_rule_instance_details_id || '''' ;
							Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Affiliation Rule Executed Successfully for SCN_ID - '||l_r_scenario_details.scn_rule_instance_details_id ||  ' & Input Count - '|| l_input_count || ' & output count - '||l_output_count ,
										 555, l_t_fn_name) ;
							---- ELSE IF Level 8
							ELSEIF (l_i_return_flg = -1) THEN
								EXECUTE 'UPDATE t_scn_rule_instance_details 
											SET status = ''Error Occured'' 
										  WHERE status = ''In Progress'' 
											AND sfdc_id  = ''' || l_r_scenario_details.scn_rule_instance_details_id || '''' ;
							Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Error - Affiliation Rule Failed for SCN_ID - '||l_r_scenario_details.scn_rule_instance_details_id ||  ' & Input Count - '|| l_input_count || ' & output count - '||l_output_count ,
										 555, l_t_fn_name) ;
								flag := 1;
							---- END IF Level 8
							END IF;
						---- ELSE Level 6
						/* Merge Rule */
						---- ELSEIF Level 10
						ELSEIF (l_r_scenario_details.component_type_label = c_t_merge) THEN
							INSERT INTO t_log(scenario_id, log, line_no, function_name)
							values ( l_t_scn_id,'Fetch Merge Rule - '||l_r_scenario_details.scn_rule_instance_details_id ,512, l_t_fn_name);
							
							Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 
										  'Fetch Merge Rule - '||l_r_scenario_details.scn_rule_instance_details_id ||' output table name - '||l_t_output_table,
										 708, l_t_fn_name) ; 
							SELECT fn_merge_rule(l_r_scenario_details.scn_rule_instance_details_id, l_t_output_table) INTO l_i_return_flg;

							l_t_str_insert_data := 'select count (0) from '||l_t_input_table ;
									execute l_t_str_insert_data  into l_input_count ;
									
									l_t_str_insert_data := 'select count (0) from '||l_t_output_table ;
									execute l_t_str_insert_data  into l_output_count ;
								
							---- IF Level 8
							IF (l_i_return_flg = 1) THEN
								EXECUTE 'UPDATE t_scn_rule_instance_details 
											SET status = ''Success'' 
										  WHERE status = ''In Progress'' 
											AND sfdc_id   = ''' || l_r_scenario_details.scn_rule_instance_details_id || '''' ;
							Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Merge Rule Executed Successfully for SCN_ID - '||l_r_scenario_details.scn_rule_instance_details_id ||  ' & Input Count - '|| l_input_count || ' & output count - '||l_output_count ,
										 555, l_t_fn_name) ;
							---- ELSE IF Level 8
							ELSEIF (l_i_return_flg = -1) THEN
								EXECUTE 'UPDATE t_scn_rule_instance_details 
											SET status = ''Error Occured'' 
										  WHERE status = ''In Progress'' 
											AND sfdc_id  = ''' || l_r_scenario_details.scn_rule_instance_details_id || '''' ;
							Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Error - Merge Rule failed for SCN_ID - '||l_r_scenario_details.scn_rule_instance_details_id ||  ' & Input Count - '|| l_input_count || '  & output count - '||l_output_count ,
										 555, l_t_fn_name) ;
								flag := 1;
							---- END IF Level 8
							END IF;

							
						---- ELSE Level 10
						ELSE
					
							/* commented for BRM - 141 on 10/31/2017 */
							-- if (c_i_error_flag = 0) then 
							/* Execute business rule function  */

							INSERT INTO t_log(scenario_id, log, line_no, function_name)
								 VALUES (l_t_scn_id, 'Business Rule scn rule inst det : ' || l_r_scenario_details.scn_rule_instance_details_id, '233', 
										l_t_fn_name);
						
						Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 
										  'Business Rule scn rule inst det : ' || l_r_scenario_details.scn_rule_instance_details_id ||' output table name - '||l_t_output_table,
										 708, l_t_fn_name) ; 
							PERFORM fn_execute_business_rule(l_r_scenario_details.scn_rule_instance_details_id, l_t_input_table,l_t_output_table, 
															l_t_rule_mode,c_t_run_mode, c_t_debug_mode, l_t_scn_id, c_i_error_flag);

							INSERT INTO t_log(scenario_id, log, line_no, function_name)
								 VALUES (l_t_scn_id, 'business rule function end', '199', l_t_fn_name);

							l_t_str_insert_data := 'select count (0) from '||l_t_input_table ;
									execute l_t_str_insert_data  into l_input_count ;
									
									l_t_str_insert_data := 'select count (0) from '||l_t_output_table ;
									execute l_t_str_insert_data  into l_output_count ;
									
							/* Issue Fix BRM-141 */
							SELECT COUNT(1)  
							  FROM t_business_rule 
							 WHERE scenario_rule_instance_details = l_r_scenario_details.scn_rule_instance_details_id 
							   AND status = 'Success' 
							  INTO l_i_bsness_rul_status_count;  

							SELECT COUNT(1)  
							  FROM t_business_rule 
							 WHERE scenario_rule_instance_details = l_r_scenario_details.scn_rule_instance_details_id
							  INTO l_i_bsness_rul_count ;
							
							---- IF Level 9
							IF (l_i_bsness_rul_status_count = l_i_bsness_rul_count ) THEN
								EXECUTE 'UPDATE t_scn_rule_instance_details 
											SET status = ''Success'' 
										  WHERE status = ''In Progress'' 
											AND sfdc_id = ''' || l_r_scenario_details.scn_rule_instance_details_id || '''' ;
							Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Business Rule executed successfully for SCN_ID - '||l_r_scenario_details.scn_rule_instance_details_id ||  ' & Input Count - '|| l_input_count || ' & output count - '||l_output_count ,
										 555, l_t_fn_name) ;
							---- ELSE Level 9
							ELSE
								EXECUTE 'UPDATE t_scn_rule_instance_details 
											SET status = ''Error Occured'' 
										  WHERE status = ''In Progress'' 
											AND sfdc_id = ''' || l_r_scenario_details.scn_rule_instance_details_id || '''' ;
							Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
										l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Error - Business Rule failed for SCN_ID - '||l_r_scenario_details.scn_rule_instance_details_id ||  '& Input Count - '|| l_input_count || ' & output count - '||l_output_count ,
										 555, l_t_fn_name) ;
								flag := 1;
							---- END IF Level 9
							END IF;
							/* commented for BRM - 141 on 10/31/2017 */
							-- END if ; -- c_i_error_flag
					   
					   ---- END IF Level 6
						END IF; --   component_type_label    
					
					---- ELSE Level 2
					ELSE
						c_i_error_flag := 1;
						PERFORM fn_status_update_fail(l_t_scn_id);
						
					---- END IF Level 2
					END if; -- l_r_scenario_details.status
					
					ELSE
					EXECUTE 'UPDATE t_scn_rule_instance_details 
											SET status = ''Pending'' 
										  WHERE status = ''In Progress'' 
											AND sfdc_id   = ''' || l_r_scenario_details.scn_rule_instance_details_id || '''' ;
					END IF; -- error flag
					l_last_scn_id :=l_r_scenario_details.scn_rule_instance_details_id ;
			
			END LOOP;
			
			Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
						l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Shared Flag Update Start'   ,
						 891 , l_t_fn_name) ; 

		---- END IF Level 1
		END IF; -- l_i_scn_detail_count 

		if l_i_scn_detail_count <> 0 then  -- SHARED WITH IF
		--- UPDATE SHARED FLAG FOR LAST COMPONENT -- along with de-duping of rule_id and rule_name for the final output
				SELECT fn_update_shared_flag_final(l_t_scn_id)  INTO l_i_return_flg;
				
				IF (l_i_return_flg = 1) THEN
								EXECUTE 'UPDATE t_scenario 
										SET request_process_stage = ''Success'' 
									  WHERE request_process_stage = ''In Progress'' 
										AND sfdc_id = ''' || l_last_scn_id || '''' ;
							---- ELSE IF Level 8
				ELSEIF (l_i_return_flg = -1) THEN
								EXECUTE 'UPDATE t_scenario 
											SET request_process_stage = ''Error Occured'' 
										  WHERE request_process_stage = ''In Progress'' 
											AND sfdc_id = ''' || l_last_scn_id || '''' ;
								flag := 1;
							---- END IF Level 8
				END IF;
				--- UPDATE SHARED FLAG FOR LAST COMPONENT
		End if;	-- SHARED WITH IF

		/* Issue Fix BRM-141 */
		SELECT COUNT(1) 
		  FROM t_scn_rule_instance_details 
		 WHERE scenario_id = l_t_scn_id 
		   AND status = 'Success'  
		  INTO l_i_scn_ins_status_count ;
			
		SELECT COUNT(1) 
		  FROM t_scn_rule_instance_details 
		 WHERE scenario_id = l_t_scn_id
		  INTO l_i_scn_ins_count ;
		/* commented for BRM - 141 on 10/31/2017 */
		-- if (c_i_error_flag = 0)then
		IF (l_i_scn_ins_status_count = l_i_scn_ins_count) THEN
			EXECUTE 'UPDATE t_scenario 
						SET request_process_stage = ''Success'' 
					  WHERE request_process_stage = ''In Progress'' 
						AND sfdc_id = ''' || l_t_scn_id || '''' ;
		ELSE
			EXECUTE 'UPDATE t_scenario 
						SET request_process_stage = ''Error Occured'' 
					  WHERE request_process_stage = ''In Progress'' 
						AND sfdc_id = ''' || l_t_scn_id || '''' ;
		END if;
			/* commented for BRM - 141 on 10/31/2017 */
			-- END if ;

		if l_i_scn_detail_count <> 0 then  -- execute promote only if the scenario exist  
		  if p_b_do_promote = true then
						  /*select fn_promote_scenario_pr (p_t_scn_id ,p_b_do_promote,  p_b_is_delta , p_t_compare_dataset_id ,
													 p_t_cust_univ_tbl_nm, p_t_cust_univ_dataset_id , p_t_upstream_tbl_nm,
													 p_t_upstream_dataset_id	, p_filter_condition) into i ; 	*/
						  select fn_promote_scenario (p_t_scn_id ,p_b_do_promote,  p_b_is_delta , p_t_compare_dataset_id ,
													 p_t_cust_univ_tbl_nm, p_t_cust_univ_dataset_id , p_t_upstream_tbl_nm,
													 p_t_upstream_dataset_id	, p_filter_condition) into i ; 	
			end if;
			if i = -2 then
					execute 'UPDATE t_scenario 
						SET request_process_stage = ''Error Occured'' 
					  WHERE request_process_stage = ''In Progress'' 
						AND sfdc_id = ''' || l_t_scn_id || '''' ;
					return '-2';
			end if;

		end if;	-- execute promote only if the scenario exist
			/* Issue Fix BRM-141 */
		  INSERT INTO t_log(scenario_id, log, line_no, function_name)
						 VALUES (l_t_scn_id, 'Scenario Execution Ended', '669', l_t_fn_name);
		  Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
						l_log_type_info  , l_log_level_basic , l_run_status_complete, 'Scenario Executed Successfully'   ,
						 944 , l_t_fn_name) ;
		RETURN 'Success';

/* Error handling */
EXCEPTION 
	WHEN OTHERS THEN
        GET STACKED DIAGNOSTICS l_t_error = PG_EXCEPTION_CONTEXT;
        INSERT INTO t_error_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,srid_stage)
        	 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,'','',l_t_scn_id,l_t_str_insert_data,l_t_colm,l_t_ds);
        INSERT INTO t_error_run_log (proc_name,error_name,error_state,table_name,rule_type,scenario_id,error_detail,dynamic_query,srid_stage)
        	 VALUES (l_t_fn_name,SQLERRM,SQLSTATE,'','',l_t_scn_id,l_t_error,l_t_str_insert_data,l_t_srid_stage);
        PERFORM fn_status_update_fail(l_t_scn_id);	
        -- c_i_error_flag := 1; /* commented for BRM - 141 on 10/31/2017 */
          
		Perform fn_log( l_org_id , 	p_t_scn_id , l_scenario_run_id , 
							l_log_type_error  , l_log_level_basic , l_run_status_error, 'Error Scenario Execution failed - '||SQLERRM || ' - '||SQLSTATE || ' Query -> '||l_t_str_insert_data   ||'  ',
							1053 , l_t_fn_name) ;
        RETURN 'Unsuccess';

END;

$BODY$;

ALTER FUNCTION public.fn_execute_scenario(text, text, boolean, boolean, text, text, text, text, text, text)
    OWNER TO postgres;
