-- FUNCTION: public.fn_update_cycle_team_instance(text)

-- DROP FUNCTION public.fn_update_cycle_team_instance(text);

CREATE OR REPLACE FUNCTION public.fn_update_cycle_team_instance(
	p_t_scenario_id text)
    RETURNS integer
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

DECLARE
	l_t_scenario_id 										text:= p_t_scenario_id;
	l_t_sql 												text;
	l_t_position_account_table_name 						text;
	l_t_position_account_data_set_id 						text;
	l_t_position_account_alignment_period_column 			text;
	l_t_position_account_assignment_status_column 			text;
	l_t_position_geography_table_name 						text;
	l_t_position_geography_data_set_id 						text;
	l_t_position_geography_alignment_period_column 			text;
	l_t_position_geography_assignment_status_column 		text;
	i_rec 													record;
	j_rec													record;
	l_t_dataset_id											text;
	l_t_alignment_period									text;
	l_t_assignment_status									text;
	l_t_start_date											text ; 
	l_t_table_name 											text;
	l_t_table_exists										INTEGER;
	l_t_team_instance										text;
	l_t_team_instance_column								text;
begin
	l_t_sql := '';
	
	raise notice 'started -> l_t_scenario_id -> %', l_t_scenario_id;
	
	select team_instance from t_scenario 
	where sfdc_id=l_t_scenario_id 
	into  l_t_team_instance ;
	raise notice 'l_t_team_instance - >  %',l_t_team_instance;
	
	for i_rec in 
	SELECT DISTINCT TDS.TABLE_NAME ,  TD.DATA_SET_OBJECT_NAME , tsd.filter_expression_destn  , 
				     TDS.DATASET_ID 
  	 FROM  T_DATASET  TD, T_DATASET_RULE_MAP TDS , 
		   T_SCN_RULE_INSTANCE_DETAILS  TSID , 
		   INFORMATION_SCHEMA.TABLES INT , 
		   t_scn_data_object_map tsd  
	WHERE  TD.SFDC_ID = TDS.DATASET_ID 
	  AND  TDS.SCENARIO_RULE_INSTANCE_ID =  TSID.SFDC_ID 
	  AND  TSID.SCENARIO_ID= l_t_scenario_id
	  AND  TD.DATA_SET_OBJECT_NAME IN ( 'Position_Account__c','AxtriaSalesIQTM__Position_Account__c',
										 'AxtriaSalesIQTM__Position_Geography__c','Position_Geography__c')
	  AND  TD.IS_INTERNAL <>TRUE
	  AND  TDS.TABLE_NAME = INT.TABLE_NAME
	  and  tsd.data_set = TD.base_data_set	   
		LOOP
			
					l_t_table_name :=  i_rec.table_name ;
					raise  notice 'Table Name : -  %', i_rec.table_name ; 
			
					l_t_dataset_id :=i_rec.dataset_id ;
					raise notice 'Dataset ID: - %',l_t_dataset_id;
					
					select tb_col_nm from t_ds_col_detail where dataset_id = l_t_dataset_id
					and source_column in ( 'AxtriaSalesIQTM__Assignment_Status__c' ,'Assignment_Status__c')
					into l_t_assignment_status ;
					raise notice 'l_t_assignment_status -> %',l_t_assignment_status; 
					
					select tb_col_nm from t_ds_col_detail where dataset_id = l_t_dataset_id
					and source_column  in ('Team_Instance__r.Alignment_Period__c' , 'AxtriaSalesIQTM__Team_Instance__r.AxtriaSalesIQTM__Alignment_Period__c' )
					into l_t_alignment_period ;
					
					raise notice 'l_t_alignment_period -> %',l_t_alignment_period; 
					
					select tb_col_nm from t_ds_col_detail where dataset_id = l_t_dataset_id
					and source_column  in ('AxtriaSalesIQTM__Effective_Start_Date__c' ,'Effective_Start_Date__c' )
					into l_t_start_date ;
					
					select tb_col_nm from t_ds_col_detail where dataset_id = l_t_dataset_id
					and source_column  in ('AxtriaSalesIQTM__Effective_Start_Date__c' ,'Effective_Start_Date__c' )
					into l_t_start_date ;
					
					raise notice 'l_t_start_date -> %',l_t_start_date; 
					
					
					select tb_col_nm from t_ds_col_detail where dataset_id = l_t_dataset_id
					and source_column  in ('AxtriaSalesIQTM__Team_Instance__c' ,'Team_Instance__c' )
					into l_t_team_instance_column ;
					
					raise notice 'l_t_start_date -> %',l_t_start_date;
					
					l_t_sql := ' update '|| l_t_table_name ||' set '|| l_t_assignment_status ||' = ''Active'' where '||l_t_assignment_status || ' = ''Future Active''  and ' ||l_t_start_date||' <= current_date ' ||' and '|| l_t_team_instance_column || ' = '''||l_t_team_instance  ||'''';
					raise notice ' Assignment Status Query: - % ', l_t_sql;
					Execute l_t_sql ;
					
					/*l_t_sql := ' update '|| l_t_table_name ||' tf set '|| l_t_assignment_status ||' = ''Active'' from t_scenario tt where tf.'||l_t_assignment_status || ' = ''Future Active''  and tf.' ||l_t_start_date||' <=   tt.last_modified_date ' ||' and tf.'|| l_t_team_instance_column || ' = '''||l_t_team_instance  ||''' and    tf.'||l_t_team_instance_column ||' = tt.team_instance ';
					raise notice '1 Assignment Status Query: - % ', l_t_sql;*/
					
					l_t_sql := ' update '|| l_t_table_name ||' set '|| l_t_alignment_period ||' = ''Current'' where '||l_t_alignment_period || ' = ''Future''  and ' ||l_t_start_date||' <= current_date ' ||' and '|| l_t_team_instance_column || ' = '''||l_t_team_instance  ||'''';
					raise notice ' Alignment Period Query: - % ', l_t_sql;
					Execute l_t_sql ;
					
					/*l_t_sql := ' update '|| l_t_table_name ||' tf set '|| l_t_alignment_period ||' = ''Current'' from t_scenario tt where tf.'||l_t_alignment_period || ' = ''Future''  and tf.' ||l_t_start_date||' <=   tt.last_modified_date ' ||' and tf.'|| l_t_team_instance_column || ' = '''||l_t_team_instance  ||''' and    tf.'||l_t_team_instance_column ||' = tt.team_instance ';
					raise notice '2 Alignment Status Query: - % ', l_t_sql;*/
					
		End loop;
		raise notice 'Loop 2 starts';
	

		return 1;
exception
	when others then
		return -1;
		
END;

$BODY$;

ALTER FUNCTION public.fn_update_cycle_team_instance(text)
    OWNER TO postgres;
