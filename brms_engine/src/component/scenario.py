import logging

from src.entity.component import Component
from src.entity.metadata import MetaData
from src.job.runscenario import RunScenario
from src.service.componentlogger import component_logger
from src.service.config import brms_config
from src.service.register import register_component
from src.vendor.redshift import Redshift


@component_logger
@register_component("Scenario")
class ScenarioComponent(Component):

    def __init__(self, scenario_rule_instance, metadata: MetaData):
        super().__init__(scenario_rule_instance, metadata)

    def is_valid(self):
        # s3_path_prefix = 's3://' + brms_config['aws']['incoming_bucket'] + '/'
        # key = self._get_s3_location()[len(s3_path_prefix):]
        # return DataStorage.exists(key)
        return True

    def is_prepared(self) -> bool:
        # create output table with schema
        dataset = self.metadata.get_input_dataset(self.scenario_rule_instance)
        column_details = self.metadata.get_column_details(dataset)
        schema = self.construct_schema(column_details)

        schema_name = brms_config['redshift']['schema_name']
        table_name = self.metadata.get_output_table_name(self.scenario_rule_instance)

        query = """ CREATE TABLE %s.%s %s """ % (schema_name, table_name, schema)
        return Redshift.execute_query(query)

    def execute_scenario_rule_instance(self):
        # run child scenario
        child_scenario = self.metadata.get_child_scenario(self.scenario_rule_instance)
        run_scenario = RunScenario(child_scenario, False)
        run_scenario.invoke()
        logging.debug("Triggered run scenario %s" % child_scenario)

        schema_name = brms_config['redshift']['schema_name']
        output_table = self.metadata.get_output_table_name(self.scenario_rule_instance)
        s3_location = self._get_s3_location()
        iamrole = brms_config['redshift']['iamrole']

        query = """ COPY %s.%s FROM '%s' iam_role '%s' """ % (schema_name, output_table, s3_location, iamrole)
        self.collect_business_rule_status.append(Redshift.execute_query(query))

    def cleanup_scenario_rule_instance(self):
        pass

    def execute_business_rule(self, index, rule):
        pass

    def make_query(self, br_expression) -> str:
        pass

    def _get_s3_location(self):
        return self.metadata.get_input_dataset(self.scenario_rule_instance)[
                   'dst_path__c'] + self.metadata.get_input_table_name(self.scenario_rule_instance) + '/'
        # self.metadata.get_child_scenario(self.scenario_rule_instance) + '/' + \
        # self.metadata.get_input_table_name(self.scenario_rule_instance) + '/'
