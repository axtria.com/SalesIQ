from src.entity.component import Component
from src.entity.metadata import MetaData
from src.service.componentlogger import component_logger
from src.service.config import brms_config
from src.service.register import register_component
from src.vendor.redshift import Redshift


@component_logger
@register_component("Derived Field")
class DerivedField(Component):
    """
    Derived Field implementation.

    Adds a column based on rules derived from existing columns, eg. c = a+b
    """

    def is_valid(self) -> bool:
        """ Validates if the output of previous execution is present or not."""
        return True

    def is_prepared(self) -> bool:
        return True

    def execute_business_rule(self, index, rule):
        """
        Executes the business rule - adds the corresponding derived field.

        :param index: index of the business rule to be executed for a given scenario rule instance
        :param rule: record for the business rule to be executed
        :return:
        """
        self.rule_index = index
        query = self.make_query(self.metadata.get_br_expression(rule))
        return Redshift.execute_query(self.prepare_create_query(query, suffix='_%d' % self.rule_index))

    def make_query(self, br_expression):
        """
        Generates the query to be executed in redshift for adding the derived field.

        :param br_expression: record for the business rule expression for a business rule
        :return: the generated query
        """
        input_tbl = self.metadata.get_input_table_name(self.scenario_rule_instance, with_schema=True)
        suffix = '' if self.rule_index - 1 < 0 else '_%d' % (self.rule_index - 1)

        query = 'select ' + input_tbl + suffix + '.* '
        select_expr = ''
        for _, expr in br_expression.iterrows():
            select_expr += ' , ' + expr['Select_Expr__c'] + ' as ' + expr['column_alias__c']
        query += select_expr + ' from ' + input_tbl + suffix
        return query

    def cleanup_scenario_rule_instance(self):
        """
        Copies the last table created (suffix based) to the original table
        and drops intermediate tables created for adding each derived field
        """
        output_table = self.metadata.get_output_table_name(self.scenario_rule_instance)
        schema_name = brms_config['redshift']['schema_name']

        # Drop original table
        Redshift.drop_table(output_table, schema_name)

        # Copy suffix based table to original table
        select_query = "SELECT * from %s_%d " % (output_table, self.rule_index)
        Redshift.execute_query(self.prepare_create_query(select_query))

        # Drop intermediate tables
        input_table = self.metadata.get_input_table_name(self.scenario_rule_instance)
        for i in range(0, self.rule_index + 1):
            Redshift.drop_table(input_table + '_' + str(i), schema_name)
