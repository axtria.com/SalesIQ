import logging
import re

from src.entity.component import Component
from src.entity.metadata import MetaData
from src.service.componentlogger import component_logger
from src.service.config import brms_config
from src.service.register import register_component
from src.vendor.redshift import Redshift


@component_logger
@register_component("Dedup Aggregate")
class DedupAggregation(Component):
    """
    Dedupe Aggregation implementation.

    De-duplicates entries and aggregates a column after de-duplicating.
    We create the table with all the columns in the output table mentioned in dataset columnn details in SFDC
    We delete the columns not being used in this component in cleanup scenario.
    """

    def __init__(self, scenario_rule_instance, metadata: MetaData):
        super().__init__(scenario_rule_instance, metadata)
        output_dataset = self.metadata.get_output_dataset(self.scenario_rule_instance)
        self.output_table_name = self.metadata.get_output_table_name(self.scenario_rule_instance)
        self.output_column_details = self.metadata.get_column_details(output_dataset)
        self.schema_name = brms_config['redshift']['schema_name']

    def is_valid(self) -> bool:
        """Validates if the output of previous execution is present or not."""
        return True

    def is_prepared(self) -> bool:
        """Create output table using schema."""
        schema = self.construct_schema(self.output_column_details)

        logging.debug('Table created for dedup aggregation ::::' + self.output_table_name)
        query = """ CREATE TABLE %s.%s %s """ % (self.schema_name, self.output_table_name, schema)
        return Redshift.execute_query(query)

    def execute_business_rule(self, index, rule):
        query = self.make_query(self.metadata.get_br_expression(rule))
        return Redshift.execute_query(query)

    def make_query(self, br_expression):
        """Explain why we need ordered columns list as an instance variable."""
        br_expression = br_expression.loc[0, :].to_dict()
        self.ordered_columns = self.get_ordered_columns(br_expression['Select_Expr__c'])
        ordered_columns_str = ','.join(self.ordered_columns)

        dedupe_query = 'SELECT ' + br_expression[
            'Dedup_Expr__c'] + ' FROM ' + self.schema_name + '.' + self.metadata.get_input_table_name(
            self.scenario_rule_instance) + ' GROUP BY ' + br_expression['Dedup_Expr__c']

        aggregate_query = 'SELECT ' + br_expression['Select_Expr__c'] + ' FROM (' + dedupe_query + ') GROUP BY ' + \
                          br_expression['Group_By_Expr__c']

        insert_query = 'INSERT INTO ' + self.schema_name + '.' + self.output_table_name + '(' + ordered_columns_str + \
                       ') ' + aggregate_query

        return insert_query

    def cleanup_scenario_rule_instance(self):
        extra_columns = set(self.output_column_details['tb_col_nm__c']) - set(self.ordered_columns)
        query = ''
        for column in extra_columns:
            query += 'ALTER TABLE %s.%s DROP COLUMN %s;' % (self.schema_name, self.output_table_name, column)
        # Single transaction
        if query != '':
            Redshift.execute_query(query)
        else:
            logging.info("No extra columns found to be removed.")

    def get_ordered_columns(self, expression: str) -> list:
        return re.findall(r'as (\w+)', expression)

        # # select expression
        # _, rule = next(self.metadata.get_business_rules(self.scenario_rule_instance))
        # select_expr = (self.metadata.get_br_expression(rule)).ix[0]['Select_Expr__c']
        #
        # columns = pd.DataFrame(list(map(lambda x: x.split(' as ')[1]), select_expr.split(',')), columns=['columns'])
        #
        # # inner join
        # schema_columns = pd.merge(column_details, columns, on=['tb_col_nm__c', 'columns'])


        # self.columns is needed to find extra columns to be deleted
        #         self.columns = pd.DataFrame(list(map(lambda x: (x.split(' as '))[1], select_expr.split(','))),
        #                                     columns=['columns'])
        #         column_names = self.columns['columns'].str.cat(sep=',')
