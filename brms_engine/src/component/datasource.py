import logging

from src.entity.component import Component
from src.entity.metadata import MetaData
from src.service.componentlogger import component_logger
from src.service.config import brms_config
from src.service.register import register_component
from src.vendor.datastorage import DataStorage
from src.vendor.redshift import Redshift


@component_logger
@register_component("Data Source")
class DataSource(Component):
    """
    Execution of the Data Source Component

    Create external table in Redshift if the input is external else do nothing

    Methods (Override from parent class):
        __init__(): Added attribute table_is_external for given scenario rule instance.

        is_prepared(): Create external table if input table type is external.

        execute_scenario_rule_instance(): Instead of looping through the Business Rules, copy the data from external
        table (input) to internal table (output).

        cleanup_scenario_rule_instance(): In case of external table (input), call the parent method.
    """

    def __init__(self, scenario_rule_instance, metadata: MetaData):
        """

        :param scenario_rule_instance: Scenario rule instance record
        :param metadata: Metadata
        """
        super().__init__(scenario_rule_instance, metadata)
        self.table_is_external = False if self.metadata.is_input_table_internal(self.scenario_rule_instance) else True

    def is_valid(self):
        """
        Checks if the input file is present in S3 location
        :return: True if the file exists in S3 else False
        """
        if self.table_is_external:
            s3_path_prefix = 's3://' + brms_config['aws']['incoming_bucket'] + '/'
            key = self._get_s3_location()[len(s3_path_prefix):]
            return DataStorage.exists(key)
        return True

    def is_prepared(self) -> bool:
        """
        Create external table from S3
        :return: True if the method runs successfully without any error
        """
        if self.table_is_external:
            # create output table with schema
            dataset = self.metadata.get_input_dataset(self.scenario_rule_instance)
            column_details = self.metadata.get_column_details(dataset)
            schema = self.construct_schema(column_details)

            schema_name = brms_config['redshift']['schema_name']
            table_name = self.metadata.get_output_table_name(self.scenario_rule_instance)

            query = """ CREATE TABLE %s.%s %s """ % (schema_name, table_name, schema)
            return Redshift.execute_query(query)
        return True

    def execute_scenario_rule_instance(self):
        """
        Copies 'input' external table to 'output' internal table
        """
        if self.table_is_external:
            schema_name = brms_config['redshift']['schema_name']
            output_table = self.metadata.get_output_table_name(self.scenario_rule_instance)
            s3_location = self._get_s3_location()
            iamrole = brms_config['redshift']['iamrole']

            query = """ COPY %s.%s FROM '%s' iam_role '%s' """ % (schema_name, output_table, s3_location, iamrole)
            self.collect_business_rule_status.append(Redshift.execute_query(query))

    def cleanup_scenario_rule_instance(self):
        pass

    def execute_business_rule(self, index, rule):
        pass

    def make_query(self, br_expression) -> str:
        pass

    def _get_s3_location(self):
        """

        :return: S3 location of the input table to be copied
        """
        return self.metadata.get_input_dataset(self.scenario_rule_instance)['dst_path__c'] + \
               self.metadata.get_input_table_name(self.scenario_rule_instance) + '/'
