from src.entity.component import Component
from src.service.componentlogger import component_logger
from src.service.register import register_component
from src.vendor.redshift import Redshift

@component_logger
@register_component("Filter")
class Filter(Component):
    """Filter component implementation.

    This component takes scenarioRuleInstanceId and a copy of MetaData class. MetaData copy is retained
    to avoid calls to Salesforce and S3.

    Methods:
        is_valid():

        is_prepared():

        execute_business_rule():

        make_query():

        clean_up_scenario_rule_instance():

    Attributes:
        scenario_rule_instance_id: A component is defined by scenario rule instance Id.
        metadata: A component receives a copy of metadata

    """

    def is_valid(self) -> bool:
        """Validates output of previous execution is present or not."""
        return True
        # key = brms_config['data_storage']['src_path'].format(folder='RE', scenario_id=self.metadata.scenario_id,
        #                                                      table_name=self.metadata.get_input_table_name(
        #                                                          self.scenario_rule_instance))
        # return DataStorage.exists(brms_config['aws']['incoming_bucket'], key)

    def is_prepared(self) -> bool:
        return True

    def execute_business_rule(self, index, rule) -> bool:
        """For each business rule create BR expression and execute unload query."""
        br_expression = self.metadata.get_br_expression(rule)
        query_str = self.make_query(br_expression)
        return Redshift.execute_query(self.prepare_create_query(query_str))

    def make_query(self, br_expression):
        """Use metadata to fetch BR expression based on rule Id and return select query."""
        br_expression = br_expression.loc[0, :].to_dict()
        query = ''
        if br_expression['Select_Expr__c']:
            query += 'SELECT ' + br_expression['Select_Expr__c']
        query += ' FROM ' + self.metadata.get_input_table_name(self.scenario_rule_instance, with_schema=True)
        if br_expression['Where_Expr__c']:
            query += ' WHERE ' + br_expression['Where_Expr__c']
        return query

    def cleanup_scenario_rule_instance(self):
        # TODO: Add no rule found exception.
        pass
