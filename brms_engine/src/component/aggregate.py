from src.entity.component import Component
from src.service.componentlogger import component_logger
from src.service.register import register_component
from src.vendor.redshift import Redshift


@component_logger
@register_component('Aggregation')
class Aggregate(Component):
    """
    Aggregate component implementation.

    This component takes scenarioRuleInstanceId and a copy of MetaData class. MetaData copy is retained
    to avoid calls to Salesforce and S3.

    Methods:
        is_valid(): Currently returns True

        is_prepared(): Currently returns True

        execute_business_rule(): Override abstract method and uses metadata class to get br_expression
        to execute query

        make_query(): get input table and br_expression and return the query as string

        cleanup_scenario_rule_instance(): Currently function is passed as nothing to be dropped

    """

    def is_valid(self) -> bool:
        return True

    def is_prepared(self) -> bool:
        return True

    def execute_business_rule(self, index, rule):
        """For each business rule create BR expression and execute unload query."""
        query = self.make_query(self.metadata.get_br_expression(rule))
        return Redshift.execute_query(self.prepare_create_query(query))

    def make_query(self, br_expression):
        """Use metadata to fetch BR expression based on rule Id and return select query."""
        br_expression = br_expression.loc[0, :].to_dict()
        input_table_name = self.metadata.get_input_table_name(self.scenario_rule_instance, with_schema=True)
        query = "SELECT " + br_expression['Select_Expr__c'] + \
                " FROM " + input_table_name + \
                " GROUP BY " + br_expression['Group_By_Expr__c']
        return query

    def cleanup_scenario_rule_instance(self):
        pass
