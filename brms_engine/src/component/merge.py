import logging

from src.entity.component import Component
from src.service.componentlogger import component_logger
from src.service.config import brms_config
from src.service.register import register_component
from src.vendor.datastorage import DataStorage
from src.vendor.redshift import Redshift

@component_logger
@register_component("Merge")
class Merge(Component):
    """
    Merge component implementation inherits Component class.

    Tables to be joined can have different schemas.


    """

    def is_valid(self) -> bool:
        return True

    def is_prepared(self) -> bool:
        return True

    def execute_business_rule(self, index, rule):
        br_expression = self.metadata.get_join_br_expression(rule)
        query = self.make_query(br_expression)
        return Redshift.execute_query(self.prepare_create_query(query))

    def make_query(self, br_expression):
        br_expression = br_expression.loc[0, :].to_dict()
        query = self._add_schema_name(br_expression['Select_Execute_Expression__c'])
        return query

    def _add_schema_name(self, query_expr):
        query_expr = query_expr.replace(self.metadata.get_input_table_name(self.scenario_rule_instance),
                                        self.metadata.get_input_table_name(self.scenario_rule_instance,
                                                                           with_schema=True))
        query_expr = query_expr.replace(self.metadata.get_input_table_name(self.scenario_rule_instance, index=1),
                                        self.metadata.get_input_table_name(self.scenario_rule_instance, index=1,
                                                                           with_schema=True))
        return query_expr

    def cleanup_scenario_rule_instance(self):
        pass
