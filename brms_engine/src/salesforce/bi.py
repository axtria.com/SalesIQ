import logging

import pandas as pd
from simple_salesforce import Salesforce

from src.service.config import brms_config
from src.service.helpers import decoder
from src.service.register import register_client
from src.vendor.client import Client


@register_client("BI")
class BI(Client.Operations):
    sf = Salesforce(username=brms_config['salesforce-bi']['username'],
                    password=decoder(brms_config['salesforce-bi']['password']),
                    security_token=brms_config['salesforce-bi']['security_token'])

    def query(self, query_string):
        logging.info('\n\n %s \n' % query_string)
        records = BI.sf.query(query_string)
        return pd.DataFrame(records['records'])

    def query_all(self, query_string):
        records = BI.sf.query_all(query_string)
        # filtering result returned by query
        dic = records["records"]
        df = pd.DataFrame.from_dict(dic)
        df.drop('attributes', axis=1, inplace=True)
        return df

    def update_business_rule(self, rule_id, status):
        pass

    def update_scenario(self, scenario_id, status):
        pass

    def update_scenario_rule_instance(self, scenario_rule_instance_id, status):
        pass
