import logging

import pandas as pd
from simple_salesforce import Salesforce

from src.service.config import brms_config
from src.service.helpers import decoder
from src.service.register import register_client
from src.vendor.client import Client


@register_client("Product")
class Product(Client.Operations):
    sf = Salesforce(username=brms_config['salesforce-product']['username'],
                    password=decoder(brms_config['salesforce-product']['password']),
                    security_token=brms_config['salesforce-product']['security_token'])
    scenario = sf.Scenario__c
    scenario_rule_instance = sf.Scenario_Rule_Instance_Details__c
    business_rule = sf.Business_Rules__c

    def query(self, query_string):
        logging.info('\n\n %s \n' % query_string)
        records = Product.sf.query(query_string)
        return pd.DataFrame(records['records'])

    def query_all(self, query_string):
        records = Product.sf.query_all(query_string)
        # filtering result returned by query
        dic = records["records"]
        df = pd.DataFrame.from_dict(dic)
        df.drop('attributes', axis=1, inplace=True)
        return df

    def update_scenario(self, scenario_id, status):
        logging.info('Scenario id:: %s flag updated: %s' % (scenario_id, status))
        Product.scenario.update(scenario_id, {'Request_Process_Stage__c': status})

    def update_business_rule(self, rule_id, status):
        logging.info('Scenario rule id:: %s flag updated: %s' % (rule_id, status))
        Product.scenario_rule_instance.update(rule_id, {'Status__c': status})

    def update_scenario_rule_instance(self, scenario_rule_instance_id, status):
        logging.info('Scenario scenario rule instance id:: %s flag updated: %s' % (scenario_rule_instance_id, status))
        Product.scenario_rule_instance.update(scenario_rule_instance_id, {'Status__c': status})
