from src.entity.parser import Parser
from src.service.register import register_parser


@register_parser("Product")
class Product(Parser):
    def get_metadata_from_sfdc(self):
        pass

    def make_business_rules(self):
        self.business_rules = self.client.query(
            """SELECT Id,Name, Business_Rules__c, Business_Rules__r.Name, column_alias__c, Select_Expr__c, Where_Expr__c,
            Group_By_Expr__c, Business_Rules_Expression_Ext_Id__c FROM Business_Rules_Expression__c WHERE
            Business_Rules__c IN
            (SELECT Id FROM Business_Rules__c WHERE ScenarioRuleInstanceDetails__c IN ({scenario_instance_details}))
            """.format(scenario_instance_details="'" + "','".join(self.scenario_rule_instance['Id']) + "'")
        )

    def make_dataset_rule_map(self):
        self.dataset_rule_map = self.client.query(
            """SELECT Id, dataset_id__c, dataset_id__r.Name, DS_Rule_Map_Id__c, ds_type__c, scenario_rule_instance_id__c,
            scenario_rule_instance_id__r.Name, Table_Display_Name__c, table_name__c, base_data_set_Id__c
            FROM Data_Set_Rule_Map__c WHERE scenario_rule_instance_id__c IN
            (SELECT Id FROM Scenario_Rule_Instance_Details__c WHERE Scenario_Id__c = '{scenario_id}')
            """.format(scenario_id=self.scenario_id)
        )

    def make_br_join_rule(self):
        self.br_join_rule = self.client.query(
            """
            SELECT Id, Name, Business_Rules__c, Business_Rules__r.Name, Scenario__c, Scenario__r.Name,
            Join_Display_expression__c, Join_Execute_Expression__c, Select_Display_Expression__c,
            Select_Execute_Expression__c FROM BR_JoinRule__c WHERE Scenario__c = '{scenario_id}'
            """.format(scenario_id=self.scenario_id)
        )

    def make_dataset_column_details(self):
        self.dataset_column_details = self.client.query(
            """SELECT Id, dataset_id__c, dataset_id__r.Name, ds_col_name__c, datatype__c, is_mandatory__c, Key_Column__c,
            validation__c, Error_String__c, tb_col_nm__c, Table_Data_Type__c, value_type__c, variable_type__c,
            column_id__c FROM Data_Set_Column_Detail__c WHERE dataset_id__c IN
            (SELECT Id FROM Data_Set__c WHERE Scenario__c ='{scenario_id}')
            """.format(scenario_id=self.scenario_id)
        )

    def make_scenario(self):
        self.scenario = self.client.query(
            """SELECT Id, Name, BusinessRuleTemplate__c, BusinessRuleTemplate__r.Name, Data_Object__c,
            Data_Object__r.name, Request_Process_Stage__c, Scenario_Name__c, Scenario_Stage__c, Scenario_Status__c,
            Scn_Type__c, Scn_Type__r.Name, Source_Team_Instance__c, Source_Team_Instance__r.Name, Team_Instance__c,
            Team_Instance__r.Name, Team_Name__c, Team_Name__r.Name  FROM Scenario__c  WHERE Id = '{scenario_id}'
            """.format(scenario_id=self.scenario_id)
        )

    def make_scenario_rule_instance(self):
        self.scenario_rule_instance = self.client.query(
            """SELECT Id, Name, Scenario_Id__c, Scenario_Id__r.Name, BRT_Details_Id__c, BRT_Details_Id__r.Name,
            BusinessRuleTemplateId__c, BusinessRuleTemplateId__r.Name, Component_Type_Master__c,
            Component_Type_Master__r.Name, ComponentTypeLabel__c, Component_Display_Order__c, Component_Level__c,
            Exec_Seq__c, Is_Editable__c, Is_Visible__c, Parent_Display_Order__c, Parent_Level__c,
            Scenario_Rule_Instance_Details_Ext_Id__c, Status__c FROM Scenario_Rule_Instance_Details__c WHERE
            Scenario_Id__c = '{scenario_id}' ORDER BY Exec_Seq__c ASC
            """.format(scenario_id=self.scenario_id)
        )

    def make_dataset(self):
        self.dataset = self.client.query(
            """SELECT Id, Name, Base_Data_Set__c, Base_Data_Set__r.Name, is_internal__c, period__c, Scenario__c,
            source__c, src_conn_str__c, src_path__c, destination__c, dst_conn_str__c, dst_path__c, version__c,
            Data_Set_Id__c FROM Data_Set__c WHERE Scenario__c = '{scenario_id}'
            """.format(scenario_id=self.scenario_id)
        )

    def make_business_rules_expression(self):
        self.business_rules_expression = self.client.query(
            """SELECT Id,Name, Business_Rules__c, Business_Rules__r.Name, column_alias__c, Select_Expr__c, Where_Expr__c,
            Group_By_Expr__c, Business_Rules_Expression_Ext_Id__c FROM Business_Rules_Expression__c WHERE
            Business_Rules__c IN
            (SELECT Id FROM Business_Rules__c WHERE ScenarioRuleInstanceDetails__c IN ({scenario_instance_details}))
            """.format(scenario_instance_details="'" + "','".join(self.scenario_rule_instance['Id']) + "'")
        )
