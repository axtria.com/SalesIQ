import pandas as pd

from src.entity.parser import Parser
from src.service.register import register_parser
from src.vendor.client import Client


@register_parser("BI")
class BI(Parser):
    def __init__(self, scenario_id: str, client: Client):
        super().__init__(scenario_id, client)
        self.measure_execution_details = None
        self.measure_execution_parameter = None
        self.measure_instance_parameter = None
        self.measure_input_table = None
        self.measure_merge_parameter = None
        self.derived_parameter = None
        self.measure_reporting_column = None

    def get_metadata_from_sfdc(self):
        self.measure_execution_details = self.client.query(
            """SELECT ConnectionReceivedId,ConnectionSentId,CreatedById,CreatedDate,Definition_Status__c,
            EmailId__c,EmailNotify__c,End_Date__c,ExecutedByText__c,Executed_By__c,Executed_On__c,
            ExecutionPeriod__c,Execution_End_Time__c,Execution_Start_Time__c,External_Id__c,
            Folder_External_Id__c,Folder__c,Id,Instance_Name__c,IsDeleted,LastActivityDate,LastModifiedById,
            LastModifiedDate,Log__c,Measure_Error_Log__c,Measure_External_Id__c,Measure_Master__c,
            Measure_Output_Table__c,Name,Overall_Status__c,prevOrdr__c,selectListOriginal__c,Select_Column__c,
            Start_Date__c,Status__c,SystemModstamp,Tenant__c,Time_Period__c,Used_Column__c 
            FROM Measure_Execution_Details__c WHERE Id = '{scenario_id}'
            """.format(scenario_id=self.scenario_id)
        )
        self.measure_execution_parameter = self.client.query(
            """SELECT ConnectionReceivedId,ConnectionSentId,CreatedById,CreatedDate,
            External_Id__c,Id,IsDeleted,LastActivityDate,LastModifiedById,LastModifiedDate,
            Measure_Execution_Details_External_Id__c,Measure_Instance_Id__c,Name,OwnerId,
            Parameter_Name__c,Parameter_Value__c,seqnumber__c,SystemModstamp,Tenant__c 
            FROM MEASURE_EXECUTION_PARAMETER__c WHERE Measure_Instance_Id__c = '{scenario_id}'
            """.format(scenario_id=self.scenario_id)
        )
        self.measure_instance_parameter = self.client.query(
            """SELECT ConnectionReceivedId,ConnectionSentId,CreatedById,CreatedDate,DonotShow__c,
            EndDateBaseline__c,End_Date__c,Entity__c,External_Id__c,Id,IsDeleted,IsTimebound__c,
            IsTimeSeries__c,Label__c,LastActivityDate,LastModifiedById,LastModifiedDate,
            Measure_Execution_Details_External_Id__c,Measure_Instance_Id__c,MergedDataSet__c,
            MergedField__c,Name,Object_Id__c,Object_Name__c,OwnerId,Period__c,Sequence__c,
            Start_Date__c,SystemModstamp,Tenant__c,Till_Date__c 
            FROM Measure_Instance_Parameter__c WHERE Measure_Instance_Id__c = '{scenario_id}' 
            """.format(scenario_id=self.scenario_id)
        )

        self.measure_input_table = self.client.query(
            """SELECT ConnectionReceivedId,ConnectionSentId,CreatedById,CreatedDate,Entity__c,External_Id__c,
            Id,IsDeleted,isTimeSeries__c,LastActivityDate,LastModifiedById,LastModifiedDate,
            MeasureInstanceId__c,Measure_External_Id__c,Measure__c,Name,Object_Id__c,
            Object_Name__c,OwnerId,SystemModstamp,Tenant__c 
            FROM Measure_Input_Table__c WHERE MeasureInstanceId__c = '{scenario_id}'
            """.format(scenario_id=self.scenario_id)
        )

        self.measure_merge_parameter = self.client.query(
            """SELECT aggregateselect__c,collapseTab__c,conditionList1__c,conditionList2__c,ConnectionReceivedId,
            ConnectionSentId,CreatedById,CreatedDate,DataSet1Id__c,DataSet2Id__c,External_Id__c,Formatted_Condition1__c,
            Formatted_Condition2__c,Id,IsDeleted,jsonString__c,label1__c,label2__c,LastActivityDate,LastModifiedById,
            LastModifiedDate,MeasureInstanceId__c,Measure_External_Id__c,Measure__c,mergedDataSetId__c,Name,
            onCondition__c,Operation_Type__c,OwnerId,SelectedDataSet1__c,SelectedDataSet2__c,
            SelectedMeasure1__c,SelectedMeasure2__c,Select_List2__c,Select_List__c,Sequence_Number__c,
            SystemModstamp,Tenant__c,whereCondition1__c,whereCondition2__c 
            FROM measure_merge_Parameter__c WHERE MeasureInstanceId__c = '{scenario_id}'
            """.format(scenario_id=self.scenario_id)
        )

        self.derived_parameter = self.client.query(
            """SELECT Aggregation_Column__c,Base_Line_Period__c,ConnectionReceivedId,
            ConnectionSentId,CreatedById,CreatedDate,Datatype__c,Data_Type__c,
            delete1__c,Derived_Operation_Id__c,Dimension_Column_1__c,Dimension__c,
            Dimenson_Column_2__c,Expression__c,External_Id__c,function_name__c,
            Function__c,Grid__c,Group_By_Column__c,Id,IsDeleted,Label__c,LastModifiedById,
            LastModifiedDate,Length__c,MeasureInstanceId__c,Measure_External_Id__c,
            Measure__c,mergeCount__c,Name,New_Expression__c,Option__c,
            Order_By_Column__c,OwnerId,Pay_Curve_Attainment__c,Period__c,
            Precision__c,Rank_Type__c,SelfReference_Date__c,Sequence__c,
            seq__c,ShowHide_State__c,SystemModstamp,Tenant__c,Till_Date__c, Type__c 
            FROM Derived_Parameter__c WHERE MeasureInstanceId__c = '{scenario_id}'
            """.format(scenario_id=self.scenario_id)
        )

        self.measure_reporting_column = self.client.query(
            """SELECT Adapter__c,ConnectionReceivedId,ConnectionSentId,CreatedById,CreatedDate,
            Decimal_Places__c,External_Id__c,Field_Name_Original__c,Field_Name__c,
            formatType__c,Format__c,Header_Name__c,Id,IsDeleted,LastActivityDate,
            LastModifiedById,LastModifiedDate,MeasureInstanceId__c,Measure_External_Id__c,
            Measure_Master__c,Name,OwnerId,precisionValue__c,Precision__c,Sequence__c,
            SystemModstamp,Tenant__c,type2__c,Type__c 
            FROM Measure_Reporting_column__c WHERE MeasureInstanceId__c = '{scenario_id}'
            """.format(scenario_id=self.scenario_id)
        )

    def make_scenario(self):
        self.scenario = pd.DataFrame({
            'Id': self.scenario_id,
            'Name': self.measure_execution_details['Instance_Name__c']
        })

    def make_scenario_rule_instance(self):
        scenario_rule_instance = []
        #for index, row in self.
        scenario_rule_instance.append({
            'Component_Type_Master__r': '',
            'Exec_Seq__c': '',
            'Id': '',
            'Name': '',
            'Scenario_Id__c': self.scenario_id
        })
        self.scenario_rule_instance = pd.DataFrame()

    def make_business_rules(self):
        pass

    def make_dataset_rule_map(self):
        pass

    def make_br_join_rule(self):
        pass

    def make_dataset_column_details(self):
        pass

    def make_dataset(self):
        pass

    def make_business_rules_expression(self):
        pass
