import logging

import pandas as pd
from simple_salesforce import Salesforce

from src.service.config import brms_config
from src.service.helpers import decoder, deprecated


class SalesForce:
    """

    Salesforce class to connect and interact with salesforce platform.
    Credentials are taken from config file and logined using security token method whenever object is called.
    We can write queries in Salesforce Object Query Language(SOQL).

        Methods
            query(): Return query execution records/result and log the query .
            :param query_string: A string
            :return : A list

            query_all(): In order to retrieve large number of records into a dataframe using a single call.
            :param query_string: A string
            :return : A dataframe

        """

    sf = Salesforce(username=brms_config['salesforce-product']['username'],
                    password=decoder(brms_config['salesforce-product']['password']),
                    security_token=brms_config['salesforce-product']['security_token'])
    scenario = sf.Scenario__c
    scenario_rule_instance = sf.Scenario_Rule_Instance_Details__c
    business_rule = sf.Business_Rules__c

    @staticmethod
    @deprecated
    def query(query_string):
        logging.info('\n\n %s \n' % query_string)
        records = SalesForce.sf.query(query_string)
        return records['records']

    @staticmethod
    @deprecated
    def query_all(query_string: str) -> pd.DataFrame:
        records = SalesForce.sf.query_all(query_string)

        # filtering result returned by query
        dic = records["records"]
        df = pd.DataFrame.from_dict(dic)
        df.drop('attributes', axis=1, inplace=True)
        return df
