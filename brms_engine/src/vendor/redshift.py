import logging

from psycopg2._psycopg import ProgrammingError, InternalError, NotSupportedError
from psycopg2.pool import SimpleConnectionPool

from src.service.config import brms_config
from src.service.helpers import decoder


class Redshift:
    """

    Redshift class to connect and execute queries on Redshift cluster.
    Credentials are taken from config file and connected to cluster whenever object is called.
    No explicit operator required for commiting DML statements as autocommit is True.

    Methods
        execute_query(): Return query execution status and log if any error occurs.
        :param query: A string
        :return : A bool

        return_output(): Execute query and return output if no error occurs.

        drop_table(): Execute drop table query.
        :param table_name: Name of table
        :param schema_name: Name of table schema

        unload_table(): Output Table to S3 a single file.
        :param table_name: Name of table
        :param schema_name: Name of table schema
        :param s3_location: S3 Bucket location for file output

    """
    brms_config['redshift']['password'] = decoder(brms_config['redshift']['password'])
    connection_str = "dbname='{dbname}' host='{host}' port='{port}' user='{user}' password='{password}'".format(
        **dict(brms_config['redshift']))
    connection_pool = SimpleConnectionPool(1, 5, connection_str)

    data_type_mapper = {'text': 'varchar', 'numeric': 'numeric(28, 13)'}

    @staticmethod
    def execute_query(query: str) -> bool:
        logging.info('\n\n %s \n' % query)
        try:
            conn = Redshift.connection_pool.getconn()
            conn.autocommit = True
            cur = conn.cursor()
            cur.execute(query)
            Redshift.connection_pool.putconn(conn=conn)
            return True
        except (ProgrammingError, InternalError, NotSupportedError) as e:
            logging.error(e)
            return False

    @staticmethod
    def return_output(query: str):
        logging.info('\n\n %s \n' % query)
        try:
            conn = Redshift.connection_pool.getconn()
            conn.autocommit = True
            cur = conn.cursor()
            cur.execute(query)
            result = cur.fetchall()
            Redshift.connection_pool.putconn(conn=conn)
            return result
        except (ProgrammingError, InternalError, NotSupportedError) as e:
            logging.error(e)
            return []

    @staticmethod
    def drop_table(table_name: str, schema_name: str) -> bool:
        query = """ DROP TABLE IF EXISTS %s.%s """ % (schema_name, table_name)
        return Redshift.execute_query(query)

    @staticmethod
    def unload_table(table_name, schema_name: str, s3_location, parallel='OFF'):
        iamrole = brms_config['redshift']['iamrole']
        query = """UNLOAD ($$SELECT * FROM %s.%s$$) TO '%s' iam_role '%s' ALLOWOVERWRITE PARALLEL %s""" % (
            schema_name, table_name, s3_location, iamrole, parallel)
        return Redshift.execute_query(query)
