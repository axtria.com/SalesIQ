from abc import ABC, abstractmethod

import boto3

from src.service.config import brms_config
from src.service.helpers import decoder


class Notification(ABC):
    @staticmethod
    @abstractmethod
    def send(subject, message):
        pass


class Email(Notification):
    """
        Simple Email Service

        Concrete class to send the notification via email medium.
    """

    client = boto3.client('ses', region_name=brms_config['aws']['region_name'],
                          aws_access_key_id=brms_config['aws']['access_key'],
                          aws_secret_access_key=decoder(brms_config['aws']['secret_key']))

    @staticmethod
    def send(subject, message):
        Email.client.send_email(
            Source=brms_config['email']['from'],
            Destination={
                'ToAddresses': [
                    brms_config['email']['to'],
                ]
            },
            Message={
                'Subject': {
                    'Data': subject,
                    'Charset': 'UTF-8'
                },
                'Body': {
                    'Text': {
                        'Data': '',
                        'Charset': 'UTF-8'
                    },
                    'Html': {
                        'Data': Email._add_header() + message + Email._add_footer(),
                        'Charset': 'UTF-8'
                    }
                }
            }
        )

    @staticmethod
    def _add_header():
        """Templatize header - may contain CSS, or greeting message."""
        return ''

    @staticmethod
    def _add_footer():
        """Templatize footer - closing remarks."""
        return ''
