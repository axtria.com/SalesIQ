import logging
import os
from uuid import uuid4

import boto3
from pandas import read_csv, DataFrame
from pandas.errors import EmptyDataError

from src.service.config import brms_config
from src.service.helpers import decoder


class DataStorage:
    """
    DataStorage class implements AWS S3.

    This class implements boto3 API to interact with S3 service. Currently it supports the following methods:
    upload_file(): copy an input dataframe into a csv at unique S3 temp location,
    use default s3 client method to copy to desired path

    download_file(): copy csv from remote path to unique S3 path with temp filename,
    return csv as dataframe to user and delete temp file.

    copy_file(): use boto3 resource methods to loop and filter out src_path objects and copy one by one

    exists(): use try catch block to validate whether given key exists in the bucket

    list_files(): use it to get list of files at a given prefix of location

    Attributes:
        :param: src_path: source path
        :param: dest_path: destination_path
        :param:
    """
    s3_client = boto3.client('s3', region_name=brms_config['aws']['region_name'],
                             aws_access_key_id=brms_config['aws']['access_key'],
                             aws_secret_access_key=decoder(brms_config['aws']['secret_key']))
    s3_resource = boto3.resource('s3',
                                 aws_access_key_id=brms_config['aws']['access_key'],
                                 aws_secret_access_key=decoder(brms_config['aws']['secret_key']))
    bucket = brms_config['aws']['incoming_bucket']
    directory = './/tmp//'

    @staticmethod
    def upload_file(data_obj, remote_path):
        """For a given filename and path, upload from ./tmp directory to AWS incoming bucket."""
        local_path = DataStorage.directory + str(uuid4())
        try:
            data_obj.to_csv(local_path, index=False)
            DataStorage.s3_client.upload_file(local_path, DataStorage.bucket, remote_path)
            os.remove(local_path)
        except Exception as e:
            logging.error(e.args)

    @staticmethod
    def download_file(remote_path):
        """downloads file from s3 and returns the data frame"""

        try:
            local_path = DataStorage.directory + str(uuid4())
            DataStorage.s3_client.download_file(DataStorage.bucket, remote_path, local_path)
            data_obj = read_csv(local_path)
            os.remove(local_path)
        except Exception as e:
            logging.warning(e.args)
            logging.warning(remote_path)
            data_obj = DataFrame()
        return data_obj

    @staticmethod
    def copy_file(src_path, dest_path):
        """Copies all the files from src_path to dest_path"""
        bucket_resource = DataStorage.s3_resource.Bucket(DataStorage.bucket)
        for obj in bucket_resource.objects.filter(Prefix=src_path):
            source = {'Bucket': DataStorage.bucket, 'Key': obj.key}
            # replace the prefix for each file
            dest_key = obj.key.replace(src_path, dest_path)
            bucket_resource.copy(source, dest_key)

    @staticmethod
    def exists(key):
        """Validates presence of key for a given S3 bucket."""
        logging.info('Validating file presence... %s' % key)
        try:
            contents = DataStorage.s3_client.list_objects(Bucket=DataStorage.bucket, Prefix=key)['Contents']
            return True
        except KeyError:
            logging.error("File not found %s" % key)
            return False

    @staticmethod
    def list_files(prefix: str) -> list:
        """returns all 'folders' and file names in s3 under the prefix folder"""
        keys = []
        result = DataStorage.s3_client.list_objects(Bucket=DataStorage.bucket, Prefix=prefix, Delimiter='/')
        if result.get('CommonPrefixes'):
            for o in result.get('CommonPrefixes'):
                keys.append(o.get('Prefix'))
        if result.get('Contents'):
            for o in result.get('Contents'):
                keys.append(o.get('Key'))
        return keys
