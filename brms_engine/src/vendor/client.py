from abc import abstractmethod, ABC

from src.service.register import REGISTERED_CLIENTS


class Client:
    def __init__(self, name):
        self.client = REGISTERED_CLIENTS[name]()

    def query(self, query_string):
        return self.client.query(query_string)

    def query_all(self, query_string):
        return self.client.query_all(query_string)

    def update_scenario(self, scenario_id, status):
        return self.client.update_scenario(scenario_id, status)

    def update_scenario_rule_instance(self, scenario_rule_instance_id: str, status: str):
        return self.client.update_scenario_rule_instance(scenario_rule_instance_id, status)

    def update_business_rule(self, rule_id: str, status: str):
        return self.client.update_business_rule(rule_id, status)

    class Operations(ABC):
        @abstractmethod
        def query(self, query):
            pass

        @abstractmethod
        def query_all(self, query_string):
            pass

        @abstractmethod
        def update_scenario(self, scenario_id, status):
            pass

        @abstractmethod
        def update_scenario_rule_instance(self, scenario_rule_instance_id, status):
            pass

        @abstractmethod
        def update_business_rule(self, rule_id, status):
            pass
