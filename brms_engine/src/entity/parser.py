from abc import abstractmethod, ABC

from src.vendor.client import Client


class Parser(ABC):
    def __init__(self, scenario_id: str, client: Client):
        self.scenario_id = scenario_id
        self.client = client
        self.scenario = None
        self.scenario_rule_instance = None
        self.dataset_rule_map = None
        self.dataset_column_details = None
        self.dataset = None
        self.business_rules = None
        self.br_join_rule = None
        self.business_rules_expression = None

    @abstractmethod
    def get_metadata_from_sfdc(self):
        pass

    @abstractmethod
    def make_scenario(self):
        pass

    @abstractmethod
    def make_scenario_rule_instance(self):
        pass

    @abstractmethod
    def make_dataset_rule_map(self):
        pass

    @abstractmethod
    def make_dataset_column_details(self):
        pass

    @abstractmethod
    def make_dataset(self):
        pass

    @abstractmethod
    def make_business_rules(self):
        pass

    @abstractmethod
    def make_br_join_rule(self):
        pass

    @abstractmethod
    def make_business_rules_expression(self):
        pass
