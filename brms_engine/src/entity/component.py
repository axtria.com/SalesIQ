from abc import ABC, abstractmethod

from src.entity.metadata import MetaData
from src.service.config import brms_config
from src.vendor.redshift import Redshift


class Component(ABC):
    def __init__(self, scenario_rule_instance, metadata: MetaData):
        """
        Attributes:
            scenario_rule_instance: A component is defined by scenario rule instance.
            metadata: A component receives a copy of metadata
        """
        self.metadata = metadata
        self.scenario_rule_instance = scenario_rule_instance
        self.collect_business_rule_status = []

    def invoke(self):
        if self.is_valid() and self.is_prepared():
            self.execute_scenario_rule_instance()
            self.cleanup_scenario_rule_instance()
            self.mark_scenario_rule_instance_complete()
        else:
            self.mark_scenario_rule_instance_invalid()

    def execute_scenario_rule_instance(self):
        for index, rule in self.metadata.get_business_rules(self.scenario_rule_instance):
            business_rule_execution_status = self.execute_business_rule(index, rule)
            self.mark_business_rule_complete(rule, business_rule_execution_status)

    def get_scenario_rule_instance_completion_status(self):
        return all(self.collect_business_rule_status)

    def mark_scenario_rule_instance_complete(self):
        """Evaluate scenario rule completion status, referencing the collection of business rule status"""
        status = 'Success' if self.get_scenario_rule_instance_completion_status() else 'Error Occured'
        self.metadata.update_scenario_rule_instance(self.scenario_rule_instance, status)

    def mark_scenario_rule_instance_invalid(self):
        """Mark scenario rule instance Failed due to invalid or unprepared failed statements"""
        self.metadata.update_scenario_rule_instance(self.scenario_rule_instance, 'Error Occured')

    def mark_business_rule_complete(self, rule, br_status: bool):
        """Update business rule status depending upon BR execution status,
            and collect the output in collection of business rule status"""
        self.collect_business_rule_status.append(br_status)
        status = 'Success' if br_status else 'Error Occured'
        self.metadata.update_business_rule(rule, status)

    def prepare_create_query(self, select_query_str: str, prefix='', suffix='') -> str:
        output_table_name = prefix + self.metadata.get_output_table_name(self.scenario_rule_instance) + suffix
        schema_name = brms_config['redshift']['schema_name']
        # query wrapper
        query = """ CREATE TABLE %s.%s AS %s """ % (schema_name, output_table_name, select_query_str)
        return query

    def construct_schema(self, column_details):
        schema = '('
        for index, (column_name, column_data_type) in column_details.iterrows():
            data_type = Redshift.data_type_mapper.get(column_data_type.lower(), column_data_type.lower())
            schema += column_name + ' ' + data_type + ','
        schema = schema[:-1] + ')'
        return schema

    @abstractmethod
    def is_valid(self) -> bool:
        pass

    @abstractmethod
    def is_prepared(self) -> bool:
        pass

    @abstractmethod
    def execute_business_rule(self, index, rule) -> bool:
        pass

    @abstractmethod
    def cleanup_scenario_rule_instance(self):
        pass

    @abstractmethod
    def make_query(self, br_expression) -> str:
        pass
