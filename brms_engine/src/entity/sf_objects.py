import logging
from src.vendor.salesforce import SalesForce

class SfObjects:
    """
    Class that connects to Salesforce and Retrieves 3 objects: DataObject, DataSet, DataSet Column Details
    It is instantiated in the FileMovement class .
    """
    def __init__(self, dataobject_id:str):
        """
        Queries the DO, DS, DSCD objects based on the DO id it recieves in the constructor.
        :param dataobject_id: The dataobject id to retrieve file details. Using the dataset id, retrieves metadata of file
        from DS and DSCD objects.
        """
        self.dataobject_id = dataobject_id
        do_query = "SELECT dataset_id__c,data_name__c,delimeter__c,error_records_count__c,Error_Table__c," \
                   "Final_Table__c,Id,Name,status__c,Staging_Table__c,success_records__c,Success_Table__c FROM Data_Object__c where Id = '" \
                   + str(self.dataobject_id)+"'"
        logging.info("Executing DataObject Query")
        self.do = SalesForce.query_all(do_query)
        self.dataset_id = self.do["dataset_id__c"].iloc[0]

        ds_query = "SELECT Data_Set_Id__c,destination__c,dst_conn_str__c,dst_path__c,Id,Name,Scenario__c,source__c,src_conn_str__c,src_path__c " \
                   "FROM Data_Set__c where Id = '" + str(self.dataset_id) + "'"
        logging.info("Executing DataSet Query")
        self.ds = SalesForce.query_all(ds_query)

        dscd_query = "SELECT column_id__c,dataset_id__c,datatype__c,Data_Set_Field_Desc__c,ds_col_name__c,Error_String__c,Id,is_mandatory__c," \
                     "Key_Column__c,Name,Table_Data_Type__c,tb_col_nm__c,validation__c,value_type__c,variable_type__c FROM Data_Set_Column_Detail__c " \
                     "where dataset_id__c = '" + str(self.dataset_id)+"'"
        logging.info("Executing DataSet Column DetailsQuery")
        self.dscd = SalesForce.query_all(dscd_query)
        self.file_name = self.do["data_name__c"].iloc[0]
        self.staging_name = self.do["Staging_Table__c"].iloc[0]   # filename to save-as using staging table name


