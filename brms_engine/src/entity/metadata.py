import collections
import logging

import pandas as pd

from collections import OrderedDict
from src.service.config import brms_config
from src.vendor.client import Client
from src.vendor.datastorage import DataStorage


class MetaData:
    """
    Class that handles operations related to metadata from SFDC (fetching, updating SFDC data) for the scenario being executed.

    SFDC objects handled in metadata include : Scenario, Scenario Rule Instance, Dataset Rule Map,
    Scenario Data Object Map, Data Object, Dataset, Dataset Column Details, Business Rule,
    BR Expression, BR Join Rule.
    """

    def __init__(self, scenario_id: object, client: Client):
        """
        Initializes scenario_id, mode and summary (data frame that stores)
        :param scenario_id: SFDC Id of the scenario
        :param mode: True - enable status updates in SFDC; False otherwise
        """
        self.scenario_id = scenario_id
        self.client = client
        self.summary = collections.OrderedDict()

    @classmethod
    def initialize(cls, scenario_id: str, client: Client):
        """
        Initializes metadata
        :param client:
        :param scenario_id: SFDC id for scenario
        :return: metadata for the given scenario
        """
        _metadata = cls(scenario_id, client)
        _metadata._initialize_from_s3()
        return _metadata

    def internal_tables(self) -> pd.Series:
        """

        :return: dataframe of all the internal tables for the given scenario
        """
        drm = self.dataset_rule_map[['dataset_id__c', 'table_name__c']].drop_duplicates().reset_index(drop=True)
        dataset = self.dataset[self.dataset['is_internal__c'] == True]
        merged_df = pd.merge(dataset, drm, left_on='Id', right_on='dataset_id__c')
        return merged_df['table_name__c']

    # ------------------------------------------------------------------------------------------------------------------
    # Operations based methods

    def update_scenario_flag(self, status: str):
        """
        Updates Request_Process_Stage__c in Scenario object in SFDC if Status updates are enabled
        :param status: Success or Error Occurred
        """
        if self.client:
            self.client.update_scenario(self.scenario_id, status)

    def update_scenario_rule_instance(self, scenario_rule_instance: pd.Series, status: str):
        """
        Updates Status__c in Scenario Rule Instance object in SFDC if Status updates are enabled
        :param scenario_rule_instance: scenario rule instance record
        :param status: Success or Error Occurred
        """
        if self.client:
            self.client.update_scenario_rule_instance(scenario_rule_instance['Id'], status)

    def update_business_rule(self, rule: pd.Series, status: str):
        """
        Updates Status__c in Business Rule object in SFDC if Status updates are enabled
        :param rule: Business Rule record
        :param status: Success or Error Occurred
        """
        if self.client:
            self.client.update_business_rule(rule['Id'], status)

    # ------------------------------------------------------------------------------------------------------------------
    # Pandas based operations

    def get_component_name(self, scenario_rule_instance):
        return eval(scenario_rule_instance['Component_Type_Master__r'])['Name']

    def get_input_dataset(self, scenario_rule_instance, index=0) -> pd.Series:
        return self._get_dataset(scenario_rule_instance, 'I', index)

    def get_output_dataset(self, scenario_rule_instance, index=0) -> pd.Series:
        return self._get_dataset(scenario_rule_instance, 'O', index)

    def get_input_table_name(self, scenario_rule_instance, index=0, with_schema=False) -> str:
        return self._get_table_name(scenario_rule_instance, 'I', index, with_schema)

    def get_output_table_name(self, scenario_rule_instance, index=0, with_schema=False) -> str:
        return self._get_table_name(scenario_rule_instance, 'O', index, with_schema)

    def is_input_table_internal(self, scenario_rule_instance, index=0) -> bool:
        return self._is_table_internal(scenario_rule_instance, 'I', index)

    def is_output_table_internal(self, scenario_rule_instance, index=0) -> bool:
        return self._is_table_internal(scenario_rule_instance, 'O', index)

    def get_business_rules(self, scenario_rule_instance: pd.Series):
        return self.business_rules[
            self.business_rules['ScenarioRuleInstanceDetails__c'] == scenario_rule_instance['Id']].reset_index(
            drop=True).iterrows()

    def get_child_scenario(self, scenario_rule_instance: pd.Series):
        return self.dataset_rule_map.loc[
            (self.dataset_rule_map['scenario_rule_instance_id__c'] == scenario_rule_instance[
                'Id']), 'Child_Scenario__c'].values[0]

    # ------------------------------------------------------------------------------------------------------------------

    def get_column_details(self, dataset: pd.Series):
        return self.dataset_column_detail.loc[
            self.dataset_column_detail['dataset_id__c'] == dataset['Id'], ['tb_col_nm__c',
                                                                           'datatype__c']].reset_index(drop=True)

    def get_br_expression(self, rule) -> pd.DataFrame:
        rule_id = rule['Id']
        return self.business_rule_expressions[
            self.business_rule_expressions['Business_Rules__c'] == rule_id].reset_index(drop=True)

    def get_join_br_expression(self, rule) -> pd.DataFrame:
        rule_id = rule['Id']
        return self.br_join_rule[self.br_join_rule['Business_Rules__c'] == rule_id].reset_index(drop=True)

    # ------------------------------------------------------------------------------------------------------------------
    def _get_dataset(self, scenario_rule_instance, type, index) -> pd.Series:
        dataset_id = self.dataset_rule_map.loc[
            (self.dataset_rule_map['scenario_rule_instance_id__c'] == scenario_rule_instance['Id']) & (
                self.dataset_rule_map['ds_type__c'] == type), 'dataset_id__c'].values[index]
        return self.dataset[self.dataset['Id'] == dataset_id].reset_index(drop=True).iloc[0, :]

    def _get_table_name(self, scenario_rule_instance, type: str, index: int, with_schema: bool) -> str:
        """

        :param scenario_rule_instance: scenario rule instance
        :param type: internal or external
        :param index: index
        :param with_schema: table name prefixed with schema name in Redshift
        :return: table name
        """
        table_name = self.dataset_rule_map.loc[
            (self.dataset_rule_map['scenario_rule_instance_id__c'] == scenario_rule_instance['Id']) & (
                self.dataset_rule_map['ds_type__c'] == type), 'table_name__c'].reset_index(drop=True).values[index]
        if with_schema:
            return brms_config['redshift']['schema_name'] + '.' + table_name
        return table_name

    def _is_table_internal(self, scenario_rule_instance, type, index) -> bool:
        dataset = self._get_dataset(scenario_rule_instance, type, index)
        return dataset['is_internal__c']

    def _initialize_from_s3(self):
        """
        initializes metadata from s3
        """
        prefix = brms_config['metadata_param']['data_sync_path'].format(scenario_id=self.scenario_id)

        # Meta data - primary & secondary
        self.scenario = DataStorage.download_file(prefix + 'scenario.csv')

        self.scenario_rule_instance = DataStorage.download_file(prefix + 'scenario_rule_instance.csv')
        self.scenario_rule_instance = self.scenario_rule_instance.sort_values('Exec_Seq__c')

        self.dataset_rule_map = DataStorage.download_file(prefix + 'dataset_rule_map.csv')
        self.scenario_data_object_map = DataStorage.download_file(prefix + 'scenario_data_object_map.csv')

        self.data_objects = DataStorage.download_file(prefix + 'data_objects.csv')

        self.dataset = DataStorage.download_file(prefix + 'dataset.csv')

        self.dataset_column_detail = DataStorage.download_file(prefix + 'dataset_column_detail.csv')

        self.business_rules = DataStorage.download_file(prefix + 'business_rules.csv')
        self.business_rules = self.business_rules.sort_values('Execution_Sequence__c')

        self.business_rule_expressions = DataStorage.download_file(prefix + 'business_rule_expressions.csv')

        self.br_join_rule = DataStorage.download_file(prefix + 'br_join_rule.csv')
