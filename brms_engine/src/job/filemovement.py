import boto3
import logging
import pandas as pd
import dask.dataframe as dd

from src.service.register import register_job
from src.vendor.redshift import Redshift
from src.service.config import brms_config
from src.vendor.salesforce import SalesForce
from src.job.filevalidation import FileValidation
from src.entity.sf_objects import SfObjects


# class S3:
#     """
#     Handles connections to s3 and a common s3_upload function
#     """
#
#     def invoke(self,aws_id_key,aws_sec_key):
#         """
#
#         :param aws_id_key: aws id key
#         :param aws_sec_key: aws secret key
#         :return: returns a boto3 client object
#         """
#
#         s3_client = boto3.client('s3',aws_access_key_id=aws_id_key, aws_secret_access_key=aws_sec_key)
#         return s3_client
#
#     # def s3_upload(self,ds,staging_name):
#     #     """
#     #
#     #     :param ds: salesforce object dataset
#     #     :param staging_name: staging table name. The name of the file that will be created in S3.
#     #     :return:
#     #     """
#     #
#     #     logging.info("Getting Upload credentials")
#     #     aws_id_dest, aws_sec_key_dest, bucket_dest, path_dest = self.get_s3_creds(ds, "dst")  # destination path
#     #
#     #     s3_client = S3.invoke(aws_id_dest, aws_sec_key_dest)
#     #     logging.info("Uploading File")
#     #
#     #     path_dest += staging_name
#     #     s3_client.upload_file(staging_name, bucket_dest,
#     #                           path_dest + staging_name)  # uploading file to s3 with staging table name
#     #     logging.info("File Uploaded!")

@register_job("FileMovement")
class FileMovement:
    """
        Controller class for for data movement and header validations.

        Implements all the functions for data management module. Reads metadata from Salesforce and copies file from source
        to destination. Performs validations on the data in Redshift and dumps the error and success tables to s3.
    """

    def __init__(self, dataobject_id :str):
        """
        Class constructor. Receieves the dataobject id as the starting point for module execution.
        Instantiates Redshift connections and loads the SF objects. Instantiates Redshift class and SfObject class

        :param dataobject_id: sales force generated id for the file details in Data_Object object in SF.
        """
        self.SfObjects = SfObjects(dataobject_id)
        self.df = pd.DataFrame()
        self.FileValidation = FileValidation(self.SfObjects)

    def invoke(self):
        """
        File movement and validation functions are invoked through here

        Detects extension of the file and calls the appropriate copy function. Calls for external table creation,
        error table and drops tables created in redshift after execution.

        """
        logging.info("Invoking Functions...")
        self.file_extension_check()
        ext_table_query = self.prepare_external_table_query()
        error_query = self.prepare_error_table_query()
        try:
            self.create_external_table(ext_table_query)
            self.create_error_table(error_query)
            flag = self.FileValidation.validations()
            if flag:
                self.FileValidation.unload_final_table(self.SfObjects.do["Final_Table__c"].iloc[0])
                self.FileValidation.unload_error_table(self.SfObjects.do["Error_Table__c"].iloc[0])
            status = "Success"
            SalesForce.sf.Data_Object__c.update(self.SfObjects.dataobject_id, {'status__c': status})
        finally:
            self.FileValidation.cleanup()

    def file_extension_check(self):
        if self.SfObjects.file_name.split(".")[-1] == "gz":
            logging.info("GZIP extension detected.")
            self.s3_copy_gz()
        else:
            self.s3_copy_csv()
            logging.info("CSV extension detected.")

    def s3_copy_gz(self):
        """
        Copies GZIP file from source to destination. Doesn't change the delimiter. Creates two connections using boto3
        and uses the get_s3_creds method internally.
        """
        logging.info("Initiating file transfer.")
        aws_id_s,aws_sec_key_s,bucket_s,path_s = self.get_s3_creds(self.SfObjects.ds,"src")
        aws_id_d,aws_sec_key_d,bucket_d,path_d = self.get_s3_creds(self.SfObjects.ds,"dst")
        s3_s = boto3.resource('s3', aws_access_key_id=aws_id_s, aws_secret_access_key=aws_sec_key_s)
        s3_d = boto3.resource('s3', aws_access_key_id=aws_id_d, aws_secret_access_key=aws_sec_key_d)
        bucket_object_d = s3_d.Bucket(bucket_d)
        copy_source = {
            'Bucket': bucket_s,
            'Key': path_s+self.SfObjects.file_name
        }
        bucket_object_d.copy(copy_source, path_d+self.SfObjects.staging_name+"/"+self.SfObjects.staging_name + "." + self.SfObjects.file_name.split(".")[-1])
        logging.info("File copied.")

    def s3_copy_csv(self):
        """
        Copies csv file from source to destination after compressing it to GZIP. Uses Dask and sets the delimeter to pipe (|).
        Uses get_s3_cred method internally.

        """
        logging.info("Initiating file compression and transfer.")
        aws_id_s, aws_sec_key_s, bucket_s, path_s = self.get_s3_creds(self.SfObjects.ds, "src")
        aws_id_d, aws_sec_key_d, bucket_d, path_d = self.get_s3_creds(self.SfObjects.ds, "dst")
        df = dd.read_csv('s3://' + bucket_s + "/" + path_s + self.SfObjects.file_name, sep = self.SfObjects.do["delimeter__c"].iloc[0],
                         storage_options={'use_ssl': False, 'key': aws_id_s,
                                          'secret': aws_sec_key_s},dtype=object
                         )
        logging.info ("converting to GZIP")
        df.to_csv('s3://' + bucket_d + "/" + path_d + self.SfObjects.staging_name + "/" + self.SfObjects.staging_name + "_*.gz" ,
                  sep='|',index = False,
                  storage_options={'use_ssl': False, 'key': aws_id_d,
                                   'secret': aws_sec_key_d},compression = 'gzip'
                  )
        logging.info("File copied.")

    # def column_validate(self,staging_name,dscd):                                                                                      #validate column count and names
    #     """
    #     checks for column count and column names (basic validation) on the server itself.
    #     :param staging_name: the staging table name. the name of the file that is created on the server after s3 download.
    #     :param dscd: salesforce object dataset column details. contains metadata of the columns of the file to be validated.
    #     :return: returns 1 of validations passed. Else 0.
    #     """
    #
    #     df = open(staging_name,"r")
    #     cols = dscd["tb_col_nm__c"].tolist()
    #     file_cols = df.readline().split("\"")[1::2]
    #     df.close()
    #     file_cols = [i.replace("__c","").lower() for i in file_cols]
    #     col_match = c.Counter(cols) == c.Counter(file_cols)                                                     #set removes duplicates so counter used
    #     if col_match:
    #         return 1
    #     else:
    #         return 0

    def get_s3_creds(self, ds, pos):                                                                             #df to be passed is ds
        """
        Extracts necessary s3 credentials and file attributes to connect to s3
        :param ds: dataset object in SF
        :param pos: Indicates which s3 credentials to receive, source or destination. Use 'src' for source and
        'dst' for destination
        :return: returns aws key, aws secret key, bucket where the file is, and the path to the file relative
        to the bucket
        >>> aws_id,aws_sec_key,bucket,path = self.get_s3_creds(self.SfObjects.ds,"src")
        """
        cred = ds[pos + "_conn_str__c"].iloc[0].split("'")                                                       #extracting source credentials for s3
        aws_id = cred[1]                                                                                         #aws access id for source
        aws_sec_key = cred[3]                                                                                    #aws secret key for source
        path = ds[pos + "_path__c"].iloc[0].split("//")                                                          #source path of filein s3
        a = path[1].split("/")
        bucket = a[0]                                                                                            #bucket name
        path = "/".join(a[1:])

        return aws_id,aws_sec_key,bucket,path

    def prepare_external_table_query(self):
        """
        Prepares the query to create external table in redshift .All columns dtypes are varchar.
        :return: returns the query for external table creation.
        """
        logging.info("Generating external table query.")
        column_details = self.SfObjects.dscd.loc[self.SfObjects.dscd["dataset_id__c"] == self.SfObjects.dataset_id, ["Table_Data_Type__c", "tb_col_nm__c"]]
        schema = '('
        for _, (Table_Data_Type__c, tb_col_nm__c) in column_details.iterrows():
            data_type = Table_Data_Type__c if Table_Data_Type__c != 'text' else 'varchar'
            schema += tb_col_nm__c + ' ' + data_type + ','
        schema = schema[:-1] + ')'
        _ , _, bucket_d, path_d = self.get_s3_creds(self.SfObjects.ds,"dst")
        s3_location = "s3://" + bucket_d + "/" + path_d + self.SfObjects.staging_name
        query = ("create external table {}.{}.{} {} \n"
                 "                ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' \n"
                 "                stored as textfile location '{}'").format(brms_config['redshift']['dbname'],
                                                                            brms_config['redshift']['external_schema_name'],
                                                                            self.SfObjects.staging_name, schema, s3_location)
        logging.info("Create external table query : " + query)
        return query

    def prepare_error_table_query(self):
        """
        Prepares query for error table in redshift
        :return: query for error table
        """
        logging.info("Generating error table query.")
        column_details = self.SfObjects.dscd.loc[self.SfObjects.dscd["dataset_id__c"] == self.SfObjects.dataset_id, ["Table_Data_Type__c", "tb_col_nm__c"]]
        schema = '('
        for _, (Table_Data_Type__c, tb_col_nm__c) in column_details.iterrows():
            data_type = Table_Data_Type__c if Table_Data_Type__c != 'text' else 'varchar'
            schema += tb_col_nm__c + ' ' + data_type + ','
        schema = schema[:-1] + ')'
        query = "create table {} {} ".format(self.SfObjects.do["Error_Table__c"].iloc[0], schema)
        logging.info("Error table query : " + query)
        return query

    @staticmethod
    def create_external_table(query):
        """
        creates the external table in redshift
        :param query: the external table query
        """
        logging.info ('Creating external table.')
        Redshift.execute_query(query)

    @staticmethod
    def create_error_table(query):
        """
        creates error tablei in redshift
        :param query: query for error table
        """
        logging.info("Creating error table")
        Redshift.execute_query(query)


    # def file_download_sftp(self,ds,file_name,staging_name):
    #     """
    #     downloads file from s3 to server
    #     :param ds: dataset object. contains the source path of the file on sftp.
    #     :param file_name: the name of the file on sftp
    #     :param staging_name: the name to be given to the file to save on the server.
    #     :return:
    #     """
    #     logging.info("SFTP connection initiated")
    #     import pysftp  # wrapper library of paramiko
    #     cnopts = pysftp.CnOpts()
    #     cnopts.hostkeys = None
    #     hostname = 'smft.axtria.com'  # Make connection to SFTP
    #     sftp_username = 'Manas.Jain'
    #     sftp_pw = 'MJsep@2017'
    #     sftp = pysftp.Connection(hostname, username=sftp_username, password=sftp_pw, cnopts=cnopts)
    #     path = ds["src_path__c"].iloc[0].split("//")[1]
    #     if sftp.isfile(path + file_name):  # if TRUE (file exists)
    #         _ = sftp.get(path + file_name, staging_name + ".csv")
    #     sftp.close()

    # def file_download_s3(self,ds,file_name,staging_name):
    #     """
    #     downloads file from s3 . contains source of file
    #     :param ds: dataset object. contains source path of file on s3 and the credentials to connect to s3
    #     :param file_name: name of the file on s3
    #     :param staging_name: name to be given to file on the server.
    #     :return:
    #     """
    #
    #     logging.info("Extracting source info")
    #     aws_id_src, aws_sec_key_src, bucket_src, path_src = self.get_s3_creds(ds,"src")                   # path of file from bucket (called key)
    #     path_src += file_name
    #     path_src = "BRMS/DEV/client_loc/DM/Input/Accounts.csv"
    #
    #     # -------------------------------------  S3 connection and download  ----------------------------------------------------------------------------
    #
    #     s3_client = S3.invoke(aws_id_src, aws_sec_key_src)
    #     logging.info("S3 connection Established")
    #     logging.info("Downloading File...")
    #     s3_client.download_file(bucket_src, path_src, staging_name)  # downloading file to server
    #     logging.info("File Downloaded!")
    #     status = "In Progress"
    #     logging.info("Updating Status")
    #     SalesForce.Data_Object__c.update(self.dataobject_id,{'status__c': status})                           # updating status corresponding to the id of the file
    #     logging.info("Status Updated")
    #
    #     # path = "BRMS/DEV/client_loc/DM/Input/Accounts.csv"

    # def file_movement(self):
    #     """
    #     controller function that calls the other functions after executing slesforce queries to retrieve dataset,
    #     dataobject and dataset column details onjects.
    #     :return:
    #     """
    #
    #     # ------------------------------------- File Movement  -----------------------------------------------------------------------
    #
    #     if self.sf_objects.ds["source__c"].iloc[0] == "S3":                                                                         #checking source of file
    #         self.file_download_s3(self.sf_objects.ds,self.sf_objects.file_name,self.sf_objects.staging_name)
    #     else:
    #         self.file_download_sftp(self.sf_objects.ds,self.sf_objects.file_name,self.sf_objects.staging_name)
    #
    #     # ------------------------------------- Header Validation  ----------------------------------------------------------------------------
    #
    #     a = self.column_validate(self.sf_objects.staging_name, self.sf_objects.dscd)
    #     if a == 1:
    #         logging.info("Column count and names match")
    #         S3.s3_upload(self.sf_objects.ds, self.sf_objects.staging_name)
    #         return 1
    #     else:
    #         logging.info("Column Validations failed")
    #         status = ("Column Validation Error")
    #         SalesForce.sf.Data_Object__c.update(self.sf_objects.dataobject_id, {'status__c': status})
    #         return 0

# if __name__ == "__main__":
#     fm = FileMovement("a1cf4000000Yb2RAAS")
#     fm.invoke()
