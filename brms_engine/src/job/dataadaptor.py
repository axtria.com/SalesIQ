import logging

from src.service.config import brms_config
from src.service.register import register_job, REGISTERED_PARSERS
from src.vendor.client import Client
from src.vendor.datastorage import DataStorage


@register_job("DataAdaptor")
class DataAdaptor:
    def __init__(self, client_name: str, scenario_id: str):
        self.scenario_id = scenario_id
        self.client = Client(client_name)
        self.parser = REGISTERED_PARSERS[client_name](scenario_id, self.client)

    def invoke(self):
        self.parser.get_metadata_from_sfdc()
        self.parser.make_scenario()
        self.parser.make_scenario_rule_instance()
        self.parser.make_dataset_rule_map()
        self.parser.make_dataset()
        self.parser.make_dataset_column_details()
        self.parser.make_business_rules()
        self.parser.make_br_join_rule()
        self.parser.make_business_rules_expression()
        self.sync_to_s3()

    def sync_to_s3(self):
        sync_path = brms_config['metadata_param']['data_sync_path'].format(scenario_id=self.scenario_id)
        sfdc_objects = ['scenario', 'scenario_rule_instance', 'dataset_rule_map', 'dataset', 'dataset_column_details',
                        'business_rules', 'br_join_rule', 'business_rules_expression']
        for sfdc_object in sfdc_objects:
            logging.info('Sync %s ...' % sfdc_object)
            remote_path = sync_path + sfdc_object + '.csv'
            DataStorage.upload_file(getattr(self.parser, sfdc_object), remote_path)
