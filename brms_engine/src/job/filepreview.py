from src.service.register import register_job
import boto3
from src.service.config import brms_config
from src.service.helpers import decoder
import pandas as pd
from time import sleep


@register_job("FilePreview")
class FilePreview:
    # athena client
    athena = boto3.client('athena', region_name=brms_config['aws']['region_name'],
                          aws_access_key_id=brms_config['aws']['access_key'],
                          aws_secret_access_key=decoder(brms_config['aws']['secret_key']))

    paginator = athena.get_paginator('get_query_results')

    s3_prefix = 's3://salesiq-mapserver/BRMS/DEV/RE/'
    database = 'testSalesIQ'
    output_location = 's3://salesiq-mapserver/BRMS/BIUS/'

    def __init__(self, scenario_id, table_name, filter_rows, filter_columns, order_by, page_size, offset, format,
                 index):
        # maintain history of data frames viewed - list
        self.history = []
        # construct s3 path for table
        location = self.generate_path(scenario_id, table_name)
        # create table in database
        self.create_table(table_name, location)
        # construct query: select clause, where clause, order by clause
        query = self.generate_query(table_name, filter_columns, filter_rows, order_by)
        # execute query
        query_id = (self.execute_query(query))['QueryExecutionId']
        # query_id = response['QueryExecutionId']
        # while loop to check if query execution complete
        status = self.query_waiter(query_id, table_name)
        # create paginator - page size, offset, max items?? - iterator
        if status == 'SUCCEEDED':
            self.paginator = self.create_paginator(query_id, page_size, offset, index)
        # drop table from athena
        self.drop_table(table_name)

    def invoke(self, index):
        if index < len(self.history):
            return self.send_result(self.history[index])
        elif index == len(self.history):
            return self.send_result(self.next_function(self.paginator))
        else:
            # random access possible:
            while index > len(self.history):
                self.next_function(self.paginator)
            return self.send_result(self.next_function(self.paginator))

    def generate_path(self, scenario_id, table_name):
        # TODO: different paths for data management files and scenario execution files
        return FilePreview.s3_prefix + scenario_id + '/' + table_name + '/'

    def create_table(self, table_name, location):
        query = "CREATE EXTERNAL TABLE testSalesIQ." + table_name \
                + "("  # + TODO: get Schema + \
        ") ROW FORMAT DELIMITED FIELDS TERMINATED BY '|' LINES TERMINATED BY '\n' \
        LOCATION " + location + ";"

        self.execute_query(query)

    def drop_table(self, table_name):
        query = 'DROP TABLE ' + FilePreview.database + '.' + table_name
        self.execute_query(query)

    def generate_query(self, table_name, filter_columns, filter_rows, order_by):
        query = 'SELECT '

        if filter_columns.isspace():
            query += filter_columns
        else:
            query += ' * '

        query += ' FROM ' + table_name

        if filter_rows.isspace():
            query += ' WHERE ' + filter_rows  # filter conditions containing quotes should be escaped

        if order_by.isspace():
            query += ' ORDER BY ' + order_by

        return query

    def execute_query(self, query):
        response = FilePreview.athena.start_query_execution(
            QueryString=query,
            QueryExecutionContext={'Database': FilePreview.database},
            ResultConfiguration={'OutputLocation': FilePreview.output_location})
        return response

    def create_paginator(self, query_id, page_size, offset):
        # check if can be paginated
        # else return error
        response_paginator = FilePreview.paginator.paginate(
            QueryExecutionId=query_id,
            PaginationConfig={
                'PageSize': page_size,
                # TODO: just in case if starting token to be calculated from index, offset and page_size
            }
        )

        # create a generator for paginator
        def generator(paginator):
            for page in paginator:
                data_dict = {}
                for row in (page['ResultSet']['Rows']):
                    for index, column in enumerate(row['Data']):
                        data_dict[index] = data_dict.get(index, [])
                        data_dict[index].append(column['VarCharValue'])
                yield pd.DataFrame(data_dict)

        return generator(response_paginator)

    def next_function(self, paginator):
        data =  next(paginator)
        self.history.append(data)
        return data
        # for page in paginator:
        #     res_dict = {}
        #     for row in (page['ResultSet']['Rows']):
        #         for index, column in enumerate(row['Data']):
        #             res_dict[index] = res_dict.get(index, [])
        #             res_dict[index].append(column['VarCharValue'])
        #     self.history.append(pd.DataFrame(res_dict))
        #     return pd.DataFrame(res_dict)

    def send_result(self, data: pd.DataFrame, format):
        if format == 'csv':
            return data.to_csv()
        elif format == 'json':
            return data.to_json()
        else:
            return 'Invalid Format'

    def query_waiter(self, query_id, table_name):
        sec = 2
        while True:
            response = FilePreview.athena.get_query_execution(QueryExecutionId=query_id)
            status = response['Status']
            if status == 'SUCCEEDED':  # TODO: check other possible status
                # drop table
                self.drop_table(table_name)
                break
            elif status == 'FAILED' | status == 'CANCELLED':
                # TODO:
                break
            elif status == 'QUEUED' | status == 'RUNNING':
                sleep(sec)
                sec += 2
        return status
