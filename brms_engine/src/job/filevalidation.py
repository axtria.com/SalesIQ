import pandas as pd
import logging
import collections as c

from src.vendor.redshift import Redshift
from src.service.config import brms_config
from src.vendor.salesforce import SalesForce


class FileValidation:

    def __init__(self, sfobjects):
        self.SfObjects = sfobjects

    def validations(self):
        """
        Controller function for all validations. Performs Level 1 validation. If L1 check is passed then all L2 validations
        are run. Reads SF objects to retrieve all validations.

        :return: retuns True if L1 checks are passed. False if not. This is used because in case L1 check fails, no entries are
        loaded in the error table, so it is not to be unloaded to s3.
        """
        logging.info("Beginning Validations..")
        logging.info("Running header count and name check...")
        headers = pd.DataFrame(Redshift.return_output("select top 1 * from " + brms_config['redshift']['external_schema_name']+"."+str(self.SfObjects.staging_name))).values[0]
        cols = self.SfObjects.dscd["ds_col_name__c"].tolist()          #actual col_names used in file
        headers = [i.lower() for i in headers]
        cols = [i.lower() for i in cols]
        meta_cols = self.SfObjects.dscd["tb_col_nm__c"].tolist()  #col_names to be used internally
        if c.Counter(headers) == c.Counter(cols):
            logging.info("Header Check Passed")
            is_mand = self.SfObjects.dscd.loc[self.SfObjects.dscd["is_mandatory__c"] == True, ["tb_col_nm__c"]].iloc[:, 0].tolist()
            key_col = self.SfObjects.dscd.loc[self.SfObjects.dscd["Key_Column__c"] == True, ["tb_col_nm__c"]].iloc[:, 0].tolist()
            numeric_cols = self.SfObjects.dscd.loc[self.SfObjects.dscd["datatype__c"] == "Numeric", ["tb_col_nm__c"]].iloc[:, 0].tolist()
            validation_cols = self.SfObjects.dscd.loc[self.SfObjects.dscd["validation__c"].isnull() == False,["validation__c"]].iloc[:, 0].tolist()
            count = self.move_to_redshift()
            count = self.remove_duplicates(meta_cols)
            if len(is_mand) > 0:
                self.is_mandatory(is_mand, count)
            else:
                logging.info("No mandatory columns. Check not required.")
            if len(key_col)>0:
                 self.key_column(key_col, count)
            else:
                 logging.info("No key columns found. Check not required")
            if len(numeric_cols) > 0:
                 self.check_numeric(numeric_cols, count)
            else:
                 logging.info("No numeric columns. Data Type check not required.")
            if len(validation_cols)>0:
                self.col_validation(validation_cols, count)
            else:
                logging.info("No column validations to run.")
            self.create_final_table(meta_cols)
            return True

        else:
            logging.info("Header check failed.")
            status = "Column Validation Error"
            SalesForce.sf.Data_Object__c.update(self.SfObjects.dataobject_id, {'status__c': status})
            return False

    def move_to_redshift(self):
        """
        Creates the query to create a table in redshift from the external table(in effect moving the file the external
        table refers to, to redshift). Also executes the query.
        :return: returns a count of 1 to indicate this is the first (public schema) table created in redshift.
        This to ensure that order of queries can be easily changed.
        """
        logging.info("Moving data to redshift")
        query = "create table {0}_1 as select * from {1}.{2} except select top 1 * from {1}.{2} ".format(str(self.SfObjects.staging_name),
                                                                                                         brms_config['redshift']['external_schema_name'],
                                                                                                         str(self.SfObjects.staging_name))
        Redshift.execute_query(query)
        logging.info("Moving Query : " + query)
        return 1

    def remove_duplicates(self, meta_cols):
        """
        Creates a redshift table without any duplicate records from the previous table.
        :param meta_cols: meta_cols is the list of all columns in the redshift table previously created.
        :return: returns 2 to indicate a second table has been created in redshift
        """
        logging.info("Removing duplicates...")
        query = "create table {0}_2 as select *  from  {0}_1 group by ".format(str(self.SfObjects.staging_name))
        for col_name in meta_cols:
            query += col_name + ','
        query = query[:-1]
        Redshift.execute_query(query)
        logging.info("Removing duplicates Query : " + query)
        return 2

    def is_mandatory(self, is_mand, count):
        """
        Runs the mandatory column check on the columns marked mandatory in Dataset column Details SF object
        Checks if those columns contain any null or blank values. Moves those bad records to the error table.
        :param is_mand: list of mandatory columns to perform validations on
        :param count: This is used to perform the query on the latest table. In case we decide to skip 'remove
        duplicate cols' a count of 1 will be passed here.
        """
        logging.info("Validating mandatory columns...")
        query = "select count(*) from {0}_{1} where ".format(str(self.SfObjects.staging_name), str(count))
        for col_name in is_mand:
            query += col_name + " is null or trim(" + col_name + ") = '' or "
        query = query[:-4]
        logging.info("Mandatory errors Query: " + query)
        is_mand_error_count = pd.DataFrame(Redshift.return_output(query)).squeeze()
        if is_mand_error_count > 0:
            logging.info("Mandatory Column violations found.")
            query = "insert into {0} select * from {1}_{2} where ".format(str(self.SfObjects.do["Error_Table__c"].iloc[0]),
                                                                          str(self.SfObjects.staging_name),
                                                                          str(count))
            for col_name in is_mand:
                query += col_name + " is null or trim(" + col_name + ") = '' or "
            query = query[:-4]
            logging.info("Mandatory Column Check Query : " + query)
            Redshift.execute_query(query)
        else:
            logging.info("No mandatory column violations found.")

    def key_column(self, key_col, count):
        """
        Runs the key_column check on the columns marked in DataSet Column Details object in SF.
        Checks for any vioaltions and inserts any bad records found into the error table.
        :param key_col: list of key columns to perform validations on
        :param count: Tells count of previous table created on which the query is to be performed.
        Dynamically updates the query. Useful in case we decide to skip any previous validations that create a new table.

        """
        logging.info("Validating  check...")
        query = "select count(*) from ( select count(*) from {0}_{1} group by ".format(str(self.SfObjects.staging_name), str(count))
        for col_name in key_col:
            query += col_name + ','
        query = query[:-1] + " having count(*) > 1 ) "
        logging.info("Key column violation count Query : " + query)
        df = pd.DataFrame(Redshift.return_output(query))
        print(df)
        is_key_col_error_count = df.squeeze()
        if is_key_col_error_count > 0:
            logging.info("Key Column violations found")
            query = "insert into {} select * from {}_{} where ( ".format(str(self.SfObjects.do["Error_Table__c"].iloc[0]),
                                                                         str(self.SfObjects.staging_name),
                                                                         str(count))
            for col_name in key_col:
                query+=col_name + ','
            query = query[:-1] + " ) in ( select "
            for col_name in key_col:
                query += col_name + ','
            query = query[:-1] + " from " + self.SfObjects.staging_name+"_"+str(count) + " group by "
            for col_name in key_col:
                query += col_name + ','
            query = query[:-1] + " having count(*) > 1 )"
            logging.info("Key Column Check Query : " + query)
            Redshift.execute_query(query)
        else:
            logging.info("No key column violations found")

    def check_numeric(self, numeric_cols, count):
        """
        Checks for datatype violations. Only columns with numeric dtype in metadata are checked using Regular expression.
        Regexp checks for int as well as decimal points. In later sprints, regex will depend on whether decimal points to
        be allowed or not by referencing the metadata. Moves any bad records to the error table.
        :param numeric_cols: list of numeric columnss
        :param count: count of the latest table created. Same use as above.
        """
        logging.info("running datatype check on numeric columns")
        query = "insert into {0} select * from  {1}_{2} where ".format(str(self.SfObjects.do["Error_Table__c"].iloc[0]),
                                                                       str(self.SfObjects.staging_name), str(count))
        for col_name in numeric_cols:
            query += col_name + " not SIMILAR TO '[0-9]*(\.[0-9]*)?' or "
        query = query[:-4]
        logging.info("Numeric dtype check Query : " + query)
        Redshift.execute_query(query)

    def col_validation(self, validation_cols, count):
        """
        Performs any column specific Validations. Inserts bad records to the error table.
        :param validation_cols: list of cols with additional validations to check
        :param count: count of the last table created. Same use as above
        """
        query = "insert into {0} select * from  {1}_{2} where ".format(str(self.SfObjects.do["Error_Table__c"].iloc[0]),
                                                                       str(self.SfObjects.staging_name), str(count))
        for validation in validation_cols:
            query += validation + " and "
        query = query[:-5]
        logging.info("Validation check Query : " + query)
        Redshift.execute_query(query)

    def create_final_table(self, cols):
        """
        Creates a final table in redshift without any error records. Subtracts all the error records in error table from
        the initial table loaded in redshift.
        :param cols: list of all columns
        """
        logging.info("Creating final table")
        final_table_name = self.SfObjects.do["Final_Table__c"].iloc[0]
        logging.info(final_table_name)
        query = "create table {} as select ".format(final_table_name)
        for col_name in cols:
            query += col_name + ','
        query = query[:-1] + " from {} minus select ".format(self.SfObjects.staging_name + "_2")
        for col_name in cols:
            query += col_name + ','
        query = query[:-1] + " from " + self.SfObjects.do["Error_Table__c"].iloc[0]
        logging.info(query)
        Redshift.execute_query(query)

    @staticmethod
    def unload_final_table(final_table_name):
        """
        Unloads the final table to s3
        :param final_table_name: the name of the final table to be used to unload. The file created will also have the same name.

        """
        logging.info("Unloading final table")
        s3_location = 's3://salesiq-mapserver/BRMS/DEV/DM/Input/' + final_table_name + '/{}'.format(final_table_name)
        Redshift.unload_table(final_table_name, brms_config['redshift']['schema_name'], s3_location, "ON")

    @staticmethod
    def unload_error_table(error_table_name):
        """
        Unloads the error table to s3
        :param error_table_name: error table name
        """
        logging.info("Unloading error table")
        s3_location = 's3://salesiq-mapserver/BRMS/DEV/DM/Input/' + error_table_name + '/{}'.format(error_table_name)
        Redshift.unload_table(error_table_name, brms_config['redshift']['schema_name'], s3_location)

    def cleanup(self):
        """
        Drops all tables created in redshift. called through 'finally' method.
        :return:
        """
        _ = Redshift.drop_table(self.SfObjects.staging_name, brms_config['redshift']['external_schema_name'])
        logging.info("Dropped external table")
        _ = Redshift.drop_table(brms_config['redshift']['schema_name'], self.SfObjects.do["Error_Table__c"].iloc[0])
        logging.info("Dropped error table")
        _ = Redshift.drop_table(self.SfObjects.staging_name + "_1", brms_config['redshift']['schema_name'])
        logging.info("Dropped redshift table")
        _ = Redshift.drop_table(self.SfObjects.staging_name + "_2", brms_config['redshift']['schema_name'])
        logging.info("Dropped filtered table")
        _ = Redshift.drop_table(self.SfObjects.do["Final_Table__c"].iloc[0], brms_config['redshift']['schema_name'])
        logging.info("Dropped final table")
