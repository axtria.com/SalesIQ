import logging

import pandas as pd

from src.service.config import brms_config
from src.service.helpers import deprecated
from src.service.register import register_job
from src.vendor.datastorage import DataStorage
from src.vendor.salesforce import SalesForce


@register_job("DataSync")
class DataSync:
    def __init__(self, scenario_id):
        self.scenario_id = scenario_id

    def invoke(self):
        self.get_metadata_from_sfdc()
        self.sync_to_s3()

    def get_metadata_from_sfdc(self):
        self._set_primary_objects()
        self.scenario_instance_details_str = "'" + "','".join(self.scenario_rule_instance['Id']) + "'"
        self._set_secondary_objects()

    def sync_to_s3(self):
        for primary_sfdc_object in brms_config['metadata_primary']:
            self._sync_sfdc_object_to_s3(primary_sfdc_object)
        for sec_sfdc_object in brms_config['metadata_secondary']:
            self._sync_sfdc_object_to_s3(sec_sfdc_object)

    def get_data_sync_path(self):
        return brms_config['metadata_param']['data_sync_path'].format(scenario_id=self.scenario_id)

    def _set_secondary_objects(self):
        # Secondary objects
        for secondary_sfdc_object in brms_config['metadata_secondary']:
            setattr(self, secondary_sfdc_object, pd.DataFrame(
                SalesForce.query(
                    brms_config['metadata_secondary'][secondary_sfdc_object].format(
                        scenario_id=self.scenario_id, scenario_instance_details=self.scenario_instance_details_str))))

    def _set_primary_objects(self):
        # Primary objects
        for sfdc_object in brms_config['metadata_primary']:
            setattr(self, sfdc_object,
                    pd.DataFrame(
                        SalesForce.query(
                            brms_config['metadata_primary'][sfdc_object].format(scenario_id=self.scenario_id))))

    @deprecated
    def _sync_sfdc_object_to_s3(self, sfdc_object):
        logging.info('Sync %s ...' % sfdc_object)
        remote_path = self.get_data_sync_path() + sfdc_object + '.csv'
        DataStorage.upload_file(getattr(self, sfdc_object), remote_path)
