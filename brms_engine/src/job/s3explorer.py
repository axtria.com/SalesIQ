import pandas as pd

from src.service.register import register_job
from src.vendor.datastorage import DataStorage


@register_job("S3Explorer")
class S3Explorer:
    """
    Implements s3 explorer.

    Allows the user to navigate through the keys(objects) present in s3 in the 'prefix' folder in a web page.
    Folders can be explored and files can be previewed as HTML tables
    """

    # to display entire contents in a cell
    pd.set_option('display.max_colwidth', -1)

    def __init__(self, prefix: str):
        """
        Initializes prefix
        :param prefix: the starting point for navigation in s3
        """
        self.prefix = prefix

    def invoke(self):
        """

        :return: HTML string for a list of links to files/folders in the 'prefix' folder
        """

        if self.prefix.endswith('.csv'):
            return DataStorage.download_file(self.prefix).to_html()

        html_str = ''
        for filepath in DataStorage.list_files(self.prefix):
            html_str += '<a href="?prefix=%s">%s</a><br/><br/>' % (filepath, filepath)
        return html_str
