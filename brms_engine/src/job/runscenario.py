import logging
from datetime import datetime

from pandas import DataFrame

from src.entity.metadata import MetaData
from src.service.config import brms_config
from src.service.register import REGISTERED_COMPONENTS, register_job
from src.vendor.client import Client
from src.vendor.datastorage import DataStorage
from src.vendor.notification import Email
from src.vendor.redshift import Redshift


@register_job("RunScenario")
class RunScenario:
    """
    Scenario execution flow implementation

    Defines the complete flow of scenario execution - actual execution, status updates and
    after-execution activities including uploading a summary of execution, cleanup from database(redshift)
    """

    def __init__(self, scenario_id: str, client_name: str = None):
        """
        Initializes metadata and list of status for scenario rule instances in the scenario.

        :param scenario_id: SFDC id of the scenario to be executed
        :param is_sfdc_update_enabled: true - scenario status would be updated in SFDC; false otherwise
        """
        client = Client(client_name) if client_name else None
        self.metadata = MetaData.initialize(scenario_id, client)
        self.collect_scenario_rule_instance_status = []

    def invoke(self):
        """
        Defines the flow for scenario execution - status updates in SFDC ->
        actual execution of a scenario -> unloading from redshift to s3 ->
        uploading the scenario execution summary logs -> cleanup internal tables in redshift ->
        final status updates in SFDC.
        """
        self.update_flags()
        self.execute_scenario()
        self.unload_scenario()
        self.upload_summary()
        self.cleanup_scenario()
        self.mark_scenario_complete()
        self.send_notification()

    def execute_scenario(self):
        """
        Executes scenario rule instances for the scenario ordered by execution order

        Calls the corresponding component class's invoke method for execution of the scenario rule instance
        """
        for index, scenario_rule_instance in self.metadata.scenario_rule_instance.iterrows():
            component_name = self.metadata.get_component_name(scenario_rule_instance)
            logging.debug("(--------------------------- %s ---------------------------)" % component_name)
            component = REGISTERED_COMPONENTS[component_name](scenario_rule_instance, self.metadata)
            component.invoke()
            self.collect_scenario_rule_instance_status.append(component.get_scenario_rule_instance_completion_status())

    def get_scenario_completion_status(self):
        """
        Calculates scenario status based on status of the scenario rule instances stored in
        global list - collect_scenario_rule_instance_status

        :return: True- if each scenario rule instance status is true or if the list is empty; False- otherwise
        """
        return all(self.collect_scenario_rule_instance_status)

    def mark_scenario_complete(self):
        """
        Updates scenario status in SFDC for the executed scenario
        """
        status = 'Success' if self.get_scenario_completion_status() else 'Error Occured'
        self.metadata.update_scenario_flag(status)

    def update_flags(self):
        """
        Updates status in scenario, scenario rule instances and business rules in SFDC to 'In Progress'
        """
        self.metadata.update_scenario_flag('In progress')
        for _, scenario_rule_instance in self.metadata.scenario_rule_instance.iterrows():
            self.metadata.update_scenario_rule_instance(scenario_rule_instance, 'In Progress')
        for _, rule in self.metadata.business_rules.iterrows():
            self.metadata.update_business_rule(rule, 'In Progress')

    def unload_scenario(self):
        """
        Unloads Redshift table to S3 location - {S3-prefix}/{scenario_id}/{table_name}/
        """
        logging.debug('(--------------------------- %s ---------------------------)' % 'Unload Scenario')
        for table in self.metadata.internal_tables():
            s3_location = brms_config['redshift']['s3_location'].format(scenario_id=self.metadata.scenario_id,
                                                                        table_name=table)
            Redshift.unload_table(table, brms_config['redshift']['schema_name'], s3_location)

    def upload_summary(self):
        """
        Generates and uploads summary files for scenario execution to S3 -
        {S3-prefix}/{scenario_id}/summary/{filename}
        """
        logging.debug('(--------------------------- %s ---------------------------)' % 'Upload summary')
        filename = 'summary_execution_%s.csv' % (datetime.now().strftime("%Y-%m-%d_%H%M%S"))
        remote_path = brms_config['metadata_param']['summary_path'].format(
            scenario_id=self.metadata.scenario_id, filename=filename)
        logging.info('summary ::: %r' % self.metadata.summary)
        DataStorage.upload_file(DataFrame(self.metadata.summary).transpose().reset_index(), remote_path)

    def cleanup_scenario(self):
        """
        Drops all internal tables in redshift created for this scenario execution
        """
        logging.debug('(--------------------------- %s ---------------------------)' % 'Cleanup Scenario')
        for table in self.metadata.internal_tables():
            Redshift.drop_table(table, brms_config['redshift']['schema_name'])

    def send_notification(self):
        logging.debug('(--------------------------- %s ---------------------------)' % 'Send Notification')
        Email.send("BRMS: Run summary :: %s :: %s" % (self.metadata.scenario_id, self.get_scenario_completion_status()),
                   DataFrame(self.metadata.summary).transpose().reset_index().to_html())
