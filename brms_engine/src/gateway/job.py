import logging

from src.service.helpers import thread
from src.service.register import REGISTERED_JOBS


class Job:
    def __init__(self):
        self._queue = []

    def build(self, job_name, request):
        self._queue.append(REGISTERED_JOBS[job_name](**request))

    @thread
    def execute_all(self):
        for job in self._queue:
            job.invoke()

    def execute(self):
        try:
            current_job = self._queue.pop()
            return current_job.invoke()
        except IndexError:
            logging.warning("No job found to execute.")
