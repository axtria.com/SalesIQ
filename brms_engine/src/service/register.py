import logging

REGISTERED_COMPONENTS = {}
REGISTERED_JOBS = {}
REGISTERED_PARSERS = {}
REGISTERED_CLIENTS = {}

"""
    A decorator to log and save which python object is being called into a global dict referencing 
    object location in memory.
"""


def register_component(name):
    def wrapper(cls):
        logging.debug("Register component - %s" % name)
        REGISTERED_COMPONENTS[name] = cls
        return cls

    return wrapper


def register_job(name):
    def wrapper(cls):
        logging.debug("Register job - %s" % name)
        REGISTERED_JOBS[name] = cls
        return cls

    return wrapper


def register_parser(name):
    def wrapper(cls):
        logging.debug("Register parser - %s" % name)
        REGISTERED_PARSERS[name] = cls
        return cls

    return wrapper


def register_client(name):
    def wrapper(cls):
        logging.debug("Register client - %s" % name)
        REGISTERED_CLIENTS[name] = cls
        return cls

    return wrapper
