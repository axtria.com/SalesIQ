import logging
from functools import wraps
from time import time, strftime, localtime

from src.service.config import brms_config
from src.vendor.redshift import Redshift


def debug_query(func):
    """function decorator to log query - wraps make_query functions in components"""

    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)

        # Debug query using args
        _metadata = args[0].metadata
        _br_expression = args[1].loc[0, :].to_dict()
        component_name = _metadata.get_component_name(args[0].scenario_rule_instance)
        # _metadata.summary.loc[_br_expression['Business_Rules__c'], 'query'] = result
        # _metadata.summary.loc[_br_expression['Business_Rules__c'], 'component name'] = component_name

        logging.debug("Metadata summary :: logging query ...")
        _metadata.summary[_br_expression['Business_Rules__c']] = {
            **_metadata.summary.get(_br_expression['Business_Rules__c'], {}),
            'query': result,
            'component name': component_name
        }

        return result

    return wrapper


def debug_rule_execution(func):
    """function decorator to log execution of a business rule - wraps execute_business_rule function in components"""

    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time()
        result = func(*args, **kwargs)
        end = time()

        _metadata = args[0].metadata
        _rule_id = args[2]['Id']
        _rule_name = args[2]['Name']

        output_table = _metadata.get_output_table_name(args[0].scenario_rule_instance)

        if hasattr(args[0], 'rule_index'):
            output_table += '_%d' % args[0].rule_index

        schema_name = brms_config['redshift']['schema_name']
        try:
            count_rows = Redshift.return_output("SELECT COUNT(1) FROM %s.%s" % (schema_name, output_table))[0][0]
        except IndexError:
            count_rows = 0

        logging.debug("Metadata summary :: logging execution time ...")
        # _metadata.summary.loc[_rule_id, 'start'] = start
        # _metadata.summary.loc[_rule_id, 'end'] = end
        # _metadata.summary.loc[_rule_id, 'time taken'] = end - start
        # _metadata.summary.loc[_rule_id, 'rule name'] = _rule_name
        # _metadata.summary.loc[_rule_id, 'output table'] = output_table
        # _metadata.summary.loc[_rule_id, 'rows'] = count_rows

        _metadata.summary[_rule_id] = {
            **_metadata.summary.get(_rule_id, {}),
            'start time': strftime('%Y-%m-%d %H:%M:%S', localtime(start)),
            'end time': strftime('%Y-%m-%d %H:%M:%S', localtime(end)),
            'execution time': end - start,
            'rule name': _rule_name,
            'output table': output_table,
            'row count': count_rows
        }

        return result

    return wrapper


def debug_scenario_execution(func):
    """Function decorator to log execution of a scenario rule instance -
        wraps execute_scenario_rule_instance function in components
    """

    @wraps(func)
    def wrapper(*args, **kwargs):

        start = time()
        result = func(*args, **kwargs)
        end = time()
        _metadata = args[0].metadata
        _scenario_rule_instance = args[0].scenario_rule_instance
        _scenario_rule_instance_id = _scenario_rule_instance['Id']
        component_name = _metadata.get_component_name(_scenario_rule_instance)

        output_table = _metadata.get_output_table_name(_scenario_rule_instance)
        schema_name = brms_config['redshift']['schema_name']
        try:
            count_rows = Redshift.return_output("SELECT COUNT(1) FROM %s.%s" % (schema_name, output_table))[0][0]
        except IndexError:
            count_rows = 0

        logging.debug("Metadata summary :: logging execution time ...")
        # _metadata.summary.loc[_scenario_rule_instance_id, 'component name'] = component_name
        # _metadata.summary.loc[_scenario_rule_instance_id, 'start'] = start
        # _metadata.summary.loc[_scenario_rule_instance_id, 'end'] = end
        # _metadata.summary.loc[_scenario_rule_instance_id, 'time taken'] = end - start
        # _metadata.summary.loc[_scenario_rule_instance_id, 'output table'] = output_table
        # _metadata.summary.loc[_scenario_rule_instance_id, 'rows'] = count_rows

        _metadata.summary[_scenario_rule_instance_id] = {
            **_metadata.summary.get(_scenario_rule_instance_id, {}),
            'start time': strftime('%Y-%m-%d %H:%M:%S', localtime(start)),
            'end time': strftime('%Y-%m-%d %H:%M:%S', localtime(end)),
            'execution time': end - start,
            'component name': component_name,
            'output table': output_table,
            'row count': count_rows
        }

        return result

    return wrapper


def component_logger(cls):
    """
    class decorator for a components's class to log its execution statistics

    Iterates through all the items in a class and calls the corresponding
    decorators for functions, as defined above.
    """
    for key, val in vars(cls).items():  # cls.__dict__
        if key == 'make_query':
            setattr(cls, key, debug_query(val))
        elif key == 'execute_business_rule':
            setattr(cls, key, debug_rule_execution(val))
        elif key == 'execute_scenario_rule_instance':
            setattr(cls, key, debug_scenario_execution(val))
    return cls
