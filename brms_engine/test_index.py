import unittest

import sys

test_suite = unittest.TestLoader().discover('test/job/')

# test_suite = unittest.TestSuite()
#
# test_suite.addTests(unittest.makeSuite(TestDataSource))
# test_suite.addTest(unittest.makeSuite(TestFilter))
# test_suite.addTest(unittest.makeSuite(TestJoin))
# test_suite.addTest(unittest.makeSuite(TestAggregate))

unittest.TextTestRunner(verbosity=10).run(test_suite)

