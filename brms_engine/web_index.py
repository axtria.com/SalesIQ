import logging

from flask import Flask, request, render_template

from src.gateway.job import Job

app = Flask(__name__)
PREFIX = '/salesiq_brms'


@app.route(PREFIX + "/file_movement")
def file_movement():
    logging.debug("Triggered file movement controller")
    data_object_id = request.args.get('do_id', default='1', type=str)
    job = Job()
    job.build('FileMovement', {'dataobject_id': data_object_id})
    job.execute_all()
    return " "


@app.route(PREFIX + "/file_preview")
def preview_file():
    scenario_id = request.args.get('scenario_id', default='1', type=str)
    file_name = request.args.get('file_name', default='1', type='str')
    select_query = request.args.get('select_query', default='1', type='str')
    job = Job()
    job.build('FilePreview', {'scenario_id': scenario_id, 'file_name': file_name, 'select_query': select_query})
    job.execute_all()
    return 'JSON/HTML rendered'


@app.route(PREFIX + "/file_download")
def download_file():
    scenario_id = request.args.get('scenario_id', default='1', type=str)
    file_name = request.args.get('file_name', default='1', type='str')
    job = Job()
    job.build('FileDownload', {'scenario_id': scenario_id, 'file_name': file_name})
    job.execute_all()
    return 'JSON/HTML rendered'


@app.route(PREFIX + "/run_scenario")
def run_scenario():
    scenario_id = request.args.get('scenario_id', default='1', type=str)
    client_name = request.args.get('client_name', default=None, type=str)
    job = Job()
    if client_name:
        job.build('DataAdaptor', {'scenario_id': scenario_id, 'client_name': client_name})
    logging.debug("Triggered run scenario %s" % scenario_id)
    job.build('RunScenario', {'scenario_id': scenario_id, 'client_name': client_name})
    job.execute_all()
    return "Running scenario async - %s" % scenario_id


@app.route(PREFIX + "/s3_explorer")
def s3_explorer():
    prefix = request.args.get('prefix', default='BRMS/DEV/RE/', type=str)
    job = Job()
    job.build('S3Explorer', {'prefix': prefix})
    return render_template('s3_explorer.html', context=job.execute())


if __name__ == '__main__':
    app.debug = True
    logging.basicConfig(level=logging.INFO, handlers=[logging.FileHandler("system.log"), logging.StreamHandler()],
                        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    app.run(threaded=True)
