src\.component package
======================

Submodules
----------

src\.component\.aggregate module
--------------------------------

.. automodule:: src.component.aggregate
    :members:
    :undoc-members:
    :show-inheritance:

src\.component\.datasource module
---------------------------------

.. automodule:: src.component.datasource
    :members:
    :undoc-members:
    :show-inheritance:

src\.component\.derivedfield module
-----------------------------------

.. automodule:: src.component.derivedfield
    :members:
    :undoc-members:
    :show-inheritance:

src\.component\.filter module
-----------------------------

.. automodule:: src.component.filter
    :members:
    :undoc-members:
    :show-inheritance:

src\.component\.join module
---------------------------

.. automodule:: src.component.join
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: src.component
    :members:
    :undoc-members:
    :show-inheritance:
