src\.entity package
===================

Submodules
----------

src\.entity\.component module
-----------------------------

.. automodule:: src.entity.component
    :members:
    :undoc-members:
    :show-inheritance:

src\.entity\.metadata module
----------------------------

.. automodule:: src.entity.metadata
    :members:
    :undoc-members:
    :show-inheritance:

src\.entity\.sf\_objects module
-------------------------------

.. automodule:: src.entity.sf_objects
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: src.entity
    :members:
    :undoc-members:
    :show-inheritance:
