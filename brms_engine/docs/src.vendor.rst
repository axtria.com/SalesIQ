src\.vendor package
===================

Submodules
----------

src\.vendor\.datastorage module
-------------------------------

.. automodule:: src.vendor.datastorage
    :members:
    :undoc-members:
    :show-inheritance:

src\.vendor\.redshift module
----------------------------

.. automodule:: src.vendor.redshift
    :members:
    :undoc-members:
    :show-inheritance:

src\.vendor\.salesforce module
------------------------------

.. automodule:: src.vendor.salesforce
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: src.vendor
    :members:
    :undoc-members:
    :show-inheritance:
