from unittest import TestCase
from test import _metadata


class TestMetaData(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.data_source = _metadata.scenario_rule_instance.loc[0, :]
        cls.filter = _metadata.scenario_rule_instance.loc[1, :]
        cls.join = _metadata.scenario_rule_instance.loc[4, :]
        cls.guardrail = _metadata.scenario_rule_instance.loc[9, :]

    def test_get_component_name(self):
        self.assertEqual(_metadata.get_component_name(self.data_source), 'Data Source')
        self.assertEqual(_metadata.get_component_name(self.filter), 'Filter')
        self.assertEqual(_metadata.get_component_name(self.guardrail), 'Derived Field')

    def test_get_input_dataset(self):
        dataset1 = _metadata.get_input_dataset(self.data_source)
        self.assertEqual(dataset1['Name'], 'Account Master')
        dataset2 = _metadata.get_input_dataset(self.filter)
        self.assertEqual(dataset2['Name'], 'Account Master Internal')

    def test_get_output_dataset(self):
        dataset1 = _metadata.get_output_dataset(self.data_source)
        self.assertEqual(dataset1['Name'], 'Account Master Internal')
        dataset2 = _metadata.get_output_dataset(self.filter)
        self.assertEqual(dataset2['Name'], 'Eligible Account Master')

    def test_get_input_table_name(self):
        self.assertEqual(_metadata.get_input_table_name(self.data_source), 't_fin_do50')
        self.assertEqual(_metadata.get_input_table_name(self.filter), 't_acct_mst_scenario_2044')

    def test_get_output_table_name(self):
        self.assertEqual(_metadata.get_output_table_name(self.data_source), 't_acct_mst_scenario_2044')
        self.assertEqual(_metadata.get_output_table_name(self.filter), 't_elig_acct_mst_scenario_2044')

    def test_is_input_table_internal(self):
        self.assertFalse(_metadata.is_input_table_internal(self.data_source))
        self.assertTrue(_metadata.is_input_table_internal(self.filter))

    def test_is_output_table_internal(self):
        self.assertTrue(_metadata.is_output_table_internal(self.data_source))
        self.assertTrue(_metadata.is_output_table_internal(self.filter))

    def test_input_column_details(self):
        dataset1 = _metadata.get_input_dataset(self.data_source)
        column_details1 = _metadata.get_column_details(dataset1)
        self.assertEqual(len(column_details1), 26)
        self.assertEqual(column_details1.loc[0, 'tb_col_nm__c'], 'accountnumber')

        dataset2 = _metadata.get_input_dataset(self.filter)
        column_details2 = _metadata.get_column_details(dataset2)
        self.assertEqual(len(column_details2), 26)
        self.assertEqual(column_details2.loc[0, 'tb_col_nm__c'], 'accountnumber')

    def test_output_column_details(self):
        dataset1 = _metadata.get_output_dataset(self.data_source)
        column_details1 = _metadata.get_column_details(dataset1)
        self.assertEqual(len(column_details1), 26)
        self.assertEqual(column_details1.loc[0, 'tb_col_nm__c'], 'accountnumber')

        dataset2 = _metadata.get_output_dataset(self.filter)
        column_details2 = _metadata.get_column_details(dataset2)
        self.assertEqual(len(column_details2), 26)
        self.assertEqual(column_details2.loc[0, 'tb_col_nm__c'], 'accountnumber')

    def test_business_rules(self):
        for index, rule in _metadata.get_business_rules(self.filter):
            self.assertEqual(index, 0)
            self.assertEqual(rule['Name'], 'Account Filter')

        guardrail_rules = {0: 'Territory Rank', 1: 'Guardrail'}
        for index, rule in _metadata.get_business_rules(self.guardrail):
            self.assertEqual(rule['Name'], guardrail_rules[index])

    def test_br_expression(self):
        for _, rule in _metadata.get_business_rules(self.filter):
            self.assertEqual(_metadata.get_br_expression(rule).loc[0, 'Where_Expr__c'],
                             '(speciality1 is not null or speciality is not null)')

        guardrail_br_expr = {0: 'territory_rank', 1: 'workload_threshold'}
        for index, rule in _metadata.get_business_rules(self.guardrail):
            self.assertEqual(_metadata.get_br_expression(rule).loc[0, 'column_alias__c'], guardrail_br_expr[index])

    def test_join_br_expression(self):
        for _, rule in _metadata.get_business_rules(self.join):
            self.assertEqual(_metadata.get_join_br_expression(rule).loc[0, 'Join_Execute_Expression__c'],
                             't_elig_acct_mst_scenario_2044 inner join t_zip_terr_scenario_2044 on billingpostalcode = '
                             'zip')

    def test_internal_tables(self):
        from pandas import Series
        internal_tables = _metadata.internal_tables()
        self.assertIsInstance(internal_tables, Series)
        self.assertEqual(len(internal_tables), 5)
        self.assertIn('t_acct_mst_scenario_2044', list(internal_tables))
