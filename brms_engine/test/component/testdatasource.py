# from unittest import TestCase
#
# from src.component.datasource import DataSource
# from test import _metadata
#
#
# class TestDataSource(TestCase):
#     @classmethod
#     def setUpClass(cls):
#         scenario_rule_instance = _metadata.scenario_rule_instance.loc[0, :]
#         cls._datasource1 = DataSource(scenario_rule_instance, _metadata)
#         cls._datasource2 = DataSource(_metadata.scenario_rule_instance.loc[3, :], _metadata)
#
#     def test_1_datasource_instance(self):
#         self.assertIsInstance(self._datasource1, DataSource)
#
#     def test_2_datasource_is_valid(self):
#         self.assertTrue(self._datasource1.is_valid())
#
#     def test_3_datasource_is_prepared(self):
#         self.assertTrue(self._datasource1.is_prepared())
#
#     def test_4_datasource_validate_keys(self):
#         src_key, dest_key = self._datasource1.get_keys()
#         self.assertEqual('BRMS/DEV/DM/a06f400000266I0AAI/t_fin_do50/', src_key)
#         self.assertEqual('BRMS/DEV/RE/a06f400000266I0AAI/t_acct_mst_scenario_2044/', dest_key)
#
#     def test_5_datasource_is_valid(self):
#         self.assertTrue(self._datasource2.is_valid())
#
#     def test_6_datasource_is_prepared(self):
#         self.assertTrue(self._datasource2.is_prepared())
#
#     def test_7_datasource_validate_keys(self):
#         src_key, dest_key = self._datasource2.get_keys()
#         self.assertEqual('BRMS/DEV/DM/a06f400000266I0AAI/t_fin_do51/', src_key)
#         self.assertEqual('BRMS/DEV/RE/a06f400000266I0AAI/t_zip_terr_scenario_2044/', dest_key)
