# from unittest import TestCase
#
# from src.component.join import Join
# from test import _metadata
#
#
# class TestJoin(TestCase):
#     @classmethod
#     def setUpClass(cls):
#         scenario_rule_instance = _metadata.scenario_rule_instance.loc[4, :]
#         cls._join = Join(scenario_rule_instance, _metadata)
#
#     @classmethod
#     def tearDownClass(cls):
#         pass
#
#     def test_1_join_instance_creation(self):
#         self.assertIsInstance(self._join, Join)
#
#     def test_2_join_is_valid(self):
#         self.assertTrue(self._join.is_valid())
#
#     def test_3_join_is_prepared(self):
#         """Test scenario rule instance execution - external query"""
#         self.assertTrue(self._join.is_prepared())
#
#     def test_4_join_validate_external_query(self):
#         pass
#         #self.assertEqual(self._filter.prepare_external_query(),
#         #                 "drop table if exists s3.t_acct_mst_scenario_2044 cascade; create external table s3.t_acct_mst_scenario_2044 (accountnumber varchar,accounttype varchar,active varchar,alignment_type varchar,billingaddress varchar,billingcity varchar,billingcountry varchar,billinggeocodeaccuracy varchar,billinglatitude varchar,billinglongitude varchar,billingpostalcode varchar,billingstate varchar,billingstreet varchar,external_account_number varchar,firstname varchar,first_name varchar,id varchar,last_name varchar,metric1 varchar,metric2 varchar,metric3 varchar,name varchar,parentid varchar,speciality1 varchar,speciality varchar,specialty varchar) row format delimited fields terminated by '|' stored as textfile location 's3://salesiq-mapserver/BRMS/DEV/RE/a06f400000266I0AAI/t_acct_mst_scenario_2044/'")
#
#     def test_5_join_execute_business_rule(self):
#         for _, rule in self._join.metadata.get_business_rules(self._join.scenario_rule_instance):
#             self.assertTrue(self._join.execute_business_rule(rule))
