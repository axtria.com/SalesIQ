from unittest import TestCase

from src.job.datasync import DataSync
from src.job.runscenario import RunScenario


class TestRunScenario(TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    # def test_1_scenario_with_sfdc(self):
    #     # ds = DataSync('a06f400000266I0AAI')
    #     # ds.invoke()
    #     rs = RunScenario('a06f400000266I0AAI', is_sfdc_update_enabled=True)
    #     rs.invoke()
    #     self.assertTrue(rs.get_scenario_completion_status())

    def test_2_scenario_with_sfdc(self):
        # ds = DataSync('a06f40000026WcMAAU')
        # ds.invoke()
        rs = RunScenario('ScnCompScn001', is_sfdc_update_enabled=False)
        rs.invoke()
        self.assertTrue(rs.get_scenario_completion_status())
