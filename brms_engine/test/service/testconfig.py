from unittest import TestCase

from src.service.config import brms_config


class TestConfig(TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def test_read_config(self):
        self.assertGreater(len(brms_config), 0)
