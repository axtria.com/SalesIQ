import pandas as pd

from src.entity.metadata import MetaData

_scenario_id = 'a06f400000266I0AAI'
_metadata = MetaData(_scenario_id, False)
_metadata.br_join_rule = pd.read_csv('.//test//fixture//%s//br_join_rule.csv' % _scenario_id)
_metadata.business_rule_expressions = pd.read_csv('.//test//fixture//%s//business_rules_expression.csv' % _scenario_id)
_metadata.business_rules = pd.read_csv('.//test//fixture//%s//business_rules.csv' % _scenario_id)
_metadata.data_objects = pd.read_csv('.//test//fixture//%s//data_objects.csv' % _scenario_id)
_metadata.dataset = pd.read_csv('.//test//fixture//%s//dataset.csv' % _scenario_id)
_metadata.dataset_column_detail = pd.read_csv('.//test//fixture//%s//dataset_column_detail.csv' % _scenario_id)
_metadata.dataset_rule_map = pd.read_csv('.//test//fixture//%s//dataset_rule_map.csv' % _scenario_id)
_metadata.scenario = pd.read_csv('.//test//fixture//%s//scenario.csv' % _scenario_id)
_metadata.scenario_data_object_map = pd.read_csv('.//test//fixture//%s//scenario_data_object_map.csv' % _scenario_id)
_metadata.scenario_rule_instance = pd.read_csv('.//test//fixture//%s//scenario_rule_instance.csv' % _scenario_id)
