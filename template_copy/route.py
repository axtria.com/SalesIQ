import logging

from flask import Flask,request
from main.template_copy import TemplateCopy
from main.util import fetch_parameter
from logging.handlers import TimedRotatingFileHandler

app = Flask(__name__)
PREFIX = '/org_transfer'
METHODS = ['POST','GET']

@app.route(PREFIX + "/TemplateCopy",methods=METHODS)
def template_copy():
    source_username   =  fetch_parameter(request, "source_username"   , default = '',      type = str)   #source org username
    source_password   =  fetch_parameter(request, "source_password"   , default = '',      type = str)   #source org password
    source_token      =  fetch_parameter(request, "source_token"      , default = '',      type = str)   #source org securitytoken
    source_sandbox    =  fetch_parameter(request, "source_sandbox"    , default = "False", type = str)   #source org sandbox (bool)
    dest_username     =  fetch_parameter(request, "dest_username"     , default = '',      type = str)   #destination org username
    dest_password     =  fetch_parameter(request, "dest_password"     , default = '',      type = str)   #destination org password
    dest_token        =  fetch_parameter(request, "dest_token"        , default = '',      type = str)   #destination org securitytoken
    dest_sandbox      =  fetch_parameter(request, "dest_sandbox"      , default = 'False', type = str)   #destination org sandbox (bool)
    is_source_managed =  fetch_parameter(request, "is_source_managed" , default = '',      type = str)   #is source org managed
    is_dest_managed   =  fetch_parameter(request, "is_dest_managed"   , default = '',      type = str)   #is destination org managed
    template_id       =  fetch_parameter(request, "template_id"       , default = '',      type = str)   #template ID in source org 
    is_rule_config    =  fetch_parameter(request, "is_rule_config"    , default = '',      type = str)   #preconfigured rules to be copied or not (bool)

    source_sandbox    = True if source_sandbox.lower() == "true" else False
    dest_sandbox      = True if dest_sandbox.lower() == "true" else False
    is_rule_config    = True if is_rule_config.lower() == "true" else False

    controller_obj    = TemplateCopy(source_username, source_password, source_token, source_sandbox, dest_username,dest_password,
                                     dest_token, dest_sandbox, is_source_managed, is_dest_managed, template_id, is_rule_config)
    output            = controller_obj.invoke()
    return output


@app.before_request
def log_request_info():
    logging.debug('URL HIT %r' % request.url)   #logs the URL  to log file

logging.basicConfig(level=logging.DEBUG,
                        handlers=[TimedRotatingFileHandler("logs//TemplateCopy.log", when="midnight"),
                                  logging.StreamHandler()],
                        format='%(asctime)s - %(module)s.%(funcName)s() - %(levelname)s - %(message)s')
logging.getLogger('requests').setLevel(logging.CRITICAL)
logging.getLogger('urllib3').setLevel(logging.CRITICAL)


if __name__ == '__main__':
    app.debug = True
    # logging.basicConfig(level=logging.DEBUG,
    #                     handlers=[TimedRotatingFileHandler("logs//TemplateCopy.log", when="midnight"),
    #                               logging.StreamHandler()],
    #                     format='%(asctime)s - %(module)s.%(funcName)s() - %(levelname)s - %(message)s')

    app.run()