import json


class MetaConfig(type):
    def __getitem__(cls, key):
        return cls()._config[key]


class CONFIG(metaclass=MetaConfig):
    static_config = {}

    def __init__(self):
        self._config = CONFIG.static_config

    @staticmethod
    def parse_config():
        with open("./config.json") as f:
            CONFIG.static_config = json.load(f)