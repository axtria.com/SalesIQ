import re
import logging

from pandas import merge

def map_id(dest_df, parent_column, source_df):
    logging.info("Mapping {}'s Ids from Source & Destination Orgs".format(parent_column))
    try:
        dest_df.drop("Name",axis =1, inplace = True)
    except:
        logging.warning("Dest_df has no column 'Name'")
    df = merge(source_df,dest_df, how = 'left',left_on = parent_column, right_on = 'old_id')
    df.drop(["old_id",parent_column], axis = 1, inplace = True)
    df.rename(columns = {"Id_y" : parent_column, "Id_x" : "Id"}, inplace = True)
    return df

def prepare_query(query: str, namespace) -> str:
        query = query.replace(namespace, "")
        for custom_object in re.findall('( [0-9aA-zZ_]+__c| [0-9aA-zZ_]+__r)', query):
            query = query.replace(custom_object, ' ' + prepare_object_name(custom_object[1:],namespace))
        for custom_object in re.findall('(\.[0-9aA-zZ_]+__c)', query):
            query = query.replace(custom_object, '.' + prepare_object_name(custom_object[1:],namespace))
        for custom_object in re.findall('(\.[0-9aA-zZ_]+__r)', query):
            query = query.replace(custom_object, '.' + prepare_object_name(custom_object[1:],namespace))
        return query

def prepare_object_name(object_name, namespace):
    if object_name.find(namespace) == -1 and (object_name.endswith('__c') or object_name.endswith('__r')):
        return namespace + object_name
    return object_name

def prepare_df(df, dest_namespace: str):
    df.columns = [dest_namespace+col if col.endswith('__c') else col for col in df.columns]
    logging.info("No. of records to upload %d" % len(df))
    return df

def remove_namespace(df,namespace):
    try:
        df.columns = df.columns.str.replace(namespace,"")
    except:
        logging.warning("Namespace Not found. No worries!")
    return df

def add_namespace_to_data(df, columns, source_namespace, dest_namespace):
    logging.debug("Namespace handling in the data itself")
    if source_namespace == dest_namespace:
        return df
    df.fillna('',inplace = True)
    logging.debug("Before namespace handling in data : \n\n {}".format(df.head()))
    for col in columns:
        logging.debug("Handling Column : {}".format(col))
        df[col] = df[col].str.replace(source_namespace, "")
        data = df[col].values.tolist()
        # logging.debug(" {} values = \n\n {}".format(col, data))
        df[col] = [prepare_query(" " + i.replace(';',', '), dest_namespace).replace(", ",";") for i in data]
        # df.loc[(df[col].str.endswith("__c")),col] = dest_namespace + df[col]
    logging.debug("After namespace handling in data : \n\n {}".format(df.head()))

    return df





# def attach_id(df):
#     if not df.empty:
#         df["Name"] = df["Id"] + df["Name"]
#         # df.drop('Id', axis=1, inplace = True)
#     else:
#         logging.info("Nothing to upload.")
#     return df

# def detach_id(dest_df):
#     dest_df['old_id'] = dest_df['Name'].str[0:18]
#     dest_df['Name']   = dest_df['Name'].str[18:]
#     # dest_df.drop(['id'], axis = 1, inplace = True)
#     return dest_df

# def prepare_keys(values, namespace):
#     result = {}
#     for key, value in values.items():
#         result[namespace + key] = value
#     return result

