import logging

from pandas import DataFrame
from .config_parser import CONFIG
from .data_processor import map_id, prepare_df, merge


class RuleSync:
    
    def __init__(self, dest_brtd_final, dest_ctm_final, dest_ds_final, dest_dscd_final, source_sf, 
                 dest_sf, template_id):
        self.template_id        = template_id
        self.dest_brtd_final    = dest_brtd_final
        self.dest_ctm_final     = dest_ctm_final
        self.dest_ds_final      = dest_ds_final
        self.dest_dscd_final    = dest_dscd_final
        self.source_salesforce  = source_sf
        self.dest_salesforce    = dest_sf

    def invoke(self):
        dest_brtm_final         = self.sync_business_rule_type_master()
        dest_br_final           = self.sync_business_rules(dest_brtm_final)
        dest_brjrsc_final       = self.sync_brjoinruleselectcondition(dest_br_final)
        dest_brjrc_final        = self.sync_br_join_rule_condition(dest_br_final)
        dest_brjr_final         = self.sync_br_joinrule(dest_br_final)
        dest_arl_final          = self.sync_aggregate_rule_level(dest_br_final)
        dest_ard_final          = self.sync_aggregate_rule_detail(dest_br_final)

    def sync_business_rule_type_master(self):        
        logging.info("\n syncing Business_Rule_Type_Master__c")
        source_brtm         = self.source_salesforce.execute_query(CONFIG["name_queries"]["source_brtm_query"])
        dest_brtm           = self.dest_salesforce.execute_query(CONFIG["name_queries"]["dest_brtm_query"])
        if not dest_brtm.empty:
            diff            = set(source_brtm["Name"]).difference(dest_brtm["Name"])
            if diff:
                final_brtm  = source_brtm.loc[source_brtm["Name"].isin(diff)]
            else:
                final_brtm  = DataFrame()
        else:
            final_brtm      = source_brtm.drop("Id",axis=1)  
        if not final_brtm.empty:     
            final_brtm      = merge(final_brtm, self.dest_ctm_final.loc[:,~self.dest_ctm_final.columns.isin(["Name"])],  
                                        left_on = "Component_Type__c", right_on = "old_id")
            final_brtm.drop(["Component_Type__c","old_id"],axis = 1, inplace = True)
            final_brtm.rename(columns = {"Id_x" : "Id", "Id_y" : "Component_Type__c"},inplace = True)
            final_brtm      = prepare_df(final_brtm, self.dest_salesforce.namespace)
            final_brtm.drop("Id",axis = 1, inplace =True)
            up_data         = final_brtm.to_dict(orient='records')
            response        = getattr(self.dest_salesforce.sf.bulk,self.dest_salesforce.namespace+"Business_Rule_Type_Master__c").insert(up_data)
            logging.debug("\n\n BRTM response \n\n {}".format(response))
        dest_brtm_query     = CONFIG["name_queries"]["dest_brtm_query"] + " WHERE "
        for name in source_brtm["Name"]:
            dest_brtm_query += " Name = '{}' OR ".format(name)
        dest_brtm_query     = dest_brtm_query[:-4]
        dest_brtm           = self.dest_salesforce.execute_query(dest_brtm_query)
        dest_brtm_final     = merge(dest_brtm,source_brtm[["Id","Name"]],on='Name')
        dest_brtm_final.rename(columns = {"Id_x": "Id","Id_y":"old_id"},inplace = True)
        return dest_brtm_final
                
    def sync_business_rules(self, dest_brtm_final):
        logging.info("\n syncing Business_Rules__c")
        br_query            = CONFIG["id_queries"]["source_br_query"].format(self.template_id)
        source_br           = self.source_salesforce.execute_query(br_query)
        final_br            = map_id(dest_brtm_final, "Business_Rule_Type_Master__c", source_br)
        final_br            = map_id(self.dest_brtd_final, "BusinessRuleTemplateDetailsId__c", final_br)
        return self.dest_salesforce.upload(final_br, "Business_Rules__c")
        
    def sync_brjoinruleselectcondition(self, dest_br_final):
        logging.info("\n syncing BRJoinRuleSelectCondition__c")
        brjrsc_query        = CONFIG["id_queries"]["source_brjrsc_query"] 
        for id in dest_br_final["old_id"]:
            brjrsc_query   += " Business_Rules__c = '{}' OR ".format(id)
        brjrsc_query        = brjrsc_query[:-4]
        source_brjrsc       = self.source_salesforce.execute_query(brjrsc_query)
        final_brjrsc        = map_id(self.dest_dscd_final,"Data_Set_Field__c", source_brjrsc)
        final_brjrsc        = map_id(dest_br_final, "Business_Rules__c", final_brjrsc)
        final_brjrsc        = map_id(self.dest_ds_final, "Data_Set__c", final_brjrsc)
        return self.dest_salesforce.upload(final_brjrsc.drop("Name",axis=1), "BRJoinRuleSelectCondition__c")

    def sync_br_join_rule_condition(self, dest_br_final):
        logging.info("\n syncing BR_Join_Rule_Condition__c")
        brjrc_query         = CONFIG["id_queries"]["source_brjrc_query"]
        for id in dest_br_final["old_id"]:
            brjrc_query    += " Business_Rules__c = '{}' OR ".format(id) 
        brjrc_query         = brjrc_query[:-4]
        source_brjrc        = self.source_salesforce.execute_query(brjrc_query)
        final_brjrc         = map_id(self.dest_dscd_final, "Data_Set_1_Field__c", source_brjrc)
        final_brjrc         = map_id(self.dest_dscd_final, "Data_Set_2_Field__c", final_brjrc)
        final_brjrc         = map_id(dest_br_final, "Business_Rules__c", final_brjrc)
        final_brjrc         = map_id(self.dest_ds_final, "Data_Set_1__c", final_brjrc)
        final_brjrc         = map_id(self.dest_ds_final, "Data_Set_2__c", final_brjrc)
        return self.dest_salesforce.upload(final_brjrc.drop("Name",axis=1), "BR_Join_Rule_Condition__c")

    def sync_br_joinrule(self, dest_br_final):

        logging.info("\n syncing BR_JoinRule__c")
        brjr_query          = CONFIG["id_queries"]["source_brjr_query"]
        for id in dest_br_final["old_id"]:
            brjr_query     += " Business_Rules__c = '{}' OR ".format(id) 
        brjr_query          = brjr_query[:-4]
        source_brjr         = self.source_salesforce.execute_query(brjr_query)
        final_brjr          = map_id(dest_br_final, "Business_Rules__c", source_brjr)
        return self.dest_salesforce.upload(final_brjr, "BR_JoinRule__c")

    def sync_aggregate_rule_level(self, dest_br_final):

        logging.info("\n syncing Aggregate_Rule_Level__c")      
        arl_query           = CONFIG["id_queries"]["source_arl_query"]
        for id in dest_br_final["old_id"]:
            arl_query      += " Business_Rules__c = '{}' OR ".format(id) 
        arl_query           = arl_query[:-4]
        source_arl          = self.source_salesforce.execute_query(arl_query)
        final_arl           = map_id(dest_br_final, "Business_Rules__c", source_arl)
        return self.dest_salesforce.upload(final_arl, "Aggregate_Rule_Level__c")

    def sync_aggregate_rule_detail(self, dest_br_final):

        logging.info("\n syncing Aggregate_Rule_Detail__c")
        ard_query           = CONFIG["id_queries"]["source_ard_query"]
        for id in dest_br_final["old_id"]:
            ard_query      += " Business_Rules__c = '{}' OR ".format(id) 
        ard_query           = ard_query[:-4]
        source_ard          = self.source_salesforce.execute_query(ard_query)
        final_ard           = map_id(dest_br_final, "Business_Rules__c",source_ard)
        return self.dest_salesforce.upload(final_ard, "Aggregate_Rule_Detail__c")