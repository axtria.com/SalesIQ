import logging

def fetch_parameter(request, param, default=None, type=str):
	val = default
	try:
		if request.method == 'POST':
			val = request.form[param]
		else:
			val = request.args.get(param, default, type)
	finally:
		logging.debug(param + ' : ' + str(val) )
		return val

