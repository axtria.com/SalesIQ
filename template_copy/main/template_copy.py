import logging
import traceback
import importlib
from pandas import DataFrame
from .rule_sync import RuleSync
import main.config_parser 
importlib.reload(main.config_parser)
from main.config_parser import CONFIG
from .salesforce import SalesforceConnector
from .data_processor import map_id, prepare_df, merge, add_namespace_to_data


class TemplateCopy:
    """
    Controller class that manages the execution flow. It is instantiated in the Flask file(route.py).
    
    """
    CONFIG.parse_config()
    def __init__(self, source_username: str, source_password: str, source_token: str, source_sandbox: bool, dest_username: str,
                 dest_password: str, dest_token: str, dest_sandbox: bool, is_source_managed: str, is_dest_managed: str, 
                 template_id: str, is_rule_config: bool):
        self.source_namespace  = "AxtriaSalesIQTM__" if is_source_managed.lower() == "true" else ''
        self.dest_namespace    = "AxtriaSalesIQTM__" if is_dest_managed.lower() == "true" else ''
        self.source_salesforce = SalesforceConnector(source_username, source_password, source_token, source_sandbox, self.source_namespace)
        logging.info("Connection to source-org Established.")
        self.dest_salesforce   = SalesforceConnector(dest_username, dest_password, dest_token, dest_sandbox, self.dest_namespace)
        logging.info("Connection to destination-org Established.")
        self.template_id       = template_id
        self.is_rule_config    = is_rule_config

    def invoke(self):
        """
        Controller function. Starting point. All other functions invoked through here.
        """
        try:
            check = self.execution_checks()
            if check:
                return check
            
            dest_ctm_final,source_ctm   = self.sync_componenttypemaster()
            dest_brt_final,source_brt   = self.sync_business_rule_template()
            dest_brtd_final,source_brtd = self.sync_businessrule_template_details(dest_ctm_final,dest_brt_final)
            dest_ds_final,source_ds     = self.sync_data_set()
            self.sync_data_object(dest_ds_final)
            dest_dsrm_final,source_dsrm = self.sync_dataset_rule_map(dest_brtd_final, dest_ds_final)
            dest_dscd_final,source_dscd = self.sync_dataset_column_details(dest_ds_final,source_ds)
            
            if self.is_rule_config:
                logging.info("Beginning pre-configured rule sync")
                rule_sync               = RuleSync(dest_brtd_final, dest_ctm_final, dest_ds_final, 
                                                   dest_dscd_final, self.source_salesforce, self.dest_salesforce,
                                                   self.template_id)
                rule_sync.invoke()    
            else:
                logging.info("Skipping pre-configured rule sync")
            return "Successfully Completed"
        except Exception as e:
            logging.exception("Exception Occured")
            return traceback.format_exc()

    def execution_checks(self):
        source_template_id_query = "SELECT Id, Name from Business_Rule_Template__c"
        source_template = self.source_salesforce.execute_query(source_template_id_query)
        if self.template_id not in source_template["Id"].values.tolist():
            logging.info("Given Template Id not found in Source org. Halting service execution.")
            return "Given Template Id not found in Source org. Halting service execution."
        name_query      = "SELECT Id, Name FROM ComponentTypeMaster__c"   
        source_names    = self.source_salesforce.execute_query(name_query)
        if not len(source_names):
            logging.info("ComponentTypeMaster__c object empty in source org. Halting execution")
            return "ComponentTypeMaster__c object empty in source org. Halting execution"
        
        # dest_template_name_query = "SELECT Name from BusinessRule_Template_Details__c"
        # df = self.dest_salesforce.execute_query(dest_template_name_query)
        # if name in df["Name"].values.tolist():
        #     logging.info("Template with the same name {},  found in the destination org. Halting service execution".format(name))
        #     return "Template with the same name {},  found in the destination org. Halting service execution".format(name)

        # dest_dataset_name_query = "SELECT Dataset_Id__c, Dataset_Id__r.Name FROM Data_Set_Rule_Map__c \
        #                            where  Business_Rule_Template_Details__r.Business_Rule_Template_Id__c = '{}'"format(self.template_id)
        # ds_dest = self.dest_salesforce.lookup_query(dest_dataset_name_query)
        # source_dataset_name_query = "SELECT "
        return ''
        
    def sync_componenttypemaster(self):
        logging.info("Syncing ComponentTypeMaster__c")
        source_ctm          = self.source_salesforce.execute_query(CONFIG["name_queries"]["source_ctm_query"])
        dest_ctm            = self.dest_salesforce.execute_query(CONFIG["name_queries"]["dest_ctm_query"])
        if not dest_ctm.empty:
            diff            = set(source_ctm["Name"]).difference(dest_ctm["Name"])
            if diff:
                final_df    = source_ctm.loc[source_ctm["Name"].isin(diff)]
            else:
                final_df    = DataFrame()
        else:
            final_df        = source_ctm.drop("Id",axis=1)
        if not final_df.empty:
            final_df        = prepare_df(final_df, self.dest_namespace)
            response        = getattr(self.dest_salesforce.sf.bulk, self.dest_namespace+"ComponentTypeMaster__c").insert(final_df.to_dict(orient='records'))
        dest_idquery        = CONFIG["name_queries"]["dest_ctm_query"] + " WHERE "
        for name in source_ctm["Name"]:
          dest_idquery     += " Name = '{}' OR ".format(name)
        dest_idquery        = dest_idquery[:-4]
        dest_id             = self.dest_salesforce.execute_query(dest_idquery)
        dest_ctm_final      = merge(dest_id, source_ctm[["Id", "Name"]], on='Name', how='left')
        dest_ctm_final.rename(columns = {"Id_y" : "old_id", "Id_x" : "Id"},inplace = True)
        return dest_ctm_final, source_ctm
        
    def sync_business_rule_template(self):
        logging.info("Syncing Business_Rule_Template__c")
        brt_query       = CONFIG["id_queries"]["source_brt_query"].format(self.template_id) 
        source_brt      = self.source_salesforce.execute_query(brt_query)
        return self.dest_salesforce.upload(source_brt,"Business_Rule_Template__c"),source_brt
            
    def sync_businessrule_template_details(self, dest_ctm_final, dest_brt_final):
        logging.info("Syncing BusinessRule_Template_Details__c")
        brtd_query      = CONFIG["id_queries"]["source_brtd_query"].format(self.template_id)
        source_brtd     = self.source_salesforce.execute_query(brtd_query)
        final_df        = map_id(dest_ctm_final, "Component_Type_Id__c", source_brtd)
        final_df        = map_id(dest_brt_final, "Business_Rule_Template_Id__c", final_df)
        return self.dest_salesforce.upload(final_df, "BusinessRule_Template_Details__c"),final_df
            
    def sync_data_set(self):
        logging.info("Syncing Data_set__c")
        data_set_id_query   = CONFIG["id_queries"]["filter_ds_query"].format(self.template_id)
        source_ds_id        = self.source_salesforce.lookup_query(data_set_id_query)
        data_set_query      = CONFIG["id_queries"]["source_ds_query"]
        for id in source_ds_id["dataset_id__c"]:
            data_set_query += " Id = '{}' OR ".format(id)
        data_set_query      = data_set_query[:-4]
        source_ds           = self.source_salesforce.execute_query(data_set_query)
        source_ds          = add_namespace_to_data(source_ds, CONFIG["namespace"]["ds_fields"].split(","),
                                                    self.source_namespace, self.dest_namespace)
        return self.dest_salesforce.upload(source_ds, "Data_Set__c"),source_ds
            
    def sync_data_object(self,dest_ds_final):
        logging.info("Syncing Data_Object__c")
        data_object_query      = CONFIG["id_queries"]["source_do_query"]
        for id in dest_ds_final["Id"]:
            data_object_query += " Id = '{}' OR ".format(id)
        data_object_query      = data_object_query[:-4]+")"
        dest_ds                = self.dest_salesforce.execute_query(data_object_query)
        dest_ds.rename(columns = {"Id":"dataset_id__c"},inplace = True)
        dest_ds["status__c"]   = "Success"
        dest_ds                = prepare_df(dest_ds, self.dest_namespace)
        response               = getattr(self.dest_salesforce.sf.bulk, self.dest_namespace+"Data_Object__c").insert(dest_ds.to_dict(orient='records'))
        logging.debug("\n {}".format(response))

    def sync_dataset_rule_map(self,dest_brtd_final, dest_dataset_final):
        logging.info("Syncing Data_Set_Rule_Map__c")
        dsrm_query          = CONFIG["id_queries"]["source_dsrm_query"].format(self.template_id)
        source_dsrm         = self.source_salesforce.execute_query(dsrm_query)
        final_df            = map_id(dest_brtd_final, "Business_Rule_Template_Details__c", source_dsrm)
        final_df            = map_id(dest_dataset_final, "dataset_id__c", final_df)
        return self.dest_salesforce.upload(final_df, "Data_Set_Rule_Map__c"),final_df


    def sync_dataset_column_details(self,dest_ds_final,source_ds):
        logging.info("Syncing Data_Set_Column_Details__c")
        dscd_query          = CONFIG["id_queries"]["source_dscd_query"]
        for id in source_ds["Id"]:
            dscd_query     += " dataset_id__c = '{}' OR ".format(id)
        dscd_query          = dscd_query[:-4]
        source_dscd         = self.source_salesforce.execute_query(dscd_query)
        final_dscd          = map_id(dest_ds_final, "dataset_id__c", source_dscd)
        final_dscd          = add_namespace_to_data(final_dscd, CONFIG["namespace"]["dscd_fields"].split(","),
                                                    self.source_namespace, self.dest_namespace)
        return self.dest_salesforce.upload(final_dscd, "Data_Set_Column_Detail__c"),final_dscd
        
#     def _retrieve_fields(self, object_name, org):
#         logging.info("Retrieving fields for {}".format(object_name))
#         details = getattr(self.source_salesforce.sf, object_name).describe()
#         fields  = []
#         parents = []
#         for field in details["fields"]:
#             fields.append(field["name"]) if field["updateable"] else ''
#             parents.append(field["name"]) if field["referenceTo"] and field["updateable"] else ''
#         return fields,parents

# ###################################################################################################################################

#     def prepare_dest_df(self, object_name, final_df):
#         dest_query      = "SELECT Id, Name FROM {} WHERE ".format(object_name)
#         for name in final_df["Name"]:
#             dest_query += " Name = '{}' OR ".format(name)
#         dest_query      = dest_query[:-4]
#         dest_df         = self.dest_salesforce.execute_query(dest_query)
#         dest_df_final   = detach_id(dest_df)
#         logging.info("Updating Names")
#         up_data         = dest_df_final.drop("old_id",axis = 1)
#         up_data         = prepare_df(up_data,self.dest_namespace)
#         up_data         = up_data.to_dict(orient = 'records')
#         getattr(self.dest_salesforce.sf.bulk,self.dest_namespace+object_name).update(up_data)
        # logging.info("prepare_dest_df \n\n {}".format(dest_df_final))
        # self.dest_salesforce.bulk_update(object_name, dest_df_final.loc[:,~dest_df_final.columns.isin(["old_id"])])
        # return dest_df_final