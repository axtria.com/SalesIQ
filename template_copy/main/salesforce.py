import re
import logging
import pandas as pd

from collections import defaultdict
from simple_salesforce import Salesforce
# from salesforce_bulk import SalesforceBulk, CsvDictsAdapter
from .data_processor import prepare_query, remove_namespace, prepare_df


class SalesforceConnector:
    def __init__(self, username: str, password: str, security_token: str, sandbox: bool, namespace: str):
        self.namespace = namespace
        self.sf      = Salesforce(username=username, password=password, security_token=security_token, sandbox=sandbox)
        # self.bulk_sf = SalesforceBulk(username=username, password=password, security_token=security_token, sandbox=sandbox)

    def execute_query(self, query: str):
        prepared_query = prepare_query(query, self.namespace)
        logging.info('\n\n %s \n' % prepared_query)
        dic = self.sf.query_all(prepared_query)
        df =  pd.DataFrame(dic['records'])
        logging.info("No of records retrieved = {}".format(len(df)))
        df = remove_namespace(df,self.namespace)
        try:
            df.drop("attributes", axis = 1, inplace = True)
        except Exception as e:
            logging.warning("Attribute column not found in DF")
        return df

    def lookup_query(self, query: str):
        df = self.execute_query(query)
        dct = defaultdict(list)
        for i in re.findall('[0-9aA-zZ]+__r+\.[0-9aA-zZ]+', query.split("FROM")[0]):
            dct[i.split(".")[0]].append(i.split(".")[1])
        for col in df.columns:
            dct1 = defaultdict(list)
            if col.endswith("__r"):
                for index,row in df.iterrows():
                    for i in dct[col]:
                        if row[col]:
                            dct1[col+"."+i].append(row[col][i])
                        else:
                            dct1[col+"."+i].append('')
            for c in dct1:
                df[c] = dct1[c]
        for col in dct:
            try:
                df.drop(col, axis = 1, inplace = True)
            except:
                pass
        return df

    def upload(self,final_df,object_name):
        final_df.fillna('',inplace=True)
        dest_df_final           = pd.DataFrame()
        dest_df_final["old_id"] = final_df['Id']
#        final_df.drop("Id",axis = 1, inplace=True)
        final_df                = prepare_df(final_df.drop("Id",axis=1), self.namespace)
        upload_data             = final_df.to_dict(orient = 'records')
        response                 = getattr(self.sf.bulk,self.namespace + object_name).insert(upload_data)
        logging.info("{} uploaded successfully!".format(object_name))
        logging.debug("\n {}".format(response))
        id = []
        for res in response:
            id.append(res["id"])
        logging.debug("\n\n Length of source ids = {}".format(len(dest_df_final["old_id"])))
        logging.debug("\n Length of response ids = {}".format(len(id)))
        dest_df_final["Id"]     = id
        logging.info("Mapped {} ids from Source & destination Orgs".format(object_name))
        return dest_df_final